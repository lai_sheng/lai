#include <signal.h>
#include <unistd.h>
#include <sys/reboot.h>
#include "System/UserManager.h"
#include "MultiTask/Semaphore.h"
#include "System/Log.h"
//#include "Devices/DevGraphics.h"
#include <list>
#include <deque>
#include <vector>

#include "Configs/ConfigManager.h"
#include "APIs/Application.h"
#include "APIs/Net.h"
#include "Devices/DeviceManager.h"
#include "Functions/General.h"
#include "Functions/DriverManager.h"
//#include "Functions/Display.h"
#include "Functions/Alarm.h"
#include "Functions/Ptz.h"
#include "Functions/Comm.h"
#include "Functions/AutoMaintain.h"
#include "Functions/Daylight.h"
#if defined (_USE_720P_MODULE_) 
#include "Functions/Camera.h"
#endif

#include "Functions/Encode.h"
#include "Functions/Record.h"
#include "Functions/Snap.h"
#include "Functions/LedCtrl.h"//chenzj debug

#include "Functions/AudioManager.h"
#include "Main.h"

#ifdef _PF_DM365_SERIES_
#include "nipdebug.h"
#endif

#ifdef FUNC_WDT
#include "Functions/WatchDogTime.h"
#endif


#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"

#include "System/Packet.h"
#include "System/Log.h"
#include "System/CMOS.h"
#include "System/AppConfig.h"
//#include "AuxLibs/Lua/ScriptEngine.h"
#include "System/Version.h"
#include "System/UserManager.h"
#include "System/Console.h"

#include "Net/NetWorkService.h"
#if  defined(POLICE_PROJECT)    
#include "Net/CNetWorkClient.h"
#endif

#if defined(USING_PROTOCOL_NTP)
#include "Net/Dlg/DlgNtpCli.h"
#endif

#include "Configs/ConfigNet.h"
#include "Net/NetApp.h"
//#include "Functions/Preview.h"

#ifdef __INTERVIDEO_
    #include "../Intervideo/InterVideo.h"
#endif
#ifdef TUPU
//////////////////////TUPU////////////////////////////////
#include "Intervideo/Tupu/TupuManager.h"
/////////////////////////////////////////////////////////
#endif
#include "Configs/ConfigDigiChManager.h"

#include "Intervideo/DevSearch/DevSearch.h"
#ifdef BPI_S2L
#include "Functions/FFFile.h"
#endif

#include "Functions/WifiLink.h"
#include "Devices/DevExternal.h"

#include "Functions/DaemonManager.h"

//#ifdef DOOR_LOCK
#include "Functions/IntelligentDoorLock.h"
//#endif

#include "APIs/CommonBSP.h"
#include "Functions/WatchDogTime.h"

#include "Functions/PtzTrace.h"
#include "Functions/AlarmDetect.h"
#include "Functions/AiUploadManager.h"
#include "Functions/LedStat.h"

SYSTEM_CAPS_EX        g_CapsEx;//全局扩展的性能结构
int                    g_ShieldUSBDev;            //是否屏蔽usb设备备份
SYSTEM_TIME         g_StartTime;                  //开机时间
int 				g_iSIGSEGV; 

static char* web_lang_file[LANG_NR] = {
    "webEN.lang", "webCN.lang", "webCNT.lang", "webIT.lang",
    "webES.lang", "webJP.lang", "webRU.lang", "webFR.lang",
    "webDe.lang","webPT.lang","webTR.lang",
    "webPL.lang","webRO.lang","webHU.lang","webFI.lang",
    "webEE.lang","webKR.lang","webFas.lang","webDK.lang",
};

#ifdef WIN32
extern "C"
{
    void InitApiLib();
}
#elif defined(LINUX)
void SignalFunc(int argc)
{
}
#endif

extern std::string g_strDefaultHostIp;
extern std::string g_strDefaultNetMask;
extern std::string g_strDefaultGateway;
extern int g_iFirstUseDefaultIp;

int  CheckJffs2File(char* file1)
{
	int ret=0;
	FILE* fd=NULL;
    fd=fopen(file1,"rb");
	if(fd==NULL)		
	{
		infof("open faile  %s \r\n",file1);
		return 0;
	}
	fseek(fd,0,SEEK_END);	
	int length=ftell(fd);		
	fseek(fd,0,SEEK_SET);	
	char* buffer=(char*)malloc(length);			
	int readlen=fread(buffer,1,length,fd);	
	if(readlen!=length)	
	{		
		remove(file1);					
		infof("\r\n@@2@@@@@@remove file @@@%s@\\r\n",file1);	
		ret=1;
	}
	free(buffer);
	fclose(fd);
	return ret;
}
void FlashCheckJffs2()
{
	/**********************************************************************
	规避异常断电flash丢失问题:
	异常断电导致的flash丢失问题，目前读写操作都是发生在mnt分区
	主要有三个文件cmos.cfg  Log  Config1 这三个文件操作频繁，也是最容易异常断电导致文件破坏的区域
	**************************************************************************/
	//!原始文件
	extern char FileLogName[VD_MAX_PATH];
	extern char configFileFirst[VD_MAX_PATH];
	extern char configDigiChFileFirst[VD_MAX_PATH];
	extern char configFileSecond[VD_MAX_PATH];
	extern char configDigiChFileSecond[VD_MAX_PATH];
	char FileCmosName[VD_MAX_PATH]=CONFIG_DIR"/cmos.cfg";
	
	int ret=0;
	ret |= CheckJffs2File(FileLogName);
	ret |= CheckJffs2File(configDigiChFileFirst);
	ret |= CheckJffs2File(configFileFirst);
	ret	|= CheckJffs2File(configDigiChFileSecond);
	ret	|= CheckJffs2File(configFileSecond);
	ret	|= CheckJffs2File(FileCmosName);
	if(ret==1)
	{
    #ifndef WIN32
		sleep(1);
    #endif
		SystemReboot();
	}
}

PATTERN_SINGLETON_IMPLEMENT(CChallenger);

int g_SystemAuthFail = 0;

CChallenger::CChallenger():m_TimerAlive("KeepAlive"),m_cTimer("SwitchVga"),m_timerReboot("Reboottimer"),CThread("CChallenger", TP_INIT)
{
    int nRet;
    m_BInitializeState = VD_FALSE;
    
    trace("CChallenger::CChallenger()>>>>>>>>>\n");
//    ScriptEngine *luaEngine = ScriptEngine::Instance();
#if 0
	nRet = luaEngine->LoadFile("/app/ipcam/lua/init.lua");
    
    //CMOS初始化
    //g_Cmos.Initialize(); //jsk

    OnStartAlive();//challenger一起来就要先喂狗一次，否则机器可能在加载dsp的时候就重启了。
#endif	
}

CChallenger::~CChallenger()
{
}

extern std::string INI_DEFAULT_USER_NAME;
extern std::string INI_DEFAULT_USER_PWD;

void CChallenger::Initialize(int argc, char * argv[])
{        
    //setbuf(stdout, NULL);//将标准输出设为0缓冲
    AppConfig * pAppConfig = AppConfig::instance();
#if 0
    /* 启动时进行语言校验 */
    int  iLanguage = SystemGetUILanguage(); 

    int iSupportLan = AppConfig::instance()->getNumber("Global.Language", 0x87);
    int iDefaultLan = AppConfig::instance()->getNumber("Global.LanguageDefault", 0x0);
	printf ("[%s][%d]Start #############################\n", __FILE__, __LINE__);
    /* 设置的语言为不支持的语言，修改成默认的语言 */
    if (!(iSupportLan & (1<<iLanguage)))
    {
        printf("not support the language = %d\r\n",iLanguage);
            
        /* 针对部分产品可能写不成功的问题，多写几次 */
        for (int i=0;i<10;i++)
        {
            SystemSetUILanguage((ui_language_t)iDefaultLan);
            if ((ui_language_t)iDefaultLan != SystemGetUILanguage())
            {
                printf("###SystemSetUILanguage failed,iDefaultLan  =  %d\r\n",iDefaultLan);
                SystemSleep(10);
            }
            else
            {
                break;
            }
        }
    }

    g_ShieldUSBDev = (int)pAppConfig->getNumber("Global.Hardware.usbbackupdev", 1);

#endif
    //int totalsize = (int)AppConfig::instance()->getNumber("Global.Memory.PacketBufSize", PKT_KILOS_DEFAULT) * 1024;

#ifdef IPC_JZ
	#ifdef BCAM_FACTORY_VERSION
	int totalsize = 4*1024*1024;
	#else
	int totalsize = 2*1024*1024;
	#endif
#else
	#ifdef TUPU
	int totalsize = 2*1024*1024;
	#else
	int totalsize = 2*1024*1024;
	#endif
#endif
	CPacketManager::config(totalsize, PKT_PAGE_SIZE);
	
	//_printd("################################totalsize = %d\n", totalsize);

    //!控制packet所使用的内存，是否使用内存延迟策略，对内存较少的产品最好打开，以便于内存合理管理
    //!增加产品的稳定性
#ifdef _DISALBE_PACKET_MEMORY_DELAY
    CPacketManager::MemoryDelay(false);
#else
	CPacketManager::MemoryDelay(true);
#endif
	_printd("Total Memory:%d kb",g_PacketManager.GetBufferSize());
    //得到系统扩展性能
//    memset(&g_CapsEx, 0, sizeof(g_CapsEx));
    SystemGetCapsEx(&g_CapsEx);
#if 0
	printf(">>>>>>>>>>>>>has audio %d , MatrixBoard has %d loop, VNMatrixBoard has %d.\n",
		g_CapsEx.HasAudioBoard,g_CapsEx.HasMatrixBoard, g_CapsEx.HasVNMatrixBoard);
#endif
    g_strDefaultHostIp  = pAppConfig->getString("Global.DefaultHostIp", "192.168.1.88");
    g_strDefaultNetMask = pAppConfig->getString("Global.DefaultNetMask", "255.255.255.0");
    g_strDefaultGateway = pAppConfig->getString("Global.DefaultGateway", "192.168.1.1");
    g_iFirstUseDefaultIp = (int)pAppConfig->getNumber("Global.UseDefaultIP", 1);

    g_ThreadManager.RegisterMainThread(ThreadGetID());    // 注册主线程
	g_TimerManager.Start();

    g_DeviceManager.Start();

	CDlgNtpCli::instance();
	//g_NtpClient.Start();
	ServerInit();	

   // jsk g_Ptz.StartSemiduplex();    //主要是因为必须在Alarm运行之后才能启动
//#if defined(IPC_HISI) ||defined(IPC_A3) ||defined(IPC_P2) ||defined(IPC_JZ_NEW)
	g_WifiLink.Start();
//#endif



#ifdef __INTERVIDEO_ 
    g_InterVideo.Start(0, NULL);
#endif

//	g_AiUpload.Start();	
	g_AlarmDetect.Start();

#ifdef MOTOR
	g_PtzTrace.Start();
#endif

    char version[32];
    //获得版本号
    CVersion::instance()->GetVersion(version, sizeof(version));

    //记录启动日志
    SYSTEM_TIME time;
	//打印启动信息
    SystemGetCurrentTime(&time);
    infof("Challenger[%s] Launched at Local Time %d/%d/%d %02d:%02d:%02d!\n",version,
        time.year, time.month, time.day, time.hour, time.minute, time.second);

	//测试热插拔
   //  g_HotPlug.start();
    //停止保活定时器
    m_TimerAlive.Stop();
    g_Challenger.g_UpgradeOK = FALSE;
	
    g_ThreadManager.DumpThreads();
    
    m_BInitializeState = VD_TRUE;
#ifndef WIN32
	g_DevTools.Start();
#endif
}
void CChallenger::ServerInit()
{	
	m_bLoop = 1;
	CreateThread(); 
}
void CChallenger::ThreadProc()
{
	pthread_detach(pthread_self());
#ifdef IPC_JZ
    g_Record.Start();            
#endif 

    #ifndef SYS_NO_RECORD//将该宏定义转移到DriverManager.cpp
    g_DriverManager.Start();          
    #endif
	g_DaemonManager.Start();

	//!将耗时部分挪到后面，让logo先出来
  	g_Config.initialize();        //启动配置

	//!包含数字通道的宏
	g_ConfigDigiCh.initialize();        //启动配置

#if defined(IPC_JZ) //&& defined(LIVECAM)
	g_DevExternal.Start();
#endif

#ifdef VN_IPC
	//判断是否按了RESET键，如果按了，恢复默认值 ,added by billhe at 2009-8-28
	if (CONFIG_RESUME_YES == SystemGetConfigResume())
	{
	    g_Config.SetDefaultConfig(DEFAULT_CFG_ALL); //全部恢复默认会重启，所以放后面
		g_userManager.setDefaultPwd();
	    Reboot();
	}
#endif
#if defined (_USE_720P_MODULE_) 
	g_Camera.Start();
#endif

	ICaptureManager::instance();
	
//	setenv("TZ", "GMT", 1);//CST-8 GMT-8
//	tzset();               

	//启动编码
  	g_Encode.Start();           

    //g_Console.Start();            //启动控制台
    //g_Comm.Start();                //启动显示功能

	//g_AutoMaintain.Start();     //启动自动维护功能
	//g_Daylight.Start();            //启动夏令时校正功能

#if defined(USING_PROTOCOL_NTP) && !defined(_WIN32)
    g_NtpClient.Start();
#endif

#ifndef IPC_JZ
#ifndef SYS_NO_RECORD
//	#ifndef FAC_CHECK	//T20合一版本产测屏蔽录像功能20180821
//    g_Record.Start();            //启动录像功能, 必须在报警模块之前，因为报警会调用录像
//	#endif
#endif
#endif

    //为了保证录像模块在报警模块前面，而开机启动过长，先开启GUI
	g_localUser.login(INI_DEFAULT_USER_NAME, INI_DEFAULT_USER_PWD, "WEB");	// login as dbg usr
	
    /* 目前有时候会修改ID。避免修改ID的时候，权限存在问题，预先按照最大
       通道数进行处理 */
    GetAuthority(N_SYS_CH); //获得登录用户的权限列表
    g_userManager.start();        // 启动用户管理模块，这里实际上只是用来生成默认配置。
    GetAuthority(ICaptureManager::instance()->GetLogicChnNum()); //获得登录用户的权限列表
    g_General.Start();            //启动普通功能，主要是读取一些重要配置，设备可能会用到这些配置



   g_AudioManager.init(); 


//#ifndef VN_IPC	//IPC也有硬盘报警功能 ilena zhou 2010-06-18
#if !defined (_PF_DM365_SERIES_) && !defined(_PRODUCT_2900)  //2800，2900暂时屏蔽硬盘报警功能
	g_DriverManager.StartHddAlm();        //在报警模块之后启动，初始化硬盘报警;
#endif

    g_NetApp.Start();      //网络功能要在所有网络服务前启动

    g_Net.Run(0, NULL);            //start net module
    
#if defined(POLICE_PROJECT) 
    g_NetClient.Run(0, NULL);
#endif
	
#ifdef TUPU
/////////////////////TUPU////////////////////
	g_Tupu.Start();
////////////////////////////////////////////
#endif	

    // httpd
#ifndef USE_INTERNAL_HTTPD
    WebStart(CConfigNetCommon::getLatest().HttpPort, CConfigNetCommon::getLatest().TCPPort,
        CConfigLocation::getLatest().iLanguage, WEB_DIR,
        web_lang_file[MIN(CConfigLocation::getLatest().iLanguage, LANG_NR - 1)]);
#endif

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)//启动wifi
	g_NetApp.ReStartWifi();
#endif

#ifndef VFWARE_DEBUG
#if defined(FUNC_WDT) && defined(IPC_JZ_NEW)
	g_WatchDogTime.Start();//lhc add T20 watchdog 
#endif
#endif

#ifdef DOOR_LOCK
	g_DoorLockManager.Start();
#endif

	//led
	g_ledstat.Start();
	//g_SdLog.Start();//lhc

}

VD_BOOL CChallenger::GetInitializeState(void)
{
    return m_BInitializeState;
}
void CChallenger::onTimerReboot(uint arg)
{

   Reboot(0);
}
    
void CChallenger::OnStartAlive()
{
#if !defined(FUNC_FRONTBOARD_CTRL)
    CDevFrontboard *pDev = CDevFrontboard::instance();
    pDev->OnStartAlive();
#endif
}

//关机的时候需要停止保活命令，否则机器会不断重启。针对老的前面板程序，新的前面板程序已经解决了这个问题。
void CChallenger::OnStopAlive()
{
#if !defined(FUNC_FRONTBOARD_CTRL)
    CDevFrontboard *pDev = CDevFrontboard::instance();
    pDev->OnStopAlive();
#endif
}


void CChallenger::Cleanup()
{
	//如果升级成功，则重启时不保存配置
	if (FALSE == g_UpgradeOK)
	{
		g_Config.saveFile();//否则在采用延时保存配置的情况下会丢配置 modified wangqin 20070726
	}
    g_Cmos.SetExitState(0);
    g_Record.Stop();
    g_Comm.ResetComm();

}

void CChallenger::Restart()
{
    trace("Challenger is restarting...\n");
#ifdef __INTERVIDEO_
        g_InterVideo.Stop(0, NULL);
#endif
    Cleanup();
    ApplicationRestart();
}

void CChallenger::Exit()
{
    trace("Challenger is exiting...\n");
#ifdef __INTERVIDEO_
    g_InterVideo.Stop(0, NULL);
#endif
    Cleanup();
    ApplicationExit();
}


void CChallenger::Shutdown()
{
    trace("Challenger is shutting down...\n");

#ifdef __INTERVIDEO_
    g_InterVideo.Stop(0, NULL);
#endif
    Cleanup();

    SYSTEM_TIME systime;
	DHTIME dhtime;
	SystemGetCurrentTime(&systime);
    timesys2dh(&dhtime, &systime);
    g_Cmos.SetExitTime(&dhtime);
	g_Log.Append(LOG_SHUT, 0, &dhtime, sizeof(DHTIME));
	SystemShutdown();
	#if !defined(FUNC_FRONTBOARD_CTRL)
	    CDevFrontboard *pDev = CDevFrontboard::instance();
	    pDev->OnStopAlive();
	    pDev->Shutdown();
	#endif
}

void CChallenger::Reboot(int iSec,int Flag/*=0*/)
{
    //延时重启
    if (iSec > 0)
    {
        m_timerReboot.Start(this, (VD_TIMERPROC)&CChallenger::onTimerReboot, iSec*1000, 0, 0, 0);
        return;
    }
	g_WatchDogTime.SetRebootFlag(2);//不喂狗 add by jwd on 20181213	
	g_Record.StopRec(REC_MAN, 0);
    trace("vfware is rebooting...\n");
	char ReStartStion[150] = {0};
	if(1 == g_RecordStatus )
	{
		printf("##Upgrading...StopManulRecord##\n");
		g_Record.StopRec(REC_MAN, 0);
	}


	sync(); // 同步磁盘数据,将缓存数据回写到硬盘,以防数据丢失[luther.gliethttp]
	sleep(2);
//	reboot(RB_AUTOBOOT);
	int RebootFlag = Flag;
	g_Cmos.Write(CMOS_REBOOTFLAG,&RebootFlag,4);
//	sprintf(ReStartStion,"reboot");
//	printf("%s\n",ReStartStion);
//	execl("/bin/sh","sh","-c",ReStartStion,(char*)0);
	SYSTEM_TIME systime;
	DHTIME dhtime;
	SystemGetCurrentTime(&systime);
    timesys2dh(&dhtime, &systime);
    g_Cmos.SetExitTime(&dhtime);
	g_Log.Append(LOG_SHUT, Flag, &dhtime, sizeof(DHTIME));	
#ifdef __INTERVIDEO_
    g_InterVideo.Stop(0, NULL);
#endif
	reboot(RB_AUTOBOOT);//::此函数可能卡死,卡死后用看门狗重启
//直接这样退出用看门狗来实现重启
	exit(0);
	return;
    Cleanup();

    SystemReboot();
	#if !defined(FUNC_FRONTBOARD_CTRL)
	    CDevFrontboard *pDev = CDevFrontboard::instance();
	    pDev->OnStopAlive();
	    pDev->Reboot();
	#endif
}

void CChallenger::Suspend()
{
	ICapture::instance(0)->SetSuspend(1);
	CDevAudioIn::instance(0)->SetSuspend(1);
}
void CChallenger::Resume()
{
	ICapture::instance(0)->SetSuspend(0);
	CDevAudioIn::instance(0)->SetSuspend(0);
}
void CChallenger::RebootForUsbConfig()
{    
    g_Cmos.SetExitState(0);
    g_Record.Stop();
    g_Comm.ResetComm();

    SystemReboot();

	#if !defined(FUNC_FRONTBOARD_CTRL)
	    CDevFrontboard *pDev = CDevFrontboard::instance();
	    pDev->OnStopAlive();
	    pDev->Reboot();
	#endif
}

void CChallenger::GetAuthority(int logic_num)
{    
#ifndef WIN32
	int authNums = (2+COMMON_AUTHORITYNUMS  + logic_num * 3 + 1);
#else
	int authNums = (COMMON_AUTHORITYNUMS  + N_SYS_CH * 4 + 1);
#endif

	void *adminAuth = malloc (authNums * AUTHORITY_STRING_LEN * sizeof (char ));
	void *userAuth = malloc (authNums * AUTHORITY_STRING_LEN * sizeof (char ));

	memset(adminAuth,'\0',authNums * AUTHORITY_STRING_LEN);
	memset(userAuth,'\0',authNums * AUTHORITY_STRING_LEN);

	const char *generalAdminAuthorityFront[] =
	{
	    "CtrlPanel", "ShutDown"
	};

	const char *generalAdminAuthorityBack[] =
	{
	    "RecordMode", "Backup", 
	    "StorageManager", "PTZControl", "Account", "SysInfo", 
	    "AlarmIO", "QueryLog", "DelLog", "SysUpgrade", 
	    "AutoMaintain","GeneralConfig", "EncodeConfig", "RecordConfig", 
	    "CommConfig", "NetConfig", "VideoConfig", 
	    "PtzConfig", "OutputConfig", "DefaultConfig", "DigitalConfig"
	};

	const char *generalUserAuthorityFront[] = 
	{
	    "CtrlPanel"
	};

	const char *generalUserAuthorityBack[] = 
	{
		""
	};

	int iAdminIdx = 1;
	int iUserIdx = 1;
	uint i;

    //将公共的管理员和用户权限分别拷贝到总的管理员和用户权限数组
	for(i = 0; i < sizeof(generalAdminAuthorityFront)/sizeof(generalAdminAuthorityFront[0]); i++)
	{
		memcpy((char *)adminAuth + iAdminIdx * AUTHORITY_STRING_LEN * sizeof(char),generalAdminAuthorityFront[i],strlen(generalAdminAuthorityFront[i]));
		iAdminIdx++;
	}
	for (i = 0; i < sizeof(generalUserAuthorityFront) / sizeof (generalUserAuthorityFront[0]); i++ )
	{
		memcpy((char *)userAuth + iUserIdx * AUTHORITY_STRING_LEN * sizeof(char),generalUserAuthorityFront[i],strlen(generalUserAuthorityFront[i]));
		iUserIdx++;
	}

    sprintf((char *)adminAuth + iAdminIdx * AUTHORITY_STRING_LEN * sizeof(char),"LocalReplay");
    ++iAdminIdx;
    sprintf((char *)userAuth + iUserIdx * AUTHORITY_STRING_LEN * sizeof(char),"LocalReplay");
    ++iUserIdx;

    int iAdminTMP =iAdminIdx;
    int iUserTMP = iUserIdx;

    for(i = 1; i < (uint)(logic_num + 1); i++)
    {
	
        sprintf((char *)adminAuth + iAdminTMP * AUTHORITY_STRING_LEN * sizeof(char),"Monitor_%02d",i);
        ++iAdminIdx;
        sprintf((char *)adminAuth + (iAdminTMP+logic_num )* AUTHORITY_STRING_LEN * sizeof(char),"Replay_%02d",i);
        ++iAdminIdx;
        sprintf((char *)adminAuth + (iAdminTMP + logic_num*2) * AUTHORITY_STRING_LEN * sizeof(char),"NetPreview_%02d",i);
        ++iAdminIdx;

		iAdminTMP ++;

        sprintf((char *)userAuth + iUserTMP * AUTHORITY_STRING_LEN * sizeof(char),"Monitor_%02d",i);
        ++iUserIdx;
        sprintf((char *)userAuth + (iUserTMP+logic_num) * AUTHORITY_STRING_LEN * sizeof(char),"Replay_%02d",i);
        ++iUserIdx;
        sprintf((char *)userAuth + (iUserTMP + logic_num*2) * AUTHORITY_STRING_LEN * sizeof(char),"NetPreview_%02d",i);
        ++iUserIdx;

		iUserTMP ++;		

    }

	for(i = 0;  i < sizeof(generalAdminAuthorityBack) / sizeof (generalAdminAuthorityBack[0]); i++)
	{
		memcpy((char *)adminAuth+ iAdminIdx* AUTHORITY_STRING_LEN * sizeof(char),generalAdminAuthorityBack[i],strlen(generalAdminAuthorityBack[i]));
		++iAdminIdx;
	}    
	for(i = 0;  i < sizeof(generalUserAuthorityBack) / sizeof (generalUserAuthorityBack[0]); i++)
	{
		if(strlen(generalUserAuthorityBack[i]) > 0)
		{
			memcpy((char *)userAuth + iUserIdx * AUTHORITY_STRING_LEN * sizeof(char),generalUserAuthorityBack[i],strlen(generalUserAuthorityBack[i]));
			++iUserIdx;
		}
	}

    CUserManager::config(0, 4, CONFIG_DIR"/Account1", CONFIG_DIR"/Account2", DATA_DIR"Account",
                        (char (*)[AUTHORITY_STRING_LEN])adminAuth, iAdminIdx-1,
                        (char (*)[AUTHORITY_STRING_LEN])userAuth, iUserIdx-1);
	
	free(adminAuth);
	free(userAuth);
}

#define VFWARE_VERSION "2020-04-27 15:44:50"


extern "C" char * BSP_GetVersion(void);



#ifdef __cplusplus
 extern "C" {
#endif
int S1_Defualt_Config()
{
	_printd("defualt config==========>>>>");

	//修复密码、恢复标清
	g_userManager.modPassword("guest","123456",needEncrypt);	
	return 0;
}
#ifdef __cplusplus
	}
#endif

void print_VersionInfo(void)
{
	printf("\nversion info:\n");
	printf("\tvfware :%s\n", VFWARE_VERSION);	
//	printf("\tBSP    :%s\n", BSP_GetVersion());	
}

int InitAutoLogService()
{
	_printd(">>>>>>>>> Start Auto Log Service <<<<<<<<<<\n");
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in addrServer;
	bzero(&addrServer,sizeof(addrServer));
	addrServer.sin_family=AF_INET;
	addrServer.sin_addr.s_addr=inet_addr("127.0.0.1");
	addrServer.sin_port=htons(8010);
	
	if(connect(sockfd,(struct sockaddr*)&addrServer,sizeof(addrServer))==-1)
	{
		perror("error in connecting");
		exit(1);
	}
	dup2(sockfd, STDOUT_FILENO);
	return 0;
}

#if 1 //zwz log 20190328
#define LOG_TOOL_RUN_FILE		"/tmp/LogToolRun"

void *LogDetectThread(void *args)
{
	pthread_detach(pthread_self());
	
	while (1)
	{
		if (access(LOG_TOOL_RUN_FILE, F_OK) == 0) {
			InitAutoLogService();
			break;
		} else {
			sleep(1);
		}
	}
	
	pthread_exit(0);
}

int LogSendInit()
{
	int ret = 0;
	pthread_t logPid;
	
	ret = pthread_create(&logPid, NULL, LogDetectThread, NULL);
	if (ret < 0) {
		printf("Fail to pthread_create LogDetectThread\n");
		return -1;
	}
	
	return 0;
}
#endif

#if 0
#ifdef IPC_JZ  //因s1需要快速启动，开机即获取IP
void* ProcessThread(void* arg)
{
	char ip[32] = {0};
	pthread_detach(pthread_self());
	
	while(1)
	{
		if(0 == RTGetServerIpAddr(ip) && ip[0] != 0){
			break;
		}
		usleep(100*1000);
	}
}
#endif
#endif

void recvSignal(int sig)  
{  
	//printf("received signal %d !!!\n",sig);  
	g_iSIGSEGV = sig;
} 

void StartUpProcess()
{
	pthread_t mPid;
#if 0
#ifdef IPC_JZ  	
	if(0 != pthread_create(&mPid,NULL,ProcessThread,NULL))
		_printd("ProcessThread error");
#endif
#endif

#ifndef WIN32
	signal(SIGPIPE, SIG_IGN);
	sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGALRM);
    sigprocmask(SIG_BLOCK, &set, NULL);
#endif

#ifdef VFWARE_SIGNAL
	signal(SIGSEGV, recvSignal);  //段错误信号
	signal(SIGILL, recvSignal); //4 非法指令
	signal(SIGABRT, recvSignal);//6 由abort(3)发出的退出指令
	signal(SIGFPE, recvSignal); //8 浮点异常
	signal(SIGSEGV, recvSignal);//11段错误信号
#endif
#ifndef VFWARE_DEBUG
//暂时屏蔽等找到段错误不重启时问题再打开 modify by jwd 20190116
//	signal(SIGSEGV, recvSignal); //段错误信号  
#endif

#ifdef WIN32
    InitApiLib();
#elif defined(LINUX)
	//signal(SIGPIPE, SignalFunc); //原来是接到SIGPIPE消息后，调用函数SignalFunc, 但signal捕获一次后就不再捕获了，所以不能用
	//直接忽略SIGPIPE消息，Modified by billhe at 2009-9-17
	struct sigaction action; //信号处理结构体
	action.sa_handler = SignalFunc; //产生信号时的处理函数
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0; 
	sigaction(SIGPIPE,&action,NULL); //信号类型
#endif
}

int main(int argc, char * argv[])
{
	int timeout = 0;
	int timeout2 = 0;
	g_iSIGSEGV = 0;
	if (argc >= 2){
		if (!strncmp(argv[1], "-V", 3)){
			print_VersionInfo();
			exit(0);
		}else if(!strncmp(argv[1], "-log", 5)) {
			InitAutoLogService();
		}else{
			printf("usage: %s -V | -log\n", argv[0]);
			exit(0);
		}
	}

	LogSendInit();//zwz tf log
	
	print_VersionInfo();
	StartUpProcess();

#ifdef _PF_DM365_SERIES_
    init_nipdebug(0, 0, 0, 0);
#endif
#if 0	
    trace("main(%d,", argc);
    for(int i = 0; i < argc; i++)
    {
        trace(" %s", argv[i]);
    }
    trace(")\n");
    
    //设置系统启动时间，added by billhe at 2009-5-30
    memset(&g_StartTime, 0, sizeof(SYSTEM_TIME));
    SystemGetCurrentTime(&g_StartTime);

    //printf("\r\n\r\nsofia ver:2009-09-03 11:24\r\n\r\n");
    printf("\r\nstart vfware on %d-%d-%d %02d:%02d:%02d\r\n",g_StartTime.year,g_StartTime.month,g_StartTime.day,g_StartTime.hour,g_StartTime.minute,g_StartTime.second);
#endif
	
    g_Challenger.Initialize(argc, argv);

#if defined(IPC_JZ)
	system("bootup_timer ipcam_finish >> /tmp/bootup_time");
#endif
	//为了防止程序在初始化模块中途意外崩溃，在/app/start.sh 中有定时重启
	//如果程序正常执行则停止脚本，系统不重启 add by Le0
	//system("kill -9 `ps | grep start.sh | grep -v grep | awk '{print $1}'`");
	//gettimeofday(&end,NULL);
	//timer = end.tv_sec-start.tv_sec;
	//_printd("#########main killall start.sh time=%d \n",timer);
	//_printd("#########main killall start.sh\n");

	//增加flash自检功能，降低设备启动异常的概率
	FlashCheckJffs2();

#ifdef VFWARE_SIGNAL
	int printcount = 0;
	while(1) 
	{		
		int upgradestatuscur  = 0;
		switch(g_iSIGSEGV)
		{
			case 4:
			case 6:
			case 8:
			case 11:				
			{
				upgradestatuscur = RealTimeSpecialStatus();
				_printd("system  will be reboot [%d] RealTimeSpecialStatus()[%d]", timeout,upgradestatuscur);
				//不在升级时段错误20秒重启，升级则 最长3分钟
				//在升级20秒临界值跳过给升级程序升级
				if(timeout++ >= 20 && RT_UPGRADING != upgradestatuscur && RT_UPGRADE_OK != upgradestatuscur) 
				{
					_printd("system  reboot!!!");
					g_Challenger.Reboot(0,g_iSIGSEGV);
				}
				else
				{
					if(20 < timeout )
					{
						timeout = 0;
						//3分钟升级不成功也得重启
						if(timeout2++ > 9)
						{
							//保证段错误重启
							_printd("system   reboot!!!");
							g_Challenger.Reboot(0,g_iSIGSEGV);
						}
					}
				}

			}
			break;
			default:
			{
				timeout = 0;
				if(printcount++ > 10)
				{
					printcount = 0;
					_printd("g_iSIGSEGV[%d] ",g_iSIGSEGV);
				}
			}
			break;
		}

		sleep(1);
	}
#endif
    CSemaphore sem;
    sem.Pend();
    return 0;
}



