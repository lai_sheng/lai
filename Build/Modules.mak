
#软巡航
CFLAGS += -DDEF_SOFT_TOUR
#限制通道名长度
CFLAGS += -DLMT_TITLE_LEN
#CFLAGS += -DFUNC_MUTILINE_TITLE
CFLAGS += -D_REENTRANT -DLINUX -DUCLINUX -DDVR_HI
CFLAGS += -DAPP_TOUR_TURN
#CFLAGS += -Wall -g $(INC_PATH)

CFLAGS += $(EXT_FUNCTION_DEF)

#CFLAGS	+=  -D_NET_USE_DHCPCLIENT 
#CFLAGS  += -D_HAVE_WEB_FLASH_REGION
CFLAGS	+=  -D_HAVE_WEB_FLASH_REGION
#A2 JSK
CFLAGS	+=	-DUSING_PROTOCOL_NTP -DUSING_PROTOCOL_NTP_OP
##A2 JSK
#CFLAGS	+=	-DUSING_DDNS
#CFLAGS	+=	-DFUNCTION_NET_ALARM_CENTER
CFLAGS 	+= -DFUNC_GET_HWSN


CFLAGS 	+= -DFUNC_WDT  

#网络状态日志
CFLAGS 	+= -DLOG_APPEND_NETABORT

#长期测试自动恢复默认IP的BUG
#CFLAGS += -DDEFAULT_IP_BUG

#预览区域遮挡
#CFLAGS	+= -DFUNC_PREVIEW_COVER

#标准协议
#A2 JSK
#CFLAGS	+=	-D_FUN_DDNS_NEW

#A2 JSK
#FLAGS	+=	-D_USE_SMTP_MODULE

####//#tiger多码流存储
#CFLAGS	+=	-D_FUN_RECORD_MULTISTREAM
#tiger//start//20130607for RESET
CFLAGS	+=	-DSDK_3516
#tiger//stop//20130607for RESET
#osd 放大
#A2 JSK
#CFLAGS += -D_SUPPORT_OSD_ZOOM_

CFLAGS	+=	-DRECORD_NET_THREAD_MULTI
CFLAGS	+=	-DSNAP_NET_THREAD_MULTI
#upnp协议
#A2 JSK
#CFLAGS += -D_FUNC_UPNP_MOUDLE

#2761自检宏，在无音频或视频码流的时候自动重启
#CFLAGS += -D_2761_EXCEPTION_CHK_


#A2 JSK
#CFLAGS += -D_FUN_DDNS_NEW_QingDaoMeiBu

#rtsp功能
#A2 JSK
#CFLAGS += -DFUNCTION_SUPPORT_RTSP
#CFLAGS += -DFUNCTION_SUPPORT_RTSP_MULTICAST
#SUPPORT_RTSP = 1

#明景功能宏
ifeq ($(OEM_VENDOR),MJING)
CFLAGS += -DDEV_MJ_PTZ
CFLAGS += -DBASE2HIGH
CFLAGS += -DFUNTION_PRESET_TITLE
endif

#X6相机
ifeq ($(OEM_VENDOR),BPI_S2L)
CFLAGS += -DBPI_S2L
endif

#亚邦功能宏
ifeq ($(OEM_VENDOR),YABANGEXT)
CFLAGS += -DDEV_MJ_PTZ
CFLAGS += -DFUNTION_PRESET_TITLE
endif

#主动注册
#CFLAGS	+=	-DNET_AUTO_REGISTER

#设置dns服务器功能
#CFLAGS	+= -D_DNS_FUNCTION=1

#9504 只支持D1的需求
#CFLAGS	+= -DDEF_ONLY_D1=1

#ftp功能
#ftp抓包功能
#A2 JSK
#CFLAGS += -DFTP_TRAN_SNAPSHOT=1
#CFLAGS	+=	-DFUNC_FTP_UPLOAD

#报警抓图Email功能
#CFLAGS += -DALM_REQUIRE_SNAP_EMAIL=1

#超级密码
CFLAGS += -DUSE_SUPER_PASSWORD=\"vision\"

#CFLAGS += -DFUN_TRANS_COMM

#分辨率
#CFLAGS += -DDEF_RESOLUTION_ADJUST 

#预览暂停功能
#_FUNC_PREVIEW_SPT1_PAUSE

CFLAGS += -DVN_IPC
#CFLAGS += -DSYS_NO_RECORD
CFLAGS += -DSYS_NO_DISPLAY
CFLAGS += -DSYS_NO_GUI
CFLAGS += -DSYS_NO_BACKUP
CFLAGS += -DSYS_NO_PLAY
CFLAGS += -DSYS_NO_FRONTBOARD_FUNCTION
CFLAGS += -DFUNC_FRONTBOARD_CTRL		
CFLAGS += -DSYS_NO_MOUSE	
CFLAGS += -D_FUNC_ADJUST_VOLUME_	
CFLAGS += -DENC_SUPPORT_SNAPSHOT

CFLAGS += -D_USE_720P_MODULE_


include $(SRCROOT)/Build/SrcFileList

#有外接协议时需要编译
SRCS_gbl += $(SRCS_InterVideo)

#rtmp
ifeq ($(RTMP),ON)
	include ../src_intervideo_rtmp
	SRCS_gbl += $(SRCS_RTMP)
	CFLAGS += -D__INTERVIDEO_
	CFLAGS  += -DRTMP
	SHARELIBS += $(LIBSDIR)/librtmp.so.0 $(COMLIBSDIR)/libssl.a $(COMLIBSDIR)/libcrypto.a
endif

#NEW rtsp
ifeq ($(RTSPNEW_PROTOCL),RTSPNEW)
	include ../src_intervideo_rtsp
	SRCS_gbl += $(SRCS_RTSPNEW)
	CFLAGS += -D__INTERVIDEO_
	CFLAGS  += -DRTSPNEW
endif

	include ../src_intervideo_devsearch
	SRCS_gbl += $(SRCS_DEVSEARCH)
	
####直播相机
ifeq ($(LIVECAM),ON)
	include ../src_intervideo_LiveCam
	SRCS_gbl += $(SRCS_LIVECAM)
	CFLAGS   += -D__INTERVIDEO_
	CFLAGS	 += -I$(SRCROOT)/Include/Intervideo/LiveCam
	CFLAGS   += -DLIVECAM
	ifneq ($(TUPU_PROTOCL),ON)
		ifeq ($(COMPLIE_TYPE),IPC_HiSi)
			SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libJCPPP_Hisi.so 
			SHARELIBS += -L$(COMLIBSDIR)   $(COMLIBSDIR)/libcrypto.a 
		else ifeq ($(COMPLIE_TYPE),IPC_JZ)
			#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libAVAPIs.so
			#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libIOTCAPIs.so
			#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libP2PTunnelAPIs.so
			#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libRDTAPIs.so
			#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libJCPPP_JZ.so 
			ifeq ($(debug), 1)		
				ifeq ($(TUTK_P2P), ON)				
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libAVAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libIOTCAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libP2PTunnelAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libRDTAPIs.so	
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libJCPPP_JZ.a 
				else
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libJCPPP_JZ.a  
				endif
			else
				ifeq ($(TUTK_P2P), ON)
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libAVAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libIOTCAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libP2PTunnelAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libRDTAPIs.so
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/p2p2.0/libJCPPP_JZ.a 
				else
				SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libJCPPP_JZ.so 
				endif
			endif
			ifneq ($(LOW_BATTERY_CAMERA), ON)
				SHARELIBS += -L$(COMLIBSDIR)   $(COMLIBSDIR)/libcrypto.so 
			endif
		else ifeq ($(COMPLIE_TYPE),IPC_FH)
			SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libJCPPP_FH.so
		endif
	endif
	
	ifeq ($(LOW_BATTERY_CAMERA), ON)
		SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libcurl.so.4 $(COMLIBSDIR)/libssl.a
	else
		SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/libcurl.so.4 $(COMLIBSDIR)/libssl.so
	endif
	
	SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/zbar/libzbar.a
	#SHARELIBS += -L$(LIBSDIR) $(LIBSDIR)/zbar/libzbar.la
endif

#####开机录像
ifeq ($(REC_STARTUP),ON)
	CFLAGS   += -DREC_STARTUP
endif
#####声波 (动态库)
ifeq ($(SOUNDWAVE),ON) 
SHARELIBS += $(LIBSDIR)/libwave.so
endif
####声波 (静态库)
ifeq ($(SOUNDWAVE_STATIC),ON) 
SHARELIBS += $(LIBSDIR)/libwave.a
endif


ifeq ($(LIVECAMGUI),ON)
	CFLAGS   += -DLIVECAMGUI
endif

ifeq ($(CAMERA2), ON)
	CFLAGS += -DCAMERA2
endif


#  音频格式
ifeq ($(AUDIO_TYPE),AAC)
	CFLAGS  += -DAUDIO_TYPE_AAC
else
	CFLAGS  += -DAUDIO_TYPE_G711A
endif

# VServer
ifeq ($(VSERVER),ON)
	CFLAGS += -DVSERVER
endif

#贵州G项目
ifeq ($(GUIZHOU_G_PROJECT),ON)
     CFLAGS += -DGUIZHOU_G_PROJECT
endif
#警用一体机
ifeq ($(POLICE_PROJECT),ON)
     CFLAGS += -DPOLICE_PROJECT
endif

ifeq ($(MEGA_RF),1)
	include $(SRCROOT)/Build/src_intervideo_mega_rf
	include $(SRCROOT)/Build/src_intervideo_megahttp
	
	SRCS_gbl += $(SRCS_InterVideo)
	SRCS_gbl += $(SRCS_mega_rf)
	SRCS_gbl += $(SRCS_megahttp)
	CFLAGS += -D__INTERVIDEO_ -DMEGAEYES_QQY
	CFLAGS += -DMEGA_RF=1	
endif

#CFLAGS += -DPOLICE_PROJECT

# Onvif接入
ifeq ($(ONVIF),ON)
	include ../src_intervideo_ONVIF
	CFLAGS += -DONVIF
ifeq ($(ONVIF_SERVER),ON)
	CFLAGS += -DONVIF_SERVER
endif

ifeq ($(ONVIF_CLIENT),ON)
	CFLAGS += -DONVIF_CLIENT
endif

ifeq ($(ONVIF_VERSION),211)
	CFLAGS	+=	-I$(SRCROOT)/Include/Onvif211
	SRCS_gbl += $(SRCS_ONVIF211)
endif

ifeq ($(ONVIF_VERSION),242)
	CFLAGS	+=	-I$(SRCROOT)/Include/Onvif242
	SRCS_gbl += $(SRCS_ONVIF242)
	#Onvif上拉报警模式 Le0
	CFLAGS  += -DONVIF_PULL_ALARM
endif
endif

ifeq ($(TUPU_PROTOCL),ON)
	include ../src_intervideo_Tupu
	SRCS_gbl  += $(SRCS_TUPU)
	SHARELIBS += $(COMLIBSDIR)/libcrypto.a
	CFLAGS	  +=	-I$(SRCROOT)/Include/Intervideo/Tupu
	CFLAGS    += -D__INTERVIDEO_
	CFLAGS    += -DTUPU

endif

ifeq ($(SUNING_PROTOCL),ON)
	include ../src_sunning_cloud
	SRCS_gbl  += $(SRCS_SUNING)
	SHARELIBS += $(COMLIBSDIR)/libcrypto.a
	CFLAGS	  +=	-I$(SRCROOT)/Include/Intervideo/RecordTs
	CFLAGS    += -D__INTERVIDEO_
	CFLAGS    += -DSUNING
endif

ifeq ($(HEMU_CLOUD),ON)
include ../src_hemu_cloud
SRCS_gbl  += $(SRCS_HEMU)
SHARELIBS += $(COMLIBSDIR)/libprotobuf-c.a
CFLAGS	  +=	-I$(SRCROOT)/Include/Intervideo/HeMu
SHARELIBS += $(LIBSDIR)/libcurl.a $(COMLIBSDIR)/libssl.a $(COMLIBSDIR)/libcrypto.a $(LIBSDIR)/zbar/libzbar.a
SHARELIBS += $(COMLIBSDIR)/yidong/liblwm2msdkL.a
CFLAGS    += -D__INTERVIDEO_
CFLAGS    += -DHEMU_CLOUD

endif

ifeq ($(MOBILE_COUNTRY),ON)
include ../src_mobile_country
SRCS_gbl  += $(SRCS_MOBILE_COUNTRY)
CFLAGS	  += -I$(SRCROOT)/Include/Intervideo/MobileCountry
CFLAGS	  += -I$(SRCROOT)/Include/Intervideo/MobileCountry/camerasdk
CFLAGS	  += -I$(SRCROOT)/Include/Intervideo/MobileCountry/iotsdk
SHARELIBS += $(LIBSDIR)/libcurl.a $(COMLIBSDIR)/libssl.a $(COMLIBSDIR)/libcrypto.a $(LIBSDIR)/zbar/libzbar.a
SHARELIBS += $(COMLIBSDIR)/MobileCountry/libdotsdk.a $(COMLIBSDIR)/MobileCountry/libntiotsdk.a
SHARELIBS += $(COMLIBSDIR)/yidong/liblwm2msdkL.a
CFLAGS    += -D__INTERVIDEO_
CFLAGS    += -DMOBILE_COUNTRY
endif


ifeq ($(MQTT_PROTOCL),ON)
include ../src_Funtion_Mqtt
SRCS_gbl  += $(SRCS_MQTT)
CFLAGS    += -DWJA_MQTT
endif

ifeq ($(AliYunSDK),ON)
SHARELIBS += $(COMLIBS_ALIYUN)
endif

SHARELIBS += $(COMLIBS_YIDONG)

ifeq ($(MP3PLAY),ON)
CFLAGS    += -DMP3PLAYWITHURL
endif

ifeq ($(FACE_FEATURE),ON)
CFLAGS	+= -DFACE_FEATURE
endif

ifeq ($(MOTOR),ON)
CFLAGS	+= -DMOTOR
endif

ifeq ($(DOOR_LOCK),ON)
CFLAGS	+= -DDOOR_LOCK
endif

ifeq ($(YiDong),ON)
CFLAGS	+= -DYIDONG
endif
