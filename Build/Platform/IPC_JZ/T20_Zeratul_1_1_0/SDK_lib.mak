COMLIBSDIR_JZ=$(PLATFORMDIR)/T20_Zeratul_1_1_0

COMLIBS_JZ=\
	$(COMLIBSDIR_JZ)/libimp.a \
	$(COMLIBSDIR_JZ)/libalog.a \
	$(COMLIBSDIR_JZ)/libzrtcam.a \
	$(COMLIBSDIR_JZ)/libdbus-1.so \
	$(COMLIBSDIR_JZ)/libexpat.so \
	$(COMLIBSDIR_JZ)/libsysutils.a \
	$(COMLIBSDIR_JZ)/libZalive_dev.a \
	$(COMLIBSDIR_JZ)/libprotobuf-c.a
