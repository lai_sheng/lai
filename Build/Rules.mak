#svn_test=$(shell ${SRCROOT}/Build/GetSvnInfo.sh)
#VISIONDIGI_SVN_URL="$(shell svn info ${SRCROOT} | awk '{if (NR==2) print $2}')"
#VISIONDIGI_SVN_VERSION="$(shell svn info ${SRCROOT} | awk '{if (NR==5) print $2}')"
#VISIONDIGI_SVN_URL:=$(shell ${SRCROOT}/Build/GetSvnInfo.sh ${SRCROOT} "svnurl")
#VISIONDIGI_SVN_VERSION:=$(shell ${SRCROOT}/Build/GetSvnInfo.sh ${SRCROOT} "svnversion")
#CFLAGS += -DVISIONDIGI_SVN_VERSION=\"$(VISIONDIGI_SVN_VERSION).\" -DVISIONDIGI_SVN_URL=\"$(VISIONDIGI_SVN_URL).\"

#VISONDIGI_SVN_INFO="$(shell svn info ${SRCROOT})"
#CFLAGS += -DVISONDIGI_SVN_INFO=\"$(VISONDIGI_SVN_INFO).\"

CFLAGS_RELEASE= -Os -O2
CFLAGS_DEBUG= -g -O0
LFLAGS_DEBUG = $(LDLIBS)
#通过手动指定生成的最终执行文件的存放目录或目录及文件名
ifndef DIST
DIST := $(TARGETDIR)/vfware
endif

ifeq ($(CASCADE),1)
slave := $(TARGETDIR)/pciv_test_slave
endif

ifeq ($(debug),1)
CFLAGS += -DVFWARE_DEBUG
CFLAGS += -DDEBUG
#编译.d文件使用，.d文件编译不能添加-g选项
CFLAGS_RELEASE := $(CFLAGS)
CFLAGS += $(CFLAGS_DEBUG)
INTERMEDIATEDIR := $(INTERMEDIATEDIR)_debug
DIST := $(TARGETDIR)/debug/vfware
#LFLAGS := $(LFLAGS_DEBUG)
ifeq ($(CASCADE),1)
slave := $(TARGETDIR)/debug/pciv_test_slave
endif
else
CFLAGS_RELEASE += $(CFLAGS)
CFLAGS := $(CFLAGS_RELEASE)
endif

TMPOBJS_gbl = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_gbl))
OBJS_gbl = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_gbl))

TMPOBJS_sys = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_sys))
OBJS_sys = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_sys))

TMPOBJS_dev = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_dev))
OBJS_dev = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_dev))

TMPOBJS_fun = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_fun))
OBJS_fun = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_fun))

#ifeq ($(findstring SYS_NO_GUI,$(CFLAGS)),)
#TMPOBJS_gui = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_gui))
#OBJS_gui = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_gui))
#endif

ifeq ($(LIVECAMGUI),ON)
TMPOBJS_gui = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_LiveCamGUI))
OBJS_gui = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_gui))
endif

TMPOBJS_net = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_net))
OBJS_net = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_net))

TMPOBJS_config = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_config))
OBJS_config = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_config))

ifdef SUPPORT_RTSP
TMPOBJS_rtsp= $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_Rtsp))
OBJS_rtsp = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_rtsp))
endif

TMPOBJS_Slave = $(patsubst $(SRCROOT)/%.cpp, $(INTERMEDIATEDIR)/%.o, $(SRCS_Slave))
OBJS_Slave = $(patsubst $(SRCROOT)/%.c, $(INTERMEDIATEDIR)/%.o, $(TMPOBJS_Slave))

OBJS := $(OBJS_gbl) $(OBJS_config) $(OBJS_sys) $(OBJS_dev) $(OBJS_fun) $(OBJS_gui) $(OBJS_net) $(OBJS_rtsp) $(OBJS_goahead) $(OBJS_sdk)

OBJD := $(patsubst %.o, %.d, $(OBJS))
LIBAPP := $(INTERMEDIATEDIR)/libapp.a

LIBAPP_gbl := $(INTERMEDIATEDIR)/libgbl.a
LIBAPP_sys := $(INTERMEDIATEDIR)/libsys.a
LIBAPP_dev := $(INTERMEDIATEDIR)/libdev.a
LIBAPP_fun := $(INTERMEDIATEDIR)/libfun.a
LIBAPP_gui := $(INTERMEDIATEDIR)/libgui.a
LIBAPP_net := $(INTERMEDIATEDIR)/libnet.a
LIBAPP_config := $(INTERMEDIATEDIR)/libconfig.a
LIBAPP_rtsp := $(INTERMEDIATEDIR)/librtsp.a
LIBAPP_SUM :=  $(LIBAPP_net) $(LIBAPP_fun) $(LIBAPP_gui) $(LIBAPP_dev)
LIBAPP_SUM += $(LIBAPP_rtsp) $(LIBAPP_sys) $(LIBAPP_config)
datatime=`date '+%Y-%m-%d %H:%M:%S'`
#结束时，删除versin.o 下次编译时，就重新编译这个，重新取版本号，以保证sofia记录的版本是正确的
all: PreBuild rtsp sys dev fun net gui config $(OBJD) $(DIST) $(slave)
	@echo dist: $(DIST)
	@echo slave: $(slave)
#	@echo SvnURL: $(VISIONDIGI_SVN_URL)
#	@echo SvnVersion: $(VISIONDIGI_SVN_VERSION)
	@rm $(INTERMEDIATEDIR)/System/Version.o
$(INTERMEDIATEDIR):
	@mkdir -p $(INTERMEDIATEDIR)

#$(OBJD):PreBuild
ifeq ($(debug),1)
    CFLAGS += -DDEBUG
endif
ifeq ($(CASCADE),1)
$(slave): $(OBJS_Slave) $(LDLIBS) $(COMLIBS)
	$(CPP) $(CFLAGS) $(COMPILEOPT) -o $@ $+ $(LFLAGS)
	$(STRIP) $(slave)
endif

$(DIST): $(OBJS_gbl) $(LIBAPP_SUM) $(LDLIBS) $(COMLIBS) $(LIBAPP_sys) 
	@mkdir -p $(dir $@)
	@mkdir -p $(TARGETDIR)
	@echo here?

	$(CPP) $(CFLAGS)  -o $@ $+ $(LFLAGS) $(SHARELIBS);
ifneq ($(debug),1)
	$(STRIP) $(DIST)	
endif
#调试使用Le0
ifneq ($(debug),1)
ifeq ($(PRODUCT_TYPE),IPC_JZ_T30Z)
	cp ../../demo/$(OEM_VENDOR)/vfware ipcam
	zip ipcam.zip ipcam
	rar a ipcam.rar ipcam
	cp ipcam.zip /opt/nfs/  -f
	cp ipcam /opt/nfs/ipcam -f
else
	cp ../../demo/$(OEM_VENDOR)/vfware ipcam
	rar a ipcam.rar ipcam
	cp ipcam.rar /opt/nfs/ -f
#	cp ../../demo/$(OEM_VENDOR)/vfware /opt/nfs/$(OEM_VENDOR)/ -f
	cp ../../demo/$(OEM_VENDOR)/vfware /opt/nfs/ipcam -f
endif
else
ifeq ($(PRODUCT_TYPE),IPC_S2L)
	cp ../../demo/$(OEM_VENDOR)/debug/vfware /opt/nfs/$(OEM_VENDOR)/debug/ -f
else
	cp ../../demo/$(OEM_VENDOR)/debug/vfware ipcam
	rar a ipcam.rar ipcam
	cp ipcam.rar /opt/nfs/ -f
#	cp ../../demo/$(OEM_VENDOR)/vfware /opt/nfs/$(OEM_VENDOR)/ -f
	cp ../../demo/$(OEM_VENDOR)/debug/vfware /opt/nfs/ipcam -f
endif
endif

$(INTERMEDIATEDIR)/%.o:$(SRCROOT)/%.c
	@mkdir -p $(dir $@)
	$(CC)  -c $(CFLAGS) $< -o $@

$(INTERMEDIATEDIR)/%.o:$(SRCROOT)/%.cpp
	@mkdir -p $(dir $@)
	$(CPP)  -c $(CFLAGS) $< -o $@
	
$(INTERMEDIATEDIR)/%.d:$(SRCROOT)/%.c
	@mkdir -p $(dir $@)
	@rm -f $@
	$(CC)  $(CFLAGS_RELEASE) -MM -MD -c $< -o $@
	@sed 's/.*\.o/$(subst /,\/, $(dir $@))&/g' $@ >$@.tmp
	@mv $@.tmp $@

$(INTERMEDIATEDIR)/%.d:$(SRCROOT)/%.cpp
	@mkdir -p $(dir $@)
	@rm -f $@
	$(CPP)  $(CFLAGS_RELEASE) -MM -MD -c $< -o $@
	@sed 's/.*\.o/$(subst /,\/, $(dir $@))&/g' $@ >$@.tmp
	@mv $@.tmp $@

ifneq ($(MAKECMDGOALS), clean)	
sinclude $(OBJD)
endif
	
print:
	@echo $(INTERMEDIATEDIR)
	@echo $(TARGETDIR)
	@echo $(DIST)
	@echo "########OBJS_gbl#################"
	@echo $(OBJS_gbl)
	@echo "########LIBAPP_SUM#################"
	@echo $(LIBAPP_SUM)
	@echo "########LDLIBS#################"
	@echo $(LDLIBS)
	@echo "########COMLIBS#################"
	@echo $(COMLIBS)
	@echo "########LIBAPP_sys#################"
	@echo $(LIBAPP_sys)
	@echo "########LFLAGS#################"
	@echo $(LFLAGS)
	@echo "########CFLAGS#################"
	@echo $(CFLAGS)
	@echo "###########COMLIBS_ALIYUN###################"
	@echo $(COMLIBS_ALIYUN)
	@echo "#####################################"
DBG_PATHNAME = $(TARGETDIR)/ch
dbg: $(DIST)
	cp -f $(DIST) $(DBG_PATHNAME)

clean:
	find $(INTERMEDIATEDIR)/ -type f -name 'core' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*tags*' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*.bak' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*~' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*.o' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '.depend' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*.a' -print | xargs rm -f
	find $(INTERMEDIATEDIR)/ -type f -name '*.d' -print | xargs rm -f
	find $(TARGETDIR)/ -type f -name 'vfware' -print | xargs rm -f
#
# include dependency files if they exist
#
PreBuild:
	@echo $(datatime)
	@sed -i "0,/VFWARE_VERSION/{s/VFWARE_VERSION.*/VFWARE_VERSION \"$(datatime)\"/}" $(pwd)../../Main/Main.cpp
	@sed -i "0,/BUIDDATETIME/{s/BUIDDATETIME.*/BUIDDATETIME \"$(datatime)\"/}" $(pwd)../../Functions/HMSOTA/OTAManager.cpp
#	sed -i "s/VFWARE_VERSION.*/VFWARE_VERSION \"`date '+%Y-%m-%d %H:%M:%S'`\"/" $(pwd)../../Main/Main.cpp
app: $(OBJS)
	$(AR) crus $@ $(OBJS)

#模块静态库生成

sys: $(OBJS_sys)
	$(AR) crus $(LIBAPP_sys) $^

dev: $(OBJS_dev)
	$(AR) crus $(LIBAPP_dev) $^

fun: $(OBJS_fun)
	$(AR) crus $(LIBAPP_fun) $^

gui: $(OBJS_gui)
	$(AR) crus $(LIBAPP_gui) $^

net: $(OBJS_net)
	$(AR) crus $(LIBAPP_net) $^
	
config: $(OBJS_config)
	$(AR) crus $(LIBAPP_config) $^

rtsp: $(OBJS_rtsp)
	$(AR) crus $(LIBAPP_rtsp) $^
		
lib : rtsp sys dev fun net gui config $(OBJD)
	
clean_all_lib : 
	rm -f $(OBJD)
	rm -f $(LIBAPP_gbl)
	rm -f $(LIBAPP_sys) 
	rm -f $(LIBAPP_dev) 
	rm -f $(LIBAPP_fun) 
	rm -f $(LIBAPP_gui) 
	rm -f $(LIBAPP_net) 
	rm -f $(LIBAPP_config)	
	rm -f $(LIBAPP_rtsp) 	
  
                
                
