#include "ez_libs/ez_socket/ez_socket.h"
#include "System/Log.h"
#include "System/Locales.h"
#include "System/ZipFile.h"
#include "System/BaseTypedef.h"
#include "System/Support.h"
#include "Functions/DriverManager.h"

#include "Net/Dlg/DlgNtpCli.h"


/////////////////////////////
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
///////////////////////////////

#define LOG_FUN_CLOSE		1

#define LOG_MAGIC            "DVRLog0.1" //日志指针保存在文件中


char FileLogName[VD_MAX_PATH] = LOG_DIR"/Log";
char FileLogSetName[VD_MAX_PATH] = LOG_DIR"/LogSet";        //添加一个文件保存用户对需要保存日志类型的选择

#ifdef WIN32
#define    ez_snprintf _snprintf
#endif

static std::string type_str_all;
static std::string type_str_reboot;
static std::string type_str_shut;
static std::string type_str_shut_abnorm;
static std::string type_str_confsave;
static std::string type_str_confload;
static std::string type_str_genset;
static std::string type_str_comctrl;
static std::string type_str_netset;
static std::string type_str_timing;
static std::string type_str_imgset;
static std::string type_str_channame;
static std::string type_str_outmode;
static std::string type_str_ptzctrl;
static std::string type_str_mdset;
static std::string type_str_almset;
static std::string type_str_userset;
static std::string type_str_groupset;
static std::string type_str_comall;
static std::string type_str_fserror;
static std::string type_str_alarm;
static std::string type_str_netalarm;
static std::string type_str_alarmend;
static std::string type_str_lossin;
static std::string type_str_lossend;
//#ifdef VN_SUPPORT_NEUTER
static std::string type_str_motionin;
static std::string type_str_motionend;
//#endif
static std::string type_str_blindin;
static std::string type_str_blindend;
static std::string type_str_recstart;
static std::string type_str_recstop;
static std::string type_str_hddwrite;
static std::string type_str_hddread;
static std::string type_str_hddtype;
static std::string type_str_hddformat;
static std::string type_str_hddnospace;
static std::string type_str_nohdd;
static std::string type_str_hddsetspace;
static std::string type_str_hdderror;
static std::string type_str_login;
static std::string type_str_illegalaccess;//added by wyf on 090814
static std::string type_str_logout;
static std::string type_str_adduser;
static std::string type_str_deluser;
static std::string type_str_moduser;
static std::string type_str_modpassword;//add by wyf on 090730
static std::string type_str_lockuser;//add by wyf on 090813
static std::string type_str_addgroup;
static std::string type_str_delgroup;
static std::string type_str_modgroup;
static std::string type_str_search;
static std::string type_str_download;
static std::string type_str_playback;
static std::string type_str_playbackFile;
static std::string type_str_playbackStop;
static std::string type_str_backup;
static std::string type_str_backuperror;
static std::string type_str_clear;
static std::string type_str_guiset;
static std::string type_str_atmset;
static std::string type_str_recworksheet;    
static std::string type_str_mtdworksheet;
static std::string type_str_almworksheet;
static std::string type_str_net_abortresume;
static std::string type_str_coderbreakdown;
static std::string type_str_coderresume;
static std::string type_str_unknown;
//增加异常处理相关日志
static std::string type_str_netabort;
static std::string type_str_netresume;
static std::string type_str_ipconflict;
static std::string type_str_digital_channel;



PATTERN_SINGLETON_IMPLEMENT(CLog);

CLog::CLog()
{
	if(LOG_FUN_CLOSE)	return;

	
    m_File = new CFile;
    m_FileLogSet = new CFile;
    trace("CLog::CLog()>>>>>>>>>\n");
    m_BufferPos = (uint)(-1);

    //check system log lable
    uint Bytes;
    if(!m_File->Open(FileLogName, CFile::modeCreate|CFile::modeReadWrite | CFile::modeNoTruncate)){
        trace("CLog::CLog() open file failed!\n");
        return;
    }
    m_File->Seek(0, CFile::begin);
    Bytes = m_File->Read(&m_Header, sizeof(m_Header));
    if (Bytes != sizeof(LOG_HEADER) || strcmp(m_Header.lable, LOG_MAGIC) != 0) {        //日志未曾记录过
reinit:
        trace("CLog::CLog() check failed, init...%d, %.28s\n", Bytes, m_Header.lable);
        strcpy(m_Header.lable, LOG_MAGIC);
        m_File->Seek(0, CFile::begin);
        m_Header.head = m_Header.tail = 0;
        Bytes = m_File->Write(&m_Header, sizeof(m_Header));
        m_File->Flush();
    } else {
        //trace("CLog::CLog() read log data...\n");
        if(m_Header.head >= LOG_MAX || m_Header.tail > m_Header.head + LOG_MAX || m_Header.head > m_Header.tail)
        {
            trace("m_Header.head = %d, m_Header.tail = %d\n", m_Header.head, m_Header.tail);
            goto reinit;
        }
    }
    m_File->Close();

    if (m_FileLogSet->Open(FileLogSetName, CFile::modeReadWrite))
    {
        if(m_FileLogSet->Read(m_SavedLogType, (LOG_TYPE_MAX + 1)* sizeof(int)) != ((LOG_TYPE_MAX + 1) * sizeof(int)))
        {
            m_FileLogSet->Close();
            if(m_FileLogSet->Open(FileLogSetName, CFile::modeCreate | CFile::modeNoTruncate))
            {
                for(int i = 0; i < (LOG_TYPE_MAX + 1); i++)
                {
                    m_SavedLogType[i] = ON;        //默认所有的log都保存
                }
                m_FileLogSet->Write(m_SavedLogType, (LOG_TYPE_MAX + 1) * sizeof(int));
            }
            else
            {
                trace("Create file LogSet fail!\n");
                return;
            }
        }
        m_FileLogSet->Close();
    }
    else
    {
        if(m_FileLogSet->Open(FileLogSetName, CFile::modeCreate | CFile::modeReadWrite | CFile::modeNoTruncate))
        {
            for(int i = 0; i < (LOG_TYPE_MAX + 1); i++)
            {
                m_SavedLogType[i] = ON;        //默认所有的log都保存
            }
            m_FileLogSet->Write(m_SavedLogType, (LOG_TYPE_MAX + 1) * sizeof(int));
            m_FileLogSet->Close();
        }
        else
        {
            trace("Open file LogSet fail!\n");
        }
    }

    //先读一个数据, 使缓冲被填满(LB需要2.7s), 从而提高第一次读数据的速度.
    LOG_ITEM item;
    GetItem(0, &item);
}

void CLog::Init()
{
	if(LOG_FUN_CLOSE)	return;
	
    //字符串初始化
    type_str_all = LOADSTR("comstring.comall");
    type_str_reboot = LOADSTR("info_log.logReboot");
    type_str_shut = LOADSTR("info_log.logShut");
	type_str_shut_abnorm= LOADSTR("info_log.logAbnormShut");
    type_str_confsave = LOADSTR("info_log.logConfSave");
    type_str_confload = LOADSTR("info_log.logConfLoad");
    type_str_genset = LOADSTR("titles.genset");
    type_str_comctrl = LOADSTR("titles.comctrl");
    type_str_netset = LOADSTR("titles.netset");
    type_str_timing = LOADSTR("titles.timing");
    type_str_imgset = LOADSTR("titles.imgset");
    type_str_channame = LOADSTR("conf_out.channelname");
    type_str_outmode = LOADSTR("titles.outmode");
    type_str_ptzctrl = LOADSTR("titles.ptzctrl");
    type_str_mdset = LOADSTR("titles.mdset");
    type_str_almset = LOADSTR("titles.ali");
    type_str_userset = LOADSTR("titles.userset");
    type_str_groupset = LOADSTR("titles.groupset");
    type_str_comall = LOADSTR("comstring.comall");
    type_str_fserror = LOADSTR("info_log.logFSError");
    type_str_alarm = LOADSTR("info_log.logAlarm");
    type_str_netalarm = LOADSTR("info_log.logNetAlarm");
    type_str_alarmend = LOADSTR("info_log.logAlarmEnd");
    type_str_lossin = LOADSTR("info_log.logLossIn");
    type_str_lossend = LOADSTR("info_log.logLossEnd");
//#ifdef VN_SUPPORT_NEUTER
	type_str_motionin = LOADSTR("info_log.logMotionIn");
	type_str_motionend = LOADSTR("info_log.logMotionEnd");
//#endif
    type_str_blindin = LOADSTR("info_log.logBlindIn");
    type_str_blindend = LOADSTR("info_log.logBlindEnd");
    type_str_recstart = LOADSTR("info_log.logManualRecordStart");
    type_str_recstop = LOADSTR("info_log.logManualRecordStop");
    type_str_hddwrite = LOADSTR("info_log.logHddWrite");
    type_str_hddread = LOADSTR("info_log.logHddRead");
    type_str_hddtype = LOADSTR("info_log.logHddType");
    type_str_hddformat = LOADSTR("info_log.logHddFormat");
    type_str_hddnospace = LOADSTR("titles.hddnospace");
    type_str_nohdd = LOADSTR("titles.nohdd");
    type_str_hddsetspace = LOADSTR("titles.hddnospace");
    type_str_hdderror = LOADSTR("titles.hdderror");
    type_str_login = LOADSTR("info_log.logLogin");
    type_str_illegalaccess = LOADSTR("info_log.logIllegalAccess");//added by wyf on 090814
    type_str_logout = LOADSTR("info_log.logLogout");
    type_str_adduser = LOADSTR("info_log.logAddUser");
    type_str_deluser = LOADSTR("info_log.logDeleteUser");
    type_str_moduser = LOADSTR("info_log.logModifyUser");
    type_str_modpassword = LOADSTR("info_log.logModifyPassword");//add by wyf on 090730
    type_str_lockuser = LOADSTR("info_log.logLockUser");//added by wyf  on 090813
    type_str_addgroup = LOADSTR("info_log.logAddGroup");
    type_str_delgroup = LOADSTR("info_log.logDeleteGroup");
    type_str_modgroup = LOADSTR("info_log.logModifyGroup");
    type_str_search = LOADSTR("info_log.logRecSearch");
    type_str_download = LOADSTR("info_log.logRecDownload");
    type_str_playback = LOADSTR("info_log.logRecPlayback");
    type_str_playbackFile = LOADSTR("info_log.logRecPlaybackFile");
    type_str_playbackStop = LOADSTR("info_log.logRecPlaybackStop");
    type_str_backup = LOADSTR("info_log.logBackup");
    type_str_backuperror = LOADSTR("info_log.logBackupError");
    type_str_clear = LOADSTR("info_log.logClear");    
    type_str_guiset = LOADSTR("info_log.logguiset");
    type_str_atmset = LOADSTR("info_log.logatmset");
    type_str_recworksheet = LOADSTR("titles.recworksheet");
    type_str_mtdworksheet = LOADSTR("titles.mtdworksheet");
    type_str_almworksheet = LOADSTR("titles.almworksheet");
    type_str_unknown = LOADSTR("info_log.logUnknown");
    //增加异常处理相关日志
    type_str_netabort = LOADSTR("info_log_net_abort");
    type_str_netresume = LOADSTR("info_log_net_resume");
    type_str_ipconflict = LOADSTR("info_alarm.ipconflict");
    type_str_digital_channel = LOADSTR("titles.netcamera");
    //添加不允许写日志的标志量，目前在升级时不允许写日志
    m_bLogDisable = FALSE;
}

CLog::~CLog()
{
	if(LOG_FUN_CLOSE)	return;
	
    delete m_File;
    delete m_FileLogSet;
}

void CLog::Append(ushort Type, uchar Data /* = 0 */, void* pContext /* = NULL */, uint Length /* = 8 */)
{
	if(LOG_FUN_CLOSE)	return;
	
    //在升级的时候不允许写日志
    if (m_bLogDisable == TRUE)
    {
        return;
    }

    LOG_ITEM item;
    SYSTEM_TIME time;
    LOG_HEADER temp;
    uint write_pos;

    //判断日志是否要添加进去，判断使用日志类型的前两位
    if(m_SavedLogType[(Type & 0xff00) >> 8] == 0)
    {
//        trace("no need to save this log\n");
#if !defined(LOG_SUPPORT_PLAYBACK)
        return;
#endif//LOG_SUPPORT_PLAYBACK
    }

    CGuard guard(m_Mutex);
    SystemGetCurrentTime(&time);

    timesys2dh(&item.time, &time);
    item.type = Type;
    item.data = Data;
	//!异常关机的情况下,记录关机时间
	if(Type==LOG_SHUT&&Data==1)
	{
//	g_Cmos.GetExitTime(&item.time);
		int temp=(item.time.second>30)?(-item.time.second):(item.time.second-50);
		item.time=add_time(item.time,temp);
	}
    if (pContext)
    {
        memcpy(item.context, pContext, MIN(8, Length));
    }

    //偏移 ...
    if (m_Header.tail- m_Header.head == LOG_MAX)    
    {   //判断是否覆盖
        if (m_SavedLogType[LOG_TYPE_MAX] == 0)
        {
            return;
        }
        m_Header.head++;
    }

    m_File->Open(FileLogName);

    //同步缓冲
    write_pos = m_Header.tail % LOG_MAX;
    if(write_pos >= m_BufferPos && write_pos < m_BufferPos + LOG_BUFFER)
    {
        m_BufferItem[write_pos - m_BufferPos] = item;
    }

    //存储日志项到文件 ...
    m_File->Seek(sizeof(LOG_HEADER) + sizeof(LOG_ITEM) * write_pos, CFile::begin);
    m_File->Write(&item, sizeof(LOG_ITEM));

    m_Header.tail++;
    if(m_Header.tail == 0) //计数溢出处理
    {
        temp = m_Header;
        m_Header.head %= LOG_MAX;
        m_Header.tail -= (temp.head - m_Header.head);
    }

    //存储日志头到文件, 日志位置采用一直递增的模式, 取日志项时需要先取余
    m_File->Seek(0, CFile::begin);
    temp = m_Header;
    temp.head %= LOG_MAX;
    temp.tail -= (m_Header.head - temp.head);
    m_File->Write(&temp, sizeof(LOG_HEADER));

    m_File->Flush();
    m_File->Close();
}

void CLog::Clear(const char *pUserName)
{
	if(LOG_FUN_CLOSE)	return;
	
    m_Mutex.Enter();
    m_Header.head = m_Header.tail = 0;
    m_Mutex.Leave();
    g_Log.Append(LOG_CLEAR, 0, (void*)pUserName, 8);
}

void CLog::DumpInfo()
{
	if(LOG_FUN_CLOSE)	return;
	
    CGuard guard(m_Mutex);
    uint Language = 0, pos;
    LOG_ITEM item;
    int at;

    pos = m_Header.head;
    /*Language = dvr->scm->config()->GenSet.Language;    //语言
    if (Language > 1) {    Language = 0;  }        //默认选择英语*/

    for (at = 0; at < LOG_MAX; at++) {
        if (pos != m_Header.tail) {
            _GetItem(pos, &item);

            trace("%03d %02d-%02d-%02d %02d:%02d:%02d ", at,
            item.time.year, item.time.month, item.time.day,
            item.time.hour, item.time.minute, item.time.second);
            //item.time.year, item.time.month, item.time.day,
            //item.time.hour, item.time.minute, item.time.second);
            Translate(&item, Language);
            trace("\n");
            pos++;
        } else {    //结束
            return;
        }
    }
}

void CLog::DumpStat()
{
	if(LOG_FUN_CLOSE)	return;
	
    trace("Log stat:\n");
    trace("count = %5d\n", GetCount());
    trace("head  = %5d\n", m_Header.head);
    trace("tail  = %5d\n", m_Header.tail);
    trace("buf   = %5d - %5d\n", m_BufferPos, m_BufferPos + LOG_BUFFER);
}

uint CLog::GetCount()
{
	if(LOG_FUN_CLOSE)	return 0;
	
    CGuard guard(m_Mutex);
    return (m_Header.tail - m_Header.head);
}

uint CLog::_GetItem(uint Pos, LOG_ITEM *pBuffer, uint Count)
{
	if(LOG_FUN_CLOSE)	return 0;
	
    uint left;

    left = m_Header.tail - Pos;        //剩余数据长
    if(left <= 0)
    {
        return 0;
    }
    Pos %= LOG_MAX;            //当前读位置
    Count = (left < Count) ? left : Count;                //取小值

    if(Count == 1) // 日志缓冲处理, 以提高效率
    {
        if(!(Pos >= m_BufferPos && Pos < m_BufferPos + LOG_BUFFER))
        {
            m_File->Open(FileLogName, CFile::modeRead);
            m_BufferPos = Pos / LOG_BUFFER * LOG_BUFFER;
            m_File->Seek(sizeof(LOG_HEADER) + m_BufferPos * sizeof(LOG_ITEM), CFile::begin);
            m_File->Read(m_BufferItem, LOG_BUFFER * sizeof(LOG_ITEM));
            m_File->Close();
        }
        *pBuffer = m_BufferItem[Pos - m_BufferPos];
        //trace("head = %d, tail = %d, pos = %d, ret = %d", m_Header.head, m_Header.tail, Pos, Count);
        return 1;
    }

    m_File->Open(FileLogName, CFile::modeRead);
    if(Pos + Count > LOG_MAX)
    {
        m_File->Seek(sizeof(LOG_HEADER) + Pos * sizeof(LOG_ITEM), CFile::begin);
        m_File->Read(pBuffer, (LOG_MAX - Pos) * sizeof(LOG_ITEM));
        pBuffer += LOG_MAX - Pos;
        Count -= LOG_MAX - Pos;
        Pos = 0;
    }
    m_File->Seek(sizeof(LOG_HEADER) + Pos * sizeof(LOG_ITEM), CFile::begin);
    m_File->Read(pBuffer, Count * sizeof(LOG_ITEM));
    m_File->Close();


    return Count;
}

uint CLog::GetItem(uint Offset, LOG_ITEM *pBuffer, uint Count)
{
	if(LOG_FUN_CLOSE)	return 0;
	
    uint pos;
    CGuard guard(m_Mutex);

    if(Offset >= (uint)(m_Header.tail - m_Header.head))
    {
        return 0;
    }

    pos = m_Header.head + Offset;            //要取的第一项位置

    return _GetItem(pos, pBuffer, Count);
}

//得到起止位置
void CLog::GetPos(uint *pHead, uint *pTail)
{
	if(LOG_FUN_CLOSE)	return;
	
    *pHead = m_Header.head;
    *pTail = m_Header.tail;
}

//先设定范围
void CLog::SetRange(SYSTEM_TIME* pStartTime, SYSTEM_TIME* pEndTime)
{
	if(LOG_FUN_CLOSE)	return;
	
	CGuard guard(m_Mutex);

    timesys2dh(&m_StartTime, pStartTime);
    timesys2dh(&m_EndTime, pEndTime);
}

//再查找
/* bFlag,查找方向标志，0从小到大，1从大往小 */
int CLog::GetContext(ushort Type, uint *pPos, char *Str, LOG_ITEM *pLogItem,bool bFlag)
{
	if(LOG_FUN_CLOSE)	return 0;
	

    CGuard guard(m_Mutex);
    int i;
    uint mask;
    char mask_channels[32] = "";
    const char* fmt;
    char* context;
    int len;
    char shuttime[32], rec_operatetime[32];
    char username[16];
    uint pos = *pPos;
    LOG_ITEM_RECORD m_log_record;
    LOG_ITEM item;    
    int iData = 0;

    if (0 == bFlag)
    {
        if(pos < m_Header.head)
        {
            pos = m_Header.head;//从首项找起
        }

        _GetItem(pos, &item);
        while(pos < m_Header.tail && ((Type != LOG_TYPE_NR && item.type >> 8 != Type)
            || compare_in_time(item.time, m_StartTime, m_EndTime) != 0))
        {
            pos++;
            _GetItem(pos, &item);
        }
        
        if(pos >= m_Header.tail)
        {
            return -1;
        }
    }
    else
    {
        if(pos >= m_Header.tail)
        {
            pos = m_Header.tail - 1;//从首项找起
        }

        if (VD_NULL_LONG == pos)
        {
            return -1;
        }
            
        if (pos < m_Header.head)
        {
            return -1;
        }
        
        _GetItem(pos, &item);
        
        while(pos >= m_Header.head && ((Type != LOG_TYPE_NR && item.type >> 8 != Type)
           || compare_in_time(item.time, m_StartTime, m_EndTime) != 0))
        {            
            if (0 == pos)
            {
                return -1;
            }
            pos--;
            _GetItem(pos, &item);
        }
        
        if(pos < m_Header.head)
        {
            return -1;
        }
    }
    
    *pPos = pos;

    /*获取日志*/
    if ( NULL != pLogItem )
    {
        memcpy(pLogItem, &item, sizeof(LOG_ITEM));
    }
    
    FormatTimeStringDH(&item.time, Str, FT_HALF_YEAR);
    len = strlen(Str);
    strcpy(&Str[len], "  ");
    context = &Str[len+2];
    len = LOG_LINE_MAX - len - 3;
    memset(username, 0, 16);


    switch (item.type) {
    case LOG_REBOOT:
        //ez_snprintf(context, len, type_str_reboot.c_str(), item.data);//, txt[1], txt[2]);
        ez_snprintf(context, len, "%s", type_str_reboot.c_str());
        break;
    case LOG_SHUT:
 //       FormatTimeStringDH((DHTIME*)item.context, shuttime, FT_HALF_YEAR);
//        ez_snprintf(context, len, type_str_shut.c_str(), shuttime);
#if 1
			ez_snprintf(context, len, "%s", type_str_shut.c_str());
#else
		if(item.data==0)
		{
			ez_snprintf(context, len, "%s", type_str_shut.c_str());
		}
		else
		{
			ez_snprintf(context, len, "%s", type_str_shut_abnorm.c_str());
		}
	#endif
        break;
    case LOG_CONFSAVE:
    case LOG_CONFLOAD:

        fmt = (item.type == LOG_CONFSAVE)?type_str_confsave.c_str():type_str_confload.c_str();
        switch(item.data)
        {
        case CFG_GENERAL:
            ez_snprintf(context, len, fmt, type_str_genset.c_str());
            break;
        case CFG_COMM:
            ez_snprintf(context, len, fmt, type_str_comctrl.c_str());
            break;
        case CFG_NET:
        case CFG_NET_COMMON:
        case CFG_NET_FTPSERVER:
        case CFG_NET_FTPAPPLICATION:
        case CFG_NET_IPFILTER:
        case CFG_NET_MULTICAST:
        case CFG_NET_PPPOE:
        case CFG_NET_DDNS:
        case CFG_NET_ALARMSERVER:
        case CFG_NET_NTP:
        case CFG_NET_EMAIL:
        case CFG_NET_SNIFFER:                        
            ez_snprintf(context, len, fmt, type_str_netset.c_str());
            break;
        case CFG_RECORD:
            ez_snprintf(context, len, fmt, type_str_timing.c_str());
            break;
        case CFG_RECWORKSHEET:
            ez_snprintf(context, len, fmt, type_str_recworksheet.c_str());                
            break;
        case CFG_TITLE:
            ez_snprintf(context, len, fmt, type_str_channame.c_str());
            break;
        case CFG_MONITORTOUR:
            ez_snprintf(context, len, fmt, type_str_outmode.c_str());
            break;
        case CFG_DSPINFO:
            ez_snprintf(context, len, fmt, type_str_imgset.c_str());
            break;
        case CFG_PTZ:
            ez_snprintf(context, len, fmt, type_str_ptzctrl.c_str());
            break;
        case CFG_DETECT:
        case CFG_LOSS:
        case CFG_BLIND:
            ez_snprintf(context, len, fmt, type_str_mdset.c_str());
            break;                
        case CFG_MTDWORKSHEET:
            ez_snprintf(context, len, fmt, type_str_mtdworksheet.c_str());                
            break;
        case CFG_ALARM:
            ez_snprintf(context, len, fmt, type_str_almset.c_str());
            break;
        case CFG_ALMWORKSHEET:
            ez_snprintf(context, len, fmt, type_str_almworksheet.c_str());                
            break;
        case CFG_USER:
            ez_snprintf(context, len, fmt, type_str_userset.c_str());                
            break;
        case CFG_GROUP:
            ez_snprintf(context, len, fmt, type_str_groupset.c_str());
            break;
        case CFG_STNOTEXIST:
            ez_snprintf(context, len, fmt, type_str_nohdd.c_str());                
            break;
        case CFG_STFAILURE:
            ez_snprintf(context, len, fmt, type_str_hdderror.c_str());            
            break;
        case CFG_STLOWSPACE:
            ez_snprintf(context, len, fmt, type_str_hddsetspace.c_str());            
            break;
        case CFG_NETABORT:
            ez_snprintf(context, len, fmt, type_str_netabort.c_str());                
            break;
        case CFG_IPCONFICT:
            ez_snprintf(context, len, fmt, type_str_ipconflict.c_str());                
            break;			
        case CFG_GUISET:
            ez_snprintf(context, len, fmt, type_str_guiset.c_str());
            break;
        case CFG_ATM:
            ez_snprintf(context, len, fmt, type_str_atmset.c_str());
            break;
        case CFG_DIGI_CHANNEL:
             ez_snprintf(context, len, fmt, type_str_digital_channel.c_str());
            break;
        default:
            ez_snprintf(context, len, fmt, type_str_genset.c_str());
            break;
        }//switch(item.xData)
        break;
    case LOG_FSERROR:
        ez_snprintf(context, len, type_str_fserror.c_str());
        break;
    case LOG_ALM_IN:
        ez_snprintf(context, len, type_str_alarm.c_str(), item.data); 
        break;
    case LOG_NETALM_IN:
    {
         ez_snprintf(context, len, type_str_netalarm.c_str(), item.data); 
        break;
    }

    case LOG_ALM_END:
        ez_snprintf(context, len, type_str_alarmend.c_str(), item.data); 
        break;   
    case LOG_LOSS_IN:
        ez_snprintf(context, len, type_str_lossin.c_str(),item.data);     
        break;
    case LOG_LOSS_END:
        ez_snprintf(context, len, type_str_lossend.c_str(), item.data);        
        break;
//#ifdef VN_SUPPORT_NEUTER
	case LOG_MOTION_IN:
		ez_snprintf(context, len, type_str_motionin.c_str(), item.data); 	   
		break;
	case LOG_MOTION_END:
		ez_snprintf(context, len, type_str_motionend.c_str(), item.data); 	   
		break;
//#endif
	case LOG_BLIND_IN:
		ez_snprintf(context, len, type_str_blindin.c_str(),item.data);	  
		break;
	case LOG_BLIND_END:
		ez_snprintf(context, len, type_str_blindend.c_str(), item.data); 	   
		break;
#ifdef LOG_APPEND_CODERABORT
    case LOG_CODER_BREAKDOWN://编码器故障
        ez_snprintf(context, len, type_str_coderbreakdown.c_str(), item.data);    
        break;
    case LOG_CODER_BREAKDOWN_RESUME://编码器故障恢
        ez_snprintf(context, len, type_str_coderresume.c_str(), item.data);    
        break;
#endif
    case LOG_AUTOMATIC_RECORD:
    case LOG_MANUAL_RECORD:
    case LOG_CLOSED_RECORD:

        mask = *(uint*)item.context;
        VD_BOOL show_sembol;
        show_sembol = TRUE;
        for(i =0;i<g_nCapture;i++){
            if((mask & BITMSK(i)))
            {
                char mask_channel[4];
                int iChannel = i;

                if (i >= 10)
                {
                    if (show_sembol)
                    {
                        strcat(mask_channels, "-");
                        show_sembol = FALSE;
                    }
                }

                if (iChannel >= 10)
                {
                    iChannel -= 10; 
                }
                sprintf(mask_channel, "%d", iChannel+1);
                strcat(mask_channels,  mask_channel);
            }
        }
        if (item.type == LOG_AUTOMATIC_RECORD)
        {
            ez_snprintf(context, len, LOADSTR("info_log.logAutomaticRecord"), mask_channels);
        }
        else if (item.type == LOG_MANUAL_RECORD)
        {
            ez_snprintf(context, len, LOADSTR("info_log.logManualRecord"), mask_channels);
        }
        else
        {
            ez_snprintf(context, len, LOADSTR("info_log.logClosedRecord"), mask_channels);
        }
        break;
    case LOG_HDD_WERR:
        ez_snprintf(context, len, type_str_hddwrite.c_str(), item.data, *(uint *)item.context);
        break;
    case LOG_HDD_RERR:
        ez_snprintf(context, len, type_str_hddread.c_str(), item.data, *(uint *)item.context);
        break;
    case LOG_HDD_TYPE:
        ez_snprintf(context, len, type_str_hddtype.c_str(), item.data);
        break;
    case LOG_HDD_FORMAT:
        ez_snprintf(context, len, type_str_hddformat.c_str(), item.data);
        break;
    case LOG_HDD_NOSPACE:
        ez_snprintf(context, len, type_str_hddnospace.c_str(), item.data);
        break;
    case LOG_LOGIN:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_login.c_str(),username);
        break;
    case LOG_ILLEGAL_ACCESS:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_illegalaccess.c_str(),username);
        break;
    case LOG_LOGOUT:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_logout.c_str(),username);
        break;
    case LOG_ADD_USER:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_adduser.c_str(),username);
        break;
    case LOG_DELETE_USER:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_deluser.c_str(),username);
        break;
    case LOG_MODIFY_USER:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_moduser.c_str(),username);
        break;
        
    //add by wyf on 090730
    case LOG_MODIFY_PASSWORD:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_modpassword.c_str(),username);
        break;
    //added by wyf on 090813
    case LOG_LOCK_USER:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_lockuser.c_str(),username);
        break;
        
    case LOG_ADD_GROUP:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_addgroup.c_str(),username);
        break;
    case LOG_DELETE_GROUP:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_delgroup.c_str(),username);
        break;
    case LOG_MODIFY_GROUP:
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, type_str_modgroup.c_str(),username);
        break;
    case LOG_CLEAR:
        //ez_snprintf(username, 8, "%s", (char*)item.xContext);
        //ez_snprintf(context, len, "%s <%s>", type_str_clear.c_str(), username);
        //For Log Test
        ez_snprintf(username, 8, "%s", (char*)item.context);
        ez_snprintf(context, len, "%s <%s>", type_str_clear.c_str(), username);
        break;
    case LOG_SEARCH:
        memcpy(&m_log_record, item.context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, rec_operatetime, FT_HALF_YEAR);
        ez_snprintf(context, len, type_str_search.c_str(), 
                m_log_record.chan, rec_operatetime);
        break;
    case LOG_DOWNLOAD:
        memcpy(&m_log_record, item.context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, rec_operatetime, FT_HALF_YEAR);
        ez_snprintf(context, len, type_str_download.c_str(), 
                m_log_record.chan, rec_operatetime);
        break;
    case LOG_PLAYBACK:
        memcpy(&m_log_record, item.context, sizeof(LOG_ITEM_RECORD));
        iData = item.data;
        FormatTimeStringDH(&m_log_record.time, rec_operatetime, FT_HALF_YEAR);
#if defined(LOG_SUPPORT_PLAYBACK)
        if (0 == iData)
        {
            ez_snprintf(context, len, type_str_playback.c_str(), 
                    m_log_record.chan, rec_operatetime);
        }        
        else if (1 == iData)
        {
            ez_snprintf(context, len, type_str_playbackFile.c_str(), 
                    m_log_record.chan, rec_operatetime);
        }        
        else if (2 == iData)
        {
            ez_snprintf(context, len, type_str_playbackStop.c_str(), 
                    m_log_record.chan, rec_operatetime);
        }
#else//LOG_SUPPORT_PLAYBACK
        ez_snprintf(context, len, type_str_playback.c_str(), 
                m_log_record.chan, rec_operatetime);
#endif//LOG_SUPPORT_PLAYBACK
        break;
    case LOG_BACKUP:
        memcpy(&m_log_record, item.context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, rec_operatetime, FT_HALF_YEAR);
        ez_snprintf(context, len, type_str_backup.c_str(), 
                m_log_record.chan, rec_operatetime);
        break;
    case LOG_BACKUPERROR:
        ez_snprintf(context, len, type_str_backuperror.c_str());
        break;
    case LOG_NET_ABORT:
        ez_snprintf(context, len, type_str_netabort.c_str());
        break;
    case LOG_NET_ABORT_RESUME:
        ez_snprintf(context, len, type_str_netresume.c_str());
        break;
    case LOG_NET_IP_CONFLICT:
        ez_snprintf(context, len, type_str_ipconflict.c_str());
        break;
    case LOG_HDD_NODISK:
        ez_snprintf(context, len, type_str_nohdd.c_str());
        break;
    case LOG_HDD_ERROR:
        ez_snprintf(context, len, type_str_hdderror.c_str());
        break;
    default:
        ez_snprintf(context, len, type_str_unknown.c_str());
        break;
    }
    return (item.type >> 8);
}

static char *logReboot[] = {
    "System reboot! flag<0x%02x>",
    "系统启动! 标志<0x%02x>",
};
static char *logShut[] = {
    "System shut down! %s", 
    "系统关机! %s", 
};
static char *conf0[] = {
    "general", "comm", "net", "timing", "image", "ptz", "md", "alarm","timmtd","timalm","title"
};
static char *conf1[] = {
    "普通设置", "串口设置", "网络设置", "定时设置", 
    "图像设置", "云台控制", "动态检测", "报警设置",
    "动检定时",    "报警定时", "通道名称",
};
static char **ConfStr[] = {
    conf0, conf1,
};
static char *logConfSave[] = {
    "Config %s Saved!",
    "保存%s系统配置!",
};
static char *logConfLoad[] = {
    "Config %s Loaded!",
    "读取%s默认配置!",
};

static char *logFSError[] = {
    "File System error!",
    "文件系统错误!",
};

static char *logAlarm[] = {
    "Alarm In! ",
    "外部报警! ",
};
static char *logNetAlarm[] = {
    "Net Alarm! ",
    "网络报警! ",
};

static char *logAlarmEnd[] = {
    "Alarm %d End!",
    "外部报警 %d 结束! ",
};

static char *logHddRead[] = {
    "HDD<%d> read error[%x]!", 
    "读硬盘<%d> 错误 [%x]!",
};

static char *logHddWrite[] = {
    "HDD<%d> write error[%x]!", 
    "写硬盘<%d> 错误 [%x]!",
};
static char *logLogin[] = {
    "<%s>user login!",
    "<%s>用户登录!",
};
static char *logLogout[] = {
    "<%s>user login out!",
    "<%s>用户退出!",
};
static char *logClear[] = {
    "Log cleared!",
    "日志被清除!",
};
static char *logSearch[] = {
    "Search files ch%d type%d %s",
    "录像查询 通道%d 类型%d %s",
};
static char *logDownload[] = {
    "Download files ch%d type%d %s",
    "录像下载 通道%d 类型%d %s",
};
static char *logPlayback[] = {
    "Playback files ch%d type%d %s",
    "录像回放 通道%d 类型%d %s",
};
static char *logUnknown[] = {
    "Unknown log item!",
    "未知日志项!",
};

void CLog::Translate(LOG_ITEM *pItem, uint Language)
{
	if(LOG_FUN_CLOSE)	return;
	
    uchar *context = pItem->context;
    uint alm;
    int i;
    char time[32];
    LOG_ITEM_RECORD m_log_record;

    switch (pItem->type) {
    case LOG_REBOOT:
        trace(logReboot[Language], pItem->data);
        break;
    case LOG_SHUT:
        FormatTimeStringDH((DHTIME*)context, time, FT_NORMAL);
        trace(logShut[Language], time);
        break;
    case LOG_CONFSAVE:
        trace(logConfSave[Language], ConfStr[Language][pItem->data]);
        break;
    case LOG_CONFLOAD:
        trace(logConfLoad[Language], ConfStr[Language][pItem->data]);
        break;
    case LOG_FSERROR:
        trace(logFSError[Language]);
        break;
    case LOG_ALM_IN:
        trace(logAlarm[Language],pItem->data);
        alm = *(uint*)pItem->context;
        for(i =0; i<N_ALM_IN; i++){
            if(alm & BITMSK(i)){
                trace("%d ",i+1);
            }
        }
        break;
    case LOG_NETALM_IN:
    {
	trace(logNetAlarm[Language],pItem->data);
        alm = *(uint*)pItem->context;
        for(i =0; i<N_ALM_IN; i++){
            if(alm & BITMSK(i)){
                trace("%d ",i+1);
            }
        }        
        break;
    }
    case LOG_ALM_END:
        trace(logAlarmEnd[Language],pItem->data); 
        break;
    case LOG_HDD_WERR:
        trace(logHddWrite[Language], pItem->data, *(uint *)pItem->context);
        break;
    case LOG_HDD_RERR:
        trace(logHddRead[Language], pItem->data, *(uint *)pItem->context);
        break;
    case LOG_LOGIN:
        if(!pItem->data)
            trace(logLogout[Language],pItem->context);
        else 
            trace(logLogin[Language],pItem->context);
        break;
    case LOG_CLEAR:
        trace(logClear[Language]);
    case LOG_SEARCH:
        memcpy(&m_log_record, context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, time, FT_NORMAL);
        trace(logSearch[Language], m_log_record.chan, m_log_record.type, time);
        break;
    case LOG_DOWNLOAD:
        memcpy(&m_log_record, context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, time, FT_NORMAL);
        trace(logDownload[Language], m_log_record.chan, m_log_record.type, time);
        break;
    case LOG_PLAYBACK:
        memcpy(&m_log_record, context, sizeof(LOG_ITEM_RECORD));
        FormatTimeStringDH(&m_log_record.time, time, FT_NORMAL);
        trace(logPlayback[Language], m_log_record.chan, m_log_record.type, time);
        break;
    default:
        trace(logUnknown[Language]);
    }
}

//保存用户对于日志类型的设置
void CLog::SetLogFilter(int *Type)
{
	if(LOG_FUN_CLOSE)	return;
	
    CGuard guard(m_Mutex);
    if(!m_FileLogSet->Open(FileLogSetName, CFile::modeReadWrite))
    {
        trace("Fail to open logset file\n");
        return;
    }

    memcpy(m_SavedLogType, Type, LOG_TYPE_NR * sizeof(int));
    m_SavedLogType[LOG_TYPE_MAX] = Type[LOG_TYPE_NR];
    m_FileLogSet->Write(m_SavedLogType, (LOG_TYPE_MAX + 1) * sizeof(int));
    m_FileLogSet->Close();
}


//读取用户对于日志类型的设置
void CLog::GetLogFilter(int *Type)
{
	if(LOG_FUN_CLOSE)	return;
	
    CGuard guard(m_Mutex);
    memcpy(Type, m_SavedLogType, LOG_TYPE_NR * sizeof(int));
    Type[LOG_TYPE_NR] = m_SavedLogType[LOG_TYPE_MAX];
}

void CLog::setLogDisable()
{
	if(LOG_FUN_CLOSE)	return;
	
    m_bLogDisable = TRUE;
    return;
}

PATTERN_SINGLETON_IMPLEMENT(CLogToSDCard);

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CLogToSDCard::CLogToSDCard() :CThread("CLogToSDCard", TP_NET)
,m_iEnable(0),m_iWirteStatus(0),m_iFd(-1)
{
	trace("CLogToSDCard Enter--------\n");

}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CLogToSDCard::~CLogToSDCard()
{	
	m_bLoop = 0;
}



VD_BOOL CLogToSDCard::Start()
{
	if (m_bLoop)
	{
		return TRUE;
	}

	int ret = CreateThread();

	return ret;
}
VD_BOOL CLogToSDCard::NetGetDebugEnable()
{
	int Enable	= 0;
	FILE *ConfFp = NULL;
	int iRet =0;
	char NetDbgPath[1024];	
	snprintf(NetDbgPath,1024,"%s/log/%s",TARGET_DIR,"DbgEnv");
	if(0 != access(NetDbgPath,0))
	{
		_printd("DbgEnv:%s is not exist! \n",NetDbgPath);
		return -1;
	}
	else
	{		
		ConfFp = fopen(NetDbgPath, "r");
		if (ConfFp == NULL) {
			_printd("open file %s Error", NetDbgPath);
			return -1;
		}
		iRet = fscanf(ConfFp, "%d",&Enable);
		_printd("read %s file enable = %d ",NetDbgPath,Enable);
		fclose(ConfFp); 	
	}
    m_iEnable = Enable;
	_printd("DEBUG VERSION %d!!!!!!!!!\n",Enable);
	return Enable;
}
VD_BOOL CLogToSDCard::NetSetDebugEnable(int iEnable)
{
	FILE *ConfFp = NULL;
	int iRet =0;
	char NetDbgPath[1024] = {0};	

	snprintf(NetDbgPath,1024,"%s/log/%s",TARGET_DIR,"DbgEnv");
	ConfFp = fopen(NetDbgPath, "w");
	if (ConfFp == NULL) return -1;
	fprintf (ConfFp, "%d", iEnable);
	fclose(ConfFp);
	
    m_iEnable = iEnable;
	_printd("DEBUG VERSION %d!!!!!!!!!\n",iEnable);
	return iEnable;
}

VD_BOOL CLogToSDCard::WriteToFile()
{
	if(0 == m_iWirteStatus && g_DriverManager.GetDetectEnable())
	{
		char logpath[128] = {0};
		char logfullpath[128] = {0};
		//只用一个文件 防止占用SD卡过大
		snprintf(logpath, 128, "%s/log",TARGET_DIR);

		if(access(logpath, F_OK) != 0)
		{
			mkdir(logpath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		}
#if 1
		time_t currTime;             
		struct tm *currDate;
		struct timezone tz;
		struct timeval tv;

		gettimeofday(&tv, &tz);
		currTime = tv.tv_sec + tz.tz_minuteswest * 60;
		currDate = gmtime(&currTime);

		memset(m_DateStr, 0, 40);

		strftime(m_DateStr, 40, "%Y-%m-%d-%H-%M-%S", currDate); //24小时制
		printf("WriteToFile Log date is %s\n",m_DateStr);
#endif
		
		

		//snprintf(logfullpath, 128, "%s/log",logpath);//org
		snprintf(logfullpath, 128, "%s/log-%s",logpath,m_DateStr);//test
		
		_printd("@logfile :%s",logfullpath);
		int fd = open(logfullpath, O_CREAT|O_RDWR|O_TRUNC,S_IRUSR|S_IWUSR);
		if(fd < 0)
		{
			printf("Open Error!!\n");
			return 0;
		}


		m_iFd = dup2(fd, STDOUT_FILENO);
		if(m_iFd < 0)
		{
			printf("Error!!\n");
			//close(m_iFd);
			m_iFd = -1;
			return 0;
		}
		m_iWirteStatus = 1;

	}
}

void CLogToSDCard::ThreadProc()
{
	int timeout = 0;
	while (m_bLoop)
	{
		NetGetDebugEnable();
		if(m_iEnable)
		{
			WriteToFile();
		}
		sleep(30);
		if(m_iWirteStatus)
		{
			timeout++;
			if(timeout >= 120)// 一个小时
			{
				//m_iEnable--;
				timeout = 0;
				//if(m_iEnable <= 0)
				//{
					m_iWirteStatus = 0;
					close(m_iFd);
					m_iFd = -1;
				//NetSetDebugEnable(0);					
				//}
			}
		}
	}
}


