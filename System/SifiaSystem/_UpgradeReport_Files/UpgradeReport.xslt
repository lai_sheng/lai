<<<<<<< HEAD
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:output omit-xml-declaration="yes"/>

  <!-- Keys -->
  <xsl:key name="ProjectKey" match="Event" use="@Project"/>

  <!-- String split template -->
  <xsl:template name="SplitString">
    <xsl:param name="source" select="''"/>
    <xsl:param name="separator" select="','"/>
    <xsl:if test="not($source = '' or $separator = '')">
      <xsl:variable name="head" select="substring-before(concat($source, $separator), $separator)"/>
      <xsl:variable name="tail" select="substring-after($source, $separator)"/>
      <part>
        <xsl:value-of select="$head"/>
      </part>
      <xsl:call-template name="SplitString">
        <xsl:with-param name="source" select="$tail"/>
        <xsl:with-param name="separator" select="$separator"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Intermediate Templates -->
  <xsl:template match="UpgradeReport" mode="ProjectOverviewXML">
    <Projects>
      <xsl:for-each select="Event[generate-id(.) = generate-id(key('ProjectKey', @Project))]">
        <Project>
          <xsl:variable name="pNode" select="current()"/>
          <xsl:variable name="errorCount" select="count(../Event[@Project = current()/@Project and @ErrorLevel=2])"/>
          <xsl:variable name="warningCount" select="count(../Event[@Project = current()/@Project and @ErrorLevel=1])"/>
          <xsl:variable name="messageCount" select="count(../Event[@Project = current()/@Project and @ErrorLevel=0])"/>
          <xsl:variable name="pathSplitSeparator">
            <xsl:text>\</xsl:text>
          </xsl:variable>
          <xsl:variable name="projectName">
            <xsl:choose>
              <xsl:when test="@Project = ''">Solution</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@Project"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:attribute name="IsSolution">
            <xsl:value-of select="@Project = ''"/>
          </xsl:attribute>
          <xsl:attribute name="Project">
            <xsl:value-of select="$projectName"/>
          </xsl:attribute>
          <xsl:attribute name="ProjectDisplayName">

            <xsl:variable name="localProjectName" select="@Project"/>

            <!-- Sometimes it is possible to have project name set to a path over a real project name,
                 we split the string on '\' and if we end up with >1 part in the resulting tokens set
                 we format the ProjectDisplayName as ..\prior\last -->
            <xsl:variable name="pathTokens">
              <xsl:call-template name="SplitString">
                <xsl:with-param name="source" select="$localProjectName"/>
                <xsl:with-param name="separator" select="$pathSplitSeparator"/>
              </xsl:call-template>
            </xsl:variable>

            <xsl:choose>
              <xsl:when test="count(msxsl:node-set($pathTokens)/part) &gt; 1">
                <xsl:value-of select="concat('..', $pathSplitSeparator, msxsl:node-set($pathTokens)/part[last() - 1], $pathSplitSeparator, msxsl:node-set($pathTokens)/part[last()])"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$localProjectName"/>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:attribute>
          <xsl:attribute name="ProjectSafeName">
            <xsl:value-of select="translate($projectName, '\', '-')"/>
          </xsl:attribute>
          <xsl:attribute name="Solution">
            <xsl:value-of select="/UpgradeReport/Properties/Property[@Name='Solution']/@Value"/>
          </xsl:attribute>
          <xsl:attribute name="Source">
            <xsl:value-of select="@Source"/>
          </xsl:attribute>
          <xsl:attribute name="Status">
            <xsl:choose>
              <xsl:when test="$errorCount &gt; 0">Error</xsl:when>
              <xsl:when test="$warningCount &gt; 0">Warning</xsl:when>
              <xsl:otherwise>Success</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ErrorCount">
            <xsl:value-of select="$errorCount"/>
          </xsl:attribute>
          <xsl:attribute name="WarningCount">
            <xsl:value-of select="$warningCount"/>
          </xsl:attribute>
          <xsl:attribute name="MessageCount">
            <xsl:value-of select="$messageCount"/>
          </xsl:attribute>
          <xsl:attribute name="TotalCount">
            <xsl:value-of select="$errorCount + $warningCount + $messageCount"/>
          </xsl:attribute>
          <xsl:for-each select="../Event[@Project = $pNode/@Project and @ErrorLevel=3]">
            <ConversionStatus>
              <xsl:value-of select="@Description"/>
            </ConversionStatus>
          </xsl:for-each>
          <Messages>
            <xsl:for-each select="../Event[@Project = $pNode/@Project and @ErrorLevel&lt;3]">
              <Message>
                <xsl:attribute name="Level">
                  <xsl:value-of select="@ErrorLevel"/>
                </xsl:attribute>
                <xsl:attribute name="Status">
                  <xsl:choose>
                    <xsl:when test="@ErrorLevel = 0">Message</xsl:when>
                    <xsl:when test="@ErrorLevel = 1">Warning</xsl:when>
                    <xsl:when test="@ErrorLevel = 2">Error</xsl:when>
                    <xsl:otherwise>Message</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="Source">
                  <xsl:value-of select="@Source"/>
                </xsl:attribute>
                <xsl:attribute name="Message">
                  <xsl:value-of select="@Description"/>
                </xsl:attribute>
              </Message>
            </xsl:for-each>
          </Messages>
        </Project>
      </xsl:for-each>
    </Projects>
  </xsl:template>



  <!-- Project Overview template -->
  <xsl:template match="Projects" mode="ProjectOverview">

    <table>
      <tr>
        <th></th>
        <th _locID="ProjectTableHeader">项目</th>
        <th _locID="PathTableHeader">路径</th>
        <th _locID="ErrorsTableHeader">错误</th>
        <th _locID="WarningsTableHeader">警告</th>
        <th _locID="MessagesTableHeader">消息</th>
      </tr>

      <xsl:for-each select="Project">

        <xsl:sort select="@ErrorCount" order="descending"/>
        <xsl:sort select="@WarningCount" order="descending"/>
        <!-- Always make solution last within a group -->
        <xsl:sort select="@IsSolution" order="ascending"/>
        <xsl:sort select="@ProjectSafeName" order="ascending"/>

        <tr>
          <td>
            <img width="16" height="16">
              <xsl:attribute name="src">
                <xsl:choose>
                  <xsl:when test="@Status = 'Error'">_UpgradeReport_Files\UpgradeReport_Error.png</xsl:when>
                  <xsl:when test="@Status = 'Warning'">_UpgradeReport_Files\UpgradeReport_Warning.png</xsl:when>
                  <xsl:when test="@Status = 'Success'">_UpgradeReport_Files\UpgradeReport_Success.png</xsl:when>
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="alt">
                <xsl:value-of select="@Status"/>
              </xsl:attribute>
            </img>
          </td>
          <td>
            <strong>
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat('#', @ProjectSafeName)"/>
                </xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@ProjectDisplayName = ''">
                    <span _locID="OverviewSolutionSpan">解决方案</span>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@ProjectDisplayName"/>
                  </xsl:otherwise>
                </xsl:choose>
              </a>
            </strong>
          </td>
          <td>
            <xsl:value-of select="@Source"/>
          </td>
          <td class="textCentered">
            <a>
              <xsl:if test="@ErrorCount &gt; 0">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat('#', @ProjectSafeName, 'Error')"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="@ErrorCount"/>
            </a>
          </td>
          <td class="textCentered">
            <a>
              <xsl:if test="@WarningCount &gt; 0">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat('#', @ProjectSafeName, 'Warning')"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="@WarningCount"/>
            </a>
          </td>
          <td class="textCentered">
            <a href="#">
              <xsl:if test="@MessageCount &gt; 0">
                <xsl:attribute name="onclick">
                  <xsl:variable name="apos">
                    <xsl:text>'</xsl:text>
                  </xsl:variable>
                  <xsl:variable name="JS" select="concat('ScrollToFirstVisibleMessage(', $apos, @ProjectSafeName, $apos, ')')"/>
                  <xsl:value-of select="concat($JS, '; return false;')"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="@MessageCount"/>
            </a>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

  <!-- Show messages row -->
  <xsl:template match="Project" mode="ProjectShowMessages">
    <tr>
      <xsl:attribute name="name">
        <xsl:value-of select="concat('MessageRowHeaderShow', @ProjectSafeName)"/>
      </xsl:attribute>
      <td>
        <img width="16" height="16" src="_UpgradeReport_Files\UpgradeReport_Information.png"/>
      </td>
      <td class="messageCell">
        <xsl:variable name="apos">
          <xsl:text>'</xsl:text>
        </xsl:variable>
        <xsl:variable name="toggleRowsJS" select="concat('ToggleMessageVisibility(', $apos, @ProjectSafeName, $apos, ')')"/>

        <a _locID="ShowAdditionalMessages" href="#">
          <xsl:attribute name="name">
            <xsl:value-of select="concat(@ProjectSafeName, 'Message')"/>
          </xsl:attribute>
          <xsl:attribute name="onclick">
            <xsl:value-of select="concat($toggleRowsJS, '; return false;')"/>
          </xsl:attribute>
          显示 <xsl:value-of select="@MessageCount"/> 其他消息
        </a>
      </td>
    </tr>
  </xsl:template>

  <!-- Hide messages row -->
  <xsl:template match="Project" mode="ProjectHideMessages">
    <tr style="display: none">
      <xsl:attribute name="name">
        <xsl:value-of select="concat('MessageRowHeaderHide', @ProjectSafeName)"/>
      </xsl:attribute>
      <td>
        <img width="16" height="16" src="_UpgradeReport_Files\UpgradeReport_Information.png"/>
      </td>
      <td class="messageCell">
        <xsl:variable name="apos">
          <xsl:text>'</xsl:text>
        </xsl:variable>
        <xsl:variable name="toggleRowsJS" select="concat('ToggleMessageVisibility(', $apos, @ProjectSafeName, $apos, ')')"/>

        <a _locID="HideAdditionalMessages" href="#">
          <xsl:attribute name="name">
            <xsl:value-of select="concat(@ProjectSafeName, 'Message')"/>
          </xsl:attribute>
          <xsl:attribute name="onclick">
            <xsl:value-of select="concat($toggleRowsJS, '; return false;')"/>
          </xsl:attribute>
          隐藏 <xsl:value-of select="@MessageCount"/> 其他消息
        </a>
      </td>
    </tr>
  </xsl:template>

  <!-- Message row templates -->
  <xsl:template match="Message">
    <tr>
      <xsl:attribute name="name">
        <xsl:value-of select="concat(@Status, 'RowClass', ../../@ProjectSafeName)"/>
      </xsl:attribute>

      <xsl:if test="@Level = 0">
        <xsl:attribute name="style">display: none</xsl:attribute>
      </xsl:if>
      <td>
        <a>
          <xsl:attribute name="name">
            <xsl:value-of select="concat(../../@ProjectSafeName, @Status)"/>
          </xsl:attribute>
        </a>
        <img width="16" height="16">
          <xsl:attribute name="src">
            <xsl:choose>
              <xsl:when test="@Status = 'Error'">_UpgradeReport_Files\UpgradeReport_Error.png</xsl:when>
              <xsl:when test="@Status = 'Warning'">_UpgradeReport_Files\UpgradeReport_Warning.png</xsl:when>
              <xsl:when test="@Status = 'Message'">_UpgradeReport_Files\UpgradeReport_Information.png</xsl:when>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="alt">
            <xsl:value-of select="@Status"/>
          </xsl:attribute>
        </img>
      </td>
      <td class="messageCell">
        <strong>
          <xsl:value-of select="@Source"/>:
        </strong>
        <span>
          <xsl:value-of select="@Message"/>
        </span>
      </td>
    </tr>
  </xsl:template>

  <!-- Project Details Template -->
  <xsl:template match="Projects" mode="ProjectDetails">

    <xsl:for-each select="Project">
      <xsl:sort select="@ErrorCount" order="descending"/>
      <xsl:sort select="@WarningCount" order="descending"/>
      <!-- Always make solution last within a group -->
      <xsl:sort select="@IsSolution" order="ascending"/>
      <xsl:sort select="@ProjectSafeName" order="ascending"/>

      <a>
        <xsl:attribute name="name">
          <xsl:value-of select="@ProjectSafeName"/>
        </xsl:attribute>
      </a>
      <xsl:choose>
        <xsl:when test="@ProjectDisplayName = ''">
          <h3 _locID="ProjectDisplayNameHeader">解决方案</h3>
        </xsl:when>
        <xsl:otherwise>
          <h3>
            <xsl:value-of select="@ProjectDisplayName"/>
          </h3>
        </xsl:otherwise>
      </xsl:choose>

      <table>
        <tr>
          <xsl:attribute name="id">
            <xsl:value-of select="concat(@ProjectSafeName, 'HeaderRow')"/>
          </xsl:attribute>
          <th></th>
          <th class="messageCell" _locID="MessageTableHeader">消息</th>
        </tr>

        <!-- Errors and warnings -->
        <xsl:for-each select="Messages/Message[@Level &gt; 0]">
          <xsl:sort select="@Level" order="descending"/>
          <xsl:apply-templates select="."/>
        </xsl:for-each>

        <xsl:if test="@MessageCount &gt; 0">
          <xsl:apply-templates select="." mode="ProjectShowMessages"/>
        </xsl:if>

        <!-- Messages -->
        <xsl:for-each select="Messages/Message[@Level = 0]">
          <xsl:apply-templates select="."/>
        </xsl:for-each>

        <xsl:choose>
          <!-- Additional row as a 'place holder' for 'Show/Hide' additional messages -->
          <xsl:when test="@MessageCount &gt; 0">
            <xsl:apply-templates select="." mode="ProjectHideMessages"/>
          </xsl:when>
          <!-- No messages at all, show blank row -->
          <xsl:when test="@TotalCount = 0">
            <tr>
              <td>
                <img width="16" height="16" src="_UpgradeReport_Files\UpgradeReport_Information.png"/>
              </td>
              <xsl:choose>
                <xsl:when test="@ProjectDisplayName = ''">
                  <td class="messageCell" _locID="NoMessagesRow2">
                    解决方案未记录任何消息。
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td class="messageCell" _locID="NoMessagesRow">
                    <xsl:value-of select="@ProjectDisplayName"/> 未记录任何消息。
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
          </xsl:when>
        </xsl:choose>
      </table>
    </xsl:for-each>
  </xsl:template>

  <!-- Document, matches "UpgradeReport" -->
  <xsl:template match="UpgradeReport">
    <!-- Output doc type the 'Mark of the web' which disabled prompting to run JavaScript from local HTML Files in IE -->
    <!-- NOTE: The whitespace around the 'Mark of the web' is important it must be exact -->
    <xsl:text disable-output-escaping="yes"><![CDATA[<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
]]>
    </xsl:text>
    <html>
      <head>
        <meta content="en-us" http-equiv="Content-Language"/>
        <meta content="text/html; charset=utf-16" http-equiv="Content-Type"/>
        <link type="text/css" rel="stylesheet" href="_UpgradeReport_Files\UpgradeReport.css"/>
        <title _locID="ConversionReport0">
          迁移报告
        </title>

        <script type="text/javascript" language="javascript">
          <xsl:text disable-output-escaping="yes">
          <![CDATA[
          
            // Startup 
            // Hook up the the loaded event for the document/window, to linkify the document content
            var startupFunction = function() { linkifyElement("messages"); };
            
            if(window.attachEvent)
            {
              window.attachEvent('onload', startupFunction);
            }
            else if (window.addEventListener) 
            {
              window.addEventListener('load', startupFunction, false);
            }
            else 
            {
              document.addEventListener('load', startupFunction, false);
            } 
            
            // Toggles the visibility of table rows with the specified name 
            function toggleTableRowsByName(name)
            {
               var allRows = document.getElementsByTagName('tr');
               for (i=0; i < allRows.length; i++)
               {
                  var currentName = allRows[i].getAttribute('name');
                  if(!!currentName && currentName.indexOf(name) == 0)
                  {
                      var isVisible = allRows[i].style.display == ''; 
                      isVisible ? allRows[i].style.display = 'none' : allRows[i].style.display = '';
                  }
               }
            }
            
            function scrollToFirstVisibleRow(name) 
            {
               var allRows = document.getElementsByTagName('tr');
               for (i=0; i < allRows.length; i++)
               {
                  var currentName = allRows[i].getAttribute('name');
                  var isVisible = allRows[i].style.display == ''; 
                  if(!!currentName && currentName.indexOf(name) == 0 && isVisible)
                  {
                     allRows[i].scrollIntoView(true); 
                     return true; 
                  }
               }
               
               return false;
            }
            
            // Linkifies the specified text content, replaces candidate links with html links 
            function linkify(text)
            {
                 if(!text || 0 === text.length)
                 {
                     return text; 
                 }

                 // Find http, https and ftp links and replace them with hyper links 
                 var urlLink = /(http|https|ftp)\:\/\/[a-zA-Z0-9\-\.]+(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\._\?\,\/\\\+&%\$#\=~;\{\}])*/gi;
                 
                 return text.replace(urlLink, '<a href="$&">$&</a>') ;
            }
            
            // Linkifies the specified element by ID
            function linkifyElement(id)
            {
                var element = document.getElementById(id);
                if(!!element)
                {
                  element.innerHTML = linkify(element.innerHTML); 
                }
            }
            
            function ToggleMessageVisibility(projectName)
            {
              if(!projectName || 0 === projectName.length)
              {
                return; 
              }
              
              toggleTableRowsByName("MessageRowClass" + projectName);
              toggleTableRowsByName('MessageRowHeaderShow' + projectName);
              toggleTableRowsByName('MessageRowHeaderHide' + projectName); 
            }
            
            function ScrollToFirstVisibleMessage(projectName)
            {
              if(!projectName || 0 === projectName.length)
              {
                return; 
              }
              
              // First try the 'Show messages' row
              if(!scrollToFirstVisibleRow('MessageRowHeaderShow' + projectName))
              {
                // Failed to find a visible row for 'Show messages', try an actual message row 
                scrollToFirstVisibleRow('MessageRowClass' + projectName); 
              }
            }
          ]]>
        </xsl:text>
        </script>
      </head>
      <body>
        <h1 _locID="ConversionReport">
          迁移报告 - <xsl:value-of select="Properties/Property[@Name='Solution']/@Value"/>
        </h1>

        <div id="content">
          <h2 _locID="OverviewTitle">概述</h2>
          <xsl:variable name="projectOverview">
            <xsl:apply-templates select="self::node()" mode="ProjectOverviewXML"/>
          </xsl:variable>

          <div id="overview">
            <xsl:apply-templates select="msxsl:node-set($projectOverview)/*" mode="ProjectOverview"/>
          </div>

          <h2 _locID="SolutionAndProjectsTitle">解决方案和项目</h2>

          <div id="messages">
            <xsl:apply-templates select="msxsl:node-set($projectOverview)/*" mode="ProjectDetails"/>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
=======
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

    <xsl:key name="ProjectKey" match="Event" use="@Project"/>

    <xsl:template match="Events" mode="createProjects">
        <projects>
            <xsl:for-each select="Event">
                <!--xsl:sort select="@Project" order="descending"/-->
                <xsl:if test="(1=position()) or (preceding-sibling::*[1]/@Project != @Project)">

                    <xsl:variable name="ProjectName" select="@Project"/>

                    <project>
                        <xsl:attribute name="name">
                            <xsl:value-of select="@Project"/>
                        </xsl:attribute> 

                        <xsl:if test="@Project=''">
                        <xsl:attribute name="solution">
                            <xsl:value-of select="@Solution"/>
                        </xsl:attribute> 
                        </xsl:if>

                        <xsl:for-each select="key('ProjectKey', $ProjectName)">
                            <!--xsl:sort select="@Source" /-->
                            <xsl:if test="(1=position()) or (preceding-sibling::*[1]/@Source != @Source)">

                                <source>
                                    <xsl:attribute name="name">
                                        <xsl:value-of select="@Source"/>
                                    </xsl:attribute>

                                    <xsl:variable name="Source">
                                        <xsl:value-of select="@Source"/>
                                    </xsl:variable>

                                    <xsl:for-each select="key('ProjectKey', $ProjectName)[ @Source = $Source ]">

                                        <event>
                                            <xsl:attribute name="error-level">
                                                <xsl:value-of select="@ErrorLevel"/>
                                            </xsl:attribute> 
                                            <xsl:attribute name="description">
                                                <xsl:value-of select="@Description"/>
                                            </xsl:attribute> 
                                        </event>
                                    </xsl:for-each>
                                </source>
                            </xsl:if>
                        </xsl:for-each>

                    </project>
                </xsl:if>
            </xsl:for-each>
        </projects>
    </xsl:template>

    <xsl:template match="projects">
    <xsl:for-each select="project">
    <xsl:sort select="@Name" order="ascending"/>
        <h2>
        <xsl:if test="@solution"><a _locID="Solution">解决方案</a>: <xsl:value-of select="@solution"/></xsl:if>
        <xsl:if test="not(@solution)"><a _locID="Project">项目</a>: <xsl:value-of select="@name"/>
            <xsl:for-each select="source">
                <xsl:variable name="Hyperlink" select="@name"/>
            <xsl:for-each select="event[@error-level='4']">
             <A class="note"><xsl:attribute name="HREF"><xsl:value-of select="$Hyperlink"/></xsl:attribute><xsl:value-of select="@description"/></A>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:if>
        </h2>

        <table cellpadding="2" cellspacing="0" width="98%" border="1" bordercolor="white" class="infotable">
            <tr>
                <td nowrap="1" class="header" _locID="Filename">文件名</td>
                <td nowrap="1" class="header" _locID="Status">状态</td>
                <td nowrap="1" class="header" _locID="Errors">错误</td>
                <td nowrap="1" class="header" _locID="Warnings">警告</td>
            </tr>

            <xsl:for-each select="source">
                <xsl:sort select="@name" order="ascending"/>
                <xsl:variable name="source-id" select="generate-id(.)"/>

                <xsl:if test="count(event)!=count(event[@error-level='4'])">

                <tr class="row">
                    <td class="content">
                        <A HREF="javascript:"><xsl:attribute name="onClick">javascript:document.images['<xsl:value-of select="$source-id"/>'].click()</xsl:attribute><IMG border="0" _locID="IMG.alt" _locAttrData="alt" alt="展开/折叠节" class="expandable" height="11" onclick="changepic()" src="_UpgradeReport_Files/UpgradeReport_Plus.gif" width="9"><xsl:attribute name="name"><xsl:value-of select="$source-id"/></xsl:attribute><xsl:attribute name="child">src<xsl:value-of select="$source-id"/></xsl:attribute></IMG></A> <xsl:value-of select="@name"/> 
                    </td>
                    <td class="content">
                        <xsl:if test="count(event[@error-level='3'])=1">
                            <xsl:for-each select="event[@error-level='3']">
                            <xsl:if test="@description='Converted'"><a _locID="Converted1">已转换</a></xsl:if>
                            <xsl:if test="@description!='Converted'"><xsl:value-of select="@description"/></xsl:if>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="count(event[@error-level='3'])!=1 and count(event[@error-level='3' and @description='Converted'])!=0"><a _locID="Converted2">已转换</a>
                        </xsl:if>
                    </td>
                    <td class="content"><xsl:value-of select="count(event[@error-level='2'])"/></td>
                    <td class="content"><xsl:value-of select="count(event[@error-level='1'])"/></td>
                </tr>

                <tr class="collapsed" bgcolor="#ffffff">
                    <xsl:attribute name="id">src<xsl:value-of select="$source-id"/></xsl:attribute>

                    <td colspan="7">
                        <table width="97%" border="1" bordercolor="#dcdcdc" rules="cols" class="issuetable">
                            <tr>
                                <td colspan="7" class="issuetitle" _locID="ConversionIssues">转换问题 - <xsl:value-of select="@name"/>:</td>
                            </tr>

                            <xsl:for-each select="event[@error-level!='3']">
                                <xsl:if test="@error-level!='4'">
                                <tr>
                                    <td class="issuenone" style="border-bottom:solid 1 lightgray">
                                        <xsl:value-of select="@description"/>
                                    </td>
                                </tr>
                                </xsl:if>
                            </xsl:for-each>
                        </table>
                    </td>
                </tr>
                </xsl:if>
            </xsl:for-each>

            <tr valign="top">
                <td class="foot">
                    <xsl:if test="count(source)!=1">
                        <xsl:value-of select="count(source)"/><a _locID="file1"> 个文件</a>
                    </xsl:if>
                    <xsl:if test="count(source)=1">
                        <a _locID="file2">1 个文件</a>
                    </xsl:if>
                </td>
                <td class="foot">
					<a _locID="Converted3">已转换</a>: <xsl:value-of select="count(source/event[@error-level='3' and @description='Converted'])"/><BR/>
					<a _locID="NotConverted">未转换</a>: <xsl:value-of select="count(source) - count(source/event[@error-level='3' and @description='Converted'])"/>
                </td>
                <td class="foot"><xsl:value-of select="count(source/event[@error-level='2'])"/></td>
                <td class="foot"><xsl:value-of select="count(source/event[@error-level='1'])"/></td>
            </tr>
        </table>
    </xsl:for-each>
    </xsl:template>

    <xsl:template match="Property">
        <xsl:if test="@Name!='Date' and @Name!='Time' and @Name!='LogNumber' and @Name!='Solution'">
        <tr><td nowrap="1"><b><xsl:value-of select="@Name"/>: </b><xsl:value-of select="@Value"/></td></tr>
        </xsl:if>
    </xsl:template>

    <xsl:template match="UpgradeLog">
        <html>
            <head>
                <META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8"/>
                <link rel="stylesheet" href="_UpgradeReport_Files\UpgradeReport.css"/>
                <title _locID="ConversionReport0">转换报告 
                    <xsl:if test="Properties/Property[@Name='LogNumber']">
                        <xsl:value-of select="Properties/Property[@Name='LogNumber']/@Value"/>
                    </xsl:if>
                </title>
                <script language="javascript">
                    function outliner () {
                        oMe = window.event.srcElement
                        //get child element
                        var child = document.all[event.srcElement.getAttribute("child",false)];
                        //if child element exists, expand or collapse it.
                        if (null != child)
                            child.className = child.className == "collapsed" ? "expanded" : "collapsed";
                    }

                    function changepic() {
                        uMe = window.event.srcElement;
                        var check = uMe.src.toLowerCase();
                        if (check.lastIndexOf("upgradereport_plus.gif") != -1)
                        {
                            uMe.src = "_UpgradeReport_Files/UpgradeReport_Minus.gif"
                        }
                        else
                        {
                            uMe.src = "_UpgradeReport_Files/UpgradeReport_Plus.gif"
                        }
                    }
                </script>
            </head>
            <body topmargin="0" leftmargin="0" rightmargin="0" onclick="outliner();">
                <h1 _locID="ConversionReport">转换报告 - <xsl:value-of select="Properties/Property[@Name='Solution']/@Value"/></h1>

                <p><span class="note">
                <b _locID="TimeOfConversion">转换时间:</b>  <xsl:value-of select="Properties/Property[@Name='Date']/@Value"/>  <xsl:value-of select="Properties/Property[@Name='Time']/@Value"/><br/>
                </span></p>

                <xsl:variable name="SortedEvents">
                    <Events>
                        <xsl:for-each select="Event">
                            <xsl:sort select="@Project" order="ascending"/>
                            <xsl:sort select="@Source" order="ascending"/>
                            <xsl:sort select="@ErrorLevel" order="ascending"/>
                            <Event>
                                <xsl:attribute name="Project"><xsl:value-of select="@Project"/> </xsl:attribute> 
                                <xsl:attribute name="Solution"><xsl:value-of select="/UpgradeLog/Properties/Property[@Name='Solution']/@Value"/> </xsl:attribute> 
                                <xsl:attribute name="Source"><xsl:value-of select="@Source"/> </xsl:attribute> 
                                <xsl:attribute name="ErrorLevel"><xsl:value-of select="@ErrorLevel"/> </xsl:attribute> 
                                <xsl:attribute name="Description"><xsl:value-of select="@Description"/> </xsl:attribute> 
                            </Event>
                        </xsl:for-each>     
                    </Events>
                </xsl:variable>
                
                <xsl:variable name="Projects">
                    <xsl:apply-templates select="msxsl:node-set($SortedEvents)/*" mode="createProjects"/>
                </xsl:variable>

                <xsl:apply-templates select="msxsl:node-set($Projects)/*"/>

                <p></p><p>
                <table class="note">
                    <tr>
                        <td nowrap="1">
                            <b _locID="ConversionSettings">转换设置</b>
                        </td>
                    </tr>
                    <xsl:apply-templates select="Properties"/>
                </table></p>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
>>>>>>> b2941f5eb3dc22f6be9f55ccc82f51a1ec6d6dee
