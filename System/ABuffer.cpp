
#include <string.h>
#include <stdlib.h>

#include "System/ABuffer.h"

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
//#define W_NEW(var,classname) {var = new classname;}
//#define W_DELETE(p) if(NULL != (p)) {delete((p)); (p)=NULL; }
#define W_FREE(p) if(NULL != (p)) {free((p)); (p)=NULL; }


/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
CABuffer::CABuffer():
m_iIncreaseSize(32)
{
	m_pBuf = NULL;
	Initialize();
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

CABuffer::~CABuffer()
{
	// W_DELETE(m_pBuf);
	W_FREE(m_pBuf);
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

void CABuffer::Initialize()
{
	// W_DELETE(m_pBuf);
	W_FREE(m_pBuf);

	m_iDataSize = 0;
	m_iBufSize = 0;
	m_iMaxSize = 65535;

	if (m_iIncreaseSize < 1)
	{
		m_iIncreaseSize = 32;
	}
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

int CABuffer::Append(unsigned char * pszStr, const unsigned int stSize)
{
	if (0 == stSize)
	{
		//
		// Do not return error here
		//
		return 0;
	}

	//
	// I add many conditiion to 
	//		avoid delete the buffered data
	// If want pre alloc mem, 
	//		you should use Reset(true) yourself
	//
	if (NULL == pszStr
		&& NULL == m_pBuf
		&& 0 == m_iDataSize)
	{
		m_pBuf = (unsigned char *) malloc(stSize);

		if (NULL == m_pBuf)
		{
			m_iBufSize = 0;
			return -1;
		}
		else
		{
			m_iBufSize = stSize;
			return 0;
		}
	}

	if (NULL == pszStr)
	{
		return -1;
	}

	size_t tsize;
	tsize = stSize+m_iDataSize;

	// should alloc mem
	if ( tsize > m_iBufSize )
	{
		tsize = ( (unsigned int)( (tsize/m_iIncreaseSize) + 1 ) )
		        * m_iIncreaseSize;

		//alloc a new mem
		//unsigned char *tmpBuf = new unsigned char[tsize];
		unsigned char *tmpBuf = (unsigned char *) malloc(tsize);

		if (NULL == tmpBuf)
		{
			return -1;
		}

		// ins the old one
		memcpy(tmpBuf, m_pBuf, m_iDataSize);
		// ins the new one
		memcpy(tmpBuf+m_iDataSize, pszStr, stSize);

		m_iBufSize = tsize;

		// delete the old buf
		// W_DELETE(m_pBuf);
		W_FREE(m_pBuf);

		m_pBuf = tmpBuf;
	}
	else
	{
		memcpy(m_pBuf+m_iDataSize, pszStr, stSize);
	}

	m_iDataSize += stSize;

	return stSize;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/* */
int CABuffer::Get(unsigned char * pszStr, const unsigned int stSize)
{
	// check input
	if (NULL == pszStr
	    || 0 == stSize
	    || stSize > m_iDataSize)
	{
		return -1;
	}

	memcpy(pszStr, m_pBuf, stSize);

	return 0;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
int CABuffer::Pop(unsigned char * pszStr, const unsigned int stSize)
{
	// check input
	if (0 == stSize)
	{
		return 0;
	}
	else if (stSize > m_iDataSize)
	{
		return -1;
	}

	// 
	if (NULL != pszStr)
	{
		memcpy(pszStr, m_pBuf, stSize);
	}

	m_iDataSize -= stSize;
	memmove(m_pBuf, m_pBuf+stSize, m_iDataSize);

	return 0;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
int CABuffer::Pour(unsigned char * pszStr, const unsigned int stSize)
{
	// check input
	if (0 == stSize)
	{
		return 0;
	}
	else if (stSize > m_iDataSize)
	{
		return -1;
	}

	m_iDataSize -= stSize;

	if (NULL != pszStr)
	{
		memcpy(pszStr, m_pBuf+m_iDataSize, stSize);
	}

	return 0;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
void CABuffer::Reset(const bool isClear)
{
	if (true == isClear)
	{
		Initialize();
	}
	else
	{
		m_iDataSize = 0;
	}
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
unsigned const char *  CABuffer::Buf() const
{
	return m_pBuf;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
unsigned int CABuffer::Size()
{
	return m_iDataSize;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*  */
void CABuffer::SetIncreaseSize(const unsigned int IncreaseSize)
{
	if (IncreaseSize > 0)
	{
		m_iIncreaseSize = IncreaseSize;
	}
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
