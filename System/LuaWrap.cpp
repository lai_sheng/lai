

#include "AuxLibs/Lua/LuaWrap.h"

#if ( defined(DVR_HI) ) && !defined(WIN32)
#undef APP_HAVE_LUAZIP
#undef APP_HAVE_LUASOCKET
#undef APP_HAVE_LUATHREAD
#define APP_HAVE_LUAZIP 	1
#define APP_HAVE_LUASOCKET 	1
#define APP_HAVE_LUATHREAD	1
#endif


extern "C" {
	int luaopen_zip (lua_State *L);
	int luaopen_socket_core (lua_State *L);
	int luaopen_mime_core(lua_State *L);
	int luaopen_lfs(lua_State *L);
	int luaopen_thread(lua_State *L);
	int luaopen_bitlib(lua_State *L);
}

LuaWrap::LuaWrap(lua_State* luaVM)
		:luaStack(luaVM)
{
	if(m_pluaVM) {
		// initialize lua standard library functions
		lua_baselibopen(m_pluaVM);
		lua_tablibopen(m_pluaVM);
		lua_iolibopen(m_pluaVM);
		lua_strlibopen(m_pluaVM);
		lua_mathlibopen(m_pluaVM);
		lua_dblibopen(m_pluaVM);
		luaopen_lfs(m_pluaVM);
		luaopen_bitlib(m_pluaVM);

#if defined(APP_HAVE_LUAZIP) && (APP_HAVE_LUAZIP == 1)
		luaopen_zip(m_pluaVM);  
#endif

#if defined(APP_HAVE_LUASOCKET) && (APP_HAVE_LUASOCKET == 1)
		luaopen_mime_core(m_pluaVM);
                luaopen_socket_core(m_pluaVM);
#endif

#if defined(APP_HAVE_LUATHREAD) && (APP_HAVE_LUATHREAD == 1)
		luaopen_thread(m_pluaVM);
#endif
		m_pParentLuaVM = NULL;
	}
}

LuaWrap::~LuaWrap()
{
	if(m_pParentLuaVM) {
		lua_State *parent = m_pParentLuaVM;
		
		assert(m_pluaVM);
	    /* kill registry reference to thread object */
		lua_lock(parent);
	        lua_pushlightuserdata(parent, m_pluaVM);
	        lua_pushnil(parent);
	        lua_settable(parent, LUA_REGISTRYINDEX);    
		lua_setgcthreshold(parent, 0);    
		lua_unlock(parent); 
	} 
	else 
	{
		if(m_pluaVM)
			lua_close(m_pluaVM);
	}
}

LuaWrap * LuaWrap::NewThread()
{
	lua_State *parent;
	
	assert(this);
	parent = m_pluaVM;
	lua_State *L;

	lock();
	L = lua_newthread(parent);

	if(L) {
    	    /* create a hard reference to the thread object into the registry */
    	    	lua_pushlightuserdata(parent, L);
    	    	lua_insert(parent, -2);
    	    	lua_settable(parent, LUA_REGISTRYINDEX);
	}
	unlock();


	LuaWrap *luaThread = NULL;
	
	if(L) {
		luaThread = new LuaWrap(NULL);
		luaThread->m_pluaVM       = L;
		luaThread->m_pParentLuaVM = parent;
	}
	return luaThread;
}

int  LuaWrap::get_field(const char *name)
{
	int idx = LUA_GLOBALSINDEX;
	lua_State * L = m_pluaVM;
	
    const char *end = strchr(name, '.');
    lua_pushvalue(L, idx);
    while (end) {
        lua_pushlstring(L, name, end - name);
        lua_gettable(L, -2);
        if (lua_isnil(L, -1)) return 0;
        name = end+1;
        end = strchr(name, '.');
    }
    lua_pushstring(L, name);
    lua_gettable(L, -2);
    lua_remove(L, -2);
    return 1;
}

int  LuaWrap::set_field(const char *name)
{
	int idx = LUA_GLOBALSINDEX;
	lua_State * L = m_pluaVM;

    const char *end = strchr(name, '.');
    lua_pushvalue(L, idx);
    while (end) {
        lua_pushlstring(L, name, end - name);
        lua_gettable(L, -2);
        /* create table if not found */
        if (lua_isnil(L, -1)) {
            lua_pop(L, 1);
            lua_newtable(L);
            lua_pushlstring(L, name, end - name);
            lua_pushvalue(L, -2);
            lua_settable(L, -4);
        }
        lua_remove(L, -2);
        name = end+1;
        end = strchr(name, '.');
    }
    lua_pushstring(L, name);
    lua_pushvalue(L, -3);
    lua_settable(L, -3);
    lua_pop(L, 2);
    return 1;
}


//
// End of "$Id: LuaWrap.cpp 12023 2007-10-22 01:10:31Z yuan_shiyong $"
//

