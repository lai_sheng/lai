
#include "System/ZipFile.h"
#include "AuxLibs/Zlib/zlib.h"

///////////////////////////////////////////////////////////////////////////////
///////   CZipFile implement
CZipFile::CZipFile()
{
}

CZipFile::~CZipFile()
{
}

bool CZipFile::Open(const char* pFileName, unsigned int dwFlags /* = modeReadWrite */)
{
	const char* mode = "";

	switch (dwFlags & 0xf) 
	{
	case modeRead:
		mode = "rb";
		break;
	case modeWrite:
		if (dwFlags & modeNoTruncate)
		{
			mode = "ab";
		}
		else
		{
			mode = "wb";
		}
		break;
	case modeReadWrite:
		if(dwFlags & modeCreate)
		{
			if (dwFlags & modeNoTruncate)
			{
				mode = "ab+";
			}
			else
			{
				mode = "wb+";
			}
		}
		else
		{
			mode = "rb+";
		}
	}

	m_fp = gzopen(pFileName, mode);

	if(!m_fp)
	{
		return false;
	}

	return true;

}

bool CZipFile::IsOpened()
{
	return m_fp != 0;
}

void CZipFile::Close()
{
	if(!m_fp)
	{
		return;
	}

	gzclose(m_fp);

	m_fp = NULL;
}

unsigned int CZipFile::Read(void *pBuffer, unsigned int dwCount)
{
	if(!m_fp)
	{
		return 0;
	}

	return gzread(m_fp, pBuffer, dwCount) == Z_OK ? dwCount : 0;
}

unsigned int CZipFile::Write(void *pBuffer, unsigned int dwCount)
{
	if(!m_fp)
	{
		return 0;
	}

	return gzwrite(m_fp, pBuffer, dwCount);
}

void CZipFile::Flush()
{
	if(!m_fp)
	{
		return;
	}

	gzflush(m_fp, Z_FINISH);
}

unsigned int CZipFile::Seek(long lOff, unsigned int nFrom)
{
	if(!m_fp)
	{
		return 0;
	}
	int origin = 0;
	switch(nFrom){
	case begin:
		origin = SEEK_SET;
		break;
	case current:
		origin = SEEK_CUR;
		break;
	case end:
		origin = SEEK_END;
		break;
	}

	if(gzseek(m_fp, lOff, origin) >= 0){
		return GetPosition();
	};
	return 0;
}

unsigned int CZipFile::GetPosition()
{
	if(!m_fp)
	{
		return 0;
	}

	int pos = gztell(m_fp);
	if(pos >= 0)
	{
		return pos;
	}
	return 0;
}

unsigned int CZipFile::GetLength()
{
	uint newpos = 0;
	uint oldpos = GetPosition();

	if(Seek(0, end))
	{
		newpos = GetPosition();
	};
	Seek(oldpos, begin);

	return newpos;
}

char * CZipFile::Gets(char *s, int size)
{
	if(!m_fp)
	{
		return 0;
	}

	return gzgets(m_fp, s, size);
}

int CZipFile::Puts(const char *s)
{
	if(!m_fp)
	{
		return 0;
	}

	return gzputs(m_fp, s);
}
