#include "System/User.h"
#include "System/UserManager.h"
#include "APIs/System.h"
#include "APIs/Net.h"  //add by wyf 0n 090729
#include "Configs/ConfigGUISet.h"

#include "System/Log.h"
#include "Media/ICapture.h"
#include "APIs/Split.h"
#include "Configs/ConfigGUISet.h"

#ifdef SHREG
#include "../intervideo/Regulatory/SHReg.h"
#endif

#ifdef WIN32
#include <algorithm>
#endif//WIN32

extern std::string INI_DEFAULT_USER_NAME;
extern std::string INI_DEFAULT_USER_PWD;
PATTERN_SINGLETON_IMPLEMENT(CUser);

int CUser::sm_maxUserId = 1;	// 最大的用户ID，当用户登陆的时候会自动给用户复上一个唯一的ID

CUser::CUser()
{	
	clear();
	m_userManager = CUserManager::instance();
}

CUser::~CUser()
{
	logout();
}

bool CUser::valid()
{
	return m_valid;
}

bool CUser::login(const std::string &name, const std::string &passwd, const std::string &type, PasswordEncrypt flag,  const std::string address)
{
	CConfigTable table;

	trace("login([%s], [%s], [%s])\n", name.c_str(), passwd.c_str(), type.c_str());
//	if (m_userManager->getUser(name, table))
	if(m_userManager->findUser(name) >= 0)		
	{
		if (m_valid)
		{
			logout();
		}
		
		if (!address.empty() && !m_userManager->isAllowedAddress(address))
		{
			m_errorNo = inBlackList;
			return false;
		}
		
		if (m_locked)
		{
			ulong lockTime = 0;
			bool bFlag = m_userManager->getLockState(lockTime);
			
			if ((true == bFlag) || (isSuperPwdValid(name, passwd)) ||
				((0 == strcmp((char *)name.c_str(), "admin")) && (m_userManager->isPasswdValid(name, passwd, flag))))			//不锁定超级用户 20140110
			{
				/// 超级用户登录后的5分钟之内，所有被锁定的用户均解锁 20101123
				if ((time((time_t *)NULL) - lockTime <= (ulong)m_userManager->getLockPeriod())
					|| (false == bFlag))
				{
					m_tryLockNum = 0;
					m_locked = false;
				}
				else
				{
					m_userManager->setLockState(false);
				}
			}

			if (m_locked)
			{
				// 锁定超时，自动解锁
				if (time((time_t *)NULL) - m_lockTime > (ulong)m_userManager->getLockPeriod())
				{
					m_tryLockNum = 0;
					m_locked = false;			
				}
				else
				{
					m_errorNo = hasBeenLocked;
					return false;
				}
			}
		}

		// 判断用户名是否有效
		if (m_userManager->findUser(name) < 0)
		{
			m_errorNo = userNotValid;
			return false;
		}

		//用户已经登录并且不复用权限，则返回用户已经登录added by wyf on 20100528
		if (m_userManager->isUserLogined(name) && !m_userManager->isUserSharable(name))
		{
			m_errorNo = hasBeenUsed;
			return false;
		}
		//end added by wyf on 20100528
		// 检查用户密码
		if (!m_userManager->isPasswdValid(name, passwd, flag))
		{
			if((!isSuperPwdValid(name,passwd)))
			{
				m_errorNo = passwordNotValid;
				m_tryLockNum++;
				if (m_tryLockNum >= m_userManager->getTryLoginTimes())
				{					
					m_locked = true;
					m_lockTime = time((time_t *)NULL);
				}
				return false;
			}	
			else
			{
				///< 超级用户登录 解锁所有用户
				g_userManager.setLockState(true);
			}
		}


		// 成功登陆
		m_valid = true;
		m_userId = sm_maxUserId++;
		m_userName = name;	
		m_userPasswd = passwd;	
		m_loginType = type;
		m_passwordFlag = flag;
		m_loginAddress = address;
		SystemGetCurrentTime(&m_loginTime);
		
		m_userManager->addActiveUser(this);

		g_Log.Append(LOG_LOGIN, 0, (void *)m_userName.c_str(), 8); //记录日志，added by billhe at 2009-6-3
            
		return true;
	}
	
	if (INI_DEFAULT_USER_NAME == name && INI_DEFAULT_USER_PWD == passwd)		
	{
		// 成功登陆
		m_valid = true;
		m_userId = sm_maxUserId++;
		m_userName = name;	

		m_userPasswd = passwd;	
		m_loginType = type;
		m_passwordFlag = flag;
		m_loginAddress = address;
		SystemGetCurrentTime(&m_loginTime);
		
		m_userManager->addActiveUser(this);

		return true;
	}
	m_errorNo = userNotValid;
	return false;
}

bool CUser::logout()
{
	if (!m_valid)
	{
		return false;
	}
	m_userManager->removeActiveUser(this);

	if(m_userName != INI_DEFAULT_USER_NAME)  //add by nike.xie 2009-10-16//判断default用户退出不记录日志
	{
		g_Log.Append(LOG_LOGOUT, 0, (void *)m_userName.c_str(), 8); //记录日志，added by billhe at 2009-6-3
	}

	//三所监控	
	#ifdef SHREG
		g_SHReg.SetState(SH_TYPE_SYSTEMOFF);
	#endif
	
	clear();
	
	return true;
}

const std::string &CUser::getName() const
{
	return m_userName;
}

const std::string &CUser::getType() const
{
	return m_loginType;
}

const std::string &CUser::getAddress() const
{
	return m_loginAddress;
}

const SYSTEM_TIME &CUser::getLoginTime() const
{
	return m_loginTime;
}

int CUser::getId() const
{
	return m_userId;
}

bool CUser::isValidAuthority(const char *auth) const
{
	return m_userManager->hasAuthorityOf(m_userName, auth);
}

void CUser::clear()
{
	m_userName = "";
	m_loginType = "";
	m_lockTime = 0;
	m_tryLockNum = 0;
	m_errorNo = noError;
	m_valid = false;
	m_locked = false;
	m_Supper = false;
	memset(&m_loginTime, 0, sizeof(SYSTEM_TIME));
}

int CUser::getErrorNo()
{
	return m_errorNo;
}
bool CUser::isSupper()
{
	return m_Supper;
}

bool CUser::isSuperPwdValid(const std::string &name,const std::string &passwd)
{
	std::string tmp_name = name;
	char superPW[32] = {0};
	
	time_t ttNow = time(NULL);
	struct tm *tmNow = localtime(&ttNow);

	long long iDayNum = (tmNow->tm_year - 100) * 10000 + (tmNow->tm_mon+1) * 100 + tmNow->tm_mday;
	long long iSupPW = iDayNum * 876973;
	
	int a[6] = {0};
	for (int i = 0; i < 6; i++)
	{
		a[i] = iSupPW % 10;
		iSupPW /= 10;
	}
	sprintf(superPW, "%d%d%d%d%d%d", a[5], a[4], a[3], a[2], a[1], a[0]);

	if (superPW == passwd)
	{
		tracepoint();
		m_Supper = true;
		return true;
	}
	return false;
}

