
#include <stdio.h>
#if 0
extern "C"
{
	#include "AuxLibs/Lua/lua.h"
	#include "AuxLibs/Lua/lauxlib.h"
	#include "AuxLibs/Lua/lualib.h"
};


#include "AuxLibs/Lua/ScriptEngine.h"
#endif
#include "System/AppConfig.h"

AppConfig * AppConfig::_instance = NULL;

AppConfig * AppConfig::instance()
{
	if( NULL == _instance) 
	{
		_instance = new AppConfig();
	}
	
	return _instance;
}

AppConfig::AppConfig()
{
#if 0
	ScriptEngine *engine = ScriptEngine::Instance();
	_lua_state = (lua_State *) (*engine);
#endif
}

AppConfig::~AppConfig()
{
}

std::string AppConfig::getString(std::string key, std::string defaultValue)
{
#if 0
	ScriptEngine::Guard guard(ScriptEngine::Instance());

	int nTop   = lua_gettop(_lua_state);
	int status = TravelTable(key);
	std::string returnValue = defaultValue;
	
	if( (status == 0) && (lua_isstring(_lua_state, -1))) 
	{
		returnValue = lua_tostring(_lua_state, -1);
	}
	
	lua_settop(_lua_state, nTop);
	return returnValue;	
#else
	return defaultValue;	
#endif
}

double AppConfig::getNumber(std::string key, double defaultValue)
{
#if 0
	ScriptEngine::Guard guard(ScriptEngine::Instance());
	int nTop   = lua_gettop(_lua_state);
	int status = TravelTable(key);
	double returnValue = defaultValue;
	
	if( (status == 0) && (lua_isnumber(_lua_state, -1)))
	{
		returnValue = lua_tonumber(_lua_state, -1);
	}

	lua_settop(_lua_state, nTop);
	return returnValue;	
#else
	return defaultValue;
#endif

}

int AppConfig::loadFromFile(std::string filename)
{
#if 0
	ScriptEngine::Guard guard(ScriptEngine::Instance());

	int status = luaL_loadfile(_lua_state, filename.c_str());
	
	if(status == 0) 
	{
		status = lua_pcall(_lua_state, 0, LUA_MULTRET, 0);  /* call main */		
	}
	return status;
#endif
	return 0;
}

int AppConfig::TravelTable(std::string& key)
{
#if 0

	int status;
	std::string func("return ");  // Create anonymous function
	
	func += key;
	
	status = luaL_loadbuffer(_lua_state, func.c_str(), func.length(), "table_travel");
	if(status == 0) 
	{
		status = lua_pcall(_lua_state, 0, LUA_MULTRET, 0);  /* call main */		
	}	
	return status;
#endif
	return 0;
}

