
#include "System/Support.h"
#include "System/CMOS.h"
#include "Configs/ConfigLocation.h"
#include "System/Locales.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

////////////////////////////////////////////////////////////////////////////////////
/////////时间转换函数
void timedh2sys(SYSTEM_TIME * systime, DHTIME * dhtime)
{
	systime->second = dhtime->second; 
	systime->minute = dhtime->minute; 
  	systime->hour = dhtime->hour; 
  	systime->day = dhtime->day; 
  	systime->month = dhtime->month; 
  	systime->year = dhtime->year + 2000; 
	systime->wday = 0; 
	systime->isdst = 0;
}

void timesys2dh(DHTIME * dhtime, SYSTEM_TIME * systime)
{
	dhtime->second = systime->second; 
	dhtime->minute = systime->minute; 
  	dhtime->hour = systime->hour; 
  	dhtime->day = systime->day; 
  	dhtime->month = systime->month; 
  	dhtime->year = systime->year - 2000;
}

static int monthday[12] = 
{
	0, 
	31, 
	31+28, 
	31+28+31, 
	31+28+31+30, 
	31+28+31+30+31, 
	31+28+31+30+31+30, 
	31+28+31+30+31+30+31, 
	31+28+31+30+31+30+31+31, 
	31+28+31+30+31+30+31+31+30, 
	31+28+31+30+31+30+31+31+30+31, 
	31+28+31+30+31+30+31+31+30+31+30, 
};
//是否闰年
uint is_leap_year(int year)
{
	return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
}

void clear_time(DHTIME *ptime)
{
	memset(ptime, 0, sizeof(DHTIME));
}

//得到总天数
int time_to_day(DHTIME t)
{
	int day;

	//日期检查
	if(t.month == 0)
	{
		t.month = 1;
	}
	else if(t.month > 12)
	{
		t.month = 12;
	}

	if(t.day == 0)
	{
		t.day = 1;
	}

	//每年365天 + 润年增加的天数 + 当年天数
	day = t.year * 365 + (t.year / 4 + 1) + monthday[t.month - 1] + (t.day - 1);

	if(is_leap_year(t.year) && t.month < 3) //还没到闰月
	{
		day--;
	}
	
	return day;
}

int time_to_second(DHTIME t)
{
	//先得到小时值
	int second = time_to_day(t) * 24;

	second += t.hour;
	second *= 60;

	second += t.minute;
	second *= 60;

	second += t.second;

	return second;
}
//把相应的秒数转换成时间日期
DHTIME second_to_time(int second)
{
	DHTIME time;
	int day = second / ( 24 * 3600);	//得到总天数
	int daysec = second % ( 24 * 3600);	//得到当前秒数

	clear_time(&time);

	while(day > 365)
	{
		if(is_leap_year(time.year))
		{
			day--;
		}
		time.year++;
		day -= 365;
	}

	if(day == 365)
	{
		if(is_leap_year(time.year))
		{
			time.day = 31;
			time.month = 12;
		}
		else
		{
			time.year++;
			time.day = 1;
			time.month = 1;
		}
	}
	else
	{
		for(time.month = 1; time.month <= 11; time.month++)
		{
			if(time.month == 3)
			{
				if(is_leap_year(time.year))
				{
					if(day == monthday[2])
					{
						time.month = 2;
						break;
					}
					day--;
				}
			}
			if(day < monthday[time.month])
			{
				break;
			}
		}
		
		time.day = day - monthday[time.month - 1] + 1;
	}

	time.hour = daysec / 3600;
	daysec %= 3600;
	time.minute = daysec / 60;
	daysec %= 60;
	time.second = daysec;

	return time;
}
DHTIME add_time(DHTIME time, int second)
{
	int sec = time_to_second(time) + second;

	return second_to_time(sec);
}



int compare_time(DHTIME t1,DHTIME t2)
{
	if(time_to_second(t1) == time_to_second(t2)) //(UInt32*)(UInt8*) to avoid gcc warnings
	{
		return 0;
	}

	return time_to_second(t1) > time_to_second(t2) ? 1 : -1;
}

int compare_in_time(DHTIME t,DHTIME t1,DHTIME t2)
{
	ASSERT(compare_time(t1, t2) <= 0);
	if(compare_time(t, t1) < 0)
	{
		return -1;
	}
	if(compare_time(t, t2) > 0)
	{
		return 1;
	}
	return 0;
}

int changeto_second(DHTIME end_time, DHTIME start_time)
{
	uint sec1, sec2;

	sec1 = time_to_second(end_time);
	sec2 = time_to_second(start_time);

	return sec1 - sec2;
}



int timecompare(SYSTEM_TIME * pSysTime1, SYSTEM_TIME * pSysTime2)
{
	DHTIME t1, t2;

	timesys2dh(&t1, pSysTime1);
	timesys2dh(&t2, pSysTime2);

	return compare_time(t1, t2);
}



void timeadd(SYSTEM_TIME * pOldTime, SYSTEM_TIME * pNewTime, int second)
{
	DHTIME t;

	timesys2dh(&t, pOldTime);
	t = add_time(t, second);
	timedh2sys(pNewTime, &t);
}

int time2second(SYSTEM_TIME * pSysTime1, SYSTEM_TIME * pSysTime2)
{
	DHTIME t1, t2;

	timesys2dh(&t1, pSysTime1);
	timesys2dh(&t2, pSysTime2);

	return changeto_second(t1, t2);
}

int log2i(uint x)
{
	int r = 31;

	if (!x)
		return 0;
	if (!(x & 0xffff0000u)) {
		x <<= 16;
		r -= 16;
	}
	if (!(x & 0xff000000u)) {
		x <<= 8;
		r -= 8;
	}
	if (!(x & 0xf0000000u)) {
		x <<= 4;
		r -= 4;
	}
	if (!(x & 0xc0000000u)) {
		x <<= 2;
		r -= 2;
	}
	if (!(x & 0x80000000u)) {
		x <<= 1;
		r -= 1;
	}
	return r;
}
////////////////////////////////////////////////////////////////////////////////////
/////////文件系统用到的外部函数
int SYS_getrtctime(DHTIME* ptime)
{
	if(!ptime){
		return -1;
	}

	g_Cmos.GetExitTime(ptime);
	return 0;
}

int SYS_getsystime(DHTIME* ptime)
{
	int ret;
	SYSTEM_TIME systime;
	if(!ptime){
		return -1;
	}
	ret = SystemGetCurrentTime(&systime);
	timesys2dh(ptime,&systime);
	return ret;
}

int SYS_getsysweek(SYSTEM_TIME * systime, char *buf, int flag)
{
	if (systime == NULL || buf == NULL)
	{
		return -1;
	}
	int a = (14 - systime->month) / 12;
	int y = systime->year - a;
	int m = systime->month + 12 * a - 2;
	int w = (systime->day + y + y / 4 - y / 100 + y / 400 + ( 31 * m ) / 12) % 7;
	int i = -1;

	if (0 == flag)
	{
		VD_PCSTR temp = LOADSTR("conf_tim.weekday");
		VD_PCSTR string = temp;
		while(*temp)
		{
			if(*temp++ == '|')
			{
				i++;
				if (i == w)
				{
					strncpy(buf, string, temp - string - 1);
					return 0;
				}
				else
				{
					string = temp;
				}
			}
		}
		return -1;
	}
	else
	{
		return w;
	}
}

uint SYS_10msCnt()
{
	return SystemGetMSCount()/10;
}

void SYS_sleep(int ms)
{
	SystemSleep(ms);
}

////////////////////////////////////////////////////////////////////////////////////
/////////时间格式化函数
void FormatTimeString(SYSTEM_TIME *p,char* buf,int flag)
{
	//_printd("FormatTimeString");
	char spector;
	if(!p || !buf)
	{
		return;
	}
	switch(CConfigLocation::getLatest().iDateSeparator) {
	case DS_DOT:
		spector = '.';
		break;
	case DS_DASH:
		spector = '-';
		break;
	case DS_SLASH:
		spector = '/';
		break;
	default:
		spector = '-';
		break;
	}
	int hour = p->hour;
	if(CConfigLocation::getLatest().iTimeFormat==TF_12){
		if(hour > 12){
			hour -= 12;
		}else if(hour == 0){
			hour = 12;
		}
	}
	if(flag&FT_ONLY_TIME)
	{
		sprintf(buf,"%02d:%02d:%02d",hour,p->minute,p->second);
	}
	else switch(CConfigLocation::getLatest().iDateFormat){
	case DF_YYMMDD:
		if(flag&FT_HALF_YEAR){
			sprintf(buf,"%02d%c%02d%c%02d %02d:%02d:%02d",p->year-2000,spector,p->month,spector,p->day,hour,p->minute,p->second);
		}else if(flag&FT_NO_SIGNS){
			sprintf(buf,"%04d%02d%02d%02d%02d%02d",p->year,p->month,p->day,hour,p->minute,p->second);
		}else if(flag&FT_ONLY_DATE){
			sprintf(buf,"%04d%c%02d%c%02d",p->year,spector,p->month,spector,p->day);
		}else{
			sprintf(buf,"%04d%c%02d%c%02d %02d:%02d:%02d",p->year,spector,p->month,spector,p->day,hour,p->minute,p->second);
		}
		break;
	case DF_MMDDYY:
		if(flag&FT_HALF_YEAR){
			sprintf(buf,"%02d%c%02d%c%02d %02d:%02d:%02d",p->month,spector,p->day,spector,p->year-2000,hour,p->minute,p->second);
		}else if(flag&FT_NO_SIGNS){
			sprintf(buf,"%02d%02d%04d%02d%02d%02d",p->month,p->day,p->year,hour,p->minute,p->second);
		}else if(flag&FT_ONLY_DATE){
			sprintf(buf,"%02d%c%02d%c%04d",p->month,spector,p->day,spector,p->year);
		}else{
			sprintf(buf,"%02d%c%02d%c%04d %02d:%02d:%02d",p->month,spector,p->day,spector,p->year,hour,p->minute,p->second);
		}
		break;
	case DF_DDMMYY:
		if(flag&FT_HALF_YEAR){
			sprintf(buf,"%02d%c%02d%c%02d %02d:%02d:%02d",p->day,spector,p->month,spector,p->year-2000,hour,p->minute,p->second);
		}else if(flag&FT_NO_SIGNS){
			sprintf(buf,"%02d%02d%04d%02d%02d%02d",p->day,p->month,p->year,hour,p->minute,p->second);
		}else if(flag&FT_ONLY_DATE){
			sprintf(buf,"%02d%c%02d%c%04d",p->day,spector,p->month,spector,p->year);
		}else{
			sprintf(buf,"%02d%c%02d%c%04d %02d:%02d:%02d",p->day,spector,p->month,spector,p->year,hour,p->minute,p->second);
		}
		break;
	default:
		break;
	}
	if(flag&FT_NO_SECOND){
		buf[strlen(buf)-3] = '\0';
	}
	if(CConfigLocation::getLatest().iTimeFormat==TF_12 && !(flag&FT_ONLY_DATE))
	{
		strcat(buf,p->hour/12?"PM":"AM");
	}
}

void FormatTimeStringDH(DHTIME *p,char* buf,int flag)
{
	SYSTEM_TIME systime;
	if(!p || !buf)
	{
		return;
	}

	timedh2sys(&systime, p);
	FormatTimeString(&systime, buf, flag);
}

void DumpHex(uchar *pDat)
{
	int i, j;

	for (i = 0; i < 512; i += 16) {
		trace("%08x: ", (uint)pDat);
		for (j = 0; j < 16; j++) {
			trace("%02x ", *pDat++);
		}
		pDat -= 16;
		trace("  ");
		for (j = 0; j < 16; j++) {
			trace("%c", *pDat > 31 && *pDat <= 'z' ? *pDat:'.');
			pDat++;
		}
		trace("\n");
	}
}

#define TIMER_POINTS_CNT 20
typedef struct {
	const char *file;
	int line;
	uint cnt;
} TIMER_POINT;

static TIMER_POINT timer_points[TIMER_POINTS_CNT];
static int timer_index = 0;
void TestTimerStart(const char* file, int line)
{
	timer_index = 0;
	timer_points[timer_index].file = file;
	timer_points[timer_index].line = line;
	timer_points[timer_index].cnt = SystemGetUSCount();
}

void TestTimerSample(const char* file, int line)
{
	timer_index++;
	if(timer_index >= TIMER_POINTS_CNT - 1)
	{
		return;
	}
	timer_points[timer_index].file = file;
	timer_points[timer_index].line = line;
	timer_points[timer_index].cnt = SystemGetUSCount();
}

void TestTimerStop(const char* file, int line)
{
	timer_index++;
	timer_points[timer_index].file = file;
	timer_points[timer_index].line = line;
	timer_points[timer_index].cnt = SystemGetUSCount();
	for(int i = 1; i <= timer_index; i++)
	{
		trace("%4d us to %s %d\n", timer_points[i].cnt - timer_points[i-1].cnt, timer_points[i].file, timer_points[i].line);
	}
	trace("\n");
}

/*!
	\b Description		:	把容量转化成字符串表示\n
	\b Argument			:	char *pstr, int capability
	\param	pstr		:	存放字符串指针
	\param	capa 	:	容量，单位由unit指定
	\param 	unit	:	单位，0表示K,1表示M，2表示G,3表示T
	\return	０：成功；1：存放字符串的指针为空			

	\b Revisions		:	
		- 2007-06-27 		liweijun	modify
*/
void Capa2Str(char *pstr, uint64 capa, int unitindex)
{
	int	i, point;
	char	unit[] = "KMGT";

	for (i = unitindex, point = 0; capa / 1024 && i < 3; i++)
	{
		point = static_cast<int>(capa % 1024);	// 不会溢出
		capa /= 1024;
	}
	sprintf(pstr, "%d.%02d %cB", static_cast<int>(capa), point * 100 / 1024, unit[i]);
}
#if 0
//////////////////////////////////////////////////////////////////
void clear_time(DHTIME *ptime)
{
	memset(ptime, 0, sizeof(DHTIME));
}

uint iszero_time(DHTIME t)
{
	return (*(uint*)((uchar*)&t) == 0);
}

static int monthday[12] = 
{
	0, 
		31, //1 
		31+28, //2 
		31+28+31, //3 
		31+28+31+30, //4 
		31+28+31+30+31, //5 
		31+28+31+30+31+30, //6 
		31+28+31+30+31+30+31, //7 
		31+28+31+30+31+30+31+31, //8 
		31+28+31+30+31+30+31+31+30, //9 
		31+28+31+30+31+30+31+31+30+31, //10
		31+28+31+30+31+30+31+31+30+31+30, //11
};

//是否闰年
uint is_leap_year(int year)
{
	return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
}

//得到总天数
int time_to_day(DHTIME t)
{
	int day;

	//日期检查
	if(t.month == 0)
	{
		t.month = 1;
	}
	else if(t.month > 12)
	{
		t.month = 12;
	}

	if(t.day == 0)
	{
		t.day = 1;
	}

	//每年365天 + 润年增加的天数 + 当年天数
	day = t.year * 365 + (t.year / 4 + 1) + monthday[t.month - 1] + (t.day - 1);

	if(is_leap_year(t.year) && t.month < 3) //还没到闰月
	{
		day--;
	}
	
	return day;
}

//得到总秒数
int time_to_second(DHTIME t)
{
	//先得到小时值
	int second = time_to_day(t) * 24;

	second += t.hour;
	second *= 60;

	second += t.minute;
	second *= 60;

	second += t.second;

	return second;
}

//把相应的秒数转换成时间日期
DHTIME second_to_time(int second)
{
	DHTIME time;
	int day = second / ( 24 * 3600);	//得到总天数
	int daysec = second % ( 24 * 3600);	//得到当前秒数

	clear_time(&time);

	while(day > 365)
	{
		if(is_leap_year(time.year))
		{
			day--;
		}
		time.year++;
		day -= 365;
	}

	if(day == 365)
	{
		if(is_leap_year(time.year))
		{
			time.day = 31;
			time.month = 12;
		}
		else
		{
			time.year++;
			time.day = 1;
			time.month = 1;
		}
	}
	else
	{
		for(time.month = 1; time.month <= 11; time.month++)
		{
			if(time.month == 3)
			{
				if(is_leap_year(time.year))
				{
					if(day == monthday[2])
					{
						time.month = 2;
						break;
					}
					day--;
				}
			}
			if(day < monthday[time.month])
			{
				break;
			}
		}
		
		time.day = day - monthday[time.month - 1] + 1;
	}

	time.hour = daysec / 3600;
	daysec %= 3600;
	time.minute = daysec / 60;
	daysec %= 60;
	time.second = daysec;

	return time;
}

int changeto_second(DHTIME end_time, DHTIME start_time)
{
	uint sec1, sec2;

	sec1 = time_to_second(end_time);
	sec2 = time_to_second(start_time);

	return sec1 - sec2;

}

//比较pt与pt1至pt2时间段的关系
//返回：0-->pt在pt1与pt2之间 
//      1-->pt>pt2
//     -1-->pt<pt1
int compare_in_time(DHTIME t,DHTIME t1,DHTIME t2)
{
	if(compare_time(t, t1) < 0)
	{
		return -1;
	}
	if(compare_time(t, t2) > 0)
	{
		return 1;
	}
	return 0;
}

void display_time(DHTIME t)
{
	trace("%4d-%2d-%2d %2d:%2d:%2d\n",t.year ,t.month ,t.day ,\
		t.hour ,t.minute ,t.second );
}
#endif

// 记录上次输出最后一个字符是否是换行，还不完全可靠
static bool returned = true;
static char printbuffer[8192] = {0, };

#ifdef WIN32
#define LOG_MSG(x, y) \
	if(returned) \
	{ \
		SYSTEM_TIME t; \
		SystemGetCurrentTime(&t); \
		printf("%02d:%02d:%02d|", t.hour, t.minute, t.second); \
		printf(x); \
	} \
	size_t n; \
	va_list ap; \
	va_start(ap, fmt); \
	assert(strlen(fmt) < 8192); \
	n = vsprintf(printbuffer, fmt, ap); \
	fputs(printbuffer, stdout); \
	returned = (n && printbuffer[n - 1] == '\n'); \
	return(n);
#else //#ifdef WIN32
#define LOG_MSG(x, y) \
	memset(printbuffer, 0, sizeof(printbuffer));	\
	if(returned) \
	{ \
		printf("\033[%d;40m", y);\
		SYSTEM_TIME t; \
		SystemGetCurrentTime(&t); \
		printf("%02d:%02d:%02d|", t.hour, t.minute, t.second); \
		printf(x); \
	} \
	size_t n; \
	va_list ap; \
	va_start(ap, fmt); \
	n = vsnprintf(printbuffer, 8192, fmt, ap); \
	fputs(printbuffer, stdout); \
	returned = (n && printbuffer[n - 1] == '\n'); \
	if(returned)printf("\033[0m");\
	return(n);
#endif //#ifdef WIN32

int logTrace(const char* fmt, ...)
{
	LOG_MSG("trace ", 37);
}

int logDebug(const char* fmt, ...)
{
	LOG_MSG("debug ", 36);
}

int logInfo(const char* fmt, ...)
{
	LOG_MSG("info  ", 32);
}

int logWarn(const char* fmt, ...)
{
	LOG_MSG("warn  ", 33);
}

int logError(const char* fmt, ...)
{
	LOG_MSG("error ", 31);
}

int logFatal(const char* fmt, ...)
{
	LOG_MSG("fatal ", 35);
}

