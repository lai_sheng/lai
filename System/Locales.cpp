
#include "System/Locales.h"
#include "System/AppConfig.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigLocation.h"
#include "math.h"
#include "APIs/System.h"
//modified log by wangqin
//20070413 增加对葡萄牙文的支持 StringsPt.txt
//20070515 增加对土耳其文的支持 StringsTr.txt
static char* strings_file[LANG_NR] = {
	"StringsEn.txt", "StringsCn.txt", "StringsCnT.txt", "StringsIt.txt",
		"StringsSp.txt", "StringsJp.txt", "StringsRu.txt", "StringsFr.txt",
		"StringsDe.txt","StringsPt.txt","StringsTr.txt",//modified by wangin 2007-05-15
		"StringsPl.txt","StringsRo.txt","StringsHung.txt","StringsFi.txt",
		"StringsEs.txt","StringsKor.txt","StringsPersian.txt","StringsDa.txt",
		"StringsBu.txt","StringsArabic.txt","StringsHI.txt",
};

PATTERN_SINGLETON_IMPLEMENT(CLocales);

CLocales::CLocales()
{
	trace("CLocales::CLocales()>>>>>>>>>\n");

	m_iLanguage = -1;
	m_bFontValid = FALSE;

#if 0//def ENC_SHOW_SMALL_FONT
  	m_bFontValidSmall = FALSE;           //zgzhit
    /*读取小字库头信息, 和小型ASCII点阵: zgzhit*/
	if (m_FileFontSmall.Open(FONT_BIN"/FontSmallEn.bin", CFile::modeRead)
		&& m_FileFontSmall.Read(&m_UFHSmall, sizeof(UCS_FONT_HEADER)) == sizeof(UCS_FONT_HEADER)
		&& m_UFHSmall.size == m_FileFontSmall.GetLength()
       && m_UFHSmall.blocks
	   && strcmp(m_UFHSmall.magic, UCS_FONT_MAGIC) == 0)
	{
		m_bFontValidSmall = TRUE;
		m_pUFBSmall = new UCS_FONT_BLOCK[m_UFHSmall.blocks];
		m_FileFontSmall.Read(m_pUFBSmall, sizeof(UCS_FONT_BLOCK) * m_UFHSmall.blocks);
		m_sizeFontSmall.w = m_pUFBSmall[0].width;	//暂时只取第一个区点阵的宽度和高度
		m_sizeFontSmall.h = m_pUFBSmall[0].height;
		m_nFontBytesSmall = m_sizeFontSmall.w * m_sizeFontSmall.h / 8;
		m_pASCIIFontSmall = new uchar[m_nFontBytesSmall * 128 + 128];
		memset(m_pASCIIFontSmall, 0, m_nFontBytesSmall * 128 + 128);

		/*所有小型ASCII字体拷到缓冲: zgzhit*/
		for(uint i = 0; i < m_UFHSmall.blocks; i++)
		{
			if(m_pUFBSmall[i].end <= 0x80)
			{
				m_FileFontSmall.Seek(m_pUFBSmall[i].roffs, CFile::begin);
				m_FileFontSmall.Read(&m_pASCIIFontSmall[m_pUFBSmall[i].start * m_nFontBytesSmall], (m_pUFBSmall[i].end - m_pUFBSmall[i].start) * m_nFontBytesSmall);
				m_FileFontSmall.Seek(m_pUFBSmall[i].xoffs, CFile::begin);
				m_FileFontSmall.Read(&m_pASCIIFontSmall[ 128 * m_nFontBytesSmall + m_pUFBSmall[i].start], m_pUFBSmall[i].end - m_pUFBSmall[i].start);
			}
		}
	}
	else
	{
		trace("CLocales::CLocales Open FontSmallEn.bin File Failed!\n");
	}
#endif
	//读取字库头信息, 和ASCII点阵, 先不读取其他字符点阵

	std::string font_file;
	int langType;
	langType=SystemGetUILanguage(); 
#ifdef USE_RAR_FONT
	if ( langType == 20
		|| langType== 17 )
	{
		font_file = FONT_BIN"/FontArbic.bin";
	}
	else
	{
		font_file = "/var/Font.bin";	
	}
#else
	if (langType== 20 
		||langType== 17 )
	{
		font_file = FONT_BIN"/FontArbic.bin";
	}
	else
	{
		font_file = FONT_BIN"/Font.bin";
	}
	
#endif
	   
	if(	m_FileFont.Open(font_file.c_str(), CFile::modeRead)
		&& m_FileFont.Read(&m_UFH, sizeof(UCS_FONT_HEADER)) == sizeof(UCS_FONT_HEADER)
		&& m_UFH.size == m_FileFont.GetLength()
		&& m_UFH.blocks
		&& strcmp(m_UFH.magic, UCS_FONT_MAGIC) == 0 )
	{
		m_bFontValid = TRUE;
		m_pUFB = new UCS_FONT_BLOCK[m_UFH.blocks];
		m_FileFont.Read(m_pUFB, sizeof(UCS_FONT_BLOCK) * m_UFH.blocks);
		m_sizeFont.w = m_pUFB[0].width;	//暂时只取第一个区点阵的宽度和高度
		m_sizeFont.h = m_pUFB[0].height;
		m_nFontBytes = m_sizeFont.w * m_sizeFont.h / 8;
		m_pASCIIFont = new uchar[m_nFontBytes * 128 + 128];
		memset(m_pASCIIFont, 0, m_nFontBytes * 128 + 128);

		//所有ASCII字体拷到缓冲
		for(uint i = 0; i < m_UFH.blocks; i++)
		{
			if(m_pUFB[i].end <= 0x80)
			{
				m_FileFont.Seek(m_pUFB[i].roffs, CFile::begin);
				m_FileFont.Read(&m_pASCIIFont[m_pUFB[i].start * m_nFontBytes], (m_pUFB[i].end - m_pUFB[i].start) * m_nFontBytes);
				m_FileFont.Seek(m_pUFB[i].xoffs, CFile::begin);
				m_FileFont.Read(&m_pASCIIFont[ 128 * m_nFontBytes + m_pUFB[i].start], m_pUFB[i].end - m_pUFB[i].start);
			}
		}
	}
	else
	{
		trace("CLocales::CLocales Open Font File Failed!\n");

		trace("##1.m_FileFont.Open:%d\n",m_FileFont.Open(FONT_BIN"/font.bin", CFile::modeRead));//add for test by james.xu  on 090209
		trace("##2.m_FileFont.Read:%d,size:%d\n",m_FileFont.Read(&m_UFH, sizeof(UCS_FONT_HEADER)),sizeof(UCS_FONT_HEADER));//add for test by james.xu  on 090209
	//	trace("##3.m_UFH.size:%d,m_FileFont.GetLength():%d \n",m_UFH.size,m_FileFont.GetLength());//add for test by james.xu  on 090209
	//	trace("##4.m_UFH.blocks:%d \n",m_UFH.blocks);//add for test by james.xu  on 090209
	//	trace("##5.m_UFH.magic:%s, UCS_FONT_MAGIC:%s \n",m_UFH.magic, UCS_FONT_MAGIC);//add for test by james.xu  on 090209
	}

	m_iLanguage = -1;
	m_nCommonChars = 0;
	m_pCommonChars = NULL;
	m_pCommonFont = NULL;

	tracef("CConfigLocation::getLatest().iLanguage = %d.\n", langType);
	//设置语言, 一定要保证区域配置CConfigLocation已经加载
	SetLanguage(langType);
}

CLocales::~CLocales()
{
}

void CLocales::SetLanguage(int index)
{
	char path[VD_MAX_PATH];
	int i, n;
	uint length;
	std::list<ushort> common_list;
	std::list<ushort>::iterator pi;
	ushort code = 0; //unicode
	char *buf;
	char *p;
	char *key;
	char *value;

	char *alias_key = NULL;

	if(!m_bFontValid || index == m_iLanguage)
	{
		return;
	}

	if(m_iLanguage >= 0) //if language already be set, release first.
	{
		m_FileStrings.UnLoad();
		delete []m_pCommonChars;
		delete []m_pCommonFont;
		m_mapStrings.clear();
	}

	m_iLanguage = index;

	// 先加载指定目录，再加载特殊目录。为了升级语言使用
	sprintf(path, "%s/%s", FONT_DIR, strings_file[index]);
	if(!(buf = (char *)m_FileStrings.Load(path)))
	{
		trace("CLocales::SetLanguage Load Strings File Failed! file path %s, and Load from Default Directory\n",path);

		sprintf(path, "%s/%s", FONT_DEFAULT_DIR, strings_file[index]);
		if(!(buf = (char *)m_FileStrings.Load(path)))
		{
			trace("CLocales::SetLanguage Load Strings File Failed! file path %s\n",path);
			return;
		}
	}

	//检查文件格式是否UTF-8
	if(buf[0] != (char)0xEF || buf[1] != (char)0xBB || buf[2] != (char)0xBF)
	{
		trace("CLocales::SetLanguage Strings File Format Error!\n");
		m_FileStrings.UnLoad();
		return;
	}

	//扫描字符串文件, 提取其中的常用字符
	length = m_FileStrings.GetLength();
	p = buf;
	while((code = GetCharCode(p, &n)))
	{
		p += n;
		if(code >= 0x80)//非ASCII码
		{
			common_list.push_back(code);
		}
		if(p >= buf + length)
		{
			break;
		}
	}
	if(!code) //解析出错
	{
		trace("CLocales::SetLanguage Parse Strings File Failed!\n");
	}

	//排序,去掉重复的字符.
	common_list.sort();
	common_list.unique();
	if((n = common_list.size()))
	{
		m_nCommonChars = n;
		m_pCommonChars = new ushort[n];
		m_pCommonFont = new uchar[n * m_nFontBytes + n];
		for(i = 0, pi = common_list.begin(); pi != common_list.end(); pi++, i++)
		{
			m_pCommonChars[i] = *pi;
			m_pCommonFont[n * m_nFontBytes + i] = GetOneFontFromFile(m_pCommonChars[i], &m_pCommonFont[i * m_nFontBytes]);
		}
	}

	//提取字符串, 并按键值排列
	p = buf;
	key = p;
	value = NULL;
	while(*p)
	{
		if(*p == '=')
		{
			if(!value)
			{
				*p = '\0';
				value = p + 1;
			}
		}
		else if(*p == '\n')
		{
			if(value)
			{
				*p = '\0';
				if(*(p-1) == '\r') //windows format
				{
					*(p-1) = '\0';
				}
				if(alias_key && !strncmp(key, "titles", 6))//处理各ODM版本标题不同的问题
				{
					if(strncmp(key, alias_key, 9))
					{
						goto jump_over;
					}
					i = 6;
					while(1)
					{
						key[i] = key[i+3];
						if(!key[i])
						{
							break;
						}
						i++;
					}
				}
				m_mapStrings.insert(MAPSTRING::value_type(key,value));
jump_over:
				value = NULL;
			}
			key = p + 1;
		}
		p++;
		if(p >= buf + length)
		{
			break;
		}
	}
	return;
}
//装载一个字符的字体, 传入参数为UCS-2编码, 返回值为字体宽度
uchar CLocales::GetOneFontFromFile(ushort code, uchar *p)
{
	int start = 0;
	int end = m_UFH.blocks;
	int mid;
	uchar wx;

	while(1)
	{
		mid = (start + end)/2;
		if(mid == start)break;
		if(m_pUFB[mid].start <= code)
		{
			start = mid;
		}
		else
		{
			end = mid;
		}
	}
	if(code >= m_pUFB[mid].end) //字库中也没有, 显示未知字符.
	{
		//trace("Unknown code = %04x\n", code);
		if(p)
		{
			memset(p, 0xff, m_nFontBytes);
		}
		return m_sizeFont.w / 2;
	}

	if(p)
	{
		m_FileFont.Seek(m_pUFB[mid].roffs + (code - m_pUFB[mid].start) * m_nFontBytes, CFile::begin);
		m_FileFont.Read(p, m_nFontBytes);
	}
	m_FileFont.Seek(m_pUFB[mid].xoffs + (code - m_pUFB[mid].start), CFile::begin);
	m_FileFont.Read(&wx, 1);
	return wx;
}
ushort CLocales::GetCharCode(VD_PCSTR pch, int *pn)
{
	uchar ch; //sigle char
	ushort code = 0; //unicode
	int flag = 0; //0 - empty, 1 - 1 char to finish unicode, 2 - 2 chars to finish unicode, -1 - error

	*pn = 0;
	while((ch = (uchar)*pch))
	{
		pch++;
		if(ch & 0x80)
		{
			if((ch & 0xc0) == 0xc0)
			{
				if((ch & 0xe0) == 0xe0)
				{
					if((ch & 0xf0) == 0xf0) //ucs-4?
					{
						break;
					}
					if(flag)
					{
						break;
					}
					*pn = 3;
					flag = 2;
					code |= ((ch & 0x0f) << 12);
				}
				else
				{
					if(flag)
					{
						break;
					}
					*pn = 2;
					flag = 1;
					code |= ((ch & 0x1f) << 6);
				}
			}
			else
			{
				if(flag == 0)
				{
					break;
				}
				else if(flag == 1) //unicode finished
				{
					code |= (ch & 0x3f);
					break;
				}
				else
				{
					code |= ((ch & 0x3f) << 6);
				}
				flag--;
			}
		}
		else //ASCII
		{
			if(flag)
			{
				break;
			}
			*pn = 1;
			code = ch;
			break;
		}
	}
	if(ch == 0)
	{
		code = 0;
	}

	return code;
}

VD_BOOL CLocales::GetFontSize(VD_SIZE *pSize, FONTSIZE fontsize)
{
	if(!pSize)
	{
		return FALSE;
	}

	if (fontsize == FONTSIZE_NORMAL)
	{
		*pSize = m_sizeFont;
	}
	else if (fontsize == FONTSIZE_SMALL)
	{
		*pSize = m_sizeFontSmall;
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

//二分发查找常用汉字
int CLocales::FindCommonChar(ushort code)
{
	if(!m_pCommonChars)
	{
		return -1;
	}

	int high = m_nCommonChars - 1, low = 0, mid;

	while(low <= high)
	{
		mid = (low + high) / 2;
		if(m_pCommonChars[mid] < code)
		{
			low = mid + 1;
		}
		else if(m_pCommonChars[mid] > code)
		{
			high = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}
//得到字符的宽度, 如果p不为NULL, 则用点阵填充
uchar CLocales::GetCharRaster(ushort code, uchar* p /* = NULL */, FONTSIZE fontsize)
{
	int index;

	if (fontsize == FONTSIZE_NORMAL)
	{
		if(!m_bFontValid)
		{
			return 0;
		}

		if(code < 0x80)//ASCII字符
		{
			if(p)
			{
				memcpy(p, m_pASCIIFont + code * m_nFontBytes, m_nFontBytes);
			}
			return m_pASCIIFont[128 * m_nFontBytes + code];
		}

		//其他字符
		if((index = FindCommonChar(code)) >= 0) //在常用字列表中,直接取值
		{
			if(p)
			{
				memcpy(p, &m_pCommonFont[index * m_nFontBytes], m_nFontBytes);
			}
			return m_pCommonFont[m_nCommonChars * m_nFontBytes + index];
		}
		else //不在,读文件
		{
			return GetOneFontFromFile(code, p);
		}
	}
	else if (fontsize == FONTSIZE_SMALL)
	{
		if(!m_bFontValidSmall)
		{
			return 0;
		}

		if(code < 0x80)//ASCII字符
		{
			if(p)
			{
				memcpy(p, m_pASCIIFontSmall + code * m_nFontBytesSmall, m_nFontBytesSmall);
			}
			return m_pASCIIFontSmall[128 * m_nFontBytesSmall + code];
		}
	}
	return 0;
}

VD_PCSTR LOADSTR(VD_PCSTR key)
{
	MAPSTRING::iterator pi;
	pi = g_Locales.m_mapStrings.find(key);
	if(pi != g_Locales.m_mapStrings.end())
	{
		return (*pi).second;
	}
	else
	{
		trace("Load string '%s' failed!!!\n", key);
		return key;
	}
}

/*!
\b Description		:	通过&字符进行字符串解析，便于在构造函数的时候不调用宏LOADSTRING\n
\b Revisions		:
- 2007-04-06		wangqin		Create
- 2007-04-12		wangqin		modified
*/
VD_PCSTR GetParsedString(VD_PCSTR key)
{
	if (*key == '&')
	{
		return LOADSTR(key+1);
	}
	else
		return key;
}


int CLocales::GetTextExtent(VD_PCSTR str, int len/*=1024*/, FONT_STYLE fs /*= FS_NORMAL*/)
{
	if (!str){
		return 0;
	}
	ushort code;//字符unicode
	int n;
	int w;//字符宽度
	int l;//字符字节数
	int width1 = 0;//字符串宽度
	FONT_STYLE	fontStyle = fs;
	if (len>(int)strlen(str))
	{
		len = strlen(str);
	}
	for (n = 0; n < len; n += l, width1 += w)
	{
		code = GetCharCode(&str[n], &l);
		if(l == 0)
		{
			break;
		}

		if(fontStyle == FS_SMALL)
		{
			w = GetCharRaster(code, NULL, FONTSIZE_SMALL);
		}
		else
		{
			w = GetCharRaster(code);
		}

	}
	return width1;
}

int CLocales::GetLanguage()
{
	return m_iLanguage;
}

FONT_ORDER_E CLocales::GetFontOrder()
{
	if ( m_iLanguage == 20 
		|| m_iLanguage == 17 
		|| m_iLanguage == 21 )
	{
		return FO_Right2Left;
	}

	return FO_Left2Right;
}

FONT_ORDER_E CLocales::GetFontOrderByUnicode( ushort code )
{
	if ( 
		(code >= 0x0590 && code <= 0x05ff)
		|| (code >= 0x0600 && code <= 0x06ff)
		|| (code >= 0xfb50 && code <= 0xfdff)
		|| (code >= 0xfe70 && code <= 0xfeff)
		)
	{
		return FO_Right2Left;
	}

	return FO_Left2Right;
}

