
#include <string.h>
#include <stdlib.h>

#include "System/Version.h"
#include "System/AppConfig.h"

#include "APIs/Video.h"

#if defined(WEB_SHOW_VERSION)
char FileWebName[VD_MAX_PATH] = WEB_DIR"/olp.js";        // 保存Web信息的配置文件名
#define BUFSIZE 8192
#endif

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
//
// 视新的特殊OEM版本号也移到外部定义
//GB\HB的版本号移到外面控制 2006-5-17 XUFANLI

#ifdef WIN32
#undef OEM_TYPE
#define OEM_TYPE "AB"
#undef VERSION_MAJ
#define VERSION_MAJ 1
#undef VERSION_MIN
#define VERSION_MIN 00
#undef VERSION_FIX
#define VERSION_FIX "T00"
#endif 

#ifndef VISIONDIGI_SVN_URL
#define VISIONDIGI_SVN_URL ""
#endif

#ifndef VISIONDIGI_SVN_VERSION
#define VISIONDIGI_SVN_VERSION ""
#endif
//
//
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

/***********************************************************************************
  Version history:
  __________________________________________________________________________________
  VERSION    DATE        MODIFICATION
  __________________________________________________________________________________
  100        2005-06-21    beta版
  99                    内部测试版    
***********************************************************************************/

static const char* month[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

extern int g_nCapture;
extern int g_nLogicNum;

CVersion* CVersion::_instance = NULL;

CVersion* CVersion::instance(void)
{
    if(NULL == _instance)
    {
        _instance = new CVersion();
    }
    return _instance;
}


CVersion::CVersion()
{
    trace("CVersion::CVersion()>>>>>>>>>\n");

    m_svn_url = VISIONDIGI_SVN_URL;
    m_svn_version = VISIONDIGI_SVN_VERSION;

}

CVersion::~CVersion()
{
}
static char *GetVersionInfo()
{
#define VER_FILE_PATH "/app/ipcam/verinfo"
	static char verdata[64];
	int iRet = 0;
	FILE *fpVer = NULL;
	char SensorMessage[32] = {0};
	char version[64] = {0};

//extern void GetSenSorType(char *);
	//GetSenSorType(SensorMessage);
	//ZHY 20150520 去除版本信息中的sensor信息
	//sprintf(verdata, "IPC_%s_", SensorMessage);
	sprintf(verdata, "IPC_");
	fpVer = fopen(VER_FILE_PATH, "r");
	if (NULL == fpVer) {
		sprintf(version, "open ver file error");
		goto ReadOver;
	}
	iRet = fscanf(fpVer, "%s", version);
	if(iRet <= 0) {
		sprintf(version, "fscanf return var <= 0");
		fclose(fpVer);
		goto ReadOver;
	}
	fclose(fpVer);
ReadOver:
	strcat(verdata, version);
	return verdata;
}

void CVersion::GetVersion(char *version, int length,int shortFormat)
{

//#define OEM_TYPE "VD2000-EHD-7U"

    if(0 == shortFormat)
    {
        int len = MIN(length, 32);
        if (version)
        {
            memset(version, 0, len);
            char strType[16]={0};
                
            std::string  sVendorVersion = AppConfig::instance()->getString("Global.VendorVersion", "WQ");
            int iSupportedDvrForDvs = (int)AppConfig::instance()->getNumber("Global.SupportedDvrForDvs", 0);
            if(sVendorVersion != "WQ")
            {
                strncpy((char *)version,sVendorVersion.c_str(),20);
            }
            else
            {
                /*判断 g_nCapture  g_nLogicNum*/
                if (g_nCapture > 0 && g_nLogicNum == g_nCapture )
                {
					strcpy(strType, "IPC");
                }
                else if (g_nCapture ==0 &&  g_nLogicNum > 0)
                {
                	strcpy(strType, "NVR");
                }
                else if (g_nCapture > 0 && g_nLogicNum > g_nCapture)//HVR
                {
                    strcpy(strType, "HVR");
                }
                else//DVR
                {
                    if (1 == iSupportedDvrForDvs)/* dvr改成dvs */
                    {
                        strcpy(strType, "DVS");
                    }
                    else 
                    {
                        strcpy(strType, "DVR");
                    }
                }

                sprintf((char *)version, "%s-%d.%d.%d.%s", strType, g_nLogicNum,VERSION_MAJ, VERSION_MIN, (char*)VERSION_FIX);
            }

		#ifdef SDK_3516
		
     	#ifdef FUN_SUPPORT_ISP
	 		sprintf ((char *)version, GetVersionInfo());
     	#else
        	sprintf((char *)version, "%s_%d.%d.S%d.%s", "IPD",VERSION_MAJ, VERSION_MIN, VideoInGetSensorType(0),(char*)VERSION_FIX);     
     	#endif

		#endif
        }
    }
    else
    {
        int len = MIN(length, 128);
        if (version)  
        {
            memset(version, 0, len);
            int ret = SystemGetVersion(version);
            infof("\r\n****GetVersion:%s,%d**ret:%d*****\r\n",version,length,ret);
        }
    }

    std::string str_version = version;
    str_version += ".R";
    str_version += m_svn_version;
    if( str_version.length() < (uint)length)
    {
    	strcpy( version, str_version.c_str() );
    }
    else
    {
    	infof("str_version:%d, length:%d\n", str_version.length(), length);
    }
}

void CVersion::GetVersion(int *pMaj, int *pMin)
{
    *pMaj = VERSION_MAJ;
    *pMin = VERSION_MIN;
}

void CVersion::GetBuildDate(SYSTEM_TIME *pTime)
{
    int i, year, day;
    char s1[] = __DATE__;
    char s2[] = __TIME__;
    char *p;
    for (i = 0; i < 12; i++) {
        if( strncmp(month[i], s1, 3) == 0 )
            break;
    }
    pTime->month = i+1;

    p = s1; p += 4;
    if ( *p == ' ') {
        *p = '0';
    }
    sscanf(p, "%d %d", &day, &year);
    pTime->day    = day;
    pTime->year = year;
    pTime->hour = atoi(s2);
    pTime->minute = atoi(s2 + 3);
    pTime->second = atoi(s2 + 6);
}

VD_BOOL CVersion::GetSerialNumber(char *pData, int len)
{
    VD_BOOL bRet = FALSE;
    
    if ((pData == NULL) || len <= 0)
    {
        return bRet;
    }

#if  1//defined(FUNC_GET_HWSN)
    if (!SystemGetSerialNumber(pData, len))
    {
        bRet = TRUE;
    }
#endif    

    return bRet;
}

std::string& CVersion::GetSvnVer()
{
	return m_svn_version;
}

std::string& CVersion::GetSvnUrl()
{
	return m_svn_url;
}


#if defined(WEB_SHOW_VERSION)
void CVersion::LoadWebData(uchar *WebDataInfo)
{
    uint dwRet = 0;
    uchar *pWebBuf = (uchar *) malloc(BUFSIZE);
    
    if (m_FileWeb.Open(FileWebName, CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate)) 
    {
        dwRet = m_FileWeb.Read(pWebBuf, BUFSIZE);
        if(dwRet != 0)
        {
            memcpy(WebDataInfo, pWebBuf, BUFSIZE);
        }
        else
        {
            trace("Web Version file open Error1\n");
        }
        m_FileWeb.Close();
    }
    else
    {
        trace("Web Version file open Error2\n");
    }
    free(pWebBuf);
}

void CVersion::GetWebVersion(int *web_major, int *web_minor, int *web_third, int *web_four)
{    
    char VerLine[128];

    if (!m_FileWeb.Open(FileWebName, CFile::modeRead))
    {
        trace("Open %s Failed\n", FileWebName);
        return;
    }

    while (m_FileWeb.Gets(VerLine, sizeof(VerLine)))
    {
        char *pVer;

        pVer = strstr(VerLine, "version");
        if (pVer != NULL)
        {
            sscanf(pVer, "version=%d,%d,%d,%d", web_major, web_minor, web_third, web_four);
            break;
        }
    }

    m_FileWeb.Close();
}
#endif


