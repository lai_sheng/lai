
#include <string.h>

#include "System/Tlv.h"


CTlv::CTlv()
{
	m_pData = NULL;
	m_nested = false;
	m_type = 0;
	m_len = 0;
}

CTlv::~CTlv()
{
	if( m_pData != NULL )
	{
		m_pData->Release();
		m_pData = NULL;
	}
}
//!分析数据tlv, 返回0表示成功
int CTlv::Parse(unsigned char* pbuf, uint len)
{
//        int ret = 0;

	uchar * p = pbuf;
	//!取type
	memcpy(&m_type, p, 4);
	//!判断是否嵌套, 最高位是否为1
	if( m_type & 0x80000000 )
	{
		m_nested = true;
		m_type = (m_type - 0x80000000);
	}

	//!取长度
	memcpy(&m_len, p+4, 4);

	if( m_len > len )
	{
		trace("ctlv parse error-m_len:%d, len:%d\n", m_len, len);
		return -1;
	}
    if ( !m_pData)
    {
        m_pData = g_PacketManager.GetPacket(m_len+8);
        if(m_pData == NULL )
	    {
		    trace("ctlv parse error-can't alloc packet!\n");
		    return -1;
	    }
    }
	
	memcpy(m_pData->GetBuffer(), pbuf, m_len+8);
	m_pData->SetLength(m_len+8);

	return 0;
}
//!重新打包tlv, 返回0表示成功
int CTlv::Pack(unsigned char*& pbuf, int& len)
{
	if( m_pData == NULL )
	{
		return -1;
	}

	uint * p = (uint*)m_pData->GetBuffer();
	if( m_nested )
	{
		*p = (m_type+0x80000000);
	}
	else
	{
		*p = m_type;
	}
	memcpy(m_pData->GetBuffer()+4, (unsigned char*)&m_len, 4);
	pbuf = m_pData->GetBuffer();
	len = m_len+8;
	return 0;
}

int CTlv::GetType()
{
	return m_type;
}
uint CTlv::GetLength()
{
	return m_len;
}
uint CTlv::GetTotalLen()
{
	return (m_len+8);
}
int CTlv::GetValue(unsigned char* pbuf, uint size)
{
	if(size < m_len )
	{
		trace("ctlv GetValue error, size:%d, m_len:%d!\n", size, m_len);
		return -1;
	}

	if( m_len > 0)
	{
		memcpy(pbuf, m_pData->GetBuffer()+8, m_len);
	}
	return 0;
}

unsigned char* CTlv::GetValueBuf()
{
	if( m_len <= 0 )
	{
		return NULL;
	}

	return m_pData->GetBuffer()+8;
}

int CTlv::SetType( uint type )
{
	m_type = type;
	return 0;
}

//!bTlv-扩展数据是否是一个tlv
int CTlv::SetValue(unsigned char* pbuf, uint len, bool bTlv )
{
	if( m_pData == NULL)
	{
		m_pData = g_PacketManager.GetPacket(len+8);
		if( m_pData == NULL)
		{
			trace("ctlv SetValue error-can't alloc packet!\n");
			return -1;
		}
	}
	else if( (uint)len > m_pData->GetSize() )
	{
		CPacket* pPacket = g_PacketManager.GetPacket(len+8);
		if( pPacket == NULL)
		{
			trace("ctlv SetValue error-can't alloc packet!\n");
			return -1;
		}

		memcpy(pPacket->GetBuffer(), m_pData->GetBuffer(), m_len);
		m_pData->Release();
		m_pData = pPacket;
	}
	m_len = len;

	if( m_len > 0 )
	{
		memcpy(m_pData->GetBuffer()+8, pbuf, len);
	}

	m_nested = bTlv;
	return 0;
}

void CTlv::dump()
{
	trace("dump tlv start!\n");

	uchar * p = m_pData->GetBuffer();
	for(uint i = 0; i < 8+m_len; i++)
	{
		trace("%#x ", p[i] );
		if( i %16 == 0 )
		{
			trace("\n");
		}
	}

	trace("dump tlv end!\n");
}

TlvReader::TlvReader()
{

}
TlvReader::~TlvReader()
{
	Clear();
}

int TlvReader::Parse(unsigned char* pbuf, uint len)
{
	int parse_len = 0;
	int ret = 0;

	while( parse_len < len )
	{
		CTlv* pNewTlv = new CTlv();

		ret = pNewTlv->Parse(pbuf+parse_len, len-parse_len);
		if( ret < 0 )
		{
			trace("TlvReader::Parse-error parse tlv!\n");
			return -1;
		}

		m_list.push_back(pNewTlv);
		parse_len += pNewTlv->GetTotalLen();
	}
	return 0;
}

void TlvReader::Clear()
{
	TLV_LIST::iterator it;

	for(it = m_list.begin(); it != m_list.end(); it++ )
	{
		delete (*it);
	}

	m_list.clear();
}

TLV_LIST& TlvReader::GetTlvList()
{
	return m_list;
}


CTlvEx::CTlvEx( CTlvEx* pfather )
{
	this->father = pfather;
	m_type = 0;
	m_len = 0;
	m_nested = false;
}

CTlvEx::CTlvEx(CTlvEx& tlv)
{
	m_type = tlv.m_type;
	m_len = tlv.m_len;
	m_nested = tlv.m_nested;
	father = NULL;

	if ( m_nested )
	{
		if ( m_len > 0 )
		{
			value_.m_tlv_list = new TLV_LIST_EX;

			TLV_LIST_EX::iterator it;

			for ( it = tlv.value_.m_tlv_list->begin(); it != tlv.value_.m_tlv_list->end(); it++ )
			{
				CTlvEx * pNewTlv = new CTlvEx( *(*it) );
				pNewTlv->father = this;
				value_.m_tlv_list->push_back( pNewTlv );
			}
		}
	}
	else
	{
		if ( m_len > 0 )
		{
			value_.value  = new unsigned char[m_len];
			memcpy(value_.value, tlv.value_.value, m_len);
		}
	}

}

CTlvEx::~CTlvEx()
{
	if ( m_len > 0 )
	{
		if ( m_nested)
		{
			ClearList();
		}
		else
		{
			delete[] value_.value;
		}
	}
}

unsigned int CTlvEx::GetType()
{
	return m_type;
}
unsigned int CTlvEx::GetLen()
{
	return m_len;
}
unsigned int CTlvEx::GetSize()
{
	return m_len+8;
}

int CTlvEx::SetType( unsigned int Type )
{
	m_type = Type;
	return 0;
}
int CTlvEx::SetValue( unsigned char* buf, unsigned int len )
{
	if ( m_nested )
	{
		ClearList();
		m_nested = false;
	}
	else if ( m_len > 0 )
	{
		delete value_.value;
		m_len = 0;
	}

	m_len = len;
	if ( m_len > 0 )
	{
		value_.value = new unsigned char[m_len];
		assert( value_.value != NULL );

		memcpy(value_.value, buf, m_len);
	}

	if ( father != NULL )
	{
		father->Refresh();
	}

	return 0;
}

int CTlvEx::SetValue( CTlvEx& pTlv )
{
	if ( !m_nested )
	{
		if ( m_len > 0)
		{
			delete[] value_.value;
			value_.m_tlv_list = new TLV_LIST_EX;
		}

		m_nested = true;
	}
	else
	{
		ClearList();
		m_len = 0;
	}

	m_len = pTlv.GetSize();
	CTlvEx* pNewTlv = new CTlvEx(pTlv);
	pNewTlv->father = this;
	value_.m_tlv_list->push_back(pNewTlv);
	return 0;
}

int CTlvEx::Append( unsigned char* buf, unsigned int len )
{
	if ( m_nested )
	{
		return -1;
	}

	unsigned char * p = new unsigned char[m_len+len];
	if ( m_len > 0 )
	{
		memcpy(p, value_.value, m_len);
	}

	memcpy(p+m_len, buf, len);
	m_len += len;

	delete[] value_.value;
	value_.value = p;

	if ( father != NULL )
	{
		father->Refresh();
	}

	return 0;
}
int CTlvEx::Append( CTlvEx& pTlv )
{
	CTlvEx * pNewTlv = new CTlvEx(pTlv);
	assert( pNewTlv != NULL );

	if ( !m_nested )
	{
		if ( m_len > 0)
		{
			delete pNewTlv;
			return -1;
		}
		else
		{
			value_.m_tlv_list = new TLV_LIST_EX;
			m_nested = true;
		}
	}

	pNewTlv->father = this;//pNewTlv->SetFather(this);
	value_.m_tlv_list->push_back(pNewTlv);

	m_len += pNewTlv->GetSize();

	if ( father != NULL )
	{
		father->Refresh();
	}

	return 0;	
}

int CTlvEx::GetValue( unsigned char* buf, unsigned int len )
{
	if ( m_len > len )
	{
		return -1;
	}

	if ( m_nested )
	{
		TlvWriterEx write;

		return write.Write(*value_.m_tlv_list, buf, len);
	}
	else
	{
		memcpy(buf, value_.value, m_len);
	}

	return 0;
}

CTlvEx* CTlvEx::GetSubTlv( unsigned int Type )
{
	if ( !m_nested )
	{
		return NULL;
	}

	std::list<CTlvEx*>::iterator it;

	for ( it = value_.m_tlv_list->begin(); it != value_.m_tlv_list->end(); it++)
	{
		if( (*it)->GetType() == Type )
		{
			return ( (*it) );
		}
	}

	return NULL;
}

void CTlvEx::ClearList()
{
	TLV_LIST_EX::iterator it;

	for ( it = value_.m_tlv_list->begin(); it != value_.m_tlv_list->end(); it++)
	{
		delete (*it);
	}
	value_.m_tlv_list->clear();

	delete value_.m_tlv_list;
}

int CTlvEx::GetSubTlvList( TLV_LIST_EX& list, unsigned int Type )
{
	int num = 0;
	if ( !m_nested )
	{
		return -1;
	}

	std::list<CTlvEx*>::iterator it;

	for ( it = value_.m_tlv_list->begin(); it != value_.m_tlv_list->end(); it++)
	{
		if( (*it)->GetType() == Type )
		{
			num++;
			list.push_back( (*it) );
		}
	}

	if ( num > 0 )
	{
		return 0;
	}

	return -1;
}

void CTlvEx::Refresh()
{
	if ( m_nested )
	{
		std::list<CTlvEx*>::iterator it;
		unsigned int total_len = 0;

		for ( it = value_.m_tlv_list->begin(); it != value_.m_tlv_list->end(); it++)
		{
			total_len += (*it)->GetSize();
		}

		m_len = total_len;
	}

	if ( father != NULL )
	{
		father->Refresh();
	}

}

bool CTlvEx::GetNestflag()
{
	return m_nested;
}


int TlvReaderEx::Parse( unsigned char* pbuf, unsigned int len )
{
	return Parse(pbuf, len, NULL);
}

int TlvReaderEx::Parse(unsigned char* pbuf, unsigned int len, CTlvEx* father )
{
	unsigned int parse_len = 0;
	int ret = 0;

	while( parse_len < len )
	{
		CTlvEx* pNewTlv = new CTlvEx();
		pNewTlv->father = father;

		unsigned char * p = pbuf+parse_len;
		unsigned int tmp_type = 0;
		unsigned int tmp_len = 0;

		memcpy(&tmp_type, p, 4);
		if( tmp_type & 0x80000000 )
		{
			pNewTlv->m_nested = true;
			pNewTlv->SetType(tmp_type - 0x80000000);
		}
		else
		{
			pNewTlv->SetType(tmp_type);
		}

		memcpy(&tmp_len, p+4, 4);

		if ( father == NULL )
		{
			m_list.push_back(pNewTlv);
		}
		else
		{
			if ( father->m_len == 0 )
			{
				father->value_.m_tlv_list = new TLV_LIST_EX;
			}
			father->value_.m_tlv_list->push_back(pNewTlv);
			father->m_len += pNewTlv->GetSize();
		}

		//!嵌套分析
		if ( pNewTlv->m_nested )
		{
			Parse(p+8, tmp_len, pNewTlv);
		}
		else
		{
			pNewTlv->SetValue(p+8, tmp_len);
		}

		parse_len += pNewTlv->GetSize();
	}

	return 0;
}

void TlvReaderEx::Clear()
{
	TLV_LIST_EX::iterator it;
	for ( it = m_list.begin(); it != m_list.end(); it++ )
	{
		delete (*it);
	}

	m_list.clear();
}

TLV_LIST_EX& TlvReaderEx::GetTlvList()
{
	return m_list;
}

TlvReaderEx::~TlvReaderEx()
{
	Clear();
}

TlvWriterEx::TlvWriterEx()
{

}

int TlvWriterEx::WriteAlloc( TLV_LIST_EX& tlv_list, unsigned char*& p, unsigned int& len )
{
	unsigned int total_len = 0;

	TLV_LIST_EX::iterator it;
	for( it = tlv_list.begin(); it != tlv_list.end(); it++ )
	{
		total_len += (*it)->GetSize();
	}

	p = new unsigned char[total_len];
	assert(p!= NULL);

	Write(tlv_list, p, total_len);

	len = total_len;
	return 0;
}

int TlvWriterEx::WriteAlloc( CTlvEx& tlv, unsigned char*& p, unsigned int& len )
{
	p = new unsigned char[tlv.GetSize()];

	Write(tlv, p, tlv.GetSize() );

	len = tlv.GetSize();
	return 0;
}

int TlvWriterEx::Write( TLV_LIST_EX& tlv_list, unsigned char * pBuf, unsigned int size)
{
	unsigned int total_len = 0;

	TLV_LIST_EX::iterator it;
	for( it = tlv_list.begin(); it != tlv_list.end(); it++ )
	{
		total_len += (*it)->GetSize();
	}

	if ( total_len > size )
	{
		return -1;
	}


	int tmp_len = 0;
	for( it = tlv_list.begin(); it != tlv_list.end(); it++ )
	{
		unsigned int type = (*it)->GetType();
		unsigned int it_len = (*it)->GetLen();

		if( (*it)->m_nested )
		{
			type = type+0x80000000;
		}
		memcpy( pBuf+tmp_len, &type, 4 );
		memcpy( pBuf+tmp_len+4, &it_len, 4 );
		
		if ( (*it)->m_nested )
		{
			Write( *( (*it)->value_.m_tlv_list), pBuf+tmp_len+8, size-tmp_len-8 );
		}
		else
		{
			memcpy(pBuf+tmp_len+8, (*it)->value_.value, (*it)->m_len );
		}

		tmp_len += (*it)->GetSize();
	}

	return 0;
}

int TlvWriterEx::Write( CTlvEx& tlv, unsigned char * pBuf, unsigned int size)
{
	if ( tlv.GetSize() > size )
	{
		return -1;
	}

	unsigned int type = tlv.GetType();
	unsigned int it_len = tlv.GetLen();

	if( tlv.m_nested )
	{
		type = type+0x80000000;
	}

	memcpy( pBuf, &type, 4 );
	memcpy( pBuf+4, &it_len, 4 );

	if( tlv.m_nested )
	{
		Write( *tlv.value_.m_tlv_list, pBuf+8, size-8 );
	}
	else
	{
		memcpy(pBuf+8, tlv.value_.value, tlv.m_len);
	}

	return 0;
}

#include <stdarg.h>

static int m_dbg = 1;
void TlvPrintf( const char * format, ... )
{
	if ( m_dbg == 0 )
	{
		return ;
	}
	
	va_list ap;
	va_start ( ap, format );
	vprintf(format, ap);
	va_end(ap);
}

void PrintTlvBuf( unsigned char* buf, int size )
{
	int ret = 0;
	TlvReaderEx reader;

	ret = reader.Parse(buf, size);
	if ( ret < 0 )
	{
		TlvPrintf("analyse buf error!\n");
		return ;
	}

	TlvPrintf( "tlv-num:%d\n", reader.GetTlvList().size() );
	TLV_LIST_EX::iterator it;

	for ( it = reader.GetTlvList().begin(); it != reader.GetTlvList().end(); it++ )
	{
		TlvPrintf("type:%#x, nested:%d, len:%d\n", 
			(*it)->GetType(), (*it)->GetNestflag(), (*it)->GetLen() );
		if (  (*it)->GetLen() > 0 )
		{
			unsigned char * pValueBuf = new unsigned char[ (*it)->GetLen() ];
			memset( pValueBuf, 0, sizeof( (*it)->GetLen() ) );
			(*it)->GetValue(pValueBuf, (*it)->GetLen() );
			if ( (*it)->GetNestflag() )
			{
				TlvPrintf("sub tlv======begin\n");
				PrintTlvBuf(pValueBuf, (*it)->GetLen() );
				TlvPrintf("sub tlv======end\n");
			}
			else
			{
				TlvPrintf("  value:%s\n", pValueBuf );
			}
			delete[] pValueBuf;
		}
	}

}

void Test_tlvWriter()
{
	CTlvEx tlv;
	char * buf = "12345678";

	tlv.SetType(1);
	tlv.SetValue( (unsigned char*)buf, static_cast<unsigned int>( strlen(buf)+1 ) );

	TlvWriterEx writer;

	unsigned char * p;
	unsigned int len;
	writer.WriteAlloc(tlv, p, len );

	PrintTlvBuf(p,len);
	//printf("len:%d, str:%s\n", len, p+8);
	delete[] p;
}

void Test_tlvAdd()
{
	unsigned char * p;
	unsigned int len;
	CTlvEx tlv;
	char * buf = "12345678";
	char * buf2 = "87654321";
	char * buf3 = "abcdefgh";

	tlv.SetType(1);
	tlv.SetValue( (unsigned char*)buf, static_cast<unsigned int>( strlen(buf) ) );

	//!test add value, not tlv!
	tlv.Append( (unsigned char*)buf2, static_cast<unsigned int>( strlen(buf2)+1) );

	TlvWriterEx writer;

	writer.WriteAlloc(tlv, p, len );
	PrintTlvBuf(p, len );
	//printf("add value--len:%d, str:%s\n", len, p+8);
	delete[] p;

	//!test set value, not tlv
	tlv.SetValue( (unsigned char*)buf2, static_cast<unsigned int>( strlen(buf2)+1 ) );
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!test set type
	tlv.SetType(2);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//! set value- null
	tlv.SetValue(NULL, 0);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!add tlv
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!add tlv-2
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!mod sub tlv value
	CTlvEx* pSubTlv;
	pSubTlv = tlv.GetSubTlv( tlv.GetType() );
	pSubTlv->SetValue((unsigned char*)buf3, static_cast<unsigned int>( strlen(buf3)+1) );
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

}

//!里边value会比较大
void Test_tlvAdd_BitValue()
{
	int old_dbg = m_dbg;
	m_dbg = 0;

	const char * letter = "abcdefghijklmnopqrstuvwxyz";
	unsigned char * p;
	unsigned int len;
	CTlvEx tlv;
	char  *buf = new char[10*2048];
	char * buf2 = new char[10*2048];
	char * buf3 = new char[10*2048];

	for ( int i = 0; i < 10*2048-1; i++ )
	{
		buf[i] = i%10+1;
		buf2[i] = ( (9-i%10)+1 );
		buf3[i] = letter[i%strlen(letter)];
	}
	
	tlv.SetType(1);
	tlv.SetValue( (unsigned char*)buf, static_cast<unsigned int>( strlen(buf) ) );

	//!test add value, not tlv!
	tlv.Append( (unsigned char*)buf2, static_cast<unsigned int>( strlen(buf2)+1) );

	TlvWriterEx writer;

	writer.WriteAlloc(tlv, p, len );
	PrintTlvBuf(p, len );
	//printf("add value--len:%d, str:%s\n", len, p+8);
	delete[] p;

#if 1
	//!test set value, not tlv
	tlv.SetValue( (unsigned char*)buf2, static_cast<unsigned int>( strlen(buf2)+1 ) );
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!test set type
	tlv.SetType(2);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//! set value- null
	tlv.SetValue(NULL, 0);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!add tlv
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!add tlv-2
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;

	//!mod sub tlv value
	CTlvEx* pSubTlv;
	pSubTlv = tlv.GetSubTlv( tlv.GetType() );
	pSubTlv->SetValue((unsigned char*)buf3, static_cast<unsigned int>( strlen(buf3)+1) );
	tlv.Append(tlv);
	writer.WriteAlloc(tlv, p, len );
	//printf("set value--len:%d, str:%s\n", len, p+8);
	PrintTlvBuf(p, len );
	delete[] p;
#endif

	delete[] buf;
	delete[] buf2;
	delete[] buf3;

	m_dbg = old_dbg;
}

void Test_TlvMemory()
{
	m_dbg = 0;
	for(int i = 0; i < 1000; i++)
	{
		Test_tlvWriter();
		Test_tlvAdd();
		Test_tlvAdd_BitValue();
	}

	m_dbg = 1;
}

int tlv_main( int argn, char **argc )
{
	Test_tlvWriter();
	Test_tlvAdd();

	Test_TlvMemory();

#if 0
	for (int i = 0 ; i < 1000; i++ )
	{
		printf("put any key go for:%d", i);
		getchar();
		Test_tlvAdd_BitValue();
	}
#endif
	
	return 0;
}
