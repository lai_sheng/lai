// test_signals.cpp : Defines the entry point for the console application.
//

#include <time.h>
#include "stdafx.h"
#include "System/Signals.h"
#include "APIs/System.h"
#include "Thread.h"

// TSignal3<int, uint, char *>
//!TSIG ʹ��TSinal3 ���� TSignal3_S
template <typename TSIG >
class CMyThread : public CThread
{
public:
	typedef typename TSIG::SigProc SIG_DEV_CAP_BUFFER;
public:
	CMyThread()
		:CThread("CMyThread", 0)
		,m_sigRecord(32)
	{

	}
	void Start()
	{
		CreateThread();
	}
	void Stop()
	{
		DestroyThread(TRUE);
	}

	void Attach(CObject * pObj, SIG_DEV_CAP_BUFFER pProc)
	{
		int RecSlots = m_sigRecord.Attach(pObj, pProc);
		assert(RecSlots > 0);
	}

	void Detach(CObject * pObj, SIG_DEV_CAP_BUFFER pProc)
	{
		int RecSlots = m_sigRecord.Detach(pObj, pProc);
		assert(RecSlots >= 0);
	}

	virtual void ThreadProc()
	{
		while ( m_bLoop )
		{
			m_sigRecord(0, 0, NULL);
		}
	}
protected:
	TSIG m_sigRecord;	
private:
};

class CMytest : public CObject
{
public:
	CMytest()
	{
		m_num = 0;
	}
	void callback(int ,uint,  char *)
	{
		m_num = 1;
		SystemSleep(5*1000);
		m_num = 0;
	}

	void PrintResult()
	{
		printf("test :%s\n", m_num > 0 ? "failed": "success");
	}
protected:
private:
	int m_num;
};

template <typename TSIG>
void test1( const char * str )
{
	CMytest test_m;
	CMyThread< TSIG > t_run;

	t_run.Start();
	
	int i = 0;
	while( i < 10)
	{
		printf("%s attach!\n", str );
		t_run.Attach( &test_m, (CMyThread< TSIG >::SIG_DEV_CAP_BUFFER)&CMytest::callback);
		printf("%s attach finish!\n", str );

		SystemSleep(1*1000);

		printf("%s detach!\n", str );
		uint t1 = time(NULL); 
		t_run.Detach(&test_m, (CMyThread< TSIG >::SIG_DEV_CAP_BUFFER)&CMytest::callback);
		uint t2 = time(NULL);
		printf("%s detach finish!, time:%d\n", str,  (t2-t1));

		printf("i: %d ", i );
		i++;
		test_m.PrintResult();
	}
	t_run.DestroyThread(TRUE);
}
int _tmain(int argc, _TCHAR* argv[])
{
	printf("test tsignal3\n");
	test1< TSignal3<int, uint, char *> >( "tsignal3");

	printf("test tsignal3_S\n");
	test1< TSignal3_S<int, uint, char *> >("tsignal3_S");
	return 0;
}

