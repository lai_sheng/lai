#include "System/UserManager.h"
#include "System/Packet.h"
#include "System/Console.h"
#include "Net/NetWorkService.h"
#include "Functions/DriverManager.h"
#include "Devices/DevCapture.h"
#include "APIs/Memory.h"

#include "WFS/adapt.h"
#ifdef WIN32
#include <direct.h>
#endif

#include "Main.h"
#include "Functions/General.h"
#include "APIs/Split.h"

#ifdef __linux__
#include <termios.h>
#include <signal.h>

// 退出时恢复中断状态
void myExit(int signo)
{
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    tty.c_lflag |= ECHO;    /*enable echo*/
    tty.c_lflag |= ICANON;
    tcsetattr(STDIN_FILENO,TCSANOW, &tty);
    exit(0);    
}
#endif


CConsoleArg::CConsoleArg(int argc, char **argv)
{
    m_internal = new ConsoleArgInternal(argc, argv);
}

CConsoleArg::~CConsoleArg()
{
    delete m_internal;
}

char *CConsoleArg::getOptions()
{
    if (m_internal->m_argc < 1)
    {
        return 0;
    }

    char *str = m_internal->m_argv[0];

    if ('-' != str[0])//命令行选项必须以-开头
    {
        return 0;
    }

    return &m_internal->m_argv[0][1];
}

char * CConsoleArg::GetArg(int pos)
{
    if (m_internal->m_argc < (pos + 2))
    {
        return 0;
    }

    return m_internal->m_argv[pos + 1];
}

int CConsoleArg::getHex(int pos)
{
    unsigned int value = 0;

    if (m_internal->m_argc < (pos + 1))
    {
        return 0;
    }

    sscanf(m_internal->m_argv[pos + 1], "%x", &value);

    return value;
}

int CConsoleArg::getInt(int pos)
{
    int value = 0;

    if (m_internal->m_argc < (pos + 1))
    {
        return 0;
    }

    sscanf(m_internal->m_argv[pos + 1], "%d", &value);

    return value;
}

PATTERN_SINGLETON_IMPLEMENT(CConsole);

CConsole::CConsole() 
: CThread("Console", TP_COM, 1024), m_iState(stateOff), m_bUsage(false), 
    m_bPacket(false), m_bBitrate(false), m_Timer("Console")
{
    registerCmd(CConsole::Proc(&CConsole::onHelp, this), "help", "Dump this help message!");
    registerCmd(CConsole::Proc(&CConsole::onResource, this), "cpu", "Dump the CPU usage!");
    registerCmd(CConsole::Proc(&CConsole::onPacket, this), "packet", "Dump packet infomation or packet!");
    registerCmd(CConsole::Proc(&CConsole::onBitRate, this), "bitrate", "Show the bit rate!");
    registerCmd(CConsole::Proc(&CConsole::onReboot, this), "reboot", "Reboot the system!");

    registerCmd(CConsole::Proc(&CConsole::onTime, this), "time", "Time operating!");
    registerCmd(CConsole::Proc(&CConsole::onQuit, this), "quit", "Logout myself!");
    registerCmd(CConsole::Proc(&CConsole::onShell, this), "shell", "Entering system shell!");
    registerCmd(CConsole::Proc(&CConsole::onTimer, this), "timer", "Dump all timer!");
    registerCmd(CConsole::Proc(&CConsole::onThread, this), "thread", "Dump all thread!");

    registerCmd(CConsole::Proc(&CConsole::onPartition, this), "part", "Partition ide !");
#if defined (SDK_3516)
    registerCmd(CConsole::Proc(&CConsole::onSwitchVideoOut, this), "videoout", "VideoOut set !");
    registerCmd(CConsole::Proc(&CConsole::onSwitchProductType, this), "vnPro", "VN Pro type set !");
#endif
    registerCmd(CConsole::Proc(&CConsole::OnAuth, this), "auth", "auth control !");
}

CConsole::~CConsole()
{
    unregisterCmd("help");
}

bool CConsole::Start()
{
    #ifdef __linux
    signal(SIGINT, &myExit);
    #endif

    //创建线程
    CreateThread();
    m_Timer.Start((CObject*)this, (VD_TIMERPROC)&CConsole::onPrintTimer, 3000, 3000);

    return true;
}

bool CConsole::Stop()
{
    //销毁线程
    DestroyThread();

    m_Timer.Stop();

    return true;
}

bool CConsole::registerCmd(Proc proc, char *cmdStr, char *helpStr)
{
    Command cmd;    ///<注册函数的结构体
    cmd.proc = proc;
    cmd.helpStr = helpStr;

    std::pair<CommandTable::iterator, bool> ret = 
            m_sDbgTbl.insert(CommandTable::value_type(cmdStr, cmd));

    if (!ret.second)
    {
        tracef("CConsole::registerCmd had been register!!\n");

        return false;
    }
    //tracef("CConsole::registerCmd OK!!\n");

    return true;
}

bool CConsole::unregisterCmd(char *cmdStr)
{
    if(m_sDbgTbl.erase(cmdStr))
    {
        tracef("CConsole::unregisterCmd OK!\n");

        return true;
    }
    tracef("CConsole::unregisterCmd not find the proc !\n");

    return false;
}

void CConsole::ThreadProc()
{
    char    buf[64];
    int i = 0;
    setEcho(TRUE);    // 设置为普通状态
    while(m_bLoop)
    {
        fflush(stdout);
        memset(buf, 0, sizeof(buf));

        for (i = 0; i < 64; i++)
        { 
            buf[i]=getchar();
            if(buf[i] == '\n')
            {
                //printf("\n");
                break;
            }            
        }

        // 命令长度超出，丢弃
        if(i >= 64)
        {
            ///避免串口作为其他用途时，getchar总是立即返回构成死循环
            SystemSleep(1000);

            continue;
        }
    
        // 去掉多余的换行符
        if(i > 0 && buf[i - 1] == '\r')
        {
            i--;
        }
        buf[i] = '\0';

        onLine(buf);
    }
}

void CConsole::onLine(char* buf)
{
    char    *p[16];

    int i = 0;
    int n = 0;

    if (m_iState != stateRunning)
    {
        switch(m_iState)
        {
        case stateOff:
            printf("user name:");
            m_iState = stateUserName;
            break;
        case stateUserName:
            // 设置为密码状态
            setEcho(FALSE);
            printf("\n");
            strcpy(m_szUserName, buf);
            printf("password:");
            m_iState = statePassword;
            break;
        case statePassword:
        
            printf("\n"); 

            bool bRet = m_cUgmUser.login(m_szUserName, buf, "Console");
            setEcho(TRUE);    // 设置为普通状态
            if (false == bRet)
            {
                printf("User not valid!\n");
                printf("user name:");
                m_iState = stateUserName;
            }
            else
            {
                m_iState = stateRunning;
                printf("%s$", m_szUserName);
            }
            break;
        }
    }
    else//命令行状态
    {
        n = 0;
        p[0] = buf;

        if(strlen(buf) == 0) //空字符串
        {
            printf("\n");
            printf("%s$", m_szUserName);
            return;
        }

        while(buf[i])
        {
            while(buf[i] == ' ')
            {
                buf[i] = '\0';
                i++;
            }//去掉命令行中前面的空格

            if(buf[i])
            {
                p[n++] = &buf[i];//保存首地址
            }

            while(buf[i] && buf[i] != ' ')
            {
                i++;
            }
        }

        CommandTable::iterator iter = m_sDbgTbl.find(p[0]);
        if (iter != m_sDbgTbl.end())
        {
            if (n == 1)
            {
                //各个模块接收到的参数是从'-'开始的
                p[1] = NULL;
            }
            (*iter).second.proc(n - 1, &p[1]);
        }
        else
        {
            printf("'%s' is not a command.\n", p[0]);
        }

        if(m_iState == stateRunning)
        {
            printf("%s$", m_szUserName);
        }
    }
}

int CConsole::onHelp(int argc, char ** argv)
{
    tracef("-------------------------------------------------------------------\n");
    for(CommandTable::iterator iter = m_sDbgTbl.begin(); iter != m_sDbgTbl.end(); ++iter)
    {    
        tracef("%-9s   %s\n", (*iter).first.c_str(), (*iter).second.helpStr.c_str());
    }
    tracef("-------------------------------------------------------------------\n");
    tracef("To see details, please use `cmd -h`. \n");
    
    return true;
}

int CConsole::onResource(int argc, char **argv)
{
    if(argc || argv)
    {
        m_bUsage = !m_bUsage;
    }

    if(!m_bUsage)
    {
        return 0;
    }

    dumpCPUandPacket(0);

    return 0;
}

int CConsole::onPacket(int argc, char **argv)
{
    if(argc || argv)
    {
        m_bPacket = !m_bPacket;
    }

    if(!m_bPacket)
    {
        return 0;
    }
    dumpCPUandPacket(0);
        
    return 0;
}

int CConsole::onBitRate(int argc, char **argv)
{    
    if(argc || argv)
    {
        m_bBitrate = !m_bBitrate;
    }

    if(!m_bBitrate)
    {
        return 0;
    }

    dumpCPUandPacket(0);
    return 0;
}


int CConsole::onReboot(int argc, char **argv)
{
    g_Challenger.Reboot();
    return 0;
}

int CConsole::onTime(int argc, char **argv)
{
    char *pszCmd0, *par1, *par2;
    int year, mon, day, hour, min, sec;
    SYSTEM_TIME date;

    CConsoleArg arg(argc, argv);

    pszCmd0 = arg.getOptions();

    if (pszCmd0 == NULL)
    {
        goto show;
    }

    switch (pszCmd0[0])
    {
    case 's':
        par1 = arg.GetArg(0);
        par2 = arg.GetArg(1);
        if(!par1 || !par2)
        {
            goto usage;
        }

        sscanf(par1, "%d-%d-%d", &year, &mon, &day);
        sscanf(par2, "%d:%d:%d", &hour, &min, &sec);

        if (year < 2000 || year > 2063 || mon < 1 || mon > 12 ||
            day < 1 || day > 31 || hour > 23 || min > 59 || sec > 59)
        {
            debugf("Set time to %d-%02d-%02d %02d:%02d:%02d FAILED!!!\n",
                year, mon, day, hour, min, sec);
            return 0;
        }

        date.year        = year;
        date.month        = mon;
        date.day        = day;
        date.hour        = hour;
        date.minute        = min;
        date.second        = sec;

        SystemSetCurrentTime(&date);

        return 0;
    default:
usage:
        tracef("time command usage:\n");
        tracef("time -s yyyy-mm-dd hh:mm:ss : set time to [yyyy]-[mm]-[dd] [hh]:[mm]:[ss]\n");
        tracef("time : show current time\n");

        return 0;
show:
        SystemGetCurrentTime(&date);

        tracef("current time : %d-%02d-%02d %02d:%02d:%02d\n",
            date.year, date.month, date.day,
            date.hour, date.minute, date.second);

        return 0;
    }
}

int CConsole::onQuit(int argc, char **argv)
{
    m_iState = stateOff;

    m_cUgmUser.logout();

    tracef("CConsole::onQuit quit debug.\n");
    return 0;
}

int CConsole::onShell(int argc, char **argv)
{
    system("sh");
    return 0;
}

int CConsole::dumpCPUandPacket(uint arg)
{
    /************************************************************************
    定时显示的内容：
    1、定时显示CPU的利用率；
    2、定时显示包的使用率；
    ************************************************************************/
    if(m_bUsage)
    {
        trace("The current cpu usage is %d%%\n ", SystemGetCPUUsage());
    }

    if(m_bPacket)
    {
        g_PacketManager.Dump();
    }
    
    if(m_bBitrate)
    {
        CDevCapture::getManager()->dumpStatInfo();
    }
    return 0;
}

int CConsole::onThread(int argc, char **argv)
{
    g_ThreadManager.DumpThreads();
    return 0;
}

void CConsole::onPrintTimer(uint arg)
{
    dumpCPUandPacket(0);
}

int CConsole::onTimer(int argc, char **argv)
{
    g_TimerManager.DumpTimers();
    return 0;
}


#if defined (SDK_3516)
//98 产品id最后一位标识
#define E2PROM_PRODUCTID_OFFSET		(64 + 4)
#define E2PROM_READ_SIZE 128
int CConsole::onSwitchVideoOut(int argc, char **argv)
{
    char *pszCmd0, *par1;
    int iVideoOut = 0;
    CConsoleArg arg(argc, argv);

    pszCmd0 = arg.getOptions();

    if (pszCmd0 == NULL)
    {
        goto usedHelp;
    }

    switch (pszCmd0[0])
    {
    	case 's':
		{
			par1 = arg.GetArg(0);
			if(!par1)
			{
				goto usedHelp;
			}

			iVideoOut = atoi(par1);
			if (iVideoOut > 3 || iVideoOut < 0)
			{
				goto usedHelp;
			}
			SystemSetVGAPixel(iVideoOut);
			if (0 == SystemSetVGAResolution(iVideoOut))
			{
				//g_Challenger.Reboot();
			}						
			return 0;
		}
    default:
usedHelp:
        tracef("videoout command usage:\n");
        tracef("videoout -s 0/1/2/3: set videoout to 0:1080p, 1:1280*1024, 2:720p, 3:1024*768\n");
    }
    return 0;
}

int CConsole::onSwitchProductType(int argc, char **argv)
{
	char *pszCmd0, *par1,*par2;
	pszCmd0 = 0;
	par1 = 0;
	par2 = 0;
	
	int iProductType = 0;
	CConsoleArg arg(argc, argv);

	pszCmd0 = arg.getOptions();

	if (pszCmd0 == NULL)
	{
		goto usedHelp;
	}

	switch (pszCmd0[0])
	{
		case 's':
		{
			par1 = arg.GetArg(0);
			if(!par1)
			{
				goto usedHelp;
			}
			iProductType = atoi(par1);
			unsigned char productType = iProductType;
			EepromWrite(E2PROM_PRODUCTID_OFFSET, (void *)&productType, 1);
			
			infof("CConsole::SwitchProductType 0x%2x.\n", productType);

		   	return 0;
		}
		case 'g':
		{
			unsigned char productType = 0;
			EepromRead(E2PROM_PRODUCTID_OFFSET, &productType, 1);
			infof("CConsole::productType = 0x%2x.\n", productType);
			return 0;
		}
		case 'e':
		{
			int iOffset = -1;
			int iValue = -1;
			par1 = arg.GetArg(0);
			if(!par1)
			{    
			      goto usedHelp;
			}
			
	       iOffset = atoi(par1);
			if ( iOffset < 0 || iOffset >=E2PROM_READ_SIZE)
			{
				for(iOffset = 0; iOffset < E2PROM_READ_SIZE; iOffset++)
				{
				   iValue = 0;
					EepromRead(iOffset, &iValue, 1);
					SystemSleep(10);

					if (0 == iOffset % 16)
					{ 	
						if (0 != iOffset)
						{
							printf("\n");	
						}
						printf("0x%04x: ", iOffset); 	
					}
					printf("%2x ", iValue);
				}
				printf("\n"); 
				   
				goto usedHelp;
			}

			par2 = arg.GetArg(1);
			if(!par2)
			{
				EepromRead(iOffset, &iValue, 1);
				infof("Eeprom:[%2x], %2x.\n", iOffset, iValue);			       
				goto usedHelp;
			}
			
			iValue = atoi(par2);
			EepromWrite(iOffset, (void *)&iValue, 1);
			
			return 0;
		}			
	default:
usedHelp:
		tracef("vnPro -s value:edit productid!\n");		
		tracef("vnPro -e offset value: edit e2prom(offset 0~128) value!\n");
	}
	return 0;
}
#endif

int CConsole::onPartition(int argc, char **argv)
{
    enum
    {
        MAX_PARTITIONS = 4,
    };
    
    char *pszCmd0;
    char * pArg1;
    int i,part = 0; //分区号
    int dev, type;
    int sectors;
    int percents[MAX_PARTITIONS] = {0,};    //每一个分区占硬盘总容量的百分比
    PARTITION_INFO strPartitionInfo[MAX_PARTITIONS]; //分区信息
    
    CConsoleArg arg(argc, argv);
         pszCmd0 = arg.getOptions();//获得命令选项
         pArg1  = arg.GetArg(0);

     if(!pszCmd0 ||!pArg1)//解决用户只输入net命令时，程序崩溃的问题
     {
        trace("Partition command usage:\n");
        trace("part -i     dev    : get ide [dev] partition info\n");
        trace("part -p     dev-percent0-percent1-percent2-percent3    : Partition  ide[dev] to 4 parts with [percent] of total space\n");
        trace("part -s     dev-part-type : type::0--rw, 1--ronly, 2--event, 3--redundant, 4--snap\n");
        trace("part -g    dev-part : get partition's type\n");
        trace("part -f     dev-part : format ide [dev]-[part]\n");
        return 0;
     }

    if(!g_Record.IsThreadOver())
    {
        g_Record.Stop();
    }
#ifdef LINUX    
     switch(pszCmd0[0])
     {
        case 'i':
            sscanf(pArg1, "%d", &dev);

            trace("get ide#%d partition info :\n", dev);
            //wfs_read_partitions(dev, strPartitionInfo, MAX_PARTITIONS); //fyj
            for (i = 0; i < MAX_PARTITIONS; i++)
            {
                trace("dev = %d  partition = %d  start_sector = %d  total_sector = %d\n",
                      dev, i, (int)strPartitionInfo[i].start_sector, (int)strPartitionInfo[i].total_sector);
            }
            break;
        case 'p':
	{
            sectors = 0;
            memset(strPartitionInfo, 0, sizeof(PARTITION_INFO) * MAX_PARTITIONS);
            sscanf(pArg1, "%d-%d-%d-%d-%d", &dev, &percents[0], &percents[1], &percents[2], &percents[3]);

            trace("partiton ide#%d to 4 Parts with Percents %d %d %d %d of total space\n",
                  dev, percents[0], percents[1], percents[2], percents[3]);

            IDE_INFO64 strIdeInfo;
            ide_getinfo64(&strIdeInfo);
            uint64 idecap = strIdeInfo.ide_cap[dev];

            for(i = 0; i < MAX_PARTITIONS; i++)
            {
                if(percents[i])
                {
                    strPartitionInfo[i].start_sector = sectors;
                    strPartitionInfo[i].total_sector = idecap * percents[i] / 100;
                }
                else
                {
                    strPartitionInfo[i].start_sector = 0;
                    strPartitionInfo[i].total_sector = 0;
                }
                sectors += strPartitionInfo[i].total_sector;
            }
           // wfs_write_partitions(dev, strPartitionInfo, MAX_PARTITIONS); //fyj
            break;
        }
        case 's':
            sscanf(pArg1, "%d-%d-%d", &dev, &part, &type);
            if ((type >= DRIVER_TYPE_NR) || (type < DRIVER_TYPE_BASE))
            {
                trace("SetDriverType:: type %d is error.\n", type);
                break;
            }
            g_DriverManager.SetDriverType(dev, part, type);
            g_DriverManager.IsSnapExist();

            trace("SetDriverType::dev[%d]--part[%d]--type[%d]\n", dev, part, type);
            break;
        case 'g':
            sscanf(pArg1, "%d-%d", &dev, &part);
            g_DriverManager.GetDriverType(dev, part, (uint*)&type);
            trace("GetDriverType::dev[%d]--part[%d]--type[%d]\n", dev, part, type);
            break;
        case 'f':
            sscanf(pArg1, "%d-%d", &dev, &part);
            g_DriverManager.FormatDriver(dev, part);
            trace("FormatDriver::dev[%d]--part[%d]\n", dev, part);
            break;            
        default:
            return 0;

     }
#endif    

    return 0;
}
void CConsole::setEcho(bool open)
{
#ifdef __linux__//回显
    struct termios tty;
    const int echoflags=(ECHO|ECHOE|ECHOK|ECHONL|ICANON);
    int error=0;

    error=tcgetattr(STDIN_FILENO, &tty);
    if(error==-1)
    {
        trace("can not get the attr of terminator!\n");
    }
    error=0;
    if(open)
    {
       tty.c_lflag=0;
        tty.c_lflag |= echoflags;
    }
    else
    {
        tty.c_lflag &= ~echoflags;
    }

    error=tcsetattr(STDIN_FILENO, TCSANOW, &tty);
    if(error==-1)
    {
        trace("can not set the attr of terminator!\n");
    }
#endif
}

int CConsole::OnAuth(int argc, char **argv)
{
	char *pszCmd0;
	char * pArg1;
	char* pArg2;
	int ret;

	CConsoleArg arg(argc, argv);
	pszCmd0 = arg.getOptions();//获得命令选项
	pArg1  = arg.GetArg(0);
	pArg2 = arg.GetArg(1);

	if(!pszCmd0 ||!pArg1)
	{
		trace("auth control command usage:\n");
		trace("auth -p start/stop channel: start or stop preview, channel start from 0!\n");
		trace("auth -c lp 1/0 :set bLoginPreview true or false!\n");
		return 0;
	}
	return 0;
}

