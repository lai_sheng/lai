/********************************************************************
created:	2007/04/20
created:	20:4:2007   14:59
filename: 	AppEvent.cpp

purpose:	应用程序上层事件处理
*********************************************************************/

#include "System/AppEvent.h"
#include "APIs/System.h"

PATTERN_SINGLETON_IMPLEMENT(CAppEventManager);

CAppEventManager::CAppEventManager() :
m_sigEventHandler(32),
m_mutex(MUTEX_RECURSIVE),
m_timer("EventManager"),
m_latchAllowed(FALSE)
{
	m_timer.Start(this, (VD_TIMERPROC)&CAppEventManager::onTimer, 0, 1000);
	SystemGetCurrentTime(&m_oldTime);
}

// 注册时间处理者
VD_BOOL CAppEventManager::attach(void* handler, SIG_EVENT::SigProc proc)
{
	return (m_sigEventHandler.Attach(handler, proc) >= 0);
}

// 注销时间处理者
VD_BOOL CAppEventManager::detach(void* handler, SIG_EVENT::SigProc proc)
{
	return (m_sigEventHandler.Detach(handler, proc) >= 0);
}

// 时间段检查和事件停止计数定时器
void CAppEventManager::onTimer(uint arg)
{
	CGuard guard(m_mutex);
	SYSTEM_TIME curTime;
	MAP_EVENT::iterator pi, temp;
	int i;
	int unicode;
	const EVENT_HANDLER* param;
	EVENT_LATCH latchTemp;
	VD_BOOL eraseEvent;

	SystemGetCurrentTime(&curTime);

	// 检查事件的延时时间
	for(pi = m_mapEventActive.begin(); pi != m_mapEventActive.end();)
	{
		eraseEvent = FALSE;
		for(i = 0; i < MAX_EVENT_LATCH_NUMBER; i++)
		{
			EVENT_LATCH & latch = (*pi).second.latch[i];
			if(latch.handler)
			{
				latch.second--;
				if(latch.second == 0) // 延时时间到
				{
					unicode = (*pi).first;
					param = (*pi).second.parameter;
					latchTemp = latch;
					latch.handler = NULL; // 删除该处理者
					(*pi).second.latchNumber--;
					if((*pi).second.latchNumber == 0)
					{
						if((*pi).second.inStopping)
						{
							eraseEvent = TRUE;
							temp = pi;
							temp++;
							m_mapEventActive.erase(pi); // 删除该事件
						}
					}

					// map更新后再通知事件处理者
					m_mutex.Leave();
					(((CObject*)latchTemp.handler)->*latchTemp.proc)((appEventCode)(unicode >> 8),
						(unicode & 0xff),
						appEventLatch,
						param,
						NULL);
					m_mutex.Enter();
				}
				if(eraseEvent)
				{
					break;
				}
			}
		}
		if(eraseEvent)
		{
			pi = temp;
		}
		else
		{
			pi++;
		}
	}

	// 每分钟检查一次时间段
	if(m_oldTime.year != curTime.year
		|| m_oldTime.month != curTime.month
		|| m_oldTime.day != curTime.day
		|| m_oldTime.hour != curTime.hour
		|| m_oldTime.minute != curTime.minute)
	{
		checkTimeSectionAll();
	}
	m_oldTime = curTime;
}

// 更新是否在时间段之内的标志（并不真正删除或添加事件），并根据需要通知处理者
// 只在内部使用，否则锁使用出错
void CAppEventManager::checkTimeSectionAll()
{
	MAP_EVENT::iterator pi;

	for(pi = m_mapEventActive.begin(); pi != m_mapEventActive.end(); pi++)
	{
		if ((!(*pi).second.parameter) || (!(*pi).second.workSheet))
		{
			continue;
		}

		if(checkTimeSection((*pi).second.workSheet))
		{
			if(!(*pi).second.inSection)
			{
				(*pi).second.inSection = TRUE; // must be here, for getTopEvent() from m_sigEventHandler() will use it
				m_mutex.Leave();
				m_sigEventHandler((appEventCode)((*pi).first >> 8),
					((*pi).first & 0xff),
					appEventStart,
					(*pi).second.parameter,
					NULL);
				m_mutex.Enter();
			}
		}
		else
		{
			if((*pi).second.inSection)
			{
				m_latchAllowed = FALSE; // 立即触发延时结束动作（直接调用）
				(*pi).second.inSection = FALSE; // must be here, for getTopEvent() from m_sigEventHandler() will use it
				m_mutex.Leave();
				m_sigEventHandler((appEventCode)((*pi).first >> 8),
					((*pi).first & 0xff),
					appEventStop,
					(*pi).second.parameter,
					NULL);
				m_mutex.Enter();

				//清空事件的延时处理
				(*pi).second.latchNumber = 0;
				memset((*pi).second.latch, 0, sizeof(EVENT_LATCH) * MAX_EVENT_LATCH_NUMBER);
				m_mapEventActive.erase(pi);//added by qjwang 091102
			}
		}
	}
}

// 根据事件代码得到事件名称，供GUI显示使用
const char* CAppEventManager::getEventName(appEventCode code)
{
	switch(code) {
	case appEventAlarmLocal:return "appEventAlarmLocal";
	case appEventAlarmNet:return "appEventAlarmNet";
	case appEventAlarmDecoder:return "appEventAlarmDecoder";
	case appEventAlarmManual:return "appEventAlarmManual";
	case appEventVideoMotion:return "appEventVideoMotion";
	case appEventVideoLoss:return "appEventVideoLoss";
	case appEventVideoBlind:return "appEventVideoBlind";
	case appEventVideoTitle:return "appEventVideoTitle";
	case appEventVideoSplit:return "appEventVideoSplit";
	case appEventVideoTour:return "appEventVideoTour";
	case appEventStorageNotExist:return "appEventStorageNotExist";
	case appEventStorageFailure:return "appEventStorageFailure";
	case appEventStorageLowSpace:return "appEventStorageLowSpace";
	case appEventNetAbort:return "appEventNetAbort";
	case appEventNetArp:return "appEventNetArp";
	default:return NULL;
	}
}

VD_BOOL CAppEventManager::checkTimeSection(const CONFIG_WORKSHEET *workSheet)
{
	CGuard guard(m_mutex);

	int n;

	int current, start, end;
	int week;
	SYSTEM_TIME systime;
	const TIMESECTION* timesect;

	assert(workSheet);

	// 时间段比较
	SystemGetCurrentTime(&systime);
	week = systime.wday % 7;
	timesect = workSheet->tsSchedule[week];
	current = (systime.hour * 60 + systime.minute) * 60 + systime.second;

	for (n = 0; n < N_TSECT; n++)
	{
		if(!timesect[n].enable) //未使能
		{
			continue;
		}
		start = (timesect[n].startHour * 60 + timesect[n].startMinute) * 60 + timesect[n].startSecond;  
		end = (timesect[n].endHour * 60 + timesect[n].endMinute) * 60 + timesect[n].endSecond;

		//只找选中的时间段
		if (current >= start && current < end)
		{
			break;
		}
	}

	if(n == N_TSECT) //时间段不符合
	{
		return FALSE;
	}

	return TRUE;
}

// 处理事件，调用者应该保证param内容在事件活动期间一直有效，事件处理中心会用到这个值
// 对于appEventConfig动作，这里会根据时间段和当前事件的状态转换为appEventStart或appEventStop后转发
// 对于已经start的事件再次被start时，事件的处理者需要检查其联动参数是否变化。
void CAppEventManager::notify(
							  appEventCode code,
							  int index /* = 0 */,
							  appEventAction action /* = appEventStart */,
							  const EVENT_HANDLER *param /* = NULL */,
							  const CONFIG_WORKSHEET *workSheet /* = NULL */,
							  const CConfigTable* data /* = NULL */)
{
	m_mutex.Enter();

	int unicode = (code << 8) + index;
	EVENT_ACTIVE ea;
	MAP_EVENT::iterator pi;

	if(action == appEventStart)
	{
		memset(&ea, 0, sizeof(EVENT_ACTIVE));
		ea.parameter = param;
		ea.workSheet = workSheet;
		ea.inStopping = FALSE;
		if(!param || !workSheet || checkTimeSection(workSheet))
		{
			ea.inSection = TRUE;
		}
		else
		{
			ea.inSection = FALSE;
		}
		if((pi = m_mapEventActive.find(unicode)) != m_mapEventActive.end())
		{
			if(ea.inSection)
			{
				(*pi).second = ea; // 覆盖老的参数，包括延时处理
			}
			else
			{
				(*pi).second.parameter = param; // 覆盖老的参数，不包括延时处理
				(*pi).second.workSheet = workSheet; // 覆盖老的参数，不包括延时处理
				(*pi).second.inStopping = FALSE;
				(*pi).second.inSection = FALSE;
			}
		}
		else
		{
			m_mapEventActive.insert(MAP_EVENT::value_type(unicode, ea));
		}
		if(ea.inSection)
		{
			m_mutex.Leave();
			m_sigEventHandler(code, index, action, param, data);
			return;
		}
	}
	else if(action == appEventStop)
	{
		if((pi = m_mapEventActive.find(unicode)) != m_mapEventActive.end())
		{
			(*pi).second.inStopping = TRUE;
		}
		else
		{
			m_mutex.Leave();
			return;
		}
		m_latchAllowed = TRUE;

		m_mutex.Leave();
		if ((*pi).second.inSection)
		{
			m_sigEventHandler(code, index, action, param, data);
		}
		m_mutex.Enter();
		
		// 这里删除的可能是刚添加的事件，未有效处理
		if((pi = m_mapEventActive.find(unicode)) != m_mapEventActive.end())
		{
			if((*pi).second.latchNumber == 0) // 不用延时，直接删除
			{
				m_mapEventActive.erase(pi);
			}
		}
	}
	else if(action == appEventConfig)
	{
		memset(&ea, 0, sizeof(EVENT_ACTIVE));
		ea.parameter = param;
		ea.workSheet = workSheet;
		if(param == NULL || workSheet == NULL)
		{
			m_mutex.Leave();
			return;
		}
		ea.inSection = checkTimeSection(workSheet);

		if((pi = m_mapEventActive.find(unicode)) != m_mapEventActive.end())
		{
			if (!ea.inSection)
			{
				m_latchAllowed = FALSE;
				
				m_mutex.Leave();
				m_sigEventHandler(code, index, appEventStop, param, data);
				return;
			}
			else
			{
				(*pi).second.parameter = param; // 覆盖老的参数，不包括延时处理
				(*pi).second.workSheet = workSheet; // 覆盖老的参数，不包括延时处理
				(*pi).second.inStopping = FALSE;
				(*pi).second.inSection = TRUE;

				m_mutex.Leave();
				m_sigEventHandler(code, index, appEventStart, param, data);
				return;
			}
		}
	}
	else if (action == appEventPulse)
	{
		infof("CAppEventManager-------------------appEventPulse.\n");
		m_sigEventHandler(code, index, appEventStart, param, data);
	}
	else
	{
		trace("CAppEventManager::notify invalid event code: %d\n", code);
	}
	m_mutex.Leave();
}

// 事件遍历初始化，只能在事件处理函数里面调用。
void CAppEventManager::getNextEvent()
{
	m_pi = m_mapEventActive.begin();
}

// 得到下一个事件，只能在事件处理函数里面调用，且只能在getNextEvent()之后调用。
VD_BOOL CAppEventManager::getNextEvent(void* handler, EVENT_INFO* info)
{
	int i;

	for(; m_pi != m_mapEventActive.end(); m_pi++)
	{
		if(!(*m_pi).second.inSection || (*m_pi).second.inStopping)
		{
			for(i = 0; i < MAX_EVENT_LATCH_NUMBER; i++)
			{
				if(handler == (*m_pi).second.latch[i].handler)
				{
					break;
				}
			}
			if(i == MAX_EVENT_LATCH_NUMBER)
			{
				continue;
			}
		}

		//如果在时间段之内或者该处理者发起了延时，调用回调函数检查参数
		info->code = (appEventCode)((*m_pi).first >> 8);
		info->index = ((*m_pi).first & 0xff);
		info->parameter = (*m_pi).second.parameter;
		m_pi++;
		return TRUE;
	}

	return FALSE;
}

// 如果需要对事件结束进行延时，需要在响应结束事件时调用这个函数。
void CAppEventManager::latchEvent(void* handler, SIG_EVENT::SigProc proc, appEventCode code, int index, int second)
{
	CGuard guard(m_mutex);
	int unicode = (code << 8) + index;
	MAP_EVENT::iterator pi;
	int i;

	// 事件存在且在时间段之内
	if((pi = m_mapEventActive.find(unicode)) != m_mapEventActive.end())
	{
		//如果不允许延时，直接调用回调函数
		if(!m_latchAllowed)
		{
			(((CObject*)handler)->*proc)(code,
				index,
				appEventLatch,
				(*pi).second.parameter,
				NULL);
			return;
		}

		for(i = 0; i < MAX_EVENT_LATCH_NUMBER; i++)
		{
			EVENT_LATCH & latch = (*pi).second.latch[i];

			if(latch.handler == handler) // 延时已经开启，不处理
			{
				return;
			}
		}
		for(i = 0; i < MAX_EVENT_LATCH_NUMBER; i++)
		{
			EVENT_LATCH & latch = (*pi).second.latch[i];

			if(!latch.handler)
			{
				latch.handler = handler;
				latch.proc = proc;
				latch.second = second;
				(*pi).second.latchNumber++;
				break;
			}
		}
		if(i == MAX_EVENT_LATCH_NUMBER)
		{
			trace("CAppEventManager::latchEvent failed, max latches reached.\n");
		}
	}
}

void CAppEventManager::dump()
{
	CGuard guard(m_mutex);
	MAP_EVENT::iterator pi;
	int i;

	trace("Active Application Events:\n");
	trace("Name                        Index InSection InStopping Latches\n");
	for(pi = m_mapEventActive.begin(); pi != m_mapEventActive.end(); pi++)
	{
		trace("%-26s     %2d      %d        %d      [",
			getEventName((appEventCode)((*pi).first >> 8)),
			((*pi).first & 0xff),
			(*pi).second.inSection,
			(*pi).second.inStopping);
		for(i = 0; i < MAX_EVENT_LATCH_NUMBER; i++)
		{
			trace(" %d ", (*pi).second.latch[i].second);
		}
		trace("]\n");
	}
}

