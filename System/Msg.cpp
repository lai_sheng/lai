#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <netinet/ether.h>
#include <pthread.h>
#include "System/Msg.h"

struct Msg{
	long msgtype;
	int  msgtxt;
};
void snd_msg_to_daemon(int txt)
{
	static int msqid = -1;	
	struct Msg data;
	
    if(msqid < 0)
    {
        msqid = msgget(IPCAM_MSG_KEY2, IPC_CREAT|0666);
        if(msqid <0)
        {
            printf("failed to create msq | errno MSGKEY[%d]\n", IPCAM_MSG_KEY); 
			return;
        }
    }

	memset(&data,0,sizeof(struct Msg));
	data.msgtype = 10; //这里要配合daemon改，不能随便动
	data.msgtxt  = txt;
	if(msgsnd(msqid, (void*)&data, sizeof(struct Msg) - sizeof(long), IPC_NOWAIT) == -1)  
	{  
		printf("failed to snd msg | errno MSGKEY[%d]\n", IPCAM_MSG_KEY); 	 
	} 	
}

int* rcv_msg_from_daemon()
{
	static int msqid    = -1;
	static int rcv_data = -1;
	struct Msg data;

	char i = 0;

    if(msqid < 0)
    {
        msqid = msgget(IPCAM_MSG_KEY, IPC_CREAT|0666);
        if(msqid <0)
        {
            printf("failed to create msq | errno MSGKEY[%d]\n", IPCAM_MSG_KEY); 
        }
    }

	memset(&data,0,sizeof(struct Msg));
	data.msgtype = 5; //这里要配合daemon改，不能随便动
	
	{
		if(msgrcv(msqid, (void*)&data, sizeof(struct Msg) - sizeof(long), data.msgtype, IPC_NOWAIT) > 0)
		{
			rcv_data = data.msgtxt;
		}
	}

	return &rcv_data;
}

