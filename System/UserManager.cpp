#include "System/UserManager.h"
#include "System/ZipFile.h"
#include "ez_libs/ezutil/md5.h"
#include "Configs/ConfigManager.h"
#include "System/Log.h"


// 默认组名
std::string INI_GROUP_NAME_ADMIN        = "admin";
std::string INI_GROUP_NAME_USER            = "user";

// 特殊用户,后台使用
std::string INI_DEFAULT_USER_NAME        = "root";
std::string INI_DEFAULT_USER_PWD        = "wjaipcam";

static const int LOCK_PERIOD = 5*60;        // 锁定时间
static const int TRY_LOGIN_TIMES = 5;            // 最多允许非法登陆的次数
static const int STREAGE_LENGTH = 16*1024;

CConfigTable CUserManager::sm_adminAuthorityList;
CConfigTable CUserManager::sm_userAurthorityList;
int    CUserManager::sm_versionMajor;
int    CUserManager::sm_versionMinor;
std::string CUserManager::sm_firstFilePath;
std::string CUserManager::sm_secondFilePath;
std::string CUserManager::sm_customFilePath;


static std::string GetSuperencipherment(const std::string strText)
{
    char EncodedText[9];

   DH_Superencipherment((signed char *)EncodedText, (uchar *)strText.c_str());

    EncodedText[8] = '\0';
    return EncodedText;
}

PATTERN_SINGLETON_IMPLEMENT(CUserManager);

void CUserManager::config(int versionMajor, int versionMinor, const std::string &config1, const std::string &config2, const std::string &customConfig,
                    char adminAuthList[][AUTHORITY_STRING_LEN], int adminAuthNum, 
                    char userAhthList[][AUTHORITY_STRING_LEN], int userAuthNum)

{ 
    sm_versionMajor = versionMajor;
    sm_versionMinor = versionMinor;
    sm_firstFilePath = config1;
    sm_secondFilePath = config2;
    sm_customFilePath = customConfig;
    sm_adminAuthorityList.clear();
    for (int i = 0; i < adminAuthNum; i++)
    {
        sm_adminAuthorityList.append(&adminAuthList[i][AUTHORITY_STRING_LEN]); 
    }
    sm_userAurthorityList.clear();
    for (int i = 0; i < userAuthNum; i++)
    {
        sm_userAurthorityList.append(&userAhthList[i][AUTHORITY_STRING_LEN]);
        //printf("\r\n************%s*************\r\n",userAhthList[i]);
    }
}

CUserManager::CUserManager() : m_mutex(MUTEX_RECURSIVE)
{
    m_fileConfig = NULL;
}

CUserManager::~CUserManager()
{
}

bool CUserManager::start()
{
    m_bUserLocked = false;
    m_uLockTime = 0;
    m_lockPeriod = LOCK_PERIOD;
    m_tryLoginTime = TRY_LOGIN_TIMES;

    m_stream.reserve(STREAGE_LENGTH);
    CConfigReader reader;
    bool needSetDefault = false;

    VD_BOOL bToRead2ndFile = TRUE;

    if(readConfig(sm_firstFilePath.c_str(), m_stream))
    {
        bToRead2ndFile = !reader.parse(m_stream, m_configAll);
    }
    else
    {
        trace("CUserManager::start() readConfig file1 failed\n");
    }

    if (bToRead2ndFile) 
    {
        trace("CUserManager::start() first config file parsing failed.\n");

        if(readConfig(sm_secondFilePath.c_str(), m_stream))
        {
            bToRead2ndFile = !reader.parse(m_stream, m_configAll);
        }
        else
        {
            trace("CUserManager::start() readConfig file2 failed\n");
        }    
    }

    if (bToRead2ndFile)
    {         
        trace("CConfigManager::initialize() second config file parsing failed too.\n");
        needSetDefault = true;
    }


    // 两个配置文件都非法或者配置文件版本信息不匹配，还原默认
    if (needSetDefault || !(m_configAll["Version"]["Major"] == sm_versionMajor && m_configAll["Version"]["Minor"] == sm_versionMinor))
    {
        // TODO 记录日志
        setDefault();
    }
    // 强制将管理者的权限设置为最大权限，以应对权限不断增大的情况
    m_configAll["Groups"][0u]["AuthorityList"] = sm_adminAuthorityList;

    /* admin 用户强制所有权限，避免修改ID引起的麻烦 */
    int i = 0;
    for(i = 0; i < m_configAll["Users"].size(); i++)
    {
        if (m_configAll["Users"][i]["Group"].asString() == "admin")  
        {
            m_configAll["Users"][i]["AuthorityList"] = sm_adminAuthorityList;
        }
    }
    saveFile();

    m_errorNo = eue_NoneError;
#if 0 
    CConfigNetIPFilter ipFiliter;
    // 集成白名单与黑名单
    ipFiliter.attach(this, (TCONFIG_PROC)&CUserManager::onAccessFilter);
#endif
    return true;
}

const CConfigTable& CUserManager::getAuthorityList() const
{
    return sm_adminAuthorityList;
}

bool CUserManager::addGroup(const CConfigTable &group)
{
    CGuard guard(m_mutex);
    std::string name = group["Name"].asString();

    if (name.empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }

    //增加判断是否为汉字和特殊符号，如是返回失败
    char temp[SYS_MAX_USERNAME_LEN+1] = {0};
    strncpy(temp,name.c_str(),name.length());
    for(int i=0;i<name.length();i++)
    {
        if(!((temp[i]>='0'&&temp[i]<='9') || (temp[i]>='a'&&temp[i]<='z') || (temp[i]>='A'&&temp[i]<='Z')))
        {
            m_errorNo = eue_InputNotValid;
            return false;
        }
    }

    if (findGroup(name) >= 0)
    {    
        m_errorNo = eue_IndexOverlap;
        return false;
    }

    /// 设置的权限超出管理员所具有的权限
    if (!IsChildVector(group["AuthorityList"], sm_adminAuthorityList))
    {
        m_errorNo = eue_SubsetOverlap;
        return false;
    }

    m_configAll["Groups"].append(group);

    g_Log.Append(LOG_ADD_GROUP, 0, (void *)name.c_str(), 8); //记录日志，added by billhe at 2009-6-3
    
    return true;
}

bool CUserManager::delGroup(const std::string &name)
{
    CGuard guard(m_mutex);
    CConfigTable &groups = m_configAll["Groups"];
    
    if (name.empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }
    int pos;
    
    // 尝试删除不存在的组
    if ((pos = findGroup(name)) < 0)
    {
        m_errorNo = eue_ObjectNotValid;
        return false;
    }
    if (isGroupInUse(name))
    {
        m_errorNo = eue_ObjectInUse;
        return false;
    }
    for (uint i = pos + 1; i < groups.size(); i++)
    {
        groups[i - 1] = groups[i];
    }
    groups.resize(groups.size() - 1);

    g_Log.Append(LOG_DELETE_GROUP, 0, (void *)name.c_str(), 8); //记录日志，added by billhe at 2009-6-3
    
    return true;
}

bool CUserManager::modGroup(const std::string &name, const CConfigTable &newGroup)
{
    CGuard guard(m_mutex);
    std::string newName = newGroup["Name"].asString();
    int pos;

    if (name.empty() || newName.empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }

    //用户组名已经做了修改
    //added by wyf on 090817
    if(newName != name)
    {
        //如果被修改的用户组名为admin或user     
        //added by wyf on 090826
        if(name == "admin" || name == "user") 
        {
            m_errorNo = eue_ReservedAccount;
            return false;
        }//end added by wyf on 090826
        // 用户组名已经存在，不能再修改成该用户组名
        if (findGroup(newName) >= 0)
        {    
            m_errorNo = eue_IndexOverlap;
            return false;
        }        
    }//end added by wyf on 090817
    
    if ((pos = findGroup(name)) < 0)
    {
        m_errorNo = eue_ObjectNotValid;
        return false;
    }
    // TODO group in use
    
    // 组的权限要低于admin组权限
    if (!IsChildVector(newGroup["AuthorityList"], sm_adminAuthorityList))
    {
        return false;
    }
    // 组的权限要高于该组里所有用户的权限
    for (uint i = 0; i < m_configAll["Users"].size(); i++)
    {
        //modified by wyf on 20100513
        if (m_configAll["Users"][i]["Group"].asString() == name && !IsChildVector(m_configAll["Users"][i]["AuthorityList"], newGroup["AuthorityList"]))
        {
            m_errorNo = eue_SubsetOverlap;
            return false;
        }
    }
    m_errorNo = eue_NoneError;
    m_configAll["Groups"][pos] = newGroup;

    //add by nike.xie 2009-08-17  //修改用户列表中的组名称
    CConfigTable &users = m_configAll["Users"];
    for (uint i = 0; i < users.size(); i++)
    {
        if (name == users[i]["Group"].asString())
        {
            users[i]["Group"] = newGroup["Name"];
        }
    }
    //end
    
    g_Log.Append(LOG_MODIFY_GROUP, 0, (void *)name.c_str(), 8); //记录日志，added by billhe at 2009-6-3
    
    return true;
}

bool CUserManager::getGroup(const std::string &name, CConfigTable &group)
{
    CGuard guard(m_mutex);

    if (name.empty())
    {
        m_errorNo = eue_NoneError;
        return false;
    }
    int pos = findGroup(name);

    if (pos < 0)
    {
        m_errorNo = eue_ObjectNotValid;
        return false;
    }
    group = m_configAll["Groups"][pos];
    m_errorNo = eue_NoneError;
    return true;
}

bool CUserManager::getGroupAll(CConfigTable &groups)
{
    CGuard guard(m_mutex);
    groups = m_configAll["Groups"];
    m_errorNo = eue_NoneError;
    return true;
}

bool CUserManager::addUser(const CConfigTable &curUser, PasswordEncrypt flag)
{
    CGuard guard(m_mutex);
    std::string userName = curUser["Name"].asString();
    std::string groupName = curUser["Group"].asCString();
    int pos;

    if (userName.empty() || groupName.empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }

    //增加判断是否为汉字和特殊符号，如是返回失败
    char temp[SYS_MAX_USERNAME_LEN+1] = {0};
    strncpy(temp,userName.c_str(),userName.length());
    for(int i=0;i<userName.length();i++)
    {
        if(!((temp[i]>='0'&&temp[i]<='9') || (temp[i]>='a'&&temp[i]<='z') || (temp[i]>='A'&&temp[i]<='Z')))
        {
            m_errorNo = eue_InputNotValid;
            return false;
        }
    }
    
    // 用户已经存在，不能再添加
    if (findUser(userName) >= 0)
    {
        m_errorNo = eue_ObjectNotValid;
        return false;
    }

    // 组不存在，不能添加
    if ((pos = findGroup(groupName)) < 0)
    {
        m_errorNo = eue_ObjectNotValid;
        return false;
    }
    // 检查用户所在组的权限
    if (!IsChildVector(curUser["AuthorityList"], m_configAll["Groups"][pos]["AuthorityList"]))
    {
        m_errorNo = eue_IndexOverlap;
        return false;
    }
    m_configAll["Users"].append(curUser);
    if (flag == needEncrypt)
    {
        m_configAll["Users"][m_configAll["Users"].size() - 1]["Password"] =    GetSuperencipherment(m_configAll["Users"][m_configAll["Users"].size() - 1]["Password"].asString());
    }
    m_errorNo = eue_NoneError;

    g_Log.Append(LOG_ADD_USER, 0, (void *)userName.c_str(), 8); //记录日志，added by billhe at 2009-6-3
    
    return true;
}

bool CUserManager::delUser(const std::string &name)
{
    CGuard guard(m_mutex);
    int pos;

    if (name.empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }
    if ((pos = findUser(name)) < 0)
    {
        m_errorNo = eue_ObjectNone;
        return false;
    }
    if (isUserLogined(name))
    {
        m_errorNo = eue_ObjectInUse;
        return false;
    }
    
    if (isReservedUser(name))
    {
        m_errorNo = eue_ReservedAccount;
        return false;
    }
        CConfigTable& users = m_configAll["Users"];
    int iSize = users.size();
    
    for (int i = pos + 1; i < iSize; i++)
    {
        users[i - 1] = users[i];
    }
    users.resize(iSize - 1);
    m_errorNo = eue_NoneError;

    g_Log.Append(LOG_DELETE_USER, 0, (void *)name.c_str(), 8); //记录日志，added by billhe at 2009-6-3
    
    return true;
}

bool CUserManager::modUser(const std::string &name, const CConfigTable &user, PasswordEncrypt flag)
{
    CGuard guard(m_mutex);
    CConfigTable oldUser;
    std::string newUserName = user["Name"].asString();

    if (name.empty() || user["Name"].asString().empty())
    {
        m_errorNo = eue_InputNotValid;
        return false;
    }

    //用户名已经做了修改，判断修改后的名字是否已经存在added by wyf on 090817
    if(newUserName != name)
    {
        // 用户名已经存在，不能再修改成该用户名
        if (findUser(user["Name"].asString()) >= 0)
        {
            m_errorNo = eue_IndexOverlap;
            return false;
        }
    }
    
    int pos = findUser(name);
    
    // 修改用户信息不会修改用户密码
    if (isReservedUser(name))
    {
        m_errorNo = eue_ReservedAccount;
        return false;
    }
    if (pos >= 0)
    {
        std::string passwd = m_configAll["Users"][pos]["Password"].asString();

        m_configAll["Users"][pos] = user;
        m_configAll["Users"][pos]["Password"] = passwd;
        m_errorNo = eue_NoneError;

        g_Log.Append(LOG_MODIFY_USER, 0, (void *)name.c_str(), 8); //记录日志，added by billhe at 2009-6-3
        
        return true;
    }
    m_errorNo = eue_ObjectNotValid;
    return false;
}

bool CUserManager::modPassword(const std::string &name, const std::string &passwd, PasswordEncrypt flag)
{
    CGuard guard(m_mutex);
    int pos = findUser(name);
    
    if (pos < 0)
    {
        m_errorNo = eue_ObjectInUse;
        return false;
    }

    CConfigTable& user = m_configAll["Users"][pos];

    // encode passwd
    if (flag == needEncrypt)
    {
        user["Password"] = GetSuperencipherment(passwd);
    }
    else
    {
        user["Password"] = passwd;
    }
    m_errorNo = eue_NoneError;
    
    g_Log.Append(LOG_MODIFY_PASSWORD,0,(void*)name.c_str(),8);//记录修改密码日志  aad by wyf on 090730
    saveFile();
    return true;
}

bool CUserManager::getUser(const std::string &name, CConfigTable &user)
{
    CGuard guard(m_mutex);
    CConfigTable &users = m_configAll["Users"];
    for (uint i = 0; i < users.size(); i++)
    {
        if (users[i]["Name"].asString() == name)
        {
            user = users[i];
            return true;
        }
    }
    return false;
}

bool CUserManager::getUserAll(CConfigTable &users)
{
    CGuard guard(m_mutex);

    users = m_configAll["Users"]; 
    return true;
}

bool CUserManager::setDefaultPwd()
{
    CGuard guard(m_mutex);
    CConfigTable &users = m_configAll["Users"]; 

    for (int ii=0; ii<(int)users.size(); ii++)
    {
        if (users[ii]["Name"].asString() == "admin")
            users[ii]["Password"] = GetSuperencipherment("123456");

        if (users[ii]["Name"].asString() == "guest")
            users[ii]["Password"] = GetSuperencipherment("123456");
    }

    return saveFile(); 
}

bool CUserManager::setDefault()
{
    CGuard guard(m_mutex);
    CConfigReader reader;

    // 清空现有组和用户
    m_configAll.clear();
    
    // 从默认配置读取组和用户信息
    if (readConfig(sm_customFilePath.c_str(), m_stream)
        && reader.parse(m_stream, m_configAll)
        && m_configAll["Groups"].size() >= 1
        && m_configAll["Users"].size() >= 1)
    {
        trace("CUserManager::setDefault() apply custom config.\n");
    }
    else
    {
        trace("CUserManager::setDefault() custom config file parsing failed.\n");

        m_configAll["Version"]["Major"] = sm_versionMajor;
        m_configAll["Version"]["Minor"] = sm_versionMinor;

        // 生成两个默认组
        CConfigTable group;

        group["Name"] = "admin";
        group["Memo"] = "administrator group";
        m_configAll["Groups"].append(group);

        group["Name"] = "user";
        group["Memo"] = "user group";
        m_configAll["Groups"].append(group);

        // 生成3个默认用户
        CConfigTable user;
		user["Name"] = "admin";
        user["Password"] = "123456";

        user["Group"] = "admin";
        user["Memo"] = "admin 's account";
        user["Sharable"] = true;
        m_configAll["Users"].append(user);

        user["Name"] = "guest";
        user["Password"] = "123456";
        user["Group"] = "user";
        user["Memo"] = "guest 's account";
        user["Sharable"] = true;
        m_configAll["Users"].append(user);
    }
    // 权限设置，所有管理员组的权限设置为最大权限，用户组的权限设置
    // 且规定第一个组一定是管理员组，其他组全部是用户组
    m_configAll["Groups"][(uint)0]["AuthorityList"] = sm_adminAuthorityList;
    size_t i;
    for(i = 1; i < m_configAll["Groups"].size(); i++)
    {
        m_configAll["Groups"][i]["AuthorityList"] = sm_userAurthorityList;
    }

    for(i = 0; i < m_configAll["Users"].size(); i++)
    {
        if(findGroup(m_configAll["Users"][i]["Group"].asString()) == 0) // 管理员组
        {
            m_configAll["Users"][i]["AuthorityList"] = sm_adminAuthorityList;
        }
        else // 用户组
        {
            m_configAll["Users"][i]["AuthorityList"] = sm_userAurthorityList;
        }

        // 所有默认用户都是保留用户
        m_configAll["Users"][i]["Reserved"] = true;

        // 密码加密
        m_configAll["Users"][i]["Password"] = GetSuperencipherment(m_configAll["Users"][i]["Password"].asString());
    }
    // 将配置保存到文件
    return saveFile();
}

bool CUserManager::saveFile()
{
    CGuard guard(m_mutex);
    Json::StyledWriter writer(m_stream);

    /* 正在升级，不保存配置 */
    //if (CUpgrader::instance()->getState())
    //{
    //    return false;
    //}

    m_stream = "";
    writer.write(m_configAll);
    remove(sm_secondFilePath.c_str());
    rename(sm_firstFilePath.c_str(), sm_secondFilePath.c_str());

    m_fileConfig = gzopen(sm_firstFilePath.c_str(), "wb");
    if((int)m_stream.size() != gzwrite(m_fileConfig, (char*)m_stream.c_str(), m_stream.size()))
    {
        trace("write config file failed!\n");
    }
    gzflush(m_fileConfig, Z_FINISH);
    gzclose(m_fileConfig);
    return true;
}

bool CUserManager::isReservedUser(const std::string& userName)
{
    CGuard guard(m_mutex);
    int pos;

    if ((pos = findUser(userName)) < 0)
    {
        return false;
    }
    //return m_configAll["Users"][pos]["Sharable"].asBool();//旧版代码
    return m_configAll["Users"][pos]["Reserved"].asBool();
}

bool CUserManager::isDefaultUser(const std::string& userName)
{
    return userName == INI_DEFAULT_USER_NAME;
}

bool CUserManager::isPasswdValid(const std::string &name, const std::string &passwd, PasswordEncrypt flag,  bool superPwdValid)
{
    int pos;
    std::string password;

    if((pos = findUser(name)) < 0)
    {
        return false;
    }
    password = m_configAll["Users"][pos]["Password"].asString();

    // 直接验证
    if (flag == NoEncrypt)
    {
        return password == passwd;
    }

    std::string secret = GetSuperencipherment(passwd);
    return password == secret;
}

std::vector<int> CUserManager::getActiveUserIdList()
{
    CGuard guard(m_mutex);

    std::vector<int> vec;

    UserMap::iterator pi;

    for(pi = m_activeUsers.begin(); pi != m_activeUsers.end(); pi++)
    {
        vec.push_back(pi->first);
    }

    return vec;
}

CUser* CUserManager::getActiveUser(int id)
{
    m_mutex.Enter();
    UserMap::iterator pi = m_activeUsers.find(id);
    if(pi == m_activeUsers.end())
    {
        m_mutex.Leave();
        return false;
    }

    return pi->second;
}

CUser* CUserManager::getActiveUser(const std::string &userName)
{
    m_mutex.Enter();
    UserMap::iterator pi;

    for (pi = m_activeUsers.begin(); pi != m_activeUsers.end(); pi++)
    {
        if (pi->second->getName() == userName)
        {
            return pi->second;
        }
    }
    m_mutex.Leave();
    return NULL;
}

void CUserManager::releaseActiveUser(CUser* user)
{
    m_mutex.Leave();
}

void CUserManager::onAccessFilter(CConfigNetIPFilter& config, int &ret)
{
    config.getTable(m_configAccessFilter);
}

// 检查IP地址是否有效，具体规则如下
// 如果白名单为空，那么黑名单内的将不能访问
// 否则只允许白名单内的ip地址登陆
bool CUserManager::isAllowedAddress(const std::string &address)
{
    CConfigTable netIpFilter = m_configAccessFilter[0u];

    if (netIpFilter["Enable"].asBool())
    {

        if(netIpFilter["Type"] == "Trusted" && m_configAccessFilter["Trusted"].size())
        {
            CConfigTable &addressTable = m_configAccessFilter["Trusted"];

            for (uint i = 0; i < addressTable.size(); i++)
            {
                // 白名内允许访问
                if (addressTable[i] == address) 
                {
                    return true;
                }
            }
            return false;
        }
        else if (netIpFilter["Type"] == "Banned")
        {
            CConfigTable &addressTable = m_configAccessFilter["Banned"];

            for (uint i = 0; i < addressTable.size(); i++)
            {
                // 在黑名单内属于无效
                if (addressTable[i] == address) 
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    // 没有使能，全部通过
    return true;
}

void CUserManager::setLastError(int error)
{
    m_errorNo = error;
}

int CUserManager::getLastError() const
{
    return m_errorNo;
}

bool CUserManager::getLockState(ulong& times)
{
    //infof("m_bUserLocked = %d.\n", m_bUserLocked);
    times = m_uLockTime;
    return m_bUserLocked;
}

bool CUserManager::setLockState(bool bLock)
{
    m_bUserLocked = bLock;
    m_uLockTime = time((time_t *)NULL);
    return true;
}

int CUserManager::getLockPeriod() const
{
    return m_lockPeriod;
}

int CUserManager::getTryLoginTimes() const
{
    return m_tryLoginTime;
}

bool CUserManager::addActiveUser(CUser* user)
{
    CGuard guard(m_mutex);

    int id = user->getId();
    assert(m_activeUsers.find(id) == m_activeUsers.end());

    m_activeUsers.insert(UserMap::value_type(id, user));
    return true;
}

bool CUserManager::removeActiveUser(CUser* user)
{
    CGuard guard(m_mutex);

    int id = user->getId();
    assert(m_activeUsers.find(id) != m_activeUsers.end());

    m_activeUsers.erase(id);
    return true;
}

bool CUserManager::isUserLogined(const std::string& userName)
{
    CGuard guard(m_mutex);

    UserMap::iterator pi;

    for(pi = m_activeUsers.begin(); pi != m_activeUsers.end(); pi++)
    {
        if(pi->second->getName() == userName)
        {
            return true;
        }
    }

    return false;
}

bool CUserManager::isUserSharable(const std::string& userName)
{
    CGuard guard(m_mutex);
    int pos = findUser(userName);

    if (pos >= 0)
    {
        CConfigTable &user = m_configAll["Users"][pos];

        return user["Sharable"].asBool();
    }
    return false;
}

bool CUserManager::isGroupInUse(const std::string &userName)
{
    CGuard guard(m_mutex);
    CConfigTable& users = m_configAll["Users"];

    for (uint i = 0; i < users.size(); i++)
    {
        if (userName == users[i]["Group"].asString())
        {
            return true;
        }
    }
    return false;
}

bool CUserManager::hasAuthorityOf(const std::string &userName, const char* auth)
{
    CGuard guard(m_mutex);	
    int pos = findUser(userName);
    if (pos >= 0)
    {
        CConfigTable &authList = m_configAll["Users"][pos]["AuthorityList"];

        for (uint i = 0; i < authList.size(); i++)
        {
            if (authList[i].asString() == auth)
            {
                return true;
            }
        }
    }
    return false;
}

int CUserManager::findUser(const std::string &userName)
{
    CConfigTable &users = m_configAll["Users"];

    for (uint i = 0; i < users.size(); i++)
    {
        if (userName == users[i]["Name"].asString())
        {
            return i;
        }
    }
    return -1;
}

int CUserManager::findGroup(const std::string &groupName)
{
    CGuard guard(m_mutex);
    CConfigTable &groups = m_configAll["Groups"];

    for (uint i = 0; i < groups.size(); i++)
    {
        if (groupName == groups[i]["Name"].asString())
        {
            return i;
        }
    }
    return -1;
}

VD_BOOL CUserManager::readConfig(const char* chPath, std::string& input)
{

    printf("CUserManager::readConfig()\r\n");

    m_fileConfig = gzopen(chPath, "rb");
    if(!m_fileConfig)
        return FALSE;

    const int size = 32*1024;
    char* buf = new char[size + 1];

    input = "";

    while (1)
    {
        int nLen =     gzread(m_fileConfig, buf, size);
        if(nLen <=0 )
            break;
        buf[nLen] = 0;
        input += buf;
    }
    input += '\0';
    gzclose(m_fileConfig);
    //input = buf;
    delete []buf;

    return TRUE;
}


bool CUserManager::IsChildVector(const CConfigTable &ChildVec, const CConfigTable &Vec)
{
    uint i, j;

    for (i = 0; i < ChildVec.size(); i++)
    {
        for (j = 0; j < Vec.size(); j++)
        {
            if(ChildVec[i] == Vec[j])
            {
                break;
            }
        }
        printf("authority:%s not exist!\n", ChildVec[i].asCString());
        if(j == Vec.size())
        {
            return false;
        }
    }
    return true;
}

bool CUserManager::eraseAuthority(CConfigTable &table, const std::string &authority)
{
    uint i;

    for (i = 0; i < table.size(); i++)
    {
        if (table[i].asString() == authority)
        {
            break;
        }
    }
    if (i < table.size())
    {
        while (i < table.size() - 1)
        {
            table[i] = table[i + 1];
        }
        table.resize(table.size() - 1);
        return true;
    }
    return false;
}
