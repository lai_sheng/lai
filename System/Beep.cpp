
#include "APIs/System.h"
#include "System/Beep.h"
#include "MultiTask/Thread.h"

PATTERN_SINGLETON_IMPLEMENT(CBeep);

CBeep::CBeep()
{
}

CBeep::~CBeep()
{

}

void CBeep::CallBeep(uint frequence /* = 900 */, uint duration /* = 50 */)
{
	m_threadlet.run("Beep", this, (ASYNPROC)&CBeep::beep, (frequence << 16) + duration, 0);
}

void CBeep::beep(uint param)
{
	SystemBeep((param >> 16), (param & 0xffff));
}
