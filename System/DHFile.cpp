
#include <string.h>

#include "System/DHFile.h"
#include "System/BaseTypedef.h"

#define STM_BUFFER_SIZE (32 * 1024)
//#define NONE_DISK//没有硬盘时打开这个宏可以使录像模块正常工作。

CMutex CDHFile::m_Mutex;
CMutex CDHFile::m_SearchMutex;

CDHFile::CDHFile()
{
	m_hFile = 0;
	m_pBuffer = NULL;
}

CDHFile::~CDHFile()
{
	if(m_hFile)
	{
		Close();
	}
}

VD_BOOL CDHFile::Create(STM_ATT *att)
{
#ifdef NONE_DISK
	return TRUE;
#endif
	uint ret;
	DHTIME dhtime;
	CGuard guard(m_Mutex);

	timesys2dh(&dhtime, &att->time);

	ret = wfs_file_create(att->chan, att->type, att->alrm, att->imgq, dhtime, att->redu, att->driver_type, &m_hFile);
	if(ret){
		tracepoint();
		return FALSE;
	}
//-----------------------------------------------------------------------
// 缓冲管理[4/25/2005]
//-----------------------------------------------------------------------
	m_pPacket = g_PacketManager.GetPacket(STM_BUFFER_SIZE);
	if(!m_pPacket){
		m_pBuffer = NULL;
		wfs_file_close(m_hFile);
		return FALSE;
	}
	m_pBuffer = m_pPacket->GetBuffer();
	assert(m_pBuffer);
//-----------------------------------------------------------------------
	m_Position = 0;
	m_PositionBufferStart = 0;
	m_dwLength = 0;
	m_dwMode = modeWrite;

	m_Attr = *att;

	return TRUE;
}

VD_BOOL CDHFile::Open()
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_open(&m_hFile, FILE_READ_SINGLE, FALSE);//fs will not close system read handle
	if(ret)
	{
		m_hFile = 0;
		return FALSE;
	}
//-----------------------------------------------------------------------
// buffer manage [4/25/2005]
//-----------------------------------------------------------------------
	m_pPacket = g_PacketManager.GetPacket(STM_BUFFER_SIZE);
	if(!m_pPacket){
		m_pBuffer = NULL;
/*-----------------------------------------------------------------------
	如果缓冲没有取到，则文件打开也不成功，并关闭该文件；
-----------------------------------------------------------------------*/
		ret = wfs_file_close(m_hFile);
		if(ret)
		{
			tracepoint();
		}
		m_hFile = 0;
		return FALSE;
	}
	m_pBuffer = m_pPacket->GetBuffer();
//-----------------------------------------------------------------------
	m_Position = 0;
	m_PositionBufferStart = DHFILE_INAVLID_POSITION;//无效偏移,表示缓冲无效
	m_dwLength = 0;
	m_dwLengthLimited = 0;
	m_dwMode = modeRead;
	
	return TRUE;
}

VD_BOOL CDHFile::Close()
{
#ifdef NONE_DISK
	return TRUE;
#endif
	uint ret;

	CGuard guard(m_Mutex);

	if (!m_pBuffer)
	{
		tracepoint();
		return 0;
	}
	_Flush();
	ret = wfs_file_close(m_hFile);
	m_hFile = 0;
//-----------------------------------------------------------------------
// buffer manage [4/25/2005]
//-----------------------------------------------------------------------
	if(m_pPacket)
	{
		m_pPacket->Release();
		m_pPacket = NULL;
	}
//-----------------------------------------------------------------------
	if(ret)
	{
		tracepoint();
		return FALSE;
	}
	return TRUE;
}

uint CDHFile::Read(void *pBuffer, uint dwCount)
{
	CGuard guard(m_Mutex);
//-----------------------------------------------------------------------
// buffer manage[4/25/2005]
//-----------------------------------------------------------------------
	uint used, exceed, curpos, dwRead = dwCount;
	uint length;
	DHTIME stime, etime;

	if (m_pBuffer == NULL)
	{
		return 0;
	}
	if(m_dwLengthLimited && m_Position >= m_dwLengthLimited)
	{
		return 0;
	}
	//!限制录像长度更准确
	if(m_dwLengthLimited&&m_Position+dwCount>=m_dwLengthLimited)
	{
		dwCount=m_dwLengthLimited-m_Position;
		dwRead = dwCount;
	}
	//trace("CDHFile::Read() write %d bytes\n", dwCount);

	if(m_Position + dwCount >= m_dwLength)
	{
		if(wfs_get_file_info2(m_hFile,&stime,&etime, &length))
		{
			return 0;
		}

		m_dwLength = length * 1024;
	}

	//当读出的数据长度大于缓冲长度的一半时， 不使用缓冲， 以提高效率
	if(dwCount > STM_BUFFER_SIZE / 2 && m_Position % IDE_SECTOR_SIZE == 0 && dwCount % IDE_SECTOR_SIZE == 0)
	{
		if(m_Position >= m_dwLength) // 文件已经读完
		{
			return 0;
		}
		else if(m_Position + dwCount >= m_dwLength)
		{
			dwCount = m_dwLength - m_Position;
		}

		if(wfs_get_file_pos(m_hFile, &curpos))
		{
			return 0;
		}

		curpos *= IDE_SECTOR_SIZE;

		if(m_Position != curpos)
		{
			if(wfs_file_moveto_len(m_hFile, m_Position / IDE_SECTOR_SIZE))
			{
				return 0;
			}
		}

		if (wfs_file_read(m_hFile, (uchar*)pBuffer, dwCount / IDE_SECTOR_SIZE))
		{
			//tracepoint();
			return 0;
		}

		m_Position += dwCount;
		m_PositionBufferStart = DHFILE_INAVLID_POSITION;

		return dwCount;
	}

	//缓冲无效,需要重新定位和读取
	//m_Position == m_PositionBufferStart + STM_BUFFER_SIZE的情况read_again有处理,以省去wfs_file_moveto_len操作
	if (m_Position < m_PositionBufferStart || m_Position > m_PositionBufferStart + STM_BUFFER_SIZE)
	{
		//trace("CDHFile::Read() buffer check\n");
		if(wfs_file_moveto_len(m_hFile, m_Position / IDE_SECTOR_SIZE))
		{
			return 0;
		}
		if(wfs_get_file_pos(m_hFile, &m_PositionBufferStart))
		{
			return 0;
		}
		m_PositionBufferStart *= IDE_SECTOR_SIZE;
		if (wfs_file_read(m_hFile, m_pBuffer, STM_BUFFER_SIZE / IDE_SECTOR_SIZE))
		{
			return 0;
		}
	}

read_again:

	//检查是否已经超出了文件总长度
	if(m_PositionBufferStart + STM_BUFFER_SIZE < m_dwLength)
	{
		exceed = 0;
	}
	else
	{
		exceed = m_PositionBufferStart + STM_BUFFER_SIZE - m_dwLength;
	}

	//数据拷贝
	//trace("CDHFile::Read() data copy\n");
	used = m_Position - m_PositionBufferStart;
	if (STM_BUFFER_SIZE - used - exceed >= dwCount)
	{
		memcpy(pBuffer, m_pBuffer + used, dwCount);
		m_Position += dwCount;
		return dwRead;				// 该函数执行到这里表示已经成功读取所要读的数据
	}
	memcpy(pBuffer, m_pBuffer + used, STM_BUFFER_SIZE - used - exceed);
	if(exceed)//文件已经读完
	{
		//trace("CDHFile::Read() exceed\n");
		m_Position += STM_BUFFER_SIZE - used - exceed;
		return STM_BUFFER_SIZE - used - exceed;
	}
	pBuffer = (uchar*)pBuffer + STM_BUFFER_SIZE - used;
	dwCount -= STM_BUFFER_SIZE - used;

	//读出文件
	//trace("CDHFile::Read() read from disk\n");
	if (wfs_file_read(m_hFile, m_pBuffer, STM_BUFFER_SIZE / IDE_SECTOR_SIZE))
	{
		//trace("CDHFile::Read() over\n");
		return dwRead - dwCount;	// 返回实际读的字节数
	}
	m_PositionBufferStart += STM_BUFFER_SIZE;
	m_Position = m_PositionBufferStart;
	goto read_again;
}

#ifdef RECORD_NET_THREAD_MULTI 
uint CDHFile::Write_no_mutex(void *pBuffer, uint dwCount, VD_BOOL bPacket)
{
#else
uint CDHFile::Write(void *pBuffer, uint dwCount, VD_BOOL bPacket)
{
	CGuard guard(m_Mutex);
#endif
#ifdef NONE_DISK
	return dwCount;
#endif
//-----------------------------------------------------------------------
// buffer manage[4/25/2005]
//-----------------------------------------------------------------------
	uint used, dwWritten = dwCount;

	//trace("CDHFile::Write() write %d bytes\n", dwCount);

	if ((!m_pBuffer) || (dwCount == 0))
	{
		trace("line <%d> dwCount <%d>\n",__LINE__, dwCount);
		return 0;
	}

	//文件长度最大4G,长度计数溢出时,强行打包文件
	if (m_Position + dwCount < m_Position)
	{
		trace("CDHFile::Write() length overflowed %u, pack automatically.\n", m_Position);
		_Flush();
		if(wfs_file_break(m_hFile))
		{
			return 0;
		}
		m_Position = 0;
		m_PositionBufferStart = 0;
		m_dwLength = 0;
	}

	//缓冲无效,需要重新定位和读取
	//m_Position == m_PositionBufferStart + STM_BUFFER_SIZE的情况read_again有处理,以省去wfs_file_moveto_len操作
	if (m_Position < m_PositionBufferStart || m_Position > m_PositionBufferStart + STM_BUFFER_SIZE)
	{
		//trace("CDHFile::Write() buffer check\n");
		if(wfs_file_moveto_len(m_hFile, m_Position / IDE_SECTOR_SIZE))
		{
			//tracepoint();
			return 0;
		}
		if(wfs_get_file_pos(m_hFile, &m_PositionBufferStart))
		{
			//tracepoint();
			return 0;
		}
		m_PositionBufferStart *= IDE_SECTOR_SIZE;
	
		if(m_PositionBufferStart < m_dwLength)
		{
			//定位引起的缓冲无效,需要重读数据
			//trace("CDHFile::Write() read old data\n");
			if (wfs_file_read(m_hFile, m_pBuffer, STM_BUFFER_SIZE / IDE_SECTOR_SIZE))
			{
				//tracepoint();
				return 0;
			}
			if(wfs_file_moveto_len(m_hFile, m_PositionBufferStart / IDE_SECTOR_SIZE))
			{
				//tracepoint();
				return 0;
			}
		}
	}

write_again:

	//写入的数据长度大于缓冲长度的一半时， 不使用缓冲， 以提高效率
	//还要求缓冲地址pBuffer也是对齐的，本来对齐的pBuffer传入后可能被修改而不对齐了。
	if(((dwCount > 0 && m_dwLength == 0) || dwCount > STM_BUFFER_SIZE / 2)
		&& m_Position % IDE_SECTOR_SIZE == 0
		&& dwCount % IDE_SECTOR_SIZE == 0
		&& (int)pBuffer % IDE_SECTOR_SIZE == 0)
	{
		_Flush();
		if (wfs_file_write(m_hFile, (uchar*)pBuffer, dwCount / IDE_SECTOR_SIZE))
		{
			//tracepoint();
			return 0;
		}

		//更新实际长度,自动打包等操作会引起文件指针不同步
		if(wfs_get_file_pos(m_hFile, &m_PositionBufferStart))
		{
			//tracepoint();
			return 0;
		}
		m_PositionBufferStart *= IDE_SECTOR_SIZE;

		//超出了实际长度
		if(m_PositionBufferStart < m_Position)
		{
			//		trace("CDHFile::Write() position skew %d %d, synchronize automatically.\n", m_PositionBufferStart, m_Position);
			m_dwLength = m_PositionBufferStart;
		}
		m_Position = m_PositionBufferStart;

		if(m_dwLength < m_Position)//更新文件总长度
		{
			m_dwLength = m_Position;
		}

		return dwWritten;
	}

	//数据拷贝
	//trace("CDHFile::Write() data copy\n");
	used = m_Position - m_PositionBufferStart;
	if (STM_BUFFER_SIZE - used > dwCount)
	{
		memcpy(m_pBuffer + used, pBuffer, dwCount);
		m_Position += dwCount;
		if(m_dwLength < m_Position)//更新文件总长度
		{
			m_dwLength = m_Position;
		}
		return dwWritten;
	}
	memcpy(m_pBuffer + used, pBuffer, STM_BUFFER_SIZE - used);
	pBuffer = (uchar*)pBuffer + STM_BUFFER_SIZE - used;
	dwCount -= STM_BUFFER_SIZE - used;

	//写入文件
	//trace("CDHFile::Write() write to disk\n");
	if (wfs_file_write(m_hFile, m_pBuffer, STM_BUFFER_SIZE / IDE_SECTOR_SIZE))
	{
		//tracepoint();
		return 0;
	}

	//更新实际长度,自动打包等操作会引起文件指针不同步
	if(wfs_get_file_pos(m_hFile, &m_PositionBufferStart))
	{
		//tracepoint();
		return 0;
	}
	m_PositionBufferStart *= IDE_SECTOR_SIZE;

	//超出了实际长度
	if(m_PositionBufferStart < m_Position)
	{
//		trace("CDHFile::Write() position skew %d %d, synchronize automatically.\n", m_PositionBufferStart, m_Position);
		m_dwLength = m_PositionBufferStart;
	}
	m_Position = m_PositionBufferStart;

	//每覆盖一块旧的数据时,先读出旧的数据到缓冲
	if(m_Position < m_dwLength)
	{
		//trace("CDHFile::Write() read old data\n");
		if (wfs_file_read(m_hFile, m_pBuffer, STM_BUFFER_SIZE / IDE_SECTOR_SIZE))
		{
			//tracepoint();
			return 0;
		}
		if(wfs_file_moveto_len(m_hFile, m_PositionBufferStart / IDE_SECTOR_SIZE))
		{
			//tracepoint();
			return 0;
		}
	}
	goto write_again;
}

#ifdef RECORD_NET_THREAD_MULTI 

VD_BOOL CDHFile::WriteGroup(PACKET_GROUP_RECORD* pPacketGroup)
{
	CGuard guard(m_Mutex);
	assert(pPacketGroup->count <= PACKET_GROUP_MAX_COUNT);
	uint ret;
	VD_BOOL result = VD_TRUE;
	int i;
	for( i = 0; i <pPacketGroup->count; i++)
	{
		PACKET_RECORD* pRecord = &pPacketGroup->pPacketRecord[i];
		ret = Write_no_mutex(pRecord->pPacket->GetBuffer()+pRecord->dwStartPos,
				pRecord->dwCount);
		if(ret != pRecord->dwCount)
		{
			result =VD_FALSE;
			break;
		}
	}

	for( i = 0; i <pPacketGroup->count; i++)
	{
		PACKET_RECORD* pRecord = &pPacketGroup->pPacketRecord[i];
		pRecord->pPacket->Release();
	}

	return result;

}

uint CDHFile::Write(void *pBuffer, uint dwCount, VD_BOOL bPacket)
{
	CGuard guard(m_Mutex);
	return Write_no_mutex(pBuffer, dwCount, bPacket);
}
#endif

VD_BOOL CDHFile::Write(CPacket *pPacket)
{
	//CGuard guard(m_Mutex);
	VD_BOOL ret;
	if(!pPacket)
	{
		return FALSE;
	}
	
	//m_nWritePacket++;
	ret = (Write(pPacket->GetBuffer(), pPacket->GetLength(), FALSE) == pPacket->GetLength());
	/*if(g_DHFileManager.SendMessage(XM_DHFILE_WRITE, (PARAM)this, (PARAM)pPacket))
	{
		return TRUE;
	}*/
	//m_nWritePacket--;
	pPacket->Release();

	return ret;
}

VD_BOOL CDHFile::Flush()
{
	CGuard guard(m_Mutex);
	return _Flush();
}

/*-----------------------------------------------------------------------
	等写缓冲里的数据都写入到硬盘
-----------------------------------------------------------------------*/
VD_BOOL CDHFile::_Flush()
{
	uint dwSections;
	uint dwRoundup;
	
	//trace("CDHFile::_Flush()\n");
	if(m_dwMode != modeWrite)
	{
		return TRUE;
	}
	if (m_Position < m_PositionBufferStart || m_Position > m_PositionBufferStart + STM_BUFFER_SIZE)
	{
		//trace("CDHFile::_Flush invalid file position\n");
		return FALSE;
	}
	dwSections = (m_Position - m_PositionBufferStart + IDE_SECTOR_SIZE - 1) / IDE_SECTOR_SIZE;

	if((dwRoundup = (m_Position - m_PositionBufferStart) % IDE_SECTOR_SIZE)) //一个扇区对齐
	{
		dwRoundup = IDE_SECTOR_SIZE - dwRoundup;
	}
	memset(m_pBuffer + m_Position - m_PositionBufferStart, 0xff, dwRoundup); //补齐的数据全部填成0xFF

	if(dwSections)
	{
		if (wfs_file_write(m_hFile, m_pBuffer, dwSections))
		{
			//tracepoint();
			return FALSE;
		}
		m_PositionBufferStart = DHFILE_INAVLID_POSITION;
	}

	//睿威文件系统的文件长度以K为单位, 所以写入时需要文件长度1K对齐,
	//即两个扇区对齐, 如果IDE_SECTOR_SIZE为512字节.
	if(((m_Position + dwRoundup) / IDE_SECTOR_SIZE) % 2)
	{
		memset(m_pBuffer, 0xff, IDE_SECTOR_SIZE); //补齐的数据全部填成0xFF
		if (wfs_file_write(m_hFile, m_pBuffer, 1))
		{
			//tracepoint();
			return FALSE;
		}
	}

	return TRUE;
}

/*-----------------------------------------------------------------------
	更新读结构信息
-----------------------------------------------------------------------*/
VD_BOOL CDHFile::_Update()
{
	uint length;
	DHTIME stime, etime;

	//trace("CDHFile::_Update()\n");
	if(m_dwMode == modeRead)
	{
		if(wfs_get_file_info2(m_hFile,&stime,&etime, &length))
		{
			return FALSE;
		}
		m_dwLength = length * 1024;
	}
	
	if(wfs_get_file_pos(m_hFile, &m_Position))
	{
		return FALSE;
	}
	
	m_PositionBufferStart = DHFILE_INAVLID_POSITION;
	m_Position *= IDE_SECTOR_SIZE;
	return TRUE;
}

VD_BOOL CDHFile::Pack()
{
#ifdef NONE_DISK
	return TRUE;
#endif
	return OnPack();
	//return g_DHFileManager.SendMessage(XM_DHFILE_PACK, (PARAM)this, 0);
}

VD_BOOL CDHFile::OnPack()
{
	uint ret;
	CGuard guard(m_Mutex);

	if (!m_pBuffer)
	{
		tracepoint();
		return 0;
	}
	_Flush();
	ret = wfs_file_break(m_hFile);
	if(ret)
		return FALSE;
	m_Position = 0;
	m_PositionBufferStart = 0;
	m_dwLength = 0;
	return TRUE;
}

VD_BOOL CDHFile::SeekTime(int lOff, uint nFrom)
{
	uint ret;
	uint length;
	DHTIME stime, etime;

	CGuard guard(m_Mutex);

	if(wfs_get_file_info2(m_hFile,&stime,&etime, &length))
	{
		return FALSE;
	}

	switch(nFrom) {
	case begin:
		ret = wfs_file_moveto_time(m_hFile, add_time(stime, lOff));
		break;
	case end:
		ret = wfs_file_moveto_time(m_hFile, add_time(etime, lOff));
		break;
	case current:
		if(lOff >= 0)
		{
			ret = wfs_file_move_second(m_hFile, TRUE, lOff);
		}
		else
		{
			ret = wfs_file_move_second(m_hFile, FALSE, -lOff);
		}
		break;
	default:
		return FALSE;
	}

	return(!ret && _Update());
}

VD_BOOL CDHFile::SeekDHTime(SYSTEM_TIME * psSysTime)
{
	uint ret;
	DHTIME dhtime;
	CGuard guard(m_Mutex);
	timesys2dh(&dhtime, psSysTime);
	ret = wfs_file_moveto_time(m_hFile, dhtime);
	return(!ret && _Update());
}

//有且仅有此定位函数对读写结构都有用
int CDHFile::Seek(uint lOff, uint nFrom)
{
	uint ret;
	uint position;

	CGuard guard(m_Mutex);

	switch(nFrom) {
	case begin:
		position = lOff;
		break;
	case end:
		position = m_dwLength + lOff;
		break;
	case current:
		position = m_Position + lOff;
		break;
	default:
		return -1;
	}
	if(position == m_Position)
	{
		return m_Position;
	}
	if(position < 0 || position > m_dwLength)
	{
		return -1;
	}
	if(position < m_PositionBufferStart || position >= m_PositionBufferStart + STM_BUFFER_SIZE)
	{
		_Flush();
		ret = wfs_file_moveto_len(m_hFile, position / IDE_SECTOR_SIZE);
		if(ret)
		{
			return -1;
		}
		m_PositionBufferStart = DHFILE_INAVLID_POSITION;
	}
	m_Position = position;
	return m_Position;
}

//使用结束时间来限制文件的可读长度, 需要紧跟在Locate后调用. 限制长度一经设定, 
//就一直存在, 需要将结束时间指针设为NULL再次调用此接口来取消, 或者设置为其他值.
VD_BOOL CDHFile::LimitTime(SYSTEM_TIME *end_time)
{
	DHTIME et;
	uint ret;

	CGuard guard(m_Mutex);

	//seek to end_time and get limited position
	if(end_time)
	{
		timesys2dh(&et, end_time);
		if((ret = wfs_file_moveto_time(m_hFile, et)))
		{
			return FALSE;
		}
		if((ret = wfs_get_file_pos(m_hFile, &m_dwLengthLimited)))
		{
			return FALSE;
		}
		m_dwLengthLimited *= IDE_SECTOR_SIZE;
       
	}
	else
	{
		m_dwLengthLimited = 0;
	}

	//recover old postion
	ret = wfs_file_moveto_len(m_hFile, m_Position / IDE_SECTOR_SIZE);

	return(!ret && _Update());
}

//! 文件类型和驱动器类型均使用掩码表示
VD_BOOL CDHFile::Locate(int chan, SYSTEM_TIME *start_time, uint type /* = ANY_FILE_TYPE */, uint driver_type /* = NORMAL_DRIVER_TYPE */)
{
	uint ret;
	DHTIME st;
	
	CGuard search_guard(m_SearchMutex);
	CGuard guard(m_Mutex);

	timesys2dh(&st, start_time);
	ret = wfs_file_goto(m_hFile, chan, st, type, driver_type);
	if(ret)
		return FALSE;
	ret = wfs_file_moveto_time(m_hFile, st);
	return(!ret && _Update());
}

VD_BOOL CDHFile::Locate(FILE_INFO *lst)
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_goto_list(m_hFile, lst);
	return(!ret && _Update());
}

VD_BOOL CDHFile::Locate(uint dwDisk, uint dwPart,uint dwClus)
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_goto_clus(m_hFile, dwDisk, dwPart, dwClus);

	return(!ret && _Update());
}
VD_BOOL CDHFile::Locate(uint dwDisk, uint dwClus)
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_goto_clus(m_hFile, dwDisk, 0, dwClus);

	return(!ret && _Update());
}

VD_BOOL CDHFile::Normalize(FILE_INFO *pinfo, DHTIME start_time, DHTIME end_time)
{
	uint ret;
	CGuard guard(m_Mutex);
	uint start_pos, end_pos;

	start_pos = 0;
	end_pos = pinfo->file_length;

	ret = wfs_file_goto_list(m_hFile, pinfo);
	if(ret || !_Update())
		return FALSE;

	if(compare_time(pinfo->start_time, start_time)<0)
	{
		if(wfs_file_moveto_time(m_hFile, start_time))
		{
			return FALSE;
		}
		if(wfs_get_file_pos(m_hFile, &start_pos))
		{
			return FALSE;
		}
		pinfo->start_time = start_time;
		start_pos /= 2;//扇区->KB
	}
	if(compare_time(pinfo->end_time, end_time)>0)
	{
		if(wfs_file_moveto_time(m_hFile, end_time))
		{
			return FALSE;
		}
		if(wfs_get_file_pos(m_hFile, &end_pos))
		{
			return FALSE;
		}
		pinfo->end_time = end_time;
		end_pos /= 2;//扇区->KB
	}
	pinfo->file_length = end_pos - start_pos;
	return TRUE;
}

VD_BOOL CDHFile::GotoChannel(int chan)
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_sync(m_hFile,chan,m_hFile);
	return(!ret && _Update());
}

VD_BOOL CDHFile::NextFile(VD_BOOL bLoop /* = TRUE */, uint type /* = ANY_FILE_TYPE */, uint driver_type /* = NORMAL_DRIVER_TYPE */)
{
	uint ret;

	CGuard search_guard(m_SearchMutex);
	CGuard guard(m_Mutex);

	ret = wfs_file_goto_next(m_hFile, bLoop, type, driver_type);
	return(!ret && _Update());
}

VD_BOOL CDHFile::PrevFile(VD_BOOL bLoop /* = TRUE */, uint type /* = ANY_FILE_TYPE */, uint driver_type /* = NORMAL_DRIVER_TYPE */)
{
	uint ret;

	CGuard search_guard(m_SearchMutex);
	CGuard guard(m_Mutex);

	ret = wfs_file_goto_prev(m_hFile, bLoop, type, driver_type);
	return(!ret && _Update());
}

VD_BOOL CDHFile::GetLastFile(int chan, uint type /* = ANY_FILE_TYPE */, uint driver_type /* = NORMAL_DRIVER_TYPE */)
{
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_file_goto_chan_last(m_hFile, (uchar)chan, type, driver_type);
	return(!ret && _Update());
}

int file_sys_getlist(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, uint type, uint *num, FILE_INFO *info, uint hint /* = 0 */, uint driver_type);

//! 文件类型和驱动器类型均使用掩码表示
VD_BOOL CDHFile::GetList(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, uint type, uint *num, FILE_INFO *info, uint hint /* = 0 */, uint driver_type)
{
	uint ret;
	DHTIME st, et;
    
	CGuard search_guard(m_SearchMutex);
	CGuard guard(m_Mutex);

	timesys2dh(&st, start_time);
	timesys2dh(&et, end_time);	
	ret  = 0;
	*num = 2;
	file_sys_getlist(chan, start_time, end_time, type,num, info,  hint, driver_type);
	//printf("=======ret = [%d %d  %d]\n", ret, *num, __LINE__);
	if(ret)
	{	
	    return FALSE;
	}
	return TRUE;

}

VD_BOOL CDHFile::GetAttr(STM_ATT *att)
{
	*att = m_Attr;
	return TRUE;
}

VD_BOOL CDHFile::GetInfo(STM_INFO *info)
{
	DHTIME stime, etime;
	uint ret1,ret2;
	CGuard guard(m_Mutex);
	ret1 = wfs_get_file_info(m_hFile,&info->chan,&info->alarm,&info->image,&info->av);
	ret2 = wfs_get_file_info2(m_hFile,&stime,&etime,&info->len);
	if(ret1||ret2)
		return FALSE;
	timedh2sys(&info->stime,&stime);
	timedh2sys(&info->etime,&etime);
	wfs_get_clus_size(m_hFile, &info->csize);
	info->csize >>= 1;
	return TRUE;
}

uint CDHFile::GetPosition()
{
	CGuard guard(m_Mutex);
	return m_Position;
}

VD_BOOL CDHFile::GetCurTime(SYSTEM_TIME *time)
{
	DHTIME dhtime;
	uint ms;
	uint curr_offset;
	uint clus_size;
	uint ret;
	CGuard guard(m_Mutex);
	ret = wfs_get_cur_time(m_hFile,&dhtime,&ms,&curr_offset);
	wfs_get_clus_size(m_hFile, &clus_size);
	if(ret)
		return FALSE;
	dhtime = add_time(dhtime,ms*curr_offset/clus_size/1000);
	timedh2sys(time, &dhtime);
	return TRUE;
}

VD_BOOL CDHFile::IsOpened()
{
	CGuard guard(m_Mutex);
	if (m_hFile != 0)
	{
		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDHFile::SetFileHoldTime(int second)
{
	CGuard guard(m_Mutex);
	wfs_file_set_hold_time(second);
	return TRUE;
}


