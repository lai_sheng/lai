
#include "AuxLibs/Lua/ScriptEngine.h"

ScriptEngine *ScriptEngine::_instance = NULL;

ScriptEngine::ScriptEngine()
	:LuaWrap(lua_open())
{
}

ScriptEngine::~ScriptEngine()
{
}

ScriptEngine * ScriptEngine::Instance()
{
	if(_instance == NULL) {
		_instance = new ScriptEngine();
	}
	return _instance;
}


//
// End of "$Id: ScriptEngine.cpp 1883 2006-02-13 03:02:27Z wanghw $"
//
