

#include "System/BigNumber.h"

CBigNumber::CBigNumber(unsigned long long n)
{
	len = 0;
	memset(a, 0, sizeof(a));
	while (n) {
		a[len++] = n % (1 + CBigNumber::MAXN);
		n /= (1 + CBigNumber::MAXN);
	}
	if (len == 0) len = 1;
}

CBigNumber::CBigNumber(const char s[])
{
	int j, m, n;

	len = 0;
	memset(a, 0, sizeof(a));
	for (n = strlen(s) - 1; n >= 0; n -= 4) {
		m = 0;
		if ((j = n - 3) < 0) j = 0;
		while (j <= n) m = m * 10 + s[j++] - '0';
		a[len++] = m;
	}
}
CBigNumber::CBigNumber(const CBigNumber &n)
{
	*this = n;
}

CBigNumber &CBigNumber::operator=(const CBigNumber &n)
{
	int i;

	len = n.len;
	memset(a, 0, sizeof(a));
	for (i = 0; i < len; i++) a[i] = n.a[i];
	return *this;
}

CBigNumber CBigNumber::operator+(const CBigNumber &n) const
{
	CBigNumber t;
	int i, blen;

	blen = len > n.len ? len : n.len;
	for (i = 0; i < blen; i++) {
		t.a[i] += a[i] + n.a[i];
		if (t.a[i] > CBigNumber::MAXN) {
			t.a[i + 1]++;
			t.a[i] -= (CBigNumber::MAXN + 1);
		}
	}
	if (t.a[blen] == 0) blen--;
	t.len = blen + 1;
	return t;
}

// a must bigger to b...
CBigNumber CBigNumber::operator-(const CBigNumber & n) const
{
	int     i, j, big;
	CBigNumber t, a(*this);

	big = n.len > a.len ? n.len : a.len;
	for(i = 0 ; i < big; i++){
		if(a.a[i] < n.a[i]){
			j = i + 1;
			while(a.a[j] == 0) j++;
			a.a[j--]--;
			while(j > i) a.a[j--] += CBigNumber::MAXN;
			t.a[i] = a.a[i] + CBigNumber::MAXN + 1 - n.a[i];
		} else t.a[i] = a.a[i] - n.a[i];
	}
	a.len = big;
	while(t.a[a.len - 1] == 0 && a.len > 1) a.len--;
	t.len = a.len;
	return t;
}

CBigNumber CBigNumber::operator*(const CBigNumber &n) const
{
	CBigNumber t;
	int     i, j;

	for (i = 0; i < len; i++) {
		for (j = 0; j < n.len; j++) {
			t.a[i + j] += a[i] * n.a[j];
			if (t.a[i + j] > CBigNumber::MAXN) {
				t.a[i + j + 1] += t.a[i + j] / (CBigNumber::MAXN + 1);
				t.a[i + j] %= (CBigNumber::MAXN + 1);
			}
		}
	}
	t.len = len + n.len;
	while (t.a[t.len - 1] == 0 && t.len > 1) t.len--;
	return t;
}

CBigNumber CBigNumber::operator/(unsigned long long n) const
{
	CBigNumber t;
	int     i;
	unsigned long long	down;

	down = 0;
	for (i = len - 1; i >= 0; i--) {
		t.a[i] = (down * (CBigNumber::MAXN + 1) + a[i]) / n;
		down = a[i] + down * (CBigNumber::MAXN + 1) - t.a[i]*n;
	}
	t.len = len;
	while (t.a[t.len - 1] == 0 && t.len > 1) t.len--;
	return t;
}
/*
std::ostream & operator<<(std::ostream &os, const CBigNumber &n)
{
	int i;

	os << n.a[n.len - 1];
	for(i = n.len - 2 ; i >= 0 ; i--){
		os.width(CBigNumber::DLEN);
		os.fill('0');
		os << n.a[i];
	}
	return os;
}

std::istream & operator>>(std::istream &is, CBigNumber &n)
{
	char    s[CBigNumber::DIGIT * 4 + 1];

	is >> s;
	CBigNumber t(s);
	n = t;
	return is;
}
*/
