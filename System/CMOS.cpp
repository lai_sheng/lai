
char cmos_lable[] = "XC";

#include "System/CMOS.h"
#include <string.h>
//#include <stdio.h>

PATTERN_SINGLETON_IMPLEMENT(CCMOS);

CCMOS::CCMOS()
{
}

CCMOS::~CCMOS()
{
}

void CCMOS::Initialize()
{
    int i;
    char zero = 0;
    char magic[2] = {0};
	m_stopflag=0;
    //Check cmos lable
    CMOSRead(CMOS_LABAL, magic, 2);
    if (strncmp(magic, cmos_lable,2) != 0) 
    {//load default
        trace("CCMOS::Initialize() clear...\n");
        FOR (i, CMOS_AREA_END) {
            CMOSWrite((CMOS_OFFS)i, &zero, 1);
        }
        
        CMOSWrite(CMOS_LABAL, cmos_lable, 2);

        ushort flag = BITMSK(CMOS_DST_TUNE);
        CMOSWrite(CMOS_FLAGS, &flag, 2);
        
        /* 确保处于自动录像状态 */
        zero = 0xaa;
        for (i = CMOS_MANUAL_REC; i < CMOS_MANUAL_REC + 8; i++)
        {
            CMOSWrite((CMOS_OFFS)i, &zero, 1);
        }
        
    }
}

int CCMOS::Read(CMOS_OFFS offs, void *pdat, int len)
{
	if(m_stopflag==1)
	{
		return 0;
	}
    return CMOSRead(offs, pdat, len);
}

int CCMOS::Write(CMOS_OFFS offs, void *pdat, int len)
{
	if(m_stopflag==1)
	{
		return 0;
	}
    if(offs==0x38) trace("CCMOS::Write 48 offs = [0x%x] pdat=[0x%x] len=[%d]++++++++++++++++++\r\n",offs,pdat,len);
    return CMOSWrite(offs, pdat, len);
}

int CCMOS::ReadFlag(CMOS_FLAGS_INDEX index)
{
    ushort flag;
    Read(CMOS_FLAGS, &flag, 2);
    if(flag & BITMSK(index)){
        return 1;
    }else{
        return 0;
    }
}

int CCMOS::WriteFlag(CMOS_FLAGS_INDEX index, int enable)
{
    ushort flag;
    Read(CMOS_FLAGS, &flag, 2);
    if((flag & BITMSK(index)) && !enable){
        flag &= ~BITMSK(index);
    }else if(!(flag & BITMSK(index)) && enable){
        flag |= BITMSK(index);
    }
    return Write(CMOS_FLAGS, &flag, 2);
}

void CCMOS::SetExitTime(DHTIME *time)
{
    Write(CMOS_EXIT_TIME, time, sizeof(DHTIME));
}

void CCMOS::GetExitTime(DHTIME *time)
{
    Read(CMOS_EXIT_TIME, time, sizeof(DHTIME));
}

void CCMOS::SetExitState(uchar state)
{
    Write(CMOS_EXIT_STATE, &state, 1);
}

uchar CCMOS::GetExitState()
{
    uchar state;
    Read(CMOS_EXIT_STATE, &state, 1);
    return state;
}

void CCMOS::Dump()
{
    int i = 0;
    char data = 0;

    FOR (i, CMOS_AREA_END) {
        CMOSRead((CMOS_OFFS)i, &data, 1);
        if (0 == i % 16)
        {
            trace("\n");
        }
        trace("%02x ", data);
    }
    trace("\n");
}
int CCMOS::SetCmosFlag(int flag)
{
   m_stopflag=flag;
   return 0;
}