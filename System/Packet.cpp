
////////////////////////////////////////////////////////////////////////////////////
// CPacket
////////////////////////////////////////////////////////////////////////////////////

#include <time.h>
#include <string.h>

#include "APIs/Memory.h"
#include "System/Packet.h"
#include "System/AppConfig.h"

// 默认的配置参数
int	CPacketManager::m_totalSize = (12 * 1024 * 1024);
//int	CPacketManager::m_totalSize = (8 * 1024 * 1024);
int	CPacketManager::m_pageSize = PKT_PAGE_SIZE;
bool CPacketManager::s_bMemeoryDelay = true;

#if 0
void printFrameInfo(CPacket *pPkt)
{
	int i = 0;
	char *value = (char *)pPkt->GetHeader();
	for (i = 0; i < FRAME_MAX_NUM; i++)
	{
		trace("%08x:", i);
		for (int j = 0; j < sizeof(FRAMEINFO); j++)
		{
			trace("%02x ", *value);
			value++;
		}
		trace("\n");
	}
	trace("%08x:", i);
	for (i = 0; i < 8; i++)
	{
		trace("%02x ", *value);
		value++;
	}
	trace("\n");

	trace("********************************************************\n");
}
#endif

/*!
	\b Description		:	查找包的I帧\n
	\b Argument			:	CPacket *pPkt
	\param	pPkt		:	欲查找包的指针
	\return	是否找到I帧，找到为下标，－1表示未找到			

	\b Revisions		:	
*/

int findFirstFrame(CPacket *pPkt, int itype/*= PACK_TYPE_I_FRAME*/)
{
	//视新增加了打包时查找I帧

	/*      帧定位信息
	说明: 帧定位信息一共16字节, 每4个字节为一个记录,一共4个记录
	每个记录组成如下:

	MSB								     LSB
	---------------------------------------------------------------------------------
	|31  30 | 29                      18 | 17                                     0 |
	---------------------------------------------------------------------------------
	|Flag   |     Location               |           Length                        

	Flag  		    00	                    01 						10
	Location       无效                I帧在本块中的定位            无效
	Length         无效                I帧长度                      剩余I帧的计数

	注释: 当本块中I帧数目大于4时, 前3个定位信息指示前3个I帧的定位, 
	最后一个定位信息指示无法详细定位的I帧的数目.
	*/

	/*
	不定长的流模式,每次最多传输8K字节, 也可以少于8K字节
	每次最多传输8帧(视频帧和音频帧), 帧尾部或者中间数据不包含帧头的不算.
	每满8帧必须发起传输(即使不满8K数据)
	每帧都有帧定位信息(这种流模式即可认为是帧模式)
	每个帧定位信息8个字节
	每8K数据有8个帧定位信息

	8位									8位									16位					32位							16位				16位
	帧类型标志					头尾标志							起始位置						长度							段长度			　　保留
	0xFF  无数据				0  非头尾数据					帧在本块中起始位置					帧真正的长度, 可能跨块			帧在本块中的长度
	0     P帧					1  包含帧头
	1     I 帧					2  包含帧尾
	2     B帧					3  包含帧头和帧尾
	3     音频帧				
	4    JPEG 图片帧
	5	　动态帧

	紧接着一个字节表示标志位。
	在主码流的数据下，１表示提示应用程序打包，０表示不需要打包。
	如果是抓图的情况下， 0表示数据未完，1表示图片数据的开始，２表示数据的结束, 3表示在仅包含在一个数据块中，也就是说在开始的时候同时结束。需要说明的是，两张图片应该放在不同的数据包里，即使前一张图片只占了数据包的很小一部分。

	*/

	if (!pPkt)
	{
		tracepoint();
		return -1;
	}

	int iframepos = -1;

#if  (!defined(WIN32)) 
	//printFrameInfo(pkt);
	PKT_HEAD_INFO	*pkthead = (PKT_HEAD_INFO *)(pPkt->GetHeader()); //注意由于下面内存已对齐
	
	for (int i = 0; i < FRAME_MAX_NUM; i++)
	{
		if (PACK_TYPE_FRAME_MOTION != itype)
		{
			if ((pkthead->FrameInfo[i].FrameType == itype) 
			&& ((pkthead->FrameInfo[i].FrameFlag == PACK_CONTAIN_FRAME_HEAD) || (pkthead->FrameInfo[i].FrameFlag == PACK_CONTAIN_FRAME_HEADTRAIL)))
			{
				iframepos = pkthead->FrameInfo[i].FramePos;
				break;
			}
		}
		else//动检帧开始得标识
		{
			if ((pkthead->FrameInfo[i].FrameType == PACK_TYPE_FRAME_MOTION) 
			&& ((pkthead->FrameInfo[i].FrameFlag == PACK_CONTAIN_FRAME_MOTION_HEAD) || (pkthead->FrameInfo[i].FrameFlag == PACK_CONTAIN_FRAME_HEADTRAIL)))
			{
				iframepos = pkthead->FrameInfo[i].FramePos;
				break;
			}
		}
	
	}
#else
	iframepos = 0;
#endif

	return iframepos;
}

VD_BOOL getFirstFrameTime(CPacket *pPkt, DHTIME *pDHTIME, int itype/*= PACK_TYPE_I_FRAME*/)
{
	VD_BOOL bRet = FALSE;

	if ((!pPkt) || (!pDHTIME))
	{
		return bRet;
	}
	
	int iFramePos = findFirstFrame(pPkt, itype);
	if (iFramePos >= 0)
	{
		uchar *pFrameInfo = pPkt->GetBuffer() + iFramePos;

#if	defined(CHIP_S2L) || defined(DOME_S2L) || defined(BPI_S2L)
	//ZHY 20150527 针对新私有协议修改
	int pos = 12;
#else
	int pos = 8;
#endif

#ifdef ENC_NEW_STREAM
		pos = 16;
#endif		

		if (pPkt->GetLength() - iFramePos >= 7)
		{
			if (*(pFrameInfo+3) == 0xed ///svac系列新帧头，0x000001ed为I帧
                        ||*(pFrameInfo+3) == 0xec 
                        ||*(pFrameInfo+3) == 0xea )
			{
                        pos = 12;
			}
              }
        
		if (pPkt->GetLength() - iFramePos >= (pos + sizeof(DHTIME)))
		{
			uchar *pIFameTime = pFrameInfo + pos;//这儿暂写死
			memcpy((void *)pDHTIME, pIFameTime, sizeof(DHTIME));
			bRet = true;
		}
	}

	return bRet;
}

VD_BOOL getImageSizeFromPacket(CPacket* pPkt, VD_SIZE* pSize, int itype/*= PACK_TYPE_I_FRAME*/)
{
	VD_BOOL bRet = FALSE;

	if ((!pPkt) || (!pSize))
	{
		return bRet;
	}
	
	int iFramePos = findFirstFrame(pPkt, itype);
	if (iFramePos >= 0)
	{
		uchar *pFrameInfo = pPkt->GetBuffer() + iFramePos;
		if (pPkt->GetLength() - iFramePos >= 7)
		{
			uchar *p = pFrameInfo;
			if (*(p+3) == 0xed  ///svac系列新帧头，0x000001ed为I帧
                         ||*(p+3) == 0xec
                         ||*(p+3) == 0xea) 
			{
				pSize->w = ((*(p+9))<<8) | (*(p+8));
				pSize->h = ((*(p+11))<<8) | (*(p+10));
			}
			else if (*(p+3) == 0xad  ///svac系列新帧头，0x000001ed为I帧
                         ||*(p+3) == 0xae
                         ||*(p+3) == 0xaa) 
			{
				pSize->w = ((*(p+9))<<8) | (*(p+8));
				pSize->w *= 8;
				
				pSize->h = ((*(p+11))<<8) | (*(p+10));		
				pSize->h *= 8;
			}
			else  //老帧头，0x000001fd为I帧
			{
				p = p + 6;
				pSize->w = *p * 8;
				p++;
				pSize->h = *p * 8;
			}
			bRet = TRUE;
		}
	}

	return bRet;
}

int getPackFlag(CPacket *pPkt)
{
	PKT_HEAD_INFO	*pkthead = (PKT_HEAD_INFO *)(pPkt->GetHeader());
	return pkthead->PacketInfo;
}

CPacket::CPacket()
{
}

CPacket::~CPacket()
{
}

void CPacket::Init()
{
	m_RefCount = 1;
	m_Length = 0;
	m_Size = 0;
	m_pBuffer = NULL;
	m_iLastUId = -1;
}

uint CPacket::PutBuffer(void * pdat, uint dwLength)
{
	uint left;

	CGuard guard(m_Mutex);

	left = m_Size - m_Length;
	if (left < dwLength) {		//包容量不足
		dwLength = left;
	}
	
	// 从后面增加，可以连续使用。
	memcpy(m_pBuffer + m_Length, pdat, dwLength);

	m_Length += dwLength;

	return dwLength;
}

uchar * CPacket::GetBuffer()
{
	return m_pBuffer;
}


uint CPacket::SetLength(uint dwLength)
{
	
	//assert(dwLength <= m_Size);
	
	m_Length = dwLength;

	return m_Length;
}

uint CPacket::GetLength()
{
	return m_Length;
}

uint CPacket::GetSize()
{
	return m_Size;
}

uint CPacket::GetLeft()
{
	CGuard guard(m_Mutex);
	return (m_Size - m_Length);
}

uchar* CPacket::GetHeader()
{
	return m_Header;
}

void CPacket::ClearHeader()
{
	memset(m_Header, 0, PKT_HDR_SIZE);
}

uint CPacket::Release(int iId)
{
	m_Mutex.Enter();

	assert(m_RefCount > 0);

	if(m_RefCount == 0){
		trace("CPacket::Release() reffrence count error\n");
		m_Mutex.Leave();
		return 0;
	}

	--m_RefCount;

	uint count = m_RefCount;
	m_Mutex.Leave();//必须在回收包之前解锁，否则引起严重错误

	if (count == 0) {
		g_PacketManager.PutPacket(this);
	}
	return count;
}

uint CPacket::AddRef(int iId)
{
	CGuard guard(m_Mutex);

	m_RefCount++;
	return m_RefCount;
}

uint CPacket::GetRef()
{
	CGuard guard(m_Mutex);
	return m_RefCount;
}

void CPacket::GetSysTime(SYSTEM_TIME *time)
{
	CGuard guard(m_Mutex);
	if(NULL == time)
	{
		tracepoint();
		return;
	}
	memcpy(time,&m_SysTime,sizeof(SYSTEM_TIME));
}

void	CPacket::SetSysTime(SYSTEM_TIME *time)
{
	CGuard guard(m_Mutex);
	if(NULL == time)
	{
		tracepoint();
		return;
	}
	memcpy(&m_SysTime,time,sizeof(SYSTEM_TIME));
//	trace("%d-%d-%d %d:%d:%d\n",m_SysTime.year,m_SysTime.month,m_SysTime.day,m_SysTime.hour,m_SysTime.minute,m_SysTime.second);
}

////////////////////////////////////////////////////////////////////////////////////
// CPacketManager
////////////////////////////////////////////////////////////////////////////////////

PATTERN_SINGLETON_IMPLEMENT(CPacketManager);

CPacketManager::CPacketManager()
    :m_Mutex(MUTEX_RECURSIVE)
{
	trace("CPacketManager::CPacketManager()>>>>>>>>>\n");
	int i;
	SYSTEM_CAPS syscaps;
	int size;
	PBN *pNode;

	m_pFreeList = NULL;
	 m_nTypes = 0;
	SystemGetCaps(&syscaps);
	/*if (syscaps.MemoryLeft > 4096)//剩余内存大于4M
	{//DVR应用程序将把剩余的内存减去4M，作为缓冲使用，最大32M
		m_nPages = MIN(syscaps.MemoryLeft - 4096, 32 * 1024);
	}
	else
	{
		trace("CPacketManager::CPacketManager() not enough memory!!!!\n");
		return ;
	}*/
	m_nPages = m_totalSize / m_pageSize;
	//(int)AppConfig::instance()->getNumber("Global.Memory.PacketBufSize", PKT_KILOS_DEFAULT) * 1024 / PKT_PAGE_SIZE;

	try
	{
		m_pOriginBuffer = new uchar[(m_nPages + 1) * m_pageSize];
		if( !s_bMemeoryDelay )
		{
			memset(m_pOriginBuffer, 0, (m_nPages + 1) * m_pageSize);
		}
	}
	catch (std::bad_alloc e)
	{
		trace("CPacketManager::CPacketManager() new buffer failed!!!!\n");
		return ;
	}
	m_pBuffer = (uchar *)(((unsigned long)m_pOriginBuffer + m_pageSize) & ~(m_pageSize - 1));//按页面大小对齐
	
	 
	for(m_nTypes = 0, size = m_nPages; ; m_nTypes++)
	{
		if(m_nPages < (1 << m_nTypes))
		{
			break;
		}

		// 取偶数，为了节点合并方便
		size = ((size + 1 ) & 0xfffffffe);

		m_PBAs[m_nTypes].pArray = new PBN[size];
		if(!m_PBAs[m_nTypes].pArray)
		{
			trace("CPacketManager::CPacketManager() new nodes failed!!!!\n");
		}

		// 所有节点标志清空
		for(i = 0; i < size; i++)
		{
			m_PBAs[m_nTypes].pArray[i].pNext = NULL;
			m_PBAs[m_nTypes].pArray[i].nIndex = i;
		}

		// 出始化空闲节点链表
		if (m_nPages & BITMSK(m_nTypes))
		{
			pNode = &m_PBAs[m_nTypes].pArray[m_nPages / (1 << m_nTypes) - 1];
			m_PBAs[m_nTypes].pHeader = pNode;
			pNode->pNext = pNode;
			pNode->pPrev = pNode;
		}
		else
		{
			m_PBAs[m_nTypes].pHeader = NULL;
		}
		m_PBAs[m_nTypes].nCount = size;
		size /= 2;
	}
	
	Dump();
}

CPacketManager::~CPacketManager()
{
	if(m_pOriginBuffer)
	{
		delete []m_pOriginBuffer;
	}

	for(; m_nTypes; m_nTypes--)
	{
		delete []m_PBAs[m_nTypes-1].pArray;
	}
}

CPacket* CPacketManager::AllocPacket()
{
	CPacket * p = m_pFreeList;
	
	if(p) 
	{
		m_pFreeList = p->m_pNext;
	}
	else 
	{
		CPacket *q = new CPacket[NALL];
		
		fprintf(stderr, "==>CPacketManager: allocate buffer:0x%08X size:%d\n", (int)q, NALL * sizeof(CPacket));
		/* 链接自由对象 */
		for(p = m_pFreeList = &q[NALL -1]; q<p; p--) 
		{
			p->m_pNext = p - 1;
		}
		
		(p + 1)->m_pNext = NULL; // 最后一个自由对象的后续为空
	}
	
	return p;
}

void CPacketManager::FreePacket(CPacket* p)
{
	assert(p);
	p->m_pNext = m_pFreeList;
	m_pFreeList = p;
}

CPacket * CPacketManager::GetPacket(uint dwBytes /* = 0 */)
{
	CPacket* pPacket;
	int index = 0;
	int type;
	int pages;
	int i;

	CGuard guard(m_Mutex);

	pPacket = AllocPacket();
	
	if(!pPacket)
	{
		_printd("CPacketManager::GetPacket packet error!!!!!!\n");
		return NULL;
	}

	pPacket->Init();

	// 没有缓冲，只有包头的包
	if(dwBytes == 0)
	{
		pPacket->m_Size = 0;
		pPacket->m_pBuffer = NULL;
		return pPacket;
	}

	// 根据缓冲大小计算出节点级别
	pages = (dwBytes + m_pageSize - 1) / m_pageSize;
	type = log2i(pages);
	if((1u << type) != (unsigned int)pages)
	{
		type++;
	}
	pages = 1 << type;

	// 查找最小的空闲节点
	for(i = type; i < m_nTypes; i++)
	{
		if(m_PBAs[i].pHeader)
		{
			break;
		}
	}
	//_printd("GetFreeSize = %d, GetUsedSize = %d, total = %d", GetFreeSize(), GetUsedSize(), GetFreeSize() + GetUsedSize());
	if(i >= m_nTypes)
	{
		_printd("GetFreeSize = %d GetUsedSize = %d"
			"CPacketManager::GetPacket, dwBytes = %d, m_pageSize = %d, pages = %d, type = %d"
			"CPacketManager::GetPacket none free node!!!!!!, i = %d, m_nTypes = %d",
			GetFreeSize(), GetUsedSize(), dwBytes, m_pageSize, pages, type,i, m_nTypes);
		FreePacket(pPacket);
		return NULL;
	}

	// 取出最小的空闲节点
	index = m_PBAs[i].pHeader->nIndex;
	RemoveFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index]);

	// 填充包数据成员
	pPacket->m_Size = pages * m_pageSize;
	pPacket->m_pBuffer = m_pBuffer + (index << i) * m_pageSize;

	//切分大规格的节点
	for(i--; i >= type; i--)
	{
		index *= 2;
		InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index + 1]);
	}

	// 分析出多余的空间包含的更小规格空闲节点， 比如申请11个页面时， 实际取到的是16个页面， 还多出5 = 4 + 1个页面
	if((unsigned int)pages != (1u << type))
	{
		for(; i >= 0; i--)
		{
			index = index * 2;
			if(!(pages & BITMSK(i)))
			{
				InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index + 1]);
			}
			else
			{
				index += 1;
				pages -= (1u << i);
				if(!pages)
				{
					InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index]);
					break;
				}
			}
		}
	}

	return pPacket;
}

void CPacketManager::PutPacket(CPacket *pPacket)
{
	CGuard guard(m_Mutex);
	int index;
	int theother;
	int type;
	int pages;
	int i;
	int merged = 0; // 0-没有合并 1-正在合并 2-合并完成

	if(!pPacket)
	{
		return;
	}
	if(!pPacket->m_Size || !pPacket->m_pBuffer)
	{
		FreePacket(pPacket);
		return;
	}

	// 取出包数据成员
	pages = pPacket->m_Size / m_pageSize;
	index = (pPacket->m_pBuffer - m_pBuffer) / m_pageSize;

	type = log2i(pages);
	if((1u << type) != (unsigned int)pages)
	{
		type++;
		index += pages; // index为要释放的包内存块后紧跟的0级节点序号
	}
	else
	{
		i = type;
		index /= pages; // index为当前节点的序号
		goto post_merge;
	}

	// 分析出包缓冲包含的所有2幂次节点，如 11 = 8 + 2 + 1，从小到大一一进行合并
	for(i = 0; i < type; i++)
	{
		if(index & 0x1)
		{
			if(merged == 0)
			{
				if(m_PBAs[i].pArray[index].pNext)
				{
					merged = 1;
					RemoveFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index]);
				}
				else
				{
					merged = 2;
					InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index - 1]);
				}
			}
			else if(merged == 2)
			{
				InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index - 1]);
			}
		}
		else
		{
			if(merged == 1)
			{
				if(m_PBAs[i].pArray[index + 1].pNext)
				{
					RemoveFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index + 1]);
				}
				else
				{
					merged = 2;
					InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index]);
				}
			}
		}
		index /= 2;
	}

	//将相邻小节点合并成大节点, 如1->2->4->8, 直到不能再合并为止
	if(merged == 1)
	{
post_merge:
		for(; i < m_nTypes - 1; i++)
		{
			theother = (index % 2) ? (index - 1) : (index + 1);
			if(m_PBAs[i].pArray[theother].pNext)
			{
				RemoveFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[theother]);
			}
			else
			{
				break;
			}
			index /= 2;
		}

		InsertFree(m_PBAs[i].pHeader, &m_PBAs[i].pArray[index]);
	}

	// 回收包壳对象
	FreePacket(pPacket);
}

// 删除空闲节点
inline void CPacketManager::RemoveFree(PBN * & pHeader, PBN * pThis)
{
	if(pThis == pHeader)
	{
		pHeader = pThis->pNext;
		if(pThis == pHeader) // 只有一个节点
		{
			pThis->pNext = NULL;
			pHeader = NULL;
			return;
		}
	}

	pThis->pPrev->pNext = pThis->pNext;

	if (pThis->pNext != NULL)
	{
		pThis->pNext->pPrev = pThis->pPrev;     
	}
	else
	{
	  _printd("CPacketManager::RemoveFree():pThis->pNext = NULL\r\n");
	}
	   
	pThis->pNext = NULL;
}

// 将新的空闲节点加入链表
inline void CPacketManager::InsertFree(PBN * & pHeader, PBN * pThis)
{
	if(pHeader)
	{
		pHeader->pPrev->pNext = pThis;
		pThis->pPrev = pHeader->pPrev;
		pHeader->pPrev = pThis;
		pThis->pNext = pHeader;
		pHeader = pThis;
	}
	else
	{
		pHeader = pThis;
		pThis->pPrev = pThis;
		pThis->pNext = pThis;
	}
}

//返回缓冲的大小,以KB为单位
uint CPacketManager::GetBufferSize()
{
	CGuard guard(m_Mutex);

	return m_nPages * m_pageSize / 1024;
}

void CPacketManager::Dump()
{
	int type;
	CGuard guard(m_Mutex);
	PBN* current;
	int freepages = 0;
	
	//打印利用率
	for(type = 0; type < m_nTypes; type++)
	{
		if(m_PBAs[type].pHeader)
		{
			current = m_PBAs[type].pHeader;
			while (1)
			{
				freepages += 1<<type;
				if(current == m_PBAs[type].pHeader->pPrev)
				{
					break;
				}
				current = current->pNext;
			}
		}
	}
	_printd("Packet usage : %dK / %dK, %d%%\n", m_nPages - freepages, m_nPages, 100-100*freepages/m_nPages);

#if 1
	//
	// show user
	//
	/*PPACKET pPACKET;
	CPacket* pPacket;

	for(pPACKET = m_Queue.begin(); pPACKET != m_Queue.end(); pPACKET++)
	{
		pPacket = *pPACKET;

		if (pPacket->m_iLastUId >= 0)
		{
			trace(" %d", pPacket->m_iLastUId);
		} // if
	}// for*/

	SYSTEM_TIME stNow;
	SystemGetCurrentTime(&stNow);
	trace("\n----------------------[%02d-%02d %02d:%02d:%02d]\n",
			stNow.month, stNow.day,
			stNow.hour, stNow.minute, stNow.second);
#endif

}
	
void CPacketManager::DumpNodes()
{
	int type;
	CGuard guard(m_Mutex);
	PBN* current;
	//打印所有节点的状态
#if 0
	for(type = 0; type < m_nTypes; type++)
	{
		trace("%4d : ", (1u <<type));
		for(int i = 0; i < (1 << type) - 1; i++)
		{
			trace(" ");
		}
		for(i = 0; i < m_PBAs[type].nCount; i++)
		{
			trace("%x", m_PBAs[type].pArray[i].pNext ? 1 : 0);
			for(int j = 0; (j < (2 << type) - 1) && (i != m_PBAs[type].nCount - 1); j++)
			{
				trace(" ");
			}
		}
		trace("\n");
	}
#endif
	//打印空闲链表
	_printd("______________________________\n");
	for(type = 0; type < m_nTypes; type++)
	{//顺序打印
		_printd("%4d : ", (1u <<type));
		if(m_PBAs[type].pHeader)
		{
			current = m_PBAs[type].pHeader;
			while (1)
			{
				trace("%d ", current->nIndex);
				if(current == m_PBAs[type].pHeader->pPrev)
				{
					break;
				}
				current = current->pNext;
			}
		}
		_printd("\n");
	}

	_printd("\n");
}

void CPacketManager::Test()
{
#ifdef _DEBUG_THIS
	int i;
	int count = 1024;
	unsigned long size = 0;
	unsigned long page = 0;
	unsigned long len;
	CPacket * pPacket[1024];
	SYSTEM_TIME systime;

	SystemGetCurrentTime(&systime);
	memset(pPacket, 0 , sizeof(CPacket *) * count);
	DumpNodes();
	srand(systime.second);
	for(i=0; i<count; i++)
	{
		len = ((rand() % 65536) + 1);
		//len = (1 << log2i(len));
		pPacket[i] = g_PacketManager.GetPacket(len);
		if(!pPacket[i]){
			break;
		}
		size += len;
		page += (len + m_pageSize - 1) / m_pageSize;
	}
	_printd("CPacketManager::Test() rand allocate usage: in byte %d%%, in page %d%%.\n", 100*size/m_pageSize/m_nPages, 100*page/m_nPages);
	DumpNodes();
	for(i=0; i<count; i++)
	{
		if(pPacket[i]){
			pPacket[i]->Release();
		}
	}
	DumpNodes();
#endif
}

void CPacketManager::config( int totalSize, int pageSize )
{
	assert(pageSize >= 1024 && ((1 << log2i(pageSize)) == pageSize));
	m_totalSize = totalSize;
	m_pageSize = pageSize;
}

void CPacketManager::MemoryDelay(bool bDelay)
{
	s_bMemeoryDelay = bDelay;
}

uint CPacketManager::GetFreeSize()
{
	int type;
	CGuard guard(m_Mutex);
	PBN* current;
	int freepages = 0;

	//打印利用率
	for(type = 0; type < m_nTypes; type++)
	{
		if(m_PBAs[type].pHeader)
		{
			current = m_PBAs[type].pHeader;
			while (1)
			{
				freepages += 1<<type;
				if(current == m_PBAs[type].pHeader->pPrev)
				{
					break;
				}
				current = current->pNext;
			}
		}
	}

	return freepages;
}

uint CPacketManager::GetUsedSize()
{
	int type;
	CGuard guard(m_Mutex);
	PBN* current;
	int freepages = 0;

	//打印利用率
	for(type = 0; type < m_nTypes; type++)
	{
		if(m_PBAs[type].pHeader)
		{
			current = m_PBAs[type].pHeader;
			while (1)
			{
				freepages += 1<<type;
				if(current == m_PBAs[type].pHeader->pPrev)
				{
					break;
				}
				current = current->pNext;
			}
		}
	}

	return m_nPages - freepages;
}



////////////////////////////////////////////////////////////////////////////////////
/////////////////  提供给C调用的函数。
int MemoryAlloc (MEMORY_BLOCK * pBlock, uint dwBytes)
{
	assert(pBlock != NULL);

	CPacket *pPacket = g_PacketManager.GetPacket(dwBytes);
	if (pPacket)
	{
		pBlock->handle = (VD_HANDLE)pPacket;
		pBlock->length = pPacket->GetLength();
		pBlock->size = pPacket->GetSize();
		pBlock->ptr = pPacket->GetBuffer();
		pBlock->header = pPacket->GetHeader();
		return 0;
	}else{
		_printd("g_PacketManager.GetPacket return NULL");
	}
	return -1;
}

int MemorySetLength(VD_HANDLE hMemory, uint dwBytes)
{
	if(hMemory == NULL)	return -1;
	
	CPacket * pPakcet = (CPacket *)hMemory;
	assert (dwBytes <= pPakcet->GetSize());
	pPakcet->SetLength(dwBytes);
	return 0;
}

int MemoryAddRef(VD_HANDLE hMemory)
{
	if(hMemory == NULL)	return -1;

	CPacket * pPakcet = (CPacket *)hMemory;
	pPakcet->AddRef();
	return 0;
}

int MemoryRelease(VD_HANDLE hMemory)
{
	if(hMemory == NULL)	return 0;

	CPacket * pPakcet = (CPacket *)hMemory;
	pPakcet->Release();
	return 0;
}
//
// End of "$Id: Packet.cpp 19137 2008-08-08 03:13:13Z yuan_shiyong $"
//


