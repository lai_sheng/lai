#include <string.h>
#include "System/MediaStream.h"


int FindFrameMarker(
					uchar *buf, 
					int nLen,
					uint *pMP4Code)
{
	uchar	*pFrameMarker = (uchar *) buf;
	uchar	*pEndAddress  = buf + nLen ;
	uint  nValue = *pMP4Code;

	/*
	*	根据测试，这里代码的执行时间跟Data Cache有非常大的关系
	*/
	while (pFrameMarker < pEndAddress) 
	{
		nValue = (nValue << 8 ) | *pFrameMarker;
		pFrameMarker ++;

		/*
		*	帧的起始位置以0x000001开始，后跟标记字符
		*/
		if( ((nValue & 0xFFFFFF00) == 0x00000100)  )   
			break;
	}  // while
	*pMP4Code = nValue;
	return pFrameMarker - buf - 4;
}
/*
**	********************************************************************************
**	原型	int MediaStreamParse(BYTE * pBuffer, uint *pCount, BOOL bFirst);
**	说明	分析视频数据，找到有效的帧数据边界。用来恢复断电时刻的录像数据长度，理论上
**			保证最多丢失1秒钟的数据。
**	参数	[in] pBuffer
**			数据缓冲指针。
**			[in/out] pCount
**			传入时是缓冲长度；传出时是找到最后一个有效帧（I，B，P，音频帧等等）的末尾
**			在所送入的缓冲里的偏移，如果为0， 表示未找到任何帧末尾。不管当前缓冲区数
**			据是否找到了错误，pCount都应该按上述要求返回正确的值。
**			[in] bFirst
**			是否为首次传入。如果为真， 做一些初始化处理， 如果为假， 在上次的历史数
**			据基础上继续解析。码流分析过程会执行多次， 对每个通道的码流都要分析一遍，
**			 也就是bFirst会多次为真。
**	返回值	= 0, 表示未发现错误。
**			< 0, 表示发现了错误。
**	********************************************************************************
*/

int MediaStreamParse(uchar * pBuffer, 
				  uint *pCount, 
				  VD_BOOL bFirst)
{
	uchar *pBuf;
	uint nOffset;
	uint nBufferSize;
	uint nRet=0;

	static uint len=0; 						 //length of bitty data
	static int last_hour=0;   
	static int last_minute=0;  
	static int last_second=0;					  //used to judge old mepg4
	static uchar frameHeadTruncation=0;				  //帧头有没有被打断
	static uchar data[16];					//帧头数据
	static uint expectNextHead=0;		 //期待的帧头偏移地址
	static uchar detect_coder=0; 					//检测是否是老码流的变量
	static uchar secondtime=0;						//是否第二次检测到老码流
	static uchar FLAG_OLDMPEG4=0;					
	static uchar myflag=0;							//出错以后 虽然在函数内部置了 bfirst为0，但是如果下一次调用的时候
	//不小心的在传递进来之前又修改了bfirs的值，还是有可能除错的
	//于是出错置变量myflag为1，重新初始化	
	static uchar HBflag=0;	 
	static int pos=0;
	static uchar FIRST_IFRAME=0;

	if(bFirst  || myflag)
	{
		last_second = 0;
		last_minute = 0;
		last_hour = 0;
		len =0;
		detect_coder=0;
		expectNextHead=0;
		secondtime=0;
		memset(data,'0',16);
		frameHeadTruncation=0;				  //=1 帧头信息不完整，残缺帧头信息写在data里面 =2 帧头信息和帧长信息完整
		FLAG_OLDMPEG4=0;
		myflag=0;
		pos=0;
		FIRST_IFRAME=0;
		HBflag=0;
	}

	nOffset = 0;
	nBufferSize = *pCount;
	pBuf = pBuffer;

	if(detect_coder == 0 && secondtime <2)	   //是否老的mpeg还没有检测或者检测出来了是老的mpeg 但是要检测2次以上确保没有错
	{ 
		uint MP4Code = 0xffffffff;
		while(nOffset < nBufferSize)
		{
			nRet  = FindFrameMarker(pBuf + nOffset, nBufferSize - nOffset, &MP4Code);
			nOffset+=nRet+4;
			if(MP4Code == 0x01FB ||
				MP4Code == 0x01FD ||
				MP4Code == 0x01FA ||
				MP4Code == 0x01FC ||
				MP4Code == 0x01F2)
			{
				if(secondtime)
				{
					//以前至少有一次检测到0100 但是没有检测到新码流的头 这一次却检测到了新码流的头 有问题
					myflag=1;
					*pCount=0;
					trace("old code to new\n");
					return MEDIA_STREAM_ERR;
				}
				if(MP4Code == 0x01F2)
					HBflag=1;
				FLAG_OLDMPEG4=0;
				detect_coder=1;
				nOffset-=4;
				break;
			}

			else if(MP4Code == 0x0100)
			{
				FLAG_OLDMPEG4=1;
				secondtime++;
				detect_coder=0;
				nOffset-=4;
				break;
			}

		}
		if(MP4Code !=0x01FB &&
			MP4Code != 0x01FD &&
			MP4Code != 0x01FA &&
			MP4Code != 0x01FC &&
			MP4Code != 0x01F2 &&
			MP4Code != 0x0100)
		{
			*pCount=0;
			return MEDIA_STREAM_OK; 
		}
	}

	if(!FLAG_OLDMPEG4)
	{	
		uint MP4Code = 0xffffffff;
		nOffset=0;	
		while(nOffset < nBufferSize) 
		{					
			//首先搜索上个文件遗留下来的东西 ，如果帧头信息被完整的保存    也就是说 有帧长的信息 
			//那么这一次送进来的走过expectNextHead长度后 就是新的帧头了 
			if(frameHeadTruncation == 2)   
			{
				if(expectNextHead+4 <nBufferSize)
				{
					VD_BOOL state_first=(00 == *(pBuf+expectNextHead)&&
						00 == *(pBuf+expectNextHead+1) &&
						01 == *(pBuf+expectNextHead+2));
					VD_BOOL state_second=(0xFB == *(pBuf+expectNextHead+3) ||
						0xFA == *(pBuf+expectNextHead+3) ||
						0xFD == *(pBuf+expectNextHead+3) ||
						0xFC == *(pBuf+expectNextHead+3) ||
						0xF0 == *(pBuf+expectNextHead+3) ||
						0xF2 == *(pBuf+expectNextHead+3));
					frameHeadTruncation=0;
					if(state_first && state_second) 		   
					{
						nOffset=expectNextHead;
						*pCount=nOffset-1;

					}
					else
					{

						myflag=1;
						*pCount=0;
						trace("frame error expectNextHead is %x\n",expectNextHead);
						return MEDIA_STREAM_ERR;
					}
				}
				else if(expectNextHead > nBufferSize)
				{

					while(nOffset < nBufferSize)
					{
						nRet  = FindFrameMarker(pBuf + nOffset, nBufferSize - nOffset, &MP4Code);
						nOffset += nRet + 4;
						if(MP4Code == 0x01FB ||
							MP4Code == 0x01FD ||
							MP4Code == 0x01FA ||
							MP4Code == 0x01FC ||
							MP4Code == 0x01F2 )
						{
							//这次送进来的信息里面不应该有任何视频帧的信息
							myflag=1;
							*pCount=0;
							trace("get frame in other frame  MP4Code is %x expectNextHead is %x\n",MP4Code,expectNextHead);
							return MEDIA_STREAM_ERR;
						}
					}
					expectNextHead-=nBufferSize;
					frameHeadTruncation = 2;
					*pCount=0;
					return MEDIA_STREAM_OK;
				}
				else
				{
					len =nBufferSize-expectNextHead;
					memcpy(data,pBuf+expectNextHead,len);
					*pCount=expectNextHead;
					frameHeadTruncation=1;
					return MEDIA_STREAM_OK;
				}

			}

			//搜索上个文件遗留下来的东西 ，帧头信息保存不完整	 有帧长的信息被打断了
			//那么在data的数据中，保存了前半截的帧头，将后半截的帧头从这次送进去的buffer取出，
			//接上，然后获取帧长信息和下一帧帧头的 偏移
			//下一帧头的偏移  首先获取本帧长度 然后在送进来的缓冲中首先走到“断头帧”的末尾
			//“断头帧”如果是01f0 那么它的末尾是送进来的缓冲偏移4-len 
			if(frameHeadTruncation == 1)
			{				
				memcpy(data+len,pBuf,16-len);
				if(0xF0 == *(data+3))
				{
					if(! HBflag)
						expectNextHead=*(data+6)+(*(data+7))*256+(8-len);
					else
						expectNextHead=1036+4-len;
				}	  
				else if(0xFB == *(data+3) || 0xFD == *(data+3))
					expectNextHead=(data[12]+data[13] *256 + data[14] *256*256 + data[15]  *256*256*256)+16-len; 
				else if(0xFA == *(data+3) || 0xFC == *(data+3))
					expectNextHead=(data[4]+data[5] *256 + data[6] *256*256 + data[7]  *256*256*256)+8-len;
				else if(0xF2 == *(data+3))
					expectNextHead=1036+4-len;
				else
				{
					trace("copy error ! \n");
					myflag=1;
					*pCount=0;	 //xufan
					return MEDIA_STREAM_ERR;
				}
				memset(data,0,16);
				frameHeadTruncation=0;
				if(expectNextHead+4 <nBufferSize)
				{
					VD_BOOL state_first=(00 == *(pBuf+expectNextHead)&&
						00 == *(pBuf+expectNextHead+1) &&
						01 == *(pBuf+expectNextHead+2));
					VD_BOOL state_second=(0xFB == *(pBuf+expectNextHead+3) ||
						0xFA == *(pBuf+expectNextHead+3) ||
						0xFD == *(pBuf+expectNextHead+3) ||
						0xFC == *(pBuf+expectNextHead+3) ||
						0xF0 == *(pBuf+expectNextHead+3) ||
						0xF2 == *(pBuf+expectNextHead+3));
					frameHeadTruncation=0;
					if(state_first && state_second) 		   
					{
						nOffset=expectNextHead;
						*pCount=nOffset-1;

					}
					else
					{

						myflag=1;
						*pCount=0;
						trace("frame error second expectNextHead is %x	*pCount is %x\n",expectNextHead,*pCount);
						return MEDIA_STREAM_ERR;
					}
				}
				else if(expectNextHead > nBufferSize)
				{

					while(nOffset < nBufferSize)
					{
						nRet  = FindFrameMarker(pBuf + nOffset, nBufferSize - nOffset, &MP4Code);
						nOffset += nRet + 4;
						if(MP4Code == 0x01FB ||
							MP4Code == 0x01FD ||
							MP4Code == 0x01FA ||
							MP4Code == 0x01FC ||
							MP4Code == 0x01F2 )
						{
							//这次送进来的信息里面不应该有任何视频帧的信息
							myflag=1;
							*pCount=0;
							trace("get frame in other frame oh bad MP4Code is %x expectNextHead is %x\n",MP4Code,expectNextHead);
							return MEDIA_STREAM_ERR;
						}
					}
					expectNextHead-=nBufferSize;
					frameHeadTruncation = 2;
					*pCount=0;
					return MEDIA_STREAM_OK;
				}
				else
				{
					len =nBufferSize-expectNextHead;
					memcpy(data,pBuf+expectNextHead,len);
					*pCount=expectNextHead;
					frameHeadTruncation=1;
					return MEDIA_STREAM_OK;
				}


			}

			nRet  = FindFrameMarker(pBuf + nOffset, nBufferSize - nOffset, &MP4Code);
			nOffset += nRet + 4;

			if(MP4Code == 0x01F0)
			{
				//expectNextHead:下帧帧头	noff是指向00 00 01 f0的下一个字节,要走到本帧的末尾，加上帧头
				//才能够计算到下一帧帧头，
				if(! HBflag)
				{
					if(nOffset+4 <= nBufferSize)
					{				 
						expectNextHead=nOffset+4+((*(pBuf+nOffset+2) |
							(*(pBuf+nOffset+3)) << 8 ) );				   
					}
					else
					{
						len=nBufferSize-nOffset+4;
						memcpy(data,pBuf+nOffset-4,len);			
						*pCount=(nOffset-4-1) >nBufferSize ? 0:nOffset-4-1;
						frameHeadTruncation=1;
						return MEDIA_STREAM_OK;
					}	
				}
				else
					expectNextHead=nOffset+1036;				 //hb音频数据长度是1k
			}

			else if(MP4Code == 0x01FB || MP4Code == 0x01FD)
			{			
				if(nOffset+12 <= nBufferSize)
				{
					expectNextHead=(nOffset+( (*(pBuf+nOffset+8)) |
						(*(pBuf+nOffset+9)) << 8 |
						(*(pBuf+nOffset+10)) << 16 |
						(*(pBuf+nOffset+11)) << 24 ) )
						+12;				   
				}
				else
				{
					len=nBufferSize-nOffset+4;
					memcpy(data,pBuf+nOffset-4,len);
					*pCount=(nOffset-4-1) >nBufferSize ? 0:nOffset-4-1;
					frameHeadTruncation=1;
					return MEDIA_STREAM_OK;
				}
			}

			else if(MP4Code == 0x01FA || MP4Code == 0x01FC)
			{	
				if(nOffset+4 <= nBufferSize)
				{
					expectNextHead=(nOffset+( (*(pBuf+nOffset+0)) |
						(*(pBuf+nOffset+1)) << 8 |
						(*(pBuf+nOffset+2)) << 16 |
						(*(pBuf+nOffset+3)) << 24 ) )
						+4; 						
				}
				else
				{
					len=nBufferSize-nOffset+4;
					memcpy(data,pBuf+nOffset-4,len);
					*pCount=(nOffset-4-1) >nBufferSize ? 0:nOffset-4-1;
					frameHeadTruncation=1;
					return MEDIA_STREAM_OK;
				}
			}
			else if(MP4Code == 0x01F2)						//add hb code
			{			
				expectNextHead=nOffset + 1036;	 
			}

			if(expectNextHead+4  <= nBufferSize)					  //make sure 4 byte data head can be obtained
			{
				VD_BOOL state_first=(00 == *(pBuf+expectNextHead)&&
					00 == *(pBuf+expectNextHead+1) &&
					01 == *(pBuf+expectNextHead+2));
				VD_BOOL state_second=(0xFB == *(pBuf+expectNextHead+3) ||
					0xFA == *(pBuf+expectNextHead+3) ||
					0xFD == *(pBuf+expectNextHead+3) ||
					0xFC == *(pBuf+expectNextHead+3) ||
					0xF0 == *(pBuf+expectNextHead+3) ||
					0xF2 == *(pBuf+expectNextHead+3));
				if(state_first && state_second) 		   
				{
					nOffset=expectNextHead;
					*pCount=nOffset-1;
				}
				else
				{
					myflag=1;
					*pCount=(nOffset-4-1) >nBufferSize ? 0:nOffset-4-1;
					len=0;
					//trace("expectNextHead %x mp4code %x \n",expectNextHead,MP4Code);
					//trace(" *(pBuf+expectNextHead) is %x	 %x  %x   %x\n",*(pBuf+expectNextHead),*(pBuf+expectNextHead+1),*(pBuf+expectNextHead+2),*(pBuf+expectNextHead+3));
					trace("my god after a frame is not aframe expectNextHead %x\n",expectNextHead);
					return MEDIA_STREAM_ERR;
				}
			}
			else if(expectNextHead	> nBufferSize)
			{
				*pCount=(nOffset-4-1) >nBufferSize ? 0:nOffset-4-1;
				len=0;
				memset(data,0,16);
				frameHeadTruncation=2;
				expectNextHead=expectNextHead-nBufferSize >0 ? expectNextHead-nBufferSize: 0;
				return MEDIA_STREAM_OK;
			}
			else
			{
				len =nBufferSize-expectNextHead;
				memcpy(data,pBuf+expectNextHead,len);
				*pCount=expectNextHead;
				frameHeadTruncation=1;
				return MEDIA_STREAM_OK;
			}
		}				
	}

	else		  //old mpeg4
	{
		nOffset=0;
		uint MP4Code = 0xffffffff;
		while(nOffset < nBufferSize) 
		{		
			frameHeadTruncation=3;
			int nHour, nMinute, nSecond;
			nRet  = FindFrameMarker(pBuf + nOffset, nBufferSize - nOffset, &MP4Code);
			nOffset += nRet + 4;

			if(MP4Code ==0x01FA ||MP4Code ==0x01FB ||MP4Code ==0x01FC ||MP4Code ==0x01FD)
			{
				myflag=1;
				*pCount=0;
				trace("old to new error!\n");
				trace("nOffset is %x\n",nOffset);
				return MEDIA_STREAM_ERR;
			}
			if(0x0100 == MP4Code)
			{	

				nRet =	FindFrameMarker(pBuf + nOffset+4, nBufferSize - nOffset, &MP4Code);
				if(MP4Code == 0x01B3) //get time
				{
					unsigned char *pTimeBuf = pBuf + nOffset +4 + nRet + sizeof(uint);
					VD_BOOL SOME_ERROR=0;
					FIRST_IFRAME++;
					nHour	= *(pTimeBuf)>>3;										//时
					nMinute = ( ( *(pTimeBuf) & 0x07) << 3) | ( *(pTimeBuf + 1)>>5);		//分
					nSecond = ( ( *(pTimeBuf + 1) & 0x0f) << 2) | ( *(pTimeBuf + 2)>>6);	//秒

					SOME_ERROR=(nHour*60*60 + nMinute*60 + nSecond > last_hour*60*60 + last_minute*60 + last_second + 10 ||
						nHour*60*60 + nMinute*60 + nSecond < last_hour*60*60 + last_minute*60 + last_second );

					last_hour=nHour;
					last_minute=nMinute;
					last_second=nSecond;

					if(SOME_ERROR && FIRST_IFRAME>1)
					{
						trace(" time out old mpeg4 error  \n");
						*pCount=0;
						myflag=1;
						return MEDIA_STREAM_ERR;
					}

					else
					{
						pos=nOffset-4-1;
						*pCount=pos;
					}
				}
			}		
		}
		return MEDIA_STREAM_OK;
	}
	return MEDIA_STREAM_OK;
}

