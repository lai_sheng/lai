#ifndef __CONFIG_GUISET__
#define __CONFIG_GUISET__

#include "ConfigBase.h"

/*add by yanjun 20070406*/
typedef struct __ts_calibrate
{
	VD_POINT	ptDisplay[3];		/*!< 图形显示坐标, 相对(0,0,RESOLUTION_MAX_X,RESOLUTION_MAX_Y) */
	VD_POINT	ptScreen[3];		/*!< 触摸屏原始坐标 */
}TS_CALIBRATE;
//!GUI设置
typedef struct tagCONFIG_GUISET
{
	//!窗口透明度	[128, 255]
	int iWindowAlpha;
	//!时间标题显示使能
	VD_BOOL bTimeTitleEn;
	//!通道标题显示使能	
	VD_BOOL bChannelTitleEn;
	//! 报警状态
	VD_BOOL bAlarmStatus;
	//! 录像状态显示使能
	VD_BOOL bRecordStatus;	
	//触摸屏
	TS_CALIBRATE ts_Calibrate;
	//录像标志显示使能
	VD_BOOL bChanStateRecEn;
	//视频丢失标志显示使能
	VD_BOOL bChanStateVlsEn;
	//通道锁定标志显示使能
	VD_BOOL bChanStateLckEn;
	//动态检测标志显示使能
	VD_BOOL bChanStateMtdEn;
	//码流显示使能
	VD_BOOL bBitRateEn;
#ifdef GRAPHICS_SUPPORT_SETRESOLUTION
	// 分辨率
	int resolution;
#endif
	//数字通道显示
	//!时间标题显示使能
	VD_BOOL bDigiTimeTitleEn;
	//!通道标题显示使能	
	VD_BOOL bDigiChannelTitleEn;
	//! 报警状态
	VD_BOOL bDigiAlarmStatus;
	//! 录像状态显示使能
	VD_BOOL bDigiRecordStatus;
	//!登录才能预览使能, 此处使能时，不登录的情况不能预览
	VD_BOOL bLoginPreview;
} CONFIG_GUISET;

typedef TConfig<CONFIG_GUISET, 1, 4> CConfigGUISet;

void CalibrateExchange(CConfigExchange& configExchange, CConfigTable& table,TS_CALIBRATE& ts);

#endif //__CONFIG_GUISET__

