#ifndef __CONFIG_RECORD__
#define __CONFIG_RECORD__

#include "ConfigBase.h"

//!录像设置
typedef struct tagCONFIG_RECORD
{
	int iPreRecord;			/*!< 预录时间，为零时表示关闭 */	
	VD_BOOL bRedundancy;		/*!< 冗余开关 */
	VD_BOOL bSnapShot;			/*!< 快照开关 */	
	int iWsName;			/*!< 选中的工作表名 */
	int iPacketLength;			/*!< 录像打包长度（分钟）[1, 255] */
} CONFIG_RECORD;

typedef TConfig<CONFIG_RECORD, N_SYS_CH, 4> CConfigRecord;


#endif //__CONFIG_RECORD__

