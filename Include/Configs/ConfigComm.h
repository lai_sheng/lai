#ifndef __CONFIG_COMM__
#define __CONFIG_COMM__

#include "ConfigBase.h"

/*! 串口功能，注意一定要和界面上对应，否则出错*/
enum confunc
{
	CONSOLE = 0,
	VN_KBD,
	COM_TYPES,
};

typedef struct tag_COMMATTRI
{
	/*!< 数据位 取值为5,6,7,8 */
	int	nDataBits;
	/*!< 停止位 参照comm_stopbits_t */
	int	iStopBits;
	/*!< 校验位 参照comm_parity_t*/
	int	iParity;
	/*!< 实际波特率*/
	int	nBaudBase;			
} COMMATTRI;

//!串口配置
typedef struct tagCONFIG_COMM_X
{
	/*!< 串口协议:
	“Console”  */
	int iProtocolName;
	/*!< 端口号 */
	int iPortNo;
	/*!< 串口属性 */
	COMMATTRI iCommAttri;	
} CONFIG_COMM_X;


enum KBDFunc
{
    KBDFunc_PTZ_RAW_DATA_IN  = 0,/*透明串口*//*云台控制命令的透明传输*/
    KBDFunc_DHKBD = 1  ,/*大华键盘协议*/
    KBDFunc_HK = 2 /*海康键盘协议*/
};


//!控制键盘串口配置
typedef struct tagCONFIG_KB_X
{
	/*!< 键盘协议:*/    
	int iProtocol;

    /*!< 串口属性 */
	COMMATTRI iCommAttri;
} CONFIG_KB_X;


/*用于网络*/
typedef struct 
{
    uchar Com_Version[8]; /*!< 版本号 */
    uchar Function; /*!< 串口功能 参照枚举KBDFunc*/
    uchar DataBits; /*!< 数据位 取值为5,6,7,8 */
    uchar StopBits; /*!< 停止位 参照comm_stopbits_t(DVRAPI.H) */
    uchar Parity; /*!< 校验位 参照comm_parity_t(DVRAPI.H) */
    uint  BaudBase; /*!< 波特率 {300,600,1200,2400,4800,9600,19200,38400,57600,115200}　*/
}KBD_CONFIG;

typedef TConfig<CONFIG_COMM_X, 1, 4> CConfigComm;

typedef TConfig<CONFIG_KB_X, 1, 4> CConfigKbd;


#ifdef _USE_720P_MODULE_
void CommExchange_vp26m2h(CConfigExchange& configExchange, CConfigTable& table, COMMATTRI& commattri);
#endif

void CommExchange(CConfigExchange& configExchange, CConfigTable& table, COMMATTRI& commattri);
template<> void exchangeTable<CONFIG_KB_X>(CConfigExchange& xchg, CConfigTable& table, CONFIG_KB_X& config, int index, int app);

#endif //__CONFIG_COMM__

