#ifndef __CONFIG_WORKSHEET__
#define __CONFIG_WORKSHEET__

#include "ConfigBase.h"

/*! 
	把整个工作表分成４类，
	前N_SYS_CH为录像使用，
	N_SYS_CH+1~2*N_SYS_CH为报警使用，
	2*N_SYS_CH+1~3*N_SYS_CH为动检使用
	3*N_SYS_CH+1~4*N_SYS_CH为其他共用
*/
enum WorksheetName
{
	WSNameREC = 0,
	WSNameALM = 1,
	WSNameMTD = 2,
	WSNameBLD = 3,
	WSNameVLT = 4,
	WSNameSnap = 6,
	
	WSNameDecAlm = 8,
	WSNameSuva = 9,
	WSNameALMNet = 10,	
	WSNameLed = 11,
	WSNameSchedule = 12,
	WSHemuAlarm = 13,
	WSNaneNum,
};

typedef struct tagWORKSHEET
{
	int			iName;							/*!< 时间表名称 */	
	TIMESECTION	tsSchedule[N_WEEKS][N_TSECT];	/*!< 时间段 */
}CONFIG_WORKSHEET;

/*界面显示使用*/
typedef struct tagUI_WORKSHEET_SCHEDULE
{
	TIMESECTION	tsSchedule[N_WEEKS][N_UI_TSECT];	/*!< 时间段 */
}UI_WORKSHEET_SCHEDULE;

/*网络通讯时结构体*/
typedef struct tagUI_WORKSHEET
{
    int			iName;
	TIMESECTION	tsSchedule[N_WEEKS][N_UI_TSECT];	/*!< 时间段 */
}UI_WORKSHEET;

typedef struct tagRECState
{
	VD_BOOL	recstate[N_WEEKS][N_TSECT];	/*!时间表中设定动检，报警录像的状态，使用低两位，0位动检 1位报警(其中0代表未选中，1代表选中)*/
}CONFIG_RECSTATE;

typedef TConfig<CONFIG_RECSTATE, N_SYS_CH, 4, 0>CConfigRECState;


typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameREC> CConfigRECWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_ALM_IN, 4, WSNameALM> CConfigALMWorksheet; 
typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameMTD> CConfigMTDWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameBLD> CConfigBLDWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameVLT> CConfigVLTWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameSnap> CConfigSnapWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_SYS_CH, 4, WSNameDecAlm> CConfigDecAlmWorksheet;
typedef TConfig<CONFIG_WORKSHEET, N_ALM_IN, 4, WSNameALMNet> CConfigNetAlmWorksheet; 

#ifdef POLICE_PROJECT 
typedef TConfig<CONFIG_WORKSHEET, 1, 4, WSNameLed> CConfigLEDWorksheet; 
#endif
typedef TConfig<CONFIG_WORKSHEET, 1, 4, WSNameSchedule> CConfigScheduleWorksheet; 
typedef TConfig<CONFIG_WORKSHEET, 1, 4, WSHemuAlarm> CConfigHemuAlarmWorksheet; 


void exchangeTimeSection(CConfigExchange& configExchange, CConfigTable& table, TIMESECTION& timesection);

bool SetConfigWSSchedule(CONFIG_WORKSHEET *pConfigWS,UI_WORKSHEET_SCHEDULE *pUISh);
bool GetUIWSSchedule(CONFIG_WORKSHEET *pConfigWS,UI_WORKSHEET_SCHEDULE *pUISh);

#endif
