#ifndef __CONFIG_ENCODE__
#define __CONFIG_ENCODE__

#include "ConfigBase.h"
#include "APIs/Capture.h"


//#define ENCODE_MAX_BITRATE 7200 //修改为7200，避免帧率无法设置到30帧
//LE0 20150205 升级8k 修改支持最大码率为32M
#define ENCODE_MAX_BITRATE 32768
#define ENCODE_DFT_BITRATE 512

//! 编码配置的类型
enum ENCODE_TYPE_BY_RECORD
{
	ENCODE_TYPE_TIM = 0,
	ENCODE_TYPE_MTD = 1,
	ENCODE_TYPE_ALM = 2,
	ENCODE_TYPE_NUM = 3,
	ENCODE_TYPE_SNAP_TIMER = 0,
	ENCODE_TYPE_SNAP_TRIGGER = 1,
};

#define EXTRATYPES 1 //辅码流类型 3  
typedef struct  tagVIDEO_FORMAT
{
	int		iCompression;			/*!< 压缩模式 */	
	int		iResolution;			/*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */	
	int		iBitRateControl;		/*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */	
	int		iQuality;				/*!< 码流的画质 档次1-6	*/	
	int		nFPS;					/*!< 帧率值，NTSC/PAL不区分,负数表示多秒一帧*/		
	int		nBitRate;				/*!< 0-4096k,该列表主要由客户端保存，设备只接收实际的码流值而不是下标。*/
	int		iGOP;					/*!< 描述两个I帧之间的间隔时间，2-12 */
} VIDEO_FORMAT;

typedef struct  tagAUDIO_FORMAT
{
	int		nBitRate;				/*!< 码流kbps*/	
	int		nFrequency;				/*!< 采样频率*/	
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30
	unsigned char ucLAudioVolumn;   //左声道音量
	unsigned char ucRAudioVolumn;   //右声道音量，单声道的设备左右值一样
	unsigned char ucRes[2];
#else
	int		nMaxVolume;				/*!< 最大音量阈值*/
#endif
} AUDIO_FORMAT;

//!媒体格式
typedef struct  tagMEDIA_FORMAT
{
	VIDEO_FORMAT vfFormat;			/*!< 视频格式定义 */			
	AUDIO_FORMAT afFormat;			/*!< 音频格式定义 */
	VD_BOOL	bVideoEnable;			/*!< 开启视频编码 */
	VD_BOOL	bAudioEnable;			/*!< 开启音频编码 */	
} MEDIA_FORMAT;

//!编码设置
typedef struct tagNET_ENCODE
{
	int  iChannel;
	int iSteamIndex;   
	/*!<码流格式 
	主码流 参考枚举参考ENCODE_TYPE_BY_RECORD ，现有产品默认只有一个主码流，填充为0；
	副码流 参考 ENCODE_TYPE_BY_SUBSTREAM，最多4种子码流，现在产品最多支持一种副码流，填充为0；
	捉图码流 参考 ENCODE_TYPE_BY_SUBSTREAM，最多从4种码流中进行捉图配置
	*/	
	MEDIA_FORMAT dstFmt;		/*!<码流格式 */	

} NET_ENCODE;


//!编码设置
typedef struct tagCONFIG_ENCODE
{
	MEDIA_FORMAT dstMainFmt[ENCODE_TYPE_NUM];		/*!< 主码流格式 */	
	MEDIA_FORMAT dstExtraFmt[EXTRATYPES];	/*!< 辅码流格式 */
	MEDIA_FORMAT dstSnapFmt[ENCODE_TYPE_NUM];		/*!< 抓图格式 */
} CONFIG_ENCODE;


#define MAX_SNAP_NUM 4
/*抓拍参数配置*/
typedef struct tagCONFIG_SNAP
{   
	/*定时抓拍使能*/
	int Enable;
	
	/*抓拍图像质量，初步分6档， 1 差 2 一般 3普通， 4好， 5很好， 6最好*/
	int SnapQuality;

	/*!< 定时抓图的时间间隔，以100毫秒为单位，
	设备传输按照能力集 现有设备支持500ms一张图片，参数为5~Max */
       int SnapInterval;
	
	/*抓拍张数*/
	int SnapCount;

} CONFIG_SNAP;

typedef TConfig<CONFIG_ENCODE, 1, 1> CConfigEncode;
typedef TConfig<CONFIG_SNAP,1,1> CConfigSnap;

#ifdef SHREG
/*联动抓拍参数配置*/
typedef struct tagEVENT_CONFIG_SNAP
{   
	/*联动抓拍使能*/
	int Enable;	
	/*报警前抓拍多少秒*/
	int SnapBeforeSec;
	/*报警后抓拍多少秒*/
    int SnapafterwordsSec;
	
	/*定时抓图(时间段抓图)设置*/
    TIMESECTION TimerSnap;
	int User;//0 验收人员、1 维保人员、2 操作人员
	char Reference[200];
}EVENT_CONFIG_SNAP;
typedef TConfig<EVENT_CONFIG_SNAP, N_SYS_CH, 4> CConfigEventSnap;

#endif

int getMaxFrame();
int getMaxDefaultFrame(MEDIA_FORMAT& dftFormat);
int getRealCompress(int iCompress);
int getRealImageSize(int imgSize);
int GetImageSize(VD_SIZE *pSize, int imgtype);

int getImageBitRate(int imageType, int frames, int &minBitRate, int&maxBitRate);

uint calculatePower(uchar imgtype, uchar frame);

uint getChannelPower(CONFIG_ENCODE* pConfig);

int GetStreamPower(CONFIG_ENCODE* pConfig, uint type);

//获取当前通道从DSP中能够得到的最大编码能力.
uint getChannelRemainedPower(CONFIG_ENCODE *pConfig, int iChannel);

int VD_round(int value, int divisor);


// 处理音频输入编码配置
typedef struct tagCONFIG_AUDIOIN_FORMAT
{
	uint BitRate;			/**< 码流大小，kbps为单位，比如192kbps，128kbps。*/
	uint SampleRate; 		/**< 采样率，Hz为单位，比如44100Hz。*/
	uint SampleBit;  		/*!< 采样的位深 */
	int  EncodeType;  		/*!< 编码方式，参照audio_encode_type定义 */
	uint AudioSourceType;  	/*!< 声音源方式，0为线性输入，1为mic *///audio qjwang 091029
	uint Silence;			///< 静音 1--静音 0--非静音
	int	 PlayAudioEnable;	///< 回放伴音使能
	int  DisplayAudioEnable;///< 预览伴音使能
	uint LAudioVolumn;   //左声道音量
	uint RAudioVolumn;   //右声道音量，单声道的设备左右值一样
	uint LongtimeBeepEnable;///是否启动蜂鸣器长鸣功能
} CONFIG_AUDIOIN_FORMAT;
typedef TConfig<CONFIG_AUDIOIN_FORMAT, 2, 4> CConfigAudioInFormat;


typedef struct
{
	unsigned short usTotal;  //total channel number [总通道数目]
	unsigned short usIndex;  //which channel's information [第几个通道信息]
}RV_DEV_CHANNEL;

typedef struct
{
    int type;
	int len;
}sSubType;

#ifdef _2761_EXCEPTION_CHK_
typedef  struct _encoder_detecte_   
{
	unsigned char bEnable;
	unsigned char Res[15] ;
}ENCODER_DETCETE;
typedef TConfig<ENCODER_DETCETE, 1, 4> CConfigEncoderDetect;

#endif


#endif //__CONFIG_GROUP__

