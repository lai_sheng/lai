#ifndef __CONFIG_VIDEOWIDGET__
#define __CONFIG_VIDEOWIDGET__

#include "ConfigBase.h"

#define COVERNUM 8 //覆盖区域数

//!视频物件结构
typedef struct  tagVIDEO_WIDGET
{
	
	VD_COLORREF rgbaFrontground;		/*!< 物件的前景RGB，和透明度 */	
	VD_COLORREF rgbaBackground;		/*!< 物件的后景RGB，和透明度*/	
	VD_RECT	rcRelativePos;			/*!< 物件边距与整长的比例*8191 */	
	VD_BOOL	bPreviewBlend;			/*!< 预览叠加 */	
	VD_BOOL	bEncodeBlend;			/*!< 编码叠加 */
} VIDEO_WIDGET;

typedef struct  tagNET_VIDEO_WIDGET
{
	unsigned short  usChannel;
	unsigned short	 usRes;	
	VIDEO_WIDGET	dstWidget;
} NET_VIDEO_WIDGET;

//区域遮挡
#define COVERNUM 8
#define MARKERNUM 1
typedef struct  tagNET_VIDEO_COVER
{
	unsigned short usChannel;
	unsigned short	 usCoverNum;	/*!< 当前该通道有几个叠加的区域 */
	VIDEO_WIDGET	dstCovers[COVERNUM];
} NET_VIDEO_COVER;

//!视频物件设置
typedef struct tagCONFIG_VIDEOWIDGET
{
	VIDEO_WIDGET	dstCovers[COVERNUM];
	VIDEO_WIDGET	ChannelTitle;
	VIDEO_WIDGET	TimeTitle;
	int				iCoverNum;		/*!< 当前该通道有几个叠加的区域 */
	VIDEO_WIDGET	dstMarkers[MARKERNUM];
	int             iMarkerNum;     /*!< 当前该通道有几个数字水印，当前只支持一个 */
} CONFIG_VIDEOWIDGET;

typedef TConfig<CONFIG_VIDEOWIDGET, N_SYS_CH, 4> CConfigVideoWidget;

void exchangeRect(CConfigExchange& configExchange, CConfigTable& table, VD_RECT& rect);
#endif //__CONFIG_VIDEOWIDGET__
