#ifndef __CONFIG_DEVICEEXCEPTION__
#define __CONFIG_DEVICEEXCEPTION__

#include "ConfigBase.h"
#include "APIs/Analyse.h"
#include "APIs/va.h"

enum ptz_link_type
{
	PTZ_LINK_NONE,			/*!< 不需要联动 */
	PTZ_LINK_PRESET,			/*!< 转至预置点 */
	PTZ_LINK_TOUR,			/*!< 巡航 */
	PTZ_LINK_PATTERN			/*!< 轨迹 */
};

//! 云台联动结构
typedef struct tagPTZ_LINK
{
	int iType;				/*!< 联动的类型 */
	int iValue;				/*!< 联动的类型对应的值 */
}PTZ_LINK;

//! 事件处理结构
typedef struct tagEVENT_HANDLER
{
	uint	dwRecord;				/*!< 录象掩码 */
	int		iRecordLatch;			/*!< 录像延时：10～300 sec */ 	
	uint	dwTour;					/*!< 轮巡掩码 */	
	uint	dwSnapShot;				/*!< 抓图掩码 */
	uint	dwAlarmOut;				/*!< 报警输出通道掩码 */
	int		iAOLatch;				/*!< 报警输出延时：10～300 sec */ 
	PTZ_LINK PtzLink[N_SYS_CH];		/*!< 云台联动项 */

	VD_BOOL	bRecordEn;				/*!< 录像使能 */
	VD_BOOL	bTourEn;				/*!< 轮巡使能 */
	VD_BOOL	bSnapEn;				/*!< 抓图使能 */	
	VD_BOOL	bAlarmOutEn;			/*!< 报警使能 */
	VD_BOOL	bPtzEn;					/*!< 云台联动使能 */
	VD_BOOL	bTip;					/*!< 屏幕提示使能 */	
	VD_BOOL	bMail;					/*!< 发送邮件 */	
	VD_BOOL	bMessage;				/*!< 发送消息到报警中心 */	
	VD_BOOL	bBeep;					/*!< 蜂鸣 */	
	VD_BOOL	bVoice;					/*!< 语音提示 */		
	VD_BOOL	bFTP;					/*!< 启动FTP传输 */

	int		iWsName;				/*!< 时间表的选择，由于时间表里使用数字做索引，且不会更改 */
	uint	dwMatrix;				/*!< 矩阵掩码 */
	VD_BOOL	bMatrixEn;				/*!< 矩阵使能 */
	VD_BOOL	bLog;					/*!< 日志使能，目前只有在WTN动态检测中使用 */
	int		iEventLatch;			/*!< 联动开始延时时间，s为单位 */
	VD_BOOL	bMessagetoNet;			/*!< 消息上传给网络使能 */
	uint	dwReserved[7]; 			/*!< 保留字节 */
} EVENT_HANDLER;

//!基本事件结构
typedef struct tagCONFIG_GENERIC_EVENT
{
	//!处理开启
	VD_BOOL enable;
	//!处理参数
	EVENT_HANDLER handler;
} CONFIG_GENERIC_EVENT;

//!报警配置结构
typedef struct tagCONFIG_ALARM_X
{
	//!报警输入开关
	VD_BOOL	bEnable;
	//!传感器类型常开 or 常闭
	int		iSensorType;
	//!报警联动
	EVENT_HANDLER hEvent;
} CONFIG_ALARM;

#define MAX_ALARMDECODER_NUM 8	//报警解码器数量
#define ALARMDEC_OUT_SLOTS_NUM 8
#define ALARMDEC_IN_SLOTS_NUM 8
#define ALARMDEV_OUT_SLOT_MAP_NUM 32 // 报警输出通道映射
//!报警解码器配置
typedef struct tagCONFIG_ALARMDECODER
{
	//!解码器地址
	int iAddress;
	//!使能
	VD_BOOL bEnable;
	//!映射到本地报警输出的通道号，可以重叠，但不能断开
	int iOutSlots[ALARMDEC_OUT_SLOTS_NUM];
	//!内容同报警配置
	CONFIG_ALARM InSlots[ALARMDEC_IN_SLOTS_NUM];
} CONFIG_ALARMDECODER;

//!遮挡检测配置
typedef struct tagCONFIG_BLINDDETECT
{
	//!遮挡检测开启
	VD_BOOL	bEnable;
	//!灵敏度：1～6
	int		iLevel;
	//!遮挡检测联动
	EVENT_HANDLER hEvent;
} CONFIG_BLINDDETECT;

#define MD_REGION_ROW 32

//!动态检测设置
typedef struct tagCONFIG_MOTIONDETECT
{
	VD_BOOL bEnable;							/*!< 动态检测开启 */
	int iLevel;								/*!< 灵敏度 */
	uint mRegion[MD_REGION_ROW];			/*!< 区域，每一行使用一个二进制串 */	
	EVENT_HANDLER hEvent;					/*!< 动态检测联动 */
} CONFIG_MOTIONDETECT;

//!硬盘容量不足事件结构
typedef struct tagCONFIG_STORAGE_SPACE_EVENT
{
	//!处理开启
	VD_BOOL enable;
	//!硬盘剩余容量下限, 百分数
	int lowerLimit;
	//!处理参数
	EVENT_HANDLER handler;
} CONFIG_STORAGE_SPACE_EVENT;

//!动态检测设置
typedef struct tagCONFIG_ALARMSWITCH
{
	VD_BOOL bCryEnable;				/*!< 哭声检测开启 */				
	VD_BOOL bMotionEnable;			/*!< 动态检测开启 */				
	VD_BOOL bHumanEnable;			/*!< 人形检测开启 */	
	VD_BOOL bVoiceEnable;			/*!< 声音检测开启 */
	VD_BOOL bTraceEnable;			/*!< 动态跟踪开启 */
	VD_BOOL bTimeSectionEnable;		/*!< 时间段开启 */
	uint	iTraceTimeOut;      /*!< 人型跟踪超时时间*/
} CONFIG_ALARMSWITCH;

typedef TConfig<CONFIG_ALARM, N_ALM_IN, 4, appEventAlarmLocal> CConfigAlarm;
typedef TConfig<CONFIG_ALARM, N_ALM_IN, 4, appEventAlarmNet> CConfigNetAlarm;
typedef TConfig<CONFIG_GENERIC_EVENT, N_SYS_CH, 4, appEventAlarmDecoder> CConfigDecoderAlarm;
typedef TConfig<CONFIG_BLINDDETECT, N_SYS_CH, 4, appEventVideoBlind> CConfigBlindDetect;
typedef TConfig<CONFIG_GENERIC_EVENT, N_SYS_CH, 4, appEventVideoLoss> CConfigLossDetect;
typedef TConfig<CONFIG_MOTIONDETECT, 4, 4, appEventVideoMotion> CConfigMotionDetect;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventStorageNotExist> CConfigStorageNotExist;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventStorageFailure> CConfigStorageFailure;
typedef TConfig<CONFIG_STORAGE_SPACE_EVENT, 1, 4, appEventStorageLowSpace> CConfigStorageLowSpace;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventNetAbort> CConfigNetAbort;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventEncoderError> CConfigEncoderError;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventCDNoSpace> CConfigCDNoSpace;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventNetArp> CConfigNetArp;
typedef TConfig<CONFIG_GENERIC_EVENT, 1, 4, appEventDecConnect> CConfigDecConnect;

typedef TConfig<CONFIG_ALARMSWITCH, 1, 4> CConfigAlarmSwitch;
#endif //__CONFIG_DEVICEEXCEPTION__

