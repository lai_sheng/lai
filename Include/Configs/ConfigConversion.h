
#ifndef __CONFIG_CONVERSION_H__
#define __CONFIG_CONVERSION_H__

#include "APIs/DVRDEF.H"
#include "Functions/Ptz.h"
#include "System/Object.h"
#include "System/File.h"
#include "ConfigVideoColor.h"
#include "ConfigPTZ.h"
#include "ConfigEncode.h"
#include "ConfigVideoWidget.h"
#include "ConfigChannelTitle.h"
#include "ConfigAutoMaintain.h"
#include "ConfigPlay.h"
//#include "Net/Dlg/DlgDDNSCli.h"
#include "Net/NetConfig.h"
#include "Devices/DevSplit.h"

//! 普通配置信息
typedef struct __config_general 
{
	uchar	Version[8];		/*!< 版本号 */
	ushort	LocalNo;		/*!< 本机编号 0-998 */
	uchar	VideoFmt;		/*!< 制式 参照枚举video_standard_t(DVRAPI.H)  */
	uchar	Language;		/*!< 语言选择 参照language定义(DVRAPI.H) */

	uchar	OverWrite;		/*!< 盘满时　1　覆盖,　0　停止 */
	uchar	RecLen;			/*!< 录象段长度 1-120 */
	uchar	StandbyTime;	/*!< GUI待命时间 0-60 */
	uchar	DateFmt;		/*!< 日期格式 参照枚举date_fmt定义(DVRAPI.H) */
	uchar	DateSprtr;		/*!< 日期分割符 参照枚举date_spec定义(DVRAPI.H)*/
	uchar	TimeFmt;		/*!< 时间格式 参照枚举time_fmt定义(DVRAPI.H)*/
	uchar	DST;			/*!< 是否实行夏令时 1-实行 0-不实行, 具体规则脚本中定义*/

	uchar	ReservedB[3];

} 
#ifdef LINUX
__attribute__((packed))
#endif
CONFIG_GENERAL_OLD;

//! 颜色设置内容
typedef struct 
{
	TSECT 	Sector;				/*!< 对应的时间段*/
	uchar	Brightness;			/*!< 亮度	0-100		*/
	uchar	Contrast;			/*!< 对比度	0-100		*/
	uchar	Saturation;			/*!< 饱和度	0-100		*/
	uchar	Hue;				/*!< 色度	0-100		*/
	uchar	Gain;				/*!< 增益	0-100		*/
	uchar	Reserve[3];
}COLOR_PARAM;

//! 颜色结构
typedef struct  
{
	uchar ColorVersion[8];	
	COLOR_PARAM Color[N_COLOR_SECTION];
}CONFIG_COLOR_OLD;



//! 串口配置信息
typedef struct 
{
	uchar	Com_Version[8];		/*!< 版本号 */
	uchar	Function;			/*!< 串口功能 参照枚举confunc(DevComm.h) */
	uchar	DataBits;			/*!< 数据位 取值为5,6,7,8 */
	uchar	StopBits;			/*!< 停止位 参照comm_stopbits_t(DVRAPI.H) */
	uchar	Parity;				/*!< 校验位 参照comm_parity_t(DVRAPI.H) */
	uint	BaudBase;			/*!< 波特率 {300,600,1200,2400,4800,9600,19200,38400,57600,115200}的下标　*/
} CONFIG_COMM;

//! 云台配置信息
typedef struct {
	uchar		Ptz_Version[8];		/*!< 版本号 */
	PTZ_ATTR	PTZAttr;			/*!< 串口属性　*/
	uint		DestAddr;			/*!< 目的地址  */
	uint		MonitorAddr;		/*!< 监视器地址 */
	char		Name[16];	/*!< 协议名称 */
	uchar		Reserved[8];		/*!< 保留 */
}CONFIG_PTZ_OLD;

typedef struct  
{
	uchar		Ptz_Alarm_Version[8];
	PTZ_ATTR	PTZAttr;
	uint		Address;
	char		Name[16];
	uchar		Reserved[8];
}CONFIG_PTZALARM_OLD;

typedef struct {
	VER_CONFIG sVerConfig;		/*!< 配置文件版本信息 */
	uchar	CardInfoEn;			/*!< ATM卡号叠加信息显示使能 */
	uchar	WindowAlpha;		/*!< 窗口透明度：200～255 */
	uchar	TimeTitleEn;		/*!< 时间标题显示使能 */
	uchar	ChannelTitleEn;		/*!< 通道标题显示使能 */
	uchar	TVMarginLeft;		/*!< TV左边距：0～100 */
	uchar	TVMarginTop;		/*!< TV上边距：0～100 */
	uchar	TVMarginRight;		/*!< TV右边距：0～100 */
	uchar	TVMarginBottom;		/*!< TV下边距：0～100 */
	uchar	TVBrightness;		/*!< 亮度：0～100 */
	uchar	TVContrast;			/*!< 对比度：0～100 */
	uchar	TVAntiDither;		/*!< 去抖动：0～100 */
	//uchar	ReservedB2[5];
	/*changed by yanjun 20061218
	ScreenPtcInfo[0],屏保开启使能
	ScreenPtctInfo[1],屏保开启时间*/
	uchar	ScreenPtctEn;		/*!< 屏保使能标志 */
	uchar       ScreenPtctTime;	/*!< 屏保时间 */
	uchar	ReservedB2[3];		/*!< 保留 */
	//uint	ChannelLock;		/*!< 通道锁定 */
	VD_BOOL	bTour;				/*!< 进行轮训使能 */
	uint	TourInterval;		/*!< 轮巡时间间隔：5～120 sec */
	uint	TourMask[N_SPLIT];	/*!< 轮巡掩码 */
} CONFIG_DISPLAY_OLD;

typedef struct {
	VER_CONFIG sVerConfig;		/*! 配置文件版本信息 */
	uchar    Title[NAME_LEN];	/*! 通道名称 */
} CONFIG_TITLE_OLD;

typedef struct __config_ts_calibrate
{
	uchar	Version[8];		/*!< 版本号 */
	VD_POINT	Display[3];		/*!< 图形显示坐标, 相对(0,0,RESOLUTION_MAX_X,RESOLUTION_MAX_Y) */
	VD_POINT	Screen[3];		/*!< 触摸屏原始坐标 */
}CONFIG_TS_CALIBRATE_OLD;

typedef struct _CONFIG_AUTO
{   
    VER_CONFIG verConfig;		/*! 配置文件版本信息 */
	int		AutoRebootDay;		/*! 0-从不, 1-每天, 2-每星期日, 3-每星期一,..... */
	int		AutoRebootTime;		/*! 0-0:00 1-1:00,........23-:23:00 */
	int		AutoDeleteFilesTime;	/*! 0-从不, 1-24H, 2-48H, 3-72H, 4-96H, 5-ONE WEEK, 6-ONE MONTH */
}CONFIG_AUTO_OLD;

typedef struct _CONFIG_AUTO_CLIENT
{
    uchar      Reserved[8];
	uchar		AutoRebootDay;		/*! 0-从不, 1-每天, 2-每星期日, 3-每星期一,..... */
	uchar		AutoRebootTime;		/*! 0-0:00 1-1:00,........23-:23:00 */
	uchar		AutoDeleteFilesTime;	/*! 0-从不, 1-24H, 2-48H, 3-72H, 4-96H, 5-ONE WEEK, 6-ONE MONTH */
	uchar 	    reserved[13];	        /*!为了兼容客户端的结构*/
}CONFIG_AUTO_OLD_CLIENT;

enum rec_type
{
	REC_TYP_TIM = 0,		/*定时录像*/
	REC_TYP_MTD,					
	REC_TYP_ALM,
	REC_TYP_NUM,
};

enum enc_cover_type
{
	ENC_COVER_NONE = 0,
	ENC_COVER_PREVIEW = 0x1,
	ENC_COVER_MONITOR = 0x2,
	ENC_COVER_NUM = 0x3
};

//! 编码选项
typedef struct  
{
	uchar	ImageSize;			/*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */
	uchar	BitRateControl;		/*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */
	uchar	ImgQlty;			/*!< 码流的画质 档次1-6		*/
	uchar	Frames;				/*!< 帧率　档次N制1-6,P制1-5 */	
	uchar	AVEnable;			/*!< 音视频使能 1位为视频，2位为音频。ON为打开，OFF为关闭	*/
	uchar	IFrameInterval;		/*!<I帧间隔 */	
	ushort	usBitRate;			/*!<码流限制大小,从0 - 4096K */	
}ENCODE_OPTION;

//! 标题结构
typedef struct  
{
	uint	TitlefgRGBA;			/*!< 标题的前景RGB，和透明度 */
	uint	TitlebgRGBA;		/*!< 标题的后景RGB，和透明度*/
	ushort	TitleLeft;			/*!< 标题距左边的距离与整长的比例*8192 */
	ushort	TitleTop;			/*!< 标题的上边的距离与整长的比例*8192 */
	ushort	TitleRight;			/*!< 标题的右边的距离与整长的比例*8192 */
	ushort	TitleBottom;			/*!< 标题的下边的距离与整长的比例*8192 */
	uchar	TitleEnable;			/*!< 标题使能 */
	uchar	Reserved[3];
}ENCODE_TITLE;

//! 编码信息结构(双码流在用)
typedef struct {
	uchar				CapVersion[8];				/*!< 版本号			*/		
	ENCODE_OPTION		MainOption[REC_TYP_NUM];	/*!< 主码流，REC_TYP_NUM不同录像类型*/
	ENCODE_OPTION		AssiOption[EXTRATYPES];	/*!< 支持3 路辅码流 */
	uchar				Compression;				/*!< 压缩模式 */;	
	uchar    			CoverEnable;				/*!< 区域遮盖开关　0x00不使能遮盖，0x01仅遮盖预览，0x10仅遮盖录像，0x11都遮盖	*/
	uchar 				alignres[2];			/*!< 保留对齐用 */
	VD_RECT				Cover;						/*!< 区域遮盖范围	*/	

	ENCODE_TITLE 		TimeTitle;					/*!< 时间标题*/
	ENCODE_TITLE 		ChannelTitle;				/*!< 通道标题*/

	ENCODE_OPTION		PicOption[2];				/// 暂时写２

#ifdef FUNC_COURT_DISPLAY
	uchar	Volume;				/*!< 保存音量的阀值 */
	uchar	Reserved[47];
#else

	uchar	Reserved[48];		/*!< 保存用，多留点，省得以后不停地改结构通知客户端 */
#endif

}CONFIG_CAPTURE_OLD;

//! 编码配置结构(NVS在用)
typedef struct {
	uchar	CapVersion[8];		/*!< 版本号			*/		
	uchar	CifMode;			/*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */
	uchar	VideoType;			/*!< 编码模式 参照枚举capture_comp_t(DVRAPI.H) */
	uchar	EncodeMode;			/*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */
	uchar	ImgQlty;			/*!< 码流的画质 档次1-6		*/
	uchar	Frames;				/*!< 帧率　档次N制1-6,P制1-5 */	
	uchar	Brightness;			/*!< 亮度	0-100		*/
	uchar	Contrast;			/*!< 对比度	0-100		*/
	uchar	Saturation;			/*!< 饱和度	0-100		*/
	uchar	Hue;				/*!< 色度	0-100		*/
	uchar	AudioEn;			/*!< 音频使能 ON为打开，OFF为关闭	*/
	uchar	Gain;				/*!< 增益	0-100		*/
	uchar    CoverEnable;		/*!< 区域遮盖开关　ON为打开，OFF为关闭	*/
	VD_RECT	Cover;				/*!< 区域遮盖范围	*/	

	uchar	TimeTilteEn;		/*!< 时间标题使能  ON为打开，OFF为关闭 */
	uchar	ChTitleEn;			/*!< 通道标题使能  ON为打开，OFF为关闭 */

	uchar	ExtFunction;		/*!< 扩展功能支持，主要用于双码流 */
	uchar	VideoEn;			/*!< 视频参数　*/
	ushort	TimeTitleL;			/*!< 时间标题距左边的距离与整长的比例*8192 */
	ushort	TimeTitleT;			/*!< 时间标题的上边的距离与整长的比例*8192 */
	ushort	TimeTitleR;			/*!< 时间标题的右边的距离与整长的比例*8192 */
	ushort	TimeTitleB;			/*!< 时间标题的下边的距离与整长的比例*8192 */
	ushort	ChannelTitleL;		/*!< 通道标题距左边的距离与整长的比例*8192 */
	ushort	ChannelTitleT;		/*!< 通道标题距上边的距离与整长的比例*8192 */
	ushort	ChannelTitleR;		/*!< 通道标题距右边的距离与整长的比例*8192 */
	ushort	ChannelTitleB;		/*!< 通道标题距下边的距离与整长的比例*8192 */
	uchar	Reserved[64];		/*!< 保存用，多留点，省得以后不停地改结构通知客户端 */
}CONFIG_CAPTURE_V2201;

//! 旧的编码信息结构(单码流在用)
typedef struct {
	uchar	CapVersion[8];  /*!< 版本号			*/		
	uchar	CifMode;		/*!< 分辨率CIF1, CIF2, CIF4 		*/
	uchar	VideoType;		/*!< 编码模式：MPEG1 or MPEG4		*/
	uchar	EncodeMode;		/*!< 码流控制		*/
	uchar	ImgQlty;		/*!< 码流的画质		*/
	uchar	Frames;			/*!< 帧率			*/	
	uchar	Brightness;		/*!< 亮度			*/
	uchar	Contrast;		/*!< 对比度			*/
	uchar	Saturation;		/*!< 饱和度			*/
	uchar	Hue;			/*!< 色度			*/
	uchar	AudioEn;		/*!< 音频使能		*/
	uchar	Gain;			/*!< 增益			*/
	uchar    CoverEnable;	/*!< 区域遮盖开关	*/
	VD_RECT	Cover;			/*!< 区域遮盖范围	*/		
} OLD_CONFIG_CAPTURE;


//! 定时录像的信息结构
typedef struct 
{
	uchar	RecVersion[8];					/*!< 版本号 */
	TSECT	Sector[N_WEEKS][N_TSECT];		/*!< 定时时段，一个星期七天，每天N_TSECT时间段 */
	uchar	PreRecord;						/*!< 预录时间，当为0时表示关预录 */
	uchar	Redundancy;						/*!< 冗余开关，OFF为无效，ON为有效 */
	uchar	Reserve[2];						/*!< 保留 */
} CONFIG_RECORD_OLD;

//!CAMIPC配置
typedef struct tagCAMIPCOLD
{
    uchar btRecNetOpt;        //断网条件录像选项
    VD_BOOL bAutoWhiteBalance;  //自动白平衡
    uchar btRGain;
    uchar btBGain;
    uchar btReserv[32];      //扩展用
}CONFIG_CAMIPC_OLD;

typedef struct __video_cover_t
{
	uchar iChannel; 	 // 通道号，[1~n]
	uchar iCoverNum; 	 // 覆盖的区域个数
	uchar iRev[30];	 // 保留
}VIDEO_COVER_T;

typedef struct __video_cover_attr_t
{
	VD_RECT tBlock;	// 覆盖的区域坐标
	uint Color;		// 覆盖的颜色
	uchar iBlockType;	// 覆盖方式：0-黑块，1-马赛克
	uchar Encode;	   //! 编码级遮挡, 1—生效, 0—不生郊
	uchar Priview;    //!预览遮挡, 1—生效, 0—不生效
	uchar iRev[28];	// 保留
}VIDEO_COVER_ATTR_T;

//! 查询视频区域遮挡属性信息
typedef struct __video_cover_caps_t
{
	unsigned char		iEnabled;		// 置1表示支持 区域遮挡，0 不支持
	unsigned char		iBlockNum;		// 支持的遮挡块数
	unsigned char		iRev[62];		// 动态检测的区域需要划分成的行数。
}VIDEO_COVER_CAPS_T;


/*!
	\class CConfigConvertion
	\brief 转换旧的配置结构到新的配置结构
*/
class CConfigConversion : public CObject
{
public:
	//!	构造函数 	
	CConfigConversion();

	//! 析构函数	
	~CConfigConversion();

	//! 配置转换
	void Convert();
	
	//! 临时给网络端调用

	
	//! 临时给网络端调用
	int SaveOldColor(CONFIG_COLOR_OLD* pConfig, int iChannel = -1);
	
	//! 加载旧的Color配置
	void LoadOldColor(CONFIG_COLOR_OLD* pConfig, int iChannel = -1, VD_BOOL bDefault = FALSE);



	
	/*转换监视配置*/
	void LoadOldDisplay(CONFIG_DISPLAY_OLD *pConfig);
	int SaveOldDisplay(CONFIG_DISPLAY_OLD *pConfig);

	/*转换通道名称配置*/
	int SaveOldChannelTitle(CONFIG_TITLE_OLD*pConfig);

	//!保存旧的云台配置,网络端调用
	int SaveOldPtzConfig(CONFIG_PTZ_OLD* pConfig, int iChannel = -1);
	int SaveOldPtzAlarmConfig(CONFIG_PTZ_OLD *pConfig,int iChannel = -1);

	//!加载旧的云台配置结构信息
	void LoadOldPtzConfig(CONFIG_PTZ_OLD* pConfig, int iChannel = -1, VD_BOOL bDefault = FALSE);
	void LoadOldPtzAlarmConfig(CONFIG_PTZ_OLD* pConfig, int iChannel = -1,VD_BOOL bDefault = FALSE);

	//! 网络调用双码流配置结构
	int SaveOldEncode(CONFIG_CAPTURE_OLD* pConfig, int iChannel = -1);
	
	//! 加载旧的双码流配置结构
	void LoadOldEncode(CONFIG_CAPTURE_OLD* pConfig, int iChannel = -1, VD_BOOL bDefault = FALSE);

	//! 网络调用旧的单码流配置结构
	int SaveOldEncode_V2201(CONFIG_CAPTURE_V2201* pConfig, int iChannel = -1);

	//! 加载旧的单码流配置结构
	void LoadOldEncode_V2201(CONFIG_CAPTURE_V2201* pConfig, int iChannel = -1, VD_BOOL bDefault = FALSE);

	//! 保存旧的录像配置
	int SaveOldRecord(CONFIG_RECORD_OLD* pConfig);

	//! 加载旧的录像配置
	void LoadOldRecord(CONFIG_RECORD_OLD* pConfig, int iChannel = -1, VD_BOOL bDefault = FALSE);

	//!保存旧的自动维护配置
	int SaveOldAutoMain(CONFIG_AUTO_OLD* pConfig);

	//!加载旧的自动维护配置
	void LoadOldAutoMain(CONFIG_AUTO_OLD* pConfig, VD_BOOL bDefault = FALSE);

/*****************************网络配置*******************************************/
	//!解析配置项,一个配置文件对应多个结构, 故采用定义成员变量的方式
	CONFIG_NET *m_pCfgNetOld;

	int m_bOldUseDHCP ;
	CParse m_configParse;
	void ParseConfig(); 
	//!旧的基本网络配置结构转换与保存
	void LoadNetConfigOld(CONFIG_NET *pNetConfig, VD_BOOL bDefault = FALSE);
	int SaveNetConfigOld(CONFIG_NET *pNetConfig, int bUseDHCP, int bUpdate = 1);  //bUpdate 为不生效, 1为立即生效, 2为延时生效


	void LoadCfgVIDEO_COVER( VIDEO_COVER_T * pConfig,  VIDEO_COVER_ATTR_T * pConfig_attr, const int channel) ;
	int SaveCfgVIDEO_COVER( VIDEO_COVER_T * pConfig,  VIDEO_COVER_ATTR_T * pConfig_attr, const int channel );

/*****************************网络配置*******************************************/
	
private:


	//! 转换VideoColor配置
	void ConvertVideoColor();





	//! 转换Encode配置
	void ConvertEncode();




	//! 转换Net配置
	void ConvertNetWork();


};

int convertRealFrame(int iframeIndex);
int convertFrameIndex(int frame);

#endif


