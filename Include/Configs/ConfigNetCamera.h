#ifndef __CONFIG_NETCAMERA__
#define __CONFIG_NETCAMERA__

#include "ConfigBase.h"
#include "ConfigNet.h"

enum DevType_policy
{    
    dahua = 0,
    unknown = 1,
};

enum ENUM_CHANNEL_TYPE
{
    ENUM_CHANNEL_TYPE_DATA = 0,         ///< 数字通道
    ENUM_CHANNEL_TYPE_ANALOG = 1        ///< 模拟通道            
};

enum ENUM_CHANNEL_STREAM_TYPE
{
    ENUM_STREAM_TYPE_8K = 0,        ///码流packet是8K不定长的方式     
    ENUM_STREAM_TYPE_Stream,		//!码流是流模式，没有分析
    ENUM_STREAM_TYPE_NR
};



#endif 
