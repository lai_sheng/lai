#ifndef __CONFIG_PLAY__
#define __CONFIG_PLAY__

#include "ConfigBase.h"
#include "APIs/Play.h"

#define CHANNEL 16 //回放通道数

//!回放设置
typedef struct tagCONFIG_PLAY
{
	//!回放画面数	[0, 16]
	int iChannels;
	//!回放音频	一维数组[通道]
	int iVolume[CHANNEL];
	//!查询掩码
	int iSearchMask;
	//!是否连续播放
	VD_BOOL bContinue;			
} CONFIG_PLAY;

typedef TConfig<CONFIG_PLAY, 1, 4> CConfigPlay;

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)

typedef struct 
{
	unsigned char iVolume;
	unsigned char iEnable;
	unsigned char iPlayCount;
	unsigned char iWeekDay;
	unsigned char iHour;
	unsigned char iMiniute;
	char strMusicFileName[64];
}CONFIG_MUSICPLAN; 

typedef TConfig<CONFIG_MUSICPLAN, 6, 4> CConfigMusicPlan;

#endif 

typedef struct 
{
	unsigned char iVolume;
}CONFIG_VOICEPLAYPARAM; 

typedef TConfig<CONFIG_VOICEPLAYPARAM, 1, 4> CConfigVoicePlay;


#endif //__CONFIG_PLAY__

