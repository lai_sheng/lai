#ifndef __CONFIG_MONITORTOUR__
#define __CONFIG_MONITORTOUR__

#include "ConfigBase.h"
#include "Devices/DevSplit.h"

#define SPLITTYPES 6

enum MT_TYPE{
	MT_DETECT = 0,
	MT_LOSS,
	MT_BLIND,
	MT_NR
};

enum AM_TYPE{
	AM_LOCAL = 0,
	AM_NET,
	AM_NR
};
//!监视轮巡设置
typedef struct tagCONFIG_MONITORTOUR
{
	//!进行轮训使能	
	VD_BOOL bEnable;
	//!轮巡时间间隔	[5,120]
	int iInterval;
	//!轮巡掩码	一维数组[画面分割种类]
	uint iMask[N_SPLIT];	
	//!动检轮巡方式选择0:单画面 1:8画面
	int iMotionTourType[MT_NR];
	//!报警轮巡方式选择
	int iAlarmTourType[AM_NR];
} CONFIG_MONITORTOUR;

//!Monitor tour type
enum MT_CLASS
{
	MT_CLASS_General,
	MT_CLASS_Spot,
	MT_CLASS_NUM
};
typedef TConfig<CONFIG_MONITORTOUR, MT_CLASS_NUM, 4> CConfigMonitorTour;
#endif //__CONFIG_MONITORTOUR__

