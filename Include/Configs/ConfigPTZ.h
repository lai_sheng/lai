#ifndef __CONFIG_PTZ__
#define __CONFIG_PTZ__

#include "ConfigBase.h"
#include "ConfigComm.h"
#include "System/BaseTypedef.h"

#define PTZ_CHANNELS 8

#ifdef DEF_SOFT_TOUR
#define	PTZ_PRESETNUM 255
#else
#define	PTZ_PRESETNUM 80
#endif

#define	PTZ_PRESET_IN_TOUR_NUM 64

#define MAX_PTZ_PROTOCOL_LENGTH 32

//!云台设置
typedef struct tagCONFIG_PTZ_X
{
	char strProtocolName[MAX_PTZ_PROTOCOL_LENGTH];	/*!< 协议名称 */	
	int	ideviceNo;				/*!< 云台设备地址编号 */	
	int	iNuberInMatrixs;		/*!< 在矩阵中的统一编号	*/		
	int iPortNo;				/*!< 串口端口号	[1, 4] */	
	COMMATTRI dstComm;			/*!< 串口属性 */	
} CONFIG_PTZ;

#ifdef FUNTION_PRESET_TITLE
typedef struct tagCONFIG_PRESET_TITLE
{   
	/*设置预置的名称使能*/
	uint Enable;	
	/*名称X坐标*/
	uint x;
	/*名称Y坐标*/
    uint y;
	
} CONFIG_PRESET;
typedef TConfig<CONFIG_PRESET, N_SYS_CH, 4, CFG_PRESET_TITLE> CConfigPreset;
#endif
enum config_ptz_t
{
	CONFIG_PTZ_LOCAL,
	CONFIG_PTZ_ALARM,
	CONFIG_PTZ_PRESETINFO,
	CONFIG_PTZ_TOURINFO,
};

typedef TConfig<CONFIG_PTZ, N_SYS_CH, 4, CONFIG_PTZ_LOCAL> CConfigPTZ;

typedef TConfig<CONFIG_PTZ, N_SYS_CH, 4, CONFIG_PTZ_ALARM> CConfigPTZAlarm;


#define MAXNUM_POINT  (6)
#define RUNPERIOD_NUM (3)

typedef struct tagCONFIG_PRESET_PLACE
{   
	/*设置预置的名称使能*/
	VD_BOOL Enable;	/* 0:unable 1:enable*/
	/*名称X坐标*/
	int x;
	/*名称Y坐标*/
    int y;
	uint res[5]; //保留

} CONFIG_PRESET_PLACE;

typedef struct tagCONFIG_PRESET_PLAN
{   
	/* 轨迹坐标排序 */
	int iPointQueue[MAXNUM_POINT];
	/* 每点停留时间 */
	unsigned int iStayTime[MAXNUM_POINT];

} CONFIG_PRESET_PLAN;

typedef struct tagCONFIG_PTZRUN_TIME
{   
	int startHour;
	int startMin;
	int startSec;

	int endHour;
	int endMin;
	int endSec;	

} CONFIG_PTZRUN_TIME;

typedef struct tagCONFIG_TRACE
{   
	CONFIG_PRESET_PLACE sPoint[MAXNUM_POINT];
	CONFIG_PRESET_PLAN  sPlan;

	/*航巡使能开关*/
	VD_BOOL bRunTraceEnable;
	/*航巡有效时间段*/
	CONFIG_PTZRUN_TIME	sRunTime[RUNPERIOD_NUM];
} CONFIG_TRACE;

typedef TConfig<CONFIG_TRACE, 1, 4> CConfigPtzTrace;
#endif //__CONFIG_PTZ__
