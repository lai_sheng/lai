#ifndef __CONFIG_TVADJUST__
#define __CONFIG_TVADJUST__

#include "ConfigBase.h"

//!TV调节设置
typedef struct tagCONFIG_TVADJUST
{
	//!TV缩小比率，各分量取值相同	[0,100]
	VD_RECT rctMargin;
	//!亮度	[0,100]
	int iBrightness;
	//!对比度[0,100]
	int iContrast;
	//!去抖动[0,100]
	int iAntiDither;	
} CONFIG_TVADJUST;
//!采集区域设置
typedef struct tagCONFIG_CROP
{
	//!水平偏移
	int iHoffset;
	//!垂直偏移
	int iVoffset;
	//!宽
	int iWidth;
	//!高
	int iHeight;
	//!保留字节
	int reserve[2];
} CONFIG_CROP;
typedef TConfig<CONFIG_CROP, N_SYS_CH, 4> CConfigCrop;
typedef TConfig<CONFIG_TVADJUST, 1, 4> CConfigTVAdjust;
#endif //__CONFIG_TVADJUST__

