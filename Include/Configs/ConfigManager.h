#ifndef __CONFIG_MANAGER__
#define __CONFIG_MANAGER__

#include "config-x.h"

#include "System/Object.h"
#include "System/File.h"

#include "AuxLibs/Zlib/zlib.h"
#include "MultiTask/Timer.h"

#include "ConfigLocation.h"
#include "ConfigGeneral.h"
#include "ConfigGUISet.h"
#include "ConfigVideoColor.h"
#include "ConfigWorksheet.h"
#include "ConfigRecord.h"
#include "ConfigPTZ.h"
#include "ConfigEvents.h"
#include "ConfigComm.h"
#include "ConfigPlay.h"
#include "ConfigEncode.h"
#include "ConfigVideoWidget.h"
#include "ConfigMonitorTour.h"
#include "ConfigTVAdjust.h"
#include "ConfigChannelTitle.h"
#include "ConfigAutoMaintain.h"
#include "ConfigCamera.h"
#include "ConfigNet.h"
#include "ConfigAwakeTime.h"
#include "Configs/ConfigDoorLock.h"
#include <map>
#include <set>

#define LOCKUSERINFO_STR	"LockUserInfo"
#define LOCKLEAVEMSG_STR	"LockLeaveMsg"

enum defualt_cfg
{
	DEFAULT_CFG_GENERAL = 0,
	DEFAULT_CFG_ENCODE,
	DEFAULT_CFG_RECORD,
	DEFAULT_CFG_COMM,
	DEFAULT_CFG_NET,
	DEFAULT_CFG_ALARM = 5,
	DEFAULT_CFG_DETECT,
	DEFAULT_CFG_PTZ,
	DEFAULT_CFG_AUT,
	DEFAULT_CFG_CHANTITLE = 9,
	DEFAULT_CFG_EXCEP,
	DEFAULT_CFG_ALL = 31,
	DEFAULT_CFG_END,
};

std::string getString(const CConfigTable &table);

class CConfigManager : public CObject
{
	typedef std::map<std::string, CConfigExchange*> CONFIG_MAP;
	typedef std::map<int, std::string> CONFIG_MAPR;
	typedef std::map<std::string, int> MAP_TRANSLATE;
	typedef std::map<std::string, int>::value_type valueType;
	typedef std::set<std::string> ConfigSetExceptional;

public:
	PATTERN_SINGLETON_DECLARE(CConfigManager);
	
	CConfigManager();

	//! 初始化
	void initialize();
	int recallConfig(const char* name, const char* user = NULL, int index = -1);
	int recallConfigAll(const char* user = NULL);
	//! 保存文件
	void saveFile();
	//! 设置默认配置，供GUI界面和网络模块调用
	int SetDefaultConfig(int iConfigType);

	/// 导出配置文件
	int GetConfigFileData(void* pBuf, int& iLen);
	/// 导入配置文件
	int SetConfigFileData(void* pBuf, int iLen, ConfigSetExceptional& cfgExceptional);
	
private:
	void setupConfig(const char* name, CConfigExchange& xchg);
	void onConfigChanged(CConfigExchange& xchg, int& ret);
	//CConfigTable& locate(CConfigTable& table, const char* name);
	const char* nameFromID(int id);
	CConfigExchange* xchgFromName(const char* name);
	VD_BOOL readConfig(const char* chPath, std::string& input);
	void onTimer(uint arg);
	//!恢复网络端不是保存在配置文件中的配置
	int setDefaultNetConfig();

private:
	CConfigTable m_configAll;				/*!< 配置总表 */
	VD_BOOL m_changed;							/*!< 配置表变化了 */
	gzFile m_fileConfig;					/*!< 配置文件 */

	CONFIG_MAP m_map;						/*!< 配置名称和配置类指针映射表 */
	CONFIG_MAPR m_mapReverse;				/*!< 配置ID和配置名称和映射表 */

	MAP_TRANSLATE m_mapTranslate;			/*!< 配置名称和配置类型映射表*/

	//各个子配置
	CConfigLocation			m_configLocation;	/*!< 区域配置 */
	CConfigGeneral			m_configGeneral;	/*!< 普通配置 */
	CConfigGUISet			m_configGUISet;		/*!< GUI配置 */

	CConfigMonitorTour 	m_configMonitorTour;
	CConfigTVAdjust 		m_configTVAdjust;
    CConfigCrop   m_configCrop;
	CConfigChannelTitle 	m_configChannelTitle;
	CConfigOSDParam			m_configOSDParam;

	CConfigVideoColor	m_configVideoColor;	/*!< 图像颜色配置 */

#if defined (_USE_720P_MODULE_) 
	CConfigCamera			m_configCamera;		/* 2762镜头特殊功能配置*/
	CConfigWhiteBalance     m_configWhiteBalance;  /*手动白平衡RGB 参数*/
	CConfigSenSor			m_configSensor;
	CConfigCameraExt        m_configCameraExt;
#endif
#ifdef TUPU
	CConfigTupu             m_configTupu;
#endif

	CConfigComm				m_configComm;		/*!< 串口配置 */
	CConfigRECWorksheet		m_configRECWorksheet;	/*!< 录像工作表配置 */
	CConfigALMWorksheet		m_configALMWorksheet;	/*!< 报警工作表配置 */
	CConfigNetAlmWorksheet		m_configNetALMWorksheet;	/*!< 报警工作表配置 */
	CConfigMTDWorksheet		m_configMTDWorksheet;	/*!< 动检工作表配置 */
	CConfigBLDWorksheet		m_configBLDWorksheet;	/*!< 遮挡工作表配置 */
	CConfigVLTWorksheet		m_configVLTWorksheet;	/*!< 视频丢失工作表配置 */
	CConfigSnapWorksheet		m_configSnapWorksheet;	/*!< 定时抓拍工作表配置 */
	CConfigDecAlmWorksheet    m_configDecAlmWorksheet; /*!<解码器告警工作表配置>!*/
	CConfigScheduleWorksheet  m_configScheduleWorksheet;/*!<摄像头工作表配置>!*/
	CConfigHemuAlarmWorksheet m_coffigHemuAlarmWorksheet;/*!<摄像头告警工作表配置>!*/
//////////hemu//////////////////
	CConfigHemuUeserInfo 	  m_configHemuUserInfo; //绑定用户信息
	CConfigHemuAppCtrl        m_configHemuAppCtrl;  //APP设置信息
#ifdef FUNTION_PRESET_TITLE
	CConfigPreset     m_configPreset;
#endif
	CConfigPTZ		m_configPTZ;				/*!<云台配置 */
	CConfigPTZAlarm	m_configPTZAlarm;			/*!<云台报警设备协议配置 */
	CConfigRecord			m_configRecord;		/*!< 定时录像配置 */
	
	CConfigAlarm			m_configAlarm;		/*!< 本地报警配置 */
	CConfigNetAlarm			m_configNetAlarm;	/*!< 网络报警配置 */
	CConfigMotionDetect		m_configMotion;		/*!< 动态检测配置 */
	CConfigLossDetect		m_configLoss;		/*!< 视频丢失配置 */
	CConfigDecoderAlarm          m_configDecAlm;    /*!< 解码器配置 */
	CConfigBlindDetect		m_configBlind;		/*!< 视频遮挡配置 */
	CConfigPlay				m_configPlay;		/*!< 回放配置 */

	CConfigStorageNotExist	m_configStorageNotExist;	/*!< 没有存储器事件配置 */
	CConfigStorageFailure	m_configStorageFailure;		/*!< 存储器访问错误事件配置 */
	CConfigStorageLowSpace	m_configStorageLowSpace;	/*!< 存储器容量不足事件配置 */
	CConfigNetAbort			m_configNetAbort;			/*!< 网线连接状态事件配置*/
	CConfigNetArp			m_configNetArp;				/*!< IP地址冲突事件配置*/
	//CConfigVideoMatrix		m_configVideoMatrix;/*!< 视频矩阵*/
	//CConfigVN_VideoMatrix	m_configVNVideoMatrix;
	CConfigEncode			m_configEncode;		/*!< 编码配置 */
	CConfigVideoWidget		m_configVideoWidget;/*!< 视频装饰 */

	CConfigAutoMaintain		m_configAutoMaintain;/*!< 自动维护*/

	CConfigSnap m_configSnap; /*!< 抓拍参数配置>*/
	/************network************************/
	/************network************************/
	CConfigNetCommon     m_configNetCommon;
	CConfigFTPServer            m_configFTPServer;
	CConfigFTPApplication     m_configFTPApplication;
	CConfigNetIPFilter        m_configNetIPFilter;
	CConfigNetMultiCast     m_configNetMultiCast;
	CConfigNetDDNS         m_configNetDDNS;
	CConfigNetAlarmServer  m_configNetAlarmServer;
	CConfigNetNTP           m_configNetNTP;
	CConfigNetEmail          m_configNetEmail;
	CConfigNetSniffer         m_configNetSniffer;
	CConfigRECState	m_configRECState;

#ifdef VSERVER   //add by kyle xu in 20160107	
	CConfigNetVServer m_configNetVServer;
#endif

#ifdef HMSOTA
    CConfigNetHms             m_configHmsServer;
	CConfigNetOta             m_configOtaMode;
#endif
	CConifgRegServer          m_configRegServer;
	CConfigDHCP              m_configDHCP;
	CConfigAudioInFormat 	m_configAudioInFormat;
	CConfigNetUPNP           m_configUPNP;
	
	CConfigPresetInfor m_configPresetInfor;
	CConfigTourInfor m_configTourInfor;
	CConfigPtzRegress m_configPtzRegress;
	
#ifdef OUT_COLOR_SET
    cConfigTvOutColorSet m_ConfigTvOutColorSet;
#endif

#ifdef FUNCTION_SUPPORT_RTSP
    CConfigNetRtspSet m_configNetRtspSet;
#endif

#ifdef ONVIF
    //add by kyle xu in 201603111626
	CConfigOnvifServer m_configOnvifServer;
#endif
#ifdef _2761_EXCEPTION_CHK_
	CConfigEncoderDetect m_EncoderDetect;
#endif

	CConfigAwakeTime	m_AwakeTime;

	CConfigAiUpload		m_AiUpload;

	CConfigVoicePlay	m_VoicePlay;

	CConfigAlarmSwitch	m_AlarmSwitch;

	CConfigPtzTrace		m_PtzTrace;

	CConfigLockUserInfo m_LockUserInfo;
	CConfigLockLeaveMsg m_LockLeaveMsg;
	CConfigBatAlarm m_configBatAlarm;
    CConfigPirLevel m_configPirLevel;
    CConfigLockWarnVoice m_LockWarnVoice;
	CConfigLockMsgCache  m_LockMsgCache;
	CTimer m_Timer;
	std::string m_stream;	// 字符串流
	
private:
	int ZipConfig(CConfigTable& cfg);
	int CopyConfigData(void* pBuf, int& iLen);
	int WriteConfigData(void* pBuf, int iLen);
	int UnZipConfig(CConfigTable& cfg);

private:
	gzFile	 		m_ZipConfig;
	CFile 			m_pFileReadWrite;
	std::string 	m_StrConfig;
	
#define TempZipConfigFile			"/var/tmp/ZipConfigTmp"
};

#define g_Config (*CConfigManager::instance())

#endif //__CONFIG_MANAGER__

