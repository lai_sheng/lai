#ifndef CONFIGAWAKETIME_H_
#define CONFIGAWAKETIME_H_

#include "ConfigBase.h"

#define WDAYS	(7)
#define UNITS	(6)

typedef struct tagTIMEUNIT
{
	bool Enable;
	int  StartTime; 	//( Hours << 16 | Minutes << 8 | Seconds)
	int  EndTime;		//( Hours << 16 | Minutes << 8 | Seconds)
}TimeUnit;

typedef struct tagCONFIG_AWAKETIME
{
	TimeUnit	DayPlan[UNITS];
} CONFIG_AWAKETIME;


typedef TConfig<CONFIG_AWAKETIME, WDAYS, 4> CConfigAwakeTime;

typedef enum{
	TAG_PIR_SWITCH		= 0,
	TAG_LIGHT_SWITCH 	= 1,	//灯开关
	TAG_POWER_SWITCH 	= 2,	//晾衣架开关
	TAG_DISINFECTION	= 3,	//消毒
	TAG_HOT_DRY			= 4,	//烘干
	TAG_WIND_DRY		= 5,	//风干		
	TAG_TIMERTASK_MAX,
}tagTIMERTASK_TYPE;

#define TIMERTASK_MAXNUM	(6)

//定时任务
typedef struct{
	/*<! 任务ID*/	
	int 		taskId;
	/*<! 任务总开关*/	
	VD_BOOL 	mainSwitch;
	/*<! 任务每日执行开关*/
	VD_BOOL		daySwitch[WDAYS]; 
	/*<! 任务每日执行时间段*/
	TimeUnit	dayPlan[UNITS];
	/*<! 任务执行动作*/
	VD_BOOL		taskDo[TAG_TIMERTASK_MAX];	
}Timer_Task_Info;

typedef struct{
	int				taskNum; /* taskNum <= TIMERTASK_MAXNUM */
	Timer_Task_Info	task[TIMERTASK_MAXNUM];
} CONFIG_TIMERTASK_LIST;
typedef TConfig<CONFIG_TIMERTASK_LIST, 1, 4> CConfigTimerTaskList;


//延时任务
typedef struct{
	/*<! 任务ID,大于0 则代表存在任务*/	
	int 		taskId;
	/*<! 任务开始时间*/	
	unsigned int startTimeStamp;
	/*<! 任务延时秒数*/	
	int			delaySec;
	/*<! 需要执行的动作*/
	VD_BOOL		taskSwitch[TAG_TIMERTASK_MAX];
	/*<! 任务执行动作*/
	VD_BOOL		taskDo[TAG_TIMERTASK_MAX];	
}Delay_Task_Info;

typedef struct{
	int				taskNum; /* taskNum <= TIMERTASK_MAXNUM */
	Delay_Task_Info	task[TIMERTASK_MAXNUM];
} CONFIG_DELAYTASK_LIST;
typedef TConfig<CONFIG_DELAYTASK_LIST, 1, 4> CConfigDelayTaskList;


typedef struct tagCONFIG_BATALARM
{
    char strCamLowBatTime[16];
    char strLockLowBatTime[16];
}CONFIG_BATALARM;

typedef TConfig<CONFIG_BATALARM, 1, 4> CConfigBatAlarm;

typedef enum {
    PIR_LEVEL_1     = 1,    //detect_time:1 effect_time:1    
    PIR_LEVEL_2     = 2,    //detect_time:12 effect_time:6
    PIR_LEVEL_3     = 3,    //detect_time:18 effect_time:9
    PIR_LEVEL_4     = 4,    //detect_time:20 effect_time:10
    PIR_LEVEL_5     = 5,    //detect_time:30 effect_time:15
    PIR_LEVEL_MAX
}PIR_LEVEL_E;

typedef enum{
	PIR_PUSH_MSG_OFF 			= 0,	/* 不推送PIR告警消息 */
	PIR_PUSH_MSG_NORMAL 		= 1,	/* 正常推送,即检测到运动立即推送 */
	PIR_PUSH_MSG_STAY_SHORT 	= 2,	/* 短时间逗留推送, 超过5s */
	PIR_PUSH_MSG_STAY_LONG		= 3,	/* 长时间逗留推送, 超过15s */
	PIR_PUSH_MSG_MAX
}PIR_PUSH_MSG_LEVEL;

typedef struct tagCONFIG_PIRLEVEL
{
    int level;  //PIR灵敏度等级/省电模式等级 PIR_LEVEL_E
    int pushMsgLevel; //PIR推送消息等级 PIR_PUSH_MSG_LEVEL
}CONFIG_PIRLEVEL;

typedef TConfig<CONFIG_PIRLEVEL, 1, 4> CConfigPirLevel;

#endif
