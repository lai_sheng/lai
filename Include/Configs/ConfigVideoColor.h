#ifndef __CONFIG_VIDEOCOLOR__
#define __CONFIG_VIDEOCOLOR__

#include "ConfigBase.h"
#include "APIs/Video.h"

#define N_COLOR_SECTION 2

//! 视频颜色结构
typedef struct tagVIDEOCOLOR_PARAM
{
	int	nBrightness;		/*!< 亮度	0-100 */
	int	nContrast;			/*!< 对比度	0-100 */
	int	nSaturation;		/*!< 饱和度	0-100 */
	int	nHue;				/*!< 色度	0-100 */
	int	mGain;				/*!< 增益	0-100 第７位置1表示自动增益　*/		
	int	mWhitebalance;		/*此值现在用于光圈基准值*/
}VIDEOCOLOR_PARAM; 

//!视频颜色设置
typedef struct tagVIDEOCOLOR
{
	TIMESECTION			TimeSection;		/*!< 时间段 */
	VIDEOCOLOR_PARAM	dstColor;			/*!< 颜色定义 */
	int					iEnable;
}VIDEOCOLOR;

typedef struct tagCONFIG_VIDEOCOLOR
{
	VIDEOCOLOR dstVideoColor[N_COLOR_SECTION];
}CONFIG_VIDEOCOLOR;

typedef struct tagNET_VIDEOCOLOR
{
    int iChannel;
	CONFIG_VIDEOCOLOR stVideoColor;
}NET_VIDEOCOLOR;

typedef TConfig<CONFIG_VIDEOCOLOR, N_SYS_CH, 4> CConfigVideoColor;

/*-----------视频控制配置-----------*/
typedef struct tagCONFIG_VIDEOCONTRO
{
 int  nExposure; ///< 曝光模式 1-6:手动曝光等级; 0:自动曝光
 VD_BOOL bBacklight; ///< 背光补偿 1:打开补偿; 0:停止补偿
 VD_BOOL bAutoColor2BW; ///< 自动彩黑转换 1:打开转换; 0:停止转换
 VD_BOOL bMirror;//<镜像  1 支持， 0不支持>
}CONFIG_VIDEOCONTROL;

typedef TConfig<CONFIG_VIDEOCONTROL, 1, 4> CConfigVideoControl;

typedef TConfig<VIDEOCOLOR_PARAM, 1, 4> cConfigTvOutColorSet;
template<> void exchangeTable<VIDEOCOLOR_PARAM>(CConfigExchange& xchg, CConfigTable& table, VIDEOCOLOR_PARAM& config, int index, int app);

#endif //__CONFIG_VIDEOCOLOR__

