#ifndef __CONFIG_CONFIGNET__
#define __CONFIG_CONFIGNET__

#include "System/Object.h"
#include "System/File.h"
#include "System/Parse.h"
#include "ConfigBase.h"
#include "Functions/Ptz.h"
#include "System/BaseTypedef.h"


//结构定义规则统一改为不带前缀的格式
#define  NAME_PASSWORD_LEN  64
#define  EMAIL_ADDR_LEN  32
#define  N_MIN_TSECT 2
#define  N_ALARMSERVERKEY 5

#define NEW_DDNS_TYPE_3322        "CN99 DDNS"
#define NEW_DDNS_TYPE_DAHUA        "Videon DDNS"
#define NEW_DDNS_TYPE_NOIP        "NO-IP DDNS"
#define NEW_DDNS_DYNDNS            "DynDNS DDNS"
#define NEW_DDNS_ORAYDNS        "Oray DDNS"
#define NEW_DDNS_VISIONDIGI        "OWN DDNS"
#define NEW_DDNS_ZK        			"ZK DDNS"           //gong:


enum
{
	DDNS_TYPE_ZK_ID,   //gong:  添加zkddns
    DDNS_TYPE_ORAY_ID,
    
    DDNS_TYPE_3322_ID,
    DDNS_TYPE_DYNDNS_ID ,
	DDNS_TYPE_NOIP_ID,
               
    VALID_DDNS_TYPE_NUM,  //6 --> 7
    //DDNS_TYPE_NOIP_ID ,
    DDNS_TYPE_DAHUA_ID ,
	DDNS_TYPE_VISIONDIGI_ID,
};


#define  DDNS_MAX_NUM    VALID_DDNS_TYPE_NUM//(DDNS_TYPE_DYNDNS_ID+1)        //最大同时支持ddns数目


char * GetDdnsstrByID( int ddns_id );
//!服务器结构定义
typedef struct tagREMOTE_SERVER
{
    //!服务名
    char ServerName[NAME_PASSWORD_LEN];
    //!IP地址
    IPADDR ip;
    //!端口号
    int Port;
    //!用户名
    char UserName[NAME_PASSWORD_LEN];
    //!密码
    char Password[NAME_PASSWORD_LEN];    
    //!是否匿名登录
    VD_BOOL Anonymity;
}REMOTE_SERVER;

typedef struct tagCONFIG_NET_APP
{
    char HostName[NAME_PASSWORD_LEN]; //!设备名    
    int HttpPort; //!HTTP服务端口    
    int TCPPort; //!TCP侦听端口        
    int SSLPort; //!SSL侦听端口    
    int UDPPort; //!UDP侦听端口    
    int MaxConn; //!最大连接数    

    unsigned char ucMonMode; //!监视协议 {"TCP","UDP","MCAST",…}  0|1|2    
    unsigned char ucUseTransPlan; //!是否启用网络传输策略
    unsigned char ucTransPlan; //!传输策略 2自动 1流畅性优先 0质量优先，默认1
    unsigned char ucHighSpeedDownload; //!是否启用高速录像下载测率

    int iRes[3];
}CONFIG_NET_APP;

typedef struct tagCONFIG_NET_ETH
{
    char strEthName[16]; 
    //不同网卡名称，以字符串标示”eth0”; 此不可以改
    char strMacAddr[32];//xx.xx.xx.xx.xx.xx(字符串形式)
    
    IPADDR HostIP; //!主机IP    
    IPADDR Submask; //!子网掩码
    IPADDR Gateway; //!网关IP
    
    unsigned char  ucTranMedia;   // 0：有线，1：无线
    unsigned char  ucDefaultEth; //是否作为默认的网卡, 1：默认 0：非默认
    unsigned char  ucValid;
    // 按位表示，第一位：1：有效 0：无效；
    //第二位：0：DHCP关闭 1：DHCP使能；
    //第三位：0：不支持DHCP 1：支持DHCP
    unsigned char  ucReserve;
    
    unsigned char  ucRes[16]; //保留字

} CONFIG_NET_ETH;

typedef struct tagDNS_IP_LIST
{
    char strPrimaryIp[16]; //xxx. xxx. xxx. xxx (字符串形式)
    char strSecondIp[16];
    
}DNS_IP_LIST;

typedef struct tagPPPOE_IP_SRV
{
    int iEnable; //0 不使能，1 使能
    char strip[32];
    char strUser[64];
    char strPwd[64];
}PPPOE_IP_SRV;

typedef struct tagIPADDR_SERVER
{
    //!是否开启 0 不开启，1开启
    int Enable;

    //!类型名称，如果是ddns，对应ddnskey;可以扩展服务类型使用
    int iKey;

    //!服务器 信息
    REMOTE_SERVER Server;    
}IPADDR_SERVER;

enum image_transfer_policy
{
    IMAGE_TRANSFER_QUAILITY_PRIOR = 0, //图象质量优先传输策略
    IMAGE_FLUENCY_PRIOR,               //流畅性优先    
    IMAGE_AUTOADAPT,                   //自适应
};

//录象下载策略
enum recd_download_policy
{
    //普通下载
    DOWNLOADING_AT_NORMALSPEED = 0,

    //高速下载
    DOWNLOADING_AT_HIGHSPEED,
};

//!普通网络设置
typedef struct tagCONFIG_NET_COMMON
{
    //!主机名
    char HostName[NAME_PASSWORD_LEN];
    //!主机IP
    IPADDR HostIP;
    //!子网掩码
    IPADDR Submask;
    //!网关IP
    IPADDR Gateway;
    //!HTTP服务端口
    int HttpPort;
    //!TCP侦听端口
    int TCPPort;    
    //!SSL侦听端口
    int SSLPort;
    //!UDP侦听端口
    int UDPPort;
    //!最大连接数
    int MaxConn;
    //!监视协议 {"TCP","UDP","MCAST",…}
    int MonMode;
    //!限定码流值
    int MaxBps;
    //!传输策略
    //char TransferPlan[NAME_PASSWORD_LEN];
    int TransferPlan;
    //!是否启用网络传输策略
    VD_BOOL bUseTransferPlan;

    //!是否启用高速录像下载测率
    VD_BOOL bUseHSDownLoad;
} CONFIG_NET_COMMON;


//!IP权限设置
#define MAX_FILTERIP_NUM 64
typedef struct tagCONFIG_NET_IPFILTER
{
     //!是否开启
    VD_BOOL Enable;
     //!权限类型: 白名单/黑名单
    int     FilterType;
          //!黑名单列表
    IPADDR BannedList[MAX_FILTERIP_NUM];    
    //!白名单列表
    IPADDR TrustList[MAX_FILTERIP_NUM];    
}CONFIG_NET_IPFILTER;
//!组播设置
typedef struct tagCONFIG_NET_MULTICAST
{
    //!是否开启
    VD_BOOL Enable;
    //!组播服务器
    REMOTE_SERVER Server;    
}CONFIG_NET_MULTICAST;
//!pppoe设置
typedef struct tagCONFIG_NET_PPPOE
{
    //!是否开启
    VD_BOOL Enable;
    //!PPPOE服务器
    REMOTE_SERVER Server;    
}CONFIG_NET_PPPOE;

//!Vserver设置
typedef struct tagCONFIG_NET_VSERVER
{
	//是否开启
	VD_BOOL Enable;
	//远程服务器地址
	REMOTE_SERVER Server;
}CONFIG_NET_VSERVER;

//!DDNS设置
typedef struct tagCONFIG_NET_DDNS
{
    //!是否开启
    VD_BOOL Enable;
    //!DDNS类型名称
    int DDNSKey;
    //!DDNS服务器
    REMOTE_SERVER Server;
}CONFIG_NET_DDNS;

//add by ilena
typedef struct tagCONFIG_NET_UPNP
{    
    VD_BOOL Enable; //是否开启    
    int WebPort;  // web端口    
    int TCPPort;  // tcp端口    
}CONFIG_NET_UPNP;

typedef struct tagCONFIG_UPNP
{    
    int Enable; //是否开启    
    int WebPort;// web端口    
    int TCPPort; // tcp端口    
   int iRervered;
}CONFIG_UPNP;
//end

//!报警中心设置
typedef struct tagCONFIG_NET_ALARMSERVER
{
    //!是否开启
    VD_BOOL Enable;
    //!报警中心协议类型名称
    int AlarmServerKey;
    //!报警中心服务器
    REMOTE_SERVER Server;    
}CONFIG_NET_ALARMSERVER;
//!ftp设置
typedef struct tagCONFIG_FTP_SERVER {
    //!服务器使能
    VD_BOOL Enable;             
    //!FTP服务器
    REMOTE_SERVER Server;
    //!备用服务器IP
    IPADDR SpareIP;
    //!远程目录
    char            RemotePathName[VD_MAX_PATH];            
    //!文件最大长度
    int         FileMaxLen; 
    //!上传时段
    TIMESECTION  UpLoadPeriod[N_MIN_TSECT];
}CONFIG_FTP_SERVER;
//分别对应于 00普通录像;01外部报警;02动态检测;03卡号录像
enum EnFTP{
    EnGeneral = 0,
    EnAlarm,
    EnMotion,
    EnEnCard,
};
typedef union{
    uint l;
    uchar En[4]; 
}FTPMask;
typedef struct tagTSECT_MULTISTATE {
    TIMESECTION Tsect;
   //掩码
    FTPMask StateMask; 
}TSECT_MULTISTATE;
typedef struct tagCONFIG_FTP_APPLICATION {
    //!设置时间段状态用 TSECT中的State表示第二位是定时，第三位是动态检测，第四位是报警
    TSECT_MULTISTATE  RecordPeriodSet[N_WEEKS][N_MIN_TSECT];
}CONFIG_FTP_APPLICATION;

typedef struct tagCONFIG_DAYLIGHT
{
	uint	StartTime;
	uint	EndTime;
	VD_BOOL	Enable;	
}DAYLIGHT_TIME;//夏令时时间段

//!NTP设置
typedef struct tagCONFIG_NET_NTP
{
    //!是否开启
    VD_BOOL Enable;
    //!PPPOE服务器
    REMOTE_SERVER Server;
    //!更新周期
    int UpdatePeriod;
    //!时区 
    int TimeZone;    //除以 10 为时区信息 ，例如 80 则为 8时区
    //!是否开启夏令时

//    int DaylightEnable; 
	DAYLIGHT_TIME DaylightTime;	
	//!时区额外信息
	char TimeZoneExtData[128];

}CONFIG_NET_NTP;

#define  MAX_EMAIL_TITLE_LEN 64
#define  MAX_EMAIL_RECIEVERS  5
//!EMAIL设置
typedef struct tagCONFIG_NET_EMAIL
{
    //!是否开启
    VD_BOOL Enable;
    //!smtp 服务器地址使用字符串形式填充
    //!可以填ip,也可以填域名
    REMOTE_SERVER Server;
    VD_BOOL bUseSSL;
    //!发送地址
    char SendAddr[EMAIL_ADDR_LEN];
    //!接收人地址
    char Recievers[MAX_EMAIL_RECIEVERS][EMAIL_ADDR_LEN];
    //!邮件主题
    char Title[MAX_EMAIL_TITLE_LEN];
    //!email有效时间
    TIMESECTION Schedule[N_MIN_TSECT];
    uint	dwAccePic;	 	 //附加通道掩码
}CONFIG_NET_EMAIL;

//!EMAIL设置
typedef struct tagCONFIG_EMAIL
{
    //!可以填ip,也可以填域名
    IPADDR_SERVER Server;
    int bUseSSL;

    //!发送地址
    char SendAddr[EMAIL_ADDR_LEN];

    //!接收人地址
    char Recievers[MAX_EMAIL_RECIEVERS][EMAIL_ADDR_LEN];

    //!邮件主题
    char Title[MAX_EMAIL_TITLE_LEN];

    //!email有效时间
    TIMESECTION Schedule;
     uint uiPicturesChs;  //发email带图片附件通道，按照通道掩码来处理
     uint uiRes[6];
}CONFIG_EMAIL;

typedef struct tagCONFIG_NTP
{
//!服务器
    IPADDR_SERVER szNtpServer;
//!更新周期
    int UpdatePeriod;
//!时区
    int TimeZone;
}CONFIG_NTP;

//抓包配置结构
typedef struct tagCONFIG_NET_SNIFFER
{
    IPADDR	SrcIP;           //抓包源地址
    int    	SrcPort;         //抓包源端口
    IPADDR  DestIP;          //抓包目标地址
    int    	DestPort;        //抓包目标端口
} CONFIG_NET_SNIFFER;
//主动注册服务器
typedef struct tagServerInfo
{
    char    strUsr[64];    //用户名
    char    strPwd[64];    //密码
    char    ServerIp[128];
    int     ServerPort;
    int     enable;    //标识IP是否可用，0为可用，-1为不可用
}ServerInfo;

#define SERVER_NUM 1
//主动注册服务器
typedef struct tagCONFIG_REG_SERVER
{
    int         enable;
    char        deviceID[32];
    int      	iConnectType; //连接方式，0：单连接（使用原有的信令媒体单一连接模式
    ServerInfo  serverInfo[SERVER_NUM];
}CONFIG_REG_SERVER;

typedef struct
{
   int channel;//!本地解码通道号, 从0开始
   ushort device_type;//!设备类型, 0—威乾设备  4 VS300 3 RTSP
   ushort device_port;//连接前端设备端口
   uint device_ip;//网络字节序储存, 连接的前端设备ip
   uint device_channel;//!远程通道号
   uint enable;//!使能开关
   uint stream_type; //!连接码流类型,0-主码流
   char username[128]; //!连接前端用户名
   char password[128]; //!连接密码
}CFG_DECODE_T;


typedef struct
{
   uchar ucChannel;        //!本地解码通道号, 从0开始
   uchar ucAVEnable;       //0,只解视频 1，只解音频 ，2 解音视频 ，不允许什么都不解
   uchar ucRemotePtzPreset;//远程预置点
   uchar ucRemotePtzPresetEnable;//是否一连接上就设置远程预置点   
   uchar ucUrl[256];       //rtsp模式下此配置有效,格式如：rtsp://ip:port/user=xxx&password=xxx&id=xx&type=xx
   int   iDecodePolicy;    //解码策略 	(-2, 最实时 -1 实时 0 默认情况 1 流畅 2,	最流畅 ) 以后可能扩展到自定义具体的延时时间
   uchar ucConType;/*用掉一个保留字。协议类型，1 TCP 2 UDP*/
   uchar ucLocalPtz;/*保留*/
   uchar ucRes1[122];/*保留*/
}CFG_DECODE_EX;


typedef struct _RTSP_SET
{
    ushort    usEnable;           //使能 
    ushort    usListernPort;     //RTSP侦听端口
    ushort    usUdpStartPort;  //UDP起始端口
    ushort    usUdpPortNum;   //UDP端口数量

    IPADDR   stMulticastIp;//组播地址，用掉4字节保留位 
    ushort    usPort; //组播端口，用掉2字节保留位

    ushort    usReserve[33];   //保留
} RTSP_SET;


//DHCP
typedef struct tagCONFIG_DHCP_CFG
{
    int enable;
    char ifName[32];
}CONFIG_DHCP_CFG;

//!NTP设置
typedef struct tagCONFIG_NET_DNS
{
    IPADDR        PrimaryDNS;
    IPADDR        SecondaryDNS;
}CONFIG_NET_DNS;



#ifdef HMSOTA
typedef struct _CONFIG_NET_HMS
{       
    //!是否开启
    VD_BOOL Enable;
    //!服务名
    char ServerName[128];
    //!端口号
    int Port;
    //!用户名
    char UserName[NAME_PASSWORD_LEN];
    //!密码
    char Password[NAME_PASSWORD_LEN];    

}CONFIG_NET_HMS;

typedef struct _CONFIG_NET_OTA
{       
    //!0:手动  1: 关键自动更新
    VD_BOOL Mode;
    //!服务名
    int Interval;   //6 小时 12小时
    
	//!忽略版本名
    char IgnoreVersion[128];  
}CONFIG_NET_OTA;

typedef TConfig<CONFIG_NET_OTA, 1, 4> CConfigNetOta;
typedef TConfig<CONFIG_NET_HMS, 1, 4> CConfigNetHms;

#endif


#define  N_FTP_SERVICE   2          //ftp上传的类型  目前分图片和录像
typedef TConfig<CONFIG_NET_COMMON, 1, 4> CConfigNetCommon;
typedef TConfig<CONFIG_NET_IPFILTER, 1, 4> CConfigNetIPFilter;
typedef TConfig<CONFIG_NET_MULTICAST, 1, 4> CConfigNetMultiCast;
typedef TConfig<CONFIG_NET_DDNS, DDNS_MAX_NUM, DDNS_MAX_NUM> CConfigNetDDNS;
typedef TConfig<CONFIG_NET_ALARMSERVER, MAX_ALARMSERVER_TYPE, 4> CConfigNetAlarmServer;
typedef TConfig<CONFIG_FTP_SERVER, N_FTP_SERVICE, 4> CConfigFTPServer;
typedef TConfig<CONFIG_FTP_APPLICATION, N_SYS_CH*2, 4> CConfigFTPApplication;
typedef TConfig<CONFIG_NET_NTP, 1, 4> CConfigNetNTP;
typedef TConfig<CONFIG_NET_EMAIL, 1, 4> CConfigNetEmail;
typedef TConfig<CONFIG_NET_SNIFFER, 1, 4> CConfigNetSniffer;
typedef TConfig<CONFIG_REG_SERVER,1,4> CConifgRegServer;
typedef TConfig<CONFIG_DHCP_CFG,2,4> CConfigDHCP;
typedef TConfig<CONFIG_NET_PPPOE,1,4> CConfigPPPoE;//added by wyf on 090929 
//typedef TConfig<NET_DDNS_CONFIG_NEW, DDNS_MAX_NUM, 4> CConfigNetDDNS;

#ifdef VSERVER   //add by kyle xu in 20160107
typedef TConfig<CONFIG_NET_VSERVER, 1, 4> CConfigNetVServer;//Add by kyle xu in 20151218
#endif




typedef TConfig<CONFIG_NET_UPNP, 1, 4> CConfigNetUPNP; //add by nike.xie 2001116
typedef TConfig<RTSP_SET, 1, 4> CConfigNetRtspSet;

typedef TConfig<SNET_PRESET_INFOR, N_SYS_CH, 4, CFG_NET_PRESET> CConfigPresetInfor;
template<> void exchangeTable<SNET_PRESET_INFOR>(CConfigExchange& xchg, CConfigTable& table, SNET_PRESET_INFOR& config, int index, int app);

typedef TConfig<SNET_TOUR_INFOR, N_SYS_CH, 4, CFG_NET_TOUR> CConfigTourInfor;
template<> void exchangeTable<SNET_TOUR_INFOR>(CConfigExchange& xchg, CConfigTable& table, SNET_TOUR_INFOR& config, int index, int app);

typedef TConfig<CONFIG_PTZREGRESS, N_SYS_CH, 4, CFG_CONFIG_PTZREGRESS> CConfigPtzRegress;
template<> void exchangeTable<CONFIG_PTZREGRESS>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PTZREGRESS& config, int index, int app);

typedef struct XinhuoPidCid
{
    int Cid0;     //请求通道的老的CID，没有时置-1
    int Pid0;     //请求通道的老的PID，没有时置-1
    int iRes1[2]; //保留字1

    int Cid1;     //请求通道的新的CID
    int Pid1;     //请求通道的新的PID，请求时置-1
    int iRes2[2]; //保留字2
}TXHPidCid;

#endif //__CONFIG_CONFIGNET__

