#ifndef __CONFIG_DIGI_CH_MANAGER__
#define __CONFIG_DIGI_CH_MANAGER__

#include "config-x.h"
#include "System/Object.h"
#include "System/File.h"
#include "AuxLibs/Zlib/zlib.h"
#include "MultiTask/Timer.h"
#include "Configs/ConfigNetCamera.h"

#include <map>
#include <set>


#define MAX_TOUR_NUM 1
#define NETCAMERA_NAME_PASSWORD_LEN 32

typedef struct tagREMOTE_CH_CFG
{
    VD_INT32     iLocalChannelNo;/*本地通道*/
    VD_INT32     iCfgIndex;/*配置序号，从0开始*/
    VD_BOOL      BEnable;                  //使能开关
    char         cChName[NETCAMERA_NAME_PASSWORD_LEN];/* 远程配置名称 */
    IPADDR       DeviceIP;                            ///< 待连接设备的ip地址
    VD_INT32     iDevicePort;                        ///< 待连接设备的端口号
    char         cDeviceUserName[NETCAMERA_NAME_PASSWORD_LEN]; ///< 用户名
    char         cDevicePassword[NETCAMERA_NAME_PASSWORD_LEN]; ///< 密码    
    VD_INT32     iDevType;                            ///< 设备类型，按枚举表示，即协议类型,0表示tcp直连，3 rtsp+udp 4 vs300
    VD_INT32     iRemoteChannelNo;                    ///< 远程通道号,从0开始算起  
    VD_INT32     iStreamType;                         ///< 要连接的码流类G停?0:主码流， 1:辅码流
    VD_INT32     iRemotePtzPreset;//远程预置点
    VD_INT32     iRemotePtzPresetEnable;//是否一连接上就设置远程预置点 
    VD_INT32     iConType;                        //用掉一个保留位，1tcp 2udp
    VD_INT32     reserverd[4];                        ///< 保留字节
}REMOTE_CH_CFG;

typedef REMOTE_CH_CFG NET_EMOTE_CH_CFG;

enum DIGI_CH_MODE
{
    DIGI_CH_MODE_SINGLE = 0,
    DIGI_CH_MODE_TOUR = 1
};

typedef struct tagLOCAL_CH_CFG
{
    VD_BOOL      BEnable;//使能开关
    VD_INT32     iLocalChannelNo; /*本地通道*/
    VD_INT32     iDeviceChannelType;                    ///< 通道类型，0表示数字通道，1表示模拟通道，默认为数字通道,暂时不可以切换
    VD_INT32     iMode;//0 单连接 1 多连接
    VD_UINT32    uiTourTime;/* 多连接轮巡时间,10 - 600s */
    VD_UINT32    uiNotTourCfgIndex; /* 单连接时的采用的通道配置,在轮巡列表中的位置: 从0开始*/
    VD_INT32     iDecodePolicy;   // //解码策略     (-2, 最实时 -1 实时 0 默认情况 1 流畅 2,    最流畅 ) 以后可能扩展到自定义具体的延时时间
    VD_INT32     iAVEnable;       //0,只解视频 1，只解音频 ，2 解音视频 
    VD_INT32     BAutoNet;       //自动组网功能
    VD_INT32     BPTZLocal;  /* 用掉4个字节，表示该通道数字通道
                                                    云台控制接NVR还是发到IPC前端 */
    VD_INT32     iReserverd[3];          ///< 保留字节
}LOCAL_CH_CFG;

typedef struct tagREMOTE_DIGI_CH_CFG
{
    REMOTE_CH_CFG stRemoteCfg[MAX_TOUR_NUM];/* 轮巡列表 */
}REMOTE_DIGI_CH_CFG;

typedef TConfig<LOCAL_CH_CFG, N_SYS_CH, 8> CConfigLocalDigiChCfg;
template<> void exchangeTable<LOCAL_CH_CFG>(CConfigExchange& xchg, CConfigTable& table, LOCAL_CH_CFG& config, int index, int app);

typedef TConfig<REMOTE_DIGI_CH_CFG, N_SYS_CH, 8> CConfigRemoteDigiChCfg;
template<> void exchangeTable<REMOTE_DIGI_CH_CFG>(CConfigExchange& xchg, CConfigTable& table, REMOTE_DIGI_CH_CFG& config, int index, int app);

class CConfigDigiChManager : public CObject
{
    typedef std::map<std::string, CConfigExchange*> CONFIG_MAP;
    typedef std::map<int, std::string> CONFIG_MAPR;
    typedef std::map<std::string, int> MAP_TRANSLATE;
    typedef std::map<std::string, int>::value_type valueType;
    typedef std::set<std::string> ConfigSetExceptional;

public:
    PATTERN_SINGLETON_DECLARE(CConfigDigiChManager);
    
    CConfigDigiChManager();

    //! 初始化
    void initialize();
    int recallConfig(const char* name, const char* user = NULL, int index = -1);
    int recallConfigAll(const char* user = NULL);
    //! 保存文件
    void saveFile();
    //! 设置默认配置，供GUI界面和网络模块调用
    int SetDefaultConfig(int iConfigType);

    /// 导出配置文件
    int GetConfigFileData(void* pBuf, int& iLen);
    /// 导入配置文件
    int SetConfigFileData(void* pBuf, int iLen, ConfigSetExceptional& cfgExceptional);
    
private:
    void setupConfig(const char* name, CConfigExchange& xchg);
    void onConfigChanged(CConfigExchange& xchg, int& ret);
    //CConfigTable& locate(CConfigTable& table, const char* name);
    const char* nameFromID(int id);
    CConfigExchange* xchgFromName(const char* name);
    VD_BOOL readConfig(const char* chPath, std::string& input);
    void onTimer(uint arg);
    //!恢复网络端不是保存在配置文件中的配置
    int setDefaultNetConfig();

private:
    CConfigTable m_configAll;                /*!< 配置总表 */
    VD_BOOL m_changed;                            /*!< 配置表变化了 */
    gzFile m_fileConfig;                    /*!< 配置文件 */

    CONFIG_MAP m_map;                        /*!< 配置名称和配置类指针映射表 */
    CONFIG_MAPR m_mapReverse;                /*!< 配置ID和配置名称和映射表 */

    MAP_TRANSLATE m_mapTranslate;            /*!< 配置名称和配置类型映射表*/

    CConfigLocalDigiChCfg m_CConfigLocalDigiChCfg;
    CConfigRemoteDigiChCfg m_CConfigRemoteDigiChCfg;

#ifdef ZXBELL
    CConfigBellResourceCfg m_CConfigBellResourceCfg;    
#endif

    CTimer m_Timer;
    std::string m_stream;    // 字符串流
    
private:
    int ZipConfig(CConfigTable& cfg);
    int CopyConfigData(void* pBuf, int& iLen);
    int WriteConfigData(void* pBuf, int iLen);
    int UnZipConfig(CConfigTable& cfg);

private:
    gzFile             m_ZipConfig;
    CFile             m_pFileReadWrite;
    std::string     m_StrConfig;
    
#define TempZipDigiChConfigFile            "/var/tmp/ZipConfigTmp2"
};

#define g_ConfigDigiCh (*CConfigDigiChManager::instance())
//#endif
#endif //__CONFIG_MANAGER__

