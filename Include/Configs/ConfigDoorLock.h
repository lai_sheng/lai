#ifndef CONFIGDOORLOCK_H_
#define CONFIGDOORLOCK_H_

#include "ConfigBase.h"
#include "Configs/ConfigAwakeTime.h"


#define USERNUM_MAX		(100)   //最大用户数量
#define LEAVEMSG_MAX	(10)	//留言最大数量
#define MSGCACHE_MAX	(15)	//锁端消息缓冲最大数量

typedef enum{
	_OPEN_KEY		=	0x00,  //钥匙开锁
	_OPEN_PSW		=	0x01,  //密码开锁
	_OPEN_FINGER	=	0x02,  //指纹开锁
	_OPEN_CARD		=	0x03,  //门卡开锁		
	_OPEN_TEMPPSW	=	0x04,  //临时密码开锁
	_OPEN_OTHER,
}CONFIG_OPENTYPE;


typedef struct tagCONFIG_USERINFO{
	VD_BOOL bEnable;		

/*已经录入的开锁方式(为TRUE)则已录入,对应方式对应CONFIG_OPENTYPE*/	
	VD_BOOL bSupportType[_OPEN_OTHER];	
} CONFIG_USERINFO;


typedef TConfig<CONFIG_USERINFO, USERNUM_MAX, 4> CConfigLockUserInfo;

typedef struct tagCONFIG_LEAVEMSG{
	VD_BOOL bEnable;

	int 	iUserNum;					//用户编号	
	VD_BOOL bSupportType[_OPEN_OTHER];	//支持的开锁类型
	VD_BOOL	daySwitch[WDAYS];			//播放的时间(周几)
	char 	cPlayUrl[1024];				//留言下载地址
	int 	iReplay;					//播放次数
	int		iHavePlay;					//已经播放了多少次
} CONFIG_LEAVEMSG;
typedef TConfig<CONFIG_LEAVEMSG, LEAVEMSG_MAX, 4> CConfigLockLeaveMsg;

typedef struct tagCONFIG_WARNVOICE{
    VD_BOOL bEnable;
    char 	cPlayUrl[1024];	
}CONDIG_WARNVOICE;

typedef TConfig<CONDIG_WARNVOICE, 1, 4> CConfigLockWarnVoice;

	
typedef enum{
	_LOCKMSG_OPEN		= 0x00,//开锁消息
	_LOCKMSG_ALARM		= 0x01,//防撬报警
	_LOCKMSG_ADDUSER	= 0x02,//添加密钥，数据内包含用户ＩＤ及密钥ＩＤ
	_LOCKMSG_DELUSER	= 0x03,//删除密钥，数据内包含用户ＩＤ及密钥ＩＤ
	_LOCKMSG_DOORBELL	= 0x04,//门铃
	_LOCKMSG_SYSTEMLOCK	= 0x05,//系统被锁定
	_LOCKMSG_LOCKINIT	= 0x06,//门锁初始化
	_LOCKMSG_OTHER,
}CONFIG_LCOKMSGTYPE;

typedef struct tagCONFIG_LOCKMSGCACHE{
	VD_BOOL 			bEnable;	

	CONFIG_LCOKMSGTYPE	sMsgType;	
	int 				iUserNum;	
	CONFIG_OPENTYPE		sOpenType;
	uint				uTimeStamp;
} CONFIG_LOCKMSGCACHE;

typedef TConfig<CONFIG_LOCKMSGCACHE, MSGCACHE_MAX, 4> CConfigLockMsgCache;

#endif

