#ifndef __CONFIG_AUTOMAINTAIN__
#define __CONFIG_AUTOMAINTAIN__

#include "ConfigBase.h"

//!自动维护设置
typedef struct tagCONFIG_AUTOMAINTAIN
{
	/*!< 自动重启设置日期	“Never”, “Everyday”, “Sunday”, “Monday”, 
		Tuesday”, “Wednesday”, “Thursday”, “Friday”, “Saturday”*/	
	int iAutoRebootDay;	
	//!重启整点时间	[0, 23]	
	int iAutoRebootHour;
	//!自动删除文件时间(天)	[0, 30]
	int iAutoDeleteFilesDays;	
} CONFIG_AUTOMAINTAIN;

typedef TConfig<CONFIG_AUTOMAINTAIN, 1, 4> CConfigAutoMaintain;
#endif //__CONFIG_AUTOMAINTAIN__

