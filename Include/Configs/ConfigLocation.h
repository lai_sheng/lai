#ifndef __CONFIG_LOCATION__
#define __CONFIG_LOCATION__

#include "ConfigBase.h"

enum dst_rule
{
	DST_OFF = 0,	//关闭
	DST_AUSTRALIA,	//澳洲规则
	DST_ITALY,		//意大利规则
	DST_AMERICAN,	//美国规则
	DST_NR			//种类计数
};

typedef  struct tagDSTTIMES
{
	int Hour;
	int Minute;
}DSTTIMES;

//! 夏令时结构
typedef struct tagDST_POINT
{
	//!year [2000,2037]
	int	iYear;
	//!month from January=1 [1,12]
	int	iMonth;
	//!周1:first  to2 3 4 -1:last one   0:表示使用按日计算的方法[-1,4]
	int	iWeek;
	union
	{
		int	iWeekDay;	//!weekday from sunday=0	[0, 6]
		int iDays;		//!day from one=1[0, 31]
	};
	DSTTIMES time;
} DST_POINT;

//!区域设置
typedef struct tagCONFIG_LOCATION
{
	int iVideoFormat;		/*!< 视频制式:“PAL”, “NTSC”, “SECAM” */	
	int iLanguage;			/*!< 语言选择:“English”, “SimpChinese”, “TradChinese”, “Italian”, “Spanish”, “Japanese”, “Russian”, “French”, “German” */	
	int iOldLanguage;          //add by nike.xie 增加语言是否变化的配置
	int iDateFormat;		/*!< 日期格式:“YYMMDD”, “MMDDYY”, “DDMMYY” */
	int iDateSeparator;		/*!< 日期分割符:“.”, “-”, “/” */
	int iTimeFormat;		/*!< 时间格式:“12”, “24” */
	int iDSTRule;			/*!< 夏令时规则 */
	int iWorkDay;        /*!< 工作日*/
	//int iFreeDay;        /*!< 非工作日*/
	DST_POINT	iDST[2];
} CONFIG_LOCATION;

typedef TConfig<CONFIG_LOCATION, 1, 4> CConfigLocation;
#endif //__CONFIG_LOCATION__
