#ifndef __CONFIG_GENERAL__
#define __CONFIG_GENERAL__

#include "ConfigBase.h"
#include "APIs/Video.h"

//!	普通配置
typedef struct tagCONFIG_GENERAL
{	
	/*!< 本机编号:[0, 998] */	
	int iLocalNo;
	/*!< 硬盘满时处理 "OverWrite", "StopRecord" */		
	int iOverWrite;
	/*!< 定时抓图的时间间隔，以秒为单位 */
	int iSnapInterval;
	char chMachineName[32];
	/*!<LCD屏幕自动关闭*/
	int	 iLCDScreenClsDown;
	/*!< 输出模式 */
	int iVideoStartOutPut;
	//!屏保时间(分钟)	[0, 120]
	int iScreenSaveTime;
	//!本地菜单自动注销(分钟)	[0, 120]
	int iAutoLogout;	
#if defined(DEF_RESOLUTION_ADJUST)
	/// 分辨率类型,参见enum_videout_type枚举
	int iFixType;
#endif
	int iLedSwitch; //0: open 1 :close
	//和目时间段待机模式 0:normal 1:suspend
	int iSuspendMode;
	//和目开关模式
	int iOnOffMode;
} CONFIG_GENERAL;

typedef TConfig<CONFIG_GENERAL, 1, 4> CConfigGeneral;

#endif //__CONFIG_GENERAL__
