#ifndef __CONFIG_CHANNELTITLE__
#define __CONFIG_CHANNELTITLE__

#include "ConfigBase.h"


#define CHANNEL_NAME_MAX_LEN 64

//!通道名称
typedef struct tagCONFIG_CHANNELTITLE
{
	//!通道名
	char strName[CHANNEL_NAME_MAX_LEN];
	//!通道编号	64位整型
	int64 iSerialNo;	
} CONFIG_CHANNELTITLE;

typedef struct tagCONFIG_OSDPARAM
{
    uint size; //字体大小,1表示标准倍数，2表示2倍大小  该值取值范围在1到ucZoomAbility之间
    uint index;//用掉一个保留位，表示通道
    uint res[2]; //保留
}CONFIG_OSDPARAM;


typedef TConfig<CONFIG_CHANNELTITLE, N_SYS_CH, 4> CConfigChannelTitle;
typedef TConfig<CONFIG_OSDPARAM, N_SYS_CH, 4> CConfigOSDParam;

#endif //__CONFIG_CHANNELTITLE__

