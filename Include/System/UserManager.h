#ifndef __USER_MANAGER_H__
#define __USER_MANAGER_H__

#include "User.h"
#include "Configs/ConfigBase.h"
#include "Configs/ConfigNet.h"
#include "System/Object.h"
#include <string>
#include "AuxLibs/Zlib/zlib.h"

class CFile;  

#define SYS_MAX_USERNAME_LEN 64
#define SYS_MAX_PASSWD_LEN 64
#define SYS_MAX_GROUPNAME_LEN 6

#define AUTHORITY_STRING_LEN 32	//added by wyf on 091209

enum EnUGMErrorno
{
	eue_NoneError = 0 ,   /* 无错 */
	eue_InputNotValid=1 ,   /* 输入不合法 */
	eue_IndexOverlap=2 , 		 /* 索引重复如要增加的用户已经存在等 */
	eue_ObjectNone=3,			/*不存在对象, 用于查询时*/
	eue_ObjectNotValid=4, /* 对象不存在*/
	eue_ObjectInUse=5,	/* 目标正在使用中,如组被用，不能删除*/
	/* 子集超范围
	* (如组的权限超过权限表，用户权限超出组的权限范围等等) 
	*/
	eue_SubsetOverlap = 6,
	eue_SaveCfgError = 7,		/* 保存配置失败 */
	eue_PwdNotValid = 8,		/*密码不正确 */
	eue_PwdNotMatch = 9,		/* 密码不匹配 */
	eue_ReservedAccount = 10,		/* 保留帐号 */
};

class CUserManager : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CUserManager);

	/// 用户外部配置接口，必须在用户管理启动前调用
	/// \param [in] versionMajor 主版本权限
	/// \param [in] versionMinor 次版本权限
	/// \param [in] config1 用户配置文件
	/// \param [in] config2 备份的用户配置文件
	/// \param [in] customConfig 用户自定义的配置文件
	/// \param [in] adminAuthList 管理用户权限列表
	/// \param [in] adminAuthNum 管理用户权限数
	/// \param [in] userAuthList 普通用户权限数量
	/// \param [in] userAuthList 普通用户权限数量
	static void config(int versionMajor, int versionMinor, const std::string &config1, const std::string &config2, const std::string &customConfig,
				char adminAuthList[][AUTHORITY_STRING_LEN], int adminAuthNum, 
				char userAhthList[][AUTHORITY_STRING_LEN], int userAuthNum);//modified by wyf on 091209

	/// 构造函数
	CUserManager();

	/// 析构函数
	~CUserManager();

	/// 启动用户管理
	bool start();

	/// 获取整个权限列表
	const CConfigTable& getAuthorityList() const;

	/// 增加组
	/// \param [in] group 要增加的组, 参考<<AuthorityList.doc>>
	bool addGroup(const CConfigTable &group);

	/// 删除组
	/// \param [in] name 组名
	bool delGroup(const std::string &name);

	/// 修改组
	/// \param [in] name 要修改的组名
	/// \param [in] newGroup 新组信息, 参考<<AuthorityList.doc>>
	bool modGroup(const std::string &name, const CConfigTable &newGroup);

	/// 根据组名获取组信息
	/// \param [in] name 要获取组信息的组名
	/// \param [out] group 用户存放组信息,参考<<AuthorityList.doc>>
	bool getGroup(const std::string &name, CConfigTable &group);

	/// 获取所有组信息
	/// \return 所有组的信息, 参考<<AuthorityList.doc>>
	bool getGroupAll(CConfigTable &groups);

	/// 添加用户
	/// \param [in] curUser 要添加的用户 
	/// \param [in] flag 是否加密密码，默认加密
	bool addUser(const CConfigTable &curUser, PasswordEncrypt flag = needEncrypt);

	/// 根据用户名删除用户
	/// \param [in] name 要删除的用户名
	bool delUser(const std::string &name);

	/// 根据用户名修改信息
	/// \param [in] name 要修改的用户名
	/// \param [in] user 要修改的用户信息, 参见<<AuthorityList.doc>>
	/// \param [in] flag 密码加密标识
	bool modUser(const std::string &name, const CConfigTable &user, PasswordEncrypt flag = needEncrypt);

	/// 根据用户id修改用户密码
	/// \param [in] name 要修改的用户名
	/// \param [in] passwd 新的密码
	/// \param [in] flag 密码是否加密存放
	bool modPassword(const std::string &name, const std::string &passwd, PasswordEncrypt flag = needEncrypt);

	/// 根据用户名获取用户信息
	/// \param [in] name 要获取用户信息的用户名
	/// \param [out] user 存放用户信息, 参见<<AuthorityList.doc>>
	bool getUser(const std::string &name, CConfigTable &user);

	/// 获取所有用户, 参见<<AuthorityList.doc>>
	/// \return 所有用户的信息
	bool getUserAll(CConfigTable &users);
	
      /// 还原默认用户的密码
      bool setDefaultPwd();
    
	/// 还原默认
	bool setDefault();

	/// 保存所有信息
	bool saveFile();

	/// 判断用户是否是保留用户
	bool isReservedUser(const std::string& userName);

	/// 判断用户是否是默认用户
	bool isDefaultUser(const std::string& userName);

	/// 判断所提交的密码是否合法
	/// \param [in] name 用户名
	/// \param [in] passwd 用户密码
	/// \param [in] flag 密码保存方式，明文或者秘文
	/// \param [in] superPwdValid 暂时用作保留
	bool isPasswdValid(const std::string &name, const std::string &passwd,	PasswordEncrypt flag = needEncrypt,  bool superPwdValid = false);

	/// 得到所有活动用户的ID列表
	std::vector<int> getActiveUserIdList();

	/// 得到活动用户
	CUser* getActiveUser(int id);

	/// 根据用户名得到得到活动用户
	CUser* getActiveUser(const std::string &userName);

	/// 释放活动用户，必须和GetActiveUser成对调用，并保证间隔尽量短
	void releaseActiveUser(CUser* user);

	// 地址过滤配置
	void onAccessFilter(CConfigNetIPFilter& xchg, int& ret);

	/// 是否是合法的地址
	bool isAllowedAddress(const std::string &address);

	/// 设置错误编号
	void setLastError(int error);

	/// 获取错误编号
	int getLastError() const;

public:
	/// 得到锁定状态
	/// return 锁定返回true 反之false
	bool getLockState(ulong& times);

	/// 设置锁定状态
	bool setLockState(bool bLock);
	
	/// 得到锁定周期，ms为单位
	int getLockPeriod() const;

	/// 返回密码错误时的登陆次数
	int getTryLoginTimes() const;

	/// 加入活动用户
	bool addActiveUser(CUser* user);

	/// 删除活动用户
	bool removeActiveUser(CUser* user);

	/// 该用户名是否已经登陆
	bool isUserLogined(const std::string& userName);

	/// 是否为共享用户
	bool isUserSharable(const std::string& userName);

	/// 指定组是否在使用
	bool isGroupInUse(const std::string &userName);

	/// 根据用户名查找用户的位置索引
	bool hasAuthorityOf(const std::string &userName, const char* auth);

	/// 根据用户名查找用户的位置索引
	int findUser(const std::string &userName);

	/// 根据用户名查找用户的位置索引
	int findGroup(const std::string &groupName);
	
	/// 擦除权限列表table中的authority
	static bool eraseAuthority(CConfigTable &table, const std::string &authority);

private:

//	bool loadFile(const char *filePath, std::string &input);
	VD_BOOL readConfig(const char* chPath, std::string& input);

	bool IsChildVector(const CConfigTable &ChildVec, const CConfigTable &Vec);
	
private:
	typedef std::map<int, CUser *> UserMap;
	bool			m_bUserLocked;		/// 当前是否有用户被锁定	
	ulong			m_uLockTime;		// 解锁有效时间，ms为单位
	int				m_lockPeriod;
	int				m_tryLoginTime;
	CMutex			m_mutex;
	UserMap			m_activeUsers;	
	CConfigTable	m_configAll;		// Json配置总表
	int				m_errorNo;
	std::string		m_stream;			// 配置字符串流，由Json配置转换而来，最终被写到文件
	gzFile			m_fileConfig;
    
	CConfigTable	m_configAccessFilter;
	static CConfigTable sm_adminAuthorityList;
	static CConfigTable sm_userAurthorityList;
	static int	sm_versionMajor;
	static int	sm_versionMinor;
	static std::string sm_firstFilePath;
	static std::string sm_secondFilePath;
	static std::string sm_customFilePath;
};

#define g_userManager (*CUserManager::instance())
#endif
