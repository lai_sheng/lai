


#ifndef __DVR_H__
#define __DVR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include"APIs/System.h"

extern SYSTEM_CAPS_EX g_CapsEx;//一些扩展的性能结构
extern int g_ShieldUSBDev;

#ifndef ASSERT
#define ASSERT(x) \
	if(!(x)) \
	printf("ASSERT: %s,%d\n",__FILE__,__LINE__);
#endif


void FormatTimeString(SYSTEM_TIME *p,char* buf,int flag);
void FormatTimeStringDH(DHTIME *p,char* buf,int flag);
int VD_MessageBox(VD_PCSTR psz,  int type);
int PasswordBox(const char *auth);
void timedh2sys(SYSTEM_TIME * systime, DHTIME * dhtime);
void timesys2dh(DHTIME * dhtime, SYSTEM_TIME * systime);
int timecompare(SYSTEM_TIME * pSysTime1, SYSTEM_TIME * pSysTime2);
void timeadd(SYSTEM_TIME * pOldTime, SYSTEM_TIME * pNewTime, int second);
int time2second(SYSTEM_TIME * pSysTime1, SYSTEM_TIME * pSysTime2);
int log2i(uint value);	//得到整数的底数为2的对数
void DumpHex(uchar *pdat);

//指令执行时间测试函数
void TestTimerStart(const char* file, int line);
void TestTimerSample(const char* file, int line);
void TestTimerStop(const char* file, int line);

//文件系统要用到的系统函数
void SYS_sleep(int ms);
uint SYS_10msCnt(void);
int SYS_getrtctime(DHTIME* ptime);
int SYS_getsystime(DHTIME* ptime);
int SYS_getsysweek(SYSTEM_TIME * systime, char *buf, int flag);

// 容量转换成字符串，默认单位是k,可以设置成M,G,T只需将unitindex改成1,2,3
void Capa2Str(char *pstr, uint64 capa, int index);
//! 调试用到的语句
void printAddr(void *pAddr, int col, int len);		

//时间结构操作
int compare_in_time(DHTIME t,DHTIME t1,DHTIME t2);
DHTIME add_time(DHTIME time, int second);
int compare_time(DHTIME t1,DHTIME t2);
int changeto_second(DHTIME end_time, DHTIME start_time);


#ifdef __cplusplus
}
#endif
#endif //__DVR_H__


