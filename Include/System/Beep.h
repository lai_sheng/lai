
#ifndef SYSTEM_BEEP_H
#define SYSTEM_BEEP_H

#include "System/Object.h"
#include "MultiTask/Thread.h"

class CBeep : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CBeep);

	void CallBeep(uint frequence = 900, uint duration = 50);

private:
	//! 构造函数
	CBeep();

	//! 析构函数
	~CBeep();

	void beep(uint param);

	Threadlet m_threadlet;
};

#endif
