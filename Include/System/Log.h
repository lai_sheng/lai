

#ifndef __LOG_H__
#define __LOG_H__

#include "APIs/System.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "MultiTask/Thread.h"
#include "Object.h"
#include "System/File.h"
#include "CMOS.h"
#include <string>
#include <map>

enum vd_shutdown_t {
SHUT_NORM =0,
SHUT_ERROR,
SHUT_ONFIF,
SHUT_NODATA,
};

enum dh_log_t {
    //system logs
    LOG_REBOOT     = 0x0000,
    LOG_SHUT,

    //config logs
    LOG_CONFSAVE = 0x0100,
    LOG_CONFLOAD,
    
    //fs logs
    LOG_FSERROR     = 0x0200,    //文件系统错误
    LOG_HDD_WERR,            //硬盘写错误
    LOG_HDD_RERR,            //硬盘读错误    
    LOG_HDD_TYPE,            //设置硬盘类型
    LOG_HDD_FORMAT,            //格式化硬盘
    LOG_HDD_NOSPACE,        //当前工作盘空间不足
    LOG_HDD_TYPE_RW,        //设置硬盘类型为读写盘
    LOG_HDD_TYPE_RO,        //设置硬盘类型为只读盘    
    LOG_HDD_TYPE_RE,        //设置硬盘类型为冗余盘
    LOG_HDD_NODISK,       //无硬盘
    LOG_HDD_ERROR,        //硬盘出错

    //alarm logs
    LOG_ALM_IN     = 0x0300,
    LOG_NETALM_IN,
    LOG_ALM_END,
    LOG_LOSS_IN,
    LOG_LOSS_END,    
    LOG_MOTION_IN,
    LOG_MOTION_END,
    LOG_ALM_BOSHI,
    LOG_NET_ABORT,                 //网络断开
    LOG_NET_ABORT_RESUME,         //网络恢复
    LOG_CODER_BREAKDOWN,        //编码器故障
    LOG_CODER_BREAKDOWN_RESUME,    //编码器故障恢复
    LOG_NET_IP_CONFLICT,          //IP冲突
    LOG_BLIND_IN,				//遮挡
    LOG_BLIND_END,	
	
    //record
    LOG_AUTOMATIC_RECORD = 0x0400,
    LOG_MANUAL_RECORD,
    LOG_CLOSED_RECORD,

    //user manager
    LOG_LOGIN    = 0x0500,
    LOG_LOGOUT,
    LOG_ADD_USER,
    LOG_DELETE_USER,
    LOG_MODIFY_USER,
    LOG_ADD_GROUP,
    LOG_DELETE_GROUP,
    LOG_MODIFY_GROUP,
    LOG_MODIFY_PASSWORD,//add by wyf on 090730
    LOG_LOCK_USER,//added by wyf on 090813
    LOG_ILLEGAL_ACCESS,//added by wyf on 090814

    //clear
    LOG_CLEAR    = 0x0600,
    LOG_SEARCHLOG,            //查询日志    

    //record operation
    LOG_SEARCH     = 0x0700,    //录像查询
    LOG_DOWNLOAD,            //录像下载
    LOG_PLAYBACK,            //录像回放
    LOG_BACKUP,                /*!< 备份录像文件 */
    LOG_BACKUPERROR,        /*!< 备份录像文件失败 */

    LOG_TYPE_NR = 8,
};

#define MAX_LOG_LOOP    250


#define LOG_MAX           4096    //modified by nike.xie //2048 //日志总共条数
#define LOG_BUFFER        4096    // 缓冲中的日志项数
#define LOG_TYPE_MAX    16        //日志总的类型数
#define LOG_LINE_MAX    80        //日志信息字符串最大长度
#define LF_NORMAL        0x00    //正常日志记录
#define LF_EXTERN        0x01    //扩展日志记录
#define LF_CONTXT        0x02    //扩展数据内容

struct LOG_HEADER
{
    char    lable[56];    //日志标签
    uint    head;        //日志头指针
    uint    tail;        //日志尾指针
};

struct LOG_ITEM
{
    DHTIME    time;        //日期
    ushort    type;        //类型
    uchar    flag;        //标记, 0x00 正常，0x01 扩展，0x02 数据
    uchar    data;        //数据
    uchar    context[8]; //内容
};

//用于保存录像文件操作日志信息的结构
typedef struct _LOG_Item_Record
{
    DHTIME    time;        //时间
    uchar    chan;        //通道
    uchar    type;        //录像类型
    uchar    reserved[2];
}LOG_ITEM_RECORD, *pLOG_ITEM_RECORD;

class CLog {
public:
    PATTERN_SINGLETON_DECLARE(CLog);
    CLog();
    virtual ~CLog();

public:
    void    Init();
    void    Append(ushort Type, uchar Data = 0, void* pContext = NULL, uint Length = 8);
    void    Clear(const char *pUserName);
    uint    GetCount();
    uint    GetItem(uint Offset, LOG_ITEM *pBuffer, uint Count = 1);
    void    SetRange(SYSTEM_TIME* pStartTime, SYSTEM_TIME* pEndTime);

    /* bFlag,查找方向标志，0从小到大，1从大往小 */
    int     GetContext(ushort Type, uint *pPos, char *Str,LOG_ITEM *pLogItem = NULL,bool bFlag = 0);

    void    GetPos(uint *pHead, uint *pTail);
    void    DumpInfo();
    void    DumpStat();
    void    SetLogFilter(int *Type);
    void    GetLogFilter(int *Type);
    void	setLogDisable();

private:
    uint    _GetItem(uint Pos, LOG_ITEM *pBuffer, uint Count = 1);
    void    Translate(LOG_ITEM *pItem, uint Language);

private:

    LOG_HEADER    m_Header;
    LOG_ITEM    m_BufferItem[LOG_BUFFER];
    uint        m_BufferPos;
    CMutex        m_Mutex;
    CFile        *m_File;
    CCMOS        m_CMOS;

    CFile        *m_FileLogSet;
    int            m_SavedLogType[LOG_TYPE_MAX + 1]; //最后一位用来判断日志满是否需要覆盖

    DHTIME        m_StartTime;    //查找范围的起始时间
    DHTIME        m_EndTime;        //查找范围的结束时间

    VD_BOOL m_bLogDisable;	
};

class CLogToSDCard : public CThread
{
public:
    PATTERN_SINGLETON_DECLARE(CLogToSDCard);
	
	VD_BOOL Start();
	void ThreadProc();

	VD_BOOL NetGetDebugEnable();
	VD_BOOL NetSetDebugEnable(int iEnable);
private:
	CLogToSDCard();
	virtual ~CLogToSDCard();
	VD_BOOL WriteToFile();
	

public:
	int m_iEnable;
	int m_iWirteStatus;  //0:未开始 1:已经开始写
	int m_iFd;           //文件sock
    char m_DateStr[40];
 }; 

#define g_SdLog (*CLogToSDCard::instance())



#define g_Log (*CLog::instance())

#endif //__LOG_H__
