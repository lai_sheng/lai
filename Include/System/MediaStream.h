

#ifndef _MEDIA_STREAM_H_
#define _MEDIA_STREAM_H_

#include "APIs/DVRDEF.H"
#include "APIs/Ide.h"



#define MEDIA_STREAM_ERR -1
#define MEDIA_STREAM_OK 0

int FindFrameMarker(uchar *buf, int nLen, uint *pMP4Code);
int MediaStreamParse(uchar * pBuffer, uint *pCount, VD_BOOL bFirst);

#endif// _MEDIA_STREAM_H_


