#ifndef __INTER_CONSOLE_H__
#define __INTER_CONSOLE_H__

#include "MultiTask/Timer.h"
#include "System/User.h"
#include "System/Function.h"
#include <map>

#include <string>

#ifdef WIN32
	#include <io.h>
#else
	#include <unistd.h>
#endif

struct ConsoleArgInternal
{
	ConsoleArgInternal(int argc, char **argv)
	{
		m_argc = argc;
		m_argv = argv;
	}

	int		m_argc;		///<命令行参数个数
	char**	m_argv;		///<命令行参数内容

};

/// \brief 控制台参数类，封装用户输入命令行参数，提供更方便的参数使用接口。
class CConsoleArg
{
public:
	/// 构造函数
	/// \param argc 参数个数
	/// \param argv 参数字符串数组
	CConsoleArg(int argc, char **argv);

	/// 析构函数
	~CConsoleArg();

	/// 得到命令的选项，命令选项指命令行参数中'-'符号后的字符串
	///\return 得到的命令行参数的字符串
	char* getOptions();

	/// 得到pos位置字符串形式的命令行参数
	/// \param pos 参数序号
	/// \return 字符串形式命令行参数,失败返回 false
	char * GetArg(int pos);

	/// 得到pos位置命令参数，转换为16进制
	/// \param pos 参数序号
	/// \return 十六进制形式命令行参数
	int getHex(int pos);

	/// 得到pos位置命令参数，转换为整型
	/// \param pos 参数序号
	/// \return 十进制形式命令行参数
	int getInt(int pos);

private:

	ConsoleArgInternal	*m_internal;
};


/// \brief 控制台类，其他模块的控制函数可以注册到控制台模块，当用户数据符合该入口
/// 的命令字符串时，控制函数会被自动回调。
class CConsole : public CThread
{
public:
	/// 单件模式
	PATTERN_SINGLETON_DECLARE(CConsole);
	CConsole();
	~CConsole();

public:

	/// 控制函数定义
	/// 返回值是int类型
	/// 第一个参数是int类型，表示命令函数参数的个数，不包括命令本身的名称。
	/// 第一个参数是char**类型，表示命令函数参数的列表，不包括命令本身的名称。
	typedef TFunction2<int, int, char **> Proc;

private:

	/// 控制台状态
	enum State 
	{
		stateOff = 0,	///< 控制台关闭状态
		stateUserName,	///< 用户名输入状态
		statePassword,	///< 密码输入状态
		stateRunning,	///< 命令行状态
	};

	/// 控制台命令入口
	struct Command 
	{
		Proc	proc;			///< 解析函数
		std::string	helpStr;		///< 帮助信息
	};

	/// 管理控制函数的列表
	typedef std::map<std::string, Command> CommandTable;

public:
	/// 开启控制台功能
	bool Start(void);

	/// 关闭控制台功能
	bool Stop(void);

	/// 注册命令函数
	/// \param proc		命令函数
	/// \param cmdStr	命令名称字符串
	/// \param helpStr	命令帮助信息
	/// \return true 注册成功,false 注册失败
	bool registerCmd(Proc proc, char *cmdStr, char *helpStr);	

	/// 注销命令函数
	/// \param proc		命令函数
	/// \return true 注销成功,false 注销失败
	bool unregisterCmd(char *cmdStr);

private:
	void onLine(char* buf);
	void ThreadProc();
	void onPrintTimer(uint arg);
	int dumpCPUandPacket(uint arg);
	void setEcho(bool open);

private: /// built-in commands
	int onHelp(int argc, char ** argv);
	int onResource(int argc, char **argv);
	int onPacket(int argc, char **argv);
	int onBitRate(int argc, char **argv);
	int onReboot(int argc, char **argv);
	int onTime(int argc, char **argv);
	int onQuit(int argc, char **argv);
	int onShell(int argc, char **argv);
	int onTimer(int argc, char **argv);
	int onThread(int argc, char **argv);
	int onPartition(int argc, char **argv);
#if defined (SDK_3516)
	int onSwitchVideoOut(int argc, char **argv);
	int onSwitchProductType(int argc, char **argv);
#endif
	int OnAuth(int argc, char **argv);

private:
	int			m_iState;
	char		m_szUserName[32];
	int			m_iParams;		//命令行参数数量
	char		m_dbParamTab[16][32];
	CommandTable	m_sDbgTbl;
	bool		m_bUsage;
	bool		m_bPacket;
	bool		m_bBitrate;
	CUser		m_cUgmUser;
	CTimer		m_Timer;
};
#define g_Console (*CConsole::instance())

#endif// __INTER_CONSOLE_H__
