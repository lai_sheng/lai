
#ifndef __PACKET_H__
#define __PACKET_H__

#if defined(WIN32)
	#pragma warning (disable : 4786)
#endif
#include <deque>

#include <assert.h>
#include "Support.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "Object.h"

//#define PKT_HDR_SIZE 104
//ushort->int zhy 20150710 
#define PKT_HDR_SIZE (104+8*4)
#define PKT_PAGE_SIZE (1024)
//#define PKT_KILOS_DEFAULT (4096)
#define PKT_KILOS_DEFAULT (4096 * 4) //根据王远涛     求变更，以解决4M码流下有码赛克情况

enum PackFlagInfo
{
	PACK_FLAG_NONE,
	PACK_FLAG_BREAK = 1
};

/* 帧类型定义*/
enum PackTypeFlag
{
	PACK_TYPE_FRAME_P=0,
	PACK_TYPE_FRAME_I,
	PACK_TYPE_FRAME_B,
	PACK_TYPE_FRAME_AUDIO,
	PACK_TYPE_FRAME_JEPG,
	PACK_TYPE_FRAME_MOTION,
	PACK_TYPE_FRAME_NUM,
	PACK_TYPE_FRAME_NULL=0xff,/*结束，无数据*/
};

/* 帧头尾标志 */
enum FrameBeginEndFlag
{
	PACK_CONTAIN_FRAME_NONHT = 0, /*无头无尾,不含帧头或帧尾 */
	PACK_CONTAIN_FRAME_HEAD, /*只有头*/
	PACK_CONTAIN_FRAME_TRAIL,/*只有尾*/
	PACK_CONTAIN_FRAME_HEADTRAIL,/* 包含帧头和帧尾 */
	PACK_CONTAIN_FRAME_MOTION_HEAD = 5,
	PACK_CONTAIN_FRAME_NUM,
	
};

#define FRAME_MAX_NUM (8)

//! 帧定位信息
typedef struct
{
	uchar FrameType;					/*!< 帧类型 */
	uchar FrameFlag;					/*!< 帧头尾的标识 */
//ushort->int   zhy 20150710
	//ushort FramePos;					/*!< 帧的起始位置 */
	uchar reserve[2];					/*用于字节对齐*/
	int FramePos;					/*!< 帧的起始位置 */
	uint FrameLength;				/*!< 帧的长度，可能跨块 */
	uint DataLength;				/*!< 帧在本块中的长度 */
}FRAMEINFO;

//! 包头的信息定义
typedef struct
{
	FRAMEINFO FrameInfo[FRAME_MAX_NUM];  /*!< 所有帧信息 */
	uchar			PacketInfo;								 /*!< 打包信息 */
	uchar			Reserve[7];								 /*!< 保留 */
}PKT_HEAD_INFO;


class CPacketManager;
////////////////////////////////////////////////////////////////////////////////////
// CPacket
////////////////////////////////////////////////////////////////////////////////////
class CPacket {
	
public:
	uint	PutBuffer(void * pdat, uint dwLength);
	uchar *	GetBuffer();
	uint    SetLength(uint dwLength);		//设置包长度	
	uint	GetLength();					//取数据长度
	uint	GetSize();						//获取容量
	uint	GetLeft();						//取剩余容量
	uchar *	GetHeader();					//清空包头数据
	void	ClearHeader();					//清空包头数据
	uint	Release(int iId=-1);
	uint	AddRef(int iId=-1);
	uint	GetRef();
	void	GetSysTime(SYSTEM_TIME *time);
	void	SetSysTime(SYSTEM_TIME *time);

	int		m_iLastUId;

private:
	uchar *	m_pBuffer;		//缓冲指针
	uint	m_Size;			//大小
	uint	m_Length;		//数据长度
	uint	m_RefCount;		//引用计数
	CMutex	m_Mutex;		//互斥量
	SYSTEM_TIME  m_SysTime;  //保存包的当前时间
	uchar	m_Header[PKT_HDR_SIZE];
	CPacket *m_pNext;

private:
	CPacket();
	~CPacket();
	void Init();

	friend class CPacketManager;
};

class CPacketManager {
	
	typedef struct _PBN{//packet buffer node
		_PBN *	pPrev;
		_PBN *	pNext;
		int		nIndex;
	}PBN;

	typedef struct _PBA{//packet buffer array
		PBN *	pArray;
		PBN *	pHeader;
		uint	nCount;
	}PBA;

public:
	/// 配置包缓冲管理策略，需要在单件构造之前调用，否则使用缺省的配置。
	/// \param totalSize 需要管理的内存总字节数，将从系统内存中申请，由于对齐的
	///		   原因，实际申请的量会更大一点。缺省值为8M Bytes
	/// \param pageSize 页面的字节数，必须是2^nK 字节，也是能够申请到的内存块的
	///		   最小单位。缺省值为1K Bytes
	static void config(int totalSize, int pageSize );
	/*
	 * @brief linux通过malloc, new 申请的内存，不会真实的占用内存空间,只有在使用的时侯
	 * 才会真实占用，此选项控制所申请的内存是否使用延迟
	 * bDelay : true 使用，false:不使用
	 */
	static void MemoryDelay(bool bDelay);
public:
	PATTERN_SINGLETON_DECLARE(CPacketManager);
	CPacketManager();
	~CPacketManager();
	
public:
	CPacket*	GetPacket(uint dwBytes = 0);
	void		PutPacket(CPacket *pPacket);
	uint		GetBufferSize();
	void		Dump();
	void		DumpNodes();
	void		Test();

	uint		GetFreeSize();
	uint		GetUsedSize();

private:
	CPacket*	AllocPacket();
	void		FreePacket(CPacket *p);
	void		InsertFree(PBN * & pHeader, PBN * pThis);
	void		RemoveFree(PBN * & pHeader, PBN * pThis);
	
private:
	uchar *		m_pOriginBuffer;
	uchar *		m_pBuffer;
	CMutex		m_Mutex;
	PBA 		m_PBAs[32];//管理每种缓冲的节点数组
	int			m_nTypes;
	int			m_nPages;
	enum		{ NALL = 1024 };
	CPacket*	m_pFreeList;	

	static	int	m_pageSize;
	static	int	m_totalSize;
	static bool s_bMemeoryDelay;
};

#define g_PacketManager (*CPacketManager::instance())

int findFirstFrame(CPacket *pPkt, int itype = PACK_TYPE_FRAME_I);

//int findFirstMotionFrame(CPacket *pPkt);

VD_BOOL getFirstFrameTime(CPacket *pPkt, DHTIME *pDHTime, int itype = PACK_TYPE_FRAME_I);

int getPackFlag(CPacket *pPkt);

VD_BOOL getImageSizeFromPacket(CPacket* pPkt, VD_SIZE* pSize, int itype = PACK_TYPE_FRAME_I);

#endif// __PACKET_H__
