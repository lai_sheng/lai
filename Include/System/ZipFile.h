
#ifndef __ZIP_FILE_H__
#define __ZIP_FILE_H__

#include "System/File.h"

/// \class CZipFile
/// \brief 压缩文件服务封装
class CZipFile : public CFile
{
	typedef void *gzFile;

public:

	/// 构造函数。
	CZipFile();

	/// 析构函数。如果数据缓冲还有效，会被释放。
	virtual ~CZipFile();

	/// 打开文件。打开后文件指针偏移在0处，而以modeAppend标志打开的文件文件指针在文件末尾。
	/// \param [in] pFileName 文件名。
	/// \param [in] dwFlags 打开标志，默认为modeReadWrite。
	/// \retval true  打开成功
	/// \retval false  打开失败，文件不存在或无法打开。
	virtual bool Open(const char* pFileName, unsigned int dwFlags = modeReadWrite);

	/// 关闭文件。
	virtual void Close();

	/// 读文件数据。读操作后文件指针自动累加。
	/// \param [out] pBuffer 数据缓冲的指针。
	/// \param [in] dwCount 要读出的字节数
	/// \retval >0  读出的字节数
	/// \retval <=0 读失败
	virtual unsigned int Read(void *pBuffer, unsigned int dwCount);

	/// 写文件数据。读操作后文件指针自动累加。
	/// \param [out] pBuffer 数据缓冲的指针。
	/// \param [in] dwCount 要写入的字节数
	/// \retval >0  写入的字节数
	/// \retval <=0 写失败
	virtual unsigned int Write(void *pBuffer, unsigned int dwCount);

	/// 同步文件底层缓冲，在写操作后调用，确保写入的数据已经传给操作系统。
	virtual void Flush();

	/// 文件定位。
	/// \param [in] lOff 偏移量，字节为单位。
	/// \param [in] nFrom 偏移相对位置，最后得到的偏移为lOff+nFrom。
	/// \return 偏移后文件的指针位置。
	virtual unsigned int Seek(long lOff, unsigned int nFrom);

	/// 返回当前的文件指针位置
	virtual unsigned int GetPosition();

	/// 返回文件长度
	virtual unsigned int GetLength();

	/// 从文本文件当前偏移处读取一行字符串。读操作后文件指针自动累加。
	/// \param [out] s 数据缓冲。
	/// \param [in] size 需要读取的字符串长度。
	/// \retval NULL  读取失败
	/// \retval !NULL  字符串指针。
	virtual char * Gets(char *s, int size);

	/// 从文本文件当前偏移处写入一行字符串。写操作后文件指针自动累加。
	/// \param [in] s 数据缓冲。
	/// \return  实际写入字符串长度。
	virtual int Puts(const char *s);

	/// 判断文件是否打开
	virtual bool IsOpened();

private:

	gzFile	m_fp;
};

#endif //__ZIP_FILE_H__

