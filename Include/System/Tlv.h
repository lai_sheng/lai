
#ifndef __TLV_H__
#define __TLV_H__

#include <list>
#include <assert.h>

#include "System/Packet.h"
#include "System/Object.h"

/**
     * \brief CTlv
     */
class CTlv
{
public:
    CTlv();

    ~CTlv();
    //!分析数据tlv, 返回0表示成功
    int Parse(unsigned char* pbuf, uint len);
    //!重新打包tlv, 返回0表示成功
    int Pack(unsigned char*& pbuf, int& len);
    int GetType();
    uint GetLength();
    uint GetTotalLen();
    int GetValue(unsigned char* pbuf, uint size);
    unsigned char* GetValueBuf();

    int SetType( uint type );

    //!bTlv-扩展数据是否是一个tlv
    int SetValue(unsigned char* pbuf, uint len, bool bTlv );

    void dump();
protected:
    bool m_nested;	//!嵌套标志
    uint m_type;
    uint m_len;
    CPacket* m_pData;
};

typedef std::list<CTlv*> TLV_LIST;
class TlvReader
{
public:
	TlvReader();
	~TlvReader();
	int Parse(unsigned char* pbuf, uint len);
	void Clear();
	TLV_LIST& GetTlvList();
protected:
	TLV_LIST m_list;
};

class CTlvEx;
typedef std::list<CTlvEx*> TLV_LIST_EX;

class CTlvEx
{
public:
	friend class TlvReaderEx;
	friend class TlvWriterEx;
public:
	CTlvEx( CTlvEx* pfather = NULL );
	CTlvEx( CTlvEx& tlv );
	~CTlvEx();

	unsigned int GetType();
	unsigned int GetLen();
	unsigned int GetSize();
	bool GetNestflag();

	int SetType( unsigned int Type );

	//!set value 会把当前value去掉，使用此value代替
	int SetValue( unsigned char* buf, unsigned int len );
	int SetValue( CTlvEx& pTlv );

	int Append( unsigned char* buf, unsigned int len );
	int Append( CTlvEx& pTlv );

	int GetValue( unsigned char* buf, unsigned int len );

	//!只取相同类型的第一个
	CTlvEx* GetSubTlv( unsigned int Type );

	//!把所有相同类型的插入到list中，不进行复制
	int GetSubTlvList( TLV_LIST_EX& list, unsigned int Type );

protected:
	void Refresh();
	void ClearList();
protected:
	CTlvEx* father;
	unsigned int m_type;
	unsigned int m_len;
	bool m_nested;	//!嵌套标志

	union ValueHolder
	{
		unsigned char* value;
		TLV_LIST_EX* m_tlv_list;
	}value_;
};

class TlvReaderEx
{
public:
	TlvReaderEx() {}
	~TlvReaderEx();

	int Parse( unsigned char* pbuf, unsigned int len );
	int Parse( unsigned char* pbuf, unsigned int len, CTlvEx* father );

	void Clear();
	TLV_LIST_EX& GetTlvList();
protected:
	TLV_LIST_EX m_list;
};

class TlvWriterEx
{
public:
	TlvWriterEx( );

	int WriteAlloc( TLV_LIST_EX& tlv_list, unsigned char*& p, unsigned int& len );
	int WriteAlloc( CTlvEx& tlv, unsigned char*& p, unsigned int& len );

	int Write( TLV_LIST_EX& tlv_list, unsigned char * pBuf, unsigned int size);
	int Write( CTlvEx& tlv, unsigned char * pBuf, unsigned int size);
protected:
};

#endif
