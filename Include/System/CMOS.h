

#ifndef __CCMOS_H__
#define __CCMOS_H__

#include "APIs/DVRDEF.H"
#include "APIs/Cmos.h"
#include "config-x.h"
#include "Object.h"
#include "Support.h"


//CMOS_FLAGS bits define:
typedef enum cmos_flags_t{
	CMOS_REMOTE_LOCK,				//ң������
	CMOS_DST_TUNE,					//����ʱ����
} CMOS_FLAGS_INDEX;

class CCMOS {
public:
	PATTERN_SINGLETON_DECLARE(CCMOS);

public:
	CCMOS();
	~CCMOS();
	void Initialize();
	int Read (CMOS_OFFS offs, void *pdat, int len);
	int Write(CMOS_OFFS offs, void *pdat, int len);
	int ReadFlag (CMOS_FLAGS_INDEX index);
	int WriteFlag (CMOS_FLAGS_INDEX index, int enable);
	void SetExitTime(DHTIME *time);
	void GetExitTime(DHTIME *time);
	void SetExitState(uchar state);
	uchar GetExitState();
	void Dump();
	int SetCmosFlag(int flag=0);
   int m_stopflag;
};

#define g_Cmos (*CCMOS::instance())

#endif //__CCMOS_H__
