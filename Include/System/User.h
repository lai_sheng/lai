#ifndef __UGM_USER_H__
#define __UGM_USER_H__

#include "Object.h"
#include "APIs/Types.h"
#include "APIs/System.h"
#include <string>
#include <list>

class CUserManager;

enum PasswordEncrypt
{
	needEncrypt,		// 密码加密方式
	NoEncrypt,		// 密码不加密方式
};

class CUser
{
public:
	enum ErrorNo
	{
		noError = 0,
		notLoginYet = -1,
		passwordNotValid = -2,
		userNotValid = -4,
		hasBeenLocked = -5,
		inBlackList = -6,
		hasBeenUsed = -7,
	};

	PATTERN_SINGLETON_DECLARE(CUser);

	/// 默认构造函数
	CUser();

	/// 虚析构函数
	virtual ~CUser();

	/// 当前用户是否登陆成功
	bool valid();

	/// 用户登陆
	/// \param [in] name 要登陆的用户名
	/// \param [in] passwd 用户的登陆密码
	/// \param [in] type 客户端类型，"GUI"，"Console", "DVRIP-Web", "DVRIP-Upgrade"具体参考<<AuthorityList.doc>>
	/// \param [in] flag 密码保存方式，密文方式或者明文方式
	/// \param [in] address 客户端的地址，比如192.168.0.2 而本地没有地址
	/// \return 登陆成功或者失败
	bool login(const std::string &name, const std::string &passwd, const std::string &type, PasswordEncrypt flag = needEncrypt,  const std::string address = "");

	/// 用户注销
	bool logout();

	/// 获取登陆用户名
	const std::string &getName() const;

	/// 获取客户端类型
	const std::string &getType() const;

	/// 获取客户端地址
	const std::string &getAddress() const;

	/// 获取登录时间
	const SYSTEM_TIME &getLoginTime() const;
	/// 获取有效的用户ID
	int getId() const;

	/// 检查权限是否有效
	bool isValidAuthority(const char *auth) const;

	/// 获取错误号
	int getErrorNo();
	bool isSupper();
	///检查超级密码是否有效//add by wyf on 090731
	bool isSuperPwdValid(const std::string &name,const std::string &passwd);

private:

	// 拷贝构造函数,禁止拷贝
	CUser(CUser &);

	// 清楚状态
	void clear();

private:
	CUserManager	*m_userManager;
	int				m_locked;		// 用户已经锁定
	ulong			m_lockTime;		// 锁定时间，ms为单位
	int				m_tryLockNum;	// 登陆次数
	int				m_userId;		// 用户ID，是用户存在的唯一标示
	SYSTEM_TIME		m_loginTime;		//登录时间
	bool			m_valid;
	int				m_errorNo;		// 错误号
	std::string 	m_userName;	
	std::string 	m_userPasswd;	
	std::string 	m_loginType;
	PasswordEncrypt m_passwordFlag;
	std::string 	m_loginAddress;
	static int 		sm_maxUserId;
	bool			m_Supper;
};

#define g_localUser (*CUser::instance())

#endif
