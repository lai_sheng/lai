

#ifndef __BIGNUMBER_H__
#define __BIGNUMBER_H__

#include <iostream>
#include <algorithm>
#include <cstring>

// 数字在该类中每四位逆序存放。
// 实现了加减乘除着四个最基本的运算。
// 乘法的时间复杂度为)O(n^2)
class CBigNumber 
{
public:
	enum 
	{
		MAXN	= 9999,
		DLEN	= 4,
		DIGIT	= 10
	};
	CBigNumber() : len(1) { memset(a, 0, sizeof(a)); }
	CBigNumber(unsigned long long);
	CBigNumber(const char []);
	CBigNumber(const CBigNumber &);
	CBigNumber &operator=(const CBigNumber &);
	CBigNumber operator+(const CBigNumber &) const;
	CBigNumber operator-(const CBigNumber &) const;
	CBigNumber operator*(const CBigNumber &) const;
	CBigNumber operator/(unsigned long long) const;
	friend std::ostream & operator<<(std::ostream &os, const CBigNumber &n);
	friend std::istream & operator>>(std::istream &is, const CBigNumber &n);
private:
	unsigned long long a[DIGIT];
	int len;
};

#endif
