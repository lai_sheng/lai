
#ifndef __VERSION_H__
#define __VERSION_H__

#include <list>
#include "APIs/System.h"
#include "System/File.h"
#include "config-x.h"

class CVersion
{
public:
	
	CVersion();
	virtual ~CVersion();		
	void GetVersion(char *version, int length,int shortFormat=0);
	void GetVersion(int *pMaj, int *pMin);
	void GetBuildDate(SYSTEM_TIME *pTime);		
	VD_BOOL GetSerialNumber(char *pData, int len);
	std::string& GetSvnVer();
	std::string& GetSvnUrl();
#if defined(WEB_SHOW_VERSION)
	void LoadWebData(uchar *WebDataInfo);
	void GetWebVersion(int *web_major,int *web_minor,int *web_third,int *web_four);
#endif	
	static CVersion* instance(void); 
private:

#if defined(WEB_SHOW_VERSION)
	CFile m_FileWeb;
#endif
	static CVersion* _instance; 

	std::string m_svn_version;
	std::string m_svn_url;
};	

#endif//__VERSION_H__


