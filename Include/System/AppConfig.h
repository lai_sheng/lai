
#ifndef __AppConfig_H__
#define __AppConfig_H__

#include <string>

//struct lua_State;
//typedef struct lua_State lua_State;

/*!
	\class AppConfig
	\brief  用于从脚本中获取固定的配置信息
 */
class AppConfig {
public:
	/*!
	  \fn     AppConfig * instance()
	  \brief   返回AppConfig实例
	*/
	static AppConfig * instance();

	/*!
	  \fn     std::string getString(std::string key, std::string defaultValue="")
	  \brief   根据传入的键值返回相应的字符串值
	          该函数根据传入的键值访问LUA脚本中设置的变量,并返回相应的值，如果
	          键值不是一个合法的LUA变量，或者没有这个变量，将会返回默认值
	  \param  key            要访问的键值，形式如Global.HideLogo
	  \param  defaultValue   默认值，当访问的键值不存在时返回
	  \return 变量的值
	*/
	std::string getString(std::string key, std::string defaultValue="");
	
	/*!
	  \fn     double   getNumber(std::string key, double defaultValue = 0)
	  \brief   根据传入的键值返回相应的double值,与getString类似
	  \param  key            要访问的键值，形式如Global.HideLogo
	  \param  defaultValue   默认值，当访问的键值不存在时返回
	  \return 变量的值
	*/
	double   getNumber(std::string key, double defaultValue = 0);

	/*!
	  \fn     int	loadFromFile(std::string filename)
	  \brief   加载LUA脚本
	  \param  filename    要加载的脚本文件名
	  \return 返回0表示加载成功，其它数据表示出错，一般情况下是脚本文件的语法有无
	*/
	int	   loadFromFile(std::string filename);
private:
	AppConfig();
	~AppConfig();
	
	int TravelTable(std::string& key);
	
	static AppConfig * _instance;
//	lua_State *_lua_state;
};

#endif

