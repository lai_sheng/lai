#ifndef _MSG_H_
#define _MSG_H_

#define IPCAM_MSG_KEY  332211 //要与daemon的相对应
#define IPCAM_MSG_KEY2 112233
#define IPCAM_LED_STAT 115577

#define SND_NET_CLOSE      1
#define SND_WAVE_FINISH    2
#define SND_WAVE_STARTOVER 3

#define RCV_QUIT_CON     3
#define RCV_START_CON    4
#define RCV_LINK_FAIL    5
#define RCV_LINK_SUCC  	 6
#define RCV_WAVE_LINKFIN 7
#define RCV_WAVE_START   8
#define RCV_INPUT_APMODE 9
#define RCV_RESET_INFO 10

#define RCV_WIFI_PSK_ERR 12//LHC daemon

#define RCV_START_COMPLETE 13


#define LED_STAT_LINK     1
#define LED_STAT_NO_LINK  2

#define EMPTY			 (-1)


void snd_msg_to_daemon(int txt);
int* rcv_msg_from_daemon();

#endif

