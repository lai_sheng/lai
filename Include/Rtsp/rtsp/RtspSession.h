/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              RtspSession.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-4-26   13:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/
#ifndef __RTSP_SESSION_H__
#define __RTSP_SESSION_H__

#include <time.h>
#include <string>
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "MultiTask/Timer.h"
#include "System/Packet.h"
#include "System/ABuffer.h"
#include "Rtsp/OprPdu/RtspPdu.h"
#include "Rtsp/Tplayer/TPTypedef.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "Rtsp/rtsp/RtspMedia.h"
#include "Rtsp/BaseFunc/PortPool.h"

namespace CommonLib_VD
{

#define RTSP_CONN_NUM    32     //rtsp会话的最大个数，包括server端和client端的实时监视和录像回放

//直接调用socket函数发送
int VDSocketTcpSend(int connId,const char* pData,int len);
int VDSocketUdpSend(int connId,const char* pData,int len,char *ip,unsigned short port);
int VDCreateUdpSocket(char * ip,unsigned short port);

#define MAX_RTSP_RECV_LEN           (64 * 1024)
#define OPTION_TIMER_INTERVAL       5

class CMyABuffer
{
public:
    CMyABuffer();
    virtual ~CMyABuffer();
    /**
     * Append - 
     * @ pszStr : 
     *        pointer of data.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        >=0 data size
     *
     * append data to buffer
     */
    int Append(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Get - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Get data from buffer
     */
    int Get(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Pop - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Pop data from buffer
     */
    int Pop(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Pour - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Pour data from buffer
     */
    int Pour(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Reset - 
     * @ isClear : 
     *        true : free buff
     *        false : clear data only
     *
     * Reset buf
     */
    void Reset(const bool isClear = false);
    /**
     * Size - 
     *
     * Return : 
     *        size of data
     *
     * get size of data
     */
    unsigned int Size();
    /**
     * Buf - 
     * Return : 
     *        pointer of data
     *
     * get the pointer of data
     */
    unsigned const char * Buf() const;

    /*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

private:
    /* buffer pointer */
    unsigned char * m_pBuf;
    /* data size */
    unsigned int  m_iDataSize;
    unsigned int  m_iDataOffset;
};

//!发送pdu操作接口，用以把pdu的发送或接收方式从会话中解藕出来
class IPduNetListener
{
public:
	//异步缓冲发送 
	virtual int TPSendPdu(int connId, CRtspPdu *rtspPdu) = 0;

	//不通过异步缓冲，直接发送
	virtual int SendPdu(int connId, CRtspPdu *rtspPdu) = 0;
	//!创建媒体处理对象, 返回堆上的对象，创建者负责删除
	//!des_url-为rtsp的请求url
	virtual CRtspMedia* CreateMedia(int engineId, int connId, std::string rtsp_url) = 0;
	virtual void DestoryMedia(CRtspMedia*& pRtspMedia) = 0;
	//!通知rtsp 方法处理结果, 出错或会话结束时通知
	virtual void OnRtspPduResult(int engineId, int connId, CRtspPdu *pPdu, std::string method) = 0;
};

class CRtspSession : public CObject
{
public:
	typedef int (CRtspSession::*MethodFunc)(int engineId, int connId, CRtspPdu* pReqPdu);
	typedef int (CRtspSession::*RtspTrans)(int connId, char* pData,int len, char *ip,unsigned short port);
	
	typedef enum
	{
	    RELEASE_NO_NEED = 1,
		RELEASE_MEDIA_MSG,
		RELEASE_MSG_MEDIA,
	    RELEASE_NEED,
	}eRtspState;
	
	typedef enum
	{
		RELEASE_CLIENT,
		RELEASE_SERVER,
	}eRtspMode;

	typedef enum
	{
		eRTrack_video,
		eRTrack_audio,
		eRTrack_Num,
	}eRtspTrack;

	//!轨道信息
	class CRtspTrack
	{
	public:
		CRtspTrack()
		{
			m_track_type = eRTrack_video;
			for(int i = 0; i < RCE_Num; i++)
			{
				m_RtspRemotePort[i] = 0;
				m_RtspLocalPort[i] = 0;
				m_RtspTcpPeerChannel[i] = -1;
				m_socket[i] = INVALID_SOCKET;
			}

			//!发送channel 写死 0，1
			m_RtspTcpChannel[RCE_Rtp] = 0;
			m_RtspTcpChannel[RCE_rtcp] = 1;

			m_SetupSeq = -1;
			m_bRepSetupOK = false;
			m_track_str = "";
		}

		virtual ~CRtspTrack()
		{
			for(int i = 0; i < RCE_Num; i++)
			{
				if (m_socket[i] != INVALID_SOCKET && m_rtpMode == RTP_OVER_UDP )
				{
					closesocket(m_socket[i]);
				}
			}
		}
	public:
		eRtspTrack m_track_type;
		//!rtsp over udp 模式下使用的udp接收端
		unsigned short m_RtspRemotePort[RCE_Num];
		unsigned short m_RtspLocalPort[RCE_Num];

		//!rtsp over tcp 模式下使用的channel值
		int m_RtspTcpChannel[RCE_Num];	//!本端发送出去的channel值
		int m_RtspTcpPeerChannel[RCE_Num]; //!远端发过来的数据channel值

		int m_socket[RCE_Num];
		
		std::string m_track_str;
		int m_SetupSeq; //!setup track seq
		bool m_bRepSetupOK;
		int m_rtpMode;
	protected:
	private:
	};

public:
	static void GetUrlLineData(std::string strUrl,std::string strKey,std::string &strValue);
public:
	CRtspSession(IPduNetListener* pPduNet, CPortPool* pPortPool);
	virtual ~CRtspSession();

	VD_BOOL DoMsgTask(int engineId, int connId, const char * pData, size_t len);
	eRtspState GetReleaseFlag();
	eRtspState SetReleaseFlag(eRtspState flag);
    int GetRtspSocket(void);
	int SetRtspSocket(int socket);
	void SetPeerIp(const char *ip);
	int DoRtpPacket(CPacket *packet);
	void CheckOptionsState(unsigned int arg);
	
	virtual int ReadyMedia( fd_set* read_set, fd_set* write_set, int& max_fd ) = 0;
	virtual VD_BOOL DoMediaTask(fd_set* read_set, fd_set* write_set) = 0;	

	virtual int TransRtpOverTcp(int connId, char* pData,int len,char *ip,unsigned short port) = 0;
	virtual int TransRtpOverUdp(int connId, char* pData,int len,char *ip,unsigned short port) = 0;
       virtual int GetMediaStartFlag();
protected:	
	int SendToPeerMsg(CRtspPdu*& pReqPdu);
	
	eRtspState  m_NeedRelease;          //RtspSession异步释放标志
	char m_szPeerIp[16];                //远程IP地址
	ulong m_peerip_u;
	CRtspTrack m_RtspTrack[eRTrack_Num];
    int m_RtspSocket[HanderRtspNr];
	CRtspMedia *m_Media;
	bool m_bStartMediad; //!true-表示m_Media已经打开
	CTimer* m_pCheckOption;
	time_t m_tmLastOption;
    int m_iOptionCounts;

	CMyABuffer m_RecvBuf;
        
	MethodFunc m_MethoFunc[RTSP_METHOD_NUM+1];
	RtspTrans m_RtspTrans;
	
	IPduNetListener* m_pPduNet;
	CMutex	m_mutex;
	CPortPool* m_pPortPool;
protected:	
	int GetIndexOfMethod(std::string method);
	void MethodFuncInit();

    //!以下方法是对接收到的rtsp包的处理
	//以下成员函数中的参数engineId和connId暂时没用，以备扩展
	//!接收包总入口, 仅为方便处理而设
	virtual int OnPdu(int engineId, int connId, CRtspPdu* pReqPdu) = 0;

	virtual int OnOptions(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	virtual int OnDescribe(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	//virtual int OnDescribeYY(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int OnSetup(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	virtual int OnRecord(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int OnPlay(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int OnPause(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	virtual int OnGetparameter(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	virtual int OnSetparameter(int engineId,int connId, CRtspPdu * pReqPdu) = 0;
    virtual int OnTeardown(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
	virtual int OnError(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    /**
     * @brief 二进制数据处理
     * @param engineId
     * @param connId
     * @param data :二进制数据指针
     * @param len :二进制数据长度
     * @return :处理结果
     */
	virtual int OnBinaryData(int engineId, int connId, int rtsp_channel, const uchar* data, uint len) = 0;
};

/*!
* @brief rtsp session封装rtsp客户端一些消息包，及应答包的处理
*/
class CRtspSessionClt : public CRtspSession
{
public:
	//!取出媒体通道描述信息, 最多取两个
	int GetTrackInfo( CRtspPdu* pReqPdu, std::string& track1, std::string& track2);
public:
    CRtspSessionClt(IPduNetListener* pPduNet, CPortPool* pPortPool);
	~CRtspSessionClt(){}

	virtual int ReadyMedia( fd_set* read_set, fd_set* write_set, int& max_fd );
	virtual VD_BOOL DoMediaTask(fd_set* read_set, fd_set* write_set);

	int SetAVMode(eRtpModle mode);
	//!设置此session传输的媒体类型
	void EnableAudio(bool flag);
	void SetLoginAuth(std::string user, std::string passwd);
	void EnableAuth(bool bEnable);
	int	DiscardWrongFrame(bool bFlag);

	virtual int SendDescribeReq(int engineId, int connId, const char* server_ip, int port);
	virtual int SendSetupReq(int engineId, int connId, std::string track_id, eRtspTrack type );
	virtual int SendPlayReq(int engineId, int connId );
	virtual int SendOptions(int engineId, int connId );
	virtual int SendOptions(int engineId, int connId,const char* server_ip, int port);
	virtual int SendTeardownReq(int engineId, int connId );
	
	int TransRtpOverTcp(int connId, char* pData,int len,char *ip,unsigned short port);
	int TransRtpOverUdp(int connId, char* pData,int len,char *ip,unsigned short port);

	//!取得接收到的媒体数据数目
	uint64 GetRecvMediaData(eRtspMedia media);
	//设置重发describereq的次数
	int SetSendDescribeReqTime(int iTime);

	int OnMediaProc(int id, int user);
protected:
	//!以下方法是对接收到的rtsp包的处理
	//以下成员函数中的参数engineId和connId暂时没用，以备扩展
	virtual int OnPdu(int engineId, int connId, CRtspPdu* pReqPdu);

	virtual int OnOptions(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnDescribe(int engineId, int connId, CRtspPdu* pReqPdu);
	//virtual int OnDescribeYY(int engineId, int connId, CRtspPdu* pReqPdu) {return -1;}
    virtual int OnSetup(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnRecord(int engineId, int connId, CRtspPdu* pReqPdu);
    virtual int OnPlay(int engineId, int connId, CRtspPdu* pReqPdu);
    virtual int OnPause(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnGetparameter(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnSetparameter(int engineId,int connId, CRtspPdu * pReqPdu);
    virtual int OnTeardown(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnError(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnBinaryData(int engineId, int connId, int rtsp_channel, const uchar* data, uint len);

	void SendRtcpPacket();

	int GetRtpChannel(CRtspPdu* pReqPdu, int& rtp_chn, int& rtcp_chn);
	void AddAuthInfo(CRtspPdu* pPdu);
	void AddDigestAuthInfo(CRtspPdu* pPdu);
protected:
	std::string m_session;
	std::string m_LastReqMethod;
	std::string m_server_ip; //!服务器ip地址
	int			m_server_port;	//!服务器端口
	eRtpModle m_rtp_mode;
	std::string m_url_base;

	VD_INT32 m_iSeq;

	unsigned char m_recv_buf[8*1024];
	int		m_recv_len;

	CPacket* m_pPacket_recv;

	int GetSeq(std::string& seq);

	//std::string m_track1; //!视频track
	//std::string m_track2;	//!音频track

	typedef enum
	{
		RTT_none,
		RTT_h264,
		RTT_G711,
	}RTP_TRACK_TYPE;

	RTP_TRACK_TYPE m_track1_type;
	RTP_TRACK_TYPE m_track2_type;

	int m_print_count;

	//!收到的数据大小
	uint64 m_media_data_size;
	//!会话处理的数据是音频或视频
	//eRtspMedia m_session_media;

	char m_sps_info[1024];
	int m_sps_len;
	char m_pps[1024];
	int m_pps_len;

	unsigned long m_last_send_rtcp;

	//int m_rtp_recv_channel;
	//int m_rtcp_recv_channel;

	std::string m_login_user;
	std::string m_login_passwd;
	bool m_bAuth;	//!认证标志,true-需要进行认证登陆
	int  m_iAuthType;	//! 认证类型 0 basic 1 digest
	int  m_iSendDescribeReqTime;	//! 认证失败的次数，三星有些型号需要重复认证２次

    std::string m_DigsetNonce;
    std::string m_DigsetRealm;

	//!连续n次收不到rtcp包，不再发rtcp
	int m_recv_rtcp_failed;

	bool	m_bEnableAudio;
	bool m_bDiscardWrong;
};

}
#endif

