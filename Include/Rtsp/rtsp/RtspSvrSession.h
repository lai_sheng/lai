
#ifndef _RTSPSVRSESSION_H_
#define _RTSPSVRSESSION_H_

#include "Rtsp/rtsp/RtspSession.h"

namespace CommonLib_VD
{

class CRtspSessionSvr : public CRtspSession
{
public:
	CRtspSessionSvr(IPduNetListener* pPduNet, CPortPool* pPortPool) : CRtspSession(pPduNet, pPortPool){}
	~CRtspSessionSvr(){}

	virtual int ReadyMedia( fd_set* read_set, fd_set* write_set, int& max_fd );
	virtual VD_BOOL DoMediaTask(fd_set* read_set, fd_set* write_set);
	virtual int TransRtpOverTcp(int connId, char* pData,int len,char *ip,unsigned short port);
	virtual int TransRtpOverUdp(int connId, char* pData,int len,char *ip,unsigned short port);
protected:
	//以下成员函数中的参数engineId和connId暂时没用，以备扩展
	virtual int OnPdu(int engineId, int connId, CRtspPdu* pReqPdu);

	virtual int OnOptions(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnDescribe(int engineId, int connId, CRtspPdu* pReqPdu);
	//virtual int OnDescribeYY(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnSetup(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnRecord(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnPlay(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnPause(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnGetparameter(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnSetparameter(int engineId,int connId, CRtspPdu * pReqPdu);
	virtual int OnTeardown(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnError(int engineId, int connId, CRtspPdu* pReqPdu);
	virtual int OnBinaryData(int engineId, int connId, int rtsp_channel, const uchar* data, uint len);

private:
};

}

#endif
