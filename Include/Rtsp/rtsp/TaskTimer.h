/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              TaskTimer.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-5-7   8:40
**Modified:               
**Modify Reason: 
**Description:            为媒体数据的发送做时间处理，分配给本任务的时间，
**                        就退出本任务的循环
*********************************************************************/
#ifndef __TASK_TIMER_H__
#define __TASK_TIMER_H__

#include <time.h>

#if defined(_MSC_VER) || defined(_WINDOWS_)
#include   <windows.h>
#else
 #include <sys/time.h>
#endif

namespace CommonLib_VD
{

	unsigned int VD_GetMSCount(void);
#if defined(_MSC_VER) || defined(_WINDOWS_)
	int gettimeofday(struct timeval* tv,void *tz);
#endif

typedef size_t time_base_seconds;

#define MILLION  1000000
#define TIME_PERIOD 100

class Timeval {
public:
	Timeval() //毫秒
    {
      Reset(0);
    }
	~Timeval(){}
	time_base_seconds seconds() const 
	{
	    return fTv.tv_sec;
	}
	time_base_seconds seconds() 
	{
	    return fTv.tv_sec;
	}
	time_base_seconds useconds() const 
	{
	    return fTv.tv_usec;
	}
	time_base_seconds useconds() 
	{
	    return fTv.tv_usec;
	}

    int operator<=(Timeval const& arg2) const 
	{
        return arg2 >= *this;
    }
    int operator<(Timeval const& arg2) const 
	{
        return !(*this >= arg2);
    }
    int operator>(Timeval const& arg2) const 
	{
        return arg2 < *this;
    }
    int operator==(Timeval const& arg2) const
	{
        return *this >= arg2 && arg2 >= *this;
    }
    int operator!=(Timeval const& arg2) const 
	{
        return !(*this == arg2);
    }

   bool operator>=(const Timeval& arg2) const 
   {
       return seconds() > arg2.seconds()
         || (seconds() == arg2.seconds()
	     && useconds() >= arg2.useconds());
   }

   void operator+=(const Timeval& arg2) 
   {
       fTv.tv_sec += arg2.seconds(); 
	   fTv.tv_usec += arg2.useconds();
       if (useconds() >= MILLION) 
	   {
           fTv.tv_usec -= MILLION;
           ++fTv.tv_sec;
       }
   }

   void operator-=(const Timeval& arg2) 
   {
       fTv.tv_sec -= arg2.seconds(); 
	   fTv.tv_usec -= arg2.useconds();
       if ((int)useconds() < 0) 
	   {
           fTv.tv_usec += MILLION;
           --fTv.tv_sec;
       }
       if ((int)seconds() < 0)
       {
          fTv.tv_sec = fTv.tv_usec = 0;
       }
    }
   
    void Reset(time_base_seconds mSec)
    {
        struct timeval tv;
        gettimeofday(&tv,NULL);
        fTv.tv_sec = tv.tv_sec + mSec/1000; 
	    fTv.tv_usec = tv.tv_usec + (mSec%1000)*1000;
    }

protected:
  
private:
  timeval fTv;
};
class CTaskTimer
{
public:
	CTaskTimer():m_MaxPeriodTime(TIME_PERIOD), m_DoCountLimit(16), m_DoCount(16)
	{    
	    SetTimePeriod(TIME_PERIOD);
	}
	~CTaskTimer(){}
	void SetTimePeriod(time_base_seconds mSec)
	{
	    m_Timeval.Reset(mSec);
	}
	bool IsOutTimePeriod(int flag)
	{
	    if(m_DoCount-- < 0 || flag <= 0)
		{
		    m_DoCount = m_DoCountLimit;
	        Timeval tv;
			bool bRet = (tv >= m_Timeval);
			if(bRet)
			{
			    SetTimePeriod(m_MaxPeriodTime);
			}
		    return bRet;
		}
		return false;
	}
	int SetMaxPeriodTime(time_base_seconds ms)
	{
	    if(ms < TIME_PERIOD/10 || ms > TIME_PERIOD*10)
	    {
	        return -1;
	    }
		m_MaxPeriodTime = ms;
		return 0;
	}
	void SetDoCountLimit(int count)
	{
	    m_DoCountLimit = count;
	}
	
private:
	Timeval m_Timeval;
	time_base_seconds m_MaxPeriodTime;
	int m_DoCountLimit;
	int m_DoCount;
};

}

#endif


