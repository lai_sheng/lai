/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              RtspMedia.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-5-6   16:40
**Modified:               
**Modify Reason: 
**Description:            处理视频和音频数据;部分代码是从原先的CRtspRtp
**                        和CVapVedio中拷备过来的。
*********************************************************************/

#ifndef __RTSP_MEDIA_VEDIO_H__
#define __RTSP_MEDIA_VEDIO_H__

#include <deque>
#include "System/Object.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Thread.h"
#include "System/Packet.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "System/ABuffer.h"
#include "Rtsp/rtsp/TaskTimer.h"
#include "Rtsp/BaseFunc/FrameGeneral.h"
#include "Rtsp/Dec/CRtpStreamDec.h"

namespace CommonLib_VD
{

class CRtspSession;

#define EXTRA_STREAM_NUM CHL_2END_T  //现在只有主码流和辅码流两种

typedef enum
{
    HanderRtsp   = 0,
	HanderVRtp   = 1,
	HanderVRtcp  = 2,
	HanderARtp   = 3,
	HanderARtcp  = 4,
	HanderRtspNr = 5,
}eRtspHandler;

typedef enum
{
	RCE_Rtp,
	RCE_rtcp,
	RCE_Num,
}RTP_CONNECTION_E;

typedef enum
{
    RtspVedio   = 0,
	RtspAudio   = 1,
	RtspMediaNr = 2,
}eRtspMedia;

typedef enum
{
    RTP_OVER_TCP = 0,
	RTP_OVER_UDP = 1,
	RTP_OVER_NR  = 2,
}eRtpModle;

typedef enum
{   
	MEDIA_VEDIO = 0,
	MEDIA_PLAY = 1,
	MEDIA_DOWNLOAD =2,
}eMediaType;

#define MEDIA_TYPE_OFF 4
typedef struct _RTPHDR_
{
	// little endian
	unsigned char csrccount:4;
	unsigned char extension:1;
	unsigned char padding:1;
	unsigned char version:2;
	
	unsigned char payloadtype:7;
	unsigned char marker:1;
	
	unsigned short seqnum;

	unsigned int timestamp;
	unsigned int ssrc;

}RTPHDR;
#define RTPHDR_SIZE (sizeof(RTPHDR))

#define GET_MEDIA_TYPE(x)           (*((uchar *)(x->GetBuffer())))
#define SET_MEDIA_TYPE(x,y)         (*((uchar *)(x->GetBuffer())) = y)
#define GET_MEDIA_BUF(x)            (x->GetBuffer() + MEDIA_TYPE_OFF)  
#define GEt_MEDIA_BUF_LEN(x)        (x->GetLength() - MEDIA_TYPE_OFF)

typedef enum 
{
    SPEED_SLOW_T16    = 0,
	SPEED_SLOW_T8     = 1,
	SPEED_SLOW_T4     = 2,
	SPEED_SLOW_T2     = 3,
	SPEED_NORMAL      = 4,
	SPEED_QUICK_T2    = 5,
	SPEED_QUICK_T4    = 6,
	SPEED_QUICK_T8    = 7,
	SPEED_QUICK_T16   = 8,
	SPEED_NR          = 9,
}ePlaySpeed;

const double g_PlaySpeed[SPEED_NR] =
{
    0.0625,
	0.125,
	0.25,
	0.5,// 不能写成1/2
	1,
	2,
	4,
	8,
	16,
};

class CFrame2Packet : public CObject
{
public:
	CFrame2Packet();
	
	int Parse(unsigned char* pInBuf, unsigned int nInSize, 
		unsigned int rate, unsigned int width, 
		unsigned int height, unsigned frametype);

	int Parse2(unsigned char* pInBuf, unsigned int nInSize, 
		unsigned int rate, unsigned int width, 
		unsigned int height, unsigned frametype);
	//!取出分析后的packet
	CPacket* GetPacket();
	//!弹出一个packet
	void PopPacket();

private:
	typedef std::list<CPacket*> PACKET_LIST;

	PACKET_LIST m_packet_list;

	CPacket* m_packet;
};

class CRtspMedia : public CObject
{
public:
	#define MAX_BUFFER_LEN (2*1024 * 1024)     //暂时设置为1M
	typedef std::deque<CPacket *> RtpQueue;
	
	typedef enum
	{
		RTSP_MEDIA_Client,
		RTSP_MEDIA_Server,
	}RTSP_MEDIA_MODE;

public:
    CRtspMedia();
    virtual ~CRtspMedia();
	
	//!开启媒体流
    virtual bool StartMedia() = 0;
	//!停止媒体流
    virtual bool StopMedia() = 0;
	//!获取分辨率
	virtual int GetResolution() = 0;
	virtual RTSP_MEDIA_MODE GetMode() = 0;

	//!送rtp封包进去
	virtual int PushRtpData(unsigned char* pData, int len, eRtspMedia media);
	//!为了减少内存的重复拷贝, 共用内存
	virtual int PushRtpPacket(CPacket *pPacket, eRtspMedia media);

	//!送大华packet进去
	virtual int PushPacketData(CPacket *pPacket, eRtspMedia media);

	virtual CPacket* PopFrame(eRtspMedia media);
	
    virtual int GetChannel();
	virtual int SetChannel(int Channel);
	virtual int GetStreamType();
    virtual int SetStreamType(int dwStreamType);
	virtual VD_BOOL IsAudioOn();
    virtual void SetSSRC(unsigned long ssrc);
    virtual VD_BOOL SetInterleaved(eRtspHandler type,int value);
    virtual VD_BOOL SetRtspModel(eRtpModle mode);

    virtual VD_BOOL SetMediaType(eMediaType mediaType);
	virtual eMediaType GetMediaType();
	virtual VD_BOOL SetVedioTime(char *strTime);
	virtual void SetQueueReleaseFlag(VD_BOOL flag);
	virtual int OnRtpPacket();

	void SetSpsPps(char* sps, int sps_len, char* pps, int pps_len);
    void SetH264HeadFlag(bool bFlag);
    
	void SetCallBack(CObject* pObject, MEIDA_CALLBACK proc);
	int	DiscardWrongFrame(bool bFlag);
public:
	/*virtual VD_BOOL DoMediaTask(eRtspMedia media);*/
protected:
	//!处理帧数据, 实际业务实现它，以做到把分析得到的帧数据用于何种处理
    virtual void PrcocessVideo(int iCh, CPacket *pPacket,uint iCodeType);
	int m_iFPS;
	int m_Resolution ;
	SYSTEM_TIME m_StartTime;
	SYSTEM_TIME m_EndTime;

	bool m_bMonitor;    //视频开启标志位
    bool m_bFistIFrame; //!true-需要找第一个I帧
	//rtsp会话，负责整个rtsp会话的信令解析和响应及媒体数据处理的调查度
	//CRtspSession *m_RtspSession;  
	
	CRtpStreamDec* _rtpSDec;
	int onProVideoPacket(int session, int connId, CRTPPacket* packet);
	int OnProAudioPacket(int session, int connId, CRTPPacket* packet);
protected:
    virtual void PacketFU_A(char* pBuffer, int iLen, bool bFlags , unsigned char nal_header ,int Nums);
	virtual int sendFU_A(int iCh,char* fui,char* fuh,char* data, int len, bool bMarker);
	virtual void SetTimestamp();
	virtual VD_BOOL SendPacket(CPacket *ptk, eRtspMedia media);
	virtual void ClearQueue(eRtspMedia media);
	virtual unsigned short getSequenceNum();

    bool m_flag;
	int m_iSplitNo;


    unsigned long m_ulRtpTimestamp;
	unsigned long m_ulSRtpTimestamp;//清除队列时用，用来保存成功发送的最后一个rtp包的TimeStamp
	unsigned long m_ulSSRC;
	unsigned short m_usSeq;
	
    int m_iChannel;
    int m_iStreamType;
    VD_BOOL m_bAudioOn;	

	//rtp over rtsp(TCP) 1,UDP 2
	eRtpModle m_rtpModel;
	
    //RTSP over tcp的魔数年对应的ID
	int m_Interleaved[HanderRtspNr];
	int m_Magic;
	
	//Rtp构造包
    CABuffer  m_cabRtpPacket;
	
    RtpQueue m_RtpQueue[RtspMediaNr];
	CMutex m_szMutex[RtspMediaNr];
	
	//缓冲区数据的大小，将根据此值来处理丢帧操作
	unsigned long   m_iDataLen[RtspMediaNr];
    
	//时间分配器，给每个rtspSession分配最多不超过x(ms)媒体数据传输时间表，时间用完就只有等到下一个时间片才再处理数据发送.以免多个
	//rtspSesion之间的阻塞，这个时间可以用SetMaxPeriodTime来设置.
	CTaskTimer m_TaskTimer;

	VD_BOOL m_QueueReleaseFlag;//数据发送失败后是否清除队列中相应的节点,下载时需要将此值设置为VD_FALSE;
	eMediaType m_MediaType;

	char* m_sps;
	int m_sps_len;
	char *m_pps; 
	int m_pps_len;

	int m_width;
	int m_hegiht;
	bool m_bH264HeadFlag;    

	typedef std::list<CPacket*> PACKET_LIST;
	PACKET_LIST	m_Packetlist;
	CMutex	m_PacketList_mutex;
	CVdFrame m_VDframe;
};

class CRtspMediaCli : public CRtspMedia
{
public:
	CRtspMediaCli() {}
	virtual ~CRtspMediaCli() {}

	//!自己的要求
public:
	//!获取descripe消息要使用的多媒体轨道url
	virtual std::string GetTrackUrl() = 0;
	//!获取descripe消息要使用的音频轨道url
	virtual std::string GetTrackUrl_Audio() {return "";}

	//!CRtspMedia的要求
public:
	virtual bool StartMedia() = 0;
	virtual bool StopMedia() = 0;
	virtual RTSP_MEDIA_MODE GetMode();
};

class CRtspMediaServer : public CRtspMedia
{
public:
	CRtspMediaServer();
	virtual ~CRtspMediaServer() {}
	//!获取媒体sdp描述信息
	virtual std::string GetSDPDescrbe();
	virtual int GetMediaIP(std::string& ip, std::string& mask) = 0;
protected:
private:
	std::string m_sdp_str;	//!媒体信息sdp描述
public:
	virtual bool StartMedia() = 0;
	virtual bool StopMedia() = 0;
	virtual RTSP_MEDIA_MODE GetMode();
};


}

#endif

