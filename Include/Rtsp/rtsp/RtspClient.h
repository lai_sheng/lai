#pragma once

#include "Rtsp/Tplayer/ITPObject.h"
#include "Rtsp/Tplayer/ITPListener.h"
#include "Rtsp/rtsp/RtspSession.h"
#include "Rtsp/rtsp/RtspMediaService.h"
#include "Rtsp/Tplayer/TPAckManager.h"

namespace CommonLib_VD
{


class CRtspClient : public CommonLib_VD::ITPListener, public IPduNetListener
{
public:
	//!设置媒体端口池, 供rtsp客户端使用，初始化一次即可
	static 
	int SetMediaPortPool(unsigned short firstPort, int MaxPairNum);
public:
	CRtspClient();
	virtual ~CRtspClient(void);

	VD_BOOL Start( );
	VD_BOOL Stop();

	//!设置视频传输模式，tcp/udp, 默认udp
	int SetRtpMode(eRtpModle mode);
	int SetbEnableAudio(bool enable);
	int	SetRemoteAddr(const char* ip, int port);
	void SetLoginAuth(std::string user, std::string passwd);
	int	DiscardWrongFrame(bool bFlag);

	/** 调度 */
	int heartbeat(void);
protected:
	//!重载此函数实现使用自定义的客户端session
	//!创建客户端rtsp session
	virtual CRtspSessionClt* CreateRtspCliSession();
	//!销毁客户端rtsp session
	//virtual int DestoryRtspCliSession(CRtspSessionClt*& pRtspCliSession);

	//!rtsp断开
	virtual void OnRtspDiscon(int engineId, int connId);

	//!ITPListener
public:
	int onClose(int engineId, int connId); 
	int onConnect(int engineId, int connId, const char* ip, int port);
	int onSendDataAck(int engineId, int connId, int id);
	int onSendDataFail(int engineId, int connId, int id);
	int onData(int engineId, int connId, const char* data, size_t len);

	//!IPduNetListener
public:
	virtual CRtspMedia* CreateMedia(int engineId, int connId, std::string rtsp_url) = 0;
	virtual void DestoryMedia(CRtspMedia*& pRtspMedia) = 0;

	//异步缓冲发送 
	virtual int TPSendPdu(int connId, CRtspPdu *rtspPdu);
	//不通过异步缓冲，直接发送
	virtual int SendPdu(int connId,CRtspPdu *rtspPdu);

	virtual void OnRtspPduResult(int engineId, int connId, CRtspPdu *pPdu, std::string method);
protected:
	CommonLib_VD::ITPObject*  _connection;       /**< 传输对象 */

	CRtspSessionClt*	m_pRtspSessionCli;
	CRtspMediaService*  m_RtspMediaService;
	//用于异步RTSP信令发送缓冲管理
	CTPAckManager<CRtspPdu> m_RtspSvrAck;

	std::string m_str_ip;
	int m_port;
	bool m_bStarted;	//!是否已开启
	
	int m_engineId;
	int m_connId;

	uint64 m_last_send_option; //!最后一次发option时间
	uint64 m_last_recv_time;//!最后一次接收数据时间, 单位ms
	uint64	m_timeout_value;//!超时值, ms
	uint64	m_last_get_media_time; //!上一次取媒体数据大小的时间
	uint64	m_last_media_size[RtspMediaNr]; //!上一次获取的媒体数据大小
	bool	m_bEnableAudio;
	CMutex m_Mutex;

	eRtpModle m_rtp_mode;

	std::string m_login_user;
	std::string m_login_passwd;
	bool	m_bDiscardWrongFrame;
};

class CRtspCliManager : public CThread
{
#define RTSP_CLI_NUM	RTSP_CONN_NUM
public:
	friend class CRtspClient;
	static CRtspCliManager* instance();

	int Start();
	int Stop();

	int AddClient(CRtspClient* pClient);
	int RemoveClient(CRtspClient* pClient);
protected:
	CRtspCliManager();
	virtual ~CRtspCliManager();

	CRtspClient* m_pRtspCli[RTSP_CLI_NUM];
	int		m_cli_num;	//!记录rtspclient个数
	CRtspMediaService *m_RtspMediaService;
	CPortPool m_PortPool;

	CMutex	m_mutex;
	bool m_bforce_sleep;//true-强制休息

	//!CThread
public:
	virtual void ThreadProc();
};

}