/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              CRtspMediaService.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-4-27   9:40
**Modified:               
**Modify Reason: 
**Description:            处理rtsp server/client 的媒体数据
*********************************************************************/
#ifndef __RTSP_MEDIA_SERVICE_H__
#define __RTSP_MEDIA_SERVICE_H__

#include <vector>
#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "System/Object.h"
#include "Rtsp/rtsp/RtspSession.h"

namespace CommonLib_VD
{


using namespace std;

class CRtspMediaService : public CThread
{
public:
	#define MAX_CONN_NUM 4
	PATTERN_SINGLETON_DECLARE(CRtspMediaService);

	typedef struct
	{
	    int connId;
		CRtspSession *rtspSession;
	}sRtspConn;
	
public:
    CRtspMediaService();
	~CRtspMediaService();
	
	VD_BOOL Start(void);	
	VD_BOOL Stop(VD_BOOL bClear = VD_TRUE);
	
	virtual void ThreadProc();
	VD_BOOL AddRtspSession(CRtspSession *rtspSession);
	VD_BOOL DelRtspSession(int connId, CRtspSession::eRtspState flag);
	int SetMaxConn(int connIdNum);
	//返回VRtspSession表中下一个RtspSession;
	CRtspSession *GetRtspSession(int connId);
private:

	void Clear();
	VD_BOOL TransPreRtspSession(int index);	
	int GetValidIndex();
	VD_BOOL m_VRtspSessionInit;
	CMutex m_RtspMutex;

	sRtspConn *m_RtspSvrClt;	
	CMutex m_RtspSvrCltMutex;
	int m_ConnIdNum;//缓冲连接的最大个数
};

}

#endif

