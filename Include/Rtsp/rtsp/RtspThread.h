/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              RtspThread.h
**Version:	              Version 0.01
**Author:	              
**Created:                
**Modified:               yang_shukui,2010-4-27   9:40
**Modify Reason:          重新整理rtsp部分的代码，以提高通用rtsp服务的可维护性；拟加入rtspClient端的支持。
**Description:            CRtspSrv提供rtsp server/client 端的服务。
*********************************************************************/

#ifndef __VD_RTSP_SVR_H__
#define __VD_RTSP_SVR_H__

#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/ABuffer.h"
#include "Rtsp/rtsp/RtspMediaService.h"
#include "Rtsp/OprPdu/RtspPdu.h"
#include "Rtsp/rtsp/RtspSession.h"
#include "Rtsp/rtsp/RtspSvrSession.h"
#include "Rtsp/BaseFunc/VDDebug.h"
#include "Rtsp/Tplayer/TPAckManager.h"
#include "Rtsp/Tplayer/ITPListener.h"
#include "Rtsp/Tplayer/TPTCPServer.h"
#include "Rtsp/BaseFunc/PortPool.h"

namespace CommonLib_VD
{


#define RTSP_LISTEN_PORT 554    //rtsp服务端lister的端口

class CRtspSrv : public CThread, public CommonLib_VD::ITPListener, public IPduNetListener
{
public:
	//PATTERN_SINGLETON_DECLARE(CRtspSrv);
	CRtspSrv();
	virtual ~CRtspSrv();

	virtual CRtspMedia* CreateMedia(int engineId, int connId, std::string rtsp_url) = 0;
	virtual void DestoryMedia(CRtspMedia*& pRtspMedia) = 0;

	//!设置媒体端口池
	int SetMediaPortPool(unsigned short firstPort, int MaxPairNum);
public:
	//!创建服务器端rtsp session, 重载此函数可以实现自定义的server session
	//!暂时要求一定要是new出来的
	virtual CRtspSessionSvr* CreateRtspServerSession();
	//!销毁服务器端rtsp session, 暂时不提供销毁函数，只能用delete
	/*virtual int DestoryRtspServerSession(CRtspSessionSvr*& pRtspServerSession);*/
public:
	virtual void ThreadProc();

	/* 开启线程 */
	VD_BOOL Start(unsigned short port = RTSP_LISTEN_PORT);

	/* 停止线程 */
	VD_BOOL Stop();

	//异步缓冲发送 
	int TPSendPdu(int connId, CRtspPdu *rtspPdu);

	//不通过异步缓冲，直接发送
	int SendPdu(int connId,CRtspPdu *rtspPdu);

	virtual void OnRtspPduResult(int engineId, int connId, CRtspPdu *pPdu, std::string method );
public:
	int CloseClient(int connId);
	int onClose(int engineId, int connId); 
	int onConnect(int engineId, int connId, const char* ip, int port);
	int onSendDataAck(int engineId, int connId, int id);
	int onSendDataFail(int engineId, int connId, int id);
	int onData(int engineId, int connId, const char* data, size_t len);

	VD_BOOL DoMsg(int engineId, int connId, const char* data, size_t len);
protected:	
	//用于异步RTSP信令发送缓冲管理
	CTPAckManager<CRtspPdu> m_RtspSvrAck;

	CRtspMediaService *m_RtspMediaService;	
	CommonLib_VD::TPTCPServer *m_TPServer;

	CPortPool m_PortPool;
};

}

#endif


