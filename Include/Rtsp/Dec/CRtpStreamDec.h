// CRtpStreamDec : rtp stream decode

#ifndef INCLUDED_CRTPSTREAMDEC_H
#define INCLUDED_CRTPSTREAMDEC_H

#pragma warning( disable : 4786 )

// 负责将RTP包进行解析，处理乱序、丢包等，组装成完成的视频帧提供给外面
// 只处理一路RTP流，不进行demux

// 使用方法，调用InputData()输入一个完整的RTP包，然后循环调用GetFrame()获取视频帧（0到多个）

// 表示一个输出的帧，内存由CRtpStreamDec管理自行管理，有效期到CRtpStreamDec下一个函数被调用



// RTP buffer数量，完整的RTP包会马上组成视频帧，否则会等待乱序的包，超过buffer限制时丢弃不完整的帧
// 这个值小会导致乱序时丢帧概率大，这个值大会导致实际丢包时等待延时大

#define MAX_RTP_BUFFER_FRAME_NUM	( 64 )
#define RTP_DEC_CLOCK_RATE			90000

#define SEQ_OVERTURN_DELTA     10000
#define MAX_SEQ				   0xFFFF

#define TM_OVERTURN_DELTA      1000000
#define MAX_TM				   0xFFFFFFFF

#define RTP_SDEC_SUCCESS   0


#include "Rtsp/Dec/rtpDecFrame.h"
#include <Rtsp/rtp/CRTPPacket.h>
#include <map>
#include <list>
#include "System/Packet.h"

using CommonLib_VD::CRTPPacket;

namespace CommonLib_VD
{

class CVdFrame
{
public:
    CVdFrame();
    virtual ~CVdFrame();

    void Reset();
    //!添加威乾帧头
    bool pushHead();
    //!添加帧数据
    bool pushData(const char* data, const rtp_i32 length);
    //!添加威乾帧尾
    bool pushTail();

    int SetFrameLen(int len);
    bool SetFrameFlag(enum PackTypeFlag flag);
    bool SetSize(int width, int height);
    bool SetRate(int rate);

    enum PackTypeFlag GetFrameFlag();

    //!取出分析后的packet
    CPacket* GetPacket();
    //!弹出一个packet
    void PopPacket();
private:
    typedef std::list<CPacket*> PACKET_LIST;

    PACKET_LIST m_packet_list;

    CPacket* m_packet;
    bool m_bHead;
    int m_width;
    int m_height;
    int m_rate;
    int m_FrameLen;
    enum PackTypeFlag m_frame_flag;

    int m_use_len;
};
typedef int (CObject::*MEIDA_CALLBACK)(int Id, int user);
class CRtpStreamDec
{
protected:	
    typedef std::map<rtp_u64,CRTPPacket*> CRTPPkgMap;//同一个frame的map
    typedef std::map<rtp_u64,CRTPPkgMap> CFrameMap;//所有frames
public:
    explicit CRtpStreamDec(int maxBufferFrame=MAX_RTP_BUFFER_FRAME_NUM,rtp_u32 clockRate=RTP_DEC_CLOCK_RATE);
    virtual ~CRtpStreamDec();
protected:
    CRtpStreamDec(const CRtpStreamDec&);
    CRtpStreamDec& operator=(const CRtpStreamDec&);
public:
    //返回  RTPSDEC_SUCCESS  表示成功
    int reset();
    //返回  RTPSDEC_SUCCESS  表示成功
    int inputData(CRTPPacket* rtpPacket, int buf_len,bool *pbFistIFrame);
    //返回  RTPSDEC_SUCCESS  表示有Frame
    int getVideoFrame(CRtpDecFrame& frame);
    //!得到大华帧
    int GetVideoVdFrame(CVdFrame& vdframe);
    void SetSpsPps(char* sps, int sps_len, char* pps, int pps_len);
    void SetH264HeadFlag(bool bFlag );
	void SetCallBack(CObject* pObject, MEIDA_CALLBACK proc);
	int	DiscardWrongFrame(bool bFlag);
protected:
    void checkSeqOverTurn(rtp_u16 seq);
    void checkTmOverTurn(rtp_u32 curTm);
    rtp_u64 convertSeq(rtp_u16 seq);
    rtp_u64 convertTm(rtp_u32 curTm);

    int addRtpPacket(rtp_u64 seq, rtp_u64 curTm,CRTPPacket* pkg,bool *pbFistIFrame);
    int fillFrame(CRTPPkgMap& pkgMap,CRtpDecFrame& frame);
    int FillVdFrame(CRTPPkgMap& pkgMap,CVdFrame& frame);
    int FillVdFrameWithH264Head(CRTPPkgMap& pkgMap,CVdFrame& frame);

protected:
    static const int MIN_HEAD_LENGTH = 12;

    rtp_u64 _rtpSeqBase;
    bool    _seqOverTurn;
	rtp_u64 _rtpTmBase;
	bool	_tmOverTurn;	
	rtp_u64	_lastSubmitSeq;
	rtp_u64 _lastSubmitTm;
    rtp_u64 _firstSubmitTm;
	ulong m_ssrc;
    CFrameMap _frames;

    int	_maxBufferFrame;
    rtp_u32  _clockRate;
    rtp_u64 lastSeq; //!上一个收到rtp seq
	rtp_u64 lastseq_tm;

    bool m_bAddSpsPPs; //!增加sps, pps标志
    char *m_sps;
    int m_sps_len;
    char *m_pps; 
    int m_pps_len;
    unsigned int m_width;
    unsigned int m_height;
    bool m_bH264HeadFlag;    
    
    char *m_ph264_SEI_buf;
    int      m_ih264_SEI_len;

    int    m_iRate;/* 帧率 */
    rtp_u64 m_RateFirstTime;/* 用于统计帧率的第一帧的时间 */
    int iRateFrameNum;/* 用于统计帧率的帧的数量 */

	CObject* m_pProcObject;
	MEIDA_CALLBACK m_pProcFunc;
	bool m_bDiscardWrong;
};

}

// TODO : 内存拷贝优化，增加 packet pool 和 frame pool

#endif // INCLUDED_CRTPSTREAMDEC_H
