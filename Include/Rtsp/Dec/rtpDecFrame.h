#ifndef DH_RTP_DEV_FRAME_H_INCLUDED
#define DH_RTP_DEV_FRAME_H_INCLUDED

namespace CommonLib_VD
{

//#include "libRtpLog.h"

#ifdef WIN32
	typedef unsigned short rtp_u16;
	typedef int rtp_i32;
	typedef unsigned int rtp_u32;
	typedef unsigned __int64 rtp_u64;
#else
	typedef unsigned short rtp_u16;
	typedef int rtp_i32;
	typedef unsigned int rtp_u32;
	typedef unsigned long long rtp_u64;
#endif


class CRtpDecFrame
{
public:
	CRtpDecFrame();
	virtual ~CRtpDecFrame();
	CRtpDecFrame(const CRtpDecFrame& frame);
	CRtpDecFrame& operator=(const CRtpDecFrame& frame);
	
public:
	rtp_u32 getTimeStamp();
	rtp_u64 getTimeUs();
	char* getFrameBuffer();
	rtp_i32 getFrameLength();

	void setTimeStamp(rtp_u32 timeStamp);
	void setTimeUs(rtp_u64 timeUs);
	bool setData(const char* data, const rtp_i32 length);
	bool pushData(const char* data, const rtp_i32 length);
	//!此处添加的payload不用分析payload里边的内容，直接把payload作为数据
	bool pushPayload(const char* data, const rtp_i32 length);

	bool allocBuffer(const rtp_i32 capacity);
    bool reset();

	void SetSpsPps(char* sps, int sps_len, char* pps, int pps_len);
protected:
	rtp_u32 _timestamp;  // RTP包中的timestamp，
	rtp_u64 _timeUs;    // 单位为微秒，第一个包为0，依次递增。
	rtp_i32 _length;
	char *  _buffer;
	rtp_i32 _capacity;

	bool m_bAddSpsPPs; //!增加sps, pps标志

	char *m_sps;
	int m_sps_len;
	char *m_pps; 
	int m_pps_len;
};

}

#endif//DH_RTP_DEV_FRAME_H_INCLUDED
