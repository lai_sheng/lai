#ifndef _DECSPS_
#define _DECSPS_

namespace CommonLib_VD
{

int decsps(unsigned char* pBuf, unsigned int nSize, unsigned int* width, unsigned int* height);

typedef enum
{
	RTSP_OEM_none,
	RTSP_OEM_sony,
	RTSP_OEM_NUM,
}RTSP_OEM;
//!有些厂商特殊处理使用
int SetRtspOEM(RTSP_OEM oem);
RTSP_OEM GetRtspOem();
}

#endif
