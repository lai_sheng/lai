/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：CRTCPSession.h
* 文件标识：参见配置管理计划书
* 摘　　要：RTP基本会话类。这是RTP会话的最上层类，实现RTP会话，封装了RTCP会话，自动完成。发送方支持缓冲。
*
* 当前版本：1.1
* 作    者：李明江
* 完成日期：2007年8月2日
* 修订记录：增加了多线程重入支持。

* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年7月15日
* 修订记录：稳定，标准RTP及带重发的RTP会话都完成了。

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年7月1日
* 修订记录：创建
*/

#ifndef __FE_RTP_SESSION_H__
#define __FE_RTP_SESSION_H__

#include <map>
#include <vector>
#include <deque>

#include <TPLayer/ITPListener.h>
#include <TPLayer/TPTypedef.h>

#include "Rtsp/rtp/RTPProtocol.h"
#include "Rtsp/rtp/RTPStructs.h"
#include "Rtsp/rtp/CRTPSession.h"

namespace CommonLib_VD
{
	class CRTCPPacket;
	class CRTPPacket;
	class CRefMutex;
	class ITPObject;
	class IRTPListener;
	class IRTCPListener;
	class CRTSPBase;
	class IRTPTrans;
	

    #ifdef LINUX
    long GetTickCount();   ///可以用SystemGetMSCount实现
    #endif

	class ICRTPSession: public CRTPSession
    {
    private:
        ///禁止new创建,禁止从该类继承
        ICRTPSession(CRefMutex* refMutex = 0);

        ///禁止赋值和拷贝
        ICRTPSession(ICRTPSession& rhs);
        ICRTPSession& operator=(ICRTPSession& rhs);
    public:
		/*
		功能: 由此创建会话, 保证正确的初始化, 使用者应该总是使用该方法创建会话;
		      调用该方法前, 用户应该先创建一个传输实体
	    参数: sessCfg, 会话配置; pTrans, 传输实体; refMutex, 线程锁, 单线程用户可以不创建
		返回值: NULL, 给定的参数非法; 否则为创建的会话实例
		*/
        static ICRTPSession* BuildSession(RTPSessConfig& sessCfg, 
            IRTPTrans* pTrans, CRefMutex* refMutex = 0);

        virtual ~ICRTPSession();


		/*
		功能: 修改会话参数配置, 该函数用于创建会话之后, 更改会话的配置,
		      最好在HeartBeat开启前设置, 新的配置会在下一次发送,组包时候起效
			  (会话心跳运行过程中修改RTP参数配置可能引起RTP包逻辑混乱, 应用层需要自己控制)
	    参数: newCfg, 会话配置
	    返回值: 原来的会话拷贝
		*/
        RTPSessConfig SetSessionConfig(RTPSessConfig& newCfg);


		/*
		功能: 绑定RTCP报文监听处理
		      如果没有打开RTCP功能, 则绑定并不生效, 格式不正确的RTCP数据不会通知处理者
	    参数: listener, 指向RTCP接收处理对象
		返回值: <0, 失败; >=0, 成功
		*/
        int addRTCPListener(IRTCPListener* listener);


		/*
		功能: 绑定RTP报文监听处理(送解码等)
		      这里的监听回调只有在接收的数据在RTP校验通过的时候才会通知上层,
              防止非法的乱数据, 该接口会将原有的处理监听是否以绑定新的处理者,
              并不能绑定多个回调
	    参数: listener, 指向RTP接收处理对象
		返回值: <0, 失败; >=0, 成功
		*/
        int addRTPListener(IRTPListener* listener);


		/*
		功能: 以帧为单位发送数据
		      如果打开了RTCP功能, 则RTCP SR报文自动统计并发送
	    参数: type, 1: I帧 2:P帧 3:B帧; data, 指向帧数据;
		      len, 帧长度
	    返回值: <0, 失败; >=0, 成功
		*/
        int sendFrame(MediaFrameType type, const char* data, size_t len);


		/*
		功能: 发送部分帧数据, 对应的数据必须保证为某帧数据的顺序, 连续的分片
		      传输的数据必须是裸帧数据的顺序分段算法的某一段, 并且连续两次调用
			  的分段数据应该连续, 发送时候只有包含了帧结束的包才会将Marker置位,
              并增加时间戳
	    参数: type, 1: I帧 2:P帧 3:B帧; pData, 指向帧数据;
			  len, 帧长度, bIsLastFrag, 当前数据是否为帧尾数据
	    返回值: <0, 失败; >=0, 成功
		*/
        int sendFragmentFrame(MediaFrameType type, const char* pData, size_t iLen, bool bIsLastFrag = false);


		/*
		功能: 不按帧发送数据, 即发送蒙包, 此时每一个包按照分片情况在最后一个分片上打Marker
		参数: data, 指向预发送数据; len, 预发送的数据长度
		返回值: <0, 失败; >=0, 成功
		*/
        int sendData(const char* data, size_t len);


		/*
		功能: 显示重发给定序列号的RTP数据包, 用于带外方式控制重发行为,
		      比如用TCP信令控制重发行为等; RTCP处理者也可以由此发送指定序号的RTP包
	    参数: seq, 重发的RTP序列号
	    返回值: false, 失败, 指定序号的RTP包并不在重发缓冲, 有可能传入的序号包时间太老;
		        true, 发送成功
		*/
        bool resendRTPPacket(unsigned short seq);


		/*
		功能: 设定重发缓冲大小
		      这个Buffer为RTP包的Buffer, 会话层缓冲设定数量的帧数据,
              用于解决重发问题, 如果是蒙包, 则size为蒙包的数量
	    参数: size为帧的数量
		*/
        void setRetransBufferSize(size_t size);

       
		/*
		功能: 设置底层连接缓冲大小
		      设置传输参数应该在构造之前调用传输对象自己的接口来调用设置
	    参数: type, 1 接收 2 发送; size, 缓冲区尺寸
		返回值: <0, 失败; >=0, 成功
		*/
        int setTPBufferSize(TPType type, int size);
		

		/*
		功能: 心跳程序, 处理数据发送与接收
		返回值: 默认为0
		*/
        int heartbeat(void);

        
		/*
		功能: 重传某包请求, 使用RTP中的sequence作为参数, 重传需要对方库支持, 
		      一般RTP不支持重传, 不支持时此请求会被忽略. 如果对端也使用本库
			  则可以实现重传机制. 本库支持最大缓冲300K UDP包的能力
	    参数: sequence, 重发的RTP序列号
		返回值: <0, 失败; >=0, 成功
		*/
        int sendResendRequest(unsigned short sequence);



		/*
		功能: 清理数据接口, 用于暂停操作的时候, 清理未发送完毕的异步数据
		      否则会导致内存堆积影响到其他活跃会话
		*/
        void Clear();


		/*
		功能: 打印RTP协议栈保留的RTP包缓冲, 这里的缓冲数据还没有放入传输层
		*/
        void DumpRTPBuffer();


		/*
		功能: 打印传输层没有发送的传输层数据包, 不包括RTP协议栈数据包
		*/
        void DumpUnsentBuffer();

		/*
		功能: 获取待发送的缓冲数据总量
		返回值: 待发送的缓冲数据总量
		*/
        size_t GetAsyncDataLen()const;

    protected:
        typedef std::map<unsigned int, CRTPPacket*>     RTPMap;
        typedef std::map<unsigned int, char*>           RTCPMap;
        typedef std::deque<CRTPPacket*>                 RTPPacketQueue;

        typedef std::map<unsigned int, CRTPPacket*>::value_type rtpmap_pair;
        typedef std::map<unsigned int, RTPMap*>         JitterMap;
    
    private:
        mutable CRefMutex* _mutex;        /**< 线程互斥锁 */

        RTPPacketQueue m_rtpPacketQueue;  /**< 内部RTP异步发送的包缓冲 */
        RTCPMap        m_rtcpQueue;

        RTPMap         m_resendBuffer;    /**< 重发缓冲 */
        RTPMap         m_ackBuffer;       /**< 发送确认包缓冲，用于传输层缓冲 */

        IRTCPListener* m_rtcpListener;     /**< RTCP包接收处理 */
        IRTPListener*  m_rtpListener;      /**< RTP包接收监听处理 */

    private:
        IRTPTrans*      m_pTrans;        /**< 传输对象 */
        RTPSessConfig   m_sessionCfg;    /**< 会话配置 */
        RTPStatus       m_currStatus;    /**< 会话当前状态 */
        RTCPStats       m_rtcpStats;     /**< RTCP统计信息 */ 
        JitterMap       m_jitterBuffer;
        unsigned short  m_resend_sequence;  ///重发序列号

    
		/*
		功能: 打包并发送媒体数据
		参数: type, 1: I帧 2:P帧 3:B帧; pData, 指向帧数据; iLen, 帧大小;
		      bIsLastFrag, 当前帧是否包含帧尾; bUseSepTs, 是否指定时间戳;
			  timestamp, RTP时间戳
	    返回值: <0, 失败; >=0, 成功
		*/
        int packetAndSend(MediaFrameType type, const char* pData, size_t iLen, 
            bool bIsLastFrag = false, bool bUseSepTs = false, unsigned long timestamp = 0);

	
		/*
		功能: 分析H.264帧媒体数据
		参数: pData, 指向帧数据; iLen, 帧大小; iStartPos, H264帧开始位置; 
		      iValidLen, 有效的H264帧数据长度
	    返回值: <0, 失败; >=0, 成功
		*/
		int CheckH264Packet(const char* pData, size_t iLen, size_t* iStartPos, size_t* iValidLen);



		/*
		功能: 打包H.264帧媒体数据
		参数: type, 1: I帧 2:P帧 3:B帧; pData, 指向帧数据; iLen, 帧大小;
		      bIsLastFrag, 当前帧是否包含帧尾
	    返回值: <0, 失败; >=0, 成功
		*/
		int packetH264Send(MediaFrameType type, const char* pData, size_t iLen, bool bIsLastFrag);
#ifdef FU_A
//定义了H264 OVER RTP 的扩展接口 
		/*
		功能: 发送帧数据
		参数: type, 1: I帧 2:P帧 3:B帧; data, 指向帧数据; len, 帧大小;
		      bIsFirstPacket, 当前帧是否包含帧头; bIsEndPacket, 当前帧是否包含帧尾
	    返回值: <0, 失败; >=0, 成功 
		*/
		int sendFrame(MediaFrameType type, const char* data, size_t len, bool bIsFirstPacket,
			bool bIsEndPacket);

		/*
        功能: 分析H.264帧媒体数据
		参数: pData, 指向帧数据; iLen, 帧大小; iStartPos, H.264帧数据开始位置;
		      iVaildLen, 有效的H.264帧数据长度; bIsFirstPacket, 当前帧是否包含H264帧头
		返回值: <0, 失败; >=0, 成功
		*/
		int CheckH264Packet(const char* pData, size_t iLen, size_t* iStartPos, size_t* iValidLen, bool bIsFirstPacket);
		
		/*
	    功能: 打包H.264帧媒体数据
		参数: type, 1: I帧 2:P帧 3:B帧; pData, 指向帧数据; iLen, 帧大小;
		      bIsFirstPacket, 当前帧是否包含帧头; bIsEndPacket, 当前帧是否包含帧尾
			  返回值: <0, 失败; >=0, 成功
		*/
		int packetH264Send(MediaFrameType type, const char* pData, 
			size_t iLen, bool bIsFirstPacket, bool bIsEndPacket);

		/*
		功能: 设定FU_A指示
		参数: chFUIndicator, FU_A指示标识
		*/
		void setFUIndicator(unsigned char chFUIndicator);

		/*
		功能: 设定FU_A头
		参数: chFUHeader, FU_A头
		*/
		void setFUHeader(unsigned char chFUHeader);

		
		/*
		功能: 获取FU_A指示
		返回值: FU_A指示标识
		*/
		unsigned char getFUIndicator(void) const;


		/*
		功能: 获取FU_A头
		返回值: FU_A头标识
		*/
		unsigned char getFUHeader(void) const;

		unsigned char m_chFUIndicator;
		unsigned char m_chFUHeader;
#else
		
#endif
		/*
		功能: 发送SR报文
		返回值: <0, 失败; 0, 成功
		*/
        int sendSRPackets(); 


		/*
		功能: 发送RR报文
		返回值: <0, 失败; 0, 成功
		*/
        int sendRRPackets();  

		/*
		功能: 逻辑事件处理, 如果要求做重传, 处理RTP重传请求
		参数: session, connId, 分别为会话和通道标识; packet, 指向重传的RTP包
		返回值: <0, 失败; >=0, 成功
		*/
        int onRTPPacketInside(int session, int connId, CRTPPacket* packet);

		/*
		功能: 逻辑事件处理, 如果要求做重传, 处理RTCP重传请求
		参数: session, connId, 分别为会话和通道标识; packet, 指向重传的RTP包
		返回值: <0, 失败; >=0, 成功
		*/
        int onRTCPPacketInside(int session, int connId, CRTCPPacket* packet);
        
		/*
		功能: 根据序列号设定重发请求
		参数: sequence, 重发的RTP序列号
		返回值: <0, 失败; >=0, 成功
		*/
        int sendResendRequestInside(unsigned short sequence);
public:
	    /*
	    功能: 获取RTP包序列号
		返回值: RTP包序列号
	    */
        unsigned short getSequenceNum(void);

		/*
		功能: 设定RTP包序列号
		参数: sequence, RTP包序列号
		*/
		void setSequenceNum(unsigned short sequence);

		/*
		功能: 获取RTP包时间戳
		返回值: RTP包时间戳
		*/
		unsigned int getTimeStamp(void);

		/*
		功能: 设定RTP包时间戳
		参数: nTimeStamp, RTP包时间戳
		*/
		void setTimeStamp(unsigned int nTimeStamp);


        unsigned short _resend_sequence;
		
		/*
		功能: 重设数据接收回调处理
		*/
        void reset(void);
		
    private:
        /** 传输层事件处理接口实现 */
        class CTransListener : public ITPListener
        {  
        public:
            CTransListener(ICRTPSession* pSess):m_pManager(pSess){}
            virtual ~CTransListener(){};
        
        private:
            //重定义接口
			/*
			功能: 有数据到达时候，通过该接口供上层重写，实现自己的数据处理逻辑,
			调用方需要根据首地址和长度去除对应的消息内容, 实现自己的协议逻辑;
			参数提供的数据指针, 指向的消息体中可能存放的是二进制数据
			connId, 表征客户连接的id, 在同一个客户处理中, 应该使用同样的id来发送数据
			否则可能发生数据发送失败或者串包的情况(刚好有多个客户连接)
			如果需要在接口之外的地方发数据, 需要自己保存客户ID和数据之间的对应关系
			参数: engineId, 服务标识; clientId, 客户连接ID; chnId, 内嵌二进制数据通道ID;
			data 负载数据起始指针; len, 数据长度
			返回值: <0, 失败; >=0, 成功
			*/
            virtual int onData(int engineId, int connId, const char* data, int len);

			/*
			功能: 发送时数据放在队列中, 当心跳heartbeat时才实际发送, 发送成功时将之前
			的ID确认给调用者, 用于清除数据区指针, 发送直接删除指针会引起错误
			此接口用于消息异步处理, 一个典型的应用场景是:
			ITPObject在发送数据的时候传入外部指针(或者资源句柄), 在此处得到通知后
			删除缓存起来的指针(或者释放资源句柄)
			该接口在数据网络发送确认的时候被调用, 表示数据已经被完整的从该层发送出去
			实现中可以调用其他接口实现一些特殊功能, 如关闭连接, 实现短连接等
			参数: engineId, 服务标识; clientId, 客户连接ID;
			id,  对应于ITPObject的Send操作的返回值
			返回值: <0, 失败; >=0, 成功      
			*/
            virtual int onSendDataAck(int engineId, int connId, int id);


			/*
			功能: 客户端主动关闭了该连接接时候, 通知上层处理
			参数: engineId, 服务标识; clientId, 客户连接ID
			返回值: <0, 失败; >=0, 成功
			*/
            virtual int onClose(int engineId, int connId){ return 0;}

			/*
			功能: 客户端连接回调函数
			参数: engineId, 服务标识; clientId, 客户连接ID;
			ip, port, 分别为对端的IP地址和端口
			返回值: 0, 接受此连接; 1, 拒绝此连接 
			*/
            virtual int onConnect(int engineId, int connId, const char* ip, int port){return 0;}


			/*
			功能: 发送数据失败时候的回调处理, 用于发送失败和异常处理
			该接口用于通知上层发送数据的结果, 当该接口得到调用时候,
			对应数据无法提交发送; 上层应由此清理对应的异步消息数据, 避免内存泄露
			该接口仅仅在异常情况下调用以清理资源, 调用点可能连接已经被释放, 或者资源正被清理
			此接口实现时候, 不要调用传输层的其他方法, 否则会引起内部错误
			参数: engineId, 服务标识; clientId, 客户连接ID;
			id,  对应于ITPObject的Send操作的返回值
			返回值: <0, 失败; >=0, 成功
			*/
            virtual int onSendDataFail(int engineId, int connId, int id);

            ICRTPSession*  m_pManager;
        };        
        CTransListener* m_pTransListener;


		/*
		功能: 绑定传输事件
		*/
        void CreateAndBindListener();
    };
}
#endif
