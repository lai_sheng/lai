#ifndef RTSP_RTPPORTPOOL_H 
#define RTSP_RTPPORTPOOL_H

#include <vector>

namespace CommonLib_VD
{
    ///< RTP port pair  in rtsp setup message 
    typedef std::pair<unsigned short, unsigned short> PortPair;

    /**
    * A Simple singleton template,meyers singleton                                 
    * 生成singleton类的方法：                                                     
    * 1: 公有继承Singleton<yourclass>                                             
    * 2: 声明Singleton<yourclass>为友元                                           
    * 3: 声明默认构造参数为private                                                
    * 参考CKDMConfigProxy的定义；                                                                                               * 
    */
    template<class T>
    class Singleton
    {
    public:
        static T& instance()
        {
            static T theInstance;
            return theInstance;
        }

    protected:
        Singleton(){};
        virtual ~Singleton(){};

    private:
        Singleton(const Singleton&);//surppress copy construct
        Singleton& operator ==(const Singleton&);//surppress assignment
    };

    /**
    * @brief
    * RtpPortPool class is responsible for RTP port pool management.
    * A port pool provide a pool of port pair to be inited and selected by its interface
    * 
    * @remarks  
    * This class is a singleton;single thread now
    *
    * @see
    * RFC 2326 has more specifiction on RTP port pair in rtsp setup message process.
    */
	class CRtpPortPool:public Singleton<CRtpPortPool>
    {
    protected:
        ///< Port pool element;the first elem stands for the used flag
        typedef std::pair<bool, PortPair> PortElem;
        typedef std::vector<PortElem>     PortPool;

    public:
        ///Singleton declaration
        friend class Singleton<CRtpPortPool>;
        virtual ~CRtpPortPool(void);

        /**
        * @brief
        * Init the port pool with a init start port
        * 
        * @param [in] firstPort The first port in the pool
        * @param [in] MaxPairNum How many port pair it can provide
        *
        * @remarks
        * Inner port validity is not checked. 
        */
        void InitPortPool(unsigned short firstPort, int MaxPairNum);

        /**
        * @brief
        * Get an unused port pair and pass out
        * 
        * @param [out] pair The first unused port pair in the pool
        *
        * @returns
        * -true On success,and the port is passed out
        * -false No port pair is available now
        */
        bool GetPortPair(PortPair& pair);

        /** 
        * Release port pair;If not exist,return false
        */
        bool ReleasePortPair(PortPair pair);

        /**
        * Release port pair overloads with [first, first+1];
        */
        bool ReleasePortPair(unsigned short first);

    private:
        CRtpPortPool();
        PortPool   m_pool;     
    };
}


#endif

