/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：RTPTrans.h
* 摘　　要：RTP会话创建过程中用到的传输封装。
* 作    者：闫飞 10811
*/

#ifndef __FE_RTP_TRANSPORTS_H__
#define __FE_RTP_TRANSPORTS_H__

#include <Rtsp/TPLayer/ITPListener.h>

#include "Rtsp/rtp/RTPProtocol.h"
#include "Rtsp/rtp/RTPStructs.h"

namespace CommonLib_VD
{
    class ITPObject;
    class CRTSPBase; 
    class CRefMutex;

    /** 会话所用的传输层 */
    class IRTPTrans
    {
    public:
        IRTPTrans(Model mode):model(mode){};
        virtual ~IRTPTrans(){};

		/*
		功能: 发送给定的原始数据包(RTP/RTCP)
		*/
        virtual int SendPacket(const char* rawData, size_t len, bool isRtp = true) = 0;

		/*
		功能: 获取RTP传输层通道对象
		*/
        virtual ITPObject* GetRTPTrans() = 0; 

       
		/*
		功能: 获取RTCP传输层对象
		*/
        virtual ITPObject* GetRTCPTrans() = 0;

        /*
		功能: 确定是否使用其它传输方式, 主要是RTP/RTSP模式
		*/
        virtual bool IsExternalTrans(){return false;}
        
        /*
		功能: 确定本方收发角色
		*/
        bool IsSender(){return model == SENDER;}

    private:
        Model model;
    };

    /** 实现RTP/UDP传输模式 */
    class CRTPUDPTrans : private IRTPTrans
    {
    private:
        ///不允许生成，也不允许继承
        CRTPUDPTrans(Model mode, TransPara& local, TransPara& remote);

        ///禁止拷贝、赋值
        CRTPUDPTrans(CRTPUDPTrans& rhs);
        CRTPUDPTrans& operator= (CRTPUDPTrans& rhs);
    
    public:
        enum{S_RTP_ID = 101, S_RTCP_ID = 102, R_RTP_ID = 103, R_RTCP_ID};

        virtual ~CRTPUDPTrans();

        /*
		功能: 创建CRTPUDPTrans对象, 工厂方法
		参数: srModel, 本端收发角色; local, remote,本端和远端的传输参数;
		      rtcpEnable, 本端是否启用RTCP功能
		*/
        static IRTPTrans* CreateTrans(Model srModel, TransPara& local, TransPara& remote,
            bool rtcpEnable = false);

  
		/*
		功能: 发送给定的原始数据包(RTP/RTCP)
		返回值: <0, 失败; >0, 成功
		*/
        virtual int SendPacket(const char* rawData, size_t len, bool isRtp = true);


		/*
		功能: 获取RTP传输层通道对象
		*/
        virtual ITPObject* GetRTPTrans(); 


		/*
		功能: 获取RTCP传输层通道对象
		*/
        virtual ITPObject* GetRTCPTrans();

    private:
        TransPara   m_localParam;   /**< 本地端口、参数 */
        TransPara   m_peerParam;    /**< 远程端口、参数 */

        ITPObject*  m_rtpChannel;   /**< RTP传输封装 */
        ITPObject*  m_rtcpChannel;  /**< RTCP传输封装 */
    };

    /** RTP/RTSP模式传输层实现 */
    class CRTPRTSPTrans : private IRTPTrans
    {
    private:
        ///不允许生成，也不允许继承
        CRTPRTSPTrans(Model model);

        ///禁止拷贝、赋值
        CRTPRTSPTrans(CRTPRTSPTrans& rhs);
        CRTPRTSPTrans& operator= (CRTPRTSPTrans& rhs);

    public:
        virtual ~CRTPRTSPTrans();
		/*
		功能: 创建和给定的RTSP会话对应的传输层对象, 该类的传输层时间行为
		      都递交给RTSP会话来处理(工厂方法)
	    参数: mode, Sender or Receiver; rtpChn, RTP通道ID;
		      rtcpChn, RTCP通道ID; pRtspConn, 对应的RTSP连接;
			  rtspId, rtsp连接子会话ID
		*/
        static IRTPTrans* CreateTrans(Model mode, unsigned char rtpChn, unsigned char rtcpChn, 
            CRTSPBase* pRtspConn, int rtspId);

       
		/*
		功能: 发送给定的原始数据包(RTP/RTCP)
		*/
        virtual int SendPacket(const char* rawData, size_t len, bool isRtp = true);

       
		/*
		功能: 获取RTP传输层通道对象
		*/
        virtual ITPObject* GetRTPTrans(); 

       
		/*
		功能: 获取RTCP传输层对象
		*/
        virtual ITPObject* GetRTCPTrans();
		
		
		/*
		功能: 确定是否使用其它传输方式, 主要是RTP/RTSP模式
		*/
        virtual bool IsExternalTrans(){return true;}

        ////传输层被托管实现，事件处理都递交给对应的RTSP，对应的RTSP连接需要处理对应事件
    private:
        unsigned char m_rtpChn;   /** Interleaved RTP Channel ID */
        unsigned char m_rtcpChn;  /** Interleaved RTCP Channel ID */

        CRTSPBase*    m_rtspConn;
        int           m_rtspSessionId;
    };

}

#endif
