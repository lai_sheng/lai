#ifndef _H_CRTPSESSION
#define _H_CRTPSESSION

#include "Rtsp/rtp/RTPProtocol.h"
#include "Rtsp/rtp/RTPStructs.h"
#include <TPLayer/TPTypedef.h>

#define GENERAL
namespace CommonLib_VD
{
	class IRTPTrans;
	class CRefMutex;
	class IRTPListener;
	class IRTCPListener;
	class CRTSPBase;
	class CZMutex;
	class CRTPSession
	{
	public:
		/*
		功能: 绑定RTP报文监听(送解码等)
		      这里的监听回调只有在接收的数据在RTP校验通过的时候才会通知上层，
			  防止非法的乱数据, 该接口会将原有的处理监听是否以绑定新的处理者，
			  并不能绑定多个回调
	    参数: listener, 指向RTP接收处理对象
	    返回值: <0, 失败; >=0, 成功
	    */
		virtual int addRTPListener(IRTPListener* listener) = 0;

		/*
		功能: 绑定RTCP报文监听处理, 如果没有打开RTCP功能，则绑定并不生效;
		      格式不正确的RTCP数据不会通知给处理者
	    参数: listener, 指向RTCP接收处理对象
	    返回值: <0, 失败; >=0, 成功
		*/
		virtual int addRTCPListener(IRTCPListener* listener) = 0;

		
		/*
		功能: 不按帧发送数据,即发送蒙包, 此时每一个包按照分片情况在最后一个分片上打Marker
		参数: data, 指向发送数据; len, 发送数据长度
		返回值: <0, 失败; >=0, 成功
		*/
		virtual int sendData(const char* data, size_t len) = 0;

		/*
		功能: 生命心跳
		返回值: 默认为0
		*/
		virtual int heartbeat(void) = 0;

		/*
		功能: 重传某包请求，使用RTP中的sequence作为参数; 重传需要对方库支持,
		      一般RTP不支持重传; 不支持时此请求会被忽略; 如果对端也使用本库则
			  可以实现重传机制; 本库支持最大缓冲300K UDP包的能力
	    参数: sequence, 预重传的RTP包序列号
	    返回值: 默认为0
		*/
		virtual int sendResendRequest(unsigned short sequence) = 0;

		/*
		功能: 修改会话参数配置, 该函数用于创建会话之后, 更改会话的配置,
		      最好在HeartBeat开启前设置; 新的配置会在下一次发送、组包时候起效;
              会话心跳运行过程中修改RTP参数配置可能引起RTP包逻辑混乱, 应用层
			  需要自己控制原来的会话配置拷贝
	    参数: newCfg, 新的RTP配置
		返回值: 默认返回newCfg, ICRTPSession派生后, 返回oldCfg
		*/
		virtual RTPSessConfig SetSessionConfig(RTPSessConfig& newCfg) {return newCfg;}

		/*
		功能: 设置底层连接缓冲大小, 由于传输和会话已经分开，此接口不赞成使用,
              设置传输参数应该在构造之前调用传输对象自己的接口来调用设置
		参数: type, 1 接收 2 发送; size, 缓冲区尺寸
		返回值: 默认为0
		*/
		virtual int setTPBufferSize(TPType type, int size) {return 0;}

		/*
		功能: 以帧为单位发送数据, 底层会自动分包并置帧标示位,
		      如果打开了RTCP功能, 则RTCP SR报文自动统计并发送
	    参数: type,1: I帧 2: P帧 3: B帧; bIsFirstPacket, 媒体包包含大华包头;
		      bIsEndPacket, 媒体包是分包后的最后一个媒体包
	    返回值: <0, 失败; >=0, 成功
		*/
#ifdef FU_A
		virtual int sendFrame(MediaFrameType type, const char* data, size_t len, bool bIsFirstPacket,
			bool bIsEndPacket) {return 0;}
#endif
		virtual int sendFrame(MediaFrameType type, const char* data, size_t len) {return 0;}

		/*
		功能: 发送部分帧数据，对应的数据必须保证为某帧数据的顺序、连续的分片,
		      传输的数据必须是裸帧数据的顺序分段算法的某一段, 并且连续两次调用
			  的分段数据应该连续, 发送时候只有包含了帧结束的包才会将Marker置位,
              并增加时间戳
	    参数: type,1: I帧 2: P帧 3: B帧; bIsLastFlag, 媒体包是分包后的最后一个媒体包
		返回值: <0, 失败; >=0, 成功
		*/
		virtual int sendFragmentFrame(MediaFrameType type, const char* pData, size_t iLen, bool bIsLastFrag = false) {return 0;}

		/*
		功能: 显示重发给定序列号的RTP数据包, 用于带外方式控制重发行为, 比如用TCP信令控制重发行为等;
              RTCP处理者也可以由此发送指定序号的RTP包
	    参数: 重传的RTP包序号
	    返回值: true, 发送成功; false, 失败, 指定序号的RTP包并不在重发缓冲，有可能传入的序号包时间太老
		*/
		virtual bool resendRTPPacket(unsigned short seq) {return true;}

		/*
		功能: 设定重传缓冲大小, 这个Buffer为RTP包的Buffer
		参数: size, 为帧的数量, 会话层缓冲设定数量的帧数据, 用于解决重发问题.
		      如果是蒙包,则size为蒙包的数量.
		*/
		virtual void setRetransBufferSize(size_t size) {};

		/*
		功能: 清理数据接口，用于暂停操作的时候，清理未发送完毕的异步数据,
		      否则会导致内存堆积影响到其他活跃会话
		*/
		virtual void Clear() {};

		/*
		功能: 获取待发送的缓冲数据总量
		返回值: 缓冲数据总量
		*/
		virtual size_t GetAsyncDataLen() const {return 0;}

		//ZTE扩展接口
		/*
		功能: 在会话为RTP OVER RTSP模式下需要设置RTSP的连接. 此连接只在type为2时有效
		参数: rtsp, RTSP消息处理对象; clientId, client标识; chn, 通道标识
		*/
		virtual void setRTSPConnection(CRTSPBase* rtsp, int clientid, int chn) {};

		/*
		功能: 用于设定初始时间戳, 一般不用设, 从0开始
		参数: time, RTP时间戳
		*/
		virtual void setTimestamp(unsigned long time) {};

		/*
		功能: 设置RTCP发送间隔(由于使用情形限定, 我们不使用动态间隔), 单位是秒
		参数: interval, 发送间隔
		*/
		virtual void setRTCPInterval(int interval) {};

		/*
		功能: 如果是发送方,需要指定服务器的地址,地址是RTP地址,RTCP端口号自动加1
		参数: ip, port, 分别为发送方的地址和端口
		*/
		virtual void setDistAddress(char* ip, int port) {};

		/*
		功能: 发送方也需要指定发送时的地址和端口,端口是包含RTCP的,为RTP端口加1
		参数: ip, port, 分别为本地的地址和端口
		*/
		virtual void setLocalAddress(char*ip, int port) {};

		/*
		功能: 如果是接收方,需要指定自身监听的地址和端口,RTCP端口号自动加1
		参数: ip, port, 分别为本地地址和绑定端口
		*/
		virtual void setListenAddress(char* ip, int port) {};

		/*
		功能: 设定RTP包头是否包含扩展位
		参数: ext, false, 不包含扩展; true, 包含扩展
		*/
		virtual void setExtension(bool ext) {};

		/*
		功能: 设定RTP包头扩展位缓冲
		参数: type, 扩展类型; pBuf, 指向RTP包头扩展位缓冲; len, 扩展长度
		返回值: <0, 失败; >=0, 成功
		*/
		virtual int setExtensionBuf(unsigned short type, void *pBuf, int len) {return 0;}

		/*
		功能: 设定RTP包SSRC标识
		参数: ssrc, RTP包SSRC标识
		*/
		virtual void setSSRC(unsigned long ssrc) {};

		/*
		功能: 得到RTP包SSRC标示
		返回值: ssrc, RTP包SSRC标识
		*/
		virtual unsigned long getSSRC(void) {return 0;}
		virtual ~CRTPSession();
		
	};
#ifdef ZTE
	/*
	功能: 得到CRTPSession对象指针(ZTE扩展接口)
	参数: ppCRTPSession, 传出参数, 指向CRTPSession对象指针的指针; 
	      id, RTP通道标示; mode, 标示RTP为发送或者接受方;
		  type, 用于设置RTP会话的类型，0：RTP over UDP，1：RTP over TCP，2：RTP over RTSP
	返回值: 0, 失败; 1, 成功
	*/
	extern "C" int CreateCRTPStream(CRTPSession** ppCRTPSession, int id, 
		Model mode, int type = 0);
#endif
#ifdef GENERAL
	/*
	功能: 得到CRTPSession对象指针(通用网络接口)
	参数: ppCRTPSession, 传出参数, 指向CRTPSession对象指针的指针;
	      sessCfg, RTP会话配置信息; pTrans, RTP over tcp, RTP over udp,
		  RTP over rtsp传输功能接口; refMutex, 互斥量
    返回值: 0, 失败; 1, 成功
	*/
	extern "C" int CreateCRTPStream(CRTPSession** ppCRTPSession, 
		RTPSessConfig& sessCfg, IRTPTrans* pTrans, CRefMutex* refMutex = 0);
#endif
}
#endif