/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：RTPStructs.cpp
* 摘　　要：RTP会话创建过程中用到的一些结构和操作。
* 作    者：闫飞 10811
*/


namespace CommonLib_VD
{
    class CRTPSession;
    class CRTPBasicSchedule
    {
    public:
        /**
        * @brief
        * Start this session to run heartbeat,send data
        * 
        * @param pSess
        * The Session to be scheduled,external pointer
        * 
        * @returns
        * - true On success.
        * - false On any error occurs
        */
        virtual bool StartSession(CRTPSession* pSess) = 0;

        /**
        * @brief
        * Pause this session, heartbeat is paused until next resume
        * 
        * @param pSess
        * The Session to be scheduled
        * 
        * @returns
        * - true On success.
        * - false On any error occurs
        */
        virtual bool PauseSession(CRTPSession* pSess) = 0;

        /**
        * @brief
        * Resume this session, heartbeat is resumed after former pause op.
        * If this session hasn't been paused, nothing will be done
        *
        * @param pSess
        * The Session to be scheduled
        * 
        * @returns
        * - true On success.
        * - false On any error occurs
        */
        virtual bool ResumeSession(CRTPSession* pSess) = 0;

        /**
        * @brief
        * Stop this session, the session is deleted from schedule list
        * If this session is not in schedule list,do nothing
        *
        * @param pSess
        * The Session to be scheduled
        * 
        * @returns
        * - true On success.
        * - false On any error occurs
        */
        virtual bool StopSession(CRTPSession* pSess) = 0;
    };
}
