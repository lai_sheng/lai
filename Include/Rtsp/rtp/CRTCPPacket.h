/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：CRTCPPacket.h
* 文件标识：参见配置管理计划书
* 摘　　要：RTCP PDU封装类，实现RFC中定义的RTCPPDU。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年7月15日
* 修订记录：稳定，标准RTP及带重发的RTP会话都完成了。

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年7月1日
* 修订记录：创建
*/

#ifndef __FE_RTCP_PACKET_H__
#define __FE_RTCP_PACKET_H__

#include "Rtsp/rtp/RTPProtocol.h"
#include <vector>

namespace CommonLib_VD
{
    class CRTCPPacket
    {
    public:
		/*
		功能: 重载构造函数
		参数: type, 类型定义如下:
		// RTCP消息类型 
		enum RTCPTYPE
		{
			SR		= 200,  // Sender Report 
			RR		= 201,  // Receiver Report
			SDES	= 202,  // Sender Description 
			BYE		= 203,  // Bye 
			APP		= 204,  // Application specification 
		};
		*/
        CRTCPPacket(RTCPTYPE type);

		/*
		功能: 重载构造函数(为了与中兴接口兼容)
		参数: data, 指向RTCP包缓冲区; len, RTCP包大小
		*/
        CRTCPPacket(const char* data, size_t len);
        ~CRTCPPacket();
        
		/*
		功能: 解析数据流, 检查是否为RTCP数据包
		参数: data, 指向数据包的指针; len, 数据包长度
		返回值: false, 不是RTCP数据包; true, 是RTCP数据包
		*/
        static bool ParseStream(const char*data, size_t len);

		/*
		功能: 得到RTCP版本
		返回值: RTCP版本号
		*/
        int GetVersion();

		/*
		功能: 设定RTCP版本
		参数: version, RTCP版本号
		*/
        void SetVersion(int version);

		/*
		功能: 检查RTCP包是否有填充
		返回值: 0, 不包含填充; 1, 包含填充
		*/
        int GetPadding();

		/*
		功能: 设定RTCP包填充
		参数: padding, RTCP包填充信息
		*/
        void SetPadding(int padding);

		/*
		功能: 得到RTCP包的RC值
		返回值: RTCP包的RC值
		*/
        int GetRC();

		/*
		功能: 设定RTCP包的RC值
		参数: RC, RTCP包的RC值
		*/
        void SetRC(int RC);

		/*
		功能: 得到RTCP包的消息类型
		返回值: 见如下定义
		enum RTCPTYPE
		{
			SR		= 200,  // Sender Report 
			RR		= 201,  // Receiver Report
			SDES	= 202,  // Sender Description 
			BYE		= 203,  // Bye 
			APP		= 204,  // Application specification 
		};
		*/
        RTCPTYPE GetPayloadType();

		/*
		功能: 设定RTCP包头的消息类型
		参数: payload, 见如下定义
		enum RTCPTYPE
		{
		SR		= 200,  // Sender Report 
		RR		= 201,  // Receiver Report
		SDES	= 202,  // Sender Description 
		BYE		= 203,  // Bye 
		APP		= 204,  // Application specification 
		};
		*/
        void SetPayloadType(RTCPTYPE payload);

		/*
		功能: 得到RTCP包长度
		返回值: RTCP包长度
		*/
        unsigned short GetLength();

		/*
		功能: 设定RTCP包长度
		参数: length, RTCP包长度
		*/
        void SetLength(unsigned short length);

		/*
		功能: 得到发送端SSRC标识
		返回值: 发送端SSRC标识
		*/
        unsigned long GetSenderSSRC();

		/*
		功能: 设定发送端SSRC标识
		参数: 发送端SSRC标识
		*/
        void SetSenderSSRC(unsigned long ssrc);

		/*
		功能: 得到发送端Info(SR和RR使用)
		参数: info类型定义见如下
		typedef struct
		{
			unsigned long ntpTimestampHigh; // NTP time high word
			unsigned long ntpTimestampLow;  // NTP time low word
			unsigned long rtpTimestamp;     // RTP timestamp 
			unsigned long packetCount;      // total packets sent till now 
			unsigned long octetCount;       // total octets sent
		} SSenderInfo;
		返回值: <0, 失败; >=0, 成功
		*/
        int GetSenderInfo(SSenderInfo* info);

		/*
		功能: 设定发送端Info
		参数: info类型定义见如下
		typedef struct
		{
		unsigned long ntpTimestampHigh; // NTP time high word
		unsigned long ntpTimestampLow;  // NTP time low word
		unsigned long rtpTimestamp;     // RTP timestamp 
		unsigned long packetCount;      // total packets sent till now 
		unsigned long octetCount;       // total octets sent
		} SSenderInfo;
		返回值: <0, 失败; >=0, 成功
		*/
        int SetSenderInfo(SSenderInfo* info);

		/*
		功能: 增加RTCP包ReportBlock
		参数: index从1开始, 第一个block索引是1; 
		      block定义如下
		typedef struct
		{
			SLost lost;                      //发送端的丢包数
			unsigned long highestSequence;   //接收端收到的最大RTP包序号
			unsigned long jitter;            //网络抖动
			unsigned long lastSR;            //上次SR报告接收时间
			unsigned long delaySR;           //两次SR报告接收时间间隔
		} SReportBlock;
		返回值: <0, 失败; 0, 成功
		*/
        int AddReportBlock(int index, SReportBlock* block);

		/*
		功能: 得到RTCP包ReportBlock
		参数: index从1开始, 第一个block索引是1; 
		      block类型定义见AddReportBlock(...)
	    返回值: <0, 失败; 0, 成功
		*/
        int GetReportBlock(int index, SReportBlock* block);

		/*
		功能: 增加RTCP包Chunk
		参数: index从1开始, 第一个chunk索引是1;
		      chunk类型定义如下
		typedef struct 
		{
			unsigned long ssrc;    //源标识
			SCname scname;         //机器标识,1个机器的多个源具有同样的scname
		}SChunk;
		返回值: <0, 失败; 0, 成功
		*/
        int AddChunk(int index, SChunk* chunk);

		/*
		功能: 得到RTCP包Chunk
		参数: index从1开始, 第一个chunk索引是1;
		      chunk类型定义见AddChunk(...)
        返回值: <0, 失败; 0, 成功
		*/
        int GetChunk(int index, SChunk* chunk);

		/*
		功能: 得到RTCP包的APP内容
		参数: info定义如下
		typedef struct 
		{
			unsigned long name;        //源标识
			unsigned short seqence1;   //丢包开始序列号
			unsigned short seqence2;   //丢包结束序列号
		}SAppResend;
		返回值: <0, 失败; 0, 成功
		*/
        int GetAppResend(SAppResend* info);

		/*
		功能: 设定RTCP包的APP内容
		参数: info类型定义见GetAppResend(...)
		返回值: <0, 失败; 0, 成功
		*/
        int SetAppResend(SAppResend* info);

		/*
		功能: 得到RTCP包内容
		返回值: 指向RTCP包内容
		*/
        unsigned char* GetStream();

		/*
		功能: 得到RTCP包长度
		返回值: RTCP包长度
		*/
        int GetStreamLen();

        /*
		功能: 释放对象引用
		返回值: 如果--_ref等于0, delete this, 返回0;
		        否则返回--_ref
		*/
        int Release(void);

		/*
		功能: 增加对象引用
		返回值: ++_ref
		*/
        int AddRef(void);

    public:
        static const int RESEND = 14;

    private:
        unsigned char* _data;
        int _length;

        int _ref;
    };


    class IRTCPListener
    {
    public:
        virtual ~IRTCPListener(){};
		/*
		功能: RTCP接收回调函数
		参数: session, connId, 分别为本次会话和通道标识, 
		      packet, 指向CRTCPPacket对象实例的指针
	    返回值: <0, 失败; 0, 成功
		*/
        virtual int onRTCPPacket(int session, int connId, CRTCPPacket* packet) = 0;
    };

}
#endif
