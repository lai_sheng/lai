/*!********************************************************************************************************
**                  Rtsp Module for General Network
***********************************************************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              FrameGeneral.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-5-13   8:33
**Modified:               
**Modify Reason: 
**Description:           录像加入,从底层上来的数据没有包头,目前是每次上来的packet都是大小为32K数据包,为了方
**                       便组包,先把这些没头的录像数据组帧,然后封装成有头的packet包,为了方便,第个packet只有
**                       一帧.
**********************************************************************************************************/

#ifndef FRAME_GENERAL_H_
#define FRAME_GENERAL_H_

#include <stdio.h>
#include <System/Packet.h>
//#include <System/MediaStream.h>
#include "Rtsp/BaseFunc/VDDebug.h"

namespace CommonLib_VD
{

#define FRAME_TYPE_NUM 4
#define KEY_WORD_LEN   4
const int Frame_Key_Word[FRAME_TYPE_NUM] = {0xFC010000, 0xFD010000, 0xFE010000,0xF0010000};
const int frameLenOff[FRAME_TYPE_NUM] = {4, 12, 4, 6};
const int frameHeaderLen[FRAME_TYPE_NUM] = {8, 16, 8, 8};

class CFrameGeneral
{
public:
	enum
	{
		IRET_ERROR = -1,
		IRET_SUCCESS = 0,
		IRET_NORMAL = 2,
	}IRET_CODE;

    #define FRAME_PACKET_LEN                  1024*256
	#define FRAME_HEADER_LEN                  16

	
	
public:
	CFrameGeneral()
	{
	    m_State = 1;
		m_parsePtk   = g_PacketManager.GetPacket(FRAME_PACKET_LEN);
		if(!m_parsePtk)
		{
		    m_State = -1;
		    VD_TRACE("CFrameGeneral,GetPacket error\n");
		}
		Init();
	}
	~CFrameGeneral()
	{
	    m_parsePtk->Release();
		m_parsePtk = NULL;
	}
	void Init()
	{
		m_PtkLeftLen = 0;
		if(m_parsePtk)
		{
		    memset(m_parsePtk->GetBuffer(), 0, m_parsePtk->GetSize());
		}
	}
	
	int AddPacket(CPacket *sPtk);//向缓冲中加入待解析的录像数据
	int Parse(CPacket *& dPtk, int beginPos, int &dstPos);//分析录像数据,组帧
	int GetState()
	{
	     return m_State;
	}
private:
	enum PackTypeFlag FindFrameHeader(char *buf,int len,int &pos);//找帧头
	/*获取帧的长度时用,str的长度至少为四个字节,little endian*/
	int FrameAtoi(char *ptr, int frameType)
	{
	    unsigned char* str = (unsigned char* )ptr;
	    if(!str/* || strlen(str) < 4*/)
	    {
	        return IRET_SUCCESS;
		}
	    int frameLen = 0;

		frameLen = (int)str[0];
	    frameLen |= (int)str[1]<<8;
		if(PACK_TYPE_FRAME_AUDIO != frameType )
		{
	        frameLen |= (int)str[2]<<16;
	        frameLen |= (int)str[3]<<24;
		}

		return frameLen;
	}
		
private:
	CPacket *m_parsePtk;//缓冲待解析的数据
	int m_PtkLeftLen;//记录上一次没有解析完的数据长度
	int m_State;
};

}

#endif
