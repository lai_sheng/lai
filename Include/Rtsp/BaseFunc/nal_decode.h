#ifndef __NAL_DECODE_H__
#define __NAL_DECODE_H__

//#include "Infra/Types.h"

typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;

/*类型定义*/
#define NAL_SLICE		1
#define NAL_DPA			2
#define NAL_DPB			3
#define NAL_DPC			4
#define NAL_IDR_SLICE		5
#define NAL_SEI			6
#define NAL_SPS			7
#define NAL_PPS			8
#define NAL_PICTURE_DELIMITER	9
#define NAL_FILTER_DATA		10
#define NAL_MAX 8

#ifndef NULL
#define NULL 0
#endif

namespace CommonLib_VD
{

typedef struct tagNalInfo{
	int iNr;/*返回解析出的nal数目*/
	unsigned char* pucAddress[NAL_MAX];
	int iSize[NAL_MAX];
	int iType[NAL_MAX];/*类型*/
}NAL_INFO;



void analyze_nal(unsigned char* pcSrc, int* piConsumed, int iLength, int* piType);
int decode_nal_info(unsigned char* pcBuf, int iBufSize, NAL_INFO *pszNalInfo);

}

#endif

