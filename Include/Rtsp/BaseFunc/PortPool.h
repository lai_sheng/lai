#pragma once

#include <list>
#include <vector>

#include "System/Object.h"
#include "MultiTask/Mutex.h"

namespace CommonLib_VD
{

///< Port pair  in rtsp setup message 
typedef std::pair<unsigned short, unsigned short> PortPair;
class CPortPoolManager;

class CPortPool:public CObject
{
public:
	friend class CPortPoolManager;
public:
	CPortPool();
	virtual ~CPortPool();
protected:
    ///< Port pool element;the first elem stands for the used flag
    typedef std::pair<bool, PortPair> PortElem;
    typedef std::list<PortElem>     PortPool;

public:

    /**
    * @brief
    * Init the port pool with a init start port
    * 
    * @param [in] firstPort The first port in the pool
    * @param [in] MaxPairNum How many port pair it can provide
    *
    * @remarks
    * Inner port validity is not checked. 
	* @return false-指定的端口范围已经被占用
    */
    bool InitPortPool(unsigned short firstPort, int MaxPairNum);

    /**
    * @brief
    * Get an unused port pair and pass out
    * 
    * @param [out] pair The first unused port pair in the pool
    *
    * @returns
    * -true On success,and the port is passed out
    * -false No port pair is available now
    */
    bool GetPortPair(PortPair& pair);

    /** 
    * Release port pair;If not exist,return false
    */
    bool ReleasePortPair(PortPair pair);

    /**
    * Release port pair overloads with [first, first+1];
    */
    bool ReleasePortPair(unsigned short first);

private:
	unsigned short m_firstPort;
	int m_MaxPairNum;
    PortPool   m_pool;
	CMutex	   m_mutex;
};

//!在CPortPool申请端口范围时，集中管理所有端口，防止重复申请，及支持多程序同时申请
class CPortPoolManager : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CPortPoolManager);

	int AddPortPool(CPortPool* pPortPool);
	int RemovePortPool(CPortPool* pPortPool);

	//!请求这个端口范围，如果全部空闲就成功-0，否则失败- -1
	//!这个端口范围必须没有人申请过
	int RequestPortPool(unsigned short firstPort, int MaxPairNum);
private:
	CPortPoolManager();
	virtual ~CPortPoolManager();

	CMutex m_Mutex;

	typedef std::vector<CPortPool*> PortPoolList;
	PortPoolList m_pool_list;
};

}

//#define g_SMPortPool (*CPortPool::instance())


