/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：ITPListener.h
* 文件标识：参见配置管理计划书
* 摘　　要：传输层监听器接口
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/
#ifndef _ITPLISTENER_H_
#define _ITPLISTENER_H_

#include <stdlib.h>

namespace CommonLib_VD
{
    class ITPListener
    {
    public: 
        /** dummy virtual destructor */
        virtual ~ITPListener(){};

        /** 
        * 有数据到达时候，通过该接口供上层重写，实现自己的数据处理逻辑
        * 调用方需要根据首地址和长度去除对应的消息内容，实现自己的协议逻辑；参数提供的数据指针
        * 指向的消息体中可能存放的是二进制数据，只有@c len指定的长度数据是有效的
        *
        * @warning @c connId,表征客户连接的id,在同一个客户处理中，应该使用同样的id来发送数据
        * 否则可能发生数据发送失败或者串包的情况(刚好有多个客户连接)
        * 如果需要在接口之外的地方发数据，需要自己保存客户ID和数据之间的对应关系！
        * 下边接口中的connId的意义与此相同
        */
        virtual int onData(int engineId, int connId, const char* data, size_t len) = 0;

        /** 客户端主动关闭了该连接接时候，通知上层处理 */
        virtual int onClose(int engineId, int connId) = 0;

        /** 返回值为0表示接受此连接，返回值为1表示拒绝接受 */
        virtual int onConnect(int engineId, int connId, const char* ip, int port) = 0;

        /** 
        * 发送时数据放在队列中,当心跳heartbeat时才实际发送,发送成功时将之前
        * 的ID确认给调用者,用于清除数据区指针.发送直接删除指针会引起错误.
        *
        * 此接口用于消息异步处理；id对应于ITPObject的Send操作的返回值.
        * 一个典型的应用场景是：ITPObject在发送数据的时候传入外部指针(或者资源句柄)，
        * 在此处得到通知后删除缓存起来的指针(或者释放资源句柄)
        *
        * @remarks 该接口在数据网络发送确认的时候被调用，表示数据已经被完整的从该层发送出去
        *          实现中可以调用其他接口实现一些特殊功能，如关闭连接，实现短连接等
        */
        virtual int onSendDataAck(int engineId, int connId, int id) = 0;

        /** 
        * @brief 发送数据失败时候的回调处理，用于发送失败和异常处理
        *
        * 类似于@c onSendDataAck，该接口用于通知上层发送数据的结果，当该接口得到调用时候，
        * 对应数据无法提交发送；上层应由此清理对应的异步消息数据，避免内存泄露
        *
        * @remarks 该接口仅仅在异常情况下调用以清理资源，调用点可能连接已经被释放，或者资源正被清理
        *  此接口实现时候，不要调用传输层的其他方法，否则会引起内部错误
        */
        virtual int onSendDataFail(int engineId, int connId, int id) = 0;
    };
}

#endif	//_ITPLISTENER_H_

