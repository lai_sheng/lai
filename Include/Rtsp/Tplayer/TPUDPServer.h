/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPUDPServer.h
* 文件标识：参见配置管理计划书
* 摘　　要：UDP服务器实现类
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/
#ifndef TPUDPSERVER
#define TPUDPSERVER

#include "Rtsp/Tplayer/ITPObject.h"
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
namespace CommonLib_VD
{

    class ITPListener;

    class TPUDPServer : public ITPObject
    {
    public:
        TPUDPServer(ITPListener* callback, int engineId = 0);

        virtual ~TPUDPServer();

        //<Methods>
    public:        
        /** 关闭一个客户端,其标识为id */ 
        virtual int CloseClient(int id);

        /** 关闭这个连接对象本身，包括所有的客户连接 */
        virtual int Close();

        /** 建立监听 */
        virtual int Listen(char* ip, int port);

        /** 连接标识为id的客户发送数据 */
        virtual int Send(int id, const char * pBuf, size_t iBufLen);
		virtual int SendRawStream(int id, const char *pBuf, size_t iBufLen) {return 0;}

        /** 保活 */
        virtual int Heartbeat();

        ///implementation
        virtual size_t GetBufferDataLen();
        virtual void DumpBufferdData();
    private:
        int sendInside(int id, const char *pBuf, size_t iBufLen);
        int closeInside();

        DataDeque    _dataList;    /**< 异步队列表 */

		//此接口新加，暂时用于server中的client子连接，传递套接字给connection
		virtual int ConnectPeer(int iSocket, unsigned int iIp, unsigned short usPort){return -1;}


    };

}

#endif

