#ifndef  _THREAD_REF_MUTEX_H
#define  _THREAD_REF_MUTEX_H

#include "Rtsp/Tplayer/ThreadMutex.h"

//Avoid annoy conflicts between DVR.H and windows.h
namespace CommonLib_VD
{
	class CThreadMutex : public CRefMutex
	{
	public:
		CThreadMutex (void);
		virtual ~CThreadMutex (void);

		virtual int lock(void);
		virtual int unlock(void);

	private:
		HANDLE  m_mutex;
	};
}

#endif
