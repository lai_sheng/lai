/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：ITPObject.h
* 文件标识：参见配置管理计划书
* 摘　　要：传输层接口抽象类
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/

/* 
* 变更一些类型定义，采取更GP的风格定义，便于测试变更
* 在性能要求高的场合，不同容器的性能区别可能要慎重考虑，经过测试来选择
* Modified by yanf,10811;070906
*/

#ifndef _ITPOBJECT_H_
#define _ITPOBJECT_H_

#include <assert.h>
#include <deque>
#include <map>
#include <vector>

#include "Rtsp/Tplayer/TPTypedef.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"

namespace CommonLib_VD
{
    class ITPListener;

    class ITPObject
    {
    public:
        ITPObject(ITPListener* instance, int engineId = 0);

        /** 两个指针队列的数据单元在这里有检查清理，其空间在实现操作中申请 */
        virtual ~ITPObject();

        virtual int CloseClient(int id);

        /** 在本地 @c ip 和 @c port上监听 */ 
        virtual int Listen(char* ip, int port);
                            
        /** 连接并建立传输链路 */
        virtual int Connect(const char* ip, int port);

        /** 给定双方地址的重载版本 @see Connect */
        virtual int Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort);

        /** 设定传输回调监听器对象 */
        void SetListener(ITPListener* listener);

        /** 
        * 将对应消息压入待发送缓冲，id在Server和Client模式下有不同意义
        * @param id Client模式下被忽略，Server模式则指定连接上来的对应客户id
        * @return
        *  - >=0  时为发送成功，返回内部序列号，该序列号需用于异步数据确认
        *  - -1   连接失败
        *  - -2   缓冲队列满
        */
        virtual int Send(int id, const char *pBuf, size_t iBufLen) = 0;

		/** 
		* 将对应数据直接发送，不入队列，id在Server和Client模式下有不同意义
		* @param id Client模式下被忽略，Server模式则指定连接上来的对应客户id
		* @return
		*  - >=0  时为发送成功，返回内部序列号，该序列号需用于异步数据确认
		*  - -1   连接失败
		*  - -2   缓冲队列满
		*/
		virtual int SendRawStream(int id, const char *pBuf, size_t iBufLen) = 0;

        /** 管理该传输对象 */
        virtual int Close(void);

        /** 心跳维护，调用者由此激活传输对象处理动作 */
        virtual int Heartbeat() = 0;

        /** 
        * 获取当前异步缓冲中堆积的待发数据总长度，可用于外部控制和调节
        */
        virtual size_t GetBufferDataLen() = 0;

        /** 获取内部定时器 */
        size_t GetSelectTimer()const{return _selectTimer;}
        /**
        * 用户设置接口,设置参数需在[0, 75000],默认为50；
        * 超出则无任何作用,并且返回false
        */
        bool SetSelectTimer(size_t milliSec);

        ///以下接口待进一步测试，用时需谨慎
        /** 设置底层套接字缓冲区大小，type表示是发送缓冲区还是接收缓冲区 */
        ERR_TYPE SetSocketBufferSize(TPType type, size_t size);

        /** 获取底层套接字缓冲区大小，type表示是发送缓冲区还是接收缓冲区 */
        size_t GetSocketBufferSize(TPType type)const;

        /** 
        * 设置传输层调用select时的超时值。如果不赋值则默认设sec=0;usec=10;
        *  如果设置为0则表示为轮循状态;sec最大允许为5，超范围返回越界错误
        */
        ERR_TYPE SetSelectTimeout(unsigned long sec, unsigned long usec);

        /** 
        * 设置传输层接收缓冲区大小，缓冲区由传输层自己维护，应用层仅指定大小
        * 如果超过64K，返回越界错误
        */
        ERR_TYPE SetRecvTPBuffSize(size_t size);			

        
        /**
        * 打印出异步缓冲消息队列的内容
        * 上层可以由此接口来获知打印传输层异步队列基本情况
        * 该接口一般应在回调事件中打印，以避免线程死锁；操作本身是线程不安全的
        */
        virtual void DumpBufferdData() = 0;
        
        ///////////////////////////////////////////////////////////////////////////////////
        ////指针异步释放是个难缠的问题，这个接口安全隐患太大，且保留这里
        /** 
        * 用于设置接收缓冲区，可以设定应用程序自己的接收缓冲区，传输层将数据直接接收在该区中。
        * 注意：传输的缓冲区应用层就不需要管理了，传输层对象被销毁时会自动释放该缓冲区。另外，传入的
        * 缓冲区需要是new出来的对象，不能使用静态变量，否则释放时会非法访问。
        */
        ERR_TYPE SetTPRecvBuffer(char* buff, int size);

		//此接口新加，暂时用于server中的client子连接，传递套接字给connection
		virtual int ConnectPeer(int iSocket, unsigned int iIp, unsigned short usPort) = 0;

    public:
        enum{BUF_SIZE = 32*1024};
        typedef std::vector<DataRow>       DataList; 
        typedef std::deque<DataRow>        DataDeque;
        typedef DataDeque::iterator         DataDequeIt;

        /// Map socket->DataList
        typedef std::map<int, DataList>     DataQueue;
        typedef DataQueue::iterator         DataQueueIt;
        typedef DataQueue::const_iterator   DataQueueCIt;

        /// Map id->clientInfo;TCP use socke as its id;UDP use internal id
        typedef std::map<int, ClientInfo>   ClientQueue;
        typedef ClientQueue::iterator       ClientQueueIt;
        typedef ClientQueue::const_iterator ClientQueueCIt;

    protected:
        unsigned int _ip;		   /**< 这里对应于对端通信实体地址，IP,PORT 都是网络字节序 */
        unsigned short _port;
        int	_socket;               /**< 本质上的socket封装 */

        unsigned int _localIp;     /**< 本地地址 */
        unsigned short _localPort; /**< 本地端口 */

        ITPListener* _listener;    /**< 通信层时间监听，用于处理接口定义的触发事件 */
        ClientQueue  _clients;     /**< Client表，仅用于Server模式 */

        int          _engineId;
        char*        _buffer;      /**< socket接收数据缓冲 */

        /** 以下两个接口用于产生异步数据ID标识，参考ITPListen关于onSendDataAck的说明 */
        int _sequence;
        int getSequence(void);

        /** 线程保护体，在派生类中得到初始化 */
        CMutex    _mutex;

        /** 传输层缓冲大小 */
        size_t         _recvBuffSize;
        size_t         _sendBufferSize;

    private:
        /** select timeout param */
        size_t         _selectTimer;       
        size_t         _tpRcvBufSize;
    };

    /** 获取数据队列缓冲数据长度 */
    size_t GetDataQueueLen(const ITPObject::DataQueue& queue);
    /** 获取单链接队列数据长度 */
    size_t GetDataListLen(const ITPObject::DataDeque& list);

    /** 打印数据队列 */
    void DumpDataQueue(const ITPObject::DataQueue& queue);
    void DumpDataList(const ITPObject::DataDeque& list);

    /** 数据清理 */
    void PurgeData(ITPObject::DataDeque& list);
}

#endif

