/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPUDPClient.h
* 文件标识：参见配置管理计划书
* 摘　　要：UDP客户端功能类
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/
#ifndef TPUDPCLIENT
#define TPUDPCLIENT

#include "Rtsp/Tplayer/ITPObject.h"

namespace CommonLib_VD
{
    class ITPListener;

    class TPUDPClient : public ITPObject
    {
    public:
        TPUDPClient(ITPListener *callback, int engineId = 0);
        virtual ~TPUDPClient();

    public:
        virtual int Close();

        virtual int Connect(const char* ip, int port);

        virtual int Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort);

        virtual int Send(int id, const char *pBuf, size_t iBufLen);
	virtual int SendRawStream(int id, const char *pBuf, size_t iBufLen);

        virtual int Heartbeat();

        ///implementation
        virtual size_t GetBufferDataLen();
        virtual void DumpBufferdData();
    private:
        int sendInside(int id, const char *pBuf, size_t iBufLen);
        int closeInside();

        DataDeque    _dataList;    /**< 异步队列表 */

		//此接口新加，暂时用于server中的client子连接，传递套接字给connection
		virtual int ConnectPeer(int iSocket, unsigned int iIp, unsigned short usPort){return -1;}

    };
}

#endif

