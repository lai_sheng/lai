/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPTypedef.h
* 文件标识：参见配置管理计划书
* 摘　　要：传输层预定义
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/
#ifndef _TPETYPEDEF_H_
#define _TPETYPEDEF_H_

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

#ifndef WIN32
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/tcp.h>		/* TCP_NODELAY */
#include <pthread.h>
#include	<arpa/inet.h>	/* inet(3) functions */
#define closesocket(S) close(S)
#endif

#include	<errno.h>
#include	<fcntl.h>		/* for nonblocking */

namespace CommonLib_VD
{
    #define REVBUF_LEN  1024
    #define KEEP_ALIVE_TIME     180  //3 Min

    #ifndef WIN32
    #define INVALID_SOCKET -1
    #endif

    #define MAX_ACCEPT_DLGS  20

    typedef enum
    {
        TP_ERROR_UNSUPPORT = -128,

        //扩展的错误码放在这里...
        TP_INVALID_PARAM,    /**< 参数错误 */
        TP_OUTOF_RANGE,      /**< 越界 */
        TP_MEM_ERROR,        /**< 内存错误 */

        TP_INVALID_ADDRESS = -10,
        TP_SELF = -11,      //for backward compliance

        //扩展请确保之前的错误码都不能为正数
        TP_ERROR_BASE = -1,
		TP_NODATA_TRANS = -2,
        TP_NORMAL_RET = 0
    }ERR_TYPE;

    typedef struct _client_info
    {
        unsigned int ip;        /**< Peer ip */
        unsigned short port;    /**< Peer port */
        int socket;             /**< corresponding socket */
        int online;             /**< 1:online 0:offline */
        unsigned int timemark;  /**< Timemarks */
    }ClientInfo;

    /*
    * 缓冲区行数据.send实际把数据保存在一个DataRow中然后放在队列中
    * data保存的是发送方的指针,数据在onSendDataAck中删除.
    */
    class DataRow
    {
    public:
        const char* data;       /**< 数据指针，这里引用上层提供的指针完成异步 */
        size_t      len;        /**< 负载数据长度 */

        int id;                 /**< 对应的id */
        int sequence;           /**< 序列号 */
    };

    /** 设置传输层缓冲大小的类型，参考ITPObject.h */
    typedef enum
    {
        TP_SEND = 1,
        TP_RECEIVE
    } TPType;
}


#endif
