///////////////////////////////////////////
/// 捕获接口类ICapture，向外提供统一的接口
/// Create 2008.11.20 YXY
///////////////////////////////////////////

#ifndef _ICAPTURE_H_
#define _ICAPTURE_H_

#include "System/Signals.h"
#include "System/Packet.h"
#include "System/Locales.h"
#include "APIs/Capture.h"
#include "System/Object.h"
#include "Configs/ConfigNetCamera.h"

class ICapture
{
public:
	//! 定义回调函数类型
	typedef TSignal3<int, uint, CPacket *>::SigProc SIG_DEV_CAP_BUFFER;
public:
	/// 创建捕获通道对象
	/// \param[in] iChannel 捕获设备通道号
	/// \return 返回捕获对象
	static ICapture* instance(int iChannel);

	/// 析构函数
	virtual ~ICapture();
	
	/// 得到通道数目
	static int GetChannels ();	
	
	//! 检测状态，如果出错则重启
	static void Synchronize();
	
	//! 设置制式
	virtual int SetVstd(uint dwStandard) = 0;

	//! 设置区域遮挡
	virtual int SetCover(VD_RECT *pRect, uint Color, int Enable, int index = 0) = 0;

	//! 设置时间
	virtual int SetTime(SYSTEM_TIME * pSysTime) = 0;

	//! 设置编码格式
	virtual int SetFormat( CAPTURE_FORMAT *pFormat, uint dwType = CHL_MAIN_T) = 0;	
	
	//! 获得底层的分辨率
	virtual int getImageSize(capture_size_t* pSize, uint dwType = CHL_MAIN_T) = 0;
	
	//! 获取编码格式
	virtual const CAPTURE_FORMAT * GetFormat(uint dwType = CHL_MAIN_T)const = 0;

	//! 设置I帧
	virtual int SetIFrame(uint dwStreamType = CHL_MAIN_T) = 0;

	//! 设置标题
	virtual int SetTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect,
							VD_PCSTR str = NULL, FONTSIZE fontsize = FONTSIZE_NORMAL) = 0;

	//! 启动捕获，开始取数据
	virtual int Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, 
							uint dwFlags,uint dwStreamType = CHL_MAIN_T) = 0;
	
	//! 启动捕获，开始取数据
	virtual int Start(uint dwStreamType = CHL_MAIN_T) = 0;

	//! 停止捕获，关闭取数据
	virtual int Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc,
							uint dwFlags, uint dwStreamType = CHL_MAIN_T) = 0;

	//! 停止捕获，关闭取数据
	virtual int Stop(uint dwStreamType = CHL_MAIN_T) = 0;
	
	//! 获得捕获状态
	virtual int GetState(uint dwStreamType = CHL_MAIN_T) = 0;

	virtual uint GetBufferLength() = 0;
	
	//! 设置I帧个数，实际上是预录秒数
	virtual void  SetIFrameNum(int number) = 0;
	
	//! 更新一下缓冲区，主要是按预录时间调整
	virtual int adjustPreBuffer() = 0;

	//! 供外部接口调用获得码流
	virtual uint GetBitRate(int dwType = CHL_MAIN_T) = 0;
	
	//! 计算码流
	virtual void CalculateBitRate() = 0;
	
	//! 计算码流1
	virtual void CalculateExBitRate() = 0;
	
	//! 捕获的线程执行体
	/// void ThreadProc();

	//! 捕获数据处理过程
	virtual void OnData(CPacket *pPacket,uint dwType) = 0;

	//! 录像数据处理
	virtual void OnRecData(CPacket ** pPacket) = 0;

	//! 取得捕获设备的音量
	virtual int GetVolume(int *pVolume) = 0;

	//! 设置待机状态 ,待机状态下全部码流不推流
	virtual void SetSuspend(int suspend){	return;};

	//!获取capture类型
	virtual ENUM_CHANNEL_TYPE GetCaptureType() { return ENUM_CHANNEL_TYPE_ANALOG;}

	//!获取码流packet是不是8k不定长的方式，主要用于IPC接入,目前公司所有产品的码流应该都是8k不定长的，
	//！暂时写死支持8k不定长,日后扩展，此值应该从配制中读取
	virtual ENUM_CHANNEL_STREAM_TYPE GetCaptureSreamType() { return ENUM_STREAM_TYPE_8K;}
};

typedef int LOGIC_CHN;
typedef int ANALOG_CHN;
typedef int DIGITAL_CHN;

typedef struct tagLOGIC_CHN_PAIR
{
	ANALOG_CHN m_analog_chn;   //!-1:表示无效
	DIGITAL_CHN m_digital_chn; //!-1:表示无效
}LOGIC_CHN_PAIR_T;

typedef std::map<LOGIC_CHN, LOGIC_CHN_PAIR_T> MAP_LOGIC_CHN;	///< 捕获对象

/// 负责完成码流统计等功能
class ICaptureManager
{
public:
	static ICaptureManager * instance();

	ICaptureManager();

	std::string& GetLogicChnStr();

	int GetLogicChnNum(); 	//!取逻辑通道数目
	int GetAnalogChnNum();	//!取模拟通道数目
	int GetDigitalChnNum();	//!取数字通道数目

	//!取逻辑通道号对应的物理通道号
	ANALOG_CHN GetAnalogChnNo( LOGIC_CHN chn );
	//!取逻辑通道对应的数字通道号
	DIGITAL_CHN GetDigitalChnNo( LOGIC_CHN chn );
	bool IsSupportAnalog( LOGIC_CHN chn );
	bool IsSupportDigital( LOGIC_CHN chn );

	void start();
	virtual ~ICaptureManager() {}

private:
	virtual void CreateProduct() = 0;
protected:
	int m_logic_chn_num;
	int m_analog_chn_num;
	int m_digital_chn_num;

	std::string m_logic_chn_str;

	//!保存逻辑通道和物理通道及数字通道的对应关系
	MAP_LOGIC_CHN m_logic_chn_map;
};

#endif//_ICAPTURE_H_

