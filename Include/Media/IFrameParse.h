/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              IFrameParse.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2011-1-14   8:40
**Modified:               
**Modify Reason: 
**Description:            
*********************************************************************/

#ifndef FRAME_PARSE_H__
#define FRAME_PARSE_H__

typedef enum
{
    MULTI_MEDIA_TYPE_VDH264 = 0,
	MULTI_MEDIA_TYPE_VIMIC,
}MULTI_MEDIA_TYPE;

class IFrameParse
{
public:
	IFrameParse();
	virtual ~IFrameParse();
	
	static IFrameParse* CreatNew(MULTI_MEDIA_TYPE type = MULTI_MEDIA_TYPE_VDH264);
	
	virtual void Init() = 0;
	virtual int AddPacket(char *sPtk, int len) = 0;//向缓冲中加入待解析的录像数据
	virtual int Parse(char *& dPtk, int &dPtkLen, bool lastNal = false) = 0;//分析录像数据,组帧
};

#endif

