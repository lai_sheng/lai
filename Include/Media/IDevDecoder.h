
#ifndef _DEV_IDECODER_H_
#define _DEV_IDECODER_H_

#include "APIs/Decoder.h"


/// \defgroup IDevDecoder接口调用方式
/// \code
///    ===========================================
///	
///		GetDecoderChannels
///	
///		IDevDecoder::instance(iChannel)->Start() 
///    ===========================================

/// 获得支持的数字解码通道
/// \return 成功返回支持的最大通道数，0表示不支持。
int GetDecoderChannels(void);

class IDevDecoder
{
public:

    /// 创建解码器通道对象
    /// \param[in] iChannel 通道对象,取值0~GetDecoderChannels()
    /// \return 通道对象
    static IDevDecoder* instance(int iChannel);

    /// 析构函数
    virtual ~IDevDecoder();

    /// 开启数字解码器
    /// \return 成功返回0　失败返回-1
    virtual int Start(void) = 0;

    /// 关闭数字解码器
    /// \return 成功返回0　失败返回-1
    virtual int Stop(void) = 0;

    /// 获取解码的分辨率，注意：此函数的返回和当前分割有关
    /// \return 分辨率，参照capture_size_t。
    virtual int GetImageSize(void) = 0;

    /// 设置解码策略
    /// \param[in] iPolicy 策略
    /// \		   目前一共有5档(-2 -1 0 1 2),值越大表示越流畅但延迟越大
    /// \		   -2表示实时性最好，2表示流畅性最好，0默认
    /// \return　成功返回0　失败返回-1
    virtual int SetPolicy(int iPolicy) = 0;

    /// 往解码器送数据,该接口采用非阻塞超时机制
    /// \param[in] pvData 码流数据
    /// \param[in] iLength 码流长度
    /// \param[in] iTimeOut 超时时间,默认为5ms
    /// \return 成功返回0　失败返回-1
    virtual int PutBuffer(void *pvData, int iLength, int iTimeOut = 5) = 0;

    virtual int PutMultiBuf(DECODER_BUF_GROUP* pDecoderBuf, int iTimeOut) = 0;
    /// 获取发送的解码器数据长度，获取后长度复位
    /// \param[in] iTimeOut 超时时间,默认为5ms
    /// \return 成功返回长度　失败返回0
    virtual int GetPutLength() = 0;

    /// 设置数字通道对应的预览通道号
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \param[in] targetDisplayChannel　显示通道号，取值0~15
    /// \retval 0  成功。
    /// \retval !=0  失败。
    virtual int MapChannel(int channel, int targetDisplayChannel) = 0;

    /// 取消数字通道和显示通道的对应关系
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \retval 0  成功。
    /// \retval !=0  失败。
    virtual int UnmapChannel(int channel) = 0;

    /// 返回数字通道对应的显示通道，如果数字通道未被map，或者其它错误，返回-1
    /// 否则返回显示通道号(>=0)
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \return 成功返回显示通道号，失败返回-1
    virtual int GetMappedChannel(int channel) = 0;

    /// 清空解码缓冲，重新搜寻I帧。上层丢数据时会调用
    /// 
    /// \param [in] channel 通道号。
    /// \retval 0  清空成功
    /// \retval <0  清空失败
    virtual int Flush(int iChannel) = 0;
};

#endif//_DEV_IDECODER_H_

