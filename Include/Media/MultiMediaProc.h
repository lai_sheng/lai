/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              MultiMediaProc.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2011-1-14   8:40
**Modified:               
**Modify Reason: 
**Description:            测试HIS解码器是否能解码第和第三方码流
*********************************************************************/

#ifndef MULTI_MEDIA_PROC_H__
#define MULTI_MEDIA_PROC_H__

//#define MULTI_MEDIA_PROC 1

#include <MultiTask/Thread.h>
#include <MultiTask/Mutex.h>
#include <MultiTask/Guard.h>
#include <System/Object.h>
#include <System/Signals.h>
#include <APIs/Capture.h>
#include <System/Packet.h>
#include <Media/IFrameParse.h>

#include <stdio.h>

typedef TSignal3<int, uint, CPacket *> TMultiMedia;
typedef TMultiMedia::SigProc TMultiMedia_PROC;

class CMultiMedia : public CThread
{
public:
	typedef struct
	{
	    bool bFistIFrame;
		unsigned int iFPS;
		unsigned int Resolution ;	
		unsigned int RecTimeOff;        //记录录像文件的偏移时间，即从开始回放到当前，录像一共偏了多少时间，豪秒级。
		unsigned int BeginTime;
		unsigned int Speed;              // > 1快放，< 0,慢放，= 0，暂停 = 1正常播放
		int SleepTime;
	}MultiMediaInfo;
	
	#define MULTI_MEDIA_BUF_LEN (1024*64)
		
public:
	PATTERN_SINGLETON_DECLARE(CMultiMedia);

	CMultiMedia();
	virtual ~CMultiMedia();

	bool Start();
    bool Stop();
	
	bool SetMediaPara(char* fileName, MULTI_MEDIA_TYPE type);
	int Attach(CObject * pObj, TMultiMedia_PROC pProc);
	int Detach(CObject * pObj, TMultiMedia_PROC pProc);

	virtual void ThreadProc();
	
private:
	IFrameParse* m_Parser;
	MULTI_MEDIA_TYPE m_MediaType;

	FILE* m_Fp;
	char m_FileName[128];
	MultiMediaInfo m_MediaInfo;

	TMultiMedia* m_Sig;
	int m_SigAttachNum;
};

#define g_MultiMedia (*CMultiMedia::instance())

#endif
