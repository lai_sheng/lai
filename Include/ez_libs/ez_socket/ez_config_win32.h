/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * ez_config_win32.h - _explain_
 *
 * Copyright (C) 2005 DAHUA Technologies, All Rights Reserved.
 *
 * $Id: ez_config_win32.h 15036 2008-03-12 02:08:36Z zhang_yaozhen $
 *
 *  Explain:
 *     -explain-
 *
 *  Update:
 *     2005-04-02 17:19:24 WuJunjie 10221 Create
 *		wjj 2005-04-02 15:16:48
 *			Create
 * 
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#ifndef __EZ_CONFIG_WIN32
#define __EZ_CONFIG_WIN32

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+- function support +-+-+-+-+-+-+-+-+-+-+-+-*/

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+ lib +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

	#pragma comment(lib, "ws2_32.lib")
	#pragma comment(lib, "mswsock.lib")
	#pragma comment(lib, "wsock32.lib")

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

typedef int ez_socket_t;
typedef int ez_socklen_t;
typedef unsigned short ez_port_t;
typedef int ez_ssize_t;
typedef unsigned short ez_u16_t;
typedef unsigned int ez_u32_t;
typedef long ez_pid_t;
typedef unsigned short ez_mode_t;
typedef unsigned int ez_sigset_t;

typedef char* caddr_t;

#define ez_get_errno()  WSAGetLastError()
#define ez_set_errno(x) WSASetLastError((x))
#    define EAFNOSUPPORT            WSAEAFNOSUPPORT
#	 define ECONNRESET				WSAECONNRESET
#define EWOULDBLOCK	WSAEWOULDBLOCK

/*-+-+-+-+-+-+-+-+-+-+- signal +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#if !defined (SIGALRM)
#  define SIGALRM 0
#endif /* SIGALRM */

#define 	 LOG_INFO 1
#define 	 LOG_ERR 1

#define 	 MAXLINE 1024
#define 	 BUFFSIZE 1024

#define 	 __true 1
#define 	 __false 0

static int 	 __socket_initialized	= __false;

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#define EZ_STDPREFIX WINAPI
typedef HANDLE					ez_pthread_t;
typedef LPSECURITY_ATTRIBUTES	ez_pthread_attr_t;
typedef DWORD					ez_threadfunc_t;
typedef LPVOID					ez_threadparam_t;	
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#define ez_EINPROGRESS WSAEINPROGRESS
//通用函数定义
#define ez_snprintf _snprintf

#endif /* __EZ_CONFIG_WIN32 */
