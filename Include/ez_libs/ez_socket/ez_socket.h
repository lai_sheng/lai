/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * ez_socket.h - socket API
 *
 * Copyright (C) 2005 DAHUA Technologies, All Rights Reserved.
 *
 * $Id: ez_socket.h 16364 2008-04-25 03:59:51Z guan_shiyong $
 *
 * socket api, fit for the past and the future.
 * Platform:
 * 		linux+gcc / win32+vc6 / cygwin
 *  Explain:
 *     -explain-
 *
 *  Update:
 *     2005-04-02 17:19:24 WuJunjie 10221 Create
 *		wjj 2005-04-02 15:16:48
 *			Create
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#ifndef	__EZ_SOCKET_H
#define	__EZ_SOCKET_H

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/* configuration options for current OS */
#include	"ez_config.h"

#ifdef  __cplusplus
extern "C" {
#endif

/* debug and test parameter */
#include	"ez_appdef.h" 

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/* Following shortens all the typecasts of pointer arguments: */
#define	SA	struct sockaddr

#ifdef WIN32
static signed char __initialized = 0;
#endif

#ifndef	HAVE_SYSLOG_H
#define syslog(x, y)
#endif

int		ez_snprintf(char *buf, size_t size, const char *fmt, ...);
#define	ez_bzero(ptr,n)		memset(ptr, 0, n)

#define	__ez_min(a,b)	((a) < (b) ? (a) : (b))
#define	__ez_max(a,b)	((a) > (b) ? (a) : (b))

char * get_date_time_string();
char * get_date_time_string_compress();


/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

ez_socket_t	 ez_tcp_server(const ez_port_t, const int);
ez_socket_t	 ez_tcp_connect(const char *, const ez_port_t);

//!超时返回，time-ms
ez_socket_t	 ez_tcp_connect_time(const char *hostip, const ez_port_t port, unsigned int time );
ez_socket_t	 ez_tcp_client(char *, char *);

ez_socket_t	 ez_udp_server(const ez_port_t);
ez_socket_t	 ez_udp_connect(const char * hostip, const ez_port_t port);
ez_socket_t	 ez_udp_client(const char * hostip, const ez_port_t port, SA *saptr, ez_socklen_t *lenp);

int			 ez_mcast_set_loop(ez_socket_t, unsigned char);

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+- raw socket api-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

ez_socket_t	 ez_ip_snif_socket(const char *);

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+basic socket api-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

ez_socket_t	 ez_socket(int, int, int);
int		 ez_bind(ez_socket_t, const SA *, ez_socklen_t);
int		 ez_listen(ez_socket_t, int);
ez_socket_t	 ez_accept(ez_socket_t, SA *, ez_socklen_t *);
int		 ez_connect(ez_socket_t, const SA *, ez_socklen_t);

int		 ez_socket_init(void);
int		 ez_socket_cleanup(void);
int		 ez_close(ez_socket_t fd);
int		 ez_close_socket(ez_socket_t *fd);

struct hostent * ez_gethostbyname(const char * name);

typedef struct _ez_host_ip
{
	int num; 		//!有郊的ip个数
	struct sockaddr_in sa[4];
}ez_host_ip;
int ez_gethostbyname_s(const char * name, ez_host_ip* host_ip );
int		 ez_getpeername(ez_socket_t, SA *, ez_socklen_t *);
int		 ez_getsockname(ez_socket_t, SA *, ez_socklen_t *);
int		 ez_gethostname(char *name, size_t len);

int		 ez_getsockopt(ez_socket_t, int, int, void *, ez_socklen_t *);
int		 ez_setsockopt(ez_socket_t, int, int, const void *, ez_socklen_t);
int		 ez_shutdown(ez_socket_t, int);
int		 ez_select(int, fd_set *, fd_set *, fd_set *, struct timeval *);

en_bool	 ez_set_nodelay(ez_socket_t s);
en_bool	 ez_set_nonblock(en_bool bNb, ez_socket_t s);

/*-+-+-+-+-+-+-+-+-+-+-+-+-+- socket io   -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

int ez_writen(ez_socket_t fd, const void *vptr, size_t n);
int ez_writeton( ez_socket_t fd, const void *vptr, size_t n, const SA *saRemote, int struct_size);

//! 返回值大于0，表示实际的收到的字节数，小于0，表示出错， 等于0，表示超时
int ez_recv( ez_socket_t fd, const char *vptr, const size_t buf_len , int time_out );

/*-+-+-+-+-+-+-+-+-+-+-+-+-+- socket addr -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

int		 ez_inet_aton(const char *cp, struct in_addr *ap);
int		 ez_inet_pton(int family, const char *strptr, void *addrptr);
const char *ez_inet_ntop(int af, const void *src, char *dst, ez_socklen_t cnt);

int		 ez_set_address(char *hname,
					 char *sname,
					 struct sockaddr_in *sap,
					 char *protocol );

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
int ez_pthread_create(ez_pthread_t *	phThread,
                      ez_pthread_attr_t	thread_attr,
                      ez_threadfunc_t (WINAPI *pStartAddress)(ez_threadparam_t),
                      ez_threadparam_t	pParameter);
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#if (defined(EZ_ERROR_FUNCTION) && (EZ_ERROR_FUNCTION != 0))
	void	 ez_err_dump(const char *, ...);
	void	 ez_err_msg(const char *, ...);
	void	 ez_err_quit(const char *, ...);
	void	 ez_err_ret(const char *, ...);
	void	 ez_err_sys(const char *, ...);
#else
	#ifdef PSOS
		#define ez_err_sys myprintf
	#else
		#define ez_err_sys printf
	#endif
/*	#define ez_err_ret(format, a...) do{ printf(format, ## a); } while(0)
	#define ez_err_sys(format, a...) do{ printf(format, ## a); exit(1); } while(0)
	#define ez_err_dump(format, a...) do{ printf(format, ## a); exit(1); } while(0)
	#define ez_err_msg(format, a...) do{ printf(format, ## a); } while(0)
	#define ez_err_quit(format, a...) do{ printf(format, ## a); exit(1); } while(0)*/
#endif	/* EZ_ERROR_FUNCTION */ 

//#define __trip __ez_pritnf("-W-%d::%s(%d)\n", (int)time(NULL), __FILE__, __LINE__);
//#define __fline __ez_pritnf("%s(%d)--", __FILE__, __LINE__);

#ifdef  __cplusplus
}
#endif

#endif	/* ADSOCKET_H */

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- end -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

