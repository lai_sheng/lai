/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.in by autoheader.  */

/* Define to 1 if you have the <assert.h> header file. */
/* #undef HAVE_ASSERT_H */
#define HAVE_ASSERT_H 1

/* Define to 1 if you have the <ctype.h> header file. */
/* #undef HAVE_CTYPE_H */
#define HAVE_CTYPE_H 1

/* Define to 1 if you have <dict/dict.h> header file */
/* #undef HAVE_DICT_DICT_H */

/* Define to 1 if you have the <dlfcn.h> header file. */
/* #undef HAVE_DLFCN_H */

/* Define to 1 if you have the <fcntl.h> header file. */
/* #undef HAVE_FCNTL_H */
#define HAVE_FCNTL_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
/* #undef HAVE_INTTYPES_H */
#define HAVE_INTTYPES_H 1
/* Defined if you have lrand48 */
/* #undef HAVE_LRAND48 */
#define HAVE_LRAND48 1
/* Define to 1 if you have the <malloc.h> header file. */
/* #undef HAVE_MALLOC_H */
#define HAVE_MALLOC_H  1
/* Define to 1 if you have the <memory.h> header file. */
/* #undef HAVE_MEMORY_H */
#define HAVE_MEMORY_H 1

/* Define if you have POSIX threads libraries and header files. */
/* #undef HAVE_PTHREAD */
#define HAVE_PTHREAD 1

/* Define to 1 if you have the <semaphore.h> header file. */
/* #undef HAVE_SEMAPHORE_H */
#define HAVE_SEMAPHORE_H 1

/* Define to 1 if you have the <signal.h> header file. */
/* #undef HAVE_SIGNAL_H */
#define HAVE_SIGNAL_H 1
/* Define to 1 if you have the <stdarg.h> header file. */
/* #undef HAVE_STDARG_H */
#define HAVE_STDARG_H 1
/* Define to 1 if you have the <stdint.h> header file. */
/* #undef HAVE_STDINT_H */
#define HAVE_STDINT_H 1
/* Define to 1 if you have the <stdio.h> header file. */
/* #undef HAVE_STDIO_H */
#define HAVE_STDIO_H 1
/* Define to 1 if you have the <stdlib.h> header file. */
/* #undef HAVE_STDLIB_H */
#define HAVE_STDLIB_H 1
/* Define to 1 if you have the <strings.h> header file. */
/* #undef HAVE_STRINGS_H */
#define HAVE_STRINGS_H  1
/* Define to 1 if you have the <string.h> header file. */
/* #undef HAVE_STRING_H */
#define HAVE_STRING_H  1
/* Define to 1 if the system has the type `struct timeval'. */
/* #undef HAVE_STRUCT_TIMEVAL */
#define HAVE_STRUCT_TIMEVAL 1

/* Define to 1 if you have the <syslog.h> header file. */
/* #undef HAVE_SYSLOG_H */

/* Define to 1 if you have the <sys/select.h> header file. */
/* #undef HAVE_SYS_SELECT_H */
#define HAVE_SYS_SELECT_H 1

/* Define to 1 if you have the <sys/sem.h> header file. */
/* #undef HAVE_SYS_SEM_H */

/* Define to 1 if you have the <sys/signal.h> header file. */
/* #undef HAVE_SYS_SIGNAL_H */
#define HAVE_SYS_SIGNAL_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
/* #undef HAVE_SYS_STAT_H */
#define HAVE_SYS_STAT_H 1
/* Define to 1 if you have the <sys/time.h> header file. */
/* #undef HAVE_SYS_TIME_H */
#define HAVE_SYS_TIME_H 1
/* Define to 1 if you have the <sys/types.h> header file. */
/* #undef HAVE_SYS_TYPES_H */
#define HAVE_SYS_TYPES_H 1
/* Define to 1 if you have the <sys/unistd.h> header file. */
/* #undef HAVE_SYS_UNISTD_H */
#define HAVE_SYS_UNISTD_H 1

/* Define to 1 if you have the <time.h> header file. */
/* #undef HAVE_TIME_H */
#define HAVE_TIME_H 1
/* Define to 1 if you have the <unistd.h> header file. */
/* #undef HAVE_UNISTD_H */
#define HAVE_UNISTD_H 1
/* Define to 1 if you have the <varargs.h> header file. */
/* #undef HAVE_VARARGS_H */
#define HAVE_VARARGS_H 1
/* Name of package */
/* #undef PACKAGE */

/* Define to the address where bug reports for this package should be sent. */
/* #undef PACKAGE_BUGREPORT */

/* Define to the full name of this package. */
/* #undef PACKAGE_NAME */

/* Define to the full name and version of this package. */
/* #undef PACKAGE_STRING */

/* Define to the one symbol short name of this package. */
/* #undef PACKAGE_TARNAME */

/* Define to the version of this package. */
/* #undef PACKAGE_VERSION */

/* Define to the necessary symbol if this constant uses a non-standard name on
   your system. */
/* #undef PTHREAD_CREATE_JOINABLE */
//#define PTHREAD_CREATE_JOINABLE  1
/* Define to 1 if you have the ANSI C header files. */
/* #undef STDC_HEADERS */
#define STDC_HEADERS  1
/* Version number of package */
/* #undef VERSION */
