// ParseData.h: interface for the CParseData class.
//
//////////////////////////////////////////////////////////////////////


/****************************************************************************

*----------*
*   Description:
协议数据解析类
*-------------------------------------------------------------------------------
*  Created By: 刘俊                            Date: 2007/08/15
*  Copyright (C) 2007      Corporation
*  All Rights Reserved
*
*-------------------------------------------------------------------------------
*  Revisions:
*
*******************************************************************************/


#ifndef _PARSEDATA_H
#define _PARSEDATA_H

#include <iostream>
#include <map>
#include <time.h>
#include <assert.h>
#include <string.h>

#define  BASEDATALENGTH  32
#define  BROARDSEARCHVER "VISIONDIGI.SEARCH.V1"
#define  BROARDSEARCHVER_2 "VISIONDIGI.SEARCH.V2"



typedef enum
{
	SDO_Wq,
	SDO_ChaoLiu,
	SDO_ZKTeco
}SEARCH_DEV_OEM_E;

typedef struct  _asclient
{
	SEARCH_DEV_OEM_E oem; //!标识厂商
	char m_ip[20];
	int  m_port;
	char m_mac[19];
	char uuid[64];
	char m_hostname[16];
	char _username[32];  //用户名
 	char _password[28];  //用户密码
 	short _device_type; 
	short ver_id ;
	
}ASIClient;

typedef struct _config_net_autosearch
{
	unsigned char Version[8]; // 8字节的版本信息
	char HostName[16]; // 主机名
	unsigned long HostIP; // IP 地址
	unsigned long Submask; // 子网掩码
	unsigned long GateWayIP; // 网关 IP
	unsigned long DNSIP; // DNS IP

	// 外部接口
	unsigned long AlarmServerIP; // 报警中心IP
	unsigned short  AlarmServerPort; // 报警中心端口
	unsigned long SMTPServerIP; // SMTP server IP
	unsigned short  SMTPServerPort; // SMTP server port
	unsigned long LogServerIP; // Log server IP
	unsigned short  LogServerPort; // Log server port

	// 本机服务端口
	unsigned short  HttpPort; // HTTP服务端口号
	unsigned short  HttpsPort; // HTTPS服务端口号
	unsigned short  TCPPort; // TCP 侦听端口
	unsigned short  TCPMaxConn; // TCP 最大连接数
	unsigned short  SSLPort; // SSL 侦听端口
	unsigned short  UDPPort; // UDP 侦听端口
	unsigned long   McastIP; // 组播IP
	unsigned short  McastPort; // 组播端口

	// 其他
	unsigned char  MonMode; // 监视协议 0-TCP, 1-UDP, 2-MCAST //待确定1-TCP
	unsigned char  PlayMode; // 回放协议 0-TCP, 1-UDP, 2-MCAST//待确定1-TCP
	unsigned char  AlmSvrStat; // 报警中心状态 0-关闭, 1-打开
}CONFIG_NET_AUTOSEARCH;

//template <>
//CONFIG_NET_AUTOSEARCH FlipBytesEx(CONFIG_NET_AUTOSEARCH data);
typedef struct tagPRODUCTINFO_V2	 
{ 
  	unsigned short vender_id;           //供应商id 
  	unsigned short device_id;           //设备id 
 	unsigned char device_name[20];  	//设备名
 	unsigned char _username[32];  		//用户名
 	unsigned char _password[28];  		//用户密码
  	unsigned short device_type;         //设备类型
  	unsigned short product_id;          //产品id 
  	unsigned short product_version;     //产品版本   
  	unsigned short mac_high;            //the high-order 16 bits of the MAC 
  	unsigned int   mac_low; 			//the low-order 32 bits of the MAC  
    unsigned int   random_id;		    //修改MAC时的唯一标识号		
    unsigned short chn_num;     		//gong 设备通道数  	
  	unsigned char  reserved[42];     	//gong 保留位  以后用 :原reserved[44]
}PRODUCTINFO_V2;

typedef struct tagPRODUCTINFO 
{ 
	unsigned short vender_id;           //供应商id 
	unsigned short device_id;           //设备id 
	unsigned char  device_name[80];     //设备名
	unsigned short device_type;         //设备类型
	unsigned short product_id;          //产品id 
	unsigned short product_version;     //产品版本   
	unsigned short mac_high;            //the high-order 16 bits of the MAC 
	unsigned int mac_low;               //the low-order 32 bits of the MAC 
    unsigned int random_id;		   //修改MAC时的唯一标识号		
}PRODUCTINFO;

typedef struct tagINETCONFIG 
{ 
	unsigned int netConfigurated;       //网络以配置，设备不能被查询,这一项是保留位，暂不使用
	unsigned int ip_addr;               //本机IP地址 
	unsigned int inet_mask;             //子网掩码 
	unsigned int gateway;               //网关 
	unsigned int major_dns;             //主DNS 
	unsigned int minor_dns;             //辅助DNS 
	unsigned short  CServer_port;       //配置端口,对应我们的TCP端口
	unsigned short  AV_port;            //数据端口，对应我们的UDP端口
	unsigned short  HTTP_port;          //WEB端口
	//unsigned char   Reserved[10];       //保留字段，作字节对齐用
	unsigned char   Reserved[2];       //保留字段，作字节对齐用
	unsigned long long SerialNumber;       //产品序列号，由mac地址逆向转换所得
}INETCONFIG;

class UUID_DATA
{
public:
	UUID_DATA( char * data, int len )
	{
		if( len > 0 )
		{
			pData = new char[len];
			memcpy(pData, data, len);
			nLength = len;
		}
		else
		{
			pData = NULL;
			nLength = 0;
		}
	}
	UUID_DATA( const UUID_DATA& rh)
	{
		if( rh.nLength > 0 )
		{
			nLength = rh.nLength;
			pData = new char[rh.nLength];
			memcpy(pData, rh.pData, nLength);
		}
		else
		{
			nLength = 0;
			pData = NULL;
		}
	}
	~UUID_DATA()
	{
		if( pData != NULL )
		{
			delete []pData;
		}
	}
	UUID_DATA& operator=( UUID_DATA& rh)
	{
		if( rh.nLength > 0 )
		{
			nLength = rh.nLength;
			pData = new char[rh.nLength];
			memcpy(pData, rh.pData, nLength);
		}
		else
		{
			nLength = 0;
			pData = NULL;
		}
		return (*this);
	}
	bool operator<(const UUID_DATA& rh) const
	{
		if( nLength == rh.nLength)
		{
			assert( pData!= NULL );
			assert( rh.pData != NULL );
			if( memcmp(pData, rh.pData, nLength) < 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return nLength > rh.nLength;
	}
	
	char * GetPtr() const
	{
		return pData;
	}

	int GetLength() const
	{
		return nLength;
	}

	void dump() const
	{
		if(nLength > 0)
		{
			std::cout << "UUID_DATA:uuid: " << pData << std::endl;
		}
		else
		{
			std::cout << "UUID_DATA:uuid: (NULL)"  << std::endl;
		}
	}
private:
	char* pData;
	int   nLength;
};

typedef struct
{
	//	unsigned char Version[8]; // 8字节的版本信息
	char HostName[16]; // 主机名
	unsigned long HostIP; // IP 地址
	unsigned long Submask; // 子网掩码
	unsigned long GateWayIP; // 网关 IP
	unsigned long DNSIP; // DNS IP
	unsigned long DNSIP2;
	// 本机服务端口
	unsigned short  TCPPort; // TCP 侦听端口
	unsigned short  UDPPort; // TCP 侦听端口
	unsigned short  HttpPort; // TCP 侦听端口	
	unsigned short _device_type; 
	char szIP[64];   //以字符的形式保存ip
	char szMacAdd[19];
	char _user_name[32];
	char _password[32];
	
}NET_INFO;


typedef struct
{
	char HostName[16]; // 主机名
	char* HostIP; // IP 地址
	char* Submask; // 子网掩码
	char* GateWayIP; // 网关 IP
	char* Macaddress; //MAC地址

}NET_MULNETCARD;

typedef std::map<UUID_DATA, NET_INFO> MAP_NETINFO;

class CParseData  
{
public:
	enum PARSETYPE
	{
		PARSETYPE_NOTHING = 0X0,

		PARSETYPE_BROADCAST,
	};
public:
	CParseData();
	virtual ~CParseData();


	int ParseData( const unsigned char* pData , int nBufLen , char* pOutData ,PARSETYPE nType);

	char* m_pTempData;  //临时存储数据,目前用来存放uuid
	int   m_nTempDataLength;  //数据长度

	int m_nFlag; //存储设备登陆返回值
   	int m_count;
	int m_nFailedReason; //登陆失败返回的原因

	int m_nIPResult;//IP修改返回的结果

	
};

#endif // !defined(AFX_PARSEDATA_H__E475C298_05A0_4ACD_9A3E_ABC47BD0EA93__INCLUDED_)
