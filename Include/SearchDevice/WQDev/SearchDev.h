#ifndef _SEARCHEDV_H
#define  _SEARCHEDV_H

#include <time.h>
#include <MultiTask/Mutex.h>
#include <MultiTask/Semaphore.h>
#include <System/Object.h>
#include "SearchDevice/ParseData.h"

class CBroadcastSocket;

class CSearchDev : public CObject
{
public:	
	PATTERN_SINGLETON_DECLARE(CSearchDev);
	CSearchDev();
	virtual ~CSearchDev();
	
	long		m_lLoginId;
	
	int Clear();
	//!发送搜索命令
	int SendSearch( );
	//void SetTimeOut(int time_seconds);
	//void WaitTime();

	//!登录设备
	int SendLogin( const char* szUserName , const char* szPsw , int nIndex );

	//!修改设备信息
	void ModifyIp(const char * dwHostIp , const char * dwSubmask ,const char * dwGateWayIP, int nIndex );
	
        void ModifyIpExt( NET_INFO* pClient, int index);
		
	//!获取广播收到的ip地址数目
	int GetRcvNum();

	//!index 从0开始
	int GetDevInfo( ASIClient &client ,int index );
	int GetDevInfoExt( NET_INFO & client ,int index );
	
protected:

	MAP_NETINFO			m_mapNetInfo;
	// !插入相应的信息到m_mapNetInfo中去,如果uuid不存在，就
	// !创建，如果已存在就修改其内的值和netinfo一样
	void Insert2List(const UUID_DATA& uuid, const NET_INFO& netinfo);
	MAP_NETINFO::iterator GetIteratorFromIndex( int nIndex );

	CParseData m_parseData;
	CMutex m_mutex;

	void Search();

	// !对接受到的数据进行解析分离
	void DoWithData( unsigned char* pData , int nBuflen  );
	
	//!对搜到的信息进行解析
	//void DecodeCString(CString source,  CStringArray& dest, char division[3]);
	
	//!发送修改设备信息命令
	void SendModifyIP( CONFIG_NET_AUTOSEARCH configNet , const char* pUUId, int nLength );  //发送修改ip的命令
	
private:
	CSemaphore m_Sem;
	CBroadcastSocket* m_Broadcast;
	//VD_UINT32 m_bTimeOut;

};
 #define g_Search (*CSearchDev::instance())
#endif //
