
/*
潮流ipc搜索
Device search schematic diagram
Send broadcast message (BroadParam)to UDP port 6789
Send receive message(RecvParam) to client 
*/

#ifndef CHAOLIUDEV_H
#define CHAOLIUDEV_H

#include "SearchDevice/SearchDevGeneral.h"

#define	MSG_STRING	"grandstream"
#define	NET_PARAM		1
#define	BROAD_PARAM	2
#define DEVICE_DVS	"DVS"
#define DEVICE_IPCAM	"IPCAMERA"
#define DEVICE_DECODER	"DECODER"

struct RecvParam
{	
	u_char	message[12];	// it’s value must be MSG_STRING
	u_char	msg_type;	// message tag, it’s value must be 1
	u_char	dev_type[10];	// type of the device(see DEVICE_DVS, DEVICE_IPCAM, DEVICE_DECODER )
	u_char	mac_value[7];		// mac of the device
	u_char	netmask_value[30];		// netmask of the device
	u_char	ip_value[30];		// IP of the device
	u_char	dev_name[64];	// name of the device
	u_int	web_port;		//port of web serveice
	u_int	rtsp_port;			//port of  RTSP service
	u_char	uc_reserve[10];	// if the dev_type is DEVICE_DVS, uc_reserve[0] is channel count
	u_char	devmodel[32];		//model string of the device
};

struct  BroadParam
{
	u_char message[12];	///  it’s value must be MSG_STRING
	u_char msg_type;	// message flag , value is 2
	u_char ipaddress[30];	// your client ip address
};

class CBroadcastSocket;
class CChaoLiuSearchDev : public CSearchDevGeneral
{
	typedef std::map<std::string, ASIClient> MAP_Client;
public:
	CChaoLiuSearchDev();
	virtual ~CChaoLiuSearchDev();

	//!发送搜索命令
	virtual int SendSearch( );
	// !对接受到的数据进行解析分离
	virtual void DoRecvData( unsigned char* pData , int nBuflen  );

protected:
};



#endif
