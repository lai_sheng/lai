
#ifndef _SEARCHDEVGENERAL_H_
#define _SEARCHDEVGENERAL_H_

#include <map>
#include <MultiTask/Mutex.h>
#include "System/Object.h"
#include "SearchDevice/ParseData.h"

typedef unsigned char u_char;
typedef unsigned int u_int;

class CBroadcastSocket;

class CSearchDevGeneral : public CObject
{
public:
	//!发送搜索命令
	virtual int SendSearch( ) = 0;
	//!处理收到的数据
	virtual void DoRecvData( unsigned char* pData , int nBuflen ) = 0;

public:
	CSearchDevGeneral(int send_port, int recv_port);
	virtual ~CSearchDevGeneral();

	int Clear();
	//!获取广播收到的ip地址数目
	int GetRcvNum();
	//!index 从0开始
	int GetDevInfo( ASIClient &client ,int index );

protected:
	typedef std::map<std::string, ASIClient> MAP_Client;
	MAP_Client::iterator GetIteratorFromIndex( int nIndex );
	// !对接受到的数据进行解析分离
	void DoWithData( unsigned char* pData , int nBuflen  );

	CBroadcastSocket* m_Broadcast;
	MAP_Client m_CliMap;
	CMutex m_mutex;
};

#endif