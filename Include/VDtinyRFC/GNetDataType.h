/************************************************************************
**GNetWork Modularization
*************************************************************************
**		(C)Copyright 2008, Zhejiang Dahua Technology Stock Co.Ltd.
**							All Rights Reserved
**File Name:	GNetDataType.h
**Version:		Version 0.01
**Purpose:		定义常用/通用数据类型，定义常用/通用宏
**Author:		Cx.Han
**Created:		2008-2-26	9:45
**Modified:		None
**Modify Reason:None
*************************************************************************/
#pragma once

#ifndef __GNET_DATA_TYPE_H__
#define	__GNET_DATA_TYPE_H__

#ifdef  __cplusplus
extern "C" {
#endif

typedef	unsigned long			GNET_DWORD;			//无符号双字
typedef signed long				GNET_SDWORD;		//有符号双字
typedef	unsigned short			GNET_WORD;			//无符号字
typedef signed short			GNET_SWORD;			//有符号字
typedef unsigned char			GNET_BYTE;			//无符号字节
typedef	signed char				GNET_SBYTE;			//有符号字节
typedef	unsigned int			GNET_UINT;			//无符号INT
typedef signed int				GNET_INT;			//有符号INT
typedef signed int				GNET_SIZE_T;		//数据尺寸大小类型
typedef void					GNET_VOID;			//VOID型

typedef char*					GNET_PSTR;			//指向字符串的指针
typedef const char*				GNET_PCSTR;			//指向常量的字符串指针，用于限制函数参数
typedef GNET_WORD				GNET_PORT16;		//16位端口数据类型
#ifndef BOOL
typedef	signed int				BOOL;				//通用BOOL型
#endif

typedef enum __gnet_bool
{
	gnet_false = 0,
	gnet_true,
}GNET_BOOL;

#ifndef GNET_TRUE
#define	GNET_TRUE				1
#else
#error "GNET_TRUE重复定义！"
#endif

#ifndef GNET_FALSE
#define	GNET_FALSE				0
#else
#error "GNET_TRUE重复定义！"
#endif

#ifndef GNET_OK
#define	GNET_OK					0
#else
#error "GNET_OK重复定义！"
#endif

#ifndef GNET_NULL
#define	GNET_NULL				0
#else
#error "GNET_NULL重复定义！"
#endif

#define GNET_MODULE_VERSION(name_v, major_v, min_v, fix_v)	\
									#name_v " Version:" #major_v "." #min_v "." #fix_v

#ifdef  __cplusplus
}
#endif

#endif	/*	__GNET_DATA_TYPE_H__	*/
