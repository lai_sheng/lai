/********************************************************************
	created:	2008/01/21
	created:	21:1:2008   11:01
	filename: 	evolution\Net\Include\NetCommunicate\INetC.h
	file path:	volution\Net\Include\NetCommunicate
	file base:	INetC
	file ext:	h
	author:		zhang_yaozhen

	purpose:	封装网络连接通信的接口
*********************************************************************/

#ifndef _INETC_H
#define _INETC_H

#include "DHTypedef.h"

namespace GNetWorkLib
{

/************************************************************************/
/*   INetC -- Interface Network Communicate*/
/************************************************************************/
#define NET_OK					0
#define NET_ERROR				-1
#define NET_DEFAULT_TIME_OUT	5000	//!默认网络操作超时时间

/*!
  \breif 面向连接的tcp/ip通信接口, 可以用于tcp, udp
*/
class INetCon
{
public:
    virtual ~INetCon() {}

    //!创建绑定本地端口，不传就随机
    virtual int Create ( const char * ip_in = NULL, const int port_in = 0 ) = 0;
    virtual int Close() = 0;

    //!tcp, udp 都可以用, 面身连接时
    /*!
    \breif 连接指定服务器端口
    \param[in] time_in 连接超时时间, 默认为不设置
    */
    virtual int Connect( const char * ip_in, const int port_in, const int time_in = 0 ) = 0;

    /*!
    \breif 发送数据
    \param[in] time_in 发送超时间, 默认阻塞发送
    \param[in] buf_len_in_out 指示buf_in 缓冲区的大小
    \param[out] buf_len_in_out	指示发送的数量
    */
    virtual int Send ( const char * buf_in, int& buf_len_in_out, const int time_in = 0 ) = 0;
    /*!
      \breif 接收数据
      \param[in] buf_len_in_out 指示接收数据缓冲区(buf_out)的大小
      \param[out] buf_len_in_out 指示接收到的数据长度
      \param[in] time_in 指示接收超时时间, 默认阻塞接收
    */
    virtual int Recv ( char * buf_out, int& buf_len_in_out, const int time_in = 0 ) = 0;

    //!for tcp server specially
    //!可选择使用
    virtual int SetListenQueue	( const int queue_len_in )									= 0;
    /*!
    \breif 等待新的连接到达
    \param[in] time_in 连接超时时间, 默认为不设置(阻塞等待)
    */
    virtual	INetCon * GetNewCon ( const int time_in = 0 ) = 0;

    //virtual int SetLocalAddr	( const char * ip_in, const int port_in )					= 0;
    virtual int GetLocaAddr		( char * ip_out, const int ip_len_in, int& port_out )		= 0;

    /*!
      \breif 设置远端地址，只对无连接时才能成功
    */
    //virtual int SetRemoteAddr	( const char * ip_in, const int port_in )					= 0;
    virtual int GetRemoteAddr	( char * ip_out, const int ip_len_in, int& port_out )		= 0;
protected:
    INetCon() {}
};

class CINetConFactory
{
public:
    CINetConFactory ( void ) {}
    ~CINetConFactory ( void ) {}
    PATTERN_SINGLETON_DECLARE_EXT ( CINetConFactory );

    //!要连接的远程服务端ip,端口
    INetCon * CreateTcpClient ( );

protected:
private:
};
}

#define g_INETCONFactory (*GNetWorkLib::CINetConFactory::instance())

#endif
