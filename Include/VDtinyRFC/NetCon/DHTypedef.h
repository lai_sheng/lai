#ifndef _DHTYPEDEF_H_
#define _DHTYPEDEF_H_

#define dh_trace printf
#define tracepoint() 			do {dh_trace("tracepoint: %s,%d\n",__FILE__,__LINE__); } while(0)

#define dh_assert(x)	\
	do							\
	{							\
	if( !(x) )				\
			{					\
			printf("assert :%s:%d \n", __FILE__, __LINE__);	\
			exit(-1);		\
			}					\
	} while(0);

#ifdef _DEBUG
#define dh_dbg_trace	dh_trace
#define ASSERT_RETURN(x, y)	\
	do							\
	{							\
	if( !(x) )				\
			{					\
			printf("assert :%s:%d, error code :%d\n", __FILE__, __LINE__, y);	\
			return y;		\
			}					\
	} while(0);
#else
#define dh_dbg_trace
#define ASSERT_RETURN(x, y)	\
	do							\
	{							\
	if( !(x) )				\
			{					\
			return y;		\
			}					\
	} while(0);
#endif

#ifndef NULL
#define NULL 0
#endif



/////////////////////////////////////////////////
///////////////// ����ģʽ
#define PATTERN_SINGLETON_DECLARE_EXT(classname)	\
	static classname * instance();

#define PATTERN_SINGLETON_IMPLEMENT_EXT(classname)	\
	classname * classname::instance()		\
{												\
	static classname * _instance = NULL;		\
	if( NULL == _instance)						\
	{											\
	_instance = new classname;				\
	}											\
	return _instance;							\
}
/////////////////////////////////////////////////







#endif