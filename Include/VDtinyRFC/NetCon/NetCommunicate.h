#ifndef _NETCOMMUNICATE_H
#define _NETCOMMUNICATE_H

#include <assert.h>
#include "ez_libs/ez_socket/ez_socket.h"
#include "INetC.h"

#ifndef _USE_SSL
#define _USE_SSL	//!默认开启ssl
#endif

#ifdef _USE_SSL
#include "MatrixSSL/sslsocket.h"
#endif

namespace GNetWorkLib
{
#define NET_IP_LEN	16
class CNetAddr
{
public:
    CNetAddr();
    int SetAddr ( const struct sockaddr_in addr_in );
    //!任意地址传0.0.0.0
    int SetAddr ( const char * ip_in, const int port_in );
    bool EqualAddr ( const char * ip_in, const int port_in );
    bool EqualAddr ( const struct sockaddr_in addr_in );
    bool IsValid();

    int GetAddr ( struct sockaddr_in * addr_out );
    char * GetIP ( char * ip_out, const int ip_len_in );
    int GetPort();
protected:
private:
    struct sockaddr_in m_addr;
};

/************************************************************************/
/*   CNetCom -- class Network Common*/
/************************************************************************/
class CNetComCon : public INetCon
{
public:
    CNetComCon();
    virtual ~CNetComCon();
    virtual int Create ( const char * ip_in = NULL, const int port_in = 0 ) = 0 ;
    virtual int Close();

    virtual int Send ( const char * buf_in, int& buf_len_in, const int time_in = 0 )	 ;
    virtual int Recv ( char * buf_out, int& buf_len_in_out, const int time_in = 0 )			 ;

    //!for tcp server specially
    virtual int SetListenQueue	( const int queue_len_in );
    virtual	INetCon * GetNewCon ( const int time_in = 0 );

    //!可选择使用
    //virtual int SetLocalAddr	( const char * ip_in, const int port_in )					;
    virtual int GetLocaAddr		( char * ip_out, const int ip_len_in, int& port_out )		;


    //virtual int SetRemoteAddr	( const char * ip_in, const int port_in )					;
    virtual int GetRemoteAddr	( char * ip_out, const int ip_len_in, int& port_out )		;
protected:
    ez_socket_t m_socket;
    CNetAddr	remote_addr;
    CNetAddr	local_addr;

    int Init();
    virtual int CommonSend ( const char * buf_in, const int buf_len_in,  const CNetAddr addr_to_in ) = 0;
    virtual int CommonRecv ( char * buf_out, const int buf_len_in_out, CNetAddr& addr_out ) = 0;
private:
};

/*!
  \breif tcp 客户端
*/
class CNetConTcpCli : public CNetComCon
{
public:
    CNetConTcpCli ( )	;
    virtual int Create ( const char * ip_in = NULL, const int port_in = 0 )				;
	virtual int Connect( const char * ip_in, const int port_in, const int time_in = 0 );
    //virtual int SetRemoteAddr	( const char * ip_in, const int port_in );					;
protected:
    virtual int CommonSend ( const char * buf_in, const int buf_len_in,  const CNetAddr addr_to_in ) ;
    virtual int CommonRecv ( char * buf_out, const int buf_len_in, CNetAddr& addr_out ) ;
private:
};

#ifdef _USE_SSL
class CNetComSsl : public CNetComCon
{
public:
    CNetComSsl();
    virtual ~CNetComSsl();
	
	virtual int Create ( const char * ip_in = NULL, const int port_in = 0 );
    virtual int Connect ( const char * ip_in, const int port_in, const int time_in = 0 ) ;
    virtual int Send ( const char * buf_in, int& buf_len_in, const int time_in = 0 ) ;
    virtual int Recv ( char * buf_out, int& buf_len_in_out, const int time_in = 0  ) ;
protected:;
	virtual int CommonSend ( const char * buf_in, const int buf_len_in,  const CNetAddr addr_to_in ) {assert(0);return -1;}
    virtual int CommonRecv ( char * buf_out, const int buf_len_in, CNetAddr& addr_out ) {assert(0);return -1;}
	
    sslConn_t		*	connssl;
    sslSessionId_t	*	sessionId;
    sslKeys_t		*	keys;

    virtual int Open( ) ;
    virtual int Close() ;
private:
};
#endif

class CNetUdp : public CNetComCon
{
public:
    CNetUdp();
    virtual int Create ( const char * ip_in = NULL, const int port_in = 0 )  ;
protected:
    virtual int CommonSend ( const char * buf_in, const int buf_len_in,  const CNetAddr addr_to_in ) ;
    virtual int CommonRecv ( char * buf_out, const int buf_len_in_out, CNetAddr& addr_out ) ;
    bool m_connect_flag;
private:
};

}

#endif