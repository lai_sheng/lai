
#ifndef __DH_DDNS_API_H__
#define __DH_DDNS_API_H__

#include "../GNetDataType.h"

/////////////////////////////////////////////////
///////////////// 单件模式
#define PATTERN_SINGLETON_DECLARE_EXT(classname)	\
	static classname * instance();

#define PATTERN_SINGLETON_IMPLEMENT_EXT(classname)	\
	classname * classname::instance()		\
{												\
	static classname * _instance = NULL;		\
	if( NULL == _instance)						\
	{											\
	_instance = new classname;				\
	}											\
	return _instance;							\
}
/////////////////////////////////////////////////


/*DDNS信息结构*/
typedef struct tagDDNS_Client_Setup
{
    int IsEnable;
    GNET_BYTE DdnsServerIp[32];
    int  DdnsServerPort;
    GNET_BYTE DdnsName[260];
    GNET_BYTE HardwareId[32];
    GNET_WORD TCPPort;
    GNET_WORD HTTPPort;
} DDNS_Client_Setup , *pDDNS_Client_Setup;

namespace GNetWorkLib
{

class IDdns
{
public:
    virtual ~IDdns() {};
    virtual int	Register() = 0;
    virtual int	KeepAlive() = 0;

    virtual void	SetResiterIP ( char * register_ip ) = 0 ;
    virtual void	SetDomainName ( char * domain_name_tmp ) = 0 ;

    virtual void SetHttpProxy ( bool enable, char * proxyIP, int proxyPort ) = 0;
protected:
private:
};

class CDdnsFactory
{
public:
    CDdnsFactory() {}
    ~CDdnsFactory() {}

    PATTERN_SINGLETON_DECLARE_EXT ( CDdnsFactory );
    /*!
      \brief 创建希网ddns对象
      \param [out] pDdns_out, 取得创建成功的ddns对象指针
    */
    int CreateDdnsCn99 ( IDdns** pDdns_out, const char * hostname, const char * username,
                         const char * password, const char * server_ip, const int server_port );

    /*!
    \breif 创建大华ddns
    */
    int CreateDdnsDahua ( IDdns** pDdns_out, char * register_ip, DDNS_Client_Setup * inter_ddns );

    /*!
    \breif 创建no ip ddns
    */
    int CreateDdnsNoip ( IDdns** pDdns_out, const char * hostname, const char * username,
                         const char * password, const char * server_ip, const int server_port );

    int CreateDyndns ( IDdns** pDdns_out, const char * hostname, const char * username,
                       const char * password, const char * server_ip, const int server_port );

    int CreateSysdns ( IDdns** pDdns_out, const char * hostname, const char * username,
                       const char * password, const char * server_ip, const int server_port );
	

    /*!
      \breif 创建花生壳ddns
      \retval GNET_OK 创建成功
      \retval DDNS_FAILED 创建失败
    */
    int CreateDdnsOray ( IDdns** pDdns_out, const char * hostname, const char * username,
                         const char * password );
	/*!
	\brieif 创建G4ip ddns
	*/
    int CreateDdnsG4ip ( IDdns** pDdns_out, const char * hostname, const char * username,
                         const char * password, const char * server_ip, const int server_port );

	int CreateDdnsVisiondigi( IDdns** pDdns_out, const char * hostname, const char * username,
		const char * password );

	int CreateDdnsZk( IDdns** pDdns_out, const char * hostname, const char * username,
		const char * password, int httpPort );   //gong

	int CreateDdnsQinDaoMeiBu( IDdns** pDdns_out, const char * hostname,
		const char * password);

public:
	int SignInDdnsQinDaoMeiBu(const char* hostname, const char* passwd,const char* servername, const int serverport);
	int SetNetDdnsQinDaoMeiBu(const char* servername, const int serverport);
protected:
private:
};

}

#define g_DDNSFactory (*GNetWorkLib::CDdnsFactory::instance())

#endif
