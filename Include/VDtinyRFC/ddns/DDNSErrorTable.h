/************************************************************************
**SMTP Modularization
*************************************************************************
**		(C)Copyright 2008, Zhejiang Dahua Technology Stock Co.Ltd.
**							All Rights Reserved
**File Name:	SMTPErrorTable.h
**Version:		Version 1.00
**Purpose:		定义SMTP模块的错误值及相关信息
**Author:		ZhangYaozhen
**Created:		2008-3-13	15:15
**Modified:		cx.han
**Modify Reason:整理代码
*************************************************************************/
#pragma once

#ifndef _DDNS_ERROR_TABLE_H__
#define	_DDNS_ERROR_TABLE_H__

/*
*组件错误
*/

/** 处理失败, 原因未知, 一般可能会从打印信息获取*/
#define	DDNS_FAILED				0x00300001

/** 字符串长度过大,超过最大支持*/
//#define SMTP_STR_BIG			0x00300002

///** 邮件接收者超过最大数目*/
//#define	SMTP_RECEIVER_MAX		0x00500003
//
///** 连接服务器失败*/
//#define SMTP_CONNECT_FAILED		0x00500004
//
///** 用户名错误*/
//#define SMTP_USER_INVALID		0x00500005
//
///** 用户密码错误*/
//#define SMTP_PASSWORD_INVALID	0x00500006
//
///** ssl 方式连接服务器失败, 即tcp连接是成功的, 在ssl握手期间出错*/
//#define SMTP_SSL_CONNECT_FAILED	0x00500007
//
///** smtp 参数无效bug*/
//#define SMTP_PARAMS_INVALID  0x00500008
//
///** smtp发送者地址不是有效的，可能是没有带@*/
//#define SMTP_SENDER_INVALID 0x00500009
//
///** smtp接收者地址不是有效的，可能是没有带@*/
//#define SMTP_RECEIVER_INVALID	0x0050000A
//
///** 邮件内容总长度过大,目前支持2M*/
//#define SMTP_CONTENT_STR_BIG	0x0050000B

#endif	/* _SMTP_ERROR_TABLE_H__ */
