/************************************************************************
**SMTP Modularization
*************************************************************************
**		(C)Copyright 2008, Zhejiang Dahua Technology Stock Co.Ltd.
**							All Rights Reserved
**File Name:	DHSmtpApi.h
**Version:		Version 1.00
**Purpose:		提供SMTP模块的使用接口，并说明SMTP使用的方式
**Author:		ZhangYaozhen
**Created:		2008-3-13	15:15
**Modified:		cx.han
**Modify Reason:1、整理代码
				2、增加两个接口：MatchVersion:用于版本匹配
								 TestSetting:用于测试SMTP的设置
*************************************************************************/
#ifndef __DHSMTPAPI_H__
#define __DHSMTPAPI_H__

#include <stdio.h>
#include "SMTPErrorTable.h"
#include "../GNetDataType.h"

/////////////////////////////////////////////////
///////////////// 单件模式
#define PATTERN_SINGLETON_DECLARE(classname)	\
static classname * instance();

#define PATTERN_SINGLETON_IMPLEMENT(classname)	\
classname * classname::instance()		\
{												\
	static classname * _instance = NULL;		\
	if( NULL == _instance)						\
	{											\
		_instance = new classname;				\
	}											\
	return _instance;							\
}												
/////////////////////////////////////////////////

/** SMTP版本号定义*/
#define	DH_SMTP_VER						"DH_SMTP Version: 1.0.0"

//typedef enum
//{
//	STATE_init = 0,	//!邮件处于初始准备状态, 还没有开始和smtp服务器联系
//	STATE_connect,
//	STATE_login,
//	STATE_Send,
//	STATE_NUM,
//}SMTP_STATE_E;		//SMTP状态表

typedef enum
{
	SMTP_STATE_INIT = 0,
	SMTP_CREATE_SOCKET,	//!邮件处于初始准备状态, 还没有开始和smtp服务器联系
	SMTP_CREATE_TCP,
	SMTP_TCP_FAILED,
	SMTP_TCP_OK,
	SMTP_HELLO_TO_SERVER,
	SMTP_SERVER_IS_READY,
	SMTP_SERVER_IS_BUSY,
	SMTP_LOGIN_SERVER,
	SMTP_LOGIN_OK,
	SMTP_LOGIN_FAILED,
	SMTP_LOGIN_AGAIN,
	SMTP_SEND_SENDER_INFO,
	SMTP_WAIT_ACK,
	SMTP_SEND_1RECEIVER_INFO,
	SMTP_SEND_RECEIVER_OK,
	SMTP_SEND_DATA,
	SMTP_SEND_MAIL_HEADER,
	SMTP_SEND_MAIL,
	SMTP_SEND_MAIL_END,
	SMTP_QUIT_SERVER,
	SMTP_DEAL_FAILED,
	STATE_NUM,
}SMTP_STATE_E;		//SMTP状态表

typedef enum
{
	SET_SERVER_ADDR = STATE_NUM,
	SET_SERVER_PORT,
	SET_USER_NAME,
	SET_PASSWORD,
	SET_SEND_ADDR,
	START_TEST_SETTING,
	SET_MAIL_SUB,
	CHECK_RECEIVER,
	ADD_RECEIVER,
	ADD_ATECHMENT,
	WRITE_MAIL,
	GET_MAX_RECEVER_NUM,
	START_SEND_MAIL,
	SEND_MAIL_OK,
	SET_MAIL_SSL,
	SET_MAIL_NOMAL,
	GET_VERSION,
	MATCH_VERSION,
	SMTP_IF_STATE_NUM,
}SMTP_IF_STATE;

class ISMTP
{
public:
	virtual ~ISMTP(){};

	/************************************************* 
	* Function:       GetVersion
	* Description:    获取当前模块版本信息，字符串方式提供
	* Calls:          
	* Input:          [const GNET_DWORD]buf_len:获取缓冲区的长度
	* Output:         [GNET_PSTR]smtp_version:版本信息字符串指针
	* Return:         GNET_OK = 获取成功;SMTP_FAILED = 获取失败
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD GetVersion(GNET_PSTR smtp_version, const GNET_DWORD buf_len) = 0;

	/************************************************* 
	* Function:       MatchVersion
	* Description:    匹配版本信息
	* Calls:          
	* Input:          [GNET_PSTR]smtp_version:版本信息字符串指针
	* Output:         
	* Return:         GNET_OK = 匹配成功;SMTP_FAILED = 匹配失败
	* Author:         cx.han
	* Created:        2008-3-19  09:28
	*************************************************/ 
	virtual GNET_DWORD MatchVersion(GNET_PSTR smtp_version) = 0;

	/************************************************* 
	* Function:       SetMailSsl
	* Description:    设备邮件发送模式是否加密, 不调用此函数, 默认是不加密
	* Calls:          
	* Input:          [const GNET_BOOL]flag_in:
	*								--false:使用明文发送(默认就是明文发送)
	*								--true: 使用ssl加密方式发送
	* Output:         
	* Return:         GNET_OK = 设置成功
	*				  SMTP_FAILED = 设置失败
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SetMailSsl(const GNET_BOOL flag_in) = 0;

	/************************************************* 
	* Function:       SetSmtpServer
	* Description:    设置smtp服务器地址及端口
	* Calls:          
	* Input:          [GNET_PCSTR]domain_server:服务器域名或点分十进制IP地址字符串指针
	*				  [const GNET_PORT16]port_in:邮件服务器端口号
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SetSmtpServer(GNET_PCSTR domain_server, const GNET_PORT16 port_in) = 0;

	/************************************************* 
	* Function:       SetUserPasswd
	* Description:    设置登录服务器的用户名和密码
	* Calls:          
	* Input:          [GNET_PCSTR]username_in:用户名字符串指针
	*				  [GNET_PCSTR]password_in:密码字符串指针
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SetUserPasswd(GNET_PCSTR username_in, GNET_PCSTR password_in) = 0;

	/************************************************* 
	* Function:       SetSenderAddr
	* Description:    设置邮件发送者的邮件地址
	* Calls:          
	* Input:          [GNET_PCSTR]sender_addr_in:发送者邮件地址字符串指针
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SetSenderAddr(GNET_PCSTR sender_addr_in ) = 0;

	/************************************************* 
	* Function:       TestSmtpSet
	* Description:    测试SMTP设置是否可用
	* Calls:          
	* Input:          
	* Output:         
	* Return:         GNET_OK = 测试成功;SMTP_FAILED = 测试失败
	* Author:         cx.han
	* Created:        2008-3-19  09:28
	*************************************************/ 
	virtual GNET_DWORD TestSmtpSet(void) = 0;

	/************************************************* 
	* Function:       GetMaxReceiver
	* Description:    获取一次性能设置的邮件接收者的最大数目,
	*				  添加邮件接收人的时侯, 不能超过这个值
	* Calls:          
	* Input:          
	* Output:         [GNET_DWORD*]receiver_max:返回邮件接收者的数量
	* Return:         GNET_OK = 获取成功;SMTP_FAILED = 获取失败
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD GetMaxReceiver(GNET_DWORD* receiver_max) = 0;

	/************************************************* 
	* Function:       AddReceiver
	* Description:    添加一个邮件接收者的地址
	* Calls:          
	* Input:          [GNET_PCSTR]recv_addr_in:接收者邮件地址字符串指针
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD AddReceiver(GNET_PCSTR recv_addr_in ) = 0;

	/************************************************* 
	* Function:       SetSubject
	* Description:    设置邮件标题
	* Calls:          
	* Input:          [GNET_PCSTR]subject_in:标题字符串指针
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SetSubject(GNET_PCSTR subject_in) = 0;

	/************************************************* 
	* Function:       AddContentMimeText
	* Description:    添加邮件内容
	* Calls:          
	* Input:          [GNET_PCSTR]text_in:写入邮件的内容的字符串指针
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD AddContentMimeText(GNET_PCSTR text_in) = 0;

	/************************************************* 
	* Function:       AddContentAttachment
	* Description:    添加附件
	* Calls:          
	* Input:          [GNET_PCSTR]file_name_in:附件名称的字符串指针
	*				  [GNET_PCSTR]attachment_in:附件的数据指针
	*				  [const GNET_DWORD]attachment_len:附件的长度
	* Output:         
	* Return:         GNET_OK = 设置成功，SMTP_STR_BIG = 字符串过长
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD AddContentAttachment(GNET_PCSTR file_name_in, 
											GNET_PCSTR attachment_in, 
											const GNET_DWORD attachment_len) = 0;

	/************************************************* 
	* Function:       ClearContent
	* Description:    清除邮件内容
	* Calls:          
	* Input:          
	* Output:         
	* Return:         GNET_OK = 清除成功
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
//	virtual GNET_DWORD ClearContent(void) = 0;

	/************************************************* 
	* Function:       SendMail
	* Description:    发送邮件
	* Calls:          
	* Input:          
	* Output:         
	* Return:         GNET_OK = 发送成功  
	*				  SMTP_FAILED = 发送失败 
	*				  SMTP_CONNECT_FAILED = 服务器连接失败
	*				  SMTP_USER_INVALID = 用户名不合法
	*				  SMTP_PASSWORD_INVALID = 密码错误
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD SendMail(void) = 0;

	/************************************************* 
	* Function:       GetSmtpState
	* Description:    增加反应模块状态的接口, 返回当前模块的所处的状态
	* Calls:          
	* Input:          
	* Output:         [SMTP_STATE_E*]smtp_state:返回当前模块的状态
	* Return:         GNET_OK = 获取成功;SMTP_FAILED = 获取失败
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	virtual GNET_DWORD GetSmtpState(SMTP_STATE_E* smtp_state ) = 0;

	virtual GNET_DWORD StartSmtpStatusTrace(void) = 0;

	virtual GNET_DWORD StopSmtpStatusTrace(void) = 0;

	virtual GNET_DWORD PrintSmtpStatusTrace(FILE* pWriteFile) = 0;

protected:
private:
};

class CSmtpFactory
{
public:
	CSmtpFactory(void);
	~CSmtpFactory(void);
	PATTERN_SINGLETON_DECLARE(CSmtpFactory);
	
	/**根据类型返回邮件发送对象为普通发送还是ssl加密方式发送*/
	/************************************************* 
	* Function:       CreateSmtp
	* Description:    用于创建邮件发送对象
	* Calls:          
	* Input:          
	* Output:         [ISMTP**]pISMTP:指向对象的指针的指针
	* Return:         GNET_OK = 邮件对象创建成功;
	*				  SMTP_FAILED = 邮件对象创建失败
	* Author:         Zhang Yaozhen
	* Created:        2008-3-13  15:28
	*************************************************/ 
	GNET_DWORD CreateSmtp(ISMTP** pISMTP);

	GNET_DWORD DestroySmtp(void);
protected:
private:
	ISMTP* m_pISMTP;
};

#define g_SMTPFactory (*CSmtpFactory::instance())

#endif
