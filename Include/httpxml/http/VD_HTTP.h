#ifndef __VD_HTTP_H__
#define __VD_HTTP_H__
/*
* Copyright (c) 2007, visiondigi
* All rights reserved.
*
* 文件名称：VD_HTTP.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：HTTP协议栈。本协议栈的设计原则是扩展性好、通用性好。对于绝大多数意义不是很明确的头域使用字符串处理
由具体的上层应用进行应用级解析和使用。HTTP数据分为请求和应答两类。对于标准的头域包括HTTP、RTSP、SIP
都进行了处理。因为HTTP协议的特点是扩展性好，所以上层应用很容易进行扩展，为了支持扩展定义了parseLine
和packetLine进行扩展。新的应用协议从基类中继承出去，实现这两个方法，对扩展头域进行正反向序列化处理
就可以支持扩展头域。
很多时候HTTP PDU会被放入队列，经过几道保存和传输，所以引入了引用计数机制。
请求类和应用类仅是HTTP第一行数据不同，所以公共处理代码和处理框架在HTTPCommon类中，HTTPRequest和
HTTPResponse仅仅是处理第一行数据的不同而已。
*
* 当前版本：1.0
* 作　　者：zdx
* 完成日期：2009年12月23日
* 修订记录：移除RTSP和SIP以及其他不常用的字段，有需要在子类中自己实现，以提高效率。
*/

#include "HttpProtocol.h"
#include <assert.h>
#include <list>
#include <string>
typedef std::list<std::string> StrList;

namespace
{
    ///////////////////////////////////////////////////////////////////
    ///如果关键字和某个字段的内容又重复，这里可能有问题？
#define PARSE_HEAD_FIELD(line, dst, tagStr)                         \
    else if ( (fieldBegin = line.find(tagStr))                          \
    != std::string::npos)                                           \
    {                                                                   \
        fieldBegin = line.find_first_of(':', fieldBegin);               \
        fieldBegin = line.find_first_not_of(" ", fieldBegin + 1);       \
        if (fieldBegin != std::string::npos)                            \
        {                                                               \
            dst.assign(line, fieldBegin, line.size() - fieldBegin);  \
        }                                                               \
    }

#define PARSE_HEAD_VIA_FIELD(line, dst, tagStr)                         \
    else if ( (fieldBegin = line.find(tagStr))                          \
        != std::string::npos)                                           \
    {                                                                   \
        fieldBegin = line.find_first_of(':', fieldBegin);               \
        fieldBegin = line.find_first_not_of(" ", fieldBegin + 1);       \
        if (fieldBegin != std::string::npos)                            \
        {                                                               \
            dst.assign(line, fieldBegin, line.size() - fieldBegin);  \
            viaList.push_back(dst);                                        \
        }                                                               \
    }

#define  PARSE_HEAD_FIELD_INT(line, dst, tagStr)                        \
    else if ( (fieldBegin = line.find(tagStr))                          \
        != std::string::npos)                                       \
    {                                                                   \
        fieldBegin = line.find_first_of(':', fieldBegin);               \
        if (fieldBegin != std::string::npos)                            \
        {                                                               \
            fieldBegin += 1;                                            \
            dst = atoi(line.c_str() + fieldBegin);                      \
        }                                                               \
    }


#define PACKET_HEAD_FIELD_STR(var, title)                        \
    if (var.size() > 0)                                            \
    {                                                            \
        assert(var.size() + sizeof(title) + 3 < sizeof(str));   \
        memset(str, 0, sizeof(str));                            \
        sprintf(str, "%s: %s\r\n", title, var.c_str());         \
        _buffer    += str;                                            \
    }

#define PACKET_HEAD_FIELD_INT(var, title)                        \
    if (var > 0)                                                \
    {                                                            \
        assert(sizeof(title) + 15 < sizeof(str));               \
        memset(str, 0, sizeof(str));                            \
        sprintf(str, "%s: %d\r\n", title, var);                 \
        _buffer += str;                                            \
    }

}

namespace CommonLib
{
    class HTTPCommon
    {
    public:
        HTTPCommon(MSG_TYPE enType);

        virtual ~HTTPCommon(void);

        /** 网络流序列化和反序列化接口 */
        int fromStream(std::string data);
        const char* toStream(void);
        /**Dump out for debug*/
        void DumpStream()const;

        /** 获取上一次操作的错误码 */
        RET_CODE GetLastError()const{return errcode;}

        /** 获取当前包体流串 */
        const char* getBody(void);
        /** 填充自定义包体 */
        int setBody(const char* body, int len);

        /** 获取当前消息缓冲流 */
        const char* getString(void);
        /** 当前流缓冲数据长度 */
        size_t getLength();

        /** 消息方法类型和协议类型获取接口 */
        MSG_TYPE getType(void);    //Request or Response
        PROTOCOL_TYPE getProtocol(void);    //SIP or HTTP or RTSP
        void setProtocol(PROTOCOL_TYPE type);

        int addRef();
        int release();

        void reset(void);
 
    private:
        /** 预解析，用于校验消息完整性等,增强协议栈的分析和校验能力 */
        virtual RET_CODE preParse();

    protected:
        /** 解析用错误码 */
        RET_CODE    errcode;

    public:

        std::string method;
        int contentType;
        size_t contentLength;
        std::string url;
        std::string contentTypeString;
        std::string accept;
        std::string userAgent;
        std::string host;

        int result; /* 只有响应的时候才有result*/
        std::string message;
    protected:
        ///< Internal buffer
        std::string _body;
        std::string _buffer;

        MSG_TYPE      _reqOrRep;
        PROTOCOL_TYPE _protocol;

    protected:
        
        virtual RET_CODE parseReqHead(const std::string& data);
        virtual RET_CODE parseRspHead(const std::string& data);
        virtual RET_CODE packetReqHead(void);
        virtual RET_CODE packetRspHead(void);

        RET_CODE packetHead(void);
        RET_CODE parseHead(const std::string& data);
        RET_CODE parseCommon(const std::string& data);
        RET_CODE packetCommon(void);

        virtual int parseLine(const std::string& data);/*对扩展字段进行解包*/
        virtual int packetLine(void);/*对扩展字段进行打包*/
    };
}

#endif //head
