/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：HttpProtocol.h
* 文件标识：参见配置管理计划书
* 摘　　要：HTTP协议预定头文件。包含了HTTP的头域、错误码、操作等的定义。因为RTSP和SIP也是基于HTTP协议风格的，所以
一并包含了RTSP和SIP的支持。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年5月28日
* 修订记录：增加完整的RTSP和SIP支持。

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年5月19日
* 修订记录：创建
*/

/*
* 修改了一些枚举类型，以去掉源代码中大量的强制类型转换，使得接口更安全, by yanf(10811),070823
* 增加content-type 的识别类型，将比较标准的x-www-url-form-encoded加入，by yanf(10811), 070831
*/

#ifndef _HTTPPROTOCOL_H_
#define _HTTPPROTOCOL_H_

namespace CommonLib
{
    /* ----------------------- HTTP Status Codes  ------------------------- */

    #define HTTP_RESPONSE_CODES 38

    #define HTTP_CONTINUE                      100
    #define HTTP_SWITCHING_PROTOCOLS           101
    #define HTTP_OK                            200
    #define HTTP_CREATED                       201
    #define HTTP_ACCEPTED                      202
    #define HTTP_NON_AUTHORITATIVE             203
    #define HTTP_NO_CONTENT                    204
    #define HTTP_RESET_CONTENT                 205
    #define HTTP_PARTIAL_CONTENT               206
    #define HTTP_MULTIPLE_CHOICES              300
    #define HTTP_MOVED_PERMANENTLY             301
    #define HTTP_MOVED_TEMPORARILY             302
    #define HTTP_SEE_OTHER                     303
    #define HTTP_NOT_MODIFIED                  304
    #define HTTP_USE_PROXY                     305
    #define HTTP_BAD_REQUEST                   400
    #define HTTP_UNAUTHORIZED                  401
    #define HTTP_PAYMENT_REQUIRED              402
    #define HTTP_FORBIDDEN                     403
    #define HTTP_NOT_FOUND                     404
    #define HTTP_METHOD_NOT_ALLOWED            405
    #define HTTP_NOT_ACCEPTABLE                406
    #define HTTP_PROXY_AUTHENTICATION_REQUIRED 407
    #define HTTP_REQUEST_TIME_OUT              408
    #define HTTP_CONFLICT                      409
    #define HTTP_GONE                          410
    #define HTTP_LENGTH_REQUIRED               411
    #define HTTP_PRECONDITION_FAILED           412
    #define HTTP_REQUEST_ENTITY_TOO_LARGE      413
    #define HTTP_REQUEST_URI_TOO_LARGE         414
    #define HTTP_UNSUPPORTED_MEDIA_TYPE        415
    #define HTTP_INTERNAL_SERVER_ERROR         500
    #define HTTP_NOT_IMPLEMENTED               501
    #define HTTP_BAD_GATEWAY                   502
    #define HTTP_SERVICE_UNAVAILABLE           503
    #define HTTP_GATEWAY_TIME_OUT              504
    #define HTTP_VERSION_NOT_SUPPORTED         505
    #define HTTP_VARIANT_ALSO_VARIES           506

    #define DOCUMENT_FOLLOWS    HTTP_OK
    #define PARTIAL_CONTENT     HTTP_PARTIAL_CONTENT
    #define MULTIPLE_CHOICES    HTTP_MULTIPLE_CHOICES
    #define MOVED               HTTP_MOVED_PERMANENTLY
    #define REDIRECT            HTTP_MOVED_TEMPORARILY
    #define USE_LOCAL_COPY      HTTP_NOT_MODIFIED
    #define BAD_REQUEST         HTTP_BAD_REQUEST
    #define AUTH_REQUIRED       HTTP_UNAUTHORIZED
    #define FORBIDDEN           HTTP_FORBIDDEN
    #define NOT_FOUND           HTTP_NOT_FOUND
    #define METHOD_NOT_ALLOWED  HTTP_METHOD_NOT_ALLOWED
    #define NOT_ACCEPTABLE      HTTP_NOT_ACCEPTABLE
    #define LENGTH_REQUIRED     HTTP_LENGTH_REQUIRED
    #define PRECONDITION_FAILED HTTP_PRECONDITION_FAILED
    #define SERVER_ERROR        HTTP_INTERNAL_SERVER_ERROR
    #define NOT_IMPLEMENTED     HTTP_NOT_IMPLEMENTED
    #define BAD_GATEWAY         HTTP_BAD_GATEWAY
    #define VARIANT_ALSO_VARIES HTTP_VARIANT_ALSO_VARIES

    #define is_HTTP_INFO(x)         (((x) >= 100)&&((x) < 200))
    #define is_HTTP_SUCCESS(x)      (((x) >= 200)&&((x) < 300))
    #define is_HTTP_REDIRECT(x)     (((x) >= 300)&&((x) < 400))
    #define is_HTTP_ERROR(x)        (((x) >= 400)&&((x) < 600))
    #define is_HTTP_CLIENT_ERROR(x) (((x) >= 400)&&((x) < 500))
    #define is_HTTP_SERVER_ERROR(x) (((x) >= 500)&&((x) < 600))

    #define status_drops_connection(x) (((x) == HTTP_BAD_REQUEST)           || \
        ((x) == HTTP_REQUEST_TIME_OUT)      || \
        ((x) == HTTP_LENGTH_REQUIRED)       || \
        ((x) == HTTP_REQUEST_ENTITY_TOO_LARGE) || \
        ((x) == HTTP_REQUEST_URI_TOO_LARGE) || \
        ((x) == HTTP_INTERNAL_SERVER_ERROR) || \
        ((x) == HTTP_SERVICE_UNAVAILABLE))

    #define CGI_MAGIC_TYPE            "application/x-httpd-cgi"
    #define INCLUDES_MAGIC_TYPE        "text/x-server-parsed-html"
    #define INCLUDES_MAGIC_TYPE3    "text/x-server-parsed-html3"
    #define MAP_FILE_MAGIC_TYPE        "application/x-type-map"
    #define ASIS_MAGIC_TYPE            "httpd/send-as-is"
    #define DIR_MAGIC_TYPE            "httpd/unix-directory"
    #define STATUS_MAGIC_TYPE        "application/x-httpd-status"
    #define    URLENCODED_MATIC_TYPE    "application/x-www-form-urlencoded"

    #define CONTENT_TYPE_XML        1
    #define CONTENT_TYPE_SDP        2
    #define CONTENT_TYPE_HTTP        3
    #define CONTENT_TYPE_HTML        4
    #define CONTENT_TYPE_OCTET        5
    #define CONTENT_TYPE_URLENC     6   
    #define STR_CONTENT_TYPE_XML    "text/xml"
    #define STR_CONTENT_TYPE_SDP    "application/sdp"
    #define STR_CONTENT_TYPE_HTTP    "application/http"
    #define STR_CONTENT_TYPE_HTML    "text/html"
    #define STR_CONTENT_TYPE_OCTET    "application/octet-stream"
    #define STR_CONTENT_TYPE_URLENC "application/x-www-form-urlencoded"

    /** 上层协议 */
    typedef enum {
        PROTOCOL_HTTP = 1,
        PROTOCOL_SIP  = 2,
        PROTOCOL_RTSP = 3,
        
        /** 未知协议 */
        PROTOCOL_INVALID = 7
    }PROTOCOL_TYPE;


    
    /** 消息类型 */
    typedef enum
    {
        MODEL_REQUEST  = 1,
        MODEL_RESPONSE = 2,
    }MSG_TYPE;

    /* Just in case your linefeed isn't the one the other end is expecting. */
    #define HTTP_LF 10
    #define HTTP_CR 13

    /* Possible values for request_rec.read_body (set by handling module):
    *    REQUEST_NO_BODY          Send 413 error if message has any body
    *    REQUEST_CHUNKED_ERROR    Send 411 error if body without Content-Length
    *    REQUEST_CHUNKED_DECHUNK  If chunked, remove the chunks for me.
    *    REQUEST_CHUNKED_PASS     Pass the chunks to me without removal.
    */
    #define REQUEST_NO_BODY          0
    #define REQUEST_CHUNKED_ERROR    1
    #define REQUEST_CHUNKED_DECHUNK  2
    #define REQUEST_CHUNKED_PASS     3

    #define STR_CONTENT_TYPE        "Content-Type"
    #define STR_USER_AGENT            "User-Agent"
    #define STR_HOST                "Host"
    #define STR_X_CLIENT_ADDRESS    "X-Client-Address"
    #define STR_X_TRANSACTION_ID    "X-Transaction-ID"
    #define STR_CONTENT_LENGTH        "Content-Length"
    #define STR_SUPPORT_UNICAST "x-SupportUnicast"
    #define STR_X_PLAYINFO "X-PLAYINFO"

    #define STR_ACCEPT                "Accept"

    #define STR_VIA                    "Via"
    #define STR_DATE                "Date"
    #define STR_SERVER                "Server"
    #define STR_SET_COOKIE            "Set-Cookie"
    #define STR_COOKIE                "Cookie"

    #define STR_FROM                "From"
    #define STR_TO                    "To"
    #define STR_CONTACT                "Contact"
    #define STR_CSEQ                "CSeq"

    #define STR_CALLID                "Call-ID"
    #define STR_MAX_FORWARDS        "Max-Forwards"

    #define STR_SIP_VERSION            "SIP/2.0"
    #define STR_HTTP_VERSION        "HTTP/1.1"
    #define STR_RTSP_VERSION        "RTSP/1.0"

        //HTTP Head Field
    #define STR_ACCEPT_ENCODING        "Accept-Encoding"
    #define STR_ACCEPT_LANGUAGE        "Accept-Language"
    #define STR_ALLOW                "Allow"
    #define STR_BANDWIDTH            "Bandwidth"
    #define    STR_BLOCKSIZE            "Blocksize"
    #define STR_CONFERENCE            "Conference"
    #define STR_CONNECTION            "Connection"
    #define STR_CONTENT_BASE        "Content-Base"
    #define STR_CONTENT_ENCODING    "Content-Encoding"
    #define STR_CONTENT_LANGUAGE    "Content-Language"

    #define STR_RANGE                "Range"
    #define STR_RTP_INFO            "RTP-Info"
    #define STR_SCALE                "Scale"
    #define STR_SPEED                "Speed"
    #define STR_SESSION                "Session"
    #define STR_TIMESTAMP            "Timestamp"
    #define STR_TRANSPORT            "Transport"
    #define STR_WWW_AUTHENTICATE    "WWW-Authenticate"
    #define STR_AUTHOR "Authorization"
    #define STR_LAST_MODIFIED        "Last-Modified"
    #define STR_EXPIRES                "Expires"
    #define STR_VARY                "Vary"
    #define STR_UNSUPPORT            "Unsupported"

    #define STR_CACHE_CONTROL_X       "Cache-Control"
    #define STR_ACCEPT_RETRANSMIT_X   "x-Accept-Retransmit"
    #define STR_ACCEPT_DYNAMIC_RATE_X "x-Accept-Dynamic-Rate"
    #define STR_DYNAMIC_RATE_X        "x-dynamic-rate"
    #define STR_RETRANSMIT        "x-retransmit"
    #define STR_TRANSPORT_OPTIONS       "x-transport-options"    
    #define STR_PLAY_CONTROL    "X-PLAYCTRL"
    #define STR_X_VOD_URL "X-VODURL"
    #define STR_X_VOD "X-VOD"
    #define STR_PUBLIC "Public"
    #define STR_ZTE_CSEQ            "Cseq"
    
    /** 错误码 */
    typedef enum
    {
        ERR_BASE = -100,     /**<基本错误*/

        /** HTTP消息解析 */
        ERR_BODYINCOMPLETE,  /**<可以解析正确的消息头，但得到的消息体长度不足*/ 
        ERR_HDRINCOMPLETE,   /**<没有找到完整的消息头，消息头和消息体的解析都不正确*/
        ERR_NOCONTENTLEN,    /**<消息头不完整，并且消息头里边也没有消息体长度字段*/
        ERR_INVALIDCONTLEN,  /**<可以读到消息体长度，但其长度字段非法*/

        ERR_UNSUPPORT,       /**<请求的方法本协议栈不支持*/

        ERR_SUCCESS = 0,
    }RET_CODE;

}

#endif /*_HTTPPROTOCOL_H_*/



