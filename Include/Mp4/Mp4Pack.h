#ifndef MP4PACK_H_
#define MP4PACK_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#pragma once


typedef enum{
	VIDEO_I_FRAME,
	VIDEO_P_FRAME,
	VIDEO_B_FRAME,
	AUDIO_FRAME,
}FrameType;

void* Mp4_Open(const char* FileName,int VideoRate,int RecordTime);
void  Mp4_Close(void* Handle);
FILE* MP4_GetFp(void* Handle);
int   Mp4_GetCurrentPTime(void* Handle);
int   Mp4_Write(unsigned char* buf,int len,int height,int width,FrameType type,void* Handle);
int   Mp4_Write_WithTime(unsigned char* buf,int len,int height,int width,
	unsigned long long timestamp/*ms*/,FrameType type,void* Handle);

void* Read_Mp4_Open(const char* FileName);
int Read_Mp4_Close(void* handle);
//返回视频帧的总数
int Read_Mp4_GetVideoFrameNums(void* handle);
//返回视频帧每帧的平均时长
int Read_Mp4_GetVideoFrameDur(void* handle);
//返回视频帧某帧的时间戳
int Read_Mp4_GetVideoTimeStamp(void* handle,int val);

int Read_Mp4_GetAudioFrameNums(void* handle);
int Read_Mp4_GetAudioFrameDur(void* handle);
int Read_Mp4_GetAudioTimeStamp(void* handle,int val);

//返回I帧位于的帧数  ;例如:Read_Mp4_Get_I_FrameNum(1) 代表寻找第一帧的I帧位于视频的第几帧
//返回-1代表错误
int Read_Mp4_Get_I_FrameNum(void* handle,int val);
//返回I帧的总数
int Read_Mp4_Get_KeyFrameNums(void* handle);


int Read_Mp4_VideoData(void* handle,unsigned char *Buf,int Num);
int Read_Mp4_AudioData(void* handle,unsigned char *Buf,int Num);

//int Try_RepairMp4File(const char *FilePath,int VideoRate,int MaxRecTime);
int Try_RepairMp4File(const char *FilePath);

#ifdef __cplusplus
}
#endif
#endif
