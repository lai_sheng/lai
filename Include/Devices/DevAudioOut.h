

#ifndef __DEV_AUDIO_H__
#define __DEV_AUDIO_H__

#include "APIs/Audio.h"
#include "MultiTask/Mutex.h"

/*enum voicetype
{
	VOICE_NONE = 0,
	VOICE_TALK,
	VOICE_TIP,
	VOICE_TYPE_NUM
};*/
		
class CDevAudio
{
public:
	CDevAudio();
	virtual ~CDevAudio();

public:
	VD_BOOL Switch(uint dwType, uint dwChannel);	
	static CDevAudio* instance(void); 
	VD_BOOL Start();
	VD_BOOL Stop();	
	int  SetFormat(int type);
	void PutBuf(uchar *data, int length);
	//void Lock();
	//void UnLock();
	//BOOL PlayVoice(BYTE *data, int length, int type);
private:		
	static CDevAudio* _instance; 
public:
	//int m_VoiceType;
	//CMutex m_VoiceMutex;
};

#endif// __DEV_AUDIO_H__

