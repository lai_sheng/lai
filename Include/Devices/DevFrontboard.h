//
//  "$Id: DevFrontboard.h 247 2008-12-12 06:46:15Z liwj $"
//
//  Copyright (c)2008-2010, RealVision Technology CO., LTD.
//  All Rights Reserved.
//
//	Description:	
//	Revisions:		Year-Month-Day  SVN-Author  Modification
//

#ifndef __DEV_FRONTBOARD_H__
#define __DEV_FRONTBOARD_H__

#include "System/Signals.h"
#include "APIs/Frontboard.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "System/Object.h"


enum fb_keys_t {
	KEY_0 = 0x00,
	KEY_1,	KEY_2,	KEY_3,	KEY_4,	KEY_5,
	KEY_6,	KEY_7,	KEY_8,	KEY_9,	KEY_10=0x0a, 
	KEY_11, KEY_12, KEY_13, KEY_14, KEY_15=0x0f, KEY_16=0x10,
	/*! 0x11 */
	KEY_UP,		KEY_DOWN,	KEY_LEFT,	KEY_RIGHT,
	KEY_PGUP,	KEY_PGDN, 	KEY_RET,	KEY_ESC,

	KEY_PLAY=0x19,	KEY_STOP=0x1a,	KEY_FAST,	KEY_SLOW,
	KEY_PREV,	KEY_NEXT,	KEY_SPLIT=0x1f,	/*! 0x20 */KEY_ADDR,
	KEY_REC,	KEY_FUNC,	KEY_ALARM,	KEY_SHUT,	KEY_SEARCH,

	KEY_LOCK,	KEY_10PLUS,	KEY_SHIFT,	KEY_BACK,
	KEY_STEPX,	KEY_MENU,	KEY_INFO,	KEY_SPLIT1,	
	KEY_SPLIT4,	KEY_SPLIT8,	KEY_SPLIT9,	KEY_SPLIT16,
	KEY_STEPF,	KEY_STEPB,  KEY_PAUSE,

	//add by xie 
	KEY_MAIN = 0x35,            //右键菜单	
	//end
	
	KEY_SWITCH=0x37,         //GUI控制切换
	KEY_VIDEOOUT=0x38,         //VIDEOOUT 从1080p-720p-1024*768切换
	KEY_AUDIO=0x39,          //回放时，静音和开启。开启或关闭某一通道的某一通道的录音
	KEY_ADDVOL=0x3a,
	KEY_SUBVOL=0x3b,
	KEY_ADDCH=0x3c,
	KEY_SUBCH=0x3d,	
	KEY_CLEARALARM=0x3e,		///< 消除报警(包含报警输出和蜂鸣)
	KEY_NEXTFRAME=0x3f,			///< 下一帧
	KEY_PREVFRAME=0x40,			///< 上一帧

//APHD USB键盘快捷键
#ifdef _VENDOR_APHD_CHNAME
	KEY_CTRL1=0X70,
	KEY_CTRL2=0X71,
	KEY_CTRL3=0X72,
	KEY_CTRL4=0X73,
	KEY_CTRLS=0X74,
	KEY_CTRL5=0X75,
	KEY_CTRL6=0X76,
	KEY_CTRL7=0X77,
	KEY_CTRL8=0X78,
#endif	
	
	KEY_AUTOTUR = 0xa0, /*!< 自动轮巡 */

	//新增加A系列的遥控键值
	//紧急呼叫
	KEY_EMERGENCY_CALL = 0xa8,
	
	//安全模式
	KEY_SAFE_MODE = 0xa9,
	
	//布防,
	KEY_ARMING = 0xaa,

	//撤防
	KEY_DISARMING = 0xab,


	KEY_PTZ = 0xb0, /*!< 云台控制菜单 */
	KEY_TELE, KEY_WIDE, /*!< 变倍 */
	KEY_IRIS_CLOSE, KEY_IRIS_OPEN, /*!< 光圈 */
	KEY_FOCUS_NEAR,	KEY_FOCUS_FAR, /*!< 聚焦 */
	KEY_BRUSH, /*!< 雨刷 */
	KEY_LIGHT, /*!< 灯光 */
	KEY_AUTOPAN, /*!< 线扫*/
	KEY_SCAN, /*!< 点间巡航 */
	KEY_PATTERN, /*!< 模式 */
	KEY_SPRESET, /*!< 设置预置点 */
	KEY_GPRESET, /*!< 转到预置点 */
	KEY_DPRESET, /*!< 删除预置点 */

	//球机菜单(0x30 - 0x32)
	KEY_MACHINE_MENU = 0xc0, KEY_MACHINE_PREV, KEY_MACHINE_NEXT,

	KEY_BACKUP = 0xdf,

	// 恢复出厂设置
	KEY_DEFAULT_CFG = 0xee,
	KEY_FORWARD = 0xf1,
	KEY_BACKWARD = 0xf2,
	KEY_ALARMIN = 0xfe,
};

enum fb_cmd_t
{
	FB_KEEPALIVE = 0,
	FB_REBOOT,
	FB_SHUTDOWN,
	FB_LED_MAINMENU=0x04,
	FB_LED_PTZ=0x05,
	FB_LED_READY = 0x10,
	FB_LED_ALARM = 0x11,
	FB_LED_RECORD = 0x12,
	FB_LED_HDD = 0x13,
	FB_LED_NETWORK = 0x14,
	FB_LED_STATUS = 0x15,
	FB_LED_CHANNEL=0x16,
	FB_LED_END,
	LED_REMOTE = 0x50,
};

/*键值标志
+------+----+----+------+
|  7-4 | 3  |2--1|  0   |
|------|----|----|------|
| 保留 |遥控|速度|飞梭键|
+------+----+----+------+
*/
enum fb_flags_t {//消息参数中的一些标志
	FB_FLAG_NONE = 0x00,
	FB_FLAG_SPEC = 0x01,
	FB_FLAG_SPEED = 0x06,
	FB_FLAG_SPEED1 = 0x00,
	FB_FLAG_SPEED2 = 0x02,
	FB_FLAG_SPEED3 = 0x04,
	FB_FLAG_SPEED4 = 0x06,
	FB_FLAG_REMOTE = 0x08,
};

//遥控器输入键值
#define RKEY_START	0x80

typedef TSignal3<uint, uint, uint>::SigProc SIG_DEV_FB_INPUT;

class CDevFrontboard : public CThread
{
public:
	CDevFrontboard();
	virtual ~CDevFrontboard();
private:
	VD_BOOL Translate(uchar * pKeys);

public:
	void LigtenLed(uint iLed, VD_BOOL bOpen, uint iChn = 0);
	void LigtenLedAll(VD_BOOL bOpen);
	void Shutdown();
	void Reboot();
	void OnRemoteKeyUp(uint param);
	VD_BOOL AttachInput(CObject * pObj, SIG_DEV_FB_INPUT pProc);
	VD_BOOL DetachInput(CObject * pObj, SIG_DEV_FB_INPUT pProc);
	virtual void ThreadProc();
	static CDevFrontboard* instance(void); 

	void OnStartAlive();
	void OnStopAlive();
	void KeepAlive();

protected:
private:
	TSignal3<uint, uint, uint> m_sigInput;
	uint	m_dwLedState;		//指示灯状态
	CTimer	m_RemoteKeyTimer;
	CMutex m_Mutex;
	static CDevFrontboard* _instance;
	
};

#endif// __DEV_FRONTBOARD_H__

