#ifndef DEVINFO_H_
#define DEVINFO_H_

#define AUDIO_SWITCH_FILE  	"/mnt/temp/AudioSwitch.ini"

void JudgeFileIsExis(int type, int data);
char * ReadVersionToFile();

#ifdef SUNING
int  GetSystemPreVer(char* Ver,int Len);
int  Get_PreVersion_Path(char *pVerPath, int nSize);
#endif
int  Get_Version_Name(char *pVerName, int nSize);
int  Get_Version_Path(char *pVerPath, int nSize);
int  Get_Video_Nfps_Igop(int *pnfps, int *pigop); //获取帧率和I帧间隔

int GetDevModel(char* Model,int Len);
int GetSystemVer(char* Ver,int Len);
int GetConnWifi(char* WifiName,int Len);
int GetConnWifiPwd(char* WifiPwd, int Len);
int GetWifiValue(int *Strength);
int GetDeviceNum(char* Imei,int Len);
int GetDevIp(char* Ip,int Len);
int GetDevMac(char* Mac,int Len);

int  GetDevVersion(char *pVerPath,int iVerMaxLen/*pVerPath 最大可支持长度*/);
#endif
