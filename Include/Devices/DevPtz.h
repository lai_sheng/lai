

#ifndef __DEV_PTZ_H__
#define __DEV_PTZ_H__

#include "APIs/Ptz.h"
#include "APIs/Comm.h"
#include "MultiTask/Thread.h"
#include "System/Object.h"
#include "System/Signals.h"
#include "config-x.h"

class CDevPtz : public CThread
{	
public:
	CDevPtz();
	virtual ~CDevPtz();
	typedef TSignal2<void *, int>::SigProc SIG_DEV_PTZ_DATA;
	
	VD_BOOL Start(CObject * pObj, SIG_DEV_PTZ_DATA pProc);
	VD_BOOL Stop(CObject * pObj, SIG_DEV_PTZ_DATA pProc);

	int Open(void);
	int SetAttribute(PTZ_ATTR *pAttr);
	int SetPTZHead(void *sbuf,int size);
	int Write(void* pData, uint len,int iCh=0);
	int Read(void *pData, int len);
	int Close(void);

	void SetDebug(VD_BOOL bNeedDebug);
	static CDevPtz* instance(void); 
private:
	void ThreadProc();
private:
	uchar m_Buf[64];
	int m_Len;
	PTZ_ATTR	m_Attr;

	TSignal2<void *, int> m_sigData;

	CSemaphore		m_Semaphore;
	VD_BOOL			m_bNeedBlocked;
	VD_BOOL			m_debug;
	static CDevPtz* _instance; 
};

#endif// __DEV_PTZ_H__

