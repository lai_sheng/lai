
#ifndef __DEV_CAPBUF_H__
#define __DEV_CAPBUF_H__

#include "System/Packet.h"
#include "System/list.h"

typedef struct bufferBlock
{
	CPacket* 			pPacket;		///< the addr of packet
	int 				iChannel;		///< 通道号	
	struct 	list_head 	timeList;		///< 按时间存放的队列顺序
	struct	list_head 	channelList;	///< 按通道存放的队列顺序
}BUFFER_BLOCK;

class CDevBufManager
{
public:	
	/// 存放数据
	int pushBuffer(CPacket *pPkt, int iChannel);
	
	/// 获得录像数据
	int popBuffer(CPacket **pPkt, int iChannel);	
	
	/// 设置录像数据
	void setBuffer(int iSeconds, int iChannel);
	
	/// 清除数据
	void clear(int iChannel);
	
	void dump();

	static CDevBufManager* instance(void);
	
private:
	CDevBufManager();
	~CDevBufManager();
	
	void delList(struct list_head *pFrom, struct list_head *pTo);

private:
	BUFFER_BLOCK*		m_pBuffer;     						///< 整个缓冲区
	uint				m_iSize;							///< 缓冲区的长度
	uint				m_iLength;								///< 使用的空间大小，以字节为单位
	
	struct 	list_head 	m_prerecordList;					///< 预录队列
	struct	list_head	m_channelList[N_SYS_CH];			///< 通道队列，供预录和录像同时使用
	struct	list_head	m_recordList;						///< 录像队列
	struct	list_head	m_emptyList;						///< 空队列

	VD_BOOL				m_bDataGetting[N_SYS_CH];						

	CMutex				m_bufferMutex;
	static CDevBufManager *_instance;
};

#endif
