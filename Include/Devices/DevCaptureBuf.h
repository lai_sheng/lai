
#ifndef _DEV_CAPTURE_BUF_H
#define _DEV_CAPTURE_BUF_H

#include "System/Packet.h"
#include "System/Object.h"
#include "System/Signals.h"
#include "MultiTask/Thread.h"

enum PUSHDATATYPE
{
	PUSH_DATA_ERROR = -1,
	PUSH_DATA_SUCCESS = 0,
	PUSH_DATA_FULL = 1,
};

enum DATATYPES
{
	STORE_DATA_NONE = 0,
	STORE_DATA_PRE = 1,
	STORE_DATA_REC = 2,
};

enum GETDATATYPE
{
	GET_DATA_ERROR = -1,
	GET_DATA_SUCCESS = 0,
};

typedef struct datablock  
{
	CPacket *pPkt;
	int		iChannel;
	uchar	flag;				/*!< 1为预录，2为录像 */
	struct datablock *pre;
	struct datablock *next;
}DataBlock;

//! 默认的环形队列大小
const int nDefaultCircleQueueSize = 50;

//! 环形模板类
template <class T> class CCircularQueue 
{
public:
	CCircularQueue(){};
	~CCircularQueue()
	{
		if (CQList) 
		{
			delete [] CQList;
		}
		front = 0;
		rear = 0;
		size = 0;
		count = 0;
	}

	void init(int initSize = nDefaultCircleQueueSize)
	{
		if (initSize < 1)
		{
			initSize = nDefaultCircleQueueSize;
		}
		CQList = new T[initSize];
		if (!CQList) 
		{
			trace("new error\n");
			exit(1);
		}
		front = 0;
		rear = 0;
		size = initSize;
		count = 0;
	}

	//判断队列是否为空
	int empty()
	{
		return count == 0;
	}

	int getlength()
	{
		return count;
	}

	int getsize()
	{
		return size;
	}

	//! 获得数据，但不从队列中清除,pos为正数，从头到尾，负数，从尾到头
	int getdata(T &data, int pos)
	{
		int index;
		int tmprear = rear < front ? rear + size : rear;
		if (tmprear + pos < 0)
		{
			return 1;
		}
		if (pos >= 0)
		{
			index = (front + pos) % size;
		}
		else
		{
			index = (tmprear + pos) % size;
		}
		data = CQList[index];
		return 0;
	}

	//一端放入数据
	void push(const T &item)
	{
		if (front == (rear + 1) % size) 
		{
			trace("Queue is full!!!\n");
			return;
		}
		CQList[rear] = item;
		count++;
		rear = (rear + 1) % size; //rear始终指向最后一个元素的下一个位置
	}

	//! 取数据
	void pop(T &data)
	{
		if (front == rear)
		{
			trace("the Queue is empty, and can't delete it\n");
			return;
		}

		data = CQList[front];
		count--;
		front = (front + 1) % size; //front移向下一位置
	}

	void clear()
	{
		rear = front;
		count = 0;
	}

	void erase(int num)
	{
		if (num <= 0)
		{
			return;
		}

		if (num > count)
		{
			num = count;
		}

		front = (front + num) % size; 
		count -= num;
	}


private:
	T   *CQList;			/*!< 存放队列元素的指针(数组) */
	int size;				/*!< 队列容量 */
	int front;				/*!< 队首位置 */
	int rear;				/*!< 队尾位置(最后一个元素的下一位置) */
	int count;				/*!< 添加数据的个数 */
};

class CBufferManager;

/*!
\class CDSPBuffer
\brief 从驱动获得数据的缓冲区，供其他调用者调用数据
*/
class CBuffer 
{
	friend class CBufferManager;
private:	
	//! 完成初始化
	CBuffer();

	//! 释放资源
	~CBuffer();
	
	void Wait();

	//! 初始化
	void Init(uint length, int iChannel);

	//! 保存数据
	int PushData(CPacket *pkt, VD_BOOL overwrite);

	//! 获得数据
	int PopData(CPacket **pkt);

	//! 移除部分数据
	void RemovePartialData();

	//! 清除数据
	void ClearData();

	uint GetLength();

	void SetIFrameNum(int number);


private:
	int						m_iChannel;
	uint					m_MaxLength;
	uint					m_Length;			/*!< 每个队列里已经保存的数据长度 */

	uint					m_Size;				/*!< 队列数据最大个数，以１K为单位  */

	DataBlock				*m_pDataBlock;
	int						m_front;
	int						m_rear;
	
	CSemaphore				m_DataSemaphore;				/*!< 取数据为空时阻塞*/
	bool					m_State;

	uchar					m_IFrameNum;
	
	
};

class CDevCapture;
class CBufferManager : public CThread
{
public:
	//! 构造函数
	CBufferManager();
	
	//! 析构函数
	~CBufferManager();

	//static CBufferManager * Instance();

	void Init(int iChannel, uint length, CDevCapture *pDevCapture);

	//! 得到预录包的大小
	uint GetLength(int iChannel);

	void SetIFrameNum(int iChannel, int number);

	//! 保存数据
	void PushData(int iChannel, CPacket *pkt);

	//! 清除数据
	void ClearData(int iChannel);

	void Start(int iChannel);

	void Stop(int iChannel);
private:
	void WakeUp(int iChannel);

	//! 线程的执行体
	void ThreadProc();

	VD_BOOL TestRecDataEmpty();

private:
	//static CBufferManager *	m_pInstance;
	CBuffer				m_Buffer[N_SYS_CH];
	CDevCapture	*		m_pDevCapture[N_SYS_CH];
	uint					m_iUser;				

	CMutex					m_BufferMutex;
	CSemaphore				m_Semaphore;				/*!< 取数据为空时阻塞*/
};


#endif

