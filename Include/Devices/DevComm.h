

#ifndef __DEV_COMM_H__
#define __DEV_COMM_H__

//#include <unistd.h>
#include "APIs/Comm.h"
#include "MultiTask/Thread.h"
#include "System/Object.h"
#include "System/Signals.h"

class CDevComm : public CThread
{
public:
	typedef TSignal2<void *, int>::SigProc SIG_DEV_COMM_DATA;
public:
	CDevComm();
	virtual ~CDevComm();
	
	int Start(CObject * pObj, SIG_DEV_COMM_DATA pProc);
	int Stop();
	
	int SetAttribute(COMM_ATTR * pAttr);
	
	int Read(void* pData, uint nBytes);
	int Write(void* pData, uint len);	
	
	int Purge (uint dwFlags);
	int AsConsole(int flag);
	void GetConsole(int * value);

	void TestData(void *pdata, int len);
	static CDevComm* instance(void); 

private:
	void ThreadProc();

	int Open();
	int Close();	
	
private:
	TSignal2<void *, int> m_sigData;
	static CDevComm* _instance; 

	CObject			*m_pObj;
	SIG_DEV_COMM_DATA m_pSigDev;

	CSemaphore		m_Semaphore;
	VD_BOOL			m_bNeedBlocked;

};

#endif// __DEV_COMM_H__

