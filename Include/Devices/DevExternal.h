#ifndef __DEV_EXTERNAL__
#define __DEV_EXTERNAL__

#include "APIs/ExtDev.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"
#include "APIs/Types.h"

#include "Configs/ConfigAwakeTime.h"
#include "Intervideo/RealTime_apiv1.h"

#ifdef MOBILE_COUNTRY
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#endif 

int GetUuid(char uuid[16]);

typedef enum {
	BAT_ALARM_TYPE_CAMAER = 0,
	BAT_ALARM_TYPE_LOCK
}BAT_ALARM_TYPE;

class CDevExternal : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CDevExternal);
	CDevExternal();
	virtual ~CDevExternal();

	VD_BOOL Init();
		
	VD_BOOL Start();
	
	VD_BOOL Stop();

	int Dev_GetElecQuantity(ZRT_CAM_BAT_Status *Stauts); //获取电量

	int Dev_GetWiFi(ZRT_CAM_WIFI_Status *Status); //获取WiFi状态

	int Dev_GetPirStatus(int* Status); //获取PIR状态
	
	int Dev_SetPirLevel(int pirLevel);
	int Dev_GetPirLevel(int *pPirLevel);
	int Dev_SetPowerSaveLevel(int powerLevel);
	int Dev_GetPowerSaveLevel(int *pPowerLevel);
    VD_BOOL Dev_SetDefaultPirLevelConfig(void);

	int Dev_GetKeyEnableStatus(int *Status);

	int Dev_SetKeyEnableStatus(int Status);

	void Dev_EnableSleepMode();  //使能休眠模式

	void Dev_UnableSleepMode();

	void Dev_SetSleepTime(int Val); //设置唤醒到睡眠的时间差

	int Dev_GetWakeUpFlag();

	VD_BOOL SendMsgToTerminal(char cmd,char *buf,int len);
	
	VD_BOOL EnterSleepMode();
	
	VD_BOOL PaassCMDSleepMode(){m_iSleepMode = TRUE;}; //区分自动休眠

	int Dev_SetAlarmTime();

	void Dev_SetManualPirStatus(int cfg){ManualPirCfg = cfg;};	
	void Dev_GetManualPirStatus(int &cfg){if( 0 != ManualPirCfg && 1 != ManualPirCfg){cfg = PirEnable;}else cfg = ManualPirCfg;};	
    int CheckBatAlarmTime(BAT_ALARM_TYPE alarmType);
    MOTION_STATE_E Dev_GetPirMotionState(void);

	VD_BOOL Dev_PushAlarmStart();
	VD_BOOL Dev_PushAlarmEndWithRecord();
private:
    int UpdateBatAlarmTimeConfig(BAT_ALARM_TYPE alarmType);
	int onConfigAwake(CConfigAwakeTime& cCfgAwe, int& iRet);
	int onConfigPirLevel(CConfigPirLevel& cCfgPirLevel, int& iRet);
	int Dev_SetPirStatus(int Status); //设置PIR状态	
	int Dev_SetPirTime();
	void OnDevExtTimer(uint arg);
	void ThreadProc();	
public:
	CConfigAwakeTime m_cCfgAwe;
private:
	/*
		手动强制设置PIR状态
		若为1,则睡眠时强制PIR状态为使能
		若为0,则睡眠时强制PIR状态为不使能
		若为其它值，则不操作
	*/
	int ManualPirCfg;
	
	int PirEnable;
	MOTION_STATE_E m_PirMotionState;
		
	int EnableSleep;		//休眠使能
	int ToSleepTime;		//距离休眠的时间
	int CurRunCnt;
	int SockFd;
	int m_iSleepMode;

	unsigned int WakeUpTime;
	
	SYSTEM_WAKEUP_FLAG m_WakeUpFlag;
	CConfigPirLevel m_cCfgPirLevel;

	CTimer m_DevExtTimer;
};

#define g_DevExternal (*CDevExternal::instance())

#endif// __DEV_EXTERNAL__

