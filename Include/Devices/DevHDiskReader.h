
#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

#ifndef _DEVHDISKREADER_H
#define _DEVHDISKREADER_H

#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "System/Object.h"
#include "System/Signals.h"
#include "System/Packet.h"

#include "System/BaseTypedef.h"

#include "Functions/FFFile.h"

enum HDISKREADER_CMD
{
	HDISKREADER_CMD_START,
	HDISKREADER_CMD_STOP,
	HDISKREADER_CMD_PAUSE,
	HDISKREADER_CMD_SEEK,
};

enum RETURN_FUNC_CMD
{
	HDISKREADER_CMD_ONPACKET,
	HDISKREADER_CMD_FINISHED,
	HDISKREADER_CMD_NOPACKET,
};

enum EnFrameType
{
	FRAME_TYPE_TRUE_VALUE,
	FRAME_TYPE_NET_VALUE,
};

class CDevHDiskReader : public CThread
{
public:
	CDevHDiskReader();
	virtual ~CDevHDiskReader();

	// cmd, packet, ch, user
	typedef TSignal4<int, CPacket *, int, int>::SigProc SIG_DEV_HDISK_READER;

	VD_BOOL Start(CObject * pObj, SIG_DEV_HDISK_READER pProc, int iCh=0, int iDlg=0);
	VD_BOOL Stop(CObject * pObj, SIG_DEV_HDISK_READER pProc);

	void ThreadProc();

//	VD_BOOL Locate(uint iDisk, uint iCluster, SYSTEM_TIME *pST = NULL);
	VD_BOOL Locate(uint iDisk, uint iPart,uint iCluster, SYSTEM_TIME *pST);
	VD_BOOL Locate(int iCh, SYSTEM_TIME *pST);
	VD_BOOL NewLocate(uint iDisk, uint iCluster, SYSTEM_TIME *pST = NULL,SYSTEM_TIME *pET = NULL);
	VD_BOOL NewLocate(uint iDisk,uint iPart, uint iCluster, SYSTEM_TIME *pST = NULL,SYSTEM_TIME *pET = NULL);
	VD_BOOL NewLocate(int iCh, SYSTEM_TIME *pST,SYSTEM_TIME *pET);
	int Seek(int iOffset, uint iFrom = 0); 
	VD_BOOL SeekTime(int iOffset, uint iFrom);
	VD_BOOL SeekDHTime(SYSTEM_TIME * psSysTime);
	int GetFrameRate();
	int GetNetFrameRate();
    VD_BOOL ResetPacketIdx();

    /* 用户直接读取 */
	VD_BOOL Read(void *pBuffer, uint length);

protected:

private:

	/*! 打开文件
	  * \param  void
	  * \return void
	  */
	VD_BOOL Open();
	VD_BOOL Close();
	VD_BOOL Read();
	void FrameRate();

	TSignal4<int, CPacket *, int, int> m_sigBuffer;

	FFFile *m_pDHFile;
	
	// 文件信息
	STM_INFO *m_pDHFileInfo;

	CMutex m_Mutex;

	int m_iCh;
	int m_iDlg;

	int m_iNetFrameRate;
	int m_iFrameRate;

	uint m_iPacketIdx;

	int m_iLimitSpeed;

	VideoFileReader m_vReader;
};

#endif // _DEVHDISKREADER_H
