

#ifndef __DEV_PREVIEW_CAPTURE_H__
#define __DEV_PREVIEW_CAPTURE_H__

#include "APIs/ExCapture.h"
#include "APIs/Capture.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "System/Object.h"
#include "System/Signals.h"
#include "System/Packet.h"

class CExCaptureManager;
class CDevExCapture : public CThread
{
public:
	typedef TSignal2<int, CPacket *>::SigProc SIG_DEV_CAP_BUFFER;

	CDevExCapture(int iChannel);
	~CDevExCapture();
	static	int GetChannels ();// 得到通道数目
	static	VD_BOOL GetCaps(CAPTURE_CAPS * pCaps);// 捕获设备特性
	VD_BOOL SetVstd(uint dwStandard);	// 设置
	VD_BOOL SetFormat(uchar Compression, uchar BitRateControl, uchar ImageSize, uchar ImageQuality,uchar FramesPerSecond);// 设置编码格式
	VD_BOOL SetColor(uchar Brightness, uchar	Contrast, uchar Saturation, uchar Hue, uchar Gain);// 设置颜色
	VD_BOOL SetTime(SYSTEM_TIME * pSysTime);
	VD_BOOL Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags);// 启动捕获
	VD_BOOL Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc);
	
	//! 设置I帧
	VD_BOOL SetIFrame();

	uint GetBitRate();
	virtual void ThreadProc(){};
	void OnData(CPacket *);

	static CDevExCapture* instance(int iChannel); 
private:
	int		m_iChannel;
	uint	m_dwBytes;
	uint	m_dwBytesOld;
	uint	m_dwBytesMask;
	uint	m_Time;
	TSignal2<int, CPacket *> m_sigBuffer;

	static CExCaptureManager* m_pManager;
	static CDevExCapture* _instance; 
};

class CExCaptureManager : public CThread
{
public:
	CExCaptureManager();
	~CExCaptureManager();
	void Start();
	void Stop();
	void Init(int iChannel, CDevExCapture *);
	void ThreadProc();
protected:
private:
	CDevExCapture * m_pDevCap[N_SYS_CH];
	int m_iUser;
	CMutex m_Mutex;
};

#endif// __DEV_PREVIEW_CAPTURE_H__
