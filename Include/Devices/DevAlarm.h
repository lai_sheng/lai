

#ifndef __DEV_ALARM_H__
#define __DEV_ALARM_H__

#include "APIs/Alarm.h"
#include "System/Object.h"

class CDevAlarm : public CObject
{
public:
	CDevAlarm();
	virtual ~CDevAlarm();
	static int GetInSlots ();	// 得到报警端口的输入通道数目
	static int GetOutSlots();	// 得到报警端口的输出通道数目
	uint GetResetState();		// 复位按键的状态
	uint GetInState();			// 每个输入端口的状态，每一位表示一个通道
	uint GetOutState();		// 每个输出端口的状态，每一位表示一个通道
	void SetOutState(uint dwState);			// 直接设置报警输出
	void AlarmOut(uint dwChannel, VD_BOOL bOpen);	// 启动报警输出，slot是报警输出各通道的掩码
	static CDevAlarm* instance(void); 

protected:
private:
	uint m_dwResetState;		// 复位键状态
	uint m_dwInState;			// 输入端口状态
	uint m_dwOutState;			// 输出端口状态,1表示取这路报警
	uint m_dwSponsor[32];		// 每个输出通道的发起数目
	static CDevAlarm *_instance; 
};

#endif// __DEV_ALARM_H__
