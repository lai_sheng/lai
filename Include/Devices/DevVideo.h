

#ifndef __DEV_VIDEO_H__
#define __DEV_VIDEO_H__

#include "APIs/Video.h"
#include "System/CMOS.h"
#include "Configs/ConfigVideoColor.h"

VD_BOOL GetCaps(WHITE_LEVEL_CAPS * pCaps);

class CDevVideo
{
public:
	CDevVideo(int video);
	virtual ~CDevVideo();

public:
	//!  得到通道数目
	static	int 	GetChannels ();		

	VD_BOOL SetControl(int channel, VIDEO_CONTROL* cVCtrl);
	VD_BOOL SetColor(int channel, VIDEOCOLOR_PARAM color);

	VD_BOOL SetCover(int channel, int index, VD_RECT *pRect, uint Color, int Enable);
	VD_BOOL SetVstd(int channel, uint dwStandard);	
	void WriteRegister(uchar Chip, uchar Page, uchar Register, uchar Data);
	uchar ReadRegister(uchar Chip, uchar Page, uchar Register);
	VD_BOOL Switch(uint dwType);
	VD_BOOL SetTVMargin(uchar left, uchar top, uchar right, uchar bottom);
	VD_BOOL SetCropRegion(int channel,int Hoffset,int Voffset,int Width,int Height);
	VD_BOOL SetTVColor(uchar brightness, uchar contrast);
	VD_BOOL SetTVAntiDither(uchar level);
	VD_BOOL SetMatrix(uchar channelin,uchar channelout);
	//BOOL SetOutput(int output);
	VD_BOOL SwitchOutput(int type = VIDEO_OUTPUT_AUTO);
	VD_BOOL InitOutput();
	void SwitchTVMonitor(VD_BOOL open);
	void SwitchTVMonitor();
	VD_BOOL GetTVMonitorState();
	VD_BOOL SetBkColor(VD_COLORREF color);

	VD_BOOL SamplingModeSwitch(int channel, int size);

	static CDevVideo* instance(int index = 0); 

/*add by yanjun 20061124 */
	uchar GetCurrentOuputMode();
private:
	uchar m_CurrentOutputMode;
/*end*/
private:
	uchar m_Output;
	CCMOS m_CMOS;
	VD_BOOL m_bTVMonitorOpened;
	int	m_index;
	static CDevVideo* _instance[N_VIDEO_OUT]; 
};

#endif// __DEV_VIDEO_H__

