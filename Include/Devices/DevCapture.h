
#ifndef __DEV_CAPTURE_H__
#define __DEV_CAPTURE_H__

#include "System/Signals.h"
#include "APIs/Capture.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "System/Object.h"
#include "System/Packet.h"
#include "System/Locales.h"
#include "Media/ICapture.h"


extern int g_nCapture;
//! 叠加的字体长度
#define RASTER_LENGTH 1024

//! 数据被使用的类型
enum DataUsedType
{
	DATA_READY =0x0,			/*!< 准备状态  */
	DATA_BUFFER = 0x01,			/*!< 预录使用  */
	DATA_RECORD = 0x02,			/*!< 录像使用　*/
	DATA_MONITOR = 0x04,		/*!< 监视使用  */
	DATA_2SND = 0x08,		    /*!< 辅码流录像  */
	DATA_ALL = 0xf				/*!< 所有使用者 */
};

//!  捕获设备特性
VD_BOOL GetCaps(CAPTURE_CAPS * pCaps);

//!  获得捕获的扩展功能
VD_BOOL GetExtCaps(CAPTURE_EXT_STREAM * pCaps);	

//! 得到支持的编码能力*/
VD_BOOL GetDspInfo(CAPTURE_DSPINFO *pDspInfo);

VD_BOOL GetChnCaps(int chn, CAPTURE_CAPS * pCaps);

class CCaptureManager;
class CDevBufManager;
/*!
	\class CDevCapture
	\brief 捕获设备
*/
class CDevCapture : /*public CThread,*/ public ICapture
{
	friend class CCaptureManager;

public:
	//! 定义回调函数类型
	typedef TSignal3<int, uint, CPacket *>::SigProc SIG_DEV_CAP_BUFFER;
	
	//! 构造函数
	CDevCapture(LOGIC_CHN iChannel, ANALOG_CHN analog_chn);

	//! 析构函数
	~CDevCapture();

	/*int BindAnalogChn(ANALOG_CHN analog_chn);*/
	ANALOG_CHN& GetAnalogChn();

	//!  得到通道数目
	static	int 	GetChannels ();		

	//! 检测状态，如果出错则重启
	static void 	Synchronize();

	//! 设置制式
	VD_BOOL SetVstd(uint dwStandard);
	//!初始化抓拍设置
//#ifdef LINUX
//	VD_BOOL CreatSnap(void);
//#endif

	//! 设置区域遮挡
	VD_BOOL SetCover(VD_RECT *pRect, uint Color, int Enable, int index = 0);

	//! 设置时间
	VD_BOOL SetTime(SYSTEM_TIME * pSysTime);

	//! 设置编码格式
	VD_BOOL SetFormat( CAPTURE_FORMAT *pFormat, uint dwType = CHL_MAIN_T);	
	
	//! 获得底层的分辨率
	VD_BOOL getImageSize(capture_size_t* pSize, uint dwType = CHL_MAIN_T);
	
    //! 获取编码格式
    const CAPTURE_FORMAT * GetFormat(uint dwType = CHL_MAIN_T) const;

	//! 设置I帧
	VD_BOOL SetIFrame(uint dwStreamType = CHL_MAIN_T);

#if 0
	VD_BOOL SetTimeTitle(int x, int y, int length);
	VD_BOOL SetChanTitle(int x, int y, char * str, int length);
	VD_BOOL SetChanTitle(int x, int y, int length);
	VD_BOOL ReSetTimeTitle();
	void SetTimeTleEn(uchar enable);
	void SetChanTleEn(uchar enable);
#endif
	//! 设置标题
	VD_BOOL SetTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect, VD_PCSTR str = NULL, FONTSIZE fontsize = FONTSIZE_NORMAL);
 
	//! 启动捕获，开始取数据
	VD_BOOL Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags,uint dwStreamType = CHL_MAIN_T);
	
	//! 启动捕获，开始取数据
	VD_BOOL Start(uint dwStreamType = CHL_MAIN_T);

	//! 停止捕获，关闭取数据
	VD_BOOL Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags, uint dwStreamType = CHL_MAIN_T);

	//! 停止捕获，关闭取数据
	VD_BOOL Stop(uint dwStreamType = CHL_MAIN_T);
	
	//! 获得捕获状态
	VD_BOOL GetState(uint dwStreamType = CHL_MAIN_T);

#ifdef ENC_ENCODE_MULTI
	VD_BOOL extStart(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwStreamType = CHL_MAIN_T);// 启动捕获
	VD_BOOL extStop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwStreamType = CHL_MAIN_T);
	void OnExtData(CPacket *pPacket, uint dwType);
#endif

	uint GetBufferLength();
	
	//! 设置I帧个数，实际上是预录秒数
	void  SetIFrameNum(int number);
	
	//! 更新一下缓冲区，主要是按预录时间调整
	VD_BOOL adjustPreBuffer();

	//! 供外部接口调用获得码流
	uint GetBitRate(int dwType = CHL_MAIN_T);
	
	//! 计算码流
	void CalculateBitRate();
    
	//! 计算码流1
	void CalculateExBitRate();
	
	////! 捕获的线程执行体
	//void ThreadProc();

	//! 捕获数据处理过程
	void OnData(CPacket *pPacket,uint dwType);

	//! 录像数据处理
	void OnRecData(CPacket ** pPacket);

	//! 取得捕获设备的音量
	int GetVolume(int *pVolume);
	// 获取监视数 added by billhe at 2009-5-20
	uint GetMonitorCount();

#ifdef _2761_EXCEPTION_CHK_
	//2761现在存在音视频不稳定的情况
	//若frame数不增长则赋给不正常的错误码
	void ChkAVException();

	int GetAVException();
#endif

	//设置待机状态 ,待机状态下全部码流不推流
	void SetSuspend(int suspend);
//#ifdef DVR_GB
//	//! 发送抓图命令
//	VD_BOOL SetSnapParam(char imgq, int second,  int millisecond);
//
//	//! 获取图片数据
//	VD_BOOL GetSnapBuffer(CPacket **pPkt);
//#endif
	//! 唯一实例
	static CDevCapture* instance(LOGIC_CHN iChannel); 

	static CCaptureManager* getManager();
	//! 计算点阵
	static void TextToRaster(uchar * pBuffer, VD_PSIZE pSize, VD_PCRECT pRect, VD_PCSTR str, FONTSIZE fontsize = FONTSIZE_NORMAL);

private:	
	// 进行帧信息统计
	void statFrameInfo(CPacket *packet);

private:
	LOGIC_CHN	m_logic_chn; //!对应的逻辑通道号, 从0开始，-1:没有设置
	ANALOG_CHN 	m_iChannel;		/*!< 通道号 */
	uint		m_dwBytes;		/*!< 码流总和  */
	uint		m_dwMS;			/*!< 取码流的间隔时间 */
	uint		m_BitRate;		/*!< 码流值 */
    
	uint		m_dwExBytes;		/*!< 码流总和  */
	uint		m_dwExMS;			/*!< 取码流的间隔时间 */
	uint		m_ExBitRate;		/*!< 码流值 */
	uint        m_dwMonitorCnt;     /* 监视连接数 added by billhe at 2009-5-20 */
#if 0
	uchar	m_TimeTleEn;
	uchar	m_ChanTleEn;
	int		m_TimeTitleX;
	int		m_TimeTitleY;
	int		m_TimeTitleW;
	char		m_TitleStr[NAME_LEN + 1];				/*!< 通道标题字符串 */
#endif

	uchar		m_RasterBuf[RASTER_LENGTH];				/*!< 保存点阵的缓冲 */

	TSignal3<int,uint, CPacket *> *m_psigMonitor[CHL_FUNCTION_NUM]; /*!< 监视的回调函数指针*/
	TSignal3<int,uint,  CPacket *> m_sigRecord;		/*!< 录像的回调函数指针 */

	uint				m_MonStreamType;				/*!< 开启监视的状态 用位来表示*/
	uint				m_RecType;						/*!< 开启录像的状态, 用位表示预录和录像 */
	uint				m_RecType2snd;                  /*!< 开启录像的状态, 用位表示预录和录像 对应辅码流*/
	int					m_preSeconds;					///< 预录时间
	VD_BOOL				m_preBufferState;

	//static CBufferManager	m_BufferManager;			/*!< 数据缓冲区 */
	static CDevBufManager	*m_pBufferManager;			///< 数据缓冲区
	static CCaptureManager  *m_pManager;				/*!< 捕获设备的全局管理对象 */
	static CDevCapture* _instance[N_SYS_CH];			/*!< 捕获设备的指针 */

#ifdef ENC_ENCODE_MULTI
	TSignal3<int,uint, CPacket *> m_sigExtRecord;
#endif	
	CAPTURE_FORMAT m_captureFormat[CHL_FUNCTION_NUM];

	// 码流信息统计
	enum{maxFrameType = 5};
	struct FrameStatInfo
	{
		int frameNumber;
		int totalSize;
		int maxSize;
	}m_frameStatInfo[maxFrameType];
	int m_packetNumber;
	int m_maxFramesInPacket;
	int m_totalFrames;
	int m_firstIPacketTime;
	int m_IFrameDelayMax;

#ifdef _2761_EXCEPTION_CHK_
	//音视频异常状态，0表示正常，第一位表示视频，第二位表示音频
	//如1表示视频不正常，2表示音频不正常
	//之所以不用原来计算的比特率，是原来的计算中音视频混在一起
	//无法单独判断音频或视频是否正常
	int m_AVExceptionStatus;  
	int m_LastAudioFrames;
	int m_LastVideoFrames;
	//记录不正常状态的次数
	int m_ExceptionTimes;
#endif

private:
	CMutex	m_Mutex;
};

/*!
	\class CCaptureManager
	\brief 实现对DevCapture对象的管理
*/
class CCaptureManager : public CThread
{
public:
	//! 构造函数
	CCaptureManager();

	//! 析构函数
	~CCaptureManager();
	
	//! 初始化
	void Init(int iChannel, CDevCapture *pDev);

	//! 处理数据
	void OnCap(uint arg);	

	//! 设置字幕
	void SetTitle();
	
	//! 开启线程
	void Start();

	//! 关闭线程
	void Stop();
	
	//! 线程的执行体
	void ThreadProc();

	// 打印统计信息
	void dumpStatInfo();

	//设置待机状态 ,待机状态下全部码流不推流
	void SetSuspend(int suspend);	

private:
	CDevCapture *	m_pDevCap[N_SYS_CH];		/*!< 捕获设备的指针 */
	int				m_iUser;					/*!< 开启捕获设备的计数 */
	CMutex			m_Mutex;					/*!< 数据锁 */

	CTimer			m_BitRateTimer;				/*!< 处理码流的定时器 */

	int	m_iSuspend;								/*!< 0:正常状态 1:待机状态  */
	int m_statTime;	// 上次统计时间
};

#endif// __DEV_CAPTURE_H__

