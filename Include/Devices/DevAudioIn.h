#ifndef _DEVAUDIOIN_H
#define _DEVAUDIOIN_H

#include "APIs/Audio.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "System/Object.h"
#include "System/Signals.h"
#include "System/Packet.h"

class CAudioInManager;

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

class CDevAudioIn : public CObject
{
public:
	typedef TSignal2<int, CPacket *>::SigProc SIG_DEV_CAP_BUFFER;

	CDevAudioIn(int iChannel);
	~CDevAudioIn();
	static	int GetChannels ();// 得到通道数目
	static	VD_BOOL GetCaps(AUDIOIN_CAPS * pCaps);// 捕获设备特性	
	int SetFormat(int iChannel, AUDIOIN_FORMAT * pFormat);
	static	int GetFormats(AUDIOIN_FORMAT* pFormat, int iMax);
	void SetSuspend(int suspend);
	void SetMicEanble(int status);
	VD_BOOL Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc);// 启动捕获
	VD_BOOL Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc);
	virtual void ThreadProc(){};
	void OnData(CPacket *);
	static CDevAudioIn* instance(int iChannle);

private:
	int		m_iChannel;
	uint	m_dwBytes;
	uint	m_dwBytesOld;
	uint	m_dwBytesMask;
	uint	m_Time;
	uint    m_AudioType;  //音频类型 0:G711a 1:AAC
	TSignal2<int, CPacket *> m_sigBuffer;

	static CAudioInManager* m_pManager;
	static CDevAudioIn* _instance[N_AUDIO_IN]; 
};

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

class CAudioInManager : public CThread
{
public:
	CAudioInManager();
	~CAudioInManager();
	void Start();
	void Stop();
	void SetSuspend(int suspend);	
	void SetMicEnable(int status){m_iMicEnable = status;}	
	void Init(int iChannel, CDevAudioIn *pDevCap);
	void ThreadProc();
protected:

private:
	CDevAudioIn * m_pDevCap[N_AUDIO_IN];
	int m_iUser;
	int m_iSuspend; /*!< 0:正常状态 1:待机状态  */
	int m_iMicEnable;  //app麦克风开关 0 关  1：开
	CMutex m_Mutex;
};

#endif // _DEVAUDIOIN_H
