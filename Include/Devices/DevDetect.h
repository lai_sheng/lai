


#ifndef __DEV_DETECT_H__
#define __DEV_DETECT_H__

#include "APIs/BlindDetect.h"
#include "APIs/MotionDetect.h"
#include "APIs/LossDetect.h"
#include "System/Object.h"

class CDevMotionDetect : public CObject
{
public:
	CDevMotionDetect();
	virtual ~CDevMotionDetect();
	static	VD_BOOL GetCaps(MOTION_DETECT_CAPS * pCaps);
	VD_BOOL SetParameter(int channel, MOTION_DETECT_PARAM *pParam);
	VD_BOOL getDetectParam(int iChannel, MOTION_DETECT_RESULT* pParam);
	uint GetState();
	VD_BOOL ShowHint(int channel, int enable);
	static CDevMotionDetect* instance(void); 

private:
	uint m_dwState;
	static CDevMotionDetect* _instance; 
};

class CDevLossDetect : public CObject
{
public:
	CDevLossDetect();
	virtual ~CDevLossDetect();
	static	VD_BOOL GetCaps(LOSS_DETECT_CAPS * pCaps);
	uint GetState();
	uint GetLossState();
	static CDevLossDetect* instance(void); 

private:
	uint m_dwState;
	uint m_dwLossState;
	static CDevLossDetect* _instance;
};

class CDevBlindDetect : public CObject
{
public:
	CDevBlindDetect();
	virtual ~CDevBlindDetect();
	static	VD_BOOL GetCaps(BLIND_DETECT_CAPS * pCaps);
	VD_BOOL SetParameter(int channel, BLIND_DETECT_PARAM *pParam);
	uint GetState();
	static CDevBlindDetect* instance(void);

private:
	uint m_dwState;
	static CDevBlindDetect* _instance;
};

#endif// __DEV_DETECT_H__
