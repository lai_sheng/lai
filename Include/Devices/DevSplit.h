

#ifndef __DEV_SPLIT_H__
#define __DEV_SPLIT_H__

#include "config-x.h"
#include "APIs/Split.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"

#define CHANNEL_ALL -1

class ICaptureManager;

class CDevSplit
{
public:
	CDevSplit(int index);
	~CDevSplit();

	VD_BOOL Combine( uint dwMode, uint dwIndex);
	VD_BOOL Lock( uint dwMask);
	VD_BOOL GetRect(int channel, VD_RECT * pRect);
	int GetBlankRects(VD_RECT * pRect);
	VD_BOOL GetFullRect(VD_RECT *pRect);
	VD_BOOL SetVideoStandard(uint dwStandard);
	VD_BOOL GetCaps(SPLIT_CAPS * pCaps);
#ifdef _SURPORT_SWITCH_
       VD_BOOL Switch(uint chn0,uint chn1);
#endif
#ifdef 	_SUPPORT_PREVIEWZOOM_
	VD_BOOL Zoom(int iPlayerChannel, VIDEO_RECT *pDestRect, VIDEO_RECT * pSrcRect = NULL);
#endif

public:
	void adjustRect(VD_RECT *pRect, int flag = 0);
public:
	static CDevSplit* instance(int index = 0); 

protected:
private:
	CMutex m_mutexChRect;		// 该互斥量用于保护视频区域的设置和获取的并发过程
	int	m_index;
	static CDevSplit* _instance[N_VIDEO_OUT]; 
	ICaptureManager* m_pCaptureM;
};

#define N_SPLIT            (SPLIT_NR+1)

#endif// __DEV_SPLIT_H__
