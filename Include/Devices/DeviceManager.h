
#ifndef __DEVICE_MANAGER_H__
#define __DEVICE_MANAGER_H__
#include "APIs/DVRDEF.H"
#include "System/Object.h"
class CDeviceManager
{
public:
	PATTERN_SINGLETON_DECLARE(CDeviceManager);
	CDeviceManager();
	virtual ~CDeviceManager();
	VD_BOOL Start();
	VD_BOOL Stop();
private:
	VD_BOOL m_bStarted;
};

#define g_DeviceManager (*CDeviceManager::instance())
extern int g_nAlarmIn;
extern int g_nAlarmOut;
extern int g_nCapture;
extern int g_nLogicNum;
//extern int g_nPlay;
extern int g_nAudioIn;
extern int g_nVideoOut;

#endif// __DEVICE_MANAGER_H__
