

#ifndef __DEV_BACKUP_H__
#define __DEV_BACKUP_H__

#include "APIs/Backup.h"
#include "MultiTask/Thread.h"
#include "System/Object.h"
#include "System/Packet.h"
#include "System/Signals.h"

#define BACKUPBUF_SIZE 32*1024
#define BK_MAX_DEV		6
#define MULTI_BK		4
#define N_BACKUPDEV_MAX 8

typedef struct  
{
	BACKUP_DEVICE	*pBKDev;		/*!< 备份设备地址 */
	uchar			type;			/*!< 备份的类型 */
}BackupInfo;

typedef TSignal1<uchar *>::SigProc SIG_DEV_BKP_BUFFER;

typedef struct __Device 
{
	BACKUP_DEVICE	BackupDevice[N_BACKUPDEV_MAX];
	int			cnt[N_BACKUPDEV_MAX];
} Device;



#endif// __DEV_BACKUP_H__
