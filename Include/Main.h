
#ifndef __APPS_MAIN_H__
#define __APPS_MAIN_H__

#include "System/Object.h"
#include "config-x.h"
#include "MultiTask/Timer.h"

class CPageStart; 

#define COMMON_AUTHORITYNUMS 23

class CChallenger : public CThread
{
public:
    PATTERN_SINGLETON_DECLARE(CChallenger);

    CChallenger();
    ~CChallenger();

    void Initialize(int argc, char * argv[]);
    void Cleanup();
    void Restart();
    void Exit();
    void Shutdown();
    void Reboot(int iSec =0,int Flag=0);  //增加重启类型
	void Suspend(); //进入待机模式
	void Resume();	//退出待机模式
    void RebootForUsbConfig();

    void OnStopAlive();
    void OnStartAlive();
#ifdef _FUN_SUPPORT_DOUBLE_OUTPUT	
#ifdef _FUN_GUI_SWITCH_TIP
    void onTimer(uint arg);
#endif
#endif
    VD_BOOL GetInitializeState(void);
    void onTimerReboot(uint arg);
    VD_BOOL g_UpgradeOK;


    void GetAuthority(int logic_num);//added by wyf on 091209
	
private:	
	CTimer 	m_cTimer;    //
    CTimer  m_TimerAlive;     
    CTimer  m_timerNetStatus;
    VD_BOOL m_BInitializeState;
    CTimer  m_timerReboot;
#ifdef _PURE_DECODE
    CTimer  m_timerDecodeStatus;
#endif
    CMutex  m_Mutex;
private:
	void ServerInit();
	void ThreadProc();
};

#define g_Challenger (*CChallenger::instance())

#endif //__CHALLENGER_H__
