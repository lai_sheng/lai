/*
** ***************************************************************************
**                                  CODEC
**                               JPEG Decoder
**
**         (c) Copyright 1992-2008, ZheJiang Dahua Technology Stock Co.Ltd.
**                            All Rights Reserved
**
** File  Name		: jpegdec.h
** Description		: JPEG解码算法
** Modification		: 2009/12/07		ZhouHT	Create the file
** ***************************************************************************
*/
#ifndef  __JPEGDEC_H__
#define  __JPEGDEC_H__ 

#ifdef __cplusplus
extern "C" {
#endif

#define STM_FLAG_TYPE						0x0000003F
#define STM_FLAG_SWAP						0x00000040
#define STM_FLAG_DEINTERLACE				0x00000080

#define STM_FLAG_TYPE_DH					0x00000000		/* 大华码流 */
#define STM_FLAG_TYPE_ADI					0x00000001		/* ADI码流 */
#define STM_FLAG_TYPE_HISI					0x00000002		/* 海思码流 */
#define STM_FLAG_TYPE_AWX					0x00000003		/* 爱微芯码流 */

typedef struct 
{
	int				nWidth;			// 解码库通道支持的最大图象宽度，648 mpeg4解码库使用
	int				nHeight;		// 解码库通道支持的最大图象高度，648 mpeg4解码库使用
} DEC_OPEN_PARAM;

typedef struct 
{
	void*			pBitstream;		// 码流首地址
	int		 		nLength;		// 码流长度
	int				nFlag;			// 码流类型标记，h264解码用
	int 			nColorspace;	// 解码后输出的YUV格式。1:420	2:422
	int 			nPostproclevel;	// 后处理类型，pc mpeg4解码库用
	int				bDeInterlace;	// 是否解交错，pc mpeg4解码库用
} DEC_INPUT_PARAM;

typedef struct 
{
	void* 			pY;				// 解码后y分量输出地址
	void*			pU;				// 解码后u分量输出地址
	void* 			pV;				// 解码后v分量输出地址
	int 			nWidth;			// 帧宽
	int 			nHeight;		// 帧高
	int				nIsUseful;		// 解码输出缓冲内数据是否有效(264 B帧用),1：有效，0：无效
} DEC_OUTPUT_PARAM;


/* $Function		:	JPEG_Dec_Init
== ===========================================================================
== Description		:	初始化解码库，只需要在开始使用解码库前调用一次
==					:
== Argument			:	nCPUFlag		- CPU判断标志，PCmpeg4/h264解码库使用
==					:
== Performance 		:
==					:
== Return			:	无
==					:
== Modification		:	2009/12/07		ZhouHT    Create
== ===========================================================================
*/
void JPEG_Dec_Init(unsigned int	nCPUFlag);

/* $Function		:	JPEG_Dec_Open
== ===========================================================================
== Description		:	初始化解码通道，获得解码句柄
==					:
== Argument			:	param1			- 通道初始化参数结构体指针
==					:
== Performance 		:
==					:
== Return			:	>  0			- 解码器句柄
==					:	<= 0			- 创建解码句柄失败
==					:
== Modification		:	2009/12/07		ZhouHT    Create
== ===========================================================================
*/
void* JPEG_Dec_Open(DEC_OPEN_PARAM* param1);

/* $Function		:	JPEG_Dec_Decode
== ===========================================================================
== Description		:	解码一帧
==					:
== Argument			:	hnd				- 解码器句柄
==					:	deci			- 解码码流输入信息
==					:	deco			- 解码帧输出信息
==					:
== Performance 		:
==					:
== Return			:	<  0			- 解码出错
==					:	>= 0			- 使用掉的码流数据的长度
==					:
== Modification		:	2009/12/07		ZhouHT    Create
== ===========================================================================
*/
int JPEG_Dec_Decode(void* hnd, DEC_INPUT_PARAM* deci, DEC_OUTPUT_PARAM* deco);

/* $Function		:	JPEG_Dec_Close
== ===========================================================================
== Description		:	释放一个解码句柄
==					:
== Argument			:	hnd				- 解码器句柄
==					:
== Performance 		:
==					:
== Return			:	无
==					:
== Modification		:	2009/12/07		ZhouHT    Create
== ===========================================================================
*/
void JPEG_Dec_Close(void* hnd);


#ifdef __cplusplus
}
#endif

#endif
