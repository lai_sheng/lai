

#ifndef __GRAPHICS_API_H__
#define __GRAPHICS_API_H__

#include "DVRDEF.H"

#ifdef __cplusplus
extern "C" {
#endif

/// \defgroup GraphicsAPI API Graphics
/// 简单图形设备接口，获取FrameBuffer（帧缓冲）描述，设置Overlay（叠加）参数等等。
/// 渲染部分由上层实现，暂时不支持硬件加速。
/// \n 调用流程图:
/// \code
///    =========================================
///                   |                         
///             GraphicsCreate                  
///         +---------|------------------+      
///         |   GraphicsSetVstd          |
///         |         |            GraphicsSetAlpha
///         |  GraphicsGetBuffer  GraphicsSetColorKey
///         |         |                  |   
///         +---------|------------------+  
///            GraphicsDestory                  
///                   |                         
///    =========================================
/// \endcode
/// @{

/// 图形帧表面结构
typedef struct GRAPHICS_SURFACE
{
	uchar *mem;	///< 显存块指针。
	uchar *mem0;	///< 保留。
	uchar *mem1;	///< 保留。
	int pitch;	///< 跨度，即显存一行扫描线的开始到下一行扫描线的开始存储空间的偏移，字节为单位。
	int width;	///< 面宽度，象素为单位。
	int height;	///< 面高度，象素为单位。
	int format;	///< 颜色格式，取graphics_color_format_t类型值的值。
} GRAPHICS_SURFACE;

typedef struct tagFBCmapUser {
	uint start; /* First entry	*/
	uint len; /* Number of entries */
	ushort *r;
	ushort *g;
	ushort *b;
	ushort *transparent;		/* transparency, can be NULL */
}FBCmapUser;

/// 图形颜色格式类型
enum graphics_color_format_t{  
	GRAPHICS_CF_RGB555,	///< 16位表示一个象素。颜色分量位掩码为XRRRRRGG GGGBBBBB。
	GRAPHICS_CF_RGB565,	///< 16位表示一个象素。颜色分量位掩码为RRRRRGGG GGGBBBBB。
	GRAPHICS_CF_PSEUDOCOLOR //8位表示一个像素。总共代表256种颜色
};

#define MAX_TEXT_LENGTH 128    //最大的文本长度
#define MAX_TEXT_REGION_NUM 5  //最大的文本区域数量

typedef enum tagTextLayout
{
	TextLayout_Normal = 0,    //正常
	TextLayout_LeftFlip,      //向左翻转
	TextLayout_RightFlip,     //向右翻转  
	TextLayout_Reverse        //倒转
}TextLayout;

typedef struct tagGraphicsText
{
	VD_RECT TextRect;
	int fontsize;
	int fonttype;
	int fontcolor;
	TextLayout layout;
	char Text[MAX_TEXT_LENGTH];
}GraphicsText;



/// 创建图形设备
/// 
/// \param 无
/// \retval <0 创建失败
/// \retval 0 创建成功
int GraphicsCreate(void);


/// 销毁图形设备
/// 
/// \param 无
/// \retval <0 销毁失败
/// \retval 0 销毁成功
int GraphicsDestory(void);	


//设置索引颜色表
int GraphicsSetColorTable(FBCmapUser	*ColorMap);


/// 获取图形绘制缓冲。视频制式改变时可能会改变，应用程序需要重新获取。
/// 
/// \param [out] pBuffer 指向显示缓冲特征结构GRAPHICS_SURFACE的指针。GDI直接通过
///        显存块指针访问显存。图形设备只能选择GDI支持的颜色格式中的一种，如果GDI
///        都不支持，就需要增加新的颜色格式，并扩充GDI功能。
/// \retval 0  获取成功
/// \retval <0  获取失败
int GraphicsGetBuffer(GRAPHICS_SURFACE * pBuffer);


///显示已经设置在framebuffer中数据
///返回值:  0 成功 -1 失败
int GraphicsDisplay();


///创建一个text显示区域
///参数描述请参考结构体:GraphicsText
///返回值 标识当前text显示区域的ID
///
int GraphicsCreateText(GraphicsText *text);

///设置指定text显示区域的内容
///TextRegionID: 指定的区域ID
///Text: 设置的内容
int GraphicsSetText(int TextRegionID,char *text);


///销毁一个text显示区域
///TextRegionID: 指定的区域ID
///成功返回0 失败返回-1
int GraphicsDestoryText(int TextRegionID);


#ifdef __cplusplus
}
#endif

#endif //__GRAPHICS_API_H__

