#ifndef LOCK_API_H_
#define LOCK_API_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "System.h"

typedef struct TagDoorLockinfo 
{
	char		SignalStrength; // 1~4
	char		Electric;		// 1~100
	char		DoorStatus;		// 0:close 1:open
	char		LockStatus;		// 0:close 1:open
}lockInfo ;

typedef enum{
	LOCKOPEN_PSW		=	0x01,  //密码开锁
	LOCKOPEN_FINGER		=	0x02,  //指纹开锁
	LOCKOPEN_CARD		=	0x03,  //门卡开锁
	LOCKOPEN_TEMPPSW	=	0x04,  //临时密码开锁	
}Lock_OpenType;

typedef enum{
	LOCK_OPENLOCK		= 0x01, //开锁记录信息
	LOCK_ACTIONINFO		= 0x02, //锁状态及动作通知
	LOCK_GETSECRETKEY	= 0x03,	//获取密钥	
}Lock_MsgType;

typedef enum{
	LOCKMSG_AWAKE		= 0x00,//门锁被唤醒
	LOCKMSG_ALARM		= 0x01,//防撬报警
	LOCKMSG_ADDUSER		= 0x02,//添加密钥，数据内包含用户ＩＤ及密钥ＩＤ
	LOCKMSG_DELUSER		= 0x03,//删除密钥，数据内包含用户ＩＤ及密钥ＩＤ
	LOCKMSG_DOORBELL	= 0x04,//门铃
	LOCKMSG_SYSTEMLOCK	= 0x05,//系统被锁定
	LOCKMSG_LOCKINIT	= 0x06,//门锁初始化
	LOCKMSG_CLOSELOCK	= 0x07,//门锁关闭	
	LOCKMSG_ADDUSERINFO = 0x08,//添加用户 
	LOCKMSG_DELUSERINFO = 0x09,//删除用户
}Lock_ActionMsg;

//开锁信息
typedef struct  {
	Lock_MsgType	Cmd;

	char			Name[128]; 
	unsigned int	UserNumber;	
	SYSTEM_TIME		UnlockTime;
	Lock_OpenType	UnlockType;
	char            UnlockTypeid;
}lockOpenInfo ;

typedef struct {
	Lock_MsgType	Cmd;
	
	Lock_ActionMsg	Action; 
//添加(删除) 密钥(用户) 信息才起作用
	unsigned int	UserNumber;
	Lock_OpenType   UserType;
    unsigned int    UserTypeNumber; // 用户类型号 ---只适用于X1000门锁
    unsigned int    UserPriType;    // 用户权限类型, 0:管理员 1:普通用户 ---只适用于X1000门锁
}lockActionInfo ;

typedef union{
	Lock_MsgType	Cmd;
	lockOpenInfo 	OpenInfo;
	lockActionInfo  ActionInfo;
}lockMsgInfo;

typedef struct {
	int		MsgCnt;
	lockMsgInfo Msg[32];
}lockAllMsg;

//以下函数返回值 为 0:成功  1:失败
int SetLockTime(SYSTEM_TIME Time);
int GetLockTime(SYSTEM_TIME *pTime);

int GetLockStatus(lockInfo *pInfo);

int GetLockSoftVersion(char* pVer);

int OpenLockByRemote();
int OpenLockByPsw(char *pPsw,int pwdLen);

int SetLockTempPsw(char *pPsw,int pwdLen,int timeOut);

int GetLockOpenInfo(lockOpenInfo *pInfo);
int GetLockMsgInfo(lockAllMsg *pInfo);

int SetLockModuleVersion(char *pVersion,char *pVerTime);

int GetSecretKeyResp(char *pToken,int tokenLen);

/*********************************** X1000新增 ************************************/
#define LOCKAPI_USER_NUM (100)  //用户个数
#define LOCKAPI_UNLOCK_ATTR_ID_NUM (50)    //每个开锁类型支持ID个数
#define LOCKAPI_UNLOCK_PEROID_NUM  (8)     //限时开锁支持时间段个数
#define LOCKAPI_AVINFO_NUM (20) //音视频留言最大个数

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

//门锁设备获取参数
typedef struct {
    uint8_t lockVolume; //锁端音量    0-100
	uint8_t lockBackLight; //背光亮度    1-100
	uint8_t bVirtualPwd; //虚位密码    0/1
	uint8_t bIrregularKey; //乱序键盘    0/1
}LockApiDevParamsInfo;

//门锁用户类型
typedef enum {
    LOCK_API_USER_PRI_ADMIN = 0, //管理员
    LOCK_API_USER_PRI_GENERAL, //普通用户
    LOCK_API_USER_PRI_NONE,
}LockApiUserPriorityType;

typedef struct {
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
}LockApiPeroidTime;

typedef struct {
    LockApiPeroidTime startTime;
    LockApiPeroidTime endTime;
}LockApiPeroid;

//限时开锁时间段信息
typedef struct {
    uint8_t enable;
    uint8_t repeat; //bit0:周末, bit1-6: 周一到周六, value为0x00: 仅一次
    uint8_t peroidCnt;
    LockApiPeroid peroid[LOCKAPI_UNLOCK_PEROID_NUM];   
}LockApiUnLockPeroidInfo;

typedef struct {
    char uuid[20];
	uint8_t online;  //1:在线，0：离线
    uint8_t wifiSignal; //0-100
    uint8_t extDevVer[32];
}LockApiExtDevInfo;

//开锁属性
typedef struct {
	uint8_t type; //开锁类型 1：密码 2：指纹 3：门卡
	uint8_t idCnt; //本开锁类型已录入个数
	uint8_t idList[LOCKAPI_UNLOCK_ATTR_ID_NUM]; //本开锁类型已录入ID表
}LockApiUnLockAttr;

//用户属性
typedef struct {
	uint8_t userId; //用户id
	uint8_t userPriType; //用户权限类型 0:管理员 1:普通用户
	uint8_t unlockTypeCnt; //本用户已添加开锁类型个数
	LockApiUnLockAttr unlockTypeAttr[3]; //本用户添加的开锁属性
}LockApiUserAttr;

//用户列表信息
typedef struct {
    int userCnt;
    LockApiUserAttr userAttr[LOCKAPI_USER_NUM];
}LockApiUserInfo;
typedef struct {
    uint8_t year;
	uint8_t mon;
	uint8_t day;
}LockApiSetWeatherDATE;

typedef struct {
    uint8_t dir;   //风向,0:东,1:南,2:西,3:北,4:东南,5:西北,6:东北,7:西南;
	uint8_t gra;   //风的等级
}LockApiSetWeatherWIND;
typedef struct {
    uint8_t wea;    //天气, 0:晴,1:阴,2:下雨,3:下雪
	uint8_t temh;   //最高温度
	uint8_t teml;   //最低温度
	uint8_t dir;   //风向,0:东,1:南,2:西,3:北,4:东南,5:西北,6:东北,7:西南;
	uint8_t gra;   //风的等级
}LockApiOtherDayWIND;

typedef struct {
    char     city[13];   //城市名称,13个字节,GB2312编码,最后一个字节一定要为\0
	LockApiSetWeatherDATE     date;      //年月日
	uint8_t  wea;    //天气, 0:晴,1:阴,2:下雨,3:下雪
	uint8_t tmp;   //当前温度
	uint8_t tmph;   //最高温度
	uint8_t tmpl;   //最低温度
	LockApiSetWeatherWIND     wind;   //风向与级数
	uint8_t  qual;         //空气质量，0：优，1：良，2：差
	uint8_t  humi;        // 湿度（百分比）
	uint8_t  uv;         //  紫外线 ，0：强，1：中，2：弱
	uint8_t  fdays;       //   天数，未来N天的天气
	LockApiOtherDayWIND  otherday[7];
}LockApiSetWeatherHap;

//音视频留言属性
typedef struct {
    uint8_t userId;  //用户编号
    uint8_t openWay; //开锁类型 bit0：钥匙, bit1：管理员密码, bit2：指纹, bit3：门卡, bit4：撬锁, bit5：临时密码开锁;Value: 1为播放, 0为不播放; 
    uint8_t replay;    //播放次数，0：不播放，1：播放一次，2：每次都播放         ，-1：不存在
    uint8_t week;     //bit0:周末, bit1-6: 周一到周六,
    uint8_t playType;  //播放类型 0：音频 1：视频 2：文本
    char    playUrl[128];  //播放链接。注意：当播放类型为2（文本）时，此字段代表文本内容
    uint8_t mid;     //消息ID好号
}LockApiAVInfo;

typedef struct {
	int cnt; //音视频留言实际个数
	LockApiAVInfo astAvInfo[LOCKAPI_AVINFO_NUM]; //音视频留言属性
}LockApiAVListInfo;

typedef void (*LockMsgRecvCallback)(lockMsgInfo *pRcvMsg);

//int SetLockDevParams(LockApiDevParamsInfo stParamsInfo);

/*
 * @type: 0:锁端音量, 1: 背光亮度, 2: 虚位密码, 3: 乱序键盘  
 *
 * @value: 锁端音量0-100 背光亮度1-100 虚位密码0/1 乱序键盘0/1
 *
 */
int SetLockDevParams(uint8_t type, uint8_t value);
int GetLockDevParams(LockApiDevParamsInfo *pParamsInfo);
int SetLockAddUser(LockApiUserPriorityType enUserPriType, int *pOutUserId);
int SetLockAddUserInfo(int userId, Lock_OpenType enUnLockType);
int SetLockUnLockPeriod(LockApiUnLockPeroidInfo stUnLockPeroidInfo);
int GetLockUnLockPeriod(LockApiUnLockPeroidInfo *pstUnLockPeroidInfo);
int GetLockUserInfo(LockApiUserInfo *pUserInfo);
int SetLockExtDevInfo(LockApiExtDevInfo *pExtDevInfo);
int SetLockAvInfo(LockApiAVInfo pstAvInfo,char *filename, int *outmid);
int GetLockAvInfo(LockApiAVListInfo *pstAvInfoList);
int DelLockAvInfo(int mid);
int SetLockUpdate(char *filename);
int GetLockWakeUpCheck();
int GetRemainSpace(int *space);
int SetWeather(LockApiSetWeatherHap weather);
int SetCityCode(char *citycode);
int GetCityCode(char *citycode);
int LockMsgRegisterRecv(LockMsgRecvCallback pRecvCallback);
int LockMsgUnRegisterRecv(void);
int LockInit(void);
int LockDeinit(void);

/********************************************************************************************/

#ifdef __cplusplus
}
#endif
#endif
