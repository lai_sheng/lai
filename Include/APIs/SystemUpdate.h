#ifndef __UPDATE_34254235423625415425432_H
#define __UPDATE_34254235423625415425432_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef IN
#define VD_IN const
#endif

#ifndef OUT
#define VD_OUT
#endif

#ifdef WIN32
#define VD_IN const
#define VD_OUT
#endif


/*
 * callback for process preview
 * process: from 1 to 100
 */
typedef int (*update_cb)(int process);

/*
 * check buffer, return value:
 * 0 on success
 * -1: buffer is invalid
 * -2: magic is invalid
 * -3: version is invalid
 * -4: crc is invalid
 */
int update_checkbuffer(VD_IN char *buffer, int size);

/*
 * update, return value:
 * 0 on success
 * -1: buffer is invalid
 * -2: magic is invalid
 * -3: 升级文件不是该硬件平台
 * -4: all version is the same
 * -5: too old version
 * -6: no need update
 * -7: param err
 * -8: out of memory
 */
int update_buffer2device(VD_IN char *buffer, int size, update_cb cb, VD_IN int force_update);

int update_cmpversion(VD_IN char *buffer, VD_OUT char *file_version, VD_OUT char *flash_version);



/// 升级功能重构后相关接口 2010-06-02
/// ===================================================
typedef struct RawFlashCaps
{
	int type;			///< Flash类型，0-NAND，1-NOR
	int totalSize;		///< 总大小，字节为单位
	int sectorSize;		///< 扇区大小
	int reserved[29];	///< 保留
}RawFlashCaps;

/// 获取当前系统中的mtd*个数,bin文件升级情况下使用到
int UpdategetMtdCount();

/// 获取flash相关信息，供上层应用使用
/// \return 成功返回0 失败返回-1
int UpdategetCaps(int mtdCnt, RawFlashCaps* caps);

/// 读取数据
/// \praram mtdname
/// \return 成功返回0 失败返回 < 0
int UpdateRead(const char *mtdname, char *buf, ulong addr, int len);

/// 写入数据
/// \praram mtdname
/// \return 成功返回0 失败返回 < 0
int UpdateWrite(const char *mtdname, char*buf, ulong addr, int len);

/// 擦除数据
/// \praram mtdname
/// \return 成功返回0 失败返回 < 0
int UpdateErase(const char *mtdname, ulong addr, int len);

#ifdef __cplusplus
}
#endif
#endif

