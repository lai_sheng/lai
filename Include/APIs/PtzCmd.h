

#ifndef __PTZ_CMD_H__
#define __PTZ_CMD_H__

#ifdef __cplusplus
extern "C" {
#endif

#define ProtocolNum 40
#define NAME_MAX_LEN 16

#define N_PTZ_ALARM_IN 8		//! 报警解码器的报警输入端口数量

#define PTZ_NAMELEN		10		//协议名称长度
#define PRO_CMD_LEN		16		//协议命令长度

#define ALARM_PTZLINK_NUMS 4		//有云台联动项的报警个数added by wyf on 20100919


//! 云台按使用的分为有视频（云台及矩阵）和不需要视频的（如报警）
enum PTZ_FUNCTION_TYPE
{
	PTZ_WITH_VIDEO,
	PTZ_WITHOUT_VIDEO,
};

//! 云台支持的操作，注意：下面的顺序不能随便调整，应该和LUA保持一致
enum PTZ_OPT {
	PTZ_DIRECTION = 1,		/*!< 方向 */
	PTZ_ZOOM,				/*!< 变倍 */
	PTZ_FOCUS,				/*!< 聚焦 */
	PTZ_IRIS,				/*!< 光圈 */
	PTZ_ALARM,				/*!< 报警功能 */
	PTZ_LIGHT,				/*!< 灯光 */
	PTZ_SETPRESET,			/*!< 设置预置点 */
	PTZ_CLEARPRESET,		/*!< 清除预置点 */
	PTZ_GOTOPRESET,			/*!< 转至预置点 */
	PTZ_AUTOPANON,			/*!< 水平开始 */
	PTZ_AUTOPANOFF,			/*!< 水平结束 */
	PTZ_SETLIMIT,			/*!< 设置边界 */
	PTZ_AUTOSCANON,			/*!< 自动扫描开始 */
	PTZ_AUTOSCANOFF,		/*!< 自动扫描开停止 */
	PTZ_ADDTOUR,			/*!< 增加巡航点 */
	PTZ_DELETETOUR,			/*!< 删除巡航点 */
	PTZ_STARTTOUR,			/*!< 开始巡航 */
	PTZ_STOPTOUR,			/*!< 结束巡航 */
	PTZ_CLEARTOUR,			/*!< 删除巡航 */
	PTZ_SETPATTERN,			/*!< 设置模式 */
	PTZ_STARTPATTERN,		/*!< 开始模式 */
	PTZ_STOPPATTERN,		/*!< 停止模式 */
	PTZ_CLEARPATTERN,		/*!< 清除模式 */
	PTZ_POSITION,			/*!< 快速定位 */
	PTZ_AUX,				/*!< 辅助开关 */
	PTZ_MENU,				/*!< 球机菜单 */
	PTZ_EXIT,				/*!< 退出球机菜单 */
	PTZ_ENTER,				/*!< 确认 */
	PTZ_ESC,				/*!< 取消 */
	PTZ_MENUUPDOWN,			/*!< 菜单上下操作 */
	PTZ_MENULEFTRIGHT,		/*!< 菜单左右操作 */
	PTZ_MATRIXSWITCH,		/*!< 矩阵切换 */
	PTZ_FLIP,				/*!< 镜头翻转*/
	PTZ_RESET,				/*!< 云台复位*/
	PTZ_LIGHTCONTROLLER,	/*!< 菜单左右操作 （主要用于网络对灯光控制器的控制）*/
	PTZ_OPT_NUM				/*!< 操作的个数 */
};

// 和LUA中保持一致
enum PTZ_OPT_ACT
{
	PTZ_OPT_NONE = 0,
	PTZ_OPT_LEFTUP = 1,
	PTZ_OPT_UP,
	PTZ_OPT_RIGHTUP,
	PTZ_OPT_LEFT,
	PTZ_OPT_RIGHT,

	//6
	PTZ_OPT_LEFTDOWN,
	PTZ_OPT_DOWN,
	PTZ_OPT_RIGHTDOWN,
	PTZ_OPT_ZOOM_WIDE,
	PTZ_OPT_ZOOM_TELE,

	//11
	PTZ_OPT_FOCUS_FAR,
	PTZ_OPT_FOCUS_NEAR,
	PTZ_OPT_IRIS_LARGE,
	PTZ_OPT_IRIS_SMALL,
	PTZ_OPT_ALARM_SEARCH,

	//16
	PTZ_OPT_LIGHT_ON,
	PTZ_OPT_LIGHT_OFF,
	PTZ_OPT_SETPRESET,
	PTZ_OPT_CLEARPRESET,
	PTZ_OPT_GOTOPRESET,

	//21
	PTZ_OPT_AUTOPANON,
	PTZ_OPT_AUTOPANOFF,
	PTZ_OPT_SETLEFTLIMIT,
	PTZ_OPT_SETRIGHTLIMIT,
	PTZ_OPT_AUTOSCANON,

	//26
	PTZ_OPT_AUTOSCANOFF,
	PTZ_OPT_ADDTOUR,
	PTZ_OPT_DELTOUR,
	PTZ_OPT_STARTTOUR,
	PTZ_OPT_STOPTOUR,

	//31
	PTZ_OPT_CLEARTOUR,
	PTZ_OPT_SETPATTERNBEGIN,
	PTZ_OPT_SETPATTERNEND,
	PTZ_OPT_STARTPATTERN,
	PTZ_OPT_STOPPATTERN,

	//36
	PTZ_OPT_CLEARPATTERN,
	PTZ_OPT_POSITION,
	PTZ_OPT_AUXON,
	PTZ_OPT_AUXOFF,
	PTZ_OPT_MENU,

	//41
	PTZ_OPT_EXIT,
	PTZ_OPT_ENTER,
	PTZ_OPT_ESC,
	PTZ_OPT_MENUUP,
	PTZ_OPT_MENUDOWN,

	//46
	PTZ_OPT_MENULEFT,
	PTZ_OPT_MENURIGHT,
	PTZ_OPT_SWITCH,
	//49
	PTZ_OPT_FLIP,
	PTZ_OPT_RESET,
	PTZ_MATRIX_SWITCH,//主要用于网络控制
	PTZ_LIGHT_CONTROLLER,//主要用于网络控制
	
	//53
	PTZ_OPT_SETPRESETNAME,		// 设置预置点名称
	PTZ_OPT_ALARMPTZ,			// 云台报警联动
	PTZ_OPT_STANDARD,

	//56  /*明景*/
	PTZ_OPT_SETTOURSTART,
	PTZ_OPT_ADDPRESET,
	PTZ_OPT_SETTOURSTOP,
	
	PTZ_OPT_AUTOHOME,
	PTZ_OPT_SETAUTOHOME,
	PTZ_OPT_WAITETIME,
	//62
	PTZ_OPT_POWERRESET,
	PTZ_OPT_VIDEOBLIND,
	PTZ_OPT_BLINDSET,
	PTZ_OPT_BLINDDEL,
	PTZ_OPT_SETPARAM,
	PTZ_OPT_QUERYPPARAM,
	PTZ_OPT_SETHD,
	PTZ_OPT_QUERYHD,
	PTZ_OPT_SETSTANDARD,
	PTZ_OPT_TRANSPARENT,	
	PTZ_OPT_POSITIONIN, //明景放大
	PTZ_OPT_POSITIONOUT, //明景缩小
	PTZ_OPT_SENSOR,   //亚邦 球机设置sensor 分辨率及制式
	PTZ_OPT_INIT_END,
};

enum PTZ_TYPE
{
	PTZ_DEV = 1,			/*!< 云台控制 */
	PTZ_MATRIX = 2,			/*!< 矩阵控制 */
};

// 用位来表示操作
enum standard
{
	standardUp = 0,
	standardDown,
	standardLeft,
	standardRight,
	standardZoomWide,
	standardZoomTele,
	standardFocusFar,
	standardFocusNear,
	standardIrisLarge,
	standardIrisSmall,
};

enum PTZ_OPT_PARAM
{
	PTZ_ARG_POSITIVE = 1,
	PTZ_ARG_ZERO = 0,
	PTZ_ARG_NEGATIVE = -1,
	PTZ_ARG_NONE = 0xfffffff,
};

//! 云台操作命令结构
typedef struct 
{
	int cmd;						/*! 命令 */
	int arg1;						/*! 命令参数1 */
	int arg2;						/*! 命令参数2 */
	int arg3;						/*! 命令参数3 */
	int reserved;					/*! 保留,目前被联动用来记忆触发的事件源 */
}PTZ_OPT_STRUCT;


#ifdef __cplusplus
}
#endif

#endif //__PTZ_CMD_H__
