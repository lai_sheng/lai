#ifndef _DYNAMICSNAP_H_
#define	_DYNAMICSNAP_H_

#include "DVRDEF.H"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum ePicType
{
	AI_JPEG 	= 0,
	AI_PNG		= 1,
	AI_BMP		= 2
}PicType;

typedef struct TagRect{  //坐标
	int x;
	int y;
	int w;
	int h;
}Rect;
typedef struct TagPicSize{ //图片分辨率
	int width;			
	int heigh;
}PicSize;
typedef struct TagIdentifyInfo{
	int age;
	int sex;  	//0:男 1:女
	int glass;	//0:没带眼睛  1:带
	char strFeeling[128]; //happy ,sad and so on	
	float fr_score;//识别率
}HeadIdentifyInfo;

typedef struct TagUpBody{  	//肩图相关信息
	int 		PersonId;  	//ID
	int 		iLen;		//数据长度	  		   	
	void* 		pAddr;		//图片数据地址

	PicType		iType;		//图片类型	
	Rect 		HeadRct; 	//头部坐标
	PicSize 	mSize;		//分辨相关信息

	char 		picPath[256];//图片路径

	HeadIdentifyInfo FaceInfo;//脸部详细识别信息	
}UpBodyInfo;

typedef struct TagScenePicInfo{
	int				iLen;		//全图数据长度
	void*			pAddr;		//全图数据地址

	int 			iPackCount;	//肩图数量
	UpBodyInfo* 	pBodyInfo; 	//肩图信息指针

	PicType			iType;		//图片类型
	unsigned int	mPts;		//时间戳
	PicSize 		mSize;		//图片分辨率
}ScenePicInfo;


typedef int(*OnFaceSnapData)(ScenePicInfo* pPicInfo);

#ifdef FACE_FEATURE
int FaceChangeSnap_Start(OnFaceSnapData OnData_Cb);
int FaceChangeSnap_Stop();

int Add_UserFace(int Uid,char *Name,char *Path);
int Del_UserFace(int Uid,char *Name,char *Path);
int Mod_UserFace(int Uid,char *Name,char *Path,char *NewName,char *NewPath);
#else
#define FaceChangeSnap_Start
#define FaceChangeSnap_Stop()

#define Add_UserFace
#define Del_UserFace
#define Mod_UserFace
#endif


#ifdef __cplusplus
}
#endif
#endif
