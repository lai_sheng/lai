#ifndef __EXT_DEV__
#define __EXT_DEV__

#include "DVRDEF.H"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_SSID_LENGTH 32

/**
* 定义wifi连接状态属性
*/
typedef enum {
	WIFI_DISCONNECTED,                          /**< wifi连接断开 */
	WIFI_CONNECTING,                            /**< wifi连接中 */
	WIFI_CONNECT_FAILED,                        /**< wifi连接失败 */
	WIFI_CONNECTED,                             /**< wifi已连接ap */
	WIFI_CONNECTED_WITH_INTERNET,               /**< wifi连接ap并连接到internet */
} WIFI_CONNECTION_STATUS;

/**
 * 定义wifi状态属性
 */
typedef struct {
	WIFI_CONNECTION_STATUS connectionStatus;    /**< wifi连接状态 */
	char ssid[MAX_SSID_LENGTH];                 /**< ap ssid */
	int signalLevel;                            /**< wifi信号强度(暂时无法使用) */
} ZRT_CAM_WIFI_Status;

/**
 * 定义电池充电状态
 */
typedef enum {
	BAT_DISCHARGING = 0,                        /**< 没有充电, 放电中 */
	BAT_CHARGING,                               /**< 电池充电中 */
	BAT_FULL,                                   /**< 电池已充满 */
} BAT_CHARGING_STATUS;

/**
 * 定义电池状态
 */
typedef struct {
	int capacity;                               /**< 电池电量数据 */
	BAT_CHARGING_STATUS chargingStatus;         /**< 电池充放电状态 */
    int isLowBatState;                          /**< 低电状态标志, 1:低电 0:非低电 */
} ZRT_CAM_BAT_Status;

typedef enum {
	SYS_WAKEUP_FLAG_NONE = 0,
	SYS_WAKEUP_FLAG_KEY,    		/**< 按键唤醒 */
	SYS_WAKEUP_FLAG_PIR,    		/**< PIR唤醒 */
	SYS_WAEKUP_FLAG_ALARM,  		/**< 定时唤醒，处理完立刻休眠 */
	SYS_WAEKUP_FLAG_ALARM_B,  		/**< 定时唤醒B */
	SYS_WAKEUP_FLAG_WIFI,   		/**< wifi唤醒 */
	SYS_WAKEUP_FLAG_BAT,			/**< 电池低电唤醒 */
	SYS_WAKEUP_FLAG_WDT,			/**< 看门狗唤醒 */
	SYS_WAKEUP_FLAG_USB_CHARGE, 	/**< USB充电唤醒 */
	SYS_WAKEUP_FLAG_RECOVERY_WIFI,  /**< 恢复wifi唤醒,处理完立刻休眠 */
	SYS_WAKEUP_FLAG_LOCK, 	        /**< 门锁唤醒 */
	SYS_WAKEUP_FLAG_MAX,
}SYSTEM_WAKEUP_FLAG;

/**
 * 定义灯类型
 */
typedef enum {
    ED_LED_TYPE_RED = 0,                /**< 红灯 */
    ED_LED_TYPE_BLUE,                   /**< 蓝灯 */
    ED_LED_TYPE_FILL_LIGHT,             /**< 补光灯 */            
    ED_LED_TYPE_INFRARED_LAMP,          /**< 红外灯 */
    ED_LED_TYPE_LAST,
}ED_LED_TYPE_E;

/**
 *  移动状态
 */
typedef enum {
    MOTION_STATE_NO_MOVE            = 0,    /* 未移动 */
    MOTION_STATE_MOVE               = 1,    /* 有移动 */
    MOTION_STATE_MOVE_SHORT         = 2,    /* 短时间移动,移动超过5s */
    MOTION_STATE_MOVE_LONG          = 3,    /* 长时间移动,移动超过15s */
    MOTION_STATE_MOVE_END           = 4     /* 未移动且移动侦测结束 */
}MOTION_STATE_E;

/************************************************
* 功能描述: 创建扩展设备
* 返 回 值: 0成功 -1失败
*************************************************/
int ExtDevCreate();

/************************************************
* 功能描述: 销毁扩展设备
* 返 回 值: 0成功 -1失败
*************************************************/
int ExtDevDestory();

/************************************************
* 功能描述:  获取Wifi状态
* 参数描述:  pWifiStatus 获取Wifi状态指针
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetWifiStatus(ZRT_CAM_WIFI_Status *pWifiStatus);


/************************************************
* 功能描述:  获取电池状态
* 参数描述:  pBatStatus 获取电池状态指针
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetBatStatus(ZRT_CAM_BAT_Status *pBatStatus);


/************************************************
* 功能描述:  设置PIR使能状态
* 参数描述:  enable 设置需要使能的状态
             0 禁用
             1 使能
* 返 回 值:  0 成功 -1 失败
*************************************************/
int SetPirEnableStatus(int enable);

/************************************************
* 功能描述:  获取PIR使能状态
* 参数描述:  pEnable 获取使能状态
             0 禁用
             1 使能
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetPirEnableStatus(int *pEnable);

/************************************************
* 功能描述:  获取PIR触发状态
* 参数描述:  无
* 返 回 值:  0 无 1 触发状态
*************************************************/
int GetPirTriggerStatus();

/************************************************
* 功能描述:  设置Key使能状态
* 参数描述:  enable 设置需要使能的状态
             0 禁用
             1 使能
* 返 回 值:  0 成功 -1 失败
*************************************************/
int SetKeyEnableStatus(int enable);


/************************************************
* 功能描述:  获取Key使能状态
* 参数描述:  pEnable 获取使能状态
             0 禁用
             1 使能
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetKeyEnableStatus(int *pEnable);

/************************************************
* 功能描述:  获取按键按下状态
* 参数描述:  pStatus 获取按键按下状态指针
             0 未操作按键
             1 操作了按键
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetKeyPressStatus(int *pStatus);

/************************************************
* 功能描述:  主机进入休眠模式
* 参数描述:  无
* 返 回 值:  无
*************************************************/
void MasterEnterSleepMode();


/************************************************
* 功能描述:  主机进入复位模式
* 参数描述:  无
* 返 回 值:  无
*************************************************/
void MasterEnterResetMode();


#if 0 //推介使用 GetSystemWakeUpFlag
/************************************************
* 功能描述:  获取系统定时唤醒状态
* 参数描述:  pAlarmStatus 获取Alarm状态指针
             1 是ALarm定时唤醒
             0 非Alarm定时唤醒
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetSystemAlarmStatus(int *pAlarmStatus);
#endif

/************************************************
* 功能描述:  设置系统定时唤醒时间,24小时制
* 参数描述:  alarmTime 设置定时唤醒时间 ( Hours << 16 | Minutes << 8 | Seconds)
            
* 返 回 值:  0 成功 -1 失败
*************************************************/
int SetSystemAlarmTime(uint alarmTime);


/************************************************
* 功能描述:  禁用系统定时唤醒
* 参数描述:  无
* 返 回 值:  0 成功 -1 失败
*************************************************/
int DisableSystemAlarm(void);


/************************************************
* 功能描述:  获取系统唤醒标志
* 参数描述:  pWakeUpFlag 获取唤醒标志指针
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetSystemWakeUpFlag(SYSTEM_WAKEUP_FLAG *pWakeUpFlag);


/************************************************
* 功能描述:  获取MCU时间
* 参数描述:  tm 获取时间结构体指针
* 返 回 值:  0 成功 -1 失败
*************************************************/
int GetMcuTime(struct tm *tm);


/************************************************
* 功能描述:  同步当前时间至MCU
* 参数描述:  无
* 返 回 值:  0 成功 -1 失败
*************************************************/
int SetCurrentTimeSyncMcu();

/**
 * @brief 获取电池相机的产品模式
 *
 * @param[out] pMode 产品模式 0:套装 1:单品
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_GetProductMode(int *pMode);

/**
 * @brief 电池相机LED状态设置
 *
 * @param[in] ledType 灯类型 ED_LED_TYPE_E
 * @param[in] ledState 灯状态 0:关灯 1:开灯
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_LedSetState(ED_LED_TYPE_E ledType, int ledState);

/**
 * @brief 电池相机LED状态获取
 *
 * @param[in] ledType 灯类型 ED_LED_TYPE_E
 * @param[out] pLedState 灯状态 0:关灯 1:开灯
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_LedGetState(ED_LED_TYPE_E ledType, int *pLedState);

/**
 * @brief 复位电池相机网络
 *
 * @param 无
 * 
 * @return |0|成功| |非0|失败|
 *
 * @attention 无
 */
int BCAM_NetWorkReset();

/**
 * @brief 获取电池相机的存活状态
 *
 * @param[out] pState 存活状态 1:存活 0:非存活
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_GetAliveState(int *pState);


/**
 * @brief 单品休眠心跳创建
 *
 * @param[in] mode 心跳连接协议 0:UDP 1:TCP
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_SingleSleepHeartCreate(int mode);

/**
 * @brief 设置PIR侦测时间及PIR有效时间
 *
 * @param[in] pir_detect_time 设置PIR侦测总时间(sec)
 * @param[in] pir_effect_time 设置PIR有效触发时间(sec)
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_SetPirTime(int pir_detect_time, int pir_effect_time);

/**
 * @brief 获取PIR侦测时间及PIR有效时间
 *
 * @param[out] out_pir_detect_time 获取PIR侦测总时间(sec)
 * @param[out] out_pir_effect_time 获取PIR有效触发时间(sec)
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_GetPirTime(int *out_pir_detect_time, int *out_pir_effect_time);

/**
 * @brief 获取PIR移动侦测状态
 *
 * @param[out] pMotionState PIR侦测状 MOTION_STATE_E
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_GetPirMotionState(MOTION_STATE_E *pMotionState);

/**
 * @brief 获取wifi是否处于配置状态
 *
 * @param[out] 无
 * 
 * @return |0|否| |1|正在配置中|
 *
 * @attention 无
 */
int IsSettingWifiConfig();

/**
 * @brief 获取电池相机的按键状态
 *
 * @param[out] pPress 状态 1:按下 0:松开
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_GetKeyStatus(int *pPress/*0:loose 1:press*/);

/**
 * @brief 电池相机连接wifi接口
 *
 * @param[in] pSsid 账号
 * @param[in] pPass 密码
 * 
 * @return |0|成功| |-1|失败|
 *
 * @attention 无
 */
int BCAM_ConnectWifi(char *pSsid,char *pPass);

#ifdef __cplusplus
}
#endif

#endif //__EXT_DEV__

