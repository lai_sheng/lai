#ifndef _COMMONBSP_BATTERY_H_
#define _COMMONBSP_BATTERY_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	BAT_ChipT10,
	BAT_ChipT20,
	BAT_ChipT21N,
	BAT_ChipT30,
	BAT_ChipT20Z,
	BAT_ChipT30Z,
	BAT_ChipNUm,
}BAT_PlatformChip_E;

typedef enum {
	BAT_SensorSC2235,
	BAT_SensorJXH62,
	BAT_SensorJXF23,
	BAT_SensorOV9732,
	BAT_SensorNum,
}BAT_PlatformSensor_E;

typedef enum {
    BAT_GPIO_PHOTO_RESISTANCE,		//光敏输入
    BAT_GPIO_IR_CUT,				//IR-CUT控制
    BAT_GPIO_PLAY_ENABLE,			//喇叭控制
    BAT_GPIO_PLAY_SELECT,           //喇叭控制选择  ---用于QT2智能音箱项目
    BAT_GPIO_LED_BLUE,				//指示灯
    BAT_GPIO_LED_RED,				//指示灯
    BAT_GPIO_BUTTON_RESET_WIFI_CONFIG,
    BAT_GPIO_INFRAREDLAMP,			//红外灯开关
    BAT_GPIO_WIFI_PWDN, 			//wifi模块开关
    BAT_GPIO_WIFI_WAKE,				//wifi模块开关
    BAT_GPIO_LED_FILL_LIGHT,        //补光灯
    BAT_GPIO_LAST,
}BAT_GPIO_DEF_E;

typedef enum {
	BAT_RTL8188EUS,
	BAT_RTL8188FU,
	BAT_RTL8189ES,
	BAT_RTL8189FS,
	BAT_RTL8723BU,
	BAT_AP6212,
	BAT_WIFI_NONE,
}BAT_PlatformWifiModule_E;

typedef enum {
    BAT_BATTERY_NIMH,       //镍氢电池
    BAT_BATTERY_LITHIUM,    //锂电池
    BAT_BATTERY_DRY,       //干电池
    BAT_BATTERY_NONE
}BAT_PlatformBatteryType_E;

typedef enum {
    BAT_Client_CN,
    BAT_Client_EN, 
    BAT_Client_005,
    BAT_Client_YD,
    BAT_ClientNum,
}BAT_PlatformClientCode_E;


typedef enum {
    BAT_ProductT_SUIT,      /* 套装 */
    BAT_ProductT_SINGLE,    /* 单品 */
    BAT_ProductT_NONE,
}BAT_ProductType_E;

typedef enum {
	BAT_ProductM_S1,
	BAT_ProductM_S1Single,
	BAT_ProductM_I9M,
	BAT_ProductM_I10,
	BAT_ProductM_S50,
	BAT_ProductM_F5,	
	BAT_ProductM_LAST,
}BAT_ProductModel_E;

typedef struct{
	BAT_ProductModel_E	        ProductModel;
	BAT_ProductType_E           ProductType;
    BAT_PlatformClientCode_E    ClientCode;
	BAT_PlatformChip_E 	        Chip;
	BAT_PlatformSensor_E        Sensor;
	BAT_PlatformWifiModule_E    WifiModule;
    BAT_PlatformBatteryType_E   BatteryType;
	char 			            HardWareVersion[64];

	int 				        SensorImageWidth;
	int 		                SensorImageHeigh;
	
	char 			            GpioMap[BAT_GPIO_LAST];
	char 			            GpioTriggerLevelMap[BAT_GPIO_LAST];

	char 			            bAdcPhotoresistance;
	unsigned int 	            AdcMaxValue;
	unsigned int                AdcMinValue;

	char                        bLAN;
	char                        b4G;
	char                        bMotor;
	char 			            bUVC;
	char 			            bInitImageflip;
	char                        bAudio;
	char                        bOsdshow;
	char                        bSingle;  /* 单品/套装标识 */
}BAT_PlatformHandle;

void BAT_PlatformInit();
BAT_PlatformHandle *BAT_PlatformGetHandle();

char BAT_PlatformGpioConvert(BAT_GPIO_DEF_E _Gpio);
char BAT_PlatformGpioTriggerConvert(BAT_GPIO_DEF_E _Gpio);

#ifdef __cplusplus
}
#endif

#endif

