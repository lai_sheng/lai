/******************************************************************************
	created:	2009/11/30
	created:	30:11:2009   11:26
	filename: 	\src\valib\inc\va.h
	file path:	\src\valib\inc
	file base:	va
	file ext:	h
	author:		MAWEI
	
	purpose:
	note:尚未调试指针的对齐方式以及对函数的错误处理12-29
******************************************************************************/

#ifndef __VA_H__
#define __VA_H__

//#ifndef WIN32

#ifdef __cplusplus
extern "C" {
#endif
#ifdef WIN32_DLL
#ifdef VA_EXPORTS
#define VA_API __declspec(dllexport)
#else
#define VA_API __declspec(dllimport)
#endif
#else
#define VA_API
#endif

#define VA_CH_NO 16

//是否视频质量诊断(亮度评估、对比度评估、聚焦评估、清晰度评估、异常报警)
#define Need_VideoQuality	 0x1
//是否运动智能分析(运动强弱、运动在图像中的位置、线段、区域)
#define Need_MoitionAnalyze	 0x2

#define VS_SHAKE			0x1
#define VS_FLOST			0x2
#define VS_LIGHT			0x4
#define VS_DARK				0x8
#define VS_NOISE			0x10
#define VS_SIMPLE			0x20

typedef struct VA_RECT
{
	unsigned char left;
	unsigned char top;
	unsigned char right;
	unsigned char bottom;
}VA_RECT;

typedef struct VA_LINE
{
	unsigned char	A_X;	
	unsigned char	A_Y;	
	unsigned char   B_X;
	unsigned char   B_Y;
}VA_LINE;

typedef enum RECTTYPE 
{
	VA_RL_NOT_USED,
	VA_RL_RECT,
	VA_RL_REV_RECT,
	VA_RL_LINE,
	VA_RL_REV_LINE
}RECTTYPE;

typedef struct VA_RECT_LINE
{
	unsigned char state;//RECTTYPE	
	union
	{
		VA_RECT mrect;
		VA_LINE mline;
	}type_t;

#ifdef WIN32
}VA_RECT_LINE;
#else
}__attribute__ ((packed)) VA_RECT_LINE;
#endif

typedef struct VA_CAMMOVE{
	unsigned short			h;	
	unsigned short			v;	
}VA_CAMMOVE;

typedef struct 
{
	int		 		nWidth;				// 当前图像宽度	
	int				nHeight;			// 当前图像高度
	unsigned char * data_y;				// 当前图像的y分量
	int				time_interval;		// 当前图像与前一帧的时间间隔
	unsigned int    Resize;				// 缩放系数 CIF输入建议为0或1，D1输入建议为1或2
	unsigned int    taskmsk;			// 分析功能
	VA_RECT_LINE	mrectline[16];		// 检测矩形区域
	unsigned int	va_arg0;			// 参数0
	unsigned int	va_arg1;			// 参数1
	unsigned int	va_arg2;			// 保留
	unsigned int	va_arg3;			// 保留
	int				rsv[60];			// 保留
} VA_INPUT_PARAM;

typedef struct 
{
//Need_VideoQuality
	unsigned int	VideoState;			//报警状态
	unsigned int	VideoState_rsv1;	//
	unsigned int	VideoState_rsv2;	//
	unsigned int	VideoState_rsv3;	//
//Need_MoitionAnalyze
	unsigned short	HitStateInfor[4];	//区域、线段事件状态
	VA_CAMMOVE		CameraMotion;		//摄像头运动需求
	unsigned int	NumObj;				//人头个数
//保留
	int				rsv[32];			// 【对外保留】
	void* 			pY;					// 【对外保留】视频分析库测试图像y分量输出地址
	int 			nWidth;				// 【对外保留】视频分析库测试图像帧宽
	int 			nHeight;			// 【对外保留】视频分析库测试图像帧高
	unsigned char * VA_test_data1;		// 【对外保留】
	unsigned char * VA_test_data2;		// 【对外保留】
}VA_OUTPUT_PARAM;


/* $Function		:	VA_Process
== ============================================================================
== Description		:	视频分析
== Return			:	0			- 分析正常
==					:	<0 			- 分析内部故障 
= ============================================================================
*/
VA_API int VA_Process(void* handle, VA_INPUT_PARAM* deci, VA_OUTPUT_PARAM* deco);
/* $Function		:	VA_lib_Open
== ============================================================================
== Description		:	初始化视频分析通道，获得视频分析句柄
== Return			:	>  0			- 视频分析句柄
==					:	== 0			- 创建句柄失败
= ============================================================================
*/
VA_API void* VA_lib_Open();

/* $Function		:	VA_lib_Close
== ============================================================================
== Description		:	释放一个视频分析句柄
== ============================================================================
*/
VA_API void VA_lib_Close(void* handle);


#ifdef __cplusplus
}
#endif
#endif

//#endif
