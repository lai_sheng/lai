#ifndef _FOUREG_MANAGE_H
#define _FOUREG_MANAGE_H

#ifdef __cplusplus
extern "C" {
#endif
void HisiBspFoureGTask(void *args);
int HisiSetFGSoftRestart(int mode);

#ifdef __cplusplus
}
#endif

#endif
