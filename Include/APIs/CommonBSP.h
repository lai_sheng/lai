#ifndef _COMMON_BSP_H_
#define _COMMON_BSP_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	ChipT10,
	ChipT20,
	ChipT21N,
	ChipT30,
	ChipNUm,
}E_PlatformChip;

typedef enum {
	SensorSC2235,
	SensorJXH62,
	SensorJXF23,
	SensorNum,
}E_PlatformSensor;

typedef enum {
	GPIO_PHOTO_RESISTANCE,		//光敏输入
	GPIO_IR_CUT,				//IR-CUT控制
	GPIO_PLAY_ENABLE,			//喇叭控制
	GPIO_PLAY_SELECT,           //喇叭控制选择  ---用于QT2智能音箱项目
	GPIO_LED_BLUE,				//指示灯
	GPIO_LED_RED,				//指示灯
	GPIO_BUTTON_RESET_WIFI_CONFIG,
	GPIO_INFRAREDLAMP,			//红外灯开关
	GPIO_WIFI_PWDN, 			//wifi模块开关
	GPIO_WIFI_WAKE,				//wifi模块开关
	GPIO_LAST,
}E_GPIO_DEF;

typedef enum {
	RTL8188EUS,
	RTL8188FU,
	RTL8189ES,
	RTL8189FS,
	RTL8723BU,
	RTL8821CS,
	WIFI_NONE,
}E_PlatformWifiModule;

typedef enum {
    Client_CN,
    Client_EN, 
    Client_005,
    Client_YD,
    Client_YD_DEV,     //¿ª·¢»·¾³
    Client_YD_SLD,     //ÏÖÍø»·¾³
    Client_SN,
    ClientNum,
}E_PlatformClientCode;


typedef enum {
	ProductM_R2S1,
	ProductM_R2S2,
	ProductM_R2P2,		//呼吸灯
	ProductM_A3,		//双模卡片机    -- T20N--
	ProductM_A3S1,		//720p 卡片机	   -- T10L--
	ProductM_A3S2,		//1080p 卡片机  -- T20L--
	ProductM_P2,		//4g 摇头机     -- T20L--
	ProductM_G3S2,		//无线枪机      -- T20L--
	ProductM_Q1,
	ProductM_Q1PRO,
	ProductM_A3PRO,
	ProductM_A5,
	ProductM_A5PRO,	
	ProductM_Q2,
	ProductM_QT2,
	ProductM_Q2PRO,
	ProductM_LAST,
}E_ProductModel;

typedef struct{
	E_ProductModel	        ProductModel;
    E_PlatformClientCode    ClientCode;
	E_PlatformChip 	        Chip;
	E_PlatformSensor        Sensor;
	E_PlatformWifiModule    WifiModule;
	char 			        HardWareVersion[64];

	int 				    SensorImageWidth;
	int 		            SensorImageHeigh;
	
	char 			        GpioMap[GPIO_LAST];
	char 			        GpioTriggerLevelMap[GPIO_LAST];

	char 			        bAdcPhotoresistance;
	unsigned int 	        AdcMaxValue;
	unsigned int            AdcMinValue;

	char                    bLAN;
	char                    b4G;
	char                    bMotor;
	char 			        bUVC;
	char 			        bInitImageflip;
	char                    bAudio;
       char                   bOsdshow;  
}C_PlatformHandle;

void PlatformInit();
C_PlatformHandle *PlatformGetHandle();

char PlatformGpioConvert(E_GPIO_DEF _Gpio);
char PlatformGpioTriggerConvert(E_GPIO_DEF _Gpio);

#ifdef __cplusplus
}
#endif

#endif

