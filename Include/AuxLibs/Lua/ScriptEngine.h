
#ifndef __SCRIPT_ENGINE_H__
#define __SCRIPT_ENGINE_H__

#include "LuaWrap.h"

class ScriptEngine
	: public LuaWrap
{
public:
	ScriptEngine();
	~ScriptEngine();
	static ScriptEngine * Instance();
private:
	static ScriptEngine *_instance;	
};

#endif


//
// End of "$Id: ScriptEngine.h 1883 2006-02-13 03:02:27Z wanghw $"
//

