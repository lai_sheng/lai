#ifndef _CSMTPEXPORT_H_
#define _CSMTPEXPORT_H_

#ifdef WIN32
#if defined IPCSDK_EXPORTS 
	#define RLIPC_API  extern __declspec(dllexport) 
#else 
	#define RLIPC_API  extern __declspec(dllimport) 
#endif
#else
	#define RLIPC_API
#endif

typedef enum
{
	ENU_MAIL_EnMode_TLS,
	ENU_MAIL_EnMode_SSL,
	ENU_MAIL_EnMode_NULL
}ENU_MAIL_EnMode;
#ifdef __cplusplus
extern "C" {
#endif 
//RLIPC_API int SendMailSSL( const char * sMainServer,unsigned short port, ENU_MAIL_EnMode EncryptionMode, const char *  sMailFrom, const char *sMailTo,
//	                const char *  sUserName, const char *sPasswd,
//	                const char *  sTitle,  const char *sContent,
//	                char *  sFileName,char * sFileData,long FileLength);

RLIPC_API int SendMailSSL( const char * sMainServer,unsigned short port, ENU_MAIL_EnMode EncryptionMode,
					const char *  sMailFrom, const char *sMailTo0,
					const char *  sMailTo1,  const char *sMailTo2, 
	                const char *  sUserName, const char *sPasswd,
	                const char *  sTitle,   const char *sContent,
	                char *  sFileName,char * sFileData, int FileLength);


#ifdef __cplusplus
}
#endif 




#endif
