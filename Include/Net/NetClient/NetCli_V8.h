#ifndef _NETCLI_V8_H_
#define _NETCLI_V8_H_

#include "INetClient.h"
#include "CNetChannel.h"
#include "V8/IV8Cli.h"

class CNetChnnel_V8 : public CNetChnnel_RtspGeneral
{
public:
	virtual NET_CLI_PROTO_TYPE_E GetType();
	virtual INetChannel* clone();    
	virtual bool Compare( INetChannel * pThis );
};

class CNetCliCfg_V8 : public INetCliConfig
{
public:
    CNetCliCfg_V8();
    CNetCliCfg_V8( CNetCliCfg_V8& ps );
    virtual bool operator==( CNetCliCfg_V8& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();    
    virtual bool Compare( INetCliConfig * pThis );

    int SetServer(std::string srv_ip, int port);
    std::string GetSrvIP();
    int GetSrvPort();

    int SetAuth(std::string user, std::string passwd);    
    std::string GetUser();
    std::string GetPasswd();
    int SetLocalCh(int iCh);
    int GetLocalCh();

protected:
    std::string m_ip;
    int         m_port;
    std::string m_url;
    std::string m_username;
    std::string m_passwd;
    int m_iLocalCh;
};

class CNetCliV8: public INetClient
{
public:

	CNetCliV8(INetCliConfig * config);
	virtual ~CNetCliV8();

	virtual INetCliConfig * GetConfig() ;

	virtual int     Login();
	virtual int     Logout();
	virtual int     StartMonitor( INetChannel* net_channel);
	virtual int     StopMonitor( INetChannel* net_channel);
	//!返回实际取的数据长度
	//virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

	virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
	virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

	//!报警委托
	virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
	virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

	virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
	virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

	virtual int GetParam(int TypeMain, int index, int subIndex, void* buf,  int size );
	virtual int SetParam(int TypeMain, int index, int subIndex, void* buf,  int size );

	virtual bool    IsLogin();
	virtual int SendPtzData(int channel,char *pData,unsigned int uiLen);
    
protected:
	CNetCliCfg_V8*   m_config;
	CNetChnnel_V8*  m_pChannel;

	int m_engineId;//rtsp连接标识
	bool m_bIsMonitor;
	CMutex    m_Mutex;

	IV8Cli* m_pV8Cli;
};


#endif
