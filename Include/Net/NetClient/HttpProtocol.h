#ifndef _HTTPPROTOCOL_H__
#define _HTTPPROTOCOL_H__

typedef enum
{
	eHttpMethod = 0,
	eHttpGet,
	eHttpPost,
	eHttpOptions,
	eHttpDescribe,
	eHttpAnnounce,
	eHttpSetup,
	eHttpPlay,
	eHttpPause,
	Teardown,
	eHttpRspHttp,
	eHttpRspRtsp,
	eHttpRspSip,

	eHttpFrom,
	eHttpTo,
	eHttpCSeq,
	eHttpCallID,
	eHttpMaxForwards,
	eHttpContact,
	eHttpContentType,
	eHttpContentLength,
	eHttpUrl,
	eHttpVia,
	eHttpVontentTypeString,
	eHttpAccept,
	eHttpUserAgent,
	eHttpHost,
	eHttpXClientAddress,
	eHttpXTransactionID,
	eHttpSetCookie,
	eHttpDate,
	eHttpServer,
	eHttpPublic,
	eHttpCookie,
	eHttpAcceptEncoding,
	eHttpAcceptLanguage,
	eHttpAllow,
	eHttpBandwidth,
	eHttpBlockSize,
	eHttpScale,
	eHttpSpeed,
	eHttpConference,
	eHttpConnection,
	eHttpContentBase,
	eHttpContentEncoding,
	eHttpContentLanguage,
	eHttpRange,
	eHttpRtpInfo,
	eHttpSession,
	eHttpTimestamp,
	eHttpTransport,
	eHttpWwwAuthenticate,
	eHttpUnsupport,
	eHttpVary,
	eHttpExpires,
	eHttpLastModified,
	eHttp_x_Cache_Control,
	eHttp_x_Accept_Retransmit,
	eHttp_x_Accept_Dynamic_Rate,
	eHttp_x_Dynamic_Rate,
	eHttpXSequence,
	eHttpSoapAction,
	eHttpNr,
}eFieldType;

typedef struct
{
	int fieldNum;
	int fieldLen;
	int bodyLen;
}sHttpCfg;

#endif /*_HTTPPROTOCOL_H_*/

