#ifndef _Sunell_ALARM_H
#define _Sunell_ALARM_H

#include "MultiTask/Thread.h"
#include "Onvif/soapApp.h"
#include "INetClient.h"
#include "MultiTask/Timer.h"
#include "ClientPureC.h"

enum
{
    BOOK_STATE_NOT_ENABLE = 0,/* 不使能 */        
    BOOK_STATE_NOT_WORKING = 1,/* 配置了但是未能注册成功，或者renew失败*/
    BOOK_STATE_WORKING = 2,/* 工作正常 */
        
};

#define MAX_SUNELL_ALARM 32

enum
{
    SUNELL_ALARM_MSG_TYPE_START = 0,/* 开始报警 */
    SUNELL_ALARM_MSG_TYPE_RENEW = 1,/* 续订报警 */
    SUNELL_ALARM_MSG_TYPE_STOP = 2/*取消订阅*/
};


typedef struct _sunell_alarm_msg_struct
{
    int    iType;
    char ip[16];
    int    iPort;
}SUNELL_ALARM_MSG_STRUCT;

#define MAX_SUNELL_ALARM_LIST_SIZE 10

class CSunellAlarm
{
public:
    CSunellAlarm(int iCh);
    ~CSunellAlarm();

    VD_INT32 SendSubscribeRule();
    VD_INT32 SendUnsubscribe();
    VD_INT32 SendRenew();

    VD_INT32 StartAlarm(VD_INT32 iCh,const char *strIP);
    VD_INT32 StopAlarm();
    VD_INT32  Process();
    VD_INT32  Notify(VD_INT32 iType,VD_INT32 iState);
    VD_INT32 ProcessSubAgain();
    VD_INT32 ProcessKeepAlive();

    //!报警委托
    VD_INT32 AttachDelegateAlarm( VD_INT32 channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    VD_INT32 DetachDelegateAlarm( VD_INT32 channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

protected:
    typedef std::list<SUNELL_ALARM_MSG_STRUCT>ALARM_MSG_List;
    ALARM_MSG_List m_AlarmMsgList;
    CMutex   m_list_Mutex;

    SUNELL_ALARM_CONFIG_INFO m_AlarmConfigInfo;
        
private:
    CMutex                m_Mutex;
    SIG_CLI_ALARM* m_psigAlarm;
    VD_BOOL             m_BBookFlag; 
    VD_BOOL             m_iLocalCh; 
    VD_INT32             m_iBookState;
};

class CSunellAlarmManager : public CThread
{
public:

    PATTERN_SINGLETON_DECLARE(CSunellAlarmManager);
    CSunellAlarmManager(VD_VOID);
    virtual ~CSunellAlarmManager(VD_VOID);

    CSunellAlarm *GetCSunellAlarm(VD_INT32 iCh);
    VD_INT32 FreeCSunellAlarm(CSunellAlarm *pAlarm);
    
    VD_INT32 Start();
    VD_INT32 Stop();
    VD_INT32 Notify(VD_INT32 iType, VD_INT32 iState,char *pAddr);
    VD_INT32 GetChByAddr(char *pAddr);
    void onTimer(uint arg);

    virtual void ThreadProc();
private:
    CTimer            m_cTimer;
    CMutex            m_AlarmMutex;    
    CSunellAlarm* m_pSunellAlarm[MAX_SUNELL_ALARM];
};

#define g_SunellAlarmManager (*CSunellAlarmManager::instance())

#endif


