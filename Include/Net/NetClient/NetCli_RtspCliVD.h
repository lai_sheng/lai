#pragma once

#ifdef NET_CLIENT_RTSP

#include "Rtsp/rtsp/RtspClient.h"
#include "Rtsp/rtsp/RtspMedia.h"
#include "MultiTask/Mutex.h"
#include "APIs/Capture.h"
#include "MultiTask/Thread.h"

namespace NetCliLib
{

    using namespace CommonLib_VD;
    class INetCliRtspListener
    {
    public:
        //!chn-通道, AVType-0x00未定义数据类型:, 0x01视频, 0x10音频， 0x11音视频复合 
        //!pPacket-数据
        virtual int OnData(int chn, int AVType, int stream, CPacket* pPacket) = 0;
        //!对端关闭rtsp连接
        virtual int OnPeerClose(int chn) = 0;
    };

    //!媒体处理器
    class CRtspMediaCliVD : public CommonLib_VD::CRtspMediaCli
    {
    public:
        CRtspMediaCliVD( INetCliRtspListener* pListener, std::string track_url );
        virtual ~CRtspMediaCliVD();
        
        int DoMedia();
    protected:
        std::string m_track_url;
        INetCliRtspListener* m_pListener;

        //!CRtspMedia 接口
    public:
        virtual bool StartMedia();
        virtual bool StopMedia();
        virtual std::string GetTrackUrl();
        virtual int GetResolution();

    protected:
        int m_iWaitFrameTail;
    };

    class CRtspMediaCliVDManager : public CThread
    {
    public:
    #define MAX_RTSP_MEDIA_NUM 32
    public:
        static CRtspMediaCliVDManager* instance();
        int AddClient(CRtspMediaCliVD* pClient);
        int RemoveClient(CRtspMediaCliVD* pClient);

        int Start();
        int Stop();
    protected:
        CRtspMediaCliVDManager();
        virtual ~CRtspMediaCliVDManager();
        
        CRtspMediaCliVD* m_pRtspMedia[MAX_RTSP_MEDIA_NUM];
        CMutex m_Mutex;
        //!CThread 继承
    public:
        virtual void ThreadProc();
    };
    
    class CRtspCliGeneral : public CommonLib_VD::CRtspClient
    {
    public:
        CRtspCliGeneral(INetCliRtspListener* pListener);
        virtual ~CRtspCliGeneral(void);

        int SetChannel(int chn, enum capture_size_t stream);
        int SetURL(std::string url);
        virtual void OnRtspDiscon(int engineId, int connId);
    private:
        std::string m_track_url;
        INetCliRtspListener* m_pListener;
        int m_chn;
        enum capture_size_t m_stream;

        //!CRtspClient 实现接口
    public:
        virtual CRtspMedia* CreateMedia(int engineId, int connId, std::string rtsp_url);
        virtual void DestoryMedia(CRtspMedia*& pRtspMedia);

    public:    //!INetCliRtspListener接口
        virtual int OnData(int chn, int AVType, int stream, CPacket* pPacket);
        //!对端关闭rtsp连接
        virtual int OnPeerClose(int chn);
    };

}

#endif
