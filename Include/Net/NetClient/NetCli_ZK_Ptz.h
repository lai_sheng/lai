#ifndef _NETCLI_ZK_PTZ_H
#define _NETCLI_ZK_PTZ_H

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string.h>
#include <limits>

class CZKPtz
{
public:
    CZKPtz(std::string hostip);
    ~CZKPtz();

    char* packetBody(void* value, int len, int chan = 0);
private:
    void ParsePtzOpt(void* value, int len);

private:
    std::string m_Username;
    std::string m_Password;
	std::string m_HostIP;

    std::string m_Function;
    std::string m_ArgumentName1;
    std::string m_ArgumentValue1;
    std::string m_ArgumentName2;
    std::string m_ArgumentValue2;
    std::string m_ArgumentName3;
    std::string m_ArgumentValue3;
    std::string m_str;
};

#endif


