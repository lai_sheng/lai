#ifdef NET_CLIENT_EXPAND

#ifndef __NETCLI_INF_EXPAND_H__
#define __NETCLI_INF_EXPAND_H__

#include "System/Object.h"
#include "System/BaseTypedef.h"
#include "Net/NetClient/NetConnectExpand.h"

#define MAX_DECODE_CH 48

class CNetCliInfExpand
{
public:
    CNetCliInfExpand(VD_INT32 iCh);
    ~CNetCliInfExpand(VD_VOID);

    /* 数据回调 */
    VD_VOID OnData(int iCh,CPacket *pPacket,int iFrameType);

    /*允许挂载多个钩子*/
    VD_INT32 AttachDelegate(CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );
    VD_INT32 DetachDelegate(CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );
    
 private:
    CMutex m_cMutex;

    SIG_MONITOR     *m_pSigMonitor;
    VD_BOOL             m_BWaitIframe;

//    VD_BOOL         m_BNetInfState;
    VD_INT32        m_iCh;
    VD_INT32        m_iType;//0 tcp 1 udp
};

class CNetCliInfExpandManager : public CObject
{
public:
    PATTERN_SINGLETON_DECLARE(CNetCliInfExpandManager);
    CNetCliInfExpandManager(VD_VOID);
    ~CNetCliInfExpandManager(VD_VOID);

    VD_BOOL Lock(VD_VOID);
    VD_BOOL UnLock(VD_VOID);
    
    CNetCliInfExpand *GetNetCliInf(VD_INT32 iCh);
    CNetCliInfExpand *m_astNetCliInf[MAX_DECODE_CH];
private:
    CMutex m_CliInfmnMutex;
};

#define g_NetCliInfExPandManager (*CNetCliInfExpandManager::instance())

#endif
#endif

