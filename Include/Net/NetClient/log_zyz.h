#include <map>
#include <string>
#include <assert.h>
//#include "System/Time.h"
#include "APIs/Thread.h"
#ifdef WIN32
//#include "Windows.h"
#endif

#ifndef _LOG_ZYZ_H_
#define _LOG_ZYZ_H_
namespace Util_zyz
{
typedef enum
{
	PF_ConstatString,	//!字符串前缀,需要传递具体的字符串参数进去
	PF_Time,			//!log时间
	PF_FileName,		//!文件名
	PF_LineNumber,		//!行号
	//PF_ClassPointer,	//!类的this指针
	PF_ThreadPid,		//!线程号
	PF_Enum,
}PREFIX_E;
class CPrefix;
class CLog_zyz
{
public:	//typedef

	/*
	*@brief
	*	FATAL   指出每个严重的错误事件将会导致应用程序的退出。
	*	ERROR	指出虽然发生错误事件，但仍然不影响系统的继续运行。
	*	WARN    表明会出现潜在错误的情形。
	*	INFO    表明 消息在粗粒度级别上突出强调应用程序的运行过程。
	*	DEBUG	指出细粒度信息事件对调试应用程序是非常有帮助的。
	*	ALL     是最低等级的，用于打开所有日志记录。
	*/
	typedef enum
	{
		LL_None,
		LL_Fatal,
		LL_Error,
		LL_Warn,
		LL_Info,
		LL_Debug,
		LL_All,
		LL_Num,
	}LOG_LEVEL_E;

	typedef enum
	{
		LP_General,
		LP_Change_line,
	}LOG_PRETTY_E;

	typedef std::map<PREFIX_E, CPrefix*> PREFIX_LIST;
public:
	CLog_zyz();
	~CLog_zyz();
	static CLog_zyz* GetInstance();
	/*
	*@brief 增加前缀, param-没有参数时，传空, 一种prefix只能增加一个
	*@enable -true时打印此前缀，否则不打印此前缀
	*/
	int				SetPrefix( PREFIX_E prefix, bool enable, char * param );
	CPrefix*		GetPrefix( PREFIX_E prefix );
	int				SetLogLevel( LOG_LEVEL_E log_level );
	LOG_LEVEL_E		GetLogLevel();
	void			Log( LOG_LEVEL_E log_level, char const *file, int line, char * const fmt, ... );
	void			Log( LOG_LEVEL_E log_level, char const *file, int line, char * const fmt, va_list argptr);
	/**
	 * @brief 输出方案,让输出更适合观看.
	 */
	void 			SetPretty( LOG_PRETTY_E pretty );
protected:
	LOG_LEVEL_E		m_level;
	PREFIX_LIST		m_MsgMap;
	LOG_PRETTY_E	m_pretty;
	char			m_buffer[8192];
	std::string		m_Log_Level_str[LL_Num];
private:
public:

};

class CPrefix
{
public:
	//!初始化disable
	CPrefix( PREFIX_E const prefix )
		:m_param_len(256)
	{
		m_bEanble = false;
		m_prefix = prefix;
	}
	~CPrefix() {}
	int SetParam( char const * str );
	
	int SetActive( bool enable )
	{
		m_bEanble = enable;
		return 0;
	}
	bool GetActive()
	{
		return m_bEanble;
	}
	std::string& ToString();
	
	//int Format();
protected:
	bool m_bEanble;
	std::string m_param_str;
	std::string m_dump_str;
	const int	m_param_len;
	PREFIX_E	m_prefix;
private:
};

#ifdef WIN32
#if _MSC_VER < 1400 //!vs2003, vc6 不支持可变参数宏
void Log_Fatal(CLog_zyz* pLogzyz, char * const fmt, ... );
void Log_Error(CLog_zyz* pLogzyz, char * const fmt, ... );
void Log_Warn(CLog_zyz* pLogzyz, char * const fmt, ... );
void Log_Info(CLog_zyz* pLogzyz, char * const fmt, ... );
void Log_Debug(CLog_zyz* pLogzyz, char * const fmt, ... );
void Log_All(CLog_zyz* pLogzyz, char * const fmt, ... );

void g_Log_Fatal(char * const fmt, ... );
void g_Log_Error(char * const fmt, ... );
void g_Log_Warn(char * const fmt, ... );
void g_Log_Info(char * const fmt, ... );
void g_Log_Debug(char * const fmt, ... );
void g_Log_All(char * const fmt, ... );
#else
#define Log_Fatal(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Fatal, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );
#define Log_Error(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Error, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_Warn(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Warn, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_Info(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Info, __FILE__, __LINE__, fmt, ## __VA_ARGS__ );
#define Log_Debug(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Debug, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_All(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_All, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );

#define g_Log_Fatal(fmt, ...)	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Fatal, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );
#define g_Log_Error(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Error, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_Warn(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Warn, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_Info(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Info, __FILE__, __LINE__, fmt, ## __VA_ARGS__ );
#define g_Log_Debug(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Debug, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_All(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_All, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );

#endif

#else

#if 0

#define Log_Fatal(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Fatal, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );
#define Log_Error(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Error, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_Warn(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Warn, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_Info(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Info, __FILE__, __LINE__, fmt, ## __VA_ARGS__ );
#define Log_Debug(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_Debug, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define Log_All(clog_zyz, fmt, ...) 	(clog_zyz)->Log(CLog_zyz::LL_All, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );

#define g_Log_Fatal(fmt, ...)	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Fatal, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );
#define g_Log_Error(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Error, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_Warn(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Warn, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_Info(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Info, __FILE__, __LINE__, fmt, ## __VA_ARGS__ );
#define g_Log_Debug(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_Debug, __FILE__, __LINE__, fmt,  ## __VA_ARGS__);
#define g_Log_All(fmt, ...) 	(CLog_zyz::GetInstance())->Log(CLog_zyz::LL_All, __FILE__, __LINE__, fmt,  ## __VA_ARGS__ );
#endif

#define Log_Fatal(clog_zyz, fmt, ...) 	
#define Log_Error(clog_zyz, fmt, ...) 	
#define Log_Warn(clog_zyz, fmt, ...) 	
#define Log_Info(clog_zyz, fmt, ...) 	
#define Log_Debug(clog_zyz, fmt, ...)
#define Log_All(clog_zyz, fmt, ...)

#define g_Log_Fatal(fmt, ...)
#define g_Log_Error(fmt, ...)
#define g_Log_Warn(fmt, ...)
#define g_Log_Info(fmt, ...)
#define g_Log_Debug(fmt, ...)
#define g_Log_All(fmt, ...)


#endif

void log_main();

}

using namespace Util_zyz;

#endif
