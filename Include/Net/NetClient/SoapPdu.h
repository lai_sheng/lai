#ifndef BELL_VAP_PDU_
#define BELL_VAP_PDU_

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string.h>
#include <limits>

#include "HttpPdu.h"

#define MAX_SOAP_CONTENT_LEN   (1024 * 16)
#define MAX_SOAP_STRCMD_LEN   (30)

class CSoapPdu
{
public:
    CSoapPdu();
    ~CSoapPdu();
    static CSoapPdu* createPduWith(int method, char* strCmd);

    int parseBody(const  char* strData, size_t len, size_t &iLen);
    char* packetBody(void* value, int len, int chan = 0);

    CHttpPdu* getHdr() { return m_pHttpHdr; }
    void setHdr(CHttpPdu* pHdr) { m_pHttpHdr = pHdr; }

private:
    void ParsePtzOpt(void* value, int len);
    char* packetBodySunny(void* value, int len, int chan);
    char* packetBodyHuanghe(void* value, int len, int chan);

private:
    CHttpPdu* m_pHttpHdr;
    char m_SoapNameSpace[MAX_SOAP_CONTENT_LEN];
    int  m_SoapContentOff;
    char m_acCmd[MAX_SOAP_STRCMD_LEN];
};
#endif

