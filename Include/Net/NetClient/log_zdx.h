#ifndef _LOG_ZDX_H_
#define _LOG_ZDX_H_

#include <assert.h>

enum LOG_MODULE_ZDX
{
    MODULE_INF          =0,
    MODULE_CON_MNG      =1,
    MODULE_NCISIMPLE    =2,
    MODULE_NCI_MNG      =3,
    MODULE_NCI_VS300    =4,
    MODULE_NCI_BELL    =5,
    MODULE_BUTT         =6
};

enum LOG_LEVEL_ZDX
{
    LOG_LEVEL_ZDX_NULL  = 0,
    LOG_LEVEL_ZDX_FATAL = 1,
    LOG_LEVEL_ZDX_ERROR = 2,
    LOG_LEVEL_ZDX_WARN  = 3,
    LOG_LEVEL_ZDX_INFO  = 4,
    LOG_LEVEL_ZDX_DEBUG = 5,
    LOG_LEVEL_ZDX_ALL   = 6
};

extern int g_iLogLevel[MODULE_BUTT];
    
void zdx_set_log_level(int module, int level);

//#define __trip trace("-W-%d::%s(%d)\n", (int)time(NULL), __FILE__, __LINE__);

#ifndef WIN32

#define PRINT_ZDX(module, level,fmt, ...) \
    do{                                   \
        if (module>=MODULE_BUTT || level < 0 ) \
        {                                      \
        }else if ( g_iLogLevel[module] >= level ) \
        {   \
   			SYSTEM_TIME t;  \
   			SystemGetCurrentTime(&t);   \
            printf("%02d:%02d:%02d|Md(%d)Lv(%d):", t.hour, t.minute, t.second,module,level); \
            printf(fmt,## __VA_ARGS__); \
        } \
      }while(0);

#define ZL_Fatal(module,fmt, ...)  	PRINT_ZDX(module,LOG_LEVEL_ZDX_FATAL,fmt,## __VA_ARGS__);
#define ZL_Error(module,fmt, ...) 	PRINT_ZDX(module,LOG_LEVEL_ZDX_ERROR,fmt,## __VA_ARGS__);
#define ZL_Warn(module,fmt, ...) 	PRINT_ZDX(module,LOG_LEVEL_ZDX_WARN,fmt,## __VA_ARGS__);
#define ZL_Info(module,fmt, ...) 	PRINT_ZDX(module,LOG_LEVEL_ZDX_INFO,fmt,## __VA_ARGS__);
#define ZL_Debug(module,fmt, ...)    PRINT_ZDX(module,LOG_LEVEL_ZDX_DEBUG,fmt,## __VA_ARGS__);
#define ZL_All(module,fmt, ...)      PRINT_ZDX(module,LOG_LEVEL_ZDX_ALL,fmt,## __VA_ARGS__);

#else

void ZL_Fatal(int module, char * const fmt, ...);  	
void ZL_Error(int module, char * const fmt, ...);	
void ZL_Warn(int module, char * const fmt, ...);	
void ZL_Info(int module, char * const fmt, ...);	
void ZL_Debug(int module, char * const fmt, ...);  
void ZL_All(int module, char * const fmt, ...);

#endif

#endif


