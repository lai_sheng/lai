/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：CRTPPacket.h
* 文件标识：参见配置管理计划书
* 摘　　要：RTP PDU封装类，实现RFC中定义的RTP PDU。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年7月15日
* 修订记录：稳定，标准RTP及带重发的RTP会话都完成了。

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年7月1日
* 修订记录：创建
*/

#ifndef __FE_DEC_RTP_PACKET_H__
#define __FE_DEC_RTP_PACKET_H__

//#include "RTPProtocol.h"
#include <vector>
#include <deque>

//#include <IAddRefAble.h>
/* 
 * RFC3550.txt
 * RTP: A Transport Protocol for Real-Time Applications
 *
 * The RTP header has the following format:
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |V=2|P|X|  CC   |M|     PT      |       sequence number         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           timestamp                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           synchronization source (SSRC) identifier            |
   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
   |            contributing source (CSRC) identifiers             |
   |                             ....                              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * version (V): 2 bits
 * padding (P): 1 bit
 * extension (X): 1 bit
 * CSRC count (CC): 4 bits
 * marker (M): 1 bit
 * payload type (PT): 7 bits
 * sequence number: 16 bits
 * timestamp: 32 bits
 * SSRC: 32 bits
 * CSRC list: 0 to 15 items, 32 bits each
 */
class CDecRTPPacket;

#define MIN_HEAD_LENGTH		12
#define PROTOCOL_VERSION	2
#define RTP_BUFFER_LENGTH 1024*5	//9192
enum PayloadTypes 
{
	PCMU,         // G.711 u-Law
	FS1016,       // Federal Standard 1016 CELP
	G721,         // ADPCM - Subsumed by G.726
	G726 = G721,
	GSM,          // GSM 06.10
	G7231,        // G.723.1 at 6.3kbps or 5.3 kbps
	DVI4_8k,      // DVI4 at 8kHz sample rate
	DVI4_16k,     // DVI4 at 16kHz sample rate
	LPC,          // LPC-10 Linear Predictive CELP
	PCMA,         // G.711 A-Law
	G722,         // G.722
	L16_Stereo,   // 16 bit linear PCM
	L16_Mono,     // 16 bit linear PCM
	G723,         // G.723
	CN,           // Confort Noise
	MPA,          // MPEG1 or MPEG2 audio
	G728,         // G.728 16kbps CELP
	DVI4_11k,     // DVI4 at 11kHz sample rate
	DVI4_22k,     // DVI4 at 22kHz sample rate
	G729,         // G.729 8kbps
	Cisco_CN,     // Cisco systems comfort noise (unofficial)

	CelB = 25,    // Sun Systems Cell-B video
	JPEG,         // Motion JPEG
	H261 = 31,    // H.261
	MPV,          // MPEG1 or MPEG2 video
	MP2T,         // MPEG2 transport system
	H263,         // H.263

	LastKnownPayloadType,

	DynamicBase = 96,
	DynamicVideo = DynamicBase,
	DynamicAudio = 97,
	MaxPayloadType = 127,
	IllegalPayloadType
};



//class CRTPPacket:public IAddRefAble
class CDecRTPPacket
{
public:
	CDecRTPPacket();
	virtual ~CDecRTPPacket();

	void AddCSRC(int csrc);

	/* 版本号, 根据RFC3550定义, 版本号为2 */
	int  GetVersion() ;
	void SetVersion(int version);

	/* RTP头部扩展, 目前不支持 */
	bool GetExtension() ;
	void SetExtension(bool ext);

	/* 标记, 用来标记I帧 */ 
	bool GetMarker() ;
	void SetMarker(bool mark);

	/* 包体数据类型, DVR的流媒体数据类型设为DynamicBase */
	PayloadTypes GetPayloadType() ;
	void SetPayloadType(PayloadTypes t);

	/* 包序号 */
	unsigned short GetSequenceNumber() ;
	void SetSequenceNumber(unsigned short n);

	/* 包时戳 */
	unsigned long GetTimestamp() ;
	void SetTimestamp(unsigned long time);

	/* SSRC, 同步源标识 */
	unsigned long GetSyncSource() ;
	void SetSyncSource(unsigned long ssrc);

	/* CSRC, 可选同步源标识	 */
	int  GetContribSrcCount() ;
	unsigned long GetContribSource(int index) ;
	void SetContribSource(int index, unsigned long csrc) ;

	/* RTP头长度 */
	int GetHeaderSize() ;

	/* RTP头部扩展, 目前不支持 */
	unsigned short GetExtensionType() ;
	void SetExtensionType(unsigned short type);
	void SetExtensionSize(unsigned short len);
	unsigned short GetExtensionSize() ;
	char* GetExtensionPtr() ;

	/* 添加负载数据 */
	int AddPayload(char* data, int len);
	/* 向负载尾部添加数据 */
	int AppendPayload(char* data, int len);	
	/* 读取负载数据,不包括RTP头数据. */
	char* ReadPayload(int& len);

	int GetPayloadLen(void);

	int Copy(CDecRTPPacket* packet);

	int SetBuffer(char* data, int len);
	char* GetBufferPtr(void);
	int GetBufferLen(void);



protected:
	int	_payloadLen;	// total size = sizeof(header) + sizeof(payload)
	char _buffer[RTP_BUFFER_LENGTH];
	int _bufferLen;

};

#endif
