
/*!
 * \brief   网络捕获
 * \date    2008/10/31:10:2008   9:27
 * \file    d:\code\P_2008.08.11_GBU4\Net\NetCLient\NetCapture.h
 * \author  zhang_yaozhen
 * \bug
 */
#ifndef __NETCAPTUREMANAGER_H__
#define __NETCAPTUREMANAGER_H__

#include "MultiTask/Timer.h"

#include "Net/NetClient/CNetChannel.h"
#include "Net/NetClient/NetCapture.h"

#include "MultiTask/Thread.h"

class CaptureHelper;
class CNetChannelCapture;
class CNetCaptureManager;

typedef struct tagDVRINFO
{
    char dvrip[32]; 
    int  dvrport; 
    char dvrusername[32]; 
    char dvrpassword[32];
    int  dvrRecchannel; 
}DVRINFO, *PDVRINFO; 

//!目前把网络通道数目设为模拟实际通道数最大一样
//#define MAX_NET_CHANNEL    32   //heyidan
#define MAX_NET_CHANNEL    2    //主次
  
typedef enum _CON_PROTOCOL_ {
    CON_PROTOCOL_TCP = 1,
    CON_PROTOCOL_UDP= 2,
    CONN_TYPE_ERROR
}CON_PROTOCOL;


typedef struct tagCHANNELSTATE
    {    
        unsigned int Traffic; //网络数据流量
        int   iVideoWidth;   //视频宽
        int   iVideoHeight; //视频高        
        int      Frame;     //帧率
        unsigned int DecTraffic; //解码数据流量
        int  chnsate;    //通道处于什么状态1回放，2监视，3空闲
    }CHANNELSTATE; 

enum _PlayCommand{
    PBC_START,
    PBC_PAUSE,
    PBC_RESUME,
    PBC_FAST,
    PBC_SLOW,
    PBC_STOP
};

typedef struct tagErrChan_Info
{
    ulong errChannel[6];
}ERRCHANNELINFO;


#include "Configs/ConfigDigiChManager.h"

class CNetCaptureManager : public CObject
{

    enum _streamtype{
        ST_MONITOR,
        ST_PLAY
    };

    
    enum dspInfo{
        dspError = -1,        //加载DSP失败
        fmtError = -2,        //不支持的格式
        manuError = -3,        //不支持的制式 
        reslError = -4,        //不支持的分辨率
        ifraError = -5,        //找不到I帧
    };

    enum ErrChanInfo{
        manuErrChan = 2,    //不支持制式的通道标识
        reslErrChan = 3,  //不支持分辨率的通道标识
        fmtErrChan = 4,   //不支持格式的通道标识
        ifraErrChan = 5,  //找不到I帧的通道标识
        ErrChanNum = 6,
    };

public:
    CNetCaptureManager();
    ~CNetCaptureManager();

    //! 初始数字摄像头解码器
    int Start( );
    int Stop( );

    void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data = NULL);

    int StartAlarm( );
    int StopAlarm( );

   // VD_BOOL Locate(int iCh,  DVRINFO* pdvrinfo,  FILE_INFO *fileinfo, int playbackway);  //0 playback by file, 1 by time
    VD_BOOL PlayCtrl(int iCh, int comm);
    VD_BOOL CtrlRecFile(int iCh, int offset);


    int StopMonitor( LOGIC_CHN channel );
    int  GetDecChannelState(unsigned int i, CHANNELSTATE *chnste); 
    VD_BOOL GetChannelisSeeking(int i);
    uint  GetDownLoadSize(int i);
    int  SetDownLoadSize(int i, uint uisize, VD_BOOL bSeeking);
    int  GetMode(int channelID); 
    int  GetChannelStreamType(int channelID); 
    VD_BOOL SetPlayBackProc(int channel,  CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc);

    void OnNetCap(uint arg);
    ERRCHANNELINFO getErrChanInfo();
    void clearChannelInfo();

    void ReStartChannel();

    VD_UINT32 GetDecodeState( );
    VD_VOID   OnTimer(VD_UINT32 arg);

    //!获取设置配置
    int GetParam(VD_INT32 iCh,int TypeMain, int index, int subIndex,void* buf,  int size);
    int SetParam(VD_INT32 iCh,int TypeMain, int index, int subIndex,void* buf,  int size);
   bool  IsLogin(VD_INT32 iCh);

    VD_VOID   SendPtzData(VD_INT32 iCh,VD_VOID *pData,VD_UINT32 uiLen);
    VD_INT32  GetRemoteCh(LOGIC_CHN iCh);
    VD_INT32  GetCurrentConfigType(LOGIC_CHN iCh);

    VD_VOID   SetConfigNet( LOGIC_CHN channel, VD_BOOL BEnable, REMOTE_CH_CFG &stRemoteChCfg);
    VD_INT32  SetReconnect(LOGIC_CHN iCh, int iFlag );

public:
    //CNetDevAlarm * GetNetDevAlarm();
    //!如果不存在，就创建
    CNetChannelCapture * GetDevInstance( LOGIC_CHN channel, VD_BOOL create = true );
    static CNetCaptureManager * instance(  );
protected:
    int ConsoleHelp(int argc, char **argv);
	
private:
    void NotifyDSPInfo(int dwState, VD_BOOL bList);    ///dsp状态
    void NotifyDecCaps(int iCifCaps);
private:
    CNetChannelCapture * m_net_channel[MAX_NET_CHANNEL];
    CTimer            m_NetBitRateTimer;             /*!< 处理码流的定时器 */
    //CNetDevAlarm*    m_pNetDevAlarm;
    VD_BOOL m_isDump;               //是否需要串口打印
    int     m_CurDecVideoCaps[MAX_NET_CHANNEL];         //当前各通道解码格式－－主要是分辩率* 帧率
    int     m_CurTotalVideoCaps;
    int     m_dspInfoMask;

    int    m_StreamType[MAX_NET_CHANNEL];

    DVRINFO   m_dvrinfo[MAX_NET_CHANNEL];
//    FILE_INFO m_tmpfileinfo; //保存一份默认配置供关闭回放使用
//    FILE_INFO m_fileinfo[MAX_NET_CHANNEL]; //保存回放文件信息
    int       m_iplaybackway[MAX_NET_CHANNEL]; // 通道得回放方式

    int            m_iMode[MAX_NET_CHANNEL];
    ulong          m_tmpDownLoadSize[MAX_NET_CHANNEL];
    VD_BOOL        m_bSeeking[MAX_NET_CHANNEL]; // 0:seekover 1:seeking
    CObject * m_pbpObj[MAX_NET_CHANNEL];
    SIG_DEV_PLAYBACKPROC_DELEGATE m_sigProc[MAX_NET_CHANNEL];
    CMutex      m_GetDecChnMutex;
    ERRCHANNELINFO     m_chanErrInfo;

    ICaptureManager* m_pCaptureM;

    VD_UINT32    m_uiDecodeState;
    CTimer       m_UpdateTimer;        //解码状态刷新定时器
    
    //CNetTour m_NetTour;
    CMutex m_NetCaptureMutex;
	#define GUARD_NET_CAPTURE_MUTEX CGuard guard(m_NetCaptureMutex)
};

#endif

