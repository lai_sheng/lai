#if 1

#ifndef __NETCLIRTSPSUNELL_H__
#define    __NETCLIRTSPSUNELL_H__

#ifdef NET_CLIENT_RTSP

#include "MultiTask/Thread.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "CNetChannel.h"
#include "INetClient.h"
#include "INetConListener.h"
#include "NetCli_RtspCliVD.h"
#include "SamsungPtz.h"

#define ONVIF_PTZ_MAX_CON_NUM 1

typedef enum {
    ONVIF_TCP_STATE_INIT  = 0,
    ONVIF_TCP_STATE_CONNECT = 1,
    ONVIF_TCP_STATE_WAITING = 2,
    ONVIF_TCP_STATE_WORKING = 3,
    ONVIF_TCP_STATE_BUTT = 0xFF
}ONVIF_TCP_STATE;

typedef struct __tcp_dlg_context_onvif_ptz
{
 //   VD_BOOL BAllocFlag;
     /* 对于主动连接的套接字需要处理连接状态机 */
    VD_INT32 iState;
    VD_INT32 iCh;
    
    /* recv */
    ez_socket_t astSocket;

    /* 阻塞方式? */
 //   bool bNonBlock;
    /* remote add */
    struct sockaddr_in RemoteAddr;
    
    /* 上次访问时间 */
    struct timeval lastActiveTime;        // 最后一次接收或发送的时间，用于短连接超时逻辑处理
    CABuffer m_pSendBuf;
}TCP_DLG_CONTEXT_ONVIF_PTZ;

typedef struct _ptz_msg_struct
{
    int   iType;
    char data[20];
}PTZ_MSG_STRUCT;

class CNetCliRtspSunell : public NetCliLib::INetCliRtspListener, public INetClient
{
public:
    #define MAX_CHANNEL_NUM 16
public:
 
    CNetCliRtspSunell(INetCliConfig * config);
    virtual ~CNetCliRtspSunell();
    
    virtual INetCliConfig * GetConfig() ;
    
    virtual int     Login();
    virtual int     Logout();
    virtual int     StartMonitor( INetChannel* net_channel);
    virtual int     StopMonitor( INetChannel* net_channel);

    virtual int StartAlarm();
    virtual int StopAlarm();

    //!返回实际取的数据长度
    virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

    virtual int GetParam(int TypeMain, int index, int subIndex, void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex, void* buf,  int size );

    //virtual int     KeepAlive();
    //!返回true 表示已经登录，false没有登录
    virtual bool    IsLogin() ;
    //!返回true 表示此通道正在监视
    //virtual bool    TestOpenMonitorChannel( const int channel ) ;
    //virtual     TSignal2<DWORD, CPacket* >& GetMonitorHandler(const int channel);

    fd_set m_wFds;

    /* onvif云台相关 */
    virtual int SendPtzData(int channel,char *pData,unsigned int uiLen);
    VD_INT32 GetSocketErrorNo(VD_INT32 iSock);
    VD_BOOL DoConnect();
    VD_BOOL DoTCP_STATE_CONNECT();
    VD_BOOL DoTCP_STATE_WAITING();
    VD_BOOL DoTCP_STATE_INIT();
    VD_INT32 CheckSennQueen(void);
    VD_INT32 OnOnvifPtzData(fd_set* read_set);
    VD_INT32 GatherPtzReadSockSet( fd_set* read_set, int& max_fd );
    VD_INT32 CloseConnection();
    VD_INT32 CheckTimeOut();

    CSamsungPtz *m_pSamsungPtz;
    
public: //!INetCliRtspListener 接口
    virtual int OnData(int chn, int AVType, int stream, CPacket* pPacket);
    virtual int OnPeerClose(int chn);
protected:
    TCP_DLG_CONTEXT_ONVIF_PTZ m_stCon;

    typedef std::list< PTZ_MSG_STRUCT > PTZ_MSG_List;
    PTZ_MSG_List m_PtzMsgList;
    CMutex m_list_Mutex;
    
    CNetCliRtspCfg_Sunell* m_config;
    CNetChnnel_RtspSunell*            m_pChannel[MAX_CHANNEL_NUM];
    TSignal2<ulong, CPacket* >* m_pSigMonitor[MAX_CHANNEL_NUM];
    SIG_DEFAULT* m_pSigDef;

    int m_engineId[MAX_CHANNEL_NUM];//rtsp连接标识
    bool m_bIsMonitor;
    CMutex    m_Mutex;
    CMutex    m_socketMutex;
    struct timeval m_SelectTimev;
    PTZ_MSG_STRUCT m_LastPtzMsg;
    
    //!对应于16个通道
    NetCliLib::CRtspCliGeneral* m_pRtspCli[MAX_CHANNEL_NUM];
};

class CNetCliRtspSunellPtzManager : public CThread
{
public:
    static CNetCliRtspSunellPtzManager * instance();
    CNetCliRtspSunellPtzManager();

    int AddNetCli( const CNetCliRtspSunell * pNetCli );
    int RemoveNetCli( CNetCliRtspSunell * pNetCli );
        
    void Start();

    //! 关闭线程
    void Stop();

    //! 线程的执行体
    void ThreadProc();

    void dump();
protected:
private:
        typedef std::list< CNetCliRtspSunell* > NCS_List;
        NCS_List m_CliCollect;
        NCS_List m_CliPool;

        CMutex m_list_Mutex;
        CMutex m_list_Pool_Mutex;
        bool m_bLock;
        CNetCliRtspSunell* m_bRemove;
};

#endif

#endif

#endif
