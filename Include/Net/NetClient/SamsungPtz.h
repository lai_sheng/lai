#ifndef SAMSUNG_PTZ_H
#define SAMSUNG_PTZ_H

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string.h>
#include <limits>

#include "HttpPdu.h"

class CSamsungPtz
{
public:
    CSamsungPtz();
    ~CSamsungPtz();

    char* packetBody(void* value, int len, int chan = 0);
    int parsePaket(void* value, int len);
    int setUserName(void* value, int len);
    int setPassword(void* value, int len);

private:
    void ParsePtzOpt(void* value, int len);

private:
    CHttpPdu* m_pHttpHdr;
    
    std::string m_Realm;
    std::string m_Qop;
    std::string m_Algorithm;
    std::string m_Nonce;
    std::string m_Username;
    std::string m_Password;

    std::string m_ArgumentName1;
    std::string m_ArgumentValue1;
    std::string m_ArgumentName2;
    std::string m_ArgumentValue2;
    std::string m_str;
};

#endif


