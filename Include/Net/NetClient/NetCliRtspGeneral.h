#if 1

#ifndef __NETCLIRTSPGENERAL_H__
#define    __NETCLIRTSPGENERAL_H__

#ifdef NET_CLIENT_RTSP

#include "MultiTask/Thread.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "CNetChannel.h"
#include "INetClient.h"
#include "INetConListener.h"
#include "NetCli_RtspCliVD.h"

/**
 *@brief Rtsp 通用版本的，一个连接只能接一路
*/
class CNetCliRtspGeneral : public NetCliLib::INetCliRtspListener, public INetClient
{
public:
 
    CNetCliRtspGeneral(INetCliConfig * config);
    virtual ~CNetCliRtspGeneral();
    
    virtual INetCliConfig * GetConfig() ;
    
    virtual int     Login();
    virtual int     Logout();
    virtual int     StartMonitor( INetChannel* net_channel);
    virtual int     StopMonitor( INetChannel* net_channel);
    //!返回实际取的数据长度
    virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

    virtual int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size );

    //virtual int     KeepAlive();
    //!返回true 表示已经登录，false没有登录
    virtual bool    IsLogin() ;
    //!返回true 表示此通道正在监视
    //virtual bool    TestOpenMonitorChannel( const int channel ) ;
    //virtual     TSignal2<DWORD, CPacket* >& GetMonitorHandler(const int channel);

public: //!INetCliRtspListener 接口
    virtual int OnData(int chn, int AVType, int stream, CPacket* pPacket);
    virtual int OnPeerClose(int chn);
protected:
    CNetCliRtspCfg_General* m_config;
    CNetChnnel_RtspGeneral*            m_pChannel;
    TSignal2<ulong, CPacket* >* m_pSigMonitor;
    SIG_DEFAULT* m_pSigDef;

    int m_engineId;//rtsp连接标识
    bool m_bIsMonitor;
    CMutex    m_Mutex;

    NetCliLib::CRtspCliGeneral* m_pRtspCli;
};


#endif

#endif

#endif
