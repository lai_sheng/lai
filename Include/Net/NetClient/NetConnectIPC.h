
#ifdef NET_CLIENT_SDK

#ifndef __NETCONNECT_IPC_H__
#define __NETCONNECT_IPC_H__

#include <string>
#include "INetClient.h"


class CNetCliDahuaII;
class CClientDVR;
class INetChannel;
class CNetChnnel_DahuaII;
class CNetChnDahuaIIAlarm;

class CNetConnectIPC : public INetClient
{
public:
    typedef enum
    {
        CONNECT_NONE    = 0,
        CONNECT_ONLY        = 1,
        CONNECT_LOGIN           = 2,
        CONNECT_MONITOR     =3,      //!已经打开监视一些通道了
        CONNECT_PLAYBACK   =4,       //!开启回放
    }NET_STATE_E;
#define assert_channel(channel)	\
    if( channel < 1|| channel > 16 )	\
    {	\
    	return -1;	\
    }
public:

	CNetConnectIPC( INetCliConfig * config );
	~CNetConnectIPC();

//<Methods>
	int		Init();

    //virtual int IsMatch( INetCliConfig * config, CNetChannel* net_channel ) ;
    virtual INetCliConfig * GetConfig() ;

	// 登陆IPC
	virtual int Login();

	// 退出IPC
	virtual int Logout();

	// 开始实时监视, channel从1开始
	virtual int StartMonitor(INetChannel* net_channel);

	// 关闭实时监视
	virtual int StopMonitor(INetChannel* net_channel);

	//brief 委托方式处理监视数据, 不需要再维护保洁状态, 委托者
	virtual int AttachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );
	virtual int DetachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );

	//virtual int GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

	//virtual int KeepAlive();

	// !返回true 表示已经登录，false没有登录
	virtual bool IsLogin();
	// !返回true 表示此通道正在监视
	//virtual bool TestOpenMonitorChannel( const int channel ) ;

	void Dump();
	int Locate(int chan, FILE_INFO *fileinfo, int playbackway); 
	int CtrlRecFile(int chan, int playbackway, int offset); 
//</Methods>


//<Attributes>
	NET_STATE_E				m_net_state;
    const DWORD           	m_monitor_keep_live_time;   //!监视,保活时间, 单位ms
    DWORD                 	m_last_monitor_recv_time;      //!上次收到监视数据时间
public:
	CNetCliDahuaII*			m_config;
	CClientDVR*				m_ClientDVR;
	CNetChnnel_DahuaII*			m_pChannel[16];
	TSignal2<DWORD, CPacket* >* m_pSigMonitor[16];
	TSignal2<DWORD, DWORD>*m_pSigPBProc[16]; 

    TSignal3<int, appEventCode, int>* m_psigAlarm[16];
    CNetChnDahuaIIAlarm* 	m_pAlarmChannel[16];

    bool            m_book_alarm;   //!是否订阅报警
private:
    int 		StartAlarm( CNetChnDahuaIIAlarm* net_channel );
    int			StopAlarm( CNetChnDahuaIIAlarm* net_channel );
	long 					m_loginRealPlay;
	long 					m_loginRecord;
	long					m_loginHandle;

//</Attributes>
};



#endif

#endif

