
#ifndef __INETCLIENT_H__
#define __INETCLIENT_H__

#ifndef WIN32
#include <sys/types.h>
#include <linux/unistd.h>
#include <errno.h>
#include <pthread.h>
#endif

#include "NetCliConfig-x.h"
#include "System/Packet.h"
#include "System/BaseTypedef.h"
#include "System/Signals.h"
#include "System/Function.h"
#include <map>

#define NET_CLIENT_SIMPLE

#define NET_CLIENT_OK	0
#define NET_FAILED		-1
//!支持的最大通道数
#define NET_MAX_CHAN	32

typedef union
{
	struct dvrip	dvrip; /* struct def */
	unsigned char	c[32]; /* 1 byte def */
	unsigned short	s[16]; /* 2 byte def */
	unsigned int	l[8];  /* 4 byte def */
}DVRIP_HEAD_T;


typedef struct  tagDVRIP_MSG
{
    DVRIP_HEAD_T   head;
    unsigned char *pData;
    int            iType;
    CPacket        *ext_data;
}DVRIP_MSG_T;

/// 捕获通道类型
typedef enum tagnet_capture_channel_t {
	NET_CHL_MAIN_T = 0,		///< 主通道	－	主码流1
	NET_CHL_2END_T = 1,		///< 辅通道	－	出辅码流2
	NET_CHL_3IRD_T = 2,		///< 辅通道	－	出辅码流3
	NET_CHL_4RTH_T = 3,		///< 辅通道	－	出辅码流4
	NET_CHL_JPEG_T = 4,		///< 辅通道	－	出JPEG抓图
	NET_CHL_Play_T,			///< 回放使用，把回放当作一个码流处理
	NET_CHL_FUNCTION_NUM
}NET_CAPTURE_CHANNEL_T;

void NetPrifxTrace( char const *file, int line );
//#define net_printf(fmt, args...)	printf("line[%d], file[%s]", __LINE__, __FILE__);printf(fmt, ##args);
//#define net_printf(fmt, args...)	printf("time[%u],line[%d], file[%s], threadID[%u] ",SystemGetMSCount(), __LINE__, __FILE__, static_cast<unsigned long>(pthread_self()) );printf(fmt, ##args);
//#define net_printf(fmt, ...) 	NetPrifxTrace(__FILE__, __LINE__); printf(fmt, ## __VA_ARGS__);
#if 0
#define net_trace( fmt, ... ) \
	do { \
		time_t now = time(NULL);	\
		localtime( &now ); \
		const char * p = strrchr( __FILE__, '\\' ); \
		if( p == NULL ) \
			p = __FILE__; \
		else \
			p += 1; \
			fprintf( stderr, "[%u][%s:%d] ", \
					SystemGetMSCount(), p, __LINE__);	\
			printf(fmt, ##args); \
	} while( 0 )
#endif

// !ulong 处理结果, ulong : 用户数据
enum DefEventCode
{
	DEC_Disconnect_No_Login,	// !断开, 不会自动登陆
	DEC_Disconnect_Auto_Login,	// !断开,但会自动登陆
};

//!ulong : 指示码流类型
//!CPacket 视频数据
typedef TSignal2_S<ulong, CPacket* > SIG_MONITOR;
typedef SIG_MONITOR::SigProc SIG_CLI_MONITOR_DELEGATE;

//!ulong : 指示回放数据总大小
//!ulong : 当前已回放的数据大小
typedef TSignal2<ulong, ulong>::SigProc SIG_CLI_PLAYBACK_PROC_DELEGATE;

// 报警和通道相关的表示具体通道数,无通道相关的值为-1, 从0开始
// 指示具体的报警类型
// true-表示相应报警开,false--表示相应报警关
typedef TSignal3<int, uint, int> SIG_CLI_ALARM ;
typedef SIG_CLI_ALARM::SigProc SIG_CLI_ALARM_DELEGATE;

typedef TSignal2<enum DefEventCode, ulong> SIG_DEFAULT;
typedef SIG_DEFAULT::SigProc SIG_CLI_Def_DELEGATE;
//int channel, appEventCode type, ulong state
#define DH_STRING(x) x==NULL?"NULL":x

class INetCliConfig;
class INetChannel;
class INetClient
{
public:

	enum RESULT_E
	{
		success,
		unkown_error,
		r_error_monitor_not_login,
	};
public:
    static INetClient * instance( INetCliConfig * config );
    //static void Release(INetClient *pstINC);
    virtual ~INetClient() {}

    virtual INetCliConfig * GetConfig() = 0;
    virtual int Login() = 0;
    virtual int Logout() = 0;

    /*!
         \brief 主动获取方式接收监视数据
        */
    //! chl_type 指示码流类型, channel从1开始
    //! result: r_error_monitor_not_login
    virtual int StartMonitor( INetChannel* net_channel ) =0;
    virtual int StopMonitor( INetChannel* net_channel ) = 0;

	virtual int StartAlarm() {return -1;}
	virtual int StopAlarm() {return -1;}

    /*!
	 * \brief 委托方式处理监视数据, 不需要再维护保洁状态, 委托者, channel从1开始
    */
    virtual int AttachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) = 0;
    virtual int DetachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) = 0;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) = 0;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) = 0;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) = 0;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) = 0;


    /* index 主要表示通道，subindex表示主辅码流等 */
    virtual int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size ) = 0;
    virtual int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size ) = 0;

	//!返回true 表示已经登录，false没有登录
	virtual bool	IsLogin() = 0;
	//!返回true 表示此通道正在监视, 从1开始
	//virtual bool	TestOpenMonitorChannel( CNetChannel* net_channel ) = 0;

    virtual void Dump() {return;}
	virtual int SendPtzData(int channel,char *pData,unsigned int uiLen) {return 0;}
protected:
private:
};

typedef TFunction1<INetClient *,INetCliConfig *> NetClientProc;

/// 管理控制函数的列表
typedef std::map<int, NetClientProc> INetClientTable;

extern bool RegisterNetClientObj(int iType,NetClientProc proc);
extern bool UnRegisterNetClientObj(int iType);

#endif

