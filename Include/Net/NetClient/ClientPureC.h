#ifndef _CLIENT_PURE_C_H__
#define _CLIENT_PURE_C_H__

#include "Onvif/soapApp.h"
#include "APIs/Types.h"

#define ACTIVE_DEFAULT 0
#ifdef  __cplusplus
extern "C" {
#endif

#define MAX_IP_STRING_LENTH  16

typedef struct tagSunellAlarmConfigInfo
{
    /* 新的报警IP和端口 */
    char  acTargetRemoteIp[MAX_IP_STRING_LENTH];
    int     iTargetPort;

    /* 订阅成功的IP和端口 */
    char   acRemoteIp[MAX_IP_STRING_LENTH];
    int      iPort;
    char   alarmAddress[128];/* 输出 */
}SUNELL_ALARM_CONFIG_INFO;

extern VD_INT32 c_SendSubscribeRule(SUNELL_ALARM_CONFIG_INFO *pInfo);
extern VD_INT32 c_SendUnsubscribe(SUNELL_ALARM_CONFIG_INFO *pInfo);
extern VD_INT32 c_SendRenew(SUNELL_ALARM_CONFIG_INFO *pInfo);


#ifdef  __cplusplus
}
#endif
#endif
