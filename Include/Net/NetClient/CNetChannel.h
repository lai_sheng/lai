#ifndef CNETCHANNEL_H_HEADER_INCLUDED_B67AC1CD
#define CNETCHANNEL_H_HEADER_INCLUDED_B67AC1CD
#include "INetConListener.h"
#include "INetClient.h"

typedef enum
{
    NCO_Monitor,
    NCO_PlayBack,
    NCO_Alarm,
}NET_CHANNEL_OPERATE_E;

class INetChannel
{
public:
    virtual ~INetChannel() {}
    virtual NET_CLI_PROTO_TYPE_E GetType() = 0;
    virtual NET_CHANNEL_OPERATE_E GetOperateType() = 0;
    virtual INetChannel* clone() = 0;
    virtual bool Compare( INetChannel * pThis ) = 0;
    virtual bool IsValid() = 0;    //!测试配置合法性

    virtual int StartMonitor() = 0;
    virtual int StopMonitor() = 0;
    virtual bool GetMonitorState() = 0;

    virtual NET_CAPTURE_CHANNEL_T GetStream() = 0;
    virtual int SetStream( NET_CAPTURE_CHANNEL_T stream ) = 0;

    virtual int SetChannelNo( int const channel_NO ) = 0;
    virtual int GetChannelNO() = 0;
	
    virtual int SetLocalChannelNo( int const channel_NO ) = 0;
    virtual int GetLocalChannelNo() = 0;		
};

/*
 * TODO:=号重载
 */
class CNetChannel : public INetChannel
{
  public:
    CNetChannel( NET_CLI_PROTO_TYPE_E proto_type, NET_CHANNEL_OPERATE_E operate_type );
    CNetChannel();
    virtual ~CNetChannel();
    CNetChannel( CNetChannel& ps );

    bool operator==( CNetChannel& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType() {return m_proto_type;}
    virtual NET_CHANNEL_OPERATE_E GetOperateType() {return m_operate_type;};
    virtual INetChannel* clone() = 0;
    virtual bool Compare( INetChannel * pThis );
    virtual bool IsValid();

    virtual int StartMonitor();
    virtual int StopMonitor();
    // 1--监视开启， 0--不开启
    virtual bool GetMonitorState();

    NET_CAPTURE_CHANNEL_T GetStream() ;
    int SetStream( NET_CAPTURE_CHANNEL_T stream ) ;

    virtual int SetChannelNo( int const channel_NO );
    virtual int GetChannelNO();
    virtual int SetLocalChannelNo( int const channel_NO );
    virtual int GetLocalChannelNo() ;	
//    virtual int AttachDelegateMonitor( CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
//    virtual int DetachDelegateMonitor( CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
//    virtual TSignal2<DWORD, CPacket* >& GetMonitorHandler();

    CNetChannel& operator=(const CNetChannel& right);

    //!重置channel
    void Reset();
private:
    int m_channel_NO;    //通道号从1开始
    int m_local_channel_NO;    //通道号从1开始    
    bool m_monitor_state;
    NET_CAPTURE_CHANNEL_T m_stream;
    NET_CLI_PROTO_TYPE_E m_proto_type;
    NET_CHANNEL_OPERATE_E m_operate_type;

    //TSignal2<DWORD, CPacket* > m_MonitorHandler;
};

class CNetChnnel_DahuaII : public CNetChannel
{
public:
    CNetChnnel_DahuaII();
    ~CNetChnnel_DahuaII();
    bool operator==( CNetChnnel_DahuaII& ps );
    CNetChnnel_DahuaII& operator=(const CNetChnnel_DahuaII& right);

    CNetChnnel_DahuaII( CNetChnnel_DahuaII& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    //virtual NET_CHANNEL_OPERATE_E GetOperateType();
    virtual INetChannel* clone();
    virtual bool Compare( INetChannel * pThis );
    virtual bool IsValid();

    int SetConnType( NET_CAPTURE_CONN_TYPE const type );
    int GetconnType( NET_CAPTURE_CONN_TYPE& type );
private:
    NET_CAPTURE_CONN_TYPE m_conn_type;
};

class CNetChnDahuaIIAlarm : public CNetChannel
{
public:
    CNetChnDahuaIIAlarm();
    virtual ~CNetChnDahuaIIAlarm();

    //bool operator==( CNetChnDahuaIIAlarm& ps );
    //CNetChnDahuaIIAlarm& operator=(const CNetChnDahuaIIAlarm& right);

    //CNetChnDahuaIIAlarm( CNetChnDahuaIIAlarm& ps );
    //virtual NET_CLI_PROTO_TYPE_E GetType();
    //virtual NET_CHANNEL_OPERATE_E GetOperateType();
    virtual INetChannel* clone();
    //virtual bool Compare( INetChannel * pThis );
    //virtual bool IsValid();

private:
};

class CNetChannel_XTJC : public CNetChannel
{
public:
    CNetChannel_XTJC();
    virtual ~CNetChannel_XTJC();
    CNetChannel_XTJC( CNetChannel_XTJC& ps );
    bool operator==( CNetChannel_XTJC& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual INetChannel* clone();
    virtual bool Compare( INetChannel * pThis );
private:

};

class CNetChnnel_RtspGeneral : public CNetChannel
{
public:
    CNetChnnel_RtspGeneral();
    ~CNetChnnel_RtspGeneral();
    bool operator==( CNetChnnel_RtspGeneral& ps );
    CNetChnnel_RtspGeneral& operator=(const CNetChnnel_RtspGeneral& right);

    CNetChnnel_RtspGeneral( CNetChnnel_RtspGeneral& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual INetChannel* clone();
    virtual bool Compare( INetChannel * pThis );
    virtual bool IsValid();
    int SetConnType( NET_CAPTURE_CONN_TYPE const type );
    int GetconnType( NET_CAPTURE_CONN_TYPE& type );

protected:
private:
    NET_CAPTURE_CONN_TYPE m_conn_type;
};

class CNetChnnel_RtspSunell : public CNetChannel
{
public:
    CNetChnnel_RtspSunell();
    ~CNetChnnel_RtspSunell();
    bool operator==( CNetChnnel_RtspSunell& ps );
    CNetChnnel_RtspSunell& operator=(const CNetChnnel_RtspSunell& right);

    CNetChnnel_RtspSunell( CNetChnnel_RtspSunell& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual INetChannel* clone();
    virtual bool Compare( INetChannel * pThis );
    virtual bool IsValid();
    int SetConnType( NET_CAPTURE_CONN_TYPE const type );
    int GetconnType( NET_CAPTURE_CONN_TYPE& type ); 
protected:
private:
    NET_CAPTURE_CONN_TYPE m_conn_type;
};

class CNetChnnelOnvif : public CNetChannel
{
public:
    CNetChnnelOnvif();
    ~CNetChnnelOnvif();
    bool operator==( CNetChnnelOnvif& ps );
    CNetChnnelOnvif& operator=(const CNetChnnelOnvif& right);

    CNetChnnelOnvif( CNetChnnelOnvif& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual INetChannel* clone();
    virtual bool Compare( INetChannel * pThis );
    virtual bool IsValid();
    int SetConnType( NET_CAPTURE_CONN_TYPE const type );
    int GetconnType( NET_CAPTURE_CONN_TYPE& type );

protected:
private:
    NET_CAPTURE_CONN_TYPE m_conn_type;
};


#endif /* CNETCHANNEL_H_HEADER_INCLUDED_B67AC1CD */
