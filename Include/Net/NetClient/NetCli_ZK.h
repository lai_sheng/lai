#ifndef _NETCLI_ZK_H_
#define _NETCLI_ZK_H_

#include "INetClient.h"
#include "CNetChannel.h"
#include "MultiTask/Thread.h"

#include "NetCli_ZK_Ptz.h"
#include "System/ABuffer.h"

class CNetChnnel_ZK : public CNetChnnel_RtspGeneral
{
public:
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual INetChannel* clone();    
    virtual bool Compare( INetChannel * pThis );
};

class CNetCliCfg_ZK : public INetCliConfig
{
public:
    CNetCliCfg_ZK();
    CNetCliCfg_ZK( CNetCliCfg_ZK& ps );
    virtual bool operator==( CNetCliCfg_ZK& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();    
    virtual bool Compare( INetCliConfig * pThis );

    int SetServer(std::string srv_ip, int port);
    std::string GetSrvIP();
    int GetSrvPort();

    int SetAuth(std::string user, std::string passwd);    
    std::string GetUser();
    std::string GetPasswd();
    int SetLocalCh(int iCh);
    int GetLocalCh();

	/* 云台 */
    int SetPtzControlPort(int iPort);
    int GetPtzControlPort();
    int SetPtzType(int iType);
    int GetPtzType();
    int SetRemotePtzPreset( int iEnable,int iNum);//设置远程预置点使能与预置点号
    int GetRemotePtzPreset( int &iEnable,int &iNum);//获取远程预置点使能与预置点号

protected:
    std::string m_ip;
    int         m_port;
    std::string m_url;
    std::string m_username;
    std::string m_passwd;
    int m_iLocalCh;

    int         m_iPtzPresetEnable;
    int         m_iPtzPresetNum;

	/* 云台 */
    int         m_iPtzPort;
    int         m_iPtzType;
};

#define MAX_ZK_RECV_LEN (256*1024)

class CZKABuffer
{
public:
    CZKABuffer();
    virtual ~CZKABuffer();
    /**
     * Append - 
     * @ pszStr : 
     *        pointer of data.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        >=0 data size
     *
     * append data to buffer
     */
    int Append(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Get - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Get data from buffer
     */
    int Get(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Pop - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Pop data from buffer
     */
    int Pop(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Pour - 
     * @ pszStr : 
     *        pointer of buf.
     * @ stSize : 
     *        data size.
     * Return : 
     *        < 0 : error
     *        0 : Success
     *
     * Pour data from buffer
     */
    int Pour(unsigned char * pszStr, const unsigned int stSize);
    /**
     * Reset - 
     * @ isClear : 
     *        true : free buff
     *        false : clear data only
     *
     * Reset buf
     */
    void Reset(const bool isClear = false);
    /**
     * Size - 
     *
     * Return : 
     *        size of data
     *
     * get size of data
     */
    unsigned int Size();
    /**
     * Buf - 
     * Return : 
     *        pointer of data
     *
     * get the pointer of data
     */
    unsigned const char * Buf() const;
    void MoveToHead();

    /*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
    unsigned char * GetSpareBuf();
    int GetSpareBufLen();

    int Append2(const unsigned int stSize);/* 数据已经写入，修改下长度 */

private:
    /* buffer pointer */
    unsigned char * m_pBuf;
    /* data size */
    unsigned int  m_iDataSize;
    unsigned int  m_iDataOffset;
};


typedef enum {
    ZK_TCP_STATE_INIT  = 0,
    ZK_TCP_STATE_CONNECT = 1,
    ZK_TCP_STATE_WAITING = 2,
    ZK_TCP_STATE_WORKING = 3,
    ZK_TCP_STATE_BUTT = 0xFF
}ZK_TCP_STATE;

typedef struct
{
 //   VD_BOOL BAllocFlag;
     /* 对于主动连接的套接字需要处理连接状态机 */
    VD_INT32 iState;
    VD_INT32 iCh;
    
    /* recv */
    ez_socket_t astSocket;

    /* 阻塞方式? */
 //   bool bNonBlock;
    /* remote add */
    struct sockaddr_in RemoteAddr;
    
    /* 上次访问时间 */
    struct timeval lastActiveTime;        // 最后一次接收或发送的时间，用于短连接超时逻辑处理
    CABuffer m_pSendBuf;
}TCP_DLG_CONTEXT_ZK_PTZ;

typedef struct
{
    int   iType;
    char data[20];
}ZK_PTZ_MSG_STRUCT;

class CNetCliZK: public INetClient
{
public:
	typedef enum {
		ZK_ALARM_RESET,
		ZK_ALARM_IN = 1,
		ZK_ALARM_MOTION
	}ZK_ALARM_TYPE;
	
    typedef enum
    {
        ZK_CONNECT_FAILED = -1,
        ZK_CONNECT_NONE = 0,
        ZK_CONNECT_ONLY = 1,
        ZK_CONNECT_MONITOR =2,     
    }ZK_NET_STATE_E;

    CNetCliZK(INetCliConfig * config);
    virtual ~CNetCliZK();

    virtual INetCliConfig * GetConfig() ;

    virtual int     Login();
    virtual int     Logout();
    virtual int     StartMonitor( INetChannel* net_channel);
    virtual int     StopMonitor( INetChannel* net_channel);
    //!返回实际取的数据长度
    //virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

    virtual int GetParam(int TypeMain, int index, int subIndex, void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex, void* buf,  int size );

    virtual bool    IsLogin();
    virtual int SendPtzData(int channel,char *pData,unsigned int uiLen);
    int    OnData(char * pData , int iLen,unsigned int iPt);

    int OpenServer( const unsigned char * ip, const int port, int time_out = -1 );
    int GatherReadSockSet( fd_set* read_set, int& max_fd );
    int NotifyDisconnect(  );
    int MonitorRecvProc( fd_set* read_set, int time_out );
    int RequestStreamCommand();
    int GetVideoAndAudioAttr(char* buf);
    int ResponseStreamCommand();

    int ProcessVideoData(char * pData , int iLen);
    int ProcessAudioData(char * pData , int iLen);
    int ProcessAlarmData(char * pData , int iLen);
    int ProcessVideoFrameData();
    void KeepLive();

    fd_set m_wFds;
	/* 云台相关 */
	VD_INT32 GetSocketErrorNo(VD_INT32 iSock);
    VD_BOOL DoConnect();
    VD_BOOL DoTCP_STATE_CONNECT();
    VD_BOOL DoTCP_STATE_WAITING();
    VD_BOOL DoTCP_STATE_INIT();
    VD_INT32 CheckSennQueen(void);
    VD_INT32 OnZKPtzData(fd_set* read_set);
    VD_INT32 GatherPtzReadSockSet( fd_set* read_set, int& max_fd );
    VD_INT32 CloseConnection();
    VD_INT32 CheckTimeOut();
	CZKPtz *m_pZKPtz;
	
protected:

    SIG_MONITOR* m_pSigMonitor;
    SIG_DEFAULT* m_pSigDef;
    SIG_CLI_ALARM* m_psigAlarm;

    CZKABuffer m_RecvBuf;

    CNetCliCfg_ZK*   m_config;
    CNetChnnel_ZK*  m_pChannel;
    ZK_NET_STATE_E     m_net_state;
    bool                         m_bDisconnect;

    ez_socket_t     m_sock;
    char                m_acIp[32];
    int                  m_iPort;

    unsigned char        *m_pFrameData;
    int          m_iFrameDataLeftLen;
    int          m_BWaitIFrame;
    unsigned int m_timestamp;
	unsigned int m_alarmtime;
	ZK_ALARM_TYPE m_alarmtype;
	unsigned int m_alarmtime_in;
	ZK_ALARM_TYPE m_alarmtype_in;
    
    CMutex    m_Mutex;
    int m_iVideo_width;
    int m_iVideo_height;
    int m_iRate;
    NET_CAPTURE_CHANNEL_T m_iStreamType;
    int m_iChannel;	
    ulong m_last_recv_time;

	/* 云台相关 */
	CMutex m_socketMutex;
	ZK_PTZ_MSG_STRUCT m_LastPtzMsg;
    typedef std::list< ZK_PTZ_MSG_STRUCT > PTZ_MSG_List;
    PTZ_MSG_List m_PtzMsgList;
    CMutex m_list_Mutex;
	struct timeval m_SelectTimev;

	TCP_DLG_CONTEXT_ZK_PTZ m_stCon;

	static int m_GuardPos[32];  //默认32 通道
	
};

class CNetCliZKManager : public CThread
{

public:
    static CNetCliZKManager * instance();
    CNetCliZKManager();
    
    CNetCliZK *AllocNetCli(INetCliConfig * config);    
    bool FreeNetCli(CNetCliZK* pstNCS);
    
    void Start();

    //! 关闭线程
    void Stop();

    //! 线程的执行体
    void ThreadProc();

    void dump();
protected:
    void KeepLive();
    ulong m_last_keeplive_time;

    //! 增加
    int AddNetCli( const CNetCliZK * pNetCli );
    int RemoveNetCli( CNetCliZK * pNetCli );
private:
    typedef std::list< CNetCliZK* > NCS_List;
    NCS_List m_CliCollect;
    NCS_List m_CliPool;
    
    CMutex m_list_Mutex;
    CMutex m_list_Pool_Mutex;
    bool m_bLock;
    CNetCliZK* m_bRemove;
};

class CNetCliZKPtzManager : public CThread
{
public:
    static CNetCliZKPtzManager * instance();
    CNetCliZKPtzManager();

    int AddNetCli( const CNetCliZK* pNetCli );
    int RemoveNetCli( CNetCliZK* pNetCli );
        
    void Start();

    //! 关闭线程
    void Stop();

    //! 线程的执行体
    void ThreadProc();

    void dump();
protected:
private:
        typedef std::list< CNetCliZK* > NCS_List;
        NCS_List m_CliCollect;
        NCS_List m_CliPool;

        CMutex m_list_Mutex;
        CMutex m_list_Pool_Mutex;
        bool m_bLock;
        CNetCliZK* m_bRemove;
};



#endif
