/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              NetOnvif.h
**Version:	              Version 0.01
**Author:	              
**Created:                
**Modified:               yang_shukui,2011-11-30   14:40
**Modify Reason:         
**Description:            NVR ONVIF接入
*********************************************************************/
#ifndef __NET_ONVIF_H__
#define	__NET_ONVIF_H__

#if 0
#include "MultiTask/Thread.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "CNetChannel.h"
#include "INetClient.h"
#include "INetConListener.h"
#include "Onvif/Rtsp/OnvifRtspCli.h"
#include "rtsp/Dec/CRtpStreamDec.h"

class CNetCliOnvif : public CObject, public INetClient
{
public:
 
    CNetCliOnvif(INetCliConfig * config);
    virtual ~CNetCliOnvif();
    
    virtual INetCliConfig * GetConfig() ;
    
    virtual int Login();
    virtual int Logout();
    virtual int StartMonitor( INetChannel* net_channel);
    virtual int StopMonitor( INetChannel* net_channel);
    virtual int GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

    /* index 主要表示通道，subindex表示主辅码流等 */
    virtual int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size );

    //!返回true 表示已经登录，false没有登录
    virtual bool    IsLogin() ;

	void OnData(int chn,int stream, CPacket *pPacket);
	
private:
    CNetCfgOnvif* m_config;
    CNetChnnelOnvif*     m_pChannel;
    TSignal2<ulong, CPacket* >* m_pSigMonitor;
    SIG_DEFAULT* m_pSigDef;
    bool m_bIsMonitor;
	bool m_bFistIFrame;
    CMutex    m_Mutex;
	COnvifRtspCli *m_OnvifCli;
	CommonLib_VD::CRtpStreamDec *m_RtpSDec;
};
#else
#include "MultiTask/Thread.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "CNetChannel.h"
#include "INetClient.h"
#include "INetConListener.h"
#include "Onvif211/Rtsp/OnvifRtspCli.h"
#include "NetCli_RtspCliVD.h"

/**
 *@brief Rtsp 通用版本的，一个连接只能接一路
*/
class CNetCliOnvif : public NetCliLib::INetCliRtspListener, public INetClient
{
public:
 
    CNetCliOnvif(INetCliConfig * config);
    virtual ~CNetCliOnvif();
    
    virtual INetCliConfig * GetConfig() ;
    
    virtual int     Login();
    virtual int     Logout();
    virtual int     StartMonitor( INetChannel* net_channel);
    virtual int     StopMonitor( INetChannel* net_channel);
    //!返回实际取的数据长度
    virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate(int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

    virtual int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size );

    //virtual int     KeepAlive();
    //!返回true 表示已经登录，false没有登录
    virtual bool    IsLogin() ;
    //!返回true 表示此通道正在监视
    //virtual bool    TestOpenMonitorChannel( const int channel ) ;
    //virtual     TSignal2<DWORD, CPacket* >& GetMonitorHandler(const int channel);

public: //!INetCliRtspListener 接口
    virtual int OnData(int chn, int AVType, int stream, CPacket* pPacket);
    virtual int OnPeerClose(int chn);
private:	
	bool GetStreamUrl(char *url, char *nIp, unsigned short *nPort, char *nPara) ;
	
protected:
    CNetCfgOnvif* m_config;
    CNetChnnelOnvif*            m_pChannel;
    TSignal2<ulong, CPacket* >* m_pSigMonitor;
    SIG_DEFAULT* m_pSigDef;

    int m_engineId;//rtsp连接标识
    bool m_bIsMonitor;
    CMutex    m_Mutex;

    NetCliLib::CRtspCliGeneral* m_pRtspCli;
	COnvifRtspCli *m_OnvifCli;
};
#endif
#endif

