#ifdef NET_CLIENT_SDK

#include <stdio.h>
#include <map>
#include "../../Media/IDevDecoder.h"
#include "INetClient.h"
#include "INetConListener.h"
#include "../../MultiTask/Mutex.h"
#include "../../MultiTask/Timer.h"

#include "dhnetsdk.h"


#ifndef _CLIENTDVR_H_
#define _CLIENTDVR_H_

class CNetConnectIPC;

class CClientDVR : public CObject
{
public:
	CClientDVR( CNetConnectIPC * pNci );
	~CClientDVR();
//<Methods>
public:
	// 登入IPC
	//int Login_Dev(char *szDevIp, unsigned short nDevPort, char *szUser, char *szPassword);
	int Login_Dev(char *szDevIp, unsigned short nDevPort, char *szUser, char *szPassword);

	// 注销
	int Logout_Dev(void);

	/**
	 * @brief 请求通道视频
	 * @param nChannelNum 从0开始
	 * @param chl_type
	 * @return
	 */
	long RealPlay(int nChannelNum, NET_CAPTURE_CHANNEL_T chl_type, NET_CAPTURE_CONN_TYPE conn_type  );

	// 停止实时监视
	int StopRealPlay(int nChannelNum);

	// 订阅报警
	int StartListen();

	// 取消订阅报警
	int StopListen();

	// 请求IPC通道相关配置信息
	int GetIPCChannelConfigInfo();

	// 请求录像
	int RequestRecVideo();

	// 停止录象
	int StopRequestRec();

	//网络回放
	long    PlayBackByFileServerMode(int nChannelID,  NET_RECORDFILE_INFO fileInfo); 
	long    PlayBackByTimeServerMode(int nChannelID, int nRecChanID,LPDHNET_TIME lpStartTime, LPDHNET_TIME lpStopTime); 
	void	ReceivePlayPos(int nChannelID,  unsigned int offsettime, unsigned int offsetbyte); 
	int		ClosePlayBack(int nChannelID); 
	void    OnTimer(PARAM wParam); 


	// 登出&	登入
	int Logout_Login_Dev(int nChannelNum, NET_CAPTURE_CHANNEL_T chl_type );

	void static InitNetSDK();
	void static CleanupSDK();

	void StartTransCom(int nType);
	void StopTransCom(int nType);
	void OnTransComCallBack(void *bBuff, int iLen);
    static std::map<long, CNetConnectIPC *> m_LoginMap;
    static CMutex m_LoginMapMutex;

protected:
public:
	//登陆失败原因
	void ShowLoginErrorReason(int nError);
	bool m_First_recv[16];
	bool	m_ListenAlarm;
	unsigned long		m_LoginID1;		// 用于请求主码流
	NET_DEVICEINFO	m_DeviceInfo;
//</Methods>

//<Attributes>
public:

	TSignal2<DWORD, CPacket* >*m_ptsigMonitor; /*!< 监视的回调函数指针*/

	char			m_szDevIp[32];
	int			m_nDevPort;
	char*		m_szUser;
	char*		m_szPassword;
	unsigned int	m_nChannelCount;	// 设备的总通道数
	long		m_channel_play_handle[16];
	long 		m_channel_RecFile_PlayBack_handle[N_SYS_CH]; 
	int 		m_pbbyfile_time[N_SYS_CH]; 	// 0按文件,1按时间
	BOOL		m_ispbover[N_SYS_CH]; 
	CTimer      m_TimerPBOver;          //关闭回放定时器
	CMutex		m_Mutex; 

protected:
private:
	unsigned long		m_hRecordHandle;	// 主码流
	int			m_nDecodeChannel;	// 解码端口
	//int			m_nVideoChannel;	// IPC 视频通道
	int			m_isRecvStartRecInfo; //是否接收到开始录像信息
    CNetConnectIPC * m_Nci;
	long    m_transcomm_handle[16];

//</Attributes>
};

#endif

#endif

