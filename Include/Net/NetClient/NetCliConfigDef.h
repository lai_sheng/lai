
#ifndef __NET_CLI_CONFIG_DEF_H__
#define __NET_CLI_CONFIG_DEF_H__

#include "System/BaseTypedef.h"

#define NCL_CFG_TYPE_START        100

#define NCL_CFG_TYPE_SYS_INFO  (NCL_CFG_TYPE_START+1)
#define NCL_CFG_TYPE_OSD  (NCL_CFG_TYPE_START+2)
#define NCL_CFG_TYPE_ENCODE_COLOR  (NCL_CFG_TYPE_START + 3)
#define NCL_CFG_TYPE_ENCODE_COVER  (NCL_CFG_TYPE_START + 4)
#define NCL_CFG_TYPE_ENCODE_STEAM  (NCL_CFG_TYPE_START + 5)
#define NCL_CFG_TYPE_ENCODE_AUDIO_STEAM (NCL_CFG_TYPE_START+6)

#define NCL_CFG_TYPE_ALARM_MGR_REPORT_TYPE  (NCL_CFG_TYPE_START+7)
#define NCL_CFG_TYPE_ALARM_MGR_SERVER  (NCL_CFG_TYPE_START+8)
#define NCL_CFG_TYPE_MOTION_DETECT_ENABLE  (NCL_CFG_TYPE_START+9)   //int
#define NCL_CFG_TYPE_MOTION_DETECT_SENSITIVITY  (NCL_CFG_TYPE_START+10) //int
#define NCL_CFG_TYPE_MOTION_DETECT_WORK_TIME  (NCL_CFG_TYPE_START+11)
#define NCL_CFG_TYPE_MOTION_DETECT_AREAR  (NCL_CFG_TYPE_START+12)

typedef enum NCL_AUDIO_ENCODE_TYPE_E_
{
    NCL_AUDIO_ENCODE_TYPE_G711A,
    NCL_AUDIO_ENCODE_TYPE_G711U,        
    NCL_AUDIO_ENCODE_TYPE_G722,
    NCL_AUDIO_ENCODE_TYPE_G723,        
    NCL_AUDIO_ENCODE_TYPE_G726,        
    NCL_AUDIO_ENCODE_TYPE_BUTT
} NCL_AUDIO_ENCODE_TYPE_E;


/* 单声道，双声道 */
typedef enum NCL_AUDIO_CH_E_
{
    NCL_SOUND_CH_MOMO   =0,/*mono*/
    NCL_SOUND_CH_STEREO =1,/*stereo*/
    NCL_SOUND_CH_BUTT    
} NCL_SOUND_CH_E;

/* 线性输入还是MIC输入 */
typedef enum NCL_SOURCE_TYPE_E_
{
    NCL_SOURCE_TYPE_LINE   =0,/*mono*/
    NCL_SOURCE_TYPE_MIC =1,/*stereo*/
    NCL_SOURCE_TYPE_BUTT    
} NCL_SOURCE_TYPE_E;

//#define NCL_CFG_TYPE_ENCODE_COLOR  (NCL_CFG_TYPE_START+1)
typedef struct NCL_TYPE_SYS_VERSION_
{
    char acVersion[80];
}NCL_TYPE_SYS_VERSION;


//#define NCL_CFG_TYPE_OSD  (NCL_CFG_TYPE_START+2)
typedef struct NCL_TYPE_OSD_
{
    char acName[64];
    int    iStartX;
    int    iStartY;
}NCL_TYPE_OSD;

//#define NCL_CFG_TYPE_ENCODE_COLOR  (NCL_CFG_TYPE_START+3)
//! 视频颜色结构
typedef struct NCL_TYPE_COLOR_
{
    int    iBrightness;        /*!< 亮度    0-100 */
    int    iContrast;            /*!< 对比度    0-100 */
    int    iSaturation;        /*!< 饱和度    0-100 */
    int    iHue;                /*!< 色度    0-100 */
    int    iSharpness;                /*!< 锐度    0-100 */
    int    iResv[8];
}NCL_TYPE_COLOR; 

//#define NCL_CFG_TYPE_ENCODE_MAIN_STEAM  (NCL_CFG_TYPE_START+5)

//!媒体格式
typedef struct  NCL_TYPE_STREAM_
{
    int        iCompressionType;            /*!< 压缩模式 */
    int        iCompression;            /*!< 压缩率*/
    int        iResolution;            /*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */    
    int        iBitRateControl;        /*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */    
    int        iQuality;                /*!< 码流的画质 档次1-6    */    
    int        nFPS;                    /*!< 帧率值，NTSC/PAL不区分,负数表示多秒一帧*/        
    int        nBitRate;                /*!< 0-4096k,该列表主要由客户端保存，设备只接收实际的码流值而不是下标。*/
    int        iGOP;                    /*!< 描述两个I帧之间的间隔时间，1-100 */
    int         iVideoEnable;            /*!< 开启视频编码*/
    int         iAudioEnable;            /*!< 开启音频编码*/
    int          iResv[8];
    char       acName[128];
    int           iUseCount;
} NCL_TYPE_STREAM;


typedef struct  NCL_TYPE_STREAM_AUDIO_
{
    int        iEncodetype;             /*!< 参考枚举NCL_AUDIO_ENCODE_TYPE_E */
    int        iSoundCh;             /* NCL_SOUND_CH_E*/
    int        iSourceType;             /* NCL_SOURCE_TYPE_E */
    int        iResv[8];
} NCL_TYPE_STREAM_AUDIO;

//#define NCL_CFG_TYPE_ALARM_MGR_REPORT_TYPE  (NCL_CFG_TYPE_START+7)
typedef struct  NCL_TYPE_ALARM_MGR_REPORT_TYPE_
{
    int        iReportType;//0- 周期查询； 1 – 主动上报
    int        iResv[3];
} NCL_TYPE_ALARM_MGR_REPORT_TYPE;


//#define NCL_CFG_TYPE_ALARM_MGR_SERVER  (NCL_CFG_TYPE_START+8)
typedef struct  NCL_TYPE_ALARM_MGR_SERVER_
{
    char acIp[32];
    int    iPort;
} NCL_TYPE_ALARM_MGR_SERVER;

//#define NCL_CFG_TYPE_MOTION_DETECT_WORK_TIME  (NCL_CFG_TYPE_START+10)

#define NCL_NET_N_WEEKS 7
#define NCL_NET_N_UI_TSECT 6

typedef struct NCL_TYPE_NETSECTION_
{
    int     enable;             //!使能
    uchar     startHour;             //!开始时间:小时
    uchar    startMinute;        //!开始时间:分钟
    uchar    startSecond;         //!开始时间:秒钟
    uchar    endHour;             //!结束时间:小时
    uchar    endMinute;         //!结束时间:分钟
    uchar    endSecond;         //!结束时间:秒钟
    uchar   ucSupport;    //是否支持
    uchar   ucReserved;    //保留
}NCL_TYPE_NETSECTION;

typedef struct NCL_TYPE_NET_WORKSHEET_
{
    int            iChannel;
    int            iName;
    NCL_TYPE_NETSECTION    tsSchedule[NCL_NET_N_WEEKS][NCL_NET_N_UI_TSECT];    /*!< 时间段 */
} NCL_TYPE_NET_WORKSHEET;

//#define NCL_CFG_TYPE_MOTION_DETECT_AREAR  (NCL_CFG_TYPE_START+11)
typedef struct NCL_TYPE_NET_MD_AREA_
{
    int iEnable;
    int iX1;
    int iY1;
    int iX2;
    int iY2;
}NCL_TYPE_NET_MD_AREA;

typedef struct NCL_TYPE_NET_MD_AREAS_
{
        NCL_TYPE_NET_MD_AREA area[8];
}NCL_TYPE_NET_MD_AREAS;

/************************visiondigi protocol config start************************/

typedef struct  tagWQ_DEV_CHANNEL
{
    unsigned short usTotal;  //总通道数目
    unsigned short usIndex;  //第几个通道信息
}WQ_DEV_CHANNEL;

#define N_COLOR_SECTION 2

//! 视频颜色结构
typedef struct tagWQVIDEOCOLOR_PARAM
{
	int	nBrightness;		/*!< 亮度	0-100 */
	int	nContrast;			/*!< 对比度	0-100 */
	int	nSaturation;		/*!< 饱和度	0-100 */
	int	nHue;				/*!< 色度	0-100 */
	int	mGain;				/*!< 增益	0-100 第７位置1表示自动增益　*/		
	int	mWhitebalance;		/*此值现在用于光圈基准值*/
}WQ_VIDEOCOLOR_PARAM; 

//!视频颜色设置
typedef struct tagWQ_VIDEOCOLOR
{
	TIMESECTION			TimeSection;		/*!< 时间段 */
	WQ_VIDEOCOLOR_PARAM	dstColor;			/*!< 颜色定义 */
	int					iEnable;
}WQ_VIDEOCOLOR;

typedef struct tagWQ_CONFIG_VIDEOCOLOR
{
	WQ_VIDEOCOLOR dstVideoColor[2];
}WQ_CONFIG_VIDEOCOLOR;

typedef struct tagWQ_NET_VIDEOCOLOR
{
	int iChannel;
	WQ_CONFIG_VIDEOCOLOR stVideoColor;
}WQ_NET_VIDEOCOLOR;

typedef struct  tagWQ_VIDEO_FORMAT
{
	int		iCompression;			/*!< 压缩模式 */	
	int		iResolution;			/*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */	
	int		iBitRateControl;		/*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */	
	int		iQuality;				/*!< 码流的画质 档次1-6	*/	
	int		nFPS;					/*!< 帧率值，NTSC/PAL不区分,负数表示多秒一帧*/		
	int		nBitRate;				/*!< 0-4096k,该列表主要由客户端保存，设备只接收实际的码流值而不是下标。*/
	int		iGOP;					/*!< 描述两个I帧之间的间隔时间，2-12 */
} WQ_VIDEO_FORMAT;

typedef struct  tagWQ_AUDIO_FORMAT
{
	int		nBitRate;				/*!< 码流kbps*/	
	int		nFrequency;				/*!< 采样频率*/	
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30
	unsigned char ucLAudioVolumn;   //左声道音量
	unsigned char ucRAudioVolumn;   //右声道音量，单声道的设备左右值一样
	unsigned char ucRes[2];
#else
	int		nMaxVolume;				/*!< 最大音量阈值*/
#endif
} WQ_AUDIO_FORMAT;

//!媒体格式
typedef struct  tagWQ_MEDIA_FORMAT
{
	WQ_VIDEO_FORMAT vfFormat;			/*!< 视频格式定义 */			
	WQ_AUDIO_FORMAT afFormat;			/*!< 音频格式定义 */
	VD_BOOL	bVideoEnable;			/*!< 开启视频编码 */
	VD_BOOL	bAudioEnable;			/*!< 开启音频编码 */	
}WQ_MEDIA_FORMAT;

//!编码设置
typedef struct tagWQ_NET_ENCODE
{
	int  iChannel;
	int iSteamIndex;   
	/*!<码流格式 
	主码流 参考枚举参考ENCODE_TYPE_BY_RECORD ，现有产品默认只有一个主码流，填充为0；
	副码流 参考 ENCODE_TYPE_BY_SUBSTREAM，最多4种子码流，现在产品最多支持一种副码流，填充为0；
	捉图码流 参考 ENCODE_TYPE_BY_SUBSTREAM，最多从4种码流中进行捉图配置
	*/	
	WQ_MEDIA_FORMAT dstFmt;		/*!<码流格式 */	

}WQ_NET_ENCODE;

//主码流和辅码流配置信息
typedef struct tat_WQ_NET_MAIN_SUB_INFO
{
	WQ_NET_ENCODE mainstream;
	WQ_NET_ENCODE substream;
}WQ_NET_MAIN_SUB_INFO;

typedef struct _WQChannelName_
{
    int iChannel;
    char strChannelName[64]; 
}WQ_CHANNEL_NAME;

//!视频物件结构
typedef struct  tagWQ_VIDEO_WIDGET
{
	
	VD_COLORREF rgbaFrontground;		/*!< 物件的前景RGB，和透明度 */	
	VD_COLORREF rgbaBackground;		/*!< 物件的后景RGB，和透明度*/	
	VD_RECT	rcRelativePos;			/*!< 物件边距与整长的比例*8191 */	
	VD_BOOL	bPreviewBlend;			/*!< 预览叠加 */	
	VD_BOOL	bEncodeBlend;			/*!< 编码叠加 */
} WQ_VIDEO_WIDGET;

typedef struct  tagWQ_NET_VIDEO_WIDGET
{
	unsigned short  usChannel;
	unsigned short	 usRes;	
	WQ_VIDEO_WIDGET	dstWidget;
} WQ_NET_VIDEO_WIDGET;

typedef struct  tagWQ_NET_VIDEO_COVER
{
	unsigned short usChannel;
	unsigned short	 usCoverNum;	/*!< 当前该通道有几个叠加的区域 */
	WQ_VIDEO_WIDGET	dstCovers[8];
} WQ_NET_VIDEO_COVER;

//! 普通配置
typedef struct tagWQ_NET_GENERAL
{ 
    /*!< 按位掩码形式
    0 硬盘满时处理 1覆盖"OverWrite", 0停止"StopRecord" 
    1 是否静音
    2 LCD屏幕自动关闭
    3 是否TV输出
    4 是否VGA输出
    5 是否HDMI输出
    */
    int  iCtrlMask;
     
    /*!<?本机编号:[0,?998]?*/ 
    int iLocalNo;
     
    //!屏保时间(分钟) [0, 120]
    unsigned short usScreenSaveTime;
     
    //!本地菜单自动注销(分钟) [0, 120]
    unsigned short usAutoLogout;
     
    uchar              ucDateFormat;//日期格式/*!< 日期格式:“YYMMDD”, “MMDDYY”, “DDMMYY” */

    uchar              ucDateSprtr;//日期分隔符/*!< 日期分割符:“.”, “-”, “/” */

    uchar               ucTimeFmt;//时间格式/*!< 时间格式:1:"12";    0: "24" */

    uchar ucRes;
    
    /*设备可以支持的制式 ， 第0位 PAL ，第1位 NTSC， 第2位 SECAM*/
    uchar ucSupportVideoFmt;
    
    /*设备使用的制式 ，0 PAL ，1 NTSC， 2 SECAM*/
    uchar ucCurVideoFmt;

    //uchar ucRes1[2];将此保留字节修改，此部分只有在
    uchar ucMenuLayer;//菜单输出 ，0 HDMI，1 VGA，2 TV，说明此操作仅在98系列上支持，对于97设备默认不处理
    uchar ucResolution;//输出分辨率，0:1920*1080 , 1:1280*1024 , 2:1024*768 , 3:800*600
 
    /*设备支持的语言 ， 

    第0位English ，第1位 SimpChinese， 第2位 TradChinese， 第3位 “Italian”,
    4 “Spanish”, 5“Japanese”, 6“Russian”, 7“French”, 8“German”
     ，9 "PORTUGUê"*/

    uint uiSupportLanguage;

     /*设备当前使用的语言 ，

    0 English，1 SimpChinese，2 TradChinese，3“Italian”,4 “Spanish”, 

    5“Japanese”, 6“Russian”, 7“French”, 8“German”,9"PORTUGUê"
    */
    uint uiCurLanguage;

    uint uiRes[3];//保留字节

} WQ_NET_GENERAL;

//音频配置
typedef struct tagWQ_CONFIG_AUDIO_FORMAT_NET
{
  uint uiTalkAudioSourceType;    //!< 声音源方式，0为线性输入，1为mic
  uint uiOutSilence;        ///< 静音 1--静音 0--非静音

  uint uiOutLAudioVolumn;   //左声道音量
  uint uiOutRAudioVolumn;   //右声道音量，单声道的设备左右值一样
  uint uiLongtimeBeepEnable;    ///蜂鸣器长鸣

  uint uiReserverd[31];//保留

}WQ_CONFIG_AUDIO_FORMAT_NET;

//编码能力集
typedef struct  _WQ_ENCODE_CAPS_NET
 {
    uint uiMaxEncodePower;  
    //产品支持的最高编码能力

    ushort usSupportChannel;  
    //- 每块 DSP 支持最多输入视频通道数 
    ushort usChannelSetSync; 
    //- DSP 每通道的最大编码设置是否同步 0-不同步, 1-同步

    //每一通道支持码流情况
    uchar ucVideoStandardMask;
    //视频制式掩码，按位表示设备能够支持的视频制式 0 PAL 1 NTSC
    uchar ucEncodeModeMask; 
    //编码模式掩码，按位表示设备能够支持的编码模式设置 ，0 VBR，1 CBR

    ushort usStreamCap;  
    //按位表示设备支持的多媒体功能，
    //第一位表示支持主码流
    //第二位表示支持辅码流1
    //第三位表示支持jpg抓图，捉图功能另外定义，在此不加入

    uint uiImageSizeMask;   

    //主码流编码掩码 枚举按照capture_size_t 

    uint uiImageSizeMask_Assi[32];     

    //主码流取不同的编码时候，辅码流支持的编码格式，例如, 

    //uiImageSizeMask_Assi【0】表示主码流是CAPTURE_SIZE_D1时，辅码流支持的编码格式，

    //uiImageSizeMask_Assi【1】表示主码流是CAPTURE_SIZE_HD1时，辅码流支持的编码格式。。。

    uchar ucSupportPolicy;  
    //是否支持特殊编码策略 0 不支持 1 支持

    uchar ucCompression;//按位表示，第0位表示是否支持264，第1位表示是否支持svac

    uchar ucRes[10];   //保留

 } WQ_ENCODE_CAPS_NET;

//设备属性信息
typedef struct _wq_sysattr_ex_t
{
     unsigned char iVideoInCaps;     // 视频输入接口数量
     unsigned char iVideoOutCaps;     //视频输出接口数量
     unsigned char iAudioInCaps;     // 音频输入接口数量
     unsigned char iAudioOutCaps;     // 音频输出接口数量

     unsigned char iAlarmInCaps;     // 报警输入接口数
     unsigned char iAlarmOutCaps;    // 报警输出接口数
     unsigned char iDiskNum;          // 实际使用硬盘数 sdk的byDiskNum
     unsigned char iAetherNetPortNum;// 网络接口数sdk的iIsMutiEthIf

     unsigned char iUsbPortNum;          // USB接口数
     unsigned char iDecodeChanNum;     // 本地解码(回放)路数
     unsigned char iComPortNum;      // 串口数
     unsigned char iParallelPortNum;   // 并口口数

     unsigned char iSpeechInCaps;     // 对讲输入接口数量
     unsigned char iSpeechOutCaps;     // 对讲输出接口数量
     //unsigned char bRes[2];   // 保留字
     unsigned char ucSupportEx;   // 其他支持项目，按位操作
                                       //第0位 1支持菜单输出控制  0 不支持菜单输出控制（98支持，97以及其他产品不支持）
                                       //第1位，1支持分辨率输出切换 0 不支持分辨率输出切换（97，98，解码器支持，其他产品不支持）
     unsigned char ucReslution;   //按位支持分辨率的能力，0 1920*1080 1 1280*1024 2 1280x720 3 1024*768   4 800*600   与 ucSupportEx 组合使用 
}WQ_SYSATTR_EX_T;

//add能力集信息结构体
typedef struct tag_WQ_NET_ABILITY_INFO
{
	int iLogicChnNum;	//逻辑通道数 == 模拟通道数 + 数字通道数
	int iAnalogChnNum;	//模拟通道数
	WQ_SYSATTR_EX_T 	sysattr;	//设备属性信息
	WQ_ENCODE_CAPS_NET 	encode;		//编码能力集
}WQ_NET_ABILITY_INFO;

//布防撤防
typedef struct  tagWQ_ALARM_GUARD
{
    int iChannel;

    int iEnable;  //告警布防撤防 0撤防，1布防

    //通用配置信息，具体定义如下
    //告警输入标识传感器类型常开 or 常闭；0常开，1常闭
    //视频丢失和动检为灵敏度；0~6档次，6档次为最灵敏
    //磁盘为硬盘剩余容量下限, 百分数
    int iGeneral;
}WQ_ALARM_GUARD;

//! 云台联动结构
typedef struct tagWQ_PTZ_LINK
{
	int iType;				/*!< 联动的类型 */
	int iValue;				/*!< 联动的类型对应的值 */
}WQ_PTZ_LINK;

//! 事件处理结构
typedef struct tagWQ_EVENT_HANDLER_NET
{
    uint    dwRecord;                /*!< 录象掩码 */
    int        iRecordLatch;            /*!< 录像延时：10～300 sec */
    uint    dwTour;                    /*!< 轮巡掩码 */
    uint    dwSnapShot;                /*!< 抓图掩码 */
    uint    dwAlarmOut;                /*!< 报警输出通道掩码 */
    int        iAOLatch;                /*!< 报警输出延时：10～300 sec */
    WQ_PTZ_LINK PtzLink[16];        /*!< 云台联动项 */

    VD_BOOL    bRecordEn;                /*!< 录像使能 */
    VD_BOOL    bTourEn;                /*!< 轮巡使能 */
    VD_BOOL    bSnapEn;                /*!< 抓图使能 */
    VD_BOOL    bAlarmOutEn;            /*!< 报警使能 */
    VD_BOOL    bPtzEn;                    /*!< 云台联动使能 */
    VD_BOOL    bTip;                    /*!< 屏幕提示使能 */
    VD_BOOL    bMail;                    /*!< 发送邮件 */
    VD_BOOL    bMessage;                /*!< 发送消息到报警中心 */
    VD_BOOL    bBeep;                    /*!< 蜂鸣 */
    VD_BOOL    bVoice;                    /*!< 语音提示 */
    VD_BOOL    bFTP;                    /*!< 启动FTP传输 */

    int        iWsName;                /*!< 时间表的选择，由于时间表里使用数字做索引，且不会更改 */
    uint    dwMatrix;                /*!< 矩阵掩码 */
    VD_BOOL    bMatrixEn;                /*!< 矩阵使能 */
    VD_BOOL    bLog;                    /*!< 日志使能，目前只有在WTN动态检测中使用 */
    int        iEventLatch;            /*!< 联动开始延时时间，s为单位 */
    VD_BOOL    bMessagetoNet;            /*!< 消息上传给网络使能 */
    uint    dwReserved[7];             /*!< 保留字节 */
} WQ_EVENT_HANDLER_NET;

//联动事件
typedef struct tagWQ_NET_EVENT_HANDLER
{
    int iChannel;
    WQ_EVENT_HANDLER_NET stEventHandler;
} WQ_NET_EVENT_HANDLER;

typedef struct tagWQ_NETSECTION
{
    int     enable;             //!使能
    uchar     startHour;             //!开始时间:小时
    uchar    startMinute;        //!开始时间:分钟
    uchar    startSecond;         //!开始时间:秒钟
    uchar    endHour;             //!结束时间:小时
    uchar    endMinute;         //!结束时间:分钟
    uchar    endSecond;         //!结束时间:秒钟
    uchar   ucReserved[2];    //保留
}WQ_NETSECTION;

//联动时间段
typedef struct tagWQ_NET_WORKSHEET
{
    int  iChannel;
    int            iName;
    WQ_NETSECTION    tsSchedule[7][6];    /*!< 时间段 */
} WQ_NET_WORKSHEET;

//动检区域
typedef struct
{    
    int  iChannel;
    unsigned int uiRowColNum;//高16为表示行数，低16位表示列数
    int        iRegion[32];    //每行使用二进制串
}WQ_NET_REGION;

//add动态检测配置信息
typedef struct tag_WQ_NET_MOTION_INFO
{
	WQ_ALARM_GUARD guard;		//布防撤防
	WQ_NET_EVENT_HANDLER event;	//联动事件
	WQ_NET_WORKSHEET worksheet;	//联动时间段
	WQ_NET_REGION region;		//动检区域
}WQ_NET_MOTION_INFO;

//add视频遮挡配置信息
typedef struct tag_WQ_NET_BLIND_INFO
{
	WQ_ALARM_GUARD guard;		//布防撤防
	WQ_NET_EVENT_HANDLER event;	//联动事件
	WQ_NET_WORKSHEET worksheet;	//联动时间段
}WQ_NET_BLIND_INFO;

//add视频丢失配置信息
typedef struct tag_WQ_NET_LOSS_INFO
{
	WQ_ALARM_GUARD guard;		//布防撤防
	WQ_NET_EVENT_HANDLER event;	//联动事件
	WQ_NET_WORKSHEET worksheet;	//联动时间段
}WQ_NET_LOSS_INFO;


typedef struct tag_WQ_NET_ALARM_INFO
{
	WQ_ALARM_GUARD guard;		//布防撤防
	WQ_NET_EVENT_HANDLER event;	//联动事件
	WQ_NET_WORKSHEET worksheet;	//联动时间段
}WQ_NET_ALARM_INFO;

typedef struct tag_PTZ_ATTR
 {
	uint	baudrate;		
	uchar	databits;
	uchar	parity;
	uchar	stopbits;	
	uchar	reserved;
} WQ_PTZ_ATTR;


typedef struct {
    uchar    Ptz_Version[8];        /*!< 版本号 */
    WQ_PTZ_ATTR PTZAttr;            /*!< 串口属性　*/
    ushort    DestAddr;            /*!< 目的地址 0-255 */
    ushort      Protocol;            /*!< 协议类型 保存协议的下标，动态变化 */
    ushort    MonitorAddr;        /*!< 监视器地址 0-64 */
    uchar    Reserved[10];        /*!< 保留 */
} WQ_OLD_CONFIG_PTZ;

typedef struct _User_Auth_List
{
	unsigned int uiMonitorMask;
	unsigned int uiPlayMask;
	unsigned int uiNetPreviewMask;
	unsigned int uiControlConfigMask;
	unsigned int uiReserved[4];
} WQ_USER_AUTH_LIST;

/************************visiondigi protocol config end************************/

#endif


