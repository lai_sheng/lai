#ifndef __CONNECTDSS_H__
#define __CONNECTDSS_H__

#include "ez_libs/ez_socket/ez_socket.h"
//#include "INetClient.h"
#include "NetCliDss.h"
#include "NetCliDssChannel.h"
#include "CNetChannel.h"
class CDssChannel;
class CNetCliDss;
class CNetChannel;
class CConnectDss:public CObject
{
 typedef enum
    {
        CONNECT_NONE    = 0,
        CONNECT_ONLY        = 1,
        CONNECT_LOGIN           = 2,
        CONNECT_MONITOR     =3      //!已经打开监视一些通道了
    }NET_STATE_E;
	public:

		CConnectDss( );
		~CConnectDss();
	
		
	// 打开IPC
	//virtual int OpenServer( const unsigned char * ip, const int port, int time_out = -1 );

	// 关闭IPC
	//virtual int Close();

	// 登陆IPC
	virtual int Login();

	// 退出IPC
	virtual int Logout();

	// 开始实时监视, channel从1开始
	virtual int StartMonitor(INetChannel* net_channel);

	// 关闭实时监视
	virtual int StopMonitor(INetChannel* net_channel);

	//brief 委托方式处理监视数据, 不需要再维护保洁状态, 委托者
	virtual int AttachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );
	virtual int DetachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );

	//virtual int GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegateAlarm( CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    //virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    //virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
	//virtual int AttachPlayBackProcDelegate( int channel, CObject * pObj, SIG_CLI_PLAYBACK_PROC_DELEGATE pProc ) ; 
	//virtual int DetachPlayBackProcDelegate(int channel, CObject * pObj, SIG_CLI_PLAYBACK_PROC_DELEGATE pProc) ; 
	//virtual int KeepAlive();

	// !返回true 表示已经登录，false没有登录
	virtual bool IsLogin();
	// !返回true 表示此通道正在监视
	//virtual bool TestOpenMonitorChannel( const int channel ) ;

	//virtual int  PlayBackByTimeServerMode(int nChannelID, SYSTEM_TIME* lpStartTime, SYSTEM_TIME* lpStopTime); 
	
//</Methods>
	static CConnectDss* instance( void );

	private:
		NET_STATE_E				m_net_state;
		int m_channel_play_handle[N_SYS_CH];
		long			m_loginHandle;
		bool     m_channel_attach[N_SYS_CH];
		
		CDssChannel *m_channel_dss[N_SYS_CH];
		static CConnectDss* _instance ;
		
};
#endif
