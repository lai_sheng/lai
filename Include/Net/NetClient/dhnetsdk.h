
#ifndef DHNETSDK_H
#define DHNETSDK_H

#ifdef WIN32

#ifdef DHNETSDK_EXPORTS
#define CLIENT_API  __declspec(dllexport) 
#else	//#ifdef DHNETSDK_EXPORTS
#define CLIENT_API  __declspec(dllimport)   //VC 用
#endif

#define CALLBACK __stdcall
#define CALL_METHOD  __stdcall  //__cdecl

#else	//linux

#define CLIENT_API	extern "C"
#define CALL_METHOD
#define CALLBACK

#define RELEASE_HEADER	//发布头文件
#ifdef RELEASE_HEADER

#if 0
#define WORD	unsigned short
#define DWORD	unsigned long
#define LPDWORD	DWORD*
#define BOOL	int
#define TRUE	1
#define FALSE	0
#define BYTE	unsigned char
#define LONG	long
#define UINT	unsigned int
#define HDC		void*
#define HWND	void*
#define LPVOID	void*
#endif

#include "../../Include/DVRDEF.H"

#define HWND	void*
#ifndef NULL 
#define NULL	0
#endif
#if 0
typedef struct  tagRECT
{
    LONG left;
    LONG top;
    LONG right;
    LONG bottom;
}RECT;
#endif

#else	//内部编译
#include "../netsdk/osIndependent.h"
#endif

#endif


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
 ** 常量定义
 ***********************************************************************/

//错误类型代号，CLIENT_GetLastError函数的返回
#define _EC(x)						(0x80000000|x)
#define NET_NOERROR 				0				//没有错误
#define NET_ERROR					-1				//未知错误
#define NET_SYSTEM_ERROR			_EC(1)			//Windows系统出错
#define NET_NETWORK_ERROR			_EC(2)			//网络错误，可能是因为网络超时
#define NET_DEV_VER_NOMATCH			_EC(3)			//设备协议不匹配
#define NET_INVALID_HANDLE			_EC(4)			//句柄无效

#define NET_LOGIN_ERROR_PASSWORD	_EC(100)		//密码不正确
#define NET_LOGIN_ERROR_USER		_EC(101)		//帐户不存在
#define NET_LOGIN_ERROR_TIMEOUT		_EC(102)		//等待登录返回超时
#define NET_LOGIN_ERROR_RELOGGIN	_EC(103)		//帐号已登录
#define NET_LOGIN_ERROR_LOCKED		_EC(104)		//帐号已被锁定
#define NET_LOGIN_ERROR_BLACKLIST	_EC(105)		//帐号已被列为黑名单
#define NET_LOGIN_ERROR_BUSY		_EC(106)		//资源不足，系统忙
#define NET_LOGIN_ERROR_CONNECT		_EC(107)		//连接主机失败"
#define NET_LOGIN_ERROR_NETWORK		_EC(108)		//网络连接失败"

#define DH_SERIALNO_LEN 		48		//设备序列号字符长度

/************************************************************************
 ** 枚举定义
 ***********************************************************************/
//设备类型
enum NET_DEVICE_TYPE 
{
	NET_PRODUCT_NONE = 0,
	NET_DVR_NONREALTIME_MACE, /*!< 非实时MACE */
	NET_DVR_NONREALTIME, /*!< 非实时 */
	NET_NVS_MPEG1, /*!< 网络视频服务器 */
	NET_DVR_MPEG1_2, /*!< MPEG1二路录像机 */
	NET_DVR_MPEG1_8, /*!< MPEG1八路录像机 */
	NET_DVR_MPEG4_8, /*!< MPEG4八路录像机 */
	NET_DVR_MPEG4_16, /*!< MPEG4十六路录像机 */
	NET_DVR_MPEG4_SX2, /*!< LB系列录像机 */
	NET_DVR_MEPG4_ST2, /*!< GB系列录像机 */
	NET_DVR_MEPG4_SH2, /*!< HB系列录像机 */
	NET_DVR_MPEG4_GBE, /*!< GBE系列录像机 */
	NET_DVR_MPEG4_NVSII, /*!< II代网络视频服务器 */
	NET_DVR_STD_NEW, /*!< 新标准配置协议 */
	NET_DVR_DDNS, /*DDNS服务器*/
	NET_DVR_ATM,  /* ATM机 */
	NET_NB_SERIAL, /* 二代非实时NB系列机器 */
	NET_LN_SERIAL, /* LN系列产品 */
	NET_BAV_SERIAL, /* BAV系列产品 */
	NET_SDIP_SERIAL, /* SDIP系列产品 */
	NET_IPC_SERIAL, /* IPC系列产品 */
	NET_NVS_B,		/* NVS B系列 */
	NET_NVS_C,		/* NVS H系列 */
	NET_NVS_S,		/* NVS S系列 */
	NET_NVS_E,		/* NVS E系列 */
	NET_DVR_NEW_PROTOCOL /*从QueryDevState中查询设备类型,以字符串格式*/
};


//实时预览扩展接口增加的参数：预览类型
typedef enum _RealPlayType
{
	DH_RType_Realplay = 0,	//实时预览
	DH_RType_Multiplay,	//多画面预览
		
	DH_RType_Realplay_0,	//实时监视-主码流，等同于DH_RType_Realplay
	DH_RType_Realplay_1,	//实时监视-从码流1
	DH_RType_Realplay_2,	//实时监视-从码流2
	DH_RType_Realplay_3,	//实时监视-从码流3
		
	DH_RType_Multiplay_1,	//多画面预览－1画面
	DH_RType_Multiplay_4,	//多画面预览－4画面
	DH_RType_Multiplay_8,	//多画面预览－8画面
	DH_RType_Multiplay_9,	//多画面预览－9画面
	DH_RType_Multiplay_16,	//多画面预览－16画面

	DH_RType_Multiplay_6,	//多画面预览－6画面
	DH_RType_Multiplay_12,	//多画面预览－12画面
} DH_RealPlayType;


/************************************************************************
 ** 结构体定义
 ***********************************************************************/
//时间
typedef struct 
{
	DWORD dwYear;		//年
	DWORD dwMonth;		//月
	DWORD dwDay;		//日
	DWORD dwHour;		//时
	DWORD dwMinute;		//分
	DWORD dwSecond;		//秒
} DHNET_TIME,*LPDHNET_TIME;

//设备信息
typedef struct {
	BYTE sSerialNumber[DH_SERIALNO_LEN];	//序列号
	BYTE byAlarmInPortNum;				//DVR报警输入个数
	BYTE byAlarmOutPortNum;				//DVR报警输出个数
	BYTE byDiskNum;						//DVR 硬盘个数
	BYTE byDVRType;						//DVR类型, 见枚举DHDEV_DEVICE_TYPE
	BYTE byChanNum;						//DVR 通道个数
} NET_DEVICEINFO, *LPNET_DEVICEINFO;

//录像文件信息
typedef struct {
    unsigned int     ch;              //通道号
    char             filename[128];   //文件名
    unsigned int     size;            //文件长度
    DHNET_TIME       starttime;       //开始时间
    DHNET_TIME       endtime;         //结束时间
    unsigned int     driveno;         //磁盘号
    unsigned int     startcluster;    //起始簇号
	int				 nRecordFileType; //录象文件类型  0：普通录象；1：报警录象；2：移动检测；3：卡号录象
} NET_RECORDFILE_INFO, *LPNET_RECORDFILE_INFO;


/************************************************************************
 ** 服务器断开回调原形
 ***********************************************************************/
typedef void (CALLBACK *fDisConnect)(LONG lLoginID, char *pchDVRIP, LONG nDVRPort, DWORD dwUser);

//	断线重连成功回调函数
typedef void (CALLBACK *fHaveReConnect)(LONG lLoginID, char *pchDVRIP, LONG nDVRPort, DWORD dwUser);

/************************************************************************
 ** 实时预览回调原形
 ***********************************************************************/
typedef void(CALLBACK *fRealDataCallBack) (LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser);

/************************************************************************
 ** 原始数据回调原形
 ***********************************************************************/
typedef int(CALLBACK *fDataCallBack) (LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser);

/************************************************************************
 ** 消息（报警）回调原形
 ***********************************************************************/
typedef BOOL (CALLBACK *fMessCallBack)(LONG lCommand, LONG lLoginID, char *pBuf,
			DWORD dwBufLen, char *pchDVRIP, LONG nDVRPort, DWORD dwUser);

/************************************************************************
 ** 回放录像进度回调原形
 ***********************************************************************/
typedef void(CALLBACK *fDownLoadPosCallBack) (LONG lPlayHandle, DWORD dwTotalSize, DWORD dwDownLoadSize, DWORD dwUser);

/************************************************************************
 ** 原始数据回调原形
 ***********************************************************************/
typedef int(CALLBACK *fDataCallBack) (LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser);

/************************************************************************
 ** 透明串口回调原形
 ***********************************************************************/
typedef void(CALLBACK *fTransComCallBack) (LONG lLoginID, LONG lTransComChannel, char *pBuffer, DWORD dwBufSize, DWORD dwUser);




/************************************************************************
 ** SDK初始化
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_Init(fDisConnect cbDisConnect, DWORD dwUser);

/************************************************************************
 ** SDK退出清理
 ***********************************************************************/
CLIENT_API void CALL_METHOD CLIENT_Cleanup();

/************************************************************************
 ** 设置断线重连成功回调函数，设置后SDK内部断线自动重连
 ***********************************************************************/
CLIENT_API void CALL_METHOD CLIENT_SetAutoReconnect(fHaveReConnect cbAutoConnect, DWORD dwUser);

/************************************************************************
 ** 返回函数执行失败代码
 ***********************************************************************/
CLIENT_API DWORD CALL_METHOD CLIENT_GetLastError(void);

/************************************************************************
 ** 设置连接服务器超时时间和尝试次数
 ***********************************************************************/
CLIENT_API void CALL_METHOD CLIENT_SetConnectTime(int nWaitTime, int nTryTimes);


/************************************************************************
 ** 获取SDK的版本信息
 ***********************************************************************/
CLIENT_API DWORD CALL_METHOD CLIENT_GetSDKVersion();


/************************************************************************
 ** 向设备注册
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_Login(char *pchDVRIP, WORD wDVRPort, char *pchUserName, char *pchPassword, LPNET_DEVICEINFO lpDeviceInfo, int *error = 0);

//	nSpecCap = 0为TCP方式下的登入；nSpecCap = 2为主动注册的登入；nSpecCap = 3为组播方式下的登入；nSpecCap = 4为UDP方式下的登入
CLIENT_API LONG CALL_METHOD CLIENT_LoginEx(char *pchDVRIP, WORD wDVRPort, char *pchUserName, char *pchPassword, int nSpecCap, void* pCapParam, LPNET_DEVICEINFO lpDeviceInfo, int *error = 0);

/************************************************************************
 ** 向设备注销
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_Logout(LONG lLoginID);

/************************************************************************
 ** 
 ***********************************************************************/
CLIENT_API void CALL_METHOD CLIENT_SetDVRMessCallBack(fMessCallBack cbMessage,DWORD dwUser);


/************************************************************************
 ** 开始实时预览
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_RealPlay(LONG lLoginID, int nChannelID, HWND hWnd);

// nConnMode: 0-TCP, 1-UDP, 2-Multicast
CLIENT_API LONG CALL_METHOD CLIENT_RealPlayEx(LONG lLoginID, int nChannelID, HWND hWnd, DH_RealPlayType rType = DH_RType_Realplay, int nConnMode=0, char* szLocalIp=NULL);

CLIENT_API BOOL	CALL_METHOD CLIENT_AdjustFluency(LONG lRealHandle, int nLevel);

/************************************************************************
 ** 停止实时预览
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_StopRealPlay(LONG lRealHandle);

/************************************************************************
 ** 停止实时预览_扩展
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_StopRealPlayEx(LONG lRealHandle);

/************************************************************************
 ** 保存数据为文件
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_SaveRealData(LONG lRealHandle, const char *pchFileName);


/************************************************************************
 ** 结束保存数据为文件
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_StopSaveRealData(LONG lRealHandle);

/************************************************************************
 ** 设置实时预览回调
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_SetRealDataCallBack(LONG lRealHandle, fRealDataCallBack cbRealData, DWORD dwUser);


/************************************************************************
 ** 开始帧听设备
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_StartListenEx(LONG lLoginID);
CLIENT_API BOOL CALL_METHOD CLIENT_StopListen(LONG lLoginID);

/************************************************************************
 ** 查询设备当前时间
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_QueryDeviceTime(LONG lLoginID, LPDHNET_TIME pDeviceTime, int waittime=1000);

/************************************************************************
 ** 设置设备当前时间
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_SetupDeviceTime(LONG lLoginID, LPDHNET_TIME pDeviceTime);


/************************************************************************
 ** 回放录像文件
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_PlayBackByRecordFile(LONG lLoginID, LPNET_RECORDFILE_INFO lpRecordFile, HWND hWnd, fDownLoadPosCallBack cbDownLoadPos, DWORD dwUserData);

/************************************************************************
 ** 回放录像文件扩展_增加了数据回调参数
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_PlayBackByRecordFileEx(LONG lLoginID, LPNET_RECORDFILE_INFO lpRecordFile, HWND hWnd, fDownLoadPosCallBack cbDownLoadPos, DWORD dwPosUser, fDataCallBack fDownLoadDataCallBack, DWORD dwDataUser);

/************************************************************************
 ** 通过时间回放录像
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_PlayBackByTime(LONG lLoginID, int nChannelID, 
                                      LPDHNET_TIME lpStartTime, LPDHNET_TIME lpStopTime, HWND hWnd, fDownLoadPosCallBack cbDownLoadPos, DWORD dwPosUser);

/************************************************************************
 ** 通过时间回放录像扩展_增加了数据回调参数
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_PlayBackByTimeEx(LONG lLoginID, int nChannelID, 
                                      LPDHNET_TIME lpStartTime, LPDHNET_TIME lpStopTime, HWND hWnd, fDownLoadPosCallBack cbDownLoadPos, DWORD dwPosUser,
									  fDataCallBack fDownLoadDataCallBack, DWORD dwDataUser);

/************************************************************************
 ** 定位录像回放起始点
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_SeekPlayBack(LONG lPlayHandle, unsigned int offsettime, unsigned int offsetbyte);

/************************************************************************
 ** 停止录像回放
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_StopPlayBack(LONG lPlayHandle);


/************************************************************************
 ** 创建透明串口通道
 ***********************************************************************/
CLIENT_API LONG CALL_METHOD CLIENT_CreateTransComChannel(LONG lLoginID, int TransComType, 
                                      unsigned int baudrate, unsigned int databits,
                                      unsigned int stopbits, unsigned int parity,
                                      fTransComCallBack cbTransCom, DWORD dwUser);

/************************************************************************
 ** 透明串口发送数据
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_SendTransComData(LONG lTransComChannel, char *pBuffer, DWORD dwBufSize);

/************************************************************************
 ** 释放通明串口通道
 ***********************************************************************/
CLIENT_API BOOL CALL_METHOD CLIENT_DestroyTransComChannel(LONG lTransComChannel);



#ifdef __cplusplus
}
#endif

#endif // DHNETSDK_H










































