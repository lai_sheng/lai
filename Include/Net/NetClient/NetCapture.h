
/*!
 * \brief   网络捕获
 * \date    2008/10/31:10:2008   9:27
 * \file    d:\code\P_2008.08.11_GBU4\Net\NetCLient\NetCapture.h
 * \author  zhang_yaozhen
 * \bug
 */
#ifndef __NETCAPTURE_H__
#define __NETCAPTURE_H__

#include "Media/IDevDecoder.h"
#include "MultiTask/Timer.h"
#include "System/Signals.h"
#include "System/Packet.h"
#include "APIs/Capture.h"
#include "Devices/DevCapture.h"
#include "Net/NetClient/CVirtualNetCon.h"

#define CNetCapture CNetChannelCapture

class CaptureHelper;
class CNetChannelCapture;
class CNetCaptureManager;

typedef TSignal3<int, uint, CPacket *> SIG_DEV_CAP;
typedef TSignal3<int, uint, CPacket *>::SigProc SIG_DEV_CAP_BUFFER;
typedef TSignal3<int, ulong, ulong>::SigProc SIG_DEV_PLAYBACKPROC_DELEGATE;
typedef TSignal3<appEventCode, ulong, int>::SigProc NET_DEV_ALARM_DELEGATE;

/*!
  \brief 通道码流
 */
class CNetChannelStream : public CObject
{
public:    //! macro
    void ncs_trace(char *fmt, ...){};
    //if( m_parent_channel->GetDbg()) { printf(fmt, ##args); }
    void ncs_tracepoint(){};
    void ncs_channel_trace(char *fmt, ...){};     //ncs_trace("tracepoint: %s,%d, channel[%d] ",__FILE__,__LINE__, m_parent_channel->GetLocalChannel()); ncs_trace(fmt, ##args);

public:
    CNetChannelStream( CNetChannelCapture * parent_channel, NET_CAPTURE_CHANNEL_T stream_type );
    ~CNetChannelStream();

    //!绑定监视委托
    VD_BOOL Bind       ( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,uint dwFlags,uint dwStreamType );
    VD_BOOL Unbind   ( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,uint dwFlags,uint dwStreamType );

    VD_BOOL BindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc );
    VD_BOOL UnBindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc );

    VD_BOOL BindPlayBackProc(CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc);
    VD_BOOL UnBindPlayBackProc(CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc);

    int SetConfigAsync( INetCliConfig * config );
    int StartMonitor( INetChannel* channel );
    int StopMonitor();

	VD_BOOL adjustPreBuffer();
	void  SetIFrameNum(int number);
    //! 开启或关闭服务
    VD_BOOL Start( );
    VD_BOOL Stop( );

    int GetLoginChannel( );

    int GetStream(void);
    void ClearStream(void);
    VD_BOOL IsOnMonitorRunning();

    //!获取设置配置
    int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
    int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
     bool  IsLogin();

    int  SetDbg( int dbg );
    int  SetReconnect( int iFlag );
    
    VD_VOID SendPtzData(VD_VOID *pData,VD_UINT32 uiLen);

    //false-停止，true-开启运行
    VD_BOOL GetState();

    //!使能通道, flag-true:使能， flag:关闭
    void    Enable(bool flag);
public: //!线程继承
    void dump();
protected:
    void OnMonitor( ulong dwDataType, CPacket* pVideoPacket );
    //void OnAlarm( appEventCode app_code , char* buf, int len );
    // 报警和通道相关的表示具体通道数,无通道相关的值为-1
    // 指示具体的报警类型
    // true-表示相应报警开,false--表示相应报警关
    void OnAlarm(int channel, uint type, int state);
    void OnPBProcess(ulong dwTotalSize, ulong dwCurSize);

private:
    ///< 对码流进行切换的时候，进行强制I帧
    void SetIFrametoNetStream();

    int CheckArgument();
private:
    //!<  当前请求的码流类型
    NET_CAPTURE_CHANNEL_T m_stream_type;
    SIG_DEV_CAP *m_psigMonitor; /*!业务的回调函数*/
	SIG_DEV_CAP *m_sigRecord;
    TSignal3<int,ulong, ulong>*m_psigPBProc;
    TSignal3<appEventCode, ulong, int> *m_psigAlarm;    //!< 报警委托
    VD_BOOL            m_reconnect_flag;
    

    CNetChannelCapture * m_parent_channel;  //! 本码流所属的通道

    int             m_conn_type;   //连接模式
    int                m_streamCalculate;    ///< 码流统计

    CMutex          m_mutex;

//! debug
    unsigned int data_len_per_10second;
    ulong tmp_start_time;
    ulong m_lastending_time;
    INetCliConfig*  m_Config;

    //!记录监视此码流个数
    uint m_montior_count;
    //!连接远端通道
    INetChannel* m_pRemoteChn;
    bool             m_bEnable;
    
	int					m_preSeconds;					///< 预录时间
	VD_BOOL				m_preBufferState;
	uint				m_RecType;						/*!< 开启录像的状态, 用位表示预录和录像 */
};

class CNetChannelCapture : public ICapture
{
public:
    CNetChannelCapture( LOGIC_CHN logic_chn, DIGITAL_CHN local_channle );
    ~CNetChannelCapture();

    //! 注册或反注册委托处理给通道捕获
public:    //!ICapture
    /*int SetLogicChn(LOGIC_CHN logic_chn);*/
    int& GetLogicChn();
    int SetVstd(uint dwStandard);
    int SetCover(VD_RECT *pRect, uint Color, int Enable, int index = 0);
    int SetTime(SYSTEM_TIME * pSysTime);
    //! 设置编码格式
    int SetFormat( CAPTURE_FORMAT *pFormat, uint dwType = CHL_MAIN_T );
    int getImageSize(capture_size_t* pSize, uint dwType = CHL_MAIN_T);
    //! 获取编码格式
    const CAPTURE_FORMAT * GetFormat( uint dwType = CHL_MAIN_T ) const;
    int SetTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect, VD_PCSTR str = NULL,
            FONTSIZE fontsize = FONTSIZE_NORMAL);

    int Start( CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags,
            uint dwStreamType = CHL_MAIN_T );
    int Stop( CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags,
            uint dwStreamType = CHL_MAIN_T );

    //! 开启或关闭通道服务, 此操作会创建或销毁Stream对象
    int Start( uint dwStreamType = CHL_MAIN_T );
    int Stop( uint dwStreamType = CHL_MAIN_T );

    int GetState(uint dwStreamType = CHL_MAIN_T);
    uint GetBufferLength();
    void SetIFrameNum(int number);

    //! 设置I帧
    int SetIFrame( uint dwStreamType = CHL_MAIN_T );
    int adjustPreBuffer();

    uint GetBitRate(int dwType = CHL_MAIN_T);
    void CalculateBitRate();
    void CalculateExBitRate();

    //!某个码流类型有网络码流
	bool HaveBitRate(int dwType);

    void OnData(CPacket *pPacket,uint dwType);
    void OnRecData(CPacket ** pPacket);

    int GetVolume(int *pVolume);

    ENUM_CHANNEL_TYPE GetCaptureType() {return ENUM_CHANNEL_TYPE_DATA;};
    virtual ENUM_CHANNEL_STREAM_TYPE GetCaptureSreamType();
public:
    ulong GetChannelStreamType();
    DIGITAL_CHN GetLocalChannel();
    int GetLoginChannel( ulong dwStreamType = CHL_MAIN_T );

    int SetConfigAsync( ulong dwStreamType, INetCliConfig * config );
    int StartMonitor( INetChannel* channel );
    int StopMonitor( INetChannel* channel );

    VD_BOOL BindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc );
    VD_BOOL UnBindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc );

    VD_BOOL BindPlayBackProc( CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc,
            ulong dwFlags,ulong dwStreamType = NET_CHL_Play_T );
    VD_BOOL UnBindPlayBackProc( CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc,
            ulong dwFlags,ulong dwStreamType = NET_CHL_Play_T );

    //!获取设置配置
    int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
    int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
    bool  IsLogin();

    VD_VOID SendPtzData(VD_VOID *pData,VD_UINT32 uiLen);

    //!使能通道, flag-true:使能， flag:关闭
    void    Enable(bool flag);

public:
    static CNetChannelCapture * instance( LOGIC_CHN channle );

//!调试代码
    void dump();
    int  SetDbg( int dbg );
    int  GetDbg();

    int  SetReconnect( int iFlag );

protected:
    CAPTURE_FORMAT m_captureFormat[CHL_FUNCTION_NUM];

private:
    CNetChannelStream * m_stream[NET_CHL_FUNCTION_NUM];
    DIGITAL_CHN         m_local_channel;    //!< 从0开始
    LOGIC_CHN            m_logic_chn; //!对应的逻辑通道号, 从0开始，-1:没有设置

    ulong        m_dwNetBytes;        /*!< 码流总和  */
    ulong        m_dwNetMS;         /*!< 取码流的间隔时间 */
    ulong        m_NetBitRate;        /*!< 码流值 */
    CMutex          m_mutex;

    int             m_dbg;
    bool             m_bEnable;
};

#endif

