#ifndef CVIRTUALNETCON_H_HEADER_INCLUDED_B67AB080
#define CVIRTUALNETCON_H_HEADER_INCLUDED_B67AB080
#include "INetClient.h"
#include "INetConListener.h"
#include "CNetChannel.h"
#include "MultiTask/Timer.h"
#include "log_zdx.h"

class INetChannel;

typedef enum NCS_GET_FRONT
{
    GET_FRONT_NONE,        //!无效模式
    GET_FRONT_FRAME,    //!帧模式接收数据
    GET_FRONT_FLOW    //!流模式接收数剧
}NCS_GET_FRONT;

//! 此虚拟连接一次只能开一个通道
// !TODO: 几个异步接口要进行单元测试,以测试稳定性
class CVirtualNetCon : public INetConListener, public CThread
{
  public:
    typedef enum
    {
        CS_None,           //空闲状态
        CS_Logined,            //已登陆
        CS_Monitor,         //已开启监视
        CS_PlayBack,        //已开启回放
    }CUR_STATE_E;

    public:
        //!设置视频回调函数蜀回调数据模式，接收层以决定是否进行分析帧
        static int NetCliSetDataMode(NCS_GET_FRONT mode );
        static NCS_GET_FRONT NetCliGetDataMode();
    private:
        static NCS_GET_FRONT    m_DataMode;
  public:
    CVirtualNetCon();
    virtual ~CVirtualNetCon();

    virtual void OnMonitor(ulong channel, CPacket* pPacket);

    // 报警和通道相关的表示具体通道数,无通道相关的值为-1
    // 指示具体的报警类型
    // true-表示相应报警开,false--表示相应报警关
    virtual void OnAlarm(int channel, uint type, int state);
    virtual void OnDefMsg(DefEventCode code, ulong user);
    // 连接断开
    virtual void OnDisconnect(Result_E result);

    // 发生重连,且重连后处理结果
    virtual void OnReconnect(Result_E result);

    // 要开启的通道,1--16
    // 码流类型
    int StartMonitorAsync( INetChannel* channel );
    int StopMonitorAsync( );

    int StartAlarmAsync();
    int StopAlaramAsync();

    int SetConfigAsync( INetCliConfig * config );

    int AttachMonitor( CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );
    int DetachMonitor( CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc );

    int AttachAlarm(CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc);
    int DetachAlarm(CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc);

    //!获取设置配置
    int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
    int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size);
   bool  IsLogin();
    
    int SetDbg( bool turn );
    int AttachPBProc( CObject * pObj, SIG_CLI_PLAYBACK_PROC_DELEGATE pProc ); 
    int DetachPBProc( CObject * pObj, SIG_CLI_PLAYBACK_PROC_DELEGATE pProc ); 
    void SendPtzData( void *pData,unsigned int uiLen);
    void SetReconnect( int iFlag);
  private:
    int CheckArgument(char const *username, char const *password,
            char const *ip, int const port );
    // !返回true表示配置变化
    bool CheckConfig();
    void HandleAlarm();

    CUR_STATE_E  m_cs_state;    // !当前所处于的状态


    VD_BOOL        m_BConfigChangeFlag;
    
    //!目的状态
    CUR_STATE_E    m_dst_state;
    CUR_STATE_E    m_new_dst_state;
                                            
    INetChannel*   m_pNew_Channel;    /* 更新的通道 */
    INetCliConfig*  m_pNew_Config; /* 更新的配置 */
    
    INetChannel*     m_pReady_Channel;    // !当前通道状态
    INetCliConfig*    m_ready_Config;

    unsigned long    m_handle;
    int FsmOperation();
    void OnTimer(uint wParam);

    SIG_MONITOR* m_psigMonitor; /*!< 监视的回调函数指针*/
    SIG_CLI_ALARM* m_pSigAlarm;

    //回放进度反馈
    CObject * m_pPBObject;
    SIG_CLI_PLAYBACK_PROC_DELEGATE m_pPBProc;

    CMutex    m_Mutex;
    CSemaphore m_sem;

    //CTimer    m_timer;

    int     m_iReconnect;   //是否重连

    INetClient *m_INC;
    //!上次登陆时间点, 单位ms
    uint m_last_tm_login;
    //!重登陆间隔, 单位second
    const uint m_ReLogin_interval;
    //!订阅报警标志
    bool m_book_alarm;
    //!true-报警已开启
    bool m_bAlarmStarted;
public:
    virtual void ThreadProc();
};



#endif /* CVIRTUALNETCON_H_HEADER_INCLUDED_B67AB080 */
