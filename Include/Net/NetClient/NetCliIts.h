#ifndef __NETCLIITS_H__
#define	__NETCLIITS_H__

#ifdef NET_CLIENT_ITS

#include "../../MultiTask/Thread.h"
#include "../../PPSocket/ezsocket/include/ez_socket.h"
#include "../Include/AppByteOrder.h"
#include "INetClient.h"
#include <iostream>

class CNetCliXtjc;
class CNetCliIts : public INetClient
{
public:
	typedef struct dvrip
	{
		unsigned char	Head;		/* command  */
		unsigned char	Cmd;		/* reserved */
		unsigned char	Ver;		/* reserved */
		unsigned char	Thl;
		unsigned short	extlen;		/* ext data length */
		unsigned char 	dev_type;
		unsigned char	rsvd;
		unsigned char	data[24];	/* inter dvrip.dvrip_p */
	}DVRIP_ITS;	// !智能交通

	typedef union
	{
		struct dvrip	dvrip; /* struct def */
		unsigned char	c[32]; /* 1 byte def */
		unsigned short	s[16]; /* 2 byte def */
		unsigned int	l[8];  /* 4 byte def */
	}DVRIP_ITS_HEAD_T;

	typedef struct  tagDVRIP_MSG
	{
		DVRIP_ITS_HEAD_T    head;
	    CPacket     *  ext_data;
	}DVRIP_ITS_MSG_T;

	enum EnITSCommand
	{
		ITS_REQ_USER_LOGIN = 0xa0,
		ITS_ACK_USER_LOGIN = 0xb0,
		ITS_REQ_FILE = 0xa7,	// !请求录像,图片,文件,监视
		ITS_ACK_FILE = 0xb7,
		ITS_REQ_HEARTBEAT = 0xc8, // 心跳
		ITS_ACK_HEARTBEAT = 0xD8
	};

	typedef struct tagKEEPLIVE
	{
		BYTE dev_type; 				//!< 设备类型, 比如: dev_type = 0 表示DH-ITS-HWS100,详细请见表4.7:设备类型
		BYTE cam_num;               //!< 摄像机个数
		BYTE rsvd[6];
	} KEEP_LIVE, *p_KEEP_LIVE;

	typedef struct tagKEEP_LIVE_ACK
	{
		DHTIME center_time; //返回中心服务器本地时间
	} KEEP_LIVE_ACK;

#define DEFUALT_UNIT 8*1056
#define Its_Assert(list, x )	\
		if( !(x) )	\
		{	\
			(list).dump();	\
			assert(0);	\
		}
	class CItsMsgList
	{
	public:
		CItsMsgList( )
		:m_VideoPacket(NULL)
		{
			m_data_len = 0;
			m_size = DEFUALT_UNIT*2;
		}
		~CItsMsgList()
		{
			while( 1 )
			{
				if( GetMsgCount() <= 0 )
				{
					break;
				}
				PopFront();
			}
		}

		//!0--取成功
		DVRIP_ITS_MSG_T* GetFront( )
		{
			DoProcess();
			if( GetMsgCount() == 0 )
			{
				return NULL;
			}

			return (*m_Msg_List.begin());
		}
		int PopFront( )
		{
			DVRIP_ITS_MSG_T * msg = (DVRIP_ITS_MSG_T*)( *m_Msg_List.begin() );
			//printf("len[%d]\n", msg->head.dvrip.extlen);
			if( msg->ext_data != NULL )
			{
				msg->ext_data->Release();
			}
			delete msg;
			m_Msg_List.pop_front();

			return 0;
		}
		unsigned char * GetBufPtr( int len )
		{
			if( (m_size - m_data_len) >= len  )
			{
				return (m_RecvBuf+m_data_len);
			}
			return NULL;
		}
		int SetDataLen( int len )
		{
			m_data_len = m_data_len + len;
			assert(m_data_len < m_size);
			return 0;
		}
		int GetDataLen()
		{
			return m_data_len;
		}
		int GetMsgCount()
		{
			return m_Msg_List.size();
		}

		void dump()
		{
			printf("CItsMsgList===>start\n");
			printf("m_data_len = %d\n", m_data_len);
			printf("m_size = %d\n", m_size);
			printf("Msg list count[%d]\n", GetMsgCount() );
			if( (DWORD)m_data_len > sizeof(DVRIP_ITS_HEAD_T) )
			{
				DVRIP_ITS_HEAD_T *	pHead = (DVRIP_ITS_HEAD_T*)(m_RecvBuf+0);
				printf("     cmd[%#x]\n", pHead->dvrip.Cmd);
				printf("     extlen[%#x]\n", pHead->dvrip.extlen );
			}
			printf("CItsMsgList===>end\n");
		}
	private:
		int DoProcess()
		{
			int pos = 0;
			while(1)
			{
				if( (DWORD)m_data_len >= 1056 )
				{
					DVRIP_ITS_HEAD_T *	pHead = (DVRIP_ITS_HEAD_T*)(m_RecvBuf+pos);
					pHead->dvrip.extlen = FlipBytes( sizeof(pHead->dvrip.extlen), pHead->dvrip.extlen);

					//trace("@@@@@@@@@@@@@@@@@@@@@@@>len:%d cmd:%#x, datalen[%d], pos[%d]\n",
					//	pHead->dvrip.extlen, pHead->dvrip.Cmd, m_data_len, pos);
					assert( pHead->dvrip.Cmd != 0 );
					DVRIP_ITS_MSG_T* tmpMsg = new DVRIP_ITS_MSG_T;
					if( pHead->dvrip.Cmd == 0xbd ) //!0xbd-系统集成视频包标志
					{
						if( m_VideoPacket == NULL )
						{
							m_VideoPacket = g_PacketManager.GetPacket( DEFUALT_UNIT+1024 );
						}
						tmpMsg->ext_data = m_VideoPacket;

						if( pHead->dvrip.extlen > 0 )
						{
							tmpMsg->ext_data->PutBuffer( m_RecvBuf+pos+sizeof(DVRIP_ITS_HEAD_T), pHead->dvrip.extlen);
						}

						if( tmpMsg->ext_data->GetLength() >= DEFUALT_UNIT )
						{
							memcpy( &tmpMsg->head, pHead, sizeof(DVRIP_ITS_HEAD_T) );

							tmpMsg->ext_data->AddRef();
							m_Msg_List.push_back(tmpMsg);

							m_VideoPacket->Release();
							m_VideoPacket = NULL;
						}
						else
						{
							delete tmpMsg;
							tmpMsg = NULL;
						}
					}
					else
					{
						if( pHead->dvrip.extlen > 0 )
						{

							memcpy( &tmpMsg->head, pHead, sizeof(DVRIP_ITS_HEAD_T) );
							tmpMsg->ext_data = g_PacketManager.GetPacket( pHead->dvrip.extlen );
							assert( tmpMsg->ext_data != NULL );
							tmpMsg->ext_data->PutBuffer( m_RecvBuf+pos+sizeof(DVRIP_ITS_HEAD_T), pHead->dvrip.extlen);
						}
						else
						{
							tmpMsg->ext_data = NULL;
						}

						m_Msg_List.push_back(tmpMsg);
					}

					//! 校正数据长度和下一次分析位置
					m_data_len -= 1056;//( sizeof(DVRIP_ITS_HEAD_T) + pHead->dvrip.extlen) ;
					pos += 1056;//( sizeof(DVRIP_ITS_HEAD_T) + pHead->dvrip.extlen);
				}
				else if( pos > 0 )
				{
					memmove( m_RecvBuf, m_RecvBuf + pos, m_data_len );
					return 0;
				}
				else
				{
					return 0;
				}
			}
		}
	private:
		unsigned char m_RecvBuf[DEFUALT_UNIT*2];
		int m_data_len;
		int m_size;
		std::list<DVRIP_ITS_MSG_T*> m_Msg_List;
		CPacket * m_VideoPacket;
	};
public:
    typedef enum
    {
        CONNECT_NONE    = 0,
        CONNECT_ONLY        = 1,
        CONNECT_LOGIN           = 2,
        CONNECT_MONITOR     =3      //!已经打开监视一些通道了
    }NET_STATE_E;
    CNetCliIts( INetCliConfig * config );
    virtual ~CNetCliIts();

    //virtual int IsMatch( INetCliConfig * config, CNetChannel* net_channel );
    virtual INetCliConfig * GetConfig();
    //! time_out: 超时时间，－1 表示默认时间
	virtual int     Login() ;
	virtual int     Logout() ;
	virtual int     StartMonitor( INetChannel* net_channel ) ;
	virtual int     StopMonitor( INetChannel* net_channel ) ;
	//!返回实际取的数据长度
	//virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;

   virtual int     KeepAlive();
	//!返回true 表示已经登录，false没有登录
	virtual bool	IsLogin() ;
	//!返回true 表示此通道正在监视
	//virtual bool	TestOpenMonitorChannel( const int channel ) ;

public:
    //! 搜集其需要select 的sock
    //!max_fd [in] --现今最大的sock值, 此次sock搜集前
    //!max_fd[out] --现今最大的sock值，sock搜集后
    int             GatherReadSockSet( fd_set* read_set, int& max_fd );
    int             MonitorRecvProc( fd_set* read_set, int time_out );
protected:
	int     OpenServer( const unsigned char * ip, const int port, int time_out = -1 ) ;
	int     Close() ;
    int             SendDVRIPMsg( const ez_socket_t sock, const DVRIP_ITS_MSG_T& monitor_data );
    int             RecvDVRIPMsg( const ez_socket_t sock, DVRIP_ITS_MSG_T& monitor_data, int time_out  );
    int 			KeepAliveAck();

	ez_socket_t	connect(const char * hostip, const ez_port_t port);
private:
	CNetCliXtjc*	m_config;
    int             m_remote_channel_num;   //!远端channel数目

    ez_socket_t     m_sock;
    NET_STATE_E     m_net_state;
    bool 			m_net_disconnect;	// !网络已断

    CItsMsgList		m_its_list;

    TSignal2<DWORD, CPacket* > * m_pMonitor;
    TSignal2<DefEventCode, DWORD> m_DefEvent;

    const DWORD           m_monitor_keep_live_time;   //!监视,保活时间, 单位ms
    DWORD                 m_last_monitor_recv_time;      //!上次收到监视数据时间
    DWORD				  m_send_last_heart_beat_time;	//! 上次发送heart ack 的时间

    CMutex					m_mutex;

    CPacket*				m_pPacket;
};

class CNetCliManager_Its : public CThread
{

public:
    static CNetCliManager_Its * instance();
    CNetCliManager_Its();
    //! 增加
    int AddNetCli( const CNetCliIts * pNetCli );
    int RemoveNetCli( const CNetCliIts * pNetCli );

	void Start();

	//! 关闭线程
	void Stop();

	//! 线程的执行体
	void ThreadProc();

protected:
private:
    std::list< CNetCliIts* > m_CliCollect;
    CMutex					m_mutex;

};
#endif

#endif
