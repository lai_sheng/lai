#ifndef __NETCLISIMPLE_H__
#define	__NETCLISIMPLE_H__

#ifdef NET_CLIENT_SIMPLE
#include <iostream>
#include "MultiTask/Thread.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "INetClient.h"
#include "log_zdx.h"
#include "CVirtualNetCon.h"
#include "MultiTask/Timer.h"
#include "NetCliConfigDef.h"

class CNetCliDahuaII;
class CNetChnnel_DahuaII;

/*
 *\brief: CNetCliSimple 简单版的实现协议处理及数据接收
 * 各种逻辑还不够严密
 */
class CNetCliSimple : public INetClient,public CObject
{
public:
    
#define DEFUALT_UNIT (512*1024)
#define Its_Assert(list, x )	\
	if( !(x) )	\
	{	\
	(list).dump();	\
	assert(0);	\
	  }
	class CItsMsgList
	{
	public:
		CItsMsgList( )
		{
			m_data_len = 0;
			m_size = DEFUALT_UNIT*2;
			m_process_pos = 0;
			memset(&m_Cur_Msg, 0, sizeof(m_Cur_Msg));

			m_have_msg = false;
		}
		~CItsMsgList()
		{
			PopFront();
		}

		//!0--取成功, 这里一定要一直取，直到取完为步
		//!每调完一次，要调用一次popfront一次，把刚才的数据包清掉
		DVRIP_MSG_T* GetFront(int iType = GET_FRONT_FLOW)
		{
			DoProcess(iType);
			
			if ( m_have_msg )
			{
				//printf("line:%d, cmd:%#x, len:%d\n", __LINE__, m_Cur_Msg.head.dvrip.dvrip_cmd, m_Cur_Msg.head.dvrip.dvrip_extlen);
				return &m_Cur_Msg;
			}

			//!没有分析到消息，把缓冲区剩余数据移到头部，以简化处理过程
			if ( m_process_pos > 0 )
			{
				memmove(m_RecvBuf, m_RecvBuf+m_process_pos, m_data_len);
				m_process_pos = 0;
				m_have_msg = false;
			}

			return NULL;
		}
		int PopFront( )
		{
			if( m_Cur_Msg.ext_data != NULL )
			{
				m_Cur_Msg.ext_data->Release();
				m_Cur_Msg.ext_data = NULL;
			}
            
			m_Cur_Msg.pData = NULL;
			m_have_msg = false;
			return 0;
		}

		void Clear()
		{
			PopFront();
			m_data_len = 0;
			m_process_pos = 0;
		}

		//!取长度为len的缓冲区指针, 为了实现从socket里直接取数据不用进行一次复制
		unsigned char * GetBufPtr( int len )
		{
			assert(m_process_pos == 0 );
			if( (m_size - m_data_len ) >= len  )
			{
				return (m_RecvBuf+m_data_len);
			}
			return NULL;
		}
		int SetDataLen( int len )
		{
			assert(m_process_pos == 0 );
			m_data_len = m_data_len + len;
			assert(m_data_len <= m_size);
			return 0;
		}
		int GetDataLen()
		{
			return m_data_len;
		}

		int GetSpareLen()
		{
			assert(m_process_pos == 0 );
//            printf("m_process_pos = %d\r\n",m_process_pos);

			return m_size - m_data_len;
		}

		void dump( int id = 0 )
		{
			printf("CItsMsgList===>start\n");
			printf("m_data_len = %d\n", m_data_len);
			printf("m_size = %d\n", m_size);
			
			int data_len = m_data_len;
			int pos = 0;
			while( data_len > (int)sizeof(DVRIP_HEAD_T) )
			{
				DVRIP_HEAD_T *	pHead = (DVRIP_HEAD_T*)(m_RecvBuf+pos);
				uint *pExtLen = (uint*)(m_RecvBuf+pos+4);
				DVRIP_HEAD_T head;
				memcpy(&head, m_RecvBuf+pos, sizeof(DVRIP_HEAD_T) );
				//memcpy(&pHead->dvrip.dvrip_extlen, m_RecvBuf+pos+4, 4);
				struct dvrip* pdhead = (struct dvrip*)(m_RecvBuf+pos);
				uint texlen = 0;
				memcpy(&texlen, m_RecvBuf+pos+4, 4);
				printf("     cmd[%#x], cmd2:%#x\n", pHead->dvrip.dvrip_cmd, head.dvrip.dvrip_cmd);
				printf("     extlen[%d]\n", pHead->dvrip.dvrip_extlen );
				printf("test len:%d, len2:%d, len3:%d, plen4:%d, addr:%p\n", 
					pdhead->dvrip_extlen, texlen, head.dvrip.dvrip_extlen, *pExtLen, m_RecvBuf+pos+4);
				assert(0 != pHead->dvrip.dvrip_cmd);
				//assert(texlen == pHead->dvrip.dvrip_extlen);

				for (int i = 0; i < 8; i++)
				{
					printf("%#x ", pHead->c[i]);
				}
				printf("\n");
				if ( id == 1 )
				{
					if ( head.dvrip.dvrip_cmd != 0xbc)
					{
#if 0
						FILE * fp = NULL;
						if ( fp == NULL )
						{
							fp = fopen("/root/recvbuf.dat", "wb+");
						}

						fwrite(m_RecvBuf, 1, m_data_len, fp);
						fclose(fp);
#endif
					}
					assert( head.dvrip.dvrip_cmd == 0xbc );
				}
				
				data_len -= sizeof(DVRIP_HEAD_T);
				data_len -= head.dvrip.dvrip_extlen;

				pos += head.dvrip.dvrip_extlen;
				pos += sizeof(DVRIP_HEAD_T);

				if ( pos > m_data_len )
				{
					break;
				}
			}
			printf("CItsMsgList===>end\n");
		}
	private:
		int DoProcess(int iType = GET_FRONT_FLOW);
		
	public:
		unsigned char m_RecvBuf[DEFUALT_UNIT*2];
		DVRIP_MSG_T m_Cur_Msg;
		int m_process_pos;
		int m_data_len;
		bool m_have_msg;
		int m_size;
	};

    
public:
    typedef enum
    {
        CONNECT_FAILED = -1,
        CONNECT_NONE = 0,
        CONNECT_ONLY = 1,
        CONNECT_LOGIN = 2,
        CONNECT_MONITOR =3,      //!已经打开监视一些通道了
    }NET_STATE_E;

	typedef struct
	{
		int	type;	//T
		int res;	//保留，暂用于嵌套TLV中外层T
		int chn;	//通道号
		char* data;	//V
		int len;	//L
		long sec;
	}DataCfg;
	typedef std::list<DataCfg> DataCfgList;
	
    CNetCliSimple( INetCliConfig * config );
    virtual ~CNetCliSimple();
    virtual void Reset();

	virtual INetCliConfig * GetConfig();
	virtual int     Login( ) ;
	virtual int     Logout() ;
	virtual int     StartMonitor( INetChannel* net_channel ) ;
	virtual int     StopMonitor( INetChannel* net_channel ) ;

	virtual int StartAlarm();
	virtual int StopAlarm();

	//!返回实际取的数据长度
	//virtual int     GetMonitorData( const int channel, DVRIP_MSG_T& monitor_data, int time_out ) ;

    virtual int AttachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;
    virtual int DetachDelegate( int channel, CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc ) ;

    //!报警委托
    virtual int AttachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm( int channel, CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

	virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc );
	virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc );

    virtual int SendPtzData(int channel,char *pData,unsigned int uiLen);
	virtual int SendCfgData(int sock, char *pData, unsigned int uiLen);

    virtual int     KeepAlive();
	void DelTimeoutCfg(uint arg);
	//TypeMain :类型即T；index:通道号，对于IPC 只有一个通道传0,不需要时也传0;
	//subIndex:0; 	buf: 	针对每种配置有不同的结构体	;size:
    virtual int GetParam(int TypeMain, int index, int subIndex, void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex, void* buf,  int size );

	//以下接口供内部调用，其参数可为上面buf 作参考，在以下接口上面的注释为类型T
	//DEV_ABILITY_ENCODE 获取设备能力集
	int GetAbility(WQ_NET_ABILITY_INFO* output);
	//ENCODE_MAIN_STREAM_TYPE	ENCODE_SUB_STREAM_TYPE
	int GetMainSubStreamCfg(int chn, WQ_NET_MAIN_SUB_INFO* output);
	int SetMainSubStreamCfg(int chn, WQ_NET_ENCODE* input);
	//ENCODE_COLOR_TYPE
	int GetColorCfg(int chn, WQ_NET_VIDEOCOLOR* output);	
	int SetColorCfg(int chn, WQ_NET_VIDEOCOLOR* input);
	//ENCODE_CHANNEL_NAME_TYPE
	int GetChannelNameCfg(int chn, WQ_CHANNEL_NAME* output);
	int SetChannelNameCfg(int chn, WQ_CHANNEL_NAME* input);
	//ENCODE_TIMEOSD_TYPE
	int GetTimeOSDCfg(int chn, WQ_NET_VIDEO_WIDGET* output);
	int SetTimeOSDCfg(int chn, WQ_NET_VIDEO_WIDGET* input);
	//ENCODE_TITLEOSD_TYPE
	int GetTitleOSDCfg(int chn, WQ_NET_VIDEO_WIDGET* output);
	int SetTitleOSDCfg(int chn, WQ_NET_VIDEO_WIDGET* input);
	//ENCODE_COVER_TYPE
	int GetCoverCfg(int chn, WQ_NET_VIDEO_COVER* output);
	int SetCoverCfg(int chn, WQ_NET_VIDEO_COVER* input);
	//DEV_SYSTEM_INFO_GENERAL
	int  GetGeneralCfg(WQ_NET_GENERAL* output);
	int  SetGeneralCfg(WQ_NET_GENERAL* input);
	//DEV_SYSTEM_CUR_TIME
	int  GetCurTimeCfg(SYSTEM_TIME* output);
	int  SetCurTimeCfg(SYSTEM_TIME* input);
	//DEV_SYSTEM_AUDIO_CFG
	int  GetAudioCfg(WQ_CONFIG_AUDIO_FORMAT_NET* output);
	int  SetAudioCfg(WQ_CONFIG_AUDIO_FORMAT_NET* input);
	//DEV_ALARM_MOTION_TYPE
	int GetMotionCfg(int chn, WQ_NET_MOTION_INFO* output);
	int SetMotionCfg(int chn, WQ_NET_MOTION_INFO* input);
	//DEV_ALARM_BLIND_TYPE
	int GetBlindCfg(int chn, WQ_NET_BLIND_INFO* output);
	int SetBlindCfg(int chn, WQ_NET_BLIND_INFO* input);
	//DEV_ALARM_LOSS_TYPE
	int GetLossCfg(int chn, WQ_NET_LOSS_INFO* output);
	int SetLossCfg(int chn, WQ_NET_LOSS_INFO* input);

	int GetPtzProtTitle(int chn, char* output);
	int GetPtzCfg(int chn, char* output);
	int SetPtzCfg(int chn, char* input);

	int GetAlarmInCfg(int chn, WQ_NET_ALARM_INFO* output);
	int SetAlarmInCfg(int chn, WQ_NET_ALARM_INFO* input);

	int CtrlReboot(int chn, char* input);
	int SetDefaultCfg(int chn, char* input);


	//!返回true 表示已经登录，false没有登录
	virtual bool	IsLogin() ;
	//!返回true 表示此通道正在监视
	virtual bool	TestOpenMonitorChannel( const int channel ) ;

    /* 转到预置点 */
	virtual int     SendPTZGotoPresetMsg(int iCh,int iNum);

    virtual int     SetAllocFlag(int iFlag);
    virtual int     GetAllocFlag();
    ez_socket_t	 connect_time(const char *hostip, const ez_port_t port, unsigned int time );
    int             SetDahuaYaanFlag(int iFlag);

public:
	friend class CNetCliManager;
    //! 搜集其需要select 的sock
    //!max_fd [in] --现今最大的sock值, 此次sock搜集前
    //!max_fd[out] --现今最大的sock值，sock搜集后
    int             GatherReadSockSet( fd_set* read_set, int& max_fd );
    int             MonitorRecvProc( fd_set* read_set, int time_out );
protected:
	//! time_out: 超时时间，－1 表示默认时间
	virtual int     OpenServer( const unsigned char * ip, const int port, int time_out = -1 ) ;
	virtual int     Close() ;
	
	void MakeMonitor(DVRIP_MSG_T& monitor_msg, int channel, 
		NET_CAPTURE_CHANNEL_T stream, bool start );
	CNetCliDahuaII*			m_config;
    int             SendDVRIPMsg( const ez_socket_t sock, const DVRIP_MSG_T& monitor_data );
    int             RecvDVRIPMsg( const ez_socket_t sock, DVRIP_MSG_T& monitor_data, int time_out  );

	int				InnerLogin();

	//!对每一个请求，或者收到应答，或者超时，都会调用此函数
	//!收到应答时，传应答过来，超时，传请求包过来
	void			OnMsgAck( DVRIP_MSG_T& msg, bool time_out );

	void			NotifyDisconnect();

    /* 缓冲帧 */
    int             ProcessMonitorData(int iCh,DVRIP_MSG_T* pmonitor_data);
    int             ProcessFrameData(int iCh,const unsigned char* pData,int iLen,int iFrameType);

    int             ProcessMonitorData_Dahua(int iCh,DVRIP_MSG_T* pmonitor_data);
    int             BookAlarm(uint alarm_type);
private:
	CMutex			m_Mutex;
	CItsMsgList		m_sock_dvrip_buf;
    ez_socket_t     m_sock;
    char            m_acIp[32];
    int             m_iPort;
    
	CItsMsgList		m_monitor_dvrip_buf;
    ez_socket_t     m_Monitor_sock;
    NET_STATE_E     m_net_state;

	//!记录监视socket，tcp缓冲里最早到的时间值
	ulong			m_Monitor_sock_old_time;

	int             m_remote_channel_num;   //!远端channel数目
	int             m_remote_analog_num;   //!远端模拟通道数目
	WQ_SYSATTR_EX_T	m_sysattr; 			//登录后返回的设备属性信息

    unsigned long   m_sequence;

	CNetChnnel_DahuaII*			m_pChannel;
	SIG_MONITOR* m_pSigMonitor;
	SIG_DEFAULT* m_pSigDef;

    unsigned char        *m_pFrameData;
    int          m_iFrameDataLeftLen;
    int          m_BWaitIFrame;
    const ulong           m_monitor_keep_live_time;   //!监视,保活时间, 单位ms
    ulong                 m_last_monitor_recv_time;      //!上次收到监视数据时间
	bool				  m_bDisconnect;
	DataCfgList m_cfglist;
	CMutex		m_cfgMutex;
	CTimer*		m_pDelCfgTimer;
	
	SIG_CLI_ALARM* m_psigAlarm;
	bool           m_book_alarm;   //!是否订阅报警
protected:
	bool MatchRequest( DVRIP_MSG_T& request, DVRIP_MSG_T& reponse );
	int Request(int m_sock, DVRIP_MSG_T& request, int time_out, bool wait);

	CSemaphore m_sem;
	int m_int_sem;
	//!dvr reuqst msg
	typedef struct  
	{
		DVRIP_MSG_T msg;	//!请求消息
		ulong time_out;		//!消息超时时间
		ulong time_start;	//!请求开始时间
	}DVR_MSG_Request;
	typedef std::list<DVR_MSG_Request*> DVR_MSG_R_LIST;

	DVR_MSG_R_LIST m_DMRList;
	CMutex m_list_guard;
	int m_keeplive_failed;
        int m_iAllocFlag;
        int m_iDahuaYaanFlag;
		bool m_bAutoSyncTime;
};

class CNetCliManager : public CThread
{

public:
    static CNetCliManager * instance();
    CNetCliManager();
    
    
    CNetCliSimple *AllocNetCli(INetCliConfig * config);    
    bool FreeNetCli(CNetCliSimple* pstNCS);
    
	void Start();

	//! 关闭线程
	void Stop();

	//! 线程的执行体
	void ThreadProc();

	void dump();
protected:
	void KeepLive();
	ulong m_last_keeplive_time;

	//! 增加
	int AddNetCli( const CNetCliSimple * pNetCli );
	int RemoveNetCli( CNetCliSimple * pNetCli );
private:
	typedef std::list< CNetCliSimple* > NCS_List;
    NCS_List m_CliCollect;

    NCS_List m_CliPool;
    
	CMutex m_list_Mutex;
	CMutex m_list_Pool_Mutex;
	bool m_bLock;
	CNetCliSimple* m_bRemove;
};

#endif

#endif
