
#ifndef __NET_CLI_CONFIG_DEF_ONVIF_H__
#define __NET_CLI_CONFIG_DEF_ONVIF_H__

#include "System/BaseTypedef.h"

#define GENERAL_STRLEN 32
#define NCL_CFG_TYPE_ONVIF_START        10

//GetDeviceInformationRequest
#define NCL_CFG_TYPE_ONVIF_DEVICE_INFO  (NCL_CFG_TYPE_ONVIF_START+1)
#define NCL_CFG_TYPE_ONVIF_FACTORY_DEFAULT  (NCL_CFG_TYPE_ONVIF_START+2)
#define NCL_CFG_TYPE_ONVIF_REBOOT  (NCL_CFG_TYPE_ONVIF_START+3)
#define NCL_CFG_TYPE_ONVIF_SETDATETIME  (NCL_CFG_TYPE_ONVIF_START+4)

#define NCL_CFG_TYPE_ONVIF_VIDEO_SOURCE  (NCL_CFG_TYPE_ONVIF_START+5) //获取视频通道个数
#define NCL_CFG_TYPE_ONVIF_AUDIO_SOURCE  (NCL_CFG_TYPE_ONVIF_START+6) //获取视频通道个数
#define NCL_CFG_TYPE_ONVIF_MEDIA_PROFILE  (NCL_CFG_TYPE_ONVIF_START+7) //码流信息,码流个数及码流压缩参数等

#define NCL_CFG_TYPE_ONVIF_VIDEO_ENCODER_CONFIGURATION  (NCL_CFG_TYPE_ONVIF_START+8) //视频编码信息
#define NCL_CFG_TYPE_ONVIF_AUDIO_ENCODER_CONFIGURATION  (NCL_CFG_TYPE_ONVIF_START+9) //视频编码信息
#define NCL_CFG_TYPE_ONVIF_IMAGE_SETTING  (NCL_CFG_TYPE_ONVIF_START+10) //颜色对比度等
#define NCL_CFG_TYPE_ONVIF_IMAGE_GET_OPTIONS  (NCL_CFG_TYPE_ONVIF_START+11) //颜色对比度等的范围

#define NCL_CFG_TYPE_ONVIF_TIME_OSD  (NCL_CFG_TYPE_ONVIF_START+12)
#define NCL_CFG_TYPE_ONVIF_OSD  (NCL_CFG_TYPE_ONVIF_START+13)
#define NCL_CFG_TYPE_ONVIF_PTZAUXSWITCHES  (NCL_CFG_TYPE_ONVIF_START+14) // 辅助开关

#define NCL_CFG_TYPE_ONVIF_UPGRADE  (NCL_CFG_TYPE_ONVIF_START+15) //升级
#define NCL_CFG_TYPE_ONVIF_PTZ  (NCL_CFG_TYPE_ONVIF_START+16)  //云台控制
#define NCL_CFG_TYPE_ONVIF_PTZ_GET_PREST (NCL_CFG_TYPE_ONVIF_START+17)
#define NCL_CFG_TYPE_ONVIF_VIDEOENCODE_OPTIONS (NCL_CFG_TYPE_ONVIF_START+18)

//NCL_CFG_TYPE_ONVIF_DEVICE_INFO  
typedef struct ST_ONVIF_DEVICEINFORMATION_
{
    char acManufacturer[32];
    char acModel[32];
    char acFirmwareVersion[32];
	char acSerial[32];
    char acHardwareId[32];
}ST_ONVIF_DEVICEINFORMATION;

//NCL_CFG_TYPE_ONVIF_Time_OSD
typedef struct ST_ONVIF_TIME_OSD_
{
    int       iEnable;
    float    fx;
    float    fy;
}ST_ONVIF_TIME_OSD;

//NCL_CFG_TYPE_ONVIF_OSD
typedef struct ST_ONVIF_OSD_
{
    int       iEnable;
    char    acName[64];
    float    fx;
    float    fy;
}ST_ONVIF_OSD;

//! 视频颜色结构
typedef struct ST_ONVIF_IMAGE_
{
    int    iBrightness;        /*!< 亮度    0-100 */
    int    iContrast;            /*!< 对比度    0-100 */
    int    iSaturation;        /*!< 饱和度    0-100 */
    int    iHue;                /*!< 色度    0-100 */
    int    iSharpness;                /*!< 锐度    0-100 */
    int    iResv[8];
}ST_ONVIF_IMAGE; 

//!媒体格式
typedef struct  ST_ONVIF_VIDEO_ENCODE_
{
    int        iCompressionType;            /*!< 压缩模式 */
    int        iEncodingInterval;            /*!< 压缩率*/
    int        iResolution;            /*!< 分辨率 参照枚举capture_size_t(DVRAPI.H) */    
    int        iBitRateControl;        /*!< 码流控制 参照枚举capture_brc_t(DVRAPI.H) */    
    int        iQuality;                /*!< 码流的画质 档次1-6    */    
    int        nFPS;                    /*!< 帧率值，NTSC/PAL不区分,负数表示多秒一帧*/        
    int        nBitRate;                /*!< 0-4096k,该列表主要由客户端保存，设备只接收实际的码流值而不是下标。*/
    int        iGOP;                    /*!< 描述两个I帧之间的间隔时间，1-100 */
    int         iVideoEnable;            /*!< 开启视频编码*/
    int         iAudioEnable;            /*!< 开启音频编码*/
    char       acName[128];
    int          iUseCount;  
    int          iH264Profile;
    int          iResv[8];
} ST_ONVIF_VIDEO_ENCODE;

typedef struct  ST_ONVIF_AUDIO_ENCODE_
{
    int        iEncodetype;
    int        iSoundCh;       
    int        iSourceType;
    int        iResv[8];
} ST_ONVIF_AUDIO_ENCODE;


typedef struct ONVIF_DATE_TIME
{
    int  year;///< 年。   
    int  month;///< 月，January = 1, February = 2, and so on.   
    int  day;///< 日。   
    int  wday;///< 星期，Sunday = 0, Monday = 1, and so on   
    int  hour;///< 时。   
    int  minute;///< 分。   
    int  second;///< 秒。   
}ONVIF_DATE_TIME;

typedef struct  ST_ONVIF_TIME_SET_
{
    int iType;//时间同步方式:0 manu 手动1: NTP

    int iDayLightSavings;//夏令时开关0 关闭，1使能
    int  TimeZone;

    ONVIF_DATE_TIME UTCDateTime;
} ST_ONVIF_TIME_SET;

//NCL_CFG_TYPE_ONVIF_UPGRADE

#define ONVIF_UPGRATE_TYPE_FIRMWARE_UPGRADE  0
#define ONVIF_UPGRATE_TYPE_START_FIRMWARE_UPGRADE 1

typedef struct  ST_ONVIF_UPGRADE_
{
    int iType;//0 Firmware upgrade  1:Start firmware upgrade 升级方式
    int iFileType;//0 从内存升级1:从文件升级
    char acFile[256];//文件路径和名字
    char *pData;//从内存升级时内存的地址
    int iLen;//从内存升级时升级包的长度     
} ST_ONVIF_UPGRADE;

typedef struct
{
    int ucPresetId;
    char presetName[GENERAL_STRLEN];
	char presetToken[GENERAL_STRLEN];
}ONVIF_PRESET_INFOR;

typedef struct ONVIF__IntRange_
{
	int Min;	/* required element of type xsd:int */
	int Max;	/* required element of type xsd:int */
}ONVIF__IntRange;

typedef struct  ST_ONVIF_VIDEOENCODE_OPTIONS_
{
	ONVIF__IntRange stQualityRange;
	unsigned int uiResolutions;//按照掩码表示
	ONVIF__IntRange stGovLengthRange;
	ONVIF__IntRange stFrameRateRange;
	ONVIF__IntRange stEncodingIntervalRange;
//	tt__H264Profile__Baseline = 0, tt__H264Profile__Main = 1, tt__H264Profile__Extended = 2, tt__H264Profile__High = 3}
	unsigned int uiH264ProfilesSupported;//按照掩码表示//tt__H264Profile 
}ST_ONVIF_VIDEOENCODE_OPTIONS;


#endif


