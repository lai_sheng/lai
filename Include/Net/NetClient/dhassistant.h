
#ifndef DHSYSABLE_H
#define DHSYSABLE_H


//////////////////////////////////////////////////////////////////////////
//	查询类型
typedef enum
{
	ABILITY_WATERMARK_CFG = 17,			//水印配置能力
	ABILITY_WIRELESS_CFG = 18,			//wireless配置能力
	ABILITY_DEVALL_INFO = 26,			//设备的能力列表
	ABILITY_CARD_QUERY = 0x0100,		//卡号查询能力
	ABILITY_MULTIPLAY = 0x0101,			//多画面预览能力
	ABILITY_INFRARED = 0X0121,			//无线报警能力
} DH_SYS_ABILITY;

//////////////////////////////////////////////////////////////////////////
//	设备支持功能列表
enum 
{
	EN_FTP = 0,							//FTP 按位，1：传送录像文件 2：传送抓图文件
	EN_SMTP,							//SMTP 按位，1：报警传送文本邮件 2：报警传送图片
	EN_NTP,								//NTP	按位：1：调整系统时间
	EN_AUTO_MAINTAIN,					//自动维护 按位：1：重启 2：关闭 3:删除文件
	EN_VIDEO_COVER,						//区域遮挡 按位：1：多区域遮挡
	EN_AUTO_REGISTER,					//主动注册	按位：1：注册后sdk主动登陆
	EN_DHCP,							//DHCP	按位：1：DHCP
	EN_UPNP,							//UPNP	按位：1：UPNP
	EN_COMM_SNIFFER,					//串口抓包 按位：1:CommATM
	EN_NET_SNIFFER,						//网络抓包 按位： 1：NetSniffer
	EN_BURN,							//刻录功能 按位：1：查询刻录状态
	EN_VIDEO_MATRIX,					//视频矩阵 按位：1：是否支持视频矩阵
	EN_AUDIO_DETECT,					//音频检测 按位：1：是否支持音频检测
	EN_STORAGE_STATION,					//存储位置 按位：1：Ftp服务器(Ips) 2：U盘 3：NFS 4：DISK 5：SBM
	EN_IPSSEARCH,						//IPS存储查询 按位：1：IPS存储查询	
	EN_SNAP,							//抓图  按位：1：分辨率2：帧率3：抓图方式4：抓图文件格式5：图画质量
	EN_DEFAULTNIC,						//支持默认网卡查询 按位 1：支持
	EN_SHOWQUALITY,						//CBR模式下显示画质配置项 按位 1:支持
};

typedef struct 
{
	DWORD IsFucEnable[512];				//功能列表能力集,下标对应上述的枚举值,按位表示子功能
} DH_DEV_ENABLE_INFO;

//////////////////////////////////////////////////////////////////////////
//	卡号查询能力结构体
typedef struct 
{
	char		IsCardQueryEnable;
	char		iRev[3];
} DH_CARD_QUERY_EN;

//////////////////////////////////////////////////////////////////////////
//	wireless能力结构体
typedef struct 
{
	char		IsWirelessEnable;
	char		iRev[3];
} DH_WIRELESS_EN;

//////////////////////////////////////////////////////////////////////////
//	图象水印能力结构体
typedef struct 
{
	char		isSupportWM;	//1 支持；0 不支持
	char		supportWhat;	//0：文字水印；1：图片水印；2：同时支持文字水印和图片水印
	char		reserved[2];
} DH_WATERMAKE_EN;

//////////////////////////////////////////////////////////////////////////
//	多画面预览能力结构体
typedef struct  
{
	int			nEnable;			//1 支持；0 不支持
	DWORD		dwMultiPlayMask;	//保留
	char		reserved[4];
} DH_MULTIPLAY_EN;

typedef struct  
{
	BOOL bSupport;//是否支持
	int	 nAlarmInCount;//输入个数
	int  nAlarmOutCount;//输出个数
	int  nRemoteAddrCount;//遥控器个数
	BYTE reserved[32];
}DH_WIRELESS_ALARM_INFO;

#endif // DHSYSABLE_H