#ifndef INETCONLISTENER_H_HEADER_INCLUDED_B67AF698
#define INETCONLISTENER_H_HEADER_INCLUDED_B67AF698

#include <string>
#include "System/Object.h"
#include "System/Packet.h"
#include "NetCliConfig-x.h"
#include "INetClient.h"

class INetConListener : public CObject
{
public:
    typedef enum
    {
        SUCESS = 0,
        LISTENER_ERROR,
        CONNECT_FAILED,
        Login_HaveLogined,    // !用户已登录
        DiffStream_HaveStarted,    // !不同类型的码流已经开启,所以会开启失败
        Dsiconnect_No_Relogin,    // !断开, 不自动重新登陆
        Disconnect_AutoReLogin,    // !断开,自动重新登陆
        r_error_monitor_not_login,
    }Result_E;
public:
    virtual ~INetConListener() {}
    virtual void OnMonitor(ulong channel, CPacket* pPacket) = 0;

    // 报警和通道相关的表示具体通道数,无通道相关的值为-1
    // 指示具体的报警类型
    // true-表示相应报警开,false--表示相应报警关
    virtual void OnAlarm(int channel, uint type, int state) = 0;
	virtual void OnDefMsg(enum DefEventCode , ulong) = 0;

    // 连接断开, Disconnect_AutoReLogin, Dsiconnect_No_Relogin
    virtual void OnDisconnect(Result_E result) = 0;

    // 发生重连,且重连后处理结果
    virtual void OnReconnect(Result_E result) = 0;
};

typedef enum NET_CLI_PROTO_TYPE
{
    NCPT_Dahua_II=0, // !大华二代协议
    NCPT_Dahua_XTJC=1, // !系统集成协议
    NCPT_Dahua_DSS=2,
    NCPT_Dahua_RTSP=3, //rtsp协议
    NCPT_VS300=4,//VS300协议
    NCPT_Sony_RTSP=5,//sony rtsp接入
    NCPT_Sony_PDT=6,//sony 协议接入
    NCPT_BELL=7,//中星贝尔接入
    NCPT_Huanghe_RTSP=8,//黄河IPC    
    NCPT_General_RTSP=9,//通用rtsp   
    NCPT_Sunell_RTSP=10,//景阳rtsp 云台控制采用特殊
    NCPT_Vivotek_RTSP=11,//vivotek   rtsp接入
    NCPT_ShangYang_RTSP = 12, //!三洋rtsp
    NCPT_SAMSUNG_RTSP = 13, //!三星rtsp
    NCPT_XWRJ_RTSP = 14, //!星网锐捷rtsp
    NCPT_LangChi = 15, //!LangChi ipc接入YAAN
    NCPT_DAHUA_YAAN= 16, //DAHUA实际上是大华，oem产商显示YAAN200W    
    NCPT_V8 = 17, //中星电子V8接入
    NCPT_ONVIF_CLIENT = 18,//!ONVIF rtsp
    NCPT_EXPAND = 19, //扩展的其他类型,直接绑定应用层直接塞数据       
    NCPT_ZK = 20, //中控私有协议接入
    NCPT_NUM
}NET_CLI_PROTO_TYPE_E;

typedef enum tagnet_capture_conntype_t {
    CONN_TYPE_TCP = 0,
    CONN_TYPE_UDP,
    CONN_TYPE_MCAST,
    CONN_TYPE_NUM
}NET_CAPTURE_CONN_TYPE;

class INetCliConfig : public CObject
{
public:
    virtual ~INetCliConfig() {}
    virtual NET_CLI_PROTO_TYPE_E GetType() = 0;
    virtual bool IsValid() = 0;    //!测试配置合法性
    virtual INetCliConfig* clone() = 0;
    virtual bool Compare( INetCliConfig * pThis ) = 0;

    //!TODO: 子类需要重载 == , != 操作符
protected:
private:
};

class CNetCliDahuaII : public INetCliConfig
{
public:
    CNetCliDahuaII();
    CNetCliDahuaII( CNetCliDahuaII& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();
    virtual bool Compare( INetCliConfig * pThis );
    bool operator==( CNetCliDahuaII& ps );

    /**
     * @brief : 返回0-表示成功
     */
//!set
    int SetUserName( unsigned char const *username );
    int SetPassWord( unsigned char const *password );
    int SetRemoteIp( unsigned char const *ip );
    int SetRemotePort( int const port );
    int SetRemotePtzPreset( int iEnable,int iNum);//设置远程预置点使能与预置点号

//!get
    int GetUserName( unsigned char * buf_out, int const buf_size );
    int GetPassWord( unsigned char * buf_out, int const buf_size );
    int GetRemoteIp( unsigned char * buf_out, int const buf_size );
    int GetRemotePort( int& port );
    int GetRemotePtzPreset( int &iEnable,int &iNum);//获取远程预置点使能与预置点号
    int SetDahuaYaanFlag(int iFlag);/* 设置是否是大华的协议1，默认是威乾0的 */
    int GetDahuaYaanFlag();/* 设置是否是大华的协议，默认是威乾的 */

	int EnabeAutoSynTime(bool flag);
	bool bAutoSynTime();
protected:
    int CheckArgument();
protected:
    std::string m_username;
    std::string m_password;
    std::string m_ip;
    int         m_port;
    int         m_iPtzPresetEnable;
    int         m_iPtzPresetNum;
    int         m_iDahuaYaanFlag;
	bool		m_bAutoSyncTime;
private:
};

#ifdef NET_CLIENT_RTSP

class CNetCliRtspCfg_General: public INetCliConfig
{
public:
    CNetCliRtspCfg_General();
    CNetCliRtspCfg_General( CNetCliRtspCfg_General& ps );
    virtual bool operator==( CNetCliRtspCfg_General& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();    
    virtual bool Compare( INetCliConfig * pThis );

    int SetServer(std::string srv_ip, int port);
    int SetURL(std::string url);
    std::string  GetURL();
    std::string GetSrvIP();
    int GetSrvPort();

	int SetAuth(std::string user, std::string passwd);
	std::string GetUser();
	std::string GetPasswd();

	int	DiscardWrongFrame(bool bFlag);
	bool GetDiscardFlag();
protected:
    std::string m_ip;
    int         m_port;
    std::string m_url;
    std::string m_username;
    std::string m_passwd;
	bool m_bDiscardWrognFrame;
};

class CNetCliRtspCfg_Sunell: public INetCliConfig
{
public:
    CNetCliRtspCfg_Sunell();
    CNetCliRtspCfg_Sunell( CNetCliRtspCfg_Sunell& ps );
    bool operator==( CNetCliRtspCfg_Sunell& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();    
    virtual bool Compare( INetCliConfig * pThis );

    int SetServer(std::string srv_ip, int port);
    int SetURL(std::string url);

    std::string  GetURL();
    std::string GetSrvIP();

    int SetAuth(std::string user, std::string passwd);
    std::string GetUser();
    std::string GetPasswd();

    int GetSrvPort();
    int SetPtzControlPort(int iPort);
    int GetPtzControlPort();
    int SetPtzType(int iType);/* 云台控制方式 ,景阳的和黄河的不同*/
    int GetPtzType();

    int SetLocalCh(int iCh);
    int GetLocalCh();

    
protected:

    std::string m_ip;
    int         m_port;
    std::string m_url;
    int         m_iPtzPort;
    int         m_iPtzType;
    std::string m_username;
    std::string m_passwd;    
    int         m_iLocalCh;    
};

#endif

class CNetCliXtjc : public INetCliConfig
{
public:
    CNetCliXtjc();
    CNetCliXtjc( CNetCliXtjc& ps );
    bool operator==( CNetCliXtjc& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();
    virtual bool Compare( INetCliConfig * pThis );

    /**
     * @brief : 返回0-表示成功
     */
//!set
    int SetUserName( unsigned char const *username );
    int SetPassWord( unsigned char const *password );
    int SetRemoteIp( unsigned char const *ip );
    int SetRemotePort( int const port );

//!get
    int GetUserName( unsigned char * buf_out, int const buf_size );
    int GetPassWord( unsigned char * buf_out, int const buf_size );
    int GetRemoteIp( unsigned char * buf_out, int const buf_size );
    int GetRemotePort( int& port );
protected:
    std::string m_username;
    std::string m_password;
    std::string m_ip;
    int         m_port;
private:
};

class CNetCfgOnvif: public INetCliConfig
{
public:
    CNetCfgOnvif();
    CNetCfgOnvif( CNetCfgOnvif& ps );
    bool operator==( CNetCfgOnvif& ps );
    virtual NET_CLI_PROTO_TYPE_E GetType();
    virtual bool IsValid();
    virtual INetCliConfig* clone();    
    virtual bool Compare( INetCliConfig * pThis );

    int SetServer(std::string srv_ip, int port);
    int SetURL(std::string url);
    std::string  GetURL();
    std::string GetSrvIP();
    int GetSrvPort();

	int SetAuth(std::string user, std::string passwd);
	std::string GetUser();
	std::string GetPasswd();
protected:
    std::string m_ip;
    int         m_port;
    std::string m_url;
    std::string m_username;
    std::string m_passwd;
};

#endif /* INETCONLISTENER_H_HEADER_INCLUDED_B67AF698 */
