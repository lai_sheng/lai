#ifndef __IV8CLI_H__
#define __IV8CLI_H__

/* 接口*/
//#include "Foundation/Object.h"
#include "Net/NetClient/INetClient.h"
class CV8;
class IV8Cli : public CObject
{
public:
    IV8Cli();
    virtual ~IV8Cli();
    int SetLocalCh(int iCh);    
    int SetPeer(const char* ip, const int port);
    int SetAuth(const char* user, const char* passwd);
    int SetStreamType(int iStreamType);

public://!长连接操作

    virtual int SendPtzData(int channel,char *pData,unsigned int uiLen);

    virtual int     Login();
    virtual int     Logout();

    virtual int GetParam(int TypeMain, int index, int subIndex,void* buf,  int size );
    virtual int SetParam(int TypeMain, int index, int subIndex,void* buf,  int size );

    virtual int StartMonitor(int channel) ;
    virtual int StopMonitor(int channel) ;
    virtual int AttachMonitor(CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc);
    virtual int DetachMonitor(CObject * pObj, SIG_CLI_MONITOR_DELEGATE pProc);
    virtual int AttachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    virtual int DetachDefDelegate(CObject * pObj, SIG_CLI_Def_DELEGATE pProc ) ;
    
    virtual int AttachDelegateAlarm( CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;
    virtual int DetachDelegateAlarm(CObject * pObj, SIG_CLI_ALARM_DELEGATE pProc ) ;

protected:
   CV8*    m_Cli;

};

#endif
