
/***************************************************************************************
  *
  * VAPTransProtocol.h - Bell Video Server Protocol
  *
  * Copyright (C) 1992-2006, Dahua Technology Co.,Ltd. All Rights Reserved.
  *
  * Description:
  *     视频服务器协议VSP（Video Server Protocol）用于视频服务器与Bell
  *     系统中其它设备的VSP 协议通信的协议解析及处理。
  *
  * Modify:    ChenChengBin 10782 & Create by ChZ  2006-08-05
  *
****************************************************************************************/
#ifndef __VAPTRANSPROTOCOL_H__
#define __VAPTRANSPROTOCOL_H__

#include "ez_libs/ez_socket/ez_socket.h"
#include "APIs/Types.h"
#include "System/Object.h"
#include "MultiTask/Mutex.h"
#include "stdlib.h"
#include <string>

#define MAX_V8_TCP_CONNECT 4   /*前两个连接保留做音频和视频*/

#define VIDEO_TCP_NO  0
#define AUDIO_TCP_NO  1


#define V8_SUCCESS_RET 0
#define V8_FAILURE_RET -1

#define MAX_VIDEO_RECV_BUF_LEN   (512*1024)
#define MAX_RECV_BUF_LEN   (10*1024)

enum V8_TCP_TYPE
{
    V8_TCP_TYPE_ACCEPT,
    V8_TCP_TYPE_CONNET
};
    
typedef struct __tcp_dlg_context_bell
{
    VD_BOOL BAllocFlag;    
    VD_INT32 iState;/* 1 工作态 0 非工作态 */
    VD_INT32 iType;/* V8_TCP_TYPE */

    char *m_pRecvBuf;
    VD_INT32 m_iDataLeft;
    VD_INT32 m_iMsgTotalLen;
    VD_INT32 iRecvType;/* 0,表示还在进行信令交互，1表示裸视频数据 */
    
    /* recv */
    ez_socket_t astSocket;
    struct sockaddr_in RemoteAddr;
    
    /* 上次访问时间 */
    struct timeval lastActiveTime;        // 最后一次接收或发送的时间，用于短连接超时逻辑处理
    VD_INT32  m_iChn;
}TCP_DLG_CONTEXT_BELL;

class CV8;

class CTransV8_TCP :public CObject
{
public:
    CTransV8_TCP(void * arg);
    ~CTransV8_TCP();
    int Send(VD_INT32 iTcpNo,const void * pData, int iLen);
    int Create(ushort port, ushort depth);
    int Select();
    int CloseAll();

    VD_VOID Dump();

    VD_INT32 AllocTcpDlg();

    TCP_DLG_CONTEXT_BELL m_astConn[MAX_V8_TCP_CONNECT];

    VD_INT32 CheckAndRead();
    VD_INT32 CheckAndAccept();
    VD_INT32 OnData(VD_INT32 iTcpNo);
    int ConnectToCenter(char * desip, ushort desport);
    int CloseConnection(VD_INT32 iTcpNo);

    bool SetAstNonblocking(bool bNonBlock, VD_INT32 iTcpNo);
    VD_INT32 CheckConnStatus(void);
    fd_set m_rFds;
    fd_set m_wFds;

    int V8RecvTCPTimeOut(unsigned int socket,char *pBuffer,int size,int time);
    
private:

    VD_INT32 InnerAllocTcpDlg();
    VD_INT32 InnerCloseConnection(VD_INT32 iTcpNo);
    
    int GetHttpTotalLen(char * httpbody);

private:
    CV8     *m_pV8;
    CMutex m_socketMutex;
    int        m_iBellSocketTCP;    
    struct    timeval m_SelectTimev;
    struct sockaddr_in m_RemoteAddr;
    ez_socket_t m_sockfd;
    
    struct timeval m_V8_con_current_time;//用于短连接超时判断
};

class CTransV8_UDP :public CObject
{
public:
    CTransV8_UDP(void * arg);
    ~CTransV8_UDP();

    VD_INT32 Create(ushort usLocalPort,char *serverip, unsigned short serverport);
    VD_INT32 Select();
    VD_INT32 CloseConnection();
private:
    CV8     *m_pV8;
    CMutex m_socketMutex;
    int m_iSock;
    char     *m_pRecvBuf;

};

#endif

