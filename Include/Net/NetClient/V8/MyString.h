/***************************************************************************************
//
// String.h - BELL Video Server Protocol SIP head parse and encapsulation
//
// Copyright (C) 1992-2006, Dahua Technology Co.,Ltd. All Rights Reserved.
//
// Description:
//     负责对字符串进行处理的头定义。
//
// History:
//         Created by ChenChengBin 10782 2007-01-08
//
//
****************************************************************************************/
#ifndef __MY_STRING_H_
#define __MY_STRING_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "httpxml/http/HttpProtocol.h"
#include <string>
#include <stdio.h>

#define BELL_STRING_OK 0
#define BELL_STRING_ERR 1
#define BELL_STRING_OTHER -1

//字符串处理方法类。
class MyString
{
public:
    /* 取出cChar 在pcSource 中第一个索引*/
    static int indexOf(const char* pcSource, char cChar);

    /* 取出cChar 字符在pcSource 中第iNum 次索引*/
    static int indexOf(int iNum, const char* pcSource, char cChar);

    /* 字符串pcStrSub 在pcSource 中第一次出现时的索引*/
    static int indexOf(const char* pcSource, const char* pcStrSub);

    /* 字符串pcStrSub 在pcSource 中第iNum 次出现时的索引*/
    static int indexOf(int iNum, const char* pcSource, const char* pcStrSub);

    /* 将pcSource 字符串转化成大写*/
    static void toUpperCase(char* pcSource);

    /* 将pcSource 字符串转化成小写*/
    static void toLowerCase(char* pcSource );

    /* 取出指定位置的子串*/
    static char* subString(const char* pcSource, int iStart, int iEnd, char* pcStrOut);

    /* 比较两个串是否相等，大小写敏感*/
    static int equals(const char* pcStr1, const char* pcStr2);

    /* 比较两个串是否相等，忽略大小写*/
    static int equalsIgnoreCase(const char* pcStr1, const char* pcStr2);

    /* 将字符串形式的数值转换成数据型*/
    static int str2int(const char* pcSource);

    /* 将数字转成字符串 */
    static char* int2str(int iNum, char *szOut, int iOutLen);
    
    /* 读取cSeperate 分隔的第一个子串*/
    static char* readWord(const char* pcSource, char cSpliter, char *szOut, int iOutLen);

    /* 读取cSeperate 分隔的第iNum个子串*/
    static char* readWord(const char* pcSource, char cSeperate, int iNum, char *szOut, int iOutLen);

    /* 读取pcSource 中cSeperate 后的子串*/
    static char* readValue(const char* pcSource, char cSeperate, char *szOut, int iOutLen);

    /* 读取pcSource 中cSeperate 前的子串*/
    static char* readName(const char* pcSource, char cSeperate, char *szOut, int iOutLen);

    /* 去除pcSource 左边的空格 */
    static char* LTrim(char* pcSource);

    /* 去除pcSource 右边边的空格 */
    static char* RTrim(char* pcSource);

    /* 去除pcSource 两边的空格 */
    static char* LRTrim(char* pcSource);

#if 0    
    /* 读取pcData 中的Http 头部子串*/
    static int getHttpHead(const char* pcData, char* pcStrHead);

    /* 读取pcData 中的Http 包体子串*/
    static int getHttpBody(const char* pcData, char* pcStrBody, int iType = M_POST);

    /* 获取分隔数个数*/
    static int getSpliterNum(const char* pcSource,  char cSeperate);

    /* 替换URL 字符串 */
    static int ChangeURLString(char* pString);
#endif

};

#endif
