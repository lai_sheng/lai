#ifndef __V8_HTTP_H__
#define __V8_HTTP_H__
/*
* Copyright (c) 2009, visiondigi
* All rights reserved.
*
* 文件名称：DH_HTTP.h
* 文件标识：参见配置管理计划书
* 摘　　要：对vd_http的继承，实现vd_http 中没有实现的rtsp一些功能
*
* 当前版本：1.0
* 作　　者：zdx
* 完成日期：2009年12月23日
*
*/

#include "httpxml/http/VD_HTTP.h"


/////////////////////////////////////////////////////////////
class V8HTTPCommon : public CommonLib::HTTPCommon
{

    public:
    
    V8HTTPCommon(CommonLib::MSG_TYPE enType);
    virtual ~V8HTTPCommon(void);
    std::string wwwAuthenticate;

private:

    int parseLine(const std::string& data);/*对扩展字段进行解包*/
    int packetLine(void);/*对扩展字段进行打包*/
    
};

#endif //head
