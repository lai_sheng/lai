
#include "INetClient.h"
#include "V8Tran.h"
#include "MultiTask/Thread.h"
#include "NetCliRtspSunell.h"


typedef struct __V8_FAME_STRUC__
{
    int iHeadLen;
    int iDatalen;
    int iFrameType;
    int iWidth;
    int iHeight;
    int ifps;
}V8_FAME_STRUC;

typedef enum
{
    V8_MONITOR_STATE_STOP,
    V8_MONITOR_STATE_AUTH,
    V8_MONITOR_STATE_MONITORING,
}EN_V8_MONITOR_STATE;

typedef struct{
    int id;
    char time[20]; //0000-00-00 00:00:00
    char alarm_type; //0-SensorAlarm; 1- MD;2-VideoLost
    char channel;
    char status;// 1 - alarm; 0 - clear
}AlarmRecord;

#define MAX_SPS_LEN  128
#define V8_START_UDP_PORT 110

/* 这个命令字只有V8内部定义 */
#define NCL_CFG_TYPE_V8_PTZ             11111


class CV8;


#define V8_BOOK_ALARM_STATE_DISABLE       0
#define V8_BOOK_ALARM_STATE_INIT              1
#define V8_BOOK_ALARM_STATE_SUCESS         2

class CV8PtzAlarm : public CThread
{
public:
    CV8PtzAlarm(void * arg);
    virtual ~CV8PtzAlarm();
    
    int  Start();
    int  Stop();    
    void ThreadProc();
    int  SetAlarmState(int iState);
    int  SendPtzMsg(int channel,char *pData,unsigned int uiLen);
    int  ParsePtzOpt(void* value, int len,std::string &cmd,char *pUrl);
    
private:
    int  ProcessPtzData();
    int  ProcessBookAlarmState();

    typedef std::list< PTZ_MSG_STRUCT > PTZ_MSG_List;
    PTZ_MSG_List m_PtzMsgList;
    CMutex m_list_Mutex;
    PTZ_MSG_STRUCT m_LastPtzMsg;

    int        m_iBookAlarmState;
    CV8     *m_pV8;
};

class CV8 : public CThread
{
public:
    CV8();
    virtual ~CV8();
    
    int  Start();
    int  Stop();
    void ThreadProc();

    int m_ich;
    int m_iRemoteChn;
    SIG_MONITOR* m_pSigMonitor;
    SIG_DEFAULT* m_pSigDef;
    SIG_CLI_ALARM* m_psigAlarm;

    int m_iMontiorState;//!开启视频监视
    int m_iWidth;
    int m_iHeight;

    char m_acIp[32];
    VD_UINT16 m_usPort;
    char m_acName[64];
    char m_acPwd[64];
    int m_iStreamType;
public:
    int ProcUdpAlarmMsg(char *pdata,  int iLen,struct sockaddr_in *pDstAddr);
    int SendPtzData(int channel,char *pData,unsigned int uiLen);

    int ProcVideoData(unsigned char *pdata,  int iLen);
    int ProcAudioData(unsigned char *pdata,  int iLen);
    int ProcMsg(int iDlg,unsigned char *pdata,  int iLen,int iRecvType);
    int ParseVideoHead(char *pdata,  int iLen,V8_FAME_STRUC *pFrame);
    int ProcessFrameData(const unsigned char* pData, int iLen,int iWidth, int iHeight, int iFrameType,int infs);
    int make_dhAudioframe_packet( unsigned char* pInBuf, unsigned int nInSize, CPacket* &pPacket );
    int Parsehead(char *pdata,  int iLen);
    int  SendStartCpatureAuthAgain(int iDlg);
    int StartCpature(int channel);
    int StopCpature(int channel);
    int  StartAudio();
    int  DisconnectCallBack(int iTcpNo);

    int GetParam(int TypeMain, int index,  int subIndex,void* buf,  int size );
    int SetParam(int TypeMain, int index,  int subIndex,void* buf,  int size );

    int SendRecvParamData(int TypeMain,int iType,int index,  int subIndex,void* buf,  int size );

    int SendParamData(int iSock,int TypeMain,int iType, int index,int subIndex,void* buf,  int size);
    int RecvParamDataTimeOut(int iSock,int TypeMain,int iType, int index, int subIndex, void* buf,  int size,int iTimeOut);
    int ParseAuthenticate(std::string &wwwAuthenticate);

    unsigned char *GetSPSStartFromIframe(unsigned char *pdata,  int iLen);
    unsigned char *GetSPSFromIframe(unsigned char *pdata,  int iLen,int *pSpsLen);
    
    std::string m_DigsetNonce;
    std::string m_DigsetRealm;

private:
    VD_BOOL m_BWaitIFrame;
    CTransV8_TCP *m_pTran;
    CTransV8_UDP *m_pUdpTran;
    CV8PtzAlarm *m_pPtzAlarm;
    int m_iAuthTime;
    CMutex m_Mutex;
    unsigned char *m_pSps;
    int  m_iSpsLen;
    
};

