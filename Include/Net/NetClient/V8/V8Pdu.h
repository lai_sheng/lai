
#pragma once

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string>
#include <limits>

//#include "httpxml/tinyxml/tinyxml.h"
//#include "httpxml/Http/VD_HTTP.h"
#include "V8Http.h"

#define VAP_INVALID_VALUE        -2
typedef struct _tagLineNode
{
    std::string szNodeName;
    std::string szContent;
} LINE_NODE;

class CV8Pdu
{
public:
    CV8Pdu();
    ~CV8Pdu();

    int m_iTcpNo;
    
    static CV8Pdu* createPduWith(std::string httpMethod, std::string strCmd);
    static CV8Pdu* createAck(CV8Pdu* pReqPdu,  int iResult = 0);

    /*反系列化xml--->参数*/
    bool parseBody(const std::string strData, size_t iLen);

    /*系列化参数--->xml*/
    std::string packetBody();

    V8HTTPCommon* getHdr(){return m_pHttpHdr;} 
    void setHdr(V8HTTPCommon* pHdr){m_pHttpHdr = pHdr;}

    std::string m_strMethod;    
    bool m_bReq;
    
    //获取A=xx&B=xx...格式下的值,或者A=xx\r\nB=xx...
    std::string getLineData(char * pTagName);
    int getLineData(char* pTagName,int& iValue);
    int getLineData(char * pTagName,char *TagValue,int iValueLen);    

    //添加A=xx格式
    bool addLineData(char* tagName,  char* tagValue, int iLen);
    bool addLineData(char* tagName,  int iValue);
    bool addV8ReqLineData(char* tagString);
    
    /* 从url 内提取出命令,去掉 最后 一个'/' 前面的内容*/
    bool parseCommand(void);
    std::string getCommand(void);
    
private:

    /*    根据内部构造，来实现多层节点元素的定位于查找或者添加*/
    bool parseString(const std::string strNodePath,
        size_t& iStartPos,
        std::string& strNodeName, 
        int& iNodeNum);

    /* 初始化构造私有的包格式*/
    void packetBefore(std::string strCmd,  int iResult = 0, bool bReq = false); 

    /* tagA=xx&tagB=xx,或者tagA=xx\r\ntagB=xx格式包体*/
    std::list<LINE_NODE> m_nodelist; /* A=xx&B=xx 包体内容*/

    std::string m_lineStr;
    std::string m_lineValue;
    
    std::string m_pduStr;
    
    void parseLine(const char* tagSplit="&");
    std::string& packetLine();
    void UrlDecode(std::string& strBuffer);

    V8HTTPCommon* m_pHttpHdr;
};
