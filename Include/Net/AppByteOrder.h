/*!
   实现大小端转换
  */

#ifndef __APPBYTEORDER_H__
#define __APPBYTEORDER_H__

#include "APIs/Types.h"
#include "APIs/DVRDEF.H"
#ifdef WIN32
	#ifndef LITTLE_ENDIAN
		#define LITTLE_ENDIAN 1234
		#define BIG_ENDIAN 4321
		#define BYTE_ORDER LITTLE_ENDIAN 
	#endif
#else
	#include <endian.h>
#endif
/*!
   FlipBytes用于data字节顺序的转换,高字节移到低字节,低字节移到高字节.
*/

template <typename T>
T FlipBytes(int len,T data)
{
	T mem = data;
	
#if(BYTE_ORDER == BIG_ENDIAN )
	int iii = 0;
	unsigned char ch = 0;
	for(iii=0;iii<len/2;iii++)
	{
		ch = ((unsigned char *)&mem)[iii];
		((unsigned char *)&mem)[iii] = ((unsigned char *)&mem)[len-1-iii];
		((unsigned char *)&mem)[len-1-iii] = ch;
	}
#endif
       return mem;
}

/**
 * @descrip:此模板只支持标准变量,其它变量要偏特化实现
 * @param data -short, int, long
 * @return
 */
template <typename T>
T FlipBytesEx(T data)
{
	T mem = data;

#if(BYTE_ORDER == BIG_ENDIAN )
    int len = sizeof(T);
	int iii = 0;
	unsigned char ch = 0;
    for(iii=0;iii<len/2;iii++)
	{
		ch = ((unsigned char *)&mem)[iii];
		((unsigned char *)&mem)[iii] = ((unsigned char *)&mem)[len-1-iii];
	    ((unsigned char *)&mem)[len-1-iii] = ch;
	}
#endif
       return mem;
}

#endif
