#ifndef _INET_H
#define _INET_H

#include "System/Object.h"
#include "Configs/ConfigConversion.h"

/***
网络配置转换接口，用于兼容其它各比较老的分支程序
**/
#define NI_FAIL	-1
#define NI_SUCCESS	0

#define NI_FAIL_PPPOE_ENABLE	-2
class CNetConfigInterFace : public CObject
{
public:
	CNetConfigInterFace(){}
	virtual ~CNetConfigInterFace(){}
	/*返回NULL表示失败返回的指针指向实参*/
	virtual char * 			GetHostName( char * host_name, int& name_len ) = 0;

	/*返回NULL表示失败, 如果参数不为NULL, ,  返回的指针指向实参, 如果参数为NULL的话，
	实现接口的类要为它维护配置内存*/
	virtual CONFIG_NET * 	GetConfigNet( CONFIG_NET * pConfigNet_out =NULL, bool update = false ) = 0;

	virtual int				SaveConfigNet( CONFIG_NET * pConfigNet_in ) = 0;

	/**
	获取当前的网卡名子和网卡数目
	num_in_out: in---想查询的网卡数目， out --查询到的网卡数目
	row_size: 指示每个网卡名存储缓冲区大小
	pNameArray_out --- 存储查询取的网卡名子以二维数组存放字符串
	返回－－0：成功，　-1：失败
	****/
	virtual int				GetEthName( char ** pNameArray_out, int& num_in_out, int row_size ) = 0;
	virtual int				GetIpByName( const char * eth_name, char * ip_out, char * mask_out )=0;
	virtual int				GetGateByName( const char * eth_name,  char * gate_out, int &gate_len )=0;
	virtual int				GetMacByName(const char *pEthName, char *pEthMac, const int iEthMacLen) = 0;

	virtual int				SetIpByName (const char * eth_name, const char * ip_in, const char * mask_in )= 0;

	//!返回NI_FAIL_PPPOE_ENABLE --修改失败原因是pppoe已经开启,不允许设置
	virtual int				SetGateByName( const char * eth_name,  const char * gate_in, const int gate_len )=0;
	virtual int				SetMacByName(const char *pEthName, const char *pEthMac)	=	0;
	virtual int				Dump() = 0;
	
	/// 获取设备当前正在使用的IP地址
	/// 
	/// \param [out] pCurHostIp 设备当前正在使用的IP地址点分字符串
	/// \param [out] pError 获取失败返回的错误码
	/// \retval true  获取成功
	/// \retval false 获取失败
	virtual bool 			GetNetCurrentHostIP(IPDEF *pCurHostIp, int *pError) = 0;
};


class CNetFactory : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CNetFactory);
	CNetConfigInterFace * CreateNet();
};

#define g_NetFactory (*CNetFactory::instance())

#endif

