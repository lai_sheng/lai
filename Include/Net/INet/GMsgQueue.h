#ifndef _GMSGQUEUE_H
#define _GMSGQUEUE_H

#include <list>
#include "APIs/System.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Semaphore.h"
#include "System/pool_allocator.h"

template <class T>
class CGMsgQueue
{
public:
	CGMsgQueue(int size = 1024)
	{
		m_nMaxMsg = size;
		m_nMsg = 0;
	}
	~CGMsgQueue(){};

	VD_BOOL VD_SendMessage( T * msg_tmp )
	{
		CGuard guard(m_Mutex);
	
		if( m_nMsg >= m_nMaxMsg )
		{
			return FALSE;
		}

		try
		{		
			m_msg_queue.push_back(*msg_tmp);
		}
		catch( std::bad_alloc   a)     
		{   
			tracepoint();
			printf("SendMessage error : %s !!!\n", a.what());
			return FALSE;
		}   
		m_nMsg++;

		m_Semaphore.Post();
		return TRUE;
	}
	VD_BOOL VD_RecvMessage( T * msg_tmp, VD_BOOL wait = TRUE)
	{
		if(wait)
		{
			m_Semaphore.Pend();
		}

		CGuard guard(m_Mutex);
		
		if( m_msg_queue.empty() )
		{
			return FALSE;
		}

		assert( m_nMsg );

		if(!wait)
		{
			m_Semaphore.Pend();
		}

		*msg_tmp = m_msg_queue.front();
		m_msg_queue.pop_front();
		m_nMsg--;

		return TRUE;		
	}
	VD_BOOL VD_ClearMessage()
	{
		int n = m_msg_queue.size();
		for(int i = 0; i < n; i++)
		{
			m_Semaphore.Pend();
			m_msg_queue.pop_front();
			m_nMsg--;
		}

		return TRUE;
	}
	int VD_GetMessageCount()	//获取已有的消息数目
	{
		return m_nMsg;
	}
	int VD_GetMessageSize()	//获取消息最大可有多少个结点
	{
		return m_nMaxMsg;
	}
protected:
private:
	int m_nMsg;
	int m_nMaxMsg;
	CSemaphore m_Semaphore;
	CMutex m_Mutex;

typedef std::list<T, pool_allocator<T> > DLGMSGQUEUE;
	DLGMSGQUEUE m_msg_queue;
};

#endif

