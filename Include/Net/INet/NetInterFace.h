/*****
网络配置接口，可以实现新老配置兼容
******/
#include "INet.h"

#ifndef _NETTERFACE_H
#define _NETTERFACE_H

//配置中心管理配置的实现
class CConfigCenterNet : public CNetConfigInterFace
{
public:
	CConfigCenterNet();
	virtual ~CConfigCenterNet();
	/*返回NULL表示失败返回的指针指向实参*/
	char * 					GetHostName( char * host_name, int& name_len ) ;

	/*返回NULL表示失败, 如果参数不为NULL, ,  返回的指针指向实参, 如果参数为NULL的话，
	实现接口的类要为它维护配置内存*/
	CONFIG_NET * 			GetConfigNet( CONFIG_NET * pConfigNet_out =NULL, bool update = false) ;

	int						SaveConfigNet( CONFIG_NET * pConfigNet_in ) ;

	virtual int				GetEthName(  char ** pNameArray_out, int& num_in_out, int row_size ){return NI_FAIL;}
	virtual int				GetIpByName( const char * eth_name, char * ip_out, char * mask_out );
	virtual int				GetGateByName( const char * eth_name,  char * gate_out, int &gate_len ) ;
	virtual int				GetMacByName(const char *pEthName, char *pEthMac, const int iEthMacLen);

	virtual int				SetIpByName (const char * eth_name, const char * ip_in, const char * mask_in );
	virtual int				SetGateByName( const char * eth_name,  const char * gate_in, const int gate_len );
	virtual int				SetMacByName(const char *pEthName, const char *pEthMac)	{return NI_FAIL;}
	virtual int				Dump() ;
	
	virtual bool			GetNetCurrentHostIP(IPDEF *pCurHostIp, int *pError);
private:
	CONFIG_NET 				m_ConfigNet;
};

#endif
