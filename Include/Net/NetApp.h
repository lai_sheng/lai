
#ifndef _NETAPP_H
#define _NETAPP_H

#include <string>
#include <vector>

#include "System/Object.h"
#include "System/File.h"
#include "System/BaseTypedef.h"

#include "Configs/ConfigNet.h"
#include "MultiTask/Timer.h"


#define PATH_PROCNET_TCP "/proc/net/tcp" // netstat 核查的信息
#define CACHED "Cached:"
#define SYN_FLOOD 50
#define TOO_MANY_CONNECTIONS_IN_TIME_WAIT_STATE 200
#define TOO_MANY_CONNECTIONS_NOT_CLOSED 20

#if 0
typedef enum {
    TCP_ESTABLISHED = 1,
    TCP_SYN_SENT,
    TCP_SYN_RECV,
    TCP_FIN_WAIT1,
    TCP_FIN_WAIT2,
    TCP_TIME_WAIT,
    TCP_CLOSE,
    TCP_CLOSE_WAIT,
    TCP_LAST_ACK,
    TCP_LISTEN,
    TCP_CLOSING
}TCP_STATE;
#endif

typedef struct
{
int establisheds;
int syn_sents;
int syn_recvs;
int fin_wait1s;
int fin_wait2s;
int time_waits;
int closes;
int close_waits;
int last_acks;
int listens;
int closings;
}TCP_STATE_STAT;

//列表信息结构定义
typedef struct _IPInfo{
	IPADDR HostIP; 
	IPADDR SubMask;
	IPADDR GateWay;
	VD_BOOL iDHCP;                 //实际dhcp是否生效的状态
}IPInfo; 

class CNetApp : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CNetApp);

	CNetApp();
	~CNetApp();
public:
	//!网络功能启动
	VD_BOOL Start();
	//!网络功能停止
	VD_BOOL Stop();
	//获取和设置网络IP信息
	int GetNetIPInfo(const char *pEthName, IPInfo *pInfo, VD_BOOL update = FALSE);
	//0: 临时IP，不保存到配置中，一般为DHCP的情况, 1: 保持到配置中
	int SetNetIPInfo(const char *pEthName, const IPInfo *pInfo, VD_BOOL update = TRUE);
	//!网络配置更改生效回调函数
	void OnNetCommApp(CConfigNetCommon* pConfigNetCommon, int &ret);
	void OnNetIPFilter(CConfigNetIPFilter* pConfigNetIPFilter, int &ret);
	
#ifdef _NET_USE_DHCPCLIENT
	//!DHCP实际状态回调
	//void OnSetDHCP(VD_BOOL iUseDHCP, const IPInfo *pIpInfo = NULL, const char *pEthName = NULL); //delect langzi 2010-3-24 
	void OnSetDHCP(VD_BOOL iUseDHCP, const IPInfo *pIpInfo = NULL, const char *pEthName = NULL, const unsigned long *pDNS = NULL); //add langzi 2010-3-24
#endif

	bool IsTrustIP(char *ip);
	void SaveOldConfigNet();
	void UpdateIP();
    int onNet(int argc, char **argv);//控制台操作
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	int ReStartWifi();
#endif

#ifndef WIN32
    void checkNetStat();
    int parseTcp(char *line, TCP_STATE_STAT *stat);
    int getTcpConnections(char *file, TCP_STATE_STAT *stat);
#endif    
private:
	IPInfo m_IPInfo;
	//网络IP信息更新,
	CConfigNetCommon m_ConfigNetCommon;
	CConfigFTPServer m_ConfigFTPServer;
	CConfigFTPApplication m_ConfigFTPApplication;
	CConfigNetIPFilter m_ConfigNetIPFilter;
	CConfigNetMultiCast m_ConfigNetMultiCast;
	CConfigNetDDNS m_ConfigNetDDNS;
	CConfigNetAlarmServer m_ConfigNetAlarmServer;
	CConfigNetNTP m_ConfigNetNTP;
	CConfigNetEmail m_ConfigNetEmail;

	CConfigTable	m_configAccessFilter;//added by wyf on 091221

    CTimer          m_TimerCheckNet;     
};

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#define g_NetApp (*CNetApp::instance())

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#endif // _NETCONFIG_H
