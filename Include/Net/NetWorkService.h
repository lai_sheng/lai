#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

#ifndef _GENIESERVERPROC_H
#define _GENIESERVERPROC_H

#include <map>

#ifdef WIN32
typedef void * threadfunc_t;
#elif defined(LINUX)
#include <pthread.h>
typedef void * threadfunc_t;

#endif //WIN32

#include <vector>

//#include "Net/AMessage.h"
#include "System/Object.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"

#include "MultiTask/Timer.h"
#include "Net/NetCore.h"

#include "System/User.h"



typedef enum {
    NET_TRANS_RATE= 1,
    NET_TRANS_QUALITY  = 2
} NetTransferState_t;


#define DEFAULT_BIT_RATE 1600 

typedef struct  _net_global_cfg
{
    int bNetTransPolicy; //!是否使用网络传输策略
    int emNetTransState;//!<传输策略, NetTransferState_t结构包含内容
    int bHaveDisk;//!<好像是有多个磁盘的标志
    int bExtStream;//!有辅码流标志1--表示有辅码流
    int bHighDownload;//!是否使用高速下载的标志1--表示使用高速下载

    unsigned short usDspRate[N_SYS_CH*2];//!<相应的通道的码流速率
    unsigned short usFps[N_SYS_CH*2];    //!<帧率,每秒多少帧
    unsigned short usResolution[N_SYS_CH*2];        //!分辨率

}NET_TRANS_POLICY_CFG;

typedef struct SesionInfo 
{
    CUser *user;
} 
SesionInfo_t;

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

class CNetWorkService : public CThread
{
public:
    
    //PATTERN_SINGLETON_DECLARE(CNetWorkService);

    CNetWorkService();
    virtual ~CNetWorkService();

    int Run(const int argc, char *argv[]);
    int Stop(const int argc=0, char *argv[]=NULL);

    virtual void ThreadProc();

    /*!<获得用户相关信息*/
    struct sockaddr_in *GetRemoteAddr(int iDlgNo);
    struct conn *GetConn(int iDlgNo);
    CUser * GetUser(int iDlgNo);
    int KickOffUser(int iDlgNo);
    int SetBlockPeer(IPDEF ipPeer, uint iPeriod, time_t LastValid = 0);


    static int  NetCoreIoInit(struct conn *c);
    static void NetCoreIoFini(struct conn *c);
    static void NetCoreIoProcess(struct conn *c);

    int onTrans(int argc, char **argv);

    virtual void svc();
    int OnConsoleNetUser(int argc, char **argv);
    void OnHelp( char* opr, char* value1, char* value2);
    
    //网络策略打印参数
    int m_iPrtfperiad;
    bool m_bTransPrint;
	int m_bExitNetThread;/*!<网络模块启停标识*/
	bool m_bIsNetRunning; /*!<此标识防止多个网络模块创建*/
private:

    //int m_bExitNetThread;/*!<网络模块启停标识*/
    //bool m_bIsNetRunning; /*!<此标识防止多个网络模块创建*/
};

class CNetWorkServiceFactory
{
public:
	static CNetWorkService * instance();
};

#define g_Net ( *CNetWorkServiceFactory::instance() )

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#endif // _GENIESERVERPROC_H

