
#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

#ifdef _NET_USE_DHCPCLIENT/*是否使用dhcp*/

#ifndef _CLIDHCP_H
#define _CLIDHCP_H

#if defined(_WIN32)
#include <winsock.h>
#include <io.h>
#include "getopt.h"
#endif

#if defined(VAX)
#include "getopt.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "APIs/DVRDEF.H"
#include "dhcp/dhcp.h"
//#include "DlgDDNSCli.h"

#include "MultiTask/Thread.h"

#include "Net/NetConfig.h"/*协议配置*/
#include "dhcp/dhcp.h"
#include "dhcp/DhcpProtocol.h"

#define DHCP_MAX_NUM	2

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
class CDlgDhcpCli : public CThread
{
public:
    PATTERN_SINGLETON_DECLARE(CDlgDhcpCli);
	
	VD_BOOL Start();
	void ThreadProc();

	/**
	*@description 旧的获取dhcp配置的方法,只能获取eth0网卡的
	*/
	int SaveDHCPConfig(DHCP_PROTO_CONFIG *pDhcpProtoSet, const char *pEthName);	
	int LoadDHCPConfig(DHCP_PROTO_CONFIG *pDhcpProtoSet, const char *pEthName);

	/**
	*@description 新的支持多网卡的获取dhcp配置的方法
	*/
	int SaveDhcp(const char *eth_name, bool enable );
	int LoadDhcp(const char *eth_name, DHCP_PROTO_CONFIG *pDhcpProtoSet);

/**
*@description 用于网络协议字符串的方法
*/
	int SaveDhcpStrNet(const char * dhcp_str_in );
	int LoadDhcpStrNet(char * dhcp_str_out , const unsigned int len);
	int SetWifiStatus(int status){m_iWifiStatus = status;}
private:
	CDlgDhcpCli();
	virtual ~CDlgDhcpCli();
	
	int m_iEnable;
	//CDhcpConfig * m_dhcpConfig;
	CDhcpProtocol * m_dhcp_protocol[2];

	//!所有的dhcp的网卡的数目, 第一个对应一个dhcp协议实例
	CConfigNetCommon m_netConfig;

	int OpenDhcp(CDhcpProtocol * const pdhcp_in);
public:
	int m_iWifiStatus;  //0:未连上 1:连接成功
 }; 


/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
//extern CDlgDhcpCli g_DhcpClient;
#define g_DhcpClient (*CDlgDhcpCli::instance())

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#endif  

#endif

