/*
*
*bug:
*1. DHCP_Release 尚未实现DHCP_AddLease 尚未实现
*/


#ifndef _DHCPPROTOCOL_H_
#define _DHCPPROTOCOL_H_

#ifdef _NET_USE_DHCPCLIENT

#include "dhcp.h"

#define DHCP_OK	0
#define DHCP_ERROR	-1
#define DHCP_ETH_INVALID	-2
#define DHCP_OUT_LEASE	-3	//dhcp地址超出租期

class CDhcpProtocol
{
public:
	CDhcpProtocol(const char * eth_name, const char * host_name);
	~CDhcpProtocol();
	/**
	*@description 取得服务器分配的ip地址
	*@param get_ip: dhcp取得的ip地址
	*@param buf_len: get_in 指向的缓冲区长度
	*@return DHCP_OK: 成功DHCP_ERROR-未知错误
	*DHCP_ETH_INVALID --网卡名无效
	*/
	//int DhcpGetIP( unsigned long &ip_out, unsigned long& mask_out, unsigned long& gate_way_out );//delete langzi 2010-3-24
	int DhcpGetIP( unsigned long &ip_out, unsigned long& mask_out, unsigned long& gate_way_out, unsigned long &server_DNS); //add langzi 2010-3-24 增加DNS

	/**
	*@description 测试dhcp 取得的ip的有效性
	*如果租期快到达会进行续约,如果超期了,
	*会返回失败, 这样就需要重新去取ip地址
	**@return DHCP_OK: 成功DHCP_ERROR-未知错误
	*/
	int DhcpTestValid();

	int DhcpRelease();

	/**
	*@description : 测试dhcp当前有没有正在使用的合法的ip
	*/
	bool DhcpGetValid();
	int GetLease();
protected:
private:
	DHCP_CLIENT_T m_dhcp_client;
	char m_eth_name[256];
	bool eth_name_valid;//!true--网卡名有效false--网卡名无效
#define TEST_ETH_NAME_VALID() if(!eth_name_valid) return DHCP_ETH_INVALID;

	int m_iRawsocket;
	time_t m_starttime;
	bool ip_valid;//true--当前已取到ip,且正在使用中 false--无合法的ip
};

#endif

#endif

