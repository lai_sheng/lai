

#ifndef _CLINTP_H
#define _CLINTP_H

#if defined(USING_PROTOCOL_NTP)


#include "APIs/DVRDEF.H"
#include "APIs/System.h"			/*system time*/

#include "MultiTask/Thread.h"
#include "Configs/ConfigNet.h"
#include "MultiTask/Timer.h"

typedef enum _ZONE_TIME_E
{
	ZT_ZERO_WRAP = 0,	//!0度经线
	ZT_EAST_ONE = 1,
	ZT_EAST_TWO = 2,
	ZT_EAST_THREE = 3,
	ZT_EAST_THREE_THRITY = 4,
	ZT_EAST_FOUR = 5,
	ZT_EAST_FOUR_THRITY = 6,
	ZT_EAST_FIVE = 7,
	ZT_EAST_FIVE_FIFTY = 8,
	ZT_EAST_FIVE_FORTY_FIVE = 9,
	ZT_EAST_SIX = 10,
	ZT_EAST_SIX_SIXTY = 11,
	ZT_EAST_SEVEN =12,
	ZT_EAST_EIGHT = 13,
	ZT_EAST_NINE = 14,
	ZT_EAST_NINE_THRITY = 15,
	ZT_EAST_TEN = 16,
	ZT_EAST_ELEVEN = 17,
	ZT_EAST_TWELVE = 18,
	ZT_EAST_THIRTEEN = 19,
	ZT_WEST_ONE = 20,
	ZT_WEST_TWO = 21,
	ZT_WEST_THREE = 22,
	ZT_WEST_THREE_THIRTY = 23,
	ZT_WEST_FOUR = 24,
	ZT_WEST_FIVE = 25,
	ZT_WEST_SIX = 26,
	ZT_WEST_SEVEN = 27,
	ZT_WEST_EIGHT = 28,
	ZT_WEST_NINE = 29,
	ZT_WEST_TEN = 30,
	ZT_WEST_ELEVN = 31,
	ZT_WEST_TWELVE = 32,
}ZONE_TIME_E;
int GetServerIpAddrFromDns(const char *Server, char *Ipaddr);
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
class CDlgNtpCli : public CThread
{
public:
	//! 单建模式
    PATTERN_SINGLETON_DECLARE(CDlgNtpCli);
	
	CDlgNtpCli();
	virtual ~CDlgNtpCli();

	VD_BOOL Start();
	void ThreadProc();

	int GetNtpZoneIndex( ZONE_TIME_E zone );
	int LoadNtpCfg( char * pBuf, int BufLen );
	int SaveNtpCfg( char * pBuf );
	
	int GetNtpUpdateCount();
	int GetCurrentTimeStamp();	
private:
	////return 0(ok)  -1(fail)

	int TimeWithNtp(char *pServieName, int nPort, int nTimeZone, DAYLIGHT_TIME sDayLight); 

	void AdjustTimeWithNTP(uint arg);
	
	void OnCfgNtpCfg(CConfigNetNTP& cfg_in_tmp, int &ret); 
	CConfigNetNTP m_cCfgNtp;
	time_t m_NtpUpdataTime;

	CTimer m_update_time;


	CMutex	m_Mutex;
	void DaylightCheck();
	VD_BOOL DaylightStart;	//夏令时是否触发
	CTimer m_daylight_time;
	
	int update_count;//ntp更新成功的次数 
	int m_iupdate_time_out;
 };


/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#define g_NtpClient (*CDlgNtpCli::instance())
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#endif
#endif

