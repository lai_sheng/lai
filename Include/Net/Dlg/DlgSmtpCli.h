
#ifndef __SMTP_CLIENT_H__
#define __SMTP_CLIENT_H__

#ifdef _USE_SMTP_MODULE

#include "System/Packet.h"
#include "Configs/ConfigNet.h"
#include "Net/INet/GMsgQueue.h"

#define SMTP_MAIL_SIZE 				(4 * 1024) 


#include "ez_libs/ez_socket/ez_socket.h"
#include "MultiTask/Thread.h"

/*!
	\class CDlgSmtpCli
	\brief 发送邮件类，Esmtp,smtp协议
*/


#include "Configs/ConfigEvents.h"
#include "System/AppEvent.h"			/*事件中心*/

#include "Configs/ConfigNet.h"		/*网络配置*/

typedef struct _SMTP_SNAP_T
{
	CPacket *pBuffer;
	char strName[VD_MAX_PATH];
	unsigned int iLen;
}SMTP_SNAP_T;

typedef CGMsgQueue< SMTP_SNAP_T > CSmtpQueue;

/*!
	\class CDlgSmtpCli
	\brief 发送邮件类，Esmtp,smtp协议
*/
class CDlgSmtpCli : public CThread
{
public:
//! 	发送邮件类对象单建模式
    PATTERN_SINGLETON_DECLARE(CDlgSmtpCli);


//! 	发送邮件类，构造函数
    CDlgSmtpCli();

//! 	析构函数
    virtual ~CDlgSmtpCli();

	//! 	邮件发送模块开始部分，启动线程
    VD_BOOL Start(void);
	//! 邮件发送模块停止线程
	VD_BOOL Stop(void);

	/*!
	\b Description  : 向抓图模块传递图片数据\n
	\b param[in]    : Packet *pBuffer
	\b param[in]    : har *strName
	\b param[in]    : int iLen,此长度为整个图片数据的长度,而不是一个packet的长度
     	\return    
	\b Revisions  : 
  	- 2008-01-21  yangbin  Create
   	 */
      void	OnSnap(CPacket *pBuffer,char *strName, int iLen);

//! 	通知更新Smtp config
//    void SetUpdateSmtpConfig() { m_bUpdateSmtpConfig = TRUE; }

	int ParseString(const char *data, int iSize, CONFIG_NET_EMAIL *cfg);
	int ToString(char *data, int iSize,CONFIG_NET_EMAIL *cfg);
	
//! 	测试发送邮件
    int  TestSendMail(CONFIG_NET_EMAIL *pConfig);

	/*事件处理*/
	void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data = NULL);
	void OnNetConfigMail(CConfigNetEmail* pNetConfigMail, int &ret);
    int OnTestSendMail(CONFIG_NET_EMAIL *pConfig);		
private:

//! 	线程入口函数
    void ThreadProc(void);							

//! 	处理报警消息, 构造邮件内容，发送报警邮件（包括测试）
	void Process();				

//! 	测试邮件配置，使用此测试看是否能发邮件
    void OnTestSendMail();							

//! 	处理报警消息队列，对消息过滤
    void OnHandleAlarm();							
  
    int SendMailAttach(CONFIG_NET_EMAIL *pConfigSmtp,int iChannel, unsigned int iAlarmType,appEventAction action,
							VD_BOOL bHaveAttach,char *strAttachname,char *AttachBuffer,int BufferLen);

	
	int GetMailText( int iChannel, uint action, unsigned int iAlarmType, char * m_szMail ,int buflen);
//! 	从smtp.xxx.xxx.xxx形式获得ip，有待改进
    int SmtpGetHostByName();

    std::string m_strHostIp;/*!< 通过gethostbyname获得的服务器ip */
    VD_BOOL 				m_bUpdateSmtpConfig; 		/*!< 需要更新SMTP配置 */ 

    CMutex 	m_Mutex;					/*!< 对m_bTest加锁 */
    VD_BOOL 				m_bTest; 					/*!< 请求测试的标记 */ 
    //CONFIG_SMTP 		m_TestConfig; 				/*!< 测试的smtp配置 */ 

	
	/*记录最后发送的时间类型*/
	time_t m_iLastSendTime[4]; 
	/*记载同一类型一分钟内已经发送的通道*/
	unsigned int m_HaveSendMask[4];
	/*记录交接时间的掩码*/
	unsigned int m_OldSendMask[4];		
	

    	//外部报警配置
	CConfigNetEmail m_CCfgNetServers;
	CONFIG_NET_EMAIL m_Config;
	CONFIG_NET_EMAIL m_TestConfig; 

	CSmtpQueue	* p_m_snap_smtp_queue;
	int DoSnap( const SMTP_SNAP_T * tmp_in );
	int DoSnap(int iChn );
	int SendSnap(const char * strName);
	CPacket * pSnapBuffer;
	char m_SnapName[32];
};

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#define g_SmtpClient (*CDlgSmtpCli::instance())
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#endif // _USE_SMTP_MODULE 

#endif //__SMTP_CLIENT_H__

