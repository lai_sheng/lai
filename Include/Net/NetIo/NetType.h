#ifndef NITYPE_H__
#define NITYPE_H__

#define SUCCESS_RET             0
#define FAILURE_RET             -1

#define NITRIP()                  printf("%s %d\n", __FILE__, __LINE__)

#define NICHECK_NULL(p)  if(NULL == p){NITRIP(); return;}
#define NICHECK_NULL2(p, ret)  if(NULL == p){NITRIP(); return ret;}

#define NIDELETE_POINT(pPoint) do{                        	      \
	                                 if (pPoint)                  \
	                                 {                            \
	                                     delete pPoint;           \
	                                     pPoint = NULL;           \
	                                 }                            \
                            	  } while(0);

#define MAX_PDU_BUF_LEN (1024 * 64)

#endif

