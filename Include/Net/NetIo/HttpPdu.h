#ifndef __HTTP_NI_PARSE_H__
#define __HTTP_NI_PARSE_H__

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "./HttpProtocol.h"

extern char *g_NIHttpFiled[eHttpNr] ;

class CNIHttpPdu
{
public:
	#define HTTP_FIELD_NUM                32
	#define HTTP_FIELD_LEN                256
    #define HTTP_BODY_MAX_LEN             (1024*32)
	#define HTTP_NULL                     ((char *)0)
	#define HTTP_HEADER_END               "\r\n\r\n"
	#define CRLF                          "\r\n"
	#define LF                            "\n"
	
	//printf("_%d_\n",__LINE__);
    #define HTTP_CHECK_RET(check, act, ret)   \
	        do                                \
	        {                                 \
	            if((check) != (act))          \
	            {                             \
	            	printf("_%d_\n",__LINE__);\
	        	    return (ret);             \
	            }                             \
            }while(0);

	typedef enum
	{
	    RET_OK = 0,
	    RET_ERROR,
	    RET_NO_WHOLE_PACKET,
	    RET_CLOSE_PEER,
	}eHttpRet;
	
public:
    CNIHttpPdu();
    CNIHttpPdu(CNIHttpPdu &pdu);
    virtual ~CNIHttpPdu();
	int Copy(CNIHttpPdu *pdu);
    void Reset(void);
	int IsValid(void);
	char *GetMethod(void);
	sHttpCfg &GetHttpCfg(void);
	int GetLength(void);
	char *GetBody(void);
	int GetBodyLength(void);
	char *GetHeader(void);
	int GetHeaderLength(void);
	char *GetField(eFieldType fieldType);
	int SetField(eFieldType fieldType, char *value, int len);
	int SetField(eFieldType fieldType, char *value);
	int SetBody(char *body, int len);
	int FromStream(char *data, int len);
	char * ToStream( void );
	int SetParsePara(sHttpCfg *cfg);
	void DumpField();

protected:
	
private:
	int ParseHeader(void);
	int ReadName(char *begin,char *pTitle, int len, char *seperate);
	int ReadValue(char *begin, char *pValue, int len, char *seperate, int white);
	int FillField(char *pTitle, char *pValue);
	int Parse(const char* data);
	int Packet();

private:
	int m_Valid;
	char m_Method[HTTP_FIELD_LEN];
	sHttpCfg m_HttpCfg;
	char *m_FieldBuffer;
	int m_Field[eHttpNr+1];
	int m_Pos;
	char *m_Header;
	int m_HeaderLength;
	char *m_Body;
	int m_BodyLength;
	char *m_Stream;
	int m_NeedRePacketFlag;
};

extern int HttpChange(char *src, int srcLen, char *dst, int &dstLen);

#endif
