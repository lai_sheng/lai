#ifndef SIP_PDU_
#define SIP_PDU_

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string.h>
#include <limits>

#include <httpxml/tinyxml/tinyxml.h>
#include <Net/NetIo/HttpPdu.h>


class CSipPdu
{
public:
	#define XML_NODE_LEN 128
	#define MAX_XML_LEN  1024*4

	typedef struct _tagLineNode
	{
	    char szNodeName[XML_NODE_LEN];
	    char szContent[XML_NODE_LEN];
	} LINE_NODE;
public:
    CSipPdu();
    ~CSipPdu();
    static CSipPdu* createPdu();
    static CSipPdu* createAck(CSipPdu* pReqPdu,  int iResult = 0);

    int parseBody(const  char* strData, int len, int &iLen);
    char* packetBody();

    /* 对具体的某个元素值的查找与添加操作*/
    bool changeRoot(char* pTagPath, bool bAddSubRoot = false);
    bool addAttrData(char* tagName,  int iValue);
    bool addAttrData(char* tagName, char* tagValue, int iLen);
    const char *getAttrData(char* pTagName);
    int getAttrData(char* pTagName, int& iValue);
    
    char* getData(char* pTagName);
    int getData(char* pTagName,int& iValue);
    bool addData(char* tagName,char* tagValue, int iLen);
    bool addData(char* tagName,  int iValue);

    CNIHttpPdu* getHdr() { return m_pHttpHdr; }
    void setHdr(CNIHttpPdu* pHdr) { m_pHttpHdr = pHdr; }
    
private:
	void Init(void);
    bool parseXml();
    
	/* 初始化构造私有的包格式*/
    void packetBefore(char* strCmd,  int iResult = 0, bool bReq = false); 
    char* packetXml();
    char* packetLine();

    char* getNode(TiXmlElement* pRootElement, char* pTagName);
    TiXmlElement* addNode(TiXmlElement* pRootElement, const char*  subTitle, const char*  value);
    TiXmlElement* findSubRoot(TiXmlElement* pRootElement, const char* tagName, int iIndex);

    bool parseString(const char* strNodePath,
        size_t& iStartPos,
        char* strNodeName, 
        int& iNodeNum);

    /*  xml包体 */
    TiXmlDocument *m_pXmlDocs;
    TiXmlElement *m_pRootElement;
    TiXmlElement *m_psubRootElement;
    TIXML_STRING m_xmlStr;    /*保存一些临时数据*/

    CNIHttpPdu* m_pHttpHdr;
};
#endif

