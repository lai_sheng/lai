/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * NetConfig.h - _explain_
 *
 * Copyright (C) 2005 Technologies, All Rights Reserved.
 *
 *
 * _detail_explain_.
 * 
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#ifndef _NETCONFIG_H
#define _NETCONFIG_H

#include <string>
#include <vector>
#include "Configs/ConfigNet.h"
#include "System/Packet.h"
#include "System/Object.h"
#include "System/File.h"
#include "System/BaseTypedef.h"
#include "Configs/ConfigEvents.h"
#include "Configs/ConfigLocation.h"

//
// for CONFIG_DIR
//
#ifdef WIN32
#ifndef CONFIG_DIR
#define CONFIG_DIR "Win32/Config"
#endif
#else
#include "config-x.h"
#endif

#ifndef CONFIG_DIR
#define CONFIG_DIR "."
#endif


// file name of net config
char * const DEFAULT_HOST_IP             =  "10.1.23.156";
char * const DEFAULT_SUBMASK             =  "255.255.0.0";
char * const DEFAULT_GATEWAY             =  "10.1.0.2";
const int DEFAULT_MAXCONN             =  10;

extern std::string g_strDefaultHostIp;
extern std::string g_strDefaultNetMask;
extern std::string g_strDefaultGateway;
// 没有配置时是否使用出厂IP
extern int g_iFirstUseDefaultIp;


#define __MAX_LINE_SIZE 512
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
typedef struct __config_net
{
    uchar     Version[8];                // 8字节的版本信息
    char   HostName[16];           // 主机名
    IPDEF  HostIP;                 // IP 地址
    IPDEF  Submask;                // 子网掩码
    IPDEF  GateWayIP;              // 网关 IP
    IPDEF  DNSIP;                  // DNS IP

    // 外部接口
    IPDEF      AlarmServerIP;          // 报警中心IP
    ushort AlarmServerPort;        // 报警中心端口
    IPDEF      SMTPServerIP;           // SMTP server IP
    ushort SMTPServerPort;         // SMTP server port
    IPDEF      LogServerIP;            // Log server IP
    ushort LogServerPort;          // Log server port

    // 本机服务端口
    ushort HttpPort;               // HTTP服务端口号
    ushort HttpsPort;              // HTTPS服务端口号
    ushort TCPPort;                // TCP 侦听端口
    ushort        TCPMaxConn;             // TCP 最大连接数
    ushort SSLPort;                // SSL 侦听端口
    ushort UDPPort;                // UDP 侦听端口
    IPDEF      McastIP;                // 组播IP
    ushort McastPort;              // 组播端口

    // 其他
    uchar    MonMode;                // 监视协议 TCP|UDP|MCAST <--> 0|1|2
    uchar    PlayMode;               // 回放协议 TCP|UDP|MCAST <--> 0|1|2
    uchar    AlmSvrStat;             // 报警中心状态 <开/关>
    

}CONFIG_NET;

typedef struct _config_net_vsp//实现第三方介入的网络配置
{
    ushort Enable;
    ushort CompanyId;
    IPDEF ServerIp;
    ushort ServerPort;
    ushort reserved;
    char UserName[24];
    char PassWords[32];
    char DeviceId[32];
    ushort reserved2[36];
}CONFIG_NET_VSP;


typedef struct _config_dsp_bitrate
{
    uint DspBitRateUp;
    uint Reserved[7];
}CONFIG_DSP_BITRATE;

typedef struct __sniffer_config_net
{
    IPDEF        SnifferSrcIP;            //抓包源地址
    ushort    SnifferSrcPort;            //抓包源端口
    IPDEF        SnifferDestIP;            //抓包目标地址
    ushort    SnifferDestPort;        //抓包目标端口
} CONFIG_SNIFFER;

#if defined(FUNC_FTP_UPLOAD)
#define    MAX_FTP_USERNAME_LEN    64
#define    MAX_FTP_PASSWORD_LEN    64
#define    MAX_CHANNEL_NUM    16
#define    DAYS_WEEK            7
#define    TIME_SECTION    2

typedef struct __MD_ALARM_FTP_Set
{
    struct
    {
        TSECT    m_tSect;
        int        m_MdEn;// 动态检测
        int        m_AlarmEn;//外部报警
        int        m_GeneralEn;//普通录像
        int        m_Rev[4];
    } m_Period[TIME_SECTION];
} CONFIG_MD_ALRAM_FTP_SET;

typedef struct __General_Protocol_FTP_set
{
    int             m_isEnable;       //是否启用
    IPDEF           m_unHostIP;       //主机IP
    ushort          m_nHostPort;      //主机端口
	char			___rev[2];		//对齐保留位
    char            m_cDirName[VD_MAX_PATH];            //FTP目录路径
    char            m_cUserName[MAX_FTP_USERNAME_LEN];    //用户名
    char            m_cPassword[MAX_FTP_PASSWORD_LEN];    //密码
    int             m_iFileLen;                    //文件长度
    int             m_iInterval;                    //相邻文件时间间隔
    CONFIG_MD_ALRAM_FTP_SET    m_stMdAlarmSet[MAX_CHANNEL_NUM][DAYS_WEEK];
    char             m_Rev[128];
} CONFIG_FTP_PROTO_SET;

typedef struct _FTP_SERVER_SET
{
    IPADDR_SERVER   stServer;
    char            cDirName[VD_MAX_PATH];            //FTP目录路径 240
    int             iType;                      //类型，0 录像ftp 1 图片ftp
    int             iFileLen;                    //文件长度               M为单位
    int             iInterval;                    //相邻文件时间间隔    秒为单位  //这个先保留不处理吧
    char            cRev[128];
}FTP_SERVER_SET;

typedef struct _FTP_APP_TIME_SET
{
    TSECT     stSect[DAYS_WEEK][TIME_SECTION];
    ushort    usCh;           //通道号 
    ushort    usType;        //类型，0 录像ftp 1 图片ftp
    ushort    usReserve[24];
} FTP_APP_TIME_SET;

#endif

#if defined(FUNC_WEB_UPLOAD)
#define MAX_URL_LEN                     32
typedef struct __General_Protocol_WEB_set
{
    int     m_iEnable;    // 是否抓图
    int     m_iPeriod;// 抓图周期
    IPDEF m_unHostIP; //主机IP
    ushort m_nHostPort; //主机端口
    int     m_iSecs;
    char    m_URLSta[MAX_URL_LEN];
    char    m_URLImg[MAX_URL_LEN];
    char    userName[16];//预防某些服务器需要验证功能
    char    password[16];
    char    devId[32];// 机器编号
    char    reserved[64];//用于扩展;
}CONFIG_WEB_PROTO_SET;
#endif

#if defined(USING_PROTOCOL_NTP)
typedef struct __General_Protocol_NTP_set
{
    uchar        isEnable;                    //是否启用NTP
    uchar            Reserved;
    unsigned short unHostPort;    //NTP服务器默认端口为123，pc机器不用修改
    char            HostIP[256];                //NTP主机IP, 字符串格式
    uint        nUpdateInterval;        //更新时间周期（分钟）
    int        iTimeZone;                //时区在-12~13,保存的都为0-25
}CONFIG_NTP_PROTO_SET;
#endif

#define MAX_TRUST_IP     20
typedef struct __trust_ip
{
    int           enable;                    // 1启用, 0不启用
    IPDEF         ip[MAX_TRUST_IP];
} CONFIG_TRUSTIP;

#define MAX_REMOTE_SERVER     16
//远程服务器的设置
typedef struct __remote_server 
{
    IPDEF       ip;
    ushort      port;
    uchar       reserved[26];
} REMOTE_SERVER_S;

typedef struct __config_decoder //解码器配置
{
    REMOTE_SERVER_S server[MAX_REMOTE_SERVER];
}CONFIG_DECODER;


enum EnMonMode
{
    EnMonMode_Tcp=0,
    EnMonMode_Udp,   
    EnMonMode_MultiCast,
    EnMonMode_digital
};

#define IP4_STR_LEN 15

// 
enum EnMonitorModeSet
{
    // 高性能
    EnMonitorModeSet_HighPerformance = 1,
    // 实时优先
    EnMonitorModeSet_RealTime,
    // 流畅优先
    EnMonitorModeSet_Fluency,
    // 自动处理
    EnMonitorModeSet_Auto,
};

 // 监视状态选择配置
typedef struct __config_monitor_t
{
    uint m_iMonitorModeSet; // EnMonitorModeSet
    uint m_iMonPara[15];
}CONFIG_MONITOR_T;


// 4字节， 24:共有24个条目(连通version条) 02:版本02
char * const NET_CFG_VERSION = "2302";

char * const NET_CFG_HOSTNAME = "VISION-DVR";

char * const NET_CFG_FNAME                 = CONFIG_DIR"/network";

char * const NET_CFG_ITEM_VERSION          = "VERSION"; // 2005-7-21 11:04

char * const NET_CFG_ITEM_HOSTNAME         = "HOSTNAME";
char * const NET_CFG_ITEM_HOSTIP           = "HOSTIP";
char * const NET_CFG_ITEM_SUBMASK          = "SUBMASK";
char * const NET_CFG_ITEM_GATEWAYIP        = "GATEWAYIP";
char * const NET_CFG_ITEM_DNSIP            = "DNSIP";

char * const NET_CFG_ITEM_ALARMSERVERIP    = "ALARMSERVERIP";
char * const NET_CFG_ITEM_ALARMSERVERPORT  = "ALARMSERVERPORT";
char * const NET_CFG_ITEM_SMTPSERVERIP     = "SMTPSERVERIP";
char * const NET_CFG_ITEM_SMTPSERVERPORT   = "SMTPSERVERPORT";
char * const NET_CFG_ITEM_LOGSERVERIP      = "LOGSERVERIP";
char * const NET_CFG_ITEM_LOGSERVERPORT    = "LOGSERVERPORT";

char * const NET_CFG_ITEM_HTTPPORT         = "HTTPPORT";
char * const NET_CFG_ITEM_HTTPSPORT        = "HTTPSPORT";
char * const NET_CFG_ITEM_TCPPORT          = "TCPPORT";
char * const NET_CFG_ITEM_TCPMAXCONN       = "TCPMAXCONN";
char * const NET_CFG_ITEM_SSLPORT          = "SSLPORT";
char * const NET_CFG_ITEM_UDPPORT          = "UDPPORT";
char * const NET_CFG_ITEM_MCASTIP          = "MCASTIP";
char * const NET_CFG_ITEM_MCASTPORT        = "MCASTPORT";

char * const NET_CFG_ITEM_MONMODE          = "MONMODE";
char * const NET_CFG_ITEM_PLAYMODE         = "PLAYMODE";
char * const NET_CFG_ITEM_ALMSVRSTAT       = "ALMSVRSTAT";
char * const NET_CFG_ITEM_USEDHCP          = "USEDHCP";

typedef struct __sysattr_ex_t
{
     unsigned char iVideoInCaps;     // 视频输入接口数量
     unsigned char iVideoOutCaps;     //视频输出接口数量
     unsigned char iAudioInCaps;     // 音频输入接口数量
     unsigned char iAudioOutCaps;     // 音频输出接口数量

     unsigned char iAlarmInCaps;     // 报警输入接口数
     unsigned char iAlarmOutCaps;    // 报警输出接口数
     unsigned char iDiskNum;          // 实际使用硬盘数 sdk的byDiskNum
     unsigned char iAetherNetPortNum;// 网络接口数sdk的iIsMutiEthIf

     unsigned char iUsbPortNum;          // USB接口数
     unsigned char iDecodeChanNum;     // 本地解码(回放)路数
     unsigned char iComPortNum;      // 串口数
      unsigned char iParallelPortNum;   // 并口口数

     unsigned char iSpeechInCaps;     // 对讲输入接口数量
     unsigned char iSpeechOutCaps;     // 对讲输出接口数量
     //unsigned char bRes[2];   // 保留字
     unsigned char ucSupportEx;   // 其他支持项目，按位操作
                                       //第0位 1支持菜单输出控制  0 不支持菜单输出控制（98支持，97以及其他产品不支持）
                                       //第1位，1支持分辨率输出切换 0 不支持分辨率输出切换（97，98，解码器支持，其他产品不支持）
     unsigned char ucReslution;   //按位支持分辨率的能力，0 1920*1080 1 1280*1024 2 1280x720 3 1024*768   4 800*600   与 ucSupportEx 组合使用 
}SYSATTR_EX_T;

typedef struct _AUTH_USER_
{
  char strUsr[64];
  char strPwd[64];
}AUTH_USER;


void GetSysAttrEx(SYSATTR_EX_T* psys_attr);

typedef  struct _decode_ability_    //解码器
{
    ushort usDecoderNums ;  //解码器最大路数

    //支持多种网络模式 每一位标识是否支持 0标识不支持，1标识支持
    // 第0位 通用网络vs100，vs200；默认此为支持 置1
    // 第1位 支持rtsp Client
    //  第2位 支持vs300；
    // 第3位 sony rtsp
    // 第4位黄河rtsp
    // 第5位 BELL中星贝尔接入
    // 第6位 VIVOTECK接入    
    // 第7位 三洋接入    
    // 第8位 三星接入    
    // 第9位 星网锐捷接入    
    ushort  usNetMode;

    //每路解码器解码能力 ，现有最大16通道，32位每2位标识一个通道
    // 00 最大支持CIF，01 最大支持D1，10，最大支持720p, 11 更高
    int iAbilityMask;

    // 解码器形式， 如果纯解码器，vs100根据需求裁剪页面
    // 0混合dvr（既有模拟也可以数字），1纯解码器（只有数字）
    uchar ucDecoderType;
    
    uchar   ucModeAblity;//// 第0位 是否支持多连接轮巡的方式
    ushort  usMaxChNum;//支持多连接方式时,每路支持的轮巡列表的通道的最大数目
    unsigned int  uiNetMode2;//用掉一个保留位，便于扩展接入类型
    int iRes [2]; //保留
}DECODE_ABILITY;

typedef  struct _auto_register_ability_    //主动注册
{
       ushort usRegServerNums ;  //主动注册服务器个数 现有实现为1个
       ushort  usNetMode;          //主动注册模式
// 支持多种网络模式 每一位标识是否支持 0标识不支持，1标识支持
// 第0位 视频和信令单连接（暂时只支持此种）
// 第1位 多连接（媒体多连接，支持设备主动连接服务器发送视频）
    uchar ucSupportDNSIP;//ip配置是否是域名方式，0不是（以ipxxx.xxx.xxx.xxx形式输入），1是（可以以域名方式输入ip地址，设备端自行作域名解析处理）
    uchar ucRes[15];//保留字利用一个
    //int iRes [4]; //保留
} AUTO_REGISTER_ABILITY;

typedef  struct _ptz_tour_ability_    //云台软巡航
{
       ushort usPresetNums ;  //每个通道预置点最大个数
       ushort  usTourNums;     //每个通道最大巡航轨迹个数
    int iRes [4]; //保留
} PTZ_TOUR_ABILITY;

//增加黑白名单能力集added by wyf on 20100415
typedef  struct _blackwhiteip_ability_    //黑白名单能及集结构体
{
	unsigned short usMaxBlackNums ;  //最大黑名单数目
    unsigned short usMaxWhiteNums ;  //最大白名单数目
	unsigned long Res[12] ;//保留
}BLACKWHITEIP_ABILITY;
//end added by wyf on 20100415


//rtsp 能力集
typedef  struct _rtsp_ability_    //黑白名单能及集结构体
{
	unsigned char ucAbilty;//第1位表示是否支持rtsp实时监视，第2位表示是否支持rtsp录像回放
	                                    //第3位表示是否支持组播
	unsigned char Res[31] ;//保留
}RTSP_ABILITY;

//录像相关能力结构体
typedef struct
{
    unsigned long dwFlag;//第0位表示支持新的“录像控制”协议    第1位表示是否上报最长 预录时间
    unsigned char ucSupportCardRecord;//支持卡号录像，0不支持，1支持
    unsigned char ucRes1[1];
    unsigned short usPreRecordTime;;//预录时间，单位秒
    unsigned char ucRes2[12];
}RECORD_ABILITY;

typedef struct
{
    unsigned char ucChannelCount;//通道数目
    unsigned char ucRes[3];
    unsigned char ucState[128];//第0个到第127个依次为第1到第128通道（0: 为不录像;1: 手动录像;2: 为自动录像）
}RV_RECCTL;  

//end added by wyf on 20100415

typedef struct _savia_ability_    //智能模块
{
    unsigned char ucSaviaNums ;  //savia数目
    unsigned char ucSurportAl;  //支持的算法
    unsigned char ucRes[22];   //保留字
} SAVIA_ABILITY;



//对讲（对讲音频输入+音频输出）
typedef  struct _talk_ability_
{
       uchar ucSurportTalkIn ;
//对讲音频输入（设备存在单独的音频采集） 1:支持：0：不支持
       uchar ucTalkInMask;
//设备音频输入编码格式类型  按位标识是否支持编码格式
//第0位 是否支持g711a， 第1位 pcm8k16b 第2位 g711u等
       uchar ucSurportTalkOut;  //对讲音频输出   1:支持：0：不支持
       uchar ucTalkOutMask;
//设备音频解码格式类型  按位标识是否支持解码格式
 //第0位 是否支持g711a， 第1位 pcm8k16b  第2位 g711u等
} TALK_ABILITY;

//音频输入(有该能力则表示每个通道都有音频输入，伴音混合流，现有设备默认支持g711a)
typedef  struct _audio_ability_
{
    uint uiAudio ;  //0~31为依次表示通道1~通道32有音频输入 1标识有，0标识
    uint uiAudioMask ;
//设备伴音编码格式类型  按位标识是否支持编码格式
//第0位 是否支持g711a， 第1位 pcm8k16b  第2位 g711u等
 } AUDIO_IN_ABILITY;

//网络控制操作能力集
typedef  struct _newnetopr_ability_     
{
	unsigned int uiNetOprAbilty;//按位来标识 第 0位 是否支持录像和图片查询网络新操作协议 1 支持 0 不支持 第1位 是否支持查询日志网络新操作协议 1支持，0不支持
	unsigned int ucRes[3] ;//保留
}NEW_NET_OPR_ABILITY;

typedef  struct _alarm_support_ability_     
{
//按位来标识设备支出的报警类型 ，为了对以前兼容 0 表示支持 1 表示不支持
// 0  alarmin  1 动检 2 丢失 3 遮挡 4 解码 5 网络报警 6
	unsigned int uiAlarmAbilty;
	unsigned int ucRes[3] ;//保留
}ALARM_SUPPORT_ABILITY;


// add by ilena zhou 2010-07-01
//   音量调节能力结构体
typedef struct _rv_audio_ability_
{
    //0~31为依次表示通道~通道音频输入支持音量控制,音频输入"有无"能力集参考AUDIO_IN_ABILITY
    unsigned long uiAudioVolumn ;    
    
    //第0位表示对讲输入音频支持音量控制,对讲音频输入"有无"能力集参考TALK_ABILITY
    //第1位表示对讲输出音频支持音量控制和静音,对讲音频输出"有无"能力集参考TALK_ABILITY
    //第2位表示对讲输入音频支持MIC输入
    //第3位表示对讲输入音频支持Liner输入
    unsigned char ucTalk;          

    //音频输入的声道，1：单声道；2：双声道
    unsigned char ucAudioChannel;    
    unsigned char ucRes;

    //音频输出(对讲输出)的声道，1：单声道；2：双声道
    unsigned char ucAudioOutChannel; 
    ulong uAudioCaps; //!接位表示音频通道是否支持声音, 为和以前兼容, 0表示 支持，1表示不支持
    unsigned long uiRes[13];
}AUDIO_ABILITY;

typedef struct _ABILITY_AlarmLink
{
    /* 消息处理方式,可以同时多种处理方式,包括
    * 0x00000001 - 网络:上传管理服务器
    * 0x00000002 - 录像:触发
    * 0x00000004 - 云台联动
    * 0x00000008 - 发送邮件
    * 0x00000010 - 设备本地报警轮巡
    * 0x00000020 - 设备提示使能
    * 0x00000040 - 设备报警输出使能
    * 0x00000080 - Ftp上传使能
    * 0x00000100 - 蜂鸣
    * 0x00000200 - 语音提示
    * 0x00000400 - 抓图使能*/

    /*当前报警所支持的处理方式，按位掩码表示*/
    uint        dwActionMask;
    uint        reserve[31];
} ABILITY_AlarmLink;

 typedef struct _IVIDEO_ABILITY
{
      int iIVideoMask;
      //支持平台的掩码 第0位 BELL2.10 第1位 HXHT 第2位 VS300 第3位 神眼.第7位 HTTP_VS 第8位 BELL3.0 可能设备支持多个平台接入
      int iRes[3];
}IVIDEO_ABILITY;

typedef struct _NET_WRIELESS{
      int iWrielessMask;
      //设置1支持3G，0不支持
      int iRes[3];
}NET_WRIELESS;


#define N_SPLIT_COMINE_NUMS 32 //通道组合形式最大数目

/*多画面模式*/
typedef struct _SPLIT_MODE
{
    uint iSplitMode;
    //支持多种画面预览模式，按位标识，参考枚举split_combine_t
    // 第0位 SPLIT1 此默认都支持，此为单画面，全屏
    // 第1位 SPLIT2
    //  第2位 SPLIT4
    // 第3位 SPLIT8
    //  第4位 SPLIT9
    // 第5位 SPLIT16
    //  第6位 SPLITIP
    // 第7位 SPLIT6
    //  第8位 SPLIT12
    //9	SPLIT13	
   //	10  SPLIT20,
   //11   SPLIT25,
   //	12   SPLIT33,
   //	13   SPLIT5,
    
}SPLIT_MODE;

/*通道组合模式*/
typedef struct _Split_Combine_Ch
{
    uint iChMask; //通道掩码，按位标识，总32位4字节,最大32种形式
    /* 例如 4画面 ，支持 模式（1，2，3，4通道）（1，3，4，5）
    对应的掩码为  0000 0000 0000 0000 0000 0000 0000 1111=0x0F
    0000 0000 0000 0000 0000 0000 0001 1101=0x1D
    如果9画面 支持（1，2，4，5，6，7，8，9，10通道）
    对应的掩码为0000 0000 0000 0000 0000 0011 1111 1011=0x3FB
    此处掩码内容，是需要根据 SPLIT_MODE中uiSplitType来定，现有制定是包含关系还是排除关系
    */

    uchar ucSpecialCh;/*特殊通道（约束为自动排在最前或者大窗口显示）*/
    uchar ucRes[3];

}SPLIT_COMBINE_CH;

typedef struct _Split_Combine
{
    uchar ucSplitMode;/*多画面预览模式，参考枚举split_combine_t*/
    uchar ucSplitType;
    /*支持分屏组合的类型 ，每两位对应画面预览模式iSplitMode一位
    0 全部任意组合，
    如果为任意
    1 包含关系，只有少部分组合组合满足，现有约束最大64个；
    2 排除关系，最大64个排除组合；
    如果是包含或者排除方式，SPLIT_COMBINE数组标示具体信息
    其他 暂无定义，后续扩展使用
    */
    uchar ucCombinNums;
    /*组合中总个数，现有最大N_SPLIT_COMINE_NUMS 32个*/
    uchar ucRes;
    SPLIT_COMBINE_CH stCombineCh[N_SPLIT_COMINE_NUMS];
}SPLIT_COMBINE;

/* 当前UI（VGA，TV）显示的画面预览模式，多少画面，画面通道组合*/
typedef struct _Split_Cfg
{
    int stSplitMode;/*多画面预览模式，参考枚举split_combine_t*/
    SPLIT_COMBINE_CH stCombine; /*通道组合模式*/
}DISPLAY_SPLIT_CFG;//260字节

typedef struct _tour_cfg_
{
    uchar bTourEnable;  /*使能 */
    uchar ucRes;
    ushort usIntervalTime;  /*多画面巡航时间间隔 5~120s*/
}DISPLAY_TOUR_CFG;

 //DISPLAY_SPLIT_CFG stTour[N_SPLIT];
 /*多画面模式个数，巡航根据能力集SPLIT_MODE来确定最大暂定为N_SPLIT  = 10*/

typedef TConfig<DISPLAY_SPLIT_CFG, 1, 4> CConfigSplitCombine;
//抓拍能力集
enum SNAP_TYPE
{
    SNAP_TYPE_TRIGGER = 0,
    SNAP_TYPE_TIMER= 1,
    SNAP_TYPE_ALAEM= 2,
};
typedef struct _ABILITY_SNAP
{
    //!按位表示 0-触发抓拍, 1-定时抓拍, 2-告警
    uint type_mask;

    uint uiSnapMaxCount;/*最大支持抓拍张数*/

    uint uiCountsPerSecond;

    /*!<1s几张图片，按位表示

    第0位 1s1张

    第1位 1s2张

    第2位 1s3张

    第3位 1s4张

    第4位 1s5张

     */

    uint uiMaxSeconds;/*最大支持几秒1张,默认120*/
    uint uiMinSeconds; /*最小支持几秒1张,默认2*/
    uint uiRes[27];
}ABILITY_SNAP_T;

//编码能力集
typedef struct  _ENCODE_CAPS_NET
 {
    uint uiMaxEncodePower;  
    //产品支持的最高编码能力

    ushort usSupportChannel;  
    //- 每块 DSP 支持最多输入视频通道数 
    ushort usChannelSetSync; 
    //- DSP 每通道的最大编码设置是否同步 0-不同步, 1-同步

    //每一通道支持码流情况
    uchar ucVideoStandardMask;
    //视频制式掩码，按位表示设备能够支持的视频制式 0 PAL 1 NTSC
    uchar ucEncodeModeMask; 
    //编码模式掩码，按位表示设备能够支持的编码模式设置 ，0 VBR，1 CBR

    ushort usStreamCap;  
    //按位表示设备支持的多媒体功能，
    //第一位表示支持主码流
    //第二位表示支持辅码流1
    //第三位表示支持jpg抓图，捉图功能另外定义，在此不加入

    uint uiImageSizeMask;   

    //主码流编码掩码 枚举按照capture_size_t 

    uint uiImageSizeMask_Assi[32];     

    //主码流取不同的编码时候，辅码流支持的编码格式，例如, 

    //uiImageSizeMask_Assi【0】表示主码流是CAPTURE_SIZE_D1时，辅码流支持的编码格式，

    //uiImageSizeMask_Assi【1】表示主码流是CAPTURE_SIZE_HD1时，辅码流支持的编码格式。。。

    uchar ucSupportPolicy;  
    //是否支持特殊编码策略 0 不支持 1 支持

    uchar ucCompression;//按位表示，第0位表示是否支持264，第1位表示是否支持svac，第二位表示支持265
	unsigned char   ucRebootType; //如果等于1时，配置主码流HD1，辅码流CIF需要重启设备
    uchar  ucEncoderDetect;  //码流检查
    uchar ucRes[8];   //保留

 } ENCODE_CAPS_NET;

 //!总大小1024
 typedef struct  _ENCODE_CAPS_NET2
 {
	 //!坐标索引为enum capture_size_t，每个分辨率支持的辅助码流(接位表示),
	 //!bit:0 -CHL_2END_T
	 //!bit:1 -CHL_3IRD_T
	 //!bit:2 -CHL_4RTH_T
	 //!bit:3 -CHL_5FTH_T
	 uint ImageSizeMask_Stream[64];
	 uchar rev[768]; //!保留1008个保留位
 }ENCODE_CAPS_NET2;


typedef struct  tagALARM_GUARD
{
    int iChannel;

    int iEnable;  //告警布防撤防 0撤防，1布防

    //通用配置信息，具体定义如下
    //告警输入标识传感器类型常开 or 常闭；0常开，1常闭
    //视频丢失和动检为灵敏度；0~6档次，6档次为最灵敏
    //磁盘为硬盘剩余容量下限, 百分数
    int iGeneral;
}ALARM_GUARD;


enum
{
    MAX_PARTITIONS = 4,
};

typedef struct _NET_DISK_PARTION_INFO_
{
    unsigned char ucUse;         //此分区是否有效
    unsigned char ucPartion;    //分区号；
    unsigned char ucFSType;

    //分区支持的文件系统

    // 0 WFS文件系统  现有最大支持2分区，且分区又约束如下三种方式

    // WFS2分区的话，默认第1分区为快照分区，第2分区为录像分区

    //  如果是WFS 1分区的话，根据ucPartType决定

    //  ucPartType==0，录像分区；ucPartType==1快照分区

    //  此时快照（存储图片）后者录像，设备只能进行一种媒体的存储

    //  WFS文件系统约束只作百分比2分区模式

    // 1 FAT32

    // 2 EXT3

    unsigned char ucPartType;

    //WFS文件系统分区专用 0 录像分区  1快照分区；标准文件系统，不需要，无限制

    uint uiTotalSpace; ///< 总容量，其单位参考ucSpaceType说明

    uint uiRemainSpace; ///< 剩余容量，其单位参考ucSpaceType说明

     unsigned char ucSpaceType;   

    //标识硬盘容量单位，方便比较准确的统计

    //  后4位（7  6 5 4 ） 总大小单位  0 KB，1MB，2GB，3TB

    //  前4位（3 2 1 0） 剩余小单位  0 KB，1MB，2GB，3TB

    unsigned char ucRes[3];//保留

}NET_DISK_PARTION_INFO;

typedef struct _NET_DISK_INFO_
{
    unsigned char ucDiskNo;          //盘符,第几个硬盘 最多255个

    unsigned char ucCurrentUse;     //是否当前使用工作盘

    unsigned char ucLocalStorage;  //本地存储还是远程存储介质 0本地 1远程

    unsigned char ucDiskType;       

    //硬盘介质类型 0 sata硬盘 1，U盘，3 sd卡，4，移动硬盘，5，dvd光盘

    unsigned char ucErrorFlag;      //工作盘错误标志

    unsigned char ucPartMethod;      

    //支持分区模式 

    // 第0位表示是否支持分区 0 不支持 1支持

    //  后七位 按位标识支持分区方式 

    //  第1位 按照百分比分区 0 不支持 1支持

    //   第2位 按照容量分区 MB  0 不支持 1支持

    //   第3位 按照容量分区 GB  0 不支持 1支持

    //   第4位 按照容量分区 TB 0 不支持 1支持

    unsigned char ucSupportPartionMax;         //支持硬盘最大分区个数

    unsigned char ucSupportFileSystem;      

    //支持的文件系统0 WFS 1 FAT32 2 EXT3 安位表示

    uint uiTotalSpace; ///< 总容量，其单位参考ucSpaceType说明

    uint uiRemainSpace; ///< 剩余容量，其单位参考ucSpaceType说明

     unsigned char ucSpaceType;   

    //标识硬盘容量单位，方便比较准确的统计

    //  后4位（7  6 5 4 ） 总大小单位  0 KB，1MB，2GB，3TB

    //  前4位（3 2 1 0） 剩余大小单位  0 KB，1MB，2GB，3TB

    unsigned char ucAttr;       //0 可读写 （默认），1只读， 2 冗余备份
    unsigned char ucRes[2];

    NET_DISK_PARTION_INFO szPartion[4];

    unsigned char ucRes2[64];

}NET_DISK_INFO;
 

//硬盘操作

typedef struct _NET_CTRL_DISK_INFO_
{
    unsigned char ucDiskNo;         //盘符,第几个硬盘 最多255个

    unsigned char ucCtrlType;       

    //0 可读写 （默认），1只读， 2 冗余备份 ，3 格式化 

    ///4 扇区检测错误恢复

    // 5重新分区，重新分区后面数据才有效

    unsigned char ucRes[2];  

    /////重新分区有效数据----------------------

    unsigned char ucFSType;

    // 0 WFS文件系统  现有最大支持2分区，且分区又约束如下三种方式

    // WFS2分区的话，默认第1分区为快照分区，第2分区为录像分区

    //  如果是WFS 1分区的话，ucPartType标示录像分区和快照（存储图片）

    //  此时后者录像，设备只能进行一种媒体的存储

    //  WFS文件系统约束只作百分比2分区模式

    // 1 FAT32

    // 2 EXT3

    unsigned char ucPartType;         

    //WFS文件系统分区专用 只有一个分区的时候才使用 0 录像分区  1快照分区

    //WFS  两分区时候，默认第1分区为快照分区，第2分区为录像分区，此值无效

    unsigned char ucPartNum; //分区个数

    unsigned char ucPartMethod;

    //硬盘分区方式， 0 百分比，1 KB 2MB 3 GB 4 TB

    unsigned int uiPartSpace[4];  

    // 按照分区方式，依次分区所使用的容量

    //如果按照百分比，例如 50%-50% 2分区， 

    //内部填充50，50, 0, 0；ucPartCtrlType== 2 ，ucPartNo ==2

    //如果按照容量MB分区方式，例如1G的硬盘，200MB-300MB-100MB-400MB，

    //那么依次填充 200，300，100，400；ucPartCtrlType== 2 ，ucPartNo ==4

    uint uiRes[6];  

} NET_CTRL_DISK_INFO;


//硬盘分区进行格式化以及修复操作

typedef struct _NET_CTRL_DISK_PARTION
{
    unsigned char ucDiskNo;          //盘符,第几个硬盘 最多255个

    unsigned char ucPartCtrlType;   

    //0 格式化，1分区扇区检测修复

    unsigned char ucPartNo;       

    //ucPartCtrlType==0、1，对第ucPartNo个分区进行格式化、扇区检测修复；

    unsigned char ucRes;

    unsigned int uiRes[7];       //保留

} NET_CTRL_DISK_PARTION;



/**
 * \brief 告警
 */
typedef struct  tagDEV_CHANNEL
{
    unsigned short usTotal;  //总通道数目
    unsigned short usIndex;  //第几个通道信息
}DEV_CHANNEL;
typedef struct  tagDEV_PTZ_CHANNEL
{
	unsigned short usChannel;  //通道，从 0 开始 
	unsigned short usIndex;  //预置点或者轨迹下标号，从1开始
}DEV_PTZ_CHANNEL;


typedef struct tagQueryLog
{
	uchar ucQueryType;/*查询方式0 使用原有dh方式查询，1后续考虑新的方式吧，暂时先不实现*/
	uchar ucLogType;/*日志类型*/
	/* 使用dh方式查询
	0 全部1 系统操作2 配置操作3 数据管理
	4 报警事件5 录像操作6 用户管理7 日志清楚8 文件操作
	*/
	uchar ucRes[2];
	SYSTEM_TIME stStartTime;
	SYSTEM_TIME stEndTime;
	uchar ucRes1[16];
}NET_QUERY_SYSYTEM_LOG;


typedef struct tagNetLogInfo
{
	ushort    usType;                      /*日志类型*/
	uchar    ucFlag;        //标记, 0x00 正常，0x01 扩展，0x02 数据
	uchar    ucData;        //数据
	//uchar    ucRes;
	SYSTEM_TIME stTime;			/* 日志时间 */
	uchar    ucContext[8]; 		//内容
	uchar 	 ucRes1[16];
}NET_LOG_INFO;

typedef struct tagNetOprLog
{
	uchar ucOprType;/*0 日志（一次性）清除  1 日志规则查询 2 日志规则设置*/
	uchar ucFullPolicy; //日志满覆盖还是停止 0满覆盖 1满停止
	uchar ucRes[2];
	uint uiIfRegLog[LOG_TYPE_NR];/*是否记录log*/
	uint uiRes[3];
}NET_OPR_SYSYTEM_LOG;


//! 事件处理结构
typedef struct tagEVENT_HANDLER_NET
{
    uint    dwRecord;                /*!< 录象掩码 */
    int        iRecordLatch;            /*!< 录像延时：10～300 sec */
    uint    dwTour;                    /*!< 轮巡掩码 */
    uint    dwSnapShot;                /*!< 抓图掩码 */
    uint    dwAlarmOut;                /*!< 报警输出通道掩码 */
    int        iAOLatch;                /*!< 报警输出延时：10～300 sec */
    PTZ_LINK PtzLink[16];        /*!< 云台联动项 */

    VD_BOOL    bRecordEn;                /*!< 录像使能 */
    VD_BOOL    bTourEn;                /*!< 轮巡使能 */
    VD_BOOL    bSnapEn;                /*!< 抓图使能 */
    VD_BOOL    bAlarmOutEn;            /*!< 报警使能 */
    VD_BOOL    bPtzEn;                    /*!< 云台联动使能 */
    VD_BOOL    bTip;                    /*!< 屏幕提示使能 */
    VD_BOOL    bMail;                    /*!< 发送邮件 */
    VD_BOOL    bMessage;                /*!< 发送消息到报警中心 */
    VD_BOOL    bBeep;                    /*!< 蜂鸣 */
    VD_BOOL    bVoice;                    /*!< 语音提示 */
    VD_BOOL    bFTP;                    /*!< 启动FTP传输 */

    int        iWsName;                /*!< 时间表的选择，由于时间表里使用数字做索引，且不会更改 */
    uint    dwMatrix;                /*!< 矩阵掩码 */
    VD_BOOL    bMatrixEn;                /*!< 矩阵使能 */
    VD_BOOL    bLog;                    /*!< 日志使能，目前只有在WTN动态检测中使用 */
    int        iEventLatch;            /*!< 联动开始延时时间，s为单位 */
    VD_BOOL    bMessagetoNet;            /*!< 消息上传给网络使能 */
    uint    dwReserved[7];             /*!< 保留字节 */
} EVENT_HANDLER_NET;

typedef struct tag_NET_EVENT_HANDLER
{
    int iChannel;
    EVENT_HANDLER_NET stEventHandler;
} NET_EVENT_HANDLER;


////////////////////通道编码相关/////////////////////////
typedef struct _ChannelName_
{
    int iChannel;
    char strChannelName[CHANNEL_NAME_SIZE]; 
}CHANNEL_NAME;

typedef struct _MutlineTitle_
{
    int iChannel;
    char strMutlineTitle[MUTLINT_TITLE_SIZE]; 
}MUTILE_TITLE;

//! 编码配置的类型

enum ENCODE_TYPE_BY_SUBSTREAM
{
    SUBENCODE_TYPE_NUM0 = 0,
    SUBENCODE_TYPE_NUM1 = 1,
    SUBENCODE_TYPE_NUM2 = 2,
    SUBENCODE_TYPE_NUM3 = 3,
    SUBENCODE_TYPE_NUM = 4,
};


void EVENT_HANDLER_NET_2_EVENT_HANDLER(EVENT_HANDLER_NET& ehn, EVENT_HANDLER& eh);
void EVENT_HANDLER_2_EVENT_HANDLER_NET(EVENT_HANDLER& eh, EVENT_HANDLER_NET& ehn);

#define NET_N_UI_TSECT 6
#define NET_N_WEEKS 7

typedef struct tagNETSECTION
{
    int     enable;             //!使能
    uchar     startHour;             //!开始时间:小时
    uchar    startMinute;        //!开始时间:分钟
    uchar    startSecond;         //!开始时间:秒钟
    uchar    endHour;             //!结束时间:小时
    uchar    endMinute;         //!结束时间:分钟
    uchar    endSecond;         //!结束时间:秒钟
    uchar   ucReserved[2];    //保留
}NETSECTION;

typedef struct tagNET_WORKSHEET
{
    int  iChannel;
    int            iName;
    NETSECTION    tsSchedule[NET_N_WEEKS][NET_N_UI_TSECT];    /*!< 时间段 */
} NET_WORKSHEET;

typedef struct
{    
    int  iChannel;
    unsigned int uiRowColNum;//高16为表示行数，低16位表示列数
    int        iRegion[MD_REGION_ROW];    //每行使用二进制串
}NET_REGION;

#define RED_ON_REGION_NUM 8 

typedef struct  tagNET_REDON_RECT
{
	unsigned short usChannel;
	unsigned short  usRectNum;  /*!< 当前该通道有几个区域 */
	// VD_RECT      szRect[RED_ON_REGION_NUM];
	ANALYSE_REGION szRect[RED_ON_REGION_NUM];
	unsigned int uiRes[4];

} NET_REDON_RECT;

typedef struct tagCONFIG_SNAP_NET
{   
    int iChannel;
    /*定时抓拍使能*/
    int Enable;
    
    /*抓拍图像质量，初步分6档， 1 差 2 一般 3普通， 4好， 5很好， 6最好*/
    int SnapQuality;

    /*!<抓图策略

    usSnapMode 抓拍模式， 0 表示1秒几张模式

                       1 表示几秒1张模式

                       2 表示几分钟1张模式

    usCounts   对应抓拍模式下的数值

    */

    unsigned short usSnapMode;

    unsigned short usCounts;

    /*抓拍张数*/
    int SnapCount;
} CONFIG_SNAP_NET;

//时间能力集
typedef struct  _TIME_ABILITY
{
   //是否支持校时功能
   //按位操作
   //第0位 Dst 支持夏令时
   //第一位 SNTP（udp ：123）， 现在默认是此ntp
   //第二位 NTP（udp：37，tcp：37）
   uchar ucSupTimeAdjust;

   uchar ucRes[11];   //保留字
} TIME_ABILITY;

//夏令时网络配置结构
typedef struct _NET_DST_CFG
{
    DST_POINT iDST [2];
    uchar ucEnable;//0,不使能，1使能
    uchar ucRes[3];//保留 
}NET_DST_CFG;

typedef struct _NET_VIDEO_OUT_OSD_CFG
{
	//0位 是否显示时间标题显示使能(保留和原来功能一样）
    // 1位 通道标题显示使能 (保留和原来功能一样）
    //2位 报警状态显示使能（新增加）
    // 3位 录像状态显示使能（新增加）
    //4位 是否显示数字通道时间标题显示使能（新增加，有数字通道才显示否则不显示）
    // 5位 数字通道通道标题显示使能（新增加，有数字通道才显示否则不显示） 	 
    //6位 数字通道报警状态显示使能（新增加，有数字通道才显示否则不显示）	  
    // 7位 数字通道录像状态显示使能（新增加，有数字通道才显示否则不显示）
    uchar ucOSDShow;
    uchar ucRes[15];//保留
}NET_VIDEO_OUT_OSD_CFG;


//ftp能力集
typedef struct  _FTP_ABILITY
{
    //按位操作
    //第0位 支持ftp录像上传
    //第1位 支持ftp图片上传
    uchar ucSupFtp;

    //保留字 
    uchar ucRes[15];
} FTP_ABILITY;

typedef struct _PTZ_RAW_DATA_S
{
    int  iLen;//实际数据长度
    int  iCh;//通道号
    char acData[32];//实际数据内容
}PTZ_RAW_DATA_S;

//! 普通配置
typedef struct tagNET_GENERAL
{ 
    /*!< 按位掩码形式
    0 硬盘满时处理 1覆盖"OverWrite", 0停止"StopRecord" 
    1 是否静音
    2 LCD屏幕自动关闭
    3 是否TV输出
    4 是否VGA输出
    5 是否HDMI输出
    */
    int  iCtrlMask;
     
    /*!<?本机编号:[0,?998]?*/ 
    int iLocalNo;
     
    //!屏保时间(分钟) [0, 120]
    unsigned short usScreenSaveTime;
     
    //!本地菜单自动注销(分钟) [0, 120]
    unsigned short usAutoLogout;
     
    uchar              ucDateFormat;//日期格式/*!< 日期格式:“YYMMDD”, “MMDDYY”, “DDMMYY” */

     uchar              ucDateSprtr;//日期分隔符/*!< 日期分割符:“.”, “-”, “/” */

    uchar               ucTimeFmt;//时间格式/*!< 时间格式:1:"12";    0: "24" */

    uchar ucRes;
    
    /*设备可以支持的制式 ， 第0位 PAL ，第1位 NTSC， 第2位 SECAM*/
    uchar ucSupportVideoFmt;
    
    /*设备使用的制式 ，0 PAL ，1 NTSC， 2 SECAM*/
    uchar ucCurVideoFmt;

    //uchar ucRes1[2];将此保留字节修改，此部分只有在
    uchar ucMenuLayer;//菜单输出 ，0 HDMI，1 VGA，2 TV，说明此操作仅在98系列上支持，对于97设备默认不处理
    uchar ucResolution;//输出分辨率，0:1920*1080 , 1:1280*1024 , 2:1024*768 , 3:800*600
 
    /*设备支持的语言 ， 

    第0位English ，第1位 SimpChinese， 第2位 TradChinese， 第3位 “Italian”,
    4 “Spanish”, 5“Japanese”, 6“Russian”, 7“French”, 8“German”
     ，9 "PORTUGUê"*/

    uint uiSupportLanguage;

     /*设备当前使用的语言 ，

    0 English，1 SimpChinese，2 TradChinese，3“Italian”,4 “Spanish”, 

    5“Japanese”, 6“Russian”, 7“French”, 8“German”,9"PORTUGUê"
    */
    uint uiCurLanguage;

    uint uiRes[3];//保留字节

} NET_GENERAL;
void CONFIG_WORKSHEET_2_NET_WORKSHEET(int channel, CONFIG_WORKSHEET&cw, NET_WORKSHEET& ncw);
void NET_WORKSHEET_2_CONFIG_WORKSHEET(int channel, NET_WORKSHEET& ncw, CONFIG_WORKSHEET&cw);


typedef struct tagCONFIG_AUDIO_FORMAT_NET
{
  uint uiTalkAudioSourceType;    //!< 声音源方式，0为线性输入，1为mic
  uint uiOutSilence;        ///< 静音 1--静音 0--非静音

  uint uiOutLAudioVolumn;   //左声道音量
  uint uiOutRAudioVolumn;   //右声道音量，单声道的设备左右值一样
  uint uiLongtimeBeepEnable;    ///蜂鸣器长鸣

  uint uiReserverd[31];//保留

}CONFIG_AUDIO_FORMAT_NET;

//控制键盘能力集
typedef  struct _KBD_ability_    
{
    unsigned char ucAbilty;//是否支持控制键盘
    unsigned char ucSupport;//支持的协议:第0位表示云台透明通道,第1位表示大华控制键盘,参考KBDFunc
    unsigned char ucRes[14] ;//保留
}KBD_ABILITY;

//矩阵输入能力集added by wyf on 20100902
typedef struct 
{
    unsigned int uiMatrixInNum;//支持矩阵输入路数，目前为128，不支持星火矩阵的设备，该值为0 
    unsigned int reserve[3];
}TAbbiMatrixXingHuo;

typedef struct _OSD_ZOOM_ability_
{
	//    1表示最大支持1倍缩放，2表示最大支持2倍缩放。目前720P设备为2，其他设备为1
	uchar ucZoomAbility; 
	uchar ucRes[3];
}OSD_ZOOM_ABILITY;

typedef struct _ABILITY_WATERMARK
{
    //!按位表示 标识每个通道的能力
    uint uiChMask;
    uint uiRes[3];/*保留字，便以后扩展*/
    uchar ucMaxCounts[16];/*!<每个最大叠加张数*/
     
    uchar ucRes[16];/*!<保留字，便以后扩展*/ 
}ABILITY_WATERMARK_T;

typedef struct _ABILITY_MUTILETITLE
{
    //是否支持多行osd叠加，1表示支持
    uint uiMutlineAbility;
    uint uiRes[3];/*保留字，便以后扩展*/
}ABILITY_MUTILETITLE_T;

typedef struct _ABILITY_OEM_FUN_
{
    //!按位表示 标识Oem厂商的能力
    uint uiOemMask; // 1位标示明景 bitmsk(0)
    uint uiProduct;  //表示产品类型 
    // 明景	1 高速球	5 云台TC26   	8 TC35
    
    uint uiRes[3];/*保留字，便以后扩展*/

}ABILITY_OEM_FUN_T;

#ifdef KOTI
typedef struct _KOTI_FLOOR
{
	int  iSupportNum; /*支持的楼层数目，读取时有效，配置无效*/
	int  iNum;/*配置的楼层数目*/
	char acFloorName[32][12];/*最多按照32层保留*/
} KOTI_FLOOR;

typedef struct _KOTI_ROOM
{
	int  iSupportNum;/*支持的最大房间数目，读取时有效，配置无效*/
	int  iNum;/*配置的房间数目*/
	char acRoomName[32][12];/*最多按照32个房间保留*/
} KOTI_ROOM;

typedef struct _KOTI_IP_CAM
{
	char acCam_IP[20];/*摄像头IP*/
	char cCam_ID;/*摄像头ID*/
	char cCam_Port; /*视频端口，我们设备即通道号*/
	char acRes[2];/*保留*/
	char acFloor[12]; /*楼层*/
	char acRoom[12]; /*房间*/
	//Char acIPCamName[64];/*摄像头名字，这里不提供了，显示时自己合成*/
} KOTI_IP_CAM;

typedef struct _KOTI_IP_CAM_NAME_CFG
{
	int  iSupportNum;/*支持的最大数目，读取时有效，配置无效*/
	int  iNum;/*摄像头数目*/
	KOTI_IP_CAM stIPCam[32];/*这里最多先按照32路设备实现*/
} KOTI_IP_CAM_CFG;

typedef struct _KOTI_APARTMENT
{
	char acFloorName[12];/*楼层*/
	char acRoomName[12]; /*房间*/
	//Char acName[20]; /*名字自己拼起来*/
} KOTI_APARTMENT;

typedef struct _KOTI_APARTMENT_CFG
{
	int  iSupportNum;/*支持的最大防区数目，读取时有效，配置无效*/
	int  iNum;/*配置的防区数目*/
	KOTI_APARTMENT  stApartment[32];/*最多按照32个防区保留*/
} KOTI_APARTMENT_CFG;

typedef struct _KOTI_ALARM_AREA
{
	char acName[20];/*防区名字*/
	char acAltp[20];/*探头类型*/
	char acAlppt[20];/*性质*/
	char acCamIP[20];/*摄像头iP*/
	int  iCamID1;/*摄像头ID*/
} _KOTI_ALARM_AREA;

typedef struct _KOTI_ALARM_AREA_CONF
{
	int  iSupportNum;/*支持的最大数目，读取时有效，配置无效*/
	int  iNum;/*摄像头数目*/
	char acSupportAltp [96];/*支持的探头类型，目前为：“红外探头|门磁|窗磁|烟感探头|煤气探头|紧急按钮|红外对射|红外光栅”,不同类型用’|’区分*/
	char acSupportAlppt[96]; /*支持的防区性质，目前为：”立即|延时|停用|24小时|联动立即|联动延时”,不同类型用’|’区分*/
	
	_KOTI_ALARM_AREA stAlarmArea[32];/*这里最多先按照32个防区实现*/
} KOTI_ALARM_AREA_CONF;

typedef struct _KOTI_MOBILE_NUM
{
	char acMobileNum[20];/*电话号码*/
	char cVoice;/*电话语音0 不使能1 使能*/
	char cSMS;/*短信 0 不使能1 使能*/
	char cMMS;/*彩信 0 不使能1 使能*/
	char cRes;
} _KOTI_MOBILE_NUM;

typedef struct _KOTI_MOBILE_NUM_CONF
{
	int  iSupportNum;/*支持的最大数目，读取时有效，配置无效*/
	int  iNum;/*配置数目*/
	
	_KOTI_MOBILE_NUM stMobileNum[32];/*这里最多先按照32个电话实现*/
} KOTI_MOBILE_NUM_CONF;

typedef struct _KOTI_SYSINFO
{
	char acProductType[16];/*产品型号，前端上报字符串*/
	char acMac[32];/*有线MAC*/
	char acWireMac[32]; /*无线MAC*/
	char acUID[32];/*16位UID,后几位置0*/
	char acVer[32];/*版本号，小于20位字符串*/
	char acBuildTime[32]; /*编译时间，小于20位字符串*/
} KOTI_SYSINFO;
typedef struct _KOTI_ALARM_TIME
{
	char acServerip[16]; /*服务器IP*/
	uchar ucDelayZoneTime;/*延时防区时间1-255s */
	uchar ucEnalbeDelaytime; /*布防延时时间1-255s */
	ushort usAlarmTime;/*警铃鸣响时间1-30000s*/
	uchar ucRingTime;/*振铃次数,1-10次*/
	uchar ucKeepDay;/*续传保持天数,1-30次*/
	uchar ucKeepCmd;/*续传最大次数,1-100次*/
	char          cMatrix;/*矩阵的类型，-1 空，1，AVS1 其它未知*/
	uchar ucLineCheck;/*电话线检测，0，不使能，1使能*/
	uchar ucCheckPower;/*备用电源检测报警，0，不使能，1使能*/
	uchar ucMMS;/*布撤防短信通知，0，不使能，1使能*/
	char acPhone1[16];/*接警中心电话1*/
	char acPhone2[16];/*接警中心电话2*/
	char acPhone3[16];/*接警中心电话3*/
	char acPhone4[16];/*接警中心电话4*/
	char acRes[32];/*保留*/
} KOTI_ALARM_TIME;

typedef struct _KOTI_PASSWORD
{
	char acPWD1[8];/*两个密码一致才保存成功，密码最长8位*/
	char acPWD2[8];
}KOTI_PASSWORD;

typedef struct _KOTI_RTSP_CONG
{
/*rtsp服务器配置*/
	int iRtspServerEnable;/*是否启用RTSP服务器*/
	int iRtspListenPort; /*RTSP服务器端口*/

/*rtsp客户端配置*/
	int iRtspClientEnable;/*是否启用RTSP服务器 0,不使能 1 使能*/
	int iRtspServerPort; /*远程RTSP服务器端口*/
	char acRtspServerIP[16]; /*远程RTSP服务器IP*/
}KOTI_RTSP_CONG;

#endif
typedef struct _MANUAL_SNAP
{
	int Channel; /*指定抓拍通道*/
	int  Count;
	char acTransferNet; /*是否即时附带图片数据给网络，1需要设备端返回捉图数据 0不需要设备端返回捉图数据*/
	char acRes[15];/*保留*/
}MANUAL_SNAP;

typedef struct tagNetFileInfo
{
	uchar ucFileType; /* 0 录像  1 图片 */
	uchar ucSubType; /* 子类型0 全部1 外部报警2 动检（包括视频遮挡视频丢失动态检测）3 所有报警（包括1，2类型外还包括异常报警，硬盘满等）4 手动录像 5定时录像 6卡号录像*/
	uchar ucChannel; /* 录像通道 */
	uchar ucRes;
	SYSTEM_TIME stStartTime;
	SYSTEM_TIME stEndTime;
	uchar ucFileName[80];/* 文件名格式:通道号_开始时间(yyyy-MM-dd-HH-mm-ss)_结束时间(yyyy-MM-dd-HH-mm-ss)_盘号_分区号_簇号_文件序号_文件类型(00录像 01图片)_文件长度*/  
	uchar ucRes1[16];
}NET_FILEINFO;

typedef  struct _sensor_ability_   
{
	unsigned int iMskList;//0 1080P@30fps、1:1080P@25fps、720P@30fps、720P@25fps   按位表示
	unsigned int iMskFun; // bit 0  表示不支持区域遮挡功能  bit1  不支持巡航 自学习 线扫等设置
	unsigned int Res[2] ;//保留

}SENSOR_ABILITY;

#endif // _NETCONFIG_H
