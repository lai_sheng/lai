#ifndef REALTIMEH264CRYPTO_H
#define REALTIMEH264CRYPTO_H
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif

typedef struct Frame_Buffer
{
	uint8_t *  pBuf;
    /* data size */
    uint32_t   iDataSize;
    /* current buffer size */
    uint32_t  iBufSize;
    /* 
     * the step to alloc mem when need 
     * the default value is 32 Byte
     */
    uint32_t  iIncreaseSize;
    /* 
     * Do not use now
     * you shoule judge the buff if too max yourself
     */
    uint32_t  iMaxSize;
	uint32_t  iHead;
	uint32_t  iInit;
}FrameBuffer;

void InitFrameBuffer(FrameBuffer *fbuf);
uint32_t AesEncryptH264IFrame(uint8_t *in_buff, uint32_t in_len, FrameBuffer* out_buff);
#ifdef __cplusplus
}
#endif

#endif

