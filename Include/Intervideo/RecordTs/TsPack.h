#ifndef TSPACK_H_
#define TSPACK_H_
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <sys/types.h>

typedef enum{
	TS_AUDIO_FRAME   = 0x0001,
	TS_VIDEO_I_FRAME = 0x0002,
	TS_VIDEO_P_FRAME = 0x0004,
	TS_VIDEO_B_FRAME = 0x0008,	
}TSFrameType;

//type : 加type云录像要连续的时间戳  云告警不需要
void* TS_Open(int type, const char* FileName);
int   TS_Write(int wtype, void* Handle,unsigned char* buf,int len,uint64_t pts,TSFrameType type);
void  TS_Close(int type, void* Handle);
int  TS_Write_Status();
uint64_t  TS_TimeDuration(void* Handle);

#ifdef __cplusplus
}
#endif
#endif
