#ifndef __RECORDTSUPLOAD_H__
#define __RECORDTSUPLOAD_H__

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"

using namespace std;

void  compose_header(struct curl_slist **headerlist);
size_t write_memory(void *contents, size_t size, size_t nmemb, void *userp);


typedef struct {
  char *memory;
  size_t size;
} MemChunk;

class TImage
{
    public:
        TImage();
        TImage(const TImage & img);
        virtual ~TImage();

        TImage & operator=(const TImage & img);

    public:
        //Set remote link of image
        void setURL(const std::string & url);
        //Set local path of image
        void setPath(const std::string & filepath);
        //Set binary data of image
        void setBinary(const void * buf, size_t buf_len, const std::string & filename);
        //Set tag of image to classify images in recognition result 
        void setTag(const std::string & tag) { m_tag = tag; }

    public:
        const std::string & url() const { return m_url; }
        const std::string & path() const { return m_path; }
        const void * buffer() const { return m_buffer; }
        size_t bufferLength() const { return m_len; }
        const std::string & filename() const { return m_filename; }
        const std::string & tag() const { return m_tag; }

    private:
        std::string m_url;
        std::string m_path;
        std::string m_tag;  //事件类型：1:移动侦测（人形），2:哭声侦测
        void * m_buffer;
        size_t m_len;
        std::string m_filename;
}; //Class TImage

typedef struct TS_FILE_INFO {
	 std::string filename;//文件名
     uint  filesize;    //文件大小
     uint  count;		//文件引用次数
}TS_FILE_INFO;


typedef struct PACKET_INFO {
        int uploadtype;   //0:云录像   1:云告警  
        TS_FILE_INFO *pFileInfo;
        TImage *pImage;     // 图片
		uint sttimestamp;  //开始时间戳
		uint edtimestamp;  //结束时间戳
}PACKET_INFO;

class CTsUpload :public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(CTsUpload);
		CTsUpload();
		~CTsUpload();
	public:
		VD_INT32 Start();
		VD_INT32 Stop ();
		void setUID(const std::string & uid) { m_uid = uid; }
		VD_BOOL NetGetDebug();
		void OnFileList(PACKET_INFO & uploadinfo);
		void OnFileList(const vector<PACKET_INFO> & uploadinfovect);
		
private:		
		VD_INT32  sendRequest(int type, struct curl_slist *headerlist,
									struct curl_httppost *post,	string & result, long *statusCode);
		VD_INT32 handleResponse(const char * resp, size_t resp_len, string & result);
		VD_INT32 perform(int type, const string & filename, TImage *images,uint64 sttimestamp,
    								uint64 edtimestamp,string & result, long *statusCode );		
		bool SendTsToServer();
		void ThreadProc();
	private:



typedef std::list<PACKET_INFO> PACKET_LIST;
	
	uint m_allfile_totalsize;
	uint m_list_count;
	PACKET_LIST m_packet_list;
	CMutex m_mutex_list;
	int  m_iDebug;
	std::string m_uid;
	std::string m_apiUrl;
	std::string m_warningurl;
	std::string m_tsurl;
};

#define g_TsUpLoadManager  (*CTsUpload::instance())

#endif
