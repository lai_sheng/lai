#ifndef __SNDEVICEATTR__
#define __SNDEVICEATTR__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/RecordTs/cJSON.h"

using namespace std; 


class CSNDeviceAttr:public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(CSNDeviceAttr);
		CSNDeviceAttr();
		~CSNDeviceAttr();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		
		void ThreadProc();
		
		void 	onTimer(uint arg);		
		void 	SetNtpSuccess();
		VD_INT32 PackDeviceAttrData(string & result);
		VD_INT32 GetCurTime(string &result);
		VD_INT32 GetWlanIp(string & result);
		VD_INT32 GetDeviceWlanPostion(string & result);
		VD_INT32 SystemGetCPUUsage(string & result);		
		VD_INT32 SystemGetMemoryUsage(string & result);
		VD_INT32 sendRequest(int type,struct curl_slist *headerlist,
							const char *post,	string & result, long *statusCode);		
		VD_INT32 handleResponse(const char * resp, size_t resp_len, string & result);
		VD_INT32 SendDeviceAttr();		
		VD_INT32 PackOtaResultData(string & result);
		VD_INT32 SendOtaResult(int upgradestatus = 0);		

	private:
		int 	m_iTimeCount;
		CTimer 		m_cTimer;
	private:
		string m_uid;
		string m_position;
		string m_sysversion;
		string m_mac;
		string m_apiUrl;
		int    m_iFlag;
		int    m_iNtpSuccess;
		//ota
		string m_Preversion;
		string m_otaUrl;
		int    m_iupgradestatus;
		int    m_iRebootFlag; //2:������־
};

#define g_SN_DevAttr_OTA (*CSNDeviceAttr::instance())


#endif /**/

