
#ifndef __RECORD_CAM_TRANS_H__
#define __RECORD_CAM_TRANS_H__

#include <sys/stat.h>
#include <unistd.h>
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "APIs/DVRDEF.H"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"

#include "APIs/Net.h"
#include "APIs/System.h"
#include "Intervideo/RecordTs/RecordTsUpLoad.h"
#include "Intervideo/RealTimeH264Crypto.h"


//实时录2路流
//云录像  及云告警  云告警预录
class CRecordTsManager :public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(CRecordTsManager);
		CRecordTsManager();
		~CRecordTsManager();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();

		void ThreadProc();
		
		VD_INT32 StartVideo(int Channel, int StreamType);
		void OnCapture(int iChannel, uint iStreamType, CPacket *pPacket);
		void OnEvent(TImage *pImage);
		void onRecordTimer(uint arg);
		VD_INT32 StartAudioIn(int channel);		
		VD_INT32 OnVideoData(CPacket *pPacket);
		VD_INT32 OnAudioData(int iChannel, CPacket *pPacket);
		void OpenTsFile(SYSTEM_TIME *start_time);//云录像打开函数
		void OpenAlarmTsFile(SYSTEM_TIME *start_time);//云告警打开函数
		void OpenAlarmMp4File(SYSTEM_TIME *start_time);
		void WritePacket();
		void WriteTsFile(int isVideo,CPacket *pPacket);				
		VD_INT32 WriteVideotoAlarmTsFile(uchar *pData, unsigned int iLen);		
		VD_INT32 WriteVideotoAlarmMp4File(uchar *pData, unsigned int iLen);
		VD_INT32 WriteVideotoTsFile(uchar *pData, unsigned int iLen);
		void WriteAduiotoTsFile(CPacket *pPacket);
		void CloseTsFile(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec);		
		void CloseAlarmTsFile(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec);
		void CloseAlarmMp4File(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec);
		void SetNtpSuccess(){m_iNtpSuccess = 1;}
		void SetEnable(int type, int Enable);
		void SetSuspendMode(int mode);//{m_iSuspendMode = mode;}
	private:
		int 	m_iTimeCount;
		CTimer 		m_cRecordTimer;
	public:
		typedef struct PACKET_CAPTURE {
				int chn;
				int isVideo;
				CPacket* pPacket;
				uint dwStreamType;
		}PACKET_CAPTURE;

		
		//音视频队列
		typedef std::list<PACKET_CAPTURE> PACKET_LIST;
		uint m_packet_count;
		uint m_videopacket_count;
		uint m_audiopacket_count;  
		PACKET_LIST m_packet_list; //队列
		CMutex m_mutex_list;	  //队列锁

		//事件队列
		typedef std::list<TImage* > EVENT_LIST; 
		EVENT_LIST m_event_list;
		CMutex m_mutex_eventlist;	  //事件队列锁
		int    m_event_count;     //事件数
		
		CABuffer m_ptkBufMain;
		void*       m_TsHandle;   //云录像TS操作Handle
		void*       m_TsAlarmHandle; //云告警TS操作Handle
		void*       m_Mp4AlarmHandle;  //云告警Mp4操作Handle
		char   m_cTsFileName[128];//云录像文件名
		char   m_cAlarmFileName[128]; //云告警文件名
		int    m_iFirstIFrame;  //云录像第一帧
		int    m_iAlarmFirstIFrame; //云告警第一帧
		
		struct tm start_tm; //云录像文件开始时间  
		struct tm end_tm;	//云录像文件结束时间
		struct tm alarm_sttm; //云告警文件开始时间  
		struct tm alarm_edtm;	//云告警文件结束时间
		int   m_iNtpSuccess; //1: NTP时间同步成功;
		int   m_iAlarming;   //0:当前无告警  1:当前告警
		int   m_iAlarmEnable;
		int   m_iTsCloudEnable; 
		FrameBuffer m_cryptbuff;  //加密BUF
		int m_iSuspendMode;
};

#define g_RecordTsManager  (*CRecordTsManager::instance())

#endif

