#ifndef _DEVSEARCH_CPP
#define _DEVSEARCH_CPP

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "sys/types.h"
#include "APIs/DVRDEF.H"
#include "DevSearchGlobal.h"
#include "IPCAM_NetCommunication.h"
#include "IPCAM_NetCommunicationT.h"
#include "Functions/FFFile.h"

typedef enum{
	TESTSOCKET_READ		= 0,	
	TESTSOCKET_WRITE	= 1
}TESTCTRL;

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
enum PLAYCMD{ STOPPLAY = 0, STARTPLAY, VOLUMEUP,VOLUMEDOWN };
#endif

#if defined(IPC_HISI) || defined(CHIP_3516)
#define FACINFONANDDEVPATH		"/dev/mtdblock3"
#else
#define FACINFONANDDEVPATH		"/mnt/FACINFO"
#endif

#ifdef IPC_JZ
#define FACINFOBACKUPPATH       "/mnt/backup/"        //FACINFO备份目录
#define FACINFOBACKUPDEVPATH    "/mnt/backup/FACINFO" //FACINFO备份文件,与上对应
#else
#define FACINFOBACKUPPATH       "/app/backup/"        //FACINFO备份目录
#define FACINFOBACKUPDEVPATH    "/app/backup/FACINFO" //FACINFO备份文件,与上对应
#endif


#define IPCAM_HOME_DIR					"/app"
#define APP_PTHNAME						IPCAM_HOME_DIR
#define WEB_BACKUPPTHNAME				IPCAM_HOME_DIR"/backup"
#define PTZ_PTHNAME						IPCAM_HOME_DIR"/ptz"
#define WEB_UPDATE_TMPDIR				"/app/upgrade/webupdate"
#define WEB_UPDATE_BACKUPDIR			"/app/upgrade/backup"
#ifdef BPI_S2L
#define WEB_UPDATE_FILENAME				"_tmpupdate.tar"
#else
#define WEB_UPDATE_FILENAME				"_tmpupdate.tar.gz"
#endif

#define COMMAND_UPDATE_DEL		 		"rm -rf "
#define COMMAND_UPDATE_DEL_TMPFILE 		"rm -rf "WEB_UPDATE_TMPDIR"/"WEB_UPDATE_FILENAME

#define CLEANUP_DEL_REBOOT				0X01
#define CLEANUP_DEL 					0X02
#define RECV_IPNCPATH					"/app/ipnc"
#define IPNCPATH						"/app/"

#define APP_DEVNAODE 					"/dev/mtd4"
#define APP_DEVBLOCK 					"/dev/mtd4block"

#define FAC_TOTAL_CHECK_UUID_PATH  		TARGET_DIR"/FAC_TOTAL_CHECK/UUID.txt"

typedef struct {
    UINT        devNo;                      //内部编号
	CHAR        devModel[20];               //出厂型号
	CHAR        devSerialNumb[64];          //出厂编号
    CHAR        devRegcode[26];             //设备注册码
    CHAR        devName[24];                //设备显示名称
	CHAR		szFactoryName[32];			//厂家名称
	CHAR		NetMacAddr[24];				//MAC addr
	
	UCHAR 		LogoType;					//logo 类型
	ULONG 		FactoryTime;				//出厂时间
	INT			WifiOFF;
	INT			SDOFF;
	INT			AudioInOFF;
	INT			AudioOutOFF;
	INT			IOOFF;						//是否支持IO
  	INT			ThreeMachineSupport;		//是否支持三倍机
	CHAR 		PTPUUID[32];           	    //设备号
	CHAR		sensor[32];                 //
	INT			nChannel;					//通道 NVR用
	CHAR 		PTPCMEI[32];           	    //device CMEI
	CHAR        PTPDevKey[76];              //yd 密钥 有效72
}SAVENANDINFO_T;


typedef struct 
{
	UINT                client_sock;				        //accept得到的socket
	CHAR                client_ip[16];			            //连接上来的客户端IP
	UINT                client_port;				        //客户端所使用的client_port
	INT               	DTChannelID;
	CHAR                enable;
	CHAR                videoType;                          //客户端浏览IPC视频或者NDVR本地视频
	BOOL                IsPS;                               //PS 发包方式为PS
	pthread_mutex_t     DTMutex;
	CHAR 				client_name[16];
} CLINETNETINFO,*LPCLIENTNETINFO;

class CDevTools: public CObject
{

	public:
		PATTERN_SINGLETON_DECLARE(CDevTools);
		CDevTools();
		~CDevTools();
		int Start();
		int Stop();
	private:
		int DevUpdateService();
		int DevGetSearchInfo();
		int DevGetNetInfo();
		int DevSetNetInfo();
		int DevGetFacInfo();
		int DevSetFacInfo();
		
	private:
		unsigned short m_selfcode;
		
};

#define g_DevTools (*CDevTools::instance())


int GetUuid(char uuid[16]);
int GetCmei(char cmei[15]);
int GetDevKey(char DevKey[72]);
int GetConfigMac(char mac[16]);

#endif


