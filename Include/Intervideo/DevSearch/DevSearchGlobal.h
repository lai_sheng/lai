//IPCAM_global.h
/*##############################################################################
#工程描述:
#       IP网络摄像机
#
#文件描述:
# 
#作者信息:
#       Fifo2005@gmail.com                                   2011-03-28   创建
#版权信息:
#       Copyright(c) 2008~201x 
#  	    Fifo2005@gmail All rights reserved.
###############################################################################*/
#ifndef __IPCAM_GLOBAL__
#define __IPCAM_GLOBAL__

// GLOBAL define
#ifndef  NULL
#define NULL (void *)0
#endif
#ifndef  BOOL
#define  BOOL int
#endif
#ifndef TRUE
#define TRUE	0x01
#endif
#ifndef FALSE
#define  FALSE	0x00
#endif
#ifndef CHAR
#define  CHAR char
#endif
#ifndef UCHAR 
#define  UCHAR unsigned char
#endif
#ifndef  SHORT 
#define   SHORT short
#endif
#ifndef USHORT 
#define  USHORT unsigned short
#endif
#ifndef INT
#define INT   int
#endif
#ifndef UINT
#define UINT unsigned int
#endif
#ifndef LONG 
#define LONG long
#endif
#ifndef ULONG
#define ULONG unsigned long
#endif
#ifndef LLONG
#define LLONG long long
#endif
#ifndef ULLONG
#define ULLONG unsigned long long
#endif
#ifndef VOID 
#define VOID void
#endif
#ifndef HANDLE
#define HANDLE  void *
#endif

#ifndef HIWORD 
#define HIWORD(dword) (USHORT)(dword>>16)
#endif
#ifndef LOWORD
#define LOWORD(dword) (USHORT)(dword&0xFFFF)
#endif
#ifndef DWORDHI
#define DWORDHI(dword,word) (dword=word<<16)
#endif
#ifndef DWORDLO
#define DWORDLO(dword,word) (dword|=word   )
#endif
#ifndef MAKELONG
#define MAKELONG(low, high) ((DWORD)(((WORD)(low)) | (((DWORD)((WORD)(high))) << 16)))
#endif


#endif

