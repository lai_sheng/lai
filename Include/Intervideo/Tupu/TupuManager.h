#ifndef _TUPUMANAGER_H
#define _TUPUMANAGER_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"

#include "Intervideo/Tupu/Base64.hpp"
#include "Intervideo/Tupu/TImage.hpp"
#include "Intervideo/Tupu/Recognition.hpp"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"


using namespace TUPU2;

//class Recognition;

class CTupuSnap : public CObject
{
public:
	//! 构造函数
	CTupuSnap();

	//! 析构函数
	~CTupuSnap();
	void StartSnap(); //开始抓图
	void StopSnap();  //结束抓图
	int  SnapStatus();
	void OnSnap(int iChannel, uint dwStreamType, CPacket *pPacket);//抓图回调
	unsigned const char* GetBuffer();
	unsigned int GetBufferSize();
public:
	int  m_iStart;  //JPEG开始
	int  m_iReady;
	CABuffer m_ptkBufJPEG;
	CABuffer m_ptkBufTmp;
	CMutex 	m_SnapMutex;				//! 抓图开关锁
};

class TupuSend : public CThread
{
	public:
		TupuSend();
		~TupuSend();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		void OnCapture(uchar *buf, uint len);
		void ThreadProc();
		void SendCapture();
		typedef struct PACKET_CAPTURE
	    {
	        int chn;
	        CPacket* pPacket;
	        uint dwStreamType;
	    }PACKET_CAPTURE;
	    typedef std::list<PACKET_CAPTURE> PACKET_LIST;
	    PACKET_LIST m_packet_list;
	    uint m_packet_count; //!主要实时更新m_packet_list的数量，这样判断数量可以不用进入锁里去
	    CMutex    m_mutex_list;
	private:
		char m_UUID[32];
		Recognition *m_rec;	
};

class TupuManager : public CThread
{
	public:
//		PATTERN_SINGLETON_DECLARE(TupuManager);
		static TupuManager * instance();
		TupuManager();
		~TupuManager();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		VD_INT32 StartSnap();
		VD_INT32 StopSnap();
		void ThreadProc();
		int TimerSnap(int arg);
		int TimerStartRec(int arg);
		int TimerSection(int arg);
		void onConfigTupu(CConfigTupu& cCfgTupu, int& ret);
		int Log(char *log);

	private:
		CTupuSnap 	m_Snap;
		TupuSend    m_TupuSend;
//		Recognition *m_rec;	
		CTimer  m_ctimer;
		CTimer  m_cSnapTimer;
		CTimer  m_rectimer;
//		char m_UUID[32];
		int  m_iSnapTimerStatus; //抓图定时器状态 0:停止 1:开启
		CConfigTupu m_cCfgTupu;			/*!< 图普配置类 */
};

#define g_Tupu (*TupuManager::instance())


#endif
