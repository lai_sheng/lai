#ifndef REALTIMEINTERFACE_H_
#define REALTIMEINTERFACE_H_

#ifdef __cplusplus
	extern "C" {
#endif

/***************************************************************/
int RealTimeNetWorkStatus();
int RealTimeNetDns(char*hostname,char*retIP,int size); //仅给vfware用不对外SDK
int UpgradeStatus();
/***************************************************************/

#ifdef __cplusplus
}
#endif

#endif

