
#ifndef __LIVE_CAM_TRANS_H__
#define __LIVE_CAM_TRANS_H__

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "APIs/DVRDEF.H"
#include "LiveCamVideo.h"
#include "LiveCamAudio.h"
#include "APIs/Net.h"
#include "APIs/System.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"
#include "APIs/CommonBSP.h"

class CLiveCamTrans : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CLiveCamTrans);
	CLiveCamTrans();
	~CLiveCamTrans();
public:
	VD_INT32 Start();
	VD_INT32 Stop();

	void ThreadProc();

	int GetSnapData(int iChn,char *FileName);

	//����ͼƬ�ļ�
	int SavePicture(int Chn, char *PathName);
	int StartVideo(int Channel, int StreamType,bool WithAudio, int nNetType);
	int StopVideo(int Channel, int StreamType,int Type);		
	int StartAudio(int Channel);
	int StopAudio(int Channel);
				
private:
	CPacket         *pSnapBuffer;	

    CLiveCamVideo 	*m_VideoWanTran ; //��Ƶ����������
    CLiveCamAudio 	*m_AudioWanTran ; //��Ƶ����������
public:
	uint			m_dwState;
	uint  			m_initFlag;
};

#define g_LiveCamTrans  (*CLiveCamTrans::instance())

#endif

