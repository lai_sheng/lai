#ifndef _LIBQINIUYUN_H_
#define _LIBQINIUYUN_H_

#ifdef __cplusplus
extern "C"
{
#endif

int QiniuUpLoadPictureInit(const char *fileName,const char *LoadPath);
int  Setcamerashot(double macid,double videoId,const char *fileName);
char* PicturedownloadUrl(const char* key);
int  DownUpgradePack(char *DownUrl, char *file_md5sum, char *version, char *filepath);
int UpTpLoadFile(char* uploadName, char* pLocalFilePath);
#ifdef __cplusplus
}
#endif
#endif