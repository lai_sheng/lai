#ifndef __REALTIME_TASKPUSH_STREAM_H__
#define __REALTIME_TASKPUSH_STREAM_H__

#define RTMP_HANDLE void

#ifdef __cplusplus
extern "C" {
#endif

void *TaskPushStreamThreadStart();

int TaskPushStreamStart();
void TaskPushStreamStop();
void TaskPushStreamPush(char *buffer,int framelen);

void RealTimeCloseSocket();
void RealTimeStartLink();
int DelUrlAllConfig();

#ifdef TUPU
int TaskUploadFileStart();
int UpLoadFileStatus();
#endif

#ifdef __cplusplus
}
#endif


#endif

