#ifndef __LIVE_CAM_AUDIO__
#define __LIVE_CAM_AUDIO__

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "Devices/DevAudioIn.h"

class CLiveCamAudio : public CObject
{
	public:
		CLiveCamAudio();
		~CLiveCamAudio();
	public:
		VD_INT32 StartAudioOut(int channel);
		VD_INT32 StartAudioIn(int channel);
		VD_INT32 StopAudioOut(int channel);
		VD_INT32 StopAudioIn(int channel);
		VD_INT32 SendAuidoInData();
//		VD_INT32 PutAuidoOutData(int channel, unsigned char *buf, unsigned int len);
	private:
		VD_INT32 OnAudioData(int iChannel, CPacket *pPacket);
};

#endif /*__LIVE_CAM_AUDIO__*/

