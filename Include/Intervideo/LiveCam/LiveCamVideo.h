#ifndef __LIVECAMVIDEO_H__
#define __LIVECAMVIDEO_H__

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"

class CLiveCamVideo :public CThread
{
	public:
		CLiveCamVideo();
		~CLiveCamVideo();
	public:
		VD_INT32 Start(int Channel, int StreamType);
		VD_INT32 Stop (int Channel, int StreamType);
		
	private:
		VD_INT32 UpdateEncodeCfg();
		VD_INT32 OnVideoData(int iChannel, uint iCodeType, CPacket *pPacket);
		VD_INT32 SendVideoData(uchar *pData, unsigned int iLen, unsigned char iChannel, unsigned char iStreamType, unsigned char FrameType);
		void OnCapture(int iChannel, uint iStreamType, CPacket *pPacket);
		bool SendVideoToServer();
		void ThreadProc();
	private:
		int       m_MainFps;  //帧率
		int       m_MainReslution; //分辨率
		int       m_SubFps;  //帧率
		int       m_SubReslution; //分辨率
typedef struct PACKET_CAPTURE {
        int chn;
        CPacket* pPacket;
        uint dwStreamType;
}PACKET_CAPTURE;

typedef std::list<PACKET_CAPTURE> PACKET_LIST;

	uint m_packet_count;
	PACKET_LIST m_packet_list;
	CMutex m_mutex_list;
	unsigned char m_bIsFirstFrame;
	CABuffer m_ptkBufMain;
	CABuffer m_ptkBufSecond;
};

#endif
