/**
 * RealTime_apiv1.h
 *
 * External APIs for device side implementation.
 *
 * Created by    2017-07-01
 */

#ifndef __REAL_TIME_APIV2_H__
#define __REAL_TIME_APIV2_H__

#ifdef __cplusplus
extern "C" {
#endif


#include<stdint.h>
#include<inttypes.h>

#define RTPACKET_STR_LEN_32		32
#define RTPACKET_STR_LEN_16		16
#define RT_UUID_LEN				16
#define RT_CMEI_LEN             15
#define RT_KEY_LEN				64
#define RT_VIDEO_NANME_LEN      64
#define FILES_PAGE (8)
#define LIST_SIZE  (256)

#define RT_CODEC_H264						0x34363248	//H264
#define CODEC_MJPEG							0x45464758	//mjpeg

#define RT_ERR_NOERR             0       // No error
#define RT_ERR_MUTEX             -0x101  // Mutex creation error
#define RT_ERR_THREAD            -0x102  // Thread creation error
#define RT_ERR_SOCKET            -0x103  // Socket creation error
#define RT_ERR_SOCK_OPT          -0x104  // Socket option setting error
#define RT_ERR_SOCK_BIND         -0x105  // Socket bind error
#define RT_ERR_TIMEOUT           -0x106  // timeout error
#define RT_ERR_NOMEM             -0x107  // memory error
#define RT_ERR_PARAM             -0x108  // invalid param
#define RT_ERR_IVALSID           -0x109  // invalid session id
#define RT_ERR_NAT_TRAVERSAL     -0x10A  // NAT traversal failure
#define RT_ERR_PROTOCOL     	 -0x10B  // 
#define RT_ERR_MAX               -0x200  //
#define RT_ERR_BUFNOTEXIST       -0x202  // buffer not exist
#define RT_ERR_NOTSTART          -0x203  // RealTime module not start
#define RT_ERR_CLOSING           -0x204  // RealTime module is closing
#define RT_ERR_NOENOUTGMEM       -0X205  // not enough memery
#define RT_ERR_APINOTSUPPORT     -0x206  // in the case the api is not support
#define RT_ERR_BUSY              -0x205  // busy


#ifndef RT_WAVE_FORMAT_G711
#define RT_WAVE_FORMAT_G711					0x7A19
#endif

#ifndef WAVE_FORMAT_AAC
#define WAVE_FORMAT_AAC                   0x7A26
#endif
#define RT_WEEKS					    7
#define RT_TSECT        				6

typedef enum {

	DevType_Gun 	= 1,  //枪机
	DevType_Nvr 	= 2,  
	DevType_Card 	= 4,  //卡片机
	DevType_Shake	= 5,  //摇头机
	DevType_NewCard = 6,
	DevType_Battery = 8,	
	DevType_4G 		= 9,
	DevType_NewShake= 10, //新摇头机 (16k全双工)
	DevType_NewGun	= 11, //为G3S2新加	
	DevType_FaceRcgn= 12, //人脸识别Q1	
	DevType_BatSingle 	= 13, //电池单品	
	DevType_FamilyBall 	= 14, //
	DevType_LockI9M		= 16,
	DevType_LockI9P	  	= 17,
	DevType_PtzCamera	= 20, //球机
	DevType_Other,
	//.......
}RT_DevType;


//对接时存在与冲突修改为RT_ modify by jwd on 20171221
//编码

//编码
typedef enum
{
	RT_ERMODE  =   0x00, //初始化
	RT_HDMODE	=	0x01, //   高清
	RT_SDMODE	=	0x02, //   标清
	RT_LUMODE  =   0x03, //   流畅
}RT_EncodeMode;


//文件保存路径
typedef enum
{
	RTMPADDR	=	0x01,  //url目录
	STREAMID	=	0x02,  //streamId目录
	UPGRADEPATH =   0x03,  //升级目录
	LiveMuteSta =   0x04,  //声音开关目录 
	AiPhotoDir	=   0x05,  //Ai下载图片存放目录
	AlarmRecFile= 	0x06,
}SysParamType;


typedef enum
{
	RT_CONTINUE	=	0x00,  //继续播放
	RT_PAUSE	=	0x01,  //暂停
	RT_STOP     =   0x02,  //停止
	RT_FAST     =   0x03,  //快进
	RT_SLOW     =   0x04,  //慢放
	RT_JUMP     =   0x05,  //拖动  ms
}CONTROLTYPE;

typedef enum _RT_PTZControlCmd
{
	RT_PTZ_MV_STOP      = 0,    // 停止运动
	RT_PTZ_ZOOM_DEC     = 5,
	RT_PTZ_ZOOM_INC     = 6,
	RT_PTZ_FOCUS_INC    = 7,    //焦距
	RT_PTZ_FOCUS_DEC    = 8,
	RT_PTZ_MV_UP        = 9,    //向上
	RT_PTZ_MV_DOWN      = 10,   //向下
	RT_PTZ_MV_LEFT      = 11,   //向左
	RT_PTZ_MV_RIGHT     = 12,   //向右
	RT_PTZ_IRIS_INC     = 13,   //光圈
	RT_PTZ_IRIS_DEC     = 14,   //
	RT_PTZ_AUTO_CRUISE  = 15,	  //自动巡航
	RT_PTZ_GOTO_PRESET  = 16,   //跳转预置位
	RT_PTZ_SET_PRESET   = 17,   //设置预置位点
	RT_PTZ_CLEAR_PRESET = 18,   //清除预置位点
	RT_PTZ_ACTION_RESET = 20,   //PTZ复位
	RT_PTZ_MV_LEFTUP    = 21,
	RT_PTZ_MV_LEFTDOWN  = 22,
	RT_PTZ_MV_RIGHTUP   = 23,
	RT_PTZ_MV_RIGHTDOWN = 24,
	RT_PTZ_CLEAR_TOUR   = 25,
	RT_PTZ_ADD_PRESET_TO_TOUR  = 26,
	RT_PTZ_DEL_PRESET_TO_TOUR  = 27
}RT_PTZControlCmd;

/// 系统时间结构
typedef struct _RTDateTime{
    int  year;///< 年。   
	int  month;///< 月，January = 1, February = 2, and so on.   
	int  day;///< 日。   
	int  wday;///< 星期，Sunday = 0, Monday = 1, and so on   暂时未用到
	int  hour;///< 时。   
	int  minute;///< 分。   
	int  second;///< 秒。   
}RTDateTime;

typedef enum{
   RT_ENABLE = 0,
   RT_UNABLE = 1,
}RT_ABLE;

typedef struct _RT_TracePointInfo{
	int iPointNum; 	//0-5:the num of preset , -1:unset
	int iStayTime;
}RT_TracePointInfo;

typedef struct _RT_TraceRunTime{
	RTDateTime Start; 	//0-5:the num of preset , -1:unset
	RTDateTime End;
}RT_TraceRunTime;

typedef struct _RT_SetTraceRoute{
	RT_ABLE Enable;//0:enable 1:unable
	RT_TraceRunTime	  runTime[3];
	RT_TracePointInfo traceInfo[6];  
}RT_SetTraceRoute;

typedef struct _RT_PresetPoint{
	int x;
	int y;
	RT_ABLE enable;
}RT_PresetPoint;

typedef struct _RT_ALLPresetPointInfo{
	RT_PresetPoint pointInfo[6];
}RT_ALLPresetPointInfo;


typedef struct _RT_SystemFilePath
{
	SysParamType type;
	char FilePath[128];
}RTSystemFilePath;

typedef struct _RT_PushAlarm
{
    short   alarm_type;         //报警类型  目前未用到
    short   channel;			//报警通道  目前未用到
	short   alarm_flag;			//报警内容  1:start  0:end   2:PIR唤醒报警 3:4G流量告警
    RTDateTime   timestamp;      //报警时的时间戳  目前未用到
} RTPushAlarm;


typedef struct _RT_FileInfo
{
    char FileName[RT_VIDEO_NANME_LEN];
    int  FileTypeMask; 				//文件类型
    char FileCreateTime[64]; 		//文件生成时间    格式: "%d-%d-%d %d:%d:%d"
   	int  RecordDuration; 			//时长
    int  FileSize; 					//文件大小
}RTFileInfo;

typedef struct _RT_FileInfo2
{
    char FileName[RT_VIDEO_NANME_LEN];
    int  FileTypeMask; 				//文件类型
	RTDateTime FileStartTime;		//录像开始时间
	RTDateTime FileEndTime;			//录像接收时间
   	int  RecordDuration; 			//时长
    int  FileSize; 					//文件大小
}RTFileInfo_2;

typedef struct _RT_FileInfo3
{
    char FileName[RT_VIDEO_NANME_LEN];
    int  FileTypeMask; 				//文件类型
	uint32_t FileStartStamp;		//录像开始时间
	uint32_t FileEndStamp;			//录像接收时间
   	int  RecordDuration; 			//时长
    int  FileSize; 					//文件大小
}RTFileInfo_3;

typedef struct _RT_SDInfo
{
	int  SDExist;		//0 not or 1 yes;
	int	 SDTotalSize;	//总容量(M)
	int	 SDFreeSize;	//空闲量
	char EarlyFileName[64]; //当前SD卡最早一个录像文件
}RTSDInfo;


typedef struct _RTFileListPerPage
{
	int FileCount;
	RTFileInfo fileinfo[LIST_SIZE];
}RTFileListPerPage;

typedef struct _RTFileListPerPage2
{
	int FileCount;
	RTFileInfo_2 fileinfo[LIST_SIZE];
}RTFileListPerPage_2;

typedef struct _RTFileListPerPage3
{
	int FileCount;
	RTFileInfo_3 fileinfo[LIST_SIZE];
}RTFileListPerPage_3;

typedef struct _QueryRTRecordTime{
	RTDateTime StartTime;
	RTDateTime EndTime;
}QueryRecordTime;



typedef struct _RT_V_AudioInfo
{
	unsigned char  channelId;
	unsigned char  reserve;
	unsigned short checksum;
	unsigned int time;
}RT_V_AudioInfo;

typedef struct _RT_VideoDataFormat
{
	unsigned int codec;				//编码方式
	unsigned int bitrate;        	//比特率, bps
	unsigned short width;			//图像宽度
	unsigned short height;			//图像高度
	unsigned char framerate;		//帧率, fps
	unsigned char colorDepth;		//should be 24 bits
	unsigned char frameInterval;   //I帧间隔
	unsigned char reserve;

} RealTimeVideoDataFormat;

//音频数据格式
typedef struct _RT_AudioDataFormat
{
	unsigned int samplesRate;		//每秒采样
	unsigned int bitrate;			//比特率, bps
	unsigned short waveFormat;		//编码格式
	unsigned short channelNumber;	//音频通道号单通道1 双通道2
	unsigned short blockAlign;		//块对齐, channelSize * (bitsSample/8)
	unsigned short bitsPerSample;	//每采样比特数
	unsigned short frameInterval;	//帧间隔, 单位ms
	unsigned short reserve;

} RealTimeAudioDataFormat;



typedef struct _RT_V_PTZControlRequest
{
	unsigned int deviceId;
	unsigned char  channel;
	unsigned char  cmdCode;
	unsigned short size;
} RT_V_PTZControlRequest;


typedef struct _RT_ControlArgData
{
	unsigned int arg1;
	unsigned int arg2;
	unsigned int arg3;
	unsigned int arg4;
} RT_ControlArgData;


typedef struct _RT_DeviceInfo
{
	char DevType[48];      //设备型号  
	char SystemVersion[64];//系统版本 
	char WifiName[48];     //wifi名字
	int  WifiValue;        //wifi强度
	int  UpBandwidth;      //上行带宽
	int  DownBandwidth;    //下行带宽
	char DeviceNum[32];    //设备号
	char IpAddr[24];       //IP地址
	char MacAddr[24]; 	   //MAC地址

}RT_DeviceInfo;

//#ifdef IPC_4G
typedef struct _RT_4GInfo
{
	char PhoneNumber[16]; //手机号码
	char IMEINumber[32];  //4G卡IMEI号
	int  Operator;		  //4G卡运营商（1---移动   2---联通  3--电信）
	int  Signal;		  //4G卡信号强度（1---弱   2---中等  3---强）

	int  FlowDayUsed;	  //日消耗流量  单位:MB
	int  FlowMonUsed;	  //月消耗流量  单位:MB	
}RT_4GInfo;

typedef struct _FlowLimit
{
	int Enable;
	int MaxUsedFlow;	//限制使用流量 单位:Mb
}RT_FlowLimit;
//#endif

#ifdef NVR_TAOSHI
typedef struct _RT_NVRInfo
{
	int  channel;         //4 通道数量
	unsigned long long  State  ;         //4 通道状态按bit位至位 :0通道对应最低位(1:使能0:不使能)
}RT_NVRInfo;
#endif
/*--------------------face----------------------------------*/
//??????
typedef struct _RT_ModifyFaceInfo
{
	char strUrl[1024];    // ?URL??,?????
	char strName[128];   //??
}RT_ModifyFaceInfo;
//??????????
typedef struct _RT_ModNewFaceInfo
{
//	unsigned char *buf;    //??buf
//	unsigned int  size;    //????
	char strUrl[1024];    // ?URL??,?????
	char strName[128];   //??
	char jpegPath[256];  //图片存储位置
	char strsex[8];  //??,?:"M" , ?:"F"
	unsigned int  age;    //??
	unsigned int glass;   //0:?? 1:???
}RT_ModNewFaceInfo;

typedef struct _RT_QueryFaceInfo
{
	char strUrl[1024];    // strUrl[0] ?0 ???URL?
	char strName[128];   //char strName[128];   //strName[0] if null or 0 ,check all
 	char strsex[8];  //strsex[0]  ?0 ?????
	int  age;            //  ?-1  ????? ?
	int glass;           //?-1 ???????
	int page;            // ?-1 ??????1?50?
}RT_QueryFaceInfo;


typedef struct _RT_QueryRespFaceInfo
{
	int count ;//??????
	RT_ModNewFaceInfo  faceinfo[50];
}RT_QueryRespFaceInfo;

typedef struct _Face_Rectangle_t {
	int x;
	int y;
	int width;
	int height;
} Face_Rectangle_t;

typedef struct _RT_FaceInfo
{
	unsigned char *buf;    //??buf
	unsigned int  size;    //????
	char strUrl[1024];    //face URL
	char strsex[8];  	  //??,?:"M" , ?:"F"
	unsigned int  age;    //??
	unsigned int glass;   //0:?? 1:???
	char  strFeeling[128];// "happy"  ...	
	Face_Rectangle_t fangle; 	/* face x,y,width and height */
}RT_FaceInfo;

typedef struct _RT_PanoramaInfo
{
	unsigned char *buf;    //??buf
	unsigned int  size;    //????
	char		  ImageUrl[1024];	
}RT_ImageInfo;

typedef struct _RT_IdentifyInfo 
{
	char strFaceUrl[1024]; //??URL 
	char strName[128];   //????
	double fIdentirate;  //????
}RT_IdentifyInfo;


typedef enum
{
	RT_FACE		=	0x00,  //脸部识别
	RT_CRY		=	0x01,  //哭声识别
	RT_MOTIOM	= 	0X02,  //motion detect
	RT_VOICE	=	0x03,
	RT_PEOPLE	=	0x04,
	RT_OTHER,
}RT_IdentifyType;

typedef struct _RT_UpLoadFaceinfo 
{
	uint32_t		TimeStamp;
	RT_ImageInfo	ImageInfo;
	RT_IdentifyType	AlarmType;
	
//当FaceOrCry为 RT_FACE 时,以下参数才起作用
	RT_FaceInfo 	FaceInfo;
	RT_IdentifyInfo IdentifyInfo;
}RT_UpLoadAlarmInfo;
typedef struct _RT_UpLoadFaceFreqinfo 
{
	uint32_t		FaceUploadFreq;
	uint32_t		StrangerUploadFreq;
	uint32_t		CryUploadFreq;
}RT_UpLoadFaceFreqinfo;

/*----------------------mp3-----------------------------*/

typedef enum _RT_MP3PLAYCTRL
{
	MP3_CLOSE	=	0x00,  
	MP3_PAUSE	=	0x01,  
	MP3_RESUME	= 	0X02,  
	MP3_OTHER,
}RT_Mp3PlayCtrl;

/*------------------------------------------------------*/

#ifdef HMSOTA
typedef struct rttagCONFIGNETHMS
{       
    //!是否开启
    int Enable;
    //!服务名
    char ServerName[128];
    //!端口号
    int Port;
    //!用户名
    char UserName[64];
    //!密码
    char Password[64];    

}RT_CONFIGNETHMS;

typedef struct rttagCONFIGNETOTA
{       
    //!模式
    int Mode;
	//!间隔时长 6 小时是默认
	int Interval;

}RT_CONFIGNETOTA;
typedef struct rttagUPLOADNEWVERINFO
{	
	int UnAttended; 
	int Critical;
	char RelNotes[5*1024]; 
	char Lang[64];	
	char FileName[128];
	int FileSize; 
	char BuildDateTime[128];
}RT_UPLOADNEWVERINFO;
#endif



typedef struct rttagTIMESECTION
{
    //!使能
    int enable;
    //!开始时间:小时
    int startHour;
    //!开始时间:分钟
    int    startMinute;
    //!开始时间:秒钟
    int    startSecond;
    //!结束时间:小时
    int    endHour;
    //!结束时间:分钟
    int    endMinute;
    //!结束时间:秒钟
    int    endSecond;
}RT_TIMESECTION;  
typedef struct rttagWORKSHEET
{
	int			iName;							/*!< 时间表名称 */	
	RT_TIMESECTION	tsSchedule[RT_WEEKS][RT_TSECT];	/*!< 时间段 */
}RT_WORKSHEET;


typedef struct {
	int capacity;               /**< 电池电量数据   */
	int chargingStatus;         /**< 电池充放电状态 0:未充电  1:正在充电 2:已充满电量*/
} RT_BAT_Status;




/* 日期时间定义 */
typedef struct _RT_DateTime
{
	uint32_t		m_microsecond;	//毫秒	0-1000
	uint32_t 		m_year;			//年,2009
	uint32_t		m_month;		//月,1-12
	uint32_t		m_day;			//日,1-31
	uint32_t		m_hour;			//0-24
	uint32_t		m_minute;		//0-59
	uint32_t		m_second;		//0-59
} RT_DateTime;

typedef enum{
	PCM          = 0,
}AUDIOPLY_TYPE;

typedef enum _MirrorFlip
{	
	NORMAL  = 0,   //  正常
	HORFLIP  = 1,  //   水平翻转
  	VERFLIP  = 2,  //   垂直翻转
	HORVERFLIP = 3,//   水平垂直都翻转
} MirrorFlip;


typedef enum _UpFileType
{	
	RT_MP4  = 0,
	RT_JPEG = 1,
} UpFileType;

typedef enum _RTSpecialState
{
	RT_NORMAL	= 0,   
	RT_UPGRADING= 1,	//正在升级
	RT_UPLOADING= 2,	//正在上传文件
	RT_UPGRADE_OK=3,
	RT_UPGRADE_FAIL=4,
}RTSpecialState;
/*
typedef enum _RTMusicPriority{
	RT_PRIORITY_LOW 	= 0,
	RT_PRIORITY_MEDIUM  = 1,
	RT_PRIORITY_HIGH  	= 2,
	RT_PRIORITY_HIGHEST = 3,	
}RTMusicPriority;
*/
typedef enum _RTSystemStatus{
	RT_SYSTEM_SUSPEND 	= 0,
	RT_SYSTEM_NORMAL   	= 1,
}RTSystemStatus;

/*---------the switch of all kinds of alarm-------------*/
typedef enum _RTAlarmSwitchType{
	RT_ALARM_REMAIN = -1,  //该次不修改
	RT_ALARM_OPEN   = 0,
	RT_ALARM_CLOSE  = 1,
}RTAlarmSwitchType;

typedef struct _RTAlarmSwitch{
	RTAlarmSwitchType AlarmSwitch_Cry;
	RTAlarmSwitchType AlarmSwitch_Motion;
	RTAlarmSwitchType AlarmSwitch_Human;
	RTAlarmSwitchType AlarmSwitch_Voice;
	RTAlarmSwitchType AlarmSwitch_Trace;
}RTAlarmSwitch;
/*------------------------------------------------------*/
//门锁相关
//LOCK
typedef enum
{
	LOCK_KEY		=	0x00,  //钥匙开锁
	LOCK_PSW		=	0x01,  //密码开锁
	LOCK_FINGER		=	0x02,  //指纹开锁
	LOCK_CARD		=	0x03,  //门卡开锁
	LOCK_REMOTE		=	0x04,  //遥控开锁
	LOCK_PICK		=	0x05,  //撬锁
	LOCK_OTHER,
}RT_UnlockType;	

typedef enum {
	RT_LOCK_OPEN		=	0x00,
	RT_LOCK_PICKALARM	=	0x01,
	RT_LOCK_ADDUSER		=	0x02,
	RT_LOCK_DELUSER		=	0x03,
	RT_LOCK_DOORBELL	= 	0x04,
	RT_LOCK_SYSTEMLOCK	=	0x05,
	RT_LOCK_LOWBAT		=	0x06,

}RT_LockMsgType;
typedef struct _RT_LockOpenInfo 
{
	char			Name[128]; 
	RT_DateTime		UnlockTime;
	RT_UnlockType	UnlockType;
	uint32_t		UserNumber;
}RT_LockOpenInfo ;

typedef struct _RT_LockUserInfo 
{
	RT_UnlockType	UnlockType;
	uint32_t		UserNumber;
}RT_LockUserInfo ;

typedef struct _RT_LockMsgInfo{
	RT_LockMsgType	MsgType;
	
//type 为 RT_LOCK_OPEN 才起作用	
	RT_LockOpenInfo	Open;

//type 为 RT_LOCK_ADDUSER 或者 RT_LOCK_DELUSER 才起作用	
	RT_LockUserInfo	User;

//type 为 RT_LOCK_LOWBAT 才起作用	
	uint8_t			Electric;		// 1~100
}RT_LockMsgInfo;

typedef struct _RT_DoorLockinfo 
{
	uint8_t		SignalStrength; // 1~4
	uint8_t		Electric;		// 1~100
	uint8_t		DoorStatus;		// 0:close 1:open
	uint8_t		LockStatus;		// 0:close 1:open
}RT_DoorLockinfo ;

typedef struct _RT_DoorLockTempPasswd 
{
	uint32_t	Time; 			// unit: s
	uint8_t		ValidNums;		// 生效次数，若无可忽略
	char		Passwd[128];	// 
}RT_DoorLockTempPasswd;

typedef struct _RT_DoorLockOperate
{
	uint8_t		Type; 		// 0:密码开锁 1:遥控开锁
	uint8_t		Action;		// 0:正常开锁 1:常开  2:关锁  3:限时关锁
//密码锁才生效
	char		Passwd[128];
//限时开关锁 该参数才生效	
	uint8_t		Time;		// 0:close 1:open	
}RT_DoorLockOperate;

typedef struct _RT_DoorLockVersion
{
	char		MacType[128]; 
	char		FirmwareVer[128]; 
	char		HardwareVer[128]; 
	char		ZigbeeVer[128]; 
}RT_DoorLockVersion;
/*------------------------------------------------------*/
struct WiFiInfo
{
	char ssid[33];
	int ssidLen;
	char pwd[80];
	int pwdLen;
};

// Functionality:
//   The callback function of starting recognizer
// Parameters:
//   N/A
// Return:
//   N/A
typedef void (*RecognizStart)(void);
// Functionality:
//   The callback function of finishing recognizer
// Parameters:
//   @[in]info: The info of wifi
// Return:
//   N/A
typedef void (*RecognizEnd)(struct WiFiInfo info);



// Functionality:
//   Malloc resource before Initialize( is not necessary)
// Parameters:
//   @[in]sampleRate :The samplerate of Sound Data
// 	 @[in]sampleRate :The bitwidth of Sound Data
// Ps:
//   there is unnecessary to call this func
void SoundWave_PreInit(int sampleRate,int bitWidth);

// Functionality:
//   Initialize sound recognizer services
// Parameters:
//   @[in]sampleRate :The samplerate of Sound Data
// 	 @[in]sampleRate :The bitwidth of Sound Data
// Return:
//   SoundWave Handle
void* SoundWave_Init(int sampleRate,int bitWidth);

// Functionality:
//   Initialize sound recognizer services
// Parameters:
//   @[in]recognizer 	:SoundWave Handle
// 	 @[in]start_cbfunc 	:The callback function of starting
// 	 @[in]end_cbfunc 	:The callback function of finishing 
// Return:
//	0 : success  
// !0 : fail 
int SoundWave_Start(void *recognizer,RecognizStart start_cbfunc,RecognizEnd end_cbfunc);

// Functionality:
//witedata sound recognizer services
// Parameters:
//   @[in]recognizer 	:SoundWave Handle
// 	 @[in]data 			:Audio data collection           
// 	 @[in]len 	        :data lenght
// Return:
// >0 : the number of data written actually
// <=0: fail 
int32_t SoundWave_WriteData(void *recognizer, const void *data, unsigned long len);
// Functionality:
// Stop sound recognizer services
// Parameters:
//   @[in]recognizer 	:SoundWave Handle
// Return:
//	0 : success  
// !0 : fail 

int32_t SoundWave_Stop(void *recognizer);



// Functionality:
//   Restart all of server
// Parameters:
//   N/A
// Return:
//   N/A
void RealTimeServerReStart();


// Functionality:
//   close all of server
// Parameters:
//   N/A
// Return:
//   N/A

void RealTimeServerClose();

// Functionality:
//   Push an alarm to RealTime cloud
// Parameters:
//   @[in]alarm: the alarm 
// Return:
//   0 : succeed
//   1:  fail
int32_t RealTimePushAlarm(RTPushAlarm alarm);



// Functionality:
//   Initialize Realtime terminal services. If net_params is not NULL, the setting
//   will be used to configure the device network interface.
// Parameters:
//   @[in]netparam: init info
//   @[in]upnpparams: network parameter settings, support multiple settings.
//   @[in]param_size: number of network parameter settings
// Return:
//   0 on success, other values on error
//
int32_t RealTimeInit(RT_DevType type);


// Functionality:
//   Packet video frame packet to the dest OwspData
// Parameters:
//   @[in] channel: the channel of the video data
//   @[in] ismainorsub: is main or subnumber videostream
//	 @[in] isIFrame: 1 for the frame is I frame, 0 for the frame is P frame
//	 @[in] videoData: the video data of the frame
//	 @[in] videoDataLen: the video data length
//	 @[out] ptrDst: last finish owsp data. containing the source data;
// Return:
//   packeted length

int32_t RealTimeSendVideoData(uint8_t channel,char isIFrame, 
			                    void* videoData, 
			                    uint32_t videoDataLen,uint32_t timestamp);


// Functionality:
//   Packet audio frame to the dest OwspData
// Parameters:
//   @[in] channel: the channel of the video data
//	 @[in] audioData: the video data of the frame
//	 @[in] audioDataLen: the video data length
//	 @[out] ptrDst: last finish owsp data. containing the source data;
// Return:
//   packeted length
//
int32_t RealTimeSendAudioData(uint8_t channel,void* audioData, 
						uint32_t audioDataLen, uint32_t timestamp);



// Functionality:
//  stop record send frame
// Parameters:
//   @[in] channel : channel;
// Return:
//  0 on success
//
int32_t RealTimeStopSendPlayBackData(uint8_t channel);

// Functionality:
//   Packet Record audio frame to the dest OwspData
// Parameters:
//	 @[in] channel: channel
//	 @[in] VideoData: the video data of the frame
//	 @[in] VideoDataLen: the video data length
//	 @[out] timestamp: the video data of the tiemstamp;
// Return:
//   packeted length
//
int32_t RealTimeSendRecordVideoData(uint8_t channel,char isIFrame, 
			                    void* videoData, 
			                    uint32_t videoDataLen,uint32_t timestamp);


// Functionality:
//   Packet Record audio frame to the dest OwspData
// Parameters:
//	 @[in] audioData: the video data of the frame
//	 @[in] audioDataLen: the video data length
//	 @[out] timestamp: the Audio data of the tiemstamp;
// Return:
//   packeted length
//
int32_t RealTimeSendRecordAudioData(uint8_t channel,void* audioData, 
						uint32_t audioDataLen, uint32_t timestamp);


// Functionality:
//   Upload the file to server 
// Parameters:
//	 @[in] FilePath: the path of file
//	 @[in] Type    : the type of file
// Return:
//   0:success 
//
int32_t RealTimeUploadFile(char FilePath[128],UpFileType Type);

// Functionality:
//  connect to server  status 
// Parameters:
//	null
// Return:
//   0:success 
//   -1: fail

int32_t RealTimeConnectToServerStatus();

// Functionality:
//  get special  status 
// Parameters:
//	null
// Return:
//  RTSpecialState
RTSpecialState RealTimeSpecialStatus();

int RTGetServerIpAddr(char * Ipaddr); //0:~{;qH!3I9&~}

int UpRecLoadFile(char* uploadName, char* pLocalFilePath);
// Functionality:
//   Request  synchronization time
// Parameters:
//	 @[in] timeout    : the type of int
// Return:
//   timestamps:success 
//	 -1 : fail

int64_t RealTimeReqSyncTime(int timeoutsec);

#ifdef HMSOTA
int64_t RealTimeUploadOtaNewVersionInfo();
int32_t RealTimeSendUpgradeStatus(int cmd, int progress);

#endif

#ifdef __cplusplus
}
#endif

#endif // __REALTIME_API_H__

