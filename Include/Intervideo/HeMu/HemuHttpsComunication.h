#ifndef __HEMUHTTPSCOMUNICATION__
#define __HEMUHTTPSCOMUNICATION__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <curl/curl.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/HeMu/HemuProbuf.pb-c.h"	



typedef enum
{
	MSG_P_THD_ADD_CAMERA_REQ,
	MSG_P_THD_SET_CAMERA_CAPACITY_REQ,
	MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ,
	MSG_P_THD_GET_CAMERA_SETTINGS_REQ,
	MSG_P_THD_SET_CAMERA_SETTINGS_REQ,
	MSG_P_THD_SET_CAMERA_INFO_REQ,
}MSG_P_THD;



using namespace std; 


class  CHemuHttpsComunication:public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE( CHemuHttpsComunication);
		 CHemuHttpsComunication();
		~ CHemuHttpsComunication();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		
		void ThreadProc();
		
		void  HeMucompose_header(struct curl_slist **headerlist);
		void 	onTimer(uint arg);		
		/*
		 * 上电联网同步成功
		 */
		void 	SetNtpSuccess();
		VD_INT32 GetCurTime(string &result);
		void ComposeForm(struct curl_httppost ** post,const string & jsonObject,const string & signature);
		void ComposeMsgObject(int msgType,string &msgout);		
		void ComposeSignature(string &objson,string &signature);
		VD_INT32 sendRequest(int msgType,string &url,struct curl_slist *headerlist,
							struct curl_httppost *post,	string & result, long *statusCode);
		VD_INT32 handleResponse(int msgType,const char * resp, size_t resp_len, string & result);
		VD_INT32 SendOpenAPIReq(MSG_P_THD cmd);		
		void     GetServerInfo(string &addr, string &port, int isupdate=0);// {addr = m_cserverAddr; port = m_cserverPort; }
		void     GetDevLogInfo(DevLoginReq	&logreq);
		VD_INT32  DownLoad();
	    void 	  SetUpgradeInfo(string url, string checksum,string version,int msgSession);
	     /*
	      * 二维码信息
	      */
		VD_INT32   QRcodeInfo(string usermobile,string usertoken);
		 /*
	      *加载配置信息
	      */
		void onConfigUserInfo(CConfigHemuUeserInfo& cCfgUserInfo, int& ret);
		/*
	      *在线配置
	      */
		VD_INT32   ConfigSyncSever(CConfigTable        severconfig);
		/*升级发正在下载状态*/
//		int TimerUpgrade(int arg);
	private:
		int 	m_iTimeCount;
//		CTimer 		m_cTimer;
	private:		
		int    m_iFlag;		
		int    m_iNtpSuccess;
		string m_mac;
		string m_cDeviceID;
		string m_caccessKey;
		string m_caccessSecret;
		string m_cDeviceModelId; //设备类型
		string m_cUserMobile;  //app 用户
		string m_cUserToken;   //app usertoken
		string m_cdeviceToken; //deviceTocken
		string m_cunifiedId; //unifiedId;
		string m_cserverAddr; //serveraddr
		string m_cserverPort; //serverport
		string m_cipupdatetime; //ip更新时间
		string m_cFirmwareVersion;  
		string m_cCameraAppVersion;
		//upgrade URL
		string m_cupgradeurl;
		string m_cchecksum;
		string m_cversion;
		string m_cLastUpgradeversion;
		int    m_imsgsession;
		int    m_istartupgrade;
		int    m_iBindStatus;//0:未绑定 1：绑定  2：正在绑定
		int    m_iLastBindStatus; //前一次绑定状态
		int    m_isendcmd;  //确保每个信令发送成功，不成功重发
		int    m_iLastUpgrade; //上次是否为升级
		int    m_iLastUpgradeMsgsession; //上次升级session
		int    m_iLastUpgradeStatus; //上次升级状态
		int	   m_nConfigSynchronize; //配置同步
//		CTimer  m_ctimerupgrade;     //定时上报升级状态
		CConfigHemuUeserInfo m_userInfo;
};

#define g_HeMuHttpsCom (* CHemuHttpsComunication::instance())


#endif /**/

