#ifndef __HWCOMUNICATION__
#define __HWCOMUNICATION__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <curl/curl.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/HeMu/TcpSock.h"
#include "Intervideo/HeMu/Openssl.h"
#include "Intervideo/HeMu/AppTalk.h"
#include "Intervideo/HeMu/HemuCmd.h"



using namespace std; 

class  CHWComunication:CThread
{
	public:
		/*
	     * 连接状态
	     */
	    enum ConnectStatus
	    {
	        eUnconnected,
	        eConnecting,
	        eConnected,
	    };
		enum 
		{
		    SSL_FINISHED        =1,     /* Connection to be closed    */
		    SSL_SOCK_READABLE    =2,
		    SSL_SOCK_WRITABLE    =4,
		    SSL_SSLACCEPTED    =8,    /* SSL_accept() succeeded    */
		    SSL_ALWAYS_READY    =16    ,/* Local channel always ready for IO */
		    SSL_IO_READY       = (SSL_SOCK_READABLE | SSL_SOCK_WRITABLE)
		};	

	    /*
	     * 发数据的返回状态
	     */
	    enum ReturnStatus
	    {
	        eRetError = -1,
	        eRetOk    = 0,
	        eRetFull  = 1,
	    };
		// 认证状态：
		enum AUTH_STATE
		{
		    AUTH_INIT = -127,    // 鉴权的未初始化值
		    AUTH_SUCC = 0,       // 鉴权成功完成
		    AUTH_WAIT_RSP = 127, // 等待AUTH服务鉴权返回,这种状态下不应该收到鉴权请求
		};
		PATTERN_SINGLETON_DECLARE( CHWComunication);
		 CHWComunication();
		~CHWComunication();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		/*
		  *TLS会卡住所以用定时器控制
		  */	
		void onTimer(uint arg);
		/*定时截图*/
		void onSnapTimer(uint arg);
		VOID ThreadProc();
		VOID SetNtpSuccess(){m_iNtpSuccess = 1;}
		/*
		  * 创建连接，初始化
		  */		
		VD_INT32  Connect();
		/*
		  * 重新创建连接
		  */
		VD_INT32  Reconnect();
		/*
		  * 检查连接是否超时
		  */
		VD_INT32  DoTimeout();
		/*
		  * 设置当前连接态
		  */
		VD_INT32  SetConnected();
		/** 
	         ** 鉴权初始化请求
	         **/
		VD_INT32  DoAuthReq();
		/*
		  * 关闭连接
		  */
		VD_INT32  Close();
		/*
	     * APP 控制
	     * @return int
	     */

		VD_INT32   AppCtrol(char *msg);
	  /*
	     * HW 服务器 控制
	     * @return int
	     */
	    VD_INT32   HwCtrol();
		/*
	     * 设置分辨率
	     * @return int
	     */
		VD_INT32   SetEncodeMode(int mode);
	  /*
	     * 处理发送接收
	     *timout   毫秒
	     * @return int
	     */
		VD_INT32 DoRecvSend(uint timeout);
	   /*
	     * 摄像头参数设置
	     * @return int
	     */
	   VD_INT32   ModCameraCfg(int ctrl,int value);  
	   /*
	     * 摄像头工作时间段
	     * @return int
	     */
	   VD_INT32  CameraSchedule(int type,CConfigTable        &schedule);
	   /*
	     * LED 控制
	     * @return int
	     */
	   VD_INT32    LedCtrol(int value);
		/*
	     * 摄像机开关
	     * @return int
	     */
		VD_INT32   SetCameraStatus(int value);
	   /*
	     * 处理接收事务
	     * @return int
	     */
	  	VD_INT32   DoRecvHandle();
	   /*
	     * 处理发送事务
	     * @return int
	     */
		VD_INT32   DoSendHandle();
	  /*
	     * 处理事务
	     * @return int
	     */
		VD_INT32   Handle();
	  /*
	     * 往fd里面发送数据
	     * 如果fd缓冲区已满,返回错误
	     * 如果数据发送一半，缓冲区满了,返回成功
	     */
		VD_INT32  SendRequest(const char * pData, size_t iSize, bool forceSend);
	  /*
	     * 网络发送接口
	     * @param buf
	     * @param len
	     * @param flag
	     * @return int
	     */
		VD_INT32  Send(const void* buf, uint32_t len, uint32_t flag);
		VD_INT32  Readv(const struct iovec* vecs, int32_t vcnt);
	  /*
	     * 网络接收接口
	     * @param buf
	     * @param len
	     * @param flag
	     *
	     * @return int
	     */
		VD_INT32  recv(void* buf, uint32_t len, uint32_t flag);
		/** 
     		  ** 物理连接成功回调
     		  **/
		VOID      OnConnect();
	  /*
	     * 是否有效
	     */
	    bool isValid() const
	    {
	        return (m_fd != -1);
	    }
	  /*
	     * 判断是否正在连接
	     */
	    bool isConnecting()
	    { 
	        return isValid() && (m_connStatus == eConnecting); 
	    }
	  /*
	     * 获取系统到现在毫秒数
	     */	
		int64_t getNowMs();
	  /*
	     * 设置连接失败
	     */
	    void setConnectFailed()
	    { 
	        m_connStatus = eUnconnected; 
	    }
	  /*
	     * 连接心跳
	     */
	  	VD_INT32   HwPing();
	   /*
	     * 音视频数据
	     */
	    void   OnMediaData(int pagetype,int sync,unsigned char* data,unsigned int size,unsigned int timestamp); 
		/*
	     * 加入音视频数据缓存队列
	     */
		void   AddCacheList(int pagetype, int sync, unsigned char* data,unsigned int size,unsigned int timestamp);
		/*
	     * 打包音视频数据
	     */
	     void PackMediaData(int pagetype, int sync, unsigned char* data,unsigned int size,unsigned int timestamp,int64_t frametime);
	     /*
	     * 连接服务器状态
	     */
	     VD_INT32   GetConectStatus(){return m_connStatus;}
		/*
	     * Schedule配置变化回调函数
	     */
		 void onConfigSchedule(CConfigScheduleWorksheet& cCfgSchedule, int& ret);
		
		 /*
		  * alarm配置变化回调函数
		  */
		  void onConfigAlarm(CConfigHemuAlarmWorksheet& cCfgSchedule, int& ret);
		 /*
		  * 时间段有效性
		  */
		 void  CheckSection(int arg);
		 /*
		  * wifi列表
		  */
		 void  GetWifiList(int msgSession);
		  /*
		  * 云台位置
		  */
		 void  GetPtzPosition(int msgSession);	
		 /*
		  * SD卡信息
		  */
		 void  GetSdInfo(int msgSession);
		 /*
		  * 升级
		  */
		 int Upgrade(CConfigTable &jvaule);
		 /*
		  * 升级状态
		  */
		 int PushUgradeStatus(string version,int status,int msgSession,char *comment);
		 /*
		  * SD卡格式化
		  */	
		 int SdcardFormat(int msgSession);
		 /*
		  * SD卡格式化响应
		  */	
		 int SdcardFormatResp(int result, int msgSession);
		 /*
		  * 告警事件
		  * @param type  1：动检事件，2：声音事件，3：人脸事件
	      * @param status 0:结束  1：开始
	      * @param timestamp 时间戳  
		  */ 
		 int PushEvent(int type,int status,uint timestamp);
		 /*
		  * 修改wifi
		  */	
		 int ChangeConectWifi(int msgSession,char* Ssid,char* Passwd);
		 /*
		  * 设置连接状态
		  */
		 void SetConect(int conectstatus);// { m_iHWConect = conectstatus; m_tls.SetBreak(); }
		  /*
		  * 每天上报设备统计信息
		  */
		  void SendEeveryDayDeviceInfo();
		  /*
		  *修改分辨率通知
		  */
		  void ModResolutionNotice(int with, int height);
		  /*
		  *维护配置文件读
		  */
		  VD_BOOL  readConfig(const char* chPath, std::string& input);
		  /*
		  *初始化维护表
		  */
		  void  InitMaintainConfig();
		  /*
		  *保存维护信息
		  */
		  void  saveFile();
		  /*
		  *维护变量转json
		  */
		  void MaintainVartoJson();
		  /*
		  *维护json转变量
		  */
		  void MaintainJsontoVar();
		  /*
		  *更新维护变量
		  */
		  void MaintainUpdateVar(int updateall=0);
		  /*
		  *正在升级
		  */
		  void SetUpgradeStatus(int status){ m_iupgrade = status; }
	private:
		//睡眠
		SYSTEM_TIME m_oldTime;
		CTimer 		m_cTimer;
		CTimer      m_cSnapTimer;
		/*
		  *Ntp同步成功
		  */
		int 		m_iNtpSuccess;	

	    /*
		  * 连接的超时时间
		  */
		int64_t     	m_iconTimeoutTime;
		/*
		  * tcp sock 类
		  */
		TcpSocket   m_tcpsock;	
		/*
		  * ssl 类
		  */
		OpenSSL     m_tls;
		/* 
		  * 鉴权状态 
		  */ 
    	int         m_iauthState;
		/*
		  * 连接状态
		  */
    	ConnectStatus            m_connStatus;
		/*
	        * 套接字
	        */
    	int                      m_fd;
		/*
	        *服务器地址或者域名
	        */
	    string  				m_cserveraddr;
		/*
	        *服务器端口
	        */
	    int  					m_iport;
		/*
	        *登录失败次数
	        */
		int   					m_ifailcnt; 
		/*
	        *发送buf
	        */
		CABuffer 				m_pSendBuf;
		/*
	        *发送buf
	        */
		CABuffer 				m_pRecvBuf;
		typedef struct PACKET_CAPTURE {
				void* pBuf;
				uint size;
				int64_t timenow;
		}PACKET_CAPTURE;

		typedef struct PACKET_CAPTURE_CACHE {
				int pagetype;
				int sync;				
				CPacket* pPacket;
				unsigned int timestamp;
				int64_t frametime;
		}PACKET_CAPTURE_CACHE;

		
		//音视频队列
		typedef std::list<PACKET_CAPTURE> PACKET_LIST;
		uint m_packet_count;
		uint m_packet_size;
		uint m_packet_max_size;
		uint m_dropFramFlag;
		PACKET_LIST m_packet_list; //队列
		CMutex m_mutex_list;

		typedef std::list<PACKET_CAPTURE_CACHE> PACKET_CACHE_LIST;
		uint m_packet_cache_count;
		PACKET_CACHE_LIST m_packet_cache_list; //事件队列
		uint m_icacheIFrameFlag; //保证丢列第一帧必须是I帧
		CMutex m_mutex_cache_list;
		//ping 间隔
		int 					m_ipingspan;
		//音频序列号
		uint32_t  				m_iaudioSeq;
		//视频序列号
		uint32_t  				m_ivideoSeq;
		//控制标志，只有事件录制套餐时候需要
		uint32_t                m_icontrolflag;
   		//设备标识符
		string 					m_csrcdeviceid;
		// 目标设备标识符 
        string  		 	    m_cdstdeviceid;
		//打开视频第一帧
		uint32_t                m_iFirstIFrame;
		//pong 回复超时
		uint32_t                m_ipongtimeout;
		//对讲类
		AppTalk                 m_Talk;
		//SOCK状态
		int    					m_isockstaus;
		//发送超时10s则关闭sock
		int						m_isendout;
		//控制 类
		HemuCmd                 m_Cmd;
		//TD 关闭schedule
		CConfigScheduleWorksheet m_configScheduleWorksheet;
		CConfigHemuAlarmWorksheet m_configHemuAlarmWorksheet;
		CConfigHemuAppCtrl        m_configHemuAppCtrl;
		int   					m_iHWConect; //https设置连接装状态
		int  					m_idevicestatus; //1：云存储 2：云告警
		int 					m_ieventcount;
		uint					m_ithumbnail_span;  //截图间隔
		int						m_ithumbnail_height; //截图高
		int						m_ithumbnail_width;  //截图宽
		int						m_isdreupload;       //SD卡重传
		uint64                  m_isdstartimestamp;  //重传开始时间戳
		uint64					m_isdendtimestamp;   //重传结束时间戳
		uint64                  m_isdeventstartimestamp;  //重传开始时间戳
		uint64					m_isdeventendtimestamp;   //重传结束时间戳
		/////////////////维护接口///////////////////////////////////
		int						m_imaintainsend;          //维护发送
		uint64					m_imaintainstartimestamp; //维护开始时间ms
		uint64					m_imaintainendtimestamp;  //维护结束时间ms
		uint64					m_imaintaintotalsize;     //总数据size  MB
		uint					m_imaintaintotalVideoFrames;  //总视频帧数
		uint					m_imaintaindropVideoFrames;   //丢弃的video帧数
		uint					m_imaintaintotalAudioFrames;  //总音频帧数
		uint					m_imaintaindropAudioFrames;   //丢弃的audio帧数
		uint					m_imaintainonlineDuration;    //总在线时长
		uint				    m_imaintainlogintime;		  //在线开始时间
		uint					m_imaintaindisconnectTimes;   //掉线次数
		uint					m_imaintainrebootTimes;       //重启次数
		uint					m_imaintaindelay;			  //平均延时
		uint64                  m_imaintaintotaldelay;        //总发送延时
		uint					m_imaintaintotalsend;		  //总发送次数
		uint					m_iopenApiFailureTimes;       //调用 openapi 失败次数
		uint					m_iassignHedgwIpTimes;        //调用 获取HEDGW服务次数
		VD_BOOL					m_changed;					  //维护表变化了 
		FILE *					m_fileConfig;				  //维护文件 
		std::string				m_stream;					  //字符串流
		CConfigTable			m_maintainTable;              //维护表
		SYSTEM_TIME				m_maintainDate;				  //维护日期
		int						m_iupgrade;                   //升级状态
};

#define g_HWComm (*CHWComunication::instance())

#endif
