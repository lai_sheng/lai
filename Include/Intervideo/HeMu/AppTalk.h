#ifndef _APPTALK_H
#define _APPTALK_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"





class AppTalk : public CThread
{
public:
	AppTalk();
	~AppTalk();
public:
	VD_INT32 Start();
	VD_INT32 Stop();
	void ThreadProc();
	VD_INT32 AddTalkData(uchar* pbuf, uint size);
	VD_INT32  HandleTalkData();
private:
	typedef struct PACKET_AUDIO{
				uchar* pBuf;
				uint size;
		}PACKET_AUDIO;
	typedef std::list<PACKET_AUDIO> PACKET_LIST;
	
	uint m_packet_count;
	PACKET_LIST m_packet_list; //队列
	CMutex m_mutex_list;
	//对讲打开
	int 		m_iplayStart;
	int         m_iInit;
	int         m_itimeout;


};


#endif
