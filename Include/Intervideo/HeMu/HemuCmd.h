#ifndef _HEMUCMD_H
#define _HEMUCMD_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"





class HemuCmd : public CThread
{
public:
	HemuCmd();
	~HemuCmd();
public:
	VD_INT32 Start();
	VD_INT32 Stop();
	void ThreadProc();
	VD_INT32 SDCardFormat();
	VD_INT32 AddCmdData(int ctrl,uint value,uint value1 = 0);
	VD_INT32  HandleCmdData();
private:
	typedef struct PACKET_CMD{
				int  ctrl;  //0:短按   1：长按     2:sd卡格式化  3：PTZ快捷设置
				uint value; //1:左 2：右 3:上 4：下
				uint value1;//y轴
		}PACKET_CMD;
	typedef std::list<PACKET_CMD> PACKET_LIST;
	
	uint m_packet_count;
	PACKET_LIST m_packet_list; //队列
	CMutex m_mutex_list;
	int         m_iInit;
};


#endif
