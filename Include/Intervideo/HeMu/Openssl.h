#ifndef __OPENSSL_H__
#define __OPENSSL_H__

#include <sys/stat.h>
#include <unistd.h>
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "APIs/DVRDEF.H"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"

#include "APIs/Net.h"
#include "APIs/System.h"
#include <openssl/ssl.h>
#include <openssl/err.h>


class OpenSSL
{
	public:
		OpenSSL();
		~OpenSSL();
		static VD_INT32 Initctx();
		VD_INT32 Connect(int fd);
		void Close();
		VD_INT32 Recv(unsigned char *data, int size);
		VD_INT32 Send(unsigned char *data, int size);
		void SetBreak(){ m_ibreak = 1; }
	public:
		SSL* m_ssl;
    	int  m_fd;
		/*ǿ���˳�����*/
		int  m_ibreak;
   		static SSL_CTX* m_ctx;
};
#endif

