#ifndef __HEMURECORDMANAGER__
#define __HEMURECORDMANAGER__
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <curl/curl.h>

#include "AuxLibs/Zlib/zlib.h"
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/HeMu/TcpSock.h"
#include "Intervideo/HeMu/Openssl.h"

typedef std::basic_string<char>         ByteBuffer;

using namespace std;
class  CHemuRecordManager :CThread
{
public:
	/*
	* 连接状态
	*/
	enum ConnectStatus
	{
		eUnconnected,
		eConnecting,
		eConnected,
	};
	enum
	{
		SSL_FINISHED = 1,     /* Connection to be closed    */
		SSL_SOCK_READABLE = 2,
		SSL_SOCK_WRITABLE = 4,
		SSL_SSLACCEPTED = 8,    /* SSL_accept() succeeded    */
		SSL_ALWAYS_READY = 16,/* Local channel always ready for IO */
		SSL_IO_READY = (SSL_SOCK_READABLE | SSL_SOCK_WRITABLE)
	};

	/*
	* 发数据的返回状态
	*/
	enum ReturnStatus
	{
		eRetError = -1,
		eRetOk = 0,
		eRetFull = 1,
	};
	// 认证状态：
	enum AUTH_STATE
	{
		AUTH_INIT = -127,    // 鉴权的未初始化值
		AUTH_SUCC = 0,       // 鉴权成功完成
		AUTH_WAIT_RSP = 127, // 等待AUTH服务鉴权返回,这种状态下不应该收到鉴权请求
	};
	PATTERN_SINGLETON_DECLARE(CHemuRecordManager);
	CHemuRecordManager();
	~CHemuRecordManager();
public:
	VD_INT32 Start();
	/*
	*初始化上传列表
	*/
	void InitUploadList();
	/*
	*保存列表到SD文件
	*/
	void saveFile();
	VD_INT32 Stop();
	void onTimer(uint arg);
	/*读取配置*/
	VD_BOOL  readConfig(const char* chPath, std::string& input);
	VOID ThreadProc();
	VOID SetNtpSuccess(){ m_iNtpSuccess = 1; }
	/*
	* 创建连接，初始化
	*/
	VD_INT32  Connect();
	/*
	* 重新创建连接
	*/
	VD_INT32  Reconnect();
	/*
	* 检查连接是否超时
	*/
	VD_INT32  DoTimeout();
	/*
	* 设置当前连接态
	*/
	VD_INT32  SetConnected();
	/**
	** 鉴权初始化请求
	**/
	VD_INT32  DoAuthReq();
	/**
	** 心跳
	**/
	VD_INT32  HwPing();
	/*
	* 关闭连接
	*/
	VD_INT32  Close();
	/*
	* 处理发送接收
	*timout   毫秒
	* @return int
	*/
	VD_INT32 DoRecvSend(uint timeout);
	/*
	* 处理接收事务
	* @return int
	*/
	VD_INT32   DoRecvHandle();
	/*
	* 处理发送事务
	* @return int
	*/
	VD_INT32   DoSendHandle();
	/*
	* 处理事务
	* @return int
	*/
	VD_INT32   Handle();
	/*
	* 获取系统当前毫秒数
	*/
	int64_t getNowMs();
	/*
	* 网络发送接口
	* @param buf
	* @param len
	* @param flag
	* @return int
	*/
	VD_INT32  Send(const void* buf, uint32_t len, uint32_t flag);

	/*
	* 是否有效
	*/
	bool isValid() const
	{
		return (m_fd != -1);
	}
	/*
	* 判断是否正在连接
	*/
	bool isConnecting()
	{
		return isValid() && (m_connStatus == eConnecting);
	}

	/*
	* 设置连接失败
	*/
	void setConnectFailed()
	{
		m_connStatus = eUnconnected;
	}
	/*
	*打包数据包
	*/
	void PackMediaData(int pagetype, int sync, unsigned char* data, unsigned int size, unsigned int timestamp, int64_t frametime);
	/*
	*https设置连接装状态
	*/
	void SetConect(int conectstatus){ m_iHWConect = conectstatus; }
	/*
	*是否阻塞
	*/
	int  _IsBlock(int error_code);
	/*
	*打开录像文件
	*/
	void OpenRecordFile(int type, uint  recordstarttime, uint recordenttime);
	/*
	*SPS长度
	*/
	VD_UINT32 FindIunit(uchar * buf, uint size);
	/*
	*读取录像文件
	*/
	void ReadRecordFile();

	/*
	*重传录像文件开始结束时间戳
	*/
	void ReUploadTime(int type, int status, uint timestamp, int update=0, uint updatetimestamp=0,int errordate=0/*误差值*/);

	/*
	*SD卡状态查询
	*/
	void SDStatus();
	/*
	*重传状态查询
	*/
	void   DectectConfig(int openfile = 0);
	/*
	*推送事件
	*/
	int PushEvent(int type, int status, uint timestamp);
private:
	CTimer 		m_cTimer;
	/*
	*Ntp同步成功
	*/
	int 		m_iNtpSuccess;

	/*
	* 连接的超时时间
	*/
	int     	m_iconTimeoutTime;
	/*
	* tcp sock 类
	*/
	TcpSocket   m_tcpsock;
	/*
	* ssl 类
	*/
	OpenSSL     m_tls;
	/*
	* 鉴权状态
	*/
	int         m_iauthState;
	/*
	* 连接状态
	*/
	ConnectStatus            m_connStatus;
	/*
	* 套接字
	*/
	int                      m_fd;
	/*
	*服务器地址或者域名
	*/
	string  				m_cserveraddr;
	/*
	*服务器端口
	*/
	int  					m_iport;
	/*
	*登录失败次数
	*/
	int   					m_ifailcnt;
	/*
	 *发送数据锁
	 */
	CMutex					m_Mutex;
	/*
	*发送buf
	*/
	ByteBuffer 				m_pSendBuf;
	/*
	*接收buf
	*/
	CABuffer 				m_pRecvBuf;
	//设备状态
	int						m_idevice_status;
	//心跳间隔
	int						m_ipingspan;
	//音频序列号
	uint32_t  				m_iaudioSeq;
	//视频序列号
	uint32_t  				m_ivideoSeq;
	//I帧
	int						m_iIFrameFlag;
	//pong 回复超时
	uint32_t                m_ipongtimeout;
	//SOCK状态
	int    					m_isockstaus;
	//主链接状态
	int   					m_iHWConect;
	//需要重传数据
	int						m_ireload;
	//打开录像文件
	int						m_iOpenFile;
	//当前视频类型			
	int						m_itype;
	//当前查询开始时间戳
	uint					m_iQureyStartTimeStamp;
	//当前查询结束时间戳
	uint					m_iQureyEndTimeStmap;
	//当前
	//当前读取视频帧数
	int						m_iVideoCurFrame;
	//当前读取音频帧数
	int						m_iAudioCurFrame;
	//当前文件时长
	int						m_iRecrdTimeDuration;
	//当前文件开始时间戳
	uint						m_iRecrdStartTimeStamp;
	//事件时间戳，夸文件
	uint					m_iSeekTimeStamp;
	//读取pBuf
	uchar					*m_pBuf;
	//record tm
	struct tm				m_record_tm;
	char                    testh264filename[128];

	///////////////file list///////////////////
	typedef struct OFFLINE_EVENT_tag {
		int type; //1:云录像  2：云告警
		uint start_times;
		uint end_times;
	}OFFLINE_EVENT;


	//音视频队列
	typedef std::list<OFFLINE_EVENT> OFFLINE_EVENT_LIST;
	uint m_event_count;
	OFFLINE_EVENT_LIST m_event_list; //队列
	CMutex m_mutex_eventlist;
	
	//SD卡状态
	int					m_iSdStatus;

	VD_BOOL m_changed;						/*!< 配置表变化了 */
	gzFile m_fileConfig;					/*!< 配置文件 */
	std::string m_stream;					// 字符串流
	CConfigTable m_listAll;                /*!< 配置总表 */
};

#define g_HMRecordManager (*CHemuRecordManager::instance())
#endif


