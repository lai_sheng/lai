#ifndef __TCPSOCK_H__
#define __TCPSOCK_H__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <vector>
#include <string>
#include <sys/un.h>
#include "APIs/DVRDEF.H"

using namespace std; 
/**
* @brief  Socket类, 封装了socket的基本方法
*/
class TcpSocket
{
public:
    /**
     * @brief  构造函数
     */
    TcpSocket();

    /**
     * @brief  析够
     */
    ~TcpSocket();



    /**
    * @brief  生成socket, 如果已经存在以前的socket, 则释放掉, 
    *         生成新的.
    *  
    * @return
    */
    int createSocket();

    /**
    * @brief 获取socket句柄. 
    *  
    * @return  socket句柄
    */
    int getfd() const { return m_isock; }

    /**
    * @brief  socket是否有效. 
    *  
    * @return true标识有效，否则返回false
    */
    bool isValid() const { return m_isock != INVALID_SOCKET; }

    /**
    * @brief  关闭socket. 
    *  
    * @return void
    */
    void Close();


    /**
    * @brief  修改socket选项. 
    *  
    * @param opt       选项名称
    * @param pvOptVal  选项值指针
    * @param optLen    pvOptVal对应的长度
    * @param level     socket操作层次, 默认是socket层
    * @return int
    */
    int setSockOpt(int opt, const void *pvOptVal, socklen_t optLen, int level = SOL_SOCKET);

    /**
    * @brief  获取socket选项值. 
    *  
    * @param opt       选项名称
    * @param pvOptVal  输出, 选项值指针
    * @param optLen    输入pvOptVal指向的缓存的长度
    * @param level     socket操作层次, 默认是socket层
    * @return          socket选项值
    */
    int getSockOpt(int opt, void *pvOptVal, socklen_t &optLen, int level = SOL_SOCKET);




    /**
     * @brief 发起连接，连接失败的状态不通过异常返回, 
     *        通过connect的返回值,在异步连接的时候需要
     * @param sServerAddr  ip地址
     * @param port         端口
     * @throws             TC_Socket_Exception: 
     *                     其他错误还是通过异常返回(例如),例如地址错误
     * @return int
     */
    int connectServer(const string &sServerAddr, uint16_t port);


    /**
    * @brief  接收数据(一般用于tcp). 
    *  
    * @param pvBuf  接收buffer
    * @param iLen   buffer长度
    * @param iFlag  标示
    * @return int   接收的数据长度
    */
    int  recv(void *pvBuf, size_t iLen, int iFlag = 0);

    /**
    * @brief  发送数据(一般用于tcp). 
    *  
    * @param pvBuf 发送buffer
    * @param iLen  buffer长度
    * @param iFlag 标示
    * @return int  发送了的数据长度
    */
    int  send(const void *pvBuf, size_t iLen, int iFlag = 0);

  
    /**
    * @brief 关闭. 
    *  
    * @param iHow 关闭方式:SHUT_RD|SHUT_WR|SHUT_RDWR
    * @return
    */
    void shutdown(int iHow);

    /**
    * @brief 设置socket方式 .
    *  
    * @param   bBlock true, 阻塞; false, 非阻塞
    * @return
    */
    void setblock(bool bBlock = false);

    /**
    * @brief 设置非closewait状态, 可以重用socket. 
    *  
    * @return void
    */
    void setNoCloseWait();

    /**
     * @brief 设置为closewait状态, 最常等待多久. 
     *  
     * @param delay  等待时间秒
     */
    void setCloseWait(int delay = 30);

    /**
     * @brief 设置closewait缺省状态， 
     *        close以后马上返回并尽量把数据发送出去
     */
    void setCloseWaitDefault();

    /**
     * @brief 设置nodelay(只有在打开keepalive才有效).
     *  
     */
    void setTcpNoDelay();

    /**
     * @brief 设置keepalive. 
     *  
     * @return 
     */
    void setKeepAlive();

    /**
    * @brief 获取recv buffer 大小. 
    *  
    * @return recv buffer 的大小
    */
    int getRecvBufferSize();

    /**
    * @brief 设置recv buffer 大小. 
    * @param  recv buffer 大小 
    */
    void setRecvBufferSize(int sz);

    /**
    * @brief 获取发送buffer大小.
    * @param 发送buffer大小  
     */
    int getSendBufferSize();

    /**
     * @brief 设置发送buffer大小. 
     *  
     * @param  发送buffer大小
     */
    void setSendBufferSize(int sz);


    /**
    * @brief 设置socket方式. 
    *  
    * @param fd      句柄
    * @param bBlock  true, 阻塞; false, 非阻塞
    * @return
    */
    static void setblock(int fd, bool bBlock);

    /**
     * @brief 生成管道,抛出异常时会关闭fd. 
     *  
     * @param fds    句柄
     * @param bBlock true, 阻塞; false, 非阻塞
     */
    static int parseAddr(const string &sAddr, struct in_addr &stAddr);




protected:

    /**
    * @brief 连接其他服务. 
    *  
    * @param pstServerAddr  服务地址
    * @param serverLen      pstServerAddr指向的结构的长度
    * @return int
    */
    int connect(struct sockaddr *pstServerAddr, socklen_t serverLen);    

 

private:

protected:
    static const int INVALID_SOCKET = -1;

    /**
     * socket句柄
     */
    int  m_isock;

};


#endif