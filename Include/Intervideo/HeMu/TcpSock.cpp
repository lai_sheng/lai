#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <cerrno>
#include <cassert>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "Intervideo/HeMu/TcpSock.h"

TcpSocket::TcpSocket() : m_isock(INVALID_SOCKET)
{
}

TcpSocket::~TcpSocket()
{
    if(m_bOwner)
    {
        close();
    }
}


int TcpSocket::createSocket()
{
    close();

    m_isock = socket(SOCK_STREAM, AF_INET, 0);

    if(m_isock < 0)
    {
        m_isock = INVALID_SOCKET;
        _printd("[TcpSocket::createSocket] create socket error! :" + string(strerror(errno)));
		return FALSE;
	}
	return TRUE;
}


int TcpSocket::parseAddr(const string &sAddr, struct in_addr &stSinAddr)
{
    int iRet = inet_pton(AF_INET, sAddr.c_str(), &stSinAddr);
    if(iRet < 0)
    {
        _printd("[TcpSocket::parseAddr] inet_pton error:%d", errno);
		return FALSE;
    }
    else if(iRet == 0)
    {
        struct hostent stHostent;
        struct hostent *pstHostent;
        char buf[2048] = "\0";
        int iError;

        gethostbyname_r(sAddr.c_str(), &stHostent, buf, sizeof(buf), &pstHostent, &iError);

        if (pstHostent == NULL)
        {
            _printd("[TcpSocket::parseAddr] gethostbyname_r error! :" + string(hstrerror(iError)));
			return FALSE;
		}
        else
        {
            stSinAddr = *(struct in_addr *) pstHostent->h_addr;
        }
    }
	return TRUE;
}

void TcpSocket::close()
{
    if (m_isock != INVALID_SOCKET)
    {
        ::close(m_isock);
        m_isock = INVALID_SOCKET;
    }
}

int TcpSocket::connectServer(const string &sServerAddr, uint16_t port)
{
	if(!isValid() && !createSocket())
	{
		//sock 无效且创建sock失败
		_printd("socket invalid!!!");
		return FALSE;
	}
    if (sServerAddr == "")
    {
        _printd("[TcpSocket::connect] server address is empty error!");
		return FALSE;
    }

    struct sockaddr_in serverAddr4;
    struct sockaddr *serverAddr =  (struct sockaddr *)&serverAddr4;
    socklen_t len = sizeof(serverAddr4);

    bzero(serverAddr, len);


    serverAddr4.sin_family = AF_INET;
    int ret = parseAddr(sServerAddr, serverAddr4.sin_addr);
	if(ret)
	{
	    serverAddr4.sin_port = htons(port);

	    return connect(serverAddr, len);
	}
	return FALSE;
}



int TcpSocket::connect(struct sockaddr *pstServerAddr, socklen_t serverLen)
{
    return ::connect(m_isock, pstServerAddr, serverLen);

}

int TcpSocket::recv(void *pvBuf, size_t iLen, int iFlag)
{
    return ::recv(m_isock, pvBuf, iLen, iFlag);
}

int TcpSocket::send(const void *pvBuf, size_t iLen, int iFlag)
{
    return ::send(m_isock, pvBuf, iLen, iFlag);
}

void TcpSocket::shutdown(int iHow)
{
    if (::shutdown(m_isock, iHow) < 0)
    {
        _printd("[TcpSocket::shutdown] shutdown error:%d", errno);
    }
}

void TcpSocket::setblock(bool bBlock)
{
    assert(m_isock != INVALID_SOCKET);

    setblock(m_isock, bBlock);
}

int TcpSocket::setSockOpt(int opt, const void *pvOptVal, socklen_t optLen, int level)
{
    return setsockopt(m_isock, level, opt, pvOptVal, optLen);
}

int TcpSocket::getSockOpt(int opt, void *pvOptVal, socklen_t &optLen, int level)
{
    return getsockopt(m_isock, level, opt, pvOptVal, &optLen);
}

void TcpSocket::setNoCloseWait()
{
    linger stLinger;
    stLinger.l_onoff = 1;  //在close socket调用后, 但是还有数据没发送完毕的时候容许逗留
    stLinger.l_linger = 0; //容许逗留的时间为0秒

    if(setSockOpt(SO_LINGER, (const void *)&stLinger, sizeof(linger), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setNoCloseWait] error:%d", errno);
    }
}

void TcpSocket::setCloseWait(int delay)
{
    linger stLinger;
    stLinger.l_onoff = 1;  //在close socket调用后, 但是还有数据没发送完毕的时候容许逗留
    stLinger.l_linger = delay; //容许逗留的时间为delay秒

    if(setSockOpt(SO_LINGER, (const void *)&stLinger, sizeof(linger), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setCloseWait] error:%d", errno);
    }
}

void TcpSocket::setCloseWaitDefault()
{
    linger stLinger;
    stLinger.l_onoff  = 0;
    stLinger.l_linger = 0;

    if(setSockOpt(SO_LINGER, (const void *)&stLinger, sizeof(linger), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setCloseWaitDefault] error:%d", errno);
    }
}

void TcpSocket::setTcpNoDelay()
{
    int flag = 1;

    if(setSockOpt(TCP_NODELAY, (char*)&flag, int(sizeof(int)), IPPROTO_TCP) == -1)
    {
        _printd("[TcpSocket::setTcpNoDelay] error:%d", errno);
    }
}

void TcpSocket::setKeepAlive()
{
    int flag = 1;
    if(setSockOpt(SO_KEEPALIVE, (char*)&flag, int(sizeof(int)), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setKeepAlive] errorz:%d", errno);
    }
}

void TcpSocket::setSendBufferSize(int sz)
{
    if(setSockOpt(SO_SNDBUF, (char*)&sz, int(sizeof(int)), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setSendBufferSize] error:%d", errno);
    }
}

int TcpSocket::getSendBufferSize()
{
    int sz;
    socklen_t len = sizeof(sz);
    if(getSockOpt(SO_SNDBUF, (void*)&sz, len, SOL_SOCKET) == -1 || len != sizeof(sz))
    {
        _printd("[TcpSocket::getSendBufferSize] error:%d", errno);
    }

    return sz;
}

void TcpSocket::setRecvBufferSize(int sz)
{
    if(setSockOpt(SO_RCVBUF, (char*)&sz, int(sizeof(int)), SOL_SOCKET) == -1)
    {
        _printd("[TcpSocket::setRecvBufferSize] error:%d", errno);
    }
}

int TcpSocket::getRecvBufferSize()
{
    int sz;
    socklen_t len = sizeof(sz);
    if(getSockOpt(SO_RCVBUF, (void*)&sz, len, SOL_SOCKET) == -1 || len != sizeof(sz))
    {
        _printd("[TcpSocket::getRecvBufferSize] error:%d", errno);
    }

    return sz;
}

void TcpSocket::setblock(int fd, bool bBlock)
{
    int val = 0;

    if ((val = fcntl(fd, F_GETFL, 0)) == -1)
    {
        _printd("[TcpSocket::setblock] fcntl [F_GETFL] error:%d", errno);
    }

    if(!bBlock)
    {
        val |= O_NONBLOCK;
    }
    else
    {
		int ret = 0;
        val &= ~O_NONBLOCK;
		struct timeval timeout;
		timeout.tv_sec = 3;
		timeout.tv_usec = 0;

		int len = sizeof(timeout);
		ret = setsockopt(m_fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, len);
		ret = setsockopt(m_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, len);

    }

    if (fcntl(fd, F_SETFL, val) == -1)
    {
        _printd("[TcpSocket::setblock] fcntl [F_SETFL] error:%d", errno);
    }
}






