#ifndef _HEMUDMMANAGER_H
#define _HEMUDMMANAGER_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/MobileCountry/lwm2msdk.h"

#define DM_FILE			("DmConfig")

#define DMTYPE_STR		("DMParam")

#define DM_ENV_STR		("Environment")
#define CMEI_STR		("CMEI")
#define DM_APPKEY_STR	("Account")
#define DM_PSW_STR		("Password")
#define RT_CMEI_LEN             15


#define  DM_SV_TEST_ADDR     "shipei.fxltsbl.com"   //适配sv平台
#define  DM_SV_TEST_PORT     5683
#define  DM_SV_FORMAL_ADDR   "m.fxltsbl.com"        //商用sv平台
#define  DM_SV_FORMAL_PORT   5683
#define  DM_DEV_APPKEY       "M100000141"
#define  DM_DEV_PWD          "n88090RXM06t9J0c699MS4P998Qr069h"
#define  DM_DEV_TAC_TUI      "11100145"             //dev tac|tui
#define  DM_DEV_VERSION      "v2.0"       
#define  DM_DEV_TIMEOUT_MIN  10          

typedef enum{
	DM_MODE_TEST = 0,   //test
	DM_MODE_FORMAL = 1,   //正式环境
}DM_SV_MODE;



class DmManager : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE( DmManager);
	DmManager();
	~DmManager();
public:
	VD_INT32 Start();
	VD_INT32 Stop();
	int DM_Start(char *pCMEI, char *pAPP, char *pPSW, int timeout, DM_SV_MODE enMode);
	void DM_Stop();
	int  GetDeviceCMEI(char cmei[RT_CMEI_LEN]);
	int  DM_GetRunStat();
	void ThreadProc();

	int         m_iInit;
};

#define g_DmManager (*DmManager::instance())
#endif
