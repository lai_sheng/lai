#ifndef _MOBILE_COUNTRY_SDK_API_H
#define _MOBILE_COUNTRY_SDK_API_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"
#include "Configs/ConfigCamera.h"

#include "Intervideo/MobileCountry/MobileCountryAppTalk.h"
#include "Intervideo/MobileCountry/camerasdk/dot_define.h"
#include "Intervideo/MobileCountry/iotsdk/nt_iot_define.h"
#include "Intervideo/MobileCountry/camerasdk/dot_callback.h"

#define  SDK_CONFIG_FILE_PATH "/mnt/temp"
#ifdef IPC_JZ
#define  SDK_CA_FILE "/app/ca-bundle-add.crt" //"/tmp/SDdir/ca-bundle-add.crt"//"/app/ca-bundle-add.crt"
#else
#define  SDK_CA_FILE "/app/ipcam/ca-bundle-add.crt"
#endif
#define  SDK_LOG_FILE_PATH "/tmp"
enum API_INIT
{
	NOT_INIT = 0, //未初始化
	INIT_OK = 1,  //初始话成功
};

#define OUTPUT_LOG(format,...) print_log(__FILE__,__FUNCTION__,__LINE__, format, ## __VA_ARGS__)
void print_log(const char* file, const char* fun, int line, const char* format,...);

class CMobileCountrySdkApi : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CMobileCountrySdkApi);
	CMobileCountrySdkApi();
	~CMobileCountrySdkApi();
public:
	/*启动*/
	VD_INT32 Start();
	/*乡村平台SDK 初始化*/
	VD_INT32 MobileCountrySdkInit();
	/*获取网卡地址*/
	VD_INT32 GetMmacAddress(const char *ifname, char *mac);
	/*初始化SDK参数*/
	VD_INT32 InitSDKParams(DoT_InitParams_st* init_params);
	/*获取版本号*/
	VD_INT32 GetVersion();
	/*设置设备能力集*/
	VD_INT32 SetDeviceFeature();
	/*设置回调函数*/
	void  InitSDKCallback(DoT_CallbackFuncList_st* cb_list);
	/*设置设备能力*/
	void InitDeviceCapbility(DoT_DeviceHWCapability_st* hw_capbility, 
                                		DoT_DeviceSWCapability_st* sw_capbility);
	/*低功耗启动SDK*/
	int LowPowerStartSDK(DoT_WakeupReason_en reason);
	/*stop SDK*/
	int StopSDK();
	/*注册*/
	int StartBind(int mode);
	/*停止注册*/
	int StopBind();
	/*停止*/
	VD_INT32 Stop();
	/*线程体*/
	void ThreadProc();
	/*获取毫秒1970到现在数据*/
	int64_t  getNowMs();
	/*媒体数据*/
	void OnMediaData(int pagetype, int sync, unsigned char* data, unsigned int size, unsigned int timestamp);
	/*设置宽高*/
	void SetImageSize(int nWidth, int nHeight){ m_nImageWidth = nWidth; m_nImageHeight = nHeight; }
	/*设置参数*/
	void SetParameter(int type, int value);
	/*对讲数据*/
	void AppendTalkAudioData(unsigned char *pBuf,int nDataLen);
	/*IOT事件上传*/
	void SendIOTEvent(NTIOT_EventType_en    type);
	/*Alarm事件开始*/
	void StartAlarm(DoT_AlarmType_en type);
	/*Alarm事件结束*/
	void StopAlarm(DoT_AlarmType_en type);
	/*恢复API出厂默认配置*/
	void ApiFactoryReset();
	/*截图*/
	void SnapImage();
	/*设置NTP 成功标志*/
	void SetNtpSuccess(){m_nNtpSuccess = 1; }
private:
	/*初始化变量*/
	int         m_nSdkInit;

	/*分辨率*/
	int			m_nImageWidth; /*宽*/
	int			m_nImageHeight;/*高*/
	/*升级状态*/
	int			m_nUpgrade; //0：未升级 1:正在升级
	/*SDK 控制发送状态*/
	int			m_nSendControl; //0:不发送  1：发送
	/*发送视频序列号*/
	int         m_nVideoSeq;
	/*发送音频序列号*/
	int			m_nAudioSeq;
	/*第一帧视频帧*/
	int         m_nFirstKeyFrame;
	/*SD 卡状态*/
	int			m_nSdStatus;
	/*对讲*/
	AppTalk		m_oTalk;
	/*延后初始化减少BUG*/
	int m_nNtpTimeOut;
	int m_nNtpSuccess;

	std::string m_strFirmwareVersion;
	std::string m_strCameraAppVersion;
};

#define g_MobileCountrySdkApi (* CMobileCountrySdkApi::instance())
#endif