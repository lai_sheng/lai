#ifndef _MOBILE_COUNTRY_SDK_CALLBACK_H
#define _MOBILE_COUNTRY_SDK_CALLBACK_H

#include "Intervideo/MobileCountry/camerasdk/dot_define.h"
#include "Intervideo/MobileCountry/camerasdk/dot_callback.h"
#ifdef __cplusplus
extern "C" {
#endif

int cbSetDevConfig(DoT_SetDevConfigCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);
int cbGetDevRunningInfo(DoT_GetDevRunningInfo_en info_e, DoT_uint32 channel_id, void* data, void* user_data);
int cbControlDevCmd(DoT_ControlDevCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);
int cbAudioPlayCmd(DoT_AudioPlayCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);
int cbPTZControl(DoT_PTZControl_en cmd, DoT_uint32 channel_id, void* data, void* user_data);
int cbServerStatus(DoT_ServerStatusMsg_en msg, DoT_uint32 channel_id, void* data, void* user_data);
int cbUpgrade(DoT_UpgradeCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);
int cbDevBindStatus(DoT_BindStatus_en status, void* data, void* user_data);

#ifdef __cplusplus
} 
#endif

#endif