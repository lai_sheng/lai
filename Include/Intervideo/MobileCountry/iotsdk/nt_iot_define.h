#ifndef __NTIOT_DEFINE_H__
#define __NTIOT_DEFINE_H__


#define NTIOT_MAX_PATH_LEN                            256     /**< 最大路径长度 */
#define NTIOT_MAX_MAC_LEN                             32      /**< MAC地址最大长度 */
#define NTIOT_MAX_VERSION_LEN                         50      /**< 版本号最大长度 */
#define NTIOT_MAX_ID_LEN                              64      /**< 序列号最大长度 */
#define NTIOT_MAX_SERVER_ADDRESS_LEN                  256     /**< 地址最大长度 */

#define NTIOT_EC_SUCCESS  0                               /**< 成功 */


#define NTIOT_FALSE  0        /**< 基础类型定义 */
#define NTIOT_TRUE   1        /**< 基础类型定义 */

typedef char                    NTIOT_bool;       /**< 基础类型定义 */
typedef unsigned int            NTIOT_uint32;     /**< 基础类型定义 */
typedef int                     NTIOT_int32;      /**< 基础类型定义 */
typedef unsigned long long      NTIOT_uint64;     /**< 基础类型定义 */
typedef long long               NTIOT_int64;      /**< 基础类型定义 */
typedef char                    NTIOT_char;       /**< 基础类型定义 */
typedef unsigned char           NTIOT_uchar;      /**< 基础类型定义 */
typedef void                    NTIOT_void;       /**< 基础类型定义 */
typedef float                   NTIOT_float;      /**< 基础类型定义 */

typedef enum {
    NTIOT_DOORLOCK_FINGER_PRINT_UNLOCK = 1000,                ///< 指纹开锁
    NTIOT_DOORLOCK_PASSWORD_UNLOCK = 1001,                    ///< 密码开锁
    NTIOT_DOORLOCK_KEY_UNLOCK = 1002,                         ///< 钥匙开锁
    NTIOT_DOORLOCK_DOOR_CARD_UNLOCK = 1003,                   ///< 门卡开锁    
    NTIOT_DOORLOCK_TEMPORARY_PASSWD_UNLOCK = 1004,            ///< 临时密码开锁

    NTIOT_DOORLOCK_FINGER_PRINT_ERR = 1010,                   ///< 指纹多次错误
    NTIOT_DOORLOCK_PASSWORD_ERR = 1011,                       ///< 密码多次错误    
    NTIOT_DOORLOCK_DOOR_CARD_ERR = 1012,                      ///< 门卡多次错误    
    NTIOT_DOORLOCK_HIJACK_UNLOCK = 1013,                      ///< 挟持开锁    
    NTIOT_DOORLOCK_TAMPER_UNLOCK = 1014,                      ///< 暴力开锁    
} NTIOT_EventType_en;

typedef enum {
    NTIOT_DOORLOCK_FINGER_PRINT_TYPE = 1,                ///< 指纹类型
    NTIOT_DOORLOCK_PASSWORD_TYPE = 2,                    ///< 密码类型
} NTIOT_DoorLockInfoType_en;

typedef enum {
    NT_IOT_DEVICE_TYPE_DOORLOCK = 1,
}NTIOT_DeviceType_en;

typedef enum {
    NTIOT_BATTERY_STATUS_NULL,                    /// < 表示取不到电池信息；
    NTIOT_BATTERY_STATUS_LITE,                    /// < 表示低电量；
    NTIOT_BATTERY_STATUS_CHARGING,                /// < 表示正在充电；
    NTIOT_BATTERY_STATUS_FULL,                    /// < 表示电池充满了；
    NTIOT_BATTERY_STATUS_NORMAL                   /// < 表示其它正常状态
} NTIOT_BatteryStatus_en;

/** 设备控制命令回调 */
typedef enum {
    NTIOT_CMD_GET_DOORLOCK_UPDATE_INFO,                ///< 强制同步门锁信息列表，门锁收到消息后，主动将本地门锁列表同步到服务器（调用NTIOT_PushDoorLockInfoList()函数）
    NTIOT_CMD_SET_TEMP_PASSWORD_KEY,                   /// < 设置门锁生成临时密码的密钥，回调指针类型：char*
    NTIOT_CMD_GET_BATTERY_VALUE,              /// < 获取门锁电池电量值，回调指针类型：NTIOT_BatteryValue_st 
} NTIOT_CMD_Control_en;    

/** 上报密码或指纹列表的状态回调 */
typedef enum {
    NTIOT_DOORLOCK_UPDATE_INFO_SUCCEED,               /// < 同步门锁信息列表成功，回调指针类型：NULL
    NTIOT_DOORLOCK_UPDATE_INFO_FAIL,                  /// < 同步门锁信息列表失败，需通过 NTIOT_PushDoorLockInfoList() 函数上报，回调指针类型：NULL
} NTIOT_UpdateInfoStatus_en;


typedef struct {
    NTIOT_BatteryStatus_en status;            ///< 事件类型为 CC_DETECT_BATTERY 时有效 
    NTIOT_int32 percent;                              ///< 电量百分比，范围0~100
} NTIOT_BatteryValue_st;

typedef struct {    
    union {
        struct {
            NTIOT_int32                 id;             ///< type为指纹或密码开锁事件时有效，表示指纹或密码的编号
        }doorlock_event_attr;                           ///< 门锁事件属性，type为指纹或密码开锁事件时有效
    };
    NTIOT_EventType_en          type;                 ///< 事件类型，参考NTIOT_EventType_en枚举类型
    NTIOT_uint64    systime;                          ///< 事件发生时间，UTC时间，毫秒，0表示使用当前时间
} NTIOT_EventInfo_st;

typedef struct {    
    NTIOT_int32                 battery;              ///< 电量百分比，0~100
} NTIOT_Battey_st;

typedef struct {
    NTIOT_int32                 id;                   ///< type为指纹或密码开锁事件时有效，表示指纹或密码的编号
    NTIOT_DoorLockInfoType_en   type;                 ///< 门锁类型：指纹     或密码，参考NTIOT_DoorLockInfoType_en枚举类型
} NTIOT_DoorLockInfo_st;

typedef struct {
    NTIOT_int32 info_num;                             ///< 指纹或密码编码信息个数
    NTIOT_DoorLockInfo_st *info;                      ///< 指纹或密码编码信息
} NTIOT_DoorLockInfoList_st;


/** 设备初始化信息 */
typedef struct {
    NTIOT_char            fw_version[NTIOT_MAX_VERSION_LEN];         /**< 设备固件版本 */
    NTIOT_char            mac[NTIOT_MAX_MAC_LEN];                    /**< 设备MAC地址，不带冒号，采用小写字母格式 */
    NTIOT_char            serial_number[NTIOT_MAX_ID_LEN];           /**< 设备序列号，由平台预先分配 */
    NTIOT_char            device_model[64];                          /**< 设备型号ID，由平台预先分配 */
} NTIOT_InitParams_st;


/**
 * 门锁控制命令回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef NTIOT_int32 (*NTIOT_Control_Callback)(NTIOT_CMD_Control_en cmd, void* data, void* user_data);

/**
 * 上报密码或指纹列表的状态回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef NTIOT_int32 (*NTIOT_UpdateInfoStatus_Callback)(NTIOT_UpdateInfoStatus_en status, void* data, void* user_data);

/** 回调函数指针列表 */
typedef struct {
    NTIOT_Control_Callback                cb_iot_control;     /**< 门锁控制命令回调 */
    NTIOT_UpdateInfoStatus_Callback       cb_iot_update_info_status;  /**< 上报密码或指纹列表的状态回调 */
} NTIOT_CallbackFuncList_st;


#endif
