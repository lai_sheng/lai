#ifndef __NTIOT_API_H__
#define __NTIOT_API_H__

#ifdef __cplusplus
  extern "C" {
#endif

#include "nt_iot_define.h"


/**
 * NTIOT SDK初始化函数
 * @param [in] init_param  初始化参数
 * @param [in] cb  回调函数列表指针
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
NTIOT_int32 NTIOT_Init(NTIOT_InitParams_st* init_param, NTIOT_CallbackFuncList_st* cb);

/**
 * 启动 SDK服务，初始化之后调用
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
NTIOT_int32 NTIOT_Start(void);

/**
 * 停止 SDK服务，反初始化之前调用
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
NTIOT_int32 NTIOT_Stop(void);

/**
 * SDK反初始化函数
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
NTIOT_int32 NTIOT_Unit(void);

/**
 * 上报DoorLock全量的密码或指纹列表，当密码或者指纹列表有变化时调用。如果上报成功，下次启动则无需再次调用
 * @param [in] list  上报全量的密码或指纹列表信息
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
NTIOT_int32 NTIOT_PushDoorLockInfoList(NTIOT_DoorLockInfoList_st*  list);


/**
 * 上报设备事件信息
 * @param [in] event  上报设备事件
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
NTIOT_int32 NTIOT_PushEvent(NTIOT_EventInfo_st* event);

/**
 * 上报设备电量值
 * @param [in] battery  上报电量值
 * @return 返回函数执行结果
 * - NTIOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
NTIOT_int32 NTIOT_PushBatteryStatus(NTIOT_Battey_st*  info);


#ifdef __cplusplus
}
#endif


#endif /* __NTIOT_API_H__ */

