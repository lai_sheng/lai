


#ifndef __CC_DEVICES_H__
#define __CC_DEVICES_H__
#ifndef __HuaweiLite__
#include <stdbool.h>
#endif

#include "cc_datatypes.h"

/**
 * @文件名       cc_device.h
 * @作者
 * @日期
 *
 * @简介  数据结构定义
 *
 *
 */

CC_CDECLS_BEGIN

#define CC_VERSION_MAX_LEN 40
#define CC_PRODUCTION_INFO_MAX_LEN 40
#define CC_CHIP_NAME_MAX_LEN 40
#define CC_MAC_MAX_LEN 40
#define CC_SERIAL_NUMBER_MAX_LEN 40

typedef struct cc_product_info {
    cc_char product_module_id[CC_PRODUCTION_INFO_MAX_LEN];   ///< 产品module id，表示设备型号，使用前需申请
    cc_char product_name[CC_PRODUCTION_INFO_MAX_LEN];        ///< 产品型号名字，显示在客户端设备详情页面
} cc_product_info;

typedef struct cc_device_info {
    cc_char    device_type[CC_CHIP_NAME_MAX_LEN];  ///< 设备类型，目前支持的类型有： IPC(网络摄像头), Doorbell（门铃）, LowPowerCam（低功耗摄像头）, RelaySingle(中继一拖一)
    cc_char    chip_name[CC_CHIP_NAME_MAX_LEN];    ///< 设备芯片名字：例如Hi3518ev200,Hi3518ev201,
    cc_char    app_version[CC_VERSION_MAX_LEN];    ///< 设备主应用程序版本号
    cc_char    fw_version[CC_VERSION_MAX_LEN];     ///< 设备固件版本号
    cc_char    mac[CC_MAC_MAX_LEN];                ///< 设备mac地址，不带分隔符，全小写，例如：001122aabbcc
    cc_char    sn[CC_SERIAL_NUMBER_MAX_LEN];       ///< 设备序列号，设备唯一标识码，使用前需申请
    cc_uint32  stream_num;                         ///< 设备的码流数，设置为1
}cc_device_info;

typedef enum cc_device_cmd_e
{
    CC_CMD_LED_SWITCH,                             ///< 指示灯开关，关（0），开（1）
    CC_CMD_REBOOT,                                 ///< 设备重启
    CC_CMD_PACK_LOGS,                              ///< 获取设备日志信息，设备需要支持tar命令    
    CC_CMD_SET_SYSTEM_TIME,                        ///< 系统时间设置，utc时间，目前仅在liteos系统上有效）
    CC_CMD_SET_PIR_SENSITIVITY,                    ///< 设置PIR灵敏度，0~100，值越大越灵敏
    CC_CMD_GET_BATTERY,                            ///< 获取电池电量信息，返回信息参考cc_battery_status    
    CC_CMD_GET_BATTERY_PERCENT,                    ///< 获取电池电量百分比信息，返回信息如50,60
    CC_CMD_SET_PIR_STATUS,                         ///< 设置PIR检测开关，关（0），开（1）
    CC_CMD_DEV_INACTIVE,                           ///< 设备处于没有事件，也没有直播的状态，建议设备休眠（低功耗设备），设备收到此回调时，需要结合当前设备的状态判断是否需要进入休眠，比如在升级过程中是不能休眠的
    CC_CMD_SET_AUTOTRACKING,                       ///< 设置自动跟踪开关，关闭（0），开启（1）
    CC_CMD_QR_START,                               ///< 开启扫描二维码，并获取扫描到的数据
    CC_CMD_QR_RELEASE,                             ///< 释放在扫描二维码时申请的资源
    CC_CMD_QR_STOP,                                ///< 停止二维码扫描
    CC_CMD_GET_RESOURCE_USAGE,                     ///< 获取CPU、内存等系统资源使用量    
    CC_CMD_SET_PRIVATE_MODE,                       ///< 设置隐私模式控制开关（操作摄像头开关时调用），enable（0），disable（1）
    CC_CMD_STREAM_RATELIMIT_STATUS,                ///< 设备码率受限状态，0：码流过大停止上传                  1：码流恢复正常
    CC_CMD_QR_DATA_START,                          ///< 开启扫描二维码，并获取zbar库解析过的数据
    CC_CMD_QR_DATA_RELEASE,                        ///< 释放在扫描二维码时申请的资源和zbar库解析后的数据
    CC_CMD_QR_DATA_STOP,                           ///< 停止二维码扫描
    CC_CMD_SET_SOUNDTRACKING,                      ///< 设置声音跟踪开关
    CC_CMD_PLAY_STATUS,                            ///< 客户端播放通知，有人播放（1），无人播放（0）

}cc_device_cmd_e;

typedef enum cc_video_cmd_e
{
    CC_CMD_SET_NIGHT_MODE,                         ///< 设置夜视模式开关，关闭（0）,开启（1）,自动（2）
    CC_CMD_SET_ANDTIFLICKER,                       ///< 设置防闪烁频率，目前支持50、60HZ
    CC_CMD_SET_ROTATE,                             ///< 设置图像翻转，不翻转（0），翻转（180）
    CC_CMD_SET_MOTION_DETECTION_SESSITIVITY,       ///< 设置运动侦测灵敏度，1~5
    CC_CMD_SET_MOTION_DETECTION_REGION,            ///< 运动侦测区域，参考结构体：cc_rect_s
    CC_CMD_SET_HD_VIDEO,                           ///< 高清和标清切换开关，高清（1），标清（0），参考结构体：cc_hd_s
}cc_video_cmd_e;

typedef enum cc_audio_cmd_e
{
    CC_CMD_AUDIO_START,                            ///< 对讲开始
    CC_CMD_AUDIO_STOP,                             ///< 对讲结束
    CC_CMD_AUDIO_TALK,                             ///< 对讲音频数据
    CC_CMD_SET_SOUND_DETECTION_SENSITIVITY,        ///< 声音侦测灵敏度，1~5从低到高
    CC_CMD_SET_VOLUME,                             ///< 扬声器音量,0~100
    CC_CMD_SET_MIC,                                ///< 咪头开关
}cc_audio_cmd_e;

typedef enum cc_network_cmd_e
{
    CC_CMD_GET_WIFI_LIST,                          ///< 获取wifi列表
    CC_CMD_SWITCH_WIFI,                            ///< 切换wifi
	CC_CMD_GET_WIFI_DEVICE_NAME,                   ///< 获取wifi设备名，如wlan0   
    CC_CMD_CURRENT_WIFI,                           ///< 获取设备当前连接的wifi
}cc_network_cmd_e;

typedef enum cc_server_status_e
{
    CC_REGISTER_SUCCESS,                           ///< 注册成功通知
    CC_REGISTER_FAIL,                              ///< 注册失败通知
    CC_SERVER_STATUS_OFFLINE,                      ///< 设备离线通知
    CC_SERVER_STATUS_ONLINE,                       ///< 设备在线通知
    CC_SERVER_STATUS_DEVICE_REMOVED,               ///< 设备解绑通知    
    CC_SERVER_STATUS_DEVICE_PAY,                   ///< 设备付费通知
    CC_SERVER_STATUS_DEVICE_NO_SERVICE,            ///< 设备没付费通知
}cc_server_status_e;

typedef enum cc_upgrade_cmd_e
{
    CC_CMD_UPGRADE_INFO,                           ///< 固件升级：固件包信息，包括固件包的大小，checksum等信息
    CC_CMD_UPGRADE_DOWNLOAD,                       ///< 下载的固件包数据
    CC_CMD_UPGRADE_EXECUTE,                        ///< 下载成功，执行安装
   	CC_CMD_UPGRADE_WAITING,//服务器要求等待
}cc_upgrade_cmd_e;

typedef enum cc_storage_cmd_e
{
    CC_CMD_GET_PARTITION_INFO,                     ///< 获取本地存储分区容量信息
    CC_CMD_FORMAT,                                 ///< 本地存格式化
}cc_storage_cmd_e;

typedef enum cc_timeline_cmd_e 
{
    CC_CMD_GET_TINELINE_SECTION,                   ///< 获取本地存储的历史视频片段列表
    CC_CMD_GET_TIMELINE_EVENT,                     ///< 获取本地存储的事件列表，暂不支持
    CC_CMD_RELEASE_TIMELINE_LIST,                  ///< 释放分配的列表数据缓存
}cc_timeline_cmd_e;

typedef enum cc_playback_cmd_e
{
    CC_CMD_START_PLAYBACK,                         ///< 开始本地回放
    CC_CMD_STOP_PLAYBACK,                          ///< 停止本地回放
}cc_playback_cmd_e;

typedef enum cc_media_read_cmd_e
{
    CC_CMD_MEDIA_OPEN,                             ///< 打开本地数据（根据指定时间点），可用于SD卡回放，数据补录等功能
    CC_CMD_MEDIA_READ,                             ///< 读取本地数据
    CC_CMD_MEDIA_CLOSE,                            ///< 关闭本地数据
}cc_media_read_cmd_e;

typedef enum cc_ptz_cmd_e
{
    CC_CMD_PTZ_MOVE_ABSOLUTELY,                    ///< 绝对位移
    CC_CMD_PTZ_MOVE_CONTINUOUSLY,                  ///< 连续移动,转动方向参考cc_space
    CC_CMD_PTZ_MOVE_RELATIVELY,                    ///< 步进，目前不支持
    CC_CMD_PTZ_GOTO_HOME,                          ///< 回到中心位置
    CC_CMD_PTZ_GET_POSITION,                       ///< 获取当前位置
    CC_CMD_PTZ_STOP_MOVE,                          ///< 停止移动
 
}cc_ptz_cmd_e;


///< 设备相关回调函数
typedef int (*cc_device_cb)(cc_device_cmd_e cmd,  void* data, void* user_data);

///< 视频相关回调函数
typedef int (*cc_video_cb)(cc_video_cmd_e cmd, void* data, void* user_data);

///< 音频相关回调函数
typedef int (*cc_audio_cb)(cc_audio_cmd_e cmd, void* data, void* user_data);

///< 网络相关回调函数
typedef int (*cc_network_cb)(cc_network_cmd_e cmd, void* data, void* user_data);

///< 服务状态回调函数
typedef int (*cc_server_status_cb)(cc_server_status_e server_status, 
                                    void* data, 
                                    void* user_data);
///< 本地历史数据记录相关回调函数
typedef int (*cc_timeline_cb)(cc_timeline_cmd_e cmd, int channel, 
                        cc_uint64 start_time, cc_uint64 end_time,int page_size,cc_section_event_list** pplist);

///< 本地存分区信息回调函数
typedef int(*cc_get_partition_info)(cc_storage_partition_info list[], int *max);

///< 本地存储格式化回调函数
typedef int (*cc_storage_format)();


///< 获取设备默认值回调函数，注册时，配置设备默认值，例如夜视初始化为开，ROTATE 默认配置为不旋转0，如不实现，采用SDK里面默认的值
typedef int (*cc_get_feature_default_value_cb)(cc_device_default_setting_type_e device_feature, 
                                                void* value, 
                                                int len,
                                                void* user_data);
												
///< 获取某些能力支持的范围，注册时，配置设备设置能力范围，目前只支持使用场景，如不实现，采用SDK里面默认的值
typedef int (*cc_get_feature_capacity_cb)(cc_device_capacity_e device_feature, 
                                                void* value, 
                                                int len,
                                                void* user_data);
///< 远程升级回调函数
typedef int (*cc_upgrade_cb)(cc_upgrade_cmd_e cmd,void* data, void* user_data);

///< 本地回放相关函数
typedef int (*cc_media_read_cb)(cc_media_read_cmd_e cmd,void* param1, void* param2, void* user_data);

///< PTZ相关回调函数
typedef int (*cc_ptz_cb)(cc_ptz_cmd_e cmd, void* data, void* user_data);


typedef struct cc_callback {
    cc_device_cb                        cb_device;      
    cc_video_cb                         cb_video;        
    cc_audio_cb                         cb_audio;        
    cc_network_cb                       cb_network;     
    cc_server_status_cb                 cb_server_status; 
    cc_get_feature_default_value_cb     cb_get_feature_default_value;
    cc_get_feature_capacity_cb          cb_get_feature_capacity;
    cc_upgrade_cb                       cb_upgrade;
    cc_timeline_cb                      cb_timeline;
    cc_get_partition_info               cb_storage_partition_info;
    cc_storage_format                   cb_storage_format;
    cc_media_read_cb                    cb_media_read;
    cc_ptz_cb                           cb_ptz;
    void                                *user_data;
} cc_callback;

CC_CDECLS_END

#endif /* __CC_DEVICE_H__ */


