
#ifndef __CC_TYPES_H__
#define __CC_TYPES_H__

/**
 * @文件名   	cc_types.h
 * @作者
 * @日期
 * 
 * @简介  类型定义
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#  ifndef NULL
#    define NULL		((void*) 0)
#  endif /* NULL */

#undef true
#  define true			1

#undef false
#  define false			0

#ifndef __ANDROID__
#ifndef bool
//   typedef char	bool;
#endif /* byte */
#endif

typedef unsigned char		cc_byte;
typedef int					cc_int;
typedef unsigned int		cc_uint;
typedef long				cc_long;
typedef unsigned long		cc_ulong;
typedef unsigned long		cc_dword;
typedef float				cc_float;
typedef double				cc_double;
typedef void				cc_void;
typedef char				cc_bool;
typedef char				cc_char;
typedef unsigned char		cc_uchar;
typedef wchar_t				cc_unichar;
typedef cc_void*            cc_pointer;

#define cc_null				NULL
#define cc_true				true
#define cc_false			false
#define cc_atomic           int

typedef char                cc_int8;
typedef unsigned char       cc_uint8;
typedef short				cc_int16;
typedef unsigned short		cc_uint16;
typedef int					cc_int32;
typedef unsigned int		cc_uint32;

#define cc_atoi32			atoi

#ifdef __LP64__
     typedef long				cc_int64;
     typedef unsigned long		cc_uint64;
#    define CC_INT64_VALUE(v)	(v ## L)
#    define CC_UINT64_VALUE(v)	(v ## UL)
#    define CC_INT64_FMT		"ld"
#    define CC_UINT64_FMT		"lu"
#    define CC_UINT64_HEX_FMT	"lx"
#    define cc_atoi64			atol
#  else /* ! __LP64 __ */
     typedef long long			cc_int64;
     typedef unsigned long long	cc_uint64;
#    define CC_INT64_VALUE(v)	(v ## LL)
#    define CC_UINT64_VALUE(v)	(v ## ULL)
#    define CC_INT64_FMT		"lld"
#    define CC_UINT64_FMT		"llu"
#    define CC_UINT64_HEX_FMT	"llx"
#    define cc_atoi64			atoll
#  endif /* __LP64__ */

typedef cc_int32                cc_result;

#ifndef __CC_INLINE
#  ifdef _MSC_VER
#    if (_MSC_VER >= 1200)
#      define __CC_INLINE	__forceinline
#    else /* ! (MSC_VER >= 1200) */
#      define __CC_INLINE	__inline
#    endif /* (_MSC_VER >= 1200) */
#  else /* ! _MSC_VER */
#    ifdef __cplusplus
#      define __CC_INLINE	inline
#    else /* ! __cplusplus */
#      define __CC_INLINE
#    endif /* __cplusplus */
#  endif /* _MSC_VER */
#endif /* __CC_INLINE */

#ifndef __GNUC__
#  define  __attribute__(x)
#endif /* __GNUC__ */

#include <ctype.h>
#define CC_ISXDIGIT(x)	isxdigit((x))
#define CC_ISBLANK(x)	(' ' == (x) || '\t' == (x))

#define CC_XCHR(x)		CC_CHR(x)
#define CC_CHR(x)		#x[0]
#define CC_XSTR(x)		CC_STR(x)
#define CC_STR(x)		#x

#include <assert.h>
#define CC_ASSERT(x)    assert(x)

#define CC_MAX_PATH     260

#define CCAPI __attribute__ ((visibility ("default")))

#ifndef IN
#define IN                      /**< 用于区分输入参数. */
#endif

#ifndef OUT
#define OUT                     /**< 用于区分输出参数. */
#endif

#ifndef INOUT
#define INOUT                   /**< 用于区分输入输出参数. */
#endif

#define CC_CDECLS_BEGIN
#define CC_CDECLS_END

#ifdef UNUSED
#elif defined(__GNUC__)
#define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
#define UNUSED(x) /*@unused@*/ x
#else
#define UNUSED(x) x
#endif

#endif /* __CC_TYPES_H__ */
