

#ifndef __CC_API_H__
#define __CC_API_H__

#include "cc_types.h"
#include "cc_datatypes.h"
#include "cc_device.h"

#ifdef __cplusplus
  extern "C" {
#endif


 /**
 * \文件名  cc_api.h
 * \作者
 * \日期
 *
 * \简介  核心API定义
 *
 */


CC_CDECLS_BEGIN

/** \功能 获取SDK版本号
 * 
 * \参数 [输入/输出] version  输入：缓冲区，输出：版本号
 * \参数 [输入] size          缓冲区长度
 * 
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_get_version(cc_char *version, cc_uint32 size);


/** \功能 初始化SDK
 *
 * 这个接口必须在调用其它接口之前先调用。
 *
 * \参数 [输入] pconfig 配置文件路径、证书路径、log路径，音视频内存池大小
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_init(cc_config_t* pconfig);


/** \功能 反初始化SDK
 *
 * 调用SDK接口结束后需要调用该接口。
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_uninit(void);


/** \功能 设置回调接口
 *
 * 初始化之后，调用其他接口之前设置，网络回调、信令设置回调、音视频回调
 * \参数 [输入] cb  回调函数指针，该指针指向的结构体对象不能是临时变量，需要整个生命周期存在
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_set_callback(cc_callback* cb);


/** \功能 设置设备信息
 *
 * 初始化之后调用
 *
 * \参数 [输入] dev_info 设备信息
 * \参数 [输入] pro_info 产品信息
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_set_device_info(cc_device_info* dev_info, cc_product_info* pro_info); 


/** \功能 设置设备能力集属性
*
* 初始化之后调用
*
* \参数 [输入] feature_count 设备支持feature的个数
* \参数 [输入] feature_array 支持feature数组
*
* \返回值 CC_SUCCESS  	成功
*			其它		失败
*
*/
cc_result cc_api_set_device_feature(cc_uint32 feature_count, cc_device_feature_e feature_array[]); 


/** \功能 设备SDK启动
 *
 * 初始化之后调用
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_start(void); 


/** \功能 设备SDK停止
 *
 * 初始化之后调用
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_stop(void); 


/** \功能 进入配置模式
*
* 按reset键时或者无账号时，调用此函数
*
* \参数 [输入] mode 注册的模式：ap、qrcode、smartconfig
*
* \返回值 CC_SUCCESS     成功    
*			其它		失败
*
*/
cc_result cc_api_start_register(cc_register_mode_e mode);

/** \功能 进入直接注册模式（前提是已经获取到注册信息），目前仅用作demo程序
*
*
* \参数 [输入] 注册参数
*
* \返回值 CC_SUCCESS     成功    
*			其它		失败
*
*/
cc_result cc_api_direct_register(cc_register_info info);


/** \功能 停止配置模式
 * 
* 设备主动停止注册流程的时候，调用此函数，注意不要在SDK的回调中调用此函数
* 
* \返回值 CC_SUCCESS     成功
*			其它		失败
*
*/
cc_result cc_api_stop_register(void);


/** \功能 检查设备注册状态
 * 
* 参数 [输出] device_status 0：未注册  1：已注册  
*
* \返回值 CC_SUCCESS 成功
*        其它		失败
*
*/
cc_result cc_api_check_register_status(int* device_status);


/** \功能 设置实时流
 *
 * \参数 [输入] info   音视频流
 * \参数 [输入] type   目前设置为0 
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_set_streaming(cc_stream_info const* info, int type);


/** \功能 设置事件
 *
 * \参数 [输入] utcms 		时间错
 * \参数 [输入] type 		事件类型
 * \参数 [输入] action  	表示事件开始或结束
 * \参数 [输入] pattr  		事件属性，目前只有事件类型为 CC_DETECT_SDCARD 或者 CC_DETECT_BATTERY时需要填写该字段
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_set_event(cc_uint64 utcms,
                                  cc_detect_type_e type,
                                  cc_detect_identify action,
                                  cc_event_attribute *pattr);



/** \功能 设置缩略图，设备按照固定间隔调用此接口设置缩略图（间隔5秒）
 *
 * \参数 [输入] channel 	通道号
 * \参数 [输入] image 		缩略图（JPEG格式）
 * \参数 [输入] size 		缩略图大小（字节）
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_set_thumbnail(cc_int32 channel,
                                      cc_void* image,
                                      cc_uint32 size);


/** \功能 休眠通知
*
* 低功耗设备休眠时调用，获取服务器保活相关配置信息
*
* \参数 [输出] phbconfig 服务器保活相关配置信息
*
* \返回值 CC_SUCCESS     成功
*			其它		失败
*
*/
cc_result cc_api_suspend(cc_hb_config *phbconfig);


/** \功能 唤醒
 *
 * 设备唤醒时调用，快速启动服务器连接
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_restore(cc_wakeup_type_e type, cc_wakeup_reason_e reason);


/** \功能 恢复出厂设置
 *
 * 
 *
 * \返回值 CC_SUCCESS  	成功
 *			其它		失败
 *
 */
cc_result cc_api_factoryreset();


CC_CDECLS_END

#ifdef __cplusplus
}
#endif


#endif /* __CC_API_H__ */

