


#ifndef __CC_DATATYPES_H__
#define __CC_DATATYPES_H__
#ifndef __HuaweiLite__
#include <stdbool.h>
#endif
#include <stdint.h>

#include "cc_types.h"

/**
 * @文件名       cc_datatypes.h
 * @作者
 * @日期
 *
 * @简介  数据结构定义
 *
 *
 */

CC_CDECLS_BEGIN

typedef enum cc_detect_type_e{
    CC_DETECT_MOTION = 0,                ///< 移动侦测
    CC_DETECT_SOUND,                     ///< 声音侦测
    CC_DETECT_FACE,                      ///< 人脸侦测
    CC_DETECT_PIR = 15,                  ///< 人体红外侦测
    CC_DETECT_RING,                      ///< 门铃事件
    CC_DETECT_BATTERY = 21,              ///< 电池电量事件
    CC_DETECT_PEOPLE = 23,               ///< 人形检测

    CC_DETECT_TIMELINE_EVENT_MAX = 29,        
    
    CC_DETECT_SDCARD = 30,				 ///< SD卡事件
    CC_DETECT_DOORBELL_STATUS,         ///< 中继门铃（一拖一）状态上报
    CC_DETECT_MAX,
} cc_detect_type_e;

typedef enum cc_detect_identify {
    CC_DETECT_START,                         ///< 事件开始
    CC_DETECT_STOP,                          ///< 事件结束
    CC_DETECT_PULSE,                         ///< 脉冲事件（固定长度的事件，比如:PIR，门锁事件等）
}cc_detect_identify;

typedef enum cc_register_mode_e {
    CC_REGISTER_WITH_QRCODE    = 0x0001,     ///< 二维码方式
    CC_REGISTER_WITH_CABLE     = 0x0002,     ///< 有线绑定方式
    CC_REGISTER_WITH_AP_MODE   = 0x0008,     ///< AP热点方式
} cc_register_mode_e;

typedef struct cc_register_info {
    char username[128];                      ///< 用户名
    char password[128];                      ///< 临时密码
    char qkey[5];                            ///< 产品short key
    char locale[20];                         ///< 语言地区信息
} cc_register_info;

typedef enum cc_wakeup_type_e {
    CC_WAKEUP_NONE = 0,
    CC_WAKE_UP_BY_RING = 1,                  ///< 门铃唤醒类型：手动唤醒
    CC_WAKE_UP_BY_PIR,                       ///< 门铃唤醒类型：人体红外检测唤醒
    CC_WAKE_UP_BY_APP_CLIENT,                ///< 门铃唤醒类型：客户端远程唤醒
    CC_WAKEUP_ERROR,                         ///< 异常重启，cc_wakeup_reason_e中可以填写错误码
} cc_wakeup_type_e;

typedef enum cc_stream_type_e {
    CC_STREAM_AUDIO = 1,                     ///< 数据类型：音频
    CC_STREAM_VIDEO,                         ///< 数据类型：视频
}cc_stream_type_e;

typedef enum cc_media_type_e {
    CC_MEDIA_H264 = 1,                      ///< 媒体类型：H264
    CC_MEDIA_G711A = 6,                         ///< 媒体类型：G711 A-law数据
    CC_MEDIA_G711U = 7,                         ///< 媒体类型：G711 u-law数据
    CC_MEDIA_H265 = 8,                          ///< 媒体类型：H265
    CC_MEDIA_MAX,
} cc_media_type_e;

typedef enum cc_battery_status{
    BATTERY_STATUS_NULL,                    /// < 表示取不到电池信息；
    BATTERY_STATUS_LITE,                    /// < 表示低电量；
    BATTERY_STATUS_CHARGING,                /// < 表示正在充电；
    BATTERY_STATUS_FULL,                    /// < 表示电池充满了；
    BATTERY_STATUS_NORMAL                   /// < 表示其它正常状态
}cc_battery_status;

typedef enum cc_sdcard_status{
    SDCARD_STATUS_NO_SDCARD = 1,	/**< 设备无SD卡   */
    SDCARD_STATUS_ABNORMAL,     /**< 设备SD卡异常 */
    SDCARD_STATUS_NORMAL,       /**< 设备SD卡正常 */
}cc_sdcard_status;

typedef enum cc_doorbell_status {
    DOORBELL_STATUS_ONLINE = 1,    /*中继门铃设备上线*/
    DOORBELL_STATUS_OFFLINE,       /*中继门铃设备离线*/
    DOORBELL_STATUS_SLEEP,         /*中继门铃设备休眠*/
}cc_doorbell_status;

typedef enum cc_sdcard_record_mode{
    CC_SDCARD_RECORD_MODE_NONE = 1,	/**< 不录制   */
    CC_SDCARD_RECORD_MODE_EVENT,     /**< 事件录制 */
    CC_SDCARD_RECORD_MODE_FULL,       /**< 全天录制 */
}cc_sdcard_record_mode;


typedef struct cc_section_event_info {
    cc_uint64   start_time;        ///< 开始时间
    cc_uint64   end_time;        ///< 结束时间
    cc_detect_type_e event_type;    ///< 事件类型，目前暂时不用
} cc_section_event_info;

typedef struct cc_section_event_list {    
    cc_uint32   num;               ///< section数量
    cc_bool     b_has_more;        ///< 起止时间内的section是否返回完毕
    cc_section_event_info*  plist;    ///< section列表
} cc_section_event_list;

typedef struct cc_playback_param{
    int channel;
    cc_uint64 start_time; 
    cc_void* play_handle;///< 本次回放的句柄，应用层释放相应资源
}cc_playback_param;

typedef struct cc_storage_partition_info {
    
    char        name[32]; /**< 名字，"/dev/sda1" */
    char        fs[16];   /**< 文件系统, "vfat" */
    cc_uint64   total;    /**< 总大小, byte */
    cc_uint64   remain;   /**< 剩余大小, byte */
} cc_storage_partition_info;


typedef struct cc_stream_info {
    cc_uint16    id;             ///< 码流 ID
    cc_uint16    channel;        ///< 码流通道号
    cc_uint64    utcms;          ///< UTC time, unit MilliSeconds
    cc_uint32    pts;            ///< 时间戳，音视频需要保证同步，音频打包间隔必须是20ms或者40ms
    cc_uint32    sequence;       ///< 音视频帧序号，音频和视频分别计数，越界后从0重新开始
    cc_uint32    format    : 8;  ///< 数据类型, 取值：cc_stream_type_e
    cc_uint32    newformat : 1;  ///< 是否新数据格式
    cc_uint32    head      : 1;  ///< 数据包分包，首包标志
    cc_uint32    tail      : 1;  ///< 数据包分包，尾包标志
    cc_uint32    reserved1 : 13;
    cc_uint32    fisheye   : 1;  ///< 1-need fisheye correct, 0-normal
    cc_uint32    reserved;
    void*       payload;
    cc_uint32    length;         ///< 数据包长
    cc_uint32    refcount;       ///< Reference count

    union {
        uint8_t placeholder[32];

        /// 视频详细信息, 适用格式: STREAM_VIDEO
        struct {
            cc_int8  encode;     ///< 视频编码格式, 取值 cc_media_type_e
            char    frame;      ///< 帧类型, 'R'(IDR帧), 'I', 'P', 'B'
            cc_uint32 width;     ///< 图像宽度
            cc_uint32 height;    ///< 图像高度
        } video;

        /// 音频详细信息, 适用格式: STREAM_AUDIO
        struct {
            cc_int8  encode;     ///< 音频编码格式, 取值 cc_media_type_e
            cc_int8  channelcount; ///< 音频声道数
            cc_uint32 samplerate;  ///< 音频采样率
            cc_uint32 samplebits;  ///< 采样大小
        } audio;

        /// 点阵详细信息, 适用格式: STREAM_PIXELS
        struct {
            cc_int8  layout;     ///< 点阵排列格式, 取值 PixelsLayout
        } pixels;
    };

} cc_stream_info;


typedef struct cc_frame {
    cc_stream_info* info;           ///< 帧信息
    cc_int8*      data;             ///< 帧数据
    cc_uint32     bytes;            ///< 帧数据长度
    void*         reserved[3];

    void (*release)(void* priv);    ///< 释放资源
    void*         priv;             ///< 私有参数
} cc_frame;



typedef struct cc_rect{
    int left;                       ///< 所选区域距离左边框坐标（范围0~4）
    int top;                        ///< 所选区域距离上边框坐标（范围0~4）
    int right;                      ///< 所选区域距离右边框坐标（范围1~5）
    int bottom;                     ///< 所选区域距离下边框坐标（范围1~5）
} cc_rect;

typedef struct cc_rect_s {
    int regions_num;                ///< 侦测区域个数，区域为5*5区块
    cc_rect *rect;                  ///< 侦测区域坐标信息列表，包含客户端所设置的每个区块的坐标
} cc_rect_s;


typedef struct cc_region{
    int id;                         ///< 编号
    int left;                       ///< left区域左上角x坐标
    int top;                        ///< top区域左上角y坐标
    int width;                      ///< width区域的宽
    int height;                     ///< height区域的高
} cc_region;                        ///< 通用型区域坐标信息

typedef struct cc_region_s {
    int regions_num;                ///< 区域个数
    cc_region *rect;                ///< 区域坐标
} cc_region_s;

typedef struct cc_hd_s {
    int streamid;
    int hd;
}cc_hd_s;

typedef struct cc_buf {
    char* buf;
    int bytes;
} cc_buf;


typedef enum cc_sdcard_record_type{
    SDCARD_RECORD_MODE_NONE,                   /**< 如果SD卡录制逻辑由设备厂商处理，则设置为此模式*/
    SDCARD_RECORD_MODE_RAW_DATA = 1,           /**< SDK内部录制模式，录制raw data文件模式，支持客户端回放和查询   */
    SDCARD_RECORD_MODE_MP4 = 2,                /**< SDK内部录制模式，录制mp4文件模式，暂不支持*/    
}cc_sdcard_record_type;

typedef struct cc_event_attribute {
    union {
        struct {
            cc_battery_status status;             ///< 事件类型为 CC_DETECT_BATTERY 时有效 
            int percent;                          ///< 电量百分比，范围0~100
        } battery_value;

        struct {
            cc_sdcard_status status;             ///< 事件类型为 CC_DETECT_SDCARD 时有效，表示SD卡状态
            cc_sdcard_record_type recordmode;    ///< 录制的模式，不需要设置
            char path[124];                      ///< SDCARD路径
        } sdcard_value;
        
        struct {
            int imgwidth;             ///< 事件的状态，事件类型为 CC_DETECT_PEOPLE 时，richtext为传入参数
            int imgheight;
            int imgsize;
            const char* imgbuf;
        } richtext_value;  

        struct {
            char* deviceid;         
            int device;             ///< 表示外设种类：0为电机，1为镜头 
            int value;              ///< 表示是否到达边界：0为离开，1为到达
            int direction;          ///< 表示方向：0为上，1为下，2为左，3为右，4为前，5为后；前后方向特表示镜头
        } ptzborder_value;

        struct {
            char* picture;
            int len;
            char bid[64];
        } thirdpart_pic;

        struct {
            char mac[32];           ///< 子门铃的mac地址
            cc_doorbell_status status;   ///< 中继（一拖一）门铃状态，事件类型为 CC_DETECT_DOORBELL_STATUS 时有效
        } doorbell_status;
    };
} cc_event_attribute;


typedef struct cc_hb_config {
    cc_char    heartbeatrequest[64];    ///< 心跳包内容，门铃的Wifi模块发往保活服务器
    cc_int       heartbeatrequestlen;     ///< 心跳包内容长度
    cc_char    wakeuprequest[64];       ///< 唤醒包内容，保活服务器发往门铃的Wifi模块
    cc_int       wakeuprequestlen;        ///< 唤醒包内容长度
    struct {
        cc_char    server[64];            ///< 保活服务器地址
        cc_int       port;                ///< 保活包服务器端口(tcp)
        } serverport[4];
    cc_int     servernum;               ///< 保活服务器的个数
    cc_int     interval;                ///< 心跳包间隔（秒）
} cc_hb_config;


typedef enum cc_wifi_mode {
    CC_WIFI_MODE_NULL = -1,
    CC_WIFI_MODE_OPEN,
    CC_WIFI_MODE_WPA,
    CC_WIFI_MODE_WPA2,
    CC_WIFI_MODE_WEP,
    CC_WIFI_MODE_WPA_WPA2,
    CC_WIFI_MODE_MAX,
} cc_wifi_mode;


typedef struct cc_wifi {
    char             bssid[128];     /**< BSSID */
    char             ssid[128];      /**< SSID */
    char             passwd[128];    /**< 密码 */
    cc_int32         signal;         /**< 信号强度(0 - 5) */
    cc_wifi_mode     mode;           /**< 加密模式 */
    cc_int8          connected;      /**< 标识设备是否连接 */
    bool             hidden;         /**< 标识设备是否隐藏 */
    cc_int32         signal_level;   /**< 信号级别，dBm*/
    cc_int32         noise_level;    /**< 噪声级别，dBm*/
} cc_wifi;

typedef struct cc_wifi_s{
    void  *wifi_list_buf;            /**< wifi信息列表，类型参考结构体：cc_wifi */
    int   count;                     /**< 列表中的wifi个数 */
} cc_wifi_s;

typedef struct cc_fisheye_circle_param{
    int cx;                     /**< 以左上为原点，圆心的横坐标位置，范围为0~10000 */
    int cy;                     /**< 以左上为原点，圆心的纵坐标位置，范围为0~10000 */
    int rx;                     /**< 圆的横轴半径，范围为0~5000 */
    int ry;                     /**< 圆的纵轴半径，范围为0~5000 */
} cc_fisheye_circle_param;


typedef struct cc_resource_usage {
    int cpu;                    /**< CPU使用率，0~100 */
    int mem;                    /**< 内存使用率，0~100 */
    int free_mem_size;          /**< 剩余内存大小，单位KB */
} cc_resource_usage;


typedef enum cc_device_feature_e {
    // 硬件特性
    CC_FEATURE_HAVE_BEGIN                   = 0x100,
    CC_FEATURE_HAVE_WIFI                    = CC_FEATURE_HAVE_BEGIN, /// < 有wifi芯片
    CC_FEATURE_HAVE_LED                     , /// < 有LED状态指示灯
    CC_FEATURE_HAVE_MIC                     , /// < 有MIC
    CC_FEATURE_HAVE_SPEAKER                 , /// < 有喇叭
    CC_FEATURE_HAVE_SDCARD                  , /// < 有SD卡槽
    CC_FEATURE_HAVE_PAN                     , /// < PTZ，可以左右转动
    CC_FEATURE_HAVE_TILT                    , /// < PTZ，可以上下转动
    CC_FEATURE_HAVE_BATTERY                 , /// < 电池供电设备
    CC_FEATURE_HAVE_PIR                     , /// < PIR(人体感应) 红外
    CC_FEATURE_HAVE_FISHEYE                 , /// < 鱼眼设备
    CC_FEATURE_HAVE_RING                    , /// < 门铃设备
    CC_FEATURE_HAVE_BUZZER                  , /// < 蜂鸣器
    CC_FEATURE_HAVE_MAX_SUM                 ,
    
    // 软件特性
    CC_FEATURE_SUPPORT_BEGIN                   = 0x200,
    CC_FEATURE_SUPPORT_ROTATE                  = CC_FEATURE_SUPPORT_BEGIN, // 支持旋转，0，180
    CC_FEATURE_SUPPORT_FULL_DUPLEX_SPEECH      , /// < 支持全双工语音对讲
    CC_FEATURE_SUPPORT_MOTION_DETECTION        , /// < 支持运动检测
    CC_FEATURE_SUPPORT_MOTION_REGION           , /// < 支持运动检测区域设置
    CC_FEATURE_SUPPORT_MOTION_SENSITIVITY      , /// < 支持运动检测灵敏度设置
    CC_FEATURE_SUPPORT_SOUND_DETECTION         , /// < 支持声音检测
    CC_FEATURE_SUPPORT_SOUND_SENSITIVITY       , /// < 支持声音检测灵敏度设置
    CC_FEATURE_SUPPORT_PEOPLE_DETECTION        , /// < 支持人形检测
    CC_FEATURE_SUPPORT_SPEAKER_VOLUME          , /// < 支持扬声器音量调节
	CC_FEATURE_SUPPORT_HDVIDEO                 , /// < 支持高标清切换，HD/SD
	CC_FEATURE_SUPPORT_PIR_SENSITIVITY         , /// < 支持PIR灵敏度设置
	CC_FEATURE_SUPPORT_ANTIFLICKER             , /// < 抗闪烁
    CC_FEATURE_SUPPORT_NIGHT_VISION            , /// < 支持夜视
    CC_FEATURE_SUPPORT_AUTOTRACKING            , /// < 运动跟踪
    CC_FEATURE_SUPPORT_FACE_DETECTION          , /// < 支持人脸检测
    CC_FEATURE_SUPPORT_PTZ_PRESET              , /// < 支持PTZ预置位设置
    CC_FEATURE_SUPPORT_SOUNDTRACKING           , /// < 支持声音跟踪
    CC_FEATURE_SUPPORT_MAX_SUM,
} cc_device_feature_e;


typedef enum device_capacity_e {
    CC_CAPACITY_USAGE_SCENARIO,  /// <设备支持的场景能力集，按照二进制从高位到低位依次表示：背光补偿，红外，强光抑制，运动，宽动态
    CC_CAPACITY_CAMERA_FOCUS,    /// <设备长短焦分别支持的最大放大倍数，用 | 分隔，| 前短焦后长焦，例如： 2.5|3.5 表示
}cc_device_capacity_e;


//如需修改设备注册时，默认的配置属性，可实现回调函数cc_get_feature_default_value_cb以下类型值
typedef enum device_default_setting_type_e {

    CC_DEFAULT_SETTING_TYPE_NIGHT_VISION_MODE                   ,/// < 夜视模式默认值，关闭（0）,开启（1）,自动（2）
    CC_DEFAULT_SETTING_TYPE_PIR_STATUS                          ,/// < PIR开关，关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_MOTION_DETECT_STATUS                ,/// < 运动检测推送开关，关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_SOUND_DETECT_STATUS                 ,/// < 声音检测推送开关，关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_PEOPLE_DETECT_STATUS                ,/// < 人形检测推送开关，关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_MOTION_DETECT_SENSITIVITY           ,/// < 运动检测灵敏度，1~5
    CC_DEFAULT_SETTING_TYPE_SOUND_DETECT_SENSITIVITY            ,/// < 声音检测灵敏度，1~5
    CC_DEFAULT_SETTING_TYPE_HD_VIDEO                            ,/// < 高清或标清，高清（1），标清（0）
    CC_DEFAULT_SETTING_TYPE_PIR_SENSITIVITY                     ,/// < PIR灵敏度默认值，0~100
    CC_DEFAULT_SETTING_TYPE_LED_STATUS                          ,/// < LED开关，关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_ANTIFLICKER                         ,/// < 防闪烁频率，50hz/60hz
    CC_DEFAULT_SETTING_TYPE_AUTOTRACKING                        ,/// < 运动跟踪开关默认值， 关闭（0），开启（1）
    CC_DEFAULT_SETTING_TYPE_FACE_DETECT_STATUS                  ,/// < 人脸检测开关默认值，关闭（0），开启（1）
	CC_DEFAULT_SETTING_TYPE_CAMERAIMAGEROTATE                   ,/// < 图像翻转，不翻转（0），翻转（180）	
	CC_DEFAULT_SETTING_TYPE_SOUNDTRACKING                       ,/// < 声音跟踪开关默认值，关闭（0），开启（1）
    CC_DEFAULT_MAX = 0xff,
}cc_device_default_setting_type_e;

typedef enum log_level_e{
    CC_LOG_LEVEL_DEBUG = 1,
    CC_LOG_LEVEL_TRACE,
    CC_LOG_LEVEL_INFO,
    CC_LOG_LEVEL_WARNNING = 7,
    CC_LOG_LEVEL_ERROR,
    CC_LOG_LEVEL_OFF = 0xff,
}cc_log_level_e;

typedef struct cc_config_t {
    cc_char*    config_file_path;   //配置文件路径
    cc_char*    ca_file_path;       //证书文件路径
    cc_char*    log_path;           //日志文件路径
    cc_uint32   max_buffer_size;    //音视频缓冲区大小，720P建议1M，1080P建议2M
    cc_uint32   log_max_line;       //日志最大行数（默认10000行，最小1000行）
} cc_config_t;

typedef enum cc_upgrade_package_type {
    CC_UPGRADE_PACKAGE_APP,     /**< 固件应用程序包 */
    CC_UPGRADE_PACKAGE_FW,      /**< 固件Firmware包*/
} cc_upgrade_package_type;

typedef struct cc_upgrade_info{
    cc_upgrade_package_type pkg_type;          /**< 包类型 */
    cc_uint64               pkg_size;          /**< 包大小 */
    char                    filemd5[64];
    char                    url[256];          /**< 升级包的URL地址，SDK中会自动下载升级包，用户也可以在安装失败的情况下主动下载*/
}cc_upgrade_info;

typedef struct cc_update_recieve_buf{
    char        *buf;   /// <接收的升级包
    cc_uint64   size;   /// <数据大小
}cc_update_recieve_buf;



typedef enum cc_wakeup_reason_e {    
    CC_WAKE_UP_REASON_TYPE_HISI_LITEOS_BEGIN=1000, 
    CC_WAKE_UP_REASON_TYPE_NULL = 1000,             ///< 正常上电模式
    CC_WAKE_UP_REASON_TYPE_MAGIC_PACKET,            ///< magic packet唤醒
    CC_WAKE_UP_REASON_TYPE_NETPATTERN_TCP,          ///< tcp netpattern包唤醒
    CC_WAKE_UP_REASON_TYPE_NETPATTERN_UDP,          ///< udp netpattern包唤醒
    CC_WAKE_UP_REASON_TYPE_DISASSOC_RX,             ///< 接收到去关联/去认证唤醒
    CC_WAKE_UP_REASON_TYPE_DISASSOC_TX,             ///< 主动去关联唤醒
    CC_WAKE_UP_REASON_TYPE_AUTH_RX,                 ///< 接收到认证帧唤醒（只存在于AP模式下的待机唤醒）
    CC_WAKE_UP_REASON_TYPE_TCP_UDP_KEEP_ALIVE,      ///< TCP断链唤醒
    CC_WAKE_UP_REASON_TYPE_HOST_WAKEUP_NONE,        ///< HOST侧触发的唤醒
    CC_WAKE_UP_REASON_TYPE_HOST_WAKEUP_KEY,         /// < HOST侧触发的唤醒：按键唤醒
    CC_WAKE_UP_REASON_TYPE_HOST_WAKEUP_PIR,         /// < HOST侧触发的唤醒：PIR唤醒
    CC_WAKE_UP_REASON_TYPE_OAM_LOG,                 ///< wifi错误日志导致的唤醒
    CC_WAKE_UP_REASON_TYPE_SSID_SCAN,               ///< 指定SSID触发的唤醒

    CC_WAKE_UP_REASON_TYPE_USER_DEFINED_BEGIN = 2000,  ///< 用户自定义错误码，范围2000~2999
    CC_WAKE_UP_REASON_TYPE_USER_DEFINED_END = 2999,
} cc_wakeup_reason_e;

typedef struct cc_smartlink_channel_info{
    unsigned char channel[13];       /**< 信道, 1-13 */
    unsigned char count;         /**< 当前信道的个数 */
    cc_uint32 duration;       /**< 需要在该信道停留多久，单位ms */
} cc_smartlink_channel_info;


typedef enum cc_media_read_type {
    CC_MEDIAREADER_PLAYBACK = 0,            ///< 本地存储回放
    CC_MEDIAREADER_FILL_TIMELINE,           ///< 音视频补录，如果要使用此功能，SDK使用者需要根据设备在线状态主动记录离线视频的时间片段，待
	                                        ///< 网络恢复后，SDK回调获取断网时间内的视频
}cc_media_read_type;

typedef struct cc_media_open_info {
    cc_media_read_type type;           ///< 打开类型
    cc_uint32  channel;                ///< 通道号，IPC默认为0
    cc_uint64  starttime;              ///< 开始时间，UTC时间（ms），只有回放时有效，表示开始回放的时间点
    cc_uint64  endtime;                ///< 结束时间，此参数目前无效
}cc_media_open_info;

typedef struct cc_space {
    float x;        ///< pan，PTZ的水平移动，右为正，范围为-1.000000到1.000000
    float y;        ///< tilt PTZ的垂直移动，上为正，范围为-1.000000到1.000000
    float zoom;     ///< zoom 目前没有用到
    int id;         ///< 自动巡航依次的id位置
}cc_space;

typedef struct cc_snapshot {
    float x;         ///< pan，PTZ的水平移动，右为正，范围为-1.000000到1.000000
    float y;         ///< tilt PTZ的垂直移动，上为正，范围为-1.000000到1.000000
    char  bid[64];   ///< bid，业务id，上传图片时需将此参数带上
}cc_snapshot;

typedef struct cc_cruise {
    cc_space* pos;      ///巡航点坐标数组
    cc_uint32  num;     ///巡航点个数，个数为0时关闭巡航
}cc_cruise;

typedef struct cc_qrcode_info{
    int  width;        ///< 灰度图宽
    int  height;        ///< 灰度图高
    char *pdata;        ///< 灰度图数据
}cc_qrcode_info;

typedef struct cc_apmode_userinfo {
    char username[128];     ///< AP直连模式用户名
    char password[128];     ///< AP直连模式密码
    char oldpassword[128];
}cc_apmode_userinfo;

typedef struct cc_chip_info {
    char factory[10];
    char imeiDeviceType[10];
}cc_chip_info;

typedef struct cc_selfdef_info {
    int len;
    char *value;
}cc_selfdef_info;

typedef enum cc_md_status
{
    CC_MD_STATUS_STOPPED = 0,
    CC_MD_STATUS_STATIC,
    CC_MD_STATUS_NONSTATIC,
}cc_md_status;
typedef struct cc_seldef_ability {
    char key[64];              ///< 自定义能力key
    char support[8];           ///< 自定义能力默认值
}cc_seldef_ability;

typedef struct cc_sdcard {
    cc_sdcard_status status;
    cc_sdcard_record_mode recordmode;
}cc_sdcard;

typedef enum cc_sdcard_record_data_mode{
    SDCARD_RECORD_RAW_DATA_MODE = 1,	            /**< 录制raw data文件模式   */
    SDCARD_RECORD_MP4_DATA_MODE = 2,                /**< 录制mp4文件模式*/    
    SDCARD_RECORD_FILL_TIMELINE_WRITE_MODE = 3,      /**< offline模式下raw data录制到sdcard   */
    SDCARD_RECORD_FILL_TIMELINE_READ_MODE =  4,      /**< online raw data补录到云端模式   */
    SDCARD_RECORD_DATA_MODE_MAX,        
}cc_sdcard_record_data_mode;

typedef struct cc_osd_config
{
    bool    enable;
    int     format;     ///< see Stream::EncodeFormat
    int     width;
    int     height;
}cc_osd_config;


typedef struct cc_picture_quality {
    int level;
    int bitrate;
    int maxqp;
    int minqp;
} cc_picture_quality;
CC_CDECLS_END

#endif /* __CC_DATATYPES_H__ */
