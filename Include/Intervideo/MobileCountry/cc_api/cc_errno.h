#ifndef __CC_ERRNO_H__
#define __CC_ERRNO_H__

/**
 * \文件名  cc_errno.h
 * \作者
 * \日期
 *
 * \简介  错误码
 */

#define CC_SUCCESS							0 	/**< 成功 */
#define CC_FAILURE							-1 	/**< 失败 */

#define CC_ERR_NOTINIT				0xCC008801	/**< 未初始化或初始化失败 */
#define CC_ERR_INVALIDARG			0xCC008802 	/**< 无效参数 */
#define CC_ERR_NO_FILE_PATH			0xCC008803	/**< 文件路径不存在 */
#define CC_ERR_FILE_CREATE_FAIL		0xCC008804	/**< 创建文件失败 */
#define CC_ERR_SERIALID_NOTINIT		0xCC008805	/**< 序列号未初始化 */
#define CC_ERR_REGISTERD			0xCC008806	/**< 已注册 */
#define CC_ERR_UNREGISTERD 			0xCC008807	/**< 未注册 */
#define CC_ERR_SERIALID_USED		0xCC008808	/**< 已被其它用户注册 */
#define CC_ERR_ADD_DEVICE_FAIL		0xCC008809	/**< 添加设备失败 */
#define CC_ERR_NO_MEMORY			0xCC00880A	/**< 没有足够空间 */
#define CC_ERR_CMD_NOTSUPPORT		0xCC00880B	/**< 命令不支持 */
#define CC_ERR_UPGRADING			0xCC00880C	/**< 设备升级中 */
#define CC_ERR_AUTHENTICATION		0xCC00880D	/**< 认证失败 */
#define CC_ERR_LAST_VERSION			0xCC00880E	/**< 版本是最新的 */
#define CC_ERR_SERVER_RESTING		0xCC00880F	/**< 正在重置 */
#define CC_ERR_READ 				0xCC008810	/**< 读失败 */
#define CC_ERR_WRITE 				0xCC008811	/**< 写失败 */
#define CC_ERR_SEND 				0xCC008812	/**< 发送数据失败 */
#define CC_ERR_RECEIVE 				0xCC008813	/**< 接收数据失败 */
#define CC_ERR_DISARM 				0xCC008814 	/**< 撤防 */
#define CC_ERR_UNKNOWN				0xCC0088FF	/**< 未知错误 */

//_VIDEO_

//_AUDIO_

//_SERVER_
#define CC_ERR_SERVER_CURLFAILED			0xCC008B01	/**< 连接服务器失败 */
#define CC_ERR_SERVER_NO_RETURN				0xCC008B02	/**< 服务器返回空 */
#define CC_ERR_SERVER_TIMEOUT				0xCC008B03	/**< 连接服务器超时 */
#define CC_ERR_SERVER_NOT_CONNECT			0xCC008B04	/**< 没有连接到服务器 */
//#define CC_ERR_SERVER_UNREGISTER			0xCC008B05	/**< 服务器未注册 */
//#define CC_ERR_SERVER_REGISTERD				0xCC008B06	/**< 服务器已注册 */
#define CC_ERR_SERVER_GATEWAY_UNREGISTER	0xCC008B05	/**< 网关未注册 */

//_NET_
#define CC_ERR_NET_SWITCH_WIFI				0xCC008C01	/**< 切换wifi错误 */
#define CC_ERR_NET_AP_RUNNING				0xCC008C02	/**< app已启动 */

#endif /* __CC_ERRNO_H__ */
