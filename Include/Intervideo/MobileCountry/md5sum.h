#ifndef  MD5SUM_H
#define MD5SUM_H

#ifdef __cplusplus
extern "C"
{
#endif
int GetMd5Value (char *filename, char *md5value);
int GetBufMd5Value (char *buf,int len, char *md5value);
#ifdef __cplusplus
}
#endif

#endif