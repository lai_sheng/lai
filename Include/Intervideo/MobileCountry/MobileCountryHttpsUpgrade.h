#ifndef __MOBILECOUNTRYHTTPSUPGRAE__
#define __MOBILECOUNTRYHTTPSUPGRAE__

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <curl/curl.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"


using namespace std; 


class  CMobileCountryHttpsUpgrade:public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE( CMobileCountryHttpsUpgrade);
		 CMobileCountryHttpsUpgrade();
		~ CMobileCountryHttpsUpgrade();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		
		void ThreadProc();
		
		/*
		 * 上电联网同步成功
		 */
		void 	SetNtpSuccess();

		VD_INT32  DownLoad();

	private:
		int 	m_nTimeRebootTick;
	private:		
		int    m_iNtpSuccess;
		//upgrade URL
		string m_cupgradeurl;
		int    m_istartupgrade;

};

#define g_MobileCountryUpgrade (* CMobileCountryHttpsUpgrade::instance())


#endif /**/

