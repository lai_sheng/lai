#ifndef _MOBILECOUNTRYAPPCMD_H
#define _MOBILECOUNTRYAPPCMD_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"
#include "Configs/ConfigCamera.h"
#include "Intervideo/MobileCountry/cc_api/cc_datatypes.h"

class MobileCountryAppCmd : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(MobileCountryAppCmd);
	MobileCountryAppCmd();
	~MobileCountryAppCmd();
public:
	VD_INT32 Start();
	VD_INT32 Stop();
	/*灵敏度，声音配置信息*/
	void onConfigAppCtrl(CConfigHemuAppCtrl& cCfgAppCtrl, int& ret);
	void ThreadProc();
	VD_INT32 SDCardFormat();
	VD_INT32 AddCmdData(int ctrl,uint value,uint value1 = 0);
	VD_INT32  HandleCmdData();
	/*设置分辨率*/
	VD_INT32  SetEncodeMode(int mode);
	/*摄像头配置*/
	VD_INT32  ModCameraCfg(int ctrl, int value);
	/*切换wifi*/
	int  SwitchWifi(char* Ssid, char* Passwd);
	/*获取WIFI列表*/
	void GetWifiList(cc_wifi_s *pWifiListInfo);
	/*LED 控制*/
	VD_INT32  LedCtrol(int value);
	/*人型跟踪*/
	VD_INT32 SetAutoTrack(int value);
	/*断开wifi*/
	VD_INT32 DisconectWifi();
private:
	typedef struct PACKET_CMD{
				int  ctrl;  //0:短按   1：长按     2:sd卡格式化  3：PTZ快捷设置
				uint value; //1:左 2：右 3:上 4：下
				uint value1;//y轴
		}PACKET_CMD;
	typedef std::list<PACKET_CMD> PACKET_LIST;
	
	uint m_packet_count;
	PACKET_LIST m_packet_list; //队列
	CMutex m_mutex_list;
	int         m_iInit;
	CConfigHemuAppCtrl  m_configAppCtrl;
};
#define g_MobileCountryAppCmd  (* MobileCountryAppCmd::instance())

#endif
