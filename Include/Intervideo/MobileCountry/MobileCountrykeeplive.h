#ifndef __KEEP_LIVE_H__
#define __KEEP_LIVE_H__

#include "dot_define.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*wakeup_cb)(DoT_WakeupReason_en reason);

int StartKeepLive(DoT_KeepLiveInfo_st* liveinfo);
void StopKeepLive();
void SetWakeupCb(wakeup_cb cb);

#ifdef __cplusplus
} 
#endif


#endif
