#ifndef __DOT_ERROR_H__
#define __DOT_ERROR_H__
 /**  
     *\file dot_error.h
     *
     *\brief DOT SDK 错误码定义
     *
     *   
     *
     *\author 作者信息
     */
#define DOT_EC_SUCCESS  0								/**< 成功 */

/* 系统相关错误码 */
#define DOT_EC_INVALID_FILE_PATH                -1000    /**< 无效的文件路径 */
#define DOT_EC_FILE_OPERATE_ERROR               -1001    /**< 文件操作失败 */
#define DOT_EC_NO_ENOUGH_MEMORY                 -1002    /**< 内存不足 */
#define DOT_EC_NULL_POINTER                     -1003    /**< 空指针 */
#define DOT_EC_OP_FAILED                        -1004    /**< 硬件操作错误 */
#define DOT_EC_INIT_ERROR                       -1006    /**< 初始化错误 */
#define DOT_EC_SET_SYSTIME_ERROR                -1007    /**< 设置系统时间错误 */

/* 流媒体操作错误码 */
#define DOT_EC_HANDLE_STREAM_ERROR              -1100    /**< 音视频处理错误 */
#define DOT_EC_HANDLE_THUMBNAIL_ERROR           -1101    /**< 缩略图处理错误 */
#define DOT_EC_INVALID_AUDIO_PARAMS             -1102    /**< 无效的音频参数 */
#define DOT_EC_INVALID_VIDEO_PARAMS             -1103    /**< 无效的视频参数 */
#define DOT_EC_UNSUPPORT_PIC_TYPE               -1104    /**< 不支持的图片类型 */

/* 设备绑定错误码  */
#define DOT_EC_BIND_ID_NOTINITED                -1200    /**< 绑定所用的序列号未被初始化 */
#define DOT_EC_BIND_ID_USED                     -1201    /**< 绑定所用的序列号已被使用 */
#define DOT_EC_BIND_OTHER_FAILURE               -1202    /**< 其它绑定错误 */
#define DOT_EC_BIND_INFO_ERROR                  -1203    /**< 绑定信息不完整、或者错误 */
#define DOT_EC_NO_QRCODE                        -1204    /**< 没有扫描到二维码信息 */
#define DOT_EC_BIND_PASSWORD_WRONG              -1205    /**< 密码错误 */
#define DOT_EC_BIND_DEVICE_MODEL_ERROR          -1206    /**< 设备model ID不存在*/
#define DOT_EC_BIND_USER_INFO_ERROR             -1207    /**< 二维码中用户信息错误*/


/* 网络操作错误码 */
#define DOT_EC_WIFI_CONNECT_ERROR               -1300    /**< Wifi连接错误 */
#define DOT_EC_TCP_CONNECT_ERROR                -1301    /**< tcp连接服务器异常 */
#define DOT_EC_SSL_CONNECT_ERROR                -1302    /**< ssl连接服务器异常  */
#define DOT_EC_LOGIN_SERVER_ERROR               -1303    /**< 登录服务器失败 */
#define DOT_EC_TIMEOUT                          -1304    /**< 网络超时 */

/* 报警事件错误码  */
#define DOT_EC_HANDLE_ALARM_ERROR               -1400    /**< 内部报警处理错误 */
#define DOT_EC_INVALID_EVENT                    -1401    /**< 无效的事件类型 */


/* 其他错误码 */
#define DOT_EC_INVALID_STORAGE_STATUS           -1500    /**< 无效的本地存储状态 */
#define DOT_EC_INVALID_BATTERY_STATUS           -1501    /**< 无效的电池状态 */
#define DOT_EC_UNSUPPORT                        -1502    /**< 不支持的操作、命令或者消息等 */
#define DOT_EC_INVALID_PARAM                    -1503    /**< 无效的参数 */
#define DOT_EC_PARSE_KEEPLIVE_INFO_FAIL         -1504    /**< 获取保活信息失败 */
#define DOT_EC_INTERNAL_ERROR                   -1505    /**< 内部处理错误 */


#endif
