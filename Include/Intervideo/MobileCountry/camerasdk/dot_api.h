#ifndef __DOT_API_H__
#define __DOT_API_H__

#ifdef __cplusplus
  extern "C" {
#endif

/**@mainpage DOT SDK接口文档
* 本文档针对DOT SDK中相关的接口函数，回调函数以及数据结构进行说明
* @class
*/

 /**  
     *\file dot_api.h
     *
     *\brief DOT SDK API接口定义
     *
     *   
     *
     *\author 作者信息
     */


#include "dot_define.h"
#include "dot_callback.h"


/**
 * SDK初始化函数
 * @param [in] init_params  设备初始化参数
 * @param [in] cb  回调函数列表指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_Initialize(DoT_InitParams_st* init_params,  DoT_CallbackFuncList_st* cb);

/**
 * SDK反始化函数
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_DeInitialize(void);

/**
 * 获取SDK版本号信息
 * @param [out] ver  设备版本号
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_GetVersion(char* ver);

/**
 * 初始化设备能力集
 * @param [in] channel_id   通道id，IPC默认为0
 * @param [in] hw_capbility 硬件能力集属性
 * @param [in] sw_capbility 软件能力集属性
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_InitDeviceCapability(DoT_uint32 channel_id, DoT_DeviceHWCapability_st* hw_capbility, DoT_DeviceSWCapability_st* sw_capbility); 


/**
 * 启动SDK服务，初始化之后调用
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_StartService(void); 

/**
 * 停止SDK服务，反初始化之前调用
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_StopService(void); 

/**
 * 查询设备绑定状态，启动SDK服务之后调用，用于判断是否被绑定
 * @param [out] bind_status   绑定状态，DOT_TRUE:已绑定 DOT_FALSE：未绑定
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_QueryDeviceBindStatus(DoT_bool* bind_status);

/**
 * 启动绑定流程
 * @param [in] mode   绑定模式，支持同时启动AP绑定、有线绑定、二维码绑定中的一种或者多种，例如 mode = DOT_BIND_MODE_AP|DOT_BIND_MODE_WIRED 表示启动AP和有线绑定两种模式
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_StartDeviceBind(DoT_uint32 mode); 

/**
 * 停止绑定流程
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_StopDeviceBind(void);

/**
 * 推送音频流数据，设备绑定成功后应保持推流
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] subid   子通道id，IPC主码流默认为0，子码流为1
 * @param [in] info   音频编码信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_PushAudioData(DoT_uint32 channel_id, DoT_uint32 subid, DoT_AudioInfo_st* info);

/**
 * 推送视频流数据，设备绑定成功后应保持推流
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] subid   子通道id，IPC主码流默认为0，子码流为1
 * @param [in] info   视频编码信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
DoT_int32 DoT_PushVideoData(DoT_uint32 channel_id, DoT_uint32 subid, DoT_VideoInfo_st* info);

/**
 * 推送缩略图信息，建议5秒调用一次，jpg格式，分辨率640*360
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] pic_info   缩略图数据信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_PushThubmnail(DoT_uint32 channel_id,DoT_PictureInfo_st* pic_info);

/**
 * 上报报警事件信息
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] alarm_info  报警信息 
 * @param [in] utcms       报警时utc值，单位ms，若填写0，sdk会自动填写为调用该函数时的utc值
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_StartAlarm(DoT_uint32 channel_id, DoT_uint64 utcms, DoT_DeviceAlarmInfo_st* alarm_info);

/**
 * 停止报警事件信息
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] alarm_type  报警类型
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_StopAlarm(DoT_uint32 channel_id, DoT_AlarmType_en alarm_type);

/**
 * 上报设备运行状态信息
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] attr  状态信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_PushDeviceRunningStatus(DoT_uint32 channel_id, DoT_DeviceStatusInfo_st* attr);

/**
 * 设置存储设备信息(如：TF卡)
 * @param [in] channel_id  通道id，IPC默认为0
 * @param [in] info  存储设备信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_SetStorageInfo(DoT_uint32 channel_id, DoT_StorageInfo_st* info);

/**
 * 清空配置，恢复出厂设置
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_Reset();

/**
 * 低功耗设备获取保活服务器地址信息(休眠之前调用)
 * @param [out] info  保活服务器地址信息
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_GetKeepLiveInfo(DoT_KeepLiveInfo_st* info);

/**
 * 低功耗设备唤醒
 * @param [in] reason  设备唤醒的原因
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_Wakeup(DoT_WakeupReason_en reason);

/**
 * 设置login状态，该接口仅仅适用低功耗门铃设备，在DoT_StartService()之前调用，设置false状态，就不连接服务器，
 * 待网络就绪后，设置为true，此时会立刻连接服务器 （非必须接口）
 * @param [in] status 	false：不连接服务器，true：连接服务器
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_SetLoginStatus(DoT_bool status);

/**
 * 设置快速重连服务器，该接口仅仅适用中继模拟低功耗门铃设备休眠上线操作（非必须接口）
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_Reconnect();


/**
 * 设置SDK日志级别
 * @param [in] level  日志级别
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败 
 */
DoT_int32 DoT_SetLogLevel(DoT_LogLevel_en level);

#endif

#ifdef __cplusplus
}
#endif
