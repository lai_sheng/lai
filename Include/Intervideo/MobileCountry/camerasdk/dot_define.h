#ifndef __DOT_DATATYPES_H__
#define __DOT_DATATYPES_H__
 /**  
     *\file dot_define.h
     *
     *\brief DOT SDK 数据结构定义
     *
     *   
     *
     *\author 作者信息
     */

#define DOT_MAX_PATH_LEN                            256     /**< 最大路径长度 */
#define DOT_MAX_MAC_LEN                             32      /**< MAC地址最大长度 */
#define DOT_MAX_VERSION_LEN                         50      /**< 版本号最大长度 */
#define DOT_MAX_ID_LEN                              64      /**< 序列号最大长度 */
#define DOT_MAX_SERVER_ADDRESS_LEN                  256     /**< 地址最大长度 */
#define DOT_MAX_HEARTBEAT_PACKET_LEN                128     /**< 心跳包最大长度 */
#define DOT_MAX_WAKEUP_PACKET_LEN                   128     /**< 唤醒包最大长度 */
#define DOT_MAX_KEEP_LIVE_SERVER_NUM                4       /**< 保活服务器最大个数 */



#define DOT_FALSE  0        /**< 基础类型定义 */
#define DOT_TRUE   1        /**< 基础类型定义 */

typedef char                    DoT_bool;       /**< 基础类型定义 */
typedef unsigned int            DoT_uint32;     /**< 基础类型定义 */
typedef int                     DoT_int32;      /**< 基础类型定义 */
typedef unsigned long long      DoT_uint64;     /**< 基础类型定义 */
typedef long long               DoT_int64;      /**< 基础类型定义 */
typedef char                    DoT_char;       /**< 基础类型定义 */
typedef unsigned char           DoT_uchar;      /**< 基础类型定义 */
typedef void                    DoT_void;       /**< 基础类型定义 */
typedef float                   Dot_float;      /**< 基础类型定义 */

/** 设备绑定类型 */
typedef enum {
    DOT_BIND_MODE_AP      = 1,   /**< AP热点绑定模式 */
    DOT_BIND_MODE_WIRED   = 2,   /**< 有线绑定方式 */
    DOT_BIND_MODE_QRCODE  = 4,   /**< 二维码绑定方式 */
} DoT_BindMode_en;

/** 设备类型枚举定义 */
typedef enum {
    DOT_DEVICE_TYPE_IPC,                /**< 普通长上电IPC设备 */
    DOT_DEVICE_TYPE_NVR,                /**< NVR设备，暂不支持 */
    DOT_DEVICE_TYPE_LOWPOWER_DOORBELL,  /**< 低功耗门铃设备  */
    DOT_DEVICE_TYPE_LOWPOWER_IPC,       /**< 低功耗IPC设备 */
    DOT_DEVICE_TYPE_REPEATER_SINGLE,    /**< 中继，目前只支持一拖一 */
} DoT_DeviceType_en;

/** 日志级别 */
typedef enum {
    DOT_LOG_LEVEL_DEBUG = 1,			
    DOT_LOG_LEVEL_INFO,					
    DOT_LOG_LEVEL_ERROR,				
    DOT_LOG_LEVEL_NONE = 0xff,
} DoT_LogLevel_en;

/** 设备硬件能力集属性 */
typedef struct {
    DoT_bool hw_support_wifi;             /**< 硬件支持WIFI */
    DoT_bool hw_support_tfcard;           /**< 硬件支持TF卡 */
    DoT_bool hw_support_speaker;          /**< 硬件支持扬声器 */
    DoT_bool hw_support_pir;              /**< 硬件支持PIR侦测 */
    DoT_bool hw_support_ptz_pan;          /**< 硬件支持PTZ左右转动 */
    DoT_bool hw_support_ptz_tilt;         /**< 硬件支持PTZ上下转动 */
    DoT_bool hw_support_microphone;       /**< 硬件支持麦克风采集声音 */
    DoT_bool hw_support_battery;          /**< 硬件支持电池供电 */
    DoT_bool hw_support_led_indicate;     /**< 硬件支持状态指示灯 */
    DoT_bool hw_support_night_vision;     /**< 硬件支持夜视模式 */
    DoT_bool hw_support_tamer_alarm;      /**< 硬件支持防拆报警 */
    DoT_bool hw_support_light;            /**< 硬件支持白光灯控制 */
    DoT_bool hw_support_ring;             /**< 硬件支持门铃按键 */
    DoT_bool hw_support_doorlock;                   /**< 支持门锁功能 */
    DoT_bool hw_cap_reserve[60];          /**< 预留字段 */ 
}DoT_DeviceHWCapability_st;

/** 设备软件能力集属性 */
typedef struct {
    DoT_bool sw_support_rotate;                     /**< 支持画面180度翻转  */
    DoT_bool sw_support_bi_directional_audiotalk;   /**< 支持双向语音对讲  */
    DoT_bool sw_support_motion_detect;              /**< 支持运动检测报警 */
    DoT_bool sw_support_mition_detect_region;       /**< 支持运动检测区域 */
    DoT_bool sw_support_motion_track;               /**< 支持运动跟踪 */
    DoT_bool sw_support_motion_detect_sesitivity;   /**< 支持运动检测灵敏度设置 */
    DoT_bool sw_support_sound_detect;               /**< 支持声音检测报警 */
    DoT_bool sw_support_sound_detect_sensitivity;   /**< 支持声音检测灵敏度设置 */
    DoT_bool sw_support_people_detect;              /**< 支持人形检测报警 */
    DoT_bool sw_support_speaker_volume;             /**< 支持扬声器音量调节 */
    DoT_bool sw_support_doorbell_volume;            /**< 支持门铃音量调节 */
    DoT_bool sw_support_video_resolution_modify;    /**< 支持视频分辨率修改 */
    DoT_bool sw_support_pir_sensitivity;            /**< 支持PIR灵敏度设置 */
    DoT_bool sw_support_antiflicker;                /**< 支持防闪烁设置（50HZ/60HZ） */
    DoT_bool sw_support_ptz_preset_position;        /**< 支持PTZ预置点设置 */
    DoT_bool sw_support_defense_alarm;              /**< 支持防御报警设置 */
    DoT_bool sw_support_full_color;                 /**< 支持全彩设置 */
    DoT_bool sw_support_remote_wakeup;               /**< 支持远程唤醒 */

    DoT_bool sw_cap_reserver[80];                   /**< 预留字段 */ 

} DoT_DeviceSWCapability_st;

/** 报警开始结束标记 */
typedef enum {
    DOT_ALARM_START = 0,   /**< 报警开始 */ 
    DOT_ALARM_STOP  , /**< 报警结束 */
} DoT_AlarmStatus_en;


/** 设备分辨率信息 */
typedef enum {
    DOT_VIDEO_RESOLUTION_360P,
    DOT_VIDEO_RESOLUTION_480P,
    DOT_VIDEO_RESOLUTION_720P,
    DOT_VIDEO_RESOLUTION_1080P,
    DOT_VIDEO_RESOLUTION_2048_X_1536,//3M
    DOT_VIDEO_RESOLUTION_2304_X_1296,//3M
} DoT_VideoResolution_en;

/** 设备初始化信息 */
typedef struct {
    DoT_char 			config_path[DOT_MAX_PATH_LEN];           /**< 配置文件目录路径，至少预留80KB左右空间，必须是flash中的可读写路径 */
    DoT_char 			log_path[DOT_MAX_PATH_LEN];              /**< 日志文件目录路径，通常建议使用tmp路径，如果路径为空则打印在屏幕上 */
    DoT_char 			fw_version[DOT_MAX_VERSION_LEN];         /**< 设备固件版本 */
    DoT_char 			mac[DOT_MAX_MAC_LEN];                    /**< 设备MAC地址，不带冒号，采用小写字母格式 */
    DoT_char 			serial_number[DOT_MAX_ID_LEN];           /**< 设备序列号，由平台预先分配 */
    DoT_char 			device_model[64];                        /**< 设备型号ID，由平台预先分配 */
    DoT_uint32 			max_channel_num;                         /**< 设备最大通道个数，普通IPC默认为1 */
    DoT_uint32          streams_per_channel;       				 /**< 每个通道的流个数，默认为1 */
    DoT_uint32 			buf_size_per_stream;                     /**< 每条流的缓存空间大小，最小1MByte，（建议720P 1M， 1080P 2M)，最大不超过5M */
    DoT_DeviceType_en   device_type;               				 /**< 设备类型 */
    DoT_uint32          log_max_line;                            /**< 日志最大行数（0的话默认10000行，最小1000行） */ 
} DoT_InitParams_st;

/** 音视频编码类型 */
typedef enum  {
    DOT_VIDEO_TYPE_BEGIN = 1,
    DOT_VIDEO_TYPE_H264 = DOT_VIDEO_TYPE_BEGIN,   
    DOT_VIDEO_TYPE_H265,                          
    DOT_VIDEO_TYPE_END,

    DOT_AUDIO_TYPE_BEGIN = 11,
    DOT_AUDIO_TYPE_G711A = DOT_AUDIO_TYPE_BEGIN,  
    DOT_AUDIO_TYPE_G711U,                         
    DOT_AUDIO_TYPE_END,
} DoT_MediaType_en;

/** 视频帧类型 */
typedef enum {
    DOT_VIDEO_FRAME_TYPE_IFRAME,                  /**< 关键帧 */
    DOT_VIDEO_FRAME_TYPE_PFRAME,                  /**< P帧 */
    DOT_VIDEO_FRAME_TYPE_BFRAME                   /**< B帧 */
} DoT_VideoFrameType_en;

/** 音频帧信息 */
typedef struct {
    DoT_uint32              timestamp;                /**< 音频帧时间戳，音视频需要保证同步(差值100ms以内) ，回调语音对讲数据时，此参数无意义*/
    DoT_uint64              utcms;                    /**< 音频utc值，单位ms，若填写0，sdk会自动填写为调用该函数时的utc值 */
    DoT_uint32              seq;                      /**< 音频帧序号，音频和视频分别计数，越界后从0重新开始，回调语音对讲数据时，此参数无意义 */
    DoT_MediaType_en        media_type;               /**< 音频编码类型 */
    DoT_void*               data;                     /**< 音频数据 */
    DoT_uint32              data_size;                /**< 数据包长度（字节）*/
    DoT_uint32              channel_num;              /**< 音频声道数 */
    DoT_uint32              sample_rate;              /**< 音频采样率 */
    DoT_uint32              sample_bit;               /**< 采样大小 */
} DoT_AudioInfo_st;

/** 视频帧信息 */
typedef struct {
    DoT_uint32              timestamp;                  /**< 视频时间戳，音视频需要保证同步(差值100ms以内) */    
    DoT_uint64              utcms;                      /**< 视频utc值，单位ms，若填写0，sdk会自动填写为调用该函数时的utc值 */
    DoT_uint32              seq;                        /**< 视频帧序号，音频和视频分别计数，越界后从0重新开始 */
    DoT_MediaType_en        media_type;                 /**< 视频编码类型 */
    DoT_void*               data;                       /**< 视频数据 */
    DoT_uint32              data_size;                  /**< 数据包长，长度最大不超过300KB*/
    DoT_VideoFrameType_en   frame_type;                 /**< 视频帧类型 */
    DoT_uint32              width;                      /**< 图像宽度 */
    DoT_uint32              height;                     /**< 图像高度 */
} DoT_VideoInfo_st;

/** 码流编码类型 */
typedef struct {
    DoT_MediaType_en        main_video_type;            /**< 主码流视频编码类型，没有主码流视频取无效类型DOT_VIDEO_TYPE_END */
    DoT_MediaType_en        sub_video_type;             /**< 子码流视频编码类型，没有子码流视频取无效类型DOT_VIDEO_TYPE_END */
    DoT_MediaType_en        main_audio_type;            /**< 主码流音频编码类型，没有主码流音频取无效类型DOT_AUDIO_TYPE_END */
    DoT_MediaType_en        sub_audio_type;             /**< 子码流音频编码类型，没有子码流音频取无效类型DOT_AUDIO_TYPE_END */
} DoT_StreamMediaType_st;

/** 图片格式定义 */
typedef enum {
    DOT_PICTURE_TYPE_JPG,                    /**< JPG图片 */
} DoT_PictureType_en;

/** 图片信息，缩略图目前只支持 640*360 分辨率 */
typedef struct {
    DoT_void*               data;                       /**< 图片数据 */
    DoT_uint32              data_size;                  /**< 图片数据长度 */
    DoT_PictureType_en      pic_type;                   /**< 图片类型，目前只支持jpg图片 */
    DoT_uint32              width;                      /**< 图片宽度，缩略图固定为640 */
    DoT_uint32              height;                     /**< 图片高度，缩略图固定为360 */
} DoT_PictureInfo_st;

/** 存储设备状态 */
typedef enum {
    DOT_STORAGE_STATUS_READY,                          /**< 设备外部存储(如TFCard)插入，且状态正常 */
    DOT_STORAGE_STATUS_NOT_READY,                      /**< 设备无外部存储   */
    DOT_STORAGE_STATUS_ABNORMAL,                       /**< 设备外部存储异常 */
    DOT_STORAGE_FILE_SYSTEM_NOT_SUPPORT,               /**< 不支持的文件系统 */
} DoT_StorageStatus_en;

/** 存储设备类型 */
typedef enum {
    DOT_STORAGE_TYPE_TFCARD, 
} DoT_StorageType_en;

/** 存储设备状态和路径信息 */
typedef struct {
    DoT_StorageType_en  storage_type;				/**< 存储类型 */
    DoT_StorageStatus_en storage_status;			/**< 存储设备状态 */
    DoT_char storage_path[DOT_MAX_PATH_LEN];		/**< 存储设备路径，storage_type=DOT_STORAGE_STATUS_READY时有效 */
} DoT_StorageInfo_st;

/** 存储空间大小 */
typedef struct  {    
    DoT_uint64      total_size;                     /**< 本地存储空间总大小，单位：字节 */
    DoT_uint64      free_size;                      /**< 本地存储空间剩余大小，单位：字节 */
    DoT_char        file_system[32];                /**< 文件系统格式,如： fat32, extfat*/
    DoT_StorageType_en  storage_type;               /**< 存储类型 */
} DoT_StorageSize_st;

/** 低功耗设备电池状态 */
typedef enum {
    DOT_BATTERY_STATUS_CHARGING,                    /**< 正在充电 */
    DOT_BATTERY_STATUS_FULL,                        /**< 电池充满了 */
    DOT_BATTERY_STATUS_NORMAL,                      /**< 正常状态  */ 
    DOT_BATTERY_STATUS_NO_BATTERY,                  /**< 没有电池  */
    DOT_BATTERY_STATUS_LOW                          /**< 低电量 */
} DoT_DeviceBatteryStatus_en;

/** 低功耗设备唤醒原因 */
typedef enum {
    DOT_WAKEUP_NONE = 0,
    DOT_WAKE_UP_BY_RING = 1,                  /**< 手动按键唤醒 */
    DOT_WAKE_UP_BY_PIR,                       /**< PIR检测唤醒 */
    DOT_WAKE_UP_BY_REMOTE,                    /**< 客户端远程唤醒 */
    DOT_WAKE_UP_OTHER,                        /**< 其他原因唤醒 */
} DoT_WakeupReason_en;

/** 中继或NVR子设备状态 */
typedef enum {
    DOT_SUB_DEV_STATUS_ONLINE = 1,    /**< 子设备上线 */
    DOT_SUB_DEV_STATUS_OFFLINE,       /**< 子设备离线 */
    DOT_SUB_DEV_STATUS_SLEEP,         /**< 子设备休眠 */
} DoT_SubDeviceStatus_en;

/** 低功耗设备保活信息 */
typedef struct {    
    DoT_uint32      heartbeat_interval;                             /**< 低功耗设备休眠之后，发往服务器的心跳包间隔（秒）*/
    DoT_char        heartbeat_pkt[DOT_MAX_HEARTBEAT_PACKET_LEN];    /**< 低功耗设备休眠之后，按照指定间隔发送的心跳包内容 */
    DoT_uint32      heartbeat_pkt_len;                              /**< 心跳包内容长度 */
    DoT_char        wakeup_pkt[DOT_MAX_WAKEUP_PACKET_LEN];          /**< 唤醒包内容，客户端远程唤醒时，保活服务器发往低功耗设备 */
    DoT_uint32      wakeup_pkt_len;                                 /**< 唤醒包内容长度*/

    /**< 保活服务器地址信息*/
    struct {
        DoT_char    server_ip[DOT_MAX_SERVER_ADDRESS_LEN];          /**< 保活服务器IP地址 */
        DoT_uint32  server_port;                                    /**< 保活包服务器TCP端口 */
    } server_address[DOT_MAX_KEEP_LIVE_SERVER_NUM];                 /**< 服务器地址列表 */
    DoT_uint32     server_count;                                    /**< 保活服务器的个数，目前最多2个，低功耗设备休眠之后，需要跟列表中的保活服务器都建立连接 */
} DoT_KeepLiveInfo_st;


/** WIFI加密模式 */
typedef enum  {
    DoT_WIFI_MODE_NULL = -1,
    DoT_WIFI_MODE_OPEN,
    DoT_WIFI_MODE_WPA,
    DoT_WIFI_MODE_WPA2,
    DoT_WIFI_MODE_WEP,
    DoT_WIFI_MODE_WPA_WPA2,
    DoT_WIFI_MODE_MAX,
} DoT_WifiEncryptMode_en;

/** WIFI信息 */
typedef struct {
    DoT_char             bssid[128];        /**< BSSID */
    DoT_char             ssid[128];         /**< SSID */
    DoT_char             password[128];     /**< 密码 */
    DoT_int32            signal;            /**< 信号强度(0 - 5) */
    DoT_WifiEncryptMode_en     encrypt_mode;           /**< 加密模式 */
} DoT_WifiInfo_st;

/** WIFI列表 */
typedef struct {
    DoT_WifiInfo_st  *wifi_list;            /**< wifi列表 */
    DoT_int32  wifi_count;                  /**< wifi列表个数 */
} DoT_WifiList_st;

/** 矩形区域坐标信息 */
typedef struct {
    DoT_uint32 upper_left_x;                       /**< 矩形左上角x坐标（0~4）*/
    DoT_uint32 upper_left_y;                       /**< 矩形左上角y坐标（0~4）*/
    DoT_uint32 lower_right_x;                      /**< 矩形右下角x坐标（1~5）*/
    DoT_uint32 lower_right_y;                      /**< 矩形右下角y坐标（1~5）*/
} DoT_RegionInfo_st;

/** 矩形区域坐标信息列表 */
typedef struct  {
    DoT_uint32 regions_num;                /**< 侦测区域个数，范围0~25 */
    DoT_RegionInfo_st *rect;               /**< 侦测区域坐标信息列表，包含每个矩形检测区域的坐标 */
} DoT_DetectRegionList_st;

/** 电池状态信息 */
typedef struct {
    DoT_DeviceBatteryStatus_en status;         /**< 电池状态 */
    DoT_int32 percent;                         /**< 电量百分比，范围0~100 */
} DoT_BatteryInfo_st;  

/** 固件升级包信息 */
typedef struct {
    DoT_uint64                  pkg_size;          /**< 固件升级包大小 */
    DoT_char                    md5[64];           /**< 固件升级包文件md5值，用于一致性校验 */
    DoT_char                    url[256];          /**< 升级包的URL地址，SDK中会自动下载升级包，用户也可以在安装失败的情况下主动下载, 需务必支持http和https*/
} DoT_UpgradePackageInfo_st;

/** 数据包信息 */
typedef struct {
    DoT_char        *data;          /**< 数据块 */
    DoT_uint32      data_size;      /**< 数据块大小 */
} DoT_DataInfo_st;

/** 状态信息类型 */
typedef enum {
    DOT_EVENT_DEVICE_STATUS_BATTERY,            /** <电池电量事件上报 */
    DOT_EVENT_NETWORK_STATUS_CHANGE,            /** <网络切换，比如有线、无线、4G网络切换 */
} DoT_StatusType_en;

/** 设备状态信息 */
typedef struct {
    union {
        /**< event为DOT_EVENT_DEVICE_STATUS_BATTERY时需要填写该字段 */
        struct {
            DoT_DeviceBatteryStatus_en status;         /**< 电池状态 */
            DoT_int32 percent;                         /**< 电量百分比，范围0~100 */
        } battery_info;         

        /**< 此字段暂时不需要填写 */
        struct {
            DoT_char wifiname[64];             /**< 表示wifi的ssid名字 */
            DoT_uint32 strength;               /**< 表示wifi的信号强度 */
        } wifi_value;
    };
    DoT_StatusType_en  status_type;     /**< 状态信息类型 */
} DoT_DeviceStatusInfo_st;

/** PTZ转动方向 */
typedef enum {
    DOT_PTZ_MOVE_NONE=0,
    DOT_PTZ_MOVE_LEFT,
    DOT_PTZ_MOVE_RIGHT,
    DOT_PTZ_MOVE_UP,
    DOT_PTZ_MOVE_DOWN,
} DoT_PTZMoveDirection_en;

/** PTZ转动位置坐标信息 */
typedef struct {
    Dot_float pan;        /**< PTZ的水平方向坐标，[-1.000000~1.000000]，左为负，右为正 */
    Dot_float tilt;       /**< PTZ的垂直方向坐标，[-1.000000~1.000000]，下为负，上为正 */
} DoT_PtzSpace_st;

/** 防御报警触发信息 */
typedef struct {
    DoT_int32 type;          /**< 动作类型： 1：打开声音警报   2：打开白光灯   4：打开蜂鸣器 */
    DoT_int32 spkdur;        /**< 扬声器警报时长 */
    DoT_int32 litmode;       /**< 白光灯亮灯模式  */
    DoT_int32 litdur;        /**< 白光灯亮灯时长   */
    DoT_int32 buzdur;        /**< 蜂鸣器的报警时长 */
} Dot_Strategy_Action;


/** 报警事件类型定义 */
typedef enum {
    DOT_ALARM_BEGIN = 0,
    DOT_ALARM_MOTION_DETECT,               /**< 移动侦测，需要上报开始和结束 */
    DOT_ALARM_SOUND_DETECT,                /**< 声音侦测，需要上报开始和结束 */
    DOT_ALARM_PIR_DETECT,                  /**< PIR侦测，脉冲事件，只需要上报开始 */
    DOT_ALARM_PEOPLE_DETECT,               /**< 人形检测，需要上报开始和结束 */
    DOT_ALARM_RING,                        /**< 门铃按键，脉冲事件，只需要上报开始 */
    DOT_ALARM_TAMPER_ALARM,                /**< 防拆报警，脉冲事件，只需要上报开始 */
    DOT_ALARM_MAX = 0xFFFF,
} DoT_AlarmType_en;

/** 设备报警附加信息 */
typedef struct {
    DoT_AlarmType_en  alarm_type;   /**< 报警类型 */
} DoT_DeviceAlarmInfo_st;

/** 设备绑定结果信息 */
typedef struct  {
    DoT_int32 error_code;                     /**< 错误码*/
    DoT_char username[256];                 /**< 用户名，绑定成功时有效 */
} DoT_DeviceBindInfo_st;

#endif


