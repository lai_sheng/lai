#ifndef __DOT_CALLBACK_H__
#define __DOT_CALLBACK_H__
 /**  
     *\file dot_callback.h
     *
     *\brief DOT SDK 回调函数定义
     *
     *   
     *
     *\author 作者信息
     */

/** 设备设置参数相关回调 */
typedef enum {
    /// 通用配置
    /// 设置指示灯开关状态 \n 回调指针类型：int \n 取值范围： 0：关闭 1：打开
    DOT_CMD_SET_LED_STATUS = 0x0,

    /// 设置夜视模式开关状态 \n 回调指针类型：int \n 取值范围： 0：关闭 1：打开 2：自动
    DOT_CMD_SET_NIGHTVISIGON_MODE,

    /// 设置防闪烁频率 \n 回调指针类型：int \n 取值范围：50:50hz  60:60hz 
    DOT_CMD_SET_ANTIFLICKER_VALUE,

    /// 设置图像翻转 \n 回调指针类型：int \n 取值范围：0：不翻转 180：翻转180度
    DOT_CMD_SET_VIDEO_ROTATE_VALUE, 

    /// 设置摄像头分辨率切换，切换后的第一帧必须是关键帧 \n 回调指针类型：int \n 
    ///      取值范围：0： 分辨率640*360, 1：1280*720或1920*1080(根据设备自身能力决定) 
    DOT_CMD_SET_VIDEO_QUALITY_STATUS,

    /// 设置设备开关状态，关闭时停止数据流和事件注入 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_DEVICE_STATUS, 


    /// 事件检测配置
    /// 设置运动检测功能开关，关闭后设备停止运动检测功能，目前暂不支持
    DOT_CMD_SET_MOTION_DETECTION_STATUS = 0x100,            

    /// 设置运动侦测灵敏度 \n 回调指针类型：int \n 取值范围：1~5，值越大，灵敏度越高 
    DOT_CMD_SET_MOTION_DETECTION_SENSITIVITY,

    /// 设置运动侦测区域 \n 回调指针类型：DoT_DetectRegionList_st
    DOT_CMD_SET_MOTION_DETECTION_REGION,            

    /// 设置声音检测功能开关，关闭后设备停止声音检测功能，目前暂不支持
    DOT_CMD_SET_SOUND_DETECTION_STATUS,       

    /// 设置声音侦测灵敏度 \n 回调指针类型：int \n 取值范围：1~5，值越大，灵敏度越高
    DOT_CMD_SET_SOUND_DETECTION_SENSITIVITY,

    /// 设置PIR检测开关状态 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_PIR_STATUS,  

    /// 设置PIR灵敏度 \n 回调指针类型：int \n 取值范围：0~100， 值越大越灵敏
    DOT_CMD_SET_PIR_SENSITIVITY,                   

    /// 设置人形检测功能开关状态 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_PEOPLE_DETECTION_STATUS,



    /// 设置防拆报警功能开关状态 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_TAMPER_ALARM_STATUS,                 

 
    /// 声音配置
    /// 设置麦克风开关状态 \n 回调指针类型：int \n 取值范围： 0：关闭 1：打开
    DOT_CMD_SET_MIC_STATUS = 0x200,

    /// 设置麦克风采集音量大小，目前暂不支持
    DOT_CMD_SET_MICPOHNE_VALUE,

    /// 设置扬声器音量大小 \n 回调指针类型：int \n 取值范围：0~100
    DOT_CMD_SET_SPEAKER_VOLUME,

    /// 设置扬声器开关状态 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_SPEAKER_STATUS,

    /// 设置门铃音量大小 \n 回调指针类型：int \n 取值范围：0~100
    DOT_CMD_SET_DOORBELL_VOLUME,

    /// 智能算法配置
    /// 设置运动跟踪开关状态 \n 回调指针类型：int \n 取值范围：0：关闭 1：打开
    DOT_CMD_SET_MOTION_TRACKING_STATUS = 0x230,             


    /// 设置全彩模式开关状态 \n 回调指针类型：int \n 取值范围： 0：关闭 1：打开 2：自动
    DOT_CMD_SET_FULL_COLOR_STATUS = 0x233,     


} DoT_SetDevConfigCmd_en;

/** 获取设备运行状态信息回调 */
typedef enum {

    /// 获取WIFI强度 \n 回调指针类型： int \n 取值范围：TODO
    DOT_CMD_GET_WIFI_SIGNAL_QUANLITY,       

    /// 获取WIFI列表 \n 回调指针类型： DoT_WifiList_st
    DOT_CMD_GET_WIFI_LIST, 

    /// 获取当前WIFI信息 \n 回调指针类型： DoT_WifiInfo_st
    DOT_CMD_GET_CURRENT_WIFI,     

    /// 获取电池状态信息\n 回调指针类型： DoT_DeviceBatteryStatus_en \n 取值范围：参考枚举定义 
    DOT_CMD_GET_BATTERY_INFO,               

     /// 获取电池电量百分比信息 \n 回调指针类型： int \n 取值范围：0~100
    DOT_CMD_GET_BATTERY_PERCENT,

    /// 获取本地存储分区大小信息 \n 回调指针类型：DoT_StorageSize_st
    DOT_CMD_GET_STORAGE_PARTITION, 

    /// 获取码流编码类型 \n  回调指针类型：DoT_StreamMediaType_st
    DOT_CMD_GET_MEDIA_ATTR, 

} DoT_GetDevRunningInfo_en;

/** 设备控制相关回调 */
typedef enum {

    /// 连接WIFI \n 回调指针类型：DoT_WifiInfo_st
    DOT_CMD_CHANGE_WIFI, 

    /// 设置系统utc时间，设备收到此回调需要修改系统的时间 \n 回调指针类型：DoT_uint64， 单位：毫秒 \n 
    DOT_CMD_SYNC_SYSTEM_TIME,

    /// 重启设备  \n 回调指针类型：NULL 
    DOT_CMD_REBOOT_DEVICE,

    /// 格式化本地存储(TFCARD) \n 回调指针类型：NULL
    DOT_CMD_FORMAT_LOCAL_STORAGE,

    /// 获取设备日志信息，设备需要支持tar命令， 如果设备没有日志信息需要打包，直接返回即可 \n
    /// 回调指针类型： char
    DOT_CMD_GET_LOGS, 

    /// 开始触发防御报警，设备收到此回调后需要按照指定的策略执行相应的报警  \n 
    /// 回调指针类型： Dot_Strategy_Action
     DOT_CMD_START_DEFENSE_ALARM, 

     /// 停止触发防御报警，目前暂时没有使用 \n 回调指针类型： NULL 
    DOT_CMD_STOP_DEFENSE_ALARM, 

    /// 防拆报警清除通知，设备收到此回调需要停止防拆警报声 \n 回调指针类型： int \n 取值说明：0:不清除报警  1:清除报警
    DOT_CMD_STOP_TAMPER_ALARM, 

} DoT_ControlDevCmd_en;

/** PTZ控制命令回调 */
typedef enum {    

    /// 获取当前PTZ位置坐标 \n 回调指针类型：DoT_PtzSpace_st  
    DOT_CMD_PTZ_GET_POSITION,               

    /// 控制PTZ转到指定坐标的位置 \n 回调指针类型：DoT_PtzSpace_st 
    DOT_CMD_PTZ_MOVE_TO_POSITION,

    /// 控制PTZ转动到初始位置  \n 回调指针类型：NULL
    DOT_CMD_PTZ_GOTO_HOME,

    /// 控制PTZ步进转动，步长由设备自行控制 \n 
    ///  回调指针类型：DoT_PTZMoveDirection_en \n 取值说明： 转动方向
    DOT_CMD_PTZ_MOVE_BY_STEP,

    /// 控制PTZ开始连续转动，转动速度设备自行控制 \n
    /// 回调指针类型：DoT_PTZMoveDirection_en \n 取值说明： 转动方向
    DOT_CMD_PTZ_START_MOVE, 

    /// 控制PTZ停止转动，和DOT_CMD_PTZ_START_MOVE配套使用 \n 回调指针类型：NULL
    DOT_CMD_PTZ_STOP_MOVE,

} DoT_PTZControl_en;

/** 语音对讲相关回调 */
typedef enum {     

    /// 开始播放音频 \n  回调指针类型：NULL
    DOT_CMD_AUDIO_PLAY_START, 

    /// 停止播放音频 \n  回调指针类型：NULL
    DOT_CMD_AUDIO_PLAY_STOP,

    /// 播放的音频数据 \n 回调指针类型：DoT_AudioInfo_st 
    DOT_AUDIO_PLAY_DATA,  

} DoT_AudioPlayCmd_en;


/** 设备运行时状态信息回调 */
typedef enum {    
    ///  客户端播放通知 \n 回调指针类型：int \n 取值说明：0:无人播放  1：有人播放
    DOT_MSG_PLAY_STATUS,  

    /// 设备处于没有事件，也没有直播的状态，建议设备休眠(低功耗设备),
    /// 设备收到此回调时，需要结合当前设备的状态判断是否需要进入休眠，
    /// 比如在升级过程中是不能休眠的 \n 回调指针类型：NULL  
    DOT_MSG_DEV_INACTIVE,  

    ///  设备码率受限状态通知，如果码率过大，SDK会停止上传数据，设备需要降低码率 \n
    ///  回调指针类型：int  \n 取值说明：0：码流过大停止上传  1：码流恢复正常 
    DOT_MSG_STREAM_RATELIMIT_STATUS,  

    ///  连接服务器成功，设备根据此通知可以控制指示灯状态  \n 回调指针类型：NULL
    DOT_MSG_SERVER_ONLINE,

    /// 和服务器连接断开，设备根据此通知可以控制指示灯状态  \n 回调指针类型：NULL
    DOT_MSG_SERVER_OFFLINE,         

    /// 设备解绑通知，设备根据此通知控制指示灯状态，并且重新进入绑定模式 \n
    /// 回调指针类型：NULL 
    DOT_MSG_SERVER_UNBIND,          

} DoT_ServerStatusMsg_en;


/** 固件升级命令回调 */
typedef enum {    

    ///  固件升级包信息，SDK开始下载升级包时调用，用于下载完毕后做完整性校验 \n
    /// 回调指针类型：DoT_UpgradePackageInfo_st 
    DOT_CMD_UPGRADE_PKG_INFO,   
    
    /// 下载固件升级包数据  \n 回调指针类型：DoT_DataInfo_st 
    DOT_CMD_UPGRADE_DOWNLOAD,   
        
    /// 下载固件升级包完毕，设备需对升级包做md5校验  \n 回调指针类型：NULL 
    DOT_CMD_UPGRADE_DOWNLOAD_FINISH,  
            
    ///  下载固件升级包失败  \n 回调指针类型：NULL
    DOT_CMD_UPGRADE_DOWNLOAD_FAIL,      
    
    ///  开始安装固件升级包 \n 回调指针类型：NULL 
    DOT_CMD_UPGRADE_INSTALL,        
} DoT_UpgradeCmd_en;

/** 绑定状态回调 */
typedef enum {

    /// 开始进入绑定状态，设备调用DoT_StartDeviceBind接口后会收到此回调，设备需要启动相应的模块,
    /// 如果设置了二维码绑定模式，设备需要进入二维码扫描状态,如果设置了AP绑定模式，设备需要启动AP热点，
    /// 并且必须在热点启动成功之后（网关有IP地址）返回，如果只设置了有线绑定，则不需要做任何处理\n 
    /// 回调指针类型：NULL
    DOT_DEVICE_BIND_REQUEST_INFO_START,     

    /// 获取二维码信息，包括结束符最长512字节，设备启动二维码绑定方式后会持续收到此回调，直到解析到正确
    /// 的二维码为止  \n  回调指针类型： char 
    DOT_DEVICE_BIND_REQUEST_QRCODE_INFO,    

    /// 退出绑定状态，SDK收到绑定信息后，设备会收到此回调，需要停止相应模块，如果启动了二维码绑定模式，需要
    /// 退出二维码扫描，如果启动了AP绑定模式，需要退出热点模式 \n 回调指针类型：NULL
    DOT_DEVICE_BIND_REQUEST_INFO_STOP,     

    /// 设备配网连接WIFI，建议在连接WIFI成功并且获取到IP地址之后返回 \n 回调指针类型：DoT_WifiInfo_st
    DOT_DEVICE_BIND_CONNECT_WIFI,
    
    /// 绑定成功 设备需要播放相应音频，并且修改指示灯状态 \n 回调指针类型：DoT_DeviceBindInfo_st
    DOT_DEVICE_BIND_SUCCEED, 

    /// 绑定失败，设备需要播放相应音频，并且重新进入绑定状态 \n 回调指针类型：DoT_DeviceBindInfo_st
    DOT_DEVICE_BIND_FAILED, 
    
    /// 有线绑定连接状态通知， 设备需根据此状态控制指示灯闪烁  \n 回调指针类型：int \n
    /// 取值范围： 0：和服务器断开连接   1：连接服务器成功  
    DOT_DEVICE_BIND_WIRED_CONNECT_STATUS, 


} DoT_BindStatus_en;

/**
 * 设备配置回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_SetDevConfig_Callback)(DoT_SetDevConfigCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 获取设备状态回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_GetDevRunningInfo_Callback)(DoT_GetDevRunningInfo_en info_e, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 设备控制回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_ControlDevCmd_Callback)(DoT_ControlDevCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 语音播放回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_AudioPlayCmd_Callback)(DoT_AudioPlayCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * PTZ控制回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_PTZControl_Callback)(DoT_PTZControl_en cmd, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 服务状态回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_ServerStatus_Callback)(DoT_ServerStatusMsg_en msg, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 在线升级回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_Upgrade_Callback)(DoT_UpgradeCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data);

/**
 * 设备绑定回调函数定义
 * @param [in] cmd  命令类型
 * @param [in] data 回调数据指针，根据不同的回调类型，转成不同的数据结构进行处理 
 * @param [in] user_data 用户数据指针
 * @return 返回函数执行结果
 * - DOT_EC_SUCCESS：成功
 * - 其他值：失败
 */
typedef int (*DoT_DevBindStatus_Callback)(DoT_BindStatus_en status, void* data, void* user_data);

/** 回调函数指针列表 */
typedef struct {
    DoT_SetDevConfig_Callback                cb_set_device_config;     /**< 参数设置回调 */
    DoT_GetDevRunningInfo_Callback           cb_get_running_info;      /**< 获取设备运行状态回调 */
    DoT_ControlDevCmd_Callback               cb_dev_ctrl;              /**< 设备控制回调 */
    DoT_AudioPlayCmd_Callback                cb_audio_play;            /**< 语音对讲回调   */
    DoT_PTZControl_Callback                  cb_ptz_ctrl;              /**< PTZ控制回调 */
    DoT_ServerStatus_Callback                cb_server_status;         /**< 服务状态通知回调 */
    DoT_Upgrade_Callback                     cb_upgrade;               /**< 固件升级回调 */
    DoT_DevBindStatus_Callback               cb_bind_status;           /**< 设备绑定状态回调 */
} DoT_CallbackFuncList_st;

#endif
