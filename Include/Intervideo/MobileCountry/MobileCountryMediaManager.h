
#ifndef __MOBILECOUNTRYMANAGER_H__
#define __MOBILECOUNTRYMANAGER_H__

#include <sys/stat.h>
#include <unistd.h>
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "APIs/DVRDEF.H"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"

#include "APIs/Net.h"
#include "APIs/System.h"



typedef enum{
	HM_AUDIO_FRAME   = 0x0001,
	HM_VIDEO_I_FRAME = 0x0002,
	HM_VIDEO_P_FRAME = 0x0004,
	HM_VIDEO_B_FRAME = 0x0008,	
}HumuFrameType;

//实时录2路流
//云录像  及云告警  云告警预录
class CMobileCountryMediaManager :public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(CMobileCountryMediaManager);
		CMobileCountryMediaManager();
		~CMobileCountryMediaManager();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();

		void ThreadProc();
		
		VD_INT32 StartVideo(int Channel, int StreamType);
		void OnCapture(int iChannel, uint iStreamType, CPacket *pPacket);
		VD_INT32 StartAudioIn(int channel);		
		VD_INT32 OnVideoData(CPacket *pPacket);
		VD_INT32 OnAudioData(int iChannel, CPacket *pPacket);
		void WritePacket();
		void WriteToHw(int isVideo,CPacket *pPacket);		
		VD_UINT32 FindIunit(uchar * buf, uint size);
		VD_INT32 WriteVideotoHw(uchar *pData, unsigned int iLen);
		void WriteAduiotoHw(CPacket *pPacket);
		void SetNtpSuccess(){m_iNtpSuccess = 1;}
		void SetEnable(int Enable);
		void SetSuspendMode(int mode);//{m_iSuspendMode = mode;}
	private:
		int 	m_iTimeCount;
		CTimer 		m_cRecordTimer;
	public:
		typedef struct PACKET_CAPTURE {
				int chn;
				int isVideo;
				CPacket* pPacket;
				uint dwStreamType;
		}PACKET_CAPTURE;

		
		//音视频队列
		typedef std::list<PACKET_CAPTURE> PACKET_LIST;
		uint m_packet_count;
		uint m_videopacket_count;
		uint m_audiopacket_count;  
		PACKET_LIST m_packet_list; //队列
		CMutex m_mutex_list;	  //队列锁

		
		CABuffer m_ptkBufMain;
		
		
		int   m_iNtpSuccess; //1: NTP时间同步成功;
		int   m_iCloudEnable; 
		int   m_iSuspendMode;
};

#define g_MobileCountryMediaManager  (*CMobileCountryMediaManager::instance())

#endif

