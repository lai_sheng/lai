
#ifndef __REALTIME_CALLBACK_H
#define __REALTIME_CALLBACK_H

#include "RealTime_apiv1.h"

#ifdef __cplusplus
extern "C" {
#endif

int32_t RealTimeGetUUID_Callback(char uuid[RT_UUID_LEN]);
int32_t RealTimeVideoOpen_Callback(uint8_t channel,RT_EncodeMode eCodeMode,uint32_t SuggestBitRate,RealTimeVideoDataFormat* videoinfo);
int32_t RealTimeGetVideoCurInfo_Callback(uint8_t channel,RT_EncodeMode *pCodeMode, RealTimeVideoDataFormat* videoinfo);
int32_t RealTimeSetVideoInfo_Callback(uint8_t channel,RT_EncodeMode eCodeMode,uint32_t SuggestBitRate, RealTimeVideoDataFormat* videoinfo);
int32_t RealTimeVideoClose_Callback(uint8_t channel);

int32_t RealTimeAudioOpen_CallBack(uint8_t channel,RealTimeAudioDataFormat* audioinfo);
int32_t RealTimeAudioClose_CallBack(uint8_t channel);

int32_t RealTimeFileListPerPage_Callback(uint8_t channel,RTFileListPerPage *FilePage,int NumsOfPerPage,char LastFileName[64]);
int32_t RealTimeQueryRecordFile_Callback(uint8_t channel,uint8_t ReType,QueryRecordTime FileTime,RTFileListPerPage_2 *FilePage);
int32_t RealTimeQueryRecordPage_Callback(uint8_t channel,uint8_t ReType,uint32_t StartStamp,uint32_t EndStamp, \
	uint32_t Page,uint32_t PageNum,RTFileListPerPage_3 *FilePage);
int32_t RealTimeRecordOpen_CallBack(uint8_t channel,char*  recordname,RealTimeVideoDataFormat* videoinfo, RealTimeAudioDataFormat* audioinfo,uint32_t* FileTotalTime);
int32_t RealTimeRecordCotrol_CallBack(uint8_t channel,CONTROLTYPE type,uint32_t value);
int32_t RealTimeGetSDInfo_Callback(RTSDInfo *info); 
int32_t RealTimeRecordFileDelete_CallBack(char RecordFileName[64]);

int32_t RealTimeReBoot_CallBack();
int32_t RealTimeSuspend_Callback(RTSystemStatus mode);
RTSystemStatus RealTimeGetCurSystemStatus_Callback();

int32_t RealTimePTZOpen_Callback(uint8_t channel);
int32_t RealTimePTZCmd_Callback(uint8_t channel,RT_PTZControlCmd ptzcmd, RT_ControlArgData* arg);
int32_t RealTimePTZClose_Callback(uint8_t channel);

int32_t RealTimeSetCurToPtzPoint_Callback(int id,int opt/*0:add,1:del*/);
int32_t RealTimeGetAllPtzPoint_Callback(RT_ALLPresetPointInfo *pInfo);
int32_t RealTimeSetPtzTraceRoute_Callback(RT_SetTraceRoute route);
int32_t RealTimeGetPtzTraceRoute_Callback(RT_SetTraceRoute *pRoute);

int32_t RealTimeAudioPlayStart_Callback(AUDIOPLY_TYPE EncordType,int32_t SampleRate,
	int32_t BitWidth,uint32_t Volume,int Priority/*the biger ,the higher*/);
int32_t RealTimeAudioPlayProGress_Callback(uint8_t* buf, int32_t size);
int32_t RealTimeAudioPlayStop_Callback(void);

int32_t RealTimeImageMirrorFlip_Callback(MirrorFlip direct);
int32_t RealTimeImageGetMirrorFlip_Callback(MirrorFlip *pdirect);

//wakeup

#ifdef IPC_JZ
//sleep
int32_t RealTimeSleepIpcam();
int32_t RealTimeGetElectricity(RT_BAT_Status *pEle);
int32_t RealTimeSetSleepStatu(int32_t enable);
int32_t RealTimeGetTimePIRWorkSheet(RT_WORKSHEET *pPirWorkSheet);
int32_t RealTimeSetTimePIRWorkSheet(RT_WORKSHEET PirWorkSheet);


#endif

#ifdef IPC_4G
int32_t RealTimeGet4GInfo(RT_4GInfo *p4GInfo);
int32_t RealTimeSet4GFlowLimit(RT_FlowLimit Limit);
int32_t RealTimeGet4GFlowLimit(RT_FlowLimit *Limit);
#endif


#ifdef NVR_TAOSHI
int32_t RealTimeSystemTimeSynchronization_Callback(int32_t timestamps);
int32_t RealTimeGetNVRChannelInfo_Callback(RT_NVRInfo *pInfo);

#endif

//#if defined(NVR_TAOSHI) || defined(IPC_TAOSHI)
int32_t RealTimeSystemTimeSynchronization_Callback(int32_t timestamps);
//#endif


int32_t RealTimeGetDeviceInfo(RT_DeviceInfo *pDevInfo);
int32_t RealTimeSetTimeZone_Callback(long TimeSec,int DaylightEnable);
int32_t RealTimeGetTimeZone_Callback(long *pTimeSec,int *pDaylightEnable);
int32_t RealTimeSetSDCardFormat_Callback();

int32_t RealTimeSetDevSleepSta(int32_t Enable,int32_t Val);
int32_t RealTimeConnectWifi_Callback(char* Ssid,char* Passwd);
int32_t RealTimeForceIFrame_Callback();
int32_t RealTimePwdAuth_Callback(char* username, char* pwd);
int32_t RealTimeResetUsrPsword_Callback(char* username, char* oldpwd, char* newpwd);
#ifdef HMSOTA
int32_t RealTimeGetSDTimeWorkSheet(RT_WORKSHEET *PirWorkSheet);
int32_t	RealTimeSetSDTimeWorkSheet(RT_WORKSHEET PirWorkSheet);
int32_t RealTimeGetHmsConfig(RT_CONFIGNETHMS *pHmsConfig);
int32_t	RealTimeSetHmsConfig(RT_CONFIGNETHMS HmsConfig);
int32_t RealTimeGetOTAConfig(RT_CONFIGNETOTA *pOtaConfig);
int32_t RealTimeSetOTAConfig(RT_CONFIGNETOTA OtaConfig);
int32_t RealTimeGetOTANewVersionInfo(RT_UPLOADNEWVERINFO *pNewVerInfo);
int32_t RealTimeOTAUpgradeNewVersion(int ignore,char Filename[128]);
int32_t RealTimeGetOTABuildTimeDate(char buildatetime[124]);
#endif


//0:success other:fail
int32_t RealTimeUploadAlarmInfo(RT_UpLoadAlarmInfo faceifo);
//push the end enent of alarm
int32_t RealTimePushEndEvent(RT_IdentifyType event,uint32_t endtime);


int DownFile(char *DownUrl,char *FilePath);
int32_t RealTimeSetMp3Url(char url[1024],int priority/*the biger ,the higher*/);
int32_t RealTimeMp3PlayCtrl(RT_Mp3PlayCtrl ctrl);
int32_t RealTimeGetMp3PlayStatus(char url[1024]);

//#ifdef DOORLOCK	
int32_t RealTimeUploadLockMsgInfo(RT_LockMsgInfo lockinfo);
int32_t RealTimeGetLockInfo_Callback(RT_DoorLockinfo *LockInfo);
int32_t RealTimeSetLockTempPsw_Callback(RT_DoorLockTempPasswd LockInfo);
int32_t RealTimeGetLockTime_Callback(RT_DateTime *TimeInfo);
int32_t RealTimeLockOperate_Callback(RT_DoorLockOperate LockInfo);
int32_t RealTimeGetLockVersion_Callback(RT_DoorLockVersion *VerInfo);
int32_t RealTimeSetExtLedSwitch(RT_ABLE val /*0:enable 1:unable*/);
/*0:enable 1:unable*/
RT_ABLE RealTimeGetExtLedSwitch();
//#endif


#ifdef IPC_AI
int32_t RealTimeNVRAddFaceInfo_Callback(RT_ModNewFaceInfo FaceInfo);
int32_t RealTimeNVRDelFaceInfo_Callback(RT_ModifyFaceInfo FaceInfo);
int32_t RealTimeNVRChangeFaceInfo_Callback(RT_ModifyFaceInfo ModifyFaceInfo,RT_ModNewFaceInfo ModNewFaceInfo);
int32_t RealTimeNVRQueryFaceInfo_Callback(RT_QueryFaceInfo QueryFaceInfo,RT_QueryRespFaceInfo *RespFaceInfo);

int32_t RealTimeSetAiUploadFreq(RT_UpLoadFaceFreqinfo FreqInfo);
int32_t RealTimeGetAiUploadFreq(RT_UpLoadFaceFreqinfo *FreqInfo);
#endif

int32_t RealTimeSetStreamPush(int32_t status);

int32_t RealTimeSetLedStatus(int val /*0:open 1:close*/);
int32_t RealTimeGetLedStatus();

int32_t RealTimeSetAudioOutVolume(int val/*0-100*/);	
int32_t RealTimeGetAudioOutVolume();

int32_t RealTimeResetAllConfigure();
int32_t RealTimeResetNetWork();		

int32_t RealTimeAKeyToShelter_Callback();
#ifdef  SUNING
//type:0  Cloud   1:alarm
int32_t  RealTimeSetTsUpLoadEnable_Callback(int type,int Enable);  
int32_t  RealTimeUpgradeFail_Callback();

#endif

int32_t  RealTimeGetDeviceCMEI_Callback(char cmei[RT_CMEI_LEN]);

#ifdef __cplusplus
}
#endif
#endif

