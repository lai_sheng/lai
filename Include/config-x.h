


#ifndef __config_x_h__
#define __config_x_h__

#include "Functions/FFFile.h"

#ifdef WIN32
	#define PRTSC_DIR "Win32/PrtSc"		//抓图文件目录
	#define CONFIG_DIR "Win32/Config"		//配置文件目录
	#define LOG_DIR "Win32/Log"			//日志文件目录
	#define PROFILE_DIR "Common/Lua"		//Lua文件目录
	#define FONT_BIN "Common/FontBin"	//解析字体的工具
	#define DATA_DIR "Win32/Pics"  //图片目录
	#define FONT_DIR "Win32/Log" //字体
	#define	FONT_DEFAULT_DIR "Win32/Strings" //字体
	#define LOGO_DIR "Win32/Logo"	//Logo目录
	#define WEB_DIR	"Win32/Web"		//Web目录
#elif defined(DVR_HI)
	#define PRTSC_DIR ""

	#define CONFIG_DIR "/mnt/mtd/Config"
	#define LOG_DIR "/mnt/mtd/Log"
	//#define PROFILE_DIR "/usr/bin/lua"
	#define PROFILE_DIR "/app/ipcam/lua"
	#define DATA_DIR "/usr/data/Data"
	#define FONT_BIN "/usr/data/Data"
	#define	FONT_DIR "/mnt/custom" //字体
	#define FONT_DEFAULT_DIR "/usr/data/Data"	//字体
	#define LOGO_DIR "/usr/data/Data/logo"	//Logo目录
    #ifndef FAC_CHECK
	#define WEB_DIR "/mnt/web"
    #else   
    #define WEB_DIR TARGET_DIR"/FAC_TOTAL_CHECK/web" 
    #endif
#endif

#endif //__config_x_h__

