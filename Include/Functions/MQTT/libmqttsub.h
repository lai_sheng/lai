/*
 * This file is part of libemqtt.
 *
 * libemqtt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemqtt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemqtt.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *
 * Created by Filipe Varela on 09/10/16.
 * Copyright 2009 Caixa Mágica Software. All rights reserved.
 *
 * Fork developed by Vicente Ruiz Rodríguez
 * Copyright 2012 Vicente Ruiz Rodríguez <vruiz2.0@gmail.com>. All rights reserved.
 *
 */

#ifndef __LIBMQTTSUB_H__
#define __LIBMQTTSUB_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "libemqtt.h"

typedef struct _socket_info
{
	int socketfd;
	uint8_t *buffer;
	uint32_t bufsize;
}CSocketInfo;

int mqtt_init_socket(mqtt_broker_handle_t* broker, const char* hostname, short port);
int mqtt_send_packet(void* socket_info, const void* buf, unsigned int count);
int mqtt_read_packet(mqtt_broker_handle_t* broker,int timeout);
int mqtt_close_socket(mqtt_broker_handle_t* broker);
#ifdef __cplusplus
}
#endif

#endif // __LIBEMQTT_H__
