#ifndef _MQTTMANAGER_H
#define _MQTTMANAGER_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"
#include "MultiTask/Guard.h"
#include "Configs/ConfigCamera.h"

#include "Functions/MQTT/libemqtt.h"
#include "Functions/MQTT/libmqttsub.h"
//#include "Functions/MQTT/cJSON.h"

using namespace std; 

#define HMSDATELEN 32
class MqttManager : public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(MqttManager);
		MqttManager();
		~MqttManager();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		int GetUUID();
		int GetMac();
		int GetDevTime(char date[HMSDATELEN]);
		long GetDevUptime();
		int GetSysBootTime();
		int GetIP(char date[HMSDATELEN]);		
		void ThreadProc();
		int ConnectMqttServer();
		void onTimer(uint arg);
		int PackData(string &packdata);
		int SendData(int     isping , const char* topic, const char* msg);
		int PackSendAlertData(int AlertCode,int chn,int Reason,int Raise);
		int PackSendHeartBeatData();
		int TestSendDataToHmsServer(char *HmsServerUrl,char *Pwd);
		int GetWlanIp(std::string & result);
		 int CheckSDStatus();
		 int GetRecordStartTime(char date[HMSDATELEN]);
	private:
		string m_cUUID;
		string m_cbootime;
		CTimer 		m_cTimer;
		char m_cMac[32];
		int  m_UUIDStatus;
		CMutex    m_mutex;
		mqtt_broker_handle_t m_broker;
		CSocketInfo *m_psock;
		int m_iMqttConStatus;  //MQTT连接状态
		int m_iMqttConFailCount; //MQTT连接拒绝次数
		int m_iRebootStatus;  //重启状态
		int m_iSdStatus;      //SD卡状态
		int m_iSDerroCount;   //出现次数
};

#define g_WjaMqtt (*MqttManager::instance())


#endif
