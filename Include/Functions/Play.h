

#ifndef __PLAY_H__
#define __PLAY_H__

#ifndef NEW_PLAY_GUI

#include "General.h"
#include "MultiTask/Semaphore.h"
#include "Configs/ConfigPlay.h"

class CDevCapture;

enum swich_direction
{
	SWITCH_LEFT,
	SWITCH_RIGHT,
};

#define BORDER_OUTPUT 2

#define MAX_LIST_FILES		128			// 列表中最多存放多少个文件


	#define OUTPUT_LOCAL_LEFT 8
	#define OUTPUT_LOCAL_TOP 22

	#define OUTPUT_LOCAL_RIGHT (598 - 0)
	#define OUTPUT_LOCAL_BOTTOM (488 - 0)

	#define OUTPUT_LOCAL_LENGTH (OUTPUT_LOCAL_RIGHT - OUTPUT_LOCAL_LEFT)
	#define OUTPUT_LOCAL_HEIGHT (OUTPUT_LOCAL_BOTTOM - OUTPUT_LOCAL_TOP)


/*-----------------------------------------------------------------------
	注意，下面两个宏的右值不能添加小括号，因为在进行计算的时候要先计算
	乘法再计算除法，否则先除如果结果小于1的话，结果会被整算成0，这样再
	计算乘法的话就肯定为0了；
-----------------------------------------------------------------------*/
#define PROPORTION_LOCAL_LENGTH OUTPUT_LOCAL_LENGTH / (m_iSourceWidth * 2)
#define PROPORTION_LOCAL_HEIGHT OUTPUT_LOCAL_HEIGHT / (m_iSourceHeight * 2)

#define PLAYER_ERROR -1
#define BORDER_WIDTH 10
#define MIN_WIDTH 100

// 下面这些尺寸是相对于回放页面的，而不是相对于全屏

	#define FRAME_TOP_HEIGHT  20					// 回放窗口顶部的高度
	#define FRAME_MIDDLE_HEIGHT  20					// 回放窗口中间条的高度
	#define FRAME_INTERVAL_HEIGHT 16				// 回放窗口间隔条（工具条之间的间隔条）的高度
	#define FRAME_TOOLBAR_HEIGHT 43					// 回放窗口上卡号查询等工具条的宽度
	#define FRAME_BOTTOM_HEIGHT  67					// 回放窗口底部控制条的高度
	#define FRAME_RIGHTREGION_WIDTH 200


#define ALL_PLAYERCHANNEL -1

enum dh_ply_t 
{
	PLAY_STOPPED = 0,
	PLAY_RESETED,
	PLAY_PAUSED,
	PLAY_PLAYING,
};

enum play_start_mode
{
	PLAY_START_QUICK = 0,
	PLAY_START_LIST,
};

enum ZOOM_TYPE
{					//源窗口	目标窗口
	STYLE_HIDE = 0,//const	0
	STYLE_MIN,		//const		one of 4 or 2 or 1窗口显示
	STYLE_MAX,		//const		画面左上角撑满
	STYLE_FUL,		//const		全屏
	STYLE_NUM,		//类型数量
	STYLE_CUS = STYLE_NUM//(STYLE_MAX+STYLE_CUS表示最大化的自定义缩放)
};

typedef struct
{
	ZOOM_TYPE curinx;
	int nRectID;	//在目标矩形框数组中的下标
}tagZOOM;

enum STYLESTEP
{
	SWITCH_PREV = -1,
	SWITCH_REFRESH = 0,
	SWITCH_NEXT = 1
};
#define _MAINTYPE_(x) ZOOM_TYPE((x) % STYLE_CUS)		//主类型,min max ful
#define _CUSTYPE_(x) ZOOM_TYPE((x) / STYLE_CUS * STYLE_CUS) //是否局部缩放

enum player_slider
{
	SLIDER_GETMARGIN = 0,
	SLIDER_MARGINCHANGED,
};

enum search_types
{
	VD_SEARCH_ALL = 0,
	SEARCH_ALARM,
	SEARCH_MOTION,
	SEARCH_AM,
	SEARCH_CARD,
	SEARCH_HAND,
	SEARCH_2SND,
	DHFILE_TYPES,
};

struct PLAY_INFO 
{
	int		chan;		/*! 回放通道:取值可以在1～N_PLY_CH之间 */
	int		state;		/*! 回放状态 */
	int		speed;		/*! 回放速度：可以是常量play_speed_t中的值 */
	int		alarm;		/*! 报警标志 */
	int		volume;		/*! 音量：取值范围0～50 */
	VD_BOOL	ahead;		/*! 向前播放 */
	uint	position;	/*! 文件位置 */
	uint	length;		/*! 文件长度 单位KB*/
	SYSTEM_TIME	time;	/*! 回放当前时间 */
	SYSTEM_TIME	stime;	/*! 录像文件的开始时间 */
	SYSTEM_TIME	etime;	/*! 录像文件的结束时间 */
};

struct PLAYER_CHANNEL_RESOURCE
{
	PLAYER_CHANNEL_RESOURCE();

	CDHFile		m_aDHFile;			// 每个回放通道的录像文件对象
	STM_INFO	m_sStmInfo;			// 每个回放通道的视频文件信息
	uint		m_dwPlayerState;	// 每个回放通道的状态
	int			m_iSpeed;			// 每个回放通道的回放速度
	VD_BOOL		m_bAhead;			// 每个回放通道的回放方向
	int			m_dwVolume;			// 每个回放通道的声音大小
	tagZOOM		m_zoom;				// 
	int			m_iRecChannel;		// 该变量用于记录本回放通道要回放第几通道的录像文件
	VD_BOOL		m_bIsEof;			// 该变量用来指示本回放线程中的录像文件是否已经放完
	VD_BOOL		m_bIsAbort;			// 该变量用来指示本回放是否异常中止
	VD_BOOL		m_bIsTrail;			// 该变量用来指示文件读到尾部
	SYSTEM_TIME m_tmCurTime;		// 当前解码的时间戳, 用来判断是否发生了数据错乱
	VD_BOOL		m_bCurTimeValid;	// 当前时间是否有效, 在文件重新定位后, 时间无效
	uint		m_dwCurMS;			// 取解码时间时的系统毫秒数
	CTimer		m_tmrJumpForward;	// 跳进定时器
	CTimer		m_tmrJumpBackward;	// 跳退定时器
	SYSTEM_TIME m_tmLastJump;		// 最后一次定位的时间
#ifdef FUNC_SYN4_PLAY
	int			m_lastFileTimeCount;	
#endif
};

enum player_count
{
	DELETE_PLAYERS = -1,				// 回放对象计数器减1
	GET_PLAYERS,						// 获得回放对象计数器的值
	ADD_PLAYERS,						// 回放对象计数器加1
	RESET_PLAYERS,						// 重置回放对象计数器
	ADD_PLAYERNO,						// 增加回放对象创建序号
	RESET_PLAYERNO,						// 重置回放对象创建序号
	GET_PLAYERNO,						// 获取回放对象创建序号
	SET_PLAYERNO,						// 指定要创建回放对象的序号
	RESET_REGION,						// 重置源缓冲区域和目标输出区域
};

#endif

#endif// __PLAY_H__
