#ifndef  __MKFS_VFAT_H__
#define  __MKFS_VFAT_H__

#ifdef __cplusplus
extern "C" {
#endif

//FAT32 格式化pdevname设备
int mkfs_vfat(char *pdevname);

//FAT32 格式化进度查询
int mkfs_vfat_percent(void);
#ifdef __cplusplus
}
#endif

#endif
