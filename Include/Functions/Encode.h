
#ifndef __ENCODE_H__
#define __ENCODE_H__

#include "APIs/Capture.h"
#include "System/File.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Timer.h"

#include "Configs/ConfigVideoColor.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigVideoWidget.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigChannelTitle.h"

//! OSD显示位置
enum OSD_LAYOUT
{
	OSD_BOTTOM_LEFT_CORNER = 1,			/*!< 左下角 */
	OSD_BOTTOM_CENTER,					/*!< 下面中间 */
	OSD_BOTTOM_RIGHT_CORNER,			/*!< 右下角 */
	OSD_LEFT_CENTER,					/*!< 左中 */
	OSD_CENTER,							/*!< 中间 */
	OSD_RIGHT_CENTER,					/*!< 右中 */
	OSD_TOP_LEFT_CORNER,				/*!< 左上角 */
	OSD_TOP_CENTER,						/*!< 上面中间 */
	OSD_TOP_RIGHT_CORNER				/*!< 右上角 */
};

typedef struct OSD_TITLE
{
	int enable;
	int layout;
	char content[32];
}OSDTITLE;

#define N_OSD_TITLE 8

class CEncode : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CEncode);

	CEncode();
	virtual ~CEncode();
	
	//! 加载选项
	VD_BOOL Start();

private:
	//! 设置叠加标题
	void SetInfoTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect, VD_PCSTR str, int iChannel);
public:
	//! 设置叠加标题，指定位置
	void setOSDTitle(int index, int layout, VD_BOOL enable, VD_PCSTR str, int iChannel);

	//! 清除叠加标题
	void ClearInfoTitle(int iChannel);

	//! DSP是否正常运行，主要是根据码流来判断
	VD_BOOL IsAlive();
	
	//! 不同的录像类型重新设置编码参数
	void setEncodeType(int irecType, int iChannel);

	//! 不同的抓图类型设置编码参数

	//! 获得编码类型
	int getEncodeType(int iChannel);

	void getCapFormat(CAPTURE_FORMAT format[]);
		//! 设置时间标题和通道标题
	void SetTimeChannelTitle(int index, int iChannel);

private:
	//! 图像设置改变
	void onConfigEncode(CConfigEncode& cConfigEncode, int& iRet);

	//! 图像叠加改变
	void onConfigVideoWidget(CConfigVideoWidget& cConfigVideoWidget, int& iRet);

	//! 应用图像颜色
	void onConfigVideoColor(CConfigVideoColor& cConfigVideoColor, int& iRet);

	/// 应用图像控制
	void onConfigVideoControl(CConfigVideoControl& cConfigVCtrl, int& iRet);

	//! 修改格式时更新时间
	void onConfigLocation(CConfigLocation& cConfigLocation, int &iRet);

	//! 修改通道名称
	void onConfigChannelTitle(CConfigChannelTitle& config, int& ret);

	//! m_EncodeTimer的回调函数，完成同步时间等功能
	void Monitor(uint arg);

	//! 应用编码选项的值
	void AppEncodeConfig(CONFIG_ENCODE& cfgNew, CONFIG_ENCODE& cfgOld, int iChannel, uint dwStreamRestart = 0xffffffff);

	void CompareEncode(CONFIG_ENCODE& cfgNew, CONFIG_ENCODE& cfgOld, 
		uint* dwStreamChanged, int iChannel);

	uint costPower(int iChannel);
	
	void autoAdjustFrame(CONFIG_ENCODE& cfgEncode, int iChannel);

private:
	CConfigEncode		m_cCfgEncode;					/*!< 图像编码配置类 */
	CConfigVideoWidget	m_cCfgVideoWidget;				/*!< 图像视频叠加配置类 */
	CConfigVideoColor	m_cCfgColor;					/*1< 图像颜色配置类 */
	CConfigVideoControl m_cCfgControl;					/*!< 图像控制配置类 */

	CMutex				m_ResetCapMutex;				/*!< 获得DSP是是否有码流的锁 */
	VD_BOOL				m_ResetCapTimes[N_SYS_CH];		/*!< 无码流的次数，1秒1次 */
	
	CTimer				m_EncodeTimer;					/*!< 编码的定时器，完成同步，设时间，更改颜色等功能 */

	int					m_CurColorIndex[N_SYS_CH];		/*!< 当前选中的颜色时间段索引 */	

	uint				m_dwStreamMask;

	int					m_iEncodeType[N_SYS_CH];

	int 				m_iSnapType[N_SYS_CH];

	OSDTITLE			m_OSDTitle[N_SYS_CH][N_OSD_TITLE];

	int					m_CoverNum;

};
void convertSize(VD_RECT& desRect, VD_RECT& srcRect, VD_SIZE& size);

#define g_Encode (*CEncode::instance())

#endif
