#ifndef __CAMERA_H_
#define __CAMERA_H_

#include "Configs/ConfigCamera.h"

class CCamera : CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CCamera);
	CCamera();
	~CCamera();
	void Start();

private:
	void onConfigCamera(CConfigCamera* pConfig, int& ret);
	void onConfigCameraExt(CConfigCameraExt* pConfig, int& ret);	
	
	int OnConsole(int argc, char **argv);
	void OnHelp( char* opr, char* value1, char* value2);
	
protected:
private:
	CConfigCamera	m_CcfgCamera;
	CConfigCameraExt   m_ConfigCameraExt;
};

#define g_Camera (*CCamera::instance())

#endif
