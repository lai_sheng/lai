#ifndef _WIFILINK_H_
#define _WIFILINK_H_

#include "System/Object.h"
#include "APIs/Types.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Timer.h"
#include "APIs/Thread.h"
#include "APIs/DVRDEF.H"
#include "APIs/Audio.h"

#ifdef IPC_JZ

#define AUDIO_LANG_DEFAULT_CHINA

#ifdef AUDIO_LANG_DEFAULT_CHINA
#define AUDIO_WELCOME           "/system/audio/cn/welcome.mp3"
#define AUDIO_LINK_SUCCESS      "/system/audio/cn/linksuccess.mp3"
#define AUDIO_LINK_FAIL         "/system/audio/cn/linkfail.mp3"
#define AUDIO_START_CON         "/system/audio/cn/startconfig.mp3"
#define AUDIO_QUIT_CON          "/system/audio/cn/quitconfig.mp3"
#define AUDIO_RCVWAVE_SUCCESS   "/system/audio/cn/rcvsuccess.mp3"
#define AUDIO_RESET_SUCCESS     "/system/audio/cn/resetsuccess.mp3"
#else
#define AUDIO_WELCOME           "/system/audio/en/welcome.mp3"
#define AUDIO_LINK_SUCCESS      "/system/audio/en/linksuccess.mp3"
#define AUDIO_LINK_FAIL         "/system/audio/en/linkfail.mp3"
#define AUDIO_START_CON         "/system/audio/en/startconfig.mp3"
#define AUDIO_QUIT_CON          "/system/audio/en/quitconfig.mp3"
#define AUDIO_RCVWAVE_SUCCESS   "/system/audio/en/rcvsuccess.mp3"
#define AUDIO_RESET_SUCCESS     "/system/audio/en/resetsuccess.mp3"
#endif

#else
#define AUDIO_LINK_SUCCESS		"/app/audio/linksuccess.mp3"
#define AUDIO_LINK_FAIL			"/app/audio/linkfail.mp3"
#define AUDIO_START_CON			"/app/audio/startconfig.mp3"
#define AUDIO_QUIT_CON			"/app/audio/quitconfig.mp3"
#define AUDIO_RCVWAVE_SUCCESS	"/app/audio/rcvsuccess.mp3"
#define AUDIO_RESET_SUCCESS           "/app/audio/resetsuccess.mp3"
#define AUDIO_WELCOME                    "/app/audio/welcome.mp3"
#define AUDIO_LINK_PAAS                     "/app/audio/linkpaas.mp3"
#define AUDIO_WIFI_PSK_ERR			"/app/audio/pskerr.mp3"
#define AUDIO_START_COMPLETE			"/app/audio/startcomplete.mp3" //suning

#define AUDIO_START_TALK			"/app/audio/starttalk.mp3"
#endif

#define DEFAULTE_RESOLVE_CONFIG "/mnt/mtd/Config/resolv.conf"
#define STORE_WIFI_CONFIG		"/mnt/mtd/Config/wpa_supplicant.conf"
#define STORE_WIFI_CONFIG_BAK	"/mnt/mtd/Config/wpa_supplicant.conf_bak"

bool CheckSsid(char* str,int len); 
int Store_Cfgfile(const char *filepath, char *ssid,char *psw);

#define USERIDLEN	(17)
enum QRCORE_STATUS
{
	QRCORE_INIT = 0,
	QRCORE_START,
	QRCORE_SUCCESS,
	QRCORE_FAIL,
};

enum WIFI_CONECT_STATUS
{
	WIFI_CONECT_INIT = 0,
	WIFI_CONECT_SUCCESS = 1,
	WIFI_CONECT_FAIL =	2,
};
class WifiLink : public CThread
{
public:

	PATTERN_SINGLETON_DECLARE(WifiLink);
	
	WifiLink();
	~WifiLink();

	void Start();
	void GetUserId(char* DevId,int Len);
	void SetUserId(char* id);
	void GetWifiLinkStat(int *pLinkStat);
	void ResetAllCfg();
	int  ResetNetwork();	
	int  SetWifiInfo(struct SSIDWiFiInfo *wifi);
	int  GetQRCodeStatus(){ return m_nQcoreStatus; }
	int  GetWifiConectStatus(){ return m_nWifiConectStatus; }
	/*等待播放退出*/
	void WaitPlayAudioExit();
	/*二维码识别onPacket*/
	void OnPacket(void *pData, int nDataLen);
	/*弹出BUF 长度*/
	void *PopPacket(int *pLenght);
	/*清识别数据*/
	void ClearQrcodePacket();
protected:
	volatile VD_BOOL m_bLoop;
	
private:	
	void ThreadProc();
private:
	
	char Id[USERIDLEN];
	int  m_iPlayStatus;
	int  m_wifilinkstat;
	int  m_config_over;
	struct SSIDWiFiInfo m_oWifiInfo;
	int  m_nQcoreStatus;
	int  m_nWifiConectStatus;
	//二维码识别队列
	typedef struct PACKET_QRCORE_DATA {
		void* pQrodeData;
		int	  nLenght;
	}PACKET_QRCORE_DATA;
	typedef std::list<PACKET_QRCORE_DATA> PACKET_LIST;
	uint m_packet_count;
	PACKET_LIST m_packet_list; //队列
	CMutex m_mutex_list;
};

#define g_WifiLink (*WifiLink::instance())
#endif
