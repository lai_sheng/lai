

#ifndef __PTZ_H__
#define __PTZ_H__

#include "System/File.h"
#include "Devices/DevPtz.h"
#include "MultiTask/Timer.h"
#include "System/Log.h"
//#include "AuxLibs/Lua/ScriptEngine.h"
#include "Configs/ConfigPTZ.h"
#include "Configs/ConfigEvents.h"
#include "System/AppEvent.h"
#include "APIs/PtzCmd.h"

struct PTZ_HDR {			//云台控制头
	ushort cmd_len  : 5;		//字节单位.字节数==实际字节数+1。
	ushort bit_rate : 3;		//1200,2400,4800,9600,19200,38400,...
	ushort send_num : 3;		//发送次数
	ushort parity   : 2;		//奇偶校验位 (0:无校验  1:奇校验  2:偶检验)
	//	WORD ab_spec  : 1;		
	ushort reverse  : 3;		//保留//AB 解码器特殊位,parity 位必须为 0 000B:无特殊 
	//001B:A/D选择位数据为1 010B:A/D位选择地址为1
}
#ifdef LINUX
__attribute__ ((packed)) 
#endif
; 

enum MJ_Ptz_Param
{
	AUTO_REVERSE =1, //自动翻转
	TELEPHOOT_SPEED,	//长焦限速
	PRESCAN_FREEZE,//预置位画面冻结
	NORTH_SET,//北位设置
	FAN_START_TEMPERATURE,//风扇启动温度
	LOW_POWER,//低功耗
	RESET_CTRL_PANEL,//控制板复位
	
	DIGITAL_ZOOM = 21,//数字变倍
	CARMERA_OSD,         //摄像机OSD
	CARMERA_MENU,        //摄像机菜单
	ILLUMINATION, // 低照度
	BACKLIGHT , //背光补偿
	AUTO_SHUTTER,//自动慢快门
	
	SHUTTER_ADJUST = 29,//快门调节
	WHITE_BAANCE, // 白平衡
	WHITE_BAANCE_R,//
	WHITE_BAANCE_B,
	EXPOSURE_COMP, // 曝光补偿
	EXPOSURE_COMP_VALUE,
	
	AUTO_FOCUS = 36,
	EXPOSURE, //曝光模式 
	APERTURE_ADJUST, // 光圈调节
	SHARP_ADJUST, //锐度调节
	WIDE_DYNAMIC, //宽动态
	DIGIT_NOISE_REDUCTION, // 数字降噪
	
	WIPER_CTRL=61 , // 雨刷控制
	INFRARED_CTRL  //红外灯控制
};	

enum MJ_WHITE_BALANCE
{
	 MJ_WHITE_BALANCE_MANUA, //手动white balance
	 MJ_WHITE_BALANCE_AUTO,  //自动white balance
	 MJ_WHITE_BALANCE_INDOOR, //室外
	 MJ_WHITE_BALANCE_OUTDOOR, //室内
	 MJ_WHITE_BALANCE_ATW,//
	 MJ_WHITE_BALANCE_ONEPUSH,//
	 MJ_WHITE_BALANCE_NR,
};
enum MJ_EXPOSURE_MODE
{
	MJ_EXPOSURE_AUTO = 0,  //自动曝光
	MJ_EXPOSURE_MANUAL = 1, //手动曝光
	MJ_EXPOSURE_SHUTTER = 2, //快门优先
	MJ_EXPOSURE_APERTURE,   //光圈优先
};


typedef struct {
	uint HighMask;					/*!< 操作的掩码高位 */
	uint LowMask;					/*!< 操作的掩码低位 */
	char Name[NAME_MAX_LEN];		/*!< 操作的协议名 */
	ushort CamAddrMin;				/*!< 通道地址的最小值 */
	ushort CamAddrMax;				/*!< 通道地址的最大值 */
	ushort MonAddrMin;				/*!< 监视地址的最小值 */
	ushort MonAddrMax;				/*!< 监视地址的最大值 */
	uchar PresetMin;					/*!< 预置点的最小值 */
	uchar PresetMax;					/*!< 预置点的最大值 */
	uchar TourMin;					/*!< 自动巡航线路的最小值 */
	uchar TourMax;					/*!< 自动巡航线路的最大值 */
	uchar PatternMin;				/*!< 轨迹线路的最小值 */
	uchar PatternMax;				/*!< 轨迹线路的最大值 */
	uchar TileSpeedMin;				/*!< 垂直速度的最小值 */
	uchar TileSpeedMax;				/*!< 垂直速度的最大值 */
	uchar PanSpeedMin;				/*!< 水平速度的最小值 */
	uchar PanSpeedMax;				/*!< 水平速度的最大值 */
	uchar AuxMin;					/*!< 辅助功能的最小值 */
	uchar AuxMax;					/*!< 辅助功能的最大值 */
	int	Internal;					/*!< 发送命令的时间间隔 */
	char Type;						/*!< 协议的类型 */
	char AlarmLen;					/*!< 协议的报警长度 */ 
	char Reserved[2];
		/*!<
	云台辅助功能扩展需要，将操作掩码进行扩LowAuxMask为云台支持的低32个辅助操作掩码
	*/
	uint LowAuxMask;			
}PTZ_OPT_ATTR;

typedef struct 
{
	int cmd;						/*!< 联动项 */
	int value;						/*!< 联动值 */
	appEventCode code;				/*!< 触发源 */
	int index;						/*!< 触发通道 */
	int leftTime;						/*!动作剩余执行时间单位s */
}PTZ_LINK_STRUCT;

//!预置点属性结构
typedef struct tagCONFIG_PRESET_INFOR
{
	unsigned int uiPresetId;
	char szPresetName[NAME_LEN];
    int iSpeed;
    int iDWellTime;
}CONFIG_PRESET_INFOR;

typedef struct tagSCONFIG_PRESET_INFOR
{
	CONFIG_PRESET_INFOR	strPresetInfor[PTZ_PRESETNUM]; /* 预置点信息 */
}SCONFIG_PRESET_INFOR;

//!预置点属性结构|使用TLV之后
typedef struct tagNET_PRESET_INFOR
{
	unsigned char ucChannel;//通道
    unsigned char ucPresetId; //预置点号
    unsigned char ucSpeed;   //预置点 速度:1~15等级
	unsigned char ucDWellTime; //预置点 停留时间: 1~255s
	char cPresetName[NAME_LEN]; //预置点名称
}NET_PRESET_INFOR; 

typedef struct tagSNET_PRESET_INFOR
{
	NET_PRESET_INFOR	strPresetInfor[PTZ_PRESETNUM]; /* 预置点信息 */
}SNET_PRESET_INFOR;

//巡航线路属性结构
typedef struct tagCONFIG_TOUR_INFOR
{
    int iTourIndex;
	unsigned int uiDwellTime;//巡航停留时间
	unsigned int uiPresetCnt;//一个巡航组中的预置点数
	unsigned int uiPresetNum[PTZ_PRESETNUM];//此数组保存在一个巡航组中的预置点号码
}CONFIG_TOUR_INFOR;

typedef struct tagSCONFIG_TOUR_INFOR
{
	CONFIG_TOUR_INFOR	strTourInfor[PTZ_CHANNELS]; /* 巡航信息 */
}SCONFIG_TOUR_INFOR;

//巡航线路属性结构|使用TLV之后
typedef struct tagNET_TOUR_INFOR
{
	unsigned char ucTourIndex;//巡航线路
	unsigned char ucDwellTime;//停留时间
	unsigned char ucPresetCnt;//一个巡航组中的预置点数 0~128
	unsigned char ucRes2;//保留
	unsigned char ucPresetNum[PTZ_PRESET_IN_TOUR_NUM];//此数组保存在一个巡航组中的预置点号码
}NET_TOUR_INFOR;

typedef struct tagSNET_TOUR_INFOR
{
	NET_TOUR_INFOR	strTourInfor[PTZ_CHANNELS]; /* 巡航信息 */
}SNET_TOUR_INFOR;

typedef struct tagCONFIG_PTZREGRESS
{
	unsigned short usEnableRegress;
	unsigned short usRegressTime;
	unsigned short usPresetIndex;
	char szPresetName[NAME_LEN];
}CONFIG_PTZREGRESS;
typedef struct tagDefaultPreset
{
    char ResetTime[20];
	int PresetId;
}DefaultPreset;

class CPtz : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CPtz);
	CPtz();
	virtual ~CPtz();

	//! 读取配置参数
	void ReadConfig();

	//! 加载脚本
	VD_BOOL LoadLuaScript();

	//! 初始化
	void Init();

	//! 开始半双工通讯
	void StartSemiduplex();

	//! 根据名字获得下标
	int GetIndex(const char * name);

	//! 获得协议数量
	int GetProtocolCount();

	//! 获得指定协议的属性
	void GetProtocolAttr(PTZ_OPT_ATTR *pAttr, int index);	

	//! 开始云台控制
	VD_BOOL Start(int iChannel, PTZ_OPT_STRUCT *pPTZStruct);

	//! 停止云台控制
	VD_BOOL Stop(int iChannel, PTZ_OPT_STRUCT *pPTZStruct);

	//！直接向485写数据，用于某些特殊的云台控制命令,需要先对485属性进行设置
	void WritePtz(uchar * pData, uint  length,int iChannel);

    //透明通道，先设置485属性，然后发送透明数据
    void WritePtzRawData(uchar * pData, uint length,int iChannel);

	//!发送数据
	void Send(int iChannel, int len, int Group ,uchar *buffer, uchar *lenBuffer);

#ifdef FUNTION_PRESET_TITLE
	bool PTZ_SetPresetName(int iChannel,char *pPresetName,int enable,int x,int y);
#endif
	//!添加某通道的预置点配置信息;
	bool PTZ_AddPreset(int iChannel,NET_PRESET_INFOR *pszPresetInfo);

	//!获取某通道的所有预制点信息，预制点ID为0表示该预制点无效,根据获取得结果进行判断
	NET_PRESET_INFOR* PTZ_GetPreset(int iChannel,int iPresetIndex = -1);
    
	//!删除某通道的预置点配置信息，如果iPresetId为默认值则删除全部
	bool PTZ_DeletePreset(int iChannel,int iPresetId = -1);

	//!修改某通道的预置点配置信息
	bool PTZ_ModifyPreset(int iChannel,NET_PRESET_INFOR *pszPresetInfo,bool bSetPreset=false);

	//!查询某通道的某个预制点信息
	NET_PRESET_INFOR &PTZ_QueryPreset(int iChannel,int iPresetId);

	//巡航操作接口,操作成功返回０，失败返回－１
	int PTZ_OnPtzTour(PTZ_OPT_STRUCT szPtzOpt,int iChannel);
	
	//获取某通道巡航组数目
	int PTZ_GetTourNumber(int iChannel);

	//获取某通道某巡航组预置点数目
	int PTZ_GetTourPresetNumbers(int iChannel,int iTourNumber);

	//根据通道信息和巡航组的序号获取该巡航组中的预置点序列
	NET_TOUR_INFOR *PTZ_GetTourInfo(int iChannel, int iTourNumber = -1); 
	
	//根据通道信息和巡航组的序号设置巡航点之间的停留时间，操作成功返回0，操作失败返回-1,单位为秒
	int PTZ_SetTourDwellTime(int iChannel, int iTourNumber, int iDwellTime);

	//根据通道信息和巡航组的序号获取巡航组的停留时间
	int PTZ_GetTourDwellTime(int iChannel, int iTourNumber);

	//巡航操作计时器回调函数
	void PTZ_TourExecute(uint uiArg);

    //!保存修改后的预置点配置信息
    bool PTZ_SavePresetInfoFile(int iChannel = -1);

    //!保存修改后的巡航组配置信息
    bool PTZ_SaveTourInfoFile(int iChannel = -1);

    //!获取预置点配置信息
    bool PTZ_LoadPresetInfoFile(void);

    //!获取巡航组配置信息
    bool PTZ_LoadTourInfoFile(void);
	bool PTZ_DefaultPreset(DefaultPreset *prest,int iChn,int flag);

private:
	//! 验证配置参数
	VD_BOOL PtzValid(CONFIG_PTZ *pConfig);

	//! 应用云台的配置结构
	void AppPtzConfig(CONFIG_PTZ *pConfig, int iChannel);

	void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data);

	//! 从LUA中加载协议属性
	void GetProtocolAttr();

	//! 检测协议名，可能更新文件
	//void CheckName();
	
	//! 设置选中的协议
	VD_BOOL SetProtocol(int iChannel, uchar type = PTZ_WITH_VIDEO);

	//! 从LUA中取得数据写入云台设备
	int Operate(uchar opttype, PTZ_OPT_STRUCT *pPTZStruct, uchar *buffer,uchar *lenBuffer);
	
	//! 发送半双工的命令，主要是查询命令或查找报警命令
	void SendSemiduplexCMD(void *pData, int length);

	//! 清除叠加在屏幕上的信息
	void OnClearTitle(uint arg);

	//! 从pData中找到报警解码器数据
	int ParseDHCC440Data(uchar *pData, int length, int iChannel);

	//! 解析报警解码器数据
	void DHCC440Proc(uchar *pData, int length, int iChannel);

	//! 从pData中找到LTM8230C数据
	int ParseLTM8230CData(uchar *pData, int length, int iChannel);
	
	//! 解析LTM8230C数据
	void LTM8230CProc(uchar *pData, int length, int iChannel);

	//! 从pData中找到点钞机数据
	int ParseBanknoteData(uchar *pData, int length, int iChannel);

	//! 解析点钞机数据
	void BanknoteProc(uchar *pData, int length, int iChannel);

	//! 报警解码器输出，由定时器控制
	void SendAlarmOut(uint arg);

#ifdef FUNTION_PRESET_TITLE
	void onConfigPresetSet(CConfigPreset* pConfig, int & ret);
#endif
	//!云台配置变化回调函数
	void onConfigPTZSet(CConfigPTZ	*pConfig, int &ret);

	//!云台报警配置变化回调函数
	void onConfigPTZAlarmSet(CConfigPTZAlarm *pConfig, int &ret);

	void stopLinkPtz(int iChannel);

	void ptzLinkOperate(uint arg);
	
	void Write(uchar *buffer, int length, int iChannel);


#ifdef VN_IPC
	void SetDomeAddr(char Addr);
#endif
	
private:
	CFile			  m_FilePtz;				/*!< 配置文件类 */
	CFile			  m_FilePtzAlarm;

#ifdef FUNTION_PRESET_TITLE
	CConfigPreset       m_CConfigPreset;
#endif
	CConfigPTZ			m_CConfigPtz;			/*!<云台配置类实例化*/
	CConfigPTZAlarm		 m_CConfigPtzAlarm;		/*!<云台报警配置类实例化*/
	
	CONFIG_PTZ			m_ConfigPtz[N_SYS_CH];		/*!<云台配置结构*/

	CONFIG_PTZ		m_ConfigPtzAlarm[N_SYS_CH];	/*!<云台报警配置结构*/


	CMutex			m_Mutex;				/*!< 操作命令的锁 */

	CDevPtz			*m_pDevPtz;				/*!< 备份设备 */

//	LuaWrap         *m_luaEngine;			/*!< Lua Engine */
	int				m_ptzCtrlRef;			/* 对应与LUA中Global.PtzCtrl的引用指针 */

	PTZ_HDR			m_HDR;					/*!< 云台协议的头，主要给视新视豪用 */

	PTZ_OPT_ATTR	*m_pPtz_Opt_Attr;		/*!< 协议属性 */
	int				m_ProtocolCount;		/*!< 协议数量 */

	CTimer			MJPtzTimer;                     /*名景定时器用于定时查询坐标温度等信息*/

	CTimer			m_Timers;				/*!< 清除编码字幕的定时器 */

	CTimer			m_PtzLinkTimer;			/*!<云台联动使用的定时器控制,避免两个连续的较短的时间间隔内
												发送云台控制命令,防止无效操作,但是存在丢失部分联动的可能,
												连续两百毫秒对同一通道的联动将要丢失*/
	CMutex			m_MutexLink;			/*!< 联动数据保护锁 */	

	PTZ_LINK_STRUCT	m_ptzLinkCmd[ALARM_PTZLINK_NUMS][N_SYS_CH];//added by wyf on 20100919

	/*预置点相关成员变量*/
	NET_PRESET_INFOR m_szPresetInfor[N_SYS_CH][PTZ_PRESETNUM]; //各个通道的点间预制点信息
	int m_iPresetNum[N_SYS_CH]; //用于记录当前通道的预置点的数目
#ifdef FUN_ALARMIN_LINKER_PTZ
	int     m_iTourPause[N_SYS_CH];  //
	int     m_iPrePreset[N_SYS_CH];
#endif	
	/*巡航组相关成员变量*/
	int     m_iTourGroup; //记录当前的巡航组
	int 	m_iTourGroupArray[N_SYS_CH];//记录每个通道当前的巡航组//added by wyf on 20100511
	int     m_iTourPosition[N_SYS_CH]; //记录当前巡航组中的当前巡航位置
	int     m_iTourNum[N_SYS_CH]; //记录每个通道有多少条巡航线路
#ifdef FUNTION_PRESET_TITLE
	int    m_iExit[N_SYS_CH];
	CONFIG_PRESET m_ConfigPreset[N_SYS_CH];
#endif
	CTimer  *m_pTimerTour[N_SYS_CH]; //用来点间巡航执行时记时
	NET_TOUR_INFOR m_szTourInfor[N_SYS_CH][PTZ_CHANNELS]; //各个通道的各条线路的巡航组中的预置点信息
	DefaultPreset m_szDefaultPreset[N_SYS_CH];//各个通道的自动归位点
};
int getStandardType(int hor = PTZ_OPT_NONE, int ver = PTZ_OPT_NONE, int zoom = PTZ_OPT_NONE, int focus = PTZ_OPT_NONE, int iris = PTZ_OPT_NONE);
#define g_Ptz (*CPtz::instance())

#endif// __PTZ_H__
