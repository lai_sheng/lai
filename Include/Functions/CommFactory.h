
#ifndef __COMM_FACTORY_H__
#define __COMM_FACTORY_H__

#include "Comm_Base.h"

class CCommFactory : public CObject
{
public:
	CCommFactory();
	~CCommFactory();	

	CCommBase * Create(int iProductID);
};

#endif

