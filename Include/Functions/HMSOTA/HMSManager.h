#ifndef _HMSMANAGER_H
#define _HMSMANAGER_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>
#include <errno.h>


#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"

#include "Functions/HMSOTA/MQTTClient.h"
#include "Functions/HMSOTA/cJSON.h"

using namespace std; 
#define  HMSDATELEN   20


class HMSManager : public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(HMSManager);
		HMSManager();
		~HMSManager();
	public:
		VD_INT32 Start();
		VD_INT32 Stop();
		int GetUUID();
		int GetMac();
		int GetDevTime(char date[HMSDATELEN]);
		long GetDevUptime();
		int GetSysBootTime(char date[HMSDATELEN]);
		int GetIP(char date[HMSDATELEN]);		
		void onConfigHms(CConfigNetHms& cCfgHms, int& ret);
		void ThreadProc();
		int SendHmsData(int datetype,uchar* data,VD_UINT32 len);
		int PackSendHealthData();
		int PackSendAlertData(int AlertCode,int chn,int Reason,int Raise);
		int PackSendHeartBeatData();
		int TestSendDataToHmsServer(char *HmsServerUrl,char *Pwd);
		int GetWlanIp(std::string & result);
		 int CheckSDStatus();
		 int GetRecordStartTime(char date[HMSDATELEN]);
	private:
		char m_UUID[32];
		char m_cMac[32];
		int  m_UUIDStatus;
		CMutex    m_mutex;
		CConfigNetHms             m_configHmsServer;
		MQTTClient m_mqttClient;
	    MQTTClient_deliveryToken m_token;
		int m_iMqttConStatus;  //MQTT连接状态
		int m_iMqttConFailCount; //MQTT连接拒绝次数
		int m_iRebootStatus;  //重启状态
		int m_iSdStatus;      //SD卡状态
		int m_iSDerroCount;   //出现次数
};

#define g_HMS_Indian (*HMSManager::instance())


#endif
