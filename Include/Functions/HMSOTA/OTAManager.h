#ifndef _OTAMANAGER_H
#define _OTAMANAGER_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/RealTime_apiv1.h"

size_t write_memory(void *contents, size_t size, size_t nmemb, void *userp);


enum {
    OPC_OK = 0,
    OPC_WRONGPARAM = -1, //Wrong parameter(s)
    OPC_SIGNFAILED = -2, //Failed to sign request
    OPC_SENDFAILED = -3, //Failed to send request
    OPC_REQFAILED = -4, //Request failed
    OPC_PARSEFAILED = -5, //Failed to parse response data
    OPC_VERIFYFAILED = -6, //Failed to verify response signature
    OPC_OTHERS = -10
};

enum {
    REQ_OK = 0,
    REQ_DOWNLOAD = 1, //Wrong parameter(s)
    REQ_MESSAGE = 2, //Failed to sign request
};



typedef struct {
  char *memory;
  size_t size;
} MemChunk;


class OTAManager : public CThread
{
	public:
		PATTERN_SINGLETON_DECLARE(OTAManager);
		OTAManager();
		~OTAManager();
	public:
		VD_INT32 Init();
		VD_INT32 Start();
		VD_INT32 Stop();
		void ThreadProc();
		int GetUUID();
		int GetMac();
		int  perform(std::string & result, long *statusCode);
		int sendRequest(int reqtype,char* url,char* data,
			std::string & result, long *statusCode,std::string  filename);
		int PackSendOTAMsgData();
		int HandleResponse(std::string & result);
		int CheckUpGrade(std::string &filename,unsigned int filesize);
		void onConfigOta(CConfigNetOta& cCfgOta, int& ret);
		void StartUpGrade(){m_iUpDateStatus = 1;}
		int CheckUpGradeCondition();
		int UpGradeVersion();
		int QueryVersionInfo(RT_UPLOADNEWVERINFO* puploadInfo);
		int CallBackUpgrade(char FileName[128]);
private:
		char m_UUID[32];
		char m_cMac[32];
		int  m_iUpDateStatus;
		int  m_iUpGradeMode ; //�ֶ�����
		int  m_iUPLoadStatus;//??????????��?????????
		CConfigNetOta m_configOtaMode;
		RT_UPLOADNEWVERINFO m_uploadInfo;
};

#define g_OTA_Indian (*OTAManager::instance())


#endif
