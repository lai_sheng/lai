

#ifndef __DAEMON_MANAGER_H__
#define __DAEMON_MANAGER_H__

#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>    
#include <sys/stat.h> 
#include <sys/un.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include "APIs/DVRDEF.H"
#include "APIs/Ide.h"
#include "System/File.h"
#include "Functions/FFFile.h"
#include "System/Log.h"
#include "System/Signals.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "MultiTask/Timer.h"
#include "Devices/DeviceManager.h"
#include "Configs/ConfigEvents.h"

enum unix_domain_t {
	WIFI_STATUS_GET  = 0x0001,	
	WIFI_STATUS_POST = 0x0002,
	WIFI_CONNECT_POST = 0x0003,
	LED_STATUS_POST  =  0x0004,
	SYSTEM_CMD_ERROR_POST = 0x0005,
	WIFI_CONECTING_POST  = 0x0006,
	WIFI_KEY_PRESS_POST  = 0x0007,
	WIFI_RECONECT       = 0x0008,
	WIFI_MOD_SSID       = 0x0009,
	WIFI_DISCONECT      = 0x000a,
};
typedef struct ipcheader
{
	unsigned int	 ipc_cmd;		 /* command  命令 */
	unsigned int	 ipc_r0;		 /* 运行位置*/
}IPC_HEADER;			  //进程间通信字段

class CDaemonManager : public CThread
{
	
public:
	PATTERN_SINGLETON_DECLARE(CDaemonManager);
	CDaemonManager();
	virtual ~CDaemonManager();

	VD_BOOL Start();
	VD_BOOL Stop();	
	VD_BOOL ConnectToDaemon();
	VD_BOOL CmdProcess(IPC_HEADER cmd);	
	int     SendCmdToDaemon(void* buf,unsigned int len);
	void    OnPrintTimer(uint param);
private:	
	void ThreadProc();


public:
private:
	CMutex	m_Mutex;
	CTimer 	m_TimerPrint;
	int     m_iDaemonLossmes;
	int     m_iDaemonCmderror;
	int     m_iSock;
	int     m_iWifiStatus;
	int     m_iWifiConectStatus;
	int     m_iLedStatus;
	int     m_iConnectPostion;
};
#define g_DaemonManager (*CDaemonManager::instance())
#endif// __DRIVER_MANAGER_H__

