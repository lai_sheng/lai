#ifndef __LEDCTRL_H_
#define __LEDCTRL_H_

int InitLedTty(void);
int SetChnLedState(int no, int flag);
int SetNetLedState(int flag);
int SetHddLedState(int flag);

#endif
