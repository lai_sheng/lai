/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * WatchDogTime.h - _explain_
 *
 * Copyright (C) 2005 Technologies, All Rights Reserved.
 *
 * v 1.0.0.1 2006-02-16 14:13:32 wangk 
 *
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#ifndef __WATCH_GOG_TIME_
#define __WATCH_GOG_TIME_
#if defined(FUNC_WDT)

#include "MultiTask/Thread.h"
#include "System/File.h"

class WatchDogTime :public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(WatchDogTime);

	WatchDogTime();
	~WatchDogTime();
	void Start(void);
	void Stop();	
	void SetRebootFlag(int rebootflag);
	int m_irebootflag;	 
private:	
	
	void ThreadProc(void);
};

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#define g_WatchDogTime (*WatchDogTime::instance())
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#endif
#endif //__WATCH_GOG_TIME_
