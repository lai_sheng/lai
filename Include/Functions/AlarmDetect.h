#ifndef _ALARMDETECT_H_
#define _ALARMDETECT_H_


#include "APIs/Ptz.h"
#include "APIs/Types.h"
#include "APIs/Thread.h"
#include "APIs/DVRDEF.H"
#include "APIs/System.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"
#include "MultiTask/Thread.h"
#include "Functions/General.h"
#include "Configs/ConfigPTZ.h"
#include "APIs/MotionDetect.h"
#include "Functions/PtzTrace.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"

enum EVENT_DECECT_STATUS 
{
	EVENT_DECECT_FAIL = 0,	 //事件检测失败
	EVENT_DECECT_SUCCESS = 1,//事件检测成功
};

enum EVENT_STATUS
{	
	EVENT_START = 0, //事件开始
	EVENT_STOP  = 1, //事件结束
};
//声音报警
#define VoiceGapTime		(5*60)	//每次报警的最短间隔 s
#define VoiceAlarmStopTime	(20)	//判断声音侦测为结束的最短时长

//哭声报警
#define CryAlarmStopTime	(15)	//判断哭声侦测为结束的最短时长

//移动报警
#if defined(HMSOTA)
#define AlarmGapTime 	(60)
#define AlarmStopTime	(10)
#else
#define AlarmGapTime 	(10)  //每次报警的最短间隔 s
#define AlarmStopTime	(10)  //判断侦测为结束的最短时长
#endif
#define ForceStopTime	(5*60) //移动侦测开始后的强制结束时间


//移动追踪
class CMoveTracker : public CThread
{
public:
	CMoveTracker();
	~CMoveTracker();

	void SetTrackerSwitch(VD_BOOL val){bTraceEnable = val;}
	void SetTimePlan(VD_BOOL val){bTimePlanEnable = val;}
	private:
		VD_BOOL bTraceEnable;
		//时间使能时间段
		VD_BOOL bTimePlanEnable;

private:	
	VD_BOOL IsMatchCondition();
	void SwtichTracker(VD_BOOL val);
	void ThreadProc();

};
//声音(哭声)监测
class CVoiceDetecter : public CThread
{
public:
	CVoiceDetecter();
	~CVoiceDetecter();

	void SetVoiceAlarmSwitch(VD_BOOL val){bVoiceEnable = val;}
	void SetCryAlarmSwitch(VD_BOOL val){bCryEnable = val;}
//	void SetTimePlan(VD_BOOL val){bTimePlanEnable = val;}
private:
	VD_BOOL bCryEnable;
	VD_BOOL bVoiceEnable;
	//时间使能时间段
//	VD_BOOL bTimePlanEnable;
	uint alarmTime_cry;
	uint alarmTime_voice;
private:	
	int PushVoiceAlarm();
	int PushCryAlarm();
	void PushAlarmEvent();

	void ThreadProc();

};
//移动(人形)侦测
class CMotionDetecter : public CThread
{
public:
	CMotionDetecter();
	~CMotionDetecter();

	void SetMotionAlarmSwitch(VD_BOOL val){bMotionEnable = val;}	
	void SetHumanDetectSwitch(VD_BOOL val){bHumanEnable = val;}
	void SetTimePlan(VD_BOOL val){bTimePlanEnable = val;}
private:
	VD_BOOL bMotionEnable;
	VD_BOOL bHumanEnable;
	//时间段使能
	VD_BOOL bTimePlanEnable;
//移动侦测
	uint m_dwStopCount;
	uint m_dwStartCount;
	int  m_MotionDetectStatus;		
	#ifdef SUNING
	int  m_iMotionFrameTimerCount;
	#endif
private:	
	int PushMotionDetect(int status);	
	uint GetMotionDetectState();
	void onDetectTimer();	
	void ThreadProc();

};

class CAlarmDetecterManager : public CThread
{
public:
	CAlarmDetecterManager();
	~CAlarmDetecterManager();

	PATTERN_SINGLETON_DECLARE(CAlarmDetecterManager);	
	
	void Start();
	void Stop();
	
	/*关闭移动追踪功能*/
	void ForceCloseTracker();

private:
	CMoveTracker 	*m_pMoveTracker;
	CVoiceDetecter 	*m_pVoiceDetecter;
	CMotionDetecter *m_pMotionDetecter;
	
	void onConfigAlarmSwitch(CConfigAlarmSwitch& cCfg, int& ret);
	CConfigAlarmSwitch	m_AlarmSwitch;	
private:	
	void ThreadProc();

};
#define g_AlarmDetect (*CAlarmDetecterManager::instance())


#ifdef IPC_JZ
inline int RealTimeUploadAlarmInfo(RT_UpLoadAlarmInfo faceifo){ return 0;};
//push the end enent of alarm
inline int RealTimePushEndEvent(RT_IdentifyType event,uint32_t endtime){ return 0;};
#endif

#endif
