

#ifndef __LINKAGE_H__
#define __LINKAGE_H__

#include "Configs/ConfigEvents.h"

class Ilinkage
{
public:
	Ilinkage();
	virtual ~Ilinkage();
	
	virtual void setLinkItem(appEventCode code) = 0;

	virtual VD_BOOL start(appEventCode code, int index) = 0;

	virtual VD_BOOL stop(appEventCode code, int index) = 0;

	virtual VD_BOOL isEmpty() = 0;

	virtual uint getEventSource(appEventCode code) = 0;
};

class CLinkage: Ilinkage
{
public:
	CLinkage();
	~CLinkage();

	void setLinkItem(appEventCode code);

	VD_BOOL start(appEventCode code, int index);

	VD_BOOL stop(appEventCode code, int index);

	VD_BOOL isEmpty();

	uint getEventSource(appEventCode code);
protected:
	uint64	m_dwEventItem;
	uint	m_LowEvent[appEventAll];
	uint	m_HighEvent[appEventAll];

};

#endif
