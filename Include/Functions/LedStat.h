#ifndef __LED_STAT_
#define __LED_STAT_


#include "MultiTask/Thread.h"
#include "System/File.h"

using namespace std;
class LedStat :public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(LedStat);

	LedStat();
	~LedStat();
	void Start(void);

	int ForceOpenLed();	
	int Resume_ForceOpenLed();	
	int SetLedSwitch(int val/*1:turn off ; other:open*/,int update = 0 /**/);		
	int GetLedSwitch();/*1:turn off ; 0: turn on*/
	void SetServerStatus(int nServerStatus){ m_nSeverStatus = nServerStatus; }
private:	
	int OpenLedSwitch();
	int CloseLedSwitch();
	
	void ThreadProc(void);
private:
	//���ӷ�����״̬
	int m_nSeverStatus;
};


#define g_ledstat (*LedStat::instance())

#endif //__LED_STAT_

