//////////////////////////////////////////////////////////////////////////
/// 配置导入导出功能类，供网络和本地GUI调用，方便后续添加新需求和维护
/// 
//////////////////////////////////////////////////////////////////////////

#ifndef _IN_EXPORT_H_
#define _IN_EXPORT_H_

#include "System/Object.h"
#include "System/Packet.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "config-x.h"

enum configfile_type
{//与下面的数组一一对应
	CONFIG_PPPOE_ENABLE = 0,		///< pppoe-enable配置文件
	CONFIG_DHCP_CONFIG  = 1,		///< dhcp.cfg配置文件modified by wyf on 091125
	CONFIG_ACCOUNT1 = 2,			///< account1配置文件
	CONFIG_ACCOUNT2 = 3,			///< account2配置文件
	CONFIG_CONFIG1  = 4,			///< config1配置文件
	CONFIG_CONFIG2  = 5,			///< config2配置文件
	CONFIG_DIGICHCONFIG1  = 6,	    ///< config1配置文件
	CONFIG_DIGICHCONFIG2  = 7,		///< config1配置文件
	CONFIG_NETWORK=8,				///network配置文件
	CONFIG_RESOLV_CONF=9,		       ///resolv.conf配置文件
	CONFIG_MAX
};

//每个配置文件的头结构体
typedef struct tag_confFileHeader
{
	int type;
	int len;
}CONFFILEHEADER;

class CInExport
{
public:
	///< 导入导出配置文件的类型
	enum InExportFileType
	{
	    InExportFileAll 		= 0,			//全部的配置文件
		InExportFileLocal 		= 1,			//本地配置文件
		InExportFileNetwork 	= 2,			//网络配置文件
		InExportFileUser		= 3,			//用户配置文件
	};

	///< 定义部分常量
	enum
	{
		enum_configbuf_len		= 64*1024,	///< 配置缓冲区的大小
		enum_crchead_len		= 4,		///< CRC校验值占用的长度
		enum_filehead_len		= 8,		///< 文件头长度，不含CRC
		enum_vision_len			= 20,		///< 版本信息占用的长度
		enum_languagevideo_len	= 8,		///< 语言和视频制式占用的长度
		enum_file_path_len		= 200,		///< 文件路径长度
	};
	
public:
	/// 单件模式
	PATTERN_SINGLETON_DECLARE(CInExport);

	/// 构造函数
	CInExport();

	/// 析构函数
	~CInExport();

	/// 导出配置的接口函数
	/// \param[in] type导出配置的类型，参见
	/// \param[out] pPacket存放需要导出的配置
	/// \return 成功返回文件总长度，返回-1表示内存申请失败，-2表示文件不全
	int ExportConfig(int type, CPacket* pPacket);

	/// 导入配置的接口函数
	/// \param[in] type导入配置的类型，参见
	/// \param[in] pPacket待导入的配置
	/// \return 0成功 -1失败
	int InportConfig(int type, CPacket* pPacket, int iRecvDataLen);
	
private:
	CMutex		m_Mutex;		///< 配置导入导出互斥锁

};
#endif //_IN_EXPORT_H_

