

#ifndef __SNAP_H__
#define __SNAP_H__

#include "Devices/DevCapture.h"
#include "System/AppEvent.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigRecord.h"
#include "Configs/ConfigEncode.h"

#include "Functions/FFFile.h"

#if defined(_USE_720P_MODULE_)
#define N_SNAP_PACKET 150
#else
#define N_SNAP_PACKET 100
#endif

typedef struct tagSNAPDATA
{
	int iLength;					/*!< 图片的大小 */
	int iPktNum;					/*!< 图片中包含的数据包的个数 */
	SYSTEM_TIME sys_Createtime;		/*!< 图片抓取时间 */
	CPacket *pPkt[N_SNAP_PACKET];	/*!< 图片数据包的指针数组 */
}Snap_Data;


typedef struct tagSNAPGROUP
{
	appEventCode	appCode;		/*!< 触发源 */
	int				iIndex;			/*!< 触发源的序列号 */
	uint			dwChannel;		/*!< 抓图的通道掩码 */		
	int				iTimes;
	int				counts;
	int				iChanIndex;

}Snap_Group;

class CSnapBuffer
{
public:
	enum snapDirection
	{
		snapForward,	/*!< 向前取，即由新到旧 */
		snapBackward	/*!< 向后取，即由旧到新 */
	};

	CSnapBuffer();

	~CSnapBuffer();

	void init(int iNum);
	
	void pushData(CPacket* pPacket, uchar byFlag, DHTIME* pDHTime);

	int popData(Snap_Data* pSnap, snapDirection iDirection);

	int popData(Snap_Data* pSnap, int seconds);

	void clearData();

	int getSnapNum();
private:
	Snap_Data	*m_pSnapData;//! 图片数据的指针

	int m_iFront;		//! 循环队列的队头
	int m_iRear;		//! 循环队列的队尾
	int m_iSize;		//! 循环队列的大小
};

/*!
	\class CSnap
	\brief 
*/
class CSnap : public CObject
{
#ifdef SNAP_NET_THREAD_MULTI
	typedef struct PACKET_SNAP
	{
		CPacket* pPacket;
		uint dwFrameFlag;
	}PACKET_SNAP;
	typedef std::list<PACKET_SNAP> PACKET_LIST_SNAP;
#endif

public:
	enum snapStateType
	{
		snapStateStop = 0,
		snapStateStart,
		snapStateReadyStop
	};

	enum snapTriggerMode
	{
		snapTriggerOver,      /*初始值*/
		snapTriggerTimer,    /*定时抓拍*/
		snapTriggerEdgeALM,      /**/
		snapTriggerEdgeDYN,
		snapTriggerPeriod,   /**/
		snapTriggerManual,  /**/
	};

	//! 构造函数
	CSnap();

	//! 析构函数
	~CSnap();

	//! 初始化函数
	void init(int iNum, int iChannel);

	//!	开始抓图
	VD_BOOL start(snapTriggerMode snapMode);

	//! 停止抓图
	VD_BOOL stop();
	//此函数立即执行关闭操作，可能会导致存储了半帧数据
	VD_BOOL stopSnap();

	uchar getState();

	//! 根据时间取图片
	int getSnap(Snap_Data *pSnap, int seconds);

	//! 根据方向和张数取图片
	int getSnap(Snap_Data *pSnap, CSnapBuffer::snapDirection iDirection);
	
#ifdef SNAP_NET_THREAD_MULTI
	int storageHDD(int channel,CPacket *pPacket , uchar byFlag);
	bool DosStorage();
#endif	
	int storageHDD(CPacket *pPkt, uchar flag);

	/*web 收到抓图的回传接口*/
int SendPic(CPacket *pPacket,uchar byFlag,uchar byType,DHTIME *pDHTIME);

private:
	int SaveSnapByDir(CPacket *pPacket, uchar byFlag, char *PathName);
	
	//! 抓图回调函数
	void OnSnap(int iChannel, uint dwStreamType, CPacket *pPacket);

private:
	
#ifdef SNAP_NET_THREAD_MULTI
	PACKET_LIST_SNAP m_packet_list;
	uint m_packet_count; //!主要实时更新m_packet_list的数量，这样判断数量可以不用进入锁里去
	CMutex	m_mutex_list;
#endif	

	int 			m_iChannel;					//! 通道数

	int 			m_iSnapCount;
	CMutex 			m_SnapMutex;				//! 抓图开关锁
	
	CSnapBuffer 	m_Buffer;			//! 抓图缓冲区

	int				m_iSnapState;				//!抓图位开关0x01表示预录 0x10标示存硬盘
	CMutex			m_SnapRecMutex;		//!递归锁
	
	FFFile          m_File;
	
	int 				m_iCount;     /*用于定期检测硬盘是否满*/

	STM_ATT			m_Attr;				/*!< 文件系统属性 */
	STM_ATT			m_CurrentAttr;				/*!当前文件系统属性*/

	snapTriggerMode	m_workMode;		///< 触发模式，上升沿还是过程 */
	
	VD_BOOL			m_bContinued;
	
	DHTIME			m_startDHTime;

#define DHSNAP_BUF_SIZE 	1024*1024//512*1024
	uchar 				m_tmpBuf[DHSNAP_BUF_SIZE];
	uint 				m_tmpBuf_len;

public:
	char		m_TakePicNum; //手动抓拍的图片数
};

#ifdef SNAP_NET_THREAD_MULTI
class CSnapManager: public CThread
#else
class CSnapManager : public CObject
#endif
{
public:
	PATTERN_SINGLETON_DECLARE(CSnapManager);

	//! 构造函数
	CSnapManager();

	//! 析构函数
	~CSnapManager();

	void start();

	void stop();
	int onSnap(int argc, char **argv);

	void startSnap(int snapType, int iChannel);

	void startSnap(appEventCode appcode, int index, int iChannel, void *pData);

	void stopSnap(int snapType, int iChannel);

	void stopSnap(appEventCode appcode, int index, int iChannel);

	void take_picture(int iChannel = 0);   //拍照

	bool isSendPicEmail();
	int getSnapDataByDir(Snap_Data *pSnap,int iChn,CSnapBuffer::snapDirection iDir);
	void SynSnapCfg();

    int storageHDD(int channel,CPacket *pPacket , uchar byFlag, bool force = false);
private:

#ifdef SNAP_NET_THREAD_MULTI
	void ThreadProc();
#endif

	void onConfigSnap(CConfigSnap& cCfgSnap, int& iRet);
	void AppConfigSnap(CONFIG_SNAP *pConfigSnap,int iChn);

	void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* pData);

	void sendPolicy(Snap_Group *pGroup, int iChannel);
	
#if defined(SNAP_REQUIRE_ISTC)
	void sendISTCPolicy(Snap_Group *pGroup, int iChannel);
#endif
private:
	CConfigSnap m_cCfgSnap;
	
	CSnap*		m_pSnap[N_SYS_CH];			//! 抓图通道对象

	int			m_iInterval;				/*!< 记录抓图的时间间隔 */
	
	uchar		m_iSnapType[N_SYS_CH];

	bool 		m_bSendPicEmailEn;		//发送报警抓图图片使能

	CMutex		m_PictureMutex;
};

#define g_Snap (*CSnapManager::instance())

#endif

