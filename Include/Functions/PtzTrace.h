#ifndef PTZTRACE_H_
#define PTZTRACE_H_

#include "APIs/Ptz.h"
#include "APIs/Types.h"
#include "APIs/System.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"
#include "MultiTask/Thread.h"
#include "APIs/Thread.h"
#include "APIs/DVRDEF.H"
#include "Configs/ConfigPTZ.h"
#include "Configs/ConfigCamera.h"
#include "Functions/AlarmDetect.h"

typedef enum{
	PTZRUN_STOP      	= 0,   //停止运动
	PTZRUN_UP        	= 1,   //向上
	PTZRUN_DOWN      	= 2,   //向下
	PTZRUN_LEFT      	= 3,   //向左
	PTZRUN_RIGHT     	= 4,   //向右	
	PTZRUN_GOTO_PRESET	= 5,   //行驶至预置点
    PTZRUN_GOTO_MID  = 6,//产测归位
    PTZRUN_CRUISE = 7,//产测自动巡航
    
}m_PtzControlCmd;

class PtzTrace : public CThread
{
public:

	PATTERN_SINGLETON_DECLARE(PtzTrace);
	
	PtzTrace();
	~PtzTrace();

	void Start();
	void Stop();
private:
	CConfigPtzTrace	m_PtzTrace;

	/*航巡计划*/
	CONFIG_TRACE	m_PtzTracePlan;		
	/*当前航巡预置点*/
	uint	m_CurPresetPoint;
	/*暂停航巡标志，若为真就停止航巡*/
	VD_BOOL m_PauseTraceFlg;

	CONFIG_CAMERA m_cfgCameraParam;
	CConfigCamera m_CfgCamera ;
	CMutex mMutex;
public:
	/*关闭该功能，清空航巡时间段配置*/
	void PtzTraceClose();
	
	void PtzRunCmd(m_PtzControlCmd cmd,int presetId = -1);
	int  PtzRunToPrePoint(int x,int y);
private:	
	VD_BOOL IsMatchCondition();
	void onConfigCamera(CConfigCamera& cCfg, int& ret);
	void onConfigPtzTrace(CConfigPtzTrace& cCfg, int& ret);
	void ThreadProc();

};

#define g_PtzTrace (*PtzTrace::instance())

#endif
