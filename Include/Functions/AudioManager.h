
#ifndef __AUDIOMANAGER_H__
#define __AUDIOMANAGER_H__

#include "config-x.h"
#include "Devices/DevAudioOut.h"
#include "Devices/DeviceManager.h"
#include "System/Signals.h"
#include "System/AppEvent.h"
#include "System/File.h"
#include "Configs/ConfigEncode.h"//audio qjwang 091029

//优先级由高到低:语音对讲->语音提示->回放->监视
enum Audio_Type
{
	AUDIO_TALK_TYPE = 0,	//语音对讲
	AUDIO_TIP_TYPE, 		//语音提示
	AUDIO_PLAY_TYPE,		//回放
	AUDIO_MONITOR_TYPE, 	//监视
};
typedef struct _StartAudioInfo{
	int iChannel;
	VD_BOOL   bMute;
	}StartAudioInfo;

class CAudioManager : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CAudioManager);
	CAudioManager();
	~CAudioManager();
	void 	init(void);	
	VD_BOOL	StartAudio(int iAudioType, int iPlayChannel, VD_BOOL bMute=FALSE);			//开启语音提示
	void	StopAudio(int iAudioType);												//关闭语音提示	
	void 	Lock();
	void 	UnLock();
	VD_BOOL 	PlayVoice(uchar *data, int length, int type, int format = 0);
	void 	onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data = NULL);

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	void onTimer(uint arg);
	void onMusicPlayTime(uint arg);
#endif

private:
	void	SetAudioState(int iAudioType);
	void	ResetAudioState(int iAudioType);
	VD_BOOL	IsAudioRunning(int iAudioType); 
	void 	onAudioTip(void);					//语音提示	
	void		onConfigAudio(CConfigAudioInFormat* pConfig, int& ret);		// 音频输入格式更改//audio qjwang 091029
private:
	CFile	m_FileAudioTip;						//语音文件
	uint	m_dwAudioState;						//按位表示语音类型
	int		m_VoiceType;
	
	CMutex	m_AudioMutex;						//保护状态位的修改
	CMutex	m_TipTalkMutex;						//语音提示和语音对讲Mutex
	CDevAudio *m_pDevAudio;
	//保存开启声音设备的参数
	StartAudioInfo m_StartAudioInfo[AUDIO_MONITOR_TYPE+1];	

	CConfigAudioInFormat m_CcfgAudio;//audio qjwang 091029
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	CTimer 	m_cTimer;  			//定时检查播放计划
	CTimer 	m_cMusicTimer; 	    //播放时间
	char 	m_cMusicFile[64] ;  //当前播放文件
	int 	m_iMiniute;         //播放时间
	int  	m_iIndex;
	int     m_iVolume;
#endif
};
#define g_AudioManager (*CAudioManager::instance())
#endif //__AUDIOMANAGER_H__

