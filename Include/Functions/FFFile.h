#ifndef __FFFILE_H__
#define __FFFILE_H__

#include <stdio.h>
#include "Mp4/Mp4Pack.h"
#include "System/Packet.h"
#include "System/Object.h"
#include "MultiTask/Thread.h"
#include "Intervideo/DevSearch/DevSearch.h"

//#ifndef __DH_FILE_H__

#ifdef RECORD_NET_THREAD_MULTI 

#define PACKET_GROUP_MAX_COUNT (8)
typedef struct PACKET_RECORD
{
	CPacket *pPacket;
	uint	dwStartPos; //!使用的packet 开始位置
	uint	dwCount;//!对应的packet使用长度
}PACKET_RECORD;
typedef struct PACKET_GROUP_RECORD
{
	PACKET_RECORD pPacketRecord[PACKET_GROUP_MAX_COUNT];
	int 	count;	//!缓冲区个数
}PACKET_GROUP_RECORD;
#endif

struct STM_ATT {		//控制在32字节内
	uchar	chan;		//通道
	uchar	type;		//av
	uchar	imgq;		//画质
	uchar	alrm;		//报警
	SYSTEM_TIME	time;	//文件的创建时间
	VD_BOOL	redu;		//是否冗余备份
	uint	driver_type;//驱动器型
};

struct STM_INFO {		//控制在32字节内
	uchar	chan;		//通道
	uchar	av;			//av
	uchar	image;		//高4位帧率，低4位画质
	uchar	alarm;		//报警
	SYSTEM_TIME		stime;		//开始时间
	SYSTEM_TIME		etime;		//结束时间
	uint	len;		//文件长度, KB为单位!!!
	uint	csize;		//文件簇大小, KB为单位!!!
};

// ??????
typedef int DHFILE;

// 外部调用错误号
enum FileSystemErrorCode
{
	FS_NO_ERROR		= 0,
	FS_OK			= FS_NO_ERROR,	///< 无错误
	FS_NO_HANLE		= 1,		///< 没有句柄
	FS_EOF			= 2,		///< 文件结束
	FS_READ_ERROR	= 3,		///< 底层读错误
	FS_WRITE_ERROR	= 4,		///< 底层写错误
	FS_NO_SPACE		= 5,		///< 没有空间
	FS_NO_EXIST_FILE = 6,		///< 文件不存在
	FS_ERROR		= 7,		///< 没有硬盘
	FS_OVERLAY		= 8,		///< 单个硬盘或硬盘之间时间冲突
	FS_MUST_REINIT	= 9,		///< 必须从新初始化
	FS_UD_NOT_VALID	= 10,		///< 用户数据无效
	FS_INVALID_HANDLE	= 11,	///< 句柄无效
	FS_INVALID_FILENAME = 12,	///< 文件名无效
	FS_ALREADY_EXISTS	= 13,	///< 文件已经存在
	FS_TIME_ERROR	= 14,		///< 文件系统最后时间与当前时间冲突
	FS_BAD_DISK		= 15,		///< 有坏盘
	FS_NO_HDD		= 16,		//无硬盘
};


enum
{
	FILE_READ_SINGLE = 1,		///< 读单个文件
	ALL_CHANNELS	 = 255,
};


/// 驱动器类型
enum FileSystemDriverTypes
{
	DRIVER_TYPE_BASE	= 0,	///< 驱动器类型起始值
	DRIVER_READ_WRITE	= 0,	///< 读写驱动器
	DRIVER_READ_ONLY	= 1,	///< 只读驱动器
	DRIVER_EVENTS		= 2,	///< 事件驱动器
	DRIVER_REDUNDANT	= 3,	///< 冗余驱动器
	DRIVER_SNAPSHOT		= 4,	///< 快照驱动器
	DRIVER_BACKUP		= 5,	///< 备份驱动器
    DRIVER_GENERIC		= 6,	///< 常规目录对应驱动器
    DRIVER_RESERVE		= 7,	///< 保留分区驱动器
    DRIVER_TYPE_NR		= 8,	///< 驱动器类型个数
	DRIVER_CHECK_MEM	= 9,
	DRIVER_UNUSED		= 0xff,	///< 没有使用的驱动器结构
};

/// 文件系统支持的文件类型
enum FileSystemFileTypes
{
	F_COMMON	= 0x00,		///< 普通录像
	F_ALERT		= 0x01,		///< 外部报警
	F_DYNAMIC	= 0x02,		///< 动态检测
	F_CARD		= 0x03,		///< 卡号录像
	F_HAND		= 0x04,		///< 手动录像
	F_2SND		= 0x05,     ///< 辅码流录像
};
#ifndef __SMALL_FILE_TIME_DEFINED__
#define __SMALL_FILE_TIME_DEFINED__
//时间结构
typedef struct _DHTIME								
{
#ifdef __BIG_ENDIAN__
	uint year		:6;				//	年	2000-2063	
	uint month		:4;				//	月	1-12		
	uint day		:5;				//	日	1-31		
	uint hour		:5;				//	时	0-23		
	uint minute		:6;				//	分	0-59		
	uint second		:6;				//	秒	0-59	
#else
	uint second		:6;				//	秒	0-59		
	uint minute		:6;				//	分	0-59		
	uint hour		:5;				//	时	0-23		
	uint day		:5;				//	日	1-31		
	uint month		:4;				//	月	1-12		
	uint year		:6;				//	年	2000-2063	
#endif
}DHTIME,*pDHTIME;

#endif

//驱动器信息结构
typedef struct _DRIVER_INFO{
	uint	driver_type;		///< 驱动器类型
	uint	is_current;			///< 是否为当前工作盘
	uint	section_count;		///< 时间段数
	DHTIME  start_time1;		///< 录像时间段1开始时间
	DHTIME	end_time1;			///< 录像时间段1结束时间
	uint	two_part;			///< 是否有第二段
	DHTIME	start_time2;		///< 录像时间段2开始时间
	DHTIME	end_time2;			///< 录像时间段2结束时间
	uint	total_space;		///< 总容量，MB为单位
	uint	remain_space;		///< 剩余容量，MB为单位
	uint	error_flag;			///< 错误标志，文件系统初始化时被设置
	uint	index;				///< 硬盘物理序号
}DRIVER_INFO,*pDRIVER_INFO;

//驱动器时间段信息
typedef struct _DRIVER_SECTION{
	DHTIME start_time;			///< 开始时间
	DHTIME end_time;			///< 结束时间
}DRIVER_SECTION,*pDRIVER_SECTION;

//文件系统信息结构
typedef struct _SYS_USE_INFO
{
	uchar	drive_num;					///< 工作驱动器数量
	uchar	work_drive ;				///< 当前工作驱动器所在盘
	uchar   not_work_num;				///< 非工作驱动器数量
	uchar   not_work_driver;			///< 当前非工作驱动器所在盘
	uint	drive_space;				///< 工作驱动器总共空间,MB为单位
	uint	remain_space ;				///< 硬盘保留空间,MB为单位
	uint	drive_free_space;			///< 工作驱动器剩余空间,MB为单位

	DHTIME	start_time;					///< 开始录像时间
	DHTIME	end_time;					///< 结束录像时间
	uint	cur_work_free_space;		///< 当前工作驱动器剩余空间(盘不满),MB为单位
	uint	cur_work_overlay_space;		///< 当前工作驱动器剩余空间(盘己满，循环录),MB为单位
	uint  not_work_space;				///< 非工作驱动器总共空间,MB为单位
	uint  not_work_free_space;			///< 非工作驱动器剩余空间,MB为单位
	uint  error_space;					///< 坏空间,MB为单位
}SYS_USE_INFO,*pSYS_USE_INFO;

///< 文件信息结构,本地使用
typedef struct _FILE_INFO
{
	uchar		channel;				///< 通道号
	uchar		alarm;					///< 报警类型
	uchar		rev;					///< 保留
	uchar		video_audio;			///< 视频或音频
	uchar		image;					///< 图像质量
	uchar		hint;					///< 查找文件时使用的暗示参数
	DHTIME		start_time ;			///< 开始时间
	DHTIME		end_time ;				///< 结束时间
	uint		dur_time;				///< 录像时长 (s)
	uint		file_length ;			///< 文件长度，单位为KB
	uint		disk_no;				///< 所在的硬盘号
	uint		disk_part;				///< 所在的分区
	uint		first_clus_no ;			///< 文件首簇号
	uint		ud_no;					///< 用户数据号
	char		base_name[128];			///< 文件名
	char		ext_name[4];			///< 扩展名
	uint		checked;				///< 是否选中，用于文件备份
}FILE_INFO,*pFILE_INFO;

//用户数据结构
typedef struct _USER_DATA{
	union{
		uchar b_ud[8];
		ushort w_ud[4];
		uint dw_ud[2];
	}ud8;
	uchar b8;
	uchar b9;
	uchar channel;
	uchar flag;  //loop num
}USER_DATA,*pUSER_DATA;

typedef struct _DISK_ERROR_INFO64
{
	uint readmask;		///< 读错误
	uint writemask;		///< 写错误
	uint64 offset[32];	///< 错误偏移
	uint removemask;	///< 已移除的硬盘掩码 
}DISK_ERROR_INFO64, *pDISK_ERROR_INFO64;

typedef struct _DRIVER_MT_HOOK		///< 多任务有关的回调函数
{
	void (*wakup)(uint disk_no);	///< 唤醒回调函数
	void (*lock)();					///< 锁回调函数
	void (*unlock)();				///< 解锁回调函数
	int (*recover_hook)(uchar *, uint *, int);
}DRIVER_MT_HOOK;

#define NORMAL_DRIVER_TYPE 0x3
#define SNAP_DRIVER_TYPE 16
#define ANY_FILE_TYPE		0xff

#define DHFILE_INAVLID_POSITION 0xFFFFFFFF


//#endif
                                             
#define MB (1024.0)                                       
#define GB (1024.0*1024.0)//1048576.0  

#define VIDEO 1
#define IMAGE 0

#ifdef IPC_HISI
#define TARGET_DIR     "/home"
#elif defined(IPC_JZ)
#define TARGET_DIR     "/tmp/SDdir"
#elif defined(IPC_JZ_NEW)
#define TARGET_DIR     "/media"
#endif
#define SOURCE_DEV_DIR "/dev/"
#define FILE_SYSTEM    "vfat"

#define VIDEO_DIR  	TARGET_DIR"/video"
#define IMAGE_DIR  	TARGET_DIR"/image"

#define TIMER_STR	"timer"      //与TIMER_DIR相对应
#define ALARM_STR	"alarm"		 //与ALARM_DIR相对应
#define RECEXT_STR  "extern"
#define TIMER_DIR   VIDEO_DIR"/"TIMER_STR  //定时录像文件夹
#define ALARM_DIR   VIDEO_DIR"/"ALARM_STR  //移动报警录像文件夹
#define RECEXT_DIR	VIDEO_DIR"/"RECEXT_STR


//////////////////////小录像///////////////////////////////////////////
#define PIRREC_ON	(1)	
#define PIRREC_OFF  (0)


//用于S1开机唤醒录制报警短视频
//(原因:开机SD卡挂载速度过慢,S1需要从开机第一帧开始录制)


//是否开启小视频录像 (与普通录像同时运行) 
#ifdef IPC_JZ
#define PIRREC_SET	PIRREC_ON
#else
#define PIRREC_SET	PIRREC_OFF
#endif
#define PIRREC_DUR	(10) //录像时长   单位为 s
#define PIRALM_DIR	"/tmp/extern"

extern int ext_rec;	//小视频录像标志 (与普通录像同时运行)
////////////////////////////////////////////////////////////////////////

#define MIN_CAPACITY    (300*MB)   //SD卡最低允许内存，小于该容量存储变成覆盖模式
#define INTERVAL_VIDEO  (60)   //单个录像的最大时长 单位为 s，若超过该时长则分割视频
#define ISFULL_CAPACITY (300*MB)//SD最低允许内存,小于该容量可认为SD已存满

#define FILENUMS		(30)
typedef struct 
{
    char FileName[128];
	char DirPath[128];
}FilePath; 

typedef struct 
{
	int 		FilesCount;
	FilePath	FilesList[FILENUMS];
}VideoFiles;


#define TIMELISTNUMS (2048)
typedef struct{
	int TimeCount;
	unsigned long TimeList[TIMELISTNUMS];
}VideoTimeList;

class FFFile
{
public:
	FFFile();
	virtual ~FFFile();
public:
	VD_BOOL Open (char mtype = VIDEO);
	VD_BOOL Close(bool divide = false);
	VD_BOOL IsOpened();
	int Seek(uint lOff, uint nFrom);
	int GetPosition();
	int Write(void *pBuffer, uint dwCount,VD_BOOL bPacket = FALSE);
#ifdef RECORD_NET_THREAD_MULTI 
	VD_BOOL WriteGroup(PACKET_GROUP_RECORD* pPacketGroup);
#endif
	VD_BOOL GetList(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, 
	uint type, uint *num, FILE_INFO *info, uint hint = 0, uint driver_type = 0x03); //NORMAL_DRIVER_TYPE 0x03

public:
	void*	ExtOpen();
	VD_BOOL ExtClose();
	VD_BOOL GetExtRecFile(char FileName[128]);
	
private:
	void ThreadProc();
public:
	uchar almtype;
private:
	void *handle;
	FILE *m_fd;
	struct tm start_tm;
	struct tm end_tm;
	struct tm pre_end_tm;//上个文件的结束时间

	char type;
	char namebuf[128];
	
	int  v_count;
	int  i_count;

	CMutex	m_Mutex;

	void *handle_ext;
	FILE *m_fd_ext;
	char namebuf_ext[128];
	int m_iWriteError;
};

class VideoFileReader{
public:
	VideoFileReader();
	virtual ~VideoFileReader();

public:
	VD_BOOL Open();
	VD_BOOL IsOpen();
	VD_BOOL Close();
	VD_BOOL Locate(SYSTEM_TIME st);
	uint 	Read(void* pBuffer,uint len);
	
private:
	FILE 	*m_fd;

	CMutex	m_Mutex;
	SYSTEM_TIME ftime;
};
class FileSync: public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(FileSync);

	FileSync();
	virtual ~FileSync();
	VD_BOOL Start();
	VD_BOOL Stop();
	VD_BOOL SetFd(FILE **fd);
	
private:
	void ThreadProc();
public:
	VD_BOOL Data_in;
	CMutex  m_SyncMutex;
private:
	FILE 	**Sync_fd;
};
#define g_FSync (*FileSync::instance())

int GetPidByName(char* task_name);

int  file_sys_init();
int  file_sys_deinit();
int	 file_sys_format();
//检测SD卡是否连接
int  file_sys_get_linkstat();
//得到SD总容量和剩余容量
int  file_sys_get_cap(int* total,int* free); //单位为kb
int  file_sys_get_cap_bit(int64_t* total,int64_t* free);

bool file_sys_is_full();
bool file_sys_del();
int file_umount_all(char* pSourceNode);
int mountStatus();
//start_time:查询开始时间
//end_time:  查询结束时间
//若想查询该目录下全部文件则给 start_time 或 end_time 赋值 NULL 即可
int file_get_all_videolist(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time);    //得到全部录像，包括定时和报警
int file_get_earlist_videolist(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time);
int file_get_all_videolist_smallsort(VideoFiles *list, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time);//小序排列
int file_get_all_videolist_bigsort(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time);//大序排列
int file_sys_getlist(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, uint type, uint *num, FILE_INFO *info, uint hint /* = 0 */, uint driver_type);
//查询时间段存在录像的天数列表
//如果某天存在录像 则存下该天零点对应的时间戳到list结构体
int file_get_video_timelist(VideoTimeList *list,SYSTEM_TIME* startTime, SYSTEM_TIME* endTime);
//////////////////write//////////////////////////////////////////
void* MP4Writer_Open (const char *FileName);
void  MP4Writer_Close(void* Handle);
FILE* MP4Writer_GetFp(void* Handle);
int	  MP4Writer_GetCurrentPTime(void* Handle);
int   MP4Writer_Write(unsigned char* buf,int len,void* Handle);
//////////////////read///////////////////////////////////////////
int MP4Reader_Open (const char *FileName);
int MP4Reader_GetVFrameNums(void);
int MP4Reader_GetAFrameNums(void);
int MP4Reader_GetVFrameDur();
int MP4Reader_GetAFrameDur();
int MP4Reader_GetVideoTimeStamp(int val);
int MP4Reader_GetAudioTimeStamp(int val);
int MP4Reader_GetKeyFrameNums();
int MP4Reader_Get_I_FrameNum(int val);
int MP4Reader_VideoData(unsigned char *Buf,int Num);
int MP4Reader_AudioData(unsigned char *Buf,int Num);
void MP4Reader_Close();
VD_BOOL GetExtRecFile(char FileName[128]);
////////////////////////////////////////////////////////////////
int mkdir_mul(char *mulDir) ;
#endif
