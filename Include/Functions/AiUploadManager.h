#ifndef AIUPLOADMANAGER_H_
#define AIUPLOADMANAGER_H_

#include "APIs/Types.h"
#include "APIs/Thread.h"
#include "APIs/DVRDEF.H"
#include "System/Packet.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"
#include "APIs/DynamicSnap.h"
#include "Functions/FFFile.h"
#include "MultiTask/Thread.h"
#include "APIs/MotionDetect.h"
#include "Configs/ConfigEvents.h"
#include "Configs/ConfigCamera.h"
#include "Configs/ConfigGeneral.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"

#define STRANGER_NUM	(2)		//用于临时存储陌生人	
#define FACELIB_MAX		(9 - STRANGER_NUM)  // 人脸库最大数目
#define FACEUPLOAD_S	(60)  // 同一图片60s上传一次 //单位 s
#define CRYUPLOAD_S		(10)  // 10s上传一次哭声检测 //单位 s


typedef std::list<ScenePicInfo *> mPACKET_LIST;

typedef struct TagHeadInfo{
	int  cnt;
	char rec[500];
}HeadInfo;


typedef struct TagFaceJpegInfo{
	int		uid;
	char 	name[32];
	int  	sex;
	int	 	age;
	int		glass;
	char	path[256];
	char	url[2048];
}FaceJpegInfo; 

using namespace std;

#define OPERA_OK	(0)
#define	OPERA_FAIL	(1)
#define PARAM_ERR	(2)
#define PARAM_FULL	(3)

class AiUploadManager: public CThread
{
public:
	AiUploadManager();
	~AiUploadManager();
	
	PATTERN_SINGLETON_DECLARE(AiUploadManager);

	void Start();
	void Stop();
	
/*-------------------------------------------------------------------------------------*/
//人脸相关接口	
public:
//the api of face
	int GetStoreDir(char *pPath);
	int AddUserFace(RT_ModNewFaceInfo FaceInfo);
	int DelUserFace(RT_ModifyFaceInfo FaceInfo);
	int ModUserFace(RT_ModifyFaceInfo ModifyFaceInfo,RT_ModNewFaceInfo ModNewFaceInfo);
	int QueryUserFace(RT_QueryFaceInfo QueryFaceInfo,RT_QueryRespFaceInfo *RespFaceInfo);
	int QueryUserFace(int FaceUid,RT_QueryRespFaceInfo *RespFaceInfo);

	int UploadFaceData(ScenePicInfo* pInfo);	
	
	friend int OnPushFaceData(ScenePicInfo* pPicInfo);	
protected:
	volatile VD_BOOL m_bLoop;	
private:
	int ProcessList();	
	int FreePicBuffer(ScenePicInfo* pPicInfo);
	int OpenRecordFile();
	int AddRecordFileInfo();

	//void LoadConfig();
	void onConfigAiUpload(CConfigAiUpload& cCfgAi, int& ret);	

	void ThreadProc();

private:
	CMutex mMutex;
	int m_packet_count ;
	
	mPACKET_LIST PacketList;
private:
	FILE* 		m_fp;
	HeadInfo 	hInfo;
	int m_inputData;

	uint faceUpFreq;
	uint strangerUpFreq;
	uint cryUpFreq;		//该值现在没用到
	CConfigAiUpload m_AiUpload;	
/*-------------------------------------------------------------------------------------*/
//other
private:
	int	iSuspend;		//0:normal 1 :suspend
	CConfigGeneral		m_cCfgGeneral;

	void onConfigGeneral(CConfigGeneral& cCfg, int& ret);
};
#define g_AiUpload (*AiUploadManager::instance())
#endif
