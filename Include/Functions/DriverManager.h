

#ifndef __DRIVER_MANAGER_H__
#define __DRIVER_MANAGER_H__

#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>    
#include <sys/stat.h> 
#include "APIs/DVRDEF.H"
#include "APIs/Ide.h"
#include "System/File.h"
#include "Functions/FFFile.h"
#include "System/Log.h"
#include "System/Signals.h"
#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "MultiTask/Timer.h"
#include "Devices/DeviceManager.h"
#include "Configs/ConfigEvents.h"

typedef enum _hdd_alarm_type
{
	TYPE_HDD_ERROR = 0,
	TYPE_HDD_NOSPACE,
	TYPE_HDD_NONE,
}HDD_ALARM_TYPE;
class CDriverManager : public CThread
{
	friend void dhfile_hook_wakeup(uint disk_no);
	friend void dhfile_hook_lock();
	friend void dhfile_hook_unlock();

	
public:
	PATTERN_SINGLETON_DECLARE(CDriverManager);
	CDriverManager();
	virtual ~CDriverManager();
	
	typedef TSignal2<uint, int>::SigProc SIG_HDDALARM_BUFFER;
	VD_BOOL Start();
	VD_BOOL Stop();	
	
	//Ӳ�̹���
	void InitializeFS();
	uint GetFSError();
	void UpdateState();
	VD_BOOL ScanDisk(DISK_ERROR_INFO64 *pError);
	VD_BOOL IsFull(uint *stat, uint driver_type = DRIVER_READ_WRITE);
	VD_BOOL IsSnapExist();
	VD_BOOL IsFormatDriver();
	VD_BOOL GetIdeInfo(IDE_INFO64 *pide_info);
	VD_BOOL GetDriverInfo(uint disk_no,uint part,DRIVER_INFO *pdriver_info);
	VD_BOOL SetDriverType(uint disk_no,uint part,uint driver_type);	
	VD_BOOL GetDriverType(uint disk_no,uint part,uint* pdriver_type);
	VD_BOOL FormatDisk(uint disk_no);
	
	VD_BOOL FormatDriver(uint disk_no,uint part);
	VD_BOOL ParoleDriver(uint disk_no,uint part);
	VD_BOOL GetDriverSection(uint disk_no,uint part, uint index, DRIVER_SECTION *pdriver_section);
	VD_BOOL GetFSInfo(SYS_USE_INFO *info);
	VD_BOOL WakeupCurrDriver();

	//Ӳ�̱������
	void StartHddAlm();
	void OnHddAlmTimer(uint param);

	//����Ӳ�̱���
	void NotifyHDDError(appEventCode appCode);
	void NotifyHDDSpace(int iPercent);
	void NotifyHDDNone();
	void NotifyHDDState();//���Ӳ��״̬

	//! ���õ�ǰӲ�̱�����״̬
	void SetAlarmState(VD_BOOL bState);

	//! ��õ�ǰӲ�̱�����״̬
	VD_BOOL GetAlarmState();

	//add by nike.xie 2009-07-08  ��ȡ����Ӳ�̵�״̬
	VD_BOOL GetHDDNotExistFlag();
	VD_BOOL CheckHDDNotExist();

	//���Ӳ�̳���������Ӳ�̿ռ䲻�㱨��״̬����
	uint GetHDDErrorState(int type);		//�õ�Ӳ��״̬����;type == 0----Ӳ�̳�����type == 1----Ӳ�̿ռ䲻��

	// ���ñ仯�Ĵ���
	void onConfigStorageNotExist(CConfigStorageNotExist* pConfig, int& ret);
	void onConfigStorageFailure(CConfigStorageFailure* pConfig, int& ret);
	void onConfigStorageLowSpace(CConfigStorageLowSpace* pConfig, int& ret);
	int GetSDStatus(){return m_dwSDErrState;}
	void SetSDStatus(uint status){ m_dwSDErrState = status;}
	void DetectEnable(VD_BOOL val){ m_bDetectEn = val;}	
	int  GetDetectEnable(){return m_bDetectEn;}
private:	
	void ThreadProc();
	VD_BOOL Dispatch(VD_MSG &msg);
	int WriteSDFile(uint8_t *buf, uint len);
	uint SDBufCrc32( const unsigned char *buf, uint size);
	int DetectSDStatus();	
	int ReadFlashFile(char* pFilename,CPacket *&pPackt, int *len);

public:
	VD_BOOL  m_bSnapExist;	
	VD_BOOL	 m_bDetectEn;
private:
	CMutex	m_Mutex;
	CTimer 	m_TimerHDDAlarm;	
//	CDHFile m_DHFile;
	VD_BOOL	m_bHDDLowerAlarmed; 		//Ӳ�̿ռ䲻���ѱ���
	VD_BOOL	m_bStopHDDRrrorAlarm;		//ֹͣӲ�̳�������
	VD_BOOL	m_bStopNOHDDAlarm;			//ֹͣ��Ӳ�̱���

	VD_BOOL      m_bHDDNotExistFlag;              //add by nike.xie 2009-07-08 �Ƿ����Ӳ�̵ı�־

	uint m_dwHddErrState;				//��ǰӲ�̴�������
	uint m_dwHddSpaceState;			//Ӳ����״̬����

	uint m_dwSDErrState;				//��ǰӲ�̴�������
	
	uint  m_dwFSError;
	uint  m_dwFull[DRIVER_TYPE_NR];
	
	VD_BOOL	m_AlarmState;
	VD_BOOL m_bFormatDriver;
	
	CConfigStorageNotExist	m_configStorageNotExist;	/*!< û�д洢���¼����� */
	CConfigStorageFailure	m_configStorageFailure;		/*!< �洢�����ʴ����¼����� */
	CConfigStorageLowSpace	m_configStorageLowSpace;	/*!< �洢�����������¼����� */
};
#define g_DriverManager (*CDriverManager::instance())
#endif// __DRIVER_MANAGER_H__

