

#ifndef __DAYLIGHTN_H__
#define __DAYLIGHTN_H__

#include "APIs/System.h"
#include "System/CMOS.h"
#include "MultiTask/Timer.h"
#include "Configs/ConfigLocation.h"
class CDaylight : CObject
{
public:
    PATTERN_SINGLETON_DECLARE(CDaylight);
    CDaylight();
    ~CDaylight();
    void Start();

private:
    void onConfigLocation(CConfigLocation* pConfig, int& ret);
    void onConfigLocationChange(CConfigLocation* pConfig, int& ret);
    
    void OnTimer(uint par);
    int PrepareTime(int year);
    void prepareDSTtime(SYSTEM_TIME* pDesTime, DST_POINT* pDST, int year);

protected:
private:
    SYSTEM_TIME m_LastTime;        //最后记录的时间
    SYSTEM_TIME m_DecreasingTime;    //当年减一小时的时刻
    SYSTEM_TIME m_IncreasingTime;    //当年加一小时的时刻
    SYSTEM_TIME m_DecreasedTime;    //当年减一小时的后的时间
    SYSTEM_TIME m_IncreasedTime;    //当年加一小时的后的时间
    CTimer m_Timer;                //定时器
    int    m_iType;                //夏令时规则
    VD_BOOL m_bTuned;                //夏令时标志
    VD_BOOL m_bNorth;                //是否在北半球
    CConfigLocation    m_CcfgLocation;
};

#define g_Daylight (*CDaylight::instance())

#endif
