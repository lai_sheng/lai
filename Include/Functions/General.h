

#ifndef __GENERAL_H__
#define __GENERAL_H__

#include "Language.h"
#include "MultiTask/Timer.h"
#include "Record.h"
#include "System/Log.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigGUISet.h"

class CGeneral : public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CGeneral);
	CGeneral();
	virtual ~CGeneral();
	
	VD_BOOL Start();
	VD_BOOL Stop();
	
//	uint GetStandbyTime();

	//! 处理保存系统时间, iToleranceSeconds表示容许多少秒内不修改
	VD_BOOL UpdateSystemTime(SYSTEM_TIME *systime, int iToleranceSeconds = 0);

	int GetSuspendStatus();
	void OnDetect(uint arg);
private:

	//! Location配置变化回调函数
	void onConfigLocation(CConfigLocation& cCfgLocation, int& ret);

	void onConfigGeneral(CConfigGeneral& cCfg, int& ret);
private:
	CConfigLocation		m_CCfgLocation;
	CConfigGUISet		m_configGUISet;
	CConfigGeneral		m_cCfgGeneral;
	CTimer				m_DetectTimer;				/*!< 用来检测时间是否跳变的定时器 */
	DHTIME				m_DHTime;				/*!< 用来检测系统时间 */
	int 				m_iSuspend; 			/*!< 0:正常状态 1:待机状态  */
};

#define g_General (*CGeneral::instance())

#endif// __GENERAL_H__
