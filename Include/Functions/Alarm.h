

#ifndef __ALARM_H__
#define __ALARM_H__

#include "MultiTask/Thread.h"
#include "System/File.h"
#include "System/Signals.h"
#include "MultiTask/Timer.h"
#include "config-x.h"
#include "Configs/ConfigEvents.h"
#include "Configs/ConfigPTZ.h"
#include "System/AppEvent.h"
#include "Linkage.h"

#include "Configs/ConfigEvents.h"
#include "Devices/DevFrontboard.h"
class CDevAlarm;
class CDevMotionDetect;
class CDevLossDetect;
class CDevBlindDetect;


#define ALARMOUT_USER_MAX 128
typedef struct {
	int 	iEvent;						/*! 事件类型*/
	int		index;						/*! 序号 */
	int		iDelay[N_ALM_OUT];			/*! 各个报警输出通道延时, 0表示未开启, -1表示持续输出, >0表示需要继续输出的时间 */
	VD_BOOL	bUsed;						/*! 是否已经被使用*/
} ALARM_OUT_USER;

typedef enum _alarm_type
{
	TYPE_ALARM,
	TYPE_MOTION,
	TYPE_LOSS,
	TYPE_BLIND,
	TYPE_ALARM_PTZ,
	TYPE_NETALARM,
	TYPE_DECALARM,
	TYPE_NONE,//未定义
}ALART_TYPE;

enum _alarm_out_ctrl
{
	ALARM_OUT_CLOSED = 0,
	ALARM_OUT_AUTO,
	ALARM_OUT_MANUAL,
	ALARM_OUT_ALL
};

enum
{
	ALARM_CORRELATION_TIP = 1,
	ALARM_CORRELATION_OUTPUT,	
};

typedef enum _alarm_net_event_
{
	ALARM_NET_RETICLE = 0,		/*!< 网络断开 */ 
	ALARM_NET_ARP,				/*!< IP地址冲突 */
	ALARM_NET_ALL,				/*!< 全部事件 */
}ALARM_NET_EVENT;

typedef TSignal2<uint, int>::SigProc SIG_ALARM_BUFFER;
class CAlarmManager;
class CAlarm : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CAlarm);
	CAlarm();
	virtual ~CAlarm();
	VD_BOOL Start();
	VD_BOOL Stop();
	void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data = NULL);
	
	uint GetAlarmState();
	uint GetNetAlarmState();//add by yanjun 20061218
	uint GetAlarmPtzState(int index);
	uint GetMotionDetectState();
	uint GetLossDetectState();
	uint GetBlindDetectState();
	uint GetNetShowState(ALARM_NET_EVENT type     );

	void Alarm(uint dwState, int iEventType, char *pContext = NULL);
	void Motion(uint dwState);
	void Loss(uint dwState);
	void Blind(uint dwState);

#ifdef POLICE_PROJECT 
		void onTimerLed(int arg);
#endif


	VD_BOOL Attach(CObject * pObj, SIG_ALARM_BUFFER pProc);
	VD_BOOL Detach(CObject * pObj, SIG_ALARM_BUFFER pProc);
	
	VD_BOOL VD_SendMessage (uint msg, uint wpa = 0, uint lpa = 0, uint priority = 0);
	void SetAlarmOut(uint dwState, int iType = ALARM_OUT_AUTO);		// 直接设置报警输出
	uint GetAlarmOut();

	void Dump();

	uint 	GetAlarmOutType();
	void 	SetAlarmOutType(uint dwMod);
private:
	void ThreadProc();	
	void onConfigAlarm(CConfigAlarm& config, int& ret);
	void onConfigNetAlarm(CConfigNetAlarm &config, int &ret);
	void onConfigMotion(CConfigMotionDetect &config, int &ret);
	void onConfigLoss(CConfigLossDetect &config, int &ret);
	void onConfigDecAlm(CConfigDecoderAlarm &config, int &ret);

	void onConfigBlind(CConfigBlindDetect &config, int &ret);
	void onConfigNetAbort(CConfigNetAbort &config, int &ret);
	void onConfigNetArp(CConfigNetArp &config, int &ret);
	void GetReticleState(uint param);
	void VGAAlarmTip(uint param);/* VGA输入提示 */
	void GetNetState(int type);

private:
	CDevAlarm 		* m_pDevAlarm;
	CDevMotionDetect* m_pDevMotionDetect;
	CDevLossDetect	* m_pDevLossDetect;
	CDevBlindDetect	* m_pDevBlindDetect;
	
	uint m_dwAlarmState;							//当前外部报警状态掩码
	uint m_dwManualAlarmState;
	uint m_dwNetAlarmState;
	uint m_dwMotionDetectState;					//当前动态检测状态掩码	
	uint m_dwLossDetectState;						//当前视频丢失状态掩码	
	uint m_dwBlindDetectState;						//当前视频遮挡状态掩码	
	uint m_dwDecAlmState;

	uint m_dwAlarmPtzState[N_PTZ_ALARM];
	uint m_dwOldLossState;
	uint m_dwAlarmOutState;
	CLinkage m_cAlarmOutLinkage[N_ALM_OUT];				

	VD_BOOL  m_bNetState[ALARM_NET_ALL];
	CTimer m_cTimer;								// 用于处理各种延时操作的定时器
	CTimer m_cReticleTimer;
	CTimer m_cVGATipTimer;

	uint	m_dwAlarmOutType;						//报警输出模式
	int m_iAlarmLatchDelay[N_ALM_IN];				//外部报警输入延时时间
	uint m_iMotionLatchDelay[N_SYS_CH];				//动态检测输入延时时间
	int m_iBeepstate[N_SYS_CH];
	unsigned char m_ucVgaStd;
	VD_BOOL m_BVgaStdFlag;
    VD_BOOL m_BVGAAlarmTip;
        
	//外部报警配置
	CConfigAlarm		m_CCfgAlarm;
	//网络报警配置
	CConfigNetAlarm 	m_CCfgNetAlarm;
	//动态检测配置
	CConfigMotionDetect	m_CCfgMotion;
	//视频丢失配置
	CConfigLossDetect	m_CCfgLoss;
	//视频检测配置
	CConfigBlindDetect	m_CCfgBlind;
	//解码器警配置
	CConfigDecoderAlarm		m_CDecAlarm;	

#ifdef POLICE_PROJECT 
	CTimer m_cTimerLed;
#endif

	//网线连接状态
	CConfigNetAbort		m_CCfgNetAbort;	
	//IP地址冲突
	CConfigNetArp		m_CCfgNetArp;	

	CMutex	m_Mutex;										// 用来保存上述变量（临界资源）的互斥量
	TSignal2<uint, int> m_sigBuffer;						// 注册到报警模块的注册变量
public:
	CAlarmManager* m_alarmManager;					// 用来集中管理报警模块采集到的各种信号
};

class CAlarmManager : public CThread
{
public:
	
	CAlarmManager();
	virtual ~CAlarmManager();
	VD_BOOL Start();
	VD_BOOL Stop();
	
private:
	void ThreadProc();
	VD_BOOL Dispatch(VD_MSG &msg);
	VD_BOOL bAlarmOut;
};

#define g_Alarm (*CAlarm::instance())

#endif// __ALARM_H__


