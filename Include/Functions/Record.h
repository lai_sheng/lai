
#ifndef __RECORD_H__
#define __RECORD_H__

#include "System/File.h"
#include "MultiTask/Timer.h"
#include "Devices/DevCapture.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigRecord.h"
#include "System/AppEvent.h"

#include "Linkage.h"
#include "ExtRecord.h"

#include "Functions/FFFile.h"
#include "Devices/DevAudioIn.h"
#include "Net/Dlg/DlgNtpCli.h"

extern int g_RecordStatus;;

//定义录像模式
#define REC_SYS					0x0001		/*!< 系统预录事件	*/
#define REC_TIM					0x0002		/*!< 定时录像事件	*/
#define REC_MTD					0x0004		/*!< 动检录像事件	*/
#define REC_ALM					0x0008		/*!< 报警录像事件	*/
#define REC_CRD					0x0010		/*!< 卡号录像事件	*/
#define REC_MAN					0x0020		/*!< 手动录像事件	*/
#define REC_CLS					0x0040		/*!< 停止所有录像，并不能录像 */
#define REC_ALL					0x007e		/*!< 所有录像事件(不包括预录)	*/

//! 消息头结构
/*!
自定义消息头和消息内容组成一条消息，多条消息组成一帧消息，多帧消息组成了扩展用户数据。
消息头后面紧跟消息内容，完全以文本的形式写到文件中，甚至包含制表符，换行符。
每一帧消息结束时，录像模块生成一帧字符点阵叠加到图像。
*/
typedef struct {
	uint	Type;		/*!< 消息类型，用来生成对应的文本。 */
	uint	Length;		/*!< 消息内容的长度。				*/
	DHTIME	Time;		/*!< 消息记录的时间，GUI叠加时有用。*/
} CUSTOM_MESSAGE_HEADER;


//!WTN用的宏定义
enum record_control
{
	CLOSED_REC,						/*!< 禁止录像 */
	MANUAL_REC,						/*!< 手动录像 */
	AUTOMATIC_REC,					/*!< 自动录像 */	
	CONTROL_REC,
};

enum storageInfoTypes
{
	storageInfoCard = 0,
	storageInfoFrame = 1,
	storageInfoMotion = 2
};

enum recFileTypes
{
	recFileAudio = 0x01,
	recFileVideo = 0x02,
	recFileAV = recFileAudio | recFileVideo
};

//! 信息内容
typedef struct tagInfoGroup
{
	VD_BOOL bUsed;							/*!< 是否使用 FALSE:未使用　TRUE:使用*/
	int iStartPos;						/*!< 开始位置(绝对值) */
	int iLength;						/*!< 长度 */
}DHINFORGROUP;

//! 录像数据头信息，占用64字节
typedef struct tagDHFILEHEAD 
{
	uchar			DHHead[4];			/*!< 文件头标识 DHII*/
	int				iTotalLen;			/*!< 头所占的长度 */
	DHINFORGROUP	FrameIndex;			/*!< I帧定位信息 */
	DHINFORGROUP	MotionIndex;		/*!< 动检信息 */
	DHINFORGROUP	CardIndex;			/*!< 卡号信息 */
	uchar			Reserved[20];		/*!< 保留 */
}DHFILEHEAD;

class VD_IStorage : public CObject
{
public:
	VD_IStorage(void* p);
	virtual ~VD_IStorage();
	virtual VD_BOOL createStorage() = 0;
	virtual VD_BOOL closeStorage() = 0;
	virtual VD_BOOL writeStorage(void *pBuffer, uint dwCount) = 0;


	virtual uint getLength() = 0;
	virtual VD_BOOL setInfo(void* pContext, int iLength, storageInfoTypes type) = 0;

};

class CDHStorage : public VD_IStorage
{
public:
	CDHStorage(void* pAttr, int iChannel, enum capture_channel_t type);
	~CDHStorage();

	VD_BOOL createStorage();
	VD_BOOL closeStorage();
	VD_BOOL writeStorage(void* pBuffer, uint dwCount);

#ifdef RECORD_NET_THREAD_MULTI
	VD_BOOL writeStorage(CPacket *pPacket, uint dwCount, uint start_pos);
#endif	

	uint getLength();
	VD_BOOL setInfo(void* pContext, int iLength, storageInfoTypes type);

private:
	int					m_iChannel;
	STM_ATT*			m_pAttr;

	FFFile				m_File;

	CPacket*			m_pHeadPacket;

	CMutex				m_StorageMutex;
	
#ifdef RECORD_NET_THREAD_MULTI
#define DHSTORE_BUF_SIZE 	512*1024
	//!缓存录像有效数，一次写多个， 以避免录像锁老是被抢走，影响录像效率
	//!同时也为了达到，让录像能高抢到优先级
	PACKET_GROUP_RECORD	m_packet_group;
	//!当前packet group总共有效录像数据长度
	uint				m_pg_totoal_len;
#else
#define DHSTORE_BUF_SIZE 	32*1024
#endif

	uchar 				m_tmpBuf[DHSTORE_BUF_SIZE];
	uint 				m_tmpBuf_len;

	enum capture_channel_t  m_RecordType;
};

enum packetTypes
{
	packetNone = 0,			/*1< 不打包 */
	packetAuto = 1,			/*!< 自动打包 */
	packetForce = 2,		/*!< 强制打包 */
	packetChanged = 3,		/*!< 分辨率切换打包 */
	packetAdvanced = 4		///< 更高优先级别的录像打包
};

class CRecordStorage : public VD_IStorage
{
#ifdef RECORD_NET_THREAD_MULTI

	typedef struct PACKET_RECORD
	{
		CPacket* pPacket;
		uint dwStreamType;
	}PACKET_RECORD;
	typedef std::list<PACKET_RECORD> PACKET_LIST_RECORD;
#endif

public:
	CRecordStorage(int iChannel, enum capture_channel_t type, STM_ATT * pAttr, CMutex* pMutex);

	~CRecordStorage();

	VD_BOOL createStorage();
	VD_BOOL closeStorage();
	VD_BOOL writeStorage(void *pBuffer, uint dwCount);
	uint getLength();
	VD_BOOL setInfo(void* pContext, int iLength, storageInfoTypes type);

	void onRecord(int iChannel, uint dwStreamType, CPacket *pPacket);
	VD_INT32 OnAudioData(int iChannel, CPacket *pPacket);
	
	VD_BOOL start(VD_BOOL bPre);
	VD_BOOL stop(VD_BOOL bPre);
	VD_BOOL pack(packetTypes type = packetAuto);
	packetTypes getPackType() const;
	
#ifdef RECORD_NET_THREAD_MULTI
	VD_BOOL writePacket(CPacket *pPacket, uint dwCount, uint start_pos);
	bool DoPreview();
#endif
private:
	int								m_iChannel;	
	ICapture*						m_pDevCapture;
	CDHStorage*						m_pDhfsStorage;			/*!< 睿威录像存储类 */
	VD_IStorage*					m_pRemoteStorage;		/*!< 远程录像存储类，如FTP/SMB */
	VD_IStorage*					m_pDirectStorage;		/*!< 本地直接录像存储类，如USB直写 */
	packetTypes						m_bPacketType;
	VD_BOOL							m_bPre;
	CMutex*							m_pMutex;						///< 为了处理开录像和切换分辨率时的同步问题 
	VD_SIZE							m_imgSize;
	int								m_framerate;
    const enum capture_channel_t  	m_RecordType;
    const enum DataUsedType       	m_DataType;

#ifdef RECORD_NET_THREAD_MULTI
	void SaveRecord(int iChannel, uint dwStreamType, CPacket *pPacket);
	PACKET_LIST_RECORD m_packet_list;
	uint m_packet_count; //!主要实时更新m_packet_list的数量，这样判断数量可以不用进入锁里去
	CMutex	m_mutex_list;

	uint m_packet_audio_count;
	PACKET_LIST_RECORD m_packet_audio_list;
	CMutex	m_mutex_audio;
#endif

	
};


/*!
\class CRecord
\brief 通道录像对象
*/
class CRecord : public CObject
{
	friend class CRecordManager;

private:
	CRecord(int iChannel, enum capture_channel_t type);
	~CRecord();

	//! 开始录像
	VD_BOOL Start(uint arg);
	
	//! 停止录像
	VD_BOOL Stop(uint arg);
	
	//! 打包
	VD_BOOL Pack(packetTypes types = packetAuto);

	//! 获得录像模式
	uint getMode();

	//! 切换编码类型
	void switchEncodeType(uint arg);

	//! 检测开始条件
	int checkStart(uint arg);
	
	//! 检测停止条件
	int checkStop(uint arg);
	
	//! 设置录像文件信息
	void setFileInfo(const MEDIA_FORMAT& format);
	
	//! 设置录像文件类型
	void setFileType(uint rec_mode);
	
	
	//! 设置数据前一段数据
	VD_BOOL setInfo(void * pcontext, uint length, storageInfoTypes type);
	
	//! 设置冗余
	void setRedundancy(uchar redundancy);
	
	//! 得到录像的时间长度
	int getPacketLen(SYSTEM_TIME *pTime);
	
	//! 更新系统时间
	void updateSystemTime(SYSTEM_TIME *systime);

	int getFileLength();

	packetTypes getPackType() const;

	CRecordStorage	m_RecordStorage;		/*!< 录像基本功能类 */	
private:
	int				m_iChannel;				/*!< 录像通道号 */
	
	STM_ATT			m_Attr;					/*!< 录像文件信息 */
	

	uint			m_recMode;				/*!< 录像模式 */
	CMutex			m_RecordMutex;
	enum capture_channel_t m_Record_Type;
};

//录像对象的全局管理者
class CRecordManager : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CRecordManager);

	//! 构造函数
	CRecordManager();

	//! 析构函数
	~CRecordManager();

	//! 开启录像管理对象
	VD_BOOL Start();

	//! 关闭录像管理对象,停止一切录像（不改变预录状态）
	VD_BOOL Stop();

	//! 开始录像，供外部接口调用
	uint StartRec(uint arg, int channel = -1);

	//! 停止录像，供外部接口调用
	uint StopRec(uint arg, int channel = -1);

	//! 获得指定录像模式的录像状态
	uint GetState(uint arg = 0);

	//! 获得指定通道的录像模式
	uint GetRecMode(int iChannel);

	uint64 LoadRecordControl();

	void AppRecordControl(uint64 RecCtrl);
	
	VD_BOOL SaveRecordControl(uint64 RecCtrl, VD_BOOL bLogSave);
	
	//! 时间跳变
	void UpdateSystemTime(SYSTEM_TIME *systime);

	//! 检测开录的条件
	int CheckLimit(uint arg, int iChannel);


	//! 设置数据前一段数据
	VD_BOOL setInfo(void * pcontext, uint length, storageInfoTypes type, int iChannel);

	//! 设置是否允许打包
	void pack(packetTypes types, int iChannel);

	int getLength(int channel);

	packetTypes getPackType(int channel) const;


#ifdef RECORD_NET_THREAD_MULTI

	void OnTimer(uint arg);       //定时刷新标题和时间回调函数
#endif

        /* 手动停止预录 */
	void StopPreRecord();
	void SetNtpSuccess(){m_iNtpSuccess = 1;}

private:
	//! 线程的执行体, 主要代替定时器的任务
	void ThreadProc();

	//! 凌晨更新时操作
	void dateChanged(SYSTEM_TIME *current_time);
	
	//! 定时打包或开启录像
	void timerWork(SYSTEM_TIME *pTime);
	
	//! 定时打包
	void timerPack(SYSTEM_TIME *pTime, int iChannel);
	
	//! 检测时间段的有效性
	VD_BOOL CheckSection(uint arg, int iChannel);
	//!转换动检和报警时间表的使能值,flag=0转换动检，1转换报警
	void ConvertState(CONFIG_WORKSHEET* psheet, const CONFIG_RECSTATE* pState, VD_BOOL type);
	//! 检测CMOS的限制条件
	VD_BOOL CheckCmos(int iChannel);

	//! 应用指定通道定时录像配置信息
	void AppRecordConfig(CONFIG_RECORD *pConfig, int iChannel);
	
  //! 验证CMOS 的值 的合法性
  int ValidateCMOS(uint64 *dwCMOSValue);

#ifdef ENC_ENCODE_MULTI    
	void OnExtTimer(uint arg);
#endif
	//! General配置变化回调函数
	void onConfigGeneral(CConfigGeneral& cCfgGeneral, int& ret);

	//! Encode配置变化时
	void onConfigEncode(CConfigEncode& cCfgEncode, int& iRet);

	//! 录像配置变化时
	void onConfigRecord(CConfigRecord& cCfgRecord, int& iRet);
	
	//! 联动处理
	void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* pData);

    //!批量处理通道录像的开始及停止
	uint StartPair(int chn, uint arg);
	uint StopPair(int chn, uint arg);
	VD_BOOL PackPair(int chn, packetTypes types = packetAuto);
	uint getModePair(int chn);
	int getPacketLenPair(int chn, SYSTEM_TIME *pTime);
private:
	CConfigGeneral	m_cCfgGeneral;			/*!< 普通配置类 */
	CConfigEncode	m_cCfgEncode;			/*!< 编码配置类 */
	CConfigRecord	m_cCfgRecord;			/*!< 录像配置类 */

	CRecord*		m_pRecord[N_SYS_CH];				/*!< 通道对象 */
	CLinkage		m_cAlarmLinkage[N_SYS_CH];
	CLinkage		m_cMotionLinkage[N_SYS_CH];

	CMutex			m_RecordCtrlMutex;					/*!< 录像开关锁 */
	
	SYSTEM_TIME		m_SystemTime;					/*!< 系统时间 */


	uint64			m_RecControl;					/*!< 录像控制，从CMOS中获得 */

	VD_BOOL			m_bAllowPack[N_SYS_CH];

#ifdef ENC_ENCODE_MULTI
	CExtRecord		*m_pExtRecord[N_SYS_CH];
	CAPTURE_DSPINFO m_DSPInfo;
	CTimer					m_ExtTimer;
#endif		

#ifdef RECORD_NET_THREAD_MULTI
	CTimer	m_RecordTimer;
#endif
	int   m_iNtpSuccess; //1: NTP时间同步成功;
public:
	int TimerStartRec(int arg);

private:
	int m_SuspendMode;
};

#define g_Record (*CRecordManager::instance())

#endif// __RECORD_H__


