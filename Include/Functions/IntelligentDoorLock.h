﻿#ifndef INTELLIGENTDOORLOCK_H_
#define INTELLIGENTDOORLOCK_H_
#include "APIs/ExtDev.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"
#include "APIs/Types.h"

#include "APIs/LockApi.h"
#include "Configs/ConfigDoorLock.h"

#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"

#include "Functions/FFFile.h"
#include "Devices/DevExternal.h"
#include "Devices/DevInfo.h"
#include "APIs/Audio.h"

#define LEAVEMESSAGE_DIR		"/mnt/music"
#define LEAVEMESSAGE_TEMP_DIR	"/tmp/music"

typedef struct{
	int		id;
	char 	downUrl[1024];
	LockApiAVInfo stAvInfo; //适用于X1000系列门锁
}AudioDownInfo;

using namespace std;
class CDoorLockManager : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(CDoorLockManager);
	
	CDoorLockManager();
	virtual ~CDoorLockManager();

	VD_BOOL Start();
	
	VD_BOOL Stop();

	int DownLoadFile(AudioDownInfo stDownInfo);
	int PushDownInfo(AudioDownInfo stDownInfo);
	
	VD_BOOL SyncLockTime();
	VD_BOOL PlayWarnVoice(void);

	int ProcessRemainLockMsg();
#ifdef X1000_LOCK	
    void ProcessLockMsgRecv(lockMsgInfo *pRcvMsg);
    int GetUserInfoList(RT_LockUserList * pUserInfo);
#endif
private:
	VD_BOOL AddUserOpenType(int UserNum,CONFIG_OPENTYPE OpenType);	
	VD_BOOL DelUserOpenType(int UserNum,CONFIG_OPENTYPE OpenType);	
	VD_BOOL DelAllUser();

	VD_BOOL PlayLeaveMessage(int UserNum,CONFIG_OPENTYPE OpenType);
    
	void onConfigLockLeaveMsg(CConfigLockLeaveMsg& cCfg, int& ret);
	void onConfigLockWarnVoice(CConfigLockWarnVoice& cCfg, int& ret);
		
	void ThreadProc();	
private:
	CTimer 	m_lockTimer;
	typedef std::list<CPacket *> LockMsg_List;

	LockMsg_List m_lockList;
	uint 	m_lock_count;
	CMutex	m_lock_mutex;

	int QueryLockTimer(int arg);
	int ProcessLockMsg();
	int m_alarm_msg_count;
	VD_BOOL m_lock_awake_flg;  //????????????
	VD_BOOL m_lock_gettoken_flg; //?????????????

	CConfigLockUserInfo m_LockUserInfo;
	CConfigLockLeaveMsg m_LockLeaveMsg;	
    CConfigLockWarnVoice m_LockWarnVoice;
    CConfigLockMsgCache m_LockMsgCache;

	typedef std::list<AudioDownInfo> readyToDown_List;
	readyToDown_List m_downList;
	uint			 m_down_count;
	CMutex			 m_down_mutex;

	int ProcessDownInfo();
	int PushLockMsgCache(RT_LockMsgInfo LockInfo);
	int PopLockMsgCache();

	VD_BOOL SendTidTokenToLock();
private:
	CConfigNetNTP m_cCfgNtp;	
	VD_BOOL	ntpInfoUpdate;
	void OnConfigNtp(CConfigNetNTP& cfg_in_tmp, int &ret);	
};

#define g_DoorLockManager (*CDoorLockManager::instance())
#endif
