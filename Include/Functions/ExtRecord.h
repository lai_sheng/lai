
#ifndef __EXT_RECORD_H__
#define __EXT_RECORD_H__

#include "Functions/FFFile.h"
#include "MultiTask/Timer.h"
#include "System/File.h"
#include "Devices/DeviceManager.h"
#include "Devices/DevCapture.h"

class CExtRecord : public CObject
{
public:
	//! 构造函数，完成一些初始化工作
	CExtRecord(int iChannel);

	//! 析构函数
	~CExtRecord();

	//! 开始录像 
	VD_BOOL Start();
	
	//! 关闭录像
	VD_BOOL Stop();
	
	void setRecLen(uchar length);
	
	VD_BOOL Pack(VD_BOOL now);

private:
	//! 录像的回调函数
	void OnExtRecord(int iChannel, uint dwType, CPacket *pPacket);

	VD_BOOL CheckDisk();

private:
	int				m_iChannel;			/*!< 通道号 */
	CDevCapture *	m_pDevCapture;		/*!< Capture设备 */
//	CDHFile			m_DHFile;			/*!< 文件类　*/
	CMutex			m_RecMutex;			/*!< 录像的线程锁 */
	STM_ATT			m_Attr;				/*!< 文件系统属性 */

	uint			m_dwChannel;
	VD_BOOL			m_State;
	
	uchar 		m_RecLen;
};

#endif// __RECORD_H__

