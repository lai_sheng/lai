

#ifndef __AUTOMAINTAIN_H__
#define __AUTOMAINTAIN_H__


#include "System/File.h"
#include "MultiTask/Timer.h"
#include "Configs/ConfigAutoMaintain.h"

//自动维护各项值说明：
enum auto_reboot_day 
{
	DAY_NERVER = 0,		//从不
	EVERYDAY,			//每天
	SUNDAY,			  //周日
	MONDAY,       //周一
	TUESDAY,      //周二
	WEDNESDAY,    //周三
	THURSDAY,     //周四
	FRIDAY,       //周五
	SATURDAY     //周六
};

enum auto_delete_file_time
{
	TIME_NERVER = 0,	  //从不
	ONE_DAY,      //24小时
	TWO_DAY,      //48小时
	THREE_DAY,    //72小时
	FOUR_DAY,     //96小时
	ONE_WEEK,     //一周
	ONE_MONTH		//一个月
};

enum maintain_type
{
	MAINTAIN_REBOOT,
	MAINTAIN_DELETE_FILES,

	MAINTAIN_TYPES,
};

char * const AUTO_VERSION="0308";

#define AUTO_CUR_VERSION ((uint)((uint)'A'<<24 | (uint)'T'<<16 | (uint)'O'<<8 | (uint)AUTO_VERSION))

//struct CONFIG_AUTO
//{
//	VER_CONFIG verConfig;		/*! 配置文件版本信息 */
//	int		AutoRebootDay;		/*! 0-从不, 1-每天, 2-每星期日, 3-每星期一,..... */
//	int		AutoRebootTime;		/*! 0-0:00 1-1:00,........23-:23:00 */
//	int		AutoDeleteFilesTime;	/*! 0-从不, 1-24H, 2-48H, 3-72H, 4-96H, 5-ONE WEEK, 6-ONE MONTH */
//};

typedef void (CObject:: * MAINTAINPROC)(uint wParam);

class CAutoMaintain : CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CAutoMaintain);
	CAutoMaintain();
	~CAutoMaintain();
	void Start();
//	BOOL SaveAutoConfig(CONFIG_AUTO * pconAuto);
//	BOOL LoadAutoConfig(CONFIG_AUTO * pconAuto);
	void OnConfigAutoMaintain(CConfigAutoMaintain& config, int& ret);
private:
	void OnReboot(uint par);
	void OnTimer(uint par);
//	BOOL ReadConfig();
	VD_BOOL IsMaintainNow(int iMaintainType);
//	void OnConChanged();

protected:
private:
	MAINTAINPROC m_pfnMaintain[MAINTAIN_TYPES];
	SYSTEM_TIME m_LastTime[MAINTAIN_TYPES];
	CTimer m_timerAuto;
	CFile m_fileAutoConfig;
	//CONFIG_AUTO m_conAuto;
	//CONFIG_AUTO m_conDefaultAuto;
	CConfigAutoMaintain m_ConfigAutoMaintain;
	CMutex m_mutexCout;
};

#define g_AutoMaintain (*CAutoMaintain::instance())

#endif
