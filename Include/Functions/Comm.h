

#ifndef __COMM_H__
#define __COMM_H__

#include "System/File.h"
#include "Comm_Base.h"
#include "System/Console.h"
#include "System/Log.h"
#include "Configs/ConfigComm.h"
#define COM_NAME_LENGTH 30

//char * const COMM_VERSION = "0601";
//char * const COMM_ATM_VERSION = "0301";

class CComm : public CObject
{
public:
	typedef TSignal2<uint, int>::SigProc SIG_COM_BUFFER;
	PATTERN_SINGLETON_DECLARE(CComm);
	CComm();
	virtual ~CComm();
	VD_BOOL Start();
	
	int GetProtocolCount();
	char * GetProtocolName(int Index);
	uint GetProtocolMask();

	VD_BOOL Attach(CObject * pObj, SIG_COM_BUFFER pProc);
	VD_BOOL Detach(CObject * pObj, SIG_COM_BUFFER pProc);
	void SendAlarm(uint mask, int type);

	//! 获得串口工作状态
	VD_BOOL GetCommState();


	void ResetComm();

	CCommBase * GetWorkingComm();

private:
	void CreateProduct();

	//! 应用串口数据
	void onConfigComm(CConfigComm *pConfig, int& ret);

private:
	CDevComm  *	m_pDevComm;
	
	CConfigComm m_ccCfgCom;
    TSignal2<uint, int>	m_sigBuffer;			/*!< 回调函数 */

	CCommBase				*m_pCommObj[COM_TYPES];
	uint					m_dwProtocolMask;

	/*存放串口协议名字, 限每个名字最长30字节*/
	char m_protocol_name_list[COM_TYPES][COM_NAME_LENGTH];

};

#define g_Comm (*CComm::instance())

#endif// __COMM_H__
