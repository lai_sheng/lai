
#ifndef __COMM_BASE_H__
#define __COMM_BASE_H__

#include "AuxLibs/Lua/ScriptEngine.h"
#include "Devices/DevComm.h"
#include "MultiTask/Timer.h"

#include "System/AppEvent.h"

extern int g_nCapture;


enum channel_recd_type
{
	OPEN_INSURANCE_DOOR_RECD = 0xe0,
	CLOSE_INSURANCE_DOOR_RECD = 0xe1,
	MONEY_OUT_NOTICE_RECD= 0xf1,
	MONEY_OUT_SUCCESS_RECD=0xf2,
};

enum sysstateinfo
{
	CAMERA_STATE = 0,
	ALARM_STATE = 16,
	ENVIROMENT_ALARM = 17,
	HDD_STATE = 19,
	RECD_STATE = 20,
	ALL_STATE_LENGTH = 32
};

enum dvrstateinfo
{
	NO_ALARM = 0,
	SYS_UNSTABLE_ALARM = 1,	//!系统不稳定报警
	DISK_ERROR_ALARM, 		//!硬盘满或者硬盘错误报警
	OTHER_ALARM, 	//!其他情况
	SMOKE_ALARM ,	//!烟雾报警
	FRAG_ALARM ,	//!破碎报警
	CONCUSS_ALARM , //!震动报警
	DISK_FULL_ALARM, //!硬盘满报警
	
	STATE_NORMAL = 0xa0,
	STATE_BLIND ,
	STATE_RECORDING ,
	STATE_VIDEOLOSS ,
};

enum LuaDataType
{
	NIL_TYPE = 0,
	NUMBER_TYPE,
	C_STRING_TYPE,
	BOOLEAN_TYPE,
	COM_TYPE_NUM
};

class CCommBase : public CObject
{
public:
	CCommBase();
	virtual ~CCommBase();
	virtual void OnData(void *pdat, int len);
	virtual void OnNoneData();

	virtual int KeyParse(uchar *pBuffer);

	virtual int Start(CObject * pObj, CDevComm::SIG_DEV_COMM_DATA Proc);
	virtual int Start();
	virtual int Stop();

	VD_BOOL GetState();

	static uint	m_DisconTime;					/*!< 串口不发数据认为断的时间，以秒为单位 */
private:

	void OnDetectData(uint arg);

	//事件中心处理函数
	virtual void onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data);
	
protected:
	CDevComm		*m_pDevComm;
				
	

	static VD_BOOL		m_bBroken;

	uchar			m_byBuffer[1024];
	int				m_iLen;


private:
	static int      m_ReadTimes;			/*!< 1秒内读数据的次数*/
	static CTimer	m_Timer;				/*!< 用于检测串口是否有数据 */	
	static uint	m_ContinuousTime;		/*!< 持续的时间 */ 

};

#endif

