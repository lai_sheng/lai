#ifndef __JPEG__H
#define __JPEG__H

#include "System/BaseTypedef.h"

#define LABELUNITLENGTH 2

typedef struct rtp_packet_header_s
{
#if (BYTE_ORDER == LITTLE_ENDIAN)
	VD_UINT16 cc      :4;   // CSRC count 
	VD_UINT16 x       :1;   // header extension flag 
	VD_UINT16 p       :1;   // padding flag 
	VD_UINT16 version :2;   // protocol version 
	// byte 1 
	VD_UINT16 pt      :7;   // payload type 
	VD_UINT16 marker  :1;   // marker bit 
#elif (BYTE_ORDER == BIG_ENDIAN)
	VD_UINT16 version :2;   // protocol version 
	VD_UINT16 p       :1;   // padding flag 
	VD_UINT16 x       :1;   // header extension flag 
	VD_UINT16 cc      :4;   // CSRC count 
	//byte 1
	VD_UINT16 marker  :1;   // marker bit 
	VD_UINT16 pt      :7;   // payload type 
#else
	#error YOU MUST DEFINE BYTE_ORDER == LITTLE_ENDIAN OR BIG_ENDIAN !  
#endif
	//bytes 2, 3 
	VD_UINT16 seqno;   // sequence number 
	// bytes 4-7 
	VD_UINT32 ts;           // timestamp in ms 
	// bytes 8-11 
	VD_UINT32 ssrc;         // synchronization source 
} rtp_packet_header_t;


//rtp jpeg头
typedef struct jpeg_hdr_s 
{
#if (BYTE_ORDER == LITTLE_ENDIAN)
    VD_UINT32 off:24;    /* fragment byte offset */ 
    VD_UINT32 tspec:8;   /* type-specific field */
#elif (BYTE_ORDER == BIG_ENDIAN)
    VD_UINT32 tspec:8;   /* type-specific field */
    VD_UINT32 off:24;    /* fragment byte offset */ 
#else
	#error YOU MUST DEFINE BYTE_ORDER == LITTLE_ENDIAN OR BIG_ENDIAN !  
#endif
    VD_UINT8 type;            /* id of jpeg decoder params */
    VD_UINT8 q;               /* quantization factor (or table id) */
    VD_UINT8 width;           /* frame width in 8 pixel blocks */
    VD_UINT8 height;          /* frame height in 8 pixel blocks */
}jpeg_hdr_t;

//复位标记头
typedef struct jpeg_hdrrst_s 
{
    VD_UINT16 dri;
#if (BYTE_ORDER == LITTLE_ENDIAN)
	VD_UINT16 count   :14;
	VD_UINT16 l        :1;   
	VD_UINT16 f        :1;  
#elif (BYTE_ORDER == BIG_ENDIAN)
	VD_UINT16 f        :1;   
	VD_UINT16 l        :1;  
	VD_UINT16 count   :14; 
#else
#error YOU MUST DEFINE BYTE_ORDER == LITTLE_ENDIAN OR BIG_ENDIAN !  
#endif       
}jpeg_hdrrst_t;


//量化表头
typedef struct jpeg_hdrqtable_s 
{
    VD_UINT8  mbz;
    VD_UINT8  precision;
    VD_UINT16 length;
}jpeg_hdrqtable_t;


typedef struct jpeg_sndhdr_s
{
	jpeg_hdr_t m_JpegHead;
	jpeg_hdrrst_t m_JpegRstHead;
	jpeg_hdrqtable_t m_JpegQHead;
	VD_UINT8 uch_qtable[128]; 
	VD_UINT8 uch_haftable[0x1A4];
	
}jpeg_sndhdr_t;

struct rtp_jpeg 
{
	int type;
	int q;
	int width;
	int height;
	int luma_table;
	int chroma_table;
	unsigned char *quant[16];
	unsigned char *scan_data;
	int scan_data_len;
	int init_done;
	int ts_incr;
    char dqt[128];
    char * dht;
    int dht_len;
    
	unsigned int timestamp;
	jpeg_hdrrst_t rsth;
};

#define RTP_JPEG_HEADLENGTH (sizeof(rtp_packet_header_t) + sizeof(jpeg_hdr_t) + sizeof(jpeg_hdrrst_t))
#define RTP_JPEG_FIRSTHEADLENGTH (RTP_JPEG_HEADLENGTH + sizeof(jpeg_hdrqtable_t) + 128)


#define PUT_16(p,v) ((p)[0]=((v)>>8)&0xff,(p)[1]=(v)&0xff)
#define PUT_32(p,v) ((p)[0]=((v)>>24)&0xff,(p)[1]=((v)>>16)&0xff,(p)[2]=((v)>>8)&0xff,(p)[3]=(v)&0xff)
#define GET_16(p) (((p)[0]<<8)|(p)[1])
#define GET_32(p) (((p)[0]<<24)|((p)[1]<<16)|((p)[2]<<8)|(p)[3])

#define JPEG_MAX_PAYLOAD_LEN 1440

struct frame 
{
    char *d;
    int length;
};

//void rtp_jpeg_init(void * p);

//int InitRtpJpegHead(jpeg_sndhdr_t *pRTPJHdr, unsigned short _width, unsigned short _height);

//void jpeg_packet_rtp(void *_pMedia, char *_pInputBuffer, int _iLength);

#endif


