#ifndef _MP3PLAY_H
#define _MP3PLAY_H


#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <time.h>

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Devices/DevCapture.h"
#include "System/ABuffer.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/Packet.h"

#include "Configs/ConfigCamera.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Functions/HMSOTA/curl/curl.h"


enum {
    MP3_REQ_OK = 0,
	MP3_REQ_FAIL = 1, 
};


class MP3Play : public CThread
{
public:
	PATTERN_SINGLETON_DECLARE(MP3Play);
	MP3Play();
	~MP3Play();
public:
	VD_INT32 Init();
	VD_INT32 Start();
	VD_INT32 Stop();
	void ThreadProc();
	VOID SetMP3Url(std::string url){m_iDownLoadStatus = 1;m_imp3url = url;}
	VD_INT32 DownLoadMp3File();
	VOID SetMP3PlayStatus(int status);
//m_iPlayStatus : 0  为未播放  1：下载中  2：播放失败超时
//3: 播放中  4：播放结束	
	int GetMP3PlayStatus(char url[1024]);

	int MP3PlayPause();	
	int MP3PlayResume();		 

	int SetVoicePlayVolume();
	int ResumeVoiceDefaultVolume();

	int SetMP3PlayVoiceVolume(int percent);
	int GetMP3PlayVoiceVolume();

	void SetMp3PlayCurPriority(int priority){m_iPriority = priority;};
	int  GetMp3PlayCurPriority(){return m_iPriority;};
private:
	int m_iPlayStatus;
	int m_iDownLoadStatus;

	int m_iVolume;		  //播放音乐时的声音大小
	int m_iDefaultVolume; //系统默认声音大小

	int m_iPriority;	  //当前音乐播放优先级
	std::string m_imp3url;
};

#define g_MP3Play (*MP3Play::instance())


#endif
