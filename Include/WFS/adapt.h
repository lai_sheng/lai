
#ifndef __FS_ADAPT_H_
#define __FS_ADAPT_H_

#include <stdio.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef FILE_INFO_DEFINED
/// \struct FileInfo
/// \brief 文件信息结构
typedef struct _FileInfo
{
	char	name[108];		///< 文件名(短文件名，减少体积)
	uint	attrib;			///< 文件属性
	uint64	time_create;	///< 文件创建时间
	uint64	time_access;	///< 文件访问时间
	uint64	time_write;		///< 文件修改时间
	uint64	size;			///< 文件大小
}FileInfo;
#define FILE_INFO_DEFINED
#endif

/// 文件系统分区结构
typedef struct _PARTITION_INFO
{
	uint64 start_sector;		///< 分区开始扇区
	uint64 total_sector;		///< 分区扇区数
}PARTITION_INFO;

/// 文件系统初始化结构
typedef struct DHFS_INIT_INFO		//多任务有关的回调函数
{
	void (*wakup)(uint disk_no);	//唤醒回调函数
	void (*lock)();					//锁回调函数
	void (*unlock)();				//解锁回调函数
	int (*recover)(uchar *, uint *, int); // 文件恢复
}DHFS_INIT_INFO;


#ifdef __cplusplus
}
#endif

#endif //__FS_ADAPT_H_

