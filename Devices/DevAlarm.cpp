#include "Devices/DevAlarm.h"

extern int g_nAlarmOut;

CDevAlarm* CDevAlarm::_instance = NULL; 

CDevAlarm* CDevAlarm::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevAlarm();
	}
	return _instance;
}

CDevAlarm::CDevAlarm()
{	
	AlarmCreate();
	m_dwOutState = 0;
	AlarmWrite(&m_dwOutState);
}

CDevAlarm::~CDevAlarm()
{
	AlarmDestory();
}



int CDevAlarm::GetInSlots ()
{
	return AlarmGetInSlots();
}

int CDevAlarm::GetOutSlots()
{
	return AlarmGetOutSlots();
}

uint CDevAlarm::GetInState()
{
	m_dwInState = 0;
	AlarmRead(&m_dwInState);
	return m_dwInState;
}

uint CDevAlarm::GetResetState()
{
	m_dwResetState = 0;
	
	ResetRead(&m_dwResetState);
	
	return m_dwResetState;
}

uint CDevAlarm::GetOutState()
{
	return m_dwOutState;
}

void CDevAlarm::SetOutState(uint dwState)
{
	m_dwOutState = dwState;
	AlarmWrite(&m_dwOutState);	
}

void CDevAlarm::AlarmOut(uint dwChannel, VD_BOOL bOpen)	// 启动报警输出，slot是报警输出各通道的掩码
{
/************************************************************************
	启动报警输出：
	1、根据要启动的端口，设置报警输出掩码；
	2、调用写API函数即OUT；
************************************************************************/
	uint dwSlot = m_dwOutState;

	if (bOpen)
	{
		dwSlot |= BITMSK(dwChannel);
	}
	else
	{
		dwSlot &= ~BITMSK(dwChannel);
	}
	if (dwSlot != m_dwOutState)
	{
		m_dwOutState = dwSlot;
		AlarmWrite(&m_dwOutState);
	}
}

