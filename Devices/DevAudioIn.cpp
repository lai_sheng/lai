
#include "Devices/DevAudioIn.h"
#include "System/Packet.h"

#define TIMEOUT 100

#define PACKET_SIZE 2*1024

extern int g_nAudioIn;

CAudioInManager* CDevAudioIn::m_pManager = NULL;
CDevAudioIn* CDevAudioIn::_instance[N_AUDIO_IN] = {NULL, };

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CDevAudioIn* CDevAudioIn::instance(int iChannle)  //默认的音频格式是G711a 
{
	if(NULL == _instance[iChannle])
	{
		_instance[iChannle] = new CDevAudioIn(iChannle);
	}
	return _instance[iChannle];
}

CDevAudioIn::CDevAudioIn(int iChannel) : m_sigBuffer(32*16)
{
	m_dwBytes = 0;
	m_dwBytesOld = 0;
	m_dwBytesMask = 0;
	m_iChannel = iChannel;

//通过编译选项指定音频格式
#ifdef AUDIO_TYPE_AAC
	m_AudioType = AudioType_AAC;
#else
	m_AudioType = AudioType_G711A;
#endif

#if defined (CHIP_S2L) || defined(BPI_S2L)
	AudioEncodeType(m_AudioType);
#endif

	AudioInCreate(m_iChannel);

	if(NULL == m_pManager)
	{
		m_pManager = new CAudioInManager;
	}

	m_pManager->Init(iChannel, this);
}

CDevAudioIn::~CDevAudioIn()
{
	AudioInDestroy(m_iChannel);
}

int CDevAudioIn::GetChannels ()
{
	return AudioInGetChannels();
}

VD_BOOL CDevAudioIn::GetCaps(AUDIOIN_CAPS * pCaps)
{
	if (!AudioInGetCaps(pCaps)) 
	{
		return TRUE;
	}

	return FALSE;
}

int CDevAudioIn::SetFormat(int iChannel, AUDIOIN_FORMAT * pFormat)
{
	return AudioInSetFormat(iChannel, pFormat);
}

int CDevAudioIn::GetFormats(AUDIOIN_FORMAT* pFormat, int iMax)
{
	memset(pFormat, 0, sizeof(AUDIOIN_FORMAT) * iMax);
#if  defined(DVR_HI)
	return AudioGetFormat(pFormat, iMax);
#else
	return 0;
#endif
}
void CDevAudioIn::SetSuspend(int suspend)
{
	if(NULL != m_pManager){
		m_pManager->SetSuspend(suspend);
	}
}
void  CDevAudioIn::SetMicEanble(int status)
{
	if(NULL != m_pManager){
		m_pManager->SetMicEnable(status);
	}
}
VD_BOOL CDevAudioIn::Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc)
{
/************************************************************************
	开始一个捕获设备：
	1、把启动该捕获设备的对象注册到本设备上；
	2、如果注册成功，则调用CaptureStart函数来启动物理上的一个捕获设备，
	   否则直接返回FALSE；
	3、如果物理设备启动成功，则启动本捕获设备对象的线程；
	   （如果2、3对换的话，那么在本对象的线程回调函数中的CaptureGetBuffer
	   函数将返回FALSE）
	4、如果3执行成功，则开启一个用于计算码流的定时器；
************************************************************************/
	int slots = m_sigBuffer.Attach(pObj, pProc);

	if(slots < 0)
	{
		trace("CDevAudioIn::Start Attach failed(%d)\n", slots);
		return FALSE;
	}
	if(slots == 1)
	{
		if (AudioInStart(m_iChannel) >= 0) 
		{
			m_pManager->Start();
			return TRUE;
		}
		else
		{
			trace("Channel = %d, CDevAudioIn::Start CaptureStart failed\n", m_iChannel);

			slots = m_sigBuffer.Detach(pObj, pProc);
			if (slots < 0)
			{
				trace("Detach Failed\n");
			}

			return FALSE;
		}
	}

	return TRUE;
}

VD_BOOL CDevAudioIn::Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc)
{
/************************************************************************
	停止一个捕获设备：
	1、把停止该捕获设备的对象注销掉；
	4、如果1返回0，则停止用于计算码流的定时器；
	2、如果1返回0，则调用DestroyThread函数来停止捕获设备的线程；
	3、如果2调用成功，则调用CaptureStop函数来停止一个物理的捕获设备；
	5、如果3调用成功，则返回TRUE，否则返回FALSE；
	6、如果1返回值大于0，表示还有其它对象注册在该捕获设备对象上，此时直接返回TRUE；
************************************************************************/
	int slots = m_sigBuffer.Detach(pObj, pProc);

	if(slots < 0)
	{
		trace("CDevAudioIn::Stop Detach failed\n");
		return FALSE;
	}

	if(0 == slots)
	{
		if (!AudioInStop(m_iChannel)) 
		{
			m_pManager->Stop();
			return TRUE;
		}

		trace("CDevAudioIn::Stop AudioInStop failed\n");
		return FALSE;
	}

	return TRUE;	// 6
}

void CDevAudioIn::OnData(CPacket * pPacket)
{
	m_dwBytes += pPacket->GetLength();

	m_sigBuffer(m_iChannel, pPacket);
}

/********************************************************************************************************************
	CAudioInManager
********************************************************************************************************************/
void CAudioInManager::ThreadProc()
{
	CPacket *pPacket;
	uchar *pBuffer;
	uint length;
	int iChannel;

	SET_THREAD_NAME("AudioInGetBuffer");
/************************************************************************
	捕获设备线程的消息回调函数：
	1、调用AudioInGetBuffer函数来获取一块CPacket对象指针；
	2、如果1中函数调用成功，则调用注册到捕获设备的回调函数；
	3、记录好本次获取packet的长度，该长度用于计算码流；
	4、释放由AudioInGetBuffer函数申请的packet内存空间；
	5、当线程回调函数循环控制变量m_bLoop为FALSE时，退出循环体，
	   否则循环执行1、2、3、4；
************************************************************************/
	int  printcount = 0;
	while (m_bLoop) 
	{
		pPacket = g_PacketManager.GetPacket(PACKET_SIZE);
		if (NULL == pPacket)
		{
			trace("Ex -- GetPacket failed.\n");
			SystemUSleep(3000);
			continue;
		}
		//assert(pPacket);

		pBuffer 	= pPacket->GetBuffer();
		length 		= pPacket->GetSize();
		iChannel 	= AudioInGetBuffer(-1, pBuffer, &length, TIMEOUT);

		if(iChannel < 0
			|| iChannel >= g_nAudioIn)
		{
			//trace("CDevAudioIn::ThreadProc() CaptureGetBuffer failed\n");
			pPacket->Release();
			SystemUSleep(3000);
			continue;
		}
		pPacket->SetLength(length);
		if(length > pPacket->GetSize())
		{
			_printd("audio buffer out of bound!!!");
		}
		if(m_iSuspend){
			if(printcount++ > 300){
				printcount = 0;	
				_printd("The system is suspend");
			}
			pPacket->Release(); 
			continue;
		}	
		if(m_iMicEnable)
		{//麦克风开关  1：开  0：关
			m_pDevCap[iChannel]->OnData(pPacket);
		}
		pPacket->Release();
		if(printcount++ > 500)
		{
		 	printcount = 0;	
			_printd("[%d]audio get data!!!",m_iMicEnable);
		}
	}
}


CAudioInManager::CAudioInManager() : CThread("AudioInManager", TP_CAPTURE)
{
	m_iUser = 0;
	m_iMicEnable = 1;
	m_iSuspend = 0;
}

CAudioInManager::~CAudioInManager()
{
}

void CAudioInManager::Init(int iChannel, CDevAudioIn * pDevCap)
{
	CGuard guard(m_Mutex);
	m_pDevCap[iChannel] = pDevCap;
	if (AudioInStart(iChannel) >= 0 && m_iUser <= 0) {
		m_iUser = 1;
		CreateThread();
	}
}

void CAudioInManager::Start()
{
	CGuard guard(m_Mutex);
	if (m_iUser <= 0)
	{
		m_iUser = 0;
		CreateThread();
	}
	m_iUser++;
}

void CAudioInManager::Stop()
{
	CGuard guard(m_Mutex);
	m_iUser--;
	if (m_iUser == 0)//0   //hyd
	{
		DestroyThread();
	}else if(m_iUser < 0){//0
		m_iUser = 0;
	}
}
void CAudioInManager::SetSuspend(int suspend)
{
	if(suspend > 1 || suspend < 0){
		m_iSuspend = 0; 
		_printd("the val of suspend is unnormal");
	}
	else{
		m_iSuspend = suspend;
	}

	_printd("set val of suspend is %d",m_iSuspend);
}
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

