#include <stdio.h>
#include <error.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include "Functions/Record.h"
#include "Net/Dlg/DlgNtpCli.h"
#include "System/BaseTypedef.h"
#include "Devices/DevExternal.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"
#include "APIs/CommonBSP_Battery.h"
#include "Functions/IntelligentDoorLock.h"

PATTERN_SINGLETON_IMPLEMENT(CDevExternal);

extern int g_RecordStatus;

CDevExternal::CDevExternal():CThread("CDevExternal",TP_EXT),m_DevExtTimer("DevExtTimer")
{
	SockFd       = -1;
	EnableSleep  = 1;
	CurRunCnt	 = 0;
	ToSleepTime  = 25 - 2;
	m_iSleepMode = FALSE;

	PirEnable 	 = 0;
	
	ManualPirCfg = -1;
	m_PirMotionState = MOTION_STATE_NO_MOVE;

	WakeUpTime	 = 0;
	
_printd("ExtDevCreate start");	
	ExtDevCreate();
_printd("ExtDevCreate end");	
}
CDevExternal::~CDevExternal()
{
	ExtDevDestory();
}

VD_BOOL CDevExternal::Init()
{

}

void CDevExternal::OnDevExtTimer(uint arg)
{
	_printd("OnDevExtTimer");
	
	static char pflg = 0;
	
	if(SYS_WAKEUP_FLAG_PIR == m_WakeUpFlag)
	{
		m_PirMotionState = Dev_GetPirMotionState();
		_printd("m_PirMotionState: %d\n", m_PirMotionState);
		if (m_PirMotionState == MOTION_STATE_MOVE_LONG && pflg == 0) {
			g_DoorLockManager.PlayWarnVoice();
			pflg = 1;
		}
	}
}

VD_BOOL CDevExternal::Start()
{
	int iRet = 0;
	
	//Dev_SetPirStatus(0);
	m_cCfgAwe.attach(this, (TCONFIG_PROC)&CDevExternal::onConfigAwake);
	m_cCfgAwe.update();

	m_cCfgPirLevel.attach(this, (TCONFIG_PROC)&CDevExternal::onConfigPirLevel);
	m_cCfgPirLevel.update();

	if(!m_bLoop)	CreateThread();  

	m_DevExtTimer.Start(this, (VD_TIMERPROC)&CDevExternal::OnDevExtTimer, 1000, 1000);
	
	return VD_FALSE;
}

VD_BOOL CDevExternal::Stop()
{
	if(m_bLoop)		DestroyThread();

	return VD_TRUE;
}

int CDevExternal::Dev_GetElecQuantity(ZRT_CAM_BAT_Status *Stauts) //获取电量
{
	static ZRT_CAM_BAT_Status mStauts = {50,BAT_DISCHARGING,0};
	int ret = 0; 
	
	if( 0 != (ret = GetBatStatus(Stauts))){
		memcpy(Stauts,&mStauts,sizeof(ZRT_CAM_BAT_Status));
	}
	else{
		memcpy(&mStauts,Stauts,sizeof(ZRT_CAM_BAT_Status));
	}

	_printd("############# isLowBatState %d\n", Stauts->isLowBatState);
	
	if (Stauts->isLowBatState)
	{
		static char check_once = 0;
		if (check_once == 0) {
			if (CheckBatAlarmTime(BAT_ALARM_TYPE_CAMAER) < 0) {
				Stauts->isLowBatState = 0;
			}
		} else {
			Stauts->isLowBatState = 0;
		}
		check_once = 1;
	}

	return 0;
}

int CDevExternal::Dev_GetWiFi(ZRT_CAM_WIFI_Status *Status) //获取WiFi状态
{
	int ret = 0;
	ret = GetWifiStatus(Status);
	return ret;
}

int CDevExternal::Dev_SetPirStatus(int Status) //设置PIR状态
{
	int ret = 0;
	ret = SetPirEnableStatus(Status);
	return ret;
}

int CDevExternal::Dev_GetPirStatus(int* Status) //获取PIR状态
{
	int ret = 0;
	ret = GetPirEnableStatus(Status);
	return ret;
}

int CDevExternal::Dev_SetPirLevel(int pirLevel)
{
	_printd("pushMsgLevel:%d\n", pirLevel);

	CONFIG_PIRLEVEL &config_pirLevel = m_cCfgPirLevel.getConfig(0);

	config_pirLevel.pushMsgLevel = pirLevel;
		
	m_cCfgPirLevel.commit();
	
	return 0;
}

int CDevExternal::Dev_GetPirLevel(int *pPirLevel)
{
	if (pPirLevel == NULL) {
		return 0;
	} 

 	m_cCfgPirLevel.update();
	CONFIG_PIRLEVEL &config_pirLevel = m_cCfgPirLevel.getConfig(0);

	*pPirLevel = config_pirLevel.pushMsgLevel;

	return 0;
}
int CDevExternal::Dev_SetPowerSaveLevel(int powerLevel)
{
	_printd("powerLevel:%d\n", powerLevel);

	int level = PIR_LEVEL_5;
	CONFIG_PIRLEVEL &config_pirLevel = m_cCfgPirLevel.getConfig(0);

	switch (powerLevel)
	{
		case 0:
			level = PIR_LEVEL_1;
			break;
		case 1:
		case 2:
			level = PIR_LEVEL_5;
			break;
		default:
			_printd("set powerLevel %d is invalid!\n", powerLevel);
			break;
	}
	config_pirLevel.level = level;	
		
	m_cCfgPirLevel.commit();
	
	return 0;
}

int CDevExternal::Dev_GetPowerSaveLevel(int *pPowerLevel)
{
	if (pPowerLevel == NULL) {
		return 0;
	} 

	int level = 1;
 	m_cCfgPirLevel.update();
	CONFIG_PIRLEVEL &config_pirLevel = m_cCfgPirLevel.getConfig(0);

	switch (config_pirLevel.level)
	{
		case PIR_LEVEL_1:
			level = 0;
			break;
		case PIR_LEVEL_2:
		case PIR_LEVEL_3:
		case PIR_LEVEL_4:
		case PIR_LEVEL_5:
			level = 1;
			break;
		default:
			_printd("get pir level %d is invalid!\n", config_pirLevel.level);
			break;
	}
	
	*pPowerLevel = level;
	
	return 0;
}

VD_BOOL CDevExternal::Dev_SetDefaultPirLevelConfig(void)
{
	m_cCfgPirLevel.recall();
	m_cCfgPirLevel.commit();
}

int CDevExternal::Dev_SetPirTime()
{
	int ret = 0;
	int level = 0;
	int pir_detect_time = 0;
	int pir_effect_time = 0;
	
	CONFIG_PIRLEVEL &config_pirLevel = m_cCfgPirLevel.getConfig(0);
	level = config_pirLevel.level;

	_printd("set pir level: %d", level);
	
	switch (level)
	{
		case PIR_LEVEL_1:
			pir_detect_time = 1;
			pir_effect_time = 1;	
			break;
		case PIR_LEVEL_2:
			pir_detect_time = 12;
			pir_effect_time = 6;				
			break;
		case PIR_LEVEL_3:
			pir_detect_time = 18;
			pir_effect_time = 9;	
			break;
		case PIR_LEVEL_4:
			pir_detect_time = 20;
			pir_effect_time = 10;	
			break;
		case PIR_LEVEL_5:
			pir_detect_time = 30;
			pir_effect_time = 15;	
			break;
		default:
			_printd("The pir level %d is invalid!\n", level);
			return -1;
			break;
	}

	if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel) 
	{
		ret = BCAM_SetPirTime(pir_detect_time, pir_effect_time);
		if (ret < 0) {
			_printd("Fail to BCAM_SetPirTime\n");
			return -1;
		}
	}
	
	return 0;
}

int CDevExternal::Dev_GetKeyEnableStatus(int *Status)
{
	int ret = 0;
	//ret = GetKeyEnableStatus(Status);
	return ret;
}

int CDevExternal::Dev_SetKeyEnableStatus(int Status)
{
	int ret = 0;
	ret = SetKeyEnableStatus(Status);
	return ret;
}
void CDevExternal::Dev_EnableSleepMode() 
{
	EnableSleep = 1;
}
void CDevExternal::Dev_UnableSleepMode() 
{
	EnableSleep = 0;
}
void CDevExternal::Dev_SetSleepTime(int Val) //设置唤醒到睡眠的时间
{
	if(Val < 0){
		_printd("Val(%d) is error",Val);
	}
	
	CurRunCnt 	= 0;
	ToSleepTime = Val;
}
int CDevExternal::Dev_GetWakeUpFlag() 
{
	return m_WakeUpFlag;	
}

int CDevExternal::onConfigAwake(CConfigAwakeTime& cCfgAwe, int& iRet)
{
	_printd("ON");
	
	Dev_SetAlarmTime();
}

int CDevExternal::onConfigPirLevel(CConfigPirLevel& cCfgPirLevel, int& iRet)
{
	_printd("ON");

	Dev_SetPirTime();
	
	return 0;
}

#if 1
#define SERVER_PORT (6006)
#define SERVER_IP  	("192.168.200.1")//("192.168.1.181")
int SetSockOpt(int sock,int opt)
{
	int on   = 1;
	
	return setsockopt(sock, SOL_SOCKET, opt, (const char*)&on, sizeof(on));
}
int CreateSocket(int socket_family, int socket_type, int protocol)
{
	int sock = 0;
	
	if ((sock = socket(socket_family, socket_type, protocol)) < 0) 
	{
		perror("socket");
		return -1;
	}
	
	SetSockOpt(sock,SO_KEEPALIVE);
	SetSockOpt(sock,SO_REUSEADDR);
	
	return sock;
}
int NonBlockConnect(int sock,const char *addr, const int port)
{
	int ret = 0;
	int len = 0;
	int iRet = 0;
	int flags = 0;
	int error = -1;
	int falgsbak = 0;
	fd_set wds;
	struct timeval tv;
	
	struct sockaddr_in SockAddr = {0};

	SockAddr.sin_family 		= AF_INET;
	SockAddr.sin_port 		 	= htons(port);
	SockAddr.sin_addr.s_addr 	= inet_addr(addr); 
	
	if ((flags = fcntl(sock, F_GETFL, 0)) == -1)
	{
		_printd("fcntl(F_GETFL, O_NONBLOCK)");
    }
	falgsbak = flags;
    if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) == -1)
	{
        _printd("fcntl(F_SETFL, O_NONBLOCK)");
    }
	ret = connect(sock, (struct sockaddr *)&SockAddr, sizeof(SockAddr));

	if(0 > ret)
	{    
        FD_ZERO(&wds);
        FD_SET(sock,&wds);
        tv.tv_sec  = 3;
        tv.tv_usec = 0;
        iRet = select(sock + 1, NULL, &wds, NULL, &tv);
        if(iRet>0 && FD_ISSET(sock,&wds))
		{
            len = sizeof(error);
            getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)&error, (socklen_t*)&len);
			if(0 != error)
			{
				_printd("connect failed");
				ret = -1;
			}
			else
			{
				ret = 0;
			}
       	}
	}
	fcntl(sock, F_SETFL, falgsbak);

	return ret;
}
int ConnTerMinalStart()
{
	int clifd = 0;

	if(0 > (clifd = CreateSocket(AF_INET,SOCK_STREAM,0)))
	{
		_printd("CreateSocket Error");
		return -1;
	}
	if(0 > NonBlockConnect(clifd,SERVER_IP,SERVER_PORT))
	{
		perror("NonBlockConnect");
		_printd("NonBlockConnect Error");
		close(clifd);
		return -1;
	}		
	return clifd;
}
int ConnTerMinalStop(int* sock)
{
	if(sock == NULL)	return 0;
	if(*sock < 0)		return 0;

	close(*sock);

	*sock = -1;
	
	return 0;
}
int CDevExternal::SendMsgToTerminal(char cmd,char *buf,int len)
{
	if(buf == NULL || len < 1 || len > 24)	
	{
		_printd("Data[%d] is unnormal",len);
		return -1;
	}
	
	if(SockFd < 0)
	{	
		if((SockFd = ConnTerMinalStart()) < 0)
			return -1;
	}
	
	DVRIP szHdr;
    memset(&szHdr, 0, sizeof(szHdr));
    
    szHdr.dvrip_cmd	= cmd;
    szHdr.dvrip_r0	= 0;
    szHdr.dvrip_r1	= ~(szHdr.dvrip_r0);
    memcpy(szHdr.dvrip_p,buf,len);

	return send(SockFd,&szHdr,sizeof(szHdr),0);
}
#endif

VD_BOOL CDevExternal::EnterSleepMode()
{
	return VD_FALSE;
#ifdef DOOR_LOCK
	//return VD_FALSE;
#endif

	if(!EnableSleep)	return VD_FALSE;
#ifdef LIVECAM
	if(RT_NORMAL != RealTimeSpecialStatus()){
		return VD_FALSE;
	}
#endif
	if(1 == GetPirTriggerStatus())//PIR正在触发
	{
        if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel) 
        {

			ToSleepTime  = 5;
		}
		return VD_FALSE;		
	}

	if(g_RecordStatus)
	{
		_printd("#############StopManulRecord##############");
		g_Record.StopRec(REC_CLS, 0);

		g_RecordStatus = 1; //强制设标志位，避免录像在开启
	}	
	
	Dev_SetAlarmTime();//查看PirEnable是否使能

	usleep(500*1000);

	_printd("CurRunCnt:%d,ToSleepTime:%d,EnableSleep:%d",
			CurRunCnt,ToSleepTime,EnableSleep);

	if(!EnableSleep || CurRunCnt < ToSleepTime){
		_printd("Sleep have been updated,CurRunCnt:%d,ToSleepTime:%d,EnableSleep:%d",
			CurRunCnt,ToSleepTime,EnableSleep);
		return VD_FALSE;
	}
	
	if(1 != GetPirTriggerStatus())//PIR没触发
	{
#ifndef BCAM_FACTORY_VERSION
#ifdef DOOR_LOCK

#ifdef LOCK_NETLOG
		g_SdLog.CloseFileToUpload();
#endif
		g_DoorLockManager.Stop();
		//把未发送的锁端消息发送出去或者缓存下来
		g_DoorLockManager.ProcessRemainLockMsg();
#endif	
		//保证每次打开视频为标清,因此休眠前设置视频分辨率为标清
		RT_EncodeMode eCodeMode = RT_LUMODE;
		RealTimeVideoDataFormat videoinfo = {0};
		RealTimeSetVideoInfo_Callback(0,eCodeMode,0,&videoinfo);

	    BCAM_SingleSleepHeartCreate(1);
            
		ConnTerMinalStop(&SockFd);
#ifdef LIVECAM
		RealTimeServerClose();
#endif
		MasterEnterSleepMode();
#endif
	}
	else{
        if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel ||
            BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel) 
        {

			ToSleepTime  = 5;	
		}
	}

	g_RecordStatus = 0;

	return VD_TRUE;
}

int CDevExternal::Dev_SetAlarmTime()
{	
	int 		 i 			= 0;
	int			 hour		= 0;
	int			 min		= 0;
	int 		 sec		= 0;
	int			 NowTime 	= 0;
	int			 AlarmTime 	= 0;
	int			 StartTime 	= 0;
	int			 EndTime   	= 0;
	float		 fTimeZone  = 0.0;

    time_t          t;
	struct timeval 	tv;
	struct tm       *ntime;
	
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();

	fTimeZone = ((float)cfgnetntp.TimeZone)/10;
	
	if(g_NtpClient.GetNtpUpdateCount() <= 0)
	{
		struct tm       mtime;
		_printd("Waitting for updating time");

		GetMcuTime(&mtime);

		t 		= mktime(&mtime) + fTimeZone * 3600;
		ntime 	= gmtime(&t);
		
		_printd("tv:%ld Get McuTime:[%02d:%02d:%02d] w:%02d",t,ntime->tm_hour,
			ntime->tm_min,ntime->tm_sec,ntime->tm_wday);
	}
	else
	{
		SetCurrentTimeSyncMcu();
		
		t 		= time(NULL) + fTimeZone * 3600;	
		ntime 	= gmtime(&t);
		_printd("Get CurTime:[%02d:%02d:%02d] w:%02d",ntime->tm_hour,
			ntime->tm_min,ntime->tm_sec,ntime->tm_wday);
	}
	
	if(VD_TRUE == cfgnetntp.DaylightTime.Enable &&
		t >= cfgnetntp.DaylightTime.StartTime &&
		t <= cfgnetntp.DaylightTime.EndTime){
		_printd("time(%ld) is in daylight[%ld,%ld]",t,cfgnetntp.DaylightTime.StartTime,
			cfgnetntp.DaylightTime.EndTime);
		t += 3600;
	}
	usleep(100 * 1000);

	NowTime = (ntime->tm_hour << 16) | (ntime->tm_min << 8) | ntime->tm_sec;
//	_printd("Set NowTime[%02d:%02d:%02d]",(NowTime>>16)&0xff,(NowTime>>8)&0xff,NowTime&0xff);
	
	m_cCfgAwe.update();
	CONFIG_AWAKETIME& m_Cfg = m_cCfgAwe.getConfig(ntime->tm_wday);

	PirEnable = 0;
	for(i = 0;i < UNITS; i ++)
	{
		if(m_Cfg.DayPlan[i].Enable == TRUE)
		{
			if(NowTime < m_Cfg.DayPlan[i].StartTime && (0 == AlarmTime || AlarmTime > m_Cfg.DayPlan[i].StartTime))	
			{
				AlarmTime = m_Cfg.DayPlan[i].StartTime;
				continue;
			}
			else if(NowTime >= m_Cfg.DayPlan[i].StartTime && NowTime < m_Cfg.DayPlan[i].EndTime)
			{
				AlarmTime = m_Cfg.DayPlan[i].EndTime;
				PirEnable = 1;							  //处于设置时间段内才设置PIR唤醒
				break;
			}
			else
			{
				continue;
			}
		}
	}
	
//转成格林威治时区的时分秒 

	min = (cfgnetntp.TimeZone%10)*6 + (AlarmTime>>8)&0xff;
	_printd("min:%d",min);
	if(min > 60)
	{
		hour = 1;
		min -= 60;
	}
	else
	{
		hour = 0;
	}

	hour += (AlarmTime>>16)&0xff;
	_printd("hour:%d",hour);
	hour -= (cfgnetntp.TimeZone)/10;
	_printd("hour:%d",hour);
	if(hour < 0)	hour += 24;
	if(hour > 24)   hour -= 24;

	_printd("hour:%d,min:%d",hour,min);
	
	AlarmTime = (hour << 16) | (min << 8) | (AlarmTime&0xff); 

	if (BAT_ProductM_S1 == BAT_PlatformGetHandle()->ProductModel)
	{
		_printd("Set AlarmTime[%02d:%02d:%02d]",(AlarmTime>>16)&0xff,(AlarmTime>>8)&0xff,AlarmTime&0xff);
		SetSystemAlarmTime(AlarmTime);	
	}
	
	usleep(500 * 1000);

	if(1 == ManualPirCfg){
		_printd("force open pir");
		Dev_SetPirStatus(1);
	}
	else if(0 == ManualPirCfg){
		_printd("force close pir");
		Dev_SetPirStatus(0);
	}
	else{
		i = Dev_SetPirStatus(PirEnable);		
		_printd("SetPir :%d\n",i);
	}

	if (1 == ManualPirCfg || 1 == PirEnable) {
		Dev_SetPirTime();
	}

	return 0;
}

int CDevExternal::UpdateBatAlarmTimeConfig(BAT_ALARM_TYPE alarmType)
{
    time_t          t = time(NULL);
    struct tm*      time;
    struct timeval  tv;
    struct timezone tz;

    gettimeofday(&tv, &tz);
	t += tz.tz_minuteswest*60;

	char batAlarmTime[16] = {0};	
	CConfigBatAlarm configBatAlarm; 
	configBatAlarm.update();
	CONFIG_BATALARM &config_batalarm = configBatAlarm.getConfig(0);
	sprintf(batAlarmTime, "%d", t);

	if (alarmType == BAT_ALARM_TYPE_CAMAER) {
		_printd("Update BAT_ALARM_TYPE_CAMAER config\n");
		strncpy(config_batalarm.strCamLowBatTime, batAlarmTime, sizeof(config_batalarm.strCamLowBatTime));
	} else if (alarmType == BAT_ALARM_TYPE_LOCK) {
		_printd("Update BAT_ALARM_TYPE_LOCK config\n");
		strncpy(config_batalarm.strLockLowBatTime, batAlarmTime, sizeof(config_batalarm.strCamLowBatTime));
	}
	configBatAlarm.commit();
	
	return 0;
}

int CDevExternal::CheckBatAlarmTime(BAT_ALARM_TYPE alarmType)	
{	
	time_t t = 0;
	struct tm lastTime;
	struct tm nowTime;
	CConfigBatAlarm configBatAlarm; 
	configBatAlarm.update();
	CONFIG_BATALARM &config_batalarm = configBatAlarm.getConfig(0);

	if (alarmType == BAT_ALARM_TYPE_CAMAER) {
		t = atoi(config_batalarm.strCamLowBatTime);
	} else if (alarmType == BAT_ALARM_TYPE_LOCK) {
		t = atoi(config_batalarm.strLockLowBatTime);
	}

	memset(&lastTime, 0, sizeof(lastTime));
	if (NULL == gmtime_r(&t, &lastTime)) {
		return -1;
	}
	
    struct timeval  tv;
    struct timezone tz;
	
	t = time(NULL);
    gettimeofday(&tv, &tz);
	t += tz.tz_minuteswest*60;
	memset(&nowTime, 0, sizeof(nowTime));
	if (NULL == gmtime_r(&t, &nowTime)) {
		return -1;
	}
	
	_printd("batAlarmType: %d, nowTime->tm_mday: %d, lastTime->tm_mday: %d\n", alarmType, nowTime.tm_mday, lastTime.tm_mday);
	/* 若是同一天则不上报低电告警事件 */
	if (nowTime.tm_mday == lastTime.tm_mday) {
		return -1;
	}

	UpdateBatAlarmTimeConfig(alarmType);
	
	return 0;	
}

MOTION_STATE_E CDevExternal::Dev_GetPirMotionState(void)
{
	MOTION_STATE_E motionState = MOTION_STATE_NO_MOVE;

	BCAM_GetPirMotionState(&motionState);
	if (BAT_ProductM_S1 == BAT_PlatformGetHandle()->ProductModel) {
		motionState = MOTION_STATE_MOVE;
	}	

	_printd("motionState: %d\n", motionState);
	
	return motionState;
}
VD_BOOL CDevExternal::Dev_PushAlarmStart()
{
#ifdef	MOBILE_COUNTRY
	uint cur_time = time(NULL);

//	g_MobileCountrySdkApi.SendEvent((uint64)cur_time * (uint64)1000, CC_DETECT_MOTION, CC_DETECT_START, NULL);

	return VD_TRUE;
#else
	VD_BOOL	ret = VD_FALSE;
	RT_UpLoadAlarmInfo alarminfo = {0};

	if(0 == RealTimeNetWorkStatus()){
		_printd("network is error");
		return VD_FALSE;
	}
	if(0 == WakeUpTime){
		alarminfo.TimeStamp = time(NULL);
	}
	else{
		alarminfo.TimeStamp = WakeUpTime;
	}
	alarminfo.AlarmType = RT_MOTIOM;
	CaptureGetSnapPic((char**)(&(alarminfo.ImageInfo.buf)),(int*)&(alarminfo.ImageInfo.size));
	if( 0 == RealTimePushStartEvent_WithoutRecord(alarminfo)){
		ret = VD_TRUE;
	}
	if( NULL != alarminfo.ImageInfo.buf ){
		free(alarminfo.ImageInfo.buf);
		alarminfo.ImageInfo.buf = NULL;	
	}	
	
	return ret;
#endif	
}
VD_BOOL CDevExternal::Dev_PushAlarmEndWithRecord()
{
#ifdef	MOBILE_COUNTRY
	uint cur_time = time(NULL);

//	g_MobileCountrySdkApi.SendEvent((uint64)cur_time * (uint64)1000, CC_DETECT_MOTION, CC_DETECT_STOP, NULL);

	return VD_TRUE;
#else
	int iRet = 0;
	char file[128] = {0};
	
	VD_BOOL ret = VD_FALSE;
	

	if(VD_TRUE == GetExtRecFile(file)){
		unsigned int videoStartTime = 0;

		if(0 == WakeUpTime){
			videoStartTime = time(NULL) - PIRREC_DUR;
		}
		else{
			videoStartTime = WakeUpTime;
		}
		
		if(0 == (iRet = RealTimePushEventRecord(RT_MOTIOM,file,videoStartTime,videoStartTime+PIRREC_DUR))){
			RealTimePushEndEvent(RT_MOTIOM,time(NULL));
			ret = VD_TRUE;
		}		
		_printd("upload file[%s][%d,%d].....%d\n",file,videoStartTime,videoStartTime+PIRREC_DUR,iRet);

		if(0 == iRet){
			_printd("remove file:%s....%d",file,remove(file));
		}
	}

	return ret;
#endif	
}
void CDevExternal::ThreadProc()
{
	char sflg = 0;
	char eflg = 0;
	int kState = 0;

	char pushAllowFlag = 0;
	CONFIG_PIRLEVEL pirLevel;
	
	GetSystemWakeUpFlag(&m_WakeUpFlag);
	_printd("m_WakeUpFlag:%d",m_WakeUpFlag);

	if(m_WakeUpFlag == SYS_WAEKUP_FLAG_ALARM || m_WakeUpFlag == SYS_WAKEUP_FLAG_RECOVERY_WIFI)	
		ToSleepTime  = 5;
	else if(m_WakeUpFlag == SYS_WAKEUP_FLAG_PIR || m_WakeUpFlag == SYS_WAKEUP_FLAG_LOCK)
		ToSleepTime  = 15;

	memset(&pirLevel, 0, sizeof(pirLevel));
	Dev_GetPirLevel(&(pirLevel.pushMsgLevel));

#if 0 //debug joki
	m_WakeUpFlag = SYS_WAKEUP_FLAG_PIR;
	pirLevel.pushMsgLevel = PIR_PUSH_MSG_NORMAL;
#endif
	while(m_bLoop)
	{
		if( 0 == WakeUpTime && g_NtpClient.GetNtpUpdateCount() > 0){
			WakeUpTime = time(NULL);
			_printd("WakeUpTime:%u",WakeUpTime);
		}
	
		if(SYS_WAKEUP_FLAG_PIR == m_WakeUpFlag){
			m_PirMotionState = Dev_GetPirMotionState();
			_printd("pushMsgLevel:%d\n", pirLevel.pushMsgLevel);

			switch (pirLevel.pushMsgLevel)
			{
				case PIR_PUSH_MSG_OFF:
					pushAllowFlag = 0;
					break;
				case PIR_PUSH_MSG_NORMAL:
					if (m_PirMotionState == MOTION_STATE_MOVE) {
						pushAllowFlag = 1;
					}
					break;
				case PIR_PUSH_MSG_STAY_SHORT:
					if (m_PirMotionState == MOTION_STATE_MOVE_SHORT) {
						pushAllowFlag = 1;
					}
					break;
				case PIR_PUSH_MSG_STAY_LONG:
					if (m_PirMotionState == MOTION_STATE_MOVE_LONG) {
						pushAllowFlag = 1;
					}
					break;
				default:
					_printd("The pir pushMsgLevel %d is invalid!\n", pirLevel.pushMsgLevel);
					break;
			}
#ifdef LIVECAM
			if(0 == sflg && 1 == RealTimeNetWorkStatus() && 1 == pushAllowFlag)
#else
			if(0 == sflg && 1 == pushAllowFlag)
#endif
			{
				if(VD_TRUE == Dev_PushAlarmStart()){
					sflg = 1;
				}
			}

			if(sflg){
				if(0 == eflg && VD_TRUE == Dev_PushAlarmEndWithRecord()){
					eflg = 1;
					ToSleepTime = 1;
				}
			}
			if (m_PirMotionState == MOTION_STATE_MOVE_END) { //未移动且移动侦测结束,没有事件上传可以尽快休眠
				ToSleepTime = 1;
			}
		}
		/*
		else{
			m_WakeUpFlag = SYS_WAKEUP_FLAG_NONE;
		}
		*/
		
		if(0 == BCAM_GetAliveState((int *)&kState) && 1 == kState ) //配置键按下
		{
			CurRunCnt = 1;
		}
		
		SystemSleep(1000);  //休眠 1s

		if(CurRunCnt > ToSleepTime)
		{
			EnterSleepMode();
			CurRunCnt = 0;
		}
	
		if(m_iSleepMode) //pass给指令休眠
		{
			EnterSleepMode();
		}
		CurRunCnt ++;

		_printd("[%ds to sleep]...SleepCnt :%d",ToSleepTime,CurRunCnt);
	}
}


