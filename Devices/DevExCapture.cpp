

#include "Devices/DevExCapture.h"
#include "System/Packet.h"
#include "Configs/ConfigLocation.h"

#define TIMEOUT 0
#define SIMULA_BUFFER_LEN 4096

CExCaptureManager* CDevExCapture::m_pManager = NULL;
CDevExCapture* CDevExCapture::_instance = NULL;

CDevExCapture* CDevExCapture::instance(int iChannel)
{
	if(NULL == _instance)
	{
		_instance = new CDevExCapture(0);
	}
	return _instance;
}

CDevExCapture::CDevExCapture(int iChannel) : CThread("DevExCapture", TP_CAPTURE), m_sigBuffer(32*16)
{
	m_dwBytes = 0;
	m_dwBytesOld = 0;
	m_dwBytesMask = 0;
	m_iChannel = iChannel;
	ExCaptureCreate(m_iChannel);

	if(m_pManager == NULL)
	{
		m_pManager = new CExCaptureManager;
	}
	m_pManager->Init(iChannel, this);
}

CDevExCapture::~CDevExCapture()
{
	ExCaptureDestory(m_iChannel);
}

int CDevExCapture::GetChannels ()
{
	return CaptureGetChannels();
}

VD_BOOL CDevExCapture::GetCaps(CAPTURE_CAPS * pCaps)
{
	if (!CaptureGetCaps(pCaps)) 
	{
	#ifdef MACE
		if(g_CapsEx.ForNRT)          //只有mace要求非实时取消D1
		{
			pCaps->ImageSize = BITMSK(CAPTURE_SIZE_CIF);
		}
	#endif

#ifdef ENC_COVER_BLOCKS
		pCaps->CoverBlocks = ENC_COVER_BLOCKS;
#endif

		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDevExCapture::Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags)
{
/************************************************************************
	开始一个捕获设备：
	1、把启动该捕获设备的对象注册到本设备上；
	2、如果注册成功，则调用CaptureStart函数来启动物理上的一个捕获设备，
	   否则直接返回FALSE；
	3、如果物理设备启动成功，则启动本捕获设备对象的线程；
	   （如果2、3对换的话，那么在本对象的线程回调函数中的CaptureGetBuffer
	   函数将返回FALSE）
	4、如果3执行成功，则开启一个用于计算码流的定时器；
************************************************************************/
	int slots = m_sigBuffer.Attach(pObj, pProc);
	if(slots < 0)
	{
		trace("CDevExCapture::Start Attach failed(%d)\n", slots);
		return FALSE;
	}
	if(slots == 1)
	{
		if (ExCaptureStart(dwFlags) >= 0) 
		{
			m_pManager->Start();
			return TRUE;
		}
		else
		{
			trace("Channel = %d, CDevExCapture::Start CaptureStart failed\n", m_iChannel);
			//
			// 2005-12-2 11:23 Joy 
			// 失败了要detach掉否则下次不行了
			// detach
			//
			slots = m_sigBuffer.Detach(pObj, pProc);
			if (slots < 0)
			{
				trace("Detach Failed\n");
			}

			return FALSE;
		}
	}
	return TRUE;
}

VD_BOOL CDevExCapture::Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc)
{
/************************************************************************
	停止一个捕获设备：
	1、把停止该捕获设备的对象注销掉；
	4、如果1返回0，则停止用于计算码流的定时器；
	2、如果1返回0，则调用DestroyThread函数来停止捕获设备的线程；
	3、如果2调用成功，则调用CaptureStop函数来停止一个物理的捕获设备；
	5、如果3调用成功，则返回TRUE，否则返回FALSE；
	6、如果1返回值大于0，表示还有其它对象注册在该捕获设备对象上，此时直接返回TRUE；
************************************************************************/
	int slots = m_sigBuffer.Detach(pObj, pProc);
	if(slots < 0)
	{
		trace("CDevExCapture::Stop Detach failed\n");
		return FALSE;
	}
	if(slots == 0)
	{
		m_pManager->Stop();
		if (!ExCaptureStop()) 
		{
			return TRUE;
		}
		trace("CDevExCapture::Stop ExCaptureStop failed\n");
		return FALSE;
	}
	return TRUE;	// 6
}
//多画面预览强制I帧 
VD_BOOL CDevExCapture::SetIFrame()
{
#if defined(DVR_GB)
	return !ExCaptureForceIFrame(m_iChannel);
#else
	return true;
#endif
}	

VD_BOOL CDevExCapture::SetVstd (uint dwStandard)
{
	if (!ExCaptureSetVstd(dwStandard)) 
	{
		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDevExCapture::SetFormat(uchar Compression, uchar BitRateControl, uchar ImageSize, uchar ImageQuality,uchar FramesPerSecond)
{
	CAPTURE_FORMAT Format;
	Format.Compression = Compression;
	Format.BitRateControl = BitRateControl;
	Format.ImageSize = ImageSize;
	Format.ImageQuality = ImageQuality;
	Format.FramesPerSecond = FramesPerSecond;

//	if (!ExCaptureSetFormat(m_iChannel, &Format)) 
//	{
//		return TRUE;
//	}
	return FALSE;
}

VD_BOOL CDevExCapture::SetColor(uchar Brightness, uchar Contrast, uchar Saturation, uchar Hue, uchar Gain)
{
	VIDEO_COLOR Color;
	Color.Brightness = Brightness;
	Color.Contrast = Contrast;
	Color.Saturation = Saturation;
	Color.Hue = Hue;
	Color.Gain = Gain;
	
//	if (!ExCaptureSetColor(m_iChannel, &Color)) 
//	{
//		return TRUE;
//	}
	return FALSE;
}

VD_BOOL CDevExCapture::SetTime(SYSTEM_TIME * pSysTime)
{
//	if (!ExCaptureSetTime(NULL, pSysTime, NULL, NULL, NULL)) 
//	{
//		return TRUE;
//	}
	return FALSE;
}

uint CDevExCapture::GetBitRate()
{
	uint dwMS, dwIntervalMS;

	dwMS = SystemGetMSCount();
	dwIntervalMS = dwMS - m_Time;
	m_Time = dwMS;
	m_dwBytesOld = m_dwBytes;
	m_dwBytes = 0;

	if (dwIntervalMS == 0)
	{
		return 0;
	}
	return m_dwBytesOld * 8 * 1000 / 1024 / dwIntervalMS;
}

void CDevExCapture::OnData(CPacket * pPacket)
{
	m_dwBytes += pPacket->GetLength();
	m_sigBuffer(m_iChannel, pPacket);
}

/********************************************************************************************************************
	CExCaptureManager
********************************************************************************************************************/
/*! 下面的数据最大为64K
 * J.W 2005-11-23 16:49
 */

#define PACKET_SIZE 4*1024

void CExCaptureManager::ThreadProc()
{
	CPacket *pPacket;
	uchar *pBuffer;
	uint length;
	int ret;

/************************************************************************
	捕获设备线程的消息回调函数：
	1、调用ExCaptureGetBuffer函数来获取一块CPacket对象指针；
	2、如果1中函数调用成功，则调用注册到捕获设备的回调函数；
	3、记录好本次获取packet的长度，该长度用于计算码流；
	4、释放由ExCaptureGetBuffer函数申请的packet内存空间；
	5、当线程回调函数循环控制变量m_bLoop为FALSE时，退出循环体，
	   否则循环执行1、2、3、4；
************************************************************************/
//static uint ms = 0;
//ms = SystemGetMSCount();
//trace("%d\n", SystemGetMSCount() - ms);
	while (m_bLoop) 
	{
		pPacket = g_PacketManager.GetPacket(PACKET_SIZE);
		if (NULL == pPacket)
		{
			trace("Ex -- GetPacket failed.\n");
			continue;
		}
		//assert(pPacket);

		pBuffer = pPacket->GetBuffer();
		length = pPacket->GetSize();
		ret = ExCaptureGetBuffer(pBuffer, length);

		//
		//
		if(ret <= 0)
		{
			//trace("CDevExCapture::ThreadProc() CaptureGetBuffer failed\n");
			pPacket->Release();
			continue;
		}

		pPacket->SetLength(ret);

		assert(pPacket->GetRef() == 1);
		m_pDevCap[0]->OnData(pPacket);
		pPacket->Release();
	}
}


CExCaptureManager::CExCaptureManager() : CThread("ExCaptureManager", TP_CAPTURE)
{
	m_iUser = 0;
}

CExCaptureManager::~CExCaptureManager()
{
}

void CExCaptureManager::Init(int iChannel, CDevExCapture * pDevCap)
{
	CGuard guard(m_Mutex);
	m_pDevCap[iChannel] = pDevCap;
}

void CExCaptureManager::Start()
{
	CGuard guard(m_Mutex);
	if (m_iUser == 0)
	{
		CreateThread();
	}
	m_iUser++;
}

void CExCaptureManager::Stop()
{
	CGuard guard(m_Mutex);
	m_iUser--;
	if (m_iUser == 0)
	{
		DestroyThread();
	}
}

