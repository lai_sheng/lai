
#include "Devices/DevHDiskReader.h"
#include "Rtsp/BaseFunc/VDDebug.h"

//!系统集成专用宏, 系统集成服务器转发数据录像长度不能超过9k
#if 0
#ifdef _FUNC_XTJC    
#define PACKET_SIZE_ONCE     8*1024
#else
#define PACKET_SIZE_ONCE     32*1024
#endif
#else
#define PACKET_SIZE_ONCE     (4 * 1024)
#endif
#define MSG_QUEUE_SIZE 1


extern int convertRealFrame(int iframeIndex);

CDevHDiskReader::CDevHDiskReader() :
        CThread("DevHDiskReader", TP_NET/*, MSG_QUEUE_SIZE*/),
        m_sigBuffer(1)
{
    m_pDHFile = NULL;
    m_pDHFileInfo = new STM_INFO;
    m_iPacketIdx = 0;

    m_iFrameRate = 0;
    m_iNetFrameRate = 0;

    m_iLimitSpeed = 0;
}

CDevHDiskReader::~CDevHDiskReader()
{
    DestroyThread(TRUE);

    if (NULL != m_pDHFile)
    {
        delete m_pDHFile;
        m_pDHFile = NULL;
    }

    if (m_pDHFileInfo)
    {
        delete m_pDHFileInfo;
    }
}

void CDevHDiskReader::ThreadProc()
{
    while ( TRUE == m_bLoop )
    {
        if (FALSE == Read())
        {
            SystemSleep(50);
            continue;
        }
    } // while
}

VD_BOOL CDevHDiskReader::Start(CObject * pObj, SIG_DEV_HDISK_READER pProc, int iCh, int iDlg)
{
    CGuard guard(m_Mutex);

    if(m_sigBuffer.Attach(pObj, pProc) < 0)
    {
        __fline;
        trace("attach error\n");
        return FALSE;
    }

    m_iCh = iCh;
    m_iDlg = iDlg;

    return CreateThread();
}

VD_BOOL CDevHDiskReader::Stop(CObject * pObj, SIG_DEV_HDISK_READER pProc)
{
    VD_BOOL bRet = DestroyThread(TRUE);

    CGuard guard(m_Mutex);

    if(m_sigBuffer.Detach(pObj, pProc) == 0)
    {
        // good
    }
    else
    {
        // error
        __trip;
    }

    return bRet;
}

VD_BOOL CDevHDiskReader::Open()
{
    if (NULL == m_pDHFile)
    {
        m_pDHFile = new FFFile;
    }

    //
    // 已经打开
    //
    if (m_pDHFile->IsOpened())
    {
    	Close(); //!硬盘管理操作时，会要求所有的文件要全部重新打开
//        return TRUE;
    }

    return m_pDHFile->Open();
}

VD_BOOL CDevHDiskReader::Close()
{
    if (NULL == m_pDHFile)
    {
        return TRUE;
    }

    //
    // 还没有打开
    //
    if (!m_pDHFile->IsOpened())
    {
        return TRUE;
    }

    return m_pDHFile->Close();
}
VD_BOOL CDevHDiskReader::ResetPacketIdx()
{
    m_iPacketIdx = 0;
    return TRUE;
}

VD_BOOL CDevHDiskReader::Read(void *pBuffer, uint length)
{
    int ret = 0;

    if(pBuffer)
    {
        ret = m_vReader.Read(pBuffer, length);//m_pDHFile->Read(pBuffer, length);
    }
    
    return ret;
}

VD_BOOL CDevHDiskReader::Read()
{
    CPacket *pPacket;
    uchar *pBuffer;
    DVRIP *pDHHead;
    uint length;
    uint ret;
    pPacket = g_PacketManager.GetPacket(PACKET_SIZE_ONCE);
    if (NULL == pPacket)
    {
        trace("HDR -- GetPacket failed.\n");
        return FALSE;
    }

    pBuffer = pPacket->GetBuffer();
    pDHHead = (DVRIP *)pPacket->GetHeader();
    length = pPacket->GetSize();

    ret = m_vReader.Read(pBuffer, length);//m_pDHFile->Read(pBuffer, length);

    static int iCounter = 0;
    iCounter += ret;
    if( ret < pPacket->GetSize() )
    {
        trace("CDevHDiskReader:: read file size[%d]\n", iCounter);
        iCounter = 0;
    }
/*	else
		printf("iCounter:%d\n",iCounter);
*/
    if(ret > 16)
    {
        pPacket->SetLength(ret);

        assert(pPacket->GetRef() == 1);
        //////
        pPacket->ClearHeader();
        //pDHHead->command = 0xbb;
        pDHHead->dvrip_p[0] = 1;//m_iCh+1;
        pDHHead->dvrip_extlen = ret;
        pDHHead->dvrip_r0 = GetNetFrameRate();
        pDHHead->dvrip_r1 = ~pDHHead->dvrip_r0;
        // 第10-13字节：数据序号
        memcpy( (uchar *)&(pDHHead->dvrip_p[1]),
                (uchar *)&m_iPacketIdx,
                sizeof(uint) );
        m_iPacketIdx++;
        //////
        m_sigBuffer(HDISKREADER_CMD_ONPACKET, pPacket, 0, m_iDlg);//m_iCh
    }
    else 
    {
        m_sigBuffer(HDISKREADER_CMD_FINISHED, NULL, m_iCh, m_iDlg);
    }
    
    pPacket->Release();

    return TRUE;
}

VD_BOOL CDevHDiskReader::Locate(uint iDisk, uint iPart,uint iCluster, SYSTEM_TIME *pST)
{
    VD_BOOL bRet = 0;
#if 0
    Open();

    bRet = m_pDHFile->Locate(iDisk,iPart,iCluster);
    
    m_pDHFile->LimitTime(NULL);/*取消限制结束时间*/
    
    if (NULL != pST)
    {
        m_pDHFile->SeekDHTime(pST);
    }

    m_pDHFile->GetInfo(m_pDHFileInfo);

    FrameRate();
#else
	if(pST != NULL)
	{	
		m_vReader.Locate(*pST);
		m_vReader.Open();
	}
#endif
    return bRet;
}

VD_BOOL CDevHDiskReader::Locate(int iCh, SYSTEM_TIME *pST)
{
    VD_BOOL bRet = 0;
#if 0
    uint num = 1;
    FILE_INFO info;

    Open();

    SYSTEM_TIME pDST;
    pDST.year = 0xffff;
    bRet = m_pDHFile->GetList(iCh, pST, &pDST, ANY_FILE_TYPE, &num, &info);
    if(!bRet)
    {
        Close();
        return bRet;
    }

    bRet = m_pDHFile->Locate(&info);
    if(!bRet)
    {
        Close();
        return bRet;
    }

    m_pDHFile->LimitTime(NULL);/*取消限制结束时间*/

    m_pDHFile->GetInfo(m_pDHFileInfo);


#if 0
    char buf[1024] = {0};
    FormatTimeString(&m_pDHFileInfo->stime, buf, FT_FULL_AMPM);
    trace("CDevHDiskReader>:%s\n", buf);
#endif
    FrameRate();

#else
	if(pST != NULL)
	{	
		m_vReader.Locate(*pST);
		m_vReader.Open();
	}
#endif
    return bRet;
}
VD_BOOL CDevHDiskReader::NewLocate(uint iDisk,uint iPart,uint iCluster, SYSTEM_TIME *pST ,SYSTEM_TIME *pET)
{
#if 0
    VD_BOOL bRet;

    Open();

    bRet = m_pDHFile->Locate(iDisk, iPart,iCluster);
    
    m_pDHFile->LimitTime(pET);/*限制结束时间*/
    
    if (NULL != pST)
    {
        m_pDHFile->SeekDHTime(pST);
    }

    m_pDHFile->GetInfo(m_pDHFileInfo);

    FrameRate();

    return bRet;
#else
	m_vReader.Locate(*pST);
	m_vReader.Open();
	return VD_TRUE;
#endif
}
VD_BOOL CDevHDiskReader::NewLocate(uint iDisk,uint iCluster, SYSTEM_TIME *pST ,SYSTEM_TIME *pET)
{
#if 0
    VD_BOOL bRet;

    Open();

    bRet = m_pDHFile->Locate(iDisk,iCluster);
    
    m_pDHFile->LimitTime(pET);/*限制结束时间*/
    
    if (NULL != pST)
    {
        m_pDHFile->SeekDHTime(pST);
    }

    m_pDHFile->GetInfo(m_pDHFileInfo);

    FrameRate();

    return bRet;
#else
	m_vReader.Locate(*pST);
	m_vReader.Open();
	return VD_TRUE;
#endif
}
VD_BOOL CDevHDiskReader::NewLocate(int iCh, SYSTEM_TIME *pST,SYSTEM_TIME *pET)
{
#if 0
    VD_BOOL bRet;
    uint num = 1;
    FILE_INFO info;

    Open();

    bRet = m_pDHFile->GetList(iCh, pST, pET, ANY_FILE_TYPE, &num, &info);
    if(!bRet)
    {
        Close();
        return bRet;
    }

    bRet = m_pDHFile->Locate(&info);
    if(!bRet)
    {
        Close();
        return bRet;
    }
    
    m_pDHFile->LimitTime(pET);/*限制结束时间*/

    m_pDHFile->GetInfo(m_pDHFileInfo);

    FrameRate();

    return bRet;
#else
	m_vReader.Locate(*pST);
	m_vReader.Open();
	return VD_TRUE;
#endif
}
int CDevHDiskReader::Seek(int iOffset, uint iFrom)
{
 //   Open();
    if (NULL == m_pDHFile)
    {
        m_pDHFile = new FFFile;
    }

    if (!m_pDHFile->IsOpened())
    {
    	m_pDHFile->Open();
    }

    return m_pDHFile->Seek(iOffset, iFrom);
}

VD_BOOL CDevHDiskReader::SeekTime(int iOffset, uint iFrom)
{
    Open();

   // return m_pDHFile->SeekTime(iOffset, iFrom); //fyj
   return VD_FALSE;
}

/*为了采用绝对值来定位文件实现时间偏移方式拖动回放,add by shi*/
VD_BOOL CDevHDiskReader::SeekDHTime(SYSTEM_TIME * psSysTime)
{
	Open();

	//return m_pDHFile->SeekDHTime(psSysTime); //fyj
   return VD_FALSE;
}

#include "Devices/DevCapture.h"

int CDevHDiskReader::GetFrameRate()
{
    return m_iFrameRate;
}

int CDevHDiskReader::GetNetFrameRate()
{
    return m_iNetFrameRate;
}

void CDevHDiskReader::FrameRate()
{
    m_iFrameRate = 0;
    m_iNetFrameRate = 0;

    if (NULL == m_pDHFile)
    {
        return;
    }
    
    m_iFrameRate = (m_pDHFileInfo->image&0xf0)>>4;

    //////////////////////////////////////////////////////////////////////////
    // 按协议转换
    //
    // 实时录像
    //
    if (m_iFrameRate >= 24)
    {
        m_iNetFrameRate = 0x11;
    }
    //
    // 抽帧录像
    //
    else if (m_iFrameRate <= 7)
    {
        m_iNetFrameRate = m_iFrameRate+0x33;
    }
    else if (10 == m_iFrameRate)
    {
        m_iNetFrameRate = 0x3b;
    }
    else if (12 == m_iFrameRate)
    {
        m_iNetFrameRate = 0x3c;
    }
    else if (15 == m_iFrameRate)
    {
        m_iNetFrameRate = 0x3d;
    }
    else if (20 == m_iFrameRate)
    {
        m_iNetFrameRate = 0x3e;
    }
    else
    {
        m_iNetFrameRate = 0x11;
    }

}

