#include <string.h>

#include "Devices/DevCapBuf.h"
#include "System/AppConfig.h"
#include "Media/ICapture.h"
#include "System/Support.h"


CDevBufManager* CDevBufManager::_instance = NULL; 

CDevBufManager* CDevBufManager::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevBufManager();
	}
	return _instance;
}

CDevBufManager::CDevBufManager() :
	m_pBuffer(NULL)
{
	//从LUA中获得缓冲数据包的大小
	//uint PreRecLen;
	AppConfig * pAppConfig = AppConfig::instance();

	//取其一部分用作预录,以K为单位，看设备内存大小决定
	//m_iSize = (uint)pAppConfig->getNumber("Global.Memory.PacketBufSize", 256 * 16 * 2);
	m_iSize = (uint)pAppConfig->getNumber("Global.Memory.PacketBufSize", 64 * 16 * 2);
	
#ifdef SNAP_REQUIRE_ISTC
        m_iSize = 64 * 16 * 2; //抓图的话，限制录像预录为64 K
#endif	
        //m_iSize /= 2;

        /* 部分产品内存不足，预录在这里进行控制 */
        /* 这里限制下4路及以上设备，packet小于20M时，用于预录的内存减半 */
        /* 主要针对custdown内存不足的问题 */
        if (m_iSize < 10000  && ICaptureManager::instance()->GetAnalogChnNum() >= 4 )
        {
            m_iSize /= 4;
        }
        else
        {
            m_iSize /= 3;
        }
        
	trace("PreRecLen = %d\n", m_iSize);
	//转换成字节
	m_iSize = m_iSize << 10;

	//计算个数
	int num = 0;
	num = m_iSize >> 10;
	assert(num != 0);

	m_pBuffer = new BUFFER_BLOCK[num];
	assert(m_pBuffer);

	m_iLength = 0;

	//初始化链表
	INIT_LIST_HEAD(&m_prerecordList);
	for (int i = 0; i < N_SYS_CH; i++)
	{
		INIT_LIST_HEAD(&m_channelList[i]);
		m_bDataGetting[i] = FALSE;
	}
	INIT_LIST_HEAD(&m_recordList);
	INIT_LIST_HEAD(&m_emptyList);

	//组成空链表
	for (int i = 0; i < num; i++)
	{
		list_add(&m_pBuffer[i].timeList, &m_emptyList);
	}
}

CDevBufManager::~CDevBufManager()
{
	m_iLength = 0;
	if (m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
}

///
///	\b Description		:	存放数据\n
///		1.缓冲区是否已经满，满的话释放预录数据，把列表从通道数据和预录数据里释放出来，放回到空列表里
///		2.从空列表里取出包，存放传进来的数据，再把数据放到对应的录像队列和通道队列
/// \param pPkt			：	数据包
/// \param iChannel		：	数据通道
///	\return 0:表示成功，1表示缓冲满了
///
///	\b Revisions		:	
///		- 2007-12-13		yuan_shiyong		Create
///
int CDevBufManager::pushBuffer(CPacket *pPkt, int iChannel)
{
	CGuard guard(m_bufferMutex);
	struct 	list_head *pList = NULL;
	//判断长度
	while (m_iLength + pPkt->GetSize() > m_iSize)
	{
		//释放该通道数据，主要是预录使用
		pList = m_prerecordList.next;
		if (pList == &m_prerecordList)
		{
			// 说明数据全部在录像数据里，且全部用满，直接返回
			return 1;
		}
		BUFFER_BLOCK* pBuffer = list_entry(pList, struct bufferBlock, timeList);
		m_iLength -= pBuffer->pPacket->GetSize();
		pBuffer->pPacket->Release();
		//从通道队列和预队队列删除
		list_del(&pBuffer->channelList);
		list_del(&pBuffer->timeList);
		//添加到空队列
		list_add(&pBuffer->timeList, &m_emptyList);		
	}
	
	//从空队列里取出来,如果出错说明队列太小了
	pList = m_emptyList.next;
	assert (pList != &m_emptyList);
	//释放空队列
	list_del(pList);
	
	//映射对应结构的指针
	BUFFER_BLOCK* pBuffer = list_entry(pList, struct bufferBlock, timeList);
	
	//取到的数据进行赋值并存放存相应的对列
	pPkt->AddRef();
	pBuffer->iChannel = iChannel;
	pBuffer->pPacket = pPkt;
	m_iLength += pPkt->GetSize();
	
	if (m_bDataGetting[iChannel])
	{
		//录像队列非空，并且该通道数据非空，直接放在录像数据后面
		list_add_tail(&pBuffer->timeList, &m_recordList);
	}
	else
	{
		list_add_tail(&pBuffer->timeList, &m_prerecordList);
	}

	list_add_tail(&pBuffer->channelList, &m_channelList[iChannel]);

	return 0;
}

///
///	\b Description		:	取录像数据\n
/// 		直接判断通道列表，如果不空的话，说明有数据，直接取数据，并释放队列到空列表。
///			说明：调用该之前必须调用setBuffer();
/// \param [out] pPkt	:	录像包地址
/// \param [out] pChannel	: 录像通道号
///	\return 0：成功 1：失败
///
///	\b Revisions		:	
///		- 2007-12-14		yuan_shiyong		Create
///
int CDevBufManager::popBuffer(CPacket **pPkt, int iChannel)
{
	CGuard guard(m_bufferMutex);

	if (list_empty(&m_channelList[iChannel]))
	{
		m_bDataGetting[iChannel] = FALSE;
	}

	if (m_bDataGetting[iChannel] == FALSE)
	{
		return 1;
	}

	struct list_head *pList = m_channelList[iChannel].next;
	BUFFER_BLOCK* pBuffer = list_entry(pList, struct bufferBlock, channelList);
	
	*pPkt = pBuffer->pPacket;
	
	m_iLength -= (*pPkt)->GetSize();
	
	//释放队列
	list_del(&pBuffer->channelList);
	list_del(&pBuffer->timeList);
	//添加到空队列
	list_add(&pBuffer->timeList, &m_emptyList);	

	
	return 0;
}

///
///	\b Description		:	设置缓冲区，调整数据\n
///			根据需要的预录时间，调整数据。主要是把数据队列从预录队列分离到录像队列，并同时去除以前不用的预录数据
/// \param iSeconds		：	取多少秒之前的数据
/// \param iChannel		：	录像通道号
///	\return 
///
///	\b Revisions		:	
///		- 2007-12-14		yuan_shiyong		Create
///
void CDevBufManager::setBuffer(int iSeconds, int iChannel)
{
	CGuard guard(m_bufferMutex);
	if (m_bDataGetting[iChannel] || (list_empty(&m_channelList[iChannel])))
	{
		//不需要处理，已经取预录数据
		tracepoint();
		return;
	}
	
	//说明通道数据在预录队列里，根据时间查找预录队列	
	struct list_head *pList = NULL;
	BUFFER_BLOCK* pBuffer = NULL;

#ifndef WIN32
	DHTIME dhtime;
	DHTIME curtime;

	SYS_getsystime(&curtime);
			
	for (pList = m_channelList[iChannel].prev; pList != &m_channelList[iChannel]; pList = pList->prev)
	{
		pBuffer = list_entry(pList, struct bufferBlock, channelList);
		if (getFirstFrameTime(pBuffer->pPacket, &dhtime) == TRUE)
		{
			//解决下面年不准的问题
			dhtime.year = curtime.year;
			//trace("curtime %d-%d-%d %d:%d:%d\n", curtime.year, curtime.month, curtime.day, curtime.hour, curtime.minute, curtime.second);
			//trace("time %d-%d-%d %d:%d:%d\n", dhtime.year, dhtime.month, dhtime.day, dhtime.hour, dhtime.minute, dhtime.second);
			//trace("value %d, %d\n", ABS(changeto_second(curtime, dhtime)), iSeconds);
			if (ABS(changeto_second(curtime, dhtime)) >= iSeconds)
			{
				break;
			}
		}
	}
		
	if (pList == &m_channelList[iChannel])
	{
		//说明所有的数据都需要
		pList = m_channelList[iChannel].next;
	}
	else
	{
		//释放不用的通道预录数据
		delList(&m_channelList[iChannel], pList);
	}
	
	// 过渡掉最开始非I帧的数据
	int pos = 0;
	while(pList != &m_channelList[iChannel])
	{
		pBuffer = list_entry(pList, struct bufferBlock, channelList);
		if ((pos = findFirstFrame(pBuffer->pPacket)) >= 0)
		{
			CPacket* oldPkt = pBuffer->pPacket;
			CPacket* newPkt = g_PacketManager.GetPacket(oldPkt->GetLength());
			assert(newPkt != NULL);
			newPkt->SetLength(newPkt->GetSize() - pos);
			memcpy(newPkt->GetBuffer(), oldPkt->GetBuffer() + pos, newPkt->GetLength());
			//填充一下头
			PKT_HEAD_INFO* oldHead = (PKT_HEAD_INFO*)oldPkt->GetHeader();
			PKT_HEAD_INFO* newHead = (PKT_HEAD_INFO*)newPkt->GetHeader();
			int i = 0;
			for (; i < FRAME_MAX_NUM; i++)
			{
				if (oldHead->FrameInfo[i].FrameType == PACK_TYPE_FRAME_I)
				{
						break;
				}
			}

			int j = 0;
			
			newHead->PacketInfo = oldHead->PacketInfo;
			for (; i < FRAME_MAX_NUM; i++,j++)
			{
				memcpy(&newHead->FrameInfo[j], &oldHead->FrameInfo[i], sizeof(FRAMEINFO));			
			}
			
			if (j < FRAME_MAX_NUM - 1)
			{
				newHead->FrameInfo[j].FrameType = 0xff;
			}
			
			for (int k = 0; k < j; k++)
			{
				newHead->FrameInfo[k].FramePos -= pos;
			}
			
			oldPkt->Release();
			pBuffer->pPacket = newPkt;
			break;
		}
		pList = pList->next;
	}
	
#else
	pList = m_channelList[iChannel].next;
#endif
	//此时通道指针已经自动连接起来，只要拆开预录数据的指针，链到录像队列即可
	for (; pList != &m_channelList[iChannel]; pList = pList->next)
	{
		pBuffer = list_entry(pList, struct bufferBlock, channelList);
		list_del(&pBuffer->timeList);
		list_add_tail(&pBuffer->timeList, &m_recordList);
	}

	m_bDataGetting[iChannel] = TRUE;
}

///
///	\b Description		:	清除通道数据\n
/// \param iChannel		:	通道号
/// \param 
///
///	\b Revisions		:	
///		- 2007-12-17		yuan_shiyong		Create
///
void CDevBufManager::clear(int iChannel)
{
	CGuard guard(m_bufferMutex);
	
	delList(&m_channelList[iChannel], &m_channelList[iChannel]);
	
	m_bDataGetting[iChannel] = FALSE;
}

///
///	\b Description		:	删除pFrom到pTo之间的通道列表,注意pFrom和pTo不会被删除\n
/// \param pFrom		:	开始队列指针
/// \param pTo			:	结束队列指针
///	\return 
///
///	\b Revisions		:	
///		- 2007-12-14		yuan_shiyong		Create
///
void CDevBufManager::delList(struct list_head* pFrom, struct list_head* pTo)
{
	struct list_head *pList = NULL;
	BUFFER_BLOCK* pBuffer = NULL;
	
	for (pList = pFrom->next; pList != pTo; )
	{
		//释放队列
		pBuffer = list_entry(pList, struct bufferBlock, channelList);

		m_iLength -= pBuffer->pPacket->GetSize();
		pBuffer->pPacket->Release();

		pList = pList->next;
		list_del(&pBuffer->channelList);
		list_del(&pBuffer->timeList);
		//添加到空队列
		list_add(&pBuffer->timeList, &m_emptyList);
	}
}

void CDevBufManager::dump()
{
	
}
