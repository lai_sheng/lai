
#include "Devices/DeviceManager.h"
#include "System/AppConfig.h"
#include "Devices/DevAlarm.h"
#include "Devices/DevAudioIn.h"
#include "Devices/DevAudioOut.h"
#include "Devices/DevCapture.h"
#include "Devices/DevExCapture.h"
#include "Devices/DevComm.h"
#include "Devices/DevDetect.h"
#include "Devices/DevFrontboard.h"
#include "Devices/DevPtz.h"
#include "Devices/DevSplit.h"
#include "Devices/DevVideo.h"
#include "Configs/ConfigLocation.h"

int g_nAlarmIn = 0;
int g_nNetAlarmIn = 0;
int g_nAlarmOut = 0;
int g_nCapture = 0;
//int g_nPlay = 0;
int g_nAudioIn = 0;
int g_nVideoOut = 0;
//int g_nDecoders = 0;
//逻辑通道数，等于模拟通道数加数字通道数
int g_nLogicNum=0;
PATTERN_SINGLETON_IMPLEMENT(CDeviceManager);

CDeviceManager::CDeviceManager()
{
	trace("CDeviceManager::CDeviceManager()>>>>>>>>>\n");
	m_bStarted = FALSE;
}

CDeviceManager::~CDeviceManager()
{

}

VD_BOOL CDeviceManager::Start()
{
	trace("CDeviceManager::Start()>>>>>>>>>\n");
	if(m_bStarted)
	{
		return FALSE;
	}
/************************************************************************
	创建所有的设备：
	创建顺序：
	视通16路：2824->1500->561 视频->分割->音频->图形->捕获
************************************************************************/
	int i;

	CDevAlarm		* pAlarm;
	//CDevAudio		* pAudio;



	g_nCapture = ICaptureManager::instance()->GetAnalogChnNum();//CDevCapture::GetChannels ();
	g_nLogicNum= ICaptureManager::instance()->GetLogicChnNum();

	g_nNetAlarmIn = g_nLogicNum - g_nCapture;  //网络报警数码 默认等于解码通道数

	
	int iVSD = SystemGetVideoMode();

	//printf ("Come here################  %s %d g_nCapture=%d\n", __FILE__, __LINE__, g_nCapture);	
	//trace("\n\n#########3iVSD:%d\n\n", iVSD);
	for (int i = 0; i < g_nCapture; i++)
	{
		CDevCapture::instance(i)->SetVstd(iVSD);
	}

	//pAudio		=	CDevAudio::instance();
	//pMotionDetector	=  CDevMotionDetect::instance();
	//pLossDetector		=  CDevLossDetect::instance();
	
	g_nVideoOut = CDevVideo::GetChannels();


	infof("Device Local Captures g_nCaptures = %d\n", g_nCapture);
	infof("Device Logic Captures g_nLogicNum = %d\n", g_nLogicNum);
	//assert(N_SYS_CH >= (g_nCapture + g_nDecoders) );
	//printf("\r\n########################g_nCapture:%d\r\n",g_nCapture);

	assert(g_nCapture <= N_SYS_CH);
	assert(g_nLogicNum <= N_SYS_CH);

	g_nAudioIn = CDevAudioIn::GetChannels();


	CDevAudioIn	*pAudioIn[N_AUDIO_IN];
	for (i = 0; i < g_nAudioIn; i++)
	{
		pAudioIn[i]	 =	CDevAudioIn::instance(i);
	}
	m_bStarted = TRUE;
	return TRUE;
}

VD_BOOL CDeviceManager::Stop()
{
	return TRUE;
}


