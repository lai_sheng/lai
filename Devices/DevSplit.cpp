
#include "Devices/DevSplit.h"
//#include "Devices/DevGraphics.h"
#include "APIs/Garphics.h"
#include "Configs/ConfigLocation.h"
#include "Media/ICapture.h"

//extern int g_nCapture;
extern int g_nVideoOut;


CDevSplit* CDevSplit::_instance[] = {NULL, };

CDevSplit* CDevSplit::instance(int index)
{
    assert(index < g_nVideoOut);

    if(NULL == _instance[index])
    {
        _instance[index] = new CDevSplit(index);
    }
    return _instance[index];
}

CDevSplit::CDevSplit(int index) :
m_index(index)
{
#ifdef     SPLIT_API_VERSION_2
    Split2Create(m_index);
    Split2SetVstd(m_index, dwStandard);
#else
    SplitCreate();
#endif

	m_pCaptureM = ICaptureManager::instance();
}

CDevSplit::~CDevSplit()
{
#ifdef     SPLIT_API_VERSION_2
    Split2Destroy(m_index);
#else
    SplitDestory();
#endif
}

VD_BOOL CDevSplit::Combine(uint dwMode, uint dwIndex)
{
    CGuard guard(m_mutexChRect);

#ifdef     SPLIT_API_VERSION_2
    if (!Split2Combine(m_index, dwMode, dwIndex)) 
#else
    if (!SplitCombine(dwMode, dwIndex)) 
#endif
    {
        return TRUE;
    }

    return FALSE;
}

/*!    
    函数名    : GetRect
==  ==================================================================
    功能描述: 获取指定通道的输出区域
    输入参数: iChannel,指示要获取的通道号，如果为CHANNEL_ALL，
            则表示要获取所有通道的区域；
            : pRect,矩形变量的指针，当iChannel为CHANNEL_ALL时，
            该变量为矩形变量数组指针；
*/

VD_BOOL CDevSplit::GetRect(int iChannel, VD_RECT * pRect)
{
    int i;
    VIDEO_RECT video_rect;
    CGuard guard(m_mutexChRect);

    if (iChannel == CHANNEL_ALL)
    {
        for (i = 0; i < m_pCaptureM->GetLogicChnNum(); i++)
        {
#ifdef     SPLIT_API_VERSION_2
            if (!Split2GetRect(m_index, i, &video_rect)) 
#else
            if (!SplitGetRect(i, &video_rect)) 
#endif
            {
                pRect[i].left = video_rect.left;
                pRect[i].top = video_rect.top;
                pRect[i].right = video_rect.right;
                pRect[i].bottom = video_rect.bottom;

                //SplitOutputRectConvert(&pRect[i]);
                adjustRect(&pRect[i], 1);
            }
            else
            {
                return FALSE;
            }
        }
    }
    else
    {
        if ( iChannel < 0 || iChannel >= m_pCaptureM->GetLogicChnNum() )
        {
            return FALSE;
        }
        
#ifdef     SPLIT_API_VERSION_2
        if (!Split2GetRect(m_index, iChannel, &video_rect)) 
#else
        if (!SplitGetRect(iChannel, &video_rect)) 
#endif
        {
            pRect->left = video_rect.left;
            pRect->top = video_rect.top;
            pRect->right = video_rect.right;
            pRect->bottom = video_rect.bottom;

            adjustRect(pRect, 1);        
        }
        else
        {
            return FALSE;
        }
    }

    return TRUE;
}

int CDevSplit::GetBlankRects(VD_RECT * pRect)
{
    VIDEO_RECT video_rects[N_SYS_CH];
#ifdef     SPLIT_API_VERSION_2
    int ret = Split2GetBlankRects(m_index, video_rects);
#else
    int ret = SplitGetBlankRects(video_rects);
#endif
    
    for(int i = 0; i < ret; i++)
    {
        pRect[i].left = video_rects[i].left;
        pRect[i].top = video_rects[i].top;
        pRect[i].right = video_rects[i].right;
        pRect[i].bottom = video_rects[i].bottom;
        adjustRect(&pRect[i], 1);
    }
    return ret;
}

//取到视频输出的区域， 在区域调节或制式改变时该区域会变化。
//只针对HB GB， 其他的直接使用图形区域
VD_BOOL CDevSplit::GetFullRect(VD_RECT *pRect)
{
    VIDEO_RECT video_rect;
    if(SplitGetFullRect(&video_rect))
    {
        return FALSE;
    }
    pRect->left = video_rect.left;
    pRect->top = video_rect.top;
    pRect->right = video_rect.right;
    pRect->bottom = video_rect.bottom;
    return TRUE;
}

VD_BOOL CDevSplit::SetVideoStandard(uint dwStandard)
{
#ifdef     SPLIT_API_VERSION_2
    if (!Split2SetVstd(m_index, dwStandard)) 
#else
    if (!SplitSetVstd(dwStandard)) 
#endif
    {
        return TRUE;
    }
    return FALSE;
}

VD_BOOL CDevSplit::GetCaps(SPLIT_CAPS * pCaps)
{
#ifdef     SPLIT_API_VERSION_2
    if (!Split2GetCaps(m_index, pCaps)) 
#else
    if (!SplitGetCaps(pCaps)) 
#endif
    {
        // 暂不支持画中画功能
        pCaps->dwCombine &= ~BITMSK(SPLITPIP);
#if defined(FUNC_COURT_DISPLAY)
        if (pCaps->dwCombine & BITMSK(SPLIT6))
        {
            pCaps->dwCombine = BITMSK(SPLIT6) | BITMSK(SPLIT1);
        }
#endif
#ifdef _VENDOR_HSK
        if (pCaps->dwCombine & BITMSK(SPLIT8))
        {
            pCaps->dwCombine &=~BITMSK(SPLIT8);
        }
#endif
        return TRUE;
    }
    return FALSE;
}

///进行坐标转换，转换为图形坐标
///flag=0,转化成原始坐标，flag!=0转化成GUI图形坐标
//该函数只是适用于PAL制式N制式时的坐标转换
void CDevSplit::adjustRect(VD_RECT *pRect, int flag/*=0*/)
{
#ifndef WIN32
    double ratex,ratey;
    GRAPHICS_SURFACE surface;
    int fullrectwidth = 704;
    int fullrectheight = 576;
    
    if(CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_PAL)
        fullrectheight = 576;
    else if(CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_NTSC)
        fullrectheight = 480;
        

    if(surface.width!=fullrectwidth)//对于1080P,1080I,720P制式无需调整
    {
         return ;
    }
    else if(fullrectwidth == surface.width && fullrectheight == surface.height)
    {
        return;
    }
    else//仅D1输出的时候，制式变化需要调整
    {
        if(flag)
        {
            ratex = (double)surface.width /(double)fullrectwidth;
            ratey= (double)surface.height /(double)fullrectheight;
        }
        else
        {
            ratex = (double)fullrectwidth/(double)surface.width;
            ratey = (double)fullrectheight/(double)surface.height;
        }
        pRect->left = (int)(ratex * pRect->left);
        pRect->right = (int)(ratex * pRect->right);
        pRect->top = (int)(ratey * pRect->top);
        pRect->bottom = (int)(ratey * pRect->bottom);
    }
#endif
}
#ifdef _SURPORT_SWITCH_
11111111111
VD_BOOL CDevSplit::Switch(uint chn0,uint chn1)
{
      if (!SplitSwitch(chn0, chn1)) 
    {
        return TRUE;
    }
    return FALSE;
}
#endif

#ifdef 	_SUPPORT_PREVIEWZOOM_
2222222
VD_BOOL CDevSplit::Zoom(int iPlayerChannel, VIDEO_RECT *pDestRect, VIDEO_RECT * pSrcRect/*=NULL*/)
{
	return TRUE;
}
#endif

