#include "APIs/System.h"
#include "Devices/DevPtz.h"
#include "System/BaseTypedef.h"
#include "Configs/ConfigDigiChManager.h"

#define TP_MJ_NUM  0
CDevPtz* CDevPtz::_instance = NULL;

#define UART_CMD_LEN 64

CDevPtz* CDevPtz::instance(void)
{
    if(NULL == _instance)
    {
        _instance = new CDevPtz();
    }
    return _instance;
}
void CDevPtz::ThreadProc()
{
    char l_dbData = 0;
	VD_MSG msg;
	char databuff[UART_CMD_LEN] =	{0} ;
	int datalen = 0;
	int count = 0; 
	int PreCmd = 0;
	char cmdbuf[16] = {0};
	int OutTime = 50;//毫秒
   
    while (m_bLoop) 
    {
        if (m_bNeedBlocked)
        {
            m_Semaphore.Pend();
        }
        SystemSleep(20); 
    }
}


CDevPtz::CDevPtz(): CThread("DevPtz", TP_PTZ,TP_MJ_NUM), m_sigData(32)
{
    PtzCreate();

    /* 串口创建时先设置默认属性，避免串口往外乱发数据 */
    PTZ_ATTR sPtzAttr;
    sPtzAttr.baudrate = 9600;
    sPtzAttr.databits = 8;
    sPtzAttr.parity   = 0;
    sPtzAttr.stopbits = 1;
    PtzSetAttribute(&sPtzAttr);

    memset((void*)m_Buf, 0, 64);
    memset((void *)&m_Attr, 0, sizeof(PTZ_ATTR));
    m_Len = 0;
    m_debug = TRUE;
    CommCreate();
    Open();
    CreateThread();
}

CDevPtz::~CDevPtz()
{
    DestroyThread();
    Close();
    PtzDestory();
}

VD_BOOL CDevPtz::Start(CObject * pObj, SIG_DEV_PTZ_DATA pProc)
{
    if(m_sigData.Attach(pObj, pProc) < 0)
    {
        trace("attach error\n");
        return FALSE;
    }
    m_bNeedBlocked = FALSE;
    m_Semaphore.Post();

    return TRUE;
}

VD_BOOL CDevPtz::Stop(CObject * pObj, SIG_DEV_PTZ_DATA pProc)
{
    if (m_sigData.Detach(pObj, pProc) == 0)
    {
        m_bNeedBlocked = TRUE;
    }

    return TRUE;
}

int CDevPtz::Open(void)
{
    int ret = !PtzOpen(); 
    return ret;
}

int CDevPtz::SetAttribute(PTZ_ATTR *pattr)
{
    m_Len = 0;
    if (!Close())
    {
        return FALSE;
    }

    if (!Open())
    {
        return FALSE;
    }

    if (PtzSetAttribute(pattr))
    {
        return FALSE;
    }    

    memcpy((void *)&m_Attr, pattr, sizeof(PTZ_ATTR));    
    
    return TRUE;
}

int CDevPtz::SetPTZHead(void *sbuf,int length)
{
    if(!sbuf)
    {
        return FALSE;
    }
    memcpy((void *)m_Buf,sbuf,length);
    m_Len = length;
    return TRUE;
}

int CDevPtz::Write(void* pData, uint len,int iCh)
{
    if(!pData)
    {
        return FALSE;
    }

    int result;
    memcpy((void *)&m_Buf[m_Len],pData,(int)len);
    if (m_debug)
    {
        trace("CDevPtz::Write Run<%d> : ",(int)len+m_Len);
        for(int i = 0; i < (int)len+m_Len; i++)
        {
            trace("0x%02x, ",m_Buf[i]);
        }
        trace("\n");
    }
    if ((result = PtzWrite((void *)m_Buf,(int)len+m_Len)) > 0)
    {
        return result;    
    }
    else 
    {
        trace("*************** Ptz Write fail*********\n");
    }
    return FALSE;
}

void CDevPtz::SetDebug(VD_BOOL bNeedDebug)
{
    m_debug = bNeedDebug;
}
int CDevPtz::Read(void *pData, int len)
{
    return PtzRead(pData, len);
}

int CDevPtz::Close(void)
{
    return !PtzClose();
}


