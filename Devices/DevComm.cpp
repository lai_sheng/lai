

#include "Devices/DevComm.h"

//#define DEBUG_COMM		// 当需要使用串口调试时，打开此宏

CDevComm* CDevComm::_instance = NULL;

CDevComm* CDevComm::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevComm();
	}
	return _instance;
}

void CDevComm::ThreadProc()
{
	char l_dbData;

	while (m_bLoop) 
	{
		if (m_bNeedBlocked)
		{
			m_Semaphore.Pend();
		}
		Read(&l_dbData, 1);
		//trace("read is %x\n", l_dbData);
		m_sigData(&l_dbData, 1); 
	}
}

CDevComm::CDevComm() : CThread("DevComm", TP_COM), m_sigData(1)
{
	CommCreate();

#ifndef DEBUG_COMM
	if (Open())
	{
		trace("CDevComm::CommOpen failure*******\n");
	}
	else
	{
		m_bNeedBlocked = TRUE;
		CreateThread();
	}
#endif

	m_pObj = NULL;
}

CDevComm::~CDevComm()
{
	CommDestory();
}

int CDevComm::Start(CObject * pObj, SIG_DEV_COMM_DATA pProc)
{
	if(m_sigData.Attach(pObj, pProc) < 0)
	{
		trace("CDevComm::Start():attach error\n");
		return -1;
	}

	// 由于串口只能应用一个，所以可保存当前引用的对象，主要是删除时直接删除
	m_pObj = pObj;
	m_pSigDev = pProc;

	m_bNeedBlocked = FALSE;
	m_Semaphore.Post();
	
	return 0;
}

int CDevComm::Stop()
{
	if (m_pObj == NULL)
	{
		return -1;
	}

	if (m_sigData.Detach(m_pObj, m_pSigDev))
	{
		tracepoint();
		return -2;
	}
	
	//其实什么也没做
	if (Close())
	{
		return -3;
	}
	
	m_bNeedBlocked = TRUE;
	m_pObj = NULL;

	return 0;
}

int CDevComm::Open()
{
	return CommOpen() > 0 ? 0 : 1;
}

int CDevComm::Close()
{
	return CommClose();// 返回0表示成功
}

int CDevComm::SetAttribute(COMM_ATTR * pAttr)
{
	return CommSetAttribute(pAttr); // 返回0表示成功
}

int CDevComm::Read(void* pData, uint nBytes)
{
	return CommRead(pData, nBytes);
}

int CDevComm::Write(void* pData, uint len)
{
	return CommWrite(pData, len);
}

int CDevComm::Purge (uint dwFlags)
{
	return CommPurge(dwFlags); // 返回0表示成功
}

int CDevComm::AsConsole(int flag)
{
	return CommAsConsole(flag); // 返回0表示成功
}

void CDevComm::GetConsole(int * value)
{
	CommGetConsol(value);
}

void CDevComm::TestData(void *pdata, int len)
{	
	char *pval = (char *)pdata;
	for (int i = 0;  i < len; i++)
	{
		m_sigData(pval, 1);
		pval++;
	}
}

