#include <math.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "APIs/Net.h"
#include "Net/NetApp.h"
#include "APIs/System.h"
#include "Devices/DevInfo.h"
#include "Functions/WifiLink.h"
#include "Devices/DevExternal.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/LiveCam/LiveCamTrans.h"
#include "APIs/CommonBSP.h"
#include "APIs/CommonBSP_Battery.h"

//#ifdef IPC_A3
//#define WIFI_INFO				"/proc/net/rtl8189fs/wlan0/survey_info"
//#elif  IPC_R2S2
//#define WIFI_INFO				"/proc/net/rtl8188fu/wlan0/survey_info"
//#elif  IPC_R2S1
//#define WIFI_INFO				"/proc/net/rtl8188fu/wlan0/survey_info"
//#elif  IPC_A3S2
//#define WIFI_INFO				"/proc/net/rtl8189fs/wlan0/survey_info"
//#else
//#define WIFI_INFO				"/proc/net/rtl8188fu/wlan0/survey_info"
//#endif
static int  Get_Wifi_Info(char *pContest, int nSize)
{
	if(NULL == pContest) return -1;
	memset(pContest, 0, nSize);

#ifdef IPC_JZ_NEW
	switch(PlatformGetHandle()->WifiModule){
		case RTL8188FU:
			snprintf(pContest, nSize, "%s", "/proc/net/rtl8188fu/wlan0/survey_info");
			break;
		case RTL8189FS:
			snprintf(pContest, nSize, "%s", "/proc/net/rtl8189fs/wlan0/survey_info");
			break;
		case RTL8821CS:
			snprintf(pContest, nSize, "%s", "/proc/net/rtl8821cs/wlan0/survey_info");
			break;	
		default: 
			break;
	}
#elif defined(IPC_A3)
	snprintf(pContest, nSize, "%s", "/proc/net/rtl8189fs/wlan0/survey_info");
#else
	snprintf(pContest, nSize, "%s", "/proc/net/rtl8188fu/wlan0/survey_info");
#endif

	_printd("Get_Wifi_Info path=%s\n",pContest);
	return 0;
}

static int  Get_Device_Name(char *pDevName, int nSize)
{
	if(NULL == pDevName) return -1;
	memset(pDevName, 0, nSize);
#ifdef IPC_JZ_NEW
	switch(PlatformGetHandle()->ProductModel){
		case ProductM_R2S1:
			snprintf(pDevName, nSize, "%s", "R2S1");
			break;
		case ProductM_R2S2:
			snprintf(pDevName, nSize, "%s", "R2S2");
			break;
		case ProductM_A3S1:	
			snprintf(pDevName, nSize, "%s", "A3S1");
			break;
		case ProductM_A3S2:
			snprintf(pDevName, nSize, "%s", "A3S2");
			break;
		case ProductM_R2P2:
			snprintf(pDevName, nSize, "%s", "R2P2");
			break;			
		case ProductM_A3:
			snprintf(pDevName, nSize, "%s", "A3");
			break;				
		case ProductM_P2:
			snprintf(pDevName, nSize, "%s", "P2");
			break;			
		case ProductM_G3S2:
			snprintf(pDevName, nSize, "%s", "G3S2");
			break;	
		case ProductM_Q1:
			snprintf(pDevName, nSize, "%s", "Q1");
			break;
		case ProductM_Q1PRO:
			//snprintf(pDevName, nSize, "%s", "Q1S");
			snprintf(pDevName, nSize, "%s", "Q1PRO");
			break;	
		case ProductM_Q2:
			if (PlatformGetHandle()->ClientCode == Client_YD_DEV || 
				PlatformGetHandle()->ClientCode == Client_YD_SLD){

				snprintf(pDevName, nSize, "%s", "HDC-55");
			}else{
				snprintf(pDevName, nSize, "%s", "Q2");
			}
			break;
		case ProductM_Q2PRO:
			if (PlatformGetHandle()->ClientCode == Client_YD_DEV || 
				PlatformGetHandle()->ClientCode == Client_YD_SLD){

				snprintf(pDevName, nSize, "%s", "HDC-55");
			}else{
				snprintf(pDevName, nSize, "%s", "Q2PRO");
			}		
			break;		
		case ProductM_QT2:
			snprintf(pDevName, nSize, "%s", "QT2");
			break;			
		case ProductM_A3PRO:
			snprintf(pDevName, nSize, "%s", "A3PRO");
			break;
		case ProductM_A5PRO:
			snprintf(pDevName, nSize, "%s", "A5PRO");
			break;			
		case ProductM_A5:
			if (PlatformGetHandle()->ClientCode == Client_YD_DEV || 
				PlatformGetHandle()->ClientCode == Client_YD_SLD){
				snprintf(pDevName, nSize, "%s", "HDC-51");
			}else{
				snprintf(pDevName, nSize, "%s", "A5");
			}
			break;				
		default: 
			break;
	}
#elif TUPU
	snprintf(pDevName, nSize, "%s", "A2");
#elif IPC_JZ
	switch(BAT_PlatformGetHandle()->ProductModel){
		case BAT_ProductM_S1:
			snprintf(pDevName, nSize, "%s", "S1");
			break;
		case BAT_ProductM_S1Single:
			snprintf(pDevName, nSize, "%s", "S1Single");
			break;
		case BAT_ProductM_I9M:
			{
				if (BAT_BATTERY_LITHIUM == BAT_PlatformGetHandle()->BatteryType) {
				    snprintf(pDevName, nSize, "%s", "I9M-H");
				} else {
				    snprintf(pDevName, nSize, "%s", "I9M");
				}
			}
			break;
		case BAT_ProductM_I10:
			snprintf(pDevName, nSize, "%s", "I10");
			break;
		case BAT_ProductM_S50:
			snprintf(pDevName, nSize, "%s", "S50");
			break;
		case BAT_ProductM_F5:
			snprintf(pDevName, nSize, "%s", "F5");
			break;			
		default:
			break;
	}
#elif IPC_FH
	snprintf(pDevName, nSize, "%s", "R2");
#elif IPC_P2
	snprintf(pDevName, nSize, "%s", "P2");
#elif IPC_A3
	snprintf(pDevName, nSize, "%s", "A3");
#elif IPC_HISI
	snprintf(pDevName, nSize, "%s", "A2");
#endif

	_printd("Get_Device_Name name=%s\n",pDevName);
	return 0;
}

int  Get_Version_Name(char *pVerName, int nSize)
{
	if(NULL == pVerName) return -1;
	memset(pVerName, 0, nSize);
#ifdef IPC_JZ_NEW
	#if 0
	switch(PlatformGetHandle()->ProductModel){
		case ProductM_R2S1:
			snprintf(pVerName, nSize, "%s", "R2S1");
			break;
		case ProductM_R2S2:
			snprintf(pVerName, nSize, "%s", "R2S2");
			break;
		case ProductM_A3S1:	
			snprintf(pVerName, nSize, "%s", "A3S1");
			break;
		case ProductM_A3S2:
			snprintf(pVerName, nSize, "%s", "A3S2");
			break;
		case ProductM_R2P2:
			snprintf(pVerName, nSize, "%s", "R2P2");
			break;			
		case ProductM_A3:
			snprintf(pVerName, nSize, "%s", "A3");
			break;
		case ProductM_P2:
			snprintf(pVerName, nSize, "%s", "P2");
			break;
		case ProductM_G3S2:
			snprintf(pVerName, nSize, "%s", "G3S2");
			break;
		case ProductM_Q1:
			snprintf(pVerName, nSize, "%s", "Q1");
			break;
		case ProductM_Q1PRO:
			snprintf(pVerName, nSize, "%s", "Q1S");
			break;			
		default: 
			break;
	}
	#endif

	if( ProductM_A3 == PlatformGetHandle()->ProductModel){
		snprintf(pVerName, nSize, "%s", "A3");
	}else if(ProductM_Q1 == PlatformGetHandle()->ProductModel){
		switch(PlatformGetHandle()->ClientCode){
			case Client_CN:
				snprintf(pVerName, nSize, "%s", "Q1_CN");
				break;
			case Client_EN:
				snprintf(pVerName, nSize, "%s", "Q1_EN");
				break;
			default: 
				snprintf(pVerName, nSize, "%s", "Q1_CN");
				break;
		}	
	}else if(ProductM_A5 == PlatformGetHandle()->ProductModel){
		switch(PlatformGetHandle()->ClientCode){
			case Client_EN:
				snprintf(pVerName, nSize, "%s", "A5_EN");
				break;
			case Client_CN:
				snprintf(pVerName, nSize, "%s", "A5_CN");
				break;				
			case Client_YD_DEV:	
				snprintf(pVerName, nSize, "%s", "A5_YD_DEV");
				break;	
			case Client_YD_SLD:	
				snprintf(pVerName, nSize, "%s", "A5_YD_SLD");
				break;
			default: 
				snprintf(pVerName, nSize, "%s", "A5");
				break;
		}
	}else if (ProductM_A3PRO == PlatformGetHandle()->ProductModel || 
			  ProductM_A5PRO == PlatformGetHandle()->ProductModel  ||
			  ProductM_Q1PRO == PlatformGetHandle()->ProductModel ||
			  ProductM_Q2 == PlatformGetHandle()->ProductModel ||
			  ProductM_Q2PRO == PlatformGetHandle()->ProductModel
			  ){
		switch(PlatformGetHandle()->ClientCode){
			case Client_CN:
				snprintf(pVerName, nSize, "%s", "T20X_CN");
				break;
			case Client_EN:
				snprintf(pVerName, nSize, "%s", "T20X_EN");
				break;
			case Client_005:	
				snprintf(pVerName, nSize, "%s", "T20X_005");
				break;
			case Client_YD:	
				snprintf(pVerName, nSize, "%s", "T20X_YD");
				break;	
			case Client_YD_DEV:	
				snprintf(pVerName, nSize, "%s", "T20X_YD_DEV");
				break;	
			case Client_YD_SLD:	
				snprintf(pVerName, nSize, "%s", "T20X_YD_SLD");
				break;
			case Client_SN:	
				snprintf(pVerName, nSize, "%s", "T20X_SN");
				break;	
			default: 
				break;
		}
	}
	else if(ProductM_QT2 == PlatformGetHandle()->ProductModel){
		switch(PlatformGetHandle()->ClientCode){
			case Client_CN:
				snprintf(pVerName, nSize, "%s", "T30_CN");
				break;
			case Client_EN:
				snprintf(pVerName, nSize, "%s", "T30_EN");
				break;
			case Client_005:	
				snprintf(pVerName, nSize, "%s", "T30_005");
				break;
			case Client_YD:	
				snprintf(pVerName, nSize, "%s", "T30_YD");
				break;	
			case Client_YD_DEV:	
				snprintf(pVerName, nSize, "%s", "T30_YD_DEV");
				break;	
			case Client_YD_SLD:	
				snprintf(pVerName, nSize, "%s", "T30_YD_SLD");
				break;
			case Client_SN:	
				snprintf(pVerName, nSize, "%s", "T30_SN");
				break;		
			default: 
				break;
		}
	}else{
		switch(PlatformGetHandle()->ClientCode){
			case Client_CN:
				snprintf(pVerName, nSize, "%s", "T20_CN");
				break;
			case Client_EN:
				snprintf(pVerName, nSize, "%s", "T20_EN");
				break;
			case Client_005:	
				snprintf(pVerName, nSize, "%s", "T20_005");
				break;
			case Client_YD:	
				snprintf(pVerName, nSize, "%s", "T20_YD");
				break;	
			case Client_YD_DEV:	
				snprintf(pVerName, nSize, "%s", "T20_YD_DEV");
				break;	
			case Client_YD_SLD:	
				snprintf(pVerName, nSize, "%s", "T20_YD_SLD");
				break;	
			case Client_SN:	
				snprintf(pVerName, nSize, "%s", "T20_SN");
				break;		
			default: 
				break;
		}
	}
#elif TUPU
	snprintf(pVerName, nSize, "%s", "A2_TUPU_NetPackage");
#elif IPC_JZ
	if (BAT_ProductM_S1 == BAT_PlatformGetHandle()->ProductModel) {
		snprintf(pVerName, nSize, "%s", "S1_Camera");
	} else if (BAT_ProductM_S1Single == BAT_PlatformGetHandle()->ProductModel) {
		snprintf(pVerName, nSize, "%s", "S1Single_Camera");
	} else if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel) {
		switch(BAT_PlatformGetHandle()->ClientCode){
			case BAT_Client_CN:
				snprintf(pVerName, nSize, "%s", "I9M_CN");
				break;
			case BAT_Client_EN:
				snprintf(pVerName, nSize, "%s", "I9M_EN");
				break;
			case BAT_Client_005:	
				snprintf(pVerName, nSize, "%s", "I9M_005");
				break;
			case BAT_Client_YD:	
				snprintf(pVerName, nSize, "%s", "I9M_YD");
				break;	
			default: 
				break;
		}
	} else if (BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel) {
		switch(BAT_PlatformGetHandle()->ClientCode){
			case BAT_Client_CN:
				snprintf(pVerName, nSize, "%s", "I10_CN");
				break;
			case BAT_Client_EN:
				snprintf(pVerName, nSize, "%s", "I10_EN");
				break;
			case BAT_Client_005:	
				snprintf(pVerName, nSize, "%s", "I10_005");
				break;
			case BAT_Client_YD:	
				snprintf(pVerName, nSize, "%s", "I10_YD");
				break;	
			default: 
				break;
		}
	} else if (BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel) {
		switch(BAT_PlatformGetHandle()->ClientCode){
			case BAT_Client_CN:
				snprintf(pVerName, nSize, "%s", "S50_CN");
				break;
			case BAT_Client_EN:
				snprintf(pVerName, nSize, "%s", "S50_EN");
				break;
			case BAT_Client_005:	
				snprintf(pVerName, nSize, "%s", "S50_005");
				break;
			case BAT_Client_YD:	
				snprintf(pVerName, nSize, "%s", "S50_YD");
				break;	
			default: 
				break;
		}
	} else if (BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel) {
		switch(BAT_PlatformGetHandle()->ClientCode){
			case BAT_Client_CN:
				snprintf(pVerName, nSize, "%s", "F5_CN");
				break;
			case BAT_Client_EN:
				snprintf(pVerName, nSize, "%s", "F5_EN");
				break;
			case BAT_Client_005:	
				snprintf(pVerName, nSize, "%s", "F5_005");
				break;
			case BAT_Client_YD:	
				snprintf(pVerName, nSize, "%s", "F5_YD");
				break;	
			default: 
				break;
		}
	}
#elif  IPC_FH
	snprintf(pVerName, nSize, "%s", "R2");
#elif  IPC_P2
	snprintf(pVerName, nSize, "%s", "P2");
#elif  IPC_A3
	snprintf(pVerName, nSize, "%s", "A3");
#elif  IPC_HISI
	snprintf(pVerName, nSize, "%s", "IPC_A2_NetPackage");
#endif

	_printd("Get_Version_Name name=%s\n",pVerName);
	return 0;
}

int  Get_Version_Path(char *pVerPath, int nSize)
{
	if(NULL == pVerPath) return -1;
	memset(pVerPath, 0, nSize);
	
#ifdef IPC_JZ_NEW
	snprintf(pVerPath, nSize, "%s", "/app/ipcam/verinfo");
#elif TUPU
	snprintf(pVerPath, nSize, "%s", "/mnt/config/version");
#elif IPC_JZ
	snprintf(pVerPath, nSize, "%s", "/app/verinfo");
#elif IPC_HISI
	snprintf(pVerPath, nSize, "%s", "/mnt/config/version");
#else
	snprintf(pVerPath, nSize, "%s", "/app/ipcam/verinfo");
#endif
	_printd("Get_Version_Path path=%s\n",pVerPath);
	return 0;
}

int  GetDevVersion(char *pVerPath,int iVerMaxLen/*pVerPath 最大可支持长度*/)
{
	char  strVerName[64] = {0};
	char* ReadVersion = NULL;

	if(NULL == pVerPath){
		_printd("pVerPath is null");
		return 0;
	}
	if(iVerMaxLen <= 0){
		_printd("iVerMaxLen is error(%d)",iVerMaxLen);
		return 0;		
	}
	
	JudgeFileIsExis(0,0);

	Get_Version_Name(strVerName, sizeof(strVerName));

	if (NULL != (ReadVersion = ReadVersionToFile()) && NULL  != strstr(ReadVersion, strVerName))
	{
		strncpy(pVerPath , ReadVersion,iVerMaxLen);
	}
	else
	{
		char strVerPath[64] = {0};
		Get_Version_Path(strVerPath, sizeof(strVerPath));
		remove(strVerPath);
		JudgeFileIsExis(0,0);
		if(NULL != (ReadVersion = ReadVersionToFile()))
		{
			strncpy(pVerPath , ReadVersion,iVerMaxLen);
		}
	}
	return 0;
}
int  Get_Video_Nfps_Igop(int *pnfps, int *pigop)
{
#ifdef IPC_JZ_NEW
	*pnfps = 15;
	*pigop = 30;
#elif IPC_HISI
	*pnfps = 20;
	*pigop = 40;
#elif IPC_FH
	*pnfps = 25;
	*pigop = 50;
#else
	*pnfps = 15;
	*pigop = 30;
#endif
	_printd("get nfps=%d igop=%d\n", *pnfps, *pigop);
	return 0;
}

void JudgeFileIsExis(int type, int data)
{
	char strVerPath[64] = {0};
	Get_Version_Path(strVerPath, sizeof(strVerPath));
	
	if (type == 0)
	{
		if (0 != access(strVerPath, 0))
		{
			FILE *fp = fopen(strVerPath, "wb");
			if (fp == NULL)
			{
				return;
			}
			char mversion[128] = {0};
			char strVerName[16] = {0};

			Get_Version_Name(strVerName, sizeof(strVerName));
			sprintf(mversion,"%s_V1.01", strVerName);

			fwrite(mversion,1,strlen(mversion),fp);
			fclose(fp);
		}
	}
	else
	{
		FILE *fp = fopen(AUDIO_SWITCH_FILE,"wb");
		char mAudioBuf[5] = {0};
		
		sprintf(mAudioBuf,"%d",data);
		fwrite(mAudioBuf,1,strlen(mAudioBuf),fp);
		fclose(fp);
	}
}


char* ReadVersionToFile()
{
	static char mRtmpAddr[128] = {0};
	char strVerPath[64] = {0};
	Get_Version_Path(strVerPath, sizeof(strVerPath));

	FILE *fp  = fopen(strVerPath,"rb");
	int mbyte = 0;
	int cnt = 0; 
	
	if (fp == NULL)
	{
		return NULL;
	}

	memset(mRtmpAddr,0,128);
	
	while(1)
	{
	    mbyte = fgetc(fp);
	    if(mbyte == EOF || mbyte == '\0'  || mbyte == '\n') break;
	    mRtmpAddr[cnt] = mbyte;
	    cnt ++;
	}
	mRtmpAddr[cnt] = 0;

	fclose(fp);

	return mRtmpAddr;
}


int GetDevModel(char* Model,int Len)
{
	char  strDeviceName[16] = {0};
	if(Model == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	Get_Device_Name(strDeviceName, sizeof(strDeviceName));

	snprintf(Model,Len, strDeviceName);
	return 0;
}

int GetSystemVer(char* Ver,int Len)
{
#if 0
	char* ReadVersion = NULL;

	if(Ver == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	
	
	JudgeFileIsExis(0,0);
#ifndef  TUPU
	if (NULL != (ReadVersion = ReadVersionToFile()) && NULL  != strstr(ReadVersion,"IPC"))
#else
	if (NULL != (ReadVersion = ReadVersionToFile()) && NULL  != strstr(ReadVersion,"A2_TUPU"))
#endif
	{
		strncpy(Ver , ReadVersion,Len);
	}
	else
	{
		return -1;
	}
#else
	FILE *fp = NULL;
	int    i = 0;
	
	if(Ver == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	
	memset(Ver, 0, Len);
	
	char strVerPath[64] = {0};
	Get_Version_Path(strVerPath, sizeof(strVerPath));
	if(NULL == (fp = fopen(strVerPath, "r"))){
		return -1;
	}
	
	if(0 >= (i = fread(Ver, 1, Len, fp))){
		fclose(fp);
		return -1;
	}
	
	if(Ver[i - 1] == '\n')
		Ver[i - 1] = '\0';

	if(i > 10 && Ver[i - 2] == 'g') //去掉debug
		Ver[i - 7] = '\0';
	
	fclose(fp);

#endif
	return 0;
}
#ifdef SUNING
int  Get_PreVersion_Path(char *pVerPath, int nSize)
{
	if(NULL == pVerPath) return -1;
	memset(pVerPath, 0, nSize);
	
#ifdef IPC_JZ_NEW
	snprintf(pVerPath, nSize, "%s", "/app/ipcam/pre_verinfo");
#elif TUPU
	snprintf(pVerPath, nSize, "%s", "/mnt/config/pre_version");
#elif IPC_JZ
	snprintf(pVerPath, nSize, "%s", "/app/pre_verinfo");
#elif IPC_HISI
	snprintf(pVerPath, nSize, "%s", "/mnt/config/pre_version");
#else
	snprintf(pVerPath, nSize, "%s", "/app/ipcam/pre_verinfo");
#endif
	_printd("Get_pre_Version_Path path=%s\n",pVerPath);
	return 0;
}

int GetSystemPreVer(char* Ver,int Len)
{
	FILE *fp = NULL;
	int    i = 0;
	
	if(Ver == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	
	memset(Ver, 0, Len);
	
	char strVerPath[64] = {0};
	Get_PreVersion_Path(strVerPath, sizeof(strVerPath));
	if(NULL == (fp = fopen(strVerPath, "r"))){
		return -1;
	}
	
	if(0 >= (i = fread(Ver, 1, Len, fp))){
		fclose(fp);
		return -1;
	}
	
	if(Ver[i - 1] == '\n')
		Ver[i - 1] = '\0';

	if(i > 10 && Ver[i - 2] == 'g') //去掉debug
		Ver[i - 7] = '\0';
	
	fclose(fp);

	return 0;
}

#endif
int GetConnWifi(char* WifiName,int Len)
{
#ifdef IPC_JZ
	ZRT_CAM_WIFI_Status WifiStatu;

	if(WifiName == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	
	memset(WifiName,0,Len);
	
	g_DevExternal.Dev_GetWiFi(&WifiStatu);

	if(WifiStatu.connectionStatus != WIFI_CONNECT_FAILED &&
		WifiStatu.connectionStatus != WIFI_DISCONNECTED) 	
	{
		strncpy(WifiName,WifiStatu.ssid,Len);
	}
	else
	{
		return -1;
	}
	
	return 0;
#else
	FILE *fd = NULL;
	char readline[512];
	char *str = NULL;
	int ret = -1;
	int bIsString = 0;

	if(WifiName == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	if (Len > 64)
	{
		_printd("The Len is error,len:%d", Len);
		return -1;
	}
	memset(WifiName,0,Len);
	
	fd = fopen(STORE_WIFI_CONFIG, "r");
	
	if( fd == NULL ) {	
		_printd("file open can not create file:%s !!! ",STORE_WIFI_CONFIG);
		return -1;
	}
	
	while (!feof(fd))
	{
		if (!fgets(readline, 512 , fd))
			break;
		//有引号为字符串，否则汉字
		if((str = strstr(readline,"ssid=\"")) != NULL){
			bIsString = 1;
			break;
		}else if((str = strstr(readline,"ssid=")) != NULL) {
			bIsString = 0;
			break;
		}
	}
#if 1	
	switch (bIsString){
	case 1: //字符串
		if(str != NULL){
			strncpy(WifiName,str+6,Len);
			
			if(Len > strlen(str+6)){
				WifiName[strlen(str+6) - 2] = 0;
			}else {
				WifiName[Len - 1] = 0;
			}
			ret = 0;
			_printd("Get WifiName:%s\n",WifiName);
		}
		break;
	case 0://中文	
		if(str != NULL){
			char OriBuf[64] = {0};
			char tBuf[3] = {0};
			
			long tmpl = 0;
			unsigned char tmpc = 0;
			int i = 0;
			int j = 0;
			
			strncpy(OriBuf,str+5,Len);		
			if(Len > strlen(str+5)){
				OriBuf[strlen(str+5) - 1] = 0;
			}else {
				OriBuf[Len - 1] = 0;
			}
			_printd("Get OriBuf:%s\n",OriBuf);
			
			for (i = 0;i < Len;i += 2){
				if (OriBuf[i] != 0 && OriBuf[i+1] != 0){
					sprintf(tBuf,"%c%c",OriBuf[i],OriBuf[i+1]);				
					//tmp = atoi(tBuf);
					tmpl = strtol(tBuf,NULL,16);
					tmpc =(unsigned char) (tmpl & 0x000000FF);
					//_printd("!!!Get tbuf:%x\n",tmpc);
					if (j <= Len){
						WifiName[j] = (unsigned char)tmpc;
						_printd("Get WifiName:%x\n",(unsigned char)WifiName[j]);
						j++;
					}else {
						break;
					}
				}else {
					break;
				}
			}
			ret = 0;
		}
		#if 0 //test
			char tbuf[48] = {0x41, 0x41, 0xe4, 0xb8, 0xad, 0xe6, 0x96, 0x87, 0xe6, 0xb5, 0x8b, 0xe8, 0xaf, 0x95};
			memset(WifiName,0,Len);
			memcpy(WifiName,tbuf,Len);
		#endif
		break;
	default :
		break;
		
	}
#else	//ori
	if(str != NULL)
	{
		_printd("Get WifiName:%s",str+5);

		strncpy(WifiName,str+6,Len);
		
		if(Len > strlen(str+6))
			WifiName[strlen(str+6) - 2] = 0;
		else
			WifiName[Len - 1] = 0;

		ret = 0;
	}
#endif	
	fclose(fd);

	return ret;
#endif
}
int GetConnWifiPwd(char* WifiPwd, int Len)
{
	FILE *fd = NULL;
	char readline[512];
	char *str = NULL;
	int ret = -1;
	int bIsString = 0;

	if (WifiPwd == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d", Len);
		return -1;
	}
	if (Len > 64)
	{
		_printd("The Len is error,len:%d", Len);
		return -1;
	}

	memset(WifiPwd, 0, Len);

	fd = fopen(STORE_WIFI_CONFIG, "r");

	if (fd == NULL) {
		_printd("file open can not create file:%s !!! ", STORE_WIFI_CONFIG);
		return -1;
	}

	while (!feof(fd))
	{
		if (!fgets(readline, 512, fd))
			break;
		//有引号为字符串，否则汉字
		if ((str = strstr(readline, "psk=\"")) != NULL){
			bIsString = 1;
			break;
		}
		else if ((str = strstr(readline, "psk=")) != NULL) {
			bIsString = 0;
			break;
		}
	}
#if 1	
	switch (bIsString){
	case 1: //字符串
		if (str != NULL){
			strncpy(WifiPwd, str + 5, Len);

			if (Len > strlen(str + 5)){
				WifiPwd[strlen(str + 5) - 2] = 0;
			}
			else {
				WifiPwd[Len - 1] = 0;
			}
			ret = 0;
			_printd("Get WifiPwd:%s\n", WifiPwd);
		}
		break;
	case 0://中文	
		if (str != NULL){
			char OriBuf[64] = { 0 };
			char tBuf[3] = { 0 };

			long tmpl = 0;
			unsigned char tmpc = 0;
			int i = 0;
			int j = 0;

			strncpy(OriBuf, str + 5, Len);
			if (Len > strlen(str + 5)){
				OriBuf[strlen(str + 5) - 1] = 0;
			}
			else {
				OriBuf[Len - 1] = 0;
			}
			_printd("Get OriBuf:%s\n", OriBuf);

			for (i = 0; i < Len; i += 2){

				if (OriBuf[i] != 0 && OriBuf[i + 1] != 0){
					sprintf(tBuf, "%c%c", OriBuf[i], OriBuf[i + 1]);
					//tmp = atoi(tBuf);
					tmpl = strtol(tBuf, NULL, 16);
					tmpc = (unsigned char)(tmpl & 0x000000FF);
					//_printd("!!!Get tbuf:%x\n",tmpc);
					if (j <= Len){
						WifiPwd[j] = (unsigned char)tmpc;
						_printd("Get WifiPwd:%x\n", (unsigned char)WifiPwd[j]);
						j++;
					}
					else {
						break;
					}
				}
				else {
					break;
				}
			}
			ret = 0;
		}
#if 0 //test
		char tbuf[48] = { 0x41, 0x41, 0xe4, 0xb8, 0xad, 0xe6, 0x96, 0x87, 0xe6, 0xb5, 0x8b, 0xe8, 0xaf, 0x95 };
		memset(WifiPwd, 0, Len);
		memcpy(WifiPwd, tbuf, Len);
#endif
		break;
	default:
		break;

	}
#else	//ori
	if (str != NULL)
	{
		_printd("Get WifiPwd:%s", str + 5);

		strncpy(WifiPwd, str + 6, Len);

		if (Len > strlen(str + 6))
			WifiPwd[strlen(str + 6) - 2] = 0;
		else
			WifiPwd[Len - 1] = 0;

		ret = 0;
	}
#endif	
	fclose(fd);

	return ret;

}
int GetWifiValue(int *Strength)
{
#ifdef IPC_JZ
	ZRT_CAM_WIFI_Status WifiStatu;

	if(Strength == NULL)
	{
		_printd("The pointer is error");
		return -1;
	}
	
	g_DevExternal.Dev_GetWiFi(&WifiStatu);

	if(WifiStatu.connectionStatus != WIFI_CONNECT_FAILED &&
		WifiStatu.connectionStatus != WIFI_DISCONNECTED) 	
	{
		*Strength = WifiStatu.signalLevel;
	}
	else
	{
		return -1;
	}
	
	return 0;
#else
	int   i  = 0;
	int   fd = -1;
	int   ret= -1;
	FILE *fp = NULL;
	char *db = NULL;
	char *idx= NULL;
	char line_db[32] = {0};
	char line_ssid[32] = {0};
	char strWifiInfo[64] = {0};
	char readline[512];	
	char enable   = '1';
	char ssid[32] = {0};

	if(Strength == NULL)
	{
		_printd("The pointer is error");
		return -1;
	}
	
	if( -1 == GetConnWifi(ssid,32))
		return -1;

//	printf("ssid:%s\n",ssid);
	if(-1 == Get_Wifi_Info(strWifiInfo, sizeof(strWifiInfo))){
		return -1;
	}

	fp = fopen(strWifiInfo, "r+");
	if (!fp)
	{
		_printd("Unable to open file\n");
	    return -1;
	}

//	fwrite(&enable,1,1,fp);

	//sleep(3);
	
	while (!feof(fp))
	{
		if (!fgets(readline, 512 , fp))
			break;

		idx = strtok(readline, " \r\n");
		if(!idx)	continue;

		for (i = 0; i < 3; i++)
		{
			db = strtok(NULL, " \r\n");
			if (!db) continue;
		}
		
		//SdBm
		db = strtok(NULL, " \r\n");
		if (db) {
			int dx;
			for (dx=0; dx<strlen(db); dx++) {
				if ((db[dx]==' ') || (db[dx]=='\t'))
					continue;
				else {
					db += dx;
					break;
				}
			}
			strcpy(line_db, db);
		//	printf("%s\n",line_db	);
		}	

		for (i = 0; i < 3; i++)
		{
			db = strtok(NULL, " \r\n");
			if (!db) continue;
		}
		//ssid
		db = strtok(NULL, "\t\r\n");
		if (db) {
			int dx;
			for (dx=0; dx<strlen(db); dx++) {
				if ((db[dx]==' ') || (db[dx]=='\t'))
					continue;
				else {
					db += dx;
					break;
				}
			}
			strcpy(line_ssid, db);
		//	printf("%s\n",line_ssid	);
		}	
				
		if (strcmp(line_ssid, ssid) == 0)
		{			
			*Strength = 100 - abs(atoi(line_db));

			printf("%s %s,%d\n",line_ssid,line_db,*Strength);

			ret = 0;
			
			break;
		}
		else
		{
		//	printf("%s   %s\n",line_ssid,ssid);
		}
	}
	fclose(fp);

	if((fd = open(strWifiInfo,O_RDWR)) > 0)
	{
		lseek(fd,0,SEEK_SET);
		write(fd,&enable,1);
		_printd("scan wifi");	
		close(fd);
		fd = -1;
	//	sleep(2);
	}
	
	return ret;
#endif
}

int GetDeviceNum(char* Imei,int Len)
{
	if(Imei == NULL || Len < 16)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	return GetUuid(Imei);
}

int GetDevIp(char* Ip,int Len)
{
	IPInfo Info;
	char IPAddr[50] = "";

	if(Ip == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}

	g_NetApp.GetNetIPInfo("wlan0", &Info);

	sprintf(IPAddr, "%d.%d.%d.%d", Info.HostIP.c[0], Info.HostIP.c[1], Info.HostIP.c[2], Info.HostIP.c[3]);
		
	_printd("Get Ip:%s",IPAddr);
	strncpy(Ip,IPAddr,Len);	
}

int GetDevMac(char* Mac,int Len)
{
	char MAC[20];

	if(Mac == NULL || Len <= 1)
	{
		_printd("The pointer is error,len:%d",Len);
		return -1;
	}
	
	NetGetMAC("wlan0", MAC, sizeof(MAC));

	_printd("Get Mac:%s",MAC);

	strncpy(Mac,MAC,Len);
	return 0;
}

