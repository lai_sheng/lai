
#include "Devices/DevCaptureBuf.h"
#include "Devices/DevCapture.h"

DataBlock DataHead;
DataBlock DataTail;

CBuffer::CBuffer() : m_pDataBlock(NULL)
{
	m_IFrameNum = 2;
}

CBuffer::~CBuffer()
{
	if (m_pDataBlock) 
	{
		delete [] m_pDataBlock;
	}
}

/*!
	\b Description		:	初始化参数\n
	\b Argument			:	int iChannel, uint prerec_length
	\param	iChannel	:	通道号
	\param	prerec_length	:	预录包的长度

	\b Revisions		:	
*/
void CBuffer::Init(uint prerec_length, int iChannel)
{
	m_iChannel		= iChannel;
	m_MaxLength		= prerec_length << 10;

	m_Size			= prerec_length;


	m_pDataBlock	= new DataBlock[m_Size];
	if (!m_pDataBlock)
	{
		exit(1);
	}

	m_front			= 0;
	m_rear			= 0;
	m_Length		= 0;
}

/*!
	\b Description		:	把收到的数据放到队列中\n
	\b Argument			:	CPacket *pkt
	\param	pkt			:	存放数据的包的指针
	\return 0:存放成功；-1：超出范围；1：包满需要等待。

	\b Revisions		:	
*/
int CBuffer::PushData(CPacket *pkt, VD_BOOL overwrite)
{
	//查找队列
	CPacket *tmppkt;
	uint cur_packlen = pkt->GetSize();

	while (m_Length + cur_packlen > m_MaxLength)
	{
		if (!overwrite)
		{
			//trace("ch[%d], the m_length = %d, the size = %d\n", m_iChannel,m_Length, m_MaxLength);
			m_State = PUSH_DATA_FULL;
			return PUSH_DATA_FULL;
		}
	
		tmppkt = m_pDataBlock[m_front].pPkt;
		
		m_Length -= tmppkt->GetSize();
		tmppkt->Release();

		m_front = (m_front + 1) % m_Size;
	}
	
	assert((m_rear + 1) % m_Size != (uint)m_front);

	//把数据加入队列
	pkt->AddRef();
	m_pDataBlock[m_rear].pPkt = pkt;
	m_pDataBlock[m_rear].iChannel = m_iChannel;	
	
	if (!overwrite)
	{
		m_pDataBlock[m_rear].pre = DataTail.pre;
		DataTail.pre->next = &m_pDataBlock[m_rear];
		m_pDataBlock[m_rear].next = &DataTail;
		DataTail.pre = &m_pDataBlock[m_rear];
		m_pDataBlock[m_rear].flag = STORE_DATA_REC;
	}
	else
	{
		m_pDataBlock[m_rear].flag = STORE_DATA_PRE;
	}
	
	
	m_rear = (m_rear + 1) % m_Size;	
	//trace("the m_front = %d, m_rear = %d, the ref = %d\n", m_front, m_rear, pkt->GetRef());
	m_Length += cur_packlen;

	return PUSH_DATA_SUCCESS;
}

/*!
	\b Description		:	从队列中取到数据\n
	\b Argument			:	int iChannel, CPacket *pkt
	\param	iChannel	:	取得数据的通道数
	\param	pkt			:	存放数据的包的指针	
	\return 0:取到该值的数据，1: 取到但后面将空；-1：未取到数据

	\b Revisions		:	
*/
int CBuffer::PopData(CPacket **pkt)
{
	m_Length -= (m_pDataBlock[m_front].pPkt)->GetSize();
	m_pDataBlock[m_front].next->pre = m_pDataBlock[m_front].pre;
	m_pDataBlock[m_front].pre->next = m_pDataBlock[m_front].next;
	m_pDataBlock[m_front].flag = STORE_DATA_NONE;
	
	//trace("pop data m_front = %d, m_rear = %d\n", m_front, m_rear);
	m_front = (m_front + 1) % m_Size;

	
	if (m_State == PUSH_DATA_FULL)
	{
		m_DataSemaphore.Post();
		m_State = PUSH_DATA_SUCCESS;
	}	
	
	return GET_DATA_SUCCESS;
}

void CBuffer::Wait()
{
	m_DataSemaphore.Pend();
}


/*!
\b Description		:	清除预录队列,由后往前删除\n

\b Revisions		:	
*/
void CBuffer::ClearData()
{
	CPacket * pkt;

	//全部清除
	while(m_front != m_rear)
	{
		pkt = m_pDataBlock[m_front].pPkt;
		if (m_pDataBlock[m_front].flag == STORE_DATA_REC)
		{
			m_pDataBlock[m_front].next->pre = m_pDataBlock[m_front].pre;
			m_pDataBlock[m_front].pre->next = m_pDataBlock[m_front].next;
		}

		m_pDataBlock[m_front].flag = STORE_DATA_NONE;
		
		pkt->Release();
		m_front = (m_front + 1) % m_Size;
	}

	if (m_State == PUSH_DATA_FULL)
	{
		m_DataSemaphore.Post();
		m_State = PUSH_DATA_SUCCESS;
	}
	m_Length = 0;
}

/*!
	\b Description		:	把预录队列前面的数据清除\n
	把后面的数据挂在DataTail的前面，而且挂在DataTail前面比挂在DataHead后面好，因为DataHead可能正在使用

	\b Revisions		:	
*/
void CBuffer::RemovePartialData()
{
	if (m_front != m_rear)
	{
		int remaincount = 0;
		
		int		tail = m_rear < m_front ? m_rear + m_Size - 1 : m_rear - 1;
		int		index = 0;
		
		DataBlock *pData = DataTail.pre;
#if defined(DVR_GB) 
		DHTIME  dhtime;
		DHTIME	curtime;

		SYS_getsystime(&curtime);
#else
		//以256K作为分界线
		int length = 256 << 10;
#endif

		for (; tail >= m_front; tail--)
		{
			index = tail % m_Size;
#if defined(DVR_GB)
			if (getFirstFrameTime(m_pDataBlock[index].pPkt, &dhtime) == TRUE)
			{
				//解决下面年不准的问题
				dhtime.year = curtime.year;
				//trace("curtime %d-%d-%d %d:%d:%d\n", curtime.year, curtime.month, curtime.day, curtime.hour, curtime.minute, curtime.second);
				//trace("time %d-%d-%d %d:%d:%d\n", dhtime.year, dhtime.month, dhtime.day, dhtime.hour, dhtime.minute, dhtime.second);
				//trace("value %d, %d\n", ABS(changeto_second(curtime, dhtime)), m_IFrameNum);
				if (ABS(changeto_second(curtime, dhtime)) >= m_IFrameNum + 1)
				{
					remaincount = index - m_front < 0 ? index + m_Size - m_front  : index - m_front;
					remaincount++;
					break;
				}
			}
#else
			length -= m_pDataBlock[index].pPkt->GetLength();
			if (length <= 0)
			{
				remaincount = index - m_front < 0 ? index + m_Size - m_front  : index - m_front;
				remaincount++;
				break;
			}
#endif

			//串接在DataTail的后面
			m_pDataBlock[index].flag = STORE_DATA_REC;			
			m_pDataBlock[index].next = pData->next;
			m_pDataBlock[index].pre = pData;
			pData->next->pre = &m_pDataBlock[index];
			pData->next = &m_pDataBlock[index];
		}

		//清除pos之前的数据
		while (remaincount > 0)
		{
			m_Length -= (m_pDataBlock[m_front].pPkt)->GetSize();			
			m_pDataBlock[m_front].flag = STORE_DATA_NONE;
			m_pDataBlock[m_front].pPkt->Release();

			m_front = (m_front + 1) % m_Size;
			remaincount--;
		}
		//trace("remove pati m_front = %d, m_rear = %d\n", m_front, m_rear);
	}
}

uint CBuffer::GetLength()
{
	return m_Length;
}

void CBuffer::SetIFrameNum(int number)
{
	m_IFrameNum = number;
}

//CBufferManager * CBufferManager::m_pInstance = NULL;

extern int g_nCapture;

/*!
	\b Description		:	构造函数\n

	\b Revisions		:	
*/
CBufferManager::CBufferManager() : CThread("BufferManager", TP_PRE)
{
	m_iUser = 0;
	DataHead.pre = NULL;
	DataHead.next = &DataTail;
	DataTail.pre = &DataHead;
	DataTail.next = NULL;
}


/*!
	\b Description		:	析构函数\n
	
	\b Revisions		:	
*/
CBufferManager::~CBufferManager()
{

}


/*!
	\b Description		:	线程的执行体\n

	\b Revisions		:	
*/
void CBufferManager::ThreadProc()
{
	DataBlock *pData;
	int iChannel;

	SET_THREAD_NAME("CBufferManager");
	
	while(m_bLoop)
	{
		m_Semaphore.Pend();

		m_BufferMutex.Enter();
		for (pData = DataHead.next; pData != &DataTail;)
		{
			iChannel = pData->iChannel;
			assert(pData);
			if (pData->pPkt->GetRef() == 0)
			{
				tracepoint();
			}
			m_Buffer[iChannel].PopData(&(pData->pPkt));
			
			assert(pData->pPkt->GetRef() >= 1);
			m_BufferMutex.Leave();
			m_pDevCapture[iChannel]->OnRecData(&(pData->pPkt));
			m_BufferMutex.Enter();
			pData = DataHead.next;
		}
		m_BufferMutex.Leave();
	}
}

/*!
	\b Description		:	初始化预录类\n
	\b Argument			:	int iChannel, uint uBufferSize, CDevCapture *pDevCapture
	\param	iChannel	:	通道号
	\param	uBufferSize	:	缓冲大小
	\param	pDevCapture	:	DevCapture指针

	\b Revisions		:	
*/
void CBufferManager::Init(int iChannel, uint uBufferSize, CDevCapture *pDevCapture)
{
	m_Buffer[iChannel].Init(uBufferSize, iChannel);
	m_pDevCapture[iChannel] = pDevCapture;
	static int bOnlyOne = TRUE;
	if (bOnlyOne)
	{
		CreateThread();
		bOnlyOne = FALSE;
	}
}

uint CBufferManager::GetLength(int iChannel)
{
	return m_Buffer[iChannel].GetLength();
}

void CBufferManager::SetIFrameNum(int iChannel, int number)
{
	m_Buffer[iChannel].SetIFrameNum(number);
}
/*!
	\b Description		:	放入缓冲数据\n
	\b Argument			:	int iChannel, CPacket *pPkt
	\param	iChannel	:	通道号
	\param	pPkt		:	放入数据的指针

	\b Revisions		:	
*/
void CBufferManager::PushData(int iChannel, CPacket *pPkt)
{
	CGuard l_cGuard(m_BufferMutex);
	
	if ( !(m_iUser & BITMSK(iChannel)))
	{
		m_Buffer[iChannel].PushData(pPkt, TRUE);
	}
	else
	{	
		// 检查队列是否为空
		if (((DataHead.next == &DataTail) && (DataTail.pre == &DataHead)))
		{
			m_Semaphore.Post();
		}

		while (m_Buffer[iChannel].PushData(pPkt, FALSE) == PUSH_DATA_FULL)
		{
			m_BufferMutex.Leave();
			m_Buffer[iChannel].Wait();
			m_BufferMutex.Enter();
			if (!(m_iUser & BITMSK(iChannel)))
			{
				break;
			}
		}
	}	
}

void CBufferManager::ClearData(int iChannel)
{
	CGuard l_cGuard(m_BufferMutex);
	m_Buffer[iChannel].ClearData();
}

/*!
	\b Description		:	开始获得录像数据\n
	\b Argument			:	int iChannel
	\param	iChannel	:	通道号
	\b Revisions		:	
*/
void CBufferManager::Start(int iChannel)
{
	CGuard l_cGuard(m_BufferMutex);
	m_iUser |= BITMSK(iChannel);
	if (TestRecDataEmpty())
	{
		m_Semaphore.Post();
	}
	// 下面这个必须调用，否则指针没有把预录数据挂在链表里，取数据会出错
	m_Buffer[iChannel].RemovePartialData();
}


/*!
	\b Description		:	停止获得录像数据\n
	\b Argument			:	int iChannel
	\param	iChannel	:	通道号
	
	\b Revisions		:	
*/
void CBufferManager::Stop(int iChannel)
{
	CGuard l_cGuard(m_BufferMutex);
	m_iUser &= ~BITMSK(iChannel);

	m_Buffer[iChannel].ClearData();
	
}

VD_BOOL CBufferManager::TestRecDataEmpty()
{
	return ((DataHead.next == &DataTail) && (DataTail.pre == &DataHead));
}
