

#include "Devices/DevAudioOut.h"
#include "APIs/System.h"

extern SYSTEM_CAPS_EX g_CapsEx;//一些扩展的性能结构

CDevAudio* CDevAudio::_instance = NULL;

CDevAudio* CDevAudio::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevAudio();
	}
	return _instance;
}

CDevAudio::CDevAudio()
{
	tracef("%s(), Create Audio Device.\n", __FUNCTION__);
	AudioCreate();
}

CDevAudio::~CDevAudio()
{
	AudioDestory();
}

VD_BOOL CDevAudio::Switch(uint dwType, uint dwChannel)
{
	uint dwAudioChan = dwChannel;
/*-----------------------------------------------------------------------
	g_CapsEx.HasAudioBoard为0的话表示没有音频(依据王恒文2005-10-21给我发的邮件)
-----------------------------------------------------------------------*/
	if (g_CapsEx.HasAudioBoard != 0)
	{
		if (dwType != AUDIO_SWITCH_PLAY)
		{
			dwAudioChan = channel_utol(dwAudioChan);
		}
		if(AudioSwitch(dwType, dwAudioChan))
		{
			return TRUE;
		}
	}

	return FALSE;
}

VD_BOOL CDevAudio::Start()
{
	tracef("%s(), Start Audio Out.\n", __FUNCTION__);
	return !AudioOutStart();
}

VD_BOOL CDevAudio::Stop()
{
	//m_VoiceType = VOICE_NONE;
	return !AudioOutStop();
}

int CDevAudio::SetFormat(int type)
{
	printf("\r\n#####AudioOutSetFormat\r\n");
	return AudioOutSetFormat(type);
}

void CDevAudio::PutBuf(uchar *data, int length)
{
	AudioOutPutBuf(data, length);
}


