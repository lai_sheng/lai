

#include "Devices/DevVideo.h"
//#include "Devices/DevGraphics.h"
#include "Configs/ConfigLocation.h"
#include "Devices/DevSplit.h"

extern int g_nVideoOut;
VD_BOOL GetCaps(WHITE_LEVEL_CAPS * pCaps)
{
#ifndef WIN32
	if(!WhiteLevelGetCaps(pCaps))
#endif
	{
	#ifdef WIN32
		pCaps->enable = 1;
	#endif
		return TRUE;
	}
	return FALSE;
}

CDevVideo* CDevVideo::_instance[] = {NULL, };

CDevVideo* CDevVideo::instance(int index /* = 0 */)
{
	assert(index < g_nVideoOut);

	if(NULL == _instance[index])
	{
		_instance[index] = new CDevVideo(index);
	}
	return _instance[index];
}

CDevVideo::CDevVideo(int index) :
m_index(index)
{
	VideoCreate();
	//初始化视频输出模式
	m_CurrentOutputMode = m_Output = VIDEO_OUTPUT_TV;
	m_bTVMonitorOpened = TRUE;
}

CDevVideo::~CDevVideo()
{
	VideoDestory();
}

int CDevVideo::GetChannels ()
{
	return 1;
}

VD_BOOL CDevVideo::SetControl(int channel, VIDEO_CONTROL* cVCtrl)
{
	//printf("function: CDevVideo::SetControl() only supported IPC, now\n");
	return FALSE;
}

VD_BOOL CDevVideo::SetColor(int channel, VIDEOCOLOR_PARAM color)
{
	VIDEO_COLOR Color;
	memset(&Color, 0, sizeof(VIDEO_COLOR));
	Color.Brightness 	= color.nBrightness;
	Color.Contrast 		= color.nContrast;
	Color.Saturation 	= color.nSaturation;
	Color.Hue 			= color.nHue;
	Color.Gain 			= color.mGain;
	WHITE_LEVEL_CAPS pCaps;
	GetCaps(&pCaps);
	if(pCaps.enable == 1)
	{
		Color.WhiteBalance = color.mWhitebalance;
	} 
    
	if(VideoSetColor(channel, &Color))
	{
		return FALSE;
	}
	return TRUE;
}

VD_BOOL CDevVideo::SetCover(int channel, int index, VD_RECT *pRect, uint Color, int Enable)
{
	VIDEO_COVER_PARAM param;
	CDevSplit::instance()->adjustRect(pRect);
	param.rect.left = pRect->left;
	param.rect.right = pRect->right;
	param.rect.top = pRect->top;
	param.rect.bottom = pRect->bottom;
	param.color = Color;
	param.enable = Enable;
	if(VideoSetCover(channel, index, &param))
	{
		return FALSE;
	}
	return TRUE;
}

VD_BOOL CDevVideo::SetVstd(int channel, uint dwStandard)
{
	if(VideoSetVstd(channel, dwStandard))
	{
		return FALSE;
	}
	return TRUE;
}

void CDevVideo::WriteRegister(uchar Chip, uchar Page, uchar Register, uchar Data)
{
	VideoWriteRegister(Chip, Page, Register, Data);
}

uchar CDevVideo::ReadRegister(uchar Chip, uchar Page, uchar Register)
{
	return VideoReadRegister(Chip, Page, Register);
}

VD_BOOL CDevVideo::Switch(uint dwType)
{
	if(VideoSwitch(dwType))
	{
		return FALSE;
	}
	return TRUE;
}

VD_BOOL CDevVideo::SetTVMargin(uchar left, uchar top, uchar right, uchar bottom)
{
	if(VideoSetTVMargin(left, top, right, bottom))
	{
		return FALSE;
	}
	return TRUE;
}

VD_BOOL CDevVideo::SetTVColor(uchar brightness, uchar contrast)
{
	if(VideoSetTVColor(brightness, contrast))
	{
		return FALSE;
	}
	return TRUE;
}

VD_BOOL CDevVideo::SetTVAntiDither(uchar level)
{
	return TRUE;
}

VD_BOOL CDevVideo::SetMatrix(uchar channelin,uchar channelout)
{
	VD_BOOL ret;
	ret = g_CapsEx.HasVNMatrixBoard ? (!VideoMatrix(channelin,channelout)) : TRUE;
	return ret;
}

VD_BOOL CDevVideo::SwitchOutput(int _type/*= VIDEO_OUTPUT_AUTO*/)
{
	uchar type = _type;
	if(_type == VIDEO_OUTPUT_AUTO)
	{
	if(VideoGetOutputAdapter(&m_Output))
	{
		return FALSE;
	}
	trace("CDevVideo::SwitchOutput() Get = %d\n", m_Output);

	/*
		去掉对lcd的切换，只针对VGA/TV进行切换
	*/
	if(m_Output == VIDEO_OUTPUT_VGA)
	{
		type = VIDEO_OUTPUT_TV;
	}
	else
	{/*if(m_Output == VIDEO_OUTPUT_TV)*/
		//其他所有情况都切换到VGA
		type = VIDEO_OUTPUT_VGA;
		} 
	} 
	trace("CDevVideo::SwitchOutput() Switch = %d\n", type);
	if(VideoSetOutputAdapter(type))
	{
		return FALSE;
	}
	if(VideoGetOutputAdapter(&m_Output))
	{
		return FALSE;
	}
	
	trace("CDevVideo::SwitchOutput() to type %d\n", m_Output);
	//add by yanjun 20061124
	VideoGetOutputAdapter(&m_CurrentOutputMode);
	return TRUE;
}

VD_BOOL CDevVideo::InitOutput()
{
	m_Output = VIDEO_OUTPUT_AUTO;

	if(m_Output != VIDEO_OUTPUT_AUTO)
	{
		if(VideoSetOutputAdapter(m_Output))
		{
			return FALSE;
		}
	}
	else
	{
		trace("CDevVideo::InitOutput() automatically.\n");
	}
	//add by yanjun 20061124
	VideoGetOutputAdapter(&m_CurrentOutputMode);
	return TRUE;
}

void CDevVideo::SwitchTVMonitor(VD_BOOL open)
{
	if(!VideoSwitchTVMonitor(open))
	{
		trace("CDevVideo::VideoSwitchTVMonitor to %d\n", open);
		m_bTVMonitorOpened = open;
	}
}

void CDevVideo::SwitchTVMonitor()
{
	SwitchTVMonitor(!m_bTVMonitorOpened);
}

/*!
	\b Description		:	采样分辨率设置\n
	\b Argument			:	int size
	\param	size		:	为capture_comp_t的一个枚举型
	\return	０：设置成功；非０：设置失败			
*/
VD_BOOL CDevVideo::SamplingModeSwitch(int iChannel, int size)
{
#ifdef DVR_LB
	return VideoRecordSetSize(iChannel, size);
#else
	return TRUE;
#endif
}
/*add by yanjun 20061124 */
uchar CDevVideo::GetCurrentOuputMode()
{
	return m_CurrentOutputMode;
}
VD_BOOL CDevVideo::GetTVMonitorState()
{
	return m_bTVMonitorOpened;
}

VD_BOOL CDevVideo::SetBkColor(VD_COLORREF color)
{
#ifdef VIDEO_API_VERSION_2
	if(Video2SetBkColor(m_index, color))
#else
	if(VideoSetBkColor(color))
#endif
	{
		return FALSE;
	}
	return TRUE;
}
VD_BOOL CDevVideo::SetCropRegion(int channel,int Hoffset,int Voffset,int Width,int Height)
{
	return TRUE;
}
	
