
#include <assert.h>
#include "Devices/DevCapBuf.h"
#include "Devices/DevCapture.h"
#include "Devices/DevVideo.h"
//#include "GUI/GDI/DC.h"
#include "System/AppConfig.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigVideoWidget.h"
#include "Functions/Encode.h"

#ifdef _2761_EXCEPTION_CHK_
//#include "Functions/Upgrader.h"
#include "Main.h"
#endif

CCaptureManager* CDevCapture::m_pManager = NULL;
CDevBufManager* CDevCapture::m_pBufferManager = NULL;

CDevCapture* CDevCapture::_instance [N_SYS_CH]    =    {NULL, };

//添加通道数目判断，使用全局处理 from 上海091223
int g_nCaptureCountAll = 0;
int g_nChnCaptureCount[N_SYS_CH] = {0};


void subItem(uint* pValue, uint subItem, int iMax)
{
    for (int i = 0; i < iMax; i++)
    {
        if (subItem & BITMSK(i))
        {
            *pValue &= ~BITMSK(i);
        }
    }
}

void addItem(uint *pValue, uint subItem, int iMax)
{
    for (int i = 0; i < iMax; i++)
    {
        if (subItem & BITMSK(i))
        {
            *pValue |= BITMSK(i);
        }
    }
}

VD_BOOL GetCaps(CAPTURE_CAPS * pCaps)
{
    memset(pCaps, 0, sizeof(CAPTURE_CAPS));
#ifdef WIN32
    CaptureGetCaps(pCaps);
    pCaps->TitleCount = 10;
    pCaps->ImageSize = BITMSK(CAPTURE_SIZE_D1) | BITMSK(CAPTURE_SIZE_HD1) 
        | BITMSK(CAPTURE_SIZE_BCIF) | BITMSK(CAPTURE_SIZE_CIF);

    return TRUE;
#else
    VD_BOOL bRet = !CaptureGetCaps(pCaps);
#ifdef ENC_COVER_BLOCKS
    pCaps->CoverBlocks = MIN(ENC_COVER_BLOCKS, pCaps->CoverBlocks);
#endif

#ifdef ENC_SUB_COMPRESS 
    subItem(&pCaps->Compression, ENC_SUB_COMPRESS, CAPTURE_COMP_NR);
#endif

#ifdef ENC_ADD_MP4_COMPRESS
    addItem(&pCaps->Compression, ENC_ADD_MP4_COMPRESS, CAPTURE_COMP_NR);
#endif

#ifdef MACE
    if(g_CapsEx.ForNRT)          //非实时取消D1
    {
        pCaps->ImageSize = BITMSK(CAPTURE_SIZE_CIF);
    }
#endif

#ifdef ENC_SUB_IMG
    subItem(&pCaps->ImageSize, ENC_SUB_IMG, CAPTURE_SIZE_NR);
#endif

#ifdef ENC_ADD_CIF_IMG
    addItem(&pCaps->ImageSize, ENC_ADD_CIF_IMG, CAPTURE_SIZE_NR);
#endif

#ifdef ENC_ADD_BCIF_IMG
    addItem(&pCaps->ImageSize, ENC_ADD_BCIF_IMG, CAPTURE_SIZE_NR);
#endif

#ifdef ENC_ADD_HD1_IMG
    addItem(&pCaps->ImageSize, ENC_ADD_HD1_IMG, CAPTURE_SIZE_NR);
#endif

    assert(pCaps->CoverBlocks <= COVERNUM);

    return bRet;
#endif
}

/*!
    \b Description        :    得到支持的特殊功能，主要是多码流的支持情况\n
    \b Argument            :    CAPTURE_EXT_STREAM* pCaps
    \param    pCaps        :    特殊属性的指针
    \return    非０：成功；０：失败                   
*/
VD_BOOL GetExtCaps(CAPTURE_EXT_STREAM * pCaps)
{
    memset(pCaps, 0, sizeof(CAPTURE_EXT_STREAM));
#ifdef WIN32
    pCaps->ExtraStream = 0;
    pCaps->ExtraStream |= BITMSK(CHL_MAIN_T) | BITMSK(CHL_2END_T) | BITMSK(CHL_JPEG_T);
    pCaps->CaptureSizeMask[CAPTURE_SIZE_CIF] = BITMSK(CAPTURE_SIZE_CIF) | BITMSK(CAPTURE_SIZE_D1) \
        | BITMSK(CAPTURE_SIZE_QCIF);
    pCaps->CaptureSizeMask[CAPTURE_SIZE_BCIF] = BITMSK(CAPTURE_SIZE_CIF) | BITMSK(CAPTURE_SIZE_D1)\
        | BITMSK(CAPTURE_SIZE_QCIF);
    pCaps->CaptureSizeMask[CAPTURE_SIZE_HD1] = BITMSK(CAPTURE_SIZE_CIF) | BITMSK(CAPTURE_SIZE_QCIF) ;
    pCaps->CaptureSizeMask[CAPTURE_SIZE_D1] = BITMSK(CAPTURE_SIZE_CIF) | BITMSK(CAPTURE_SIZE_D1) \
        | BITMSK(CAPTURE_SIZE_QCIF);
#ifndef ENC_SUPPORT_SNAPSHOT
    pCaps->ExtraStream &= ~BITMSK(CHL_JPEG_T);
#endif
    pCaps->ExtraStream |= BITMSK(CHL_JPEG_T);
    return TRUE;
#else
    VD_BOOL bRet = !CaptureGetExtCaps(pCaps);
#ifdef ENC_SUB_IMG
    subItem(&pCaps->CaptureSizeMask[CAPTURE_SIZE_CIF], ENC_SUB_IMG, CAPTURE_SIZE_NR);
    subItem(&pCaps->CaptureSizeMask[CAPTURE_SIZE_BCIF], ENC_SUB_IMG, CAPTURE_SIZE_NR);
#endif

#ifndef ENC_SUPPORT_SNAPSHOT
    pCaps->ExtraStream &= ~BITMSK(CHL_JPEG_T);
#endif
    return bRet;
#endif
}

/*!
    \b Description        :    获得DSP支持的编码能力\n
    \b Argument            :    CAPTURE_DSPINFO *pDspInfo
    \param    pDspInfo    :    编码能力结构的指针
    \return    非０：成功；０：失败                   
*/
VD_BOOL GetDspInfo(CAPTURE_DSPINFO *pDspInfo)
{
    memset(pDspInfo, 0, sizeof(CAPTURE_DSPINFO));
    VD_BOOL bRet = TRUE;
#ifndef WIN32
    bRet = !CaptureGetDspInfo(pDspInfo);

    assert(pDspInfo->nMaxSupportChannel);

    // 扩展通道不允许超过支持的通道号，否则转换会出错
#ifdef ENC_ENCODE_MULTI
    assert(pDspInfo->nExpandChannel <= pDspInfo->nMaxSupportChannel);
#endif

#else
    pDspInfo->nMaxEncodePower = 0xffffffff;
    pDspInfo->nMaxEncodePower = 4 * 352 * 288 * 25;
    pDspInfo->nMaxSupportChannel = 4;
    pDspInfo->bChannelMaxSetSync = 1;
#endif

    return bRet;
}

VD_BOOL GetChnCaps(int chn, CAPTURE_CAPS * pCaps)
{
    GetCaps(pCaps);
    return VD_TRUE;
}

//!取出相应的通道指定的码流所能支持的所有的ImageSize
uint GetChnExtraStreamSize(int chn, enum capture_channel_t stream)
{
	int i;
	uint ImageSize = 0;
	CAPTURE_EXT_STREAM Caps;
	CaptureGetExtCaps(&Caps);

	if( (Caps.ExtraStream & BITMSK(stream) ) == 0 )
	{
		return 0;
	}

	for( i = CAPTURE_SIZE_D1; i < CAPTURE_SIZE_NR; i++ )
	{
		if( Caps.CaptureSizeMask[i] & BITMSK(stream) != 0 )
		{
			ImageSize |= BITMSK(stream);
		}
	}

	return ImageSize;
}


#ifdef SDK_3516
//!TODO:取出相应码流支持的最大的分辨率，应该从码流能力中取出
enum capture_size_t GetStreamMaxSupportSize(enum capture_channel_t stream)
{
	extern int GetImageSize(VD_SIZE *pSize, int imgtype);

	int i;
	VD_SIZE vdsize;
	VD_SIZE qcif_size;
	GetImageSize(&qcif_size, CAPTURE_SIZE_QCIF);
	enum capture_size_t size = CAPTURE_SIZE_QCIF;
	CAPTURE_STREAM_IMAGE*  pcaps = new CAPTURE_STREAM_IMAGE;
	CaptureGetStreamImageCaps( pcaps );

	if( ( pcaps->StreamCaps & BITMSK(stream) ) == 0 )
	{
		delete pcaps;
		return CAPTURE_SIZE_NR;
	}

	for( i = 0; i < CAPTURE_SIZE_NR; i++ )
	{
		if( pcaps->CaptureSizeMask[i] & BITMSK(stream) )
		{
			GetImageSize(&vdsize, i);
			if (vdsize.w > qcif_size.w )
			{
				size = (enum capture_size_t)i;
			}
		}
	}
	delete pcaps;

	return size;
}
#endif

/*!
    \class CDevCapture
    Description        :捕获设备，用来和DSP交互，取得数据的设置封装层\n  
    \b Revisions    :
*/

/*!
    \b Description        :    唯一实例模式\n
    \b Argument            :    int iChannel
    \param    iChannel    :    通道号
    \return　捕获设备的指针                

    \b Revisions        :    
*/
CDevCapture* CDevCapture::instance(LOGIC_CHN iChannel)
{
    if(NULL == _instance[iChannel])
    {
        ICaptureManager* pCM = ICaptureManager::instance();
        _instance[iChannel] =    new    CDevCapture( iChannel, pCM->GetAnalogChnNo(iChannel) );        
    }
    return  _instance[iChannel];
}

CCaptureManager* CDevCapture::getManager()
{
    return m_pManager;
}
#if 0
//
//void CDevCapture::ThreadProc()
//{
//    VD_HANDLE hMemory = 0;
//    uint dwStreamType = CHL_MAIN_T;
//    CPacket *pPacket;
//    while (m_bLoop) 
//    {
//        //sleep(2);
//        if(CaptureGetBuffer (&m_iChannel, &dwStreamType, &hMemory))
//        {
//            trace("CDevCapture::ThreadProc() CaptureGetBuffer failed\n");
//            SystemSleep(100);
//            continue;
//        }        
//        
//        pPacket = (CPacket *)hMemory;
//        assert(pPacket);
//        assert(pPacket->GetRef() == 1);
// 
//        
//        OnData(pPacket, dwStreamType);
//
//
//        assert(pPacket->GetRef() == 1);
//        
//        pPacket->Release();
//    }
//}
#endif
CDevCapture::CDevCapture(LOGIC_CHN iChannel, ANALOG_CHN analog_chn)
    : /*CThread("DevCapture", TP_CAPTURE + iChannel),*/ m_sigRecord(32)
#ifdef ENC_ENCODE_MULTI
    ,m_sigExtRecord(4)
#endif
{
    //m_iChannel = channel_utol(iChannel);
    m_logic_chn = iChannel;
    m_iChannel = analog_chn;

    memset(m_captureFormat, 0, sizeof(CAPTURE_FORMAT) * CHL_FUNCTION_NUM);

    // 创建设备
    CaptureCreate(m_iChannel);

#ifdef IPC_JZ
	CaptureStart(m_iChannel,0);
#endif
    //printf("\r\n CaptureCreate ret:%d\r\n",ret);

    // 设置捕获策略
    //CaptureSetPacketPolicy(m_iChannel, 8192, 1, 0);
    //CaptureSetPacketPolicy(m_iChannel, 8192, 8, 200);
    
    // 创建Capture的管理对象
    if(m_pManager == NULL)
    {
        m_pManager = new CCaptureManager;
    }
    m_pManager->Init(m_iChannel, this);
    
    if (m_pBufferManager == NULL)
    {
        m_pBufferManager = CDevBufManager::instance();
    }
    // 初始化预录的缓冲区大小
    //m_BufferManager.Init(m_iChannel, BufferSize, this);

    // 初始码流为０
    m_dwBytes = 0;
    m_BitRate = 0;

    // 初始码流为０
    m_dwExBytes = 0;
    m_ExBitRate = 0;

    // 创建网络监视的回调函数队列
    for (int i = 0; i < CHL_FUNCTION_NUM; i++)
    {
        m_psigMonitor[i] = new TSignal3<int, uint, CPacket *>(32);
    }
    
    m_MonStreamType = 0;
    m_RecType = 0;
	m_RecType2snd = 0;

#if 0
    memset(m_TitleStr, 0, NAME_LEN + 1);
#endif

    // 码流统计

    memset(m_frameStatInfo, 0, sizeof(m_frameStatInfo));
    m_packetNumber = 0;
    m_maxFramesInPacket = 0;
    m_totalFrames = 0;
    m_firstIPacketTime = 0;
    m_IFrameDelayMax = 0;
    //连接计数 add by billhe at 2009-5-20
    m_dwMonitorCnt = 0; 

#ifdef _2761_EXCEPTION_CHK_
	m_AVExceptionStatus = 0;
	m_LastAudioFrames = 0;
	m_LastVideoFrames = 0;
	m_ExceptionTimes = 0;
#endif
	
}

CDevCapture::~CDevCapture()
{
    CaptureDestroy(m_iChannel);
}

/*!
    \b Description        :    得到通道数\n
    \return    支持的通道数            

    \b Revisions        :    
*/
int CDevCapture::GetChannels ()
{
    return CaptureGetChannels();
}

/*!
    \b Description        :    同步捕获设备的状态，如果出错则重启\n

    \b Revisions        :    
*/
void CDevCapture::Synchronize()
{
    CaptureSynchronize();
}


/*!
    \b Description        :    设备制式\n
    \b Argument            :    uint dwStandard
    \param    dwStandard    :    制式
    \return    非０：成功；０：失败            

    \b Revisions        :    
*/
VD_BOOL CDevCapture::SetVstd (uint dwStandard)
{
    return !CaptureSetVstd(m_iChannel,dwStandard);

    //int ret = CaptureSetVstd(m_iChannel,dwStandard);    
    //printf("\r\nCaptureSetVstd ret:%d\r\n",ret);

    //return !ret;
}
//#ifdef LINUX
//#ifndef VN_IPC//libhicap暂未合并，先对2700屏蔽抓拍的相关代码
//VD_BOOL CDevCapture::CreatSnap(void)
//{
//    //return 0;
//    return !CaptureCreateSnap(m_iChannel);
//
//}
//#endif
//#endif

/*!
    \b Description        :    设置区域遮挡\n
    \b Argument            :    RECT *pRect, uint color, int enable
    \param    pRect        :    区域指针
    \param    color        :    设置的颜色
    \param    enable        :    是否显示区域遮挡
    \return    非０：成功；０：失败                

    \b Revisions        :    
*/
VD_BOOL CDevCapture::SetCover(VD_RECT *pRect, uint color, int enable, int index)
{
    VIDEO_COVER_PARAM param;
    param.rect.left = pRect->left;
    param.rect.right = pRect->right;
    param.rect.top = pRect->top;
    param.rect.bottom = pRect->bottom;
    param.color = color;
    param.enable = enable;

    return !CaptureSetCover(m_iChannel, index, &param);
}

/*!
    \b Description        :    设置编码时间\n
    \b Argument            :    SYSTEM_TIME * pSysTime, int datefmt,int datespc,int timefmt
    \param    pSysTime    :    时间指针
    \param    datefmt        :    日期格式
    \param    datespc        :    日期分隔符
    \param    timefmt        :    时间分隔符
    \return    非０：成功；０：失败                

    \b Revisions        :    
*/
VD_BOOL CDevCapture::SetTime(SYSTEM_TIME * pSysTime)
{
    return !CaptureSetTime(m_iChannel, pSysTime,
        CConfigLocation::getLatest().iDateFormat,
        CConfigLocation::getLatest().iDateSeparator,
        CConfigLocation::getLatest().iTimeFormat);
}

uint CDevCapture::GetBufferLength()
{
    return 0;//m_BufferManager.GetLength(m_iChannel);
}

void CDevCapture::SetIFrameNum(int number)
{
    m_preSeconds = number;
    if (m_preSeconds == 0)
    {
        m_pBufferManager->clear(m_iChannel);
    }
    //m_BufferManager.SetIFrameNum(m_iChannel, number);
}



VD_BOOL CDevCapture::GetState(uint dwStreamType)
{
    VD_BOOL bDSPState = m_MonStreamType & BITMSK(dwStreamType) ? TRUE : FALSE;
    if (dwStreamType == CHL_MAIN_T)
    {
        bDSPState = bDSPState || (m_RecType != 0);
    }
    return ((m_captureFormat[dwStreamType].AVOption != 0) && bDSPState);
}



/*!
    \b Description        :    开启编码设备\n
    \b Argument            :    CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags, uint dwStreamType
    \param[in]    pObj    :    开启设备的对象指针
    \param[in]    pProc    :    函数指针
    \param[in]    dwFlags    :    开启捕获设备的用途
    \param[in]    dwStreamType    :    开启主辅码流
    \return    TRUE:成功；FALSE:失败        

    \note    同一个对象只能开一个，而且开启必须使用Stop关闭
    \par    示例
    \code    
    //开启辅码流的监视数据
    Start(this, OnXXX, DATA_MONITOR, CHL_2END_T);
    \endcode

    \b Revisions        :    
*/

extern int g_SystemAuthFail;


VD_BOOL CDevCapture::Start(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags, uint dwStreamType)
{
	if(g_SystemAuthFail > 0)
	{
		 return FALSE;
	}

    if ((dwStreamType > CHL_FUNCTION_NUM) 
        || (dwFlags <= DATA_READY) || (dwFlags >= DATA_ALL))
    {
        return FALSE;
    }

    if (dwStreamType != CHL_JPEG_T)
    {
        if (m_captureFormat[dwStreamType].AVOption == 0)
        {
            tracepoint();
           // return FALSE;
        }
    }
	
    //开启Capture和取数据线程
    if (((m_MonStreamType & BITMSK(dwStreamType)) == 0)
        && ((dwStreamType != CHL_MAIN_T && dwStreamType != CHL_2END_T) || ((m_RecType == 0) && ( dwStreamType == CHL_MAIN_T)) || ((m_RecType2snd == 0) && ( dwStreamType == CHL_2END_T))))
    {
        if (CaptureStart(m_iChannel, dwStreamType))
        {
            trace("Channel = %d, CDevCapture::Start CaptureStart failed\n", m_iChannel);
            return FALSE;
        }
    }

    if ((m_MonStreamType == 0) && (m_RecType == 0) && (m_RecType2snd == 0))
    {    
#if  defined(WIN32) || defined(DVR_HI)
            m_pManager->Start();
#else
sdfa
            if (!CreateThread())
            {
                trace("CDevCapture CreateThread failed!!!!\n");
                return FALSE;
            }
#endif
    }

    if (dwFlags & DATA_BUFFER)
    {
    	if(dwStreamType == CHL_MAIN_T)
    	{
       	 	m_RecType |= DATA_BUFFER;
		}
		else
		{
			m_RecType2snd |= DATA_BUFFER;
		}
    }
   
    //注册录像函数
    if (dwFlags & DATA_RECORD)
    {
        int RecSlots = m_sigRecord.Attach(pObj, pProc);
        assert(RecSlots >= 0 || RecSlots == -2);

        //开启取数据线程
        if (RecSlots == 1)
        {
            adjustPreBuffer();
            //m_BufferManager.Start(m_iChannel);
        }
        m_RecType |= DATA_RECORD;
    }

    //注册监视函数
    if (dwFlags & DATA_MONITOR)
    {
        int iRet = m_psigMonitor[dwStreamType]->Attach(pObj, pProc);
        assert(iRet > 0 || iRet == -2);
        m_MonStreamType |= BITMSK(dwStreamType);
        
        m_dwMonitorCnt++; //监视数加1,added by billhe at 2009-5-20
        printf("\r\nAdd moniter to stream %d, total %d viewer.\r\n", dwStreamType, m_dwMonitorCnt);
    }
    
    return TRUE;
}

VD_BOOL CDevCapture::Start(uint dwStreamType)
{
    return !CaptureStart(m_iChannel,dwStreamType); 
}

VD_BOOL CDevCapture::Stop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwFlags, uint dwStreamType)
{
    if ((dwStreamType > CHL_FUNCTION_NUM) || (dwFlags <= DATA_READY) || (dwFlags >= DATA_ALL))
    {
        return FALSE;
    }

    if (dwFlags & DATA_MONITOR)
    {
        int MonitorSlots = m_psigMonitor[dwStreamType]->Detach(pObj, pProc);
        //assert(MonitorSlots >= 0);

        if (MonitorSlots == 0)
        {
            m_MonStreamType &= ~BITMSK(dwStreamType);
        }
        m_dwMonitorCnt--; //监视数减1,added by billhe at 2009-5-20
        printf("\r\nRemove moniter from stream %d, total %d viewer.\r\n", dwStreamType, m_dwMonitorCnt);
    }

    if (dwFlags & DATA_BUFFER)
    {
        m_RecType &= ~DATA_BUFFER;
        m_pBufferManager->clear(m_iChannel);
        m_preBufferState = FALSE;
    }
    
    if (dwFlags & DATA_RECORD)
    {
        int RecordSlots = m_sigRecord.Detach(pObj, pProc);
        //assert(RecordSlots >= 0);

        if (RecordSlots == 0)
        {
            m_RecType &= ~DATA_RECORD;
        }
    }
    if (((m_MonStreamType & BITMSK(dwStreamType)) == 0) 
        && ((dwStreamType != CHL_MAIN_T && dwStreamType != CHL_2END_T)|| ((dwStreamType == CHL_MAIN_T) && (m_RecType == 0)) || ((dwStreamType == CHL_2END_T) && (m_RecType2snd == 0))))
    {
        if (CaptureStop(m_iChannel,dwStreamType)) 
        {
            trace("CDevCapture::Stop CaptureStop failed\n");
            if (dwFlags & DATA_MONITOR)
            {
                m_psigMonitor[dwStreamType]->Attach(pObj, pProc);
            }
            if (dwFlags & DATA_RECORD)
            {
                m_sigRecord.Attach(pObj, pProc);
            }
            return FALSE;
        }
    }

    if ((m_MonStreamType == 0) && (m_RecType == 0) && (m_RecType2snd == 0))
    {
#if defined(WIN32)  || defined(DVR_HI)
        m_pManager->Stop();
#else
        if (!DestroyThread())
        {
            trace("CDevCapture DestroyThread failed!!!!\n");
        }
#endif
    }

    if (m_RecType == 0)
    {
        //m_BufferManager.ClearData(m_iChannel);
    }
    return TRUE;    // 6
}

VD_BOOL CDevCapture::Stop(uint dwStreamType /* = CHL_MAIN_T */)
{
    return !CaptureStop(m_iChannel, dwStreamType);
}
#ifdef ENC_ENCODE_MULTI

VD_BOOL CDevCapture::extStart(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwStreamType)
{
    int iSlots = m_sigExtRecord.Attach(pObj, pProc);
    assert(iSlots > 0);
    return TRUE;
}

VD_BOOL CDevCapture::extStop(CObject * pObj, SIG_DEV_CAP_BUFFER pProc, uint dwStreamType)
{
    int iSlots = m_sigExtRecord.Detach(pObj, pProc);
    assert(iSlots >=0);
    return TRUE;
}

void CDevCapture::OnExtData(CPacket * pPacket, uint dwStreamType)
{
    m_sigExtRecord(m_iChannel, dwStreamType, pPacket);
}

#endif


//related to bitrate combox string in "PageConfigEncode.cpp"
//

VD_BOOL CDevCapture::SetFormat(CAPTURE_FORMAT *pFormat,uint dwType)
{
	_printd("CDevCapture::SetFormat");
    if (dwType == CHL_MAIN_T)
    {
        pFormat->Compression = getRealCompress(pFormat->Compression);
        pFormat->ImageSize = getRealImageSize(pFormat->ImageSize);
    }
    CAPTURE_CAPS Caps;
    memset(&Caps, 0, sizeof(CAPTURE_CAPS));
    CaptureGetCaps(&Caps);
    if( Caps.Reserved & BITMSK(0) )
    {
		//!各通道能力可能不一样
		memset(&Caps, 0, sizeof(CAPTURE_CAPS));
		GetChnCaps(m_iChannel, &Caps);
		/* 不支持格式先统一按照主按照CIF，副按照QCIF实现*/
		if ( dwType == CHL_MAIN_T )
		{
			 if (!(Caps.ImageSize & BITMSK(pFormat->ImageSize)))
			 {
			    pFormat->ImageSize = CAPTURE_SIZE_CIF;
			 }
		}
        else if (dwType == CHL_2END_T)
        {
           CAPTURE_EXT_STREAM exCaps;
           CaptureGetExtCaps(&exCaps);
           switch(pFormat->ImageSize)
           {
                case CAPTURE_SIZE_HD1:
                case CAPTURE_SIZE_CIF:     
                    if ( (Caps.ImageSize & BITMSK(CAPTURE_SIZE_D1))&& (exCaps.CaptureSizeMask[CAPTURE_SIZE_D1] & (BITMSK(pFormat->ImageSize))))
                    {
                        pFormat->ImageSize = CAPTURE_SIZE_CIF;
                    }
                    else
                    {
                        pFormat->ImageSize = CAPTURE_SIZE_QCIF;
                    }
                    break;
                case CAPTURE_SIZE_QVGA:
                    break;
                case CAPTURE_SIZE_QCIF:
                default:
                    pFormat->ImageSize = CAPTURE_SIZE_QCIF;
                    break;
           }
        }
    }
    memcpy(&m_captureFormat[dwType], pFormat, sizeof(CAPTURE_FORMAT));
    return !CaptureSetFormat(m_iChannel, dwType, pFormat);
}

const CAPTURE_FORMAT * CDevCapture::GetFormat(uint dwType) const
{
    return &m_captureFormat[dwType % CHL_FUNCTION_NUM];
}


VD_BOOL CDevCapture::getImageSize(capture_size_t* pSize, uint dwType)
{
#ifdef DVR_GBE
    return CaptureGetImageSize(m_iChannel, dwType, pSize) == 0 ? TRUE : FALSE;
#else
    return *pSize = (capture_size_t)m_captureFormat[dwType].ImageSize;
#endif        
}

/*!
    \b Description        :    字符串转换成点阵\n
    \b Argument            :    BYTE * pBuffer, PSIZE pSize, PCRECT pRect, PCSTR str, FONTSIZE sizetype
    \param    pBuffer        :    存放点阵的指针
    \param    pRect        :    背景区域大小
    \param    pSize        :    存放点阵的宽度和高度
    \param    str            :    转换的字符串
    \param    sizetype    :    字体类型，即大字体还是小字体
    \return                

    \b Revisions        :    
*/
void CDevCapture::TextToRaster(uchar * pBuffer, VD_PSIZE pSize, VD_PCRECT pRect, VD_PCSTR str, FONTSIZE sizetype)
{
    ushort code;//字符unicode
    int n;//字符计数
    int cw;//字符宽度
    int cl;//字符字节数
    int ox, ox_start, ox_end;//点阵偏移
    int oy, oy_start, oy_end;//点阵偏移

    int xoffset =0;//x坐标
    uchar * p;//点阵缓冲
    uchar raster[128];

    VD_SIZE fontsize; 

    int x,y; //起点坐标

    int len = (int)strlen(str);

    x = pRect->left;
    y = pRect->top;
	_printd("TextToRaster pBuffer [%s]str[%s]", pBuffer, str);
    if (x%8)
    {
        trace("the X offset  is not match\n");
        return;
    }

    //这里可能统一使用大字体会好点，需要测试
    g_Locales.GetFontSize(&fontsize, sizetype);

    oy_start = 0;

    if(y + fontsize.h >= pRect->bottom)
    {
        oy_end = pRect->bottom - y;
    }
    else
    {
        oy_end = fontsize.h;
    }

        
    for(n = 0; n < len; n += cl/*, x += cw*/)
    {
        code = g_Locales.GetCharCode(&str[n], &cl);
        if(cl == 0)
        {
            break;
        }
        if(code == '\t')
        {
            xoffset = 96;
        }

        cw = g_Locales.GetCharRaster(code, raster, sizetype);
        if ((cw == 0) && (sizetype == FONTSIZE_SMALL))
        {
            //小字体加载失败，再重新加载大字体
            cw = g_Locales.GetCharRaster(code, raster, FONTSIZE_NORMAL);
        }

        p = raster;

        ox_start = 0;
        ox_end = cw;
        if(xoffset + cw >(pRect->right - pRect->left) * 8)
        {
            ox_end = (pRect->right - pRect->left) * 8 - xoffset;
        }
        
        for (oy = oy_start; oy < oy_end; oy++)
        {
            for (ox = ox_start; ox < ox_end; ox++)
            {
                if (*(p + ox / 8) & (128 >> (ox % 8)))
                {
                    pBuffer[(y+oy)*pSize->w/8+(x+xoffset+ox)/8] |= BITMSK(7-(xoffset+ox)%8);
                }
            }
            p += fontsize.w/8;
        }
        xoffset += cw;
    }
}

/*!
    \b Description        :    设置编码叠加\n
    \b Argument            :    CAPTURE_TITLE_PARAM *pTitle, PCRECT pRect, PCSTR str
    \param    pTitle        :   叠加字幕结构指针
    \param    pRect        :   叠加字幕区域大小
    \param  str            :    参加的字幕
    \return        TURE:叠加成功；FALSE:叠加失败        

    \b Revisions        :    
*/
VD_BOOL CDevCapture::SetTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect, VD_PCSTR str, FONTSIZE fontsize)
{
    int iRet = 0;
    VD_SIZE Size;
    CAPTURE_CAPS tmpCapCaps;
    GetCaps(&tmpCapCaps);
    if((pTitle->index < OSD_OVERLAY_TIME) || (pTitle->index > OSD_OVERLAY_NR)/*( pTitle->index >= tmpCapCaps.TitleCount)*/)
    {
        trace("the index is %d, out of range(%d)\n", pTitle->index, tmpCapCaps.TitleCount);
        return FALSE;
    }
    Size.w = pTitle->width;
    Size.h = pTitle->height;

    memset((void*)&m_RasterBuf[0], 0, RASTER_LENGTH);

    if (Size.w * Size.h / 8 > RASTER_LENGTH)
    {
        trace("the length of string is out of length\n");
        return FALSE;
    }
    if(OSD_OVERLAY_TIME != pTitle->index && NULL == str)
    {
        return FALSE;
    }
    if(OSD_OVERLAY_TITLE == pTitle->index
		||OSD_OVERLAY_PRESET == pTitle->index
		)
    {
        TextToRaster(m_RasterBuf, &Size, pRect, str, fontsize);
        pTitle->raster = m_RasterBuf;
		sprintf (pTitle->RasStr, "%s", str);
    }
	

	if(OSD_OVERLAY_TITLE == pTitle->index)
	{
	    pTitle->index = OSD_OVERLAY_MULTILINE;
	}

    iRet = CaptureSetTitle(m_iChannel, pTitle);
    return (0 == iRet ? TRUE : FALSE);
}

/*!
    \b Description        :    获得码流\n

    \b Revisions        :    
*/
uint CDevCapture::GetBitRate(int dwType)
{
    if(dwType == CHL_MAIN_T)
    {
        return m_BitRate;
    }
    else if(dwType == CHL_2END_T)
    {
        return m_ExBitRate;
    }
    return 0;
}


/*!
    \b Description        :    计算码流\n

    \b Revisions        :    
*/
void CDevCapture::CalculateBitRate()
{
    uint dwMS, dwIntervalMS;

    dwMS = SystemGetMSCount();
    dwIntervalMS = dwMS - m_dwMS;

    if (dwIntervalMS > 0)
    {
        //以Kb/s为单位
        //以字节为单位，先除后乘，解决超出范围，使码流变小的问题
        m_BitRate = m_dwBytes * 8 / 1024 * 1000 / dwIntervalMS;
    }

    m_dwBytes = 0;
    m_dwMS = dwMS;
}
/*!
    \b Description        :    计算辅助码流1\n

    \b Revisions        :    
*/
void CDevCapture::CalculateExBitRate()
{
    uint dwMS, dwIntervalMS;

    dwMS = SystemGetMSCount();
    dwIntervalMS = dwMS - m_dwExMS;

    if (dwIntervalMS > 0)
    {
        //以Kb/s为单位
        m_ExBitRate = m_dwExBytes * 8 / 1024 * 1000 / dwIntervalMS;    
    }

    m_dwExBytes = 0;
    m_dwExMS = dwMS;
}



/*!
    \b Description        :    强制I帧\n

    \b Revisions        :    
*/
VD_BOOL CDevCapture::SetIFrame(uint dwType)
{
    return !CaptureForceIFrame(m_iChannel, dwType);
}

VD_BOOL CDevCapture::adjustPreBuffer()
{
    VD_BOOL bRet = FALSE;
    if (m_preSeconds)
    {
        // 只有预录开的情况下才有用
        m_preBufferState = TRUE;
        m_pBufferManager->setBuffer(m_preSeconds, m_iChannel);    
        bRet = TRUE;
    }
    else
    {
        m_preBufferState = FALSE;
    }
    
    return bRet;    
}

void CDevCapture::OnData(CPacket * pPacket, uint dwType)
{
#if 0
            static FILE *fp = NULL; 
            static int flag = 0;
            if(flag == 0)
            {
                //printf("\r\n\r\n @@@@@@@@@@@@@@@@nwrite capture date to test file@@@@@@@@@@@@@@@@\r\n\r\n");

                flag++;

                if(NULL == fp)
                {
                    fp = fopen("/tmp/test.264","w+b");
                    if (fp == NULL)     perror("Can't create file");
                }
            }            
            if(fp != NULL && m_iChannel == 0)
            {
                //printf("\r\n\r\n !!!!!!!!!!!nwrite capture date to test file:len:%d!!!!!!!!!!!\r\n\r\n",pPacket->GetLength());
                fwrite(pPacket->GetBuffer(),pPacket->GetLength(),1,fp);     
                 fflush(fp);
            }
            
#endif

    if (dwType == CHL_MAIN_T)
    {
        statFrameInfo(pPacket);

        //以B为单位
        m_dwBytes += pPacket->GetLength();
        if (m_RecType != 0)
        {
            if (m_preBufferState)
            {
                //说明需要取录像数据
                CPacket *pPkt = NULL;
                uint iLength = 0;
                int iRet = 0;
                while ((iRet = m_pBufferManager->popBuffer(&pPkt, m_iChannel)) == 0)
                {
                    //取到的数据写往硬盘
                    m_sigRecord(m_iChannel, CHL_MAIN_T, pPkt);
                    iLength += pPkt->GetLength();
                    pPkt->Release();
                    if (iLength >= pPacket->GetLength() * 4)
                    {
                        break;
                    }
                }

                if (iRet != 0)
                {
                    //说明已经取空了
                    m_preBufferState = FALSE;
                }
            }
            
            if (m_preBufferState == FALSE)
            {
                //说明空了
                //printf("%d %d %d\n", m_logic_chn, CHL_MAIN_T, pPacket->GetLength());
                m_sigRecord(m_logic_chn, CHL_MAIN_T, pPacket);
            }
            
            if (m_preSeconds)
            {
                //说明需要预录
                if (m_pBufferManager->pushBuffer(pPacket, m_iChannel))
                {
                    // 回调存在异常，m_dwBytes会变大，且是死循环，导致嵌入深入死机。
                    // OnData(pPacket, dwType);
                    
                    // 执行到这里只有一个情况：即缓冲区满且数据全是录像数据。
                    // 但是由于取录像数时长度大于当前包的长度，所以不应该执行到此，否则有异常。
                    tracepoint();
                }
            }
            //m_sigRecord(m_iChannel, CHL_MAIN_T, pPacket);
        }        
    }
    else if(dwType == CHL_2END_T)
    {
        //以B为单位
        m_dwExBytes += pPacket->GetLength();
    }
	if (m_iChannel == 0 && dwType == CHL_2END_T)
    {
        //printf("%d %d\n", dwType, pPacket->GetLength());
    }
    if (m_MonStreamType > 0)    
    {
        (*(m_psigMonitor[dwType]))(m_logic_chn, dwType, pPacket);
    }
}

void CDevCapture::OnRecData(CPacket ** pkt)
{
    m_sigRecord(m_logic_chn, CHL_MAIN_T, *pkt);
    (*pkt)->Release();
}

int CDevCapture::GetVolume(int *pVolume)
{
#ifdef FUNC_COURT_DISPLAY
    return CaptureGetVolume(m_iChannel, pVolume);
#else
    if (pVolume)
    {
        *pVolume = 50;
        return 0;
    }
    return 1;
#endif
}

// 获取监视数 added by billhe at 2009-5-20
uint CDevCapture::GetMonitorCount()
{
    return m_dwMonitorCnt;
}

#ifdef _2761_EXCEPTION_CHK_
void CDevCapture::ChkAVException()
{
	if (m_captureFormat[0].AVOption &0x1) //视频使能，只检查主码流
	{
		m_AVExceptionStatus &= 0x2;
		int newVideoFrames =  m_frameStatInfo[0].frameNumber+ m_frameStatInfo[1].frameNumber;
		if (m_LastVideoFrames == newVideoFrames)
		{
			m_AVExceptionStatus |= 0x1;
		}
		m_LastVideoFrames = newVideoFrames;
	}

	//传递到Encode.cpp的monitor函数中不实现重启，在此实现重启
	if ((m_AVExceptionStatus != 0))
	{
		m_AVExceptionStatus = 0;
		m_ExceptionTimes ++;
		//连续10次则重启，相当于30秒
		if (m_ExceptionTimes == 10)
		{
			tracef("NO data  need reboot !\n");
			g_Challenger.Reboot(0,SHUT_NODATA);
		}	
	}
	else
	{
		m_ExceptionTimes = 0;
	}
}

int CDevCapture::GetAVException()
{
	return m_AVExceptionStatus;
}
#endif

//#ifdef DVR_GB
//#ifndef VN_IPC//libhicap暂未合并，先对2700屏蔽抓拍的相关代码
///*!
//    \b Description        :    设置抓图参数\n
//    \b Argument            :    char imgq, int second, int millisecond
//    \param    imgq        :    画质
//    \param    second        :    秒
//    \param    millisecond    :    毫秒
//    \return    成功为TURE，失败为FALSE
//
//    \b Revisions        :
//*/
//VD_BOOL CDevCapture::SetSnapParam(char imgq, int second, int millisecond)
//{
//    return CaptureSetSnapParam(m_iChannel, imgq, second, millisecond) == 0;
//}
//
///*!
//    \b Description        :    获取图片数据\n
//    \b Argument            :    CPacket **pPkt
//    \param    pPkt        :    包的指针
//    \return    取到数据为TRUE，否则为FALSE
//
//    \b Revisions        :
//*/
//VD_BOOL CDevCapture::GetSnapBuffer(CPacket **pPkt)
//{
//    VD_HANDLE hMemory = 0;
//    uint dwTimeout = TIMEOUT;
//    uint GetLen = 0;
//    if ((GetLen = CaptureGetSnapBuffer(m_iChannel, &hMemory, dwTimeout)) == 0)
//    {
//        return FALSE;
//    }
//
//    *pPkt = (CPacket *)hMemory;
//
//    assert(*pPkt && (*pPkt)->GetRef() == 1);
//
//    return GetLen != 0;
//
//}
//#endif
//#endif

/*!
    \class CCaptureManager
    Description        :把多线程改成单线程，使用了这个对象，主要是通过这个线程取得所有数据，再分发给对应的捕获设备\n  
    \b Revisions    :
    
*/

void CDevCapture::statFrameInfo(CPacket *packet)
{
    PKT_HEAD_INFO* pHead = (PKT_HEAD_INFO*) packet->GetHeader();

    int delay = 0;
    int i;
    bool ifound = false;
    for(i = 0; i < FRAME_MAX_NUM; i++)
    {
        int type = pHead->FrameInfo[i].FrameType;
        int flag = pHead->FrameInfo[i].FrameFlag;

        if(type == 0xff)
        {
            i++;
            break;
        }

        if(type >= maxFrameType)
        {
            type = maxFrameType - 1;
        }

        if(flag != 0)
        {
            m_totalFrames++;
        }

        if(flag == 2 || flag == 3)
        {
            m_frameStatInfo[type].frameNumber++;
            m_frameStatInfo[type].totalSize += pHead->FrameInfo[i].FrameLength;
            if(m_frameStatInfo[type].maxSize < (int)pHead->FrameInfo[i].FrameLength)
            {
                m_frameStatInfo[type].maxSize = (int)pHead->FrameInfo[i].FrameLength;
            }
        }

        if(type == 1)
        {
            if(flag == 1)
            {
                m_firstIPacketTime = SystemGetMSCount();
            }
            else if(flag == 2) // 只有I帧尾，延时就是首尾之差
            {
                delay = SystemGetMSCount() - m_firstIPacketTime;
            }
            else if(flag == 3)// 有I帧头尾，延时是组包引起的延时
            {
                if(!ifound)
                {
                    ifound = true;
                    continue; // 跳出，避免delay多加一次
                }
            }
        }
        if(ifound && (type == 0 || type == 1 || type == 2))
        {
            delay += 1000 / m_captureFormat[CHL_MAIN_T].FramesPerSecond;
        }
    }

    if(m_IFrameDelayMax < delay)
    {
        m_IFrameDelayMax = delay;
    }

    m_packetNumber++;

    if(m_maxFramesInPacket < i)
    {
        m_maxFramesInPacket = i;
    }
}

//int CDevCapture::BindAnalogChn( ANALOG_CHN analog_chn )
//{
//    m_iChannel = analog_chn;
//    return 0;
//}

ANALOG_CHN& CDevCapture::GetAnalogChn()
{
    return m_iChannel;
}

void CDevCapture::SetSuspend(int suspend)
{
	if(NULL != m_pManager){
		m_pManager->SetSuspend(suspend);
	}
}
void CCaptureManager::ThreadProc()
{
    VD_HANDLE hMemory = 0;
    uint    streamType = 0;
    CPacket *pPacket;
    int iChannel = -1;
	int iCount	 = 0;
	CAPTURE_DSPINFO dspinfo;

	
	
    GetDspInfo(&dspinfo);

	SET_THREAD_NAME("VideoCaptureGetBuffer");
	
    while (m_bLoop)
    {
        if (CaptureGetBuffer(&iChannel, &streamType, &hMemory) == -1)
        {
            trace("CDevCapture::ThreadProc() CaptureGetBuffer failed\n");
            continue;
        }
        pPacket = (CPacket *)hMemory;
		
		if (NULL == pPacket)
			_printd(" error=================get NULL packet?  \n");
		if(NULL != pPacket && 1 !=pPacket->GetRef())
			_printd("============ pPacket->GetRef() %d  streamType[%d] iChannel[%d]", pPacket->GetRef(), streamType, iChannel);

		if(iChannel >= 0 && m_iSuspend){
			if((iCount++ % 100) == 0){
				_printd("The system is suspend");
			}
			pPacket->Release(); 
			continue;
		}		
        assert(pPacket && pPacket->GetRef() == 1);

        assert(iChannel >=0);
        if ( m_pDevCap[iChannel] == NULL )
        {
            trace("CCaptureManager::ThreadProc-not create channel:%d\n", iChannel);
            pPacket->Release();        
            continue;
        }

        if (iChannel < g_nCapture)
        {
#ifndef SHREG
	         m_pDevCap[iChannel]->OnData(pPacket, streamType);
#else

	        if (streamType == 1)
	        {
	            pPacket->Release();
	            continue;
	        }

	        m_pDevCap[iChannel]->OnData(pPacket, streamType);

	        if (streamType != 0)
	        {
	            pPacket->Release();
	            continue;
	        }

	        if (CConfigEncode::getLatest(iChannel).dstExtraFmt[ENCODE_TYPE_TIM].bVideoEnable == 0)
	        {
	            pPacket->Release();
	            continue;
	        }

	        PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)pPacket->GetHeader();
	        int iFrameNum = 0;
	        int iFrameType = 0;
	    
		    // 共FRAME_MAX_NUM个定位信息
		    for (int iFrameId=0; iFrameId<FRAME_MAX_NUM; iFrameId++)
		    {    
		        uchar iFrameType = pPacketHead->FrameInfo[iFrameId].FrameType;//祯类型
		        int iLocation = pPacketHead->FrameInfo[iFrameId].FramePos;//帧开始位置
		        uint iFrameDataLen = pPacketHead->FrameInfo[iFrameId].DataLength;///*!< 帧在本块中的长度 */
		        uint iFrameTotalLen = pPacketHead->FrameInfo[iFrameId].FrameLength;
		        uchar iFrameFlag = pPacketHead->FrameInfo[iFrameId].FrameFlag;//帧头帧尾标识

		        if( PACK_TYPE_FRAME_NULL == iFrameType) 
		        {
		            break;
		        }
		        
		        if (PACK_TYPE_FRAME_I == iFrameType)
		        {
		            CPacket *pnewPacket = g_PacketManager.GetPacket(iFrameDataLen);
		            PKT_HEAD_INFO *pnewPacketHead =  (PKT_HEAD_INFO *)pnewPacket->GetHeader();
		            memset(pnewPacketHead,0,sizeof(PKT_HEAD_INFO));
		            pnewPacketHead->FrameInfo[0].FrameType = pPacketHead->FrameInfo[iFrameId].FrameType;
		            pnewPacketHead->FrameInfo[0].FramePos = 0;
		            pnewPacketHead->FrameInfo[0].DataLength = pPacketHead->FrameInfo[iFrameId].DataLength;
		            pnewPacketHead->FrameInfo[0].FrameLength = pPacketHead->FrameInfo[iFrameId].FrameLength;
		            pnewPacketHead->FrameInfo[0].FrameFlag = pPacketHead->FrameInfo[iFrameId].FrameFlag;
		            memcpy(pnewPacket->GetBuffer(),pPacket->GetBuffer() + pPacketHead->FrameInfo[iFrameId].FramePos,
		            pPacketHead->FrameInfo[iFrameId].DataLength);

		            if (pPacketHead->FrameInfo[iFrameId].FrameFlag == PACK_CONTAIN_FRAME_HEAD
		                ||pPacketHead->FrameInfo[iFrameId].FrameFlag == PACK_CONTAIN_FRAME_HEADTRAIL)
		            {
		                uchar *pData = pnewPacket->GetBuffer();
		                pData[5] = 2;//写 死2秒一帧
		                int a = 0x2 << 5;
		                pData[5] = pData[5] |(0x2<<5);
		            }

		            pnewPacket->SetLength(pPacketHead->FrameInfo[iFrameId].DataLength);

		            //		printf("m_pDevCap[%d]->OnData PACK_TYPE_FRAME_I \r\n",iChannel);
		            m_pDevCap[iChannel]->OnData(pnewPacket, 1);
		            pnewPacket->Release();
		        }
		    }
#endif

        }
        else
        {
#ifdef ENC_ENCODE_MULTI
            //转换通道,四画面的以支持的通道号开始
            assert(dspinfo.nExpandChannel> 0);
            uint block = (iChannel - g_nCapture) / dspinfo.nExpandChannel;
            m_pDevCap[block * dspinfo.nMaxSupportChannel + iChannel - g_nCapture - block * dspinfo.nExpandChannel]->OnExtData(pPacket, streamType);
#endif
        }

        pPacket->Release();        
    }
}


CCaptureManager::CCaptureManager() : CThread("CaptureManager", TP_CAPTURE),m_BitRateTimer("CAP_BitRate")
{
    m_iUser = 0;
    int i;
    for (i = 0; i < g_nCapture; i++)
    {
        m_pDevCap[i] = NULL;
    }
    m_BitRateTimer.Start(this, (VD_TIMERPROC)&CCaptureManager::OnCap, 0, 3*1000); //OnCap中需要判断设备指针是否有效

    m_statTime = 0;
	m_iSuspend = 0;
}

CCaptureManager::~CCaptureManager()
{
}

void CCaptureManager::Init(int iChannel, CDevCapture * pDevCap)
{
    CGuard guard(m_Mutex);
    m_pDevCap[iChannel] = pDevCap;
    
}

void CCaptureManager::OnCap(uint arg)
{
    for (int i = 0; i < g_nCapture; i++)
    {
        if(m_pDevCap[i])
        {
            m_pDevCap[i]->CalculateBitRate();
            m_pDevCap[i]->CalculateExBitRate();
#ifdef _2761_EXCEPTION_CHK_
		if(CConfigEncoderDetect::getLatest().bEnable)
		{
			m_pDevCap[i]->ChkAVException();
		}	
#endif
        }
    }
}

void CCaptureManager::Start()
{
    CGuard guard(m_Mutex);
    if (m_iUser == 0)
    {
        CreateThread();
    }
    m_iUser++;
}

void CCaptureManager::Stop()
{
    CGuard guard(m_Mutex);
    m_iUser--;
    if (m_iUser == 0)
    {
        DestroyThread();
    }
}

void CCaptureManager::dumpStatInfo()
{
    static char* frameTypes[CDevCapture::maxFrameType] = {"P", "I", "B", "A", "?"};

    uint total_bitrate = 0;
    uint total_packet = 0;
    CDevCapture* pdc;

    int currentTime = SystemGetMSCount();
    int i, j;

    trace("Encoder Bitrate  Avg.   Avg.    Max.   Max. ");
    for(j = 0; j < CDevCapture::maxFrameType; j++)
    {
        trace(" %s-Frm Avg. Max. ", frameTypes[j]);
    }
    trace("\n");

    trace("Channel (kbps)  Pkt/S Frm/Pkt Frm/Pkt I-Dly ");
    for(j = 0; j < CDevCapture::maxFrameType; j++)
    {
        trace(" Frm/S Size Size ");
    }
    trace("\n");

    trace("--------------------------------------------");
    for(j = 0; j < CDevCapture::maxFrameType; j++)
    {
        trace("-----------------");
    }
    trace("\n");

    for (i = 0; i < g_nCapture; i++)
    {
        pdc = m_pDevCap[i];
        total_bitrate += pdc->GetBitRate();
        total_packet += pdc->m_packetNumber;

        trace("%2d    %8d  %5.1f %5.1f    %3d     %4d   ",
            i + 1,
            pdc->GetBitRate(),
            (float)pdc->m_packetNumber * 1000 / (currentTime - m_statTime),
            (float)pdc->m_totalFrames / MAX(1, pdc->m_packetNumber),
            pdc->m_maxFramesInPacket,
            pdc->m_IFrameDelayMax);

        for(j = 0; j < CDevCapture::maxFrameType; j++)
        {
            trace("%4.1f %4.1f %4.1f   ",
                (float)pdc->m_frameStatInfo[j].frameNumber * 1000 / (currentTime - m_statTime),
                (float)pdc->m_frameStatInfo[j].totalSize / MAX(1, pdc->m_frameStatInfo[j].frameNumber) / 1024,
                (float)pdc->m_frameStatInfo[j].maxSize/ 1024);

            pdc->m_frameStatInfo[j].frameNumber = 0;
            pdc->m_frameStatInfo[j].totalSize = 0;
            pdc->m_frameStatInfo[j].maxSize = 0;
            pdc->m_totalFrames = 0;
        }
        trace("\n");

        pdc->m_maxFramesInPacket = 0;
        pdc->m_IFrameDelayMax = 0;
        pdc->m_packetNumber = 0;
    }

    trace("--------------------------------------------");
    for(j = 0; j < CDevCapture::maxFrameType; j++)
    {
        trace("-----------------");
    }
    trace("\n");

    trace("Total %8d  %5.1f\n",
        total_bitrate,
        (float)total_packet * 1000 / (currentTime - m_statTime));

    trace("\n");

    m_statTime = currentTime;
}

void CCaptureManager::SetSuspend(int suspend)
{
	if(suspend > 1 || suspend < 0){
		m_iSuspend = 0; 
		_printd("the val of suspend is unnormal");
	}
	else{
		m_iSuspend = suspend;
	}

	_printd("set val of suspend is %d",m_iSuspend);
}

