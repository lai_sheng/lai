
#include <string.h>
#include "Devices/DevDetect.h"
#include "Devices/DeviceManager.h"

///////////////////////////////////////////////////////////////////
////////////        CDevMotionDetect
CDevMotionDetect* CDevMotionDetect::_instance = NULL;

CDevMotionDetect* CDevMotionDetect::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevMotionDetect();
	}
	return _instance;
}

CDevMotionDetect::CDevMotionDetect()
{
	MotionDetectCreate();
}

CDevMotionDetect::~CDevMotionDetect()
{
	MotionDetectDestory();
}

VD_BOOL CDevMotionDetect::GetCaps(MOTION_DETECT_CAPS * pCaps)
{
	if (!MotionDetectGetCaps(pCaps)) 
	{
		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDevMotionDetect::SetParameter(int channel, MOTION_DETECT_PARAM *pParam)
{
	if (!MotionDetectSetParameter(channel, pParam)) 
	{
		return TRUE;
	}
	return FALSE;
}

uint CDevMotionDetect::GetState()
{
	m_dwState = 0;
	MotionDetectGetState(&m_dwState);
	return m_dwState;
}

VD_BOOL CDevMotionDetect::ShowHint(int channel, int enable)
{
	if (!MotionDetectShowHint(channel, enable))
	{
		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDevMotionDetect::getDetectParam(int iChannel, MOTION_DETECT_RESULT* pParam)
{
	return !MotionDetectGetResult(iChannel,pParam);
}

///////////////////////////////////////////////////////////////////
////////////        CDevLossDetect

CDevLossDetect* CDevLossDetect::_instance = NULL;

CDevLossDetect* CDevLossDetect::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevLossDetect();
	}
	return _instance;
}

CDevLossDetect::CDevLossDetect()
{
	m_dwLossState = 0;
	LossDetectCreate();
}

CDevLossDetect::~CDevLossDetect()
{
	LossDetectDestory();
}

VD_BOOL CDevLossDetect::GetCaps(LOSS_DETECT_CAPS * pCaps)
{
	if (!LossDetectGetCaps(pCaps)) 
	{
		return TRUE;
	}
	return FALSE;
}

uint CDevLossDetect::GetState()
{
	m_dwState = 0;
	LossDetectGetState(&m_dwState);
	m_dwLossState = m_dwState;
	
	return m_dwState;
}

uint CDevLossDetect::GetLossState()
{
	return m_dwLossState;
}

///////////////////////////////////////////////////////////////////
////////////        CDevBlindDetect

CDevBlindDetect* CDevBlindDetect::_instance = NULL;

CDevBlindDetect* CDevBlindDetect::instance(void)
{
	if(NULL == _instance)
	{
		_instance = new CDevBlindDetect();
	}
	return _instance;
}

CDevBlindDetect::CDevBlindDetect()
{
	BlindDetectCreate();
}

CDevBlindDetect::~CDevBlindDetect()
{
	BlindDetectDestory();
}

VD_BOOL CDevBlindDetect::GetCaps(BLIND_DETECT_CAPS * pCaps)
{
	if (!BlindDetectGetCaps(pCaps)) 
	{
		return TRUE;
	}
	return FALSE;
}

VD_BOOL CDevBlindDetect::SetParameter(int channel, BLIND_DETECT_PARAM *pParam)
{
	if (!BlindDetectSetParameter(channel, pParam)) 
	{
		return TRUE;
	}
	return FALSE;
}

uint CDevBlindDetect::GetState()
{
	m_dwState = 0;
	BlindDetectGetState(&m_dwState);
	return m_dwState;
}

