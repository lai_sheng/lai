--
-- Copyright (C) 2005 dahua Technologies, All Rights Reserved.
-- 2006-4-25 15:54 Z:\wjj\ven\152\DAHUA\Install.lua
-- 2006-9-21  modified by zhongjl for new partition
--

-- Flash 块的大小为64K
local flashSectorSize = 0x10000;

local Installer = {};
Installer.TotalSize      = 0;    -- 总的要写到Flash中的数据大小
Installer.InProgressSize = 0;	 -- 用来在升级过程中控制进度

-- 通知上层应用程序升级的进度信息
-- params:
--    无
-- return:
--    无
function Installer:notify()
	self.InProgressSize = self.InProgressSize + flashSectorSize;

	local progress = self.InProgressSize / self.TotalSize * 100;
	if(progress > 100) then
		progress = 100;
	end;
	progress = tonumber(string.format("%d", progress));

	-- 注意调用方式,不是self::callback,否则会导致回调出错
	self.callback(0x01, progress);
end

-- 根据应用程序提供的信息判断是否可以升级
-- params:
--    无
-- return:
--    成功返回True, 失败返回False以及失败的原因
function Installer:preInstall()
	-- 这里我们需要考虑那些因数？
	-- 硬件版本号 ？
	-- 原有软件的版本号？
	--
	local board = Global.Hardware.board;
	local hwproduct;
	local hwchannel;
	local hwversion;
	local hwfunction;

	print(string.format("Checking hardware information,board name:%s version:%s",
		board.name, board.version));
	if(board.name ~= "DVR1604LB") then
		return false, "Invalid board";
	end

	local vendor = Global.Vendor;

	if(vendor.Name ~= 'VendorName' and vendor.Name ~= 'DAHUA') then
		return false, "Invalid vendor";
	end

	return true;
end

-- 升级完成后的处理,如控制系统重启
-- params:
--    无
-- return:
--    无
function Installer:postInstall()
end

function Installer:validateFile(filename, filesize)
	local myfile = self.ZipArchive:open(filename);
	-- 如果该文件打不开,则不需要对此部分进行升级
	if(not myfile) then
		print(string.format("%s not exist", filename));
		return false;
	end
	
	local data;
	local totalsize = 0;
	
	while true do
		data = myfile:read(flashSectorSize);
		if not data then
			break;
		end;
		totalsize = totalsize + string.len(data);
	end;
		
	myfile:close();
	
	if (totalsize ~= filesize) then
		print("totalsize ", totalsize);
		return false;
	end
	
	return true;	
end

-- 升级Flash分区,对于不同的应用,升级的处理可能不一样
-- params:
--         part: 表格,包含分区的起始位置以及结束位置
--     filename: 在升级包中的文件名
-- return:
--    无
function Installer:updatePart(part, filename)
	local myfile = self.ZipArchive:open(filename);
	-- 如果该文件打不开,则不需要对此部分进行升级
	if(not myfile) then
		print(string.format("%s not exist", filename));
		return ;
	end

	local addr   = part.baseAddr;
	local endAddr= part.endAddr;
	local data;
	local fldata;

	-- 跳过前面64字节的头,在新的升级程序版本中不需要对此进行校验
	myfile:seek("set", 64);
	while(addr < endAddr) do
		fldata = mtd.read(addr,flashSectorSize);
		data = myfile:read(flashSectorSize);    -- 读入一块Flash扇区大小的数据

		-- if (fldata and data and (fldata ~= data)) then
		-- 当fldata为空的时候为老的程序，直接擦除

		if(data) then
			if((not fldata) or (data and (fldata ~= data))) then
				mtd.erase(addr);
				mtd.write(addr, data);
			end
		  self:notify();
		  addr   = addr   + flashSectorSize;
		else
		  addr = endAddr;
		end
	end

	myfile:close();
end

function Installer:InstallPlayer(filename)
	local myfile = self.ZipArchive:open(filename);

	-- 如果该文件打不开,则不需要对此部分进行升级
	if(not myfile) then
		return ;
	end

	--
	-- TODO:
	-- 读入自动播放器的内容，通过IDE接口写入到硬盘
	--
end

-- 在这里控制整个升级过程
-- params:
--    无
-- return:
--    成功返回True,失败返回Flase以及错误原因
function Installer:execute()
	--[[
	 分区配置信息表,来自知识库 "Flash 分区规划"
	 80000000 － 80020000  128K     armboot
	 80020000 － 80030000  64K      出厂的配置信息
	 80030000 － 80230000  2048K    kernel + root
	 80230000 － 80780000  5540K    application + data + modules
	 80780000 － 807c0000  256K     config + log
	 807c0000 － 80800000  256K     vendor
	]]

	local flashPartions =
	{
		armboot = { baseAddr = 0x80000000  , endAddr = 0x80020000 },
		coustom  = { baseAddr = 0x80020000  , endAddr = 0x80030000 },
		rootfs  = { baseAddr = 0x80030000  , endAddr = 0x80630000 },
		web    = { baseAddr = 0x80630000  , endAddr = 0x80780000 },
		userdata= { baseAddr = 0x80780000  , endAddr = 0x807c0000 },
		logo  = { baseAddr = 0x807c0000  , endAddr = 0x80800000 },
	}

	-- self.ZipArchive 这个变量由外部程序设置,如果没有设置程序不应该运行到这里
	assert(self.ZipArchive);
	local zfile = self.ZipArchive;

	local ret, info = self:preInstall();
	if(not ret) then
		return false, info;
	end
	
	mtd.init();
	
	-- 计算要升级的文件大小以及打印升级文件清单
	local TotalSize = 0;
	print("==>Files in archive");
	for file in zfile:files() do
		print("file name: ", file.filename, "file size: ", file.uncompressed_size);
		TotalSize = TotalSize + file.uncompressed_size;
		if (not self:validateFile(file.filename, file.uncompressed_size)) then
			return false,"Validate Error!";
		end
	end
	self.TotalSize  = TotalSize;     -- 总的文件大小
	
	self:updatePart(flashPartions["coustom"],  "custom-x.cramfs.img");
	self:updatePart(flashPartions["rootfs"],    "romfs-x.cramfs.img");
	self:updatePart(flashPartions["web"],  "web-x.cramfs.img");
	self:updatePart(flashPartions["logo"],  "logo-x.cramfs.img");
	self:InstallPlayer("autoplayer.bin");
	self:postInstall();
	print("==>Upgrade finished.");
	return true;
end

return Installer;
