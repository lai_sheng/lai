
local config =
{
	-- 厂商的标记,不能随意修改,升级文件会对此进行检查
	Name = "DAHUA",
	
	-- 前面板特性
	Frontboard =
	{
		Number = 1,		-- 是否有数字键，1有，0无
		Shift = 1,		-- 是否有Shift键，1有，0无
		CorrectMap = 1,		-- 数字字母映射表是否正确，1正确，0错误
		NewATM = 1,		-- 是否为二代新ATM机面板, 1是, 0不是
	},

	-- 最多同时回放的通道数
	MaxPlaybackChannels=2,
	ForATM=0,

	-- GUI待命时间, 分钟为单位.时间到后, 本地用户自动注销, 同时关闭液晶屏.
	GUIStandbyTime = 10,

	-- 网络相关的默认值
	DefaultHostIp = "192.168.1.88",
	DefaultNetMask = "255.255.255.0",
	DefaultGateway = "192.168.1.1",
	UseDefaultIP = 0,

	-- 用户相关的默认值
	-- group
	INI_GROUP_NAME_ADMIN		= "admin";
	INI_GROUP_NAME_USER		= "user";
	-- user
	INI_SYS_USER_ADMIN		= "admin",
	INI_SYS_USER_ADMIN_PWD		= "admin",
	INI_SYS_USER_LOCAL		= "888888",
	INI_SYS_USER_LOCAL_PWD		= "888888",
	INI_USER_USER_LOCAL		= "666666",
	INI_USER_USER_LOCAL_PWD		= "666666",
	INI_DEFAULT_USER_NAME		= "default",
	INI_DEFAULT_USER_PWD		= "tluafed",

	-- 缺省的时间日期格式
	-- 可选值如下
	--[[
		enum date_fmt {
			DF_YYMMDD = 0,		//年 月 日
			DF_MMDDYY,		//月 日 年
			DF_DDMMYY,		//日 月 年
		};

		enum time_fmt {
			TF_24	= 0,		//24小时
			TF_12			//12小时
		};

		enum dst_rule
		{
			DST_OFF = 0,	//关闭
			DST_AUSTRALIA,	//澳洲规则
			DST_ITALY,	//意大利规则
			DST_NR,		//种类计数
		};
	]]

	DateFormatDefault = 0,
	TimeFormatDefault = 0,
	WeekStartDefault = 0, --一周开始的日期, 0-星期天, 1-星期一,...,6-星期六
	DaylightSavingTime = 0,

	--自动维护各项值说明：
	--[[
		enum auto_reboot_day
		{
			NERVER = 0,	//从不
			EVERYDAY,	//每天
			SUNDAY,		//周日
			MONDAY,       //周一
			TUESDAY,      //周二
			WEDNESDAY,    //周三
			THURSDAY,     //周四
			FRIDAY,       //周五
			SATURDAY,     //周六
		};

		enum auto_reboot_time
		{
	    0-0:00,
	    1-1:00,
	    ........
	    23-:23:00,
	  };

		enum auto_delete_file_time
		{
			NERVER = 0,	//从不
			ONE_DAY,	//24小时
			TWO_DAY,	//48小时
			THREE_DAY,	//72小时
			FOUR_DAY,	//96小时
			ONE_WEEK,	//一周
			ONE_MONTH,	//一个月
		};
	]]

	-- 自动维护相关默认值
	DefaultAutoRebootDay  = 1,
	DefaultAutoRebootTime = 2,
	DefaultAutoDeleteFileTime  = 0,
	
	--[[
	enum capture_size_t {
		CAPTURE_SIZE_D1 = 0,		///< 720*576(PAL)	720*480(NTSC)
		CAPTURE_SIZE_HD1,		///< 352*576(PAL)	352*480(NTSC)
		CAPTURE_SIZE_BCIF,		///< 720*288(PAL)	720*240(NTSC)
		CAPTURE_SIZE_CIF,		///< 352*288(PAL)	352*240(NTSC)
		CAPTURE_SIZE_QCIF,		///< 176*144(PAL)	176*120(NTSC)
		CAPTURE_SIZE_VGA,		///< 640*480(PAL)	640*480(NTSC)
		CAPTURE_SIZE_QVGA,		///< 320*240(PAL)	320*240(NTSC)
		CAPTURE_SIZE_SVCD,		///< 480*480(PAL)	480*480(NTSC)
		CAPTURE_SIZE_QQVGA,
		CAPTURE_SIZE_NR			///< 枚举的图形大小种类的数目。
	};
	]]
	DefaultImageSize=3,

};

return config;
