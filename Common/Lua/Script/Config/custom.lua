local custom =
{
	-- 发出去的程序支持的语言，
	-- 可选值：All, English, SimpChinese, TradChinese, Italian, Spanish, Japanese, Russian, French, German,Turkey,Hungarian,Finnish,Estonian,Farsi
	SupportedLanguage = "All",
	SupportedLanguageDefault = "SimpChinese",

	-- 支持的视频制式
	-- 可选值：All, PAL , NTSC
	SupportedVideoStand = "All",
	SupportedVideoStandDefault = "PAL",
	
	-- 是否显示图片目录下的Logo,在制作中性版本时设为0
	ShowLogo = 0,
}

return custom;
