--[[
这是YINGJIE(南望)云台控制协议.
1. 关于预置点:
   设置预置点和添加巡航中的预置点是同一条指令.
2. 关于PAN/TILE速度
   速度默认是一个不快步慢的默认值, 共有7级速度, 编号不连贯.
--]]
local Protocol = {};
Protocol.Attr = 
{
	Name = "YINGJIE",			
	Type = "PTZ",
	Internal = 200,
	CamAddrRange 		= {0x00, 0xFF}, 
	MonAddrRange		= {0x00, 0xFF},	
	PresetRange 		= {0x00, 0xFF},
	TourRange		= {0x01, 0x01},
	PatternRange		= {0xff, 0xff},
	TileSpeedRange 		= {1, 8},
	PanSpeedRange 		= {1, 8},
	AuxRange 		= {0x01, 0x08},
}

Protocol.CommandAttr =
{
	AddrPos 		= 2, 
	PresetPos 		= 4, 
	TileSpeedPos 		= 4,
	PanSpeedPos 		= 4,
	AuxPos 			= 6,
}

Protocol.Command = 
{
	Start= 
	{
		TileUp 		= "0xA0, 0x00, 0x04, 0x00, 0x00, 0x00",
		TileDown 	= "0xA0, 0x00, 0x04, 0x80, 0x00, 0x00",
		PanLeft 	= "0xA0, 0x00, 0x03, 0x80, 0x00, 0x00",
		PanRight 	= "0xA0, 0x00, 0x03, 0x00, 0x00, 0x00",
		ZoomWide 	= "0xA0, 0x00, 0x07, 0x00, 0x00, 0x00",
		ZoomTele 	= "0xA0, 0x00, 0x09, 0x00, 0x00, 0x00",
		FocusNear	= "0xA0, 0x00, 0x0C, 0x00, 0x00, 0x00",
		FocusFar 	= "0xA0, 0x00, 0x0B, 0x00, 0x00, 0x00",
		SetPreset 	= "0xA0, 0x00, 0x05, 0x00, 0x00, 0x00",
		ClearPreset	= "0xA0, 0x00, 0x05, 0x00, 0x40, 0x00",
		GoToPreset 	= "0xA0, 0x00, 0x06, 0x00, 0x00, 0x00",
		AutoPanOn	= "0xA0, 0x00, 0x01, 0x40, 0x00, 0x00",
		AutoPanOff	= "0xA0, 0x00, 0xFF, 0x00, 0x00, 0x00",
		AddTour 	= "0xA0, 0x00, 0x05, 0x00, 0x80, 0x00",
		DeleteTour 	= "0xA0, 0x00, 0x05, 0x00, 0xC0, 0x00",
		StartTour 	= "0xA0, 0x00, 0x1D, 0x01, 0x00, 0x00",
		StopTour 	= "0xA0, 0x00, 0xFF, 0x00, 0x00, 0x00",
	},
	Stop = 
	{
		TileUp 		= "0x0A, 0x00, 0xFF, 0x00, 0x00, 0x00",
		TileDown 	= "0x0A, 0x00, 0xFF, 0x00, 0x00, 0x00",
		PanLeft 	= "0x0A, 0x00, 0xFF, 0x00, 0x00, 0x00",
		PanRight 	= "0x0A, 0x00, 0xFF, 0x00, 0x00, 0x00",
		ZoomWide 	= "0x0A, 0x00, 0x0D, 0x00, 0x00, 0x00",
		ZoomTele 	= "0x0A, 0x00, 0x0D, 0x00, 0x00, 0x00",
		FocusNear 	= "0x0A, 0x00, 0x0D, 0x00, 0x00, 0x00",
		FocusFar 	= "0x0A, 0x00, 0x0D, 0x00, 0x00, 0x00",
	},
}

Protocol.Checksum = function (s)
	s[6] = math.mod(s[1] + s[2] + s[3] + s[4] + s[5], 256);
	return s;
end;

Protocol.SpeedProcess = function(s, ver, hor)
	if ver ~= 0 and s[3] ~= 0xFF then
		if ver == 1 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x07);
		elseif ver == 2 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x07);
		elseif ver == 3 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x06);
		elseif ver == 4 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x05);
		elseif ver == 5 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x00);
		elseif ver == 6 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x01);
		elseif ver == 7 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x02);
		elseif ver == 8 then
			s[Protocol.CommandAttr.TileSpeedPos] = bits.bor(s[Protocol.CommandAttr.TileSpeedPos], 0x03);
		end;
	end;
	if hor ~= 0 and s[3] ~= 0xFF then
		if hor == 1 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x07);
		elseif hor == 2 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x07);
		elseif hor == 3 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x06);
		elseif hor == 4 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x05);
		elseif hor == 5 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x00);
		elseif hor == 6 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x01);
		elseif hor == 7 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x02);
		elseif hor == 8 then
			s[Protocol.CommandAttr.PanSpeedPos] = bits.bor(s[Protocol.CommandAttr.PanSpeedPos], 0x03);
		end;
	end;
	return s;
end;

Protocol.SetTourProcess = function(opttable, tour, preset)
	opttable[Protocol.CommandAttr.PresetPos] = preset;
	return opttable;
end;

Protocol.PresetProcess = function(opttable, arg1)
	opttable[Protocol.CommandAttr.PresetPos] = arg1;
	return opttable;
end;

return Protocol;