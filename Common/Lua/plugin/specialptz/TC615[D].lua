--[[
该协议没有预置点, 也没有模式, 但是提供两种自动扫描方式: 一种通过普通控制指令执行, 另外一种通过扩展控制命令执行. 并且都没有左右边界设定的功能.
注意: 通过"辅助开"执行的自动扫描无法用普通指令中的停止自动扫描来停止, 只有通过"辅助关"来停止.
--]]
local Protocol = {};

Protocol.Attr = 
{
	Name = "TC615[D]",	
	Type = "PTZ",
	Internal = 200,		
	CamAddrRange 		= {0x00, 0x7F}, 
	MonAddrRange		= {0x00, 0xFF},	
	PresetRange 		= {0xFF, 0xFF},
	TourRange		= {0xFF, 0xFF},
	PatternRange		= {0xFF, 0xFF},
	TileSpeedRange 		= {0x00, 0x3F},
	PanSpeedRange 		= {0x00, 0x3F},
	AuxRange 		= {0, 5},
}

Protocol.CommandAttr =
{
	AddrPos 		= 2, 
	PresetPos 		= 6, 
	TileSpeedPos 		= 6,
	PanSpeedPos 		= 5,
	AuxPos 			= 6,
}

Protocol.Command = 
{
	Start= 
	{
		TileUp 		= "0xFF, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00",
		TileDown 	= "0xFF, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00",
		PanLeft 	= "0xFF, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00",
		PanRight 	= "0xFF, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00",
		LeftUp 		= "0xFF, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00",
		LeftDown 	= "0xFF, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00",
		RightUp		= "0xFF, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00",
		RightDown 	= "0xFF, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00",
		ZoomWide 	= "0xFF, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00",
		ZoomTele 	= "0xFF, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00",
		FocusNear	= "0xFF, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00",
		FocusFar 	= "0xFF, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00",
		IrisSmall 	= "0xFF, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00",
		IrisLarge 	= "0xFF, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00",			
		AutoPanOn 	= "0xFF, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00",
		AutoPanOff	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",		
		AlarmSearch 	= "0xFF, 0x00, 0x00, 0x41, 0x00, 0x00, 0x00",		
		AuxOn 		= "0xFF, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00",
		AuxOff 		= "0xFF, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00",
	},
	Stop = 
	{
		TileUp 		= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		TileDown 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		PanLeft 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		PanRight 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		LeftUp 		= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		LeftDown 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		RightUp		= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		RightDown 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",		
		ZoomWide 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		ZoomTele 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		FocusNear 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		FocusFar 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		IrisSmall 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
		IrisLarge 	= "0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00",
	},
}

Protocol.Checksum = function (s)
	local temp = s[2] + s[3] + s[4] + s[5] + s[6];
	s[7] = math.mod(temp, 256);
	return s;
end;

return Protocol;