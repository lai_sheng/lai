-- 这是LION, DH-NVS0104E(S)
local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "LION",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "PTZ",
	
	-- 以ms为单位
	Internal = 200,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 		= {0x01, 0xFF}, 
	-- 监视地址范围
	MonAddrRange		= {0x00, 0xFF},	
	-- 预置点范围
	PresetRange 		= {0x01, 80},
	-- 自动巡航线路范围
	TourRange			= {0x00, 7},
	-- 轨迹线路范围
	PatternRange		= {0x00, 4},
	-- 垂直速度范围
	TileSpeedRange 		= {0x01, 0xFF},
	-- 水平速度范围
	PanSpeedRange 		= {0x01, 0xFF},
	
	-- 辅助范围
	AuxRange 			= {0x01, 0x08},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 		= 2, 
	PresetPos 		= 1, 
	TileSpeedPos 	= 1,
	PanSpeedPos 	= 1,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 		= "0xAA, 0x00, 0x10",
		TileDown 	= "0xAA, 0x00, 0x11",
		PanLeft 	= "0xAA, 0x00, 0x12", 
		PanRight 	= "0xAA, 0x00, 0x13",

		ZoomWide 		= "0xAA, 0x00, 0x19",
		ZoomTele 		= "0xAA, 0x00, 0x18",
		FocusNear		= "0xAA, 0x00, 0x1B",
		FocusFar 		= "0xAA, 0x00, 0x1A",
		IrisSmall 	= "0xAA, 0x00, 0x1D",
		IrisLarge 	= "0xAA, 0x00, 0x1C",
		
		AuxOn 	= "0xAA, 0x00, 0x01",
		AuxOff 	= "0xAA, 0x00, 0x00",
		
		AutoScanOn 		= "0xAA, 0x00, 0x30",
		AutoScanOff		= "0xAA, 0x00, 0x22",
	},
	Stop = 
	{
		TileUp 		= "0xAA, 0x00, 0x22",
		TileDown 	= "0xAA, 0x00, 0x22",
		PanLeft 	= "0xAA, 0x00, 0x22",
		PanRight 	= "0xAA, 0x00, 0x22",
			
		ZoomWide 		= "0xAA, 0x00, 0x22",
		ZoomTele 		= "0xAA, 0x00, 0x22",
		FocusNear 	= "0xAA, 0x00, 0x22",
		FocusFar 		= "0xAA, 0x00, 0x22",
		IrisSmall 	= "0xAA, 0x00, 0x22",
		IrisLarge 	= "0xAA, 0x00, 0x22",
	},
}

Protocol.Checksum = function(s)
	return s;
end;

Protocol.SpeedProcess = function(s,ver,hor)
	return s;
end;

Protocol.AuxProcess = function(s, num)
	if num == 1 then
		if s[3] == 0x01 then
			s[3] = 0x16;
		else
			s[3] = 0x17;
		end;
	elseif num == 2 then
		if s[3] == 0x01 then
			s[3] = 0x26;
		else
			s[3] = 0x27;
		end;
	elseif num == 3 then
		if s[3] == 0x01 then
			s[3] = 0x14;
		else
			s[3] = 0x15;
		end;
	else
		s[3] = 0x00;
	end;
	return s;
end;

return Protocol;