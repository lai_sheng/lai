--[[
本协议功能非常单一, 除了上下左右 焦距 变倍 光圈 两个辅助开关外, 没有别的功能.
需要注意的是辅助开关, 共有2个, 分别为K1和K2. 用户在使用的时候输入0代表对K1操作, 输入1代表对K2操作.
--]]
local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "HHX2000",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "PTZ",
	
	-- 以ms为单位
	Internal = 200,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 		= {0xFF, 0xFF}, 
	-- 监视地址范围
	MonAddrRange		= {0xFF, 0xFF},	
	-- 预置点范围
	PresetRange 		= {0x00, 0xFF},
	-- 自动巡航线路范围
	TourRange		= {0xFF, 0xFF},
	-- 轨迹线路范围
	PatternRange		= {0xFF, 0xFF},
	-- 垂直速度范围
	TileSpeedRange 		= {0xFF, 0xFF},
	-- 水平速度范围
	PanSpeedRange 		= {0xFF, 0xFF},
	
	-- 辅助范围
	AuxRange 		= {0, 1},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 		= 2, 
	PresetPos 		= 6, 
	TileSpeedPos 		= 6,
	PanSpeedPos 		= 5,
	AuxPos 			= 1,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 		= "0x45",
		TileDown 	= "0x44",
		PanLeft 	= "0x43",
		PanRight 	= "0x42",

		ZoomWide 	= "0x4E",
		ZoomTele 	= "0x4D",
		FocusNear	= "0x4B",
		FocusFar 	= "0x4C",
		IrisSmall 	= "0x4A",
		IrisLarge 	= "0x49",
			
		-- 自动扫描，在预先设置的边界中间转动
		AutoPanOn 	= "0x40",
		AutoPanOff	= "0x41",
	
	},
	Stop = 
	{
		TileUp 		= "0x41",
		TileDown 	= "0x41",
		PanLeft 	= "0x41",
		PanRight 	= "0x41",
		LeftUp 		= "0x41",
		LeftDown 	= "0x41",
		RightUp		= "0x41",
		RightDown 	= "0x41",
		
		ZoomWide 	= "0x41",
		ZoomTele 	= "0x41",
		FocusNear 	= "0x41",
		FocusFar 	= "0x41",
		IrisSmall 	= "0x41",
		IrisLarge 	= "0x41",
	},
}

Protocol.Checksum = function (s)
	return s;
end;

Protocol.CamAddrProcess = function(s, addr)
	return s;
end;

Protocol.SpeedProcess = function(s, ver, hor)	
	return s;
end;

return Protocol;