-- 利凌协议
local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "PIH-1016",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "PTZ",
	
	-- 以ms为单位
	Internal = 200,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 		= {0x01, 0xFF}, 
	-- 监视地址范围
	MonAddrRange		= {0x00, 0xFF},	
	-- 预置点范围
	PresetRange 		= {0x00, 0xff},
	-- 自动巡航线路范围
	TourRange			= {0x01, 0xff},
	-- 轨迹线路范围
	PatternRange		= {0x01, 0xff},
	-- 垂直速度范围
	TileSpeedRange 		= {0x01, 0x3F},
	-- 水平速度范围
	PanSpeedRange 		= {0x01, 0x3F},
	
	-- 辅助范围
	AuxRange 			= {0x01, 0x08},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 		= 1, 
	PresetPos 		= 6, 
	TileSpeedPos 	= 6,
	PanSpeedPos 	= 5,
	AuxPos 			= 6,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 		= "0x00, 0x00,",
		TileDown 	= "0x00, 0x01,",
		PanLeft 	= "0x00, 0x02,", 
		PanRight 	= "0x00, 0x03,",
		LeftUp 		= "0x00, 0x15,",
		LeftDown 	= "0x00, 0x23,",
		RightUp		= "0x00, 0x1A,",
		RightDown = "0x00, 0x3A,",

		ZoomWide 	= "0x00, 0x08",
		ZoomTele 	= "0x00, 0x09",
		FocusNear	= "0x00, 0x04",
		FocusFar 	= "0x00, 0x05",
		IrisSmall = "0x00, 0x07",
		IrisLarge = "0x00, 0x06",
			
	},
	Stop = 
	{
		TileUp 		= "0x00, 0xff, ",
		TileDown 	= "0x00, 0xff, ",
		PanLeft 	= "0x00, 0xff, ",
		PanRight 	= "0x00, 0xff, ",
		LeftUp 		= "0x00, 0xff, ",
		LeftDown 	= "0x00, 0xff, ",
		RightUp		= "0x00, 0xff, ",
		RightDown = "0x00, 0xff, ",
		
		ZoomWide 	= "0x00, 0xff, ",
		ZoomTele 	= "0x00, 0xff, ",
		FocusNear = "0x00, 0xff, ",
		FocusFar 	= "0x00, 0xff, ",
		IrisSmall = "0x00, 0xff, ",
		IrisLarge = "0x00, 0xff, ",
	},
}

Protocol.Checksum = function (s)
	return s;
end;


Protocol.CamAddrProcess = function(s, addr)
	s[Protocol.CommandAttr.AddrPos] = 0x80 + addr - 1;
	return s;
end;

Protocol.SpeedProcess = function(s, arg1, arg2)
	return s;
end;

return Protocol;