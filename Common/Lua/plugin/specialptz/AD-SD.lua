-- 这是American Dynamics快球的协议，支持的型号有 SpeedDome Ultra VII,SpeedDome Optima,and later versions
local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "AD-SD",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "PTZ",
	
	-- 以ms为单位
	Internal = 200,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 			= {0x01, 0x63}, 
	-- 监视地址范围
	MonAddrRange			= {0x00, 0xFF},	
	-- 预置点范围
	PresetRange 			= {0x00, 0x5f},
	-- 自动巡航线路范围
	TourRange					= {0x00, 0x07},
	-- 轨迹线路范围
	PatternRange			= {0x01, 0x03},
	-- 垂直速度范围
	TileSpeedRange 		= {0x01, 0x64},
	-- 水平速度范围
	PanSpeedRange 		= {0x01, 0x64},
	
	-- 辅助范围
	AuxRange 					= {0x00, 0x0c},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 			= 1, 
	PresetPos 		= 4, 
	TileSpeedPos 	= 4,
	PanSpeedPos 	= 4,
	AuxPos 				= 3,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 			= "0x00, 0xc0, 0x84, 0x00, 0x00,",
		TileDown 		= "0x00, 0xc0, 0x85, 0x00, 0x00,",
		PanLeft 		= "0x00, 0xc0, 0x81, 0x00, 0x00,", 
		PanRight 		= "0x00, 0xc0, 0x82, 0x00, 0x00,",


		ZoomWide 		= "0x00, 0x8b, 0x00",
		ZoomTele 		= "0x00, 0x8a, 0x00",
		FocusNear		= "0x00, 0x87, 0x00",
		FocusFar 		= "0x00, 0x88, 0x00",
		IrisSmall 	= "0x00, 0x91, 0x00",
		IrisLarge 	= "0x00, 0x90, 0x00",
			
			
		-- 预置点操作（设置，清除，转置)
		SetPreset 	= "0x00, 0xc7, 0x01, 0x00, 0x00",
		ClearPreset	= "0x00, 0xc7, 0x01, 0x00, 0x00",
		GoToPreset 	= "0x00, 0xc7, 0x02, 0x00, 0x00",			
			
			

			
		-- 轨迹巡航, 一般指模式(设置开始，设置结束，运行，停止，清除模式
		SetPatternStart = "0x00, 0xa0, 0x00",
		SetPatternStop 	= "0x00, 0xb8, 0x00",
		StartPattern 		= "0x00, 0xcc, 0x03,0x00",
		StopPattern			= "0x00, 0x93, 0x00",
		ClearPattern 		= "0x00, 0xcc, 0x07,0x00",
		--此处辅助功能主要实现了一些云台控制菜单中不能够实现的功能，如球机翻转，复位,删除寻迹组等。具体参照协议说明。	
		AuxOn 	= "0x00, 0xcc, 0x00, 0x00",
		AuxOff 	= "0x00, 0xcc, 0x00, 0x00",
			
		-- 菜单相关操作
		Menu 			= "0x00, 0xcc, 0x01, 0x00",
		MenuExit 	= "0x00, 0xcc, 0x02, 0x00", 

	},
	Stop = 
	{
		TileUp 			= "0x00, 0x86, 0x00",
		TileDown	 	= "0x00, 0x86, 0x00",
		PanLeft 		= "0x00, 0x83, 0x00",
		PanRight 		= "0x00, 0x83, 0x00",
		
		ZoomWide 		= "0x00, 0x8c, 0x00",
		ZoomTele 		= "0x00, 0x8c, 0x00",
		FocusNear 	= "0x00, 0x89, 0x00",
		FocusFar 		= "0x00, 0x89, 0x00",
		IrisSmall 	= "0x00, 0x92, 0x00",
		IrisLarge 	= "0x00, 0x92, 0x00",
	},
}

Protocol.Checksum = function (s)
	local sum = 0;
	for j=1 ,table.getn(s) - 1 do
		sum = math.mod(sum + s[j],256);
	end;
	s[table.getn(s)] = 0xff - sum +1;
	print("CheckSum is",s[table.getn(s)]);
	return s;
end;

Protocol.CamAddrProcess = function(s, addr)
	local addr = math.mod(addr,100);
		s[1] = addr;
	return s;
end;

Protocol.SpeedProcess = function(s,hor,ver)
	if table.getn(s) == 3 then
		return s;
	end;
	if hor ~= 0x00 then
			s[4] = hor;
			return s;
	end
	
	if ver ~= 0x00 then
			s[4] = ver;
			return s;
	end;
	return s;
end;

Protocol.PatternProcess = function(s, num)
	if table.getn(s) == 3 and s[2] == 0xa0 then
		s[2] = s[2] + num -1 ;
		return s;
	end;
	if table.getn(s) == 4 and s[2] == 0xcc then
		s[3] = s[3] + num -1;
		return s;
	end;
	return s;	
end;


return Protocol;