--
-- Install.lua - _explain_
--
-- Copyright (C) 2006 Dahua Technologies, All Rights Reserved.
--
-- $Id: Install.lua, v 1.0.0.1 2006-04-28 16:46:24 -- Exp $
--

-- Flash 块的大小为64K
local flashSectorSize = 0x10000;   

local Installer = {};
Installer.TotalSize      = 0;    -- 总的要写到Flash中的数据大小
Installer.InProgressSize = 0;	 -- 用来在升级过程中控制进度

local ptzdir = "/mnt/mtd/Config/ptzPlugin/";


-- 通知上层应用程序升级的进度信息
-- params:
--    无
-- return: 
--    无
function Installer:notify()
	self.InProgressSize = self.InProgressSize + flashSectorSize;
	
	local progress = self.InProgressSize / self.TotalSize * 100;
	if(progress > 100) then
		progress = 100;
	end;
	progress = tonumber(string.format("%d", progress));
	
	-- 注意调用方式,不是self::callback,否则会导致回调出错
	self.callback(0x01, progress);
end

-- 升级完成后的处理,如控制系统重启
-- params:
--    无
-- return: 
--    无
function Installer:postInstall()
end

function Installer:InstallPlayer(filename)
	local myfile = self.ZipArchive:open(filename);
	local data;
	
	-- 如果该文件打不开,则不需要对此部分进行升级
	if(not myfile) then  
		return false, "The player isn't exist";
	end

	ide.init();
	-- get kit area
	
	local offset = 0;
	local size = 0;
	-- 升级的播放器暂时放在第一个硬盘的第一分区上
	offset, size = ide.get_kit_area(0, 0);

	if ((offset == 0) and (size == 0)) then
		return false, "The HardDisk isn't exist";
	end
		
	local blocksize = 32 * 1024;
	
	--
	-- TODO:
	-- 读入自动播放器的内容，通过IDE接口写入到硬盘 
	-- 
	
	while (1) do
		data = myfile:read(blocksize);
		if (data) then
			ide.write(0, offset, data);
			local num = string.len(data)/512;
			local tmpnum = math.floor(num);
			if (num ~= tmpnum) then
				num = tmpnum + 1;
			end;
			offset = offset + num;
		else
			break;
		end;
	end; 
	
	return true;
end

function Installer:UpdatePTZ(filename)
	if (filename == "Install.lua") then
		return true;
	end;
	local myfile = self.ZipArchive:open(filename);
	if(not myfile) then  
		return false, "try to open"..filename.."failed";
	end
	
	local ptzfilename = ptzdir .. filename;
	desfile = io.open(ptzfilename, "w+");
	if (not desfile) then
		return false, "create or open local file failed.";
	end;
	while(1) do
		data = myfile:read(flashSectorSize);
		
		if (not data) then
			break;
		end;
		
		desfile:write(data);
		
	end;
	desfile:close();
	
	return true;
end

-- 在这里控制整个升级过程
-- params:
--    无
-- return: 
--    成功返回True,失败返回Flase以及错误原因
function Installer:execute()
	
	-- self.ZipArchive 这个变量由外部程序设置,如果没有设置程序不应该运行到这里
	assert(self.ZipArchive);
	local zfile = self.ZipArchive;
	
	-- 计算要升级的文件大小以及打印升级文件清单
	local TotalSize = 0;
	print("==>Files in archive");
	for file in zfile:files() do
		print(file.filename);
		TotalSize = TotalSize + file.uncompressed_size;
	end
	self.TotalSize  = TotalSize;     -- 总的文件大小
	lfs.mkdir(ptzdir);
	for file in zfile:files() do
		if string.find(file.filename, ".lua") then
			local ret, str = self:UpdatePTZ(file.filename);
			if not ret then
				return ret,str;
			end;
		elseif string.find(file.filename, ".exe") then
			local ret, str = self:InstallPlayer("OutputBind.exe");
			if not ret then
				return ret,str;
			end;
		else
			return false, "the file isn't exsit";
		end;
	end
	
	self:postInstall();
	print("==>Upgrade finished.");
	return true;
end

return Installer;
