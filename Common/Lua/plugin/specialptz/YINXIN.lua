local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "YINXIN",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "PTZ",
	
	-- 以ms为单位
	Internal = 100,
	
	AlarmLen = 4,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 		= {0x00, 0xFF}, 
	-- 监视地址范围
	MonAddrRange		= {0x00, 0xFF},	
	-- 预置点范围
	PresetRange 		= {0x00, 0x3F},
	-- 自动巡航线路范围
	TourRange				= {0x01, 0xFF},
	-- 轨迹线路范围
	PatternRange			= {0x00, 0x07},
	-- 垂直速度范围
	TileSpeedRange 		= {0x01, 0x08},
	-- 水平速度范围
	PanSpeedRange 		= {0x01, 0x08},
	
	-- 辅助范围
	AuxRange 		= {0x01, 0x02},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 			= 1, 
	PresetPos 		        = 3, 
	TileSpeedPos 	                = 3,
	PanSpeedPos 	                = 3,
	AuxPos 				= 3,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 					= "0x00, 0xC0, 0x01",
		TileDown 				= "0x00, 0xC0, 0x02",
		PanLeft 				= "0x00, 0xC0, 0x03", 
		PanRight 				= "0x00, 0xC0, 0x04",	
		LeftUp 					= "0x00, 0xC0, 0x0a,",
		LeftDown 				= "0x00, 0xC0, 0x0b,",
		RightUp					= "0x00, 0xC0, 0x0c,",
		RightDown 			= "0x00, 0xC0, 0x0d,",

		ZoomWide 				= "0x00, 0x80, 0x12",
		ZoomTele 				= "0x00, 0x80, 0x13",
		FocusNear				= "0x00, 0x80, 0x15",
		FocusFar 				= "0x00, 0x80, 0x14",
		IrisSmall    		= "0x00, 0x80, 0x11",
		IrisLarge       = "0x00, 0x80, 0x10",
		
			
			
		-- 预置点操作（设置，清除，转置)
		SetPreset 	= "0x00, 0xA0, 0x00",

		GoToPreset 	= "0x00, 0x97, 0x00",			
		
		AutoScanOn  = "0x00, 0x80, 0x0E",
		AutoScanOff = "0x00, 0x80, 0x0F",
			
		-- 辅助功能扩展
		AuxOn 	       = "0x00, 0x80, 0x1A",
		AuxOff 	       = "0x00, 0x80, 0x1B",
			
		-- 菜单相关操作
		Menu      	= "0x00, 0x80, 0x1A",
		MenuExit  	= "0x00, 0x80, 0x1B",

		
	},
	Stop = 
	{
		TileUp 			= "0x00, 0xC0, 0x00",
		TileDown 		= "0x00, 0xC0, 0x80",
		PanLeft 		= "0x00, 0xC0, 0x00", 
		PanRight 		= "0x00, 0xC0, 0x08",
		LeftUp 			= "0x00, 0xC0, 0x00,",
		LeftDown 		= "0x00, 0xC0, 0x80,",
		RightUp			= "0x00, 0xC0, 0x08,",
		RightDown 	= "0x00, 0xC0, 0x88,",
		
		ZoomWide 		= "0x00, 0x80, 0x1F",
		ZoomTele 		= "0x00, 0x80, 0x1F",
		FocusNear		= "0x00, 0x80, 0x20",
		FocusFar 		= "0x00, 0x80, 0x20",
		IrisSmall 	= "0x00, 0x80, 0x1E",
		IrisLarge 	= "0x00, 0x80, 0x1E",
	},
}

Protocol.Checksum = function (s)
	return s;
end;

Protocol.SpeedProcess = function(s, ver, hor)

local tileSpeedTable = {0x10,0x20,0x30,0x40,0x50,0x60,0x70,0x70,0x90,0xa0,0xb0,0xc0,0xd0,0xe0,0xf0,0xf0};
local panSpeedTable =  {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x07,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x0f};
--print(tileSpeedTable[3]);
if s[3] == 0x01 then
	s[3] = tileSpeedTable[ver];
	return s;
end  

if s[3] == 0x02 then
	s[3] = tileSpeedTable[ver + 8];
	return s;
end;

if s[3] == 0x03 then
	s[3] = panSpeedTable[hor];
	return s;
end;

if s[3] == 0x04 then
	s[3] = panSpeedTable[hor + 8];
	return s;
end;

if s[3]== 0x0a then
	s[3] = bits.bxor(panSpeedTable[hor],tileSpeedTable[ver]);
	return s;
end;

if s[3] == 0x0b then
  s[3] = bits.bxor(panSpeedTable[hor],tileSpeedTable[ver + 8]);
  return s;
end;

if s[3] == 0x0c then
	s[3] = bits.bxor(panSpeedTable[ver + 8],tileSpeedTable[hor]);
	return s;
end;

if s[3] == 0x0d then
  s[3] = bits.bxor(panSpeedTable[hor + 8],tileSpeedTable[ver + 8]);
  return s;
end;

	return s;

end;
--以下处理是为了新老协议以及和网络协议的兼容
Protocol.AuxProcess = function(s, value)
	if value == 0x02 and s[3] == 0x1A then
			s[3] = 0x1C;
			return s;
	elseif value == 0x02 and s[3] == 0x1B then
			s[3] = 0x1D
			return s;
	else
			return s;
	end;
end;
return Protocol;