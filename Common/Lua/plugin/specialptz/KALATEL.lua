--这是KALATEL协议.
--有一条StartTour, 但是没有AddTour DeleteTour ClearTour StopTour指令, 功能不明确, 所以巡航预置点功能屏蔽.
local Protocol = {};

-- 表示数值可以用16或10进制(最小值，最大值)
Protocol.Attr = 
{
	-- 协议的显示名称,不能超过16字符，目前暂不支持中文
	Name = "KALATEL",	
		
	-- 指明是云台协议还是矩阵协议，使用"PTZ", "MATRIX"表示
	Type = "MATRIX",
	
	-- 以ms为单位
	Internal = 200,
				
	-- 没有对应的地址范围，请都设成0xff
	-- 云台地址范围
	CamAddrRange 		= {0, 511}, 
	-- 监视地址范围
	MonAddrRange		= {1, 64},	
	-- 预置点范围
	PresetRange 		= {0x00, 0xff},
	-- 自动巡航线路范围
	TourRange		= {0xFF, 0xFF},
	-- 轨迹线路范围
	PatternRange		= {0xFF, 0xFF},
	-- 垂直速度范围
	TileSpeedRange 		= {1, 31},
	-- 水平速度范围
	PanSpeedRange 		= {1, 31},
	-- 辅助范围
	AuxRange 		= {0x01, 0x08},
}

Protocol.CommandAttr =
{
	-- 协议中需要更改的位置，用LUA下标表示，即下标从１开始,用10进制表示
	AddrPos 		= 4, 
	PresetPos 		= 6, 
	TileSpeedPos 		= 1,
	PanSpeedPos 		= 1,
	AuxPos 			= 6,
}

Protocol.Command = 
{
	-- 写具体协议时只能用16进制或字符表示,没有的话就注释掉
	Start= 
	{
		--写上下左右, 左上，左下，右上，右下
		TileUp 		= "0x00, 0x00, 'U', 0x00, 0x00, 0x00, '~'",
		TileDown 	= "0x00, 0x00, 'D', 0x00, 0x00, 0x00, '~'",
		PanLeft 	= "0x00, 0x00, 'L', 0x00, 0x00, 0x00, '~'",
		PanRight 	= "0x00, 0x00, 'R', 0x00, 0x00, 0x00, '~'",
		LeftUp 		= "0x00, 0x00, 'L', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'U', 0x00, 0x00, 0x00, '~'",
		LeftDown 	= "0x00, 0x00, 'L', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'D', 0x00, 0x00, 0x00, '~'",
		RightUp		= "0x00, 0x00, 'R', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'U', 0x00, 0x00, 0x00, '~'",
		RightDown 	= "0x00, 0x00, 'R', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'D', 0x00, 0x00, 0x00, '~'",

		ZoomWide 	= "'M', 0x00, 'O', 0x00, 0x00, 0x00, '~'",
		ZoomTele 	= "'M', 0x00, 'I', 0x00, 0x00, 0x00, '~'",
		FocusNear	= "'M', 0x00, 'N', 0x00, 0x00, 0x00, '~'",
		FocusFar 	= "'M', 0x00, 'F', 0x00, 0x00, 0x00, '~'",
		IrisSmall 	= "'M', 0x00, 'C', 0x00, 0x00, 0x00, '~'",
		IrisLarge 	= "'M', 0x00, 'P', 0x00, 0x00, 0x00, '~'",
			
		-- 预置点操作（设置，清除，转置)
		SetPreset 	= "0x00, 0x00, 'Q', 0x00, 0x00, 0x00, '~'",
		GoToPreset 	= "0x00, 0x00, 'E', 0x00, 0x00, 0x00, '~'",
			
		-- 自动扫描，在预先设置的边界中间转动
		SetLeftLimit 	= "'6', '2', 'Q', 0x00, 0x00, 0x00, '~'",
		SetRightLimit	= "'6', '3', 'Q', 0x00, 0x00, 0x00, '~'",
		AutoScanOn 	= "'M', 0x00, 'A', 0x00, 0x00, 0x00, '~'",
		AutoScanOff	= "'M', 0x00, '_', 0x00, 0x00, 0x00, '~'",
		
		-- 菜单相关操作
		--[[
		Menu 		= "'0', '3', '#', '0', '0', '1', '~', '4', '3', ')', '0', '0', '1', '~', '6', '2', ')', '0', '0', '1', '~'",
		MenuExit 	= "'4', '7', ')', '0', '0', '1', '~'",
		MenuEnter 	= "'3', '8', ')', '0', '0', '1', '~'",
		MenuEsc 	= "'3', '9', ')', '0', '0', '1', '~'",
		MenuUp 		= "'3', '3', ')', '0', '0', '1', '~'",
		MenuDown 	= "'3', '4', ')', '0', '0', '1', '~'",
		MenuLeft 	= "'3', '6', ')', '0', '0', '1', '~'",
		MenuRight	= "'3', '5', ')', '0', '0', '1', '~'",
		--]]
		
		MatrixSwitch = "'M', '0', '#', '0', '0', '1', '~'",
	},
	Stop = 
	{
		TileUp 		= "0x00, 0x00, 'u', 0x00, 0x00, 0x00, '~'",
		TileDown 	= "0x00, 0x00, 'd', 0x00, 0x00, 0x00, '~'",
		PanLeft 	= "0x00, 0x00, 'l', 0x00, 0x00, 0x00, '~'",
		PanRight 	= "0x00, 0x00, 'r', 0x00, 0x00, 0x00, '~'",
		LeftUp 		= "0x00, 0x00, 'l', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'u', 0x00, 0x00, 0x00, '~'",
		LeftDown 	= "0x00, 0x00, 'l', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'd', 0x00, 0x00, 0x00, '~'",
		RightUp		= "0x00, 0x00, 'r', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'u', 0x00, 0x00, 0x00, '~'",
		RightDown 	= "0x00, 0x00, 'r', 0x00, 0x00, 0x00, '~', 0x00, 0x00, 'd', 0x00, 0x00, 0x00, '~'",
		
		ZoomWide 	= "'M', 0x00, 'o', 0x00, 0x00, 0x00, '~'",
		ZoomTele 	= "'M', 0x00, 'i', 0x00, 0x00, 0x00, '~'",
		FocusNear 	= "'M', 0x00, 'n', 0x00, 0x00, 0x00, '~'",
		FocusFar 	= "'M', 0x00, 'f', 0x00, 0x00, 0x00, '~'",
		IrisSmall 	= "'M', 0x00, 'c', 0x00, 0x00, 0x00, '~'",
		IrisLarge 	= "'M', 0x00, 'p', 0x00, 0x00, 0x00, '~'",
	},
}

Protocol.Checksum = function (s)
	return s;
end;

Protocol.CamAddrProcess = function(s, addr)
	local gg = math.mod(addr, 10) + 0x30;
	addr = math.floor(addr / 10);
	local ss = math.mod(addr, 10) + 0x30;
	addr = math.floor(addr / 10);
	local bb = math.mod(addr, 10) + 0x30;
	s[4] = bb;	
	s[5] = ss;
	s[6] = gg;
	if table.getn(s) > 7 then
		s[11] = bb;
		s[12] = ss;
		s[13] = gg;
	end;

	return s;
end;

Protocol.MonAddrProcess = function(opttable,addr)
	if opttable[1] == string.byte('M') then
		opttable[2] = math.mod(addr, 10) + 0x30;
		addr = math.floor(addr / 10);
		opttable[1] = math.mod(addr, 10) + 0x30;
	end;
	return opttable;
end;

Protocol.SpeedProcess = function(s, ver, hor)
	local ver_gg = math.mod(ver, 10) + 0x30;
	ver = math.floor(ver / 10);
	local ver_ss = math.mod(ver, 10) + 0x30;
	local hor_gg = math.mod(hor, 10) + 0x30;
	hor = math.floor(hor / 10);
	local hor_ss = math.mod(hor, 10) + 0x30;
	if ver ~= 0 and hor == 0 then
		s[2] = ver_gg;
		s[1] = ver_ss;
	end;
	if ver == 0 and hor ~= 0 then
		s[2] = hor_gg;
		s[1] = hor_ss;
	end;
	if ver ~= 0 and hor ~= 0 then
		s[2] = hor_gg;
		s[1] = hor_ss;
		s[9] = ver_gg;
		s[8] = ver_ss;
	end;
	return s;
end;

Protocol.PresetProcess = function(opttable, arg1)
	opttable[1] = 0x30;
	opttable[2] = arg1 + 0x30;
	return opttable;
end;

return Protocol;