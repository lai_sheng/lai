-- 印度尼西亚ATM程序脚本
local COM = {};

local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local unsettledstr = "";

-- channel为0表示所操作的通道从配置文件中获得.
local channel = 0;
local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	if unsettledstr ~= "" then
		return 1;
	end;

	if szStr == "*"  then
		return 1;
	end;
	return false;
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)

	local cardno = "";
	local transaction_type;
	local transaction_time;
	local machineno;
	local serialno;
	local branchno;
	local transaction_amount;
	local cmd_length;
	local restMount ;
	
	local transactionStart;
	local transactionStop;
	local transFlagStart;
	local transFlagStop;
	local iStartDate;
	local iEndDate;
	local iStartTime;
	local iEndTime;
	local date = "";
	local time = "";
	local dateDivisionStart;
	local dateDivisionEnd;
	local dateDivision;
	local timeDivisionStart;
	local timeDivisionEnd;
	local timeDivision;
	local restMountFlag = "SALDO";
	local branchFlag;
	local nextStart;
	local nextStop;	
	local branchStr;
	local cardStr;
	local transMountStr;
	local restMountStr;
	local blankFlag = " ";
	local blankStart;
	local blankStop;
	local midStr;
	
	
	dateDivision = "/";
	timeDivision = ":"
	cardFlag = "URUT";
	transcationAmountFlag = "RP";
	branchFlag = "-";
	nextFlag = "\r\n";
	
	local str = unsettledstr .. szStr;

	-- 判断是否接收到一个完整长度的命令. 
	if string.len(str) >= 0xff then
		print("get whole data");
		print(str);
	else
		unsettledstr = str;
		return;
	end;
	
	print("start parse data");
	-- 脚本执行到这里, 已经接收到了一条完整的命令, 下面开始解析并且执行相应的动作.
	-- 清空暂存数据.
	unsettledstr = "";
		COM.ExtraProcess(Lua2C.startrec, channel, " ") ;
	--print("\n");
	--print("////////////////////get time");
	-- 获取当前的日期和时间
	dateDivisionStart,dateDivisionEnd = string.find(str, dateDivision);
--	print("start get date");
	if dateDivisionStart ~= nil then
		date = string.sub(str, dateDivisionStart - 2, dateDivisionStart + 5);
		--print(date);
	else
	
		return;
	end;
	--print("start get time");
	timeDivisionStart, timeDivisionEnd = string.find(str, timeDivision);
	--print("start");
	if timeDivisionStart ~= nil  then
						time = string.sub(str, timeDivisionStart - 2, timeDivisionStart + 5);
						--print(time);
	else
		return;
	end;

	--将时间格式转变为YYYYMMDDHHMMSS
	local s = date..time;
	local transtime = date.." "..time;
	local s1 = string.sub(s,7,8);
	s1 = "20"..s1;
	s2 = string.sub(s,4,5);
	s1 = s1..s2;
	s2 = string.sub(s,1,2);
	s1 = s1..s2;
	s2 = string.sub(s,9,10);
	s1 = s1..s2;
	s2 = string.sub(s,12,13);
	s1 = s1..s2;
	s2 = string.sub(s,15,16);
	s1 = s1..s2;
	s1 = s1.."30";
	transaction_time = s1;
	--print(transaction_time);
	COM.AppendCard(Lua2C.transaction_time,channel,transtime);		
	
			
	--print("\n");
	--print("////////////////////get bank info");
	--获取银行代码
		local branchFlagStart;
		local branchFlagStop;
		branchFlagStart,branchFlagStop = string.find(str, branchFlag);
	--	print("start get banch");
		if branchFlagStart ~= nil then
					local branchCount = branchFlagStop + 1;
					--print("121");
					--print(branchCount);
					branchStr = string.sub(str,branchCount,branchCount + 30);
				--	print(branchStr);		
					nextStart,nextStop = string.find(branchStr,nextFlag);
					--print("ok");
					--print(nextStart,nextStop);
					if nextStart ~= nil then
				--	print("ok");
							branchno = string.sub(branchStr, 1,nextStart -1);	
							print(branchno);
					else
					 --  print("No branchno");
					   return;
					end							
		else
					--print("No branch no");
					--return;
	end;
	COM.AppendCard(Lua2C.branch_no,channel,branchno);
	
	print("\n");
	print("////////////////////start get cardno");
	--获取卡号
	cardnoFlagStart,cardFlagStop = string.find(str, cardFlag);
	--print("ok");
	--print(cardnoFlagStart,cardFlagStop);
	if cardnoFlagStart ~= nil then
		cardStr = string.sub(str,cardFlagStop + 1,cardFlagStop + 30);
		--print(cardStr);
		nextStart,nextStop = string.find(cardStr,nextFlag);
		if nextStart ~= nil then
				--	print("ok");
					midStr = string.sub(cardStr, 1,nextStart -1);	
					--print(midStr);
				-- 清除卡号中的空格
					blankStart,blankStop = string.find(midStr,blankFlag);
					while blankStart ~= nill do
								cardno =string.sub(midStr, blankStop + 1, nextStart - 1);	
								blankStart,blankStop = string.find(cardno,blankFlag);
								midStr = cardno;
					end;				 
					--print(cardno);
				
  	else
					return;
		end								
	end;
	COM.AppendCard(Lua2C.cardno, channel,  cardno, COM.CPlusPlus);

	print("\n");		
	print("////////////////////get transcation mount");
	--获取交易金额

	transFlagStart,transFlagStop = string.find(str,transcationAmountFlag)	
	--print(transFlagStart,transFlagStop);
	if 	transFlagStart ~= nil then

			transMountStr = string.sub(str,transFlagStop + 1,transFlagStop + 200)	--得到剩余的所有字符串
			nextStart,nextStop = string.find(transMountStr,nextFlag);
			if nextStart ~= nil then				
					midStr = string.sub(transMountStr,1,nextStart -1); 
									
					--去掉交易金额字符串中的空格 
					blankStart,blankStop = string.find(midStr,blankFlag);
					while blankStart ~= nill do
								transaction_amount =string.sub(midStr, blankStop + 1, nextStart - 4);	
								blankStart,blankStop = string.find(transaction_amount,blankFlag);
								midStr = transaction_amount;
					end;						 
					
				
					--去掉交易金额中的数字分隔符号
					local moneyDiv = ",";
					local moneyDivStart;
					local moneyDivStop;
					local s1 ;
					local s2 = "";
					moneyDivStart,moneyDivStop = string.find(transaction_amount,moneyDiv);
					while moneyDivStart ~= nil do
								s1 = string.sub(transaction_amount,1,moneyDivStart -1);
								s2 = s2..s1;
								local len = string.len(s2);
								transaction_amount = string.sub(transaction_amount,moneyDivStart +1,moneyDivStart + 20);
								if string.len(transaction_amount) <= 3 then
										s2 = s2..transaction_amount;
										transaction_amount = s2;
										moneyDivStart = nil;
								end 
								moneyDivStart,moneyDivStop = string.find(transaction_amount,moneyDiv);							
					end;
				 	--print(transaction_amount);
	
					
			else
					print("No Transcation amount");   --此处不返回，因为某些操作可能不涉及到交易金额
			end;

	end;
	COM.AppendCard(Lua2C.transaction_amount,channel,transaction_amount,COM.CPlusPlus);


	
	--获取剩余金额
	transFlagStart,transFlagStop = string.find(str,restMountFlag);
	--print(transFlagStart);
	if transFlagStart ~= nil then	
		midStr = string.sub(str,transFlagStart + 1,transFlagStart + 50);
		--print("111",midStr);
		transFlagStart,transFlagStop = string.find(midStr,transcationAmountFlag);
		--print(transFlagStart);
		if transFlagStart ~= nil then
			midStr = string.sub(midStr,transFlagStop + 1,transFlagStop + 30);
			nextStart,nextStop = string.find(midStr,nextFlag);
			--print("22222",midStr);
			--print(nextStart);
			--print(transFlagStop);
			if nextStart ~= nil then
				--print("33333",midStr);
				midStr = string.sub(midStr,1,nextStart -4);
				--print(midStr);
				restMount = midStr;

		--去掉剩余金额中的空格
				blankStart,blankStop = string.find(midStr,blankFlag);
				while blankStart ~= nill do
						restMount =string.sub(midStr, blankStop + 1, nextStart - 4);	
						blankStart,blankStop = string.find(restMount,blankFlag);
						midStr = restMount;
				end;	
	
		
		--去掉交易金额中的数字分隔符号
					local moneyDiv = ",";
					local moneyDivStart;
					local moneyDivStop;
					local s1 ;
					local s2 = "";
					moneyDivStart,moneyDivStop = string.find(restMount,moneyDiv);
					while moneyDivStart ~= nil do
								s1 = string.sub(restMount,1,moneyDivStart -1);
								s2 = s2..s1;
								restMount = string.sub(restMount,moneyDivStart +1,moneyDivStart + 20);
								if string.len(restMount) <= 3 then
										s2 = s2..restMount;
										restMount = s2;
										moneyDivStart = nil;
								end 
								moneyDivStart,moneyDivStop = string.find(restMount,moneyDiv);							
					end;
							 
					--print(restMount);
			end;
		end;
	end;	
	
--开启录象叠加信息

COM.AppendCard(Lua2C.rest_amount,channel,restMount,COM.CPlusPlus);		

--结束

		
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	CPlusPlus 	= CPlusPlus,
	Name		= "INDONESIA_ATM"
}
return COM;