-- 这是德赛的解析脚本
local COM = {};
local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;
local unsettledstr = "";
local cardnumber = "";

-- 这是从DVR反馈给ATMC的设备状态报告, 
local report_head = "\131".."10DVR";
local function ClearCardNo()
	cardnumber = "";
end;

local channel = 0;
local function FormatTime(s)
	-- 把时间转化为C的格式，用ASCII码表示，YYYYMMDDHHMMSS+差额，差额以S为单位, 没有的话用0表示
	local str = s .. "0";
	return str;
end;

-- DVR向ATMC发送设备状态报告
local function SendStateReport()
	local statereport = report_head;

	-- 获取DVR状态信息
	local ret;
	local AlarmState;
	local StateCh = {};
	StateCh[1],StateCh[2],StateCh[3],AlarmState = COM.ExtraProcess(Lua2C.require_state, channel, " ", COM.CPlusPlus);
	
	if StateCh[1] == nil or StateCh[2] == nil or StateCh[3] == nil or AlarmState == nil then
		print("Get State Info Failed");
		return false;
	end;
	print("DVR StateInfo:");
	print(AlarmState,StateCh[1],StateCh[2],StateCh[3]);
	--根据各种状态信息转换为ATM所需要的信息
	--转换报警信息
	if AlarmState == C2Lua.no_alarm then
		AlarmState = 0;
	elseif AlarmState == C2Lua.disk_error_alarm then
		AlarmState = 4;
	elseif AlarmState == C2Lua.concuss_alarm then
		AlarmState = 9;
	end;
	--转换摄像机状态信息
	for i = 1 ,3 do 
		if StateCh[i] == C2Lua.camera_state_normal then
			StateCh[i] = 0;
		elseif StateCh[i] == C2Lua.camera_state_blind then
			StateCh[i] = 1;
		elseif StateCh[i] == C2Lua.camera_state_recording then
			StateCh[i] = 2;
		elseif StateCh[i] == C2Lua.cameral_state_videoloss then
			StateCh[i] = 9;
		end;
	end;

	
	--填写报警状态和摄像机状态

	statereport = statereport .. AlarmState .. StateCh[1] .. StateCh[2] .. StateCh[3];
	
	-- 因为三路机器, 所以第四路置1(黑屏)
	statereport = statereport .. "1";
	
	-- 计算校验码
	local check = 0;
	local tmpstr;
	for i = 2, string.len(statereport) do
		check = bits.bxor(check, string.byte(string.sub(statereport, i, i)));
	end; 
	tmpstr = string.format("%x", math.mod(check, 256));
	if string.len(tmpstr) == 1 then
		tmpstr = "0" .. tmpstr;
	end;
	statereport = statereport .. tmpstr;
	
	-- 发送设备状态报告
	print("send state report : ", statereport);
	print("state report length : ", string.len(statereport));
	COM.ComWrite(statereport);
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	print("Parse Head");
	if unsettledstr ~= "" then
		return 1;
	end;
	
	local str  = string.byte(string.sub(szStr, 1, 1));
	if (str == 147) then
		return 1;
	end;
	
	return false;
end;
--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local Cardlen;
	local DataLen;
	
	local cardno;
	local transaction_type;
	local transaction_time;
	
	local channel = 0;
	
	local str = unsettledstr .. szStr;
	
	-- 如果读到的命令长度不足3位(无法解析命令长度), 保存后返回.
	if string.len(str) < 3 then
		unsettledstr = str;
		return;
	end;
	
	-- 开始标志后的两个字节表示命令长度, 如果它们不代表数字, 则表示出错, 丢弃后返回.
	local cmd_length = tonumber(string.sub(str, 2, 3));
	if (type(cmd_length) ~= "number") then
		unsettledstr = "";
		return;
	end;
	
	-- 如果命令的长度和命令中描述的长度不符合, 则命令还没有接收完全, 保存后返回.
	-- 由于长度不包括其本身以及开始标志, 所以需要减去3.
	if (string.len(str) - 3 ~= cmd_length) then
		unsettledstr = str;
		return;
	end;
	
	-- 计算校验码(校验码不包括开始标志)
	local checksum = 0;
	for i = 2, string.len(str) - 2 do
		checksum = bits.bxor(checksum, string.byte(string.sub(str, i, i)));
	end; 
	local check_str = string.format("%x", math.mod(checksum, 256));
	if string.len(check_str) == 1 then
		check_str = "0" .. check_str;
	end;
	
	-- add new
	print("check sum : ", check_str);
	local check_1 = string.sub(check_str, 1, 1);
	local check_2 = string.sub(check_str, 2, 2);
	local checkd_1;
	local checkd_2;

	if check_1 == "a" then
		checkd_1 = "A";
	elseif check_1 == "b" then
		checkd_1 = "B";
	elseif check_1 == "c" then
		checkd_1 = "C";
	elseif check_1 == "d" then
		checkd_1 = "D";
	elseif check_1 == "e" then
		checkd_1 = "E";
	elseif check_1 == "f" then
		checkd_1 = "F";
	else
		checkd_1 = check_1;
	end;
	if check_2 == "a" then
		checkd_2 = "A";
	elseif check_2 == "b" then
		checkd_2 = "B";
	elseif check_2 == "c" then
		checkd_2 = "C";
	elseif check_2 == "d" then
		checkd_2 = "D";
	elseif check_2 == "e" then
		checkd_2 = "E";
	elseif check_2 == "f" then
		checkd_2 = "F";
	else
		checkd_2 = check_2;
	end;
	
	local checkd = checkd_1 .. checkd_2;
	print("checkd :", checkd);
	-- add new end
	
	-- 如果校验码不正确, 说明命令出错, 不可用, 丢弃后返回.
	if check_str ~= string.sub(str, -2, -1) and checkd ~= string.sub(str, -2, -1) then
		print("check error");
		unsettledstr = "";
		return;
	else
		print("check ok");
	end;
	
	-- 脚本执行到这里, str中是一条完整的命令了.
	unsettledstr = "";
	print("ok" .. str);
	
	-- 根据命令中所含的时间更新本地时间.
	transaction_time = string.sub(str, 4, 17);
	transaction_time = FormatTime(transaction_time);
	print(Lua2C);
	COM.ExtraProcess(Lua2C.emendtime, channel, transaction_time,COM.CPlusPlus);
	
	-- 根据报文类型处理报文内容, 并且发送状态报告.
	local message_type;
	message_type = string.sub(str, 18, 19);

	-- ATM插卡通知 : 开启录像 卡号叠加 返还设备状态报告
	if message_type == "CI" then
		COM.ExtraProcess(Lua2C.startrec, channel, " ",COM.CPlusPlus)
		
		cardno = string.sub(str, 20, -3);
		print("cardno is");
		print(cardno);
		COM.AppendCard(Lua2C.cardno, channel, cardno,COM.CPlusPlus);
		
		SendStateReport();
		
	-- ATM交易通知 : 叠加交易类型 叠加交易时间 返还设备状态报告
	elseif message_type == "TT" then
		transaction_type = string.sub(str, 20, 22);
		COM.AppendCard(Lua2C.transaction_type, channel, transaction_type,COM.CPlusPlus);
		
		alarm, ch1, ch2,ch3 = COM.AppendCard(Lua2C.transaction_time, channel, transaction_time);
		
		COM.WRITE();
		
		SendStateReport();
	
	-- ATM出钞通知/应答 : 返还设备状态报告
	elseif message_type == "CB" then
		SendStateReport();
	
	-- ATM出钞成功或吞钞通知 : 返还设备状态报告
	elseif message_type == "CE" then
		SendStateReport();
	
	-- ATM退卡通知 : 停止录像 返还设备状态报告
	elseif message_type == "CO" then
		COM.ExtraProcess(Lua2C.stoprec, channel, "10",COM.CPlusPlus);
		SendStateReport();
	
	-- ATM打开保险门通知 : 开始录像 需要解决通道问题. 返还设备状态报告
	elseif message_type == "OD" then
		COM.ExtraProcess(Lua2C.startnormalrec, Lua2C.open_insurance_door, " ",COM.CPlusPlus);
		SendStateReport();
	
	-- ATM关闭保险门通知 : 关闭录像 需要解决通道问题. 返还设备状态报告
	elseif message_type == "CD" then
		COM.ExtraProcess(Lua2C.stopnormalrec, Lua2C.close_insurance_door, " ",COM.CPlusPlus);
		SendStateReport();
	
	-- 监控设备状态申请 : 返还设备状态报告
	elseif message_type == "ST" then
		SendStateReport();
		
	-- 报警状态复位申请 : 关闭报警 返还设备状态报告
	elseif message_type == "RS" then
		-- COM.ExtraProcess(Lua2C.resetalarm, channel, "");
		SendStateReport();
	end;
	
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60,	
	ParseHead	= ParseHead,
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,
	CPlusPlus = CPlusPlus,
	Name		= "HZDS_ATM"
}

COM.Name = "HZDS";
return COM;