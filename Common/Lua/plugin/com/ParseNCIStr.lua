-- 华为平ATM脚本
local COM = {};
local RetInfo = 
{
	cardno = 1, serialno = 2, transaction_type = 3, 
	transaction_amount = 4, transaction_time = 5, machineno = 6,
	emendtime = 0xa0, clear = 0xfd, startrec = 0xfe, stoprec=0xff
};

local unsettledstr = "";

-- channel为0表示所操作的通道从配置文件中获得.
local channel = 0;
local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	if szStr == "+" or szStr == "-" then
		return 1;
	end;
	
	return false;
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local mti;
	local cardno;
	local transaction_type;
	local transaction_time;
	local machineno;
	local serialno;
	local transaction_amount;
	local cmd_length;
	
	local str = szStr;
	
	-- 判断是否接收到一个完整长度的命令.
	if string.len(str) < 20  then
		return;
	end;
	
	-- 脚本执行到这里, 已经接收到了一条完整的命令, 下面开始解析并且执行相应的动作.
	-- 清空暂存数据.

	-- 如果是插卡命令, 开始卡号录像. 其他命令不用开始卡号录像.
	if string.sub(str, 1, 1) == "+" then
		COM.ExtraProcess(RetInfo.startrec, channel, " ");
		
		-- 获得卡号	
		cardno 	= string.sub(str, 2, 20);
		print("card number : ", cardno);
		COM.AppendCard(RetInfo.cardno, channel, cardno, COM.CPlusPlus);	
		
		return;
	end;
	
	-- 如果是吞卡或者退卡命令, 则停止卡号录像, 返回.
	if string.sub(str, 1, 1) == "-" then
		print("outcard");
		COM.ExtraProcess(RetInfo.stoprec, channel, "10", COM.CPlusPlus);
		-- COM.ExtraProcess(RetInfo.clear, channel, " ", COM.CPlusPlus);
		return;
	end;
		
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 20,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 60,
	CPlusPlus	= CPlusPlus,
	Name 		= "NCI_ATM"
}
return COM;