--贵州邮政ATM串口解析协议
local COM = {};

local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local unsettledstr = "";

-- channel为0表示所操作的通道从配置文件中获得.
local channel = 0;
local cardnumber= "";
local errorcode = "00";
--收到串口命令后，写入串口的数据(主要是：返回对应的命令码和错误码)
local strToWrite = "\2";
local cardnumber = "";

--此函数暂无意义，仅供C中调用
local function ClearCardNo()
	--print("call cleat cardno in lua");
	cardnumber = "";
end;
--[[
  串口数据错误码
  初始化为正常指令代码，在此脚本中只能对命令正确与否进行判断，不能获得其它错误类型，
  其他错误类型在C++中处理
 --]]
local errorCode = "00"; 

--清除账号之中的空格
local function ClearBlank(str)
	local retStr;
	if nil ~= str then
		local blankStart,blankStop = string.find(str," ")
		if nil ~= blankStart then
			retStr = string.sub(str,1,blankStart - 1);
			return retStr;
		else
			return str;
		end; 
	else
		--print("No Str is Need to Clear Blank");
	end
end;

--获取卡号字符串
local function GetCardNo(str)
	local cardno = string.sub(str,5,24);
	local cardString;
	if nil ~= cardno then
		cardString = ClearBlank(cardno);
	else
		--print("Get No CardNo");
	end
	return cardString;
end

--从交易信息中获取交易类型
local function GetTransType(str)
	local transType = string.sub(str,25,27);
	if nil ~= transType then
		return transType;
	else
		--print("No transType");
	end;
end;

--从交易信息中获取交易金额
local function GetTransAmount(str)
	local transAmount = string.sub(str,28,33);
	if nil ~= transAmount then
		return transAmount;
	else
		--print("No transAmount");
	end;
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	--print("parse head");
	if unsettledstr ~= "" then
		return true;
	end;
	
--如果是开始符号	 
	if string.byte(szStr) == 2  then
		strToWrite = szStr;
		return true;
	end;
	return false;
end;

--此函数获取录像通道信息
local function GetRecordChanels(str)	
	if nil == str then
		--print("Get iChannels wrong");
		return ;
	end;
	--先获得录像通道，通道信息在第四个字节上面
	local iChannels = string.sub(str,4,4);
	local chan;
	--通道信息转化为掩码信息
	if nil ~= iChannels then
		if "1" == iChannels then
			chan = 0x01;
		elseif "2" == iChannels then
			chan = 0x02;
		elseif "3" == iChannels  then
			chan = 0x04;
		elseif "4" == iChannels then
			chan= 0x05;
		elseif "5" == iChannels then
			chan = 0x0f;
		else
			--print(iChannels);
			--print("No Record Channels");
		end;
	end;
	--print("Lua record Channels",chan);
	return chan;	
end;

--叠加交易信息
local function AppendCustomMessage(str,channel)
	--卡号信息
	local cardString = GetCardNo(str);
	--print("cardString ",cardString);
	COM.AppendCard(Lua2C.cardno,channel,cardString,COM.CPlusPlus);	
--[[	
	--交易类型
	local transType = GetTransType(str);
	--print("transType ",transType);
	COM.AppendCard(Lua2C.transaction_type,channel,transType,COM.CPlusPlus);
	
	--交易金额
	local transAmount = GetTransAmount(str);
	--print("transAmount ",transAmount);
	COM.AppendCard(Lua2C.transaction_amount,channel,transAmount,COM.CPlusPlus);
--]]	
	--发送结束
	COM.AppendCard(Lua2C.frameend,channel," ",COM.CPlusPlus);

end;


--录像处理
--[[
		根据获得的数据进行录像控制：从第二个字节开始，长度为两个字节，根据实际情况，只需要取命令的第二个字节
		01表示启动录象
		02表示停止录象
		03表示叠加信息
		04表示控制信息
--]]
local function RecordProcess(str,cmdRecord,channel)
	--print("Record Process");
	if nil == cmdRecord then
		--print("Record Process wrong");
		return;
	end;
	--print("cmd",cmdRecord);
	if "1" == cmdRecord then 
		--print("Lua Start Record");
		--print(channel);
		COM.ExtraProcess(Lua2C.startrec,channel," ",COM.CPlusPlus);
		--print("Lua Start record finished");
	elseif "2" == cmdRecord then
		--print("Lua Stop Record");
		COM.ExtraProcess(Lua2C.stoprec,channel,"10",COM.CPlusPlus);
	elseif "3" == cmdRecord then --表示有叠加信息，需要进行叠加处理
		--print("Append Message");
		AppendCustomMessage(str,channel); 
	elseif "4" == cmdRecord then --表示控制信息
		--此处暂无处理
		--print("Control Message");
	else
		errorCode = "01";
	end;
	return errorCode;
end;

local function SendStateReport()
	local StateCh = {};
	local HddState;
	local RecdState;
	StateCh[1],StateCh[2],StateCh[3],HddState,RecdState = COM.ExtraProcess(Lua2C.require_state, channel, " ",COM.CPlusPlus);
	--print("SendStateReport:");
	--print(StateCh[1]);--print(StateCh[2]);--print(StateCh[3]);--print(HddState);--print(RecdState);
	if StateCh[1] == nil or StateCh[2] == nil or StateCh[3] == nil or HddState == nil or RecdState == nil then
		--print("Get DVR State Failed");
		return;
	end
	
	--此处以下顺序不能随便更改，主要是涉及到报警的优先级顺序	
	if HddState == disk_error_alarm then
		errorcode = "03";
	end;
	
	if StateCh[1] == C2Lua.cameral_state_videoloss then
		errorcode = "F1";
	elseif StateCh[2] == C2Lua.cameral_state_videoloss then
		errorcode = "F2";
	elseif StateCh[3] == C2Lua.cameral_state_videoloss then
		errorcode = "F3";
	end;

	if HddState == C2Lua.disk_full_alarm then 
		errorcode = "FF";
	end;
	

	--连接错误码
	strToWrite = strToWrite .. errorcode;
	
	--处理摄像机状态码
	--print("5555555");
	local recState;
	if bits.band(RecdState,0x01) == 1  then
		recState = "1";
	else
		recState = "0";
	end;

	if bits.band(RecdState,0x02) then
		recState = recState .. "1"
	else
		recState = recState .. "0";
	end;
	
	if bits.band(RecdState,0x04) then
		recState = recState .. "1";
	else
		recState = recState .. "0";
	end;

	--连接录像状态
	strToWrite = strToWrite .. recState;
	
	--连接校验码和结束码
	strToWrite = strToWrite .. "00".."\3";
	
	--print("Data Write to Com is:");
	--print(strToWrite);
	
	COM.ComWrite(strToWrite);	
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]

local function ParseData(szStr)		
	local str = unsettledstr .. szStr;

	-- 判断是否接收到一个完整长度的命令. 
	
	if string.byte(szStr) == 3 then 
		--print("get whole data");
		--print(str);
	else
		unsettledstr = str;
		return;
	end;
	
	-- 脚本执行到这里, 已经接收到了一条完整的命令, 下面开始解析并且执行相应的动作.
	--print("start parse data");

	-- 清空暂存数据.
	unsettledstr = "";
	
	--获取返回的命令字符串，此处的长度为2
	strToWrite = strToWrite..string.sub(str,2,3);
	
	--获取录像通道
	local channel = GetRecordChanels(str);	
	--print("In main Function channel is ",channel);
	
	--获取控制命令
	local cmd = string.sub(str,3,3);
	--print("cmd is",cmd);
	
	--根据命令处理
	local errCode = RecordProcess(str,cmd,channel);
	--print("errCode is ",errCode);

	--写回传数据
	SendStateReport();
end;

--此处提供的串口协议的属性供C++中调用
COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60 * 15,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,	
	CPlusPlus = CPlusPlus
}

COM.Name = "GZYZ_ATM";
return COM;