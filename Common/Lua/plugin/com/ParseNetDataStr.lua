--解析网络抓包的数据的脚本
local COM = {};

local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;


local unsettledstr = "";

-- channel为0表示所操作的通道从配置文件中获得.
local RecChannelMsk = 0;
local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)	
	print(szStr);
	if string.byte(szStr) == 0x02  then
		return 1;
	end;
	
	if  unsettledstr ~= "" then
		return 1;
	end;
	return false;
end;

local channel = 0xffff0000;
--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)	
	print(szStr);
	unsettledstr = unsettledstr .. szStr;
	--判断是否收到结束标志
	if string.byte(szStr) ~= 0x03 then
		return;
	end;
	
	--开始验证数据的正确性与否
	--先获取抓包组序号
	local snifferno = string.sub(unsettledstr, 2, 2);
	if snifferno == nil or string.byte(snifferno) > 0x04 then
		print("snifferno is error");
		return;
	end;
	
	channel = channel + string.byte(snifferno);
	--交易类型
	local transtype = string.sub(unsettledstr, 3, 3);
	if transtype == nil then
		print("Cannot get transtype");
		return;
	end;
	
	if string.byte(transtype) > 0xAC or string.byte(transtype) < 0xA0 then
		print("transtype error");
		return;
	end;
	
	local length = string.sub(unsettledstr, 4, 4);
	if length == nil then
		print("Cannot get data length");
		return;
	end;

	if string.byte(length) ~= string.len(unsettledstr) - 5 then
		print("length is wrong");
		return;
	end;
	
	--以下进行数据的处理工作,主要是根据数据类型处理
	--首先提取数据部分
	local data = string.sub(unsettledstr, 5, string.len(unsettledstr) - 1);
	--机器号
	print("transtype:",string.byte(transtype));
	if string.byte(transtype) == 0xA0 then
		COM.AppendCard(Lua2C.machineno, channel, data, COM.CPlusPlus);
	end;
	
	--营业点号
	if string.byte(transtype) == 0xA1 then
		COM.AppendCard(Lua2C.branchno, channel, data, COM.CPlusPlus);
	end;
	
	--账号或者卡号
	if string.byte(transtype) == 0xA2 then
		print("Accouint is coming");
		print("Lua2C.startrec:",Lua2C.startrec);
		print("Channel:",channel);
		print("Account:",data);
		COM.ExtraProcess(Lua2C.startrec, channel, " ", COM.CPlusPlus);
		COM.AppendCard(Lua2C.cardno, channel, data, COM.CPlusPlus);
	end;
	
	--交易序列号
	if string.byte(transtype) == 0xA4 then
		COM.AppendCard(Lua2C.serialno, channel, data, COM.CPlusPlus);
	end;
	
	--交易日期或者交易时间
	if string.byte(transtype) == 0xA5 or string.byte(transtype) == 0xA6 then
		COM.AppendCard(Lua2C.transaction_time, channel, data, COM.CPlusPlus);		
 	end;
 	
 	--交易类型
 	if string.byte(transtype) == 0xA7 then
		COM.AppendCard(Lua2C.transaction_type, channel, data, COM.CPlusPlus);
 	end;	
 	
 	--交易金额
 	if string.byte(transtype) == 0xA8 then
		COM.AppendCard(Lua2C.transaction_amount, channel, data, COM.CPlusPlus);
 	end;
	
	--交易错误码
	if string.byte(transtype) == 0xAC then
		COM.AppendCard(Lua2C.errorcode, channel, data, COM.CPlusPlus);
	end;
	
	unsettledstr = "";
	return;
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 60,
	Name 		= "ParseNetData",
	CPlusPlus	= CPlusPlus,
	Name		= "NETDATA_ATM",
}
return COM;