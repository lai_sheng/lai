-- 三泰ATM脚本
local COM = {};

local RetInfo = 
{
	cardno = 1, serialno = 2, transaction_type = 3, 
	transaction_amount = 4, transaction_time = 5, machineno = 6,
	emendtime = 0xa0, clear = 0xfd, startrec = 0xfe, stoprec=0xff
};

local unsettledstr = "";

local DataStructLen = 
{
	{mti = 4}, 
	{cardno = 21}, 
	{transaction_type = 3}, 
	{transaction_time = 10}, 
	{machineno = 15}, 
	{serialno = 6}, 
	{transaction_amount = 12}, 
	{reserved = 20},
};

local function FormatTime(s)
	print("start format time");
	-- 把时间转化为C的格式，用ASCII码表示，YYYYMMDDHHMMSS+差额，差额以S为单位, 没有年的话用0表示
	local str = "0000" .. s .. "0";
	return str;
end;

-- channel为0表示所操作的通道从配置文件中获得.
local channel = 0xff;
local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	if unsettledstr ~= "" then
		return 1;
	end;

	if szStr == "9" then
		return 1;
	end;
	
	return false;
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local mti;
	local cardno;
	local transaction_type;
	local transaction_time;
	local machineno;
	local serialno;
	local transaction_amount;
	local cmd_length;
	
	local str = unsettledstr .. szStr;

	-- 判断是否接收到一个完整长度的命令. 
	if string.len(str) >= 6 then
		if string.sub(str, 5, 6) == "00" and string.len(str) < 72 then
			unsettledstr = str;
			return;
		elseif string.sub(str, 5, 6) == "19" and string.len(str) < 91 then
			unsettledstr = str;
			return;
		end;
	else
		unsettledstr = str;
		return;
	end;
	
	-- 脚本执行到这里, 已经接收到了一条完整的命令, 下面开始解析并且执行相应的动作.
	-- 清空暂存数据.
	unsettledstr = "";
	
	mti = string.sub(str, 1, 4);
	print("mti", mti);
	
	if mti == "9510" then
	
		-- 如果是吞卡或者退卡命令, 则停止卡号录像, 返回.
		if string.sub(str, 5, 6) == "00" then
			print("outcard");
			COM.ExtraProcess(RetInfo.stoprec, channel, "10", COM.CPlusPlus);
			-- COM.ExtraProcess(RetInfo.clear, channel, " ", COM.CPlusPlus);
			return;
		end;
	
		-- 获得交易类型.	
		transaction_type = string.sub(str, 26, 28);
		print("transaction_type : ", transaction_type);
		
		-- 如果是插卡命令, 开始卡号录像. 其他命令不用开始卡号录像.
		if transaction_type == "INS" then
			COM.ExtraProcess(RetInfo.startrec, channel, " ", COM.CPlusPlus);
			
			-- 获得卡号	
			cardno 	= string.sub(str, 7, 25);
			print("card number : ", cardno);
			COM.AppendCard(RetInfo.cardno, channel, cardno, COM.CPlusPlus);	
			
			return;
		end;
		
		
		-- 获得执行时间
		transaction_time = string.sub(str, 29, 38);
		transaction_time = FormatTime(transaction_time);
		print("trans time : ", transaction_time);
		
		COM.AppendCard(RetInfo.transaction_type, channel, transaction_type, COM.CPlusPlus);	
		
		COM.AppendCard(RetInfo.transaction_time, channel, transaction_time, COM.CPlusPlus);		
		
		machineno = string.sub(str, 39, 53);
		COM.AppendCard(RetInfo.machineno, channel, machineno, COM.CPlusPlus);
		
		serialno = string.sub(str, 54, 59);
		COM.AppendCard(RetInfo.serialno, channel, serialno, COM.CPlusPlus);
		
		transaction_amount = string.sub(str, 60, 71);
		COM.AppendCard(RetInfo.transaction_amount, channel, transaction_amount, COM.CPlusPlus);
		

	elseif mti == "9520" then
		print("now 9520");
		-- 只校准时间
		transaction_time = string.sub(str, 29, 38);
		transaction_time = FormatTime(transaction_time);
		print("transaction_time : ", transaction_time);
		COM.ExtraProcess(RetInfo.emendtime, channel, transaction_time, COM.CPlusPlus);
	end;
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 60,
	Name		= "SANTAI_ATM",
	CPlusPlus	= CPlusPlus
}

return COM;