-- 该配置文件是WTN使用的脚本
-- 根据序列号开始录像，收到卡号后停止
local COM = {};
local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local DataLength = 1;
local unsettledstr = "";

local channel = 0xff;
local cardnumber = ""; -- 这里用做开关录像标志，不代表真实的卡号信息

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	-- 这里不判断，直接放进来，在下面判断
	return 1;
end;

local function ClearCardNo()
	cardnumber = "";
end;

local function locateserial(str)
	local retpos;
	local i = 0;
	local times = 5;
	while true do
		i = string.find(str, "\28", i + 1);
		if i == nil then
			break;
		end;

		times = times - 1;
		if times == 0 then
			if string.len(str) >= i + 4 then
				retpos = i;
				serialno = string.sub(str, i + 1, i + 4);
				if cardnumber == "" then
					if COM.ExtraProcess(Lua2C.startrec, channel, " ", COM.CPlusPlus) then
						cardnumber = "OK"; -- 随便写写
					end;
				end;
				COM.AppendCard(Lua2C.serialno, channel, serialno, COM.CPlusPlus);
				break;
			else
				times = times + 1;
			end;
		end;
	end;

	return retpos;	
end;

local function locatecard(str)
	local cardnohead = "CARD\32#\32";
	local retpos;
	
	for w in string.gfind(str, cardnohead .. "%d+") do
		for v in string.gfind(w, "%d+") do
			if string.len(v) >= 19 then
				local pos1 = string.find(str, w);
				local pos2 = string.find(w, v);
				retpos = pos1 + pos2;
				cardno = string.sub(v, 1, -1);
				COM.AppendCard(Lua2C.cardno, channel, cardno, COM.CPlusPlus);
				cardnumber = "";
				-- print("card no is ".. cardno);
			end;					
		end;				
	end;
	return retpos;	
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local serialno;
	local cardno;
	local remainedstr;

	if (string.len(szStr) ~= DataLength) then	
		return;
	end;
	
	local str = unsettledstr .. szStr;
	
	-- 在unsetlledstr中解析
	-- 从4开始找起
	local sectionhead = "\52\28%d+";
	
	-- 先找出每一段的开始下标
	local indextable = {};
	local index = 0;
	while true do
		index = string.find(str, sectionhead, index + 1);
		if index == nil then 
			break;
		end;
		table.insert(indextable, index);			
	end;

	if table.getn(indextable) > 0 then
		-- 解析每一段数据
		local parsestr = "";
		for i = 1, table.getn(indextable)-1 do
			parsestr = string.sub(str, indextable[i], indextable[i+1]);
			--print(parsestr.."\n");
			-- 先查找序列号，再查找卡号
			locateserial(parsestr);

			locatecard(parsestr);
		end;
		remainedstr = string.sub(str, indextable[table.getn(indextable)], -1);

	else
		remainedstr = str;
	end;

	-- 对剩下的字符串做查找处理
	local serialpos = locateserial(remainedstr);
	local cardpos = locatecard(remainedstr);
	local strpos;
		
	if serialpos and cardpos then
		strpos = serialpos > cardpos and serialpos or cardpos;
	elseif serialpos then
		strpos = serialpos;
	elseif cardpos then
		strpos = cardpos;
	else 
		strpos = 1;
	end;
	
	if strpos == 1 then
		unsettledstr = remainedstr;
	else
		unsettledstr = sectionhead .. string.sub(remainedstr, strpos, -1);
	end;
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= DataLength,
	AlarmTime	= 10,	
	ParseHead	= ParseHead,	
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,	
	Name = "WTN_ATM",
	CPlusPlus	= CPlusPlus
}

--ParseData("123456");

return COM;
