-- 这是Watchnet Dibold Pos机协议的脚本
local COM = {};

local RetInfo = 
{
	cardno = 1, serialno = 2, transaction_type = 3, 
	transaction_amount = 4, transaction_time = 5, machineno = 6,
	emendtime = 0xa0, clear = 0xfd, startrec = 0xfe, stoprec=0xff
};

local unsettledstr = "";
-- channel为0表示所操作的通道从配置文件中获得.
local channel = 0xffff;
local cardnumber= "";

--头部标志的开始位置,头部以两个*开始,两个*结束
local  headStopFlag = 0;

local function ClearCardNo()
	cardnumber = "";
end;
local headCount = 0;
local IsFindTime = false;
local IsFindBankName = false;

--需要根据交易序列号进行查询,所以需要进行特殊处理
local SerialNo = "";
--[[
	此函数用于清除字符串之中的空格
--]]
local function ClearBlank(str)
	local retStr = str;
	if nil ~= str then
		local blankStart,blankStop = string.find(str," ");
		while nil ~= blankStart do
			local str1 = string.sub(retStr,1,blankStart - 1);
			local str2 = string.sub(retStr,blankStop + 1,string.len(retStr))
			retStr = str1 .. str2;
			blankStart,blankStop = string.find(retStr," ");
		end; 
		return retStr;
	end
end;

--[[
	此函数用于从串口过来的数据中获取银行名称
--]]
local headStartFlag = 0;
local headCount = 0;
local bankNameStr = "";
local function FindBankName(szStr)
	--过来第一个*
	if headCount == 0 and szStr == "*" then
		headCount = headCount + 1;
		return false;
	end;
	
	--对于非连续的两个*放弃
	if headCount == 1 and szStr ~= "*" then
		headCount = 0;
		return false;
	end;
	
	if headCount == 1 and szStr == "*" then
		headCount = headCount + 1;
		headStartFlag = 1;
		return false;
	end;
	--过来一个结束*过来
	if headCount == 2 and szStr == "*" then
		headStartFlag = 0;
		headCount = headCount + 1;
		return false
	end;
	
	if headStartFlag > 0 then
		bankNameStr = bankNameStr .. szStr;
	end;
	
	if headCount == 3 and szStr ~= "*" then
		headCount = 2;
		return false;
	end;
		
	if headCount == 3 and szStr == "*" then
		print("bankNameStr:",bankNameStr);
		IsFindBankName = true;
		COM.ExtraProcess(RetInfo.startrec, channel, " ")
		COM.AppendCard(RetInfo.branchno , channel, bankNameStr, COM.CPlusPlus);
		bankNameStr = "";
		headCount = 0;
		headStartFlag = 0;
		return true;
	end;
	 	
end;

--[[
	此函数用于从串口过来的数据中获取日期
--]]
local DateInStr = "";
local DateSplitTimes = 0;
local DateLen = 0;
local DateStrStarted = false;	
local Date = "";
local function FindDate(szStr)
	DateInStr = DateInStr .. szStr;
	DateLen = DateLen + 1;
	--只需要保留/前面的两个字符即可
	if string.len(DateInStr) > 2 and szStr ~= "/" and DateStrStarted == false then
		DateInStr = string.sub(DateInStr, 2,3);
		DateLen = 0;
		return false;
	end;
	
	if szStr == "/" then
		DateStrStarted = true;
		DateSplitTimes = DateSplitTimes + 1;
		return;
	end;
	if DateLen == 0x08 and DateSplitTimes ~= 2 then
		DateInStr = "";
		Date = "";
		DateStrStarted = false;
		DateLen = 0;
		DateSplitTimes = 0;
	end;
	if DateStrStarted == true and DateLen == 0x08 then
		Date = DateInStr;
		print("Date:",Date);
		DateInStr = "";
		--Date = "";
		DateStrStarted = false;
		DateLen = 0;
		DateSplitTimes = 0;
		return true;
	end;
end;
--[[
	此函数用于从串口过来的数据中获取具体时间
--]]
local TimeInStr = "";
local TimeSplitTimes = 0;
local TimeLen = 0;
local TimeStrStarted = false;	
local Time = "";
local function FindTime(szStr)
	TimeInStr = TimeInStr .. szStr;
	TimeLen = TimeLen + 1;
	--只需要保留/前面的两个字符即可
	if string.len(TimeInStr) > 2 and szStr ~= ":" and TimeStrStarted == false then
		TimeInStr = string.sub(TimeInStr, 2,3);
		TimeLen = 0;
		return false;
	end;
	
	if szStr == ":" then
		TimeStrStarted = true;
		TimeSplitTimes = TimeSplitTimes + 1;
		return;
	end;
	if TimeLen == 0x08 and TimeSplitTimes ~= 2 then
		TimeInStr = "";
		Time = "";
		TimeStrStarted = false;
		TimeLen = 0;
		TimeSplitTimes = 0;
	end;
	if TimeStrStarted == true and TimeLen == 0x08 then
		Time = TimeInStr;
		print("Time:",Time);
		local DateTime = Date .. Time;
		print("Date:",Date);
		print(DateTime);
		DateTime = string.sub(DateTime, 1, string.len(DateTime) - 2);
		COM.AppendCard(RetInfo.transaction_time, channel, DateTime, COM.CPlusPlus);
		TimeInStr = "";
		Date = "";
		Time = "";
		TimeStrStarted = false;
		TimeLen = 0;
		TimeSplitTimes = 0;
		IsFindTime = true;
		return true;
	end;
end;
--[[
	此函数用于从串口过来的数据中在获取到具体的时间之后,紧接着获取机器名称
--]]
local MachineNameInStr = "";
local MachinName = "";
local MachineNameStarted = false;
local MachineNameStartedTimes = 0;
local CCount = 0;
local function FindMachineName(szStr)
	MachineNameInStr = MachineNameInStr .. szStr;
	if szStr ~= "C" and MachineNameStarted == false then
		MachineNameInStr = "";
		return false;
	end;
	if szStr == "C" and MachineNameStartedTimes == 0 then
		MachineNameStarted = true;
		MachineNameStartedTimes = MachineNameStartedTimes + 1;
		return false;
	end;
	
	if string.byte(szStr) < 0x20 then
		MachineName = string.sub(MachineNameInStr, 2, string.len(MachineNameInStr) - 1);
		COM.AppendCard(RetInfo.machineno, channel, MachineName, COM.CPlusPlus);
		MachineName = "";
		MachineNameInStr = "";
		MachineNameStarted = false;
		MachineNameStartedTimes = 0;
		CCount = 0;
		IsFindTime = false;
		IsFindBankName = false;
	end;
--[[
	if szStr == "0" then
		CCount = CCount + 1;
		return false;
	end;
	
	if CCount == 4 and MachineNameStartedTimes == 1 then
		MachineName = string.sub(MachineNameInStr, 2,string.len(MachineNameInStr) - 8);
		print("MachineName:",MachineName);
		--MachineName = string.sub(MachineName, 1,string.len(MachineName) - 1);
		COM.AppendCard(RetInfo.machineno, channel, MachineName, COM.CPlusPlus);
		MachineName = "";
		MachineNameInStr = "";
		MachineNameStarted = false;
		MachineNameStartedTimes = 0;
		CCount = 0;
		IsFindTime = false;
		IsFindBankName = false;
		return true;
	end;
--]]
end;
--[[
	此函数用于从串口过来的数据中获取PERUSAHAAN后面的值
--]]
local unknownInStr = "";
local unknownStr = "";
local PerUsaHann = "";
local function FindPERUSAHAAN(szStr)
	unknownInStr = unknownInStr .. szStr;
	local unknownStarted,unKnownStoped = string.find(unknownInStr, "PERUSAHAAN");
	if unknownStarted ~= nil then
		unknownStr = unknownStr .. szStr;
		if string.byte(szStr) < 0x20 then
			local tmp = unknownStr;
			tmp = string.sub(tmp, 2, string.len(tmp) - 1);	
			PerUsaHann = "PERUSAHAAN" .. tmp;
			print(PerUsaHann);
			COM.AppendCard(RetInfo.serialno , channel, PerUsaHann, COM.CPlusPlus);
			PerUsaHann = "";
			unknownInStr = "";
			unknownStr = "";
			return true;
		end;
	end;
end;

--[[
	此函数用于从串口过来的数据中获取PEMBELIAN后面的值
--]]
local unknownInStr2 = "";
local unknownStr2 = "";
local PemBelian = "";
local function FindPEMBELIAN(szStr)
	unknownInStr2 = unknownInStr2 .. szStr;
	local unknownStarted,unKnownStoped = string.find(unknownInStr2, "PEMBELIAN");
	if unknownStarted ~= nil then
		unknownStr2 = unknownStr2 .. szStr;
		if string.byte(szStr) < 0x20 then
			local tmp = unknownStr2;
			tmp = string.sub(tmp, 2, string.len(tmp) - 1);	
			PemBelian = "PEMBELIAN" .. tmp;
			print(PemBelian);
			COM.AppendCard(RetInfo.transaction_type , channel, PemBelian, COM.CPlusPlus);
			PemBelian = "";
			unknownInStr2 = "";
			unknownStr2 = "";
			return true;
		end;
	end;
end;

--[[
	此函数用于从串口过来的数据中获取PELANGGAN后面的值
--]]
local unknownInStr3 = "";
local unknownStr3 = "";
local PelangGan = "";
local function FindPELANGGAN(szStr)
	unknownInStr3 = unknownInStr3 .. szStr;
	local unknownStarted,unKnownStoped = string.find(unknownInStr3, "PELANGGAN");
	if unknownStarted ~= nil then
		unknownStr3 = unknownStr3 .. szStr;
		if string.byte(szStr) < 0x20 then
			local tmp = unknownStr3;
			tmp = string.sub(tmp, 2, string.len(tmp) - 1);	
			PelangGan = "NO.PELANGGAN" .. tmp;
			print(PelangGan);
			COM.AppendCard(RetInfo.rest_amount , channel, PelangGan, COM.CPlusPlus);
			PelangGan = "";
			unknownInStr3 = "";
			unknownStr3 = "";
			return true;
		end;
	end;
end;

--[[
	此函数用于从串口过来的数据中获取交易金额,也就是JUMLAH后面的值
--]]
local unknownInStr4 = "";
local unknownStr4 = "";
local JumLah = "";
local function FindJUMLAH(szStr)
	unknownInStr4 = unknownInStr4 .. szStr;
	local unknownStarted,unKnownStoped = string.find(unknownInStr4, "JUMLAH");
	if unknownStarted ~= nil then
		unknownStr4 = unknownStr4 .. szStr;
		if string.byte(szStr) < 0x20 then
			local tmp = unknownStr4;
			tmp = string.sub(tmp, 2, string.len(tmp));	
			tmp = ClearBlank(tmp);
			JumLah = "JUMLAH" .. tmp;
			print(JumLah);
			COM.AppendCard(RetInfo.transaction_amount , channel, JumLah, COM.CPlusPlus);
			JumLah = "";
			unknownInStr4 = "";
			unknownStr4 = "";
			return true;
		end;
	end;
end;

--[[
	此函数用于处理交易序列号,为了录像查询的需要
--]]
local function GetSerialNo(str)
	local flagStart, flagStop = string.find(str, ":");
	if flagStart == nil then
		print("Cannot get Serial no");
		return false;
	end;
	
	local serialno = string.sub(str, flagStop + 1, string.len(str));
	print("serialno:",serialno);
	
	SerialNo = ClearBlank(serialno);
	
	print("SerialNo:",SerialNo);
	COM.AppendCard(RetInfo.cardno, channel, SerialNo, COM.CPlusPlus);
end;

--[[
	此函数用于从串口过来的数据中获取交易序列号,也就是URUT后面的值
--]]
local unknownInStr5 = "";
local unknownStr5 = "";
local Urut = "";
local function FindURUT(szStr)
	unknownInStr5 = unknownInStr5 .. szStr;
	local unknownStarted,unKnownStoped = string.find(unknownInStr5, "URUT");
	if unknownStarted ~= nil then
		unknownStr5 = unknownStr5 .. szStr;
		if string.byte(szStr) < 0x20 then
			local tmp = unknownStr5;
			tmp = string.sub(tmp, 2, string.len(tmp) - 1);	
			Urut = "URUT" .. tmp;
			print(Urut);
			GetSerialNo(Urut);
			Urut = "";
			unknownInStr5 = "";
			unknownStr5 = "";
			return true;
		end;
	end;
end;
-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	return true;
end;


--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]

local function ParseData(szStr)

	unsettledstr = unsettledstr .. szStr;
	if string.len(unsettledstr) < 10 then
		return false;
	end;
	
	FindBankName(szStr);
	
	if IsFindBankName == true then
		FindDate(szStr);
		FindTime(szStr);
	end;
	
	--获取机器名称
	if IsFindTime == true then
		FindMachineName(szStr);
	end;
	
	--获取PERUSAHAAN
	FindPERUSAHAAN(szStr);
	
	--获取PEMBELIAN
	FindPEMBELIAN(szStr);
	
	--获取PELANGGAN
	FindPELANGGAN(szStr);
	
	--获取交易金额
	FindJUMLAH(szStr);
	
	--获取交易序列号
	FindURUT(szStr);
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	Name		= "DIEBOLD_ATM",
	CPlusPlus	= CPlusPlus
}

return COM;