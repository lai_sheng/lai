--[[
	这是根据大华自定的ATM录像机与ATM机器之间的数据交换格式,定制的协议
	Created  by yangb 2007.4.20
--]]

local COM = {};

local RetInfo = 
{
	cardno = 1, serialno = 2, transaction_type = 3, 
	transaction_amount = 4, transaction_time = 5, machineno = 6,
	emendtime = 0xa0, startnormalrec = 0xa1, stopnormalrec = 0xa2,
	clear = 0xfd, startrec = 0xfe, stoprec=0xff, resetalarm = 0xc0,
};

local unsettledstr = "";

local function ClearCardNo()
	cardnumber = "";
end;

local function FormatTime(s)
	-- 把时间转化为C的格式，用ASCII码表示，YYYYMMDDHHMMSS+差额，差额以S为单位, 没有的话用0表示
	local str = s .. "0";
	return str;
end;

-- DVR向ATMC发送设备状态报告

local function SendStateReport()
	local statereport = string.char(0xA4);
	
	statereport = statereport .. string.char(0x17);
	
	-- 填写摄像机状态
	for i = 1, 16 do
		COM.CameraState[i] = 0;
	end;
	COM.GetCameraState();
	for i = 1, 16 do
		statereport = statereport .. tostring(COM.CameraState[i]);
	end;
	
	--填写外部报警状态
	COM.GetAlarmState();
	
	statereport = statereport .. tostring(COM.AlarmState[1]);
	statereport = statereport .. tostring(COM.AlarmState[2]);
	
	--填写系统状态，暂时默认为稳定
	statereport = statereport .. "0"
	
	--填写结束字符
	local etx = string.char(0xe0);
	statereport = statereport .. etx;
	
	-- 计算校验码
	local check = 0;
	local tmpstr;
	for i = 1, string.len(statereport) do
		check = math.mod(check + string.byte(string.sub(statereport,i,i)),256);
	end; 
	
	local checkchar = string.char(check);
	statereport = statereport .. checkchar;
	
	-- 发送设备状态报告
	print("send state report : ", statereport);
	print("state report length : ", string.len(statereport));
	COM.ComWrite(statereport);
end;

local insertCardRecdChannel = 0x01;
local cashOutNoticeRecdChannel = 0x02;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	return true;
	--[[
	print("getHead:",szStr);
	if unsettledstr ~= "" then
		return true;
	end;
	
	if string.byte(string.sub(szStr, 1, 1)) == 0xA3 then
		print("~~~~~~~~get head now");
		return true;
	end;

	return false;
	--]]
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]

local function ParseData(szStr)
	local cardNo;	
	unsettledstr = unsettledstr .. szStr
	local str = unsettledstr;
	--首先获取命令包的长度
	if string.len(str) < 2 then
		return true;
	end;

	local commandLength = string.byte(string.sub(str,2,2));
	print(commandLength);
	print("cmd length is:",commandLength);
	
	-- 如果读到的命令长度不足(无法解析命令长度), 保存后返回.
	if string.len(str) < commandLength then
		unsettledstr = str;
		return;
	end;
	--此处获得的长度已经足够,将unsettledstr置空
	unsettledstr = "";
	
	--判断结束字节是否正确
	if string.byte(string.sub(str,-2,-2)) ~= 0xE0 then
		print("ETX Error");
		return;
	end;
	
	--此处已经获得了足够的命令长度,开始执行校验工作
	local checksum = 0;
	for i = 1,string.len(str) - 1 do
		checksum = math.mod(checksum + string.byte(string.sub(str,i,i)),256);
	end;
	
	if checksum ~= string.byte(string.sub(str,-1,-1)) then
		print("Checksum Error");
		--return 
	end;
	
	print("CheckSum ok");
	
	--获取事件类型,ATM机系统时间
	local eventType = string.sub(str,3,3);
	print("eventType:",string.byte(eventType));
	
	local systemTimeAtm = string.sub(str,4 ,17);
	print("System Time:",systemTimeAtm);
	
	if eventType ~= nil and systemTimeAtm ~= nil then
		print("Get ExtInfo and event Type OK");
	end;
	
	eventType = string.byte(eventType);
	--此处已经获取足够的信息,开始执行一系列动作
	
	--插卡动作,开启第一通道卡号录象 ,将卡号信息叠加在第一通道画面上,返发DVR状态数据。
	if eventType == 0x01 then
		print("start card recd of channel 1");
		COM.ExtraProcess(RetInfo.startrec, insertCardRecdChannel, " ", COM.CPlusPlus);
		--获取卡号信息长度以及卡号信息
		local cardNoLength = string.byte(string.sub(str,18,18));
		print("cardNoLength is:",cardNoLength);
		cardNo = string.sub(str,19,19 + cardNoLength - 1);
		print("cardNo:",cardNo); 
		--此处还要更新系统时间
		COM.ExtraProcess(RetInfo.emendtime, insertCardRecdChannel, systemTimeAtm, COM.CPlusPlus);
		COM.AppendCard(RetInfo.cardno, insertCardRecdChannel, cardNo, COM.CPlusPlus);
		SendStateReport();
		return;
	end;
	
	--退卡动作,延时10s关闭第一通道录像
	if eventType == 0x02 then
		COM.ExtraProcess(RetInfo.stoprec, insertCardRecdChannel, "10", COM.CPlusPlus);
		SendStateReport();
		return;
	end;
	
	--交易动作,完成交易信息叠加,将交易类型叠加在画面上，将交易金额叠加在画面,返发DVR状态数据。
	if eventType == 0x03 then
		--获取交易类型
		local transType = string.byte(string.sub(str,18,18));
		print("transType:",transType);
		--叠加交易类型
		COM.AppendCard(RetInfo.transaction_type, insertCardRecdChannel, transType, COM.CPlusPlus);	
		
		--如果是非查询和改密两项交易类型和流水号,则需要获取交易金额并叠加
		if transType ~= 0xB1 and transType ~= 0xB4 then
			local transAmount = string.sub(str,19,30);
			print("transAmountL:",transAmount);
			COM.AppendCard(RetInfo.transaction_amount, insertCardRecdChannel, transAmount, COM.CPlusPlus);
			
			local serialNo = string.sub(str,31,42);
			print("serialNoL:",serialNO);
			COM.AppendCard(RetInfo.serialno, insertCardRecdChannel, serialNO, COM.CPlusPlus);
			
			--如果是转账还需要获取转账账号,并叠加
			if transType == 0xB3 then
				--获得转账账号长度
				local otherAccountLength = string.byte(string.sub(str,43,43));
				print("otherAccountLength:",otherAccountLength);
				local otherAccount = string.sub(str,44,44 + otherAccountLength - 1);
				print("otherAccount:",otherAccount);
				COM.AppendCard(RetInfo.cardno, insertCardRecdChannel, otherAccount, COM.CPlusPlus);
			end;
		else --只获取流水号
			local serialNo = string.sub(str,19,30)
			print("serialNoL:",serialNO);
			COM.AppendCard(RetInfo.serialno, insertCardRecdChannel, serialNO, COM.CPlusPlus);
		end;	
		SendStateReport();
		return;
	end;
	
	--如果是出钞通知,开启第二通道卡号录象，在第二通道画面上叠加卡号信息,返发DVR状态数据。
	if eventType == 0x04 then
		COM.ExtraProcess(RetInfo.startrec, cashOutNoticeRecdChannel, " ");
		COM.AppendCard(RetInfo.cardno, cashOutNoticeRecdChannel, cardNo, COM.CPlusPlus);
		SendStateReport();
		return;
	end;
	
	--如果是出钞成功或吞钞确认
	if eventType == 0x05 then
		COM.ExtraProcess(RetInfo.stoprec, cashOutNoticeRecdChannel, "10", COM.CPlusPlus);
		SendStateReport();
		return;
	end
	
	if eventType == 0x06 then
		SendStateReport();
	end;

end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	ParseHead	= ParseHead,
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 60,
	AlarmState	= {0,0},
	CameraState	= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	Name = "DH_ATM",
	CPlusPlus	= CPlusPlus
}

return COM;