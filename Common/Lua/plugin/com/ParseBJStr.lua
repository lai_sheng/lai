-- 该配置文件是北京某公司使用的脚本
local COM = {};
local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local DataStructLen = 
{
	{mti = 4}, 
	{cardno = 19}, 
	{transaction_type = 3}, 
	{transaction_time = 10}, 
	{machineno = 15}, 
	{serialno = 6}, 
	{transaction_amount = 12}, 
	{reserved = 20},
};

local function ConstructTable(arg1, arg2)
	local RetTable = {};
	RetTable.beginpos = arg1;
	RetTable.endpos = arg2;
	return RetTable;
end;

local function Conver(DataTable)
	local length = 0;
	local RetTable = {};
	for i,t in ipairs(DataTable) do
			for k,v in pairs(t) do
			RetTable[k] = ConstructTable(length + 1,length + v);
			--print(k, RetTable[k].beginpos, RetTable[k].endpos);
			length = length + v;			
		end;
	end;
	
	return RetTable, length;
end;

local function FormatTime(s)
	-- 把时间转化为C的格式，用ASCII码表示，YYYYMMDDHHMMSS+差额，差额以S为单位, 没有年的话用0表示
	local str = "0000" .. s .. "0";
	return str;
end;

local DataStruct, DataLength = Conver(DataStructLen);

local channel = 0xff;

local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	local str = string.sub(szStr, DataStruct.mti.beginpos, DataStruct.mti.endpos);
	if str == "9510" or str == "9520" then
		return 1;
	end;
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]

local function ParseData(szStr)
	local mti;
	local cardno;
	local transaction_type;
	local transaction_time;
	local machineno;
	local serialno;
	local transaction_amount;
	
	if (string.len(szStr) ~= DataLength) then
		return;
	end;
	
	mti = string.sub(szStr, DataStruct.mti.beginpos, DataStruct.mti.endpos);
	
	if mti == "9510" then
		-- 校正时间
		transaction_time 	= string.sub(szStr, DataStruct.transaction_time.beginpos, DataStruct.transaction_time.endpos);
		--COM.ExtraProcess(Lua2C.emendtime, channel, transaction_time);
		
		-- 处理报文	
		transaction_type 	= string.sub(szStr, DataStruct.transaction_type.beginpos, DataStruct.transaction_type.endpos);
		if string.lower(transaction_type) == string.lower("EJT") or string.lower(transaction_type) == string.lower("RET") then
			COM.ExtraProcess(Lua2C.clear, channel, " ",COM.CPlusPlus);
			return;
		end;
			
		cardno 				= string.sub(szStr,  DataStruct.cardno.beginpos, DataStruct.cardno.endpos);
		if cardno ~= cardnumber then
			if COM.ExtraProcess(Lua2C.startrec, channel, " ",COM.CPlusPlus) then
				 cardnumber = cardno;
			end;
		end;
		
		COM.AppendCard(Lua2C.cardno, channel, cardno,COM.CPlusPlus);	
		
		COM.AppendCard(Lua2C.transaction_type, channel, transaction_type,COM.CPlusPlus);	
		
		COM.AppendCard(Lua2C.transaction_time, channel, transaction_time,COM.CPlusPlus);		
		
		machineno			= string.sub(szStr, DataStruct.machineno.beginpos, DataStruct.machineno.endpos,COM.CPlusPlus);
		COM.AppendCard(Lua2C.machineno, channel, machineno);
		
		serialno 			= string.sub(szStr, DataStruct.serialno.beginpos, DataStruct.serialno.endpos,COM.CPlusPlus);
		COM.AppendCard(Lua2C.serialno, channel, serialno);
		
		transaction_amount 	= string.sub(szStr, DataStruct.transaction_amount.beginpos, DataStruct.transaction_amount.endpos,COM.CPlusPlus);
		COM.AppendCard(Lua2C.transaction_amount, channel, transaction_amount);

	elseif mti == "9520" then
		-- 关闭卡号录像
		COM.ExtraProcess(Lua2C.stoprec, channel, " ");
		-- 只校准时间
		transaction_time 	= string.sub(szStr, DataStruct.transaction_time.beginpos, DataStruct.transaction_time.endpos);
		transaction_time = FormatTime(transaction_time);
		COM.ExtraProcess(Lua2C.emendtime, channel, transaction_time, COM.CPlusPlus);
	end;
end

COM = 
{
	HeadLength	= DataStructLen[1].mti,
	DataLength 	= DataLength,
	AlarmTime	= 60,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	CPlusPlus 	= CPlusPlus;
	Name		= "BJ_ATM",

}
return COM;