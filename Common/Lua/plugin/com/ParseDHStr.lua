-- 该配置文件是DaHua的ATM脚本
local COM = {};

local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local cardnumber = "";

local DataStructLen = 
{
	{head = 4}, 
	{ichannel = 1}, 
	{infotype = 1}, 
	{buffer = 25}, 
	{checksum = 1}, 
};

local function ClearCardNo()
	cardnumber = "";
end;

local function ConstructTable(arg1, arg2)
	local RetTable = {};
	RetTable.beginpos = arg1;
	RetTable.endpos = arg2;
	return RetTable;
end;

local function Conver(DataTable)
	local length = 0;
	local RetTable = {};
	for i,t in ipairs(DataTable) do
			for k,v in pairs(t) do
			RetTable[k] = ConstructTable(length + 1,length + v);
			--print(k, RetTable[k].beginpos, RetTable[k].endpos);
			length = length + v;			
		end;
	end;
	
	return RetTable, length;
end;

local DataStruct, DataLength = Conver(DataStructLen);

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	local str = string.sub(szStr, DataStruct.head.beginpos, DataStruct.head.endpos);
	if str == "DH  "  then
		return 1;
	end;
end;
--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local head;
	local ichannel;
	local infotype;
	local buffer;
	local check;
	local rettype;
	if (string.len(szStr) ~= DataLength) then
		return;
	end;
	
	
	
	head = string.sub(szStr, DataStruct.head.beginpos, DataStruct.head.endpos);
	
	if head == "DH  " then
		-- 处理报文
		ichannel 	= string.byte(string.sub(szStr, DataStruct.ichannel.beginpos, DataStruct.ichannel.endpos));
		infotype 	= string.byte(string.sub(szStr, DataStruct.infotype.beginpos, DataStruct.infotype.endpos));
		buffer 		= string.sub(szStr,  DataStruct.buffer.beginpos, DataStruct.buffer.endpos);
		if infotype == 0x01 then
			rettype = Lua2C.cardno;
		elseif infotype == 0x02 then
			rettype = Lua2C.serialno;
		elseif infotype == 0x03 then
			rettype = Lua2C.transaction_type;
		elseif infotype == 0x04 then
			rettype = Lua2C.transaction_amount;
		elseif infotype == 0x05 then
			rettype = Lua2C.transaction_time;
		elseif infotype == 0xfe then
			rettype = Lua2C.startrec;
		elseif infotype == 0xff then
			rettype = Lua2C.stoprec;
		else
			return;
		end;
		
		if rettype <= Lua2C.machineno then
			COM.AppendCard(rettype, ichannel, buffer, COM.CPlusPlus);	
		elseif rettype >= Lua2C.emendtime then

			COM.ExtraProcess(rettype, ichannel, buffer, COM.CPlusPlus);
		end;
	end;
end

COM = 
{
	HeadLength	= DataStructLen[1].head,
	DataLength 	= DataLength,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 20,	
	CPlusPlus	= CPlusPlus,
	Name 		= "DAHUA_ATM"	
}

COM.Name = "DAHUA_ATM";
return COM;