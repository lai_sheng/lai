-- 这是东信灵通的解析脚本
local COM = {};
local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;

local unsettledstr = "";

local cardnumber = "";

local function ClearCardNo()
	cardnumber = "";
end;

local function FormatTime(s)
	-- 把时间转化为C的格式，用ASCII码表示，YYYYMMDDHHMMSS+差额，差额以S为单位, 没有年的话用0表示
	local str = "0000" .. s .. "0";
	return str;
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	local str  = string.sub(szStr, 1, 2);
	if (str < "\123") then
		return 1;
	end;
end;
--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local iChannel
	local iStart
	local iEnd
	local iLen
	local szRet
	local szTem
	local szLeave
	local szFlag
	local dbDivision
	local dbSecDivision
	local Cardlen;
	local DataLen;
	
	local mti;
	local cardno;
	local transaction_type;
	local transaction_time;
	local machineno;
	local serialno;
	local transaction_amount;
	
	local channel = 0;
	
	local str = unsettledstr .. szStr;
	
	szTem = string.sub(str, 1, 1);
	if (szTem ~= "9") then
		unsettledstr = "";
		return;
	end;
	
	if (string.len(str) < 6) then
		unsettledstr = str;
		return;
	end;
	
	Cardlen = tonumber(string.sub(str, 5, 6));
	DataLen	= 72 + Cardlen;
	if (string.len(str) < DataLen) then
		unsettledstr = str;
		return;
	end;
	
	unsettledstr = "";
	
	mti = string.sub(str, 1, 4);
	if mti == "9510" then
		-- 处理报文
		cardno 			= string.sub(str,  7, (6+Cardlen));	
		if cardno ~= cardnumber then
			if COM.ExtraProcess(Lua2C.startrec, channel, " ") then
				 cardnumber = cardno;
			end;
		end;
		COM.AppendCard(Lua2C.cardno, channel, cardno, COM.CPlusPlus);
		
		transaction_type 	= string.sub(str, (7+Cardlen), (9+Cardlen));
		COM.AppendCard(Lua2C.transaction_type, channel, transaction_type, COM.CPlusPlus);	
		
		machineno		= string.sub(str, (20+Cardlen), (34+Cardlen));
		COM.AppendCard(Lua2C.machineno, channel, machineno, COM.CPlusPlus);
		
		serialno 		= string.sub(str, (35+Cardlen), (40+Cardlen));
		COM.AppendCard(Lua2C.serialno, channel, serialno, COM.CPlusPlus);
		
		transaction_amount 	= string.sub(str, (41+Cardlen), (52+Cardlen));
		COM.AppendCard(Lua2C.transaction_amount, channel, transaction_amount, COM.CPlusPlus);

	elseif mti == "9520" then
		-- 只校准时间
		transaction_time 	= string.sub(str, (10+Cardlen), (19+Cardlen));
		transaction_time = FormatTime(transaction_time);
		COM.ExtraProcess(Lua2C.emendtime, channel, transaction_time, COM.CPlusPlus);
	end;
end

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60,	
	ParseHead	= ParseHead,
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,
	CPlusPlus	= CPlusPlus,
	Name = "EASTCOM_ATM"
}
return COM;