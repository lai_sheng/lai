--给广电运通使用的脚本，使用是将此文件改名为ParseDVRStr.lua即可
COM = {};
local luah = require("ATMHead");
local Lua2C, C2Lua = luah.Lua2C, luah.C2Lua;
g_dbStart = "\2";
g_dbEnd = "\3";

local unsettledstr = "";

local cardnumber = "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)
	local str  = string.sub(szStr, 1, 2);
	if (str < "\123") then
		return 1;
	end;
end;
--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
	local iChannel
	local iStart
	local iStart2
	local iEnd
	local iEnd2
	local iLen
	local szRet
	local szTem
	local szLeave
	local szFlag
	local dbDivision
	local dbSecDivision
	local return_message
	local result
	
	local mti;
	local cardno;
	local transaction_type;
	local transaction_time;
	local machineno;
	local branchno;
	local serialno;
	local transaction_amount;

	dbDivision = "\28";
	dbSecDivision = "\29";
	
	--读到0x03开始分析
	local str = unsettledstr .. szStr;
	szTem = string.sub(str, string.len(str), string.len(str));
	
	if (szTem ~= g_dbEnd) then
		unsettledstr = str;
		return;
	end;
	unsettledstr = "";
		
	--读命令字
	iStart, iEnd = string.find(str, dbDivision);
	if iStart ~= nil then
		szLeave = string.sub(str, iStart+1);
	else
		return;
	end;
	
	iStart2, iEnd2 = string.find(szLeave, dbDivision);
	if iStart ~= nil then
		szFlag = string.sub(szLeave, iStart2+1, iStart2+2);
		return_message = string.sub(str, 1, iStart+iStart2+3);
	else
		return;
	end;

	
--获得通道号
	iChannel = string.sub(str, string.len(str)-6, string.len(str)-3); 
	
-----------------------------启动录像指令----------------------------
	if szFlag == "01" then
--启动录像
		COM.ExtraProcess(Lua2C.startrec, iChannel, " ");
--获得ATM号
		iStart, iEnd = string.find(str, "DTATM");
		szLeave = string.sub(str, iEnd+1);
		iStart, iEnd = string.find(szLeave, dbDivision);
		machineno = string.sub(szLeave, 1, iStart-1);
		COM.AppendCard(Lua2C.machineno, iChannel, machineno,COM.CPlusPlus);

--获得支行号
		szLeave = string.sub(szLeave, iStart+1);
		iStart, iEnd = string.find(szLeave, dbDivision);
		szRet = string.sub(szLeave, 1, iStart-1);
		branchno = szRet;
		COM.AppendCard(Lua2C.branchno, iChannel, branchno,COM.CPlusPlus);

--获得日期
		szLeave = string.sub(szLeave, iStart+1);
		szLeave = string.sub(szLeave, 4);
		transaction_time = string.sub(szLeave, 1, 8);
--		COM.AppendCard(Lua2C.cardno, iChannel, cardno, COM.CPlusPlus);
--获得时间
		szLeave = string.sub(szLeave, 9);
		szTem = string.sub(szLeave, 1, 6);
		transaction_time = transaction_time .. szTem;
		COM.AppendCard(Lua2C.transaction_time, iChannel, transaction_time,COM.CPlusPlus);
--获得字段内容
		szLeave = string.sub(szLeave, 7);
		iStart = 1;
		while (iStart ~= nil) do
			szTem = string.sub(szLeave, 1, 2);	--字段类型
			szLeave = string.sub(szLeave, 3);	--字段内容
			iStart, iEnd = string.find(szLeave, dbDivision);
			szRet = string.sub(szLeave, 1, iStart-1);
			
			if (szTem == "01") then
				cardno = szRet;
				COM.AppendCard(Lua2C.cardno, iChannel, cardno,COM.CPlusPlus);
			elseif (szTem == "03") then
				serialno = szRet;
				COM.AppendCard(Lua2C.serialno, iChannel, serialno,COM.CPlusPlus);
			elseif (szTem == "04") then
				transaction_type = szRet;
				COM.AppendCard(Lua2C.transaction_type, iChannel, transaction_type,COM.CPlusPlus);
			elseif (szTem == "05") then
				transaction_amount = szRet;
				COM.AppendCard(Lua2C.transaction_amount, iChannel, transaction_amount,COM.CPlusPlus);
			elseif (szTem == "07") then
				errorcode = szRet;
				COM.AppendCard(Lua2C.errorcode, iChannel, errorcode,COM.CPlusPlus);
			end;
			
			iStart, iEnd = string.find(szLeave, dbSecDivision);
			if (iStart ~= nil) then
				szLeave = string.sub(szLeave, iStart+1);
			else
				iStart = nil;
				break;
			end;
		end;
--显示一帧数据
		COM.AppendCard(Lua2C.frameend, iChannel, " ",COM.CPlusPlus);
		
-----------------------------停止录像指令----------------------------
	elseif szFlag == "02" then
--停止录像
		COM.ExtraProcess(Lua2C.stoprec, iChannel, "30",COM.CPlusPlus);
--获得日期
--		szLeave = string.sub(szLeave, iStart+1);
--		szLeave = string.sub(szLeave, 4);
--		transaction_time = string.sub(szLeave, 1, 8);
--获得时间
--		szLeave = string.sub(szLeave, 9);
--		szTem = string.sub(szLeave, 1, 6);
--		transaction_time = transaction_time .. szTem;
--		COM.AppendCard(Lua2C.transaction_time, iChannel, transaction_time,COM.CPlusPlus);
		
-----------------------------交易信息指令----------------------------
	elseif szFlag == "05" then
--获得ATM号
		iStart, iEnd = string.find(str, "DTATM");
		szLeave = string.sub(str, iEnd+1);
		iStart, iEnd = string.find(szLeave, dbDivision);
		machineno = string.sub(szLeave, 1, iStart-1);
		COM.AppendCard(Lua2C.machineno, iChannel, machineno, COM.CPlusPlus);

--获得支行号
		szLeave = string.sub(szLeave, iStart+1);
		iStart, iEnd = string.find(szLeave, dbDivision);
		szRet = string.sub(szLeave, 1, iStart-1);
		branchno = szRet;
		COM.AppendCard(Lua2C.branchno, iChannel, branchno, COM.CPlusPlus);

--获得日期
		szLeave = string.sub(szLeave, iStart+1);
		szLeave = string.sub(szLeave, 4);
		transaction_time = string.sub(szLeave, 1, 8);
--		COM.AppendCard(Lua2C.cardno, iChannel, cardno, COM.CPlusPlus);
--获得时间
		szLeave = string.sub(szLeave, 9);
		szTem = string.sub(szLeave, 1, 6);
		transaction_time = transaction_time .. szTem;
		COM.AppendCard(Lua2C.transaction_time, iChannel, transaction_time, COM.CPlusPlus);
--获得字段内容
		szLeave = string.sub(szLeave, 7);
		iStart = 1;
		while (iStart ~= nil) do
			szTem = string.sub(szLeave, 1, 2);	--字段类型
			szLeave = string.sub(szLeave, 3);	--字段内容
			iStart, iEnd = string.find(szLeave, dbDivision);
			szRet = string.sub(szLeave, 1, iStart-1);
			
			if (szTem == "01") then
				cardno = szRet;
				COM.AppendCard(Lua2C.cardno, iChannel, cardno, COM.CPlusPlus);
			elseif (szTem == "03") then
				serialno = szRet;
				COM.AppendCard(Lua2C.serialno, iChannel, serialno, COM.CPlusPlus);
			elseif (szTem == "04") then
				transaction_type = szRet;
				COM.AppendCard(Lua2C.transaction_type, iChannel, transaction_type, COM.CPlusPlus);
			elseif (szTem == "05") then
				transaction_amount = szRet;
				COM.AppendCard(Lua2C.transaction_amount, iChannel, transaction_amount, COM.CPlusPlus);
			elseif (szTem == "07") then
				errorcode = szRet;
				COM.AppendCard(Lua2C.errorcode, iChannel, errorcode, COM.CPlusPlus);
			end;
			
			iStart, iEnd = string.find(szLeave, dbSecDivision);
			if (iStart ~= nil) then
				szLeave = string.sub(szLeave, iStart+1);
			else
				iStart = nil;
				break;
			end;
		end;
--显示一帧数据
		COM.AppendCard(Lua2C.frameend, iChannel, " ", COM.CPlusPlus);
		
-----------------------------开始普通录像指令----------------------------
	elseif szFlag == "03" then
--开始录像
		COM.ExtraProcess(Lua2C.startnormalrec, iChannel, " ", COM.CPlusPlus);
--获得日期
		szLeave = string.sub(szLeave, iStart+1);
		szLeave = string.sub(szLeave, 4);
		transaction_time = string.sub(szLeave, 1, 8);
--获得时间
		szLeave = string.sub(szLeave, 9);
		szTem = string.sub(szLeave, 1, 6);
		transaction_time = transaction_time .. szTem;
		COM.AppendCard(Lua2C.transaction_time, iChannel, transaction_time, COM.CPlusPlus);
		
-----------------------------停止普通录像指令----------------------------
	elseif szFlag == "04" then
--开始录像
		COM.ExtraProcess(Lua2C.stopnormalrec, iChannel, " ", COM.CPlusPlus);
--获得日期
		szLeave = string.sub(szLeave, iStart+1);
		szLeave = string.sub(szLeave, 4);
		transaction_time = string.sub(szLeave, 1, 8);
--获得时间
		szLeave = string.sub(szLeave, 9);
		szTem = string.sub(szLeave, 1, 6);
		transaction_time = transaction_time .. szTem;
		COM.AppendCard(Lua2C.transaction_time, iChannel, transaction_time, COM.CPlusPlus);
		
-----------------------------录像延时指令--------------------------------
	elseif szFlag == "06" then
--读入延时时间
		szLeave = string.sub(szLeave, iStart-2, iStart+1);
		COM.ExtraProcess(Lua2C.setdelaytime, iChannel, szLeave, COM.CPlusPlus);
		
-----------------------------时间校正指令--------------------------------
	elseif szFlag == "10" then
--读入时间
		szLeave = string.sub(szLeave, iStart-2, iStart+11);
		result = COM.ExtraProcess(Lua2C.emendtime, iChannel, szLeave, COM.CPlusPlus);

-----------------------------设置录像时间指令--------------------------------
	elseif szFlag == "12" then
--读入录像长度时间
		szLeave = string.sub(szLeave, iStart-2, iStart-2);
		COM.ExtraProcess(Lua2C.setreclen, iChannel, szLeave, COM.CPlusPlus);

-----------------------------启动信息显示指令--------------------------------
	elseif szFlag == "13" then
		COM.ExtraProcess(Lua2C.showinfoon, iChannel, "", COM.CPlusPlus);
		
-----------------------------停止信息显示指令--------------------------------
	elseif szFlag == "14" then
		COM.ExtraProcess(Lua2C.showinfooff, iChannel, "", COM.CPlusPlus);

-----------------------------ping指令---------------------------------------
	elseif szFlag == "15" then
		COM.ExtraProcess(Lua2C.pingcommand, iChannel, "", COM.CPlusPlus);
	end;
	
-----------------------------发送返回指令-----------------------------------
	if result == true then
		return_message = return_message .. "00";
	else
		return_message = return_message .. "95";
	end;
	return_message = return_message .. "39";
	return_message = return_message .. g_dbEnd;
	COM.ComWrite(return_message);
--	print(return_message);
end

COM = 
{
	HeadLength	= 1,
	DataLength 	= 1,
	AlarmTime	= 60,	
	ParseHead	= ParseHead,
	ParseData	= ParseData,
	ClearCardNo	= ClearCardNo,
	CPlusPlus	= CPlusPlus,
	Name 	= "GDYT_ATM"
}

return COM;