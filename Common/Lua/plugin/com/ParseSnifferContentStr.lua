--解析网络抓包的数据的脚本
local COM = {};
Lua2C=
	{
		cardno = 1, serialno = 2, transaction_type = 3, 
		transaction_amount = 4, transaction_time = 5, machineno = 6,
		branchno = 7,errorcode = 8, rest_amount = 9,
		self_definition_item1 = 0x20,self_definition_item2 = 0x21,self_definition_item3 = 0x22,
		emendtime = 0xa0, startnormalrec = 0xa1, stopnormalrec = 0xa2,
		setdelaytime = 0xa3,setreclen = 0xa4,showinfoon = 0xa5,showinfooff = 0xa6,
		pingcommand = 0xa7,frameend = 0xfb,frameallend = 0xfc,
		clear = 0xfd, startrec = 0xfe, stoprec=0xff, resetalarm = 0xc0,
		require_state = 0xd0,open_insurance_door = 0xe0,close_insurance_door = 0xe1,
		money_out_notice = 0xf1,money_out_success = 0xf2,
	};
local FrameID1Offset = 0;
local FrameID2Offset = 0;
local FrameID1Length = 0;
local FrameID2Length = 0;
local FrameID1Value = "";
local FrameID2Value = "";
local Field1Offset1 = 0;
local Field1Length1 = 0;
local Field1Title = "";
local Field1Offset2 = 0;
local Field1Length2 = 0;
local Field2Offset1 = 0;
local Field2Length1 = 0;
local Field2Title = "";
local Field2Offset2 = 0;
local Field2Length2 = 0;
local Field3Offset1 = 0;
local Field3Length1 = 0;
local Field3Title = "";
local Field3Offset2 = 0;
local Field3Length2 = 0;

local unsettledstr = "";

-- channel为0表示所操作的通道从配置文件中获得.
local RecChannelMsk = 0;
local cardnumber= "";

local function ClearCardNo()
	cardnumber = "";
end;

-- 根据头的长度判断是不是头，是的话应用程序继续放数据，不是的话，从头存放
local function ParseHead(szStr)	
	if szStr == "+" or szStr == "-" then
		return 1;
	end;
	
	return false;
end;

--[[
	解析数据，注意在使用AppendCard和ExtraProcess时加上COM.是因为这两个函数是C注册上来的
--]]
local function ParseData(szStr)
--[[	
	print(szStr);
	print("FrameID1Offset:",COM.FrameID1Offset);	
	print(COM.FrameID2Offset);
	print(COM.FrameID1Length);
	print(COM.FrameID2Length);
	print(COM.FrameID1Value);
	print(COM.FrameID2Value);
	print(COM.Field1Offset1);
	print(COM.Field1Length1);
	print(COM.Field1Title);
	print(COM.Field1Offset2);
	print(COM.Field1Length2);
	print(COM.Field2Offset1);
	print(COM.Field2Length1);
	print(COM.Field2Title);
	print(COM.Field2Offset2);
	print(COM.Field2Length2);
	print(COM.Field3Offset1);
	print(COM.Field3Length1);
	print(COM.Field3Title);
	print(COM.Field3Offset2);
	print("COM.Field3Length2:",COM.Field3Length2);
	print("Com.RecChannelMsk:",COM.RecChannelMsk);
--]]	
	local MatchFrameID = 0;
	local Value1 = "";
	local Value2 = "";
	local Field1 = "";
	local Field2 = "";
	local Field3 = "";
	local iChannel = 0;
	--首先进行FrameID1匹配，如果匹配不成功，则直接返回
	if string.len(szStr) < COM.FrameID1Offset + COM.FrameID1Length - 1 then
		print("String is not long enough while match FrameId1");
		return;
	end;

	Value1 = string.sub(szStr, COM.FrameID1Offset, COM.FrameID1Offset + COM.FrameID1Length - 1);
	print("Value1:",Value1);

	if Value1 ==  COM.FrameID1Value then
		MatchFrameId = 1;
		print("Match 1 Successfully")
	end;

	--如果FrameID1匹配失败,则进行FrameID2匹配
	if MatchFrameId == 0 then
		if string.len(szStr) < Com.FrameID2Offset + FrameID2Length -1 then
			printf("String is not long enough while match FrameId2");
			return;
		end;
		
		Value2 = string.sub(szStr, Com.FrameID2Offset, Com.FrameID2Offset + FrameID2Length - 1);
		print("Value2:",Value2);

		if Value2 == COM.FrameID2Value then
			MatchFrameId = 2;
			printf("Match 2 successfully");
		end;
	end;

	--如果两个FrameId都没有匹配上,则直接返回,匹配成功则开启卡号录像
	if MatchFrameId == 0 then
		return;
	else
		COM.ExtraProcess(Lua2C.startrec, COM.RecChannelMsk, " ");
	end;

	
	--接下来根据匹配上的FrameId进行交易信息的获取以及录像控制
	if MatchFrameId == 1 then
		Field1 = string.sub(szStr, COM.Field1Offset1, COM.Field1Offset1 + COM.Field1Length1 - 1);
		Field2 = string.sub(szStr, COM.Field2Offset1, COM.Field2Offset1 + COM.Field2Length1 - 1);
		Field3 = string.sub(szStr, COM.Field3Offset1, COM.Field3Offset1 + COM.Field3Length1 - 1);
	elseif MatchFrameId == 1 then
		Field1 = string.sub(szStr, COM.Field1Offset2, COM.Field1Offset2 + COM.Field1Length2 - 1);
		Field2 = string.sub(szStr, COM.Field2Offset2, COM.Field2Offset2 + COM.Field2Length2 - 1);
		Field3 = string.sub(szStr, COM.Field3Offset2, COM.Field3Offset2 + COM.Field3Length2 - 1);
	end;
	print("Field1:",Field1);
	print("Field2:",Field2);
	print("Field3:",Field3);

	if Field1 ~= nil then
		COM.AppendCard(Lua2C.self_definition_item1, COM.RecChannelMsk, Field1, COM.CPlusPlus);
	end;
	if Field2 ~= nil then
		COM.AppendCard(Lua2C.self_definition_item2, COM.RecChannelMsk, Field2, COM.CPlusPlus);
	end;
	if Field3 ~= nil then
		COM.AppendCard(Lua2C.self_definition_item3, COM.RecChannelMsk, Field3, COM.CPlusPlus);
	end;
	
						
end;

COM = 
{
	HeadLength	= 1,
	DataLength 	= 20,
	ParseHead	= ParseHead,	
	ParseData	= ParseData,	
	ClearCardNo	= ClearCardNo,
	AlarmTime	= 60,
	FrameID1Offset = FrameID1Offset,	
	FrameID2Offset = FrameID2Offset,
	FrameID1Length = FrameID1Length,
	FrameID2Length = FrameID2Length,
	FrameID1Value = FrameID1Value,
	FrameID2Value = FrameID2Value,
	Field1Offset1 = Field1Offset1,
	Field1Length1 = Field1Length1,
	Field1Title = Field1Title,
	Field1Offset2 = Field1Offset2,
	Field1Length2 = Field1Length2,
	Field2Offset1 = Field2Offset1,
	Field2Length1 = Field2Length1,
	Field2Title = Field2Title,
	Field2Offset2 = Field2Offset2,
	Field2Length2 = Field2Length2,
	Field3Offset1 = Field3Offset1,
	Field3Length1 = Field3Length1,
	Field3Title = Field3Title,
	Field3Offset2 = Field3Offset2,
	Field3Length2 = Field3Length2,
	RecChannelMsk = RecChannelMsk,
	CPlusPlus	= CPlusPlus
}
COM.Name = "SNIFFERPARSE_ATM";
return COM;