--   "$Id: LiveUpdate.lua 4957 2006-11-01 04:06:25Z yuan_shiyong $"
--   (c) Copyright 1992-2005, ZheJiang Dahua Information Technology Stock CO.LTD.
--                            All Rights Reserved
--
--	文 件 名： Upgrade.lua
--	描    述:  该文件用于控制升级过程
--	修改记录： 2005-12-29 王恒文 <wanghw@dhmail.com> 初始化版本
--    

require"compat-5.1"

require('zip')

local LiveUpdate = {};

-- 升级支撑环境的版本号
LiveUpdate.Version = "1.0.0";


-- 内部升级过程控制
-- params:
--        zfile: Zip文件的句柄
--     callback: 回调函数
-- return: 
--    成功返回True, 失败返回False以及失败的原因
local function doInternalUpgrade(zfile, callback)
	-- 尝试打开LUA脚本
	local f1, err = zfile:open('Install.lua')

	if( not f1) then
		return false, "invalid package";
	end
	
	local content = f1:read("*a");
	f1:close();	
	
	-- 加载脚本并运行
	local Installer;
	local f ,err= loadstring(content, "Install.lua");
	if (f) then 
		err, Installer = pcall(f);
		if(not err) then
			return false, "pcall failed:" + err;
		end
	else 
		return false, err;
	end

	Installer.ZipArchive = zfile;
	Installer.callback   = callback;
	return Installer:execute();
end


-- 对外提供的升级控制函数,基于Zip文件升级,由上层应用调用
-- params:
--     filename: Zip文件所在的路径
--     callback: 回调函数
-- return: 
--    成功返回True, 失败返回False以及失败的原因
function LiveUpdate.doFileUpgrade(filename,callback)
	assert(type(filename) == 'string');
	
	local zfile, err = zip.open(filename);

	if(not zfile) then
		return false, err;
	end;

	return 	doInternalUpgrade(zfile, callback);
end


-- 对外提供的升级控制函数,由上层应用调用
-- params:
--      bufAddr: Zip文件在内存中的起始地址
--       bufLen: Zip文件的大小
--     callback: 回调函数
-- return: 
--    成功返回True, 失败返回False以及失败的原因
function LiveUpdate.doUpgrade(bufAddr, bufLen, callback)
	assert(type(bufAddr) == 'number');
	assert(type(bufLen) == 'number');

	local zfile, err = zip.open(bufAddr, bufLen);

	if(not zfile) then
		return false, err;
	end;
	return 	doInternalUpgrade(zfile, callback);
end

return LiveUpdate;
--
-- "$Id: LiveUpdate.lua 4957 2006-11-01 04:06:25Z yuan_shiyong $"
--
