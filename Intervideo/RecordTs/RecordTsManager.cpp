#include "Intervideo/RecordTs/RecordTsManager.h"
#include "Configs/ConfigVideoColor.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigLocation.h"
#include "Net/NetApp.h"
#include "APIs/DVRDEF.H"
#include "APIs/Capture.h"
#include "Main.h"
#include <time.h>
#include "Functions/Record.h"
#include "System/Object.h"
#include <unistd.h>
#include "Devices/DevExternal.h"
#include "Functions/DriverManager.h"
#include "Net/NetConfig.h"
#include "APIs/Ide.h"
#include <sys/stat.h>
#include <stdio.h>
#include "Functions/Snap.h"
#include <dirent.h>
#include "qiniuyunInterface.h"
#include "Functions/FFFile.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <signal.h>
#include <stdlib.h>
#include "APIs/Audio.h"
#include "Configs/ConfigCamera.h"
#include "APIs/Video.h"
#include "APIs/MotionDetect.h"
#include "Configs/ConfigEvents.h"
#include "Net/Dlg/DlgNtpCli.h"
#include "System/Msg.h"
#include "Intervideo/RecordTs/TsPack.h"
#include "Mp4/Mp4Pack.h"



PATTERN_SINGLETON_IMPLEMENT(CRecordTsManager);


CRecordTsManager::CRecordTsManager():CThread("CRecordTsManager", TP_NET)
{
	m_packet_count = 0;
	m_videopacket_count = 0;
	m_audiopacket_count = 0;
	m_iNtpSuccess = 0;
	m_iFirstIFrame = 0;	
	memset(m_cTsFileName,0,sizeof(m_cTsFileName));
	memset(&start_tm,0,sizeof(struct tm));
	memset(&end_tm,0,sizeof(struct tm));
	m_TsHandle = NULL;
	m_ptkBufMain.Reset(true);

	//alarm
	m_iAlarming = 0;
	m_iAlarmFirstIFrame = 0;
	memset(m_cAlarmFileName,0,sizeof(m_cAlarmFileName));
	memset(&alarm_sttm,0,sizeof(struct tm));
	memset(&alarm_edtm,0,sizeof(struct tm));
	m_TsAlarmHandle = NULL;
	m_Mp4AlarmHandle = NULL;
	m_event_count = 0;
	m_iAlarmEnable = 0;
	m_iTsCloudEnable = 0; 
	memset(&m_cryptbuff, 0, sizeof(FrameBuffer));
	m_iSuspendMode = 1;
}
CRecordTsManager::~CRecordTsManager()
{

}

VD_INT32 CRecordTsManager::Start()
{	
//	m_iTimeCount = 0;
	CreateThread();
//	m_cRecordTimer.Start(this,(VD_TIMERPROC)&CRecordTsManager::onRecordTimer,20000,60000);
	StartVideo(0,0);
	StartAudioIn(0);
	return 0;
}
VD_INT32 CRecordTsManager::StartVideo(int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture)
	{
		_printd("Icapture::instance ok");
		pCapture->SetIFrame(StreamType);
		VD_BOOL bRet = pCapture->Start(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CRecordTsManager::OnCapture, DATA_MONITOR,StreamType);
	}else
	{
		_printd("Icapture::instance ERror");
		return -1;
	}
	return 0;
}
void CRecordTsManager::OnCapture(int iChannel, uint iStreamType, CPacket *pPacket)
{

	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.isVideo = TRUE;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = iStreamType;
		
	int iCount = g_nLogicNum;
	if (m_videopacket_count  > iCount*50) {
		
		debugf("CRecordTsManager capture list is too large discard it, size:%d, chn:%d!type:%d status[%d]\n", m_packet_count, iChannel, iStreamType,TS_Write_Status());
		return ;
	}
	
	m_mutex_list.Enter();
	m_videopacket_count++;
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_mutex_list.Leave();
}
void CRecordTsManager::OnEvent(TImage *pImage)
{
	if(m_iAlarmEnable)
	{
		m_mutex_eventlist.Enter();
		m_event_list.push_back(pImage);
		m_event_count++;
		m_iAlarming = 1;
		m_mutex_eventlist.Leave();
	}
	else
	{
		//未开通告警就推事件
		PACKET_INFO uploadinfo = {0}; 
		uploadinfo.uploadtype = 1;
		uploadinfo.pFileInfo  = NULL;
		uploadinfo.pImage = pImage;
		uploadinfo.sttimestamp = 0;
		uploadinfo.edtimestamp = 0;
		
		g_TsUpLoadManager.OnFileList(uploadinfo);
	}
}
void CRecordTsManager::onRecordTimer(uint arg)
{

}

VD_INT32 CRecordTsManager::StartAudioIn(int channel)
{
	_printd ("RecordCam AudioIn Start");
	int ret = -1;
	

	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	ret = pCapture->Start(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRecordTsManager::OnAudioData);
	if (ret != 1)
		printf("###################ERROR Audio!!!######################\n");
	return 0;
}
VD_INT32 CRecordTsManager::OnAudioData(int iChannel, CPacket *pPacket)
{
	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.isVideo = FALSE;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = 0;
		
	int iCount = g_nLogicNum;
	if (m_audiopacket_count > iCount*50) {
		debugf("CRecordTsManager audio list is too large discard it, size:%d, chn:%d!\n", m_audiopacket_count, iChannel);
		return 0;
	}

	
	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_audiopacket_count ++;
	m_mutex_list.Leave();
	return 0;
}
VD_INT32 CRecordTsManager::OnVideoData(CPacket *pPacket)
{
	if (!pPacket) 
	{
        _printd("%s:pPacket is NULL\n",__FUNCTION__);
	    return -1;
	}
	PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());

	unsigned char * pBufSend = NULL;
	unsigned char ucFrameType = PACK_TYPE_FRAME_NULL;  /* 帧类型 */
	unsigned char ucFrameFlag = 0;                     /* 帧头尾的标识 */
	int usFramePos = 0;                     /* 帧开始位置 */
	uint usCFrameLength = 0;                 /* 帧在本块中的长度 */  
	uint usFrameLength = 0;
	pBufSend = pPacket->GetBuffer();
	for ( int iIndex = 0; iIndex < FRAME_MAX_NUM; iIndex++) {        
		ucFrameType = pPacketHead->FrameInfo[iIndex].FrameType;
		ucFrameFlag = pPacketHead->FrameInfo[iIndex].FrameFlag;
		usFramePos = pPacketHead->FrameInfo[iIndex].FramePos;
		usCFrameLength = pPacketHead->FrameInfo[iIndex].DataLength;
		usFrameLength = pPacketHead->FrameInfo[iIndex].FrameLength;        
		if (ucFrameType == PACK_TYPE_FRAME_NULL) {
			break;
		} else if(PACK_TYPE_FRAME_AUDIO == ucFrameType) {
			continue;
		}
		if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
			m_ptkBufMain.Reset();
			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			/*一个完整帧*/
			if (-2 == WriteVideotoTsFile((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size())) {
				_printd("add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
			}
			m_ptkBufMain.Reset();
			continue;
		} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
			m_ptkBufMain.Reset();
			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			continue;
		} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
			/* 只包含帧尾的帧 */
			if (m_ptkBufMain.Size() > 0) {
            			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				/*组合好一个完整帧*/
				if (-2 == WriteVideotoTsFile((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size())) {
				_printd("Tail Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
			}
			} else {
				tracepoint();
				_printd("streamtype[%d] length[%lu] total[%lu] type[%d]", usCFrameLength, usFrameLength, ucFrameType);
				m_ptkBufMain.Reset();
			}
			continue;
		} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
			/* 不包含帧头和帧尾的帧 */
			if (m_ptkBufMain.Size() > 0) {
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			} else {
				tracepoint();
				//_printd("NONHT Rtsp streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
				m_ptkBufMain.Reset();
				
			}
			continue;
		}
		
    }
    return 0;
}

void CRecordTsManager::OpenTsFile(SYSTEM_TIME *start_time)
{
	
	if(0 == m_iNtpSuccess || 0 == m_iTsCloudEnable)
	{
		return; //ntp未同步则不录TS
	}
	memset(m_cTsFileName,0,sizeof(m_cTsFileName));
	if(NULL == start_time)
	{
		snprintf(m_cTsFileName,sizeof(m_cTsFileName),"/tmp/unknow.ts");
	}
	else
	{
		snprintf(m_cTsFileName,sizeof(m_cTsFileName),"/tmp/%04d%02d%02d%02d%02d%02d.ts",start_time->year,
			start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second);
	}
	m_TsHandle = TS_Open(0,m_cTsFileName);

}
void CRecordTsManager::OpenAlarmTsFile(SYSTEM_TIME *start_time)
{
	if((0 == m_iNtpSuccess) || (0 == m_iAlarmEnable))
	{
		return; //ntp未同步则不录TS
	}
	memset(m_cAlarmFileName,0,sizeof(m_cAlarmFileName));
	if(NULL == start_time)
	{
		snprintf(m_cAlarmFileName,sizeof(m_cAlarmFileName),"/tmp/A_unknow.ts");
	}
	else
	{
		snprintf(m_cAlarmFileName,sizeof(m_cAlarmFileName),"/tmp/A_%04d%02d%02d%02d%02d%02d.ts",start_time->year,
			start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second);
	}
	m_TsAlarmHandle = TS_Open(1,m_cAlarmFileName);
}
void CRecordTsManager::OpenAlarmMp4File(SYSTEM_TIME *start_time)
{
	if((0 == m_iNtpSuccess) || (0 == m_iAlarmEnable))
	{
		return; //ntp未同步则不录mp4
	}
	memset(m_cAlarmFileName,0,sizeof(m_cAlarmFileName));
	if(NULL == start_time)
	{
		snprintf(m_cAlarmFileName,sizeof(m_cAlarmFileName),"/tmp/A_unknow.mp4");
	}
	else
	{
		snprintf(m_cAlarmFileName,sizeof(m_cAlarmFileName),"/tmp/A_%04d%02d%02d%02d%02d%02d.mp4",start_time->year,
			start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second);
	}
	m_Mp4AlarmHandle = Mp4_Open(m_cAlarmFileName,15,15);
}

void CRecordTsManager::CloseAlarmTsFile(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec)
{

	time_t t_start =  mktime(&alarm_sttm);
	time_t t_end   =  mktime(&alarm_edtm);
	int  timezonesec = 0;  //时区
	
	TS_Close(1,m_TsAlarmHandle);
	m_TsAlarmHandle = NULL;
	m_iAlarmFirstIFrame = 0;
	char tsrecname[512] = {0};		

	if(abs(t_end-t_start) > 14)
	{
		t_end = t_start + totalsec;
	}
//	snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.ts",start_time->year,
//			start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second,totalsec);
	if(start_time && end_time)
	{		
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.ts",start_time->year,
				start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second,totalsec);
	}
	else
	{
		SYSTEM_TIME start_time1 = {0};
		start_time1.year = alarm_sttm.tm_year + 1900;
		start_time1.month = alarm_sttm.tm_mon + 1;
		start_time1.day	= alarm_sttm.tm_mday;
		start_time1.hour = alarm_sttm.tm_hour;
		start_time1.minute = alarm_sttm.tm_min;
		start_time1.second = alarm_sttm.tm_sec;
		if(t_start == t_end)
		{
			remove(m_cAlarmFileName);
			_printd("remove:%s",m_cAlarmFileName);
			return ;
		}
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.ts",start_time1.year,
				start_time1.month,start_time1.day,start_time1.hour,start_time1.minute,start_time1.second,abs(t_end-t_start));


	}

	rename(m_cAlarmFileName,tsrecname);

	_printd("record:%s t_start:%ld t_end:%ld",tsrecname, t_start, t_end);
	struct stat filestat;
    stat(tsrecname, &filestat);

	if(0 == filestat.st_size)
	{
		//异常文件则返回
		memcpy(&alarm_edtm,&alarm_sttm,sizeof(struct tm));
		remove(tsrecname);
		_printd("remove:%s",tsrecname);
		return ;
	}
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();
	timezonesec = (cfgnetntp.TimeZone*3600)/10;
	//计算文件大小
	//加入发送队列 开始结束时间
	//.....

	if(0 == m_iAlarming)
	{
		remove(tsrecname);
	}
	else
	{
		TS_FILE_INFO *pFileInfo  = new TS_FILE_INFO;
		if(pFileInfo)
		{
			pFileInfo->count = 0;  //初始化为零
		}
		vector<PACKET_INFO> uploadinfovect;
		TImage *pImage = NULL; 

		m_mutex_eventlist.Enter();	
		for(int i = 0; i < m_event_count; i++)
		{
			EVENT_LIST::iterator it = m_event_list.begin();
			pImage = *it;
			m_event_list.pop_front();
			PACKET_INFO uploadinfo = {0}; 
			uploadinfo.uploadtype = 1;
			uploadinfo.pFileInfo  = pFileInfo;
			if(uploadinfo.pFileInfo)
			{
				uploadinfo.pFileInfo->filename = tsrecname;
				uploadinfo.pFileInfo->filesize = filestat.st_size;
				uploadinfo.pFileInfo->count ++;
			}
			uploadinfo.pImage = pImage;
			uploadinfo.sttimestamp = t_start - timezonesec;
			uploadinfo.edtimestamp = t_end - timezonesec;
			uploadinfovect.push_back(uploadinfo);
		}
		m_event_count = 0;
		m_iAlarming = 0;
		m_mutex_eventlist.Leave();

		g_TsUpLoadManager.OnFileList(uploadinfovect);
	}

}
void CRecordTsManager::CloseAlarmMp4File(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec)
{

	time_t t_start =  mktime(&alarm_sttm);
	time_t t_end   =  mktime(&alarm_edtm);
	int  timezonesec = 0;  //时区
	
	Mp4_Close(m_Mp4AlarmHandle);
	m_Mp4AlarmHandle = NULL;
	m_iAlarmFirstIFrame = 0;
	char tsrecname[512] = {0};		

	if(abs(t_end-t_start) > 14)
	{
		t_end = t_start + totalsec;
	}
//	snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.ts",start_time->year,
//			start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second,totalsec);
	if(start_time && end_time)
	{		
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.mp4",start_time->year,
				start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second,totalsec);
	}
	else
	{
		SYSTEM_TIME start_time1 = {0};
		start_time1.year = alarm_sttm.tm_year + 1900;
		start_time1.month = alarm_sttm.tm_mon + 1;
		start_time1.day	= alarm_sttm.tm_mday;
		start_time1.hour = alarm_sttm.tm_hour;
		start_time1.minute = alarm_sttm.tm_min;
		start_time1.second = alarm_sttm.tm_sec;
		if(t_start == t_end)
		{
			remove(m_cAlarmFileName);
			_printd("remove:%s",m_cAlarmFileName);
			return ;
		}
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/A_%04d%02d%02d%02d%02d%02d_%04d.ts",start_time1.year,
				start_time1.month,start_time1.day,start_time1.hour,start_time1.minute,start_time1.second,abs(t_end-t_start));


	}

	rename(m_cAlarmFileName,tsrecname);

	_printd("record:%s t_start:%ld t_end:%ld",tsrecname, t_start, t_end);
	struct stat filestat;
    stat(tsrecname, &filestat);

	if(0 == filestat.st_size)
	{
		//
		memcpy(&alarm_edtm,&alarm_sttm,sizeof(struct tm));
		remove(tsrecname);
		_printd("remove:%s",tsrecname);
		return ;
	}
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();
	timezonesec = (cfgnetntp.TimeZone*3600)/10;
	//计算文件大小
	//加入发送队列 开始结束时间
	//.....

	if(0 == m_iAlarming)
	{
		remove(tsrecname);
	}
	else
	{
		TS_FILE_INFO *pFileInfo  = new TS_FILE_INFO;
		if(pFileInfo)
		{
			pFileInfo->count = 0;  //初始化为零
		}
		vector<PACKET_INFO> uploadinfovect;
		TImage *pImage = NULL; 

		m_mutex_eventlist.Enter();	
		for(int i = 0; i < m_event_count; i++)
		{
			EVENT_LIST::iterator it = m_event_list.begin();
			pImage = *it;
			m_event_list.pop_front();
			PACKET_INFO uploadinfo = {0}; 
			uploadinfo.uploadtype = 1;
			uploadinfo.pFileInfo  = pFileInfo;
			if(uploadinfo.pFileInfo)
			{
				uploadinfo.pFileInfo->filename = tsrecname;
				uploadinfo.pFileInfo->filesize = filestat.st_size;
				uploadinfo.pFileInfo->count ++;
			}
			uploadinfo.pImage = pImage;
			uploadinfo.sttimestamp = t_start - timezonesec;
			uploadinfo.edtimestamp = t_end - timezonesec;
			uploadinfovect.push_back(uploadinfo);
		}
		m_event_count = 0;
		m_iAlarming = 0;
		m_mutex_eventlist.Leave();

		g_TsUpLoadManager.OnFileList(uploadinfovect);
	}

}

void CRecordTsManager::SetEnable(int type, int Enable)
{
	if(type)
	{
		m_iAlarmEnable = Enable;
	}
	else
	{
		m_iTsCloudEnable = Enable;
	}
}
void CRecordTsManager::SetSuspendMode(int mode) 
{
	m_iSuspendMode = mode;
	//_printd("m_iSuspendMode[%d]",m_iSuspendMode);
}

void CRecordTsManager::CloseTsFile(SYSTEM_TIME *start_time,SYSTEM_TIME *end_time,int totalsec)
{
	PACKET_INFO uploadinfo = {0}; 
	time_t t_start =  mktime(&start_tm);
	time_t t_end   =  mktime(&end_tm);
	int  timezonesec = 0;  //时区
	if(NULL == m_TsHandle)
	{
		return ;
	}
	if(abs(t_end-t_start) > 20)
	{
		t_end = t_start + totalsec;
	}
	TS_Close(0,m_TsHandle);
	m_TsHandle = NULL;
	m_iFirstIFrame = 0;
	char tsrecname[512] = {0};		
	if(start_time && end_time)
	{
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/%04d%02d%02d%02d%02d%02d_%04d.ts",start_time->year,
				start_time->month,start_time->day,start_time->hour,start_time->minute,start_time->second,totalsec);
		
	}
	else
	{
		//
		SYSTEM_TIME start_time1 = {0};
		start_time1.year = start_tm.tm_year + 1900;
		start_time1.month = start_tm.tm_mon + 1;
		start_time1.day	= start_tm.tm_mday;
		start_time1.hour = start_tm.tm_hour;
		start_time1.minute = start_tm.tm_min;
		start_time1.second = start_tm.tm_sec;
		if(t_start == t_end)
		{
			remove(m_cTsFileName);
			_printd("remove:%s",m_cTsFileName);
			return ;
		}
		snprintf(tsrecname,sizeof(tsrecname),"/tmp/%04d%02d%02d%02d%02d%02d_%04d.ts",start_time1.year,
				start_time1.month,start_time1.day,start_time1.hour,start_time1.minute,start_time1.second,abs(t_end-t_start));


	}
	
	rename(m_cTsFileName,tsrecname);

	_printd("record:%s t_start:%ld t_end:%ld",tsrecname, t_start, t_end);
	struct stat filestat;
    stat(tsrecname, &filestat);
	if(0 == filestat.st_size)
	{
		//
		memcpy(&end_tm,&start_tm,sizeof(struct tm));
		remove(tsrecname);
		_printd("remove:%s",tsrecname);
		return ;
	}

	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();
	timezonesec = (cfgnetntp.TimeZone*3600)/10;
		
	uploadinfo.uploadtype = 0;
	uploadinfo.pFileInfo  = new TS_FILE_INFO;
	if(uploadinfo.pFileInfo)
	{
		uploadinfo.pFileInfo->filename = tsrecname;
		uploadinfo.pFileInfo->filesize = filestat.st_size;
		uploadinfo.pFileInfo->count = 1;
	}
	uploadinfo.pImage = NULL;
	uploadinfo.sttimestamp = t_start - timezonesec;
	uploadinfo.edtimestamp = t_end - timezonesec;
	
	g_TsUpLoadManager.OnFileList(uploadinfo);

	//计算文件大小
	//加入发送队列 开始结束时间
	//.....
}

void CRecordTsManager::WritePacket()
{
	if(NULL == m_TsHandle)
	{	
		OpenTsFile(NULL);
	}
#if 0
	if(NULL == m_TsAlarmHandle)
	{
		OpenAlarmTsFile(NULL);
	}
#else
	if(NULL == m_Mp4AlarmHandle)
	{
		OpenAlarmMp4File(NULL);
	}
#endif

	do{
		CPacket* pPacket = NULL;
		uint dwStreamType = 0;
		int chn = 0;
		int isVideo = 0;

		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		chn = it->chn;
		isVideo = it->isVideo;
		m_packet_list.pop_front();
		m_packet_count--;
		if(isVideo)
		{
			m_videopacket_count--;
		}
		else
		{
			m_audiopacket_count--;
		}
		m_mutex_list.Leave();	
		WriteTsFile(isVideo, pPacket);
		pPacket->Release();
			
	}while(m_packet_count > 0);
}
void CRecordTsManager::WriteTsFile(int isVideo,CPacket *pPacket)
{
#if 0
	if(NULL == m_TsHandle && NULL == m_TsAlarmHandle)
#else
	if(NULL == m_TsHandle && NULL == m_Mp4AlarmHandle)
#endif
	{
		return;
	}
	
	if(isVideo) //video
	{
		OnVideoData(pPacket);	
	}
	else//audio
	{
		WriteAduiotoTsFile(pPacket);
	}
}
VD_INT32 CRecordTsManager::WriteVideotoAlarmTsFile(uchar *pData, unsigned int iLen)
{

	if(NULL == m_TsAlarmHandle)
	{
		return 0;
	}
	
	unsigned char frame_type[4]={0};
	long long FrameTimeStamp = 0;
	TSFrameType FrameType = TS_VIDEO_P_FRAME;
	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp
	memcpy(frame_type, pData,4);
	if(frame_type[3] == 0xfd || frame_type[3] == 0xad)
	{
		//I Frame
		FrameType =  TS_VIDEO_I_FRAME;
		if(0 == m_iAlarmFirstIFrame)
		{
			unsigned int datatime = 0;
			
			m_iAlarmFirstIFrame = 1;
			//第一帧记录开始时间
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);
			
			alarm_sttm.tm_year 	= ((datatime>>26)&0x3f) + 100;
			alarm_sttm.tm_mon 	= ((datatime>>22)&0x0f) - 1; //tm结构为0--11
			alarm_sttm.tm_mday	= (datatime>>17)&0x1f;
			alarm_sttm.tm_hour	= (datatime>>12)&0x1f;
			alarm_sttm.tm_min		= (datatime>>6)&0x3f;
			alarm_sttm.tm_sec		= datatime&0x3f;
			#if 1
			_printd("record alarm_sttm time:%04d-%02d-%02d %02d:%02d:%02d\n",
				alarm_sttm.tm_year+1900,alarm_sttm.tm_mon+1,alarm_sttm.tm_mday,
				alarm_sttm.tm_hour,alarm_sttm.tm_min,alarm_sttm.tm_sec);
		    #endif
			if(0 == alarm_edtm.tm_year && 0 == alarm_edtm.tm_mon && 0 == alarm_edtm.tm_mday)
			{
				memcpy(&alarm_edtm,&alarm_sttm,sizeof(struct tm));
			}
		}
		else
		{
			unsigned int datatime = 0;
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);

			//bsp 给的时间是从2000开始
			alarm_edtm.tm_year 	= ((datatime>>26)&0x3f) +100;
			alarm_edtm.tm_mon 	= ((datatime>>22)&0x0f)- 1; //tm结构为0--11
			alarm_edtm.tm_mday	= (datatime>>17)&0x1f;
			alarm_edtm.tm_hour	= (datatime>>12)&0x1f;
			alarm_edtm.tm_min		= (datatime>>6)&0x3f;
			alarm_edtm.tm_sec		= datatime&0x3f;
		}
		
		 _printd("startalarmtime:%04d-%02d-%02d %02d:%02d:%02d", alarm_sttm.tm_year + 1900,alarm_sttm.tm_mon+1,alarm_sttm.tm_mday,
		 alarm_sttm.tm_hour,alarm_sttm.tm_min,alarm_sttm.tm_sec);
		 _printd("end__alarmtime:%04d-%02d-%02d %02d:%02d:%02d", alarm_edtm.tm_year + 1900,alarm_edtm.tm_mon+1,alarm_edtm.tm_mday,
		 alarm_edtm.tm_hour,alarm_edtm.tm_min,alarm_edtm.tm_sec);
		 
		 time_t t_start =  mktime(&alarm_sttm);
		 time_t t_end   =  mktime(&alarm_edtm);

	 	SYSTEM_TIME start_time = {0};
		SYSTEM_TIME end_time = {0};
		start_time.year = alarm_sttm.tm_year + 1900;
		start_time.month = alarm_sttm.tm_mon + 1;
		start_time.day	= alarm_sttm.tm_mday;
		start_time.hour = alarm_sttm.tm_hour;
		start_time.minute = alarm_sttm.tm_min;
		start_time.second = alarm_sttm.tm_sec;
		
		end_time.year = alarm_edtm.tm_year + 1900; 
		end_time.month = alarm_edtm.tm_mon + 1;
		end_time.day	= alarm_edtm.tm_mday;
		end_time.hour = alarm_edtm.tm_hour;
		end_time.minute = alarm_edtm.tm_min;
		end_time.second = alarm_edtm.tm_sec;
		
		 _printd("t_start[%ld] t_end[%ld]\n", t_start,t_end);
		 //无告警时直接删除再生成下一个TS文件
		 uint64_t timeduration = TS_TimeDuration(m_TsAlarmHandle);
		 if(0 == m_iAlarming && 0 != abs(t_end - t_start))
		 {
			CloseAlarmTsFile(&start_time,&end_time,abs(t_end - t_start)); 	
			OpenAlarmTsFile(&end_time);	
			if(NULL != m_TsAlarmHandle)
			{
				//下一个文件的开始时间
				memcpy(&alarm_sttm, &alarm_edtm,sizeof(struct tm));
				m_iAlarmFirstIFrame = 1;  //当前帧是I帧则写入
			}
		 }
		 else if(timeduration > 9) //12秒 4秒一个I帧
		 {
		 	CloseAlarmTsFile(&start_time,&end_time,timeduration); 	
			OpenAlarmTsFile(&end_time);			
			if(NULL != m_TsAlarmHandle)
			{
				memcpy(&alarm_sttm, &alarm_edtm,sizeof(struct tm));
				m_iAlarmFirstIFrame = 1;  //当前帧是I帧则写入
			}	
		 }
	}
	
	if(m_iAlarmFirstIFrame)
	{	
		if(TS_VIDEO_I_FRAME == FrameType)
		{
			//AesEncryptH264IFrame(pData + 20,iLen - 20 -sizeof(long long) - 5,&m_cryptbuff);
			if(m_cryptbuff.iDataSize)
			{
				TS_Write(1,m_TsAlarmHandle, m_cryptbuff.pBuf, m_cryptbuff.iDataSize, FrameTimeStamp/1000, FrameType);
			}
			else
			{
				TS_Write(1,m_TsAlarmHandle, pData + 20, iLen - 20 -sizeof(long long) - 5 , FrameTimeStamp/1000, FrameType);
			}
		}
		else
		{
			TS_Write(1,m_TsAlarmHandle, pData + 8, iLen - 8 -sizeof(long long) - 5, FrameTimeStamp/1000, FrameType);
		}
	}
	return 0;
}
VD_INT32 CRecordTsManager::WriteVideotoAlarmMp4File(uchar *pData, unsigned int iLen)
{

	if(NULL == m_Mp4AlarmHandle)
	{
		return 0;
	}
	
	unsigned char frame_type[4]={0};
	long long FrameTimeStamp = 0;
	TSFrameType FrameType = TS_VIDEO_P_FRAME;
	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp
	memcpy(frame_type, pData,4);
	if(frame_type[3] == 0xfd || frame_type[3] == 0xad)
	{
		//I Frame
		FrameType =  TS_VIDEO_I_FRAME;
		if(0 == m_iAlarmFirstIFrame)
		{
			unsigned int datatime = 0;
			
			m_iAlarmFirstIFrame = 1;
			//第一帧记录开始时间
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);
			
			alarm_sttm.tm_year 	= ((datatime>>26)&0x3f) + 100;
			alarm_sttm.tm_mon 	= ((datatime>>22)&0x0f) - 1; //tm结构为0--11
			alarm_sttm.tm_mday	= (datatime>>17)&0x1f;
			alarm_sttm.tm_hour	= (datatime>>12)&0x1f;
			alarm_sttm.tm_min		= (datatime>>6)&0x3f;
			alarm_sttm.tm_sec		= datatime&0x3f;
			#if 1
			_printd("record alarm_sttm time:%04d-%02d-%02d %02d:%02d:%02d\n",
				alarm_sttm.tm_year+1900,alarm_sttm.tm_mon+1,alarm_sttm.tm_mday,
				alarm_sttm.tm_hour,alarm_sttm.tm_min,alarm_sttm.tm_sec);
		    #endif
			if(0 == alarm_edtm.tm_year && 0 == alarm_edtm.tm_mon && 0 == alarm_edtm.tm_mday)
			{
				memcpy(&alarm_edtm,&alarm_sttm,sizeof(struct tm));
			}
		}
		else
		{
			unsigned int datatime = 0;
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);

			//bsp 给的时间是从2000开始
			alarm_edtm.tm_year 	= ((datatime>>26)&0x3f) +100;
			alarm_edtm.tm_mon 	= ((datatime>>22)&0x0f)- 1; //tm结构为0--11
			alarm_edtm.tm_mday	= (datatime>>17)&0x1f;
			alarm_edtm.tm_hour	= (datatime>>12)&0x1f;
			alarm_edtm.tm_min		= (datatime>>6)&0x3f;
			alarm_edtm.tm_sec		= datatime&0x3f;
		}
		
		 _printd("startalarmtime:%04d-%02d-%02d %02d:%02d:%02d", alarm_sttm.tm_year + 1900,alarm_sttm.tm_mon+1,alarm_sttm.tm_mday,
		 alarm_sttm.tm_hour,alarm_sttm.tm_min,alarm_sttm.tm_sec);
		 _printd("end__alarmtime:%04d-%02d-%02d %02d:%02d:%02d", alarm_edtm.tm_year + 1900,alarm_edtm.tm_mon+1,alarm_edtm.tm_mday,
		 alarm_edtm.tm_hour,alarm_edtm.tm_min,alarm_edtm.tm_sec);
		 
		 time_t t_start =  mktime(&alarm_sttm);
		 time_t t_end   =  mktime(&alarm_edtm);

	 	SYSTEM_TIME start_time = {0};
		SYSTEM_TIME end_time = {0};
		start_time.year = alarm_sttm.tm_year + 1900;
		start_time.month = alarm_sttm.tm_mon + 1;
		start_time.day	= alarm_sttm.tm_mday;
		start_time.hour = alarm_sttm.tm_hour;
		start_time.minute = alarm_sttm.tm_min;
		start_time.second = alarm_sttm.tm_sec;
		
		end_time.year = alarm_edtm.tm_year + 1900; 
		end_time.month = alarm_edtm.tm_mon + 1;
		end_time.day	= alarm_edtm.tm_mday;
		end_time.hour = alarm_edtm.tm_hour;
		end_time.minute = alarm_edtm.tm_min;
		end_time.second = alarm_edtm.tm_sec;
		
		 _printd("t_start[%ld] t_end[%ld]\n", t_start,t_end);
		 //无告警时直接删除再生成下一个TS文件
		 uint64_t timeduration = Mp4_GetCurrentPTime(m_Mp4AlarmHandle);
		 if(0 == m_iAlarming && 0 != abs(t_end - t_start))
		 {
			CloseAlarmMp4File(&start_time,&end_time,abs(t_end - t_start)); 	
			OpenAlarmMp4File(&end_time);	
			if(NULL != m_Mp4AlarmHandle)
			{
				//下一个文件的开始时间
				memcpy(&alarm_sttm, &alarm_edtm,sizeof(struct tm));
				m_iAlarmFirstIFrame = 1;  //当前帧是I帧则写入
			}
		 }
		 else if(timeduration > 9 && 0 != abs(t_end - t_start)) //12秒 4秒一个I帧
		 {
		 	CloseAlarmMp4File(&start_time,&end_time,timeduration); 	
			OpenAlarmMp4File(&end_time);			
			if(NULL != m_Mp4AlarmHandle)
			{
				memcpy(&alarm_sttm, &alarm_edtm,sizeof(struct tm));
				m_iAlarmFirstIFrame = 1;  //当前帧是I帧则写入
			}	
		 }
	}
	
	if(m_iAlarmFirstIFrame)
	{	
		if(TS_VIDEO_I_FRAME == FrameType)
		{
			//AesEncryptH264IFrame(pData + 20,iLen - 20 -sizeof(long long) - 5,&m_cryptbuff);
			if(m_cryptbuff.iDataSize)
			{
				Mp4_Write((unsigned char*)m_cryptbuff.pBuf, m_cryptbuff.iDataSize,0,0,VIDEO_I_FRAME,m_Mp4AlarmHandle);
			}
			else
			{
				Mp4_Write((unsigned char*)pData + 20, iLen - 20 -sizeof(long long) - 5 ,0,0,VIDEO_I_FRAME,m_Mp4AlarmHandle);
			}
		}
		else
		{
			Mp4_Write((unsigned char*)pData + 8, iLen - 8 -sizeof(long long) - 5,0,0,VIDEO_P_FRAME,m_Mp4AlarmHandle);
		}
	}
	return 0;
}

static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) 
	{  
        fwrite(ptr, nmemb, size,  fp); 
		fclose(fp);
    }  
    printf("download size[%d]\n",len);      
    return written;  
}  

VD_INT32 CRecordTsManager::WriteVideotoTsFile(uchar *pData, unsigned int iLen)
{
	//alarm
	//WriteVideotoAlarmTsFile(pData,iLen);
	WriteVideotoAlarmMp4File(pData,iLen);

	if(NULL == m_TsHandle)
	{
	
		return 0;
	}

	unsigned char frame_type[4]={0};
	uint64_t FrameTimeStamp = 0;
	TSFrameType FrameType = TS_VIDEO_P_FRAME;
	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp
	memcpy(frame_type, pData,4);
	if(frame_type[3] == 0xfd || frame_type[3] == 0xad)
	{
		//I Frame
		FrameType =  TS_VIDEO_I_FRAME;
		if(0 == m_iFirstIFrame)
		{
			unsigned int datatime = 0;
			
			m_iFirstIFrame = 1;
			//第一帧记录开始时间
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);
			
			start_tm.tm_year 	= ((datatime>>26)&0x3f) + 100;
			start_tm.tm_mon 	= ((datatime>>22)&0x0f) - 1; //tm结构为0--11
			start_tm.tm_mday	= (datatime>>17)&0x1f;
			start_tm.tm_hour	= (datatime>>12)&0x1f;
			start_tm.tm_min		= (datatime>>6)&0x3f;
			start_tm.tm_sec		= datatime&0x3f;
			#if 1
			_printd("record start time:%04d-%02d-%02d %02d:%02d:%02d\n",
				start_tm.tm_year,start_tm.tm_mon,
				start_tm.tm_mday,
				start_tm.tm_hour,start_tm.tm_min,
				start_tm.tm_sec);
		    #endif
			if(0 == end_tm.tm_year && 0 == end_tm.tm_mon && 0 == end_tm.tm_mday)
			{
				memcpy(&end_tm,&start_tm,sizeof(struct tm));
			}
		}
		else
		{
			unsigned int datatime = 0;
			if(frame_type[3] == 0xad)
				memcpy(&datatime,pData + 12,4);
			else
				memcpy(&datatime,pData + 8,4);

			//bsp 给的时间是从2000开始
			end_tm.tm_year 	= ((datatime>>26)&0x3f) +100;
			end_tm.tm_mon 	= ((datatime>>22)&0x0f)- 1; //tm结构为0--11
			end_tm.tm_mday	= (datatime>>17)&0x1f;
			end_tm.tm_hour	= (datatime>>12)&0x1f;
			end_tm.tm_min		= (datatime>>6)&0x3f;
			end_tm.tm_sec		= datatime&0x3f;
		}
		
		 _printd("starttime:%04d-%02d-%02d %02d:%02d:%02d", start_tm.tm_year,start_tm.tm_mon,start_tm.tm_mday,
		 start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec);
		 _printd("end_tmtime:%04d-%02d-%02d %02d:%02d:%02d", end_tm.tm_year,end_tm.tm_mon,end_tm.tm_mday,
		 end_tm.tm_hour,end_tm.tm_min,end_tm.tm_sec);
		 
		 time_t t_start =  mktime(&start_tm);
		 time_t t_end   =  mktime(&end_tm);
		 
		 _printd("t_start[%ld] t_end[%ld]\n", t_start,t_end);
		 uint64_t timeduration = TS_TimeDuration(m_TsHandle);
		 if(timeduration >= 15) //12秒 4秒一个I帧
		 {
		 	SYSTEM_TIME start_time = {0};
			SYSTEM_TIME end_time = {0};
			start_time.year = start_tm.tm_year + 1900;
			start_time.month = start_tm.tm_mon + 1;
			start_time.day	= start_tm.tm_mday;
			start_time.hour = start_tm.tm_hour;
			start_time.minute = start_tm.tm_min;
			start_time.second = start_tm.tm_sec;
			
			end_time.year = end_tm.tm_year + 1900; 
			end_time.month = end_tm.tm_mon + 1;
			end_time.day	= end_tm.tm_mday;
			end_time.hour = end_tm.tm_hour;
			end_time.minute = end_tm.tm_min;
			end_time.second = end_tm.tm_sec;
			
		 	CloseTsFile(&start_time,&end_time,timeduration); 	
			OpenTsFile(&end_time);			
			if(NULL != m_TsHandle)
			{
				//下一个文件的开始时间
				memcpy(&start_tm, &end_tm,sizeof(struct tm));
				m_iFirstIFrame = 1;  //当前帧是I帧则写入
			}
			
		 }
	}
	
	if(m_iFirstIFrame)
	{	
	//	_printd("video[%llu]",FrameTimeStamp/1000);
		if(TS_VIDEO_I_FRAME == FrameType)
		{
			//AesEncryptH264IFrame(pData + 20,iLen - 20 -sizeof(long long) - 5,&m_cryptbuff);
			//writefile_callback(pData + 20,  iLen - 20 -sizeof(long long) -5 ,1,(void*)"/opt/testIP001.h264");
		//	writefile_callback(m_cryptbuff.pBuf, m_cryptbuff.iDataSize,1,(void*)"/opt/testcr001.h264");
		//	writefile_callback(pData,  iLen ,1,(void*)"/opt/test002.h264");
			if(m_cryptbuff.iDataSize)
			{
				TS_Write(0,m_TsHandle, m_cryptbuff.pBuf, m_cryptbuff.iDataSize, FrameTimeStamp/1000, FrameType);
			}
			else
			{
				TS_Write(0,m_TsHandle, pData + 20, iLen - 20 -sizeof(long long)-5, FrameTimeStamp/1000, FrameType);
			}
			//TS_Write(0,m_TsHandle, pData + 20, iLen - 20 -sizeof(long long), FrameTimeStamp/1000, FrameType);
		}
		else
		{
			TS_Write(0, m_TsHandle, pData + 8, iLen - 8 -sizeof(long long)-5, FrameTimeStamp/1000, FrameType);
			//writefile_callback(pData + 8, iLen - 8 -sizeof(long long)-5,1,(void*)"/opt/testIP001.h264");
			//writefile_callback(pData + 8, iLen - 8 -sizeof(long long)-5,1,(void*)"/opt/testcr001.h264");
		}
	}
	return 0;
}
void CRecordTsManager::WriteAduiotoTsFile(CPacket *pPacket)
{

	long long FrameTimeStamp = 0;
	unsigned int len = 0;
	memcpy(&FrameTimeStamp,pPacket->GetBuffer() + pPacket->GetLength() - sizeof(long long), sizeof(long long));
	len = pPacket->GetLength() - 8 - sizeof(unsigned int) - sizeof(long long);
//	_printd("audio[%llu]",FrameTimeStamp/1000);
	if(m_iFirstIFrame)
	{
		TS_Write(0, m_TsHandle, pPacket->GetBuffer() + 8 + sizeof(unsigned int), len, FrameTimeStamp/1000, TS_AUDIO_FRAME);
	}
	if(m_iAlarmFirstIFrame)
	{
		Mp4_Write((unsigned char*)pPacket->GetBuffer() + 8 + sizeof(unsigned int), len,0,0,AUDIO_FRAME,m_Mp4AlarmHandle);
		//TS_Write(1, m_TsAlarmHandle, pPacket->GetBuffer() + 8 + sizeof(unsigned int), len, FrameTimeStamp/1000, TS_AUDIO_FRAME);
	}
}

void CRecordTsManager::ThreadProc()
{
	int timecount = 0;
	while (m_bLoop) 
	{
		if(m_packet_count > 0)
		{
			WritePacket();
			timecount = 0;
		}
		else
		{
			if(0 == m_iSuspendMode)
			{
				//_printd("m_iSuspendMode[%d]",m_iSuspendMode);
				if(timecount++ > 400) //4秒			
				{
					#if 0
					if(m_TsAlarmHandle)
					{
						CloseAlarmTsFile(NULL, NULL, 0);
					}
					#else
					if(m_Mp4AlarmHandle)
					{
						CloseAlarmMp4File(NULL, NULL, 0);
					}
					#endif
					if(m_TsHandle)
					{
						CloseTsFile(NULL, NULL, 0);
					}
					timecount = 0;
				}
			}
			usleep(10*1000);
		}
	}
}

VD_INT32 CRecordTsManager::Stop()
{

	return 0;
}

