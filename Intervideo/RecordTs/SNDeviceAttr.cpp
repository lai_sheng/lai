#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <curl/curl.h>
#include "System/File.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/RecordTs/SNDeviceAttr.h"
#include "Intervideo/LiveCam/RealTimeCamMemory.h"
#include "Intervideo/RecordTs/RecordTsUpLoad.h"


PATTERN_SINGLETON_IMPLEMENT(CSNDeviceAttr);

CSNDeviceAttr::CSNDeviceAttr():CThread("CSNDeviceAttr", TP_NET)
{
	_printd("CSNDeviceAttr");
	m_iFlag = 0;
	m_iNtpSuccess = 0;
}

CSNDeviceAttr::~CSNDeviceAttr()
{
	_printd("~CSNDeviceAttr");
}




VD_INT32 CSNDeviceAttr::Start()
{
	int RebootFlag = 0;
	if(0)
	{
		m_apiUrl = "http://shreportsit.suning.com/shreport-web/shvoice/boxReport.htm";
		m_otaUrl = "http://shreportsit.suning.com/shreport-web/shvoice/otaLog.htm";
	}
	else
	{		
		m_apiUrl = "http://shreport.suning.com/shreport-web/shvoice/boxReport.htm";
		m_otaUrl = "http://shreport.suning.com/shreport-web/shvoice/otaLog.htm";
	}
#if 0	
	m_cTimer.Start(this,(VD_TIMERPROC)&CSNDeviceAttr::onTimer,30000,30000);
#endif
	_printd ("CSNDeviceAttr Init Over");
		
	g_Cmos.Read(CMOS_REBOOTFLAG,&RebootFlag,4);
	_printd("RebootFlag[%d]",RebootFlag);
	m_iRebootFlag = RebootFlag; //1是非正常重启 2:是升级
	if(1 != RebootFlag)
	{		
		RebootFlag = 1;
		g_Cmos.Write(CMOS_REBOOTFLAG,&RebootFlag,4);
	}
	CreateThread();
	return 0;
}
VD_INT32 CSNDeviceAttr::Stop()
{
	_printd("CSNDeviceAttr :: Stop ");
	return 0;
}

void CSNDeviceAttr::ThreadProc()
{
	while (m_bLoop) 
	{
		if(m_iNtpSuccess && ((0 == m_iFlag) || ++m_iTimeCount >= 60))
		{		
			if(0 != SendDeviceAttr())
			{
				m_iTimeCount = 59;
			}
			else
			{
				m_iTimeCount = 0;
			}
		}

		if(2 == m_iRebootFlag)
		{
			if(0 == SendOtaResult())
			{
				m_iRebootFlag = 0; //once
			}
		}
		sleep(1);
	}
}

void CSNDeviceAttr::onTimer(uint arg)
{
#if 0
	//由于定时器会出问题所以用线程代替
	if(m_iNtpSuccess && ((0 == m_iFlag) || ++m_iTimeCount >= 60))
	{		
		if(0 != SendDeviceAttr())
		{
			m_iTimeCount = 59;
		}
		else
		{
			m_iTimeCount = 0;
		}
	}

	if(2 == m_iRebootFlag)
	{
		if(0 == SendOtaResult())
		{
			m_iRebootFlag = 0; //once
		}
	}
#endif
}
void  CSNDeviceAttr::SetNtpSuccess()
{
	m_iNtpSuccess = 1;
}

VD_INT32 CSNDeviceAttr::PackDeviceAttrData(string & result)
{
	cJSON *root = NULL;
	char *json_data = NULL;

	char systemversion[64] = {'\0'};
	char macaddr[64] = {'\0'};
	char ipdate[64] = {'\0'};
	char wifisignal[64] = {'\0'};
	int  wifivalue = 0;
	string cpuUsedAvg;
	string memUsedAvg;
	
	string curtime;
	char  DeviceId[16]  = {'\0'};

	if(m_sysversion.empty())
	{
		if(0 == GetSystemVer(systemversion,sizeof(systemversion)))
		{
			m_sysversion = systemversion;
		}
	}
	
	if(m_uid.empty())
	{
		int ret = 0;
		char uuid[32] ={'\0'};
		if(0 == GetUuid(uuid))
		{
			m_uid = uuid;
		}
	}
	
	if(m_position.empty())
	{
		GetDeviceWlanPostion(m_position);
	}
	if(m_mac.empty())
	{
		if(0 == GetDevMac(macaddr,sizeof(macaddr)))
		{
			m_mac = macaddr;
		}
	}
	root = cJSON_CreateObject();
	if(!m_sysversion.empty())
	{
		cJSON_AddStringToObject(root,"sysVersion",m_sysversion.c_str());
	}
	cJSON_AddStringToObject(root,"hardVersion","V10_1");
	if(!m_uid.empty())
	{
		cJSON_AddStringToObject(root,"deviceId",m_uid.c_str()); 
	}
	else
	{
		cJSON_AddStringToObject(root,"deviceId","1234567890123456");
	}
	if(!m_mac.empty())
	{
		cJSON_AddStringToObject(root,"macAddress",m_mac.c_str()); 
	}
	else
	{
		cJSON_AddStringToObject(root,"macAddress","00:11:22:33:44:55"); 
	}
	char csn[32] = {0};
	snprintf(csn,sizeof(csn),"%d",m_iRebootFlag);
	cJSON_AddStringToObject(root,"sn",csn); 
	if(!m_position.empty())
	{
		cJSON_AddStringToObject(root,"position",m_position.c_str()); 
	}

	SystemGetCPUUsage(cpuUsedAvg);
	if(!cpuUsedAvg.empty())
	{
		cJSON_AddStringToObject(root,"cpuUsedAvg",cpuUsedAvg.c_str());
	}
	SystemGetMemoryUsage(memUsedAvg);
	if(!memUsedAvg.empty())
	{
		cJSON_AddStringToObject(root,"memUsedAvg",memUsedAvg.c_str()); 
	}

	GetWifiValue(&wifivalue);
	snprintf(wifisignal,sizeof(wifisignal),"%d",wifivalue);
	cJSON_AddStringToObject(root,"netSignalAvg","0"); 
	cJSON_AddStringToObject(root,"wifisignal",wifisignal); 
	if(m_iFlag)
	{
		cJSON_AddStringToObject(root,"flag","2"); 
	}
	else
	{
		m_iFlag = 2;
		cJSON_AddStringToObject(root,"flag","1");
	}
	cJSON_AddStringToObject(root,"apptype","30"); 

	GetCurTime(curtime);
	if(!curtime.empty())
	{
		cJSON_AddStringToObject(root,"time",curtime.c_str()); 
	}	
	
	json_data = cJSON_Print(root);
	if(json_data)
	{
		result = json_data;
	}
	_printd("DEVICEATTR:\n%s",json_data);
	cJSON_Delete(root);
	free(json_data);

	return 0;
}
VD_INT32 CSNDeviceAttr::PackOtaResultData(string & result)
{
	cJSON *otaresult = NULL;
	cJSON *root = NULL;
	cJSON*array = NULL;
	char *json_data = NULL;

	char systemversion[64] = {'\0'};
	char presystemversion[64] = {'\0'};
	char macaddr[64] = {'\0'};
	char ipdate[64] = {'\0'};
	char wifisignal[64] = {'\0'};
	int  wifivalue = 0;
	string cpuUsedAvg;
	string memUsedAvg;
	
	string curtime;
	char  DeviceId[16]  = {'\0'};

	if(m_sysversion.empty())
	{
		if(0 == GetSystemVer(systemversion,sizeof(systemversion)))
		{
			m_sysversion = systemversion;
		}
	}
	if(m_Preversion.empty())
	{
		if(0 == GetSystemPreVer(presystemversion,sizeof(presystemversion)))
		{
			m_Preversion = presystemversion;
		}
	}
	
	if(m_uid.empty())
	{
		int ret = 0;
		char uuid[32] ={'\0'};
		if(0 == GetUuid(uuid))
		{
			m_uid = uuid;
		}
	}
	
	if(m_mac.empty())
	{
		if(0 == GetDevMac(macaddr,sizeof(macaddr)))
		{
			m_mac = macaddr;
		}
	}
	otaresult = cJSON_CreateObject();
	root = cJSON_CreateObject();
	array = cJSON_CreateArray();
	if(!m_sysversion.empty())
	{
		cJSON_AddStringToObject(root,"sysVersion",m_sysversion.c_str());
	}
	cJSON_AddStringToObject(root,"hardVersion","V10_1");
	if(!m_uid.empty())
	{
		cJSON_AddStringToObject(root,"deviceId",m_uid.c_str()); 
	}
	if(!m_mac.empty())
	{
		cJSON_AddStringToObject(root,"macAddress",m_mac.c_str()); 
	}
	cJSON_AddStringToObject(root,"sn","0"); 
	if(!m_Preversion.empty())
	{
		cJSON_AddStringToObject(root,"oldVer",m_Preversion.c_str()); 
	}
	else
	{		
		cJSON_AddStringToObject(root,"oldVer",m_sysversion.c_str()); 
	}
	cJSON_AddStringToObject(root,"newVer",m_sysversion.c_str()); 

	char strstatus[25];
	snprintf(strstatus, sizeof(strstatus), "%d", m_iupgradestatus);
	cJSON_AddStringToObject(root,"status",strstatus); 
	
	cJSON_AddStringToObject(root,"appType","30"); 
	GetCurTime(curtime);
	if(!curtime.empty())
	{
		cJSON_AddStringToObject(root,"time",curtime.c_str()); 
	}	

	cJSON_AddItemToArray(array,root);
	cJSON_AddItemToObject(otaresult,"events",array);
	json_data = cJSON_Print(otaresult);
	if(json_data)
	{
		result = json_data;
	}
	_printd("OTA:\n%s",json_data);
	cJSON_Delete(otaresult);
	free(json_data);

	return 0;
}

VD_INT32 CSNDeviceAttr::GetCurTime(string &result)
{
	SYSTEM_TIME strNow;
	memset(&strNow, 0, sizeof(SYSTEM_TIME));
	SystemGetCurrentTime(&strNow);
	
	char requesttime[128] = {0};
	snprintf(requesttime,sizeof(requesttime), "%04d-%02d-%02d %02d:%02d:%02d",
			strNow.year, strNow.month, strNow.day, strNow.hour, strNow.minute, strNow.minute);
	result = requesttime;
	return 0;
}


VD_INT32 CSNDeviceAttr::GetWlanIp(string & result)
{
	CURL *curl;  
    CURLcode res;  
    curl = curl_easy_init();  
	if (curl) 
	{
		char* url = "http://checkip.dyndns.com/";
		MemChunk chunk;
	    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
	    chunk.size = 0; 
		curl_easy_setopt(curl, CURLOPT_URL, url);  
        //callback fun 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);  
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

		// request options
		curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5L);
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5L);
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
		
        res = curl_easy_perform(curl);  
		if (CURLE_OK == res) 
		{
			result = (char*)chunk.memory;
		//	"Current IP Address: 202.105.96.60";
			int index =  result.find_last_of(":");
			result = result.substr(index + 2, result.length());
		}
		curl_easy_cleanup(curl);  
		free(chunk.memory);
	}
	return 0;
}

VD_INT32 CSNDeviceAttr::GetDeviceWlanPostion(string & result)
{
	CURL *curl;  
    CURLcode res;  
    curl = curl_easy_init();  
	if (curl) 
	{
		char* url = "http://pv.sohu.com/cityjson?ie=utf-8";
		MemChunk chunk;
	    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
	    chunk.size = 0; 
		curl_easy_setopt(curl, CURLOPT_URL, url);  
        //callback fun 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);  
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

		// request options
		curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5L);
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5L);
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
		curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
		
        res = curl_easy_perform(curl);  
		if (CURLE_OK == res) 
		{
			std::string result1 ;	
			result1 = (char*)chunk.memory;
			int index =  result1.find_last_of(":");
			result1 = result1.substr(index + 3, result1.length());
			char positon[512] ={0};
			snprintf(positon,sizeof(positon),"%s",result1.c_str());
			for(int i = 0; i< strlen(positon);i++)
			{
				if(positon[i] == '\"')
				{
					positon[i] = '\0';
					break;
				}
				
			}
			result = positon;
			_printd("position[%s]",result.c_str());
		}
		curl_easy_cleanup(curl);  
		free(chunk.memory);
	}
	return 0;
}

VD_INT32 CSNDeviceAttr::SystemGetCPUUsage(string & result)
{
	FILE *fp;  
	char buf[128];  
	char cpuuse[128] ={0};
	char cpu[5];  
	int user,nice,sys,idle,iowait,irq,softirq;  

	int all1,all2,idle1,idle2;  
	float usage;  


    fp = fopen("/proc/stat","r");   
    if(fp == NULL)  
    {  
		perror("fopen:");  
		return 0;  
    }  


    fgets(buf,sizeof(buf),fp);  
 
    sscanf(buf,"%s%d%d%d%d%d%d%d",cpu,&user,&nice,&sys,&idle,&iowait,&irq,&softirq);  

    all1 = user+nice+sys+idle+iowait+irq+softirq;  
    idle1 = idle;  
    rewind(fp);  
    /*第二次取数据*/  
    sleep(1);  
    memset(buf,0,sizeof(buf));  
    cpu[0] = '\0';  
    user=nice=sys=idle=iowait=irq=softirq=0;  
    fgets(buf,sizeof(buf),fp);  
  
    sscanf(buf,"%s%d%d%d%d%d%d%d",cpu,&user,&nice,&sys,&idle,&iowait,&irq,&softirq);  
 
    all2 = user+nice+sys+idle+iowait+irq+softirq;  
    idle2 = idle;  

    usage = (float)(all2-all1-(idle2-idle1)) / (all2-all1)*100 ;  

	snprintf(cpuuse ,sizeof(cpuuse), "%.2f",usage);
	result = cpuuse;
    fclose(fp);  

    return 0;
}
VD_INT32 CSNDeviceAttr::SystemGetMemoryUsage(string & result)
{
	MEM_info  meminfo; 	
	char memuse[128] ={0};
	get_mem_info(&meminfo);	 
	float usage = (float)(atoi(meminfo.MemTotal)-atoi(meminfo.MemFree))/atoi(meminfo.MemTotal)*100;
	snprintf(memuse, sizeof(memuse), "%.2f",usage);
	result = memuse;
	return 0;
}
VD_INT32 CSNDeviceAttr::sendRequest(int type, struct curl_slist *headerlist,
	const char *post,	string & result, long *statusCode)
{
    CURL *curl = NULL;
    CURLcode res = CURLE_OK;
    int opc = CURLE_OK;

    MemChunk chunk;
	string url;
    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
    chunk.size = 0; 

    curl = curl_easy_init();
	if(type)
	{
		//attr
		url = m_apiUrl.c_str();
	}
	else
	{
		//ota
	 	url = m_otaUrl.c_str();
	}
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	_printd("url:%s",url.c_str());
    // This option is here to allow multi-threaded unix applications to still set/use
    // all timeout options etc, without risking getting signals.
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); 

	   // set form-data to post
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
   // curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_POST, 1);  
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post);
    /* send all data to this function  */ 
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);
    /* we pass our 'chunk' struct to the callback function */ 
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	// request options
	curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5L);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
//  curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

#if VERB_LEV >= 1
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#if VERB_LEV >= 2
    curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, debug_trace);
#endif
#endif

    res = curl_easy_perform(curl);

    long httpcode = 0;
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &httpcode);
    if (res != CURLE_OK) 
	{
        fprintf(stderr, "curl_easy_perform() failed: (%d) %s\n", res, curl_easy_strerror(res));
        opc = res;//OPC_SENDFAILED;
    }
    else if (httpcode >= 200 && httpcode < 300)
	{
        opc = handleResponse(chunk.memory, chunk.size, result);
    }
    else 
	{
        opc = -1;
    }

    curl_easy_cleanup(curl);
    

    free(chunk.memory);

    *statusCode = httpcode;
    return opc;
}

VD_INT32 CSNDeviceAttr::handleResponse(const char * resp, size_t resp_len, string & result)
{
	_printd("Result:\n%s",resp);
	return CURLE_OK;
}
VD_INT32 CSNDeviceAttr::SendDeviceAttr()
{
	long statuscode;
	string send_data;
	string result;
	struct curl_slist *headerlist = NULL;
	compose_header(&headerlist);
	PackDeviceAttrData(send_data);	
	if (NULL == headerlist)
        return -1;
	if(!send_data.empty())
	{
		headerlist = curl_slist_append(headerlist,"Content-Type: application/json");  
		sendRequest(1,headerlist,send_data.c_str(),result,&statuscode);
		curl_slist_free_all (headerlist);
		if(200 == statuscode)
		{
			return 0;
		}
	}
	return -1;	
}
VD_INT32 CSNDeviceAttr::SendOtaResult(int upgradestatus)
{
	long statuscode;
	string send_data;
	string result;
	m_iupgradestatus = upgradestatus;
	struct curl_slist *headerlist = NULL;
	compose_header(&headerlist);
	PackOtaResultData(send_data);	
	if (NULL == headerlist)
        return -1;
	if(!send_data.empty())
	{
		headerlist = curl_slist_append(headerlist,"Content-Type: application/json");  
		sendRequest(0,headerlist,send_data.c_str(),result,&statuscode);
		curl_slist_free_all (headerlist);
		if(200 == statuscode)
		{
			return 0;
		}
	}
	return -1;	
}


