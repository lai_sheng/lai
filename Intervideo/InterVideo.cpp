#ifdef __INTERVIDEO_
 
#ifdef WIN32
  #pragma warning( disable : 4786)
#endif


#include "InterVideo.h"
#include "System/Console.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#ifdef LIVECAM
#include "Intervideo/LiveCam/LiveCamTrans.h"
#endif
#ifdef RTSPNEW
#include "Rtsp/rtspManager.h"
#endif
#ifdef SUNING
#include "Intervideo/RecordTs/RecordTsManager.h"
#include "Intervideo/RecordTs/RecordTsUpLoad.h"
#include "Intervideo/RecordTs/SNDeviceAttr.h"
#endif
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#include "Intervideo/HeMu/HWComunication.h"
#include "Intervideo/HeMu/HumuMediaManager.h"
#include "Intervideo/HeMu/HemuRecordManager.h"
#include "Intervideo/HeMu/DmManager.h"
#endif

#ifdef WJA_MQTT
#include "Functions/MQTT/MqttManager.h"
#endif

#ifdef MOBILE_COUNTRY
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#include "Intervideo/MobileCountry/MobileCountryMediaManager.h"
#include "Intervideo/MobileCountry/MobileCountryAppCmd.h"
#include "Intervideo/MobileCountry/MobileCountryHttpsUpgrade.h"
#include "Intervideo/MobileCountry/DmManager.h"
#endif

PATTERN_SINGLETON_IMPLEMENT(CInterVideo);

extern int g_RecordStatus;

CInterVideo::CInterVideo()
{
    trace("CInterVideo::CInterVideo()>>>>>>>>>\n");
}

CInterVideo::~CInterVideo()
{
}

int CInterVideo::Start(const int argc, char *argv[])
{
    trace("CInterVideo::Start\n");
#if 0
#ifndef SHENYAN
    g_Console.registerCmd(CConsole::Proc(&CInterVideo::OnConsoleInter, this), "inter", "intervideo operation!");
#endif
#endif

//#ifndef TUPU

#ifdef REC_STARTUP


#endif

//#endif

//Add by kyle xu in 20160303
#ifdef LIVECAM  
//	g_LiveCamTrans.Start();//启动视频码流传输	
#endif
#ifdef RTSPNEW
		g_Rtsp.Start();
#endif
#ifdef SUNING
	g_RecordTsManager.Start();
	g_TsUpLoadManager.Start();
	g_SN_DevAttr_OTA.Start();
#endif
#ifdef WJA_MQTT
	g_WjaMqtt.Start();
#endif
#ifdef HEMU_CLOUD
	g_HeMuHttpsCom.Start();
	g_HWComm.Start();
	g_HumuMediaManager.Start();
	g_HMRecordManager.Start();	
	g_DmManager.Start();
#endif
#ifdef MOBILE_COUNTRY
	g_MobileCountrySdkApi.Start();
	g_MobileCountryMediaManager.Start();//媒体数据
	g_MobileCountryAppCmd.Start(); //APP控制指令
//	g_MobileCountryUpgrade.Start();//测试升级
	g_DmManager.Start();
#endif
    return 0;
}


int CInterVideo::Stop(const int argc, char *argv[])
{
    return 0;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#ifndef SHENYAN
int CInterVideo::OnConsoleInter(int argc, char **argv)
{
    char *pszCmd0, *pArg, *pArg2;
    CConsoleArg arg(argc, argv);
    pszCmd0 = arg.getOptions();
    pArg = pArg2 = NULL;
    if (pszCmd0 == NULL)
    {
        trace("\n<inter -h> for help\n");
        return 0;
    }

    /*按照参数个数来取，超过参数个数的可能返回不为空值*/
    if (argc>1)
    {
        pArg = arg.GetArg(0);
    }

    if (argc>2)
    {
        pArg2 = arg.GetArg(1);
    }

    return 0;
}
#endif

#endif

