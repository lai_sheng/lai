#include "Intervideo/Tupu/TupuManager.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "System/File.h"

#include "Functions/FFFile.h"
#include "Functions/Record.h"
#define PRIVATEKEYPATH "/app/ipcam/pkcs8_private_key.pem"

#ifndef YINGBIN 
	#define  SECRETID "582d5d6d3f3490493c7605f5"
#else
	#define  SECRETID "58aaa7259c4fb88458c14845"
#endif
using namespace std; 

struct timeval tpcurrentime;
extern int g_RecordStatus;
#if 0
static int FacRead(void *buf, int buflen)
{
	FILE *Fp	= NULL;
	Fp	= fopen(FACINFONANDDEVPATH, "r");
	if (NULL == Fp){
		return -1;
	}
	
	if(0 > fread(buf,buflen, 1, Fp)){
		fclose(Fp);
		return -1;
	}
	fclose(Fp);
	return 0;
}
#endif
 void printResult(int rc, long statusCode, const string & result)
{
#if 1
    cout << "- Perform returns: " << rc << " " << TUPU2::opErrorString(rc) << endl;
    cout << "- HTTP Status Code: " << statusCode << endl;
    cout << "- Result: " << endl << result << endl;
#endif
}
 
 CTupuSnap::CTupuSnap()
{
	m_iStart  = 0;
	m_iReady = 0;
	m_ptkBufJPEG.Reset(true);	
	m_ptkBufTmp.Reset(true);
}

CTupuSnap::~CTupuSnap()
{

}

void CTupuSnap::StartSnap()
{
	m_iReady = 0;
	m_iStart = 0;
	m_ptkBufJPEG.Reset();
     if (CDevCapture::instance(0)->Start(this, 
        (CDevCapture::SIG_DEV_CAP_BUFFER)&CTupuSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))
    {
		_printd("start snap.....!");
    }
}

void CTupuSnap::StopSnap()
{
     if (CDevCapture::instance(0)->Stop(this, 
     (CDevCapture::SIG_DEV_CAP_BUFFER)&CTupuSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))
    {
       _printd("stop snap.....!");
    }
}

int  CTupuSnap::SnapStatus()
{
	return m_iReady;
}

unsigned const char* CTupuSnap::GetBuffer()
{
	return m_ptkBufJPEG.Buf();
}

unsigned int CTupuSnap::GetBufferSize()
{
	return m_ptkBufJPEG.Size();
}

void CTupuSnap::OnSnap(int iChannel, uint dwStreamType, CPacket *pPacket)
{
    //! 判断是否为抓图的辅码流
    if ((dwStreamType != CHL_JPEG_T) || (!pPacket))
    {
        return;
    }

	
    CGuard l_Guard(m_SnapMutex);    

    CPacket* pPkt = pPacket;
    PKT_HEAD_INFO    *pkthead = (PKT_HEAD_INFO *)(pPkt->GetHeader());
    uchar byFlag = (uchar)pkthead->FrameInfo[0].FrameFlag;
	DHTIME dhtime;


	m_ptkBufTmp.Reset();
	if(!m_iStart && byFlag != PACK_CONTAIN_FRAME_HEAD && byFlag != PACK_CONTAIN_FRAME_HEADTRAIL)
	{
		return;
	}
	if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
    	m_iStart = 1;

		//取码流里的时间
        getFirstFrameTime(pPkt, &dhtime, PACK_TYPE_FRAME_JEPG);
        //!一张图片的第一个数据包的前16个字节去掉
      /*  pPkt = g_PacketManager.GetPacket(pPacket->GetLength());
      
        if( NULL == pPkt )
        {
            errorf("alloc packet failed!\n");
            return;
        }*/
        
        //去掉数据前的12字节(8空字节+ 4 DHTIME)
		//pPkt->SetLength(pPacket->GetLength() - 16);
      /*	memcpy(pPkt->GetHeader(), pPacket->GetHeader(), PKT_HDR_SIZE);
        memcpy(pPkt->GetBuffer(), pPacket->GetBuffer() + 16, pPkt->GetLength());*/
		m_ptkBufTmp.Append(pPacket->GetBuffer() + 16,pPkt->GetLength()-16);

    }
	else
	{
		m_ptkBufTmp.Append(pPacket->GetBuffer() ,pPkt->GetLength());
	}

	//SendPic(pPkt ,  byFlag, &dhtime);
	m_ptkBufJPEG.Append((unsigned char *)m_ptkBufTmp.Buf(),m_ptkBufTmp.Size());
    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
       // pPkt->Release();
    }
	if ((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
	{
		

		m_iStart = 0;	
		m_iReady = 1;
#if 0
		CFile *filem_file = new CFile();
		assert(filem_file);
		if(filem_file->Open("/home/JPEG.JPEG", CFile::modeCreate|CFile::modeReadWrite | CFile::modeNoTruncate))
		{
			filem_file->Write((void*)m_ptkBufJPEG.Buf(), m_ptkBufJPEG.Size());
		}
		filem_file->Close();
		delete filem_file;
#endif
		//m_ptkBufJPEG.Reset();
		StopSnap();
	}

}

TupuSend::TupuSend():CThread("TupuSend", TP_DEFAULT)
{


}
TupuSend::~TupuSend()
{

}
VD_INT32 TupuSend::Start()
{
	if(-1 == access(PRIVATEKEYPATH,F_OK))
	{
		_printd("\n\n\n pkcs8_private_key.pem file not exists! ");
		_printd("Tupu Init error!!!!\n\n\n");
		return -1;
	}
	
	memset (m_UUID, 0, sizeof(m_UUID)); 
#ifdef YINGBIN
		m_rec = new Recognition(PRIVATEKEYPATH,"http://183.60.177.232:8070/v3/recognition/");
#else
		m_rec = new Recognition(PRIVATEKEYPATH);
#endif
	if(m_rec == NULL)
	{
		_printd("Tupu Init error");
		return -1;
	}


#if 0
	SAVENANDINFO_T FacInfo;
	
	memset (&FacInfo, 0, sizeof(FacInfo));
	if (-1 == FacRead(&FacInfo, sizeof(FacInfo))){
		_printd("Read Factory Info Err");
	}
	else
	{
		//strcpy(FacInfo.PTPUUID,"1201863550000116");
		strcpy(m_UUID,FacInfo.PTPUUID);
	}
#else
	if(0 != GetUuid(m_UUID))
	{
		_printd("Read Factory Info Err");
	}
#endif

	CreateThread();
	_printd ("TupuSend Start()");
	return 0;
}
VD_INT32 TupuSend::Stop()
{

}
void TupuSend::OnCapture(uchar *buf, uint len)
{
	CPacket *pPacket = g_PacketManager.GetPacket(len);

	memcpy(pPacket->GetBuffer(),buf,len);
	pPacket->SetLength(len);
	PACKET_CAPTURE packet_capture;
	packet_capture.chn = 0;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = 0;
		
	if (m_packet_count > 30) {
		debugf("TupuSend capture list is too large discard it, size:%d\n", m_packet_count);
		return ;
	}

	m_mutex_list.Enter();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_mutex_list.Leave();
}
void TupuSend::ThreadProc()
{
	SET_THREAD_NAME("TupuSend");
	while(1)
	{
		SendCapture();
	}
}
void TupuSend::SendCapture()
{

	if(m_packet_count > 0)
	{
		string result;
		long statusCode = 0;
		int rc = 0;
		int i;
		vector<TImage> images;
		TImage img;

		CPacket* pPacket = NULL;
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pPacket = it->pPacket;
		
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();
		
		
		img.setBinary(pPacket->GetBuffer(), pPacket->GetLength(), "Image1.jpeg");
		img.setTag(m_UUID);
		images.push_back(img);


		 _printd("m_rec->perform start!");
		if(NULL != m_rec)
		{
#ifdef YINGBIN	
			Log("Send Data Start ");
#endif

			rc = m_rec->perform(SECRETID, images, result, &statusCode);
#ifdef YINGBIN
			Log("Send Data end ");
#endif
			printResult(rc, statusCode, result);
		}
		pPacket->Release();
		_printd("m_rec->perform end!");
	}
	else
	{
		usleep(500*1000);
	}

	if(m_packet_count >= 29) //丢5 张图
	{
		_printd("CAN NOT UPLOAD!!! \n\n\n");
		for(int i = 0;i < 5;i++)
		{
			CPacket* pPacket = NULL;
			m_mutex_list.Enter();
			PACKET_LIST::iterator it = m_packet_list.begin();
			pPacket = it->pPacket;
			
			m_packet_list.pop_front();
			m_packet_count--;
			m_mutex_list.Leave();
			pPacket->Release();
		}
	}
}


//PATTERN_SINGLETON_IMPLEMENT(TupuManager);
TupuManager * TupuManager::instance()		
{												
	static TupuManager * _instance = NULL;		
	if( NULL == _instance)						
	{											
		_instance = new TupuManager;				
	}											
	return _instance;							
}
TupuManager::TupuManager():CThread("TupuManager", TP_NET)
{
//	m_IsEnable = 0X00;
//	memset((char *)udid, 0, sizeof(udid));
	m_iSnapTimerStatus = 1; 
	
	_printd("TupuManager");
}
TupuManager::~TupuManager()
{
	_printd("TupuManager");
}

VD_INT32 TupuManager::Start()
{
	int iRet;
	_printd("TupuManager init!!!");
 //   string secretId = "582d5d6d3f3490493c7605f5";
 //	string secretId = "58aaa7259c4fb88458c14845";

	m_TupuSend.Start();

	m_cCfgTupu.attach(this, (TCONFIG_PROC)&TupuManager::onConfigTupu);
	m_cCfgTupu.update();
	onConfigTupu(m_cCfgTupu, iRet);	
//	m_ctimer.Start(this, (VD_TIMERPROC)&TupuManager::TimerSection, 3000, 60000);
//	m_rectimer.Start(this, (VD_TIMERPROC)&TupuManager::TimerStartRec,30000, 30000); 


	CreateThread();
	_printd ("Tupu Init Over");
	return 0;
}
VD_INT32 TupuManager::Stop()
{
	_printd("TupuManager :: Stop ");
	return 0;
}

VD_INT32 TupuManager::StartSnap()
{
	_printd("TupuManager::StartSnap");
	m_Snap.StartSnap();
	return 0;
}
VD_INT32 TupuManager::StopSnap()
{
	_printd("TupuManager::StopSnap");
	m_Snap.StopSnap();
	return 0;
}
int TupuManager::TimerSnap(int arg)
{
	_printd("TimerSnap");
	string result;
	long statusCode = 0;
	int rc = 0;
	int i;
//	vector<TImage> images;
	
#ifdef YINGBIN
	
	Log("start snap ");
	for(i = 0; i < 2; i++)
	{	
#endif
		StartSnap();
		int count = 0;
		while(1)
		{
			
			if(!m_Snap.SnapStatus())
			{
				usleep(5000);
				count++;
				if( count >= 200) //1秒截不了一张图证明 丢弃
				{	
					_printd("snap error!!!\n");
					StopSnap();
					break;  
				}
			}
			else
			{
//插入列表
				rc = 1;
				m_TupuSend.OnCapture((uchar*)m_Snap.GetBuffer(), (uint)m_Snap.GetBufferSize());
				break;
			}
		}
#ifdef YINGBIN
}	
	Log("Snap end!");
#endif
	if(1 != rc) //没有截图成功
	{
		_printd("Snap error!!!");
		return -1;
	}	
	return 0;
}

int  TupuManager::Log(char *log)
{
	if(NULL ==  log)
	{
		return -1;
	}
	FILE *fp;
    if((fp=fopen("/mnt/usb/log.txt", "a+"))==NULL)	
    {
		_printd("open log file error!!!");
		return -1;
	}
	else
	{
		time_t cur_t;  
	    struct tm* cur_tm;  
		char logtemp[1024];		  
	    time(&cur_t);  
	    cur_tm=localtime(&cur_t);
		fprintf(fp,"%s Time: %s \n",log,ctime(&cur_t)); 
		fclose(fp);
		
	}
}

void TupuManager::ThreadProc()
{
	while (m_bLoop) 
	{
		if (m_iSnapTimerStatus) 
		{
			struct timeval temp;    
	        temp.tv_sec = m_cCfgTupu.getConfig().iSnapFreq;  
			_printd("m_cCfgTupu.getConfig().iSnapFreq [%d] ",m_cCfgTupu.getConfig().iSnapFreq);

			temp.tv_usec = 0;
       		select(0, NULL, NULL, NULL, &temp); 
			TimerSnap(0);
		}
		else
		{
			usleep(500);
		}
	}	
}

int TupuManager::TimerSection(int arg)
{
	struct tm *t;
    time_t tt;
    time(&tt);
    t = localtime(&tt);
	
	_printd("t->hour[%d] m_iSnapTimerStatus[%d]",t->tm_hour,m_iSnapTimerStatus);
	if(t->tm_hour >= 9 && t->tm_hour < 23 && 0 == m_iSnapTimerStatus)
	{		
		//m_cSnapTimer.Start(this, (VD_TIMERPROC)&TupuManager::TimerSnap, 1000, m_cCfgTupu.getConfig().iSnapFreq*1000);
		m_iSnapTimerStatus = 1;
	}
	else if(((t->tm_hour < 9 && t->tm_hour >= 0) || (t->tm_hour == 23))&& 1 == m_iSnapTimerStatus)
	{
		m_iSnapTimerStatus = 0;
		//m_cSnapTimer.Stop();
	}
}
int TupuManager::TimerStartRec(int arg)
{
	
	if(file_sys_get_linkstat() < 0)
	{
		if(g_RecordStatus)
		{
			printf("#############StopManulRecord##############\n");
			g_Record.StopRec(REC_MAN, 0);
		}	
		printf("#####No SdCard#######\n");
		return -1;
	}
	
	if(!g_RecordStatus)
	{
		printf("#############StartManulRecord##############\n");
		//开启手动录像
		printf("start record[%d] :%d\n",0,g_Record.StartRec(REC_MAN, 0));
	}
}
void TupuManager::onConfigTupu(CConfigTupu& cCfgTupu, int& ret)
{
	CONFIG_TUPU &configOld = m_cCfgTupu.getConfig();
	CONFIG_TUPU &configNew = cCfgTupu.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}
/*	else
	{
		if(1 == m_iSnapTimerStatus && cCfgTupu.getConfig().iSnapFreq >= 1)
		{
			_printd("Change snap freq!!!");
			//m_cSnapTimer.Stop();
			//m_cSnapTimer.Start(this, (VD_TIMERPROC)&TupuManager::TimerSnap, 1000, cCfgTupu.getConfig().iSnapFreq*1000);
		}
	}*/
	configOld = configNew;
}

