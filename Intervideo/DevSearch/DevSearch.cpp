#include "APIs/Net.h"
#include "Net/NetApp.h"
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/sysinfo.h>
#include <arpa/inet.h>
#include <netinet/in.h> 
#include <netinet/tcp.h>
#include <sys/vfs.h>
#include <net/if.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/types.h>          /* See NOTES */
#include <dirent.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <mtd/mtd-user.h>
#include <sys/select.h>
#include "Intervideo/DevSearch/DevSearch.h"
#include "Functions/General.h"
#include "APIs/CommonBSP.h"

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
#include "APIs/Ide.h"
#include "APIs/Audio.h"
#endif


#define DEV_SHOWNAME				"IPC"
#define DEV_FACNAME				"深圳万佳安实业有限公司"
#define DEV_REGCODE				"RL0808"
#define DEV_SERIALNUM				"RL01-RL0808-001A2B3C4D5E123456"
#define DEV_NO						123456
#define DEV_MODEL					"RL01"
#define HOSTNAME					"IPC"
#define DEFAULT_NET_MACADDR		"00:1A:2B:3C:4D:5E"
#define DEFAULT_UID 	            "0000000000000000" 
#define DEFAULT_CMEI                "000000000000000" 
#define DEFAULT_DEVKEY				"000000000000000000000000000000000000000000000000000000000000000000000000"
#define DEFAULT_SENSOR              "OV9712"
#define DEFAULT_HVERSION            "HC18"
#define SUPPORT_WIFI                0
#define SUPPORT_SD                  0
#define SUPPORT_AUDIOOUTPUT         0 
#define SUPPORT_AUDIOINPUT          0
#define SUPPORT_THREEMACHINE        1
#define SUPPORT_IO                  0

#ifdef CHIP_S2L
#define APP_DEVNODE		"/dev/mtd4"

#define MTD0_DEVNODE	"/dev/mtd0"
#define MTD1_DEVNODE	"/dev/mtd1"
#define MTD2_DEVNODE	"/dev/mtd2"
#define MTD3_DEVNODE	"/dev/mtd3"
#define MTD4_DEVNODE	"/dev/mtd4"
#define MTD5_DEVNODE	"/dev/mtd5"

#define SDK_NET_UPDATE	"Update"		//SDK包标识
#define TRANSITION		"Transition"	//过渡包标识

#define BST_OFFSET		0x80			//BST分区写偏移位置
#define PTB_OFFSET		0x1840			//PTB分区写偏移位置

//SPI FLash MTD
typedef enum{
	MTD0		= 0,
	MTD1		= 1,
	MTD2		= 2,	
	MTD3		= 3,
	MTD4		= 4,
	MTD5		= 5,
}SPI_FLASH_MTD;

//系统包类型
typedef enum{
	FLASH_BST			=0x00,			
	FLASH_BLD			=0X01,
	FLASH_PTB			=0X02,
	FLASH_PRI			=0X03,
	FLASH_APP			=0X04,
	FLASH_APPEX			=0X05,
	FLASH_NUM
}SYS_PACKTYPE;

//系统包信息
typedef struct{
	int 				sysPackType; 	//系统包类型	SYS_PACKTYPE
	int 				sysPackSize;	//系统包大小
}SYSPACK_INFO;

#define	SIZEOFSYSPACK	(sizeof(SYSPACK_INFO))
#define Flash_Print(fmt, ...)  printf ("\e[31m"fmt"\e[0m",##__VA_ARGS__)


char *mtdType[6] = {MTD0_DEVNODE, MTD1_DEVNODE, MTD2_DEVNODE, MTD3_DEVNODE, MTD4_DEVNODE, MTD5_DEVNODE};




static INT Init_Flash(INT *mtd_fd, INT mtd_type, struct mtd_info_user *ptb_meminfo)
{
	*mtd_fd = open(mtdType[mtd_type], O_SYNC | O_RDWR);
	if (*mtd_fd == -1) {
		perror(mtdType[mtd_type]);
		return -1;
	}
	
	if (ioctl(*mtd_fd, MEMGETINFO, ptb_meminfo) != 0) {
		perror("MEMGETINFO");
		return -1;
	}
	
	return 0;
}

static INT Write_Flash(INT mtd_fd, struct mtd_info_user ptb_meminfo, UCHAR *buf, INT buflen, INT offset)
{
	INT ret				= 0;
	INT write_len		= 0;
	erase_info_t erase;
	ssize_t read_flash_size=0;
	char *flash_buf = NULL;

	if(buf == NULL){
		return -1;
	}
	
	flash_buf = (char *)malloc(ptb_meminfo.erasesize);
	for(write_len = 0; (write_len < buflen) && (offset < ptb_meminfo.size); ){
		erase.start = offset - offset % ptb_meminfo.erasesize;
		erase.length = ptb_meminfo.erasesize; //每次擦除64kb

		//flash_buf = (char *)malloc(ptb_meminfo.erasesize);
		memset(flash_buf, 0, ptb_meminfo.erasesize);
		read_flash_size = pread(mtd_fd, flash_buf, offset % ptb_meminfo.erasesize, erase.start);
		
		if (ioctl(mtd_fd, MEMERASE, &erase) != 0) {
			perror("MEMERASE");
		}
		ret = pwrite(mtd_fd, flash_buf, read_flash_size, erase.start);
	 	ret = pwrite(mtd_fd, buf, (buflen - write_len) < (ptb_meminfo.erasesize - offset % ptb_meminfo.erasesize) ? (buflen - write_len) : (ptb_meminfo.erasesize - offset % ptb_meminfo.erasesize), offset);//向flash设备写数据
		printf("offset: %d, ret: %d\n", offset, ret);
		write_len += ret;
		offset += ret;
	}
	free(flash_buf);
 
	return 0;
}

static INT Destroy_Flash(INT mtd_fd)
{
	close(mtd_fd);
	return 0;
}
								  	
INT Recv_SysPack_toFlash(INT Fd, INT mtd_type, INT flash_offset, UCHAR *sumBuf, SYSPACK_INFO sysPack, struct timeval *wTimeOut, fd_set *FdSet)
{	
	INT 		iRet 				= 0;
	INT 		offset	 			= 0; 
	INT			nErrCnt				= 0;
	INT 		totalBufLen 		= 0; 
	INT 		restBufLen 			= 0;  //totalBufLen restBufLen 实现缓冲区每次获取定值
	INT 		recvOnePackSize 	= 0;  //单包实时接收大小
	INT	 		mtd_fd 				= -1;
	struct mtd_info_user ptb_meminfo;

	if((Init_Flash(&mtd_fd, mtd_type, &ptb_meminfo)) < 0) {
		_printd("Init Flash fail");
		goto FLASH_ERROR_EXIT;
	}

	for (offset = flash_offset; (recvOnePackSize < sysPack.sysPackSize) && (offset < ptb_meminfo.size); offset += totalBufLen) {		
		memset(sumBuf, 0, ptb_meminfo.erasesize);
		if ((sysPack.sysPackSize - recvOnePackSize) >= ptb_meminfo.erasesize){
			totalBufLen = ptb_meminfo.erasesize;
		}else{
			totalBufLen = sysPack.sysPackSize - recvOnePackSize;
		}
		restBufLen = totalBufLen;
		nErrCnt = 0;
		while (restBufLen > 0) 
		{
			if ( 0>= (iRet=select(Fd+1,FdSet,NULL,NULL,wTimeOut))) {
				if (nErrCnt++ > 30){
					_printd("send file break off or too slow");
					goto FLASH_ERROR_EXIT;
				}
				continue;
			}
			
			if (FD_ISSET(Fd, FdSet)) {
				iRet=recv(Fd, sumBuf + (totalBufLen - restBufLen), restBufLen, 0);
				//printf("iRet ======================= %d\n", iRet);
				if (iRet <= 0)
					goto FLASH_ERROR_EXIT;
				restBufLen -= iRet; 
				recvOnePackSize += iRet;
			}				
		}
	
		if ((Write_Flash(mtd_fd, ptb_meminfo, sumBuf, totalBufLen, offset)) < 0)
			break;
	}
	
	fsync(mtd_fd);
	sync();	
	
FLASH_ERROR_EXIT:
	Destroy_Flash(mtd_fd);
	return recvOnePackSize;
}

static int get_hard_cpu_sdk_version(char *HardVersion, char *CpuVersion, char *SdkVersion)
{
	FILE    *fp_ev  	= NULL; // /etc/version
	FILE	*fp_ea 		= NULL; // /etc/ambarella.conf
	FILE 	*fp_pv		= NULL; // /proc/version
	FILE 	*fp_as		= NULL; // /appex/custom/sensortype.ini
	CHAR    buf[1024]	= {0};
	INT		flag		= 0;
typedef struct _IPC_VERSION{
	char hard[32];
	char sdk[32];
	char system[32];
}IPC_VERSION;
	
	IPC_VERSION	ipcVer;
	
	fp_ev = fopen("/etc/version", "r");
	if (fp_ev == NULL) {
		perror("Fail to fopen /etc/version");
		//return -1;
		flag = 1;
	} else {	
		memset(&ipcVer, 0, sizeof(IPC_VERSION));
		fread(buf, sizeof(char), sizeof(IPC_VERSION), fp_ev);
		if (strstr(buf, "/") != NULL) {
			sscanf(buf, "%[^/]/%[^/]/%[^/]",ipcVer.hard, ipcVer.sdk, ipcVer.system);
			sscanf(ipcVer.hard, "%*[^:]:%s", HardVersion);
			sscanf(ipcVer.sdk, "%*[^:]:%s", SdkVersion);
			sscanf(HardVersion, "%[^_]", CpuVersion);
		} else {
			flag = 1;
		}
		fclose(fp_ev);
	}

	if (flag) {
		if (NULL == (fp_ea = fopen("/etc/ambarella.conf", "r"))) {
			perror("Fail to fopen /etc/ambarella.conf");
			return -1;
		}
		memset(buf, 0, sizeof(buf));
		fread(buf, sizeof(char), sizeof(buf), fp_ea);
		if (strstr(buf, "S2L22M") != NULL) {
			strcpy(HardVersion, "MCS2L22MDV03");
			strcpy(CpuVersion, "S2L22M");
		}
		else if (strstr(buf, "S2L55M") != NULL) {
			strcpy(HardVersion, "MCS2L55MDV03");
			strcpy(CpuVersion, "S2L55M");
		} else {
			if (NULL == (fp_as = fopen("/mnt/custom/sensortype.ini", "r"))) {
				perror("Fail to fopen /mnt/custom/sensortype.ini");
				strcpy(HardVersion, "MCS2L55MDV03");
				strcpy(CpuVersion, "S2L55M");
			} else {
				memset(buf, 0, sizeof(buf));
				fread(buf, sizeof(char), sizeof(buf), fp_as);
				if (strstr(buf, "ar0130") != NULL) {
					strcpy(HardVersion, "MCS2L22MDV03");
					strcpy(CpuVersion, "S2L22M");
				} else {
					strcpy(HardVersion, "MCS2L55MDV03");
					strcpy(CpuVersion, "S2L55M");
				}
				fclose(fp_as);
			}
		}
		fclose(fp_ea);
		
		if (NULL == (fp_pv = fopen("/proc/version", "r"))) {
			perror("Fail to fopen /proc/version");
			return -1;
		}
		memset(buf, 0, sizeof(buf));
		fread(buf, sizeof(char), sizeof(buf), fp_pv);
		if (strstr(buf, "3.10.50") != NULL)
			strcpy(SdkVersion, "2_0");
		if (strstr(buf, "3.10.73") != NULL)
			strcpy(SdkVersion, "2_5");
		fclose(fp_pv);
	}
	return 0;
}

#endif

static int NandRead(VOID *buf, INT buflen)
{
	FILE *Fp	= NULL;
	Fp	= fopen(FACINFONANDDEVPATH, "r");
	if (NULL == Fp){
		return -1;
	}
	
	if(0 > fread(buf,buflen, 1, Fp)){
		fclose(Fp);
		return -1;
	}
	fclose(Fp);
	return 0;
}

static INT NandErase()
{
	FILE *Fp	= NULL;
	char buf[1024] = {0};
	memset(buf, 0, 1024);
	_printd("FAC %s", FACINFONANDDEVPATH);
	Fp	= fopen(FACINFONANDDEVPATH, "w");
	if (NULL == Fp){
		return -1;
	}
	_printd("makr 1");
	//if(0 > fwrite(buf, 1024, 2, Fp)){
	if(0 > fwrite(buf, 1024, 1, Fp)){  //hyd
		fflush(Fp);
		fclose(Fp);
		sync();
		return -1;
	}
	_printd("makr 1");
	fclose(Fp);
	_printd("makr 1");
	return 0;
}

static INT NandWrite(VOID *buf, INT buflen)
{
	FILE *Fp	= NULL;
	Fp	= fopen(FACINFONANDDEVPATH, "w");
	if (NULL == Fp){
		return -1;
	}
	
	if(0 > fwrite(buf, buflen, 1, Fp)){
		fflush(Fp);
		fclose(Fp);
		sync();
		return -1;
	}
	fclose(Fp);
	return 0;
}
//#ifdef BPI_S2L  //fyj 2016-9-7
//检测uuid是否正常
static INT CheckUuid(SAVENANDINFO_T *pInFacInfo)
{
	int i = 15;
	
	if(pInFacInfo == NULL)
		return -1;
	if(pInFacInfo->PTPUUID == NULL || strlen(pInFacInfo->PTPUUID) < 16 ||
		pInFacInfo->PTPUUID[0] == 0 || pInFacInfo->PTPUUID[0] == '\0')
		{
			_printd("==============");
			return -1;
		}
	while(i >= 0){
		if(pInFacInfo->PTPUUID[i] >= '0' && pInFacInfo->PTPUUID[i] <= '9')
			i --;
		else
		{
			_printd("==============");
			return -1;
		}
	}
	if( strlen(pInFacInfo->PTPUUID) > 16 )
		pInFacInfo->PTPUUID[16] = '\0';
		
	return 0;
}

//检测cmei是否正常
static INT CheckCmei(SAVENANDINFO_T *pInFacInfo)
{
#ifdef YiDong
	int i = 14;
	
	if(pInFacInfo == NULL)
		return -1;
	if(pInFacInfo->PTPCMEI == NULL || strlen(pInFacInfo->PTPCMEI) < 15 ||
		pInFacInfo->PTPCMEI[0] == 0 || pInFacInfo->PTPCMEI[0] == '\0')
		return -1;

	while(i >= 0){
		if(pInFacInfo->PTPCMEI[i] >= '0' && pInFacInfo->PTPCMEI[i] <= '9')
			i --;
		else
			return -1;
	}
	if( strlen(pInFacInfo->PTPCMEI) > 15 )
		pInFacInfo->PTPCMEI[15] = '\0';
#endif	
	return 0;
}

//备份FAC文件
static INT BackupFac(SAVENANDINFO_T *pInFacInfo)
{
	if(CheckUuid(pInFacInfo) <0){
		_printd("CheckUuid fail\n");
		return -1;
	}
	
	//if(CheckCmei(pInFacInfo) <0){
	//	_printd("CheckCmei fail\n");
	//	return -1;
	//}
	if(access(FACINFOBACKUPPATH,F_OK)!=0)
	{
		if(mkdir(FACINFOBACKUPPATH,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
		{
			_printd("mkdir %s unsuccessfully\n",FACINFOBACKUPPATH);
			return -1;
		}
	}
	
	FILE *Fp	  = NULL;
	char buf[1024];
	memset(buf, 0, 1024);
	
	Fp	= fopen(FACINFOBACKUPDEVPATH, "w");
	if (NULL == Fp){
		return -1;
	}
//Write Fac
	if(0 > fwrite((char*)pInFacInfo,sizeof(SAVENANDINFO_T), 1, Fp)){
		fclose(Fp);
		return -1;
	}
	fflush(Fp);
	fclose(Fp);
	sync();

	return 0;
	
}

static int FacRead(void *buf, int buflen)
{	
	FILE *Fp	= NULL;	
	Fp	= fopen(FACINFONANDDEVPATH, "r");	
	if (NULL == Fp)
	{		
		return -1;	
	}		
	if(0 > fread(buf,buflen, 1, Fp))
	{	
		fclose(Fp);		
		return -1;	
	}	
	fclose(Fp);
	return 0;
}

//从备份FAC中恢复信息
static INT GetBackupFac(SAVENANDINFO_T *pOutFacInfo)
{
	if(access(FACINFOBACKUPDEVPATH,F_OK)!=0)
		return -1;
	
	FILE *Fp	  = NULL;
	SAVENANDINFO_T  pFacInfo;
	Fp = fopen(FACINFOBACKUPDEVPATH, "r");
	if(NULL == Fp){
		return -1;
	}
	if(0 > fread(&pFacInfo,sizeof(SAVENANDINFO_T),1,Fp)){
		fclose(Fp);
		return -1;
	}
	fclose(Fp);
	
	if(CheckUuid(&pFacInfo) < 0){
		_printd("CheckUuid fail 111\n");
		return -1;
	}
	
	//if(CheckCmei(&pFacInfo) < 0){
	//	_printd("CheckCmei fail 111\n");
	//	return -1;
	//}
	memcpy(pOutFacInfo, &pFacInfo, sizeof(pFacInfo));
	return 0;
}
//#endif
INT ReadFacInfo(SAVENANDINFO_T *pOutFacInfo)
{
#if 1
	SAVENANDINFO_T FacInfo;
	int iRet = 0;
	UCHAR MarkERR	= 0;
	memset (&FacInfo, 0, sizeof(FacInfo));
	if (-1 == NandRead(&FacInfo, sizeof(FacInfo))){
		_printd("Read Factory Info Err");
		if(GetBackupFac(&FacInfo) < 0)
		{
			MarkERR		= 1;
		}
		else
		{
			//把备份到主存
			NandWrite(&FacInfo, sizeof(FacInfo));
		}
	}
	else
	{
		//读正常都备份一下
		BackupFac(&FacInfo);
	}

	if (1	== MarkERR
		|| strlen((char*)FacInfo.devModel)		>= sizeof(FacInfo.devModel)
		|| strlen((char*)FacInfo.devSerialNumb)	>= sizeof(FacInfo.devSerialNumb)
		|| strlen((char*)FacInfo.devRegcode)	>= sizeof(FacInfo.devRegcode)
		|| strlen((char*)FacInfo.devName)		>= sizeof(FacInfo.devName)
		|| strlen((char*)FacInfo.szFactoryName)	>= sizeof(FacInfo.szFactoryName)
		|| strlen((char*)FacInfo.NetMacAddr)	>= sizeof(FacInfo.NetMacAddr)
		|| strlen((char*)FacInfo.PTPUUID)		>= sizeof(FacInfo.PTPUUID)
		/*|| strlen((char*)FacInfo.PTPCMEI)		>= sizeof(FacInfo.PTPCMEI)
		|| strlen((char*)FacInfo.PTPDevKey)		>= sizeof(FacInfo.PTPDevKey)*/
		|| strlen((char*)FacInfo.sensor)		>= sizeof(FacInfo.sensor)){
		
		#if	defined(BPI_S2L)|| defined(IPC_HISI)|| defined(IPC_A3)|| defined(IPC_JZ) || defined(IPC_JZ_NEW)  
		char macAddr[24];
		NetGetMAC("wlan0", macAddr, sizeof(macAddr));
		sprintf((char*)FacInfo.devSerialNumb, "RL01-RL0808-%c%c%c%c%c%c%c%c%c%c%c%c123456", 
			macAddr[0], macAddr[1], macAddr[3], macAddr[4], macAddr[6], macAddr[7], macAddr[9], macAddr[10], 
			macAddr[12], macAddr[13], macAddr[15], macAddr[16]);
		strcpy  ((char*)FacInfo.NetMacAddr, 		macAddr);
		#elif defined CHIP_S2L   //r2p2
		char macAddr[24];
		NetGetMAC("eth0", macAddr, sizeof(macAddr));
		sprintf((char*)FacInfo.devSerialNumb, "RL01-RL0808-%c%c%c%c%c%c%c%c%c%c%c%c123456", 
			macAddr[0], macAddr[1], macAddr[3], macAddr[4], macAddr[6], macAddr[7], macAddr[9], macAddr[10], 
			macAddr[12], macAddr[13], macAddr[15], macAddr[16]);
		strcpy  ((char*)FacInfo.NetMacAddr, 		macAddr);
		#else
		strcpy  ((char*)FacInfo.devSerialNumb,		DEV_SERIALNUM);
		strcpy  ((char*)FacInfo.NetMacAddr, 		DEFAULT_NET_MACADDR);
		#endif
		strcpy  ((char*)FacInfo.devModel, 			DEV_MODEL);
		strcpy  ((char*)FacInfo.devRegcode, 		DEV_REGCODE);
		strcpy  ((char*)FacInfo.devName,			DEV_SHOWNAME);
		strcpy  ((char*)FacInfo.szFactoryName, 		DEV_FACNAME);
		strcpy  ((char*)FacInfo.PTPUUID, 			DEFAULT_UID);
		strcpy  ((char*)FacInfo.PTPCMEI, 			DEFAULT_CMEI);
		strcpy  ((char*)FacInfo.PTPDevKey, 			DEFAULT_DEVKEY);
		
		strcpy  ((char*)FacInfo.sensor, 			DEFAULT_SENSOR);

		FacInfo.devNo		        =		DEV_NO;
		FacInfo.ThreeMachineSupport =       SUPPORT_THREEMACHINE;
		FacInfo.WifiOFF				=       SUPPORT_WIFI;
		FacInfo.SDOFF				=		SUPPORT_SD;
		FacInfo.AudioInOFF			=		SUPPORT_AUDIOINPUT;
		FacInfo.AudioOutOFF			=		SUPPORT_AUDIOOUTPUT;
		FacInfo.IOOFF				=		SUPPORT_IO;
		
		_printd("Read Factory flash Get a error msg so set default facinfo");
		iRet = FALSE;
	}else {
//#ifdef BPI_S2L
		if(CheckUuid(&FacInfo) < 0)
		{
			if(GetBackupFac(&FacInfo) < 0)
				return FALSE;
		}
//#endif
		_printd("Read Factory flash OK");
		iRet = TRUE;
	}
	memcpy(pOutFacInfo, &FacInfo, sizeof(FacInfo));
	return iRet;
#else
	
	SAVENANDINFO_T FacInfo;
	int iRet = 0;
	UCHAR MarkERR	= 0;
	if (pOutFacInfo == NULL){//防止用的时候 位置放置不对 出现错误
		return -1;
	}
	memset (&FacInfo, 0, sizeof(FacInfo));
	if (-1 == NandRead(&FacInfo, sizeof(FacInfo))){
		_printd("Read Factory Info Err");
		MarkERR		= 1;
	}
	/*
	*  以下是为了防止flash上没有有效数据时的排错处理
	*
	*/
	//FacInfo.LogoType = 0;
	if (1	== MarkERR
		|| strlen((char *)FacInfo.devModel) >= sizeof(FacInfo.devModel)
		|| strlen((char *)FacInfo.devSerialNumb) >= sizeof(FacInfo.devSerialNumb)
		|| strlen((char *)FacInfo.devRegcode) >= sizeof(FacInfo.devRegcode)
		|| strlen((char *)FacInfo.devName) >= sizeof(FacInfo.devName)
		|| strlen((char *)FacInfo.szFactoryName) >= sizeof(FacInfo.szFactoryName)
		|| strlen((char *)FacInfo.NetMacAddr) >= sizeof(FacInfo.NetMacAddr)){
		
		struct sysinfo  SysMemInfo  = {0};
		struct timeval  SysTimeInfo = {0};
		char MacValue[6] = {0};
		char    temp[10] = {0};
		
		if(0>gettimeofday(&SysTimeInfo,NULL))
		 {
			 _printd("GetMacFuntion time fail.");
			 return -1;
		 }

		 if(0>sysinfo(&SysMemInfo))
		 {
			_printd(" sysinfo GetMacFunction mem fail");
			return -1;
		 }
	 
		 temp[0]=SysTimeInfo.tv_usec%100000/10000;
		 temp[1]=SysTimeInfo.tv_usec%10000/1000;
		 temp[2]=SysTimeInfo.tv_usec%1000/100;
		 temp[3]=SysTimeInfo.tv_usec%100/10;
		 temp[4]=SysTimeInfo.tv_usec%10;
		 
		 temp[5]=SysMemInfo.freeram%100000/10000;
		 temp[6]=SysMemInfo.freeram%10000/1000;
		 temp[7]=SysMemInfo.freeram%1000/100;
		 temp[8]=SysMemInfo.freeram%100/10;
		 temp[9]=SysMemInfo.freeram%10;

	#ifdef NVR_3520D
		 MacValue[0] =  11;//(temp[0]*100+temp[1]*10+temp[2])%16;//NVR标识:Mac以_0A_开头
	#else
		 MacValue[0] =  10;//(temp[0]*100+temp[1]*10+temp[2])%16;//ipcam标识:Mac以_0B_开头
	#endif
		 MacValue[1] = (temp[1]*100+temp[2]*10+temp[3])%255;
		 MacValue[2] = (temp[2]*100+temp[3]*10+temp[4])%255;

		 MacValue[3] = (temp[5]*100+temp[6]*10+temp[7])%255;
		 MacValue[4] = (temp[6]*100+temp[7]*10+temp[8])%255;
		 MacValue[5] = (temp[7]*100+temp[8]*10+temp[9])%255;
		 
		 //随机MAC地址
		 sprintf(pOutFacInfo->NetMacAddr,"%02x:%02x:%02x:%02x:%02x:%02x ",MacValue[0],MacValue[1],
		            MacValue[2],MacValue[3],MacValue[4],MacValue[5]);
		
		pOutFacInfo->devNo		=	DEV_NO;
#ifdef CHIP_S2L
		char macAddr[24];
#ifdef BPI_S2L
		NetGetMAC("wlan0", macAddr, sizeof(macAddr));
#else
		NetGetMAC("eth0", macAddr, sizeof(macAddr));
#endif
		sprintf((char*)FacInfo.devSerialNumb, "RL01-T368120808-%c%c%c%c%c%c%c%c%c%c%c%c123456", 
			macAddr[0], macAddr[1], macAddr[3], macAddr[4], macAddr[6], macAddr[7], macAddr[9], macAddr[10], 
			macAddr[12], macAddr[13], macAddr[15], macAddr[16]);
		strcpy  ((char*)FacInfo.NetMacAddr, 		macAddr);
#else
		strcpy  ((char*)FacInfo.devSerialNumb,		DEV_SERIALNUM);
		strcpy  ((char*)FacInfo.NetMacAddr, 		DEFAULT_NET_MACADDR);
#endif
		strcpy  ((char *)pOutFacInfo->devModel, 		DEV_MODEL);
		strcpy  ((char *)pOutFacInfo->devSerialNumb,	DEV_SERIALNUM);
		strcpy  ((char *)pOutFacInfo->devRegcode, 		DEV_REGCODE);
		if (0 == strlen(pOutFacInfo->devName)){//设备显示名称
			strcpy  (pOutFacInfo->devName,			    DEV_SHOWNAME);
		}
		strcpy  ((char *)pOutFacInfo->szFactoryName, 	DEV_FACNAME);
		pOutFacInfo->LogoType       = 0;
		pOutFacInfo->WifiOFF		= 0;
		pOutFacInfo->SDOFF			= 0;
		pOutFacInfo->AudioInOFF	    = 0;
		pOutFacInfo->AudioOutOFF	= 0;
		pOutFacInfo->IOOFF			= 0;
		_printd("Read Factory flash Get a error msg so set default facinfo");
		iRet =  -1;
	}else {
		memcpy(pOutFacInfo, &FacInfo, sizeof (SAVENANDINFO_T));
		_printd("Read Factory flash OK");
		_printd("devNo = %d ", FacInfo.devNo);
		_printd("devModel = %s ", FacInfo.devModel);
		_printd("devSerialNumb = %s ", FacInfo.devSerialNumb);
		_printd("devRegcode = %s ", FacInfo.devRegcode);
		_printd("devName = %s ", FacInfo.devName);
		_printd("szFactoryName = %s ", FacInfo.szFactoryName);
		_printd("NetMacAddr = %s ", FacInfo.NetMacAddr);
		_printd("WifiOFF = %d ", FacInfo.WifiOFF);
		_printd("SDOFF = %d ", FacInfo.SDOFF);
		_printd("AudioInOFF = %d ", FacInfo.AudioInOFF);
		_printd("AudioOutOFF = %d ", FacInfo.AudioOutOFF);
		_printd("IOOFF = %d ", FacInfo.IOOFF);
		iRet = 0;
	}

	_printd("####FacInfo.LogoType = %d", FacInfo.LogoType);

	return iRet;
#endif
}

INT WriteFacInfo(SAVENANDINFO_T *pInFacInfo)
{
	//NandErase();
//#ifdef BPI_S2L
	if(CheckUuid(pInFacInfo) < 0){
		_printd("Ther UUID is informal");
		return -1;
	}
//#endif
	//if(CheckCmei(pInFacInfo) < 0){
	//	_printd("Ther CMEI is informal");
	//	return -1;
	//}

	if (-1 == NandWrite(pInFacInfo, sizeof(SAVENANDINFO_T))){
		_printd("Write Factory Info Err");
		return -1;
	} else {
//#ifdef BPI_S2L
		BackupFac(pInFacInfo);
//#endif
		_printd("Write Factory Info over");
		return 0;
	}
}

int GetUuid(char uuid[16])
{
	if(uuid == NULL)	return -1;
	memset(uuid, 0, 16);
		
#ifndef FAC_CHECK
	SAVENANDINFO_T FacInfo;
	VD_INT32	   len;

	memset (&FacInfo, 0, sizeof(FacInfo));
	//if (-1 == FacRead(&FacInfo, sizeof(FacInfo))){
	if(!ReadFacInfo(&FacInfo))
	{
		_printd("Read Factory Info Err");
		return -1;
	}
	strncpy(uuid, FacInfo.PTPUUID,16);	
	if ( (len = strlen(FacInfo.PTPUUID)) > 16)
		return -1;
	strncpy(uuid, FacInfo.PTPUUID,16);
	return 0;
#else

	char fac_uid[32] = "";
	char length = 0;
	FILE *fp = NULL;
	if (0 == access(FAC_TOTAL_CHECK_UUID_PATH,F_OK))
	{
		fp = fopen(FAC_TOTAL_CHECK_UUID_PATH,"r");
		if (fp == NULL)
		{
			_printd("open %s error !!! \n",FAC_TOTAL_CHECK_UUID_PATH);
			return -1;
		}
		else 
		{
			length = fread (fac_uid, 1, 16,fp);
			if (length == 16)
			{
				strncpy(uuid, fac_uid,16);
				_printd("read %s  UID is [%s]\n",FAC_TOTAL_CHECK_UUID_PATH,uuid);
			}
			fclose(fp);
			fp = NULL;
		}
	}
	return 0;
#endif
		
	
}
int GetConfigMac(char mac[16])
{
	if(mac == NULL)	return -1;
	memset(mac, 0, 16);
		
	SAVENANDINFO_T FacInfo;
	VD_INT32	   len;
	std::string serialnumb;
	std::string macaddr;
	memset (&FacInfo, 0, sizeof(FacInfo));
	if(!ReadFacInfo(&FacInfo))
	{
		_printd("Read Factory Info Err");
		return -1;
	}
	serialnumb = FacInfo.devSerialNumb;
	macaddr = serialnumb.substr(12, 12);
	strcpy(mac,macaddr.c_str());
	_printd("%s",macaddr.c_str());
	//sscanf((char*)FacInfo.devSerialNumb, "%12s%12s%6d",tmp,mac,tmpint);
	if ( (len = strlen(mac)) > 12 || len == 0)
	{
		_printd("mac Err %s",FacInfo.devSerialNumb);
		return -1;
	}
	return 0;
	
}

int GetCmei(char cmei[15])
{
	if(cmei == NULL)	return -1;
	memset(cmei, 0, 15);
		
#ifndef FAC_CHECK
	SAVENANDINFO_T FacInfo;
	int	   len = 0;

	memset (&FacInfo, 0, sizeof(FacInfo));
	if(!ReadFacInfo(&FacInfo))
	{
		_printd("Read Factory Info Err");
		return -1;
	}
	strncpy(cmei, FacInfo.PTPCMEI,15);	
	if ( (len = strlen(FacInfo.PTPCMEI)) > 15)
		return -1;
	strncpy(cmei, FacInfo.PTPCMEI,15);
	return 0;
#else
	return -1;
#endif
}

int GetDevKey(char DevKey[72])
{
	if(DevKey == NULL)	return -1;
	memset(DevKey, 0, 72);
		
	SAVENANDINFO_T FacInfo;
	int	   len = 0;

	memset(&FacInfo, 0, sizeof(FacInfo));
	if(!ReadFacInfo(&FacInfo))
	{
		_printd("Read Factory Info Err");
		return -1;
	}
	strncpy(DevKey, FacInfo.PTPDevKey,72);	
	if ( (len = strlen(FacInfo.PTPDevKey)) > 72)
		return -1;
	strncpy(DevKey, FacInfo.PTPDevKey,72);

	return 0;
}

#define     PORT_UDP_RESP               		40001               //UDP设备搜寻回应端口
#define     PORT_UDP_SEARCH               		40002               //UDP设备搜寻信号端口
#define 	MULTICASE_ADDR						"230.1.1.1"

static INT GetDevSelfCode(unsigned short *sc1, unsigned short *sc2)
{
	struct sysinfo  SYSinfo;
	INT 			code[2];
	struct timeval	tival	= {0};
	gettimeofday(&tival, NULL);
	if (0 != sysinfo(&SYSinfo)){
		*sc1 = tival.tv_usec;
		*sc2 = tival.tv_sec %10000;
	}else{
	code[0] = (int )tival.tv_usec % 10000000;
	code[1] = (int )SYSinfo.freeram % 100000;
		*sc2 = code[0] + code[1];
	}
	_printd("*sc1=%x, *sc2= %x", *sc1, *sc2);
	return 0;
}
static int CreateSendSocketFd(int *pSendSocketFd, struct sockaddr_in *pSendAddr)
{
	int 			SendSocketFd	= -1;
	int				enable			= 1;

	if (0 >= (SendSocketFd = socket(AF_INET, SOCK_DGRAM, 0))){
		perror("Socket ERr");
		return -1;
	}
	memset(pSendAddr, 0, sizeof(struct sockaddr_in));
	pSendAddr->sin_family 	= AF_INET;
	pSendAddr->sin_port 	= htons(PORT_UDP_RESP);
	if (inet_pton(AF_INET, MULTICASE_ADDR, &(pSendAddr->sin_addr)) <= 0){
		perror("inet_pton");
		close(SendSocketFd);
		return -1;
	}
	setsockopt(SendSocketFd, SOL_SOCKET, SO_REUSEADDR, (char *)&enable, sizeof(unsigned long int));
    setsockopt(SendSocketFd, SOL_SOCKET, SO_BROADCAST, (char *)&enable, sizeof(unsigned long int));
	*pSendSocketFd = SendSocketFd;
	return 0;
}

static int CreateRecvSocketFd(int *pRecvSocketFd, struct sockaddr_in *pRecvAddr, in_addr_t in_addr)
{
	struct in_addr 		ia;
	int					enable	= 1;
	int			 		socklen;
	struct hostent 		*group;
	struct ip_mreq 		mreq;
	
	if (0 >= (*pRecvSocketFd = socket(AF_INET, SOCK_DGRAM, 0))){
		return -1;
	}
	
	bzero(&mreq, sizeof(struct ip_mreq));
	if ((group = gethostbyname(MULTICASE_ADDR)) == (struct hostent *) 0) {
		perror("gethostbyname");
		close(*pRecvSocketFd);
		return -1;
	}
	
	bcopy((void *)group->h_addr, (void *)&ia, group->h_length);
	#if 0
			bcopy(&ia, &mreq.imr_multiaddr.s_addr, sizeof(struct in_addr));
			mreq.imr_interface.s_addr = htonl(INADDR_ANY);	
	#else
			mreq.imr_multiaddr.s_addr = inet_addr(MULTICASE_ADDR);
	mreq.imr_interface.s_addr = in_addr;//htonl(INADDR_ANY);
	#endif
	if (setsockopt(*pRecvSocketFd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,sizeof(struct ip_mreq)) == -1) {
		perror("setsockopt");
		close(*pRecvSocketFd);
		return -1;
	}
	
	socklen = sizeof(struct sockaddr_in);
	memset(pRecvAddr, 0, socklen);
	pRecvAddr->sin_family 	= AF_INET;
	pRecvAddr->sin_port 	= htons(PORT_UDP_SEARCH);
	if (inet_pton(AF_INET, MULTICASE_ADDR, &(pRecvAddr->sin_addr)) <= 0) {
		perror("inet_pton");
		close(*pRecvSocketFd);
		return -1;
	}

	setsockopt(*pRecvSocketFd, SOL_SOCKET, SO_REUSEADDR, (char *)&enable, sizeof(unsigned long int));
	if (bind(*pRecvSocketFd, (struct sockaddr *)pRecvAddr,sizeof(struct sockaddr_in)) == -1) {
		perror("Bind error");
		close(*pRecvSocketFd);
		return -1;
	}
	
    setsockopt(*pRecvSocketFd, SOL_SOCKET, SO_BROADCAST, (char *)&enable, sizeof(unsigned long int));
	return 0;

}
INT CreateUPdateSock(INT port) //Create Listen Socket
{
	int serverSock;
	int iRet = 0;
	struct sockaddr_in server_addr; /* connector's address information */
	if ((serverSock = socket(AF_INET, SOCK_STREAM, 0/*IPPROTO_TCP*/)) == -1) 
	{
		_printd ("socket error return -1");
		return -1;
	}
	fcntl(serverSock,F_SETFL,O_NONBLOCK); //设置套接字为非阻塞
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	bzero(&(server_addr.sin_zero), 8);
    setsockopt(serverSock, SOL_SOCKET, SO_REUSEADDR, &iRet,sizeof(iRet));
    //setsockopt(serverSock, SOL_SOCKET, SO_REUSEPORT, &iRet,sizeof(iRet));
	if (-1 == bind(serverSock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))){

		_printd("bind socket Error return -1");
		return -1;
	}
	if(listen(serverSock, 100) == -1){
		_printd ("listen socket error return -1");
		return -1;
	}
    return serverSock;
}

INT IPCAM_TestSocket(INT SocketFD, TESTCTRL Contrl, INT timeout)
{
	fd_set			test_set;
	struct timeval  wtimeout;
	INT				Ret		= 0;
	FD_ZERO(&test_set);
	FD_SET(SocketFD, &test_set);
	wtimeout.tv_sec			= timeout;
	wtimeout.tv_usec 		= 0;
	if (Contrl == TESTSOCKET_READ){
		Ret = select(SocketFD + 1, &test_set, NULL, NULL, &wtimeout);
	}else{
		Ret = select(SocketFD + 1, NULL, &test_set, NULL, &wtimeout);
	}
	if (Ret < 0){
		_printd("select eror");
		return FALSE;
	} else if (Ret == 0){
		_printd("time out");
		return -1;
	}
	return TRUE;
}


INT IPCAM_RecvPacket(INT SocketFd, CHAR *buf, INT buflen, int waitsec)
{
	INT		RetTS			= TRUE;
	INT		HavedRecv		= 0;
	INT		RecvOnceByte	= 0;
	recvagain:
	if ((RetTS = IPCAM_TestSocket(SocketFd, TESTSOCKET_READ, waitsec)) == TRUE){
		if (0 < (RecvOnceByte = recv(SocketFd, &buf[HavedRecv], buflen - HavedRecv, 0))){
			HavedRecv += RecvOnceByte;
			if (HavedRecv == buflen){
				return HavedRecv;
			} else {
				RecvOnceByte = 0;
				goto recvagain;
			}
		} else {
			_printd("HavedRecv = %d",HavedRecv);
			return HavedRecv;
		}
	} else{
		_printd("recv error!!%d", RetTS);
		return HavedRecv;//可能是接到一半就接完了
	}	
}

INT IPCAM_SendPacket(INT SocketFd, CHAR *buf, INT buflen, int waitsec)
{
	INT		RetTS			= TRUE;
	INT		HavedSend		= 0;
	INT		SendOnceByte	= 0;
	sendagain:
	if ((RetTS = IPCAM_TestSocket(SocketFd, TESTSOCKET_WRITE, waitsec)) == TRUE){
		if (0 < (SendOnceByte = send(SocketFd, &buf[HavedSend], buflen - HavedSend, 0))){
			HavedSend += SendOnceByte;
			if (HavedSend == buflen){
				return HavedSend;
			} else {
				SendOnceByte = 0;
				goto sendagain;
			}
		} else {
			return -1;
		}
	} else{
		return -1;
	}
}

static int UpdateSendProgress (int sock, int iProgress)
{
	NETMSGPACKET NetMsgPacket = {0};
	INT iRet = 0;
	NetMsgPacket.MsgDevUpdate.head.sync 	= __start_code__;
	NetMsgPacket.MsgDevUpdate.head.msgType 	= MSG_UPDATE_RESP;
	NetMsgPacket.MsgDevUpdate.head.errCode	= 1;
	NetMsgPacket.MsgDevUpdate.head.reserve  = iProgress;
	iRet = IPCAM_SendPacket(sock,(char *)&NetMsgPacket,sizeof(NETMSGPACKET),2);
	_printd("Send Progress iRet = %d", iRet);
	return TRUE;
}

static INT UpdateRecvFile(CLINETNETINFO	*pCli, INT UpdateFileLen)
{
	INT 	WriteFd				= -1;
	CHAR 	RecvFileName[128]	= {0};
	CHAR    strCommand[128]     = {0};
	CHAR 	RecvData[4096]		= {0};
	INT     HaveRecvLen  		= 0;
	INT 	TotalRecvLen 		= 0;
	INT		iCnt				= 0;
	
#ifdef CHIP_S2L
	DIR *dir_d=NULL;
	if((dir_d = opendir(WEB_UPDATE_TMPDIR))==NULL)
	{
		if(mkdir(WEB_UPDATE_TMPDIR, S_IRWXU|S_IRWXG|S_IRWXO)<0){
			perror(WEB_UPDATE_TMPDIR);
			//Le0解决应用程序升级失败并导致app目录被清空机器起不来问题
			//c++ FALSE为0，所以返回值为-1会导致外部调用函数判断本函数是否成功执行出错
			//return -1;
			_printd("mkdir %s error", WEB_UPDATE_TMPDIR);
			//如果不能新建这个文件夹则利用system函数创建，增加下次升级的成功率
			sprintf(RecvFileName, "mkdir %s -p", WEB_UPDATE_TMPDIR);
			system(RecvFileName);
			return FALSE;
	}
	}else{
		closedir(dir_d);
	}
	
#else
	sprintf(RecvFileName, "mkdir %s -p", WEB_UPDATE_TMPDIR);//确保文件夹的存在
			system(RecvFileName);
#endif
	bzero(RecvFileName, sizeof(RecvFileName));
	sprintf (RecvFileName, "%s/%s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME);
	if (0 >= (WriteFd = open(RecvFileName, O_CREAT | O_WRONLY))){
		_printd("open recvfile error");
		return FALSE;
	}
	ftruncate(WriteFd, 0);//将文件截短为0 防止文件之前就存在

	while (1){

		HaveRecvLen = IPCAM_RecvPacket(pCli->client_sock, RecvData, sizeof(RecvData), 5);
		if (HaveRecvLen <= 0){//接包有错误
			_printd("HaveRecvLen = %d",HaveRecvLen);
			goto EXIT_RECV;
		}

		if (HaveRecvLen != write(WriteFd, RecvData, HaveRecvLen)){//写文件有错误
			goto EXIT_RECV;
		}
		TotalRecvLen += HaveRecvLen;
		if (iCnt++ % 1024 == 0){
			_printd("TotalRecfvLen = [%d]", TotalRecvLen);
		}
		if (TotalRecvLen == UpdateFileLen){
			_printd("Recv update file ok");
			close(WriteFd);
			UpdateSendProgress(pCli->client_sock, 5);
			fsync(pCli->client_sock);
			return TRUE;
		}
		HaveRecvLen = 0;
	}
	_printd("recv totalrecvlen = %d filelen=%d", TotalRecvLen, UpdateFileLen);
EXIT_RECV:
	_printd("recv totalrecvlen = %d filelen=%d", TotalRecvLen, UpdateFileLen);
	_printd("recv errror");
	//删除接受错误处理  heyidan
	snprintf(strCommand, sizeof(strCommand), "rm -rf %s", WEB_UPDATE_TMPDIR);
	system(strCommand);
	close(WriteFd);
	return FALSE;
}


void GetSenSorType(char *SensorType)
{
	static char verdata[64] = {0};
	int iRet = 0;
	FILE *fpVer = NULL;
	fpVer = fopen("/mnt/custom/sensortype.ini", "r");
	if (NULL == fpVer) {
		sprintf (verdata, "open ver file error");
		goto ReadOver;
	}
	iRet = fscanf(fpVer, "%s", verdata);
	if(iRet <= 0) {
		sprintf (verdata, "fscanf return var <= 0");
		goto ReadOver;
	}
	fclose(fpVer);
	_printd("read data [%s]", verdata);
ReadOver:
	if (NULL != strcasestr(verdata, "AR0130")){
		sprintf (SensorType, "AR0130");
	}else if(NULL != strcasestr(verdata, "MT9P006")){
		sprintf (SensorType, "MT9P006");
	}else if (NULL != strcasestr(verdata, "OV4689")){
		sprintf (SensorType, "OV4689");
	}else if (NULL != strcasestr(verdata, "OV5658")){
		sprintf (SensorType, "OV5658");
	}else if (NULL != strcasestr(verdata, "AR0230")){
		sprintf (SensorType, "AR0230");		
	}else if (NULL != strcasestr(verdata, "AR0330")){
		sprintf (SensorType, "AR0330");
	}else if (NULL != strcasestr(verdata, "IMX122")){
		sprintf (SensorType, "IMX122");
	}else if (NULL != strcasestr(verdata, "IMX222")){	
		sprintf (SensorType, "IMX222");
	}else if (NULL != strcasestr(verdata, "OV9712")){
		sprintf (SensorType, "OV9712");
	}else if (NULL != strcasestr(verdata, "IMX290")){
		sprintf (SensorType, "IMX290");	
	}else{
		sprintf (SensorType, "Unkown");
	}
	_printd ("Get Sensor is %s", SensorType);
	return;
}
static INT UpdateRecvReq(CLINETNETINFO	*pCli, INT *UpdateFileLen, INT *UpdateFileType)
{
	INT				Ret				= 1;
	NETMSGPACKET	sp				= {0};
	NETMSGPACKET    spTmp 			= {0};
	
	if (sizeof(sp) != IPCAM_RecvPacket(pCli->client_sock, (CHAR *)&sp, sizeof(NETMSGPACKET), 3)){
		return FALSE;
	}
	if (__start_code__ != sp.MsgHead.sync || MSG_UPDATE_REQ != sp.MsgHead.msgType){//包信息不正确
		return FALSE;
	}
	//回一个确认信息
	
	*UpdateFileLen = sp.MsgDevUpdate.DevUpdate.iFileLen;
	*UpdateFileType = sp.MsgDevUpdate.DevUpdate.nFileType;
    _printd("File Type [%d]\n", sp.MsgDevUpdate.DevUpdate.nFileType);
	#if 0
	char SensorMessage[64] = {0};
	GetSenSorType(SensorMessage);
	if (NULL == strstr(sp.MsgDevUpdate.DevUpdate.szUpdateFileName, SensorMessage)){
		_printd("UpdateFileName Error [%s] sensor[%s]", sp.MsgDevUpdate.DevUpdate.szUpdateFileName, SensorMessage);
		Ret = 0;
	}else{
		Ret = 1;
	}
	#endif

	bzero(&spTmp, sizeof(spTmp));
	spTmp.MsgHead.sync = __start_code__;
	spTmp.MsgHead.msgType = MSG_UPDATE_RESP;
	spTmp.MsgHead.errCode = Ret;
	IPCAM_SendPacket(pCli->client_sock, (CHAR *)&spTmp, sizeof(spTmp), 5);
#ifdef CHIP_S2L
	INT 			nErrCnt		  	  = 0;
	UCHAR			*sumBuf 		  = NULL;
	INT 			nFileLen          = 0;
    INT				iRet		      = 0;
	INT 			recvPackSize 	  = 0; //系统包实时接收大小

	SYSPACK_INFO	sysPack;
	UCHAR			packHeadBuf[1024] = {0};
	CHAR			RecvVersion[32]   = {0};
	CHAR			HardVersion[32]   = {0};
	CHAR			CPUVersion[32]	  = {0};
	CHAR			SDKVersion[32]	  = {0};	
	
	struct timeval  wTimeOut;
	fd_set			FdSet;	
	
	/********************************新升级模式-SDK升级*******************************/ //add by jwd 0n 20151124 
	///判断升级包类型: SDK包 | APP包(过渡包)
	if (strstr(sp.MsgDevUpdate.DevUpdate.szUpdateFileName, SDK_NET_UPDATE) != NULL)
	{
		nFileLen = sp.MsgDevUpdate.DevUpdate.iFileLen; 
	    get_hard_cpu_sdk_version(HardVersion, CPUVersion, SDKVersion);
		//NetMakePacket(&sp, MSG_UPDATE_RESP, 0, 0, 0);
		//NetSendPacket(Fd, &sp, SIZEOFNETMSGPACK);

		//gettimeofday(&temptime, NULL);
		//IPCAM_DEBUG("send MSG_UPDATE_REQ time = %d, %d", temptime.tv_sec, temptime.tv_usec);
		
		_printd("mark ");

		FD_ZERO(&FdSet);
		FD_SET(pCli->client_sock, &FdSet);
		wTimeOut.tv_sec  = 10L;
		wTimeOut.tv_usec = 1000L; //SuperJoki 读超时10s+1000us
		nErrCnt = 0;
		sumBuf = (UCHAR *)malloc(65536);
		if (sumBuf == NULL) {
			_printd("Fail to malloc");
		}
		
		if ( 0>= (iRet=select(pCli->client_sock + 1,&FdSet,NULL,NULL,&wTimeOut))) {
			_printd("iRet = %d", iRet);
			if (sumBuf != NULL)
			free(sumBuf);
			return FALSE;
			
		}
		memset(RecvVersion, 0, sizeof(RecvVersion));
		iRet=recv(pCli->client_sock, RecvVersion, sizeof(RecvVersion), 0); //接收硬件版本信息
		_printd("Recv CPUVersion Size == %d", iRet);
		if (iRet <= 0)
		{
		    if (sumBuf != NULL)
			free(sumBuf);
			return FALSE;
		}
		recvPackSize += iRet;
		
		if (strstr(HardVersion, RecvVersion) != NULL) {
			_printd(">>>>>>>>>>>>>>> New Update Mode <<<<<<<<<<<<<<<<");
			//system("rm -rf /app/*");
			while (recvPackSize < nFileLen)
			{
				if ( 0>= (iRet=select(pCli->client_sock + 1,&FdSet,NULL,NULL,&wTimeOut)))
				{
					_printd("iRet = %d", iRet);
					if (nErrCnt++ > 30){
						_printd("send file break off or too slow");
						if (sumBuf != NULL)
						free(sumBuf);
						return FALSE;
					}
					continue;
				}
				memset(packHeadBuf, 0, sizeof(packHeadBuf));	
				if (FD_ISSET(pCli->client_sock, &FdSet)) {
					iRet=recv(pCli->client_sock, packHeadBuf, SIZEOFSYSPACK, 0); //接收SDK升级包头
					_printd("Recv PackHead Size == %d", iRet);
					if (iRet <= 0)
					{
						if (sumBuf != NULL)
						free(sumBuf);
						return FALSE;
					}
					recvPackSize += iRet;
				}
				memset(&sysPack, 0, SIZEOFSYSPACK);
				memcpy(&sysPack, packHeadBuf, SIZEOFSYSPACK);
				_printd("Send SysPack  Type == %d", sysPack.sysPackType);
				
				int MTD[FLASH_NUM]={MTD0, MTD1, MTD2, MTD3, MTD4, MTD5};
				Flash_Print(">>> Begin %d Pack to FLASH /dev/mtd%d<<<\n", sysPack.sysPackType, MTD[sysPack.sysPackType]);
				_printd("SendOnePackSize %d Pack === %d",  sysPack.sysPackType, sysPack.sysPackSize);
				iRet = Recv_SysPack_toFlash(pCli->client_sock, MTD[sysPack.sysPackType], 0, sumBuf, sysPack, &wTimeOut, &FdSet); //接收SDK包数据,并写入至Flash
				if (iRet != sysPack.sysPackSize)
					{
						if (sumBuf != NULL)
						free(sumBuf);
						return FALSE;
					}
				else
					recvPackSize += iRet;
				_printd("RecvOnePackSize %d Pack === %d", sysPack.sysPackType, iRet);
			}	
		
		} else {
			_printd("Fail to match the Hardware!");
			if (sumBuf != NULL)
				free(sumBuf);
			return FALSE;
		}
		_printd("Recv AllPack Size === %d", recvPackSize);
		if (sumBuf != NULL)
			free(sumBuf);

		UpdateSendProgress(pCli->client_sock, 10);
		UpdateSendProgress(pCli->client_sock, 20);
		UpdateSendProgress(pCli->client_sock, 30);
		UpdateSendProgress(pCli->client_sock, 70);
		UpdateSendProgress(pCli->client_sock, 100);
		UpdateSendProgress(pCli->client_sock, 100);
		UpdateSendProgress(pCli->client_sock, 100);
		
		fsync (pCli->client_sock);
		sync();
		usleep(1000*1000);
		_printd("*********upgrade success, reboot*************");
		usleep(200*1000);
		sync();
		//reboot(RB_AUTOBOOT);
		system("reboot -f");
	}
		
#endif
	
	return Ret == 0?FALSE:TRUE;
}
static INT IsCanUpdate()
{
	return TRUE;
}

static INT UpdateCleanUp(INT WhichOperation)
{
	CHAR Command[128];
	_printd("clean up  cmd=%d\n",WhichOperation);
	switch(WhichOperation){
		case CLEANUP_DEL_REBOOT://删除 重启
			sprintf (Command, "rm -rf %s/%s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME);
			system(Command);
			//SystemReboot();   
			system("reboot");   //先用这个替换
			break;
		case CLEANUP_DEL://只删除
			sprintf (Command, "rm -rf %s/%s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME);
			   system(Command);
			break;
		default :
			break;
	}
	_printd("clean up  over\n");
	return TRUE;
}
static INT Update_DelBackUp(INT UpdateFileType)
{
	CHAR Command[128];
	//sprintf (Command, "cp %s/ipcam.conf /app/ipcam -rf",WEB_BACKUPPTHNAME);
	//system	(Command);
	sprintf (Command, "rm -rf %s", WEB_BACKUPPTHNAME);
	if (0 != system(Command)){
		return FALSE;
	}
	return TRUE;
}
static char* cmd_system(const char* command)
{
	char* result = NULL;
	FILE *fpRead;
	fpRead = popen(command, "r");
	if(fpRead)
	{
		char buf[1024];
		memset(buf,'\0',sizeof(buf));
		while(fgets(buf,1024-1,fpRead) != NULL)
		{ 
			result = buf;
		}
		pclose(fpRead);
	}
	return result;
}

static INT UpdateNow(INT *pUpdateFileType, CLINETNETINFO *lpCliInfo)
{
	CHAR UpdatePathName[64] = {0};//24改为64
	CHAR Command[128] = {0};
	switch (*pUpdateFileType){
		case UPDATE_TYPE_APP://应用程序
			sprintf (UpdatePathName, APP_PTHNAME);
            //(FALSE == ConfirmType())                   return   FALSE; 
			break;
		case UPDATE_TYPE_PTZ://ptz
			sprintf (UpdatePathName, PTZ_PTHNAME);
			break;
        case UPDATE_TYPE_ROOTFS:
			#if 0
			bzero(UpdatePathName, sizeof(UpdatePathName));
            sprintf (UpdatePathName, "%s/%s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME);
            if(FALSE == SplitePacket(UpdatePathName))    return FALSE;
            UpdateFileSys(UpdatePathName);
			#endif
			break;
		case UPDATE_TYPE_LINUX://内核
			bzero(UpdatePathName, sizeof(UpdatePathName));
            return FALSE;
			break;
		case UPDATE_TYPE_BOOT://uboot
			bzero(UpdatePathName, sizeof(UpdatePathName));
			break;
		default :
			bzero(UpdatePathName, sizeof(UpdatePathName));
			break;
	}
	if (0 == strlen(UpdatePathName)){
		_printd("pUpdateFileType error %d\n", *pUpdateFileType);
		return FALSE;
	} 
	UpdateSendProgress(lpCliInfo->client_sock, 20);
	fsync(lpCliInfo->client_sock);
	//system("rm -rf /app/*");
	system("rm -rf /app/ipcam.rar");
	UpdateSendProgress(lpCliInfo->client_sock, 30);
	fsync(lpCliInfo->client_sock);
	#ifdef CHIP_S2L
	sprintf (Command, "tar -xvf %s/%s -C %s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME, IPCAM_HOME_DIR);
	#else
	sprintf (Command, "tar -xzvf %s/%s -C %s", WEB_UPDATE_TMPDIR, WEB_UPDATE_FILENAME, IPCAM_HOME_DIR);
	_printd("Commad [%s]",Command);
	#endif

	if (NULL == cmd_system(Command)){//tar -c new 
		_printd("\n\n\ntar error !!!!!\n\n\n");
		return FALSE;
	}
	UpdateSendProgress(lpCliInfo->client_sock, 70);
	fsync(lpCliInfo->client_sock);
	_printd("tar Ok Ok!!!@@@@@####");
	if (0 == access(RECV_IPNCPATH, F_OK)){
		_printd ("#####Move ipnc to /appex!!!!");
		system("rm -rf /appex/ipnc");
		sprintf (Command, "mv %s %s -f", RECV_IPNCPATH, IPNCPATH);
		system(Command);
		sprintf (Command, "chmod 0777 -R %s", IPNCPATH);
		system(Command);
	}
	UpdateSendProgress(lpCliInfo->client_sock, 80);
	fsync(lpCliInfo->client_sock);
	Update_DelBackUp(*pUpdateFileType);//删除备份
	UpdateSendProgress(lpCliInfo->client_sock, 90);
	fsync(lpCliInfo->client_sock);
	return TRUE;
}
static void *TaskUpdata(void *args)
{
	CLINETNETINFO	cliinfo			= {0};
	INT UpdateFileLen			= 0;
	INT UpdateFileType		= -1;
	memcpy(&cliinfo, args, sizeof(CLINETNETINFO));

#if defined (IPC_HISI) || defined(IPC_A3) || defined(IPC_JZ) || defined(IPC_JZ_NEW) || defined(IPC_P2)   //A2不允许 应用程序升级
	goto EXIT_UPDATEFILE;
#endif


	if (FALSE == IsCanUpdate()){//检测是否可以升级  因为升级和jxz那边有冲突
		goto EXIT_UPDATEFILE;
	}
	_printd("Start Update");
	if (FALSE == UpdateRecvReq(&cliinfo, &UpdateFileLen, &UpdateFileType)){//接收请求
		goto EXIT_UPDATEFILE;
	}
	//IPCAM_SetSystemStatus(SYSTEM_VALUE_STATUS_UPDATE);
	_printd("Start Recv File");
	if (FALSE == UpdateRecvFile(&cliinfo, UpdateFileLen)){//接收文件
		_printd("updaterecvfile func error");
		UpdateCleanUp(CLEANUP_DEL);
		goto EXIT_UPDATEFILE;
	}
	//UpdateSend2CliOver(&cliinfo);
	UpdateSendProgress(cliinfo.client_sock, 10);
	fsync(cliinfo.client_sock);
	//IPCAM_SetSystemStatus(SYSTEM_VALUE_STATUS_UPDATE);
	_printd("Start Tar Updatefile");
	//IPCAM_ConfigExportIni(IPCAM_GetSystemContext()->ipcam_SysConfig, CFG_FILE_CONFIGINI_PATH);
	UpdateSendProgress(cliinfo.client_sock, 15);
	fsync(cliinfo.client_sock);
	if (FALSE == UpdateNow(&UpdateFileType, &cliinfo)){//升级
		_printd("updatenow func error");
		//SystemReboot();
		//system("reboot");
		//return NULL;
		goto EXIT_UPDATEFILE;
	}
	UpdateSendProgress(cliinfo.client_sock, 95);
	fsync(cliinfo.client_sock);
	printf("====>>>>>update ===6666>>>>\n\n");

	UpdateCleanUp(CLEANUP_DEL_REBOOT);//升级后的 善后 处理
	UpdateSendProgress(cliinfo.client_sock, 100);
	printf("====>>>>>update ===1111>>>>\n\n");
	fsync(cliinfo.client_sock);
	usleep (100);
	printf("====>>>>>update ===22222>>>>\n\n");
	UpdateSendProgress(cliinfo.client_sock, 100);
	fsync(cliinfo.client_sock);
	printf("====>>>>>update ===3333>>>>\n\n");
	shutdown(cliinfo.client_sock, SHUT_RDWR);
	close(cliinfo.client_sock);
	//pdateMutex = ENABLE;
	printf("====>>>>>update ===5555>>>>\n\n");	
	cmd_system("reboot");
	return NULL;
	
EXIT_UPDATEFILE:
//	gUpdateMutex = ENABLE;
	_printd("ERR:::ipcam_update kill client connect ....");
	close(cliinfo.client_sock);
	return NULL;
}

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
 static INT IsCanUpdatePlayFile()
{
	return TRUE;
}
static INT UpdateRecvPlayFileReq(CLINETNETINFO	*pCli, IDE_AUDIO_FILE *AudioFile)
{
	INT				Ret				= 1;
	NETMSGPACKET	sp				= {0};
	NETMSGPACKET    spTmp 			= {0};
	
	if (sizeof(sp) != IPCAM_RecvPacket(pCli->client_sock, (CHAR *)&sp, sizeof(NETMSGPACKET), 3)){
		_printd("sizeof(sp) error!");
		return FALSE;
	}
	if (__start_code__ != sp.MsgHead.sync || MSG_UPDATE_REQ != sp.MsgHead.msgType){//包信息不正确
		_printd("__start_code__ error!");
		return FALSE;
	}
	//回一个确认信息
	
	AudioFile->size = sp.MsgDevUpdate.DevUpdate.iFileLen;
	AudioFile->type = MP3;
	memcpy(AudioFile->name, sp.MsgDevUpdate.DevUpdate.szUpdateFileName,128);
    _printd("File Type [%d] File name [%s]\n", sp.MsgDevUpdate.DevUpdate.nFileType,sp.MsgDevUpdate.DevUpdate.szUpdateFileName);
	bzero(&spTmp, sizeof(spTmp));
	spTmp.MsgHead.sync = __start_code__;
	spTmp.MsgHead.msgType = MSG_UPDATE_RESP;
	spTmp.MsgHead.errCode = Ret;
	IPCAM_SendPacket(pCli->client_sock, (CHAR *)&spTmp, sizeof(spTmp), 5);
	return Ret == 0?FALSE:TRUE;
}
static INT UpdateRecvPlayFile(CLINETNETINFO	*pCli, IDE_AUDIO_FILE AudioFile)
{
	CHAR 	RecvData[4096]		= {0};
	INT     HaveRecvLen  		= 0;
	INT 	TotalRecvLen 		= 0;
	INT		iCnt				= 0;
	int iRead = 0,offst = 0;
	//IDE_AUDIO_FILE AudioFile;
	while (1){

		HaveRecvLen = IPCAM_RecvPacket(pCli->client_sock, RecvData, sizeof(RecvData), 5);
		if (HaveRecvLen <= 0){//接包有错误
			_printd("HaveRecvLen = %d",HaveRecvLen);
			goto EXIT_RECV;
		}
		if(HaveRecvLen == IdeWriteAudiofile(&AudioFile,offst,RecvData,HaveRecvLen))
		{
			//_printd("offst = %d ,HaveRecvLen = %d",offst,HaveRecvLen);
			offst += HaveRecvLen;
		}
		else
		{
			_printd("IdeWriteAudiofile error!\n");
			goto EXIT_RECV;
		}
		/*if (HaveRecvLen != write(WriteFd, RecvData, HaveRecvLen)){//写文件有错误
			goto EXIT_RECV;
		}*/
		TotalRecvLen += HaveRecvLen;
		if (iCnt++ % 1024 == 0){
			_printd("TotalRecfvLen = [%d]", TotalRecvLen);
		}
		if (TotalRecvLen == AudioFile.size){
			_printd("Recv update Playfile ok");
			//close(WriteFd);
			UpdateSendProgress(pCli->client_sock, 100);
			fsync(pCli->client_sock);
			return TRUE;
		}
		HaveRecvLen = 0;
	}
	_printd("recv totalrecvlen = %d filelen=%d", TotalRecvLen, AudioFile.size);
EXIT_RECV:
	_printd("recv totalrecvlen = %d filelen=%d", TotalRecvLen, AudioFile.size);
	_printd("recv errror");
	IdeDeleteAudiofile(AudioFile);
	//close(WriteFd);
	return FALSE;
}

static void *TaskUpdataPlayFile(void *args)
{
	CLINETNETINFO	cliinfo			= {0};
	INT UpdateFileLen			= 0;
	INT UpdateFileType		= -1;
	IDE_AUDIO_FILE AudioFile;
	memcpy(&cliinfo, args, sizeof(CLINETNETINFO));

	SET_THREAD_NAME("avs_init");
	
	if (FALSE == IsCanUpdatePlayFile()){//检测是否可以更新文件
		goto EXIT_UPDATEFILE;
	}
	_printd("Start Update");
	if (FALSE == UpdateRecvPlayFileReq(&cliinfo, &AudioFile)){//接收请求
		_printd("UpdateRecvPlayFileReq error!");
		goto EXIT_UPDATEFILE;
	}
	//IPCAM_SetSystemStatus(SYSTEM_VALUE_STATUS_UPDATE);
	_printd("Start Recv File");
	if (FALSE == UpdateRecvPlayFile(&cliinfo, AudioFile)){//接收文件
		_printd("updaterecvfile func error");
		goto EXIT_UPDATEFILE;
	}
	//UpdateSend2CliOver(&cliinfo);
	UpdateSendProgress(cliinfo.client_sock, 10);
	fsync(cliinfo.client_sock);
	//IPCAM_SetSystemStatus(SYSTEM_VALUE_STATUS_UPDATE);
	_printd("Start Tar Updatefile");
	//IPCAM_ConfigExportIni(IPCAM_GetSystemContext()->ipcam_SysConfig, CFG_FILE_CONFIGINI_PATH);
	UpdateSendProgress(cliinfo.client_sock, 15);
	fsync(cliinfo.client_sock);
	UpdateSendProgress(cliinfo.client_sock, 95);
	fsync(cliinfo.client_sock);	
	UpdateSendProgress(cliinfo.client_sock, 100);
	fsync(cliinfo.client_sock);
	usleep (100);
	UpdateSendProgress(cliinfo.client_sock, 100);
	fsync(cliinfo.client_sock);
	shutdown(cliinfo.client_sock, SHUT_RDWR);
	close(cliinfo.client_sock);
	return NULL;
	
EXIT_UPDATEFILE:
	_printd("ERR:::PlayFile_update kill client connect ....");
	close(cliinfo.client_sock);
	return NULL;
}

static void *Task_DevUpdataPlayFile(void *noargs)
{
	int ServerSock = CreateUPdateSock(60007);
	if (ServerSock == -1 ){
		_printd ("CreateUpdateSock Error return -1");
		return NULL;
	}
	fd_set             ww_set;
	struct sockaddr_in  client_addr; 
	int 	sockclient = 0;
	char    pzRecBuff[2048]={0};
	NETMSGPACKET      *sp = (NETMSGPACKET *)pzRecBuff;
	struct timeval wtimeout = {0};
	socklen_t addr_len    = sizeof(struct sockaddr);
	int iRet = 0;
	while (1){
		wtimeout.tv_sec =  30L;
        wtimeout.tv_usec = 1000L;
        FD_ZERO(&ww_set);
        FD_SET(ServerSock, &ww_set);
	    iRet=select(ServerSock+1, &ww_set, NULL,NULL,&wtimeout);
		if (iRet == 0){

			_printd ("TaskDevUpdataPlayFile select Server socket timeout !##@@##");
			continue;
		}else if (iRet < 0){
			_printd("TaskDevUpdataPlayFile select Server socket error !##@@##");
			continue;
		}
		if( 0>=(sockclient = accept(ServerSock, (struct sockaddr *) &client_addr, &addr_len))) {
			_printd("TaskDevUpdataPlayFile accept return [value <= 0] iRet = %d", sockclient);
			continue;
		}

		FD_ZERO(&ww_set);
        FD_SET(sockclient, &ww_set);
        wtimeout.tv_sec = 5L;
        wtimeout.tv_usec = 1000L;
        if ( 0>= (iRet =select(sockclient+1, &ww_set,NULL,NULL, &wtimeout)))
        	continue;	
      	if (FD_ISSET(sockclient, &ww_set)){ 
        	if(0>= (iRet=recv(sockclient,( NETMSGPACKET *)sp,SIZEOFNETMSGPACK,0))){
             	close(sockclient);
				continue;
        	}
      	}
		if(sp->MsgHead.sync == __start_code__ && iRet == sizeof(NETMSGPACKET))	{          //自定义协议服务
			if (sp->MsgHead.msgType == MSG_CONNECT_UPDATE){
				sp->MsgHead.msgType = MSG_USERLOGIN_RESP;
				sp->MsgHead.errCode = 1;
				send(sockclient, sp, sizeof(NETMSGPACKET), 0);
				pthread_t temppid;
				//pthread_create(&temppid, NULL, TaskUpdata, &sockclient);
				pthread_create(&temppid, NULL, TaskUpdataPlayFile, &sockclient);
			}else{

			}
        }else{
			_printd("Sorry I Can't kwon recv packet");
		}
		
		usleep (100*1000);
	}

}
#endif
static void *Task_DevUpdata(void *noargs)
{
	int ServerSock = CreateUPdateSock(60006);
	if (ServerSock == -1 ){
		_printd ("CreateUpdateSock Error return -1");
		return NULL;
	}
	fd_set             ww_set;
	struct sockaddr_in  client_addr; 
	int 	sockclient = 0;
	char    pzRecBuff[2048]={0};
	NETMSGPACKET      *sp = (NETMSGPACKET *)pzRecBuff;
	struct timeval wtimeout = {0};
	socklen_t addr_len    = sizeof(struct sockaddr);
	int iRet = 0;
	while (1){
		wtimeout.tv_sec =  30L;
        wtimeout.tv_usec = 1000L;
        FD_ZERO(&ww_set);
        FD_SET(ServerSock, &ww_set);
	    iRet=select(ServerSock+1, &ww_set, NULL,NULL,&wtimeout);
		if (iRet == 0){

			_printd ("TaskDevUpdata select Server socket timeout !##@@##");
			continue;
		}else if (iRet < 0){
			_printd("TaskDevUpdata select Server socket error !##@@##");
			continue;
		}
		if( 0>=(sockclient = accept(ServerSock, (struct sockaddr *) &client_addr, &addr_len))) {
			_printd("TaskDevUpdata accept return [value <= 0] iRet = %d", sockclient);
			continue;
		}

		FD_ZERO(&ww_set);
        FD_SET(sockclient, &ww_set);
        wtimeout.tv_sec = 5L;
        wtimeout.tv_usec = 1000L;
        if ( 0>= (iRet =select(sockclient+1, &ww_set,NULL,NULL, &wtimeout)))
        	continue;	
      	if (FD_ISSET(sockclient, &ww_set)){ 
        	if(0>= (iRet=recv(sockclient,( NETMSGPACKET *)sp,SIZEOFNETMSGPACK,0))){
             	close(sockclient);
				continue;
        	}
      	}
		if(sp->MsgHead.sync == __start_code__ && iRet == sizeof(NETMSGPACKET))	{          //自定义协议服务
			if (sp->MsgHead.msgType == MSG_CONNECT_UPDATE){
				sp->MsgHead.msgType = MSG_USERLOGIN_RESP;
				sp->MsgHead.errCode = 1;
				send(sockclient, sp, sizeof(NETMSGPACKET), 0);
				pthread_t temppid;
				pthread_create(&temppid, NULL, TaskUpdata, &sockclient);
			}else{

			}
        }else{
			_printd("Sorry I Can't kwon recv packet");
		}
		
		usleep (100*1000);
	}

}
int GetMacAddr(char *devSerialNumb, char *macaddr)
{
	char NetMacAddr[32] = {0};
	strncpy(NetMacAddr,   strrchr(devSerialNumb, '-') + 1, 12);

	
	strncpy(macaddr, 	  NetMacAddr, 2);
	strcpy (&macaddr[2],  ":");
	strncpy(&macaddr[3],  &NetMacAddr[2],  2);
	strcpy (&macaddr[5],  ":");
	strncpy(&macaddr[6],  &NetMacAddr[4],  2);
	strcpy (&macaddr[8],  ":");
	strncpy(&macaddr[9],  &NetMacAddr[6],  2);	
	strcpy (&macaddr[11], ":");
	strncpy(&macaddr[12], &NetMacAddr[8],  2);
	strcpy (&macaddr[14], ":");
	strncpy(&macaddr[15], &NetMacAddr[10], 2);
	return 0;
}


static char *GetSoftVersionInfo()
{
#define SOFTVER_FILE_PATH "/app/ipcam/verinfo"
	static char verdata[64];
	int iRet = 0;
	FILE *fpVer = NULL;
	fpVer = fopen(SOFTVER_FILE_PATH, "r");
	if (NULL == fpVer) {
		sprintf (verdata, "open ver file error");
		goto ReadOver;
	}
	iRet = fscanf(fpVer, "%s", verdata);
	if(iRet <= 0) {
		sprintf (verdata, "fscanf return var <= 0");
		fclose(fpVer);
		goto ReadOver;
	}
	fclose(fpVer);
ReadOver:
	return verdata;
}

static char *GetHardVersionInfo()
{
#ifdef CHIP_S2L
    static char HardVersion[64];
	int 	flag 			= 0;
	FILE	*fp_ev			= NULL; // /etc/version
	FILE 	*fp_ea			= NULL; // /etc/ambarella.conf
	FILE 	*fp_as			= NULL; // /appex/custom/sensortype.ini
	char    buf[1024]		= {0};
    memset(buf, 0, sizeof(HardVersion));
	fp_ev = fopen("/etc/version", "r");
	if (fp_ev == NULL) {
		perror("Fail to open /etc/version.");
		flag = 1;
	} else {
		memset(buf, 0, sizeof(buf));
		fread(buf, sizeof(char), sizeof(buf), fp_ev);
		if (strstr(buf, "/") != NULL) {
			sscanf(buf, "%*[^:]:%[^/]", HardVersion);
		} else {
			flag = 1;
		}
		fclose(fp_ev);
	}
	
	if (flag) {
		if (NULL == (fp_ea = fopen("/etc/ambarella.conf", "r"))) {
			perror("Fail to fopen /etc/ambarella.conf");
			return HardVersion;
		}
		memset(buf, 0, sizeof(buf));		
		fread(buf, sizeof(char), sizeof(buf), fp_ea);
		if (strstr(buf, "S2L22M") != NULL) {
			strcpy(HardVersion, "MCS2L22MDV03");
		}
		else if (strstr(buf, "S2L55M") != NULL) {
			strcpy(HardVersion, "MCS2L55MDV03");
		} else {
			if (NULL == (fp_as = fopen("/mnt/custom/sensortype.ini", "r"))) {
				perror("Fail to fopen /mnt/custom/sensortype.ini");
				strcpy(HardVersion, "MCS2L55MDV03");
			} else {
				memset(buf, 0, sizeof(buf));
				fread(buf, sizeof(char), sizeof(buf), fp_as);
				if (strstr(buf, "ar0130") != NULL) {
					strcpy(HardVersion, "MCS2L22MDV03");
				} else {
					strcpy(HardVersion, "MCS2L55MDV03");
				}
				fclose(fp_as);
			}			
		}
		fclose(fp_ea);
	}
	return HardVersion;	
#else
#define HARDVER_FILE_PATH "/etc/version"
	static char verdata[64];
	int iRet = 0;
	FILE *fpVer = NULL;
	fpVer = fopen(HARDVER_FILE_PATH, "r");
	if(NULL ==fpVer){
		sprintf(verdata,"open ver file error");
		goto ReadOver;
	}
	iRet = fscanf(fpVer,"%s",verdata);
	if(iRet <= 0){
		sprintf(verdata, "fscanf return var <=0");
		fclose(fpVer);
		goto ReadOver;
	}
	fclose(fpVer);
ReadOver:
	return verdata;
#endif

}

static void *Task_DevSearch(void *args)
{
	NETMSGPACKET		NetPacket			= {0};
 	int 				SendSocketFd 		= 0;	
	int 				RecvSocketFd 		= 0;
	struct sockaddr_in 	SendSockAddr_in 	= {0};	
	struct sockaddr_in 	RecvSockAddr_in 	= {0};
	socklen_t			AddrLen				= sizeof(struct sockaddr_in);
	unsigned short 		DevSelfCode1 		= 0;
	unsigned short 		DevSelfCode2 		= 0;
	int 				iRet 				= 0;
	SAVENANDINFO_T 		FacInfo = {0};
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	struct sockaddr_in 	SendAddr			= {0};
	struct sockaddr_in 	RecvAddr			= {0};
	struct sockaddr_in 	eth0Addr			= {0};
	struct sockaddr_in 	ra0Addr				= {0};
	CHAR				eth0IpAddr[16]		= {0};
	CHAR				ra0IpAddr[16]		= {0};
	CHAR				tmpNetMask[16]		= {0};
	CHAR				tmpEth0IpAddr[16]	= {0};
	CHAR				tmpRa0IpAddr[16]	= {0};
	INT 				Ra0SendSocketFd		= -1;
	INT 				Ra0RecvSocketFd		= -1;
    INT                 MaxRecvSocketFd     = -1; 
	struct timeval		TimeOut;
	fd_set				FdSet;
	struct tm *t;
	time_t timetmp;
	IPInfo info;
	static int 			iVolume = 80;
#endif

	if (0 != CreateSendSocketFd(&SendSocketFd, &SendSockAddr_in)){
		_printd("CreateSendSocketFd return error\n");
	}
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	//g_NetApp.GetNetIPInfo("eth0", &info);
	//sprintf (eth0IpAddr, "%d.%d.%d.%d", info.HostIP.c[0], info.HostIP.c[1], info.HostIP.c[2], info.HostIP.c[3]);
#if	defined(BPI_S2L) || defined(IPC_HISI)|| defined(IPC_A3)|| defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2) 
	NetGetHostIP("wlan0", eth0IpAddr, sizeof(eth0IpAddr), tmpNetMask, sizeof(tmpNetMask));
#else 
	NetGetHostIP("eth0", eth0IpAddr, sizeof(eth0IpAddr), tmpNetMask, sizeof(tmpNetMask));
#endif
	eth0Addr.sin_family 	= AF_INET;
	eth0Addr.sin_port 		= htons(PORT_UDP_RESP);
	inet_pton(AF_INET, eth0IpAddr, &(eth0Addr.sin_addr));
	bind(SendSocketFd, (struct sockaddr *)&eth0Addr, sizeof(struct sockaddr));
	ra0Addr.sin_family	 	= AF_INET;
	ra0Addr.sin_port 		= htons(PORT_UDP_RESP);
#endif
	if (0 != CreateRecvSocketFd(&RecvSocketFd, &RecvSockAddr_in, htonl(INADDR_ANY))){

		_printd("CreateRecvSocketFd return error\n");
	}
				
	GetDevSelfCode(&DevSelfCode1, &DevSelfCode2);
	_printd("Task_DevSearch is Running@@##@@\n");
	ReadFacInfo(&FacInfo);
	char softwarever[32] = {0};
	sprintf (softwarever, GetSoftVersionInfo());

	char hardwarever[32] = {0};
	sprintf (hardwarever, GetHardVersionInfo());
#if 0 
	if (NULL == strcasestr(FacInfo.sensor, "IMX122")
		&& NULL == strcasestr(FacInfo.sensor, "OV4689")
		&& NULL == strcasestr(FacInfo.sensor, "OV5658")
		&& NULL == strcasestr(FacInfo.sensor, "AR0230")
		&& NULL == strcasestr(FacInfo.sensor, "AR0331")
		&& NULL == strcasestr(FacInfo.sensor, "AR0330")
		&& NULL == strcasestr(FacInfo.sensor, "AR0130")
		&& NULL == strcasestr(FacInfo.sensor, "OV9712")
		&& NULL == strcasestr(FacInfo.sensor, "MT9P006")){
		char SensorType[128] = {0};
		GetSenSorType(SensorType);
		sprintf (FacInfo.sensor, "%s", SensorType);
		_printd ("Write nand sensor info [%s]", FacInfo.sensor);
		WriteFacInfo(&FacInfo);
	}else{
		_printd ("read fac info sensor[%s]", FacInfo.sensor);
	}
#else       
		char SensorType[128] = {0};
		GetSenSorType(SensorType);
		sprintf (FacInfo.sensor, "%s", SensorType);
		_printd ("Write nand sensor info [%s]", FacInfo.sensor);

		char macAddr[24] = {0};
#if	defined(BPI_S2L) ||defined(IPC_HISI) ||defined(IPC_A3) ||defined(IPC_JZ) ||defined(IPC_JZ_NEW) ||defined(IPC_P2)
		NetGetMAC("wlan0", macAddr, sizeof(macAddr));
#else
		NetGetMAC("eth0", macAddr, sizeof(macAddr));
#endif 

		sprintf(FacInfo.NetMacAddr,"%s",macAddr);

		_printd("Get Local MacAddr is %s\n",FacInfo.NetMacAddr);	
		
		WriteFacInfo(&FacInfo);
#endif 

	while (1){
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
		TimeOut.tv_sec  = 10;
		TimeOut.tv_usec = 1000;
		FD_ZERO(&FdSet);
		FD_SET(RecvSocketFd, &FdSet);

        memset(tmpRa0IpAddr, 0, sizeof(tmpRa0IpAddr));
		//NetGetHostIP("wlan0", tmpRa0IpAddr, sizeof(tmpRa0IpAddr), tmpNetMask, sizeof(tmpNetMask));
		strcpy(tmpRa0IpAddr, "192.168.3.1");
        if(0 != strcmp(ra0IpAddr, tmpRa0IpAddr))
        {
            close(Ra0RecvSocketFd);
            strcpy(ra0IpAddr, tmpRa0IpAddr);
            CreateRecvSocketFd(&Ra0RecvSocketFd, &RecvAddr, inet_addr(ra0IpAddr));

            close(Ra0SendSocketFd);
			CreateSendSocketFd(&Ra0SendSocketFd, &SendAddr);
			inet_pton(AF_INET, ra0IpAddr, &(ra0Addr.sin_addr));
			bind(Ra0SendSocketFd, (struct sockaddr *)&ra0Addr, sizeof(struct sockaddr));
        }
        MaxRecvSocketFd = RecvSocketFd + 1;
        if(Ra0RecvSocketFd > 0){
            FD_SET(Ra0RecvSocketFd, &FdSet);
            if(Ra0RecvSocketFd > RecvSocketFd){
                MaxRecvSocketFd = Ra0RecvSocketFd + 1;
            }
        }
        
		if(0 >= (iRet = select(MaxRecvSocketFd, &FdSet, 0, 0, &TimeOut)))
		{
			continue;
		}
#endif
		memset(&NetPacket, 0, sizeof(NetPacket));	
			iRet = recvfrom(RecvSocketFd, (NETMSGPACKET *)&NetPacket,sizeof(NETMSGPACKET),0,(struct sockaddr  *)&RecvSockAddr_in, &AddrLen);
		if( iRet > 0 && (iRet != sizeof(NETMSGPACKET) || NetPacket.MsgHead.sync != __start_code__ )){
			_printd("NetDev Search Recv a unknow packet recv len = %d, sync = %x", iRet, NetPacket.MsgHead.sync);
			usleep (200*1000);
			continue;
		}else if( iRet < 0 ){
			usleep (200*1000);
			continue;
		}else if( iRet == 0 ){
			usleep (200*1000);
			continue;
		}
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
   		memset(tmpEth0IpAddr, 0, sizeof(tmpEth0IpAddr));
#if	defined(BPI_S2L) || defined(IPC_HISI) || defined(IPC_A3) || defined(IPC_JZ) ||defined(IPC_JZ_NEW) ||defined(IPC_P2) 
		NetGetHostIP("wlan0", tmpEth0IpAddr, sizeof(tmpEth0IpAddr), tmpNetMask, sizeof(tmpNetMask));
#else
		NetGetHostIP("eth0", tmpEth0IpAddr, sizeof(tmpEth0IpAddr), tmpNetMask, sizeof(tmpNetMask));
#endif
		if(0 != strcmp(eth0IpAddr, tmpEth0IpAddr))
		{
			close(SendSocketFd);
			CreateSendSocketFd(&SendSocketFd, &SendAddr);
			strcpy(eth0IpAddr, tmpEth0IpAddr);
			inet_pton(AF_INET, eth0IpAddr, &(eth0Addr.sin_addr));
			bind(SendSocketFd, (struct sockaddr *)&eth0Addr, sizeof(struct sockaddr));
		}
   		memset(tmpRa0IpAddr, 0, sizeof(tmpRa0IpAddr));
		//NetGetHostIP("wlan0", tmpRa0IpAddr, sizeof(tmpRa0IpAddr), tmpNetMask, sizeof(tmpNetMask));
		strcpy(tmpRa0IpAddr, "192.168.3.1");
		if(0 != strcmp(ra0IpAddr, tmpRa0IpAddr))
		{
            close(Ra0RecvSocketFd);
            strcpy(ra0IpAddr, tmpRa0IpAddr);
            CreateRecvSocketFd(&Ra0RecvSocketFd, &RecvAddr, inet_addr(ra0IpAddr));
            
			close(Ra0SendSocketFd);
			CreateSendSocketFd(&Ra0SendSocketFd, &SendAddr);
			inet_pton(AF_INET, ra0IpAddr, &(ra0Addr.sin_addr));
			bind(Ra0SendSocketFd, (struct sockaddr *)&ra0Addr, sizeof(struct sockaddr));
		}
		_printd("Ra0SendSocketFd = %d, ra0IpAddr = %s", Ra0SendSocketFd, ra0IpAddr);
		_printd("DevSelfCode = %lu, subType = %u", DevSelfCode1, NetPacket.MsgHead.subType);
#endif
       // if( NetPacket.MsgHead.msgType == MSG_SEARCHQUERY_REQ && NetPacket.MsgDevSearch.DevSearchInfo.DevSerachToolVersions == __DEF_DEVSEARCHTOOL_VERSIONS){
        if( NetPacket.MsgHead.msgType == MSG_SEARCHQUERY_REQ){
		//设备搜索;
			IPInfo info;
			char DNSI[20] = {0}, DNSII[20] = {0}, MAC[20];
			NetGetDNSAddress(DNSI, sizeof(DNSI), DNSII, sizeof(DNSII));
		#if	defined(BPI_S2L)|| defined(IPC_HISI)|| defined(IPC_A3) || defined(IPC_JZ)||defined(IPC_JZ_NEW)|| defined(IPC_P2)
			NetGetMAC("wlan0", MAC, sizeof(MAC));
			g_NetApp.GetNetIPInfo("wlan0", &info);
		#else  //xxx r2p2
			//NetGetMAC("eth0", MAC, sizeof(MAC));
			//g_NetApp.GetNetIPInfo("eth0", &info);
			//A2
			NetGetMAC("wlan0", MAC, sizeof(MAC));
			g_NetApp.GetNetIPInfo("wlan0", &info);			
		#endif
						
			ReadFacInfo(&FacInfo);
			NetPacket.MsgDevSearch.DevSearchInfo.iServerPort = 			60006;
			sprintf ((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr, "%d.%d.%d.%d", info.HostIP.c[0], info.HostIP.c[1], info.HostIP.c[2], info.HostIP.c[3]);
			sprintf ((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetGwAddr, "%d.%d.%d.%d", info.GateWay.c[0], info.GateWay.c[1], info.GateWay.c[2], info.GateWay.c[3]);
			sprintf ((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetSubMask, "%d.%d.%d.%d", info.SubMask.c[0], info.SubMask.c[1], info.SubMask.c[2], info.SubMask.c[3]);
			//strcpy ((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr, FacInfo.NetMacAddr);
			strcpy ((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr, macAddr);
			_printd ("dev search use mac addr [%s]", FacInfo.NetMacAddr);
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvI, 		DNSI);
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvII, 	DNSII);          
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.devModel, 		FacInfo.devModel);
			memcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.devSerialNumb, 	( char *)FacInfo.devSerialNumb, sizeof(FacInfo.devSerialNumb));
			memcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.devRegcode, 	( char *)FacInfo.devRegcode, sizeof(FacInfo.devRegcode));
			memcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.devName, 		( char *)FacInfo.devName, sizeof(FacInfo.devName));
			memcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.szFactoryName,  ( char *)FacInfo.szFactoryName, sizeof(FacInfo.szFactoryName));
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.HardWareVersion,hardwarever);//1138
			//memcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.HardWareVersion,( char *)&FacInfo.FactoryTime, sizeof(FacInfo.FactoryTime));
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.SoftWareVersion,softwarever);
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.sensor,			FacInfo.sensor);
			NetPacket.MsgDevSearch.DevSearchInfo.LogoType = FacInfo.LogoType;
			NetPacket.MsgDevSearch.DevSearchInfo.nVideoChannel			= 1;
			NetPacket.MsgDevSearch.DevSearchInfo.bIsVideoSubChannel		= 1;
			NetPacket.MsgDevSearch.DevSearchInfo.nAudioChannel			= 1;
			NetPacket.MsgDevSearch.DevSearchInfo.WifiOFF				= FacInfo.WifiOFF;
			NetPacket.MsgDevSearch.DevSearchInfo.SDOFF					= FacInfo.SDOFF;
			NetPacket.MsgDevSearch.DevSearchInfo.AudioInOFF				= FacInfo.AudioInOFF;
			NetPacket.MsgDevSearch.DevSearchInfo.AudioOutOFF			= FacInfo.AudioOutOFF;
			NetPacket.MsgDevSearch.DevSearchInfo.IOOFF					= FacInfo.IOOFF;

			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.PTPUUID, FacInfo.PTPUUID);
			strcpy((char *)NetPacket.MsgDevSearch.DevSearchInfo.PTPCMEI, FacInfo.PTPCMEI);
			NetPacket.MsgDevSearch.DevSearchInfo.ThreeMachineSupport = FacInfo.ThreeMachineSupport;
			NetPacket.MsgHead.subType = DevSelfCode1;
			NetPacket.MsgHead.len     = DevSelfCode2;
			_printd("code [%x][%x]", DevSelfCode1, DevSelfCode2);
			NetPacket.MsgHead.msgType  = MSG_SEARCHQUERY_RESP;
			sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen);
            perror("Sendto");
			_printd("Recv a NetPack From Ip[%s] port[%d] MSG_TYPE [Dev Search]", inet_ntoa(RecvSockAddr_in.sin_addr), ntohs(RecvSockAddr_in.sin_port));
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
			if(Ra0SendSocketFd >= 0)
			{
				strcpy((char*)NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr, ra0IpAddr);
				//get_wifi_gateway("wlan0",(char*)NetPacket.MsgDevSearch.DevSearchInfo.NetGwAddr);
				//pox_get_netmask("wlan0", (char*)NetPacket.MsgDevSearch.DevSearchInfo.NetSubMask);
				//pox_get_macaddr("wlan0", (char*)NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr);
				_printd("IP %s gw %s mask %s mac %s", NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr, NetPacket.MsgDevSearch.DevSearchInfo.NetGwAddr, 
					NetPacket.MsgDevSearch.DevSearchInfo.NetSubMask, NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr);
				_printd("DnsI %s DnsII %s", NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvI, NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvII);
				sendto(Ra0SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendAddr, AddrLen);
			}
#endif			
		}else if (NetPacket.MsgHead.msgType == MSG_NETPARMSET_REQ && 0 == strcasecmp((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr, FacInfo.NetMacAddr)){
  			_printd ("Dev Tools Set Dev NetInfo\n");
			//设备搜索工具设置网络参数时启用了DHCP会无法设置成功，所以先关闭DHCP Le0 20140729
			NetSetDhcpEnable(0);
			NetPacket.MsgHead.msgType = MSG_NETPARMSET_RESP;
			sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
			perror("sendto");
			
			IPInfo info;
			info.iDHCP = 0;
			info.HostIP.l 	= Str2Ip((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr);
			info.GateWay.l	= Str2Ip((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetGwAddr);
			info.SubMask.l  = Str2Ip((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetSubMask);
			#if	defined(BPI_S2L)|| defined(IPC_HISI)|| defined(IPC_A3) ||defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2) 
			g_NetApp.SetNetIPInfo("wlan0", &info);
			#else//r2p2
			g_NetApp.SetNetIPInfo("eth0", &info);
			#endif
			
			NetSetDNSAddress((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvI, (char *)NetPacket.MsgDevSearch.DevSearchInfo.NetDnsSvII);

		}
#ifdef VSERVER   //add by kyle xu in 20160107
		else if (NetPacket.MsgHead.msgType == MSG_VSERVERIPSET_REQ &&
						 DevSelfCode1 == NetPacket.MsgHead.subType && 
						  DevSelfCode2 == NetPacket.MsgHead.len)
		{
			//Add by kyle xu in 20151221
            //增加VServer的服务器ip port的配置
            
			_printd ("Dev Tools Set Dev Vserver\n");
			NetPacket.MsgHead.msgType = MSG_VSERVERIPSET_RESP;
			iRet = sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
			if(iRet < 0)
			{
				perror("sendto");
			}

			CConfigNetVServer *pCfgNetVServer = new CConfigNetVServer();
			
	        pCfgNetVServer->update();    

			CONFIG_NET_VSERVER &cfgVServer = pCfgNetVServer->getConfig();
	        
			cfgVServer.Server.ip.l = Str2Ip((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr);
			cfgVServer.Server.Port = NetPacket.MsgDevSearch.DevSearchInfo.iServerPort;
			
		    pCfgNetVServer->commit();
		    delete pCfgNetVServer; 
			
			_printd("###########DevTool set VServer ip:%s port:%d\n",
					NetPacket.MsgDevSearch.DevSearchInfo.NetIpAddr,
					NetPacket.MsgDevSearch.DevSearchInfo.iServerPort);
			
		}
#endif	
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
		else if( NetPacket.MsgHead.msgType == MSG_AUDIO_FILELIST_REQ //音频文件列表请求
				&& DevSelfCode1 == NetPacket.MsgHead.subType 
				&& DevSelfCode2 == NetPacket.MsgHead.len){
			_printd("---------------a--------------");
			NetPacket.MsgHead.msgType = MSG_AUDIO_FILELIST_RESP;
			NetPacket.MsgHead.sync    = __start_code__;
			NetPacket.MsgHead.subType = DevSelfCode1;
			NetPacket.MsgHead.len     = DevSelfCode2;

			int i, iRet;
			IDE_AUDIO_FILE AudioFile[10];
/*			char **AudioFileName;	
				 
			AudioFileName = (char **)malloc(sizeof(char*)*10);	  
			for(i=0; i<10; i++)	
			{		
				AudioFileName[i] = (char *)malloc(sizeof(char)*32);	 
				memset(AudioFileName[i],0,32);
			}
*/
			iRet = IdeGetAudiofiles(10, AudioFile);
			
			int tmp = IdeGetAudiofileNum();
			//iRet = AudioGetAudioFiles("/app/media", 10, AudioFileName, 32, MP3);
			_printd("iRet = %d  tmp = %d ======",iRet,tmp);
			if(iRet > 0)
			{
				NetPacket.MsgAudioFileList.file.iCount = iRet;
				_printd("----------NetPacket.MsgAudioFileList.file.iCount = %d",iRet);
				for(i=0; i < iRet; i++)	
				{		
					
					memcpy(NetPacket.MsgAudioFileList.file.FileName[i],AudioFile[i].name,32);
					//AudioFileName[i] = (char *)malloc(sizeof(char)*32);	 
					_printd("AudioFileName[%d] = %s",i,NetPacket.MsgAudioFileList.file.FileName[i]);
				}
				
			}
			else
			{
				NetPacket.MsgAudioFileList.file.iCount = -1;
			}
			/*for(i=0; i<10; i++)    
			{        
				free(AudioFileName[i]);    
			}    
			free(AudioFileName);*/

		//	NetPacket.MsgAudioFileList.file.iCount = 2;
		//	memcpy(NetPacket.MsgAudioFileList.file.FileName[0],"aaa.cc",32);
		//	memcpy(NetPacket.MsgAudioFileList.file.FileName[1],"aaas.cc",32);
			sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
			perror("sendto");
			sendto(Ra0SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 
			perror("sendto");
		}
		else if( NetPacket.MsgHead.msgType == MSG_PLAY_AUDIO_FILE_REQ //音频文件播放请求
				&& DevSelfCode1 == NetPacket.MsgHead.subType 
				&& DevSelfCode2 == NetPacket.MsgHead.len){	

			int i = 0,iRet = 0;
			IDE_AUDIO_FILE AudioFile[10];
			_printd("NetPacket.MsgPlayAudioFile.play.iStatus = %d",NetPacket.MsgPlayAudioFile.play.iStatus);
			if(STARTPLAY == NetPacket.MsgPlayAudioFile.play.iStatus)//播放
			{
				if(AudioPlayAudioStatus())
				{
					AudioPlayAudioStop();	
				}
				_printd("play file name is %s",NetPacket.MsgPlayAudioFile.play.FileName);
				char PlayName[255];
				memset(PlayName,0,255);
				sprintf(PlayName,"%s",NetPacket.MsgPlayAudioFile.play.FileName);

				iRet = IdeGetAudiofiles(10, AudioFile);		
				if(iRet > 0)
				{
					for(i = 0; i < iRet; i++)	
					{						
						if(!strncmp(PlayName,AudioFile[i].name,strlen(PlayName)))
						{
							AudioPlayIdeAudioStart(AudioFile[i], 0);
							sleep(3);
							AudioOutSetVolume( 0, iVolume, 0);
							_printd("AudioFileName[%d] = %s",i,NetPacket.MsgAudioFileList.file.FileName[i]);	
							break;
						}
						
					}
					
				}
				if(0 == iRet || i == iRet)
				{
					_printd(" FILE: %s is not exist !!!!!",PlayName);
				}
				/*if( 0 == access(PlayName, 0))
				{
					
					_printd("Start play %s",PlayName);					
					AudioPlayAudioStart(PlayName, MP3, 0);
					//AudioOutSetVolume( 0, iVolume, 0);
					
					AudioOutSetVolume( 0, iVolume, 0);
				}
				else
				{
					_printd(" FILE: %s is not exist !!!!!",PlayName);
				}*/
			}
			else if(STOPPLAY == NetPacket.MsgPlayAudioFile.play.iStatus)//停止
			{
				if(AudioPlayAudioStatus())
				{
					AudioPlayAudioStop();	
				}


			}
			else if(VOLUMEUP == NetPacket.MsgPlayAudioFile.play.iStatus)//音量加
			{
				_printd("=======iVolume ++= %d=====",iVolume);
				iVolume += 5;
				if(iVolume >= 100)
				{
					iVolume = 100;
				}
				AudioOutSetVolume( 0, iVolume, 0);
			}
			else if(VOLUMEDOWN == NetPacket.MsgPlayAudioFile.play.iStatus)//音量减
			{
				_printd("=======iVolume --= %d=====",iVolume);
				iVolume -= 5;
				if(iVolume < 0)
				{
					iVolume = 0;
				}
				AudioOutSetVolume( 0, iVolume, 0);
			}
				
		}
#endif
		else if (NetPacket.MsgHead.msgType == MSG_DEVICEOPER_CMD_REQ 
				&& DevSelfCode1 == NetPacket.MsgHead.subType && DevSelfCode2 == NetPacket.MsgHead.len){
				//&& 0 == strcmp(NetPacket.MsgDevCmd.DevMacAddr, FacInfo.NetMacAddr)
			_printd("packet mac addr [%s] facinfo mac addr [%s]", NetPacket.MsgDevCmd.DevMacAddr, FacInfo.NetMacAddr);
			NetPacket.MsgHead.msgType = MSG_DEVICEOPER_CMD_RESP;
			NetPacket.MsgHead.errCode = 1;
			sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
			_printd("##############cmd######");
			printf("%s\n", NetPacket.MsgDevCmd.cmd);
			_printd("##############cmd print over##");
			system(NetPacket.MsgDevCmd.cmd);
			_printd("##############end cmd##");
		}else if( NetPacket.MsgHead.msgType == MSG_FACTORYINFOSET_REQ 
				&& DevSelfCode1 == NetPacket.MsgHead.subType && DevSelfCode2 == NetPacket.MsgHead.len){
		//厂商信息设置
			FacInfo.AudioInOFF	= NetPacket.MsgDevSearch.DevSearchInfo.AudioInOFF;
			FacInfo.AudioOutOFF	= NetPacket.MsgDevSearch.DevSearchInfo.AudioOutOFF;
			sprintf (FacInfo.devModel, (char *)NetPacket.MsgDevSearch.DevSearchInfo.devModel);
			sprintf (FacInfo.devName, (char *)NetPacket.MsgDevSearch.DevSearchInfo.devName);
			FacInfo.devNo	= 0;
			sprintf (FacInfo.devRegcode, (char *)NetPacket.MsgDevSearch.DevSearchInfo.devRegcode);
			FacInfo.FactoryTime = NetPacket.MsgDevSearch.DevSearchInfo.FactoryTime;
			FacInfo.IOOFF 	= NetPacket.MsgDevSearch.DevSearchInfo.IOOFF;
			FacInfo.LogoType = NetPacket.MsgDevSearch.DevSearchInfo.LogoType;
			if(NULL == strrchr((char*)NetPacket.MsgDevSearch.DevSearchInfo.devSerialNumb, '-')){
				#if	defined(BPI_S2L) ||defined(IPC_HISI) || defined(IPC_A3)|| defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)
				NetGetMAC("wlan0", FacInfo.NetMacAddr, sizeof(FacInfo.NetMacAddr));
				#else//r2p2
				NetGetMAC("eth0", FacInfo.NetMacAddr, sizeof(FacInfo.NetMacAddr));
				#endif
				
				sprintf((char*)FacInfo.devSerialNumb, "RL01-T368120808-%c%c%c%c%c%c%c%c%c%c%c%c123456", 
									FacInfo.NetMacAddr[0], FacInfo.NetMacAddr[1], FacInfo.NetMacAddr[3], FacInfo.NetMacAddr[4], FacInfo.NetMacAddr[6], FacInfo.NetMacAddr[7], FacInfo.NetMacAddr[9], FacInfo.NetMacAddr[10], 
									FacInfo.NetMacAddr[12], FacInfo.NetMacAddr[13], FacInfo.NetMacAddr[15], FacInfo.NetMacAddr[16]);
			}else{
				sprintf (FacInfo.devSerialNumb, (char *)NetPacket.MsgDevSearch.DevSearchInfo.devSerialNumb);
				char macaddr[24];
				GetMacAddr((char *)NetPacket.MsgDevSearch.DevSearchInfo.devSerialNumb, macaddr);
				sprintf (FacInfo.NetMacAddr, (char *)macaddr);
			}
			_printd ("[%s]",FacInfo.NetMacAddr);
			sprintf (FacInfo.PTPUUID, (char *)NetPacket.MsgDevSearch.DevSearchInfo.PTPUUID);
			sprintf (FacInfo.PTPCMEI, (char *)NetPacket.MsgDevSearch.DevSearchInfo.PTPCMEI);

			if(CheckUuid(&FacInfo) < 0)  //20170111 by fyj
			{
				printf("UUID or CMEI is error\n ");
			}
			else
			{		
				FacInfo.SDOFF	= NetPacket.MsgDevSearch.DevSearchInfo.SDOFF;
				sprintf (FacInfo.sensor, (char *)NetPacket.MsgDevSearch.DevSearchInfo.sensor);
				_printd ("%s", FacInfo.sensor);
				sprintf (FacInfo.szFactoryName, (char *)NetPacket.MsgDevSearch.DevSearchInfo.szFactoryName);
				FacInfo.ThreeMachineSupport = NetPacket.MsgDevSearch.DevSearchInfo.ThreeMachineSupport;
				FacInfo.WifiOFF  = NetPacket.MsgDevSearch.DevSearchInfo.WifiOFF;
				_printd ("PTPUUID=%s PTPCMEI=%s \n", FacInfo.PTPUUID, FacInfo.PTPCMEI);
				NetPacket.MsgHead.msgType = MSG_FACTORYINFOSET_RESP;
				NetPacket.MsgHead.errCode = 1;
				sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
				_printd ("Set Mac Addr [%s]", FacInfo.NetMacAddr);
				int mac_flag = 0;//macaddr diff reboot
				CConfigNetCommon *pCfgNetCommon = new CConfigNetCommon();
		        pCfgNetCommon->update();    
		        CONFIG_NET_COMMON& cfgComm = pCfgNetCommon->getConfig();
				strcpy(cfgComm.HostName , FacInfo.devName); 
				pCfgNetCommon->commit();
				delete pCfgNetCommon; 
				_printd("+++++++++++++++++++++++cfgComm.HostName = %s",cfgComm.HostName);
#ifdef CHIP_A5S
				char MAC[24];
				#if	defined(BPI_S2L)|| defined(IPC_HISI)|| defined(IPC_A3)|| defined(IPC_JZ)||defined(IPC_JZ_NEW) ||defined(IPC_P2)
				NetGetMAC("wlan0", MAC, sizeof(MAC));
				#else //r2p2
				NetGetMAC("eth0", MAC, sizeof(MAC));
				#endif
				
				if (0 != strcasecmp(FacInfo.NetMacAddr, MAC)){
					mac_flag = 1;
					#if	defined(BPI_S2L)|| defined(IPC_HISI)|| defined(IPC_A3)|| defined(IPC_JZ)||defined(IPC_JZ_NEW) ||defined(IPC_P2)
					mac_flag = 0;
					//NetSetMAC("wlan0", FacInfo.NetMacAddr);
					#else//r2p2
					NetSetMAC("eth0", FacInfo.NetMacAddr);
					#endif
				}
#endif

#ifdef CHIP_S2L
				char MAC[24];
				#if	defined(BPI_S2L) ||defined(IPC_HISI) ||defined(IPC_A3) || defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)
				NetGetMAC("wlan0", MAC, sizeof(MAC));
				#else//r2p2
				NetGetMAC("eth0", MAC, sizeof(MAC));
				#endif
				
				if (0 != strcasecmp(FacInfo.NetMacAddr, MAC)){
					mac_flag = 1;
					#if	defined(BPI_S2L) ||defined(IPC_HISI) ||defined(IPC_A3) ||defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)
					mac_flag = 0;
					//NetSetMAC("wlan0", FacInfo.NetMacAddr);
					#else//r2p2
					NetSetMAC("eth0", FacInfo.NetMacAddr);
					#endif
					
				}
#endif


#ifdef CHIP_3516
				#if	defined(BPI_S2L) || defined(IPC_HISI)
				NetSetMAC("wlan0", FacInfo.NetMacAddr);	
				#else
				NetSetMAC("eth0", FacInfo.NetMacAddr);	
				#endif
				
#endif			
				WriteFacInfo(&FacInfo);
				if(mac_flag){  //修改mac地址重启才生效
					SystemReboot();
				}
			}
		}else if (MSG_REBOOT_REQ == NetPacket.MsgHead.msgType  && 0 == strcasecmp((char *)NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr, FacInfo.NetMacAddr)){
			NetPacket.MsgHead.msgType = MSG_REBOOT_RESP;
			NetPacket.MsgHead.errCode = 1;
			sendto(SendSocketFd, (NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET), SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in, AddrLen); 			  
			perror("sendto");
			usleep (800*1000);
			SystemReboot();
		}else if(MSG_CLOCKSET_REQ == NetPacket.MsgHead.msgType&& 0 == strcasecmp((char*)NetPacket.MsgClockSet.DevMacAddr,FacInfo.NetMacAddr)){

			long t=NetPacket.MsgClockSet.time+NetPacket.MsgClockSet.tz*3600;
			struct tm *current_time = gmtime((time_t*)&t);
			SYSTEM_TIME szTime;
	        szTime.year    = current_time->tm_year + 1900;
	        szTime.month    = current_time->tm_mon + 1; //linux mon 0~11
	        szTime.day        = current_time->tm_mday;
	        szTime.hour    = current_time->tm_hour;
	        szTime.minute    = current_time->tm_min;
	        szTime.second    = current_time->tm_sec;
	        g_General.UpdateSystemTime(&szTime, 2);
			NetPacket.MsgHead.msgType=MSG_CLOCKSET_RESP;
			NetPacket.MsgClockSet.time=0;
			sendto(SendSocketFd,(NETMSGPACKET *)&NetPacket, sizeof(NETMSGPACKET),SIZEOFNETMSGPACK,(struct sockaddr*) &SendSockAddr_in,AddrLen);
		}else{
			_printd("Recv a Unkown Packet\n NetPacket MsgType = 0X%04x \nDevSearch Versi = %02x \nForm Ip[%s]\nPacketMacAddr[%s]\n code[%x][%x]", 
				NetPacket.MsgDevSearch.head.msgType, 
				NetPacket.MsgDevSearch.DevSearchInfo.DevSerachToolVersions, 
				inet_ntoa(RecvSockAddr_in.sin_addr),
				NetPacket.MsgDevSearch.DevSearchInfo.NetMacAddr,
				DevSelfCode1, DevSelfCode2);
		}
	}
//SCH_SERVER_THR_EXIT:
    if( SendSocketFd >= 0 ){
        shutdown(SendSocketFd, SHUT_RDWR);
        close(SendSocketFd);
		shutdown(RecvSocketFd, SHUT_RDWR);
		close(RecvSocketFd);
    }
	_printd("PTH_NetDevFound Exit");
    pthread_exit(NULL);
	return NULL;
}

PATTERN_SINGLETON_IMPLEMENT(CDevTools);
CDevTools::CDevTools()
{
}

CDevTools::~CDevTools()
{
}

int CDevTools::Start()
{
 	return 0; //屏蔽应用程序搜索，新疆项目必须屏蔽才能规避连不上服务器问题
	pthread_t pid;
	pthread_create(&pid, NULL, Task_DevSearch, NULL);
//#ifndef IPC_HISI //A2不允许应用程序升级
#if !defined(IPC_HISI) && !defined(IPC_A3) && !defined(IPC_JZ)&& !defined(IPC_JZ_NEW) && !defined(IPC_P2)//A2不允许应用程序升级
	pthread_create(&pid, NULL, Task_DevUpdata, NULL);
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	pthread_create(&pid, NULL, Task_DevUpdataPlayFile, NULL);
#endif
#endif 
	return 0;
}

int CDevTools::Stop()
{
	return 0;
}

int CDevTools::DevUpdateService()
{
	return 0;
}

int CDevTools::DevGetSearchInfo()
{
	return 0;
}

int CDevTools::DevGetNetInfo()
{
	return 0;
}

int CDevTools::DevSetNetInfo()
{	
	return 0;
}

int CDevTools::DevGetFacInfo()
{
	return 0;
}

int CDevTools::DevSetFacInfo()
{
	return 0;
}

