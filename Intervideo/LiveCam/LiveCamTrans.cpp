#include "Intervideo/LiveCam/LiveCamTrans.h"
#include "Net/NetApp.h"
#include "APIs/DVRDEF.H"
#include "APIs/Capture.h"
#include "Main.h"
#include <time.h>
#include "Functions/Record.h"
#include "System/Object.h"
#include <unistd.h>
#include "Devices/DevExternal.h"
#include "Functions/DriverManager.h"
#include "Net/NetConfig.h"
#include "APIs/Ide.h"
#include <sys/stat.h>
#include <stdio.h>
#include "Functions/Snap.h"
#include <dirent.h>
#include "Functions/FFFile.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <signal.h>
#include <stdlib.h>
#include "APIs/Audio.h"
#include "APIs/Video.h"
#include "Configs/ConfigEvents.h"
#include "Net/Dlg/DlgNtpCli.h"
#include "System/Msg.h"
#include "Functions/WifiLink.h"
#include "APIs/CommonBSP.h"
#include "APIs/CommonBSP_Battery.h"

#ifdef HMSOTA
#include "Functions/HMSOTA/HMSManager.h"
#include "Functions/HMSOTA/OTAManager.h"
#endif
#ifdef 	MP3PLAYWITHURL
#include "Functions/MP3Play.h"
#endif

extern int g_RecordStatus; //录像状态

extern void releaseSnapRef(Snap_Data* pSnap);

PATTERN_SINGLETON_IMPLEMENT(CLiveCamTrans);

//#define FILES_TRANS (8) //每次传送的文件个数/每页的文件个数


int CLiveCamTrans::GetSnapData(int iChn,char *FileName)
{
   	uint iRet = 0;
	Snap_Data SnapData;
	memset(&SnapData,0,sizeof(Snap_Data));
	memset(FileName,0,sizeof(FileName));
	int iCount = 4;
	do
	{
		iRet = g_Snap.getSnapDataByDir(&SnapData,iChn,CSnapBuffer::snapForward);

		if(iRet == 0)
		{
			break;
		}
		else if(iRet < 0 || iCount < 1)
		{
			return -1;
		}
		else if(iRet == 1)
		{
			SystemSleep(500);
			iCount--;
		}		
	}
	while(1);

	printf("####SnapData length:%d,PktNum:%d\n",SnapData.iLength,SnapData.iPktNum);
	
	if(SnapData.iLength <= 0 )
	{
		return -1;
	}
	
	if( NULL == pSnapBuffer )
	{
	    pSnapBuffer = g_PacketManager.GetPacket(SnapData.iLength);
	    if( NULL == pSnapBuffer )
	    {
	        tracepoint();
	        return -1;
	    }
	    pSnapBuffer->SetLength(0);
	}
	
	for(int i = 0; i < SnapData.iPktNum; i++)
	{
	    iRet = pSnapBuffer->PutBuffer(SnapData.pPkt[i]->GetBuffer(), SnapData.pPkt[i]->GetLength());
	    if( iRet < SnapData.pPkt[i]->GetLength() )
	    {
	        tracepoint();
	        return -1;
	    }
	}
	
	if(pSnapBuffer->GetLength() >=(uint)SnapData.iLength)
	{
		sprintf(FileName,"%4d%02d%02d%02d%02d%02d_%d.jpg",SnapData.sys_Createtime.year,SnapData.sys_Createtime.month,
			SnapData.sys_Createtime.day,SnapData.sys_Createtime.hour,SnapData.sys_Createtime.minute,SnapData.sys_Createtime.second,iChn);
		releaseSnapRef(&SnapData);//zanglei added for free memory 20130518
		return 0;    //图片数据已取完整
	}
	
	return -1;
}

//保存图片文件
int CLiveCamTrans::SavePicture(int Chn, char *PathName)
{
	int ret = 0;
	char FileName[64] = "";
	char SnapName[30] = "";
	FILE *fp = NULL;
	FILE *Cmdfp = NULL;
	char Cmd[64] = "";

	if(PathName == NULL)
	{
		printf("PathName is NULL!!!\n");
		return -1;
	}

	if(access((const char *)PathName, F_OK) != 0)
	{
		printf("#######Can't find Directory,will create it!\n");
		if(mkdir((const char *)PathName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
		{
			printf("#######Can't create Directory!!!\n");
        	return -1;
		}
		printf("########Create Directory Success!!\n");
	}

	sprintf(Cmd,"rm %s/*.jpg",PathName);
	Cmdfp = popen(Cmd,"r");

	if(Cmdfp != NULL)
	{
		pclose(Cmdfp);
	}

	ret = GetSnapData(0,SnapName);
	if(ret != 0)
	{
		printf("%s GetSnapData Failure!!\n",__FUNCTION__);

		if(pSnapBuffer != NULL)
		{
			//每次添加完一张图片附件清空下图片缓存
			pSnapBuffer->Release( );
			pSnapBuffer = NULL;
		}
		
		return -1;
	}

	sprintf(FileName,"%s/Live_%s",PathName,SnapName);
	printf("######%s filename:%s\n",__FUNCTION__,FileName);
	
	fp = fopen(FileName,"wb");
	if(fp < 0)
	{
		printf("%s fopen failure!!!\n",__FUNCTION__);

		if(pSnapBuffer)
		{
			//每次添加完一张图片附件清空下图片缓存
			pSnapBuffer->Release( );
			pSnapBuffer = NULL;
		}

		return -1;
	}
	if(pSnapBuffer)
	{
		fwrite((char *)pSnapBuffer->GetBuffer(),1,pSnapBuffer->GetLength(),fp);
		//每次添加完一张图片附件清空下图片缓存
		pSnapBuffer->Release( );
		pSnapBuffer = NULL;
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	return 0;
}


CLiveCamTrans::CLiveCamTrans():CThread("CLiveCamTrans", 20, g_nLogicNum)
{
	m_VideoWanTran = new CLiveCamVideo(); 
	m_AudioWanTran = new CLiveCamAudio(); 
	
	m_initFlag     = 0;
}


CLiveCamTrans::~CLiveCamTrans()
{
	if(NULL != m_VideoWanTran)
	{
		delete m_VideoWanTran;
	}

	if(NULL != m_AudioWanTran)
	{
		delete m_AudioWanTran;
	}
	_printd("~CLiveCamTrans"); 
}

#include "Devices/DevInfo.h"

VD_INT32 CLiveCamTrans::Start()
{	
	char Uuid[16]={0};
	char IPAddr[50] = "";
	char IfName[][128]={
#ifdef IPC_JZ
		"wlan0",
#elif IPC_JZ_NEW
		"eth0",
		"wlan0",
#elif IPC_P2
		"eth0",
		"eth1",
#else
		"wlan0",
#endif
		};
	
	int Ifnum = sizeof(IfName)/sizeof(IfName[0]);
	
	IPInfo Info;

	RT_DevType type = DevType_NewShake;
	#ifdef IPC_JZ_NEW
	switch(PlatformGetHandle()->ProductModel){
		case ProductM_R2S1:
		case ProductM_R2S2:
		case ProductM_R2P2:
			type = DevType_NewShake;
			break;		
		case ProductM_Q1PRO:
		case ProductM_A5PRO:	
		case ProductM_Q2:
		case ProductM_QT2:
		case ProductM_Q2PRO:
			//type = DevType_NewShake;
			type = DevType_FamilyBall;
			break;
		case ProductM_A3S1:	
		case ProductM_A3S2:
		case ProductM_A5:
		case ProductM_A3:			
			type = DevType_NewCard;
			break;
		case ProductM_P2:
			type = DevType_Shake;
			break;
		case ProductM_G3S2:
			type = DevType_NewGun;
			break;
		case ProductM_Q1:
			type = DevType_FaceRcgn;
			break;
		default:
			break;
	}
	#elif IPC_HISI
		type = DevType_Card;
	#elif IPC_JZ
		switch(BAT_PlatformGetHandle()->ProductModel){
			case BAT_ProductM_S1:
				type = DevType_Battery;
				break;
			case BAT_ProductM_S1Single:
				type = DevType_BatSingle;
			case BAT_ProductM_I9:				
				type = DevType_LockI9M;
				break;
			default:
				break;
		}
	#elif IPC_FH
		type = DevType_Shake;
	#elif IPC_A3
		type = DevType_NewCard;	
	#elif IPC_P2
		type = DevType_Shake;
	#endif
	
	printf("*******RT_DevType type is %d**********************\n",type);
	
	int Cnt = 0;
	
	memset(&Info,0,sizeof(Info));	
	StartVideo(0,0,0,0);

	CreateThread(); 	
	while(1)
	{	
		g_NetApp.GetNetIPInfo(IfName[Cnt%Ifnum], &Info);
		sprintf(IPAddr, "%d.%d.%d.%d", Info.HostIP.c[0], Info.HostIP.c[1], Info.HostIP.c[2], Info.HostIP.c[3]);

		#ifdef IPC_JZ	
		if(Cnt % 50 == 0)  //S1 1秒打印一次
		#endif
		_printd("GET %s IP = %s",IfName[Cnt%Ifnum],IPAddr);
		if (strcmp(IPAddr,"0.0.0.0") != 0  && strcmp(IPAddr,"47.210.0.1") != 0 && strcmp(IPAddr,"192.168.3.1") != 0){	
			break;
		}
			
		
#ifdef IPC_JZ		
		usleep(20*1000);
#else
		sleep(1);
#endif
		Cnt ++;
	}
	_printd("Cnt:%d",Cnt);
//	return 0;

	RealTimeInit(type);

	
	m_initFlag = 1;
#ifdef HMSOTA
	sleep(10);
	g_HMS_Indian.Start();
	g_OTA_Indian.Start();
#endif
#ifdef 	MP3PLAYWITHURL
	g_MP3Play.Start();
#endif	

}

VD_INT32 CLiveCamTrans::Stop()
{
	//RealTimeStop();
	
	_printd("RealTimeCamStop");

	printf("<<<<<<<<Stop CLiveCamTrans Thread\n");
	m_bLoop = 0;

	usleep(1000);
	
	DestroyThread();
	
	return 0;

}
#include "Intervideo/RealTime_apiv1.h"
#include "Functions/AiUploadManager.h"
#include "Intervideo/RealTime_callback.h"
extern char force_umount ;
void CLiveCamTrans::ThreadProc()
{
	int ret = 0;
	int Cnt = 0;
	int fd = -1;
	int flagupgrade =0;
	char strCommand[128] = {0};
	char a[3]={'1','2','3'};	
	int PushSta = 0;
	
	int Val = 17;

	SET_THREAD_NAME("CLiveCamTrans");
	
	while(m_bLoop)
	{
		usleep(100 * 1000); //休眠	

		Cnt ++;
		if(Cnt % (10*10) == 0 || (1 == g_RecordStatus && 1 == UpgradeStatus()))
		{
			if(fd < 0)
			{
				if((fd = open("/proc/sys/vm/drop_caches",O_RDWR)) > 0)
				{
					write(fd,&a[0],1);
					lseek(fd,0,SEEK_SET);
					write(fd,&a[1],1);
					lseek(fd,0,SEEK_SET);
					write(fd,&a[2],1);
					_printd("clear cache @@@@@@@@@@@@@@@@");	
					close(fd);
					fd = -1;
				}
				else
				{
					perror("open::");
					fd = -1;
				}
			}
	
			#ifdef IPC_JZ_NEW
			if(0 == flagupgrade && RT_UPGRADING == RealTimeSpecialStatus()){
				if(1 == g_RecordStatus){
					printf("##Upgrading...StopManulRecord##\n");
					g_Record.StopRec(REC_MAN, 0);							
				}

				//umount sd
				force_umount =  1;
				_printd("file_sys_deinit...");
				file_sys_deinit();
				printf("##Upgrading...umount sd##\n");
				system ("killall FAC_VERSION_CHECK.sh ");	//此脚本会自动把卡挂上,超时15秒			
					
				Upgrade_Stop_Capture();
				
				//stop  video
				StopVideo(0,0,0);
				printf("##Upgrading...stop  p2p video##\n");

				//stop  audio
				StopAudio(0);	
				printf("##Upgrading...stop  p2p audio##\n");
				flagupgrade = 1;
			}
///////////////////////////////////////////
			if(RT_UPGRADE_FAIL == RealTimeSpecialStatus()){
				flagupgrade = 0;			
				memset(strCommand, 0, sizeof(strCommand));	
				snprintf(strCommand,sizeof(0),"reboot \n");
				if(0 == (ret = system(strCommand))){
					printf("##Upgrade failed then reboot system##\n");
				}else{
					
				}
			}
			#endif
///////////////////////////////////////////////
			g_PacketManager.Dump();
		}
	}
}

int CLiveCamTrans::StartVideo(int Channel, int StreamType, bool WithAudio, int nNetType)
{
	#ifndef TUPU
	m_VideoWanTran->Start(0,0);  //启动广局域网传输
	#endif
	return 0;
}
int CLiveCamTrans::StopVideo(int Channel, int StreamType, int nNetType)
{
	m_VideoWanTran->Stop(0,0);
	return 0;
}
int CLiveCamTrans::StartAudio(int Channel)
{
	#ifndef TUPU
	m_AudioWanTran->StartAudioIn(0);  //启动音频输入流	
	#endif
	return 0;
}
int CLiveCamTrans::StopAudio(int Channel)
{
	#ifndef TUPU
	m_AudioWanTran->StopAudioIn(0);	
	#endif
	return 0;
}


