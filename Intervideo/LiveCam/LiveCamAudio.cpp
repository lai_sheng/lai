#include "Intervideo/LiveCam/LiveCamAudio.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"
#include "Intervideo/LiveCam/RealTimeTaskPushStream.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/RealTime_apiv1.h"

CLiveCamAudio::CLiveCamAudio()
{
	_printd("CLiveCamAudio");
}

CLiveCamAudio::~CLiveCamAudio()
{
	_printd("~CLiveCamAudio");
}

VD_INT32 CLiveCamAudio::StartAudioOut(int channel)
{
	_printd("LiveCam AudioOut Start");
	return 0;
}

VD_INT32 CLiveCamAudio::StopAudioOut(int channel)
{
	_printd(" StopAudioOut");
	return 0;
}

VD_INT32 CLiveCamAudio::OnAudioData(int iChannel, CPacket *pPacket)
{
	//音频发送
//	C_StreamPacket SendPacket;
	unsigned int FrameCount = 0;
	uchar *pData = NULL;
	int	 Len;
	
	#if 0
	static int n = 0;
	static FILE *fp = NULL;

	if(fp == NULL && n == 0)
		fp = fopen("Audioin.aac","wb");
	
	if(fp != NULL)
	{
		memcpy(&FrameCount,pPacket->GetBuffer() + 8,sizeof(unsigned int));
		fwrite(pPacket->GetBuffer() + 8 + sizeof(unsigned int),pPacket->GetLength() - 8 - sizeof(unsigned int)  - sizeof(long long),1,fp);
		printf("write aac length:%d count:%d\n",pPacket->GetLength() - 8 - sizeof(unsigned int)  - sizeof(long long),n);
		n++;
	}

	if(fp != NULL && n > 1*1024)
	{
		printf("write aac end!");
		fclose(fp);
		fp = NULL;
	}
	
	return 0;
	#endif
	long long FrameTimeStamp;
//	memcpy(&FrameCount, pPacket->GetBuffer()+8, sizeof(unsigned int)); //把帧数从头部取出
	memcpy(&FrameTimeStamp,pPacket->GetBuffer() + pPacket->GetLength() - sizeof(long long), sizeof(long long));
	
//	_printd("====>>>>OnAudioData  FrameTimeStamp=%llu  ", (FrameTimeStamp));
	pData 	= pPacket->GetBuffer() + 8 + sizeof(unsigned int) ;
	Len 	= pPacket->GetLength() - 8 - sizeof(unsigned int)  - sizeof(long long);
	RealTimeSendAudioData(0,(void*)pData,Len,(unsigned int)(FrameTimeStamp/1000));

	return 0;
}
VD_INT32 CLiveCamAudio::StartAudioIn(int channel)
{
	_printd ("LiveCam AudioIn Start");
	int ret = -1;

	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	ret = pCapture->Start(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CLiveCamAudio::OnAudioData);
	if (ret != 1)
		printf("###################ERROR Audio!!!######################\n");
	return 0;
}

VD_INT32 CLiveCamAudio::StopAudioIn(int channel)
{
	_printd ("LiveCam StopAudioIn");
	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	pCapture->Stop(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CLiveCamAudio::OnAudioData);
	return 0;
}

VD_INT32 CLiveCamAudio::SendAuidoInData()
{
	return 0;
}
#if 0
VD_INT32 CLiveCamAudio::PutAuidoOutData(int channel, unsigned char *buf, unsigned int len)
{
	/*uchar *data, int length, int type, int format = 0*/
	g_AudioManager.PlayVoice(buf, len, AUDIO_TALK_TYPE, G711_ALAW);
	return 0;
}
#endif
