#include "Intervideo/LiveCam/LiveCamVideo.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"
#include "Intervideo/LiveCam/RealTimeTaskPushStream.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/RealTime_apiv1.h"
#include <sys/time.h>
#include <unistd.h>

extern int g_UpdateEncode;  //编码配置是否改动的标志位 1:改动 

CLiveCamVideo::CLiveCamVideo():CThread("LiveCamVideo", TP_NET)
{
	_printd("CLiveCamVideo");
	m_ptkBufMain.Reset(true);
	m_ptkBufSecond.Reset(true);
	m_packet_count = 0;
	m_MainFps = 0;
	m_MainReslution = 0;
	m_SubFps = 0;
	m_SubReslution = 0;
	CreateThread();
}

CLiveCamVideo::~CLiveCamVideo()
{
	_printd("~CLiveCamVideo");
	DestroyThread();
}

VD_INT32 CLiveCamVideo::OnVideoData(int iChannel, uint iCodeType, CPacket *pPacket)
{
	if (!pPacket) 
	{
        _printd("%s:pPacket is NULL\n",__FUNCTION__);
	    return -1;
	}
	PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());

	unsigned char * pBufSend = NULL;
	unsigned char ucFrameType = PACK_TYPE_FRAME_NULL;  /* 帧类型 */
	unsigned char ucFrameFlag = 0;                     /* 帧头尾的标识 */
	int usFramePos = 0;                     /* 帧开始位置 */
	uint usCFrameLength = 0;                 /* 帧在本块中的长度 */  
	uint usFrameLength = 0;
	pBufSend = pPacket->GetBuffer();
	for ( int iIndex = 0; iIndex < FRAME_MAX_NUM; iIndex++) {        
		ucFrameType = pPacketHead->FrameInfo[iIndex].FrameType;
		ucFrameFlag = pPacketHead->FrameInfo[iIndex].FrameFlag;
		usFramePos = pPacketHead->FrameInfo[iIndex].FramePos;
		usCFrameLength = pPacketHead->FrameInfo[iIndex].DataLength;
		usFrameLength = pPacketHead->FrameInfo[iIndex].FrameLength;        
		if (ucFrameType == PACK_TYPE_FRAME_NULL) {
			break;
		} else if(PACK_TYPE_FRAME_AUDIO == ucFrameType) {
			continue;
		}
		if (CHL_MAIN_T == iCodeType) {
			if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
				m_ptkBufMain.Reset();
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				/*一个完整帧*/
				if (-2 == SendVideoData((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size(), iChannel, iCodeType,ucFrameType)) {
					_printd("add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				}
				m_ptkBufMain.Reset();
				continue;
			} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
				m_ptkBufMain.Reset();
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
//_printd("Head  addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				continue;
			} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
				/* 只包含帧尾的帧 */
				if (m_ptkBufMain.Size() > 0) {
                			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
					/*组合好一个完整帧*/
					if (-2 == SendVideoData((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size(), iChannel, iCodeType,ucFrameType)) {
					_printd("Tail Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				}
				} else {
					tracepoint();
					_printd("streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
					m_ptkBufMain.Reset();
				}
				continue;
			} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
				/* 不包含帧头和帧尾的帧 */
				if (m_ptkBufMain.Size() > 0) {
					m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				} else {
					tracepoint();
					//_printd("NONHT Rtsp streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
					m_ptkBufMain.Reset();
					
				}
				continue;
			}
		} else if (CHL_2END_T == iCodeType) {
			if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
				m_ptkBufSecond.Reset();
				m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				if (-2 == SendVideoData((unsigned char *)(m_ptkBufSecond.Buf()), m_ptkBufSecond.Size(), iChannel, iCodeType,ucFrameType)) {
					_printd("Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				}
				m_ptkBufSecond.Reset();
				continue;
			} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
				m_ptkBufSecond.Reset();
				m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				continue;
			} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
				if (m_ptkBufSecond.Size() > 0) {
                			m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
					if (-2 == SendVideoData((unsigned char *)(m_ptkBufSecond.Buf()), m_ptkBufSecond.Size(), iChannel, iCodeType,ucFrameType)) {
						_printd("Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
					}
					m_ptkBufSecond.Reset();
				} else {
					tracepoint();
					_printd("streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
					m_ptkBufSecond.Reset();
				}
				continue;
			} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
				/* 不包含帧头和帧尾的帧 */
				if (m_ptkBufSecond.Size() > 0) {
					m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				} else {
					tracepoint();
					m_ptkBufSecond.Reset();
				}
			}
		}
    }
    return 0;
}

VD_INT32 CLiveCamVideo::UpdateEncodeCfg()
{
	CConfigEncode* pCfgEncode = new CConfigEncode();

	pCfgEncode->update();
	CONFIG_ENCODE& ConfigEncode = pCfgEncode->getConfig();

	m_MainReslution = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
	m_MainFps       = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
	m_SubReslution 	= ConfigEncode.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
	m_SubFps        = ConfigEncode.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
	
	delete pCfgEncode;
	
	return 0;
}

VD_INT32 CLiveCamVideo::SendVideoData(uchar *pData, unsigned int iLen, unsigned char iChannel, unsigned char iStreamType, unsigned char FrameType)
{
	char isIFrame = 0;
	char Head[8]  = {0};
	long long FrameTimeStamp;
	
	memcpy(Head,pData,8);	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp

	if(Head[0] == 0 && Head[1] == 0 && Head[2] == 0 && Head[3] == 0xfd)
	{
		_printd("I Frame!!!");
		isIFrame = 1;
	}

	#if 0
	int  timelog = 0;
	struct timeval szCurTime;
    gettimeofday(&szCurTime, NULL);
	timelog = szCurTime.tv_sec*1000 + szCurTime.tv_usec/1000;
		
	_printd("====>>>>RealTimeSendVideoData  time=%d  [%d]\n", (unsigned int)(FrameTimeStamp/1000), timelog);
	#endif
//	_printd("====>>>>RealTimeSendVideoData  FrameTimeStamp=%ll ",(FrameTimeStamp));
	RealTimeSendVideoData(0,isIFrame,pData,iLen - 13,(unsigned int)(FrameTimeStamp/1000));

	return 0;
}

VD_INT32 CLiveCamVideo::Start(int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture){
		_printd("Icapture::instance ok");
		pCapture->SetIFrame(StreamType);
		VD_BOOL bRet = pCapture->Start(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CLiveCamVideo::OnCapture, DATA_MONITOR,StreamType);
	}else{
		_printd("Icapture::instance ERror");
		return -1;
	}
	return 0;
}

VD_INT32 CLiveCamVideo::Stop (int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture){
		_printd("Icapture::instance Stop");
		pCapture->SetIFrame(StreamType);	
		VD_BOOL bRet = pCapture->Stop(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CLiveCamVideo::OnCapture, DATA_MONITOR,StreamType);
		
	}else{
		_printd("Icapture::instance ERror");
		return -1;
	}
	return 0;
}

bool CLiveCamVideo::SendVideoToServer()
{
	if (m_packet_count > 0) {
		CPacket* pPacket = NULL;
		uint dwStreamType = 0;
		int chn;

		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		chn = it->chn;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();

		OnVideoData(chn, dwStreamType, pPacket);
		pPacket->Release();

		return true;
	}
	
	return false;
}

void CLiveCamVideo::OnCapture(int iChannel, uint iStreamType, CPacket *pPacket)
{

	#ifdef TUPU
	return;
	#else
	
	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = iStreamType;
		
	int iCount = g_nLogicNum;
	if (m_packet_count > (uint)(iCount*50)) {
		debugf("LiveCam capture list is too large discard it, size:%d, chn:%d!type:%d\n", m_packet_count, iChannel, iStreamType);
		return ;
	}

	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_mutex_list.Leave();

	#endif
}

void CLiveCamVideo::ThreadProc()
{
	SET_THREAD_NAME("CLiveCamVideo");

	while (m_bLoop) 
	{
		//防止没有码流数据空跑,导致cpu利用率过高
		if (false == SendVideoToServer()) 
		{
			usleep(10*1000);  
		}
	}
}
