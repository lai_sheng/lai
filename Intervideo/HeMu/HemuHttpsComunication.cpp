#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include "System/File.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#include "Intervideo/LiveCam/RealTimeCamMemory.h"
#include "Intervideo/HeMu/HWComunication.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "APIs/CommonBSP.h"
#include "Functions/DriverManager.h"
#include "APIs/MotionDetect.h"

//third.reservehemu.com
//#define HEMU_OPENAPI "http://third.stg.reservehemu.cn/andmu/v2/service/batch"
#define HEMU_OPENAPI "https://third.reservehemu.com:8443/andmu/v2/service/batch"


typedef struct {
  char *memory;
  size_t size;
} MemChunk;

#define KMaxChar 126
#define KMinChar 33
								

#define  HUMUCLOUD_USERANME "dac5a4da954"//"d7edf647ebe"//"d7edf648ebe"//"dac5a4da954" //"d7edf647ebe"
#define  HUMUCLOUD_PASSWORD "Wm6UQgqTD64DctGNkgeK"//"x9E2b61cXEJ623njPG4V"//"x9E2b61cXEJ623njPG5V"//"Wm6UQgqTD64DctGNkgeK" //"x9E2b61cXEJ623njPG4V"

#define  HUMUCLOUD_MODELID51  "cmhi_hdc_51_001"//cmhi_hdc_51_001
#define  HUMUCLOUD_USERANME51 "dac5a4da954"
#define  HUMUCLOUD_PASSWORD51 "Wm6UQgqTD64DctGNkgeK"

#define  HUMUCLOUD_MODELID55  "cmhi_hdc_55_001" //cmhi_hdc_55_001
#define  HUMUCLOUD_USERANME55 "dac5a4da954"
#define  HUMUCLOUD_PASSWORD55 "Wm6UQgqTD64DctGNkgeK"


#define  UPGRADE_FILE   "/tmp/upgrade_file.bin"
#define  UPGRADE_INFO   "/app/ipcam/upgradeinfo"
static int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  

size_t write_memory(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    MemChunk *mem = (MemChunk *)userp;

    mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL)
    {
        /* out of memory! */ 
        fprintf(stderr, "not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
 
    return realsize;
}
static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) 
	{  
        fwrite(ptr, nmemb, size,  fp); 
		fclose(fp);
    }  
    _printd("download size[%dkb]",len/1024);      
    return written;  
} 
int GetLocalFileLenth(const char* fileName)
{
	struct stat statbuff;
	if( -1 == stat(fileName, &statbuff) )
	{
		_printd("stat wrong ");
		return 0;
	}
	_printd("stat success.%ld\n", statbuff.st_size);
	return statbuff.st_size;		
}
void genrndchar(char * buff, int n)
{
    char metachar[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    srand(time(NULL));
    for (int i = 0; i < n - 1; i++)
    {
        buff[i] = metachar[rand() % 62]; 
    }
    buff[n - 1] = '\0';
}
void charDecrypt(char *aChar, int randomNum, int pos, int charNum)
{
	int offset = charNum * charNum + pos * randomNum;
	if (*aChar >= KMinChar && *aChar <= KMaxChar) 
	{
		if (*aChar - KMinChar < offset) 
		{
			int subOffset = (offset - (*aChar - KMinChar)) % (KMaxChar - KMinChar + 1);
			*aChar = KMaxChar - subOffset;
		} 
		else 
		{
			*aChar -= offset;
		}
	}
}

int QrcodeStrDecrypt(const char *ciphertext, char *buff)
{
	int ciphertextlen = strlen(ciphertext);
	if (0 == ciphertextlen)
		return -1;
	int i = 0;
	int randomPos = ciphertextlen - 1;
	int randomNum = ciphertext[randomPos] - KMinChar;
	int ciphertextNum = ciphertextlen - 1;
	char TmpChar = 0;
	for (i = 0; i < ciphertextNum; i++) 
	{
		TmpChar = ciphertext[i];
		charDecrypt(&TmpChar, randomNum, i, ciphertextNum);
		buff[i] = TmpChar;
	}
	buff[ciphertextNum] = '\0';
	return 0;
}


char * Base64Encode(const char * input, int length, bool with_new_line)
{	
	BIO * bmem = NULL;	
	BIO * b64 = NULL;
	BUF_MEM * bptr = NULL;
	b64 = BIO_new(BIO_f_base64());
	if(!with_new_line) 
	{		
		BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
	}	
	bmem = BIO_new(BIO_s_mem());
	b64 = BIO_push(b64, bmem);
	BIO_write(b64, input, length);
	BIO_flush(b64);	
	BIO_get_mem_ptr(b64, &bptr);
	char * buff = (char *)malloc(bptr->length + 1);
	memcpy(buff, bptr->data, bptr->length);
	buff[bptr->length] = '\0';
	BIO_free_all(b64); 	
	return buff;
}
void Md5Encode(const char * input, char* out)
{	
	char pmd[33] = {0};
	MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx,input,strlen(input));
    MD5_Final((unsigned char*)pmd,&ctx);	
	char tmpbuf[10] = {0};
	strcpy(out, "");
    for (int i = 0; i < 16 ; i++) 
	{
        snprintf(tmpbuf,sizeof(tmpbuf),"%02x", (unsigned char)pmd[i]);
        strcat(out, tmpbuf);
    }
}

void HeMuPasswordDigest(char *Nonce, char *TimeStamp,char *PasswordDigest,const char* AcessSecret)
{	
	unsigned char digest[SHA_DIGEST_LENGTH];
	char contex[1024] ={0};
	snprintf(contex, sizeof(contex), "%s%s%s", Nonce, TimeStamp, AcessSecret/*HUMUCLOUD_PASSWOR*/);
	 
	SHA1((unsigned char*)&contex, strlen(contex), (unsigned char*)&digest);	 
	char mdString[SHA_DIGEST_LENGTH*2+1];	 
	for(int i = 0; i < SHA_DIGEST_LENGTH; i++)	 
	sprintf(&mdString[i*2],"%02x", (unsigned int)digest[i]);
	 
	_printd("SHA1 digest: %s", mdString);

	char *pbase = Base64Encode(mdString,strlen(mdString),1);

	_printd("base: %s", pbase);
	
	memcpy(PasswordDigest,pbase,strlen(pbase));
	PasswordDigest[strlen(pbase)-1] = '\0';
	free(pbase);
}



PATTERN_SINGLETON_IMPLEMENT( CHemuHttpsComunication);

  CHemuHttpsComunication::  CHemuHttpsComunication():CThread("  CHemuHttpsComunication", TP_NET)
  	,m_istartupgrade(FALSE)
  	,m_imsgsession(0)
{
	_printd("  CHemuHttpsComunication");
	m_iFlag = 0;
	m_iNtpSuccess = 0;
	m_iBindStatus = 0;
	m_iLastBindStatus = 0;
	m_isendcmd = 0;
	m_iLastUpgrade = 0;
	m_iLastUpgradeMsgsession = 0;
	m_iLastUpgradeStatus = 0; 
	m_nConfigSynchronize = 0;
}

  CHemuHttpsComunication::~CHemuHttpsComunication()
{
	_printd("~  CHemuHttpsComunication");
}




VD_INT32   CHemuHttpsComunication::Start()
{
	char uuid[16]= {'\0'};
	char mac[16] = {'\0'};
	_printd ("  CHemuHttpsComunication Init Over");		
	
	m_userInfo.attach(this, (TCONFIG_PROC)&CHemuHttpsComunication::onConfigUserInfo);
	m_userInfo.update();
	//CONFIG_HEMUUSERINFO &configNew = m_userInfo.getConfig();

	//m_iBindStatus = configNew.BindStatus;
	//if (m_iBindStatus)
	//{
	//	m_cUserMobile = configNew.UserMobile;
	//	m_cUserToken = configNew.UserToken;
	//	m_cdeviceToken = configNew.devicetoken;
	//	m_cunifiedId = configNew.unifiedId;
	//	m_cipupdatetime = configNew.ipupdatetime;
	//	m_cserverAddr = configNew.ipaddr;
	//	char cport[8] = { 0 };
	//	snprintf(cport, 8, "%d", configNew.port);
	//	m_cserverPort = cport;
	//}
	if(!GetConfigMac(mac))
	{
		m_mac = mac;//"58b42d290111"; 
	}
	//m_mac = "58b42d290111";//"58b42d290111";
	if(!GetUuid(uuid))
	{
		m_cDeviceID = uuid;//"119000310000101"; 
	}
	if (ProductM_Q2 == PlatformGetHandle()->ProductModel)
	{
		m_cDeviceModelId = HUMUCLOUD_MODELID55;//"cmhi_hdc_21n_001";//"cmhi_hdc_51_001";
		m_caccessKey = HUMUCLOUD_USERANME55;
		m_caccessSecret = HUMUCLOUD_PASSWORD55;
	}
	else
	{
		m_cDeviceModelId = HUMUCLOUD_MODELID51;//"cmhi_hdc_55_001";
		m_caccessKey = HUMUCLOUD_USERANME51;
		m_caccessSecret = HUMUCLOUD_PASSWORD51;
	}

	string strv;
	ifstream hemuversion("/app/ipcam/verinfo");
	
	if(!hemuversion.is_open()){
		m_cFirmwareVersion = "0.00.001";
		m_cCameraAppVersion = "0.00.001";
	}
	else
	{
		if(getline(hemuversion,strv))
		{
			m_cFirmwareVersion = strv.c_str();
			m_cCameraAppVersion  = strv.c_str();
			_printd("version[%s]", strv.c_str());
		}
		hemuversion.close();
	}
	//判断是上次重启是否是升级状态
	if (0 == access(UPGRADE_INFO, 0))
	{
		ifstream hemu_upgrade(UPGRADE_INFO);
		if (hemu_upgrade.is_open())
		{
			m_iLastUpgrade = 1;
			if (getline(hemu_upgrade, strv))
			{
				if (m_cFirmwareVersion == strv)
				{
					m_iLastUpgradeStatus = 1;
				}
				else
				{
					m_iLastUpgradeStatus = 0; 
				}
				m_cLastUpgradeversion = strv;
				_printd("version[%s]", strv.c_str());
			}
			if (getline(hemu_upgrade, strv))
			{
				m_iLastUpgradeMsgsession = atoi(strv.c_str());
				_printd("m_iLastUpgradeMsgsession[%d]", m_iLastUpgradeMsgsession);
			}
			hemu_upgrade.close();
		}
		remove(UPGRADE_INFO);
	}
//	m_cFirmwareVersion = "1.01.102";
//	m_cCameraAppVersion = "1.01.102";
	int ret = CreateThread();
	_printd("ret[%d]",ret);
	curl_global_init(CURL_GLOBAL_ALL);
	return 0;
}
void CHemuHttpsComunication::onConfigUserInfo(CConfigHemuUeserInfo& cCfgUserInfo, int& ret)
{
	
	CONFIG_HEMUUSERINFO &configOld = m_userInfo.getConfig();
	CONFIG_HEMUUSERINFO &configNew = cCfgUserInfo.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}
	m_iBindStatus = configNew.BindStatus;
	if(m_iBindStatus)
	{
		m_cUserMobile = configNew.UserMobile;
		m_cUserToken = configNew.UserToken;
		m_cdeviceToken = configNew.devicetoken;
		m_cunifiedId   = configNew.unifiedId;
		m_cipupdatetime = configNew.ipupdatetime;
		m_cserverAddr = configNew.ipaddr;
		char cport[8] = {0};
		snprintf(cport, 8, "%d", configNew.iport);
		m_cserverPort = cport;
	}

	configOld = configNew;
}

VD_INT32   CHemuHttpsComunication::QRcodeInfo(string usermobile,string usertoken)
{
	m_cUserMobile= usermobile;
	m_cUserToken = usertoken;
	m_iLastBindStatus = m_iBindStatus;
	m_iBindStatus = 2;//请求绑定
	m_nConfigSynchronize = TRUE;
	g_HWComm.SetConect(0);
	return 0;
}

VD_INT32   CHemuHttpsComunication::Stop()
{
	_printd("  CHemuHttpsComunication :: Stop ");
	return 0;
}

//int CHemuHttpsComunication::TimerUpgrade(int arg)
//{
//	g_HWComm.PushUgradeStatus(m_cversion.c_str(), 2, m_imsgsession, NULL);
//}

void   CHemuHttpsComunication::ThreadProc()
{
	int once = 1;
	int config = 0;
	while (m_bLoop) 
	{
		//_printd("m_cDeviceID[%d]m_mac[%d]", m_cDeviceID.empty(), m_mac.empty());

		if(m_cDeviceID.empty() || m_mac.empty())
		{
			_printd("[%d][%d]",m_cDeviceID.empty(),m_mac.empty());
			char uuid[16]= {'\0'};
			char mac[16] = {'\0'};
			if(!GetConfigMac(mac))
			{
				m_mac = mac;//"58b42d290111"; 
			}

			if(!GetUuid(uuid))
			{
				m_cDeviceID = uuid;//"119000310000101"; 
			}
			sleep(1);
			continue;			
		}
		//_printd("m_iBindStatus[%d]m_iNtpSuccess[%d],m_cDeviceID:%s mac:%s", m_iBindStatus, m_iNtpSuccess, m_cDeviceID.c_str(), m_mac.c_str());
		if(m_iNtpSuccess)
		{		

			//更新配置
			if(1 == m_iBindStatus && once)
			{		
				//连接HW服务器
				sleep(1);
				if (!m_iLastUpgrade)
				{
					//升级等版本同步再上线
					g_HWComm.SetConect(1);
				}
				if(!m_cdeviceToken.empty())
				{
					//顺序发送确保每个信令都成功
					switch(m_isendcmd)
					{
						case 0:
						SendOpenAPIReq(MSG_P_THD_SET_CAMERA_CAPACITY_REQ);
						break;
						case 1:
							if (config)
							{
								//配置模式则更新IP
								SendOpenAPIReq(MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ);
							}
							else
							{
								m_isendcmd = 2;
							}
						break;
						case 2:
						SendOpenAPIReq(MSG_P_THD_GET_CAMERA_SETTINGS_REQ);
						break;
						case 3:
						SendOpenAPIReq(MSG_P_THD_SET_CAMERA_SETTINGS_REQ);
						break;
						case 4:
						SendOpenAPIReq(MSG_P_THD_SET_CAMERA_INFO_REQ);
						break;
						default:
							g_HWComm.SetConect(1);
							once =0;
							config = 0;
						break;
					}
					
				}
				
			}
			else if(2 == m_iBindStatus)
			{
				SendOpenAPIReq(MSG_P_THD_ADD_CAMERA_REQ);
				m_isendcmd = 0;
				once =1;
				config = 1;
			}
		}


		sleep(1);
		
		
		if(m_istartupgrade)
		{
			//升级
			g_HWComm.SetUpgradeStatus(1);
			//开始下载
//			g_HWComm.PushUgradeStatus(m_cversion.c_str(), 1, m_imsgsession, NULL);
			//正在下载
//			m_ctimerupgrade.Start(this, (VD_TIMERPROC)&CHemuHttpsComunication::TimerUpgrade, 5000, 5000);
			//g_HWComm.PushUgradeStatus(m_cversion.c_str(), 2, m_imsgsession, NULL);
			if(0 == DownLoad())
			{
				//md5 检验
//				m_ctimerupgrade.Stop();
				_printd("OK================");
				//升级
				g_HWComm.PushUgradeStatus(m_cversion.c_str(),3, m_imsgsession,NULL);
				g_HWComm.PushUgradeStatus(m_cversion.c_str(),5, m_imsgsession,NULL);
				//解压
				g_HWComm.PushUgradeStatus(m_cversion.c_str(), 6, m_imsgsession, NULL);

				ofstream upgradeifo(UPGRADE_INFO);
				if (!upgradeifo.fail())
				{
					char tmpc[16] = {0};
					upgradeifo.write(m_cversion.c_str(), m_cversion.size());
					upgradeifo.put('\n');
					snprintf(tmpc, sizeof(tmpc), "%d\n", m_imsgsession);
					upgradeifo.write(tmpc, strlen(tmpc));
					upgradeifo.close();
				}
				sleep(3);
				char UpgradeFilePath[150] = { 0 };
				sprintf(UpgradeFilePath, "/app/upgradeTool  %s", UPGRADE_FILE);
				printf("%s\n", UpgradeFilePath);
				execl("/bin/sh", "sh", "-c", UpgradeFilePath, (char*)0);
			}
			else
			{
				//下载失败
				_printd("upgrade fail!!!!!!!!!!!!!!!!!!!!!!");
				g_HWComm.PushUgradeStatus(m_cversion.c_str(),4, m_imsgsession,NULL);
			}
			m_istartupgrade = 0;
			g_HWComm.SetUpgradeStatus(0);
//			m_ctimerupgrade.Stop();
		}

		if (m_iLastUpgrade && 2 == g_HWComm.GetConectStatus() && m_isendcmd > 4 )
		{
			sleep(2);
		    //更新固件状态发送
			m_iLastUpgrade = 0;
			if (m_iLastUpgradeStatus)
			{
				g_HWComm.PushUgradeStatus(m_cLastUpgradeversion.c_str(), 7, m_iLastUpgradeMsgsession, NULL);
			}
			else
			{
				g_HWComm.PushUgradeStatus(m_cLastUpgradeversion.c_str(), 8, m_iLastUpgradeMsgsession, NULL);
			}
		}
	}
}
void  CHemuHttpsComunication::HeMucompose_header(struct curl_slist **headerlist)
{
	SYSTEM_TIME strNow;
	int timezonesec;
    memset(&strNow, 0, sizeof(SYSTEM_TIME));
    SystemGetCurrentTime(&strNow);

	char Nonce[17] = {0};
	genrndchar(Nonce, sizeof(Nonce));

	char creattimestamp[128] = {0};
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();
	timezonesec = cfgnetntp.TimeZone/10;
	if(timezonesec >= 0)
	{
		snprintf(creattimestamp,sizeof(creattimestamp),"%04d-%02d-%02dT%02d:%02d:%02d+%02d:%02d",
													    strNow.year,strNow.month,strNow.day,
													    strNow.hour,strNow.minute,strNow.second,
													    timezonesec,cfgnetntp.TimeZone%10*6);
	}
	else
	{
		snprintf(creattimestamp,sizeof(creattimestamp),"%04d-%02d-%02dT%02d:%02d:%02d-%02d:%02d",
														strNow.year,strNow.month,strNow.day,
													    strNow.hour,strNow.minute,strNow.second,
													    -timezonesec,cfgnetntp.TimeZone%10*6);
	}
	char PasswordDigest[512] ={0};
	
	HeMuPasswordDigest(Nonce,creattimestamp,PasswordDigest,m_caccessSecret.c_str());
	char xwsse[1024] = {0};
	snprintf(xwsse,sizeof(xwsse),"X-WSSE:UsernamePwd Username=\"%s-%s\",PasswordDigest=\"%s\",Nonce=\"%s\",Created=\"%s\"",
								 m_caccessKey.c_str()/*HUMUCLOUD_USERANME*/, m_cDeviceID.c_str(),PasswordDigest,Nonce,creattimestamp);
	_printd("%s",xwsse);
  	*headerlist = curl_slist_append(*headerlist, "Authorization: WSSE profile=\"UsernamePwd\"");
     curl_slist_append(*headerlist, xwsse);
//	 curl_slist_append(*headerlist, "Connection: Keep-Alive");
//     curl_slist_append(*headerlist, "Accept-Encoding: gzip");
//    curl_slist_append(*headerlist, "User-Agent: okhttp/3.6.0");

	
}
void CHemuHttpsComunication::ComposeForm(struct curl_httppost ** post,const string & jsonObject,
													const string & signature)
{
    struct curl_httppost* last = NULL;
    curl_formadd(post, &last,
               CURLFORM_COPYNAME, "accessKey",
               CURLFORM_COPYCONTENTS, m_caccessKey.c_str(),//HUMUCLOUD_USERANME,
               CURLFORM_END);
	curl_formadd(post, &last,
               CURLFORM_COPYNAME, "signature",
               CURLFORM_COPYCONTENTS, signature.c_str(),
               CURLFORM_END);
    curl_formadd(post, &last,
               CURLFORM_COPYNAME, "jsonObject",
               CURLFORM_COPYCONTENTS, jsonObject.c_str(),
               CURLFORM_END);


   
}
void CHemuHttpsComunication::ComposeMsgObject(int msgType,string &msgout)
{
	switch(msgType)
	{
		case MSG_P_THD_ADD_CAMERA_REQ:
		{
			CConfigTable  objson;
			#if 1
			objson["msgType"] = "MSG_P_THD_ADD_CAMERA_REQ";
			objson["userMobile"] = m_cUserMobile.c_str();
			objson["userToken"] = m_cUserToken.c_str();
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceModelId"] =  m_cDeviceModelId.c_str();//"cmhi_hdc_51_001";
			if (ProductM_Q2 == PlatformGetHandle()->ProductModel)
			{
				//有wifi能力及PTZ
				objson["comment"] = "[\'HW\':\'3RD-X01\',\'FEATURE\':17]";
			}
			else
			{
				//只有wifi能力
				objson["comment"] = "[\'HW\':\'3RD-X01\',\'FEATURE\':1]";
			}
			
			objson["registMode"] = "1";
			
			objson.toStyledString(msgout);
			#else			
			//	msgout = buf;
			msgout = "{\"userToken\":\"708a9c84b47404c5524405e5cbd910b8\",\"deviceMacAddress\":\"146b9c4bbf99\",\"deviceId\":\"111002990000381\",\"msgType\":\"MSG_P_THD_ADD_CAMERA_REQ\",\"comment\":\"[\'HW\':\'3RD-X01\',\'FEATURE\':17]\",\"userMobile\":\"13896118873\",\"deviceModelId\":\"cmhi_hdc_51_001\",\"registMode\":\"0\"}";
			#endif
		}
		break;
		case MSG_P_THD_SET_CAMERA_CAPACITY_REQ:
		{
			CConfigTable  objson;
			CConfigTable  supportKey;

			objson["msgType"] = "MSG_P_THD_SET_CAMERA_CAPACITY_REQ";
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceToken"] = m_cdeviceToken.c_str();

			supportKey["motionRegion"] =  0;
			supportKey["duplexAudioTalk"] = 1;
			supportKey["faceDetect"] = 0;
			supportKey["personStatistic"] = 0;
			supportKey["sdcard"] = 1;
			supportKey["eventRecord"] = 1;
			supportKey["binoCam"] = 0;	
			supportKey["ptzPano"] = 0;
			if (ProductM_Q2 == PlatformGetHandle()->ProductModel)
			{
				supportKey["motionTrack"] = 1;
			}
			else
			{
				supportKey["motionTrack"] = 0;
			}
			objson["supportKey"] = supportKey;
			
			objson.toStyledString(msgout);
		}
		break;
		case MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ:
		{
			CConfigTable  objson;
			CConfigTable  supportKey;
 
			objson["msgType"] = "MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ";
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceToken"] = m_cdeviceToken.c_str();

			objson.toStyledString(msgout);
		}
		break;
		case MSG_P_THD_GET_CAMERA_SETTINGS_REQ:
		{
			CConfigTable  objson;

			objson["msgType"] = "MSG_P_THD_GET_CAMERA_SETTINGS_REQ";
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceToken"] = m_cdeviceToken.c_str();

			objson.toStyledString(msgout);
		}
		break;
		case MSG_P_THD_SET_CAMERA_SETTINGS_REQ:
		{
			CConfigTable  objson;
			CConfigTable  camParam;
			char wifiname[48] = {0};
			GetConnWifi(wifiname, sizeof(wifiname));
			objson["msgType"] = "MSG_P_THD_SET_CAMERA_SETTINGS_REQ";
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceToken"] = m_cdeviceToken.c_str();

			CConfigGeneral cCfgGeneral;
			cCfgGeneral.update();
			CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
			if(config.iSuspendMode)
			{
				camParam["status"] = "Off";
			}
			else
			{
				camParam["status"] = "On";
			}
			camParam["personStatus"] = "Off";
			camParam["wifiSSID"] = wifiname;
			#if 1
			if(access("/dev/mmcblk0",F_OK) == 0 )
			{
				if (FS_WRITE_ERROR == g_DriverManager.GetSDStatus())
				{
					camParam["sdcardStatus"] = 2;
				}
				else
				{
					camParam["sdcardStatus"] = 3;
				}
			}
			else
			{
				camParam["sdcardStatus"] = 1;
			}
			#endif
			//camParam["sdcardStatus"] = 3;
			camParam["binoParam"] = "NULL";
			objson["camParam"] = camParam;
			objson.toStyledString(msgout);			
		}
		break;
		case MSG_P_THD_SET_CAMERA_INFO_REQ:
		{
			CConfigTable  objson;

			objson["msgType"] = "MSG_P_THD_SET_CAMERA_INFO_REQ";
			objson["deviceMacAddress"] = m_mac.c_str();
			objson["deviceId"] = m_cDeviceID.c_str();
			objson["deviceToken"] = m_cdeviceToken.c_str();
			objson["deviceModelId"] = m_cDeviceModelId.c_str();
			objson["firmwareVersion"] = m_cFirmwareVersion.c_str();
			objson["cameraAppVersion"] = m_cCameraAppVersion.c_str();

			objson.toStyledString(msgout);
		}
		break;
		default:
		break;
	}
}
void CHemuHttpsComunication::ComposeSignature(string &objson, string &signature)
{
	char buf[33]={'\0'};
	char md5iput[1024] = {'\0'};
//	snprintf(md5iput,sizeof(md5iput),"Wm6UQgqTD64DctGNkgeKaccessKey=dac5a4da954jsonObject={\"userToken\":\"708a9c84b47404c5524405e5cbd910b8\",\"deviceMacAddress\":\"146b9c4bbf99\",\"deviceId\":\"111002990000381\",\"msgType\":\"MSG_P_THD_ADD_CAMERA_REQ\",\"comment\":\"[\'HW\':\'3RD-X01\',\'FEATURE\':17]\",\"userMobile\":\"13896118873\",\"deviceModelId\":\"cmhi_hdc_51_001\",\"registMode\":\"0\"}");
	snprintf(md5iput, sizeof(md5iput), "%saccessKey=%sjsonObject=%s",m_caccessSecret.c_str()/*HUMUCLOUD_PASSWORD*/, m_caccessKey.c_str()/*HUMUCLOUD_USERANME*/, objson.c_str());
	Md5Encode(md5iput,buf);
	signature = buf;
	_printd("signature string:%s",md5iput);
	_printd("signature:%s",signature.c_str());
}
void   CHemuHttpsComunication::onTimer(uint arg)
{
#if 0
	//由于定时器会出问题所以用线程代替

#endif
}
void CHemuHttpsComunication::SetNtpSuccess()
{
	m_iNtpSuccess = 1;
}


VD_INT32 CHemuHttpsComunication::GetCurTime(string &result)
{
	SYSTEM_TIME strNow;
	memset(&strNow, 0, sizeof(SYSTEM_TIME));
	SystemGetCurrentTime(&strNow);
	
	char requesttime[128] = {0};
	snprintf(requesttime,sizeof(requesttime), "%04d-%02d-%02d %02d:%02d:%02d",
			strNow.year, strNow.month, strNow.day, strNow.hour, strNow.minute, strNow.minute);
	result = requesttime;
	return 0;
}
VD_INT32 CHemuHttpsComunication::ConfigSyncSever(CConfigTable severconfig)
{
	if (m_nConfigSynchronize)
	{
		//////////////////开关/////////////////////////////
		int status = !strncmp(severconfig["status"].asString().c_str(), "On", 2);
		if (status)
		{
			g_HWComm.SetCameraStatus(status);
		}
		else
		{
			g_HWComm.SetCameraStatus(4);
		}
		_printd("status[%d]", status);
		///////////////////灵敏度//////////////////////////
		CConfigHemuAppCtrl configappctrl;

		configappctrl.update();
		CONFIG_HEMUAPPCTRL &configAppctrl = configappctrl.getConfig();

		int soundsensitivity = atoi(severconfig["soundSensitivity"].asString().c_str());

		_printd("soundsensitivity[%d]", soundsensitivity);
		int motionSensivity = atoi(severconfig["motionSensitivity"].asString().c_str());
		_printd("motionSensivity[%d]", motionSensivity);

		configAppctrl.iAduioSensitivity = (soundsensitivity - 75) / 5;
		AudioDetectSetParam(1, configAppctrl.iAduioSensitivity);


		///////////////////清晰度/////////////////////
		int HDVideo = !strncmp(severconfig["HDVideo"].asString().c_str(), "On", 2);
		_printd("HDVideo[%d]", HDVideo);
		g_HWComm.SetEncodeMode(HDVideo);
		/////////////////////麦克风/////////////////////	
		//	int cameraMicrophone =  !strncmp(severconfig["cameraMicrophone"].asString().c_str(),"On",2);
		//	_printd("cameraMicrophone[%d]",cameraMicrophone);	
		//	
		//	configAppctrl.iAduioEnable = cameraMicrophone;
		//	CDevAudioIn::instance(0)->SetMicEanble(cameraMicrophone);
		/////////////////////LED灯/////////////////////
		//	int statusLight =   !strncmp(severconfig["statusLight"].asString().c_str(),"On",2);
		//	_printd("statusLight[%d]",statusLight);	
		//	g_HWComm.LedCtrol(statusLight);
		///////////////////IRCUT/////////////////////
		//int nightVision = 0;
		//if(0 == strncmp(severconfig["nightVision"].asString().c_str(),"On",2))
		//{
		//	nightVision = 1;
		//}
		//else if(0 == strncmp(severconfig["nightVision"].asString().c_str(),"Auto",4))
		//{
		//	nightVision = 2;
		//}
		//
		//_printd("nightVision[%d]",nightVision);	
		//g_HWComm.ModCameraCfg(21,nightVision);
		////////////////////倒置///////////////////////
		//int cameraImageRotate = !strncmp(severconfig["cameraImageRotate"].asString().c_str(),"On",2);
		//_printd("cameraImageRotate[%d]",cameraImageRotate);	
		//g_HWComm.ModCameraCfg(23,cameraImageRotate);
		////////////////////防闪烁//////////////////////
		//int antiFlicker = 	atoi(severconfig["antiFlicker"].asString().c_str());
		//g_HWComm.ModCameraCfg(36,antiFlicker);
		//	_printd("antiFlicker[%d]",antiFlicker);	
		//////////////////////移动追踪//////////////////////
		if (ProductM_Q2 == PlatformGetHandle()->ProductModel)
		{
			int motionTrack = !strncmp(severconfig["motionTrack"].asString().c_str(), "On", 2);

			CConfigAlarmSwitch	*p_AlarmSwitch;
			p_AlarmSwitch = new CConfigAlarmSwitch;

			if (p_AlarmSwitch)
			{
				p_AlarmSwitch->update();
				CONFIG_ALARMSWITCH &configtrace = p_AlarmSwitch->getConfig();
				configtrace.bTraceEnable = motionTrack;
				//TrackerRunSet(motionTrack);
				p_AlarmSwitch->commit();
				delete p_AlarmSwitch;
			}

			_printd("motionTrack[%d]", motionTrack);
		}


		//	int motionTrackBackTime = severconfig["motionTrack"].asInt();
		//	_printd("motionTrackBackTime[%d]",motionTrackBackTime);
		configappctrl.commit();
	}
	m_nConfigSynchronize = 0;
	return 0;
}

VD_INT32 CHemuHttpsComunication::sendRequest(int msgtype,string &url, struct curl_slist *headerlist,
	struct curl_httppost *post,	string & result, long *statusCode)
{
    CURL *curl = NULL;
    CURLcode res = CURLE_OK;
    int opc = CURLE_OK;

	struct curl_httppost *post1;


    MemChunk chunk;
    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
    chunk.size = 0; 

    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	_printd("url:%s",url.c_str());
    // This option is here to allow multi-threaded unix applications to still set/use
    // all timeout options etc, without risking getting signals.
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); 

	   // set form-data to post
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
    curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

    /* send all data to this function  */ 
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);
    /* we pass our 'chunk' struct to the callback function */ 
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	// request options
	curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5L);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
 // curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

#if VERB_LEV >= 1
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#if VERB_LEV >= 2
    curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, debug_trace);
#endif
#endif

    res = curl_easy_perform(curl);

    long httpcode = 0;
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &httpcode);
	_printd("res[%d]httpcode[%d]",res,httpcode);
    if (res != CURLE_OK) 
	{
        fprintf(stderr, "curl_easy_perform() failed: (%d) %s\n", res, curl_easy_strerror(res));
        opc = res;//OPC_SENDFAILED;
    }
    else if (httpcode >= 200 && httpcode <= 400)
	{
        opc = handleResponse(msgtype,chunk.memory, chunk.size, result);
    }
    else 
	{
        opc = -1;
    }

    curl_easy_cleanup(curl);
    

    free(chunk.memory);

    *statusCode = httpcode;
    return opc;
}

VD_INT32  CHemuHttpsComunication::handleResponse(int msgType,const char * resp, size_t resp_len, string & result)
{
	_printd("Result:\n%s",resp);
	CConfigReader reader;
	CConfigTable  root;
	if (reader.parse(resp, root))
	{
		int errorCode = atoi(root["errorCode"].asString().c_str());
		//if(0 ==  errorCode)
		{
			switch(msgType)
			{
				case MSG_P_THD_ADD_CAMERA_REQ:
				{
					if(0 ==  errorCode)
					{
						m_cdeviceToken = root["deviceToken"].asString();
						m_cunifiedId = root["unifiedId"].asString();
						m_iBindStatus = 1;
						m_userInfo.update();
						CONFIG_HEMUUSERINFO &configuserinfo = m_userInfo.getConfig();
						snprintf(configuserinfo.UserMobile,16,"%s",m_cUserMobile.c_str());
						snprintf(configuserinfo.UserToken,128,"%s",m_cUserToken.c_str());
						snprintf(configuserinfo.devicetoken,128,"%s",m_cdeviceToken.c_str());
						snprintf(configuserinfo.unifiedId,128,"%s",m_cunifiedId.c_str());
						configuserinfo.BindStatus = 1;
						m_userInfo.commit();
					}
					else
					{
						if(m_iLastBindStatus)
						{
							m_iBindStatus = 1;
							//绑定不成功则退出更新配置
							m_isendcmd = 10;
						}
						else
						{
							m_iBindStatus = 0;
						}
					}
					
				}
				break;
				case MSG_P_THD_SET_CAMERA_CAPACITY_REQ:
				{
					_printd("ok");
					m_isendcmd +=1;
				}
				break;
				case MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ:
				{
					struct timeval tv;
					gettimeofday(&tv, NULL);
					m_cserverAddr = root["serverAddr"].asString();
					m_cserverPort = root["serverPort"].asString();
					m_userInfo.update();
					CONFIG_HEMUUSERINFO &configuserinfo = m_userInfo.getConfig();

					snprintf(configuserinfo.ipaddr, 32, "%s", m_cserverAddr.c_str());
					snprintf(configuserinfo.ipupdatetime, 16, "%ld", tv.tv_sec);
					m_cipupdatetime = configuserinfo.ipupdatetime;
					configuserinfo.iport = atoi(m_cserverPort.c_str());

					m_userInfo.commit();
					
					m_isendcmd +=1;
				}
				break;
				case MSG_P_THD_GET_CAMERA_SETTINGS_REQ:
				{
					//root["camRW"]
					m_isendcmd +=1;
					ConfigSyncSever(root["camRW"]);
				}
				break;
				case MSG_P_THD_SET_CAMERA_SETTINGS_REQ:
				{
					m_isendcmd +=1;
				}
				break;
				case MSG_P_THD_SET_CAMERA_INFO_REQ:
				{
					m_isendcmd +=1;	
				}
				break;
				default:
				break;
			}
		}	
		//else
		//{
			//_printd("errorCode:%s ,description:%s",root["errorCode"].asString().c_str(),root["description"].asCString());
			//return FALSE;
		//}
		
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}


VD_INT32   CHemuHttpsComunication::SendOpenAPIReq(MSG_P_THD cmd)
{
	long statuscode;
	string url;
	string result;
	string msgobject;
	string signature;
	struct curl_slist *headerlist = NULL;	
	struct curl_httppost *formpost = NULL;

	HeMucompose_header(&headerlist);
	
	ComposeMsgObject(cmd, msgobject);
	ComposeSignature(msgobject,signature);	
	ComposeForm(&formpost,msgobject,signature);

	url = HEMU_OPENAPI;
	
	if (NULL == headerlist)
        return -1;
	if(!url.empty())
	{
		sendRequest(cmd,url, headerlist,formpost,result,&statuscode);
		curl_formfree(formpost);
		curl_slist_free_all (headerlist);
		if(200 == statuscode)
		{
			return 0;
		}
	}
	return -1;	
}
void CHemuHttpsComunication::GetServerInfo(string &addr, string &port,int isupdate)
{
	long ipupdatetime = 0;
	struct timeval tv;
	gettimeofday(&tv, NULL);
	if (m_cserverAddr.empty())
	{
		m_userInfo.update();
		CONFIG_HEMUUSERINFO &configNew = m_userInfo.getConfig();

		m_iBindStatus = configNew.BindStatus;
		if (m_iBindStatus)
		{
			m_cUserMobile = configNew.UserMobile;
			m_cUserToken = configNew.UserToken;
			m_cdeviceToken = configNew.devicetoken;
			m_cunifiedId = configNew.unifiedId;
			m_cipupdatetime = configNew.ipupdatetime;
			m_cserverAddr = configNew.ipaddr;
			char cport[8] = { 0 };
			snprintf(cport, sizeof(cport), "%d", configNew.iport);
			m_cserverPort = cport;
		}
	}
	sscanf(m_cipupdatetime.c_str(), "%ld", &ipupdatetime);
	if ((tv.tv_sec - ipupdatetime > 12 * 60 * 60) || isupdate || m_cserverAddr.empty())
	{
		SendOpenAPIReq(MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ);
	}
	addr = m_cserverAddr; 
	port = m_cserverPort; 
}

void CHemuHttpsComunication::GetDevLogInfo(DevLoginReq  &logreq)
{
	logreq.access_key = (char*)m_caccessKey.c_str();//HUMUCLOUD_USERANME;
	logreq.access_secret = (char*)m_caccessSecret.c_str();//HUMUCLOUD_PASSWORD;
	logreq.user_name = (char*)m_cUserMobile.c_str();
	if(m_cdeviceToken.empty() || m_cunifiedId.empty())
	{
		SendOpenAPIReq(MSG_P_THD_ADD_CAMERA_REQ);
	}
	logreq.device_token = (char*)m_cdeviceToken.c_str();
	logreq.unified_id  = (char*)m_cunifiedId.c_str();
	logreq.device_mac  = (char*)m_mac.c_str();
	logreq.channel_id  = 0;
	logreq.video_width = 1080;
	logreq.video_type = 1;
	logreq.video_height = 1024;
	logreq.audio_type = 2;
	logreq.audio_khz = 8;
	logreq.audio_channel = 1;
}
int CHemuHttpsComunication::DownLoad()
{
	_printd("upgrade============>>>>");
	CURL *curl;  
	int  nFileLenght = 0;
    CURLcode res = CURLE_RECV_ERROR;
	//尝试下载5次
	if (file_exists((char*)UPGRADE_FILE))
	{
		remove(UPGRADE_FILE);
	}
	for (int i = 0; i < 5; i++)
	{
		curl = curl_easy_init();
		if (curl)
		{
			curl_easy_setopt(curl, CURLOPT_URL, m_cupgradeurl.c_str());
			//option	
			curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 15L);
			curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 15L);
			curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
			curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 15L);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);	// 从证书中检查SSL加密算法是否存在
			//指定回调函数  
			if (file_exists((char*)UPGRADE_FILE))
			{
				nFileLenght = GetLocalFileLenth(UPGRADE_FILE);
				if(0 != nFileLenght)
				{
					curl_easy_setopt(curl, CURLOPT_RESUME_FROM, nFileLenght);
				}
				else
				{
					remove(UPGRADE_FILE);
				}
			}
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefile_callback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, UPGRADE_FILE);
			res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			if (CURLE_OK == res)
			{
				break;
			}

		}
		else
		{
			_printd("CURL curl_easy_init ERROR!!!!");
		}
	}
	return res;
}
void CHemuHttpsComunication::SetUpgradeInfo(string url, string checksum,string version,int msgSession)
{
	m_cupgradeurl = url;
	m_cchecksum = checksum; 
	m_cversion = version;
	m_imsgsession = msgSession;
	m_istartupgrade = TRUE;
}
