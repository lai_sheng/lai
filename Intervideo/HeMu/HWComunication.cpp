#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include "System/File.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#include "Intervideo/HeMu/HWComunication.h"
#include "Intervideo/HeMu/HemuProbuf.pb-c.h"	
#include "APIs/CommonBSP.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Functions/LedStat.h"
#include "System/AppEvent.h"
#include "Functions/WifiLink.h"
#include "Functions/DaemonManager.h"
#include "APIs/MotionDetect.h"
#include "Intervideo/HeMu/HemuRecordManager.h"
#include "Net/Dlg/DlgNtpCli.h"

#define HEMU_OPENAPI "third.reservehemu.com"

#define BR_HDMODE 0     //??
#define BR_SDMODE 1     //??		
#define BR_LUMODE 2     //??

char maintainFile[240] = "/mnt/temp/maintain";
#ifdef WIN32
typedef Json::StyledWriter CConfigWriter;
#else
typedef Json::FastWriter CConfigWriter;
#endif
#ifdef IPC_JZ_NEW
#ifdef FAC_CHECK
const int Bitrate_T10[3] = {1000,512,256};
const int Bitrate_T20[3] = {2000,800,256};
#else
#ifdef IPC_JZ_T21
const int Bitrate_T10[3] = {800,512,256};
const int Bitrate_T20[3] = {900,800,256};
#else
const int Bitrate_T10[3] = {800,512,256};
const int Bitrate_T20[3] = {1000,800,256};
#endif
#endif
#endif

#ifdef IPC_JZ
const int Bitrate_100W[3] = {800,512,256};
const int Bitrate_200W[3] = {1200,800,256};
#endif
//低7位有效，按二进制从低位到高位表示周日到周六
//SUN:0x0001, MON:0x0002, TUE:0x0004, WED:0x0008,
//THU:0x0010, FRI:0x0020, SATU:0x0040
static uint dayrepeat[7] ={0x0001,0x0002,0x0004,0x0008,0x0010,0x0020,0x0040};

#define  SEND_BUFFUER (512*1024)

PATTERN_SINGLETON_IMPLEMENT( CHWComunication);

CHWComunication::CHWComunication()
	:CThread("CHWComunication", TP_NET)
	, m_iNtpSuccess(0)
	, m_fd(-1)
	, m_connStatus(eUnconnected)
	, m_iconTimeoutTime(0)
	, m_iauthState(AUTH_INIT)
	, m_ifailcnt(0)
	, m_ipingspan(15)
	, m_iaudioSeq(0)
	, m_ivideoSeq(0)
	, m_icontrolflag(0)
	, m_packet_count(0)
	, m_packet_size(0)
	, m_dropFramFlag(0)
	, m_ipongtimeout(0)
	, m_isockstaus(0)
	, m_iHWConect(0)
	, m_idevicestatus(0)
	, m_packet_cache_count(0)
	, m_ieventcount(0)
	, m_packet_max_size(0)
	, m_isdreupload(0)
	, m_isdstartimestamp(0)
	, m_isdendtimestamp(0)
	, m_isdeventstartimestamp(0)
	, m_isdeventendtimestamp(0)
	, m_imaintainsend(0)
	, m_imaintainstartimestamp(0) 
	, m_imaintainendtimestamp(0)  
	, m_imaintaintotalsize(0)    
	, m_imaintaintotalVideoFrames(0)   
	, m_imaintaindropVideoFrames(0)
	, m_imaintaintotalAudioFrames(0)   
	, m_imaintaindropAudioFrames(0)
	, m_imaintainlogintime(0)
	, m_imaintainonlineDuration(0)     
	, m_imaintaindisconnectTimes(0)   
	, m_imaintainrebootTimes(0)       
	, m_imaintaindelay(0)	   
	, m_imaintaintotaldelay(0)        
	, m_imaintaintotalsend(0)  
	, m_iopenApiFailureTimes(0)        
	, m_iassignHedgwIpTimes(0)       
	, m_iupgrade(0)
{
	
	m_pSendBuf.Reset();
	m_pRecvBuf.Reset();
}

CHWComunication::~CHWComunication()
{
	Close();
	_printd("~  CHWComunication");
}




VD_INT32   CHWComunication::Start()
{
	char uuid[16] = { '\0' };
	if (!GetUuid(uuid))
	{
		m_csrcdeviceid = uuid;//"119000310000101"; 
	}
	if (ProductM_Q2 == PlatformGetHandle()->ProductModel)
	{
		m_packet_max_size = 2 * 1024 * 1024;
	}
	else
	{
		m_packet_max_size = 512 * 1024;
	}
	CreateThread();
	m_tls.Initctx();
	m_Talk.Start();
	m_Cmd.Start();
	m_configScheduleWorksheet.attach(this, (TCONFIG_PROC)&CHWComunication::onConfigSchedule);
	m_configScheduleWorksheet.update();
	m_configHemuAlarmWorksheet.attach(this, (TCONFIG_PROC)&CHWComunication::onConfigAlarm);
	m_configHemuAlarmWorksheet.update();
	m_cTimer.Start(this, (VD_TIMERPROC)&CHWComunication::onTimer,/* 20000*/20, 10000);
	SystemGetCurrentTime(&m_oldTime);

	//初始化维护信息
	InitMaintainConfig();
	_printd ("  CHWComunication Init Over");
	return 0;
}
void CHWComunication::onTimer(uint arg)
{
	SYSTEM_TIME curTime;

	SystemGetCurrentTime(&curTime);

	// 每分钟检查一次时间段
	if(m_oldTime.year != curTime.year
		|| m_oldTime.month != curTime.month
		|| m_oldTime.day != curTime.day
		|| m_oldTime.hour != curTime.hour
		|| m_oldTime.minute != curTime.minute)
	{
		CheckSection(0);
//		CheckSection(1);
	}
	m_oldTime = curTime;
	MaintainUpdateVar();
	if ((m_connStatus == eConnected)&&(m_maintainDate.year != curTime.year
		|| m_maintainDate.month != curTime.month
		|| m_maintainDate.day != curTime.day))
	{  //一天一次发送维护信息	
		if (m_imaintainstartimestamp)
		m_imaintainsend = TRUE;
	}
}
void CHWComunication::onSnapTimer(uint arg)
{
	//TODO定时截图
	_printd("snap picture");
}

VD_INT32   CHWComunication::Stop()
{
	_printd("  CHWComunication :: Stop ");
	Close();
	return 0;
}
VD_INT32   CHWComunication::CameraSchedule(int type,CConfigTable         &schedule)
{
	
	CONFIG_WORKSHEET *pcfgWorksheet =  NULL;
	CConfigGeneral cCfgGeneral;

	cCfgGeneral.update();
	CONFIG_GENERAL &config = cCfgGeneral.getConfig();
	config.iOnOffMode = 0;//定时休眠
	cCfgGeneral.commit();
	if(type)
	{//告警计划
		pcfgWorksheet = &m_configHemuAlarmWorksheet.getConfig();
	}
	else
	{//休眠计划
		pcfgWorksheet = &m_configScheduleWorksheet.getConfig();
	}
	memset(pcfgWorksheet, 0, sizeof(CONFIG_WORKSHEET));
	int valueListNum = schedule["msgContent"]["requestParams"]["valueListNum"].asInt();
	for(int i = 0; i < valueListNum; i++)
	{  //计划列表
		int utcstarthour,utcstartmin;
		int utcendhour,utcendmin;
		int starthour,startmin;
		int endhour,endmin;
		int repeat = schedule["msgContent"]["requestParams"]["valueList"][i]["repeat"].asInt();
		string cstatus = schedule["msgContent"]["requestParams"]["valueList"][i]["status"].asString();

		sscanf(schedule["msgContent"]["requestParams"]["valueList"][i]["startTime"].asString().c_str(),	"%02d:%02d",&utcstarthour,&utcstartmin);
		sscanf(schedule["msgContent"]["requestParams"]["valueList"][i]["endTime"].asString().c_str(),	"%02d:%02d",&utcendhour,&utcendmin);
		//转换为北京时间
		starthour =  (utcstarthour+8)%24;
		endhour   =  (utcendhour + 8)%24;
		startmin  = utcstartmin;
		endmin    = utcendmin;
		if(cstatus.compare("false") == 0) continue;
		//周日到周六
		for(int j =0; j < 7; j++)
		{	
			//当天是否有计划
			if(dayrepeat[j] &  repeat)
			{	
				//跨天
				if(starthour >= endhour)
				{
					//当天
					for(int k=0; k<N_UI_TSECT; k++)
                    {
                        int enable = pcfgWorksheet->tsSchedule[j][k].enable;
						if(false == enable)
						{
							pcfgWorksheet->tsSchedule[j][k].enable =  true; 
							pcfgWorksheet->tsSchedule[j][k].startHour =  starthour; 
							pcfgWorksheet->tsSchedule[j][k].startMinute = startmin;
							pcfgWorksheet->tsSchedule[j][k].startSecond = 0;
							pcfgWorksheet->tsSchedule[j][k].endHour =  24; 
							pcfgWorksheet->tsSchedule[j][k].endMinute = 0;
							pcfgWorksheet->tsSchedule[j][k].endSecond = 0;
							break;
						}
                    }
					//第二天
					for(int k=0; k<N_UI_TSECT; k++)
                    {
                        int enable = pcfgWorksheet->tsSchedule[j][k].enable;
						if(false == enable)
						{
							int workday = ((j+1)%7);
							pcfgWorksheet->tsSchedule[workday][k].enable =  true; 
							pcfgWorksheet->tsSchedule[workday][k].startHour =  0; 
							pcfgWorksheet->tsSchedule[workday][k].startMinute = 0;
							pcfgWorksheet->tsSchedule[workday][k].startSecond = 0;
							pcfgWorksheet->tsSchedule[workday][k].endHour =  endhour; 
							pcfgWorksheet->tsSchedule[workday][k].endMinute = endmin;
							pcfgWorksheet->tsSchedule[workday][k].endSecond = 0;
							break;
						}
                    }
					_printd("day[%d]:start[%d:%d] -- end[%d:%d]", j,starthour,startmin,24,00);	
					_printd("day[%d]:start[%d:%d] -- end[%d:%d]", (j+1)%7,0,0,endhour,endmin);	
				}
				else
				{
					for(int k=0; k<N_UI_TSECT; k++)
                    {
                        int enable = pcfgWorksheet->tsSchedule[j][k].enable;
						if(false == enable)
						{
							pcfgWorksheet->tsSchedule[j][k].enable =  true; 
							pcfgWorksheet->tsSchedule[j][k].startHour =  starthour; 
							pcfgWorksheet->tsSchedule[j][k].startMinute = startmin;
							pcfgWorksheet->tsSchedule[j][k].startSecond = 0;
							pcfgWorksheet->tsSchedule[j][k].endHour =  endhour; 
							pcfgWorksheet->tsSchedule[j][k].endMinute = endmin;
							pcfgWorksheet->tsSchedule[j][k].endSecond = 0;
							break;
						}
                    }
					_printd("day[%d]:start[%d:%d] -- end[%d:%d]", j,starthour,startmin,endhour,endmin);
				}
			}
		}
	}
	if(type)
	{//告警
		m_configHemuAlarmWorksheet.commit();
		CheckSection(type);
	}
	else
	{//
		m_configScheduleWorksheet.commit();
		CheckSection(type);
	}
	
	return 0;
}

VD_INT32   CHWComunication::ModCameraCfg(int ctrl,int value)
{
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();

	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();

	switch(ctrl)
	{
		case 21:
		{
			if(0 == value)
			{
				cfgCamera.IRCut = 2;
			}
			else if(1 == value)
			{
				cfgCamera.IRCut = 1;
			}
			else
			{
				cfgCamera.IRCut = 0;
			}
		}
		break;
		case 23:
		{
			//0：不旋转
			//180: 旋转180度
			if(0 == value)
			{
				cfgCamera.HorReverse = 0;
				cfgCamera.VerReverse = 0;
			}
			else
			{
				cfgCamera.HorReverse = 1;
				cfgCamera.VerReverse = 1;
			}
		}
		break;
		case 36:
		{
			if(50 == value)
			{
				cfgCamera.AntiFlicker = 1;
			}
			else
			{
				cfgCamera.AntiFlicker = 2;
			}
		}
		break;
		default:
		break;
		
	}
	ConfigCamera.commit();

	return 0;
}
VD_INT32    CHWComunication::LedCtrol(int value)
{
	if(value)
	{
		g_ledstat.SetLedSwitch(0,1);
	}
	else
	{
		g_ledstat.SetLedSwitch(1,1);
	}
	return 0;
}

VD_INT32   CHWComunication::SetEncodeMode(int mode)
{
	int Ret 		 = 0;
	int eCodeMode = mode ? mode:(mode+2);
	uint32_t BitRate = 0;
	
	CConfigEncode* pCfgEncode = new CConfigEncode();
	pCfgEncode->update();
	CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig();

	int nfps   = 0;
	int igop   = 0;
	Get_Video_Nfps_Igop(&nfps, &igop);
	int with = 1280;
	int height = 720;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iGOP = igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = nfps; 

#ifdef FAC_CHECK  //fac_check  720P=1M  1080P=2M
	eCodeMode = RT_HDMODE;
#endif
	_printd("eCodeMode[%d]",eCodeMode);
	switch(eCodeMode)
	{
		case RT_HDMODE:
		{			
		#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_1080P;
				BitRate = Bitrate_T20[BR_HDMODE];//800;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;	
				with = 1920;
				height = 1080;
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
					 ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_720P;
				BitRate = Bitrate_T10[BR_HDMODE];//800;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;	
				with = 1280;
				height = 720;
			}
		#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_HDMODE 720P Mode");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_720P;
				BitRate = Bitrate_100W[BR_HDMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_HDMODE 1080P Mode");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_1080P;
				BitRate = Bitrate_200W[BR_HDMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			}
		#else
			_printd("RT_HDMODE 720P Mode");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
			CAPTURE_SIZE_720P;
			BitRate = 800;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			with = 1280;
			height = 720;
		#endif
		}
		break;
		case RT_SDMODE:
#if 0
		{

		#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("720P RT_SDMODE"); 
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_720P;
				BitRate = Bitrate_T20[BR_SDMODE];//600;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;		
				with = 640;
				height = 360;
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
					 ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("360P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_SDMODE];//512;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
				with = 640;
				height = 360;
			}
		#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("640x360P RT_SDMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_SDMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
				with = 640;
				height = 360;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("720P RT_SDMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_720P;
				BitRate = Bitrate_200W[BR_SDMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
				with = 640;
				height = 360;
			}
		#else
			_printd("640x360P RT_SDMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
			CAPTURE_SIZE_360P;
			BitRate = 512;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;	
			with = 640;
			height = 360;
		#endif
		}
		break;
#endif
		case RT_LUMODE:
		{
		#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("360P RT_LUMODE"); 
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_T20[BR_LUMODE];//600;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;	
				with = 640;
				height = 360;
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
					 ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("360P RT_LUMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_LUMODE];//600;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;	
				with = 640;
				height = 360;
			}
		#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("360P RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_LUMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
				with = 640;
				height = 360;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("360P RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
				CAPTURE_SIZE_360P;
				BitRate = Bitrate_200W[BR_LUMODE];
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;	
				with = 640;
				height = 360;
	    	}
		#else
			_printd("360P RT_LUMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= 
			CAPTURE_SIZE_360P;
			BitRate = 256;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;	
			with = 640;
			height = 360;
		#endif
		}
		break;
		default:
		{
			Ret = -1;
		}
		break;
	}
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate = BitRate;
	if(Ret != -1)	pCfgEncode->commit();

	delete pCfgEncode;
	ModResolutionNotice(with,height);
	return Ret ;

}
VD_INT32   CHWComunication::SetCameraStatus(int value)
{
	CConfigGeneral cCfgGeneral;

	cCfgGeneral.update();
	CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
	config.iOnOffMode = 1;
	if(1 == value){
		config.iSuspendMode = 0;

	}
	else
	{
		if( 1 == config.iSuspendMode )
		{
#ifdef MOTOR
			SetMotorMidPos();
#endif
			
		}
		config.iSuspendMode = 1;
	}

	cCfgGeneral.commit();

	if(value == 4){
#ifdef MOTOR
		SetMotorTopPos();
#endif
	}
	else
	{
		 #ifdef IPC_JZ_NEW
		if(ChipT10 == PlatformGetHandle()->Chip){
			int nWidth  = 0;
			int nHeight = 0;
			CaptureGetCurHeightWidth(&nWidth, &nHeight);
			if(nWidth == 640 && nHeight == 360){
				_printd("T10 640X360 ForceIFrame...\n");
				return CaptureForceIFrame(0,1);   //双码流   强制次码流
			}
		}
		#endif
		return CaptureForceIFrame(0,0);
	}

	return 0;
}
VD_INT32   CHWComunication::AppCtrol(char *msg)
{
	CConfigReader reader;
	CConfigTable  root;
	if (reader.parse(msg, root))
	{
		if (root.isMember("msgContent"))
		{
			int request = root["msgContent"]["request"].asInt();
			if(request == 1815) //.... camera ..........
			{
				int value = root["msgContent"]["requestParams"]["value"].asInt();
				_printd("client request to turn %s camera. ",value==1?"on":"off");
				SetCameraStatus(value);
			}
			if(request == 1793)
			{
				int sub_request = root["msgContent"]["subRequest"].asInt();
				_printd("receive sub request:%d. ", sub_request);
				
				
				switch(sub_request)
				{	
					case 2:
					{
						CConfigAlarmSwitch	AlarmSwitch;
						AlarmSwitch.update();
						CONFIG_ALARMSWITCH &configalarm = AlarmSwitch.getConfig();
						configalarm.bMotionEnable = root["msgContent"]["requestParams"]["value"].asInt();
						AlarmSwitch.commit();
					}
					break;
					case 5:
					{
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						m_Cmd.AddCmdData(0,value);
					}
					break;
					case 15:
					{
						//运动检测灵敏度
						MOTION_DETECT_PARAM detect;
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						int iLevel = 0;
						m_configHemuAppCtrl.update();
						CONFIG_HEMUAPPCTRL &configAppctrl = m_configHemuAppCtrl.getConfig();
						switch (value)
						{
							case 30:
								iLevel = 1;
								break;
							case 50:
								iLevel = 2;
								break;
							case 80:
								iLevel = 3;
								break;
							case 90:
								iLevel = 4;
								break;
							case 100:
								iLevel = 5;
								break;
							default:
								break;
						}
						configAppctrl.iMotionSensitivity = iLevel;
						detect.enable = 1;
						detect.iLevel = iLevel;
						detect.sensitiveness = value;
						MotionDetectSetParameter(1, &detect);
						m_configHemuAppCtrl.commit();
					}
					break;
					case 17:
					{
						ChangeConectWifi(root["msgSession"].asInt(),
							(char*)root["msgContent"]["requestParams"]["ssid"].asString().c_str(),
							(char*)root["msgContent"]["requestParams"]["key"].asString().c_str());
						
					}
					break;
					case 19:
					{//LED:OPEN,CLOSE
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						LedCtrol(value);
					}
					break;
					case 21:
					case 23:
					case 36:
					{
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						//图像旋转
						ModCameraCfg(sub_request,value);	
					}
					break;
					case 26:
					{
						//工作时间段
						CameraSchedule(0,root);
					}
					break;
					case 28:
					{
						CConfigAlarmSwitch	AlarmSwitch;
						AlarmSwitch.update();
						CONFIG_ALARMSWITCH &configalarm = AlarmSwitch.getConfig();
						configalarm.bVoiceEnable = root["msgContent"]["requestParams"]["value"].asInt();
						AlarmSwitch.commit();
					}
					break;
					case 29:
					{//声音灵敏度
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						m_configHemuAppCtrl.update();
						CONFIG_HEMUAPPCTRL &configAppctrl = m_configHemuAppCtrl.getConfig();
						
						configAppctrl.iAduioSensitivity = (value-75)/5;
						AudioDetectSetParam(1,configAppctrl.iAduioSensitivity);
						m_configHemuAppCtrl.commit();
					}
					break;
					case 30:
					{//修改分辨率
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						
						_printd("encodemode[%d]",value);	
						SetEncodeMode(value);		
					}
					break;
					case 37:
					{//MIC 开关					
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						m_configHemuAppCtrl.update();
						CONFIG_HEMUAPPCTRL &configAppctrl = m_configHemuAppCtrl.getConfig();
						
						configAppctrl.iAduioEnable = value;
						CDevAudioIn::instance(0)->SetMicEanble(value);
						m_configHemuAppCtrl.commit();
					}
					break;
					case 81:
					{
						int  pan = root["msgContent"]["requestParams"]["pan"].asInt();
						int  tilt = root["msgContent"]["requestParams"]["tilt"].asInt();
						_printd("pan[%d] tilt[%d]",pan,tilt);
						
						m_Cmd.AddCmdData(3, pan,tilt);
					}
					break;
					case 82:
					{
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						m_Cmd.AddCmdData(1,value);
					}
					break;
					case 87:
					{
						SdcardFormat(root["msgSession"].asInt());
					}
					break;
					case 109:
					{
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						CConfigAlarmSwitch	*p_AlarmSwitch;
						p_AlarmSwitch = new CConfigAlarmSwitch;

						if (p_AlarmSwitch)
						{
							p_AlarmSwitch->update();
							CONFIG_ALARMSWITCH &configtrace = p_AlarmSwitch->getConfig();							
							configtrace.bTraceEnable = value;
							_printd("configtrace.bTraceEnable[%d]", configtrace.bTraceEnable);
							//TrackerRunSet(value);
							p_AlarmSwitch->commit();
							delete p_AlarmSwitch;
						}
						//_printd("");
					}
					break;
					case 116:
					{
						int value = root["msgContent"]["requestParams"]["value"].asInt();
						CConfigAlarmSwitch	*p_AlarmSwitch;
						p_AlarmSwitch = new CConfigAlarmSwitch;

						if (p_AlarmSwitch)
						{
							p_AlarmSwitch->update();
							CONFIG_ALARMSWITCH &configtrace = p_AlarmSwitch->getConfig();
							configtrace.iTraceTimeOut = value;
							TrackerGobackTimeSet(value);
							p_AlarmSwitch->commit();
							delete p_AlarmSwitch;
						}
					}
					break;
					case 202:
					{
						//告警时间计划
						_printd("202");
						//告警计划平台处理
						//CameraSchedule(1,root);
					}
					break;
					default:
					break;
				}
			}
			else if(request == 1792)
			{			
				int sub_request = root["msgContent"]["subRequest"].asInt();
				switch(sub_request)
				{	
					case 16:
					{
						GetWifiList(root["msgSession"].asInt());				
					}
					break;
					case 81:
					{
						GetPtzPosition(root["msgSession"].asInt());
					}
					break;
					case 85:
					{
						GetSdInfo(root["msgSession"].asInt());
					}
					break;
					default:
					break;
				}
			}
			else if (request == 4097)
			{
				Upgrade(root);
			}
		}

	}

	return 0;
}
VD_INT32   CHWComunication::HwCtrol()
{
	
	return 0;
}

VD_INT32   CHWComunication::DoRecvHandle()
{
	int recvlen = 0;
	int processlen = 0;
	unsigned char recvbuf[4096];


	if (m_connStatus != eConnected) return 0;

	memset(recvbuf, 0, sizeof(recvbuf));
	recvlen = m_tls.Recv(recvbuf, sizeof(recvbuf));

	if (recvlen <= 0)
	{
		if (-3 == recvlen)
		{
			_printd("close");
			m_isockstaus = SSL_FINISHED;
		}
		return -1;
	}

	m_pRecvBuf.Append(recvbuf, recvlen);
	while (1)
	{
		if (m_pRecvBuf.Size() >= sizeof(int))
		{
			memcpy((unsigned char*)&processlen, m_pRecvBuf.Buf(), sizeof(int));
			processlen = ntohl(processlen);
		}
		else
		{
			break;
		}
		//_printd("processlen[%d]m_pRecvBuf.Size()[%d]", processlen, m_pRecvBuf.Size());
		if (m_pRecvBuf.Size() >= (processlen + sizeof(int)))
		{
			//recvlen = ntohl(recvlen);
			//
			//
			//if(m_tls.Recv(recvbuf, recvlen) != recvlen)
			//{
			//	_printd("close");
			//	m_isockstaus = SSL_FINISHED;
			//	return -1;
			//}
			m_pRecvBuf.Pop(recvbuf, processlen + sizeof(int));

			HEDGWMessage *res_info;
			res_info = hedgwmessage__unpack(NULL, processlen, recvbuf + sizeof(int));
			if (res_info)
			{
				_printd("res_info->message_type:%d", res_info->message_type);
				switch (res_info->message_type)
				{
				case HEDGWMESSAGE__MESSAGE_TYPE__DevLoginResp:
				{
					_printd("dev_login_resp.result:%d", res_info->dev_login_resp->result);
					_printd("dev_login_resp.server_time:%s", res_info->dev_login_resp->server_time);
					_printd("dev_login_resp.device_status:%d", res_info->dev_login_resp->device_status);
					switch (res_info->dev_login_resp->device_status)
					{
					case 0:
					case 1:
					{
						m_idevicestatus = 1;
					}
					break;
					case 2:
					{
						m_idevicestatus = 0;
					}
					break;
					case 3:
					{
						m_idevicestatus = 2;
					}
					break;
					case 44:
					{//已经解绑
						//断开连接
						m_iHWConect = 0;
						TrackerRunSet(0);
						g_WifiLink.ResetAllCfg();
						g_WifiLink.ResetNetwork();
						CConfigHemuUeserInfo *pconfig = new CConfigHemuUeserInfo;
						pconfig->update();
						CONFIG_HEMUUSERINFO &config = pconfig->getConfig();
						config.BindStatus = 0;
						pconfig->commit();
						delete pconfig;
					}break;
					default:
						break;

					}
					_printd("dev_login_resp.ping_pong_span:%d", res_info->dev_login_resp->ping_pong_span);
					m_ipingspan = res_info->dev_login_resp->ping_pong_span;
					m_isdreupload = res_info->dev_login_resp->sd_upload_mode;
					if (m_isdreupload)
					{
						//支持重传就使能重传
						g_HMRecordManager.SetConect(1);
					}
					_printd("dev_login_resp.sd_upload_mode:%d", res_info->dev_login_resp->sd_upload_mode);
					_printd("dev_login_resp.person_count_span:%d", res_info->dev_login_resp->person_count_span);
				}break;
				case HEDGWMESSAGE__MESSAGE_TYPE__PONG:
				{
					_printd("Pong");
					m_ipongtimeout = 0;
				}
				break;
				case HEDGWMESSAGE__MESSAGE_TYPE__DevServerCmd:
				{
#if 0
					_printd("dev_server_cmd.type:%d",res_info->dev_server_cmd->type);
					_printd("dev_server_cmd.src_device_id:%d",res_info->dev_server_cmd->device_status);
					_printd("dev_server_cmd.dst_device_id:%d",res_info->dev_server_cmd->thumbnail_span);
					_printd("dev_server_cmd.count:%d",res_info->dev_server_cmd->thumbnail_width);
					_printd("dev_server_cmd.id:%d",res_info->dev_server_cmd->thumbnail_height);
#endif
					if (7 == res_info->dev_server_cmd->type)
					{
						switch (res_info->dev_server_cmd->device_status)
						{
						case 0:
						case 1:
						{
							m_idevicestatus = 1;
						}
						break;
						case 2:
						{
							m_idevicestatus = 0;
						}
						break;
						case 3:
						{
							m_idevicestatus = 2;
						}
						break;
						case 44:
						{//解绑
							//断开连接
							m_iHWConect = 0;
							g_WifiLink.ResetAllCfg();
							g_WifiLink.ResetNetwork();
							CConfigHemuUeserInfo *pconfig = new CConfigHemuUeserInfo;
							pconfig->update();
							CONFIG_HEMUUSERINFO &config = pconfig->getConfig();
							config.BindStatus = 0;
							pconfig->commit();
							delete pconfig;
						}
						break;
						default:
							break;

						}
						_printd("device status change to %d.", res_info->dev_server_cmd->device_status);



					}
					else if (9 == res_info->dev_server_cmd->type)
					{

						if (res_info->dev_server_cmd->has_thumbnail_span)
						{
							if (res_info->dev_server_cmd->thumbnail_span)
							{
								m_ithumbnail_span = res_info->dev_server_cmd->thumbnail_span*(uint)1000;  //截图间隔
								m_cSnapTimer.Start(this, (VD_TIMERPROC)&CHWComunication::onSnapTimer, m_ithumbnail_span, m_ithumbnail_span);
							}
							else
							{
								m_cSnapTimer.Stop();
							}
						}
						if (res_info->dev_server_cmd->has_thumbnail_width)
						{
							m_ithumbnail_width = res_info->dev_server_cmd->thumbnail_width;  //截图宽
						}
						if (res_info->dev_server_cmd->has_thumbnail_height)
						{
							m_ithumbnail_height = res_info->dev_server_cmd->thumbnail_height; //截图高
						}
					}

				}
				break;
				case HEDGWMESSAGE__MESSAGE_TYPE__DevAppCmd:
				{
					_printd("dev_app_cmd.type:%d", res_info->dev_app_cmd->type);
					if (8 == res_info->dev_app_cmd->type)
					{
						m_icontrolflag = 1;
						m_iFirstIFrame = 0;
						m_mutex_list.Enter();
						while (m_packet_count > 0)
						{
							void* pBuf = NULL;
							uint  len = 0;

							PACKET_LIST::iterator it = m_packet_list.begin();
							pBuf = it->pBuf;
							len = it->size;
							m_packet_list.pop_front();
							m_packet_count--;
							m_packet_size -= len;
							free(pBuf);
						}
						m_mutex_list.Leave();

						CaptureForceIFrame(0, 0);
					}
					else if (9 == res_info->dev_app_cmd->type)
					{
						m_icontrolflag = 0;
					}
#if 1
					_printd("dev_app_cmd.src_device_id:%s", res_info->dev_app_cmd->src_device_id);
					_printd("dev_app_cmd.dst_device_id:%s", res_info->dev_app_cmd->dst_device_id);
					_printd("dev_app_cmd.count:%d", res_info->dev_app_cmd->count);
					_printd("dev_app_cmd.id:%d", res_info->dev_app_cmd->id);
					_printd("dev_app_cmd.msg:%s", res_info->dev_app_cmd->msg);
#endif
					if (NULL != res_info->dev_app_cmd->src_device_id)
					{
						m_cdstdeviceid = res_info->dev_app_cmd->src_device_id;
					}
					if (33 == res_info->dev_app_cmd->type &&res_info->dev_app_cmd->msg)
					{

						//	m_cdstdeviceid = res_info->dev_app_cmd->dst_device_id;
						AppCtrol(res_info->dev_app_cmd->msg);
						_printd("%s", res_info->dev_app_cmd->msg);
					}
				}
				break;
				case HEDGWMESSAGE__MESSAGE_TYPE__TDMediaPackage:
				{
					if (1 == res_info->td_media_package->package_type)
					{
						_printd("this is audio talk data[%d].", res_info->td_media_package->data_size);
						if (res_info->td_media_package->data_size > 12)
						{
							m_Talk.AddTalkData(res_info->td_media_package->data.data + 12, res_info->td_media_package->data_size - 12);
						}
					}
				}
				break;
				default:
					break;
				}

				hedgwmessage__free_unpacked(res_info, NULL);
			}
		}
		else
		{
			break;
		}
	}
	return 0;
}
VD_INT32   CHWComunication::DoSendHandle()
{
	if(m_connStatus != eConnected) return 0;
	m_isendout = 0;
	if(m_pSendBuf.Size() > 0)
	{
		int len = 0;
		//_printd("m_tls.Send@@@@");
		if (m_pSendBuf.Size() > (SEND_BUFFUER-100*1024))
		{
			//拆分包，防止发送buf满而卡住发不出包
			len = m_tls.Send((unsigned char*)m_pSendBuf.Buf(), (SEND_BUFFUER - 100 * 1024));
		}
		else
		{
			len = m_tls.Send((unsigned char*)m_pSendBuf.Buf(), m_pSendBuf.Size());			
		}
		if(len == m_pSendBuf.Size())
		{
			m_pSendBuf.Reset();
		}
		else if(len > 0)
		{
			uchar *pbuf = (uchar*)malloc(m_pSendBuf.Size() - len);
			if(NULL != pbuf)
			{
				memcpy(pbuf, m_pSendBuf.Buf()+len, m_pSendBuf.Size()- len);
				m_pSendBuf.Reset();
				m_pSendBuf.Append(pbuf, m_pSendBuf.Size()- len);
				free(pbuf);
			}
			else
			{
				m_pSendBuf.Reset();
			}
			return 0;
		}
		else if (-2 ==  len)
		{
			//m_isockstaus = SSL_FINISHED;
			return 0;
		}
		else
		{
			return 0;
		}
	}

	if(m_packet_count > 0)
	{
		void* pBuf = NULL;
		uint  len = 0;
		uint  sendlen = 0;
		int64_t intime = 0;
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pBuf = it->pBuf;
		len = it->size;
		intime = it->timenow;
		m_packet_list.pop_front();
		m_packet_count--;
		m_packet_size -= len;

		m_mutex_list.Leave();	
		//_printd("m_tls.Send@@@@");
		sendlen = m_tls.Send((unsigned char *)pBuf,len);
		if(len != sendlen)
		{			
			int aplen = (sendlen > 0) ? (len - sendlen): len;
			m_pSendBuf.Append((unsigned char*)pBuf + (len - aplen), aplen);
		}
		uint senddelay = getNowMs() - intime;
		m_imaintaintotaldelay += (uint64)senddelay;
		m_imaintaintotalsend += 1;
		if (senddelay > 1000)
		{
			_printd("================debug============%u", senddelay);
		}

		free(pBuf);
	}
}
VD_INT32   CHWComunication::Handle()
{
	if(m_connStatus == eConnected)
	{

		DoRecvSend(1000);
		if (m_ipongtimeout >= 2)
		{//心跳超时
			Close();
		}
		else if (m_imaintainsend)
		{//发送维护信息
			SendEeveryDayDeviceInfo();
			m_imaintainsend = FALSE;
			MaintainUpdateVar(1);
		}
	}
	else 
	{		
		_printd("connect !!!");
		if(Connect())
		{
			m_ifailcnt = 0;
			DoAuthReq();
		}
		else
		{	
			string port;
			//if(m_ifailcnt++ >= 10 && 1 == m_isdreupload)
			//{//SD卡重传，1分钟更新一次断网结束时间戳
			//	if (m_isdendtimestamp)
			//	{
			//		int64_t sdendtimestamp = getNowMs();
			//		_printd("old:%u  update:%u", (uint)(m_isdendtimestamp / (int64_t)1000),(uint)(sdendtimestamp / (int64_t)1000));
			//		g_HMRecordManager.ReUploadTime(0, 0, (uint)(m_isdendtimestamp / (int64_t)1000), 1, (uint)(sdendtimestamp / (int64_t)1000));
			//		m_isdendtimestamp = sdendtimestamp;
			//	}
			//	else
			//	{
			//		//第一次更新断网云录像时间戳
			//		m_isdendtimestamp = getNowMs();
			//		int64_t sdendtimestamp = getNowMs();
			//		_printd("m_isdendtimestamp:%u  ", (uint)(m_isdendtimestamp / (int64_t)1000));
			//		g_HMRecordManager.ReUploadTime(0,0,(uint)(m_isdendtimestamp/(int64_t)1000));

			//	}
			//	m_ifailcnt = 0;
			////	g_HeMuHttpsCom.SendOpenAPIReq(MSG_P_THD_GET_CAMERA_CHN_ADDR_REQ);
			//}
			g_HeMuHttpsCom.GetServerInfo(m_cserveraddr,port);			
			m_iport = atoi(port.c_str());
			sleep(6);
		}
	}
}
void   CHWComunication::ThreadProc()
{
	while (m_bLoop) 
	{
		if(m_iHWConect)
		{
			Handle();
			DoTimeout();
		}
		else
		{
			if (eConnected == m_connStatus)
			{
				Close();
			}
			sleep(1);
		}
	}
}


VD_INT32   CHWComunication::Connect()
{
	int fd = -1;
	char cipadd[16] = {0};
	string port;
	if(isValid())
    {
    	_printd("fd[%d]",m_fd);
		Close();
        return FALSE;
    }
	if(m_connStatus == eConnecting || m_connStatus == eConnected)
    {
    	_printd("fd[%d]",m_fd);
		Close();
        return FALSE;
    }
	
	g_HeMuHttpsCom.GetServerInfo(m_cserveraddr, port);
	m_iport = atoi(port.c_str());
	
	//DNS 解析IP 判断网络是否正常用作换HW服务器IP的凭证
	if (GetServerIpAddrFromDns(HEMU_OPENAPI, cipadd))
	{
		return FALSE;
	}

	m_connStatus = eConnecting;
	fd = m_tcpsock.createSocket();
	if(-1 != fd )
	{
#if 0
		m_tcpsock.setblock(fd,true);
#else
		m_tcpsock.setblock(fd,false);
		m_tcpsock.setSendBufferSize(SEND_BUFFUER);
#endif
		
		bool bConnected = m_tcpsock.connectServer(m_cserveraddr,m_iport);

		_printd("server:%s port:%d connect[%d]",m_cserveraddr.c_str(),m_iport, bConnected);
		if(bConnected)
        {	
        	m_fd = fd;
			//ssl
			return SetConnected();
        }	
		else if (m_ifailcnt++ >= 20)
		{
			//3分钟网络正常情况连接不上服务器则重新申请新HW ip
			string port;
			m_ifailcnt = 0;
			_printd("changge HW Ip");
			g_HeMuHttpsCom.GetServerInfo(m_cserveraddr, port,1);
		}

	}
	Close();
	return FALSE;
}


VD_INT32   CHWComunication::Reconnect()
{
	Close();
	Connect();
}
void CHWComunication::SetConect(int conectstatus)
{ 
	m_iHWConect = conectstatus; 
	if (0 == conectstatus)
	{
		TrackerRunSet(0);//断开移动跟踪
		m_tls.SetBreak();
	}
	_printd("m_iHWConect[%d]", m_iHWConect);
}

VD_INT32   CHWComunication::DoRecvSend(uint timeout/*ms*/)
{
	int ret;
	int writesetflag = 0;
	fd_set read_set, write_set;
    struct timeval tv = {0};
    
    FD_ZERO(&read_set);
    FD_ZERO(&write_set);
    FD_SET(m_fd, &read_set);
	if(m_pSendBuf.Size() > 0 || m_packet_count > 0)
	{
    	FD_SET(m_fd, &write_set);
		writesetflag = 1;
	}
    tv.tv_sec = timeout/1000;
    tv.tv_usec = (timeout%1000)*1000;
	m_isockstaus = 0;

    ret = select(m_fd + 1, &read_set, &write_set, NULL, &tv);
	
    if(0 < ret)
    {
		if (FD_ISSET(m_fd, &read_set)) 
        {
        	m_isockstaus |= SSL_SOCK_READABLE;
           
        }
        if (FD_ISSET(m_fd, &write_set))
        {
        	m_isockstaus |= SSL_SOCK_WRITABLE;
           
        }
		
		if (m_isockstaus & SSL_SOCK_READABLE) 
        {
           	DoRecvHandle();
        }
		if (m_isockstaus & SSL_SOCK_WRITABLE) 
		{			
			DoSendHandle();
		}
		if (m_isockstaus & SSL_FINISHED) 
		{
			Close();
		}
	}
	else
	{
		if (writesetflag && (m_isendout++ > 10))
		{
			_printd("send timeout 10s!!!");
			Close();
		}
	}
	return 0;
}
VD_INT32   CHWComunication::DoTimeout()
{
	if((m_connStatus == eConnected )
		&&(abs(int(getNowMs()/1000 - m_iconTimeoutTime)) > m_ipingspan))
    {
		HwPing();
		m_iconTimeoutTime = getNowMs()/1000;
		m_ipongtimeout++;
    }
}
int64_t CHWComunication::getNowMs()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * (int64_t)1000 + tv.tv_usec/1000;
}
VD_INT32   CHWComunication::SetConnected()
{
	
	if(m_tls.Connect(m_fd))
	{
		m_connStatus = eConnected;
		m_tcpsock.setKeepAlive();
		m_tcpsock.setTcpNoDelay();
		
		return TRUE;
	}
	else
	{
		Close();
		return FALSE;
	}
}
VD_INT32   CHWComunication::DoAuthReq()
{
//登录前，先清空前一次的队列
///clear buf/////
	//更新断网区间的时间戳
	int64_t sdstarttimestamp = m_isdstartimestamp;
	int64_t sdendtimestamp = m_isdendtimestamp;
	m_isdendtimestamp = getNowMs();
	m_imaintainlogintime = (uint)(getNowMs()/(uint64)1000);
	_printd("m_imaintainlogintime[%d]", m_imaintainlogintime);
	while (m_packet_count > 0)
	{
		void* pBuf = NULL;
		uint  len = 0;
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pBuf = it->pBuf;
		len = it->size;
		if (m_isdstartimestamp > it->timenow)
		{
			m_isdstartimestamp = it->timenow;
		}
		m_packet_list.pop_front();
		m_packet_count--;
		m_packet_size -= len;

		m_mutex_list.Leave();
		free(pBuf);
	}
	while (m_packet_cache_count > 0)
	{
		CPacket  *pPacket = NULL;
		PACKET_CACHE_LIST::iterator it = m_packet_cache_list.begin();
		pPacket = it->pPacket;
		pPacket->Release();
		m_packet_cache_list.pop_front();
		m_packet_cache_count--;
	}
	//更新断网云存储录像开始结束时间戳
	_printd("m_isdendtimestamp[%llu] m_isdstartimestamp[%llu]", m_isdendtimestamp, m_isdstartimestamp);
	if ((m_isdstartimestamp > 0 )&&
		((m_isdendtimestamp - m_isdstartimestamp) > 60000))
	//if (m_isdstartimestamp)
	{
		_printd("%u %u", (uint)(sdstarttimestamp / int64_t(1000)),(uint)(m_isdstartimestamp / int64_t(1000)));
		_printd("%u %u", (uint)(sdendtimestamp / int64_t(1000)),(uint)(m_isdendtimestamp / int64_t(1000)));
		//g_HMRecordManager.ReUploadTime(0, 1, (uint)(sdstarttimestamp / int64_t(1000)), 1, (uint)(m_isdstartimestamp / int64_t(1000)));
		//g_HMRecordManager.ReUploadTime(0, 0, (uint)(sdendtimestamp / int64_t(1000)), 1, (uint)(m_isdendtimestamp / int64_t(1000)),60);
		g_HMRecordManager.ReUploadTime(0, 1, (uint)(m_isdstartimestamp / int64_t(1000)));
		g_HMRecordManager.ReUploadTime(0, 0, (uint)(m_isdendtimestamp / int64_t(1000)+60));
		//g_HMRecordManager.SetConect(1);//在更新重传
	}
	if (m_isdreupload)
	{
		g_HMRecordManager.SetConect(1);//在更新重传
	}
	m_isdstartimestamp = 0;
	m_isdendtimestamp = 0;
	///end clear buf///
	int len = 0;
	unsigned char buf[1024] = {0};
	_printd("HW Auth !!!");
	HEDGWMessage msg;
	DevLoginReq  logreq;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevLoginReq;
	msg.dev_login_req = &logreq;
	dev_login_req__init(&logreq);
	g_HeMuHttpsCom.GetDevLogInfo(logreq);
	len = hedgwmessage__get_packed_size(&msg);

	if(len < (sizeof(buf) - 4))
	{
		hedgwmessage__pack(&msg,buf+4); 
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		unsigned char *pBuf = (unsigned char *)malloc(len + 4);
		if(NULL != pBuf)
		{
			memset(pBuf,0,len+4);
			hedgwmessage__pack(&msg,pBuf+4); 
			int datasize_nl = htonl(len);
			memcpy(pBuf,&datasize_nl,4);
			Send((const void*)pBuf, len+4,0);
			free(pBuf);
		}
		
	}
	return 0;
}
VD_INT32   CHWComunication::Close()
{
	_printd("Close");
	if (1 == m_idevicestatus && 1 == m_isdreupload)
	{
		m_isdstartimestamp = getNowMs();
		_printd("m_isdstartimestamp:%u", (uint)(m_isdstartimestamp / int64_t(1000)));
		//g_HMRecordManager.ReUploadTime(0, 1, (uint)(m_isdstartimestamp / int64_t(1000)));
		m_isdendtimestamp = 0;
		g_HMRecordManager.SetConect(0);//在更新重传
	}
	if (m_connStatus == eConnected)
	{
		m_imaintaindisconnectTimes++; //离线次数
		m_imaintainonlineDuration +=   (uint)(getNowMs() / 1000) - m_imaintainlogintime;
	}
	m_connStatus = eUnconnected;
	m_icontrolflag = FALSE;
	m_ipongtimeout = 0;
	m_idevicestatus = 0;
	m_ieventcount = 0;
	m_isendout = 0;
	m_fd = -1;
	m_tcpsock.Close();
	m_tls.Close();
	
	m_pSendBuf.Reset();
	m_pRecvBuf.Reset();
	return 0;
}

VD_INT32   CHWComunication::Send(const void* buf, uint32_t len, uint32_t flag)
{
	if(m_connStatus != eConnected) return 0;
	
//	if(m_pSendBuf.Size() > 0 || m_packet_count > 0)
//	{
		unsigned char *pbuf = NULL;
		pbuf  = (unsigned char*)malloc(len);
		if(pbuf)
		{
			memset(pbuf, 0, len);
			memcpy(pbuf,buf,len);
			PACKET_CAPTURE packet_capture;
			packet_capture.pBuf = pbuf;
			packet_capture.size = len;
			packet_capture.timenow = getNowMs();
			m_mutex_list.Enter();
			m_packet_list.push_back(packet_capture);
			m_packet_count++;
			m_packet_size += packet_capture.size;
			m_mutex_list.Leave();
		}
		return 0;
 //	}
 //	return m_tls.Send((unsigned char*)buf,len);
}
VD_INT32   CHWComunication::Readv(const struct iovec* vecs, int32_t vcnt)
{
	
}
VD_INT32 CHWComunication::recv(void* buf, uint32_t len, uint32_t flag)
{
	
}

VD_INT32   CHWComunication::HwPing()
{
	int len = 0;
	unsigned char buf[1024] = {0};
	_printd("Ping !!!_m_hwconnect[%d]recvbufsize[%d]",m_iHWConect,m_pRecvBuf.Size());
	HEDGWMessage msg;
	Ping  pingreq;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__PING;
	msg.ping= &pingreq;
	ping__init(&pingreq);
	pingreq.id = 1;
	len = hedgwmessage__get_packed_size(&msg);

	hedgwmessage__pack(&msg,buf+4); 
	int datasize_nl = htonl(len);
	memcpy(buf,&datasize_nl,4);
	Send((const void*)buf,len+4,0);
	return 0;
}

void   CHWComunication::OnMediaData(int pagetype, int sync, unsigned char* data,unsigned int size,unsigned int timestamp)
{
	if (m_iupgrade)
	{
		//正在升级
		return;
	}
	m_imaintaintotalsize += size;
	if (1 == pagetype)
	{
		m_imaintaintotalAudioFrames++;
	}
	else if (2 == pagetype)
	{
		m_imaintaintotalVideoFrames++;

	}
	if(!m_icontrolflag && 0 == m_idevicestatus ) return ;

	//m_mutex_cache_list.Enter();
	if(2 == m_idevicestatus)
	{
		if(!m_icontrolflag && 0 == m_ieventcount)
		{
			//加入缓存队列
			AddCacheList(pagetype, sync,data,size,timestamp);
			return ;
		}
		else
		{	
			while(m_packet_cache_count > 0)
			{
				CPacket  *pPacket = NULL;
				PACKET_CACHE_LIST::iterator it = m_packet_cache_list.begin();
				pPacket = it->pPacket;
				if(m_ieventcount)
				{//有事件则加入发送队列
					PackMediaData(it->pagetype, it->sync, (unsigned char *) pPacket->GetBuffer(),pPacket->GetLength(), it->timestamp,it->frametime);
				}
				pPacket->Release();
				m_packet_cache_list.pop_front();
				m_packet_cache_count--;
			}
		}
	}
	//m_mutex_cache_list.Leave();
	PackMediaData(pagetype, sync,data, size,timestamp,getNowMs());
	
}
void   CHWComunication::AddCacheList(int pagetype, int sync, unsigned char* data,unsigned int size,unsigned int timestamp)
{
	while(pagetype == 3 && m_packet_cache_count > 0)
	{
		CPacket  *pPacket = NULL;
		PACKET_CACHE_LIST::iterator it = m_packet_cache_list.begin();
		pPacket = it->pPacket;
		pPacket->Release();
		m_packet_cache_list.pop_front();
		m_packet_cache_count--;
	}

	CPacket *pPack = g_PacketManager.GetPacket(size);
	if(pPack)
	{
		pPack->PutBuffer((void *)data, size);
		PACKET_CAPTURE_CACHE packet_cache;
		packet_cache.pagetype = pagetype;
		packet_cache.sync   = sync;
		packet_cache.timestamp = timestamp;
		packet_cache.frametime = getNowMs();
		packet_cache.pPacket = pPack;
		m_packet_cache_list.push_back(packet_cache);
		m_packet_cache_count++;		
	}
}
void CHWComunication::PackMediaData(int pagetype, int sync, unsigned char* data,unsigned int size,unsigned int timestamp,int64_t frametime)
{

	int len = 0;
	unsigned char *pbuf = NULL;
	HEDGWMessage msg;
	TDMediaPackage  packreq;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__TDMediaPackage;
	msg.td_media_package = &packreq;
	tdmedia_package__init(&packreq);
	packreq.package_type = pagetype;
	packreq.sync = sync;
	packreq.data.len = size; 
	packreq.data.data = data; 
	packreq.data_size = size;
	packreq.start_time = timestamp;
	if(2 == m_idevicestatus && m_ieventcount > 0)
	{
		//事件录制需要增加控制变量
		packreq.has_control_flag = 1;
		packreq.control_flag = 1;
	}

	if(1 == pagetype)
	{
		packreq.seq_num = m_iaudioSeq++;
	}
	else if(2 == pagetype)
	{
		packreq.seq_num = m_ivideoSeq++;
		
	}
	else
	{
		m_iFirstIFrame = TRUE;
		packreq.seq_num = m_ivideoSeq++;
		_printd("send I Frame!!!");		
	}
	if(!m_iFirstIFrame){return ;}
	packreq.has_ipcam_time = 1;
	packreq.ipcam_time = frametime;//getNowMs();
	len = hedgwmessage__get_packed_size(&msg);
	pbuf  = (unsigned char*)malloc(len+4);
	if(pbuf)
	{
		hedgwmessage__pack(&msg, pbuf+4); 
		int datasize_nl = htonl(len);
		memcpy(pbuf, &datasize_nl, 4);

		PACKET_CAPTURE packet_capture;
		packet_capture.pBuf = pbuf;
		packet_capture.size = len + 4;
		packet_capture.timenow = packreq.ipcam_time;
		if (m_packet_size  > m_packet_max_size/*5*1024*1024*/)
		{	
			free(pbuf);
			m_dropFramFlag = TRUE;
			if (1 == pagetype)
			{
				m_imaintaindropAudioFrames++;
			}
			else if (2 == pagetype)
			{
				m_imaintaindropVideoFrames++;
			}
			debugf("CHWComunication capture list is too large discard it, size:%d m_iHWconect[%d] m_idevciestatus[%d]\n", m_packet_size,m_iHWConect,m_idevicestatus);
			return ;
		}
		if(m_dropFramFlag && pagetype != 3)
		{
			//丢帧后等I帧
			free(pbuf);	
			return ;
		}
		m_dropFramFlag = FALSE;
		m_mutex_list.Enter();
		m_packet_list.push_back(packet_capture);
		m_packet_count++;
		m_packet_size += packet_capture.size;
		m_mutex_list.Leave();
	}
	else
	{
		_printd("malloc error!!!");
	}

}

void CHWComunication::onConfigSchedule(CConfigScheduleWorksheet& cCfgSchedule, int& ret)
{
	
	CONFIG_WORKSHEET &configOld = m_configScheduleWorksheet.getConfig();
	CONFIG_WORKSHEET &configNew = cCfgSchedule.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}

	configOld = configNew;
}
void CHWComunication::onConfigAlarm(CConfigHemuAlarmWorksheet& cCfgalarm, int& ret)
{
	CONFIG_WORKSHEET &configOld = m_configHemuAlarmWorksheet.getConfig();
	CONFIG_WORKSHEET &configNew = cCfgalarm.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}

	configOld = configNew;
}
void CHWComunication::CheckSection(int arg)
{
	int iRet = 0;
	if(arg)
	{	
		CConfigAlarmSwitch	cCfgAlarm;	
		cCfgAlarm.update();
		CONFIG_ALARMSWITCH &config	= cCfgAlarm.getConfig();
		iRet = CAppEventManager::instance()->checkTimeSection(&CConfigHemuAlarmWorksheet::getLatest()) == TRUE ? 0 : 1;
		//推送时间
		if((FALSE == config.bTimeSectionEnable) && iRet)
		{
			config.bTimeSectionEnable = TRUE;
			cCfgAlarm.commit();
		}
		//推送关闭时间
		if((TRUE == config.bTimeSectionEnable) && (0 == iRet))
		{
			config.bTimeSectionEnable = FALSE;
			cCfgAlarm.commit();
		}	
	}
	else
	{
		CConfigGeneral cCfgGeneral;
		cCfgGeneral.update();
		CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
		if (config.iOnOffMode)
		{
			return;
		}

		iRet = CAppEventManager::instance()->checkTimeSection(&CConfigScheduleWorksheet::getLatest()) == TRUE ? 0 : 1;
		_printd("iRet[%d]",iRet);
		//打开时间
		if(config.iSuspendMode && iRet)
		{
			config.iSuspendMode = 0;
			#ifdef IPC_JZ_NEW
			if(ChipT10 == PlatformGetHandle()->Chip){
				int nWidth  = 0;
				int nHeight = 0;
				CaptureGetCurHeightWidth(&nWidth, &nHeight);
				if(nWidth == 640 && nHeight == 360){
					_printd("T10 640X360 ForceIFrame...\n");
					cCfgGeneral.commit();
					CaptureForceIFrame(0,1);
					return;    //双码流   强制次码流
				}
			}
			#endif
			
			CaptureForceIFrame(0,0);
			cCfgGeneral.commit();
			//更新在线配置
			g_HeMuHttpsCom.SendOpenAPIReq(MSG_P_THD_SET_CAMERA_SETTINGS_REQ);
		}
		//睡眠时间
		if(0 == config.iSuspendMode && 0 == iRet )
		{			
			config.iSuspendMode = 1;
			cCfgGeneral.commit();
			g_HeMuHttpsCom.SendOpenAPIReq(MSG_P_THD_SET_CAMERA_SETTINGS_REQ);
		}
		
	}
}
void CHWComunication::GetWifiList(int msgSession)
{
	int i = 0;
	FILE *fp = NULL;
	char buf[1024] = {0};
	char tmpSignalLevel[8] = {0};
	unsigned int fwstate = 0;
	char moduleString[32] = {0};
	CConfigTable  objson;
	string msgout;
	char wifiname[48] = {0};
			
	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 1;
	objson["msgSession"]  = msgSession;
	objson["msgContent"]["responseRequest"] = 1792;
	objson["msgContent"]["responseSubRequest"] = 16;
	objson["msgContent"]["response"] = 0;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);
	switch (PlatformGetHandle()->WifiModule)
	{
		case RTL8188FU:
			snprintf(moduleString,sizeof(moduleString),"%s", "rtl8188fu");
			break;
		case RTL8189FS:
			snprintf(moduleString,sizeof(moduleString),"%s", "rtl8189fs");
			break;
		case RTL8821CS:
			snprintf(moduleString,sizeof(moduleString),"%s","rtl8821cs");
			break;	
		default: 
			_printd("the wifi module not's realtek, or non existent! ");
			return ;
	}
	sprintf(buf, "/proc/net/%s/wlan0/survey_info", moduleString);

	fp = fopen(buf, "w");
	if (fp == NULL) {
		printf("Fail to fopen %s\n", buf);
		return ;
	}
	fprintf(fp, "%c", '1');
	fclose(fp);
	_printd("3");
	sprintf(buf, "/proc/net/%s/wlan0/fwstate", moduleString);
	for (i = 0; i< 100; i++)
	{
		fp = fopen(buf, "r");
		if (fp == NULL) {
			printf("Fail to fopen %s\n", buf);
			return ;
		}
		fscanf(fp, "fwstate=%x", &fwstate);
		fclose(fp);
		usleep(100 * 1000);
		if ((fwstate & 0x0800) == 0) {
			break;
		}
	}
	_printd("4");
	sprintf(buf, "/proc/net/%s/wlan0/survey_info", moduleString);
	fp = fopen(buf, "r");
	if (fp == NULL) {
		printf("Fail to fopen %s\n", buf);
		return;
	}
	_printd("5");
	i = 0;
	memset(buf, 0, sizeof(buf));
	GetConnWifi(wifiname, sizeof(wifiname));
	while (fgets(buf, sizeof(buf), fp) != NULL)
	{	
		if(i != 0)
		{
			char buffer[4][128] ={0};
			sscanf(buf,"%*s %s %*s %*s %s %*s %*s %s %s",buffer[0],buffer[1],buffer[2],buffer[3]);
			if(0 == strncmp(wifiname,buffer[3],strlen(wifiname)))
			{
				objson["msgContent"]["responseParams"][i-1]["bConnected"]= 1;
			}
			else
			{
				objson["msgContent"]["responseParams"][i-1]["bConnected"]= 0;
			}
			objson["msgContent"]["responseParams"][i-1]["bssid"] = buffer[0];
			objson["msgContent"]["responseParams"][i-1]["encryption"] = buffer[2];
			objson["msgContent"]["responseParams"][i-1]["signal"] = (120 - abs(atoi(buffer[1])))/20; //1~5
			objson["msgContent"]["responseParams"][i-1]["ssid"] = buffer[3];
		}
		if (i < 30) {
			i++;
		} else {
			break;
		}
		memset(buf, 0, sizeof(buf));
	}
	fclose(fp);	
	objson.toStyledString(msgout);
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
//	resp.src_device_id = (char*)"119000310000101";
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return ; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}
	return ;
}
void CHWComunication::GetPtzPosition(int msgSession)
{
	CConfigTable  objson;
	string msgout;
	char buf[1024] = {0};
	int x,y;
	
	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 1;
	objson["msgSession"]  = msgSession;
	objson["msgContent"]["responseRequest"] = 1792;
	objson["msgContent"]["responseSubRequest"] = 81;
	objson["msgContent"]["response"] = 0;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);
	GetMotorPresetPos(&x,&y);
	objson["msgContent"]["responseParams"]["pan"] = x;
	objson["msgContent"]["responseParams"]["tilt"] = y;
	
	objson.toStyledString(msgout);
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return ; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}
}
void CHWComunication::GetSdInfo(int msgSession)
{
	int64_t Remainmem 		= 0;
	int64_t Totalmem 		= 0;
	int ret 			= -1;
	int i = 0;
	CConfigTable  objson;
	string msgout;
	char buf[1024] = {0};
	
	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 1;
	objson["msgSession"]  = msgSession;
	objson["msgContent"]["responseRequest"] = 1792;
	objson["msgContent"]["responseSubRequest"] = 85;
	objson["msgContent"]["response"] = 0;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);

	if(access("/dev/mmcblk0",F_OK) == 0 )
	{
		ret = file_sys_get_cap_bit(&Totalmem,&Remainmem);
		if(ret != -1)
		{
			if(access("/dev/mmcblk0p1",F_OK) == 0)
			{
				objson["msgContent"]["responseParams"][i]["name"] = "/dev/mmcblk0p1";
			}
			else
			{
				objson["msgContent"]["responseParams"][i]["name"] = "/dev/mmcblk0";
			}
			objson["msgContent"]["responseParams"][i]["fs"] = "fat32";
			char uint64str[25];
			memset(uint64str, 0, sizeof(uint64str));
			snprintf(uint64str, sizeof(uint64str), "%llu", Totalmem);
			objson["msgContent"]["responseParams"][i]["total"]  = uint64str;
			memset(uint64str, 0, sizeof(uint64str));
			snprintf(uint64str, sizeof(uint64str), "%llu", Remainmem);
			objson["msgContent"]["responseParams"][i]["remain"] = uint64str;
		}
		else
		{
			objson["msgContent"]["responseParams"][i]["name"] = "/dev/mmcblk0p1";
			objson["msgContent"]["responseParams"][i]["fs"] = "vfat";
			objson["msgContent"]["responseParams"][i]["total"]  = -1;
			objson["msgContent"]["responseParams"][i]["remain"] = -1;
		}
	}
	else
	{
		objson["msgContent"]["responseParams"] ="";
	}
	objson.toStyledString(msgout);
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return ; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}
}
int CHWComunication::Upgrade(CConfigTable &jvaule)
{
	string url = jvaule["msgContent"]["requestParams"]["url"].asString();
	string checkSum = jvaule["msgContent"]["requestParams"]["checkSum"].asString();
	int   msgSession = jvaule["msgSession"].asInt();
	string version = jvaule["msgContent"]["requestParams"]["newServiceVersion"].asString();
	_printd("url:%s", url.c_str());
	_printd("checkSum:%s", checkSum.c_str());
	_printd("version:%s", version.c_str());
	g_HeMuHttpsCom.SetUpgradeInfo(url,checkSum,version,msgSession);
	return 0;
}

int CHWComunication::PushUgradeStatus(string version,int status,int msgSession,char *comment)
{
	char buf[1024] = {0};
	CConfigTable  objson;
	string msgout;

	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 0;
	objson["msgSession"]  = msgSession;
	objson["msgContent"]["request"] = 2049;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);
	objson["msgContent"]["requestParams"]["firmwareversion"] = version.c_str();
	objson["msgContent"]["requestParams"]["cameraappversion"]  = version.c_str();
	objson["msgContent"]["requestParams"]["status"] = status;
	objson["msgContent"]["requestParams"]["currentSize"] = 100;
	objson["msgContent"]["requestParams"]["totalSize"]  = 100;
	if(comment)
	{
		objson["msgContent"]["requestParams"]["comment"] = comment;
	}
	else
	{
		objson["msgContent"]["requestParams"]["comment"] = "comment";
	}
	objson.toStyledString(msgout);
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 1;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return 0; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}
	return 0;
}
int CHWComunication::SdcardFormat(int msgSession)
{
	_printd("sdformat!!!");
	m_Cmd.AddCmdData(2,msgSession);
	return 0;
}
int CHWComunication::SdcardFormatResp(int result, int msgSession)
{
	char buf[1024] = {0};
	CConfigTable  objson;
	string msgout;

	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 1;
	objson["msgSession"]  = msgSession;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);
	objson["msgContent"]["responseRequest"] = 1793;
	objson["msgContent"]["responseSubRequest"] = 87;
	objson["msgContent"]["response"] = result;
	objson.toStyledString(msgout);
	
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return 0; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}	
	return 0;
}
static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
	int len = size * nmemb;
	int written = len;
	FILE *fp = NULL;
	if (access((char*)stream, 0) == -1) {
		fp = fopen((char*)stream, "wb");
	}
	else {
		fp = fopen((char*)stream, "ab");
	}
	if (fp)
	{
		fwrite(ptr, size, nmemb, fp);
		fclose(fp);
	}
	printf("download size[%d]\n", size);
	return written;
}
int CHWComunication::PushEvent(int type,int status,uint timestamp)
{	
	if (m_connStatus != eConnected && m_isdreupload)
	{
		uint curtimestamp = (uint)(getNowMs() / (int64_t)1000);
		if (status)
		{
			//开始，预录4秒
			g_HMRecordManager.ReUploadTime(type, status, curtimestamp - 4);
		}
		else
		{	
			g_HMRecordManager.ReUploadTime(type, status, curtimestamp);
		}
		return 0;
	}

	//{
	//	struct tm *p;
	//	char testdata[256] = {0};
	//	time_t timep = timestamp  + 8 * 60 * 60;
	//	p = gmtime(&timep);
	//	snprintf(testdata, 128, "%04d-%02d-%02d %02d:%02d:%02d type[%d] status[%d]\n",
	//		p->tm_year + 1900, p->tm_mon + 1, p->tm_mday, p->tm_hour,
	//		p->tm_min, p->tm_sec,type,status);
	//	writefile_callback(testdata, strlen(testdata), 1, (void*)"/tmp/test.data");

	//}
	char buf[1024] = {0};
	HEDGWMessage msg;
	DevEventCmd  req;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevEventCmd;
	msg.dev_event_cmd = &req;
	dev_event_cmd__init(&req);

	req.type = type;
	req.status = status;
	req.ipcam_time = getNowMs();
	//m_mutex_cache_list.Enter();
	if(status)
	{
		m_ieventcount++;
	}
	else
	{
		if (m_ieventcount)
		m_ieventcount--;
	}
	//m_mutex_cache_list.Leave();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("len [%d]",len);
	_printd("Push event type[%d]!!![%d]",type,status);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return -1; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}	
	return 0;
}


int  CHWComunication::ChangeConectWifi(int msgSession,char* Ssid,char* Passwd)
{
	IPC_HEADER header = {0};
	if(FALSE == CheckSsid(Ssid,strlen(Ssid)))
	return 1;
	_printd("ssid:%s passwd:%s",Ssid,Passwd);
	rename(STORE_WIFI_CONFIG, STORE_WIFI_CONFIG_BAK);
	Store_Cfgfile(STORE_WIFI_CONFIG,Ssid,Passwd);
	header.ipc_cmd = WIFI_RECONECT;
	g_DaemonManager.SendCmdToDaemon((void*)&header,sizeof(IPC_HEADER));
	_printd("close");
	sleep(2);//等daemon重连wifi
	m_isockstaus = SSL_FINISHED;
	//////////////////////////////////
#if 0
	char buf[1024] = {0};
	CConfigTable  objson;
	string msgout;

	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 1;
	objson["msgSession"]  = msgSession;
	objson["msgTimeStamp"] = (uint)(getNowMs()/(int64_t)1000);
	objson["msgContent"]["responseRequest"] = 1793;
	objson["msgContent"]["responseSubRequest"] = 17;
	objson["msgContent"]["response"] = 0;

	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg); 
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]",msgout.c_str(),len);
	if(len < (sizeof(buf)-4))
	{
		hedgwmessage__pack(&msg,(uchar*)(buf+4)); 	
		int datasize_nl = htonl(len);
		memcpy(buf,&datasize_nl,4);
		Send((const void*)buf,len+4,0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len+4);

		if(NULL == pbuf)return 0; 
		memset(pbuf,0,len+4);
		hedgwmessage__pack(&msg,pbuf+4); 	
		int datasize_nl = htonl(len);
		memcpy(pbuf,&datasize_nl,4);
		Send((const void*)pbuf,len+4,0);
		free(pbuf);
	}
#endif
	return 0;
}

void CHWComunication::SendEeveryDayDeviceInfo()
{
	char buf[1024] = { 0 };
	CConfigTable  objson;
	string msgout;
	m_imaintainonlineDuration += (uint)(getNowMs() / (uint64)1000) - m_imaintainlogintime;
	m_imaintainlogintime = (uint)(getNowMs() / 1000);
	
	snprintf(buf, sizeof(buf), "%llu", m_imaintainstartimestamp);
	objson["statistics"]["startTime"] = buf;
	snprintf(buf, sizeof(buf), "%llu", getNowMs());
	objson["statistics"]["endTime"] = buf;
	objson["statistics"]["totalSize"] = uint(m_imaintaintotalsize/(1024*1024));
	objson["statistics"]["totalVideoFrames"] = m_imaintaintotalVideoFrames;
	objson["statistics"]["dropVideoFrames"] = m_imaintaindropVideoFrames;
	objson["statistics"]["totalAudioFrames"] = m_imaintaintotalAudioFrames;
	objson["statistics"]["dropAudioFrames"] = m_imaintaindropAudioFrames;
	objson["statistics"]["onlineDuration"] = m_imaintainonlineDuration;
	objson["statistics"]["disconnectTimes"] = m_imaintaindisconnectTimes;

	objson["statistics"]["rebootTimes"] = m_imaintainrebootTimes;
	objson["statistics"]["delay"] = (uint)(m_imaintaintotaldelay/(uint64)m_imaintaintotalsend);
	objson["statistics"]["openApiFailureTimes"] = 0;
	objson["statistics"]["assignHedgwIpTimes"] = 2;
	objson["statistics"]["selfDefine"] = "";//todo uploadTIME ;
	objson["msgCategory"] = "camera";
	objson["msgSequence"] = 0;
	objson["msgSession"] = 1377903621;
	objson["msgTimeStamp"] = (uint)(getNowMs() / (int64_t)1000);
	objson.toStyledString(msgout);

	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 2;
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]", msgout.c_str(), len);
	if (len < (sizeof(buf) - 4))
	{
		hedgwmessage__pack(&msg, (uchar*)(buf + 4));
		int datasize_nl = htonl(len);
		memcpy(buf, &datasize_nl, 4);
		Send((const void*)buf, len + 4, 0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len + 4);

		if (NULL == pbuf)return ;
		memset(pbuf, 0, len + 4);
		hedgwmessage__pack(&msg, pbuf + 4);
		int datasize_nl = htonl(len);
		memcpy(pbuf, &datasize_nl, 4);
		Send((const void*)pbuf, len + 4, 0);
		free(pbuf);
	}
	 
}

void CHWComunication::ModResolutionNotice( int with, int height)
{
	char buf[64] = { 0 };
	string msgout;
	_printd("with[%d] height[%d]",with,height);
	snprintf(buf, sizeof(buf), "%dx%d", with, height);

	msgout = buf;
	HEDGWMessage msg;
	DevIpCamCmd  resp;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevIpCamCmd;
	msg.dev_ipcam_cmd = &resp;
	dev_ip_cam_cmd__init(&resp);
	resp.type = 3;
	resp.src_device_id = (char*)m_csrcdeviceid.c_str();
	resp.dst_device_id = (char*)m_cdstdeviceid.c_str();
	resp.msg = (char*)msgout.c_str();
	int len = hedgwmessage__get_packed_size(&msg);
	_printd("msg:%s len [%d]", msgout.c_str(), len);
	if (len < (sizeof(buf) - 4))
	{
		hedgwmessage__pack(&msg, (uchar*)(buf + 4));
		int datasize_nl = htonl(len);
		memcpy(buf, &datasize_nl, 4);
		Send((const void*)buf, len + 4, 0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len + 4);

		if (NULL == pbuf)return;
		memset(pbuf, 0, len + 4);
		hedgwmessage__pack(&msg, pbuf + 4);
		int datasize_nl = htonl(len);
		memcpy(pbuf, &datasize_nl, 4);
		Send((const void*)pbuf, len + 4, 0);
		free(pbuf);
	}

}
VD_BOOL CHWComunication::readConfig(const char* chPath, std::string& input)
{
	_printd("file:%s",chPath);
	m_fileConfig = fopen(chPath, "rb");
	if (!m_fileConfig)
		return FALSE;

	const int size = 32 * 1024;
	char* buf = new char[size + 1];
	int nLen = 0;

	memset(buf, '\0', size);
	input = "";

	while (1)
	{
		nLen = fread(buf, 1, size,m_fileConfig);

		if (nLen <= 0)	break;

		buf[nLen] = 0;
		input += buf;
	}
	input += '\0';
	fclose(m_fileConfig);
	//input = buf;
	delete[]buf;

	return TRUE;
}
void CHWComunication::InitMaintainConfig()
{

	CConfigReader reader;
	VD_BOOL bToRead2ndFile = TRUE;
	m_maintainTable.clear();

	if (access(maintainFile, 0))
	{
		_printd("maintian file not available !!!");
		return;
	}

	if (readConfig(maintainFile, m_stream))
	{
		bToRead2ndFile = reader.parse(m_stream, m_maintainTable);
	}
	else
	{
		_printd("readConfig file1 failed");
	}

	if (bToRead2ndFile)
	{
		MaintainJsontoVar();
		//加载 重启加1
		m_imaintainrebootTimes = m_maintainTable["rebootTimes"].asInt();
		m_imaintainrebootTimes = m_imaintainrebootTimes + 1;
		m_changed = 1;
		saveFile();
		_printd("m_stream:%s", m_stream.c_str());
	}
	else
	{
		_printd("maintain info error!!!\n");
	}
	
}
void CHWComunication::saveFile()
{
	static CMutex fileMutex;
	CGuard l_cGuard(fileMutex);
	CConfigWriter writer(m_stream);

	//if (!m_changed)
	//{
	//	return;
	//}
	if (access("/mnt/temp", 0))
	{
		if (mkdir("/mnt/temp", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
		{
			return;
		}
	}
	m_changed = FALSE;
	MaintainVartoJson();
	m_stream = "";
	writer.write(m_maintainTable);



	m_fileConfig = fopen(maintainFile, "wb");
	if (m_fileConfig)
	{
		if ((int)m_stream.size() != fwrite((char*)m_stream.c_str(), 1, m_stream.size(), m_fileConfig))
		{
			_printd("write config file failed!");
		}
		fflush(m_fileConfig);
		fclose(m_fileConfig);
	}
}

void CHWComunication::MaintainVartoJson()
{
	m_maintainTable.clear();
	char buf[128] = {0};
	snprintf(buf, sizeof(buf), "%llu", m_imaintainstartimestamp);
	m_maintainTable["startTime"] = buf;
	snprintf(buf, sizeof(buf), "%llu", m_imaintainendtimestamp);
	m_maintainTable["endTime"] = buf;
	snprintf(buf, sizeof(buf), "%llu", m_imaintaintotaldelay);
	m_maintainTable["totaldelay"] = buf;
	m_maintainTable["totalSize"] = (uint)(m_imaintaintotalsize/(1024*1024));
	m_maintainTable["totalVideoFrames"] = m_imaintaintotalVideoFrames;
	m_maintainTable["dropVideoFrames"] = m_imaintaindropVideoFrames;
	m_maintainTable["totalAudioFrames"] = m_imaintaintotalAudioFrames;
	m_maintainTable["dropAudioFrames"] = m_imaintaindropAudioFrames;
	m_maintainTable["onlineDuration"] = m_imaintainonlineDuration;
	m_maintainTable["disconnectTimes"] = m_imaintaindisconnectTimes;
	m_maintainTable["rebootTimes"] = m_imaintainrebootTimes;
	m_maintainTable["totalsend"] = m_imaintaintotalsend;

	m_maintainTable["openApiFailureTimes"] = m_iopenApiFailureTimes;
	m_maintainTable["assignHedgwIpTimes"] = m_iassignHedgwIpTimes;

	snprintf(buf, sizeof(buf), "%04d-%02d-%02d", m_maintainDate.year, m_maintainDate.month, m_maintainDate.day);
	m_maintainTable["date"] = buf;
//	m_maintainTable["selfDefine"] = "";
}

void CHWComunication::MaintainJsontoVar()
{
	if (m_maintainTable["startTime"].asCString())
	{
		sscanf(m_maintainTable["startTime"].asCString(), "%llu", &m_imaintainstartimestamp);
	}
	else
	{
		m_imaintainstartimestamp = 0;
	}
	if (m_maintainTable["endTime"].asCString())
	{
		sscanf(m_maintainTable["endTime"].asCString(), "%llu", &m_imaintainendtimestamp);
	}
	else
	{
		m_imaintainendtimestamp = 0;
	}
	if (m_maintainTable["totaldelay"].asCString())
	{ 
		sscanf(m_maintainTable["totaldelay"].asCString(), "%llu", &m_imaintaintotaldelay);
	}
	else
	{
		m_imaintaintotaldelay = 0;
	}
	if (m_maintainTable["date"].asCString())
	{
		sscanf(m_maintainTable["date"].asCString(), "%04d-%02d-%02d", &m_maintainDate.year,
			&m_maintainDate.month, &m_maintainDate.day);
	}
	else
	{
		SystemGetCurrentTime(&m_maintainDate);
	}
	m_imaintaintotalsize = (uint64)(m_maintainTable["totalSize"].asUInt()) *(uint64)(1024*1024);
	m_imaintaintotalVideoFrames = m_maintainTable["totalVideoFrames"].asUInt();
	m_imaintaindropVideoFrames = m_maintainTable["dropVideoFrames"].asUInt();
	m_imaintaintotalsize = m_maintainTable["totalAudioFrames"].asUInt();
	m_imaintaindropAudioFrames = m_maintainTable["dropAudioFrames"].asUInt();
	m_imaintainonlineDuration = m_maintainTable["onlineDuration"].asUInt();
	m_imaintaindisconnectTimes = m_maintainTable["disconnectTimes"].asUInt();
	m_imaintainrebootTimes = m_maintainTable["rebootTimes"].asUInt();
	m_imaintaintotalsend = m_maintainTable["totalsend"].asUInt();

	m_iopenApiFailureTimes = m_maintainTable["openApiFailureTimes"].asUInt();
	m_iassignHedgwIpTimes = m_maintainTable["assignHedgwIpTimes"].asUInt();

}
void CHWComunication::MaintainUpdateVar(int updateall)
{
	if ((m_connStatus == eConnected )&& (0 == m_imaintainstartimestamp))
	{
		m_imaintainstartimestamp = getNowMs();
		SystemGetCurrentTime(&m_maintainDate);
		//MaintainJsontoVar();
		saveFile();
	}

	if (updateall)
	{
		m_imaintainstartimestamp = 0;  
		m_imaintainendtimestamp = 0;
		m_imaintaintotalsize = 0;
		m_imaintaintotalVideoFrames = 0;
		m_imaintaindropVideoFrames = 0;
		m_imaintaintotalAudioFrames = 0;
		m_imaintaindropAudioFrames = 0;
		m_imaintainonlineDuration = 0;
		m_imaintainlogintime = (uint)(getNowMs()/(uint64)1000);
		m_imaintaindisconnectTimes = 0;
		m_imaintainrebootTimes = 0;
		m_imaintaindelay = 0;
		m_imaintaintotaldelay = 0;
		m_imaintaintotalsend = 0;
		m_iopenApiFailureTimes = 0;
		m_iassignHedgwIpTimes = 0;
		SystemGetCurrentTime(&m_maintainDate);
	}

}