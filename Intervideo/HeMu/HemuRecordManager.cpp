#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include "System/File.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#include "Intervideo/HeMu/HemuRecordManager.h"
#include "Intervideo/HeMu/HemuProbuf.pb-c.h"	
#include "APIs/CommonBSP.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Functions/LedStat.h"
#include "System/AppEvent.h"
#include "Functions/WifiLink.h"
#include "Functions/DaemonManager.h"
#include "Functions/DriverManager.h"

char listFileFirst[240] = "/media/tagRecord/Record1";
char listFileSecond[240] = "/media/tagRecord/Record2";
#ifdef WIN32
typedef Json::StyledWriter CConfigWriter;
#else
typedef Json::FastWriter CConfigWriter;
#endif
#define  SEND_BUFFUER (100*1024)


PATTERN_SINGLETON_IMPLEMENT(CHemuRecordManager);

CHemuRecordManager::CHemuRecordManager()
	:CThread("CHemuRecordManager", TP_NET)
	, m_iNtpSuccess(0)
	, m_fd(-1)
	, m_connStatus(eUnconnected)
	, m_iconTimeoutTime(0)
	, m_iauthState(AUTH_INIT)
	, m_ifailcnt(0)
	, m_idevice_status(0)
	, m_ipingspan(15)	
	, m_iOpenFile(FALSE)
	, m_event_count(0)
	, m_iSdStatus(0)
	, m_ireload(0)
	, m_iIFrameFlag(FALSE)
	, m_iSeekTimeStamp(0)
{
	m_pBuf = NULL;
	m_pSendBuf.clear();
	m_pRecvBuf.Reset();
}

CHemuRecordManager::~CHemuRecordManager()
{
	Close();
	_printd("~  CHWComunication");
}




VD_INT32   CHemuRecordManager::Start()
{

	CreateThread();
	m_tls.Initctx();
	m_cTimer.Start(this, (VD_TIMERPROC)&CHemuRecordManager::onTimer, 5000, 5000);

	_printd("  CHemuRecordManager Init Over");
	return 0;
}

void CHemuRecordManager::InitUploadList()
{

	CConfigReader reader;
	VD_BOOL bToRead2ndFile = TRUE;
	m_listAll.clear();

	if (access(listFileFirst, 0))
	{
		
		return ;
	}
	if (readConfig(listFileFirst, m_stream))
	{
		bToRead2ndFile = !reader.parse(m_stream, m_listAll);
	}
	else
	{
		_printd("readConfig file1 failed");
	}

	if (bToRead2ndFile)
	{
		if (access(listFileFirst, 0))
		{
			m_listAll.clear();
			return;
		}

		if (readConfig(listFileSecond, m_stream))
		{
			bToRead2ndFile = !reader.parse(m_stream, m_listAll);
			if (bToRead2ndFile)
			{
				return;
			}
		}
		else
		{
			_printd("readConfig file2 failed");
			return;
		}
	}
	//_printd("m_stream:%s",m_stream.c_str());
	//////////////////检查匹配/////////////////
	int listsize = m_listAll["record"].size();
	int listsize2 = m_listAll["warming"].size();
	uint recordstart = 0;
	uint recordstop = 0;
	int type = -1;

	///////////成对匹配//////////////////////
	for (int i = 0; i < listsize ;  )
	{		
		if (((i+ 1) < listsize) && m_listAll["record"][i]["status"].asInt()&&
			(0 == m_listAll["record"][i+1]["status"].asInt()))
		{	
			i = i + 2;
		}
		else
		{	//TDDO删除此i
			m_listAll["record"].isremove(i);
			i = i + 1;			
		}
	}

}
void CHemuRecordManager::saveFile()
{
	static CMutex fileMutex;
	CGuard l_cGuard(fileMutex);
	CConfigWriter writer(m_stream);

	if (!m_changed)
	{
		return;
	}
	if (access("/media/tagRecord", 0))
	{
		if (mkdir("/media/tagRecord", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
		{
			return;
		}
	}
	m_changed = FALSE;

	m_stream = "";
	writer.write(m_listAll);

	remove(listFileSecond);
	rename(listFileFirst, listFileSecond);

	m_fileConfig = gzopen(listFileFirst, "wb");
	if ((int)m_stream.size() != gzwrite(m_fileConfig, (char*)m_stream.c_str(), m_stream.size()))
	{
		_printd("write config file failed!");
	}
	gzflush(m_fileConfig, Z_FINISH);
	gzclose(m_fileConfig);
}
VD_INT32   CHemuRecordManager::Stop()
{
	_printd("  CHemuRecordManager :: Stop ");
	Close();
	return 0;
}
void CHemuRecordManager::onTimer(uint arg)
{
	SDStatus();
}
VD_BOOL CHemuRecordManager::readConfig(const char* chPath, std::string& input)
{


	m_fileConfig = gzopen(chPath, "rb");
	if (!m_fileConfig)
		return FALSE;

	const int size = 32 * 1024;
	char* buf = new char[size + 1];
	int nLen = 0;

	input = "";

	while (1)
	{
		nLen = gzread(m_fileConfig, buf, size);

		if (nLen <= 0)	break;

		buf[nLen] = 0;
		input += buf;
	}
	input += '\0';
	gzclose(m_fileConfig);
	//input = buf;
	delete[]buf;

	return TRUE;
}

VD_INT32   CHemuRecordManager::DoRecvHandle()
{
	int recvlen = 0;
	if (m_connStatus != eConnected) return 0;

	if (m_tls.Recv((unsigned char*)&recvlen, sizeof(int)) != sizeof(int))
	{
		_printd("close");
		m_isockstaus = SSL_FINISHED;
		return -1;
	}
	recvlen = ntohl(recvlen);
	unsigned char recvbuf[recvlen];
	memset(recvbuf, 0, recvlen);
	if (m_tls.Recv(recvbuf, recvlen) != recvlen)
	{
		_printd("close");
		m_isockstaus = SSL_FINISHED;
		return -1;
	}
	HEDGWMessage *res_info;
	res_info = hedgwmessage__unpack(NULL, recvlen, recvbuf);
	if (res_info)
	{
		_printd("res_info->message_type:%d", res_info->message_type);
		switch (res_info->message_type)
		{
		case HEDGWMESSAGE__MESSAGE_TYPE__DevLoginResp:
		{
			_printd("dev_login_resp.result:%d", res_info->dev_login_resp->result);
			_printd("dev_login_resp.server_time:%s", res_info->dev_login_resp->server_time);
			_printd("dev_login_resp.device_status:%d", res_info->dev_login_resp->device_status);
			switch (res_info->dev_login_resp->device_status)
			{
				case 0:
				case 1:
				{
					m_idevice_status = 1;
				}
				break;
				case 2:
				{
					m_idevice_status = 0;
				}
				break;
				case 3:
				{
					m_idevice_status = 2;
				}
				break;
				case 44:
				{//已经解绑
					m_listAll.clear();
				}break;
				default:
					break;

			}
			_printd("dev_login_resp.ping_pong_span:%d", res_info->dev_login_resp->ping_pong_span);
			m_ipingspan = res_info->dev_login_resp->ping_pong_span;
			_printd("dev_login_resp.sd_upload_mode:%d", res_info->dev_login_resp->sd_upload_mode);
			_printd("dev_login_resp.person_count_span:%d", res_info->dev_login_resp->person_count_span);
			if (res_info->dev_login_resp->has_service_days)
			{
				_printd("res_info->dev_login_resp->has_service_days:%d", res_info->dev_login_resp->has_service_days);
			}
		}break;
		case HEDGWMESSAGE__MESSAGE_TYPE__PONG:
		{
			_printd("Pong");
			m_ipongtimeout = 0;
		}
		break;
		case HEDGWMESSAGE__MESSAGE_TYPE__DevServerCmd:
		{

			if (7 == res_info->dev_server_cmd->type)
			{
				_printd("device status change to %d.", res_info->dev_server_cmd->device_status);
			}
			else if (9 == res_info->dev_server_cmd->type)
			{

			}

		}
		break;
		case HEDGWMESSAGE__MESSAGE_TYPE__DevAppCmd:
		{
			_printd("dev_app_cmd.type:%d", res_info->dev_app_cmd->type);
#if 1
			_printd("dev_app_cmd.src_device_id:%s", res_info->dev_app_cmd->src_device_id);
			_printd("dev_app_cmd.dst_device_id:%s", res_info->dev_app_cmd->dst_device_id);
			_printd("dev_app_cmd.count:%d", res_info->dev_app_cmd->count);
			_printd("dev_app_cmd.id:%d", res_info->dev_app_cmd->id);
			_printd("dev_app_cmd.msg:%s", res_info->dev_app_cmd->msg);
#endif

		}
		break;
		case HEDGWMESSAGE__MESSAGE_TYPE__TDMediaPackage:
		{

		}
		break;
		default:
			break;
		}

		hedgwmessage__free_unpacked(res_info, NULL);
	}
	return 0;
}

VD_INT32   CHemuRecordManager::DoSendHandle()
{
	if (m_connStatus != eConnected) return 0;
	
	if (m_pSendBuf.size() > 0)
	{
		int len = 0;
		if (m_pSendBuf.size() > (SEND_BUFFUER - 20 * 1024))
		{
			len = m_tls.Send((unsigned char*)m_pSendBuf.data(), (SEND_BUFFUER - 20 * 1024));
		}
		else
		{
			len = m_tls.Send((unsigned char*)m_pSendBuf.data(), m_pSendBuf.size());
		}

	    if (len > 0)
		{
			m_pSendBuf.erase(0, len);
			return 0;
		}
		return -1;
	}

	if (m_iOpenFile)
	{
		ReadRecordFile();
	}


}
VD_INT32   CHemuRecordManager::Handle()
{
	if (m_connStatus == eConnected)
	{
		DoRecvSend(1000);
	}
	else
	{
		_printd("connect !!!");
		if (Connect())
		{
			m_ifailcnt = 0;
			DoAuthReq();
		}
		else
		{
			string port;
			//3次失败换地址
			//if (m_ifailcnt++ >= 3)
			//{
			//	m_ifailcnt = 0;
			//}
			g_HeMuHttpsCom.GetServerInfo(m_cserveraddr, port);
			m_iport = atoi(port.c_str());
			sleep(6);
		}
	}
}
void   CHemuRecordManager::ThreadProc()
{
	while (m_bLoop)
	{
		if (m_ireload)
		{
			Handle();
			DoTimeout();
		}
		else
		{
			if (m_connStatus == eConnected)
			{
				Close();
			}
			DectectConfig();
			sleep(5);
		}

	}
}


VD_INT32   CHemuRecordManager::Connect()
{
	int fd = -1;
	if (isValid())
	{
		_printd("fd[%d]", m_fd);
		return FALSE;
	}
	if (m_connStatus == eConnecting || m_connStatus == eConnected)
	{
		_printd("fd[%d]", m_fd);
		return FALSE;
	}
	m_connStatus = eConnecting;

	fd = m_tcpsock.createSocket();
	_printd("server:%s port:%d connect[%d]", m_cserveraddr.c_str(), m_iport);
	if (-1 != fd && !m_cserveraddr.empty() && 0 != m_iport)
	{
		//非阻塞
		m_tcpsock.setblock(fd, false);
		m_tcpsock.setSendBufferSize(SEND_BUFFUER);
		bool bConnected = m_tcpsock.connectServer(m_cserveraddr, m_iport);
		_printd("connect[%d]", bConnected);
		if (bConnected)
		{
			m_fd = fd;
			//ssl
			return SetConnected();
		}
	}
	Close();
	return FALSE;
}


VD_INT32   CHemuRecordManager::Reconnect()
{
	Close();
	Connect();
}

VD_INT32   CHemuRecordManager::DoRecvSend(uint timeout)
{
	int ret;
	fd_set read_set, write_set;
	struct timeval tv = { 0 };

	FD_ZERO(&read_set);
	FD_ZERO(&write_set);
	FD_SET(m_fd, &read_set);
	if (m_pSendBuf.size() > 0 || m_iOpenFile)
	{
		FD_SET(m_fd, &write_set);
	}
	tv.tv_sec = timeout / 1000;
	tv.tv_usec = timeout % 1000;
	m_isockstaus = 0;

	ret = select(m_fd + 1, &read_set, &write_set, NULL, &tv);

	if (0 < ret)
	{
		if (FD_ISSET(m_fd, &read_set))
		{
			m_isockstaus |= SSL_SOCK_READABLE;

		}
		if (FD_ISSET(m_fd, &write_set))
		{
			m_isockstaus |= SSL_SOCK_WRITABLE;

		}

		if (m_isockstaus & SSL_SOCK_READABLE)
		{
			DoRecvHandle();
		}
		if (m_isockstaus & SSL_SOCK_WRITABLE)
		{
			DoSendHandle();
		}
		if (m_isockstaus & SSL_FINISHED)
		{
			Close();
		}
		if (!m_iOpenFile)
		{
			//此函数配合主链接更新m_listAll 变量使用
			//当主链接正在更新m_listAll则不再打开新文件传输
			DectectConfig(1);
		}
	}
	return 0;
}
VD_INT32   CHemuRecordManager::DoTimeout()
{
	if ((m_connStatus == eConnected)
		&& (abs(int(getNowMs() / 1000 - m_iconTimeoutTime)) > m_ipingspan))
	{
		HwPing();
		m_iconTimeoutTime = getNowMs() / 1000;
		if (m_ipongtimeout++ >= 3)
		{
			Close();
		}
	}
}
int64_t CHemuRecordManager::getNowMs()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * (int64_t)1000 + tv.tv_usec / 1000;
}
VD_INT32   CHemuRecordManager::SetConnected()
{

	if (m_tls.Connect(m_fd))
	{
		m_connStatus = eConnected;
		m_tcpsock.setKeepAlive();
		m_tcpsock.setTcpNoDelay();

		return TRUE;
	}
	else
	{
		Close();
		return FALSE;
	}
}
VD_INT32   CHemuRecordManager::DoAuthReq()
{

	///end clear buf///
	int len = 0;
	unsigned char buf[1024] = { 0 };
	_printd("Rcord Auth !!!");
	HEDGWMessage msg;
	DevLoginReq  logreq;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevLoginReq;
	msg.dev_login_req = &logreq;
	dev_login_req__init(&logreq);
	g_HeMuHttpsCom.GetDevLogInfo(logreq);
	logreq.channel_id = 1;//说明是录像上传通道
	len = hedgwmessage__get_packed_size(&msg);

	if (len < (sizeof(buf) - 4))
	{
		hedgwmessage__pack(&msg, buf + 4);
		int datasize_nl = htonl(len);
		memcpy(buf, &datasize_nl, 4);
		Send((const void*)buf, len + 4, 0);
	}
	else
	{
		unsigned char *pBuf = (unsigned char *)malloc(len + 4);
		if (NULL != pBuf)
		{
			memset(pBuf, 0, len + 4);
			hedgwmessage__pack(&msg, pBuf + 4);
			int datasize_nl = htonl(len);
			memcpy(pBuf, &datasize_nl, 4);
			Send((const void*)pBuf, len + 4, 0);
			free(pBuf);
		}

	}
	return 0;
}
VD_INT32   CHemuRecordManager::HwPing()
{
	int len = 0;
	unsigned char buf[1024] = { 0 };
	_printd("Ping !!!");
	HEDGWMessage msg;
	Ping  pingreq;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__PING;
	msg.ping = &pingreq;
	ping__init(&pingreq);
	pingreq.id = 1;
	len = hedgwmessage__get_packed_size(&msg);

	hedgwmessage__pack(&msg, buf + 4);
	int datasize_nl = htonl(len);
	memcpy(buf, &datasize_nl, 4);
	Send((const void*)buf, len + 4, 0);
	return 0;
}
VD_INT32   CHemuRecordManager::Close()
{
	_printd("Close");
	m_connStatus = eUnconnected;
	m_ipongtimeout = 0;
	m_fd = -1;
	m_tcpsock.Close();
	m_tls.Close();
	m_pSendBuf.erase(0, m_pSendBuf.size());
	return 0;
}
int CHemuRecordManager::_IsBlock(int error_code)
{
	return ((error_code == EINPROGRESS) || (error_code == EWOULDBLOCK));
}

VD_INT32   CHemuRecordManager::Send(const void* buf, uint32_t len, uint32_t flag)
{
	if (m_connStatus != eConnected) return 0;
	CGuard guard(m_Mutex);

	m_pSendBuf.append((char*)buf, len);

	int sended = -1;
	sended = m_tls.Send((unsigned char*)m_pSendBuf.data(), m_pSendBuf.size());

	if (sended >= 0)
		m_pSendBuf.erase(0, sended);
	return sended;
}

void CHemuRecordManager::PackMediaData(int pagetype, int sync, unsigned char* data, unsigned int size, unsigned int timestamp, int64_t frametime)
{
	int len = 0;
	unsigned char *pbuf = NULL;
	HEDGWMessage msg;
	TDMediaPackage  packreq;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__TDMediaPackage;
	msg.td_media_package = &packreq;
	tdmedia_package__init(&packreq);
	packreq.package_type = pagetype;
	packreq.sync = sync;
	packreq.data.len = size;
	packreq.data.data = data;
	packreq.data_size = size;
	packreq.start_time = timestamp;


	if (1 == pagetype)
	{
		packreq.seq_num = m_iaudioSeq++;
	}
	else if (2 == pagetype)
	{
		packreq.seq_num = m_ivideoSeq++;

	}
	else
	{
		packreq.seq_num = m_ivideoSeq++;
		_printd("send I Frame!!!");
		m_iIFrameFlag = TRUE;
	}
	if (m_iIFrameFlag)
	{
		packreq.has_ipcam_time = 1;
		packreq.ipcam_time = frametime;//getNowMs();
		len = hedgwmessage__get_packed_size(&msg);
		pbuf = (unsigned char*)malloc(len + 4);
		if (pbuf)
		{
			hedgwmessage__pack(&msg, pbuf + 4);
			int datasize_nl = htonl(len);
			memcpy(pbuf, &datasize_nl, 4);

			//TODO:发送数据
			Send(pbuf, len + 4, 0);
			free(pbuf);
		}
		else
		{
			_printd("malloc error!!!");
		}
	}

}
static int Mp4FindFrameType(unsigned char *Buffer, int Bufferlen)
{
	int i = 0;
	int Bufferlenlocal = (Bufferlen > 512) ? 512 : Bufferlen;
	for (i = 0; i < Bufferlenlocal; i++){
		if (Buffer[i + 0] == 0x00 && Buffer[i + 1] == 0x00 &&
			Buffer[i + 2] == 0x00 && Buffer[i + 3] == 0x01)
		{
			if ((Buffer[i + 4] & 0x1f) == 7 || (Buffer[i + 4] & 0x1f) == 5){
				return 1;
			}
			else if ((Buffer[i + 4] & 0x1f) == 1){
				return 2;
			}
		}

	}
	return -1;
}

//static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
//	int len = size * nmemb;
//	int written = len;
//	FILE *fp = NULL;
//	if (access((char*)stream, 0) == -1) {
//		fp = fopen((char*)stream, "wb");
//	}
//	else {
//		fp = fopen((char*)stream, "ab");
//	}
//	if (fp)
//	{
//		fwrite(ptr, size, nmemb, fp);
//		fclose(fp);
//	}
//	printf("download size[%d]\n", size);
//	return written;
//}
VD_UINT32 CHemuRecordManager::FindIunit(uchar * buf, uint size)
{
	if (NULL == buf)
	{
		return 0;
	}
	uint i;


	i = 0;
	while (1)
	{
		if ((buf[i] == 0) && (buf[i + 1] == 0)
			&& (buf[i + 2] == 0) && (buf[i + 3] == 0x01)
			&& ((buf[i + 4] & 0x1f) == 5))
		{
			return i;
		}
		i++;
		if (i + 4 >= size)
		{
			return 0;
		}
	}


	return 0;
}
void CHemuRecordManager::ReadRecordFile()
{
	if (m_iOpenFile)
	{
		if (NULL == m_pBuf)
		{
			m_pBuf = (uchar*)malloc(1024 * 1024);
		}
		else
		{
			int  iVideoTimeStamp = 0;
			int  iAudioTimeStamp = 0;
			int Len = 0;
			
			iVideoTimeStamp = MP4Reader_GetVideoTimeStamp(m_iVideoCurFrame);
			iAudioTimeStamp = MP4Reader_GetAudioTimeStamp(m_iAudioCurFrame);
			//_printd("m_iVideoCurFrame[%d] <= MP4Reader_GetVFrameNums()[%d]", m_iVideoCurFrame ,MP4Reader_GetVFrameNums());
			//_printd("((m_iRecrdStartTimeStamp + iVideoTimeStamp / 4000) < m_iQureyEndTimeStmap)[%d]", ((m_iRecrdStartTimeStamp + iVideoTimeStamp / 4000) < m_iQureyEndTimeStmap));
			//_printd("m_iVideoCurFrame[%d]MP4Reader_GetVFrameNums[%d]", m_iVideoCurFrame, MP4Reader_GetVFrameNums());
			//_printd("m_iRecrdStartTimeStamp[%u] iVideoTimeStamp / 4000[%d] m_iQureyEndTimeStmap[%u]", m_iRecrdStartTimeStamp, iVideoTimeStamp / 4000, m_iQureyEndTimeStmap);
			if (m_iVideoCurFrame <= MP4Reader_GetVFrameNums() && ((m_iRecrdStartTimeStamp + iVideoTimeStamp / 1000) < m_iQureyEndTimeStmap))
			{
				if (iVideoTimeStamp <= iAudioTimeStamp || m_iAudioCurFrame >= MP4Reader_GetAFrameNums())
				{
					if ((Len = MP4Reader_VideoData(m_pBuf, m_iVideoCurFrame)) > 0)
					{
						int iret = Mp4FindFrameType(m_pBuf, Len);

						int64_t timestamp = (int64_t)m_iRecrdStartTimeStamp*(int64_t)1000 + (int64_t)iVideoTimeStamp;
						//if (m_listAll["warming1"].size())
						//{
						//	int i = 0;
						//	if (uint(timestamp / uint64(1000)) + 4 > m_listAll["warming1"][i]["timestamps"].asUInt())
						//	{
						//		PushEvent(m_listAll["warming1"][i]["type"].asInt(),
						//				  m_listAll["warming1"][i]["status"].asInt(),
						//				  m_listAll["warming1"][i]["timestamps"].asUInt());

						//		m_listAll["warming1"].isremove(0);
						//	}
						//}
						//else if (m_listAll["warming2"].size())
						//{
						//	int i = 0;
						//	if (uint(timestamp / uint64(1000)) + 4 > m_listAll["warming2"][i]["timestamps"].asUInt())
						//	{
						//		PushEvent(m_listAll["warming2"][i]["type"].asInt(),
						//			      m_listAll["warming2"][i]["status"].asInt(),
						//			      m_listAll["warming2"][i]["timestamps"].asUInt());

						//		m_listAll["warming2"].isremove(0);
						//	}
						//}
						if (1 == iret)
						{
							int spslen = FindIunit(m_pBuf, Len);
							PackMediaData(3, 0, m_pBuf, spslen, iVideoTimeStamp, timestamp);
							PackMediaData(2, 1, m_pBuf+spslen, Len - spslen, iVideoTimeStamp, timestamp);
							//struct tm *p;
							//time_t timep = m_iRecrdStartTimeStamp + iVideoTimeStamp / 1000 + 8 * 60 * 60;
							//p = gmtime(&timep);
							//_printd("recordtimestamp[%u] iVideoTimeStam[%d]timep[%ld]", m_iRecrdStartTimeStamp, iVideoTimeStamp, timep);
							//snprintf(testh264filename, 128, "I Frame:%04d-%02d-%02d %02d:%02d:%02d\n",
							//									p->tm_year+1900,p->tm_mon+1,p->tm_mday,p->tm_hour,
							//									p->tm_min,p->tm_sec);
							//writefile_callback(testh264filename, strlen(testh264filename), 1, (void*)"/tmp/test.data");
							_printd("I : [%02x%02x%02x%02x%02x] TotalVideoNub = %d, CurVideoIndex = %d ,VideoTimestamp =%d\n", m_pBuf[0], m_pBuf[1], m_pBuf[2], m_pBuf[3], m_pBuf[4], MP4Reader_GetVFrameNums(), m_iVideoCurFrame, iVideoTimeStamp);

						}
						else if (2 == iret)
						{
							PackMediaData(2, 0, m_pBuf, Len, iVideoTimeStamp, timestamp);							
						}
						else
						{
							_printd("Read I P fream error [%d]!", iret);
							for (int i = 0; i < 16; i++)
								printf("%x,", m_pBuf[i]);
							printf("\n");
						}
					}
					else
					{
						printf("Read Video Error\n");
					}
					m_iVideoCurFrame++;


				}
				else
				{
					int64_t timestamp = (int64_t)m_iRecrdStartTimeStamp*(int64_t)1000 + (int64_t)iAudioTimeStamp;
					if ((Len = MP4Reader_AudioData(m_pBuf, m_iAudioCurFrame))> 0)
					{
						PackMediaData(1, 0, m_pBuf, Len, iAudioTimeStamp, timestamp);
					}
					else
					{
						_printd("Read Audio Error");
					}

					m_iAudioCurFrame++;
				}
			}
			else
			{
				m_iOpenFile = FALSE;
				_printd("close mp4 file....");
				MP4Reader_Close();
				_printd("m_iRecrdStartTimeStamp[%u] + m_iRecrdTimeDuration[%d] >= m_iQureyEndTimeStmap[%u]",
					    m_iRecrdStartTimeStamp, m_iRecrdTimeDuration, m_iQureyEndTimeStmap);
				
				if (m_iRecrdStartTimeStamp + m_iRecrdTimeDuration >= m_iQureyEndTimeStmap)
				{
					int i = 0;
					m_changed = TRUE; 
					//ReUploadTime(m_itype, 1, m_iQureyStartTimeStamp, 1, 0);
					//ReUploadTime(m_itype, 0, m_iQureyEndTimeStmap, 1, 0);
					switch (m_itype)
					{
					case 0:
						m_listAll["record"].isremove(i);
						break;
					case 1:
						m_listAll["warming1"].isremove(i);
						PushEvent(1, 0, m_iQureyEndTimeStmap);
						break;
					case 2:
						m_listAll["warming2"].isremove(i);
						PushEvent(2, 0, m_iQureyEndTimeStmap);
						break;
					default:
						break;
					}
					m_iSeekTimeStamp = 0;
				}
				else
				{
					if (m_itype)
					{//事件夸文件，用m_iSeekTimeStamp,则不更新开始时间
						m_iSeekTimeStamp = m_iRecrdStartTimeStamp + (uint)m_iRecrdTimeDuration;
					}
					else
					{	
						
						ReUploadTime(m_itype, 1, m_iQureyStartTimeStamp, 1, m_iRecrdStartTimeStamp + m_iRecrdTimeDuration);
						m_iSeekTimeStamp = m_iRecrdStartTimeStamp + m_iRecrdTimeDuration;
						_printd("m_iSeekTimeStamp[%d]", m_iSeekTimeStamp);
						//
						if (m_iRecrdStartTimeStamp == m_iSeekTimeStamp)
						{
							m_iSeekTimeStamp = m_iRecrdStartTimeStamp + 4;
						}

					}	
				}
				sleep(1);
			}
			
			usleep(10 * 1000);
		}
	}
}
void CHemuRecordManager::OpenRecordFile(int type, uint  recordstarttime,uint recordenttime)
{
	_printd("open record tieme:   %u", recordstarttime);
	int ret = FALSE;
	char Path[256] = {0};

	int year, mon, day, hour, min, sec;
	int timelen, snum;
	int recordsec = 0;
	long rectimestamp = 0;
	VideoFiles  List;
	SYSTEM_TIME  StartTime = {0};
	SYSTEM_TIME  EndTime = {0};
	uint querystart = recordstarttime + 8 * 60 * 60 - INTERVAL_VIDEO; //加时区 提前一个录像文件时长
	uint queryend = recordenttime + 8 * 60 * 60 + INTERVAL_VIDEO; //加时区 往后一个录像文件时长

	struct tm gmt_start = *gmtime((long int*)&querystart);
	struct tm gmt_end = *gmtime((long int*)&queryend);
	StartTime.year = gmt_start.tm_year + 1900;
	StartTime.month = gmt_start.tm_mon + 1;
	StartTime.day = gmt_start.tm_mday;
	StartTime.hour = gmt_start.tm_hour;
	StartTime.minute = gmt_start.tm_min;
	StartTime.second = gmt_start.tm_sec;

	EndTime.year = gmt_end.tm_year + 1900;
	EndTime.month = gmt_end.tm_mon + 1;
	EndTime.day = gmt_end.tm_mday;
	EndTime.hour = gmt_end.tm_hour;
	EndTime.minute = gmt_end.tm_min;
	EndTime.second = gmt_end.tm_sec;
	//查询当前时间的文件
	int filecout = 0;
	_printd("starttime:%4d-%02d-%02d %02d:%02d:%02d", StartTime.year, StartTime.month, StartTime.day,
		StartTime.hour, StartTime.minute, StartTime.second);
	_printd("endtime:%4d-%02d-%02d %02d:%02d:%02d", EndTime.year, EndTime.month, EndTime.day,
		EndTime.hour, EndTime.minute, EndTime.second);
	
	filecout = file_get_all_videolist_smallsort(&List, &StartTime, &EndTime);
	if (filecout <= 0)
	{
		//删除此段时间段 json 段
		_printd("delete!!!start[%u] end[%u]", recordstarttime, recordenttime);
		ReUploadTime(type, 1, recordstarttime, 1, 0);
		ReUploadTime(type, 0, recordenttime, 1, 0);
		m_iSeekTimeStamp = 0;
		return;
	}

	m_itype = type;
	m_iQureyStartTimeStamp = recordstarttime;	
	m_iQureyEndTimeStmap = recordenttime;
	if (!m_iSeekTimeStamp)
	{
		m_iSeekTimeStamp = recordstarttime;
	}
	for (size_t i = 0; i < filecout; i++)
	{
		sscanf(List.FilesList[i].FileName, "%04d%02d%02d%02d%02d%02d_%d_%d.mp4",
			&year, &mon, &day, &hour, &min, &sec, &timelen, &snum);

		m_record_tm.tm_year = year - 1900;
		m_record_tm.tm_mon = mon - 1;
		m_record_tm.tm_mday = day;
		m_record_tm.tm_hour = hour;
		m_record_tm.tm_min = min;
		m_record_tm.tm_sec = sec;

		m_iRecrdTimeDuration = timelen / 100 * 60 + (timelen % 100);
		rectimestamp = mktime(&m_record_tm) - 8 * 60 * 60;
		if ((rectimestamp + m_iRecrdTimeDuration) > /*recordstarttime*/m_iSeekTimeStamp)
		{
			snprintf(Path, sizeof(Path), "%s/%s", List.FilesList[i].DirPath, List.FilesList[i].FileName);
			_printd("REC path:%s/%s", List.FilesList[i].DirPath, List.FilesList[i].FileName);
			ret = TRUE;
			break;
		}
	}

	//_printd("MP4 File Name is [%s] FileTotalTime = %d - -\n", Path, FileTotalTime);

	if (ret && MP4Reader_Open(Path))
	{
		m_iOpenFile = TRUE;
		m_iVideoCurFrame = 1;
		m_iAudioCurFrame = 1;
		
		m_iRecrdStartTimeStamp = (uint)mktime(&m_record_tm) - (uint)(8 * 60 * 60);
		//跳转指定开始时间
		for (int i = 1; i < MP4Reader_GetVFrameNums(); i++)
		{
			int iVideoTimeStamp = MP4Reader_GetVideoTimeStamp(i);
			//4为4秒一个I帧，提前一个I帧
			if ((recordstarttime - 4) <= (iVideoTimeStamp / 1000 + m_iRecrdStartTimeStamp))
			{
				m_iVideoCurFrame = i;
				break;
			}
		}
		for (int i = 1; i < MP4Reader_GetAFrameNums(); i++)
		{
			int iAudioTimeStamp = MP4Reader_GetAudioTimeStamp(i);
			if ((recordstarttime - 4) <= (iAudioTimeStamp / 1000 + m_iRecrdStartTimeStamp))
			{
				m_iAudioCurFrame = i;
				break;
			}
		}
		if (m_itype && (m_iSeekTimeStamp == m_iQureyStartTimeStamp))
		{
			//事件开始时间
			PushEvent(m_itype, 1, m_iQureyStartTimeStamp);
		}
		_printd("MP4 File Name is opened successly\n");

	}
	else
	{
		//TODO 修改json文件下一个文件为开始时间戳
		if (type || (recordstarttime + m_iRecrdTimeDuration) >= recordenttime)
		{//告警就删除
			ReUploadTime(type, 1, recordstarttime, 1, 0);
		}
		else
		{
			ReUploadTime(type, 1, recordstarttime, 1, recordstarttime + m_iRecrdTimeDuration);
		}
	}

}

void CHemuRecordManager::ReUploadTime(int type, int status, uint timestamps, int update, uint updatetimestamp,int errordate)
{
#if 0
	struct tm *p;
	time_t timep = (time_t)timestamps + 8 * 60 * 60;;
	p = localtime(&timep);
	if (0 == type && status)
	{
		snprintf(testh264filename, 128, "start:%d-%d-%d  %d:%d:%d\n", 1900 + p->tm_year, 1 + p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec);
		writefile_callback(testh264filename, strlen(testh264filename), 1, (void*)"/tmp/test.data");
	}
	else if (0 == type && 0 == status)
	{
		snprintf(testh264filename, 128, "end  :%d-%d-%d  %d:%d:%d\n", 1900 + p->tm_year, 1 + p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec);
		writefile_callback(testh264filename, strlen(testh264filename), 1, (void*)"/tmp/test.data");
	}
#endif
	if (timestamps < 1546272000/*2019年*/)
	{
		//异常数据则直接跳过
		return;
	}
	if (1 != m_iSdStatus)
	{
		m_listAll.clear();
		return;
	}
	if (type == 0)
	{
		if (update)
		{//更新云录像段开始结束时间
			int i = 0;
			int list_size = m_listAll["record"].size();
			for (i = 0; i < list_size; i++)
			{
				if (type == m_listAll["record"][i]["type"].asInt()
					&& status == m_listAll["record"][i]["status"].asUInt()
					&& timestamps == m_listAll["record"][i]["timestamps"].asUInt())
				{
					if (updatetimestamp)
					{
						if (status)
						{
							m_listAll["record"][i]["timestamps"] = updatetimestamp - errordate;
						}
						else
						{//结束误差十秒，多传10秒
							m_listAll["record"][i]["timestamps"] = updatetimestamp + errordate;
						}
					}
					else
					{
						//TODO 删除此时间戳
						m_listAll["record"].isremove(i);
					}
					break;
				}
			}

		}
		else
		{
			//插入断网开始结束时间
			int list_size = m_listAll["record"].size();
			CConfigTable recordtime;
			recordtime["type"] = type;
			recordtime["status"] = status;
			recordtime["timestamps"] = timestamps;
			m_listAll["record"][list_size] = recordtime;
			_printd("======input=======");
		}
	}
	else if (1 == type)
	{
		if (update)
		{//更新开始结束时间
			int i = 0;
			int list_size = m_listAll["warming1"].size();
			for (i = 0; i < list_size; i++)
			{
				if (type == m_listAll["warming1"][i]["type"].asInt()
					&& status == m_listAll["warming1"][i]["status"].asUInt()
					&& timestamps == m_listAll["warming1"][i]["timestamps"].asUInt())
				{
					if (updatetimestamp)
					{
						if (status)
						{
							m_listAll["warming1"][i]["timestamps"] = updatetimestamp - errordate;
						}
						else
						{ 
							m_listAll["warming1"][i]["timestamps"] = updatetimestamp + errordate;
						}
					} 
					else
					{
						//TODO 删除此时间戳
						m_listAll["warming1"].isremove(i);
					}
					break;
				}
			}

		}
		else
		{
			//告警的+
			int list_size = m_listAll["warming1"].size();
			CConfigTable recordtime;
			recordtime["type"] = type;
			recordtime["status"] = status;
			recordtime["timestamps"] = timestamps;
			m_listAll["warming1"][list_size] = recordtime;
			//{
			//	//////////////test///////////////////////
			//	char buftmp[256];
			//	snprintf(buftmp, sizeof(buftmp), "type:%d status:%d timestamps:%u\n", type, status, timestamps);
			//	writefile_callback(( void*)buftmp, strlen(buftmp), 1, (void*)"/tmp/test1.data");
			////}
		}
	}
	else if (2 == type)
	{
		if (update)
		{//更新开始结束时间
			int i = 0;
			int list_size = m_listAll["warming2"].size();
			for (i = 0; i < list_size; i++)
			{
				if (type == m_listAll["warming2"][i]["type"].asInt()
					&& status == m_listAll["warming2"][i]["status"].asUInt()
					&& timestamps == m_listAll["warming2"][i]["timestamps"].asUInt())
				{
					if (updatetimestamp)
					{
						if (status)
						{
							m_listAll["warming2"][i]["timestamps"] = updatetimestamp - errordate;
						}
						else
						{
							m_listAll["warming2"][i]["timestamps"] = updatetimestamp + errordate;
						}
					}
					else
					{
						//TODO 删除此时间戳
						m_listAll["warming2"].isremove(i);
					}
					break;
				}
			}

		}
		else
		{
			int list_size = m_listAll["warming2"].size();
			CConfigTable recordtime;
			recordtime["type"] = type;
			recordtime["status"] = status;
			recordtime["timestamps"] = timestamps;
			m_listAll["warming2"][list_size] = recordtime;
		}
	}
	m_changed = TRUE;
	
	//保存在SD状态检测内
	//saveFile();
}


void  CHemuRecordManager::SDStatus()
{
	//上报到服务器SD切换状态

	int Remainmem = 0;
	int Totalmem = 0;
	int ret = -1;
	int sdcurstatus = 0; 

	ret = file_sys_get_cap(&Totalmem, &Remainmem);

	if (ret != -1)
	{
		if (FS_WRITE_ERROR == g_DriverManager.GetSDStatus())
		{
			sdcurstatus = 2;
		}
		else
		{
			sdcurstatus = 1;
		}
	}
	else
	{
		if (file_sys_get_linkstat() < 0)
		{
			sdcurstatus = 0;
		}
		else
		{ //卡异常
			sdcurstatus = 2;
		}
	}
	
	if (sdcurstatus != m_iSdStatus)
	{
		if (1 == sdcurstatus)
		{
			_printd("Init sdcard reload list!");
			InitUploadList();
		}
		m_iSdStatus = sdcurstatus;
		g_HeMuHttpsCom.SendOpenAPIReq(MSG_P_THD_SET_CAMERA_SETTINGS_REQ);
		_printd("MSG_P_THD_SET_CAMERA_SETTINGS_REQ");
		//TODO 发送SD卡状态
	}
	if (1 == m_iSdStatus)
	{//文件状态变化则保存
		saveFile();		
	}
}

void  CHemuRecordManager::DectectConfig(int openfile)
{
	
	int listsize = m_listAll["record"].size();
	int listsize1 = m_listAll["warming1"].size();
	int listsize2 = m_listAll["warming2"].size();
	uint curtimestamp = (uint)(getNowMs() / (int64_t)1000);
	int  reloadtype = 0;
	if (0 == m_listAll["record"].size() &&
		0 == m_listAll["warming1"].size() &&
		0 == m_listAll["warming2"].size())
	{
		m_ireload = 0;
		return;
	}
	if (0 == openfile && m_iHWConect)
	{
		//等待配置更新完在处理
		m_ireload = 1;
	}

	if (!m_iOpenFile && openfile)
	{
		
		uint recordstart = 0;
		uint recordstop = 0;
		int type = -1;
		
		///////////成对匹配//////////////////////
		if (1 == m_idevice_status)
		{
			if (m_iSeekTimeStamp)
			{//时间戳
				recordstart = m_iSeekTimeStamp;
				recordstop = m_iQureyEndTimeStmap;
				reloadtype = m_itype;
			}
			else
			{
				for (int i = 0; i < m_listAll["record"].size();)
				{
					if (((i + 1) < m_listAll["record"].size()) && m_listAll["record"][i]["status"].asInt() &&
						(0 == m_listAll["record"][i + 1]["status"].asInt()))
					{
						if (m_listAll["record"][i]["timestamps"].asUInt() < 1546272000)//2019年
						{
							m_listAll["record"].isremove(i);
							_printd("delete index");
							continue;
						}
						//开始超过一天
						if ((curtimestamp > m_listAll["record"][i]["timestamps"].asUInt()) &&
							(curtimestamp - m_listAll["record"][i]["timestamps"].asUInt()) > 24 * 60 * 60)
						{
							m_listAll["record"][i]["timestamps"] = curtimestamp - 24 * 60 * 60; //修改成前一天的
						}

						if (m_listAll["record"][i]["timestamps"].asUInt() < m_listAll["record"][i + 1]["timestamps"].asUInt())
						{
							if (!recordstart && !recordstop)
							{
								recordstart = m_listAll["record"][i]["timestamps"].asUInt();
								recordstop = m_listAll["record"][i + 1]["timestamps"].asUInt(); //10秒误差
								type = 0;
								break;
							}//////////////////////////////////////////
							i = i + 2;
						}
						else
						{
							//开始大于结束删除掉
							m_listAll["record"].isremove(i);
						}

					}
					else
					{	//TDDO删除此i
						m_listAll["record"].isremove(i);
					}
				}
			}
		}
		else if (2 == m_idevice_status)
		{
			if (m_iSeekTimeStamp)
			{//在一个事件中的时间戳
				recordstart = m_iSeekTimeStamp;
				recordstop = m_iQureyEndTimeStmap;
				reloadtype = m_itype;
			}
			else
			{			
				uint war_recordstart1 = 0;
				uint war_recordstop1 = 0;
				uint war_recordstart2 = 0;
				uint war_recordstop2 = 0;
				//////////////////////类型1///////////////////////////////
				_printd("m_listAll[\"warming1\"].size()[%d]", m_listAll["warming1"].size());
				for (int i = 0; i < m_listAll["warming1"].size();)
				{
					if (((i + 1) < m_listAll["warming1"].size()) && m_listAll["warming1"][i]["status"].asInt() &&
						(0 == m_listAll["warming1"][i + 1]["status"].asInt()) )
					{
						//////////////////////////////
						if (!war_recordstart1 && !war_recordstop1)
						{
							war_recordstart1 = m_listAll["warming1"][i]["timestamps"].asUInt();
							war_recordstop1 = m_listAll["warming1"][i + 1]["timestamps"].asUInt();
							type = 0;
							break;
						}
						//////////////////////////////////////////
						i = i + 2;
					}
					else
					{	//TDDO删除此i
						m_listAll["warming1"].isremove(i);
					}
				}
				//////////////////////类型2///////////////////////////////
				for (int i = 0; i < m_listAll["warming2"].size();)
				{
					if (((i + 1) < m_listAll["warming2"].size()) && m_listAll["warming2"][i]["status"].asInt() &&
						(0 == m_listAll["warming2"][i + 1]["status"].asInt()))
					{
						/////////////////此段用于测试/////////////
						if (!war_recordstart2 && !war_recordstop2)
						{
							war_recordstart2 = m_listAll["warming2"][i]["timestamps"].asUInt();
							war_recordstop2 = m_listAll["warming2"][i + 1]["timestamps"].asUInt();
							type = 0;
							break;
						}
						//////////////////////////////////////////
						i = i + 2;
					}
					else
					{	//TDDO删除此i
						m_listAll["warming2"].isremove(i);
					}
				}
				if (m_listAll["warming1"].size() && m_listAll["warming2"].size())
				{
					if (war_recordstart1 < war_recordstart2)
					{
						reloadtype = 1;
						recordstart = war_recordstart1;
						//事件时间交叉
						if (war_recordstop1 > war_recordstart2)
						{
							recordstop = war_recordstop2;
						}
						else
						{//事件时间不交叉
							recordstop = war_recordstop1;
						}
					}
					else
					{
						reloadtype = 2;
						recordstart = war_recordstart2;
						//事件时间交叉
						if (war_recordstop2 > war_recordstart1)
						{
							recordstop = war_recordstop1;
						}
						else
						{//事件时间不交叉
							recordstop = war_recordstop2;
						}
					}
				}
				else if (m_listAll["warming1"].size())
				{
					reloadtype = 1;
					recordstart = war_recordstart1;
					recordstop = war_recordstop1;
				}
				else if (m_listAll["warming2"].size())
				{
					reloadtype = 2;
					recordstart = war_recordstart2;
					recordstop = war_recordstop2;
				}
			}
		}
		else
		{
			//过期则不重传
			_printd("clear record list");
			remove(listFileSecond);
			remove(listFileSecond);
			m_listAll.clear();
		}
		//TODO 判断 不匹配则删除时间开始段
		if (recordstart && recordstop)
		{
			OpenRecordFile(reloadtype, recordstart, recordstop);
		}
	}
}

int CHemuRecordManager::PushEvent(int type, int status, uint timestamp)
{

	char buf[1024] = { 0 };
	HEDGWMessage msg;
	DevEventCmd  req;
	hedgwmessage__init(&msg);
	msg.message_type = HEDGWMESSAGE__MESSAGE_TYPE__DevEventCmd;
	msg.dev_event_cmd = &req;
	dev_event_cmd__init(&req);

	req.type = type;
	req.status = status;
	req.ipcam_time = (int64_t)timestamp*(int64_t)1000;
	//{
	////////////////test///////////////////////
	//	char buftmp[256];
	//	snprintf(buftmp, sizeof(buftmp), "type:%d status:%d timestamps:%u\n", type, status, timestamp);
	//	writefile_callback((void*)buftmp, strlen(buftmp), 1, (void*)"/tmp/test.data");
	//}

	int len = hedgwmessage__get_packed_size(&msg);
	_printd("Push event type[%d]!!![%d] timestamps[%u] ", type, status, timestamp);
	if (len < (sizeof(buf) - 4))
	{
		hedgwmessage__pack(&msg, (uchar*)(buf + 4));
		int datasize_nl = htonl(len);
		memcpy(buf, &datasize_nl, 4);
		Send((const void*)buf, len + 4, 0);
	}
	else
	{
		uchar *pbuf = (uchar *)malloc(len + 4);

		if (NULL == pbuf)return -1;
		memset(pbuf, 0, len + 4);
		hedgwmessage__pack(&msg, pbuf + 4);
		int datasize_nl = htonl(len);
		memcpy(pbuf, &datasize_nl, 4);
		Send((const void*)pbuf, len + 4, 0);
		free(pbuf);
	}
	return 0;
}