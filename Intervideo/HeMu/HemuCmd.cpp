#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "APIs/CommonBSP.h"
#include "Intervideo/HeMu/HemuCmd.h"
#include "Functions/PtzTrace.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/HeMu/HWComunication.h"
#include "Functions/Record.h"
#include "Functions/FFFile.h"



HemuCmd::HemuCmd():CThread("HemuCmd", TP_NET)
  ,m_iInit(0)
  ,m_packet_count(0)
{	
	_printd("HemuCmd");

}
HemuCmd::~HemuCmd()
{

}

VD_INT32 HemuCmd::Start()
{
	if(m_iInit) return 0;
	m_iInit = 1;
	CreateThread();
	return 0;
}
VD_INT32 HemuCmd::Stop()
{
	_printd("HemuCmd :: Stop ");
	return 0;
}

void HemuCmd::ThreadProc()
{

	while (m_bLoop) 
	{
		if(m_packet_count)
		{
			HandleCmdData();			
		}
		else
		{			
			usleep(15*1000);
		}
	}	
}
VD_INT32 HemuCmd::SDCardFormat()
{
	_printd("format SD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	if(g_RecordStatus)
	{
		_printd("#############StopManulRecord##############");
		g_Record.StopRec(REC_CLS, 0);
	}	

	return file_sys_format();
}

VD_INT32 HemuCmd::AddCmdData(int ctrl,uint value,uint value1)
{
	PACKET_CMD  packet_cmd;
	packet_cmd.ctrl = ctrl;
	packet_cmd.value = value;
	packet_cmd.value1 = value1;
	m_mutex_list.Enter();
	m_packet_list.push_back(packet_cmd);
	m_packet_count++;
	m_mutex_list.Leave();
	return 0;
}

VD_INT32 HemuCmd::HandleCmdData()
{
	int value = 0;
	int value1 = 0;
	int ctrl  = 0;
	int ptzcmd = 0;
	m_mutex_list.Enter();
	PACKET_LIST::iterator it = m_packet_list.begin();
	value  = it->value;
	ctrl   = it->ctrl;
	value1  = it->value1;
	m_packet_list.pop_front();
	m_packet_count--;
	m_mutex_list.Leave();

	_printd("ctrl[%d] value[%d]",ctrl,value);

	if(3 == ctrl)
	{
		int expendTime = 0;
#ifdef MOTOR
		SetMotorPresetPos(value, value1,&expendTime);
#endif
	}
	else if(2 == ctrl)
	{//SDFormat
		int ret = SDCardFormat();
		g_HWComm.SdcardFormatResp(ret,value);
	}
	//PTZ
	else if(0 == ctrl || 1 == ctrl)
	{
		switch(value)
		{
			case 0:
				if(ctrl)
				{
					
					ptzcmd = RT_PTZ_MV_STOP;
					_printd("stop");
				}
				else
				{
					//��λ
					ptzcmd = PTZRUN_GOTO_MID;
					_printd("reset");
				}
			break;
			case 1:
				ptzcmd = RT_PTZ_MV_LEFT;
			break;
			case 2:
				ptzcmd = RT_PTZ_MV_RIGHT;
			break;
			case 3:
				ptzcmd = RT_PTZ_MV_UP;
			break;
			case 4:
				ptzcmd = RT_PTZ_MV_DOWN;
			break;
			default:
			break;
		}
#ifdef MOTOR		
		switch(ptzcmd)
		{
			case RT_PTZ_MV_STOP:
				g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
				break;
			case RT_PTZ_MV_UP:
				g_PtzTrace.PtzRunCmd(PTZRUN_UP);
				break;
			case RT_PTZ_MV_DOWN:
				g_PtzTrace.PtzRunCmd(PTZRUN_DOWN);
				break;
			case RT_PTZ_MV_LEFT://R2P2 Q1  right|left
				g_PtzTrace.PtzRunCmd(PTZRUN_LEFT);
				break;
			case RT_PTZ_MV_RIGHT:
				g_PtzTrace.PtzRunCmd(PTZRUN_RIGHT);
				break;
			case PTZRUN_GOTO_MID:
				g_PtzTrace.PtzRunCmd(PTZRUN_GOTO_MID);
				break;
			default:
				break;
		}
		if(0 == ctrl)
		{
			usleep(200*1000);
			g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
		}
#endif
	}

	return 0;

}

