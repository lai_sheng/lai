#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <errno.h>  
#include <string.h>  
#include <dirent.h>  
#include <errno.h>  
#include <sys/stat.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <time.h> 
#include "APIs/CommonBSP.h"
#include "APIs/Audio.h"
#include "Intervideo/MobileCountry/MobileCountryAppTalk.h"
#include "Functions/WifiLink.h"

#define TALKPLAYUNIT (128)


AppTalk::AppTalk():CThread("AppTalk", TP_NET)
  ,m_iplayStart(0)
  ,m_iInit(0)
  ,m_packet_count(0)
  ,m_itimeout(0)
{	
	_printd("AppTalk");

}
AppTalk::~AppTalk()
{

}

VD_INT32 AppTalk::Start()
{
	if(m_iInit) return 0;
	m_iInit = 1;
	CreateThread();
	return 0;
}
VD_INT32 AppTalk::Stop()
{
	_printd("AppTalk :: Stop ");
	return 0;
}

void AppTalk::ThreadProc()
{

	while (m_bLoop) 
	{
		if(m_packet_count)
		{
			m_itimeout = 0;
			HandleTalkData();
			
		}
		else
		{			
			//3��
			if(m_iplayStart && (m_itimeout++ >= 600))
			{
				m_iplayStart = 0;
				_printd("AudioTalkPlayFinish");
				AudioTalkPlayFinish();	
			}
			usleep(15*1000);
		}
	}	
}

VD_INT32 AppTalk::AddTalkData(uchar* pbuf, uint size)
{
	if(NULL == pbuf || 0 == size)
	{
		return -1;
	}
	uchar *audiobuf = (uchar *)malloc(size);
	if(audiobuf)
	{
		memset(audiobuf, 0, size);
		memcpy(audiobuf, pbuf, size);
		PACKET_AUDIO  packet_audio;
		packet_audio.pBuf = audiobuf;
		packet_audio.size = size;
		m_mutex_list.Enter();
		m_packet_list.push_back(packet_audio);
		m_packet_count++;
		m_mutex_list.Leave();
	}
	
	return 0;
}

VD_INT32 AppTalk::HandleTalkData()
{
	int i = 0;
	if(0 == m_iplayStart)
	{
		//AudioPlayAudioStart(AUDIO_START_TALK, (enum audio_encode_type)16, 0);
		AudioTalkPlaySet(1 ,8000 ,16 ,100);
		_printd("AudioTalkPlaySet");
	}
	m_iplayStart = 1;
	uchar* pBuf = NULL;
	uint  len = 0;
	m_mutex_list.Enter();
	PACKET_LIST::iterator it = m_packet_list.begin();
	pBuf = it->pBuf;
	len = it->size;
	m_packet_list.pop_front();
	m_packet_count--;
	m_mutex_list.Leave();	
	if(len > TALKPLAYUNIT)
	{
		//�ײ��֧��512�ֽ�
		for( i = 0; i < len/TALKPLAYUNIT; i++)
		{
			AudioTalkPlayFrame((uchar*)pBuf + i*TALKPLAYUNIT,TALKPLAYUNIT);
		}
		
		if(len%TALKPLAYUNIT != 0)
			AudioTalkPlayFrame((uchar*)pBuf + i*TALKPLAYUNIT,len - i*TALKPLAYUNIT);
	}
	else
	{
		AudioTalkPlayFrame((uchar*)pBuf ,len);
	}
//	AudioTalkPlayFrame(pBuf,len);
 	free(pBuf);
}

