#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <curl/curl.h>

#include "Intervideo/RecordTs/RecordTsUpLoad.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"

#define  FIXEDSECUREKEY  "9f3e2081d4505c825708d81d7a50a569"
#define  SECUREKEY    "1234567890123456789012345678901234"


static FILE *errStream = stderr;


static int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  
size_t write_memory(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    MemChunk *mem = (MemChunk *)userp;

    mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL)
    {
        /* out of memory! */ 
        fprintf(errStream, "not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
 
    return realsize;
}


TImage::TImage()
    : m_buffer(NULL)
    , m_len(0)
{}

TImage::TImage(const TImage & img)
    : m_buffer(NULL)
    , m_len(0)
{
    m_tag = img.m_tag;
    if (img.m_buffer) {
        setBinary(img.m_buffer, img.m_len, img.m_filename);
    } else {
        m_url = img.m_url;
        m_path = img.m_path;
    }
}

TImage::~TImage()
{
    free(m_buffer);
}

TImage & TImage::operator=(const TImage & img)
{
    m_tag = img.m_tag;
    if (img.m_buffer) {
        setBinary(img.m_buffer, img.m_len, img.m_filename);
    } else {
        m_url = img.m_url;
        m_path = img.m_path;
    }

    return *this;
}


void TImage::setURL(const string & url)
{
    m_url = url;
    m_path.clear();
    free(m_buffer);
    m_len = 0;
    m_filename.clear();
}

void TImage::setPath(const string & filepath)
{
    m_path = filepath;
    m_url.clear();
    free(m_buffer);
    m_len = 0;
    m_filename.clear();
}

void TImage::setBinary(const void * buf, size_t buf_len, const string & filename)
{
    m_len = buf_len;
    m_filename = filename;
    m_buffer = malloc(buf_len);
    memcpy(m_buffer, buf, buf_len);

    m_url.clear();
    m_path.clear();
}
//组header
void  compose_header(struct curl_slist **headerlist)
{
	SYSTEM_TIME strNow;
    memset(&strNow, 0, sizeof(SYSTEM_TIME));
    SystemGetCurrentTime(&strNow);

	char requesttime[128] = {0};
	snprintf(requesttime,sizeof(requesttime), "appRequestTime:%04d-%02d-%02d %02d:%02d:%02d",
		strNow.year, strNow.month, strNow.day, strNow.hour, strNow.minute, strNow.minute);

	//明文
	char reqtime_fix_sec[256] = {0};	
	snprintf(reqtime_fix_sec, sizeof(reqtime_fix_sec), "%04d-%02d-%02d %02d:%02d:%02d%s%s",strNow.year, strNow.month,
		strNow.day, strNow.hour, strNow.minute, strNow.minute, SECUREKEY,FIXEDSECUREKEY);

	//秘钥
	char fix_sec[256] = {0};	
	snprintf(fix_sec, sizeof(fix_sec), "%s%s",FIXEDSECUREKEY, SECUREKEY);

	char mac[256] = {0};
	uint  maclen = 0;
	HMAC(EVP_sha1(), (const void *)fix_sec, strlen(fix_sec), (unsigned char *) reqtime_fix_sec, strlen(reqtime_fix_sec), (unsigned char *)mac, &maclen);

	_printd("reqtime_fix_sec:%s",reqtime_fix_sec);

	char buff[EVP_MAX_MD_SIZE] = {0};
	char hex[EVP_MAX_MD_SIZE] = {0};
	strcpy(hex, "");
    for (int i = 0; i != maclen; i++) {
        snprintf(buff,EVP_MAX_MD_SIZE,"%02x", (unsigned char)mac[i]);
        strcat(hex, buff);
    }
	_printd("signInfo:  size[%d] hex[%s]", maclen, hex);

	char secureKey[256] ={0} ;	
	snprintf(secureKey, sizeof(secureKey), "secureKey:%s",SECUREKEY);
	
	char signInfo[256] ={0} ;
	snprintf(signInfo, sizeof(signInfo), "signInfo:%s",hex);
	
   	*headerlist = curl_slist_append(*headerlist, "User-Agent:ipc");
	curl_slist_append(*headerlist, "clientType:3");
	curl_slist_append(*headerlist, requesttime);
	curl_slist_append(*headerlist, secureKey);
	curl_slist_append(*headerlist, signInfo);
	
}

//组表单
static
void  compose_form(struct curl_httppost ** post, int type,const string & filename, 
	TImage *pimages, const string & uid, 
    const string & sttimestamp, const string & edtimestamp)
{
    struct curl_httppost* last = NULL;
	//warning
	if(type)
	{
		if(NULL == pimages)
		{
			//图片对象指针必须有,对象指针包含事件类型
			_printd("Object image error");
			return ;
		}	
		_printd("pImage = [%p]", pimages);
		if(!uid.empty())
		{
		    curl_formadd(post, &last,
		               CURLFORM_COPYNAME, "mcId",
		               CURLFORM_COPYCONTENTS, uid.c_str(),
		               CURLFORM_END);	
		}
		if(!pimages->tag().empty())
		{
		    curl_formadd(post, &last,
		               CURLFORM_COPYNAME, "type",
		               CURLFORM_COPYCONTENTS, pimages->tag().c_str(),
		               CURLFORM_END);
		}
		//苏宁的要求图片和文件必须同时上传
		if(!filename.empty())
		{
		    curl_formadd(post, &last,
		               CURLFORM_COPYNAME, "file",
		               CURLFORM_FILE, filename.c_str(),
		               CURLFORM_END);
	

			//image
			if (!pimages->path().empty())
	        {
	            //Upload file with file path
	            curl_formadd(post, &last,
	               CURLFORM_COPYNAME, "image",
	               CURLFORM_FILE, pimages->path().c_str(),
	               CURLFORM_END);
	        }
			else if (pimages->buffer())
			{
				//CURLFORM_CONTENTTYPE, "application/octet-stream",
				/* Add a buffer to upload */
				curl_formadd(post, &last,
					CURLFORM_COPYNAME, "image",
					CURLFORM_BUFFER, pimages->filename().c_str(),
					CURLFORM_BUFFERPTR, pimages->buffer(),
					CURLFORM_BUFFERLENGTH, pimages->bufferLength(),
					CURLFORM_END);
			}
		
			curl_formadd(post, &last,
               CURLFORM_COPYNAME, "startTime",
               CURLFORM_COPYCONTENTS, sttimestamp.c_str(),
               CURLFORM_END);
			curl_formadd(post, &last,
               CURLFORM_COPYNAME, "endTime",
               CURLFORM_COPYCONTENTS, edtimestamp.c_str(),
               CURLFORM_END);
		}	  
	}
	else
	{//TS
		curl_formadd(post, &last,
	               CURLFORM_COPYNAME, "mcId",
	               CURLFORM_COPYCONTENTS, uid.c_str(),
	               CURLFORM_END);		
		if(!filename.empty())
		{
		    curl_formadd(post, &last,
		               CURLFORM_COPYNAME, "file",
		               CURLFORM_FILE, filename.c_str(),
		               CURLFORM_END);
		}

		if(!filename.empty())
		{
			curl_formadd(post, &last,
               CURLFORM_COPYNAME, "startTime",
               CURLFORM_COPYCONTENTS, sttimestamp.c_str(),
               CURLFORM_END);
			curl_formadd(post, &last,
               CURLFORM_COPYNAME, "endTime",
               CURLFORM_COPYCONTENTS, edtimestamp.c_str(),
               CURLFORM_END);
		}
	}
	
}


PATTERN_SINGLETON_IMPLEMENT(CTsUpload);


CTsUpload::CTsUpload():CThread("RecordTsUpLoad", TP_NET)
{
	_printd("CTsUpload");
	m_allfile_totalsize = 0;
	m_list_count = 0;
}

CTsUpload::~CTsUpload()
{
	_printd("~CTsUpload");
	
}



VD_INT32 CTsUpload::Start()
{

	char uuid[32] ={'\0'};
	m_iDebug = 0;
	NetGetDebug();
	
	GetUuid(uuid);
	
	setUID(uuid);

	m_iDebug = 2;
	if(2 == m_iDebug)
	{	//SIT测试环境
		//m_apiUrl = "http://camplatsit.suning.com/camplat-web";
		m_apiUrl = "http://camplatosssit.suning.com/camplat-oss";
	}
	else if(1 == m_iDebug)
	{   //PRE预生产环境
		m_apiUrl = "http://camplatosspre.cnsuning.com/camplat-oss";
	}
	else
	{	//PRD正式环境	
		m_apiUrl = "http://camplatoss.suning.com/camplat-oss";
	}
	m_tsurl = "/wja/uploadTs.do";
	m_warningurl = "/wja/uploadAlarm.do";

	_printd("api:%s", m_apiUrl.c_str());
	CreateThread();
	return 0;
}

VD_INT32 CTsUpload::Stop ()
{
	DestroyThread();
	return 0;
}
VD_BOOL CTsUpload::NetGetDebug()
{
	int Enable	= 0;
	FILE *ConfFp = NULL;
	int iRet =0;
	char NetDbgPath[1024];	
	snprintf(NetDbgPath,1024,"%s","/mnt/temp/DbgEnv");
	if(0 != access(NetDbgPath,0))
	{
		_printd("DbgEnv:%s is not exist! \n",NetDbgPath);
		return -1;
	}
	else
	{		
		ConfFp = fopen(NetDbgPath, "r");
		if (ConfFp == NULL) {
			_printd("open file %s Error", NetDbgPath);
			return -1;
		}
		iRet = fscanf(ConfFp, "%d",&Enable);
		_printd("read %s file enable = %d ",NetDbgPath,Enable);
		fclose(ConfFp); 	
	}
	m_iDebug = Enable;
	_printd("DEBUG VERSION %d!!!!!!!!!\n",Enable);
	return Enable;
}

VD_INT32 CTsUpload::sendRequest(int type, struct curl_slist *headerlist,
	struct curl_httppost *post,	string & result, long *statusCode)
{
    CURL *curl = NULL;
    CURLcode res = CURLE_OK;
    int opc = CURLE_OK;

    MemChunk chunk;
	string url;
    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
    chunk.size = 0; 

    curl = curl_easy_init();

	if(type)
	{
   		url = m_apiUrl + m_warningurl;
	}
	else
	{
		url = m_apiUrl + m_tsurl;
	}
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	_printd("url:%s",url.c_str());
    // This option is here to allow multi-threaded unix applications to still set/use
    // all timeout options etc, without risking getting signals.
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); 

    // set form-data to post
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
    curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

    /* send all data to this function  */ 
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);
    /* we pass our 'chunk' struct to the callback function */ 
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	// request options
	curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 60L);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 100L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 15L);
//  curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

#if VERB_LEV >= 1
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#if VERB_LEV >= 2
    curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, debug_trace);
#endif
#endif

    res = curl_easy_perform(curl);

    long httpcode = 0;
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &httpcode);
    if (res != CURLE_OK) 
	{
        fprintf(errStream, "curl_easy_perform() failed: (%d) %s\n", res, curl_easy_strerror(res));
        opc = res;//OPC_SENDFAILED;
    }
    else if (httpcode >= 200 && httpcode < 300)
	{
        opc = handleResponse(chunk.memory, chunk.size, result);
    }
    else 
	{
        opc = -1;
    }

    curl_easy_cleanup(curl);
    

    free(chunk.memory);

    *statusCode = httpcode;
    return opc;
}
	
VD_INT32 CTsUpload::handleResponse(const char * resp, size_t resp_len, string & result)
{
	_printd("Result:\n%s",resp);
	return CURLE_OK;
}


VD_INT32 CTsUpload::perform(int type, const string & filename, TImage *images,uint64 sttimestamp,
    uint64 edtimestamp,string & result, long *statusCode )
{
	int ret = 0;
	
	char starttimestamp[30] = {0};
	snprintf(starttimestamp,sizeof(starttimestamp), "%llu", sttimestamp*1000); //秒(s)转换成毫秒(ms)
	char endtimestamp[30] = {0};
	snprintf(endtimestamp,sizeof(endtimestamp), "%llu", edtimestamp*1000);//秒(s)转换成毫秒(ms)

	_printd("starttimestamp:%s", starttimestamp);
	_printd("edtimestamp:%s", endtimestamp);
	
	struct curl_slist *headerlist = NULL;
	compose_header(&headerlist);
	if (NULL == headerlist)
        return -1;
	
    struct curl_httppost *formpost = NULL;
    compose_form(&formpost, type,filename, images, m_uid, starttimestamp, endtimestamp);
	if (NULL == formpost)
        return -1;
	sendRequest(type, headerlist,formpost,result,statusCode);
	curl_formfree(formpost);	
	curl_slist_free_all (headerlist);
	return 0;
}
	
bool CTsUpload::SendTsToServer()
{
	int ret = 0;
	TS_FILE_INFO *pFileInfo = NULL;
	string filename;
	int uploadtype = 0;
	TImage *pImage = NULL;
	uint filesize = 0;
	uint sttimestamp = 0;
	uint edtimestamp = 0;
	long 	statusCode = 0;
	string		 result;
	if (m_list_count > 0) 
	{
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();	
		uploadtype = it->uploadtype;
		pFileInfo = it->pFileInfo;
		pImage = it->pImage;	
		if(pFileInfo)
		{
			filesize = pFileInfo->filesize;
			filename = pFileInfo->filename.c_str();
		}
		sttimestamp = it->sttimestamp;
		edtimestamp = it->edtimestamp;
		m_packet_list.pop_front();
		m_allfile_totalsize -= filesize;
		m_list_count-- ;
		m_mutex_list.Leave();
		
		for(int i = 0; i < 3; i++ ) //重传3次
		{
			if(perform(uploadtype, filename, pImage, sttimestamp, edtimestamp,result, &statusCode))
			{
				_printd("upload type:%d fail!!!",uploadtype);
				break;
			}	
			if(200 != statusCode )
			{
				_printd("upload type:%d fail!!!",uploadtype);
				sleep(1);
			}
			else
			{
				break;
			}
		} 
		
		if(uploadtype && pImage)
		{
			delete pImage;
		}

		if(pFileInfo)
		{
			pFileInfo->count-- ;
			if(0 == pFileInfo->count)
			{
				_printd("remove:%s", filename.c_str());
				//remove(filename.c_str());
				delete pFileInfo;
			}
		}
		return true;
	}
	
	return false;
}

void CTsUpload::OnFileList(PACKET_INFO &uploadinfo)
{
	uint limitsize = 0;
	limitsize = 15*1024*1024;
	
	if (m_allfile_totalsize > limitsize)
	{
		debugf("CTsUpload file list total size is too large discard it");
		//warning
		if(uploadinfo.pImage)
		{
			delete uploadinfo.pImage;
		}
		if(uploadinfo.pFileInfo)
		{
			delete uploadinfo.pFileInfo;
		}
		return ;
	}

	m_mutex_list.Enter();
	m_packet_list.push_back(uploadinfo);
	if(NULL != uploadinfo.pFileInfo)
	{
		m_allfile_totalsize += uploadinfo.pFileInfo->filesize ;
	}
	m_list_count ++;
	m_mutex_list.Leave();
}
void CTsUpload::OnFileList(const vector<PACKET_INFO> & uploadinfovect)
{	
//alarm
	m_mutex_list.Enter();
	int i = 0;
	do{
		const PACKET_INFO & uploadinfo = uploadinfovect[i];
		m_packet_list.push_back(uploadinfo);
		if(NULL != uploadinfo.pFileInfo)
		{
			m_allfile_totalsize += uploadinfo.pFileInfo->filesize ;
		}
		m_list_count ++;
	} while (++i < uploadinfovect.size());
	m_mutex_list.Leave();
}

void CTsUpload::ThreadProc()
{
	bool bSleep = true;
	uint count_sleep = 0;
	
	while (m_bLoop) {
		bSleep = true;
		if (SendTsToServer()) {
			bSleep = false;
		}
		//防止没有码流数据空跑,导致cpu利用率过高
		 if (bSleep) {
			count_sleep++;
			if (count_sleep > 0) 
			{
				SystemSleep(10);
				count_sleep = 0;
			}
		}
		else
		{
		    count_sleep = 0;
		}
	}
}
