#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Intervideo/HeMu/Openssl.h"

SSL_CTX* OpenSSL::m_ctx = NULL;

OpenSSL::OpenSSL()
{
	m_ssl = NULL;
	m_ibreak = 0;
}

OpenSSL::~OpenSSL()
{
    /* 关闭 SSL 连接 */
    if (m_ssl)
    {
        SSL_shutdown(m_ssl);
        /* 释放 SSL */
        SSL_free(m_ssl);
    }
    m_fd = -1;
	m_ssl = NULL;
}

VD_INT32 OpenSSL::Initctx()
{
#if 0
	SSL_library_init();	
	SSL_load_error_strings();	
	OpenSSL_add_all_digests();
	m_ctx = SSL_CTX_new(TLSv1_2_method());
	SSL_CTX_set_options(m_ctx, SSL_OP_ALL);
	SSL_CTX_set_default_verify_paths(m_ctx);
#else
	/* SSL 库初始化 */
    SSL_library_init();
    /* 载入所有 SSL 算法 */
    OpenSSL_add_all_algorithms();
    /* 载入所有 SSL 错误消息 */
    SSL_load_error_strings();
    /* 以 TSL1.2 标准兼容方式产生一个 SSL_CTX ，即 SSL Content Text */
    m_ctx = SSL_CTX_new(TLSv1_2_client_method());
#endif	
	

    if (m_ctx == NULL)
    {
        ERR_print_errors_fp(stdout);
        return FALSE;
    }
	SSL_CTX_set_timeout(m_ctx,60);
    return TRUE;
}
void OpenSSL::Close()
{
    /* 关闭 SSL 连接 */
    if (m_ssl)
    {
        SSL_shutdown(m_ssl);
        /* 释放 SSL */
        SSL_free(m_ssl);
		
    }
    m_fd = -1;
	m_ibreak = 0;
	m_ssl = NULL;
}

VD_INT32 OpenSSL::Connect(int fd)
{
	int ret = 0;
    /* 基于 ctx 产生一个新的 SSL */
    m_ssl = SSL_new(m_ctx);
    if (m_ssl == NULL)
    {
        ERR_print_errors_fp(stderr);
        return FALSE;
    }
    m_fd = fd;

    /* 将连接用户的 socket 加入到 SSL */
   // SSL_set_fd(m_ssl, m_fd);
	if (SSL_set_fd(m_ssl, m_fd) == 0) {
		_printd( "SSL_set_fd() failed");
		return FALSE;
	}

	SSL_set_connect_state(m_ssl);

	bool isContinue = true;
	uint   timecout = 0;
	int        n, sslerr;
	int   err;

	while(isContinue)	
	{		
		isContinue = false;	
		//_printd("SSL_connect!!!");
#if 1
		n = SSL_do_handshake(m_ssl);
		if (n == 1)
		{
			return TRUE;
		}
		sslerr = SSL_get_error(m_ssl, n);

		_printd("SSL_get_error: %d", sslerr);
		if (timecout++ > 300)
		{
			_printd("SSL connect fail time out!");
			break;
		}
		if ((sslerr == SSL_ERROR_WANT_READ) || (sslerr == SSL_ERROR_WANT_WRITE))
		{
			usleep(10 * 1000);
			isContinue = true;
			continue;
		}

		err = (sslerr == SSL_ERROR_SYSCALL) ? errno : 0;

		if (sslerr == SSL_ERROR_ZERO_RETURN || ERR_peek_error() == 0) {
			_printd("peer closed connection in SSL handshake");
			break;
		}

		_printd("SSL_do_handshake() failed");

	}
#else
		if(SSL_connect(m_ssl) == -1)	
		{  
			//_printd("SSL_connect 1 !!!");
			
			int icode = -1;	
		    int iret = SSL_get_error(m_ssl, icode);
			//_printd("SSL_connect 2!!!");
			if (((iret == SSL_ERROR_WANT_WRITE) || (iret == SSL_ERROR_WANT_READ)) 
				&& (timecout < 10000))
		   	{				
		   	 	isContinue = true;	
				timecout++;
				//_printd("wait!!!");
		    }
		    else
		    {
		    	Close();
				_printd("SSL connect fail!");
				return FALSE;
			}		
		}		
		else		
		{			
		 	_printd("SSL connect success!");
			/*break;	*/
			return TRUE;
		}	

	}
#endif

	Close();
	_printd("SSL connect fail!");
	return FALSE;
    //return TRUE;
}


VD_INT32 OpenSSL::Recv(unsigned char *data, int size)
{
    if (NULL == data || size <= 0 || NULL == m_ssl)
    {
        return -1;
    }
#if 0 
	int  recved = 0;
	uint timecount = 0;
    while(true)
    {

        recved = SSL_read(m_ssl, data, size);
        int left = SSL_pending(m_ssl);
        if (recved > 0)
        {
            int err = SSL_get_error(m_ssl,recved);
            return recved;
        }
        else
        {
            // TODO 小心陷入死循环
			if (timecount++ > size)
			{
				//防止死循环
				return -1;
			}

            if (recved < 0 &&  SSL_get_error(m_ssl,recved)  == SSL_ERROR_WANT_READ)
            {
                //_printd("got SSL_ERROR_WANT_READ");
				//usleep(10*1000);
                continue;
            }
            // recved=0 || no want read
            return recved;
        }


    }
#else
	int  n = 0, bytes = 0;
	int        sslerr;
	int  err;

	for (;;)
	{
		n = SSL_read(m_ssl, data, size);
		if (n > 0)
		{
			bytes += n;
			size -= n;

			if (size == 0) {
				return bytes;
			}

			data += n;
			continue;
		}
		sslerr = SSL_get_error(m_ssl, n);

		err = (sslerr == SSL_ERROR_SYSCALL) ? errno : 0;

		if ((sslerr == SSL_ERROR_WANT_READ) || (sslerr == SSL_ERROR_WANT_WRITE))
		{
			if (bytes)
			{
				return bytes;
			}
			else
			{
				return -2;
			}
		}

		if (sslerr == SSL_ERROR_ZERO_RETURN || ERR_peek_error() == 0)
		{
			_printd("peer shutdown SSL cleanly");
			return -3;
		}
		return -1;
	}
#endif
}

VD_INT32 OpenSSL::Send(unsigned char *data, int32_t size)
{
    if (NULL == data || size <= 0 || NULL == m_ssl)
    {
        return -1;
    }
 
    int32_t remainded = size;
    int32_t sended = 0;
#if 0 
   unsigned char* pszTmp = data;
   unsigned int timecount = 0;
   int ires = 0;

   while (remainded > 0)
   {

	   //TODO:检查此处的处理逻辑，是否会造成由于单个连接而拖累整个服务
	   sended = SSL_write(m_ssl, pszTmp, (size_t)remainded);
	   if (sended > 0)
	   {
		   pszTmp += sended;
		   remainded -= sended;
	   }
	   else // sended <= 0
	   {
		   //TODO
		   _printd("write errrrrrrrrrrrrrrrrrrrrrr");
		   if (m_ibreak || (timecount++ >= 1000))
		   {
			   //防止死循环				
			   m_ibreak = 0;
			   //break;
		   }
		   if (sended < 0 && SSL_get_error(m_ssl, sended) == SSL_ERROR_WANT_WRITE)
		   {
			   return 0;
		   }
		   //_printd("errno[%d]", errno);
		   if (-1 == sended && errno == EAGAIN)
		   {
			   return size;
		   }
		   else if (errno == EINTR || errno == EWOULDBLOCK)
		   {
			   //continue;
		   }
		   else
		   {
			   _printd(" send data(size:%d)sended[%d] error, msg = %s ", remainded, sended, strerror(errno));
			   return -2;
			   //return 0;

		   }

	   }
   }
   return (size - remainded);
#else
	int        n, sslerr;
	int  err;
	n = SSL_write(m_ssl, data, size);

	if (n > 0) {

		return n;
	}

	sslerr = SSL_get_error(m_ssl, n);

	err = (sslerr == SSL_ERROR_SYSCALL) ? errno : 0;

	if ((sslerr == SSL_ERROR_WANT_WRITE) || (sslerr == SSL_ERROR_WANT_READ))
	{		
		return -1;
	}
	//_printd("SSL_get_error: %d", sslerr);
	return -2;
#endif 
    
   
}


