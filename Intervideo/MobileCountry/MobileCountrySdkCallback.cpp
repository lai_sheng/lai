#include "Intervideo/MobileCountry/MobileCountrySdkCallback.h"
#include "Intervideo/MobileCountry/MobileCountrykeeplive.h"
#include "Intervideo/MobileCountry/MobileCountryAppCmd.h"

#include "APIs/Audio.h"
#include "Functions/WifiLink.h"
#include "Devices/DevInfo.h"
#include "Functions/PtzTrace.h"
#include "Functions/FFFile.h"
#include "Intervideo/MobileCountry/md5sum.h"
#include "Functions/LedStat.h"
#include "Devices/DevExternal.h"

#include "Intervideo/MobileCountry/camerasdk/dot_api.h"
#include "Intervideo/MobileCountry/camerasdk/dot_error.h"

#define  UPGRADE_FILE   "/tmp/upgrade_file.bin"
#define  UPGRADE_INFO   "/mnt/temp/wjaUpgradeUrl"

//是否获取到二维码
int g_get_qrcode = 0;

#ifdef __cplusplus
extern "C" {
#endif

static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
	int len = size * nmemb;
	int written = len;
	FILE *fp = NULL;
	if (access((char*)stream, 0) == -1) {
		fp = fopen((char*)stream, "wb");
	}
	else {
		fp = fopen((char*)stream, "ab");
	}
	if (fp)
	{
		fwrite(ptr, nmemb, size, fp);
		fclose(fp);
	}
	_printd("download size[%dkb]", len / 1024);
	return written;
}
	
int writeUpgradeInfo(const char *upgradeUrl, const char *md5, int fileLenght)
{
	FILE *fp = NULL;
	char upgradeInfo[1024] = { 0 };
	fp = fopen(UPGRADE_INFO, "wb");
	if (fp)
	{
		sprintf(upgradeInfo, "%s %s %d", upgradeUrl, md5, fileLenght);
		fwrite(upgradeInfo, 1, strlen(upgradeInfo), fp);
		fclose(fp);
	}
	// printf("download size[%dkb]\n",len/1024);      
	return 0;
}

//休眠函数
static void OnInactive()
{
    DoT_KeepLiveInfo_st liveinfo;
    memset(&liveinfo, 0, sizeof(DoT_KeepLiveInfo_st));
    
    //从SDK获取保活信息
    if(DOT_EC_SUCCESS == DoT_GetKeepLiveInfo(&liveinfo)) {
        //进入保活流程
        StartKeepLive(&liveinfo);
    }

    //停止推流
//    StopPushAVData();

    //停掉主程序后，断电；或者直接断电
    g_MobileCountrySdkApi.StopSDK();
}

int QRCode_OriData_Handle(char *pdata, int length)
{
	g_WifiLink.OnPacket(pdata, length);
	_printd("length[%d]",length);
	g_get_qrcode = 1;

	return 0;
}

//配置命令回调函数
int cbSetDevConfig(DoT_SetDevConfigCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data)
{
	int ret = DOT_EC_SUCCESS;
	switch (cmd)
	{ 
	case DOT_CMD_SET_LED_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_LED_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_NIGHTVISIGON_MODE:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_NIGHTVISIGON_MODE\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_ANTIFLICKER_VALUE:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_ANTIFLICKER_VALUE\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_VIDEO_ROTATE_VALUE:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_VIDEO_ROTATE_VALUE\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_VIDEO_QUALITY_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_VIDEO_QUALITY_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_DEVICE_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_DEVICE_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_FULL_COLOR_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_FULL_COLOR_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_MOTION_DETECTION_SENSITIVITY:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_MOTION_DETECTION_SENSITIVITY\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_MOTION_DETECTION_REGION:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_MOTION_DETECTION_REGION\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_SOUND_DETECTION_SENSITIVITY:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_SOUND_DETECTION_SENSITIVITY\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_PIR_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_PIR_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_PIR_SENSITIVITY:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_PIR_SENSITIVITY\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_PEOPLE_DETECTION_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_PEOPLE_DETECTION_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_TAMPER_ALARM_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_TAMPER_ALARM_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_MIC_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_MIC_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_SPEAKER_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_SPEAKER_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_SPEAKER_VOLUME:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_SPEAKER_VOLUME\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_DOORBELL_VOLUME:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_DOORBELL_VOLUME\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	case DOT_CMD_SET_MOTION_TRACKING_STATUS:
	{
		printf("%s line(%d) cmd(%d)=DOT_CMD_SET_MOTION_TRACKING_STATUS\n", __FUNCTION__, __LINE__, cmd);
		break;
	}
	default:
		ret = DOT_EC_UNSUPPORT;
		break;
	}

    return ret;
}

//设备运行信息
int cbGetDevRunningInfo(DoT_GetDevRunningInfo_en info_e, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (info_e)
    {
    case DOT_CMD_GET_WIFI_SIGNAL_QUANLITY:
        printf("%s line(%d) cmd(%d)=DOT_CMD_GET_WIFI_SIGNAL_QUANLITY\n", __FUNCTION__, __LINE__, info_e);
        break;
    case DOT_CMD_GET_BATTERY_INFO:
    {
    	int *value = (int *)data;
    	ZRT_CAM_BAT_Status Status;
	memset(&Status, 0, sizeof(Status));
	if(0 != g_DevExternal.Dev_GetElecQuantity(&Status))
		return -1;

	if (Status.chargingStatus == BAT_DISCHARGING)
	{
		*value = (DoT_int32)DOT_BATTERY_STATUS_NORMAL;
	}
	else if (Status.chargingStatus == BAT_CHARGING)
	{
		*value = (DoT_int32)DOT_BATTERY_STATUS_CHARGING;
	}
	else if (Status.chargingStatus == BAT_FULL)
	{
		*value = (DoT_int32)DOT_BATTERY_STATUS_FULL;
	}
	printf("%s line(%d) cmd(%d)=DOT_CMD_GET_BATTERY_INFO=%d\n", __FUNCTION__, __LINE__, info_e, *value);
        break;
    }
    case DOT_CMD_GET_BATTERY_PERCENT:
    {
    	int *value = (int *)data;
        ZRT_CAM_BAT_Status Status;
	memset(&Status, 0, sizeof(Status));
	if(0 != g_DevExternal.Dev_GetElecQuantity(&Status))
		return -1;
	
        *value = Status.capacity;
        printf("%s line(%d) cmd(%d)=DOT_CMD_GET_BATTERY_PERCENT=%d\n", __FUNCTION__, __LINE__, info_e, *value);
        break;
    }
    case DOT_CMD_GET_WIFI_LIST:
    {
        DoT_WifiList_st* wifi_list = (DoT_WifiList_st*)data;

        wifi_list->wifi_count = 2;
        wifi_list->wifi_list = (DoT_WifiInfo_st *)malloc(wifi_list->wifi_count * sizeof(DoT_WifiInfo_st));

        DoT_WifiInfo_st* wifi_data = (DoT_WifiInfo_st*)wifi_list->wifi_list;

        strcpy(wifi_data[0].bssid, "AA:12:34:56:78:9A");
        strcpy(wifi_data[0].ssid, "wifi_1");
        strcpy(wifi_data[0].password, "passwd_1");
        wifi_data[0].signal = 5;
        wifi_data[0].encrypt_mode = DoT_WIFI_MODE_WPA_WPA2;

        strcpy(wifi_data[1].bssid, "AA:12:34:56:78:9B");
        strcpy(wifi_data[1].ssid, "wifi_2");
        strcpy(wifi_data[1].password, "passwd_2");
        wifi_data[1].signal = 5;
        wifi_data[1].encrypt_mode = DoT_WIFI_MODE_WEP;

        printf("%s line(%d) cmd(%d)=DOT_CMD_GET_WIFI_LIST=%d\n", __FUNCTION__, __LINE__, info_e);
        break;
    }
    case DOT_CMD_GET_CURRENT_WIFI:
    {
        DoT_WifiInfo_st* wifi_data = (DoT_WifiInfo_st*)data;

	strcpy(wifi_data->bssid, "8a2593b3cf8b");
	GetConnWifi(wifi_data->ssid, 64);
	GetConnWifiPwd(wifi_data->password, 64);
	wifi_data->signal = 5;
	wifi_data->encrypt_mode = DoT_WIFI_MODE_WPA_WPA2;
	
//	memcpy(data, wifi_data, sizeof(cc_wifi));
//	free(wifi_data);
//	wifi_data = NULL;

        printf("%s line(%d) cmd(%d)=DOT_CMD_GET_CURRENT_WIFI\n", __FUNCTION__, __LINE__, info_e);
        break;
    }    
    case DOT_CMD_GET_STORAGE_PARTITION:
    {
        DoT_StorageSize_st* storageinfo = (DoT_StorageSize_st*)data;
        
        if (storageinfo) {
	#if 0
	    int i = 0;
	    int64 nTotalmem = 0, nRemainmem = 0;

	    file_sys_get_cap_bit(&nTotalmem, &nRemainmem);	
            storageinfo->total_size = nTotalmem;
            storageinfo->free_size  = nRemainmem;
            storageinfo->storage_type = DOT_STORAGE_TYPE_TFCARD;
            strcpy(storageinfo->file_system, "fat32");
	#endif
	    printf("%s line(%d) cmd(%d)=DOT_CMD_GET_STORAGE_PARTITION\n", __FUNCTION__, __LINE__, info_e);
            OUTPUT_LOG("free size:%lld, total size:%lld\n",storageinfo->free_size, storageinfo->total_size);
        }
        break;
    }
    case DOT_CMD_GET_MEDIA_ATTR:
    {
        DoT_StreamMediaType_st* streammediatype = (DoT_StreamMediaType_st*)data;

        if (streammediatype) {
            streammediatype->main_video_type = DOT_VIDEO_TYPE_H264;
            streammediatype->sub_video_type = DOT_VIDEO_TYPE_END;
            streammediatype->main_audio_type = DOT_AUDIO_TYPE_G711A;
            streammediatype->sub_audio_type = DOT_AUDIO_TYPE_END;
	    printf("%s line(%d) cmd(%d)=DOT_CMD_GET_MEDIA_ATTR\n", __FUNCTION__, __LINE__, info_e);
            OUTPUT_LOG("mainvideo:%d subvideo:%d, mainaudio:%d, subaudio:%d\n", streammediatype->main_video_type,
                streammediatype->sub_video_type, streammediatype->main_audio_type, streammediatype->sub_audio_type);
        }
        break;
    }
    default:
        ret = DOT_EC_UNSUPPORT;
        break;
    }
    
    return ret;
}

//设备控制回调
int cbControlDevCmd(DoT_ControlDevCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (cmd)
    {
    case DOT_CMD_CHANGE_WIFI:
    {
    	DoT_WifiInfo_st* wifi_data = (DoT_WifiInfo_st*)data;
    	OUTPUT_LOG("to connect wifi ssid %s, password %s, encrypt %d\n", 
            	wifi_data->ssid, wifi_data->password, wifi_data->encrypt_mode);
        if(FALSE == CheckSsid(wifi_data->ssid, strlen(wifi_data->ssid)))
		return 1;
	
	Store_Cfgfile(STORE_WIFI_CONFIG,wifi_data->ssid, wifi_data->password);
	printf("%s line(%d) cmd(%d)=DOT_CMD_CHANGE_WIFI\n", __FUNCTION__, __LINE__, cmd);
        break;
    }
    case DOT_CMD_SYNC_SYSTEM_TIME:
    {
        DoT_uint64 tnow = *(DoT_uint64*)data;
        OUTPUT_LOG("to set system time, utc time is %lld(ms)\n", tnow);

        struct timeval tv_set;

        tv_set.tv_sec  = tnow / 1000;//sec
        tv_set.tv_usec = tnow % 1000 * 1000;//usec
        if(settimeofday(&tv_set, NULL) < 0) {
            printf("settimeofday error.\n");
        }
	printf("%s line(%d) cmd(%d)=DOT_CMD_SYNC_SYSTEM_TIME\n", __FUNCTION__, __LINE__, cmd);
        break;
    }
    case DOT_CMD_REBOOT_DEVICE:
    {
    	int RebootFlag = 0;
	g_Cmos.Write(CMOS_REBOOTFLAG, &RebootFlag, 4);
	printf("reboot -f \n\n\n");
	char ReStartStion[150] = {0};
	sprintf(ReStartStion, "reboot");
	printf("%s\n", ReStartStion);
	execl("/bin/sh", "sh", "-c", ReStartStion, (char*)0);
	printf("%s line(%d) cmd(%d)=DOT_CMD_REBOOT_DEVICE\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("to reboot\n");
        break;
    }
    case DOT_CMD_FORMAT_LOCAL_STORAGE: 	
    {
	printf("%s line(%d) cmd(%d)=DOT_CMD_FORMAT_LOCAL_STORAGE\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("to format local storage\n");
        break;
    }
    case DOT_CMD_GET_LOGS:
    {
	strcpy((char*)data, "/mnt/temp/device.log");
	printf("%s line(%d) cmd(%d)=DOT_CMD_GET_LOGS\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("to get logs %s\n", (char*)data);
        break;
    }
    case DOT_CMD_START_DEFENSE_ALARM:
    {
        Dot_Strategy_Action* action = (Dot_Strategy_Action*)data;
        OUTPUT_LOG("action type:%d, spkdur:%d, litmode:%d, litdur:%d, buzdur:%d\n",
                action->type, action->spkdur, 
                action->litmode, action->litdur, action->buzdur);
       
	printf("%s line(%d) cmd(%d)=DOT_CMD_START_DEFENSE_ALARM\n", __FUNCTION__, __LINE__, cmd);
        break;
    }
    case DOT_CMD_STOP_TAMPER_ALARM:
    {
	printf("%s line(%d) cmd(%d)=DOT_CMD_STOP_TAMPER_ALARM\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("stop tamper alarm value is %d\n", *(DoT_int32*)data);
        break;
    }
    default:
    {
	ret = DOT_EC_UNSUPPORT;
        break;
    }
    
    }

    return ret;
}

//音频播放回调
int cbAudioPlayCmd(DoT_AudioPlayCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (cmd)
    {
    case DOT_CMD_AUDIO_PLAY_START:
	printf("%s line(%d) cmd(%d)=DOT_CMD_AUDIO_PLAY_START\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("audio play start\n");
        break;
    case DOT_CMD_AUDIO_PLAY_STOP:
        printf("%s line(%d) cmd(%d)=DOT_CMD_AUDIO_PLAY_STOP\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("audio play stop\n");
        break;
    case DOT_AUDIO_PLAY_DATA:
    {
        DoT_AudioInfo_st* audiodata = (DoT_AudioInfo_st*)data;
        OUTPUT_LOG("audio data len:%d, encode:%d, sample:%d, sample_bit:%d\n",
                    audiodata->data_size, audiodata->media_type, audiodata->sample_rate, audiodata->sample_bit);
	
	if (0 == audiodata->data_size) {
            OUTPUT_LOG("audio play stopped\n");
        }
	else
	{
		g_MobileCountrySdkApi.AppendTalkAudioData((unsigned char*)audiodata->data, audiodata->data_size);
	}
        break;
    }
    default:
        ret = DOT_EC_UNSUPPORT;
        break;
    }

    return ret;
}

//PTZ命令回调
int cbPTZControl(DoT_PTZControl_en cmd, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (cmd)
    {
    case DOT_CMD_PTZ_GET_POSITION:
    {
        DoT_PtzSpace_st* ptz_pos = (DoT_PtzSpace_st*)data;
        ptz_pos->pan = 0.5;
        ptz_pos->tilt = 0.5;
        OUTPUT_LOG("ptz get position [%f %f]\n", ptz_pos->pan, ptz_pos->tilt);
        break;
    }
    case DOT_CMD_PTZ_MOVE_TO_POSITION:
    {
        //坐标值表示需要移动的点的绝对位置
        DoT_PtzSpace_st* ptz_pos = (DoT_PtzSpace_st*)data;    
        OUTPUT_LOG("ptz move to position [%f %f]\n", 
            ptz_pos->pan, ptz_pos->tilt);
        break;
    }
    case DOT_CMD_PTZ_GOTO_HOME:
        OUTPUT_LOG("ptz goto home\n");
        break;
    case DOT_CMD_PTZ_MOVE_BY_STEP:
    {
        DoT_PTZMoveDirection_en direction = *(DoT_PTZMoveDirection_en*)data;
        OUTPUT_LOG("ptz move by step, direction:%d\n", direction);
        break;
    }
    case DOT_CMD_PTZ_START_MOVE:
    {
        DoT_PTZMoveDirection_en direction = *(DoT_PTZMoveDirection_en*)data;
        OUTPUT_LOG("start ptz move, direction:%d\n", direction);
        break;
    }
    case DOT_CMD_PTZ_STOP_MOVE:
        OUTPUT_LOG("ptz stop move\n");
        break;
    default:
        ret = DOT_EC_UNSUPPORT;
        break;
    }

    return ret;
}

//服务状态回调
int cbServerStatus(DoT_ServerStatusMsg_en msg, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (msg)
    {
    case DOT_MSG_PLAY_STATUS:
	printf("%s line(%d) cmd(%d)=DOT_MSG_PLAY_STATUS\n", __FUNCTION__, __LINE__, msg);
        OUTPUT_LOG("play status is %d\n", *(DoT_int32*)data);
        break;
    case DOT_MSG_DEV_INACTIVE:  
        //建议进入休眠，设备收到此回调时，需要结合当前设备的状态判断是否需要进入休眠，
        //比如在升级过程中是不能休眠的

        OUTPUT_LOG("dev inactive\n");
        //OnInactive();

	printf("%s line(%d) cmd(%d)=DOT_MSG_DEV_INACTIVE\n", __FUNCTION__, __LINE__, msg);
        break;
    case DOT_MSG_STREAM_RATELIMIT_STATUS:
	printf("%s line(%d) cmd(%d)=DOT_MSG_STREAM_RATELIMIT_STATUS\n", __FUNCTION__, __LINE__, msg);
        OUTPUT_LOG("stream rate limit status is %d\n", *(DoT_int32*)data);
        break;
    case DOT_MSG_SERVER_ONLINE:
	printf("%s line(%d) cmd(%d)=DOT_MSG_SERVER_ONLINE\n", __FUNCTION__, __LINE__, msg);
        OUTPUT_LOG("server online\n");
        break;
    case DOT_MSG_SERVER_OFFLINE:
	printf("%s line(%d) cmd(%d)=DOT_MSG_SERVER_OFFLINE\n", __FUNCTION__, __LINE__, msg);
        OUTPUT_LOG("server offline\n");
        break;
    case DOT_MSG_SERVER_UNBIND:
    	g_WifiLink.ResetAllCfg();
    	printf("%s line(%d) cmd(%d)=DOT_MSG_SERVER_UNBIND\n", __FUNCTION__, __LINE__, msg);
        OUTPUT_LOG("device unbind\n");
        break;
    default:
        break;
    }

    return ret;
}

//在线升级回调
int cbUpgrade(DoT_UpgradeCmd_en cmd, DoT_uint32 channel_id, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    static DoT_uint64 pkg_size = 0;
    static char md5sum[128] = {0};
    switch (cmd)
    {
    case DOT_CMD_UPGRADE_PKG_INFO:
    {
        DoT_UpgradePackageInfo_st* pkg_info = (DoT_UpgradePackageInfo_st*)data;
    	printf("%s line(%d) cmd(%d)=DOT_CMD_UPGRADE_PKG_INFO\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("upgrade package size %lld, md5 %s  url %s\n", pkg_info->pkg_size, pkg_info->md5, pkg_info->url);
	pkg_size = 0;
	memset(md5sum, 0, sizeof(md5sum));
	strcpy(md5sum, pkg_info->md5);
	writeUpgradeInfo((const char*)pkg_info->url, (const char*)pkg_info->md5, pkg_info->pkg_size);
	remove(UPGRADE_FILE);
	break;
    }
    case DOT_CMD_UPGRADE_DOWNLOAD:
    {
        DoT_DataInfo_st* pkgdata = (DoT_DataInfo_st*)data;
    	printf("%s line(%d) cmd(%d)=DOT_CMD_UPGRADE_DOWNLOAD\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("package data len %d\n", pkgdata->data_size);
	pkg_size += pkgdata->data_size;
	writefile_callback(pkgdata->data, 1, pkgdata->data_size, (void *)UPGRADE_FILE);
        break;
    }
    case DOT_CMD_UPGRADE_DOWNLOAD_FINISH:
    {
	printf("%s line(%d) cmd(%d)=DOT_CMD_UPGRADE_DOWNLOAD_FINISH\n", __FUNCTION__, __LINE__, cmd);
        break;
    }
    case DOT_CMD_UPGRADE_DOWNLOAD_FAIL:
    {
	printf("%s line(%d) cmd(%d)=DOT_CMD_UPGRADE_DOWNLOAD_FAIL\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("package download failed\n");
        break;
    }
    case DOT_CMD_UPGRADE_INSTALL:
    {
	printf("%s line(%d) cmd(%d)=DOT_CMD_UPGRADE_INSTALL\n", __FUNCTION__, __LINE__, cmd);
        OUTPUT_LOG("package install\n");
    	char UpgradeFilePath[150] = { 0 };
	char strMd5sum[64] = {0};
	GetMd5Value(UPGRADE_FILE, strMd5sum);
	pkg_size = 0;
	if (0 == strcmp(strMd5sum, md5sum))
	{
		_printd("MD5 OK OK OK OK !!!!!!!!!!!!!!!!!!!!!!!!!");
		sprintf(UpgradeFilePath, "/app/upgradeTool  %s", UPGRADE_FILE);
		printf("%s\n", UpgradeFilePath);
		execl("/bin/sh", "sh", "-c", UpgradeFilePath, (char*)0);
		printf("%s line(%d) cmd(%d)=CC_CMD_UPGRADE_EXECUTE, download size = %lld\n", __FUNCTION__, __LINE__, cmd, pkg_size);
	}
	else
	{
		remove(UPGRADE_FILE);
		return -1;
	}
        break;
    }
    default:
        break;
    }

    return ret;
}

static void OnBindFailed(DoT_DeviceBindInfo_st* bindInfo)
{
    int error = bindInfo->error_code;
    OUTPUT_LOG("bind failed, error %d\n", error);
    if (DOT_EC_WIFI_CONNECT_ERROR == error) {
        OUTPUT_LOG("connect wifi failed\n");
    } else if (DOT_EC_BIND_INFO_ERROR == error) {
        OUTPUT_LOG("bind info error\n");
    } else if (DOT_EC_TIMEOUT == error) {
        OUTPUT_LOG("bind request timeout\n");
    } else if (DOT_EC_BIND_ID_NOTINITED == error) {
        OUTPUT_LOG("bind id not inited\n");
    } else if (DOT_EC_BIND_ID_USED == error) {
        OUTPUT_LOG("bind id not used\n");
    } else if (DOT_EC_BIND_PASSWORD_WRONG == error) {
        OUTPUT_LOG("bind password wrong\n");
    } else if (DOT_EC_BIND_DEVICE_MODEL_ERROR == error) {
        OUTPUT_LOG("bind device model id invalid\n");
    } else if (DOT_EC_BIND_USER_INFO_ERROR == error) {
        OUTPUT_LOG("bind no user info\n");
    } else if (DOT_EC_BIND_OTHER_FAILURE == error) {
        OUTPUT_LOG("other reason\n");
    }
}


//绑定状态回调
int cbDevBindStatus(DoT_BindStatus_en status, void* data, void* user_data)
{
    int ret = DOT_EC_SUCCESS;
    switch (status)
    {
    case DOT_DEVICE_BIND_REQUEST_INFO_START:
    {
	printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_REQUEST_INFO_START\n", __FUNCTION__, __LINE__, status);
        OUTPUT_LOG("bind request info start\n");
        //启动二维码扫描模块或者AP热点，详情参看头文件注释说明
        break;
    }
    case DOT_DEVICE_BIND_REQUEST_QRCODE_INFO:     ///< 包括结束符最长512字节
    {
        if (g_get_qrcode) {
	    int nDataLen = 0;
	    void* pQrcodeData = g_WifiLink.PopPacket(&nDataLen);
	    if (pQrcodeData)
	    {
		memcpy((char *)data ,pQrcodeData, nDataLen);
       	        OUTPUT_LOG("bind request qrcode info %s\n", data);
		free(pQrcodeData);
		pQrcodeData = NULL;
	    }
	    else
	    {
	  	_printd("Get QRcode Result:Unidentified !!! =========>");
	    }
        } else {
            static int idx = 0;
            if (idx++ % 5 == 0) {
                OUTPUT_LOG("in bind proc to get qrcode!\n");
            }
            ret = DOT_EC_NO_QRCODE;  //尚未解析到二维码的时候，返回错误
        }
        g_get_qrcode = 0;
        break;
    }
    case DOT_DEVICE_BIND_REQUEST_INFO_STOP:
    {
	 printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_REQUEST_INFO_STOP\n", __FUNCTION__, __LINE__, status);
        OUTPUT_LOG("bind request info stop\n");
        //收到绑定信息，停止二维码扫描模块，或者退出AP热点模式
        break;
    }
    case DOT_DEVICE_BIND_CONNECT_WIFI:
    {
  	printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_CONNECT_WIFI\n", __FUNCTION__, __LINE__, status);
	DoT_WifiInfo_st* wifi_data = (DoT_WifiInfo_st*)data;
    	OUTPUT_LOG("to connect wifi ssid %s, password %s, encrypt %d\n", 
            	wifi_data->ssid, wifi_data->password, wifi_data->encrypt_mode);
        if(FALSE == CheckSsid(wifi_data->ssid, strlen(wifi_data->ssid)))
		return 1;
	
	Store_Cfgfile(STORE_WIFI_CONFIG,wifi_data->ssid, wifi_data->password);
        break;
    }
    case DOT_DEVICE_BIND_SUCCEED:
    {
	printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_SUCCEED\n", __FUNCTION__, __LINE__, status);
        OUTPUT_LOG("bind succeed, username:%s\n", ((DoT_DeviceBindInfo_st*)data)->username);
        g_MobileCountrySdkApi.StopBind();
        break;
    }
    case DOT_DEVICE_BIND_FAILED:
    {
	printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_FAILED\n", __FUNCTION__, __LINE__, status);
        OnBindFailed((DoT_DeviceBindInfo_st*)data);
        g_MobileCountrySdkApi.StopBind();
        break;
    }
    case DOT_DEVICE_BIND_WIRED_CONNECT_STATUS:
    {
	printf("%s line(%d) cmd(%d)=DOT_DEVICE_BIND_WIRED_CONNECT_STATUS\n", __FUNCTION__, __LINE__, status);
        //TODO:
        break;
    }
    default:
        ret = DOT_EC_UNSUPPORT;
        break;
    }

    return ret;
}

#ifdef __cplusplus
}
#endif
