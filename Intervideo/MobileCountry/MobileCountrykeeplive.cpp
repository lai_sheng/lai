#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include "Intervideo/MobileCountry/MobileCountrykeeplive.h"
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"

wakeup_cb g_cb = NULL;
int g_keep_live_loop = 0;
DoT_KeepLiveInfo_st g_liveinfo = {0};
int g_sock = -1;

static int ConnectKeepLiveServer()
{
    struct sockaddr_in servaddr;
    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family= AF_INET;
    servaddr.sin_port = htons(g_liveinfo.server_address[0].server_port);
    if(inet_pton(AF_INET, g_liveinfo.server_address[0].server_ip, &servaddr.sin_addr) <= 0)
    {
        OUTPUT_LOG("inet_pton failed.\n");
        return -1;
    }

    g_sock = socket(AF_INET,SOCK_STREAM,0);

    if(connect(g_sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
    {
        OUTPUT_LOG("connect failed.\n");
        return -1;
    }
    OUTPUT_LOG("connect %s:%d successfully!\n", 
        g_liveinfo.server_address[0].server_ip, g_liveinfo.server_address[0].server_port);
    return 0;
}

static int SendHeartBeatPkt()
{
    static uint64_t utc_now = 0;
    if (utc_now == 0 || g_MobileCountrySdkApi.getNowMs()/1000 - utc_now >= g_liveinfo.heartbeat_interval * 1000) {
        utc_now = g_MobileCountrySdkApi.getNowMs()/1000;
        OUTPUT_LOG("need to send heartbeat len %d, interval %d.\n", 
            g_liveinfo.heartbeat_pkt_len, g_liveinfo.heartbeat_interval);    //每过一段时间需要发送保活心跳包
        if (write(g_sock, g_liveinfo.heartbeat_pkt, g_liveinfo.heartbeat_pkt_len)
            != g_liveinfo.heartbeat_pkt_len) {
            OUTPUT_LOG("write error\n");
            return -1;
        }
    }
    return 0;
}

static int KeepLiveServerProc()
{
    int ret = 0;
    char databuf[64] = {0};
    OUTPUT_LOG("in keep live server proc.\n");

    while (g_keep_live_loop) {
        struct timeval tval;
        tval.tv_sec = 1;
        tval.tv_usec = 0;

        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(g_sock, &readset);
        
        ret = SendHeartBeatPkt();
        if (ret != 0) {
            return -1;
        }
        
        ret = select(g_sock+1, &readset, NULL, NULL, &tval);
        if(ret == -1) {
            OUTPUT_LOG("select error\n");
            return -1;
        }
        else if(ret == 0) {
            OUTPUT_LOG("now keeping live\n");
            continue;
        }

        if(FD_ISSET(g_sock, &readset)) {
            int readlen = 0;
_RetryRead_:
            readlen = read(g_sock, databuf, g_liveinfo.wakeup_pkt_len);
            if (readlen > 0) {
                OUTPUT_LOG("read %dB from remote.\n", readlen);
                //比较收到的报文
                if (readlen == g_liveinfo.wakeup_pkt_len 
                	&& memcmp(databuf, g_liveinfo.wakeup_pkt, g_liveinfo.wakeup_pkt_len) == 0) {
                    OUTPUT_LOG("it is wakeup package\n", databuf);     //唤醒包
                    return 0;
                } else {
                    OUTPUT_LOG("it is other: %s\n", databuf);
                }
            } else if (readlen == 0) {
                OUTPUT_LOG("read zero.\n");
                return -1;
            } else {
                if (EINTR == errno) {
                    goto _RetryRead_;
                } else if (EWOULDBLOCK == errno || EAGAIN == errno) {
                    usleep(200);
                    continue;
                } else {
                    OUTPUT_LOG("socket read error=%d.\n", errno);
                    return -1;
                }
            }
        }        
    }
    
    return -1;
}

static void *KeepLiveProc(void* args)
{
    if (g_sock != -1) {
        close(g_sock);
        g_sock = -1;
    }

    while (g_keep_live_loop) {
        if (ConnectKeepLiveServer() == 0 && KeepLiveServerProc() == 0) {
            OUTPUT_LOG("remote wakeup\n");
            if (g_cb) {
                g_cb(DOT_WAKE_UP_BY_REMOTE);//模拟低功耗设备远程唤醒后上电和启动主程序流程
            }
            break;
        }
        if (g_sock != -1) {
            close(g_sock);
            g_sock = -1;
        }
        sleep(1);
    }
    OUTPUT_LOG("leave keep live\n");
    if (g_sock != -1) {
        close(g_sock);
        g_sock = -1;
    }
    return NULL;
}

int StartKeepLive(DoT_KeepLiveInfo_st* liveinfo)
{
    g_keep_live_loop = 1;
    memcpy(&g_liveinfo, liveinfo, sizeof(DoT_KeepLiveInfo_st));
    int i=0;
    //遍历保活服务器，如果保活服务器不止一个，则设备要和每个服务器都建立TCP保活连接
    for(;i<g_liveinfo.server_count;i++) {
        OUTPUT_LOG("KeepLiveServer ip:%s port:%d\n",g_liveinfo.server_address[i].server_ip, g_liveinfo.server_address[i].server_port);

        pthread_t pid;
        pthread_create(&pid, NULL, KeepLiveProc, NULL);
        pthread_detach(pid);

        //这里仅仅连接第一个保活服务器，其它类似
        break;
    }

    return 0;
}

void StopKeepLive()
{
    g_keep_live_loop = 0;
}

void SetWakeupCb(wakeup_cb cb)
{
    g_cb = cb;
}
