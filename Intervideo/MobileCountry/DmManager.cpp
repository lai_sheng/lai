#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "Intervideo/HeMu/lwm2msdk.h"
#include "Intervideo/HeMu/DmManager.h"
/////////////此类用于对接移动DM系统//////////////////
static int s_quit = 0;
static int g_iDMstate = 0;



void  myNotifyMsg(OptNotifyParam *optNotifyParam)
{
	if (optNotifyParam == NULL)
		return;

	_printd("myNotifyMsg:type=%0x,code=%d,msg=%s\n", optNotifyParam->notify_type, optNotifyParam->notify_code, optNotifyParam->notify_msg);
	switch (optNotifyParam->notify_type)
	{
	case NOTIFY_TYPE_SYS:
		_printd("===>>>>1111===NOTIFY_TYPE_SYS");
		break;
	case NOTIFY_TYPE_REGISTER:
		if (optNotifyParam->notify_code == NOTIFY_CODE_OK)
		{
			g_iDMstate = 0;
			_printd("===>>>>2222==NOTIFY_TYPE_REGISTER ok\n");
		}
		else
		{
			g_iDMstate = 1;
			_printd("===>>>>2222===NOTIFY_TYPE_REGISTER fail\n");
		}
		break;
	case NOTIFY_TYPE_DEREGISTER:
		_printd("===>>>>3333===NOTIFY_TYPE_DEREGISTER\n");
		g_iDMstate = 3;
		break;
	case NOTIFY_TYPE_REG_UPDATE:
		if (optNotifyParam->notify_code == NOTIFY_CODE_OK)
		{
			g_iDMstate = 0;
			_printd("===>>>>4444===NOTIFY_TYPE_REG_UPDATE ok\n");
		}
		else
		{
			g_iDMstate = 2;
			_printd("===>>>>4444===NOTIFY_TYPE_REG_UPDATE fail\n");
		}
		break;
	default:
		break;
	}

}
int myDMReadInfo(int resId,char **outbuff)
{
    int iret = 0;
    *outbuff = NULL;
///////////////////////////////////////

    char buff[512];
    int  buflen = sizeof(buff);
    memset(buff,0,sizeof(buff));
    //字段赋值参看 多形态终端上报接口规范V2.0.3(多形态二期).docx
    //不支持:unSupport
    //没有值:unKnown
    snprintf(buff,buflen,"%s","unknown");//默认无法取值
    
    switch (resId)
    {
    case 6601://devinfo
        snprintf(buff,buflen,"%s","camera");  
        break;
    case 6602://appinfo
        snprintf(buff,buflen,"app_name1|pak_name1|1234000|3\r\napp_name2|pak_name2|2234000|5\r\n");  
        break;
    case 6603://mac
        snprintf(buff,buflen,"00:50:56:C0:00:01");
        break;
    case 6604://rom
        snprintf(buff,buflen,"16M");
        break;
    case 6605://ram
        snprintf(buff,buflen,"64M");
        break;
    case 6606://CPU
        snprintf(buff,buflen,"MIPS");
        break;
    case 6607://SYS VERSION
        snprintf(buff,buflen,"LINUX 3.10.14");
        break;
    case 6608://FIRMWARE VERSION
        snprintf(buff,buflen,"1.01.030");
        break;
    case 6609://FIRMWARE NAME
        snprintf(buff,buflen,"HDC-51");
        break;    
    case 6610://Volte
        snprintf(buff,buflen,"0");
        break;
    case 6611://NetType
        snprintf(buff,buflen,"WIFI");
        break;
    case 6612://Net Account
        snprintf(buff,buflen,"Account");
        break;
    case 6613://PhoneNumber
        snprintf(buff,buflen,"13812345678");
        break;
    case 6614://Location
        snprintf(buff,buflen,"25.77701556036132,123.52958679200002,1");
        break;

    default:
        iret = -1;
        break;    
    }

///////////////////////////////
    if(iret==0)
    {
        //申请的空间,sdk会释放
        *outbuff = (char*)malloc(strlen(buff)+1);
        strcpy(*outbuff,buff);
    }
    
    printf("myDMReadInfo,ID=%d,value=%s,iret=%d\n",resId,buff,iret);
    return iret;
}


PATTERN_SINGLETON_IMPLEMENT(DmManager);

DmManager::DmManager():CThread("DmManager", TP_NET)
  ,m_iInit(0)
{	
	_printd("DmManager");

}
DmManager::~DmManager()
{

}

VD_INT32 DmManager::Start()
{
	if(m_iInit) return 0;
	m_iInit = 1;
	CreateThread();
	return 0;
}
VD_INT32 DmManager::Stop()
{
	_printd("DmManager :: Stop ");
	DestroyThread();
	return 0;
}
int DmManager::DM_Start(char *pCMEI, char *pAPP, char *pPSW, int timeout, DM_SV_MODE enMode)
{
	int   nret = -1;
	char  devid[16] = { 0 };
	//Options opt = {"861111003929109","","460041850403690", "v1.0","M100000006","",4,"127.0.0.1",5683,300,56831,0};
	Options opt = { "111001451010101", "", "", DM_DEV_VERSION, DM_DEV_APPKEY, DM_DEV_PWD, 4, DM_SV_TEST_ADDR, DM_SV_TEST_PORT, timeout, 56831, 0 };


	if (NULL == pCMEI){
		_printd("pCMEI is NULL ");
		return -2;
	}

	if (strlen(pCMEI) != 15){
		_printd("pCMEI length is error(%d) ", strlen(pCMEI));
		return -2;
	}

	if (NULL != pAPP){
		memcpy(opt.szAppKey, pAPP, strlen(pAPP));
	}
	if (NULL != pPSW){
		memcpy(opt.szPwd, pPSW, strlen(pPSW));
	}
	//snprintf(opt.szCMEI_IMEI, sizeof(opt.szCMEI_IMEI), "%s%s", DM_DEV_TAC_TUI, devid);

	memcpy(opt.szCMEI_IMEI, pCMEI, 15);

	opt.nSrvPort = (enMode == DM_MODE_TEST) ? DM_SV_TEST_PORT : DM_SV_FORMAL_PORT;
	snprintf(opt.szSrvIP, sizeof(opt.szSrvIP), "%s", (enMode == DM_MODE_TEST) ? DM_SV_TEST_ADDR : DM_SV_FORMAL_ADDR);
	timeout = (timeout > DM_DEV_TIMEOUT_MIN) ? timeout : DM_DEV_TIMEOUT_MIN;
	_printd("===>>>cmie=[%s] mod:%d  [%s:%d]  timeout=%d ", opt.szCMEI_IMEI, enMode, opt.szSrvIP, opt.nSrvPort, timeout);

	OptFuncs optfuncs = { NULL };
	optfuncs.NotifyMsg = myNotifyMsg;

	//	signal(SIGINT, handle_sig);

	//init config
	nret = LWM2M_SDK_INIT(&opt, &optfuncs);
	//run
	//nret = LWM2M_SDK_RUN(1);
	_printd("===>>>DM_Start nret=%d", nret);
	return nret;
}

void DmManager::DM_Stop()
{
	LWM2M_SDK_STOP();
	LWM2M_SDK_FINI();
	_printd("===>>>DM_Stop\n");
}

int   DmManager::GetDeviceCMEI(char cmei[RT_CMEI_LEN])
{
	char uuid[16] = { 0 };
	if (!GetCmei(uuid))
	{
		memcpy(cmei, uuid, RT_CMEI_LEN);
	}
	else
	{
		return -1;
	}
	
	if (cmei[0] == '0' || cmei[0] == 0)
		return -1;

	_printd("device cmei:%s\n", cmei);

	return 0;
}

//1:注册  2：更新 3：反注册
int DmManager::DM_GetRunStat()
{
	return g_iDMstate;
}
void DmManager::ThreadProc()
{
	DM_SV_MODE	 env = DM_MODE_FORMAL;
	char Cmei[RT_CMEI_LEN + 1] = { 0 };
	char App[128] = { 0 };
	char Psw[128] = { 0 };

	while (m_bLoop)
	{
		sleep(6);
		if (0 != GetDeviceCMEI((char *)Cmei)){
			break;
		}

		_printd("App:%s,Psw:%s,Cmei:%s,env:%d ", DM_DEV_APPKEY, DM_DEV_PWD, Cmei, env);

		if (0 != DM_Start(Cmei, (char*)DM_DEV_APPKEY, (char*)DM_DEV_PWD, /* 24 * 60 * 60 */5*60, env))
		{ //4Hour
			_printd("===>>>DM_Start error ");
			continue;
		}

		while (1)
		{
			int ret = 0;
			_printd("helllo dm!!!");
			ret = LWM2M_SDK_STOP();
			ret = LWM2M_SDK_RUN(0);
			if (g_iDMstate != 0)
			{
				sleep(30);
				continue;
			}
		}
		DM_Stop();
	}
}




