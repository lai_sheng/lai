#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "APIs/CommonBSP.h"
#include "Intervideo/MobileCountry/MobileCountryAppCmd.h"
#include "Intervideo/MobileCountry/cc_api/cc_device.h"
#include "Functions/PtzTrace.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Devices/DevInfo.h"
#include "Functions/Record.h"
#include "Functions/FFFile.h"
#include "Functions/DaemonManager.h"
#include "Functions/WifiLink.h"
#include "Functions/LedStat.h"

#ifdef IPC_JZ
#include "APIs/CommonBSP_Battery.h"
#endif

#define BR_HDMODE 0     //高清
#define BR_SDMODE 1     //标清		
#define BR_LUMODE 2     //流畅



#ifdef IPC_JZ_NEW
#ifdef FAC_CHECK
const int Bitrate_T10[3] = { 1000, 512, 256 };
const int Bitrate_T20[3] = { 2000, 800, 256 };
#else
#ifdef IPC_JZ_T21
const int Bitrate_T10[3] = { 800, 512, 256 };
const int Bitrate_T20[3] = { 900, 800, 256 };
#else
const int Bitrate_T10[3] = { 800, 512, 256 };
const int Bitrate_T20[3] = { 1000, 800, 256 };
#endif
#endif
#endif

#ifdef IPC_JZ
const int Bitrate_100W[3] = { 800, 512, 256 };
const int Bitrate_200W[3] = { 1200, 800, 256 };
#endif




PATTERN_SINGLETON_IMPLEMENT(MobileCountryAppCmd);
MobileCountryAppCmd::MobileCountryAppCmd():CThread("MobileCountryAppCmd", TP_NET)
  ,m_iInit(0)
  ,m_packet_count(0)
{	
	_printd("MobileCountryAppCmd");

}
MobileCountryAppCmd::~MobileCountryAppCmd()
{

}

VD_INT32 MobileCountryAppCmd::Start()
{
	if(m_iInit) return 0;
	m_iInit = 1;
	CreateThread();
	m_configAppCtrl.attach(this, (TCONFIG_PROC)&MobileCountryAppCmd::onConfigAppCtrl);
	m_configAppCtrl.update();
	CONFIG_HEMUAPPCTRL  &configNew = m_configAppCtrl.getConfig();
	#ifdef IPC_JZ
	_printd("don't support audio detect");
	#else
	//声音灵敏度
	AudioDetectSetParam(1, configNew.iAduioSensitivity);
	#endif
	MOTION_DETECT_PARAM detect;
	detect.enable = 1;
	detect.iLevel = configNew.iMotionSensitivity;
	detect.sensitiveness = configNew.iMotionSensitivity;
	
	
	//移动侦测
	MotionDetectSetParameter(1, &detect);
	//mic开关
	CDevAudioIn::instance(0)->SetMicEanble(configNew.iAduioEnable);

	#ifdef IPC_JZ
	_printd("don't support TrackerRun");
	#else
	TrackerRunSet(0);
	#endif
	return 0;
}
VD_INT32 MobileCountryAppCmd::Stop()
{
	_printd("MobileCountryAppCmd :: Stop ");
	return 0;
}
void MobileCountryAppCmd::onConfigAppCtrl(CConfigHemuAppCtrl& cCfgAppCtrl, int& ret)
{
//	m_configHemuAppCtrl.update();
	CONFIG_HEMUAPPCTRL &configOld = m_configAppCtrl.getConfig();
	CONFIG_HEMUAPPCTRL  &configNew = cCfgAppCtrl.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}
	#ifdef IPC_JZ
	_printd("don't support audio detect");
	#else
	//声音灵敏度
	AudioDetectSetParam(1, configNew.iAduioSensitivity);
	#endif
	MOTION_DETECT_PARAM detect;
	detect.enable = 1;
	detect.iLevel = configNew.iMotionSensitivity;
	detect.sensitiveness = configNew.iMotionSensitivity;

	//移动侦测
	MotionDetectSetParameter(1, &detect);
	//mic开关
	CDevAudioIn::instance(0)->SetMicEanble(configNew.iAduioEnable);
	configOld = configNew;
}
void MobileCountryAppCmd::ThreadProc()
{

	while (m_bLoop) 
	{
		if(m_packet_count)
		{
			HandleCmdData();			
		}
		else
		{			
			usleep(50*1000);
		}
	}	
}
VD_INT32 MobileCountryAppCmd::SDCardFormat()
{
	_printd("format SD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	if(g_RecordStatus)
	{
		_printd("#############StopManulRecord##############");
		g_Record.StopRec(REC_CLS, 0);
	}	

	return file_sys_format();
}

VD_INT32 MobileCountryAppCmd::AddCmdData(int ctrl,uint value,uint value1)
{
	PACKET_CMD  packet_cmd;
	packet_cmd.ctrl = ctrl;
	packet_cmd.value = value;
	packet_cmd.value1 = value1;
	m_mutex_list.Enter();
	m_packet_list.push_back(packet_cmd);
	m_packet_count++;
	m_mutex_list.Leave();
	return 0;
}

VD_INT32 MobileCountryAppCmd::HandleCmdData()
{
	int value = 0;
	int value1 = 0;
	int ctrl  = 0;
	int ptzcmd = 0;
	m_mutex_list.Enter();
	PACKET_LIST::iterator it = m_packet_list.begin();
	value  = it->value;
	ctrl   = it->ctrl;
	value1  = it->value1;
	m_packet_list.pop_front();
	m_packet_count--;
	m_mutex_list.Leave();

	_printd("ctrl[%d] value[%d]",ctrl,value);

	if(3 == ctrl)
	{
		int expendTime = 0;
#ifdef MOTOR
		SetMotorPresetPos(value, value1,&expendTime);
#endif
	}
	else if(2 == ctrl)
	{//SDFormat
		int ret = SDCardFormat();
		//g_HWComm.SdcardFormatResp(ret,value);
	}
	//PTZ
	else if(0 == ctrl || 1 == ctrl)
	{		
		
#ifdef MOTOR		
		_printd("ctrl[%d] value[%d]",ctrl,value);
		switch(value)
		{
		case PTZRUN_STOP:
				g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
				break;
		case PTZRUN_UP:
				g_PtzTrace.PtzRunCmd(PTZRUN_UP);
				break;
		case PTZRUN_DOWN:
				g_PtzTrace.PtzRunCmd(PTZRUN_DOWN);
				break;
		case PTZRUN_LEFT://R2P2 Q1  right|left
				g_PtzTrace.PtzRunCmd(PTZRUN_LEFT);
				break;
		case PTZRUN_RIGHT:
				g_PtzTrace.PtzRunCmd(PTZRUN_RIGHT);
				break;
		case PTZRUN_GOTO_MID:
				g_PtzTrace.PtzRunCmd(PTZRUN_GOTO_MID);
				break;
			default:
				_printd("================>value[%d]",value);
				break;
		}
		if(0 == ctrl)
		{
			usleep(200*1000);
			g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
		}
#endif
	}

	return 0;

}

VD_INT32   MobileCountryAppCmd::SetEncodeMode(int mode)
{
	int Ret = 0;
	int eCodeMode = mode ? mode : (mode + 3);
	uint32_t BitRate = 0;

	CConfigEncode* pCfgEncode = new CConfigEncode();
	pCfgEncode->update();
	CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig();

	int nfps = 0;
	int igop = 0;
	Get_Video_Nfps_Igop(&nfps, &igop);
	int with = 1280;
	int height = 720;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iGOP = igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = nfps;

#ifdef FAC_CHECK  //fac_check  720P=1M  1080P=2M
	eCodeMode = RT_HDMODE;
#endif
	_printd("eCodeMode[%d]", eCodeMode);
	switch (eCodeMode)
	{
	case RT_HDMODE:
	{
#ifdef IPC_JZ_NEW
		if (SensorSC2235 == PlatformGetHandle()->Sensor &&
			ProductM_A3 != PlatformGetHandle()->ProductModel){
			_printd("1080P RT_HDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_1080P;
			BitRate = Bitrate_T20[BR_HDMODE];//800;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			with = 1920;
			height = 1080;
		}
		else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
			ProductM_A3 == PlatformGetHandle()->ProductModel){
			_printd("720P RT_HDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_720P;
			BitRate = Bitrate_T10[BR_HDMODE];//800;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			with = 1280;
			height = 720;
		}
#elif IPC_JZ
		if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
			_printd("RT_HDMODE 720P Mode");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_720P;
			BitRate = Bitrate_100W[BR_HDMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
		}
		else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
			_printd("RT_HDMODE 1080P Mode");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_1080P;
			BitRate = Bitrate_200W[BR_HDMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
		}
#else
		_printd("RT_HDMODE 720P Mode");
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
			CAPTURE_SIZE_720P;
		BitRate = 800;
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
		with = 1280;
		height = 720;
#endif
	}
	break;
	case RT_SDMODE:
#if 0
	{

#ifdef IPC_JZ_NEW
		if (SensorSC2235 == PlatformGetHandle()->Sensor &&
			ProductM_A3 != PlatformGetHandle()->ProductModel){
			_printd("720P RT_SDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_720P;
			BitRate = Bitrate_T20[BR_SDMODE];//600;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			with = 640;
			height = 360;
		}
		else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
			ProductM_A3 == PlatformGetHandle()->ProductModel){
			_printd("360P RT_SDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_T10[BR_SDMODE];//512;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			with = 640;
			height = 360;
		}
#elif IPC_JZ
		if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
			_printd("640x360P RT_SDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_100W[BR_SDMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			with = 640;
			height = 360;
		}
		else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
			_printd("720P RT_SDMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_720P;
			BitRate = Bitrate_200W[BR_SDMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			with = 640;
			height = 360;
		}
#else
		_printd("640x360P RT_SDMODE");
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
			CAPTURE_SIZE_360P;
		BitRate = 512;
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
		with = 640;
		height = 360;
#endif
	}
	break;
#endif
	case RT_LUMODE:
	{
#ifdef IPC_JZ_NEW
		if (SensorSC2235 == PlatformGetHandle()->Sensor &&
			ProductM_A3 != PlatformGetHandle()->ProductModel){
			_printd("360P RT_LUMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_T20[BR_LUMODE];//600;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
			with = 640;
			height = 360;
		}
		else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
			ProductM_A3 == PlatformGetHandle()->ProductModel){
			_printd("360P RT_LUMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_T10[BR_LUMODE];//600;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
			with = 640;
			height = 360;
		}
#elif IPC_JZ
		if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
			_printd("360P RT_LUMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_100W[BR_LUMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
			with = 640;
			height = 360;
		}
		else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
			_printd("360P RT_LUMODE");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
				CAPTURE_SIZE_360P;
			BitRate = Bitrate_200W[BR_LUMODE];
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
			with = 640;
			height = 360;
		}
#else
		_printd("360P RT_LUMODE");
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution =
			CAPTURE_SIZE_360P;
		BitRate = 256;
		cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
		with = 640;
		height = 360;
#endif
	}
	break;
	default:
	{
		Ret = -1;
	}
	break;
	}
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate = BitRate;
	if (Ret != -1)	pCfgEncode->commit();

	delete pCfgEncode;
//	ModResolutionNotice(with, height);
	return Ret;

}
VD_INT32   MobileCountryAppCmd::ModCameraCfg(int ctrl, int value)
{
	CConfigCamera ConfigCamera;
	ConfigCamera.update();

	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();

	switch (ctrl)
	{
	case CC_CMD_SET_NIGHT_MODE:
	{
		if (0 == value)
		{
			cfgCamera.IRCut = 2;
		}
		else if (1 == value)
		{
			cfgCamera.IRCut = 1;
		}
		else
		{
			cfgCamera.IRCut = 0;
		}
	}
	break;
	case CC_CMD_SET_ROTATE:
	{
		//0：不旋转
		//180: 旋转180度
		if (0 == value)
		{
			cfgCamera.HorReverse = 0;
			cfgCamera.VerReverse = 0;
		}
		else
		{
			cfgCamera.HorReverse = 1;
			cfgCamera.VerReverse = 1;
		}
	}
	break;
	case CC_CMD_SET_ANDTIFLICKER:
	{
		if (50 == value)
		{
			cfgCamera.AntiFlicker = 1;
		}
		else
		{
			cfgCamera.AntiFlicker = 2;
		}
	}
	break;
	case CC_CMD_SET_MOTION_DETECTION_SESSITIVITY:
	{		
		CONFIG_HEMUAPPCTRL  &configNew = m_configAppCtrl.getConfig();
		configNew.iMotionSensitivity = value;
		MOTION_DETECT_PARAM detect;
		detect.enable = 1;
		detect.iLevel = value;
		detect.sensitiveness = value;
		//移动侦测
		MotionDetectSetParameter(1, &detect);
		m_configAppCtrl.commit();
	}
	default:
		break;

	}
	ConfigCamera.commit();

	return 0;
}

int  MobileCountryAppCmd::SwitchWifi(char* Ssid, char* Passwd)
{
	IPC_HEADER header = { 0 };
	if (FALSE == CheckSsid(Ssid, strlen(Ssid)))
		return 1;
	_printd("ssid:%s passwd:%s", Ssid, Passwd);
	rename(STORE_WIFI_CONFIG, STORE_WIFI_CONFIG_BAK);
	Store_Cfgfile(STORE_WIFI_CONFIG, Ssid, Passwd);
	header.ipc_cmd = WIFI_RECONECT;
	g_DaemonManager.SendCmdToDaemon((void*)&header, sizeof(IPC_HEADER));
	_printd("switch wifi");
	return 0;
}
VD_INT32  MobileCountryAppCmd::DisconectWifi()
{
	IPC_HEADER header = { 0 };
	header.ipc_cmd = WIFI_DISCONECT;
	g_DaemonManager.SendCmdToDaemon((void*)&header, sizeof(IPC_HEADER));
	_printd("disconect wifi");
	return 0;
}
void MobileCountryAppCmd::GetWifiList(cc_wifi_s *pWifiListInfo)
{
	int i = 0;
	FILE *fp = NULL;
	cc_wifi *wifi_data = NULL;
	char buf[1024] = { 0 };
	char tmpSignalLevel[8] = { 0 };
	unsigned int fwstate = 0;
	char moduleString[32] = { 0 };
	char wifiname[48] = { 0 };
	
	#ifdef IPC_JZ
	switch (BAT_PlatformGetHandle()->WifiModule)
	#else
	switch (PlatformGetHandle()->WifiModule)
	#endif
	{
	case RTL8188FU:
		snprintf(moduleString, sizeof(moduleString), "%s", "rtl8188fu");
		break;
	case RTL8189FS:
		snprintf(moduleString, sizeof(moduleString), "%s", "rtl8189fs");
		break;
	case RTL8821CS:
		snprintf(moduleString, sizeof(moduleString), "%s", "rtl8821cs");
		break;
	default:
		_printd("the wifi module not's realtek, or non existent! ");
		return;
	}
	sprintf(buf, "/proc/net/%s/wlan0/survey_info", moduleString);

	fp = fopen(buf, "w");
	if (fp == NULL) {
		printf("Fail to fopen %s\n", buf);
		return;
	}
	fprintf(fp, "%c", '1');
	fclose(fp);
	sprintf(buf, "/proc/net/%s/wlan0/fwstate", moduleString);
	for (i = 0; i < 100; i++)
	{
		fp = fopen(buf, "r");
		if (fp == NULL) {
			printf("Fail to fopen %s\n", buf);
			return;
		}
		fscanf(fp, "fwstate=%x", &fwstate);
		fclose(fp);
		usleep(100 * 1000);
		if ((fwstate & 0x0800) == 0) {
			break;
		}
	}
	sprintf(buf, "/proc/net/%s/wlan0/survey_info", moduleString);
	fp = fopen(buf, "r");
	if (fp == NULL) {
		printf("Fail to fopen %s\n", buf);
		return;
	}
	i = 0;
	memset(buf, 0, sizeof(buf));
	GetConnWifi(wifiname, sizeof(wifiname));
	//g_wifi_list.count = 2;
	pWifiListInfo->wifi_list_buf = malloc(30*sizeof(cc_wifi));
	if (NULL == pWifiListInfo->wifi_list_buf)
	{
		pWifiListInfo->count = 0;
		return ;
	}
	memset(pWifiListInfo->wifi_list_buf, 0, 30 * sizeof(cc_wifi));
	wifi_data = (cc_wifi *)pWifiListInfo->wifi_list_buf;
	
	while (fgets(buf, sizeof(buf), fp) != NULL)
	{
		//if (i != 0)
		//{
		char buffer[4][128] = { 0 };
		sscanf(buf, "%*s %s %*s %*s %s %*s %*s %s %s", buffer[0], buffer[1], buffer[2], buffer[3]);
		if (0 == strncmp(wifiname, buffer[3], strlen(wifiname)))
		{
			//objson["msgContent"]["responseParams"][i - 1]["bConnected"] = 1;
			wifi_data[i].connected = 1;
		}
		else
		{
			//objson["msgContent"]["responseParams"][i - 1]["bConnected"] = 0;
			wifi_data[i].connected = 0;
		}
		//objson["msgContent"]["responseParams"][i - 1]["bssid"] = buffer[0];
		//objson["msgContent"]["responseParams"][i - 1]["encryption"] = buffer[2];
		//objson["msgContent"]["responseParams"][i - 1]["signal"] = (120 - abs(atoi(buffer[1]))) / 20; //1~5
		//objson["msgContent"]["responseParams"][i - 1]["ssid"] = buffer[3];

		strcpy(wifi_data[i].bssid, buffer[0]);
		strcpy(wifi_data[i].ssid, buffer[3]);
		//strcpy(wifi_data[0].passwd, "12345678");
		wifi_data[i].signal = (120 - abs(atoi(buffer[1]))) / 20; //1~5;
		wifi_data[i].mode = CC_WIFI_MODE_WPA_WPA2;
		wifi_data[i].noise_level = 0;
		//wifi_data[0].connected = 1;
		pWifiListInfo->count++;
		//}
		if (i < 29) {
			i++;
		}
		else {
			break;
		}
		memset(buf, 0, sizeof(buf));
	}
	fclose(fp);

}


VD_INT32    MobileCountryAppCmd::LedCtrol(int value)
{
	if (value)
	{
		g_ledstat.SetLedSwitch(0, 1);
	}
	else
	{
		g_ledstat.SetLedSwitch(1, 1);
	}
	return 0;
}
VD_INT32 MobileCountryAppCmd::SetAutoTrack(int value)
{
	CConfigAlarmSwitch	*p_AlarmSwitch;
	p_AlarmSwitch = new CConfigAlarmSwitch;

	if (p_AlarmSwitch)
	{
		p_AlarmSwitch->update();
		CONFIG_ALARMSWITCH &configtrace = p_AlarmSwitch->getConfig();
		configtrace.bTraceEnable = value;
		//TrackerRunSet(motionTrack);
		p_AlarmSwitch->commit();
		delete p_AlarmSwitch;
	}
}
