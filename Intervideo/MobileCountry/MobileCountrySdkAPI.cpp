#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#include "Intervideo/MobileCountry/MobileCountrySdkCallback.h"

#include "Functions/DriverManager.h"
#include "APIs/Audio.h"
#include "APIs/CommonBSP.h"
#include "Net/NetApp.h"
#include "Devices/DevInfo.h"
#include "Devices/DevExternal.h"
#include "APIs/ExtDev.h"

#include "Intervideo/MobileCountry/iotsdk/nt_iot_api.h"
#include "Intervideo/MobileCountry/camerasdk/dot_api.h"
#include "Intervideo/MobileCountry/camerasdk/dot_define.h"
#include "Intervideo/MobileCountry/camerasdk/dot_error.h"

//是否启动了绑定
int g_bind_loop = 0;
//是否启动了SDK、推流等
int g_start_sdk = 0;
//是否需要配置wifi
static int m_nConfigWifi = 0;

PATTERN_SINGLETON_IMPLEMENT(CMobileCountrySdkApi);
CMobileCountrySdkApi::CMobileCountrySdkApi() :CThread("CMobileCountrySdkApi", TP_NET)
, m_nSdkInit(0)
, m_nImageWidth(0)
, m_nImageHeight(0)
, m_nUpgrade(0)
, m_nSendControl(0)
, m_nVideoSeq(0)
, m_nAudioSeq(0)
, m_nFirstKeyFrame(0)
, m_nSdStatus(0)
, m_nNtpTimeOut(0)
, m_nNtpSuccess(0)
{
	_printd("CMobileCountrySdkApi");

}

void print_log(const char* file, const char* fun, int line, const char* format,...)
{
    const char* p = strrchr(file, '/');
    const char* filename = p ? p + 1 : file;

    va_list ap;
    char prefix[256];
    char buf[1024];

    snprintf(prefix,sizeof(prefix)-1,"[%s:%s:%d]",filename, fun, line);
    strncpy(buf,prefix, sizeof(prefix) - 1);
    int len = strlen(buf);

    va_start(ap,format);
    vsnprintf(buf+len, sizeof(buf) - len - 1, format, ap);
    va_end(ap);
    printf("%s",buf);
}

CMobileCountrySdkApi::~CMobileCountrySdkApi()
{

}

VD_INT32 CMobileCountrySdkApi::Start()
{
	MobileCountrySdkInit();
	m_oTalk.Start();
	CreateThread();
	
	return 0;
}

VD_INT32 CMobileCountrySdkApi::Stop()
{
	return 0;
}

void CMobileCountrySdkApi::ThreadProc()
{
	while (m_bLoop)
	{		
		sleep(5);
		SnapImage();
	}
}

VD_INT32 CMobileCountrySdkApi::GetMmacAddress(const char *ifname, char *mac)
{
	int fd = -1;
	struct ifreq if_hwaddr;

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		return -1;
	}

	memset(&if_hwaddr, 0, sizeof(if_hwaddr));
	strncpy(if_hwaddr.ifr_name, ifname, sizeof(if_hwaddr.ifr_name));

	int ret = ioctl(fd, SIOCGIFHWADDR, &if_hwaddr);
	if (ret >= 0) {
		unsigned char* hwaddr = (unsigned char *)if_hwaddr.ifr_hwaddr.sa_data;
		sprintf(mac, "%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x",
			hwaddr[0], hwaddr[1], hwaddr[2], hwaddr[3], hwaddr[4], hwaddr[5]);
	}

	close(fd);

	return ret;
}

VD_INT32 CMobileCountrySdkApi::InitSDKParams(DoT_InitParams_st* init_params)
{
	if (init_params)
	{
		char cmei[32] = { 0 };
		char strVerName[64] = {0};
		char *pVesionData = NULL;
		char firewareVersion[64] = { 0 };
		char mac[32] = { 0 };
		int waitCnt = 0;
		
		memset(init_params,0,sizeof(DoT_InitParams_st)); 

		init_params->max_channel_num      = 1;
		init_params->streams_per_channel  = 1;
		init_params->buf_size_per_stream  = 2*1024*1024;
		init_params->device_type          = DOT_DEVICE_TYPE_LOWPOWER_IPC;
		strcpy(init_params->device_model, "BELL_WJA_HS_B1_W");
		strcpy(init_params->log_path, "");
		init_params->log_max_line = 1000;
		strcpy(init_params->config_path, SDK_CONFIG_FILE_PATH);

		//获取序列号
		if (!GetUuid(cmei))
		{
			_printd("sn:%s", cmei);
		}
		else
		{
			_printd("==============sn error================");
			return -1;
		}
		strcpy(init_params->serial_number, cmei);

		//获取版本
		Get_Version_Name(strVerName, sizeof(strVerName));
		printf("strVerName = %s\n", strVerName);
		pVesionData = strchr(strVerName, 'V');
		if (pVesionData)
		{
			strcpy(firewareVersion, pVesionData + 1);
		}
		else
		{
			strcpy(firewareVersion,"1.01.010");
		}
		strcpy(init_params->fw_version, firewareVersion);
		
		//获取mac地址
		while (1)
		{
			GetMmacAddress("wlan0", mac);
			if (strlen(mac) > 0) {
				printf("get mac ok!!!\n");
				strcpy(init_params->mac, mac);
				printf("init_params->mac.mac:%s\n", init_params->mac);
				break;
			}
			usleep(100 * 1000);
			if (++waitCnt >= 50) { //5s
				printf("get mac failed!!!\n");
				break;
			}
		}
		
		OUTPUT_LOG("mac:%s  ,sn:%s, model:%s, log path:%s, config path:%s,  fw_version:%s\n",
							init_params->mac,
							init_params->serial_number, 
						        init_params->device_model,
						        init_params->log_path, 
						        init_params->config_path,
						        init_params->fw_version);
	}
}

VD_INT32 CMobileCountrySdkApi::GetVersion()
{
#if 1
	_printd("xie si 1.01.025");
	m_strFirmwareVersion = "1.01.025";
	m_strCameraAppVersion = "1.01.025";
	return 0;
#endif
	std::string strv;
#ifdef IPC_JZ
	std::ifstream hemuversion("/app/verinfo");
#else
	std::ifstream hemuversion("/app/ipcam/verinfo");
#endif
	if (!hemuversion.is_open())
	{
		m_strFirmwareVersion = "1.01.025";
		m_strCameraAppVersion = "1.01.025";//每个版本收动更新，可以不更新
	}
	else
	{
		if (getline(hemuversion, strv))
		{
			m_strFirmwareVersion = strv.c_str();
			m_strCameraAppVersion = strv.c_str();
			_printd("version[%s]", strv.c_str());
		}
		hemuversion.close();
	}
}

/*设置回调函数*/
void  CMobileCountrySdkApi::InitSDKCallback(DoT_CallbackFuncList_st* cb_list)
{
	if (cb_list) {
	        memset(cb_list, 0, sizeof(DoT_CallbackFuncList_st));
	        cb_list->cb_set_device_config = cbSetDevConfig;
	        cb_list->cb_get_running_info = cbGetDevRunningInfo;
	        cb_list->cb_dev_ctrl = cbControlDevCmd;
	        cb_list->cb_audio_play = cbAudioPlayCmd;
	        cb_list->cb_ptz_ctrl = cbPTZControl;
	        cb_list->cb_server_status = cbServerStatus;
	        cb_list->cb_upgrade = cbUpgrade;
	        cb_list->cb_bind_status = cbDevBindStatus;
        }

	return;
}

//初始化DeviceCapbility函数
void CMobileCountrySdkApi::InitDeviceCapbility(DoT_DeviceHWCapability_st* hw_capbility, 
                                		DoT_DeviceSWCapability_st* sw_capbility)
{
    memset(hw_capbility, 0, sizeof(DoT_DeviceHWCapability_st));
    memset(sw_capbility, 0, sizeof(DoT_DeviceSWCapability_st));

    // set hardware capbility
    hw_capbility->hw_support_wifi            = true;
    hw_capbility->hw_support_speaker         = true;
    hw_capbility->hw_support_microphone      = true;
    hw_capbility->hw_support_night_vision    = true;
    hw_capbility->hw_support_pir             = true;
    hw_capbility->hw_support_battery         = true;
    hw_capbility->hw_support_led_indicate    = true;
    hw_capbility->hw_support_tamer_alarm     = true;    
    hw_capbility->hw_support_doorlock        = true;

    //set software capbility
    sw_capbility->sw_support_rotate          = true;
    sw_capbility->sw_support_bi_directional_audiotalk = true;
    sw_capbility->sw_support_video_resolution_modify = true;
    sw_capbility->sw_support_remote_wakeup   = true;
}

int CMobileCountrySdkApi::StartBind(int mode)
{
    if (!g_start_sdk) {
	OUTPUT_LOG("SDK not start\n");
	m_nConfigWifi = 1;
	return -1;
    }
    else
    {
	if (!g_bind_loop) {
		DoT_int32 ret = DoT_StartDeviceBind(mode);
		OUTPUT_LOG("DoT_StartDeviceBind, ret:%d\n",ret);
		if (DOT_EC_SUCCESS == ret) {
		    g_bind_loop = 1;
		}
		m_nConfigWifi = 0;
    	}
    }

    return 0;
}

int  CMobileCountrySdkApi::StopBind()
{
    if (g_bind_loop) {
        DoT_int32 ret = DoT_StopDeviceBind();
        OUTPUT_LOG("DoT_StopDeviceBind, ret:%d\n",ret);
        g_bind_loop = 0;
    }

    return 0;
}

int64_t  CMobileCountrySdkApi::getNowMs()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * (int64_t)1000 + tv.tv_usec / 1000;
}

void   CMobileCountrySdkApi::OnMediaData(int pagetype, int sync, unsigned char* data, unsigned int size, unsigned int timestamp)
{
	if (m_nUpgrade)
	{
		//正在升级
		return;
	}

	if (!m_nSdkInit)
		return; //SDK初始化后可以一直往里面塞数据，SDK 控制推流关闭
		
	int ret = 0;

	if (2 == pagetype)
	{
		DoT_VideoInfo_st packet = {0};
		memset(&packet, 0, sizeof(DoT_VideoInfo_st));
		packet.timestamp = timestamp;
		packet.utcms = getNowMs();
		packet.seq = ++m_nVideoSeq;
		packet.media_type = DOT_VIDEO_TYPE_H264;
		packet.data = data;
		packet.width = m_nImageWidth;
		packet.height = m_nImageHeight;
		packet.data_size = size;
		packet.frame_type = (DoT_VideoFrameType_en)sync;
		if (sync)
		{
			packet.frame_type = DOT_VIDEO_FRAME_TYPE_IFRAME;
			if (!m_nFirstKeyFrame)
			{
				m_nFirstKeyFrame = 1;
			}
		}
		ret = DoT_PushVideoData(0, 0, &packet);
		if (ret != 0) 
		{
			m_nFirstKeyFrame = 0;
		}
	}
	else if (1 == pagetype)
	{
		DoT_AudioInfo_st packet = {0};
		memset(&packet, 0, sizeof(DoT_VideoInfo_st));
		packet.timestamp = timestamp;
		packet.utcms = getNowMs();
		packet.seq = ++m_nAudioSeq;
		packet.media_type = DOT_AUDIO_TYPE_G711A;
		packet.channel_num = 1;
		packet.sample_rate = 8000;
		packet.sample_bit = 8;
		packet.data = data;
		packet.data_size = size;
		DoT_PushAudioData(0, 0, &packet);
	}

	if (!m_nFirstKeyFrame) 
	{
		return;
	}
}

void CMobileCountrySdkApi::AppendTalkAudioData(unsigned char *pBuf, int nDataLen)
{
	m_oTalk.AddTalkData(pBuf, nDataLen);
}

void CMobileCountrySdkApi::SendIOTEvent(NTIOT_EventType_en    type)
{
	if (INIT_OK != m_nSdkInit)
		return;
	
	OUTPUT_LOG("DemoPushDoorLockEvent\n");
	NTIOT_EventInfo_st event;
	event.doorlock_event_attr.id = 0;
	event.type =    type;
	event.systime = 0;   //0表示默认使用当前时间
	NTIOT_PushEvent(&event);
}

void CMobileCountrySdkApi::StartAlarm(DoT_AlarmType_en type)
{
	if (INIT_OK != m_nSdkInit)
		return;
	
	DoT_DeviceAlarmInfo_st event;
    	memset(&event, 0, sizeof(DoT_DeviceAlarmInfo_st));
    	event.alarm_type = type;
    	DoT_StartAlarm(0, 0, &event);
}

void CMobileCountrySdkApi::StopAlarm(DoT_AlarmType_en type)
{
	if (INIT_OK != m_nSdkInit)
		return;

	DoT_StopAlarm(0, type);
}

void CMobileCountrySdkApi::ApiFactoryReset()
{
	if (INIT_OK != m_nSdkInit)
		return;
//	cc_api_factoryreset();
}

int CMobileCountrySdkApi::LowPowerStartSDK(DoT_WakeupReason_en reason)
{
    DoT_int32  ret = DOT_EC_SUCCESS;

    if (g_start_sdk) {
        OUTPUT_LOG("SDK has been started\n");
        return ret;
    } else {
        g_start_sdk = 1;
    }

    //低功耗设备唤醒，需要调用DoT_Wakeup函数
    //正常的IPC启动则无需调用
    if (DOT_WAKEUP_NONE != reason) {
        OUTPUT_LOG("wakeup reason %d\n", reason);
        DoT_Wakeup(reason);
    }

    //启动SDK service
    ret = DoT_StartService();
    if (ret != DOT_EC_SUCCESS) {
        OUTPUT_LOG("start service failed. ec=%d\n",ret);
        return ret;
    }

    //低功耗设备唤醒，需要设置唤醒事件，目前有两种事件需要设置：门铃唤醒和PIR唤醒
    //正常的IPC启动则无需设置
    if (DOT_WAKE_UP_BY_RING == reason) {
//        StartAlarm(DOT_ALARM_RING);
    } else if (DOT_WAKE_UP_BY_PIR == reason) {
//        StartAlarm(DOT_ALARM_PIR_DETECT);
    }

    return ret;
}

int  CMobileCountrySdkApi::StopSDK()
{
    DoT_int32 ret = DOT_EC_SUCCESS;

    if (!g_start_sdk) {
        OUTPUT_LOG("SDK has been stopped\n");
        return ret;
    }
    StopBind();

    //停止服务
    DoT_StopService();

    g_start_sdk = 0;

    return ret;
}

void CMobileCountrySdkApi::SnapImage()
{	
	char *imagebuf = NULL;
	int   imagesize = 0;

	if (INIT_OK != m_nSdkInit) return;

#ifdef IPC_JZ
	CaptureGetSnapPic((char**)(&imagebuf), (int*)&(imagesize));
#else
	jz_snap_pic((char**)(&imagebuf), (int*)&(imagesize));
#endif
	if (NULL != imagebuf)
	{
		DoT_PictureInfo_st pic_info;
		memcpy(pic_info.data, imagebuf, imagesize);
		pic_info.data_size = imagesize;
		DoT_PushThubmnail(0, &pic_info);
		free(imagebuf);
	}
}

static int update_info_status(NTIOT_UpdateInfoStatus_en status, void* data, void* user_data)
{
    if(status == NTIOT_DOORLOCK_UPDATE_INFO_FAIL) {
        OUTPUT_LOG("update_info_status fail\n");
    } else if(status == NTIOT_DOORLOCK_UPDATE_INFO_SUCCEED) {
        OUTPUT_LOG("update_info_status success\n");
    }
    return 0;
}

#define DOORLOCKINFOCNT 5
static int doorlock_control(NTIOT_CMD_Control_en cmd, void* data, void* user_data)
{ 
    OUTPUT_LOG("doorlock_control cmd:%d\n",cmd);
    if(cmd == NTIOT_CMD_SET_TEMP_PASSWORD_KEY) {
        OUTPUT_LOG("key:%s\n",(char *)data);
    } else if(cmd == NTIOT_CMD_GET_DOORLOCK_UPDATE_INFO) {
        OUTPUT_LOG("PushDoorLockInfoList\n");
        NTIOT_DoorLockInfoList_st attr;
        attr.info_num = DOORLOCKINFOCNT;
        NTIOT_DoorLockInfo_st *info = (NTIOT_DoorLockInfo_st *)malloc(sizeof(NTIOT_DoorLockInfo_st)*DOORLOCKINFOCNT);
        info[0].id = 0;
        info[0].type = NTIOT_DOORLOCK_FINGER_PRINT_TYPE;
        info[1].id = 1;
        info[1].type = NTIOT_DOORLOCK_FINGER_PRINT_TYPE;
        info[2].id = 2;
        info[2].type = NTIOT_DOORLOCK_FINGER_PRINT_TYPE;
        info[3].id = 0;
        info[3].type = NTIOT_DOORLOCK_PASSWORD_TYPE;
        info[4].id = 1;
        info[4].type = NTIOT_DOORLOCK_PASSWORD_TYPE;
        attr.info = info;
        NTIOT_PushDoorLockInfoList(&attr);
        free(attr.info);
    }  else if(cmd == NTIOT_CMD_GET_BATTERY_VALUE) {        
        OUTPUT_LOG("Get Battery value\n");
        NTIOT_BatteryValue_st *value = (NTIOT_BatteryValue_st *)data;

	ZRT_CAM_BAT_Status Status;
	memset(&Status, 0, sizeof(Status));
	if(0 != g_DevExternal.Dev_GetElecQuantity(&Status))
		return -1;
        value->percent = Status.capacity;
	if (Status.chargingStatus == BAT_DISCHARGING)
	{
		value->status = (NTIOT_BatteryStatus_en)DOT_BATTERY_STATUS_NORMAL;
	}
	else if (Status.chargingStatus == BAT_CHARGING)
	{
		value->status = (NTIOT_BatteryStatus_en)DOT_BATTERY_STATUS_CHARGING;
	}
	else if (Status.chargingStatus == BAT_FULL)
	{
		value->status = (NTIOT_BatteryStatus_en)DOT_BATTERY_STATUS_FULL;
	}
    }
    return 0;
}

VD_INT32 CMobileCountrySdkApi::MobileCountrySdkInit()
{
	char version[64] = {0};

	printf("\n\nMobileCountrySdkInit()\n\n");
	DoT_GetVersion(version);
    	OUTPUT_LOG("version:%s\n", version);
	if (INIT_OK != m_nSdkInit)
	{
		DoT_int32  ret = DOT_EC_SUCCESS;
    
    		DoT_InitParams_st init_params;
	        DoT_CallbackFuncList_st callback_list;
	        DoT_DeviceHWCapability_st hw_capability;
	        DoT_DeviceSWCapability_st sw_capability;

		//初始化SDK Params
    		InitSDKParams(&init_params);
    		InitSDKCallback(&callback_list);
   		ret = DoT_Initialize(&init_params, &callback_list);
   		if (ret != DOT_EC_SUCCESS) {
		        OUTPUT_LOG("sdk init failed. ec=%d\n",ret);
		        return ret;
		}
		DoT_SetLogLevel(DOT_LOG_LEVEL_DEBUG);

		//初始化DeviceCapbility
		InitDeviceCapbility(&hw_capability, &sw_capability);
		ret = DoT_InitDeviceCapability(0, &hw_capability, &sw_capability);
		if (ret != DOT_EC_SUCCESS) {
			OUTPUT_LOG("DeviceCapability init failed. ec=%d\n", ret);
			return ret;
		}

		NTIOT_InitParams_st iot_init_params;
		memset(&iot_init_params, 0, sizeof(iot_init_params));
		strcpy(iot_init_params.mac, init_params.mac);
		strcpy(iot_init_params.serial_number, init_params.serial_number);
		strcpy(iot_init_params.fw_version, init_params.fw_version);
		strcpy(iot_init_params.device_model, init_params.device_model);
		
		NTIOT_CallbackFuncList_st cb;
    		cb.cb_iot_control = doorlock_control;        
    		cb.cb_iot_update_info_status = update_info_status;
    		NTIOT_Init(&iot_init_params, &cb);
    		NTIOT_Start();

		//启动SDK service
		ret = LowPowerStartSDK(DOT_WAKEUP_NONE);
		if (ret != DOT_EC_SUCCESS) {
			OUTPUT_LOG("start service failed. ec=%d\n",ret);
			return ret;
		}

		if (m_nConfigWifi)
		{
			StartBind(1);
		}
	}
	return 0;
}


