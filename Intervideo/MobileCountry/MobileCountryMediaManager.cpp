#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#include "Intervideo/MobileCountry/MobileCountryMediaManager.h"
#include "Configs/ConfigVideoColor.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigLocation.h"
#include "Net/NetApp.h"
#include "APIs/DVRDEF.H"
#include "APIs/Capture.h"
#include "Main.h"
#include <time.h>
#include "Functions/Record.h"
#include "System/Object.h"
#include <unistd.h>
#include "Devices/DevExternal.h"
#include "Functions/DriverManager.h"
#include "Net/NetConfig.h"
#include "APIs/Ide.h"
#include <sys/stat.h>
#include <stdio.h>
#include "Functions/Snap.h"
#include <dirent.h>
#include "Functions/FFFile.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <signal.h>
#include <stdlib.h>
#include "APIs/Audio.h"
#include "Configs/ConfigCamera.h"
#include "APIs/Video.h"
#include "APIs/MotionDetect.h"
#include "Configs/ConfigEvents.h"
#include "Net/Dlg/DlgNtpCli.h"
#include "System/Msg.h"




PATTERN_SINGLETON_IMPLEMENT(CMobileCountryMediaManager);


CMobileCountryMediaManager::CMobileCountryMediaManager():CThread("CMobileCountryMediaManager", TP_NET)
{
	m_packet_count = 0;
	m_videopacket_count = 0;
	m_audiopacket_count = 0;
	m_iNtpSuccess = 0;
	m_ptkBufMain.Reset(true);

	m_iSuspendMode = 1;
	m_iCloudEnable = 1;
}
CMobileCountryMediaManager::~CMobileCountryMediaManager()
{

}

VD_INT32 CMobileCountryMediaManager::Start()
{	
	CreateThread();
	StartVideo(0,0);
	StartAudioIn(0);
	return 0;
}
VD_INT32 CMobileCountryMediaManager::StartVideo(int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture)
	{
		_printd("Icapture::instance ok");
		pCapture->SetIFrame(StreamType);
		VD_BOOL bRet = pCapture->Start(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CMobileCountryMediaManager::OnCapture, DATA_MONITOR,StreamType);
	}else
	{
		_printd("Icapture::instance ERror");
		return -1;
	}
	return 0;
}
void CMobileCountryMediaManager::OnCapture(int iChannel, uint iStreamType, CPacket *pPacket)
{

	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.isVideo = TRUE;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = iStreamType;
		
	int iCount = g_nLogicNum;
	if (m_videopacket_count  > iCount*50) {
		
		debugf("CMobileCountryMediaManager capture list is too large discard it, size:%d, chn:%d!type:%d \n", m_packet_count, iChannel, iStreamType);
		return ;
	}
	
	m_mutex_list.Enter();
	m_videopacket_count++;
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_mutex_list.Leave();
}



VD_INT32 CMobileCountryMediaManager::StartAudioIn(int channel)
{
	_printd ("RecordCam AudioIn Start");
	int ret = -1;
	

	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	ret = pCapture->Start(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CMobileCountryMediaManager::OnAudioData);
	if (ret != 1)
		printf("###################ERROR Audio!!!######################\n");
	return 0;
}
VD_INT32 CMobileCountryMediaManager::OnAudioData(int iChannel, CPacket *pPacket)
{
	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.isVideo = FALSE;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = 0;
		
	int iCount = g_nLogicNum;
	if (m_audiopacket_count > iCount*50) {
		debugf("CMobileCountryMediaManager audio list is too large discard it, size:%d, chn:%d!\n", m_audiopacket_count, iChannel);
		return 0;
	}

	
	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_audiopacket_count ++;
	m_mutex_list.Leave();
	return 0;
}
VD_INT32 CMobileCountryMediaManager::OnVideoData(CPacket *pPacket)
{
	if (!pPacket) 
	{
        _printd("pPacket is NULL\n");
	    return -1;
	}
	PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());

	unsigned char * pBufSend = NULL;
	unsigned char ucFrameType = PACK_TYPE_FRAME_NULL;  /* 帧类型 */
	unsigned char ucFrameFlag = 0;                     /* 帧头尾的标识 */
	int usFramePos = 0;                     /* 帧开始位置 */
	uint usCFrameLength = 0;                 /* 帧在本块中的长度 */  
	uint usFrameLength = 0;
	pBufSend = pPacket->GetBuffer();
	for ( int iIndex = 0; iIndex < FRAME_MAX_NUM; iIndex++) {        
		ucFrameType = pPacketHead->FrameInfo[iIndex].FrameType;
		ucFrameFlag = pPacketHead->FrameInfo[iIndex].FrameFlag;
		usFramePos = pPacketHead->FrameInfo[iIndex].FramePos;
		usCFrameLength = pPacketHead->FrameInfo[iIndex].DataLength;
		usFrameLength = pPacketHead->FrameInfo[iIndex].FrameLength;        
		if (ucFrameType == PACK_TYPE_FRAME_NULL) {
			break;
		} else if(PACK_TYPE_FRAME_AUDIO == ucFrameType) {
			continue;
		}
		if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
			m_ptkBufMain.Reset();
			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			/*一个完整帧*/
			if (-2 == WriteVideotoHw((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size())) {
				_printd("add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
			}
			m_ptkBufMain.Reset();
			continue;
		} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
			m_ptkBufMain.Reset();
			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			continue;
		} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
			/* 只包含帧尾的帧 */
			if (m_ptkBufMain.Size() > 0) {
            			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				/*组合好一个完整帧*/
				if (-2 == WriteVideotoHw((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size())) {
				_printd("Tail Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
			}
			} else {
				tracepoint();
				_printd("streamtype[%d] length[%lu] total[%lu] type[%d]", usCFrameLength, usFrameLength, ucFrameType);
				m_ptkBufMain.Reset();
			}
			continue;
		} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
			/* 不包含帧头和帧尾的帧 */
			if (m_ptkBufMain.Size() > 0) {
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
			} else {
				tracepoint();
				//_printd("NONHT Rtsp streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
				m_ptkBufMain.Reset();
				
			}
			continue;
		}
		
    }
    return 0;
}


void CMobileCountryMediaManager::SetEnable(int Enable)
{
	m_iCloudEnable = Enable;
}
void CMobileCountryMediaManager::SetSuspendMode(int mode) 
{
	m_iSuspendMode = mode;
}


void CMobileCountryMediaManager::WritePacket()
{

	do{
		CPacket* pPacket = NULL;
		uint dwStreamType = 0;
		int chn = 0;
		int isVideo = 0;

		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		chn = it->chn;
		isVideo = it->isVideo;
		m_packet_list.pop_front();
		m_packet_count--;
		if(isVideo)
		{
			m_videopacket_count--;
		}
		else
		{
			m_audiopacket_count--;
		}
		m_mutex_list.Leave();	
		WriteToHw(isVideo, pPacket);
		pPacket->Release();
			
	}while(m_packet_count > 0);
}
void CMobileCountryMediaManager::WriteToHw(int isVideo,CPacket *pPacket)
{
	if(0 == m_iCloudEnable)
	{
		return;
	}
	if(isVideo) //video
	{
		OnVideoData(pPacket);	
	}
	else//audio
	{
		WriteAduiotoHw(pPacket);
	}
}
VD_UINT32 CMobileCountryMediaManager::FindIunit(uchar * buf, uint size)
{
	if (NULL == buf)
	{
		return 0;
	}
	uint i;
	
	
	i = 0;
	while (1)
	{
		if((buf[i] == 0 )&& (buf[i + 1] == 0 )
		   && (buf[i + 2] == 0) && (buf[i + 3] == 0x01)
		   && ((buf[i + 4] & 0x1f) ==  5))
		{
			return i;
		}
		i++; 		
		if (i + 4 >= size) 
		{ 
			return 0;
		} 
	}
 
	
	return 0;
}

VD_INT32 CMobileCountryMediaManager::WriteVideotoHw(uchar *pData, unsigned int iLen)
{

	unsigned char frame_type[4]={0};
	uint64_t FrameTimeStamp = 0;
	HumuFrameType FrameType = HM_VIDEO_P_FRAME;
	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp
	memcpy(frame_type, pData,4);
	if(frame_type[3] == 0xfd || frame_type[3] == 0xad)
	{
		//I Frame
		FrameType =  HM_VIDEO_I_FRAME;
		unsigned int datatime = 0;
		unsigned int FrameSize = 0;
		struct tm start_tm; //I 帧时间  	
		//第一帧记录开始时间
		if(frame_type[3] == 0xad)
		{
			memcpy(&datatime,pData + 12,4);
			memcpy(&FrameSize,pData + 8,4);
		}
		else
		{
			memcpy(&datatime,pData + 8,4);
		}	
		start_tm.tm_year 	= ((datatime>>26)&0x3f) + 100;
		start_tm.tm_mon 	= ((datatime>>22)&0x0f) - 1; //tm结构为0--11
		start_tm.tm_mday	= (datatime>>17)&0x1f;
		start_tm.tm_hour	= (datatime>>12)&0x1f;
		start_tm.tm_min		= (datatime>>6)&0x3f;
		start_tm.tm_sec		=  datatime&0x3f;
		_printd("I Frame time:%04d-%02d-%02d %02d:%02d:%02d with:%d height:%d",
			start_tm.tm_year + 1900,start_tm.tm_mon + 1,
			start_tm.tm_mday,
			start_tm.tm_hour,start_tm.tm_min,
			start_tm.tm_sec,(0x0000ffff&FrameSize)*8,(0x0000ffff&(FrameSize>>16))*8);
		g_MobileCountrySdkApi.SetImageSize((0x0000ffff & FrameSize) * 8, (0x0000ffff & (FrameSize >> 16)) * 8);
		 time_t t_start =  mktime(&start_tm);
		
	}
	
	if(HM_VIDEO_I_FRAME == FrameType)
	{
		g_MobileCountrySdkApi.OnMediaData(2, 0, pData + 20 , iLen - 20 -sizeof(long long)-5, FrameTimeStamp/1000);
	}
	else if(HM_VIDEO_P_FRAME == FrameType)
	{
		g_MobileCountrySdkApi.OnMediaData(2, 1, pData + 8, iLen - 8 -sizeof(long long)-5, FrameTimeStamp/1000);
	}
	else if(HM_VIDEO_B_FRAME == FrameType)
	{
		g_MobileCountrySdkApi.OnMediaData(2, 2, pData + 8, iLen - 8 -sizeof(long long)-5, FrameTimeStamp/1000);
	}
	return 0;
}

void CMobileCountryMediaManager::WriteAduiotoHw(CPacket *pPacket)
{
	long long FrameTimeStamp = 0;
	unsigned int len = 0;
	memcpy(&FrameTimeStamp,pPacket->GetBuffer() + pPacket->GetLength() - sizeof(long long), sizeof(long long));

	len = pPacket->GetLength() - 8 - sizeof(unsigned int) - sizeof(long long);
	g_MobileCountrySdkApi.OnMediaData(1,0,(unsigned char*)pPacket->GetBuffer() + 8 + sizeof(unsigned int), len,(uint)(FrameTimeStamp/1000));
	//_printd("len[%d] timestamp[%u]pPacket->GetLength()[%d]",len,(uint)(FrameTimeStamp/1000),pPacket->GetLength());
}

void CMobileCountryMediaManager::ThreadProc()
{
	int timecount = 0;
	while (m_bLoop) 
	{
		if(m_packet_count > 0)
		{
			WritePacket();
			timecount = 0;
		}
		else
		{
			usleep(5*1000);
		}
	}
}

VD_INT32 CMobileCountryMediaManager::Stop()
{

	return 0;
}

