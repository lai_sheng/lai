#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include "System/File.h"
#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include "Configs/ConfigEncode.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/MobileCountry/MobileCountryHttpsUpgrade.h"
#include "Intervideo/LiveCam/RealTimeCamMemory.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "APIs/CommonBSP.h"
#include "Functions/DriverManager.h"
#include "APIs/MotionDetect.h"


#define UPGRAE_URL "http://www.dpcloud.xyz/HDC_55_MC_V1.01.024.bin"
//#define UPGRAE_URL "https://bj-blurams-update.oss-cn-beijing.aliyuncs.com/Device/148cd55a-2eb/IPC_HY_JZ_HDC_55/1/1.01.024/HDC_55_MC_V1.01.024.bin"

#define  UPGRADE_FILE   "/tmp/upgrade_file.bin"
#define  UPGRADE_INFO   "/app/ipcam/upgradeinfo"
static int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  


static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) 
	{  
        fwrite(ptr, nmemb, size,  fp); 
		fclose(fp);
    }  
    _printd("download size[%d]",len);      
    return written;  
} 
int GetLocalFileLenth(const char* fileName)
{
	struct stat statbuff;
	if( -1 == stat(fileName, &statbuff) )
	{
		_printd("stat wrong ");
		return 0;
	}
	_printd("stat success.%ld\n", statbuff.st_size);
	return statbuff.st_size;		
}


PATTERN_SINGLETON_IMPLEMENT( CMobileCountryHttpsUpgrade);

  CMobileCountryHttpsUpgrade::  CMobileCountryHttpsUpgrade():CThread("  CMobileCountryHttpsUpgrade", TP_NET)
  	,m_istartupgrade(FALSE)
{
	_printd("  CMobileCountryHttpsUpgrade");
	m_iNtpSuccess = 0;
	m_nTimeRebootTick = 0;

}

  CMobileCountryHttpsUpgrade::~CMobileCountryHttpsUpgrade()
{
	_printd("~  CMobileCountryHttpsUpgrade");
}




VD_INT32   CMobileCountryHttpsUpgrade::Start()
{
	char uuid[16]= {'\0'};
	char mac[16] = {'\0'};
	_printd ("  CMobileCountryHttpsUpgrade Init Over");		
	m_cupgradeurl = UPGRAE_URL;

//	m_nTimeRebootTick = 10;
	int ret = CreateThread();
	_printd("ret[%d]",ret);
	
	return 0;
}


VD_INT32   CMobileCountryHttpsUpgrade::Stop()
{
	_printd("  CMobileCountryHttpsUpgrade :: Stop ");
	return 0;
}



void   CMobileCountryHttpsUpgrade::ThreadProc()
{
	int once = 1;
	int config = 0;
	struct sysinfo info;

	while (m_bLoop) 
	{
		sleep(5);	
		sysinfo(&info);	
		_printd("uptime[%ld] m_nTimeRebootTick[%d]", info.uptime, m_nTimeRebootTick);		
		
		if (m_nTimeRebootTick 
			&& (info.uptime > m_nTimeRebootTick))
		{
			//升级
			if(0 == DownLoad())
			{
				//md5 检验
				_printd("OK================");
				//升级
				sleep(3);
				char UpgradeFilePath[150] = { 0 };
				sprintf(UpgradeFilePath, "/app/upgradeTool  %s", UPGRADE_FILE);
				printf("%s\n", UpgradeFilePath);
				execl("/bin/sh", "sh", "-c", UpgradeFilePath, (char*)0);
			}
			else
			{
				//下载失败
				_printd("upgrade fail!!!!!!!!!!!!!!!!!!!!!!");
				sleep(20);
			}

		}
	}
}

void CMobileCountryHttpsUpgrade::SetNtpSuccess()
{
	m_iNtpSuccess = 1;
	srand(time(NULL));
	if (!m_nTimeRebootTick)
	{
		m_nTimeRebootTick = rand() % 300;
		m_nTimeRebootTick += 300;
	}
}


int CMobileCountryHttpsUpgrade::DownLoad()
{
	_printd("upgrade============>>>>");
	CURL *curl;  
	int  nFileLenght = 0;
    CURLcode res = CURLE_RECV_ERROR;
	//尝试下载5次
	if (file_exists((char*)UPGRADE_FILE))
	{
		remove(UPGRADE_FILE);
	}
	for (int i = 0; i < 5; i++)
	{
		curl = curl_easy_init();
		if (curl)
		{
			curl_easy_setopt(curl, CURLOPT_URL, m_cupgradeurl.c_str());
			//option	
			curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 15L);
			curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 15L);
			curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1L);
			curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 15L);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);	// 从证书中检查SSL加密算法是否存在
			//指定回调函数  
			if (file_exists((char*)UPGRADE_FILE))
			{
				nFileLenght = GetLocalFileLenth(UPGRADE_FILE);
				if(0 != nFileLenght)
				{
					curl_easy_setopt(curl, CURLOPT_RESUME_FROM, nFileLenght);
				}
				else
				{
					remove(UPGRADE_FILE);
				}
			}
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefile_callback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, UPGRADE_FILE);
			res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			if (CURLE_OK == res)
			{
				break;
			}

		}
		else
		{
			_printd("CURL curl_easy_init ERROR!!!!");
		}
	}
	return res;
}

