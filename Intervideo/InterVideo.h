/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * InterVideo.h - E:\dev\mnt\src\P_2008.04.18_intervideo_2600\Intervideo\
 *
 * Copyright (C) 2008 DAHUA Technologies, All Rights Reserved.
 *
 * $Id: InterVideo.h 0001 2008-08-25 05:24:21 WuJunjie Exp $
 *
 * explain
 *
 * Update:
 *     2008-08-25 05:24 WuJunjie 10221 Create
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#ifdef __INTERVIDEO_

#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

#ifndef _INTERVIDEO_H
#define _INTERVIDEO_H

#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "MultiTask/Timer.h"

class CInterVideo  : public CObject
{
public:
    PATTERN_SINGLETON_DECLARE(CInterVideo);

    // start
    int Start(const int argc, char *argv[]);
    // stop
    int Stop(const int argc, char *argv[]);

    int Restart(const int argc, char *argv[]);
    int Exit(const int argc, char *argv[]);
    int Shutdown(const int argc, char *argv[]);

    int OnConsoleInter(int argc, char **argv);

private:
    CInterVideo();
    virtual ~CInterVideo();

private:
	CTimer m_RecTimer;
	int TimerStartRec(int arg);

};

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#define g_InterVideo (*CInterVideo::instance())

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#endif // _INTERVIDEO_H

#endif

