#include "rtspVideo.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//校验I帧H264头是否完整，我们家的顺序是SPS PPS IDR。但H264没有规定一定是这个顺序

static int FindStartMedia(const uchar  *Buffer, unsigned int Bufferlen)
{
	int iRet = -1;
	unsigned int i = 0; 
	unsigned int Bufferlenlocal = Bufferlen;
	if (Bufferlenlocal > 512) Bufferlenlocal = 512;
	while ( i < (Bufferlenlocal-4) ) {
		if ( Buffer[i+0] == 0x00 &&
			Buffer[i+1] == 0x00 &&
			Buffer[i+2] == 0x01 &&
			Buffer[i+3] == 0x67 ) {
				break;
		} 
		i++;
	}
	
	i += 4;
	while ( i < (Bufferlenlocal-4) ) {
		if ( Buffer[i+0] == 0x00 &&
		Buffer[i+1] == 0x00 &&
		Buffer[i+2] == 0x01 &&
		Buffer[i+3] == 0x68 ) {
			break;
		}
		i++;
	}
	
	i += 4;
	while ( i < (Bufferlenlocal-4) ) {
		if ( Buffer[i+0] == 0x00 &&
		Buffer[i+1] == 0x00 &&
		Buffer[i+2] == 0x01 &&
		Buffer[i+3] == 0x65 ) {
			iRet = 0;
			break;
		}
		i++;
	}
	return iRet;
}

#if 0
static int RtspSaveErrToFile(const char* errMsg)
{
	time_t timeTmp;
	FILE *fp = fopen("/appex/OnvifErrMsg.txt", "a");
	char * timeTmpStr = NULL;
	time(&timeTmp);
	timeTmpStr = asctime(gmtime(&timeTmp));
	fwrite((void*)timeTmpStr, sizeof(char), strlen(timeTmpStr), fp);
	fwrite((void*)errMsg, sizeof(char), strlen(errMsg), fp);
	fwrite((void*)"\r\n", sizeof(char), strlen("\r\n"), fp);
	fclose(fp);
	return 0;
}

static uchar * RtspFindStartMedia(uchar *Buffer, uint *Bufferlen)
{
	uint i = 0; 
	uint Bufferlenlocal = *Bufferlen;
	if (Bufferlenlocal > 64) Bufferlenlocal = 64;
	for (i = 0; i < Bufferlenlocal - 4 ; i++){
		if (Buffer[i + 0] == 0x00 &&
			Buffer[i + 1] == 0x00 &&
			Buffer[i + 2] == 0x00 &&
			Buffer[i + 3] == 0x01 &&
			(Buffer[i + 4] == 0x67 || Buffer[i + 4] == 0x61)){
			*Bufferlen -= i;
			return &Buffer[i];
		}
	}
	return NULL;
}

static int RtspCheckFrameHead(uchar * Buffer, int frameType)
{
	int i = 0;
	if (0 == frameType) {
		if (Buffer[i + 0] == 0x00 &&
			Buffer[i + 1] == 0x00 &&
			Buffer[i + 2] == 0x01 &&
			Buffer[i + 3] == 0xFC)
			return 0;
	} else if (1 == frameType) {
		if (Buffer[i + 0] == 0x00 &&
			Buffer[i + 1] == 0x00 &&
			Buffer[i + 2] == 0x01 &&
			Buffer[i + 3] == 0xFD)
			return 0;
	} 
	return -1;
}
#endif

CRtspVideo::CRtspVideo():CThread("RtspVideo", TP_NET)
{
	m_ptkBufMain.Reset(true);
	m_ptkBufSecond.Reset(true);
	m_packet_count = 0;
	m_bIsFirstFrame = 0;
	CreateThread();
}

CRtspVideo::~CRtspVideo()
{
	DestroyThread();
}

VD_INT32 CRtspVideo::RegisterVideo()
{
	_printd("RegisterVideo");
	return 0;
}

VD_INT32 CRtspVideo::OnVideoData(int iChannel, uint iCodeType, CPacket *pPacket)
{
	if (!pPacket) {
	   // VD_TRACE("CRtspVideo::OnVideoData : pPacket is NULL\n");
	    return 0;
	}
    
	PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());

	unsigned char * pBufSend = NULL;
	unsigned char ucFrameType = PACK_TYPE_FRAME_NULL;  /* 帧类型 */
	unsigned char ucFrameFlag = 0;                     /* 帧头尾的标识 */
	int usFramePos = 0;                     /* 帧开始位置 */
	uint usCFrameLength = 0;                 /* 帧在本块中的长度 */  
	uint usFrameLength = 0;
	pBufSend = pPacket->GetBuffer();
	for ( int iIndex = 0; iIndex < FRAME_MAX_NUM; iIndex++) {        
		ucFrameType = pPacketHead->FrameInfo[iIndex].FrameType;
		ucFrameFlag = pPacketHead->FrameInfo[iIndex].FrameFlag;
		usFramePos = pPacketHead->FrameInfo[iIndex].FramePos;
		usCFrameLength = pPacketHead->FrameInfo[iIndex].DataLength;
		usFrameLength = pPacketHead->FrameInfo[iIndex].FrameLength;
		
		if (ucFrameType == PACK_TYPE_FRAME_NULL) {
			break;
		} else if(PACK_TYPE_FRAME_AUDIO == ucFrameType) {
			continue;
		}
		
		if (CHL_MAIN_T == iCodeType) {
			if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
				m_ptkBufMain.Reset();
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				/*一个完整帧*/
				if (-2 == SendVideoData((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size(), iChannel, iCodeType,ucFrameType)) {
					_printd("CHL_MAIN_T add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				}
				m_ptkBufMain.Reset();
				continue;
			} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
				m_ptkBufMain.Reset();
				m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				continue;
			} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
				/* 只包含帧尾的帧 */
				if (m_ptkBufMain.Size() > 0) {
                			m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
					/*组合好一个完整帧*/
					if (-2 == SendVideoData((unsigned char *)(m_ptkBufMain.Buf()),m_ptkBufMain.Size(), iChannel, iCodeType,ucFrameType)) {
						_printd("CHL_MAIN_T add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
					}
					m_ptkBufMain.Reset();
				} else {
					tracepoint();
					_printd("Rtsp streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
					m_ptkBufMain.Reset();
				}
				continue;
			} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
				/* 不包含帧头和帧尾的帧 */
				if (m_ptkBufMain.Size() > 0) {
					m_ptkBufMain.Append(pBufSend + usFramePos, usCFrameLength);
				} else {
					tracepoint();
					m_ptkBufMain.Reset();					
				}
				continue;
			}
		} else if (CHL_2END_T == iCodeType) {
			if (PACK_CONTAIN_FRAME_HEADTRAIL == ucFrameFlag) {
				m_ptkBufSecond.Reset();
				m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				if (-2 == SendVideoData((unsigned char *)(m_ptkBufSecond.Buf()),m_ptkBufSecond.Size(), iChannel, iCodeType,ucFrameType)) {
						_printd("CHL_2END_T add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
				}
				m_ptkBufSecond.Reset();
				continue;
			} else if (PACK_CONTAIN_FRAME_HEAD == ucFrameFlag) {
				m_ptkBufSecond.Reset();
				m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				continue;
			} else if (PACK_CONTAIN_FRAME_TRAIL == ucFrameFlag) {
				if (m_ptkBufSecond.Size() > 0) {
                			m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
					if (-2 == SendVideoData((unsigned char *)(m_ptkBufSecond.Buf()),m_ptkBufSecond.Size(), iChannel, iCodeType,ucFrameType)) {
						_printd("CHL_2END_T add  Err=== addr[0x%X] len[%d] FrameType[%d]", pBufSend + usFramePos, m_ptkBufMain.Size() ,ucFrameType);
					}
					m_ptkBufSecond.Reset();
				} else {
					tracepoint();
					_printd("CHL_2END_T streamtype[%d] length[%lu] total[%lu] type[%d]",iCodeType, usCFrameLength, usFrameLength, ucFrameType);
					m_ptkBufSecond.Reset();
				}
				continue;
			} else if (PACK_CONTAIN_FRAME_NONHT == ucFrameFlag) {
				/* 不包含帧头和帧尾的帧 */
				if (m_ptkBufSecond.Size() > 0) {
					m_ptkBufSecond.Append(pBufSend + usFramePos, usCFrameLength);
				} else {
					tracepoint();
					m_ptkBufSecond.Reset();
				}
			}
		}
    }
    return 0;
}

#if 0
VD_INT32 CRtspVideo::SendVideoData(uchar *pData, unsigned int iLen, unsigned char iChannel, unsigned char iStreamType, unsigned char FrameType)
{
	uchar *startMedia = NULL;
	uint tmp = iLen;
	char errMsg[256] = "0";

	if (-1 == RtspCheckFrameHead(pData, FrameType)) {
		_printd("Vsip CheckFrameHead error, buffer address %x bufferlen = %d, type = %d ,FrameType = %d m_packet_count[%d]", pData, iLen, iStreamType, FrameType, m_packet_count);
		return -2;
	}
	startMedia = RtspFindStartMedia(pData, &tmp);
	if (NULL == startMedia) {
		_printd("Rtsp FindStartMedia error, buffer address %x bufferlen = %d, type = %d ,FrameType = %d m_packet_count[%d]", pData, iLen, iStreamType, FrameType, m_packet_count);
		//sprintf(errMsg, "Rtsp FindStartMedia ERR ====  streamType[%d], frameType[%d] buffLen[%d] m_packet_count[%d]", iChannel, iStreamType, iLen, m_packet_count);
		//RtspSaveErrToFile(errMsg);
		return -2;
	}
	RtspServerSendSingleStream(StVideo, (char *)pData, iLen, iChannel, iStreamType);
	return 0;
}
#endif

VD_INT32 CRtspVideo::SendVideoData(uchar *pData, unsigned int iLen, unsigned char iChannel, unsigned char iStreamType, unsigned char FrameType)
{
	char isIFrame = 0;
	char Head[8]  = {0};
	long long FrameTimeStamp;
	
	memcpy(Head,pData,8);	
	memcpy(&FrameTimeStamp,pData + iLen - 8,sizeof(long long));//SEI 00 00 00 01 06 + TimeStamp

	if(Head[0] == 0 && Head[1] == 0 && Head[2] == 0 && Head[3] == 0xfd)
	{
		_printd("I Frame!!!");
		isIFrame = 1;
	}


	if (iLen < 8)
		return -2;
	RtspServerSendSingleStream(EnuStreamTypeVideo, (unsigned char *)pData, iLen -13 , iChannel, iStreamType,(unsigned int)(FrameTimeStamp/1000));
	return 0;
}

VD_INT32 CRtspVideo::Start(int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture){
		_printd("Icapture::instance ok");
		pCapture->SetIFrame(StreamType);
		VD_BOOL bRet = pCapture->Start(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CRtspVideo::OnCapture, DATA_MONITOR,StreamType);
	}else{
		_printd("Icapture::instance ERror");
		return -1;
	}
	return 0;
}

VD_INT32 CRtspVideo::Stop (int Channel, int StreamType)
{
	ICapture *pCapture = ICapture::instance(Channel);
	if (NULL != pCapture){
		_printd("Icapture::instance Stop");
		pCapture->SetIFrame(StreamType);
		VD_BOOL bRet = pCapture->Stop(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CRtspVideo::OnCapture, DATA_MONITOR,StreamType);
	}else{
		_printd("Icapture::instance ERror");
		return -1;
	}
}

bool CRtspVideo::SendVideoToRtsp()
{
#if 0
	//用于清空数据链表
	m_packet_count = 0;
	m_mutex_list.Enter();
	
	while(1)
	{
		PACKET_LIST::iterator it = m_packet_list.begin();
		if (it == m_packet_list.end())
		{
			break;
		}

		pPacket = it->pPacket;
		m_packet_list.pop_front();
		pPacket->Release();
	}
	m_mutex_list.Leave();
#endif
	
	
	if (m_packet_count > 0) {
		CPacket* pPacket = NULL;
		uint dwStreamType = 0;
		int chn;

		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		chn = it->chn;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();
		
		OnVideoData(chn, dwStreamType, pPacket); 
		pPacket->Release();

		
		return true;
}
	return false;
}

void CRtspVideo::OnCapture(int iChannel, uint iStreamType, CPacket *pPacket)
{
	PACKET_CAPTURE packet_capture;
	packet_capture.chn = iChannel;
	packet_capture.pPacket = pPacket;
	packet_capture.dwStreamType = iStreamType;
	char errMsg[256] = "0";

	int iCount = g_nLogicNum;
	if (m_packet_count > iCount*50) {
		debugf("Rtsp capture list is too large discard it, size:%d, chn:%d!type:%d\n", m_packet_count, iChannel, iStreamType);
		//sprintf(errMsg, "Rtsp capture list is too large discard it size:%d, chn:%d!type:%d", m_packet_count, iChannel, iStreamType);
		//RtspSaveErrToFile(errMsg);
		return ;
	}

	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_capture);
	m_packet_count++;
	m_mutex_list.Leave();
}

void CRtspVideo::ThreadProc()
{
	bool bSleep = true;
	uint count_sleep = 0;
	
	while (m_bLoop) {
		bSleep = true;
		if (SendVideoToRtsp()) {
			bSleep = false;
		}
		//防止没有码流数据空跑,导致cpu利用率过高
		 if (bSleep) {
			count_sleep++;
			if (count_sleep > 0) {
				SystemSleep(1);
				count_sleep = 0;
			}
		} else {
		    count_sleep = 0;
		}
	}
}
