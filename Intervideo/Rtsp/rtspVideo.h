#ifndef _RTSPVIDEO__H
#define _RTSPVIDEO__H
#include "System/BaseTypedef.h"
#include "System/Object.h"
#include "Media/ICapture.h"
#include "Devices/DevCapture.h"
#include "System/UserManager.h"
#include "APIs/Net.h"
#include "System/ABuffer.h"
#include "RtspLibServer.h"
#include "avilib.h"

class CRtspVideo : public CThread
{
public:
	CRtspVideo ();
	~CRtspVideo();
public:
	VD_INT32 Start(int Channel, int StreamType);
	VD_INT32 Stop (int Channel, int StreamType);
		
private:
	VD_INT32 RegisterVideo();
	VD_INT32 OnVideoData(int iChannel, uint iCodeType, CPacket *pPacket);
	VD_INT32 SendVideoData(uchar *pData, unsigned int iLen, unsigned char iChannel, unsigned char iStreamType, unsigned char FrameType);
	void OnCapture(int iChannel, uint iStreamType, CPacket *pPacket);
	bool SendVideoToRtsp();
	void ThreadProc();
private:

typedef struct PACKET_CAPTURE {
        int chn;
        CPacket* pPacket;
        uint dwStreamType;
}PACKET_CAPTURE;
typedef std::list<PACKET_CAPTURE> PACKET_LIST;

	unsigned char m_bIsFirstFrame;
	uint m_packet_count;
	PACKET_LIST m_packet_list;
	CMutex m_mutex_list;
	CABuffer m_ptkBufMain;
	CABuffer m_ptkBufSecond;
};

#endif