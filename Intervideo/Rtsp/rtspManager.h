#ifndef _RTSPMANAGER__H
#define _RTSPMANAGER__H
#include "System/BaseTypedef.h"
#include "System/Object.h"

#include "avilib.h"
#include "RtspLibServer.h"
#include "rtspAudio.h"
#include "rtspVideo.h"
#include "Net/NetApp.h"
#include "System/Version.h"

class CRtspManger : public CObject
{
	public:
		PATTERN_SINGLETON_DECLARE(CRtspManger);
		CRtspManger();
		~CRtspManger();
	public:
		VD_INT32 Init();
		VD_INT32 Start();
		VD_INT32 Stop();
		VD_INT32 StartVideo(int Channel, int StreamType);
		VD_INT32 StopVideo(int Channel, int StreamType);
		VD_INT32 StartAudioIn(int Channel);
		VD_INT32 StartAudioOut(int Channel);
		VD_INT32 PutAudioOutData(int Channel, unsigned char *buf, int len);
		VD_INT32 StopAudioIn(int Channel);
		VD_INT32 StopAudioOut(int Channel);
//		VD_INT32 RtspCallBack (CallBackType cbt, void *args1,  void *args2);
	private:
		CRtspVideo 	m_RtspVideo;
		CRtspAudio 	m_RtspAudio;
	private:
		SetServiceArgs arg;

	#ifdef VSERVER
		VServerParam *Vserver;
	#endif

	//	unsigned int VideoStartEnable;
	//	unsigned int AudioStartEnable;
		unsigned int MaxClientNum;
		unsigned int RtspServerPort;
		unsigned int IsAuthorization;
	//	int (* CallBack)(CallBackType cbt, void *args1,  void *args2);
};

#define g_Rtsp (*CRtspManger::instance())

#endif