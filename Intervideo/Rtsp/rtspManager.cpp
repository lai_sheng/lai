#include "rtspManager.h"
PATTERN_SINGLETON_IMPLEMENT(CRtspManger);
CRtspManger::CRtspManger()
{
	_printd("CRtspManger");
}
CRtspManger::~CRtspManger()
{
	_printd("~CRtspManger");
}

int RtspCallBack(CallBackType cbt, EventCallBackArgsT *Ecba)
{
	int channel, StreamType;
	char *ClientIp;
	char *username, *password;
	static int MainVideoStartEnable = 0;
	static int SecondVideoStartEnable = 0;
	static int AudioStartEnable = 0;
	switch(cbt)
	{
		case CbtVisitIp:
			//ClientIp = (char *)args1;
			//_printd("Ip Comming [%s]\n", ClientIp);
		break;
		case CbtUserPasswd:
			//username = (char *)args1;
			//password = (char *)args2;
			//_printd("comming user [%s] passwd [%s]\n", username, password);
		break;
		case CbtVisitVideo:
			channel = Ecba->iChannel;
			StreamType = Ecba->iStreamType;
			_printd ("Rtsp Open Video");
			_printd("~~~~~~~~~~~~~~~~~~~~~~~~~");
			_printd("channel = %d  StreamType = %d",channel,StreamType);
			_printd("~~~~~~~~~~~~~~~~~~~~~~~~~");
			if(0 == MainVideoStartEnable && 0 == channel && 0 == StreamType){
				g_Rtsp.StartVideo(0,0);
				MainVideoStartEnable = 1;
			}
			else if (0 == SecondVideoStartEnable && 0 == channel && 1 == StreamType){
				g_Rtsp.StartVideo(0,1);
				SecondVideoStartEnable = 1;
			}
		break;

		case CbtVisitAudio:
#ifndef CHIP_A5S
			_printd ("Rtsp Open Audio");
			if (AudioStartEnable == 0){
				g_Rtsp.StartAudioIn(0);
			//	g_Rtsp.StartAudioIn(1);
				AudioStartEnable = 1;
			}
#endif
			break;
	}
	return 0;

}

#ifdef VSERVER   //add by kyle xu in 20160107

static PTZ_OPT_STRUCT g_Ptz_cmd = {PTZ_OPT_NONE, 0, 0, 0, 0};

int VCallBack(VCallBackType cbt, EventVCallBackArgsT *pEcba)
{
	PTZ_OPT_STRUCT	PtzStruct;
	static int StopFlag = 0;

	if(pEcba == NULL)
	{
		_printd("VCallBack Error Attr!\n");
		return -1;
	}
	
	switch(cbt)
	{
		case GetDeviceIP:
		{
			//sprintf(pEcba->DeviceIP, "192.168.1.102");
			IPInfo info;
			g_NetApp.GetNetIPInfo("eth0", &info);
			sprintf((char *)(pEcba->DeviceIP), "%d.%d.%d.%d", info.HostIP.c[0], info.HostIP.c[1], info.HostIP.c[2], info.HostIP.c[3]);
			_printd("VCallBack HostIP:%s\n",pEcba->DeviceIP);
		}
		break;

		case GetDeviceMAC:
		{
			//sprintf(pEcba->DeviceMac, "0062B3D42512");
			char MacBuf[24] = "";
			int i = 0;
			
			NetGetMAC("eth0", MacBuf, 24);

			for(i = 0;i < 6;i ++) //有六组需要拷贝的数据
			{
				strncpy(pEcba->DeviceMac + 2 * i ,MacBuf + 3 * i,2); 
			}

			_printd("VCallBack DeviceMac = %s", MacBuf);
		}
		break;

		case GetDeviceModel:
		{
			sprintf(pEcba->DeviceModel, "CSN-5520ASWDR20XIR_full");
			_printd("VCallBack DeviceModel = %s", pEcba->DeviceModel);
		}	
		break;

		case GetDeviceVersion:
		{
			//sprintf(pEcba->DeviceVersion, "V1.01_S2L66_123");
			char Version[64] = {0};
			char *p = NULL;
			
			CVersion::instance()->GetVersion(Version, sizeof(Version));
			p = strchr(Version,'V');  //查找到V开头的地址
			strncpy(pEcba->DeviceVersion,p,strlen(p) - 2);//尾端是.R，需要去掉
			_printd("VCallBack DeviceVersion = %s", pEcba->DeviceVersion);
		}
		break;
		case GetVServerIP:
		{
			//sprintf(pEcba->VServerIP, "211.139.201.225");

			CConfigNetVServer *pCfgVServer = new CConfigNetVServer();
   
		    pCfgVServer->update(0);
		        
		    CONFIG_NET_VSERVER &cfgVServer= pCfgVServer->getConfig(0);
	
			Ip2Str(cfgVServer.Server.ip.l,pEcba->VServerIP);
		
			delete pCfgVServer; 
			_printd("VCallBack VServerIP = %s", pEcba->VServerIP);
		}
		break;
		case GetVServerPort:
		{
			//sprintf(pEcba->VServerPort, "554");
			CConfigNetVServer *pCfgVServer = new CConfigNetVServer();
   
		    pCfgVServer->update(0);
		        
		    CONFIG_NET_VSERVER &cfgVServer= pCfgVServer->getConfig(0);
	
			sprintf(pEcba->VServerPort,"%d",cfgVServer.Server.Port);
		
			delete pCfgVServer; 
			
			_printd("VCallBack VServerPort = %s", pEcba->VServerPort);
		}
		break;
		case CbtPTZ:
		{
			if(PtzStop != pEcba->DevicePTZ.cmd)
			{
				StopFlag = 0;
			}
			
			switch(pEcba->DevicePTZ.cmd)
			{
				case PtzStop:
				{
					if(1 == StopFlag)
					{
						return 0;
					}
				
					_printd("PTZ Stop");	
					g_Ptz.Stop(0, &g_Ptz_cmd);
					StopFlag = 1;
					
				}	
				break;
				case PtzUp:
				case PtzDown:
				case PtzLeft:
				case PtzRight:
				{
					switch(pEcba->DevicePTZ.cmd)
					{
						case PtzUp:
						{
							_printd("PTZ UP Speed %d", pEcba->DevicePTZ.speed);
							PtzStruct.cmd = PTZ_OPT_UP;
						}
						break;
						case PtzDown:
						{
							_printd("PTZ DOWN Speed %d", pEcba->DevicePTZ.speed);
							PtzStruct.cmd = PTZ_OPT_DOWN;
						}
						break;
						case PtzLeft:
						{
							_printd("PTZ Left Speed %d", pEcba->DevicePTZ.speed);
							PtzStruct.cmd = PTZ_OPT_LEFT;
						}
						break;
						case PtzRight:
						{
							_printd("PTZ Right Speed %d", pEcba->DevicePTZ.speed);
							PtzStruct.cmd = PTZ_OPT_RIGHT;
						}
						break;
					}

					PtzStruct.arg1 =pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzZoomTele:
				{
					_printd("PTZ ZoomTele Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_ZOOM_TELE;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzZoomWide:
				{
					_printd("PTZ ZoomWide Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_ZOOM_WIDE;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzFocusFar:
				{
					_printd("PTZ FocusFar Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_FOCUS_FAR;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzFoucsNear:
				{
					_printd("PTZ FoucsNear Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_FOCUS_NEAR;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzIrisOpen:
				{
					_printd("PTZ IrisOpen Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_IRIS_LARGE;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzIrisClose:
				{
					_printd("PTZ IrisClose Speed %d", pEcba->DevicePTZ.speed);
					PtzStruct.cmd = PTZ_OPT_IRIS_SMALL;
					PtzStruct.arg1 = pEcba->DevicePTZ.speed;
					memcpy(&g_Ptz_cmd, &PtzStruct, sizeof(PTZ_OPT_STRUCT));
					g_Ptz.Start(0, &PtzStruct);
				}
				break;
				case PtzAuto:
				{
					_printd("PTZ Auto Speed %d", pEcba->DevicePTZ.speed);
				}
				break;
				default:
					_printd("ptz default");
					break;
			}
		}
		break;
		default:
			_printd("cbt default");
			break;
	}
	return 0;
}
#endif

VD_INT32 CRtspManger::Init()
{
	MaxClientNum = 10;
	RtspServerPort = 554;
	IsAuthorization = 0x00;
	//VideoStartEnable = 0;
	//AudioStartEnable = 0;
	//CallBack = RtspCallBack;
	RtspServerInit();

	arg.iValue = MaxClientNum;
	RtspServerSet(StMaxClientComeIn, &arg);
	arg.iValue = RtspServerPort;
	RtspServerSet(StRtspServicePort, &arg);
	arg.iValue = IsAuthorization;
	RtspServerSet(StSetAuthorization, &arg);
	arg.EventCallBack= RtspCallBack;
	RtspServerSet(StEventCallBack, &arg);

	RtspRegisterStream(EnuStreamTypeVideo, 0, 0, NULL, 0);
	RtspRegisterStream(EnuStreamTypeVideo, 0, 1, NULL, 0);
#ifndef CHIP_A5S
	RtspRegisterStream(EnuStreamTypeAudio, 0, 0, NULL, 0);
	RtspRegisterStream(EnuStreamTypeAudio, 0, 1, NULL, 0);
#endif

#ifdef VSERVER
	Vserver = GetVServerParam();
	Vserver->EventVCallBack = VCallBack;
#endif

	return 0;
}

VD_INT32 CRtspManger::Start()
{
	Init();
	RtspServerStart();

#ifdef VSERVER
	RtspVServerStart();
#endif
	
	return 0;
}
VD_INT32 CRtspManger::Stop()
{
	_printd("CRtspManger :: Stop ");
	return 0;
}

VD_INT32 CRtspManger::StartVideo(int Channel, int StreamType)
{
	_printd("CRtspManger::StartVideo");
	m_RtspVideo.Start(Channel,StreamType);
	return 0;
}
VD_INT32 CRtspManger::StopVideo(int Channel, int StreamType)
{
	_printd("CRtspManger::StopVideo");
	m_RtspVideo.Stop(Channel, StreamType);
	return 0;
}
VD_INT32 CRtspManger::StartAudioIn(int Channel)
{
	_printd("CRtspManger::StartAudioIn");
	m_RtspAudio.StartAudioIn(Channel);
	return 0;
}
VD_INT32 CRtspManger::PutAudioOutData(int Channel, unsigned char *buf, int len)
{
	_printd("CRtspManger::PutAudioOutData");
	m_RtspAudio.PutAuidoOutData(Channel, buf, len);

	return 0;
}
VD_INT32 CRtspManger::StartAudioOut(int Channel)
{
	_printd("CRtspManger::StartAudioOut");
	m_RtspAudio.StartAudioOut(Channel);
	return 0;
}
VD_INT32 CRtspManger::StopAudioIn(int Channel)
{
	_printd("CRtspManger::StopAudioIn");
	m_RtspAudio.StopAudioIn(Channel);
	return 0;
}
VD_INT32 CRtspManger::StopAudioOut(int Channel)
{
	_printd("CRtspManger::StopAudioOut");
	m_RtspAudio.StopAudioOut(Channel);
	return 0;
}

