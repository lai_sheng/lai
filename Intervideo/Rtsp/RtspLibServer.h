#ifndef _RTSPLIBSERVER_H_
#define _RTSPLIBSERVER_H_

#ifdef __cplusplus  
extern "C" {  
#endif
#include <stdio.h>
#include <string.h>
#ifndef WIN32
#include<sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h> 
#include <netdb.h> 
#include <semaphore.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/sysinfo.h>
#include <arpa/inet.h>
#include <netinet/in.h> 
#include <netinet/tcp.h>
#include <net/if.h>
#else
#include <sys/timeb.h>
#include <winsock2.h>
#include <ws2def.h>
#include <ws2ipdef.h>
#include <Windows.h>
//#include "RtspType.h"

#pragma warning (disable: 4005)
#endif
typedef enum{
	StMaxClientComeIn,
	StRtspServicePort,
	StBindLocalIp,
	StEventCallBack,
	StStreamCallBack,
	StSetAuthorization,
}SETTYPE;

typedef enum {
	RTP_OVER_INVALID	= 0X00,
	RTP_OVER_TCP,
	RTP_OVER_UDP,
	RTP_OVER_MULTICAST,
	RTP_OVER_AUTO,
}ENU_RTP_OVER_TYPE;

typedef enum {
	EnuFrameInvalid = 0x00,
	EnuFrameP		= 0x01 << 15, //这个值不能更改与nal_unit_type值不能相同
	EnuFrameB		= 0x01 << 16,
	EnuFrameI		= 0x01 << 17,
}ENU_FRAME_TYPE;

typedef enum{
	CbtInvalid			= 0x00,
	CbtVisitIp			= 0x01 << 1,
	CbtUserPasswd		= 0x01 << 2,
	CbtVisitVideo		= 0x01 << 3,
	CbtVisitAudio		= 0x01 << 4,
	CbtConnectDrop		= 0x01 << 5,
	CbtUserPasswdErr	= 0x01 << 6, 
	CbtConnectOk		= 0x01 << 7,
	CbtConnectErr		= 0x01 << 8,
	CbtCloseOk			= 0x01 << 9,
}CallBackType;

typedef enum{
	PtzStop			= 0x00,
	PtzLeft			= 0x01 << 1,
	PtzRight			= 0x01 << 2,
	PtzUp			= 0x01 << 3,
	PtzDown		= 0x01 << 4,
	PtzZoomTele		= 0x01 << 5,
	PtzZoomWide	= 0x01 << 6, 
	PtzIrisOpen		= 0x01 << 7,
	PtzIrisClose		= 0x01 << 8,
	PtzFocusFar		= 0x01 << 9,
	PtzFoucsNear	= 0x01 << 10,
	PtzAuto			= 0x01 << 11
}PTZ_CMD_TYPE;

typedef enum{
	CbtPTZ				= 0x01 << 1,
	GetDeviceIP			= 0x01 << 2,
	GetDeviceMAC		= 0x01 << 3,
	GetDeviceModel		= 0x01 << 4,
	GetDeviceVersion		= 0x01 << 5,
	GetVServerIP		= 0x01 << 6,
	GetVServerPort		= 0x01 << 7,
}VCallBackType;

typedef enum{
	EnuStreamFormatYuv420,
	EnuStreamFormatH264,
	EnuStreamFormatH265,
	EnuStreamFormatMjpeg,
	EnuStreamFormatG711A,
	EnuStreamFormatG711U,
	EnuStreamFormatG726,
	EnuStreamFormatAcc,
}ENU_STREAM_FORMAT;

typedef enum {
	EnuStreamTypeVideo,
	EnuStreamTypeAudio,
}ENU_STREAM_TYPE;

typedef struct _StreamDataInfo
{
	unsigned char		*StreamData;
	unsigned int		StreamDatalen;
	unsigned int		Framenum;
	unsigned int		FrameWidth;
	unsigned int		FrameHigh;
	unsigned char		FrameRate;

	ENU_STREAM_TYPE		StreamType;
	ENU_FRAME_TYPE		FrameType;
	ENU_STREAM_FORMAT	StreamFormat;
	struct timeval		FrameTimev;
	unsigned int		SrcInfo;//视频流源描述。
}StreamDataInfo_t;

typedef struct {
	char	RtspUrl[128];
	void	*UsrArgs;//注意此位置的内存使用限制（全局或者 malloc)。
	int		iChannel;
	int		iStreamType;
	char	UsrName[32];
	char	Passwd[32];
	ENU_RTP_OVER_TYPE Erot;
}OpenRtspArgs;

typedef struct {
	int cmd;
	int speed;
}EventVCallBackDevicePTZ;

typedef struct {
	char VServerIP[16];
	char VServerPort[12];
	char DeviceIP[16];
	char DeviceMac[16];
	char DeviceModel[32];
	char DeviceVersion[64];
	EventVCallBackDevicePTZ DevicePTZ;
}EventVCallBackArgsT;

typedef struct {
	int		iChannel;
	int		iStreamType;
	void	*UsrArgs;
	char	UserName[32];
	char	Passwd[32];
	char	VisitIp[32];
	int		StatusCode;

}EventCallBackArgsT;



typedef union{
	int iValue;
	int (* EventCallBack)(CallBackType cbt, EventCallBackArgsT *Ecba);
	int (* StreamCallBack)(ENU_STREAM_TYPE est, int iChannel, int StreamType, StreamDataInfo_t *Sdi, void *UsrArgs);
}SetServiceArgs;

typedef struct
{
	int (* EventVCallBack)(VCallBackType cbt, EventVCallBackArgsT *Ecba);
}VServerParam;

char *RtspGetVersion();
VServerParam *GetVServerParam();
int RtspServerInit();
int RtspServerStart();
int RtspVServerStart();
int RtspServerStop();
int RtspServerSet(SETTYPE st, SetServiceArgs *Args/*inet_addr*/);
//主有一个视频源。如IPC使用这个函数。节省非常多的CPU
int RtspServerSendSingleStream(ENU_STREAM_TYPE st, unsigned char *Buffer, unsigned int Bufferlen, unsigned int iChannel, unsigned int iStreamType,unsigned int timestamp);
int RtspRegisterStream(ENU_STREAM_TYPE st, int Channel, int Streamtype, char *StreamInfo, int streamlen);

//int RtspDelClientConnect(RtspServerClientsInfo *p);

int RtspClientsServiceInit();
int RtspClientsServiceStart();
int RtspClientsServiceStop();
int RtspClientsServiceSet(SETTYPE st, SetServiceArgs *Args/*inet_addr*/);


int rc_open(OpenRtspArgs *Ora);
int rc_close(int rc_ssid/*rc_open 返回值*/);

#ifdef __cplusplus  
}  
#endif 


#endif
