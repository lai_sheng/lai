#include "rtspAudio.h"

CRtspAudio::CRtspAudio()
{
	_printd("CGooLinkAudio");
}
CRtspAudio::~CRtspAudio()
{
	_printd("~CGooLinkAudio");
}

VD_INT32 CRtspAudio::StartAudioOut(int channel)
{
	_printd("AudioOut Start");
	return 0;
}
VD_INT32 CRtspAudio::OnAudioData(int iChannel, CPacket *pPacket)
{
	long long FrameTimeStamp;
	uchar *pData = NULL;
	int	 Len;
	
	memcpy(&FrameTimeStamp,pPacket->GetBuffer() + pPacket->GetLength() - sizeof(long long), sizeof(long long));

	pData	= pPacket->GetBuffer() + 8 + sizeof(unsigned int) ;
	Len 	= pPacket->GetLength() - 8 - sizeof(unsigned int)  - sizeof(long long);
//+7  ȥ��AACͷ
	int iRet = RtspServerSendSingleStream(EnuStreamTypeAudio, pData + 7, Len - 7, 0, 0,(unsigned int)(FrameTimeStamp/1000));
	//iRet = RtspServerSendSingleStream(EnuStreamTypeAudio, (unsigned  char *)(pPacket->GetBuffer() + 8), pPacket->GetLength() - 8, 0, 1);
	static int mark = 0;
	iChannel = 0;
	if (mark %100 == 0){
		//_printd("have audio data length = %d iRet = %d", pPacket->GetLength(), iRet);
	}
	mark++;
	return 0;
}
VD_INT32 CRtspAudio::StartAudioIn(int channel)
{
	_printd ("AudioIn Start");
	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	pCapture->Start(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRtspAudio::OnAudioData);
		
	return 0;
}
VD_INT32 CRtspAudio::StopAudioOut(int channel)
{
	_printd(" StopAudioOut");
	return 0;
}
VD_INT32 CRtspAudio::StopAudioIn(int channel)
{
	_printd ("StopAudioIn");
	CDevAudioIn *pCapture = CDevAudioIn::instance(channel);
	pCapture->Stop(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRtspAudio::OnAudioData);
	return 0;
}

VD_INT32 CRtspAudio::SendAuidoInData()
{
	return 0;
}
VD_INT32 CRtspAudio::PutAuidoOutData(int channel, unsigned char *buf, unsigned int len)
{
	g_AudioManager.PlayVoice(buf, len, AUDIO_TALK_TYPE, G711_ALAW);
	return 0;
}
