#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include "APIs/Audio.h"
#include "APIs/Capture.h"
#include "Devices/DevInfo.h"
#include "Functions/Record.h"
#include "Functions/FFFile.h"
#include "Functions/LedStat.h"
#include "Functions/WifiLink.h"
#include "Devices/DevExternal.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigCamera.h"
#include "Configs/ConfigEvents.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigAwakeTime.h"
#include "Functions/DriverManager.h"
#include "Configs/ConfigVideoColor.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Functions/AiUploadManager.h"
#include "Intervideo/RealTime_callback.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/LiveCam/LiveCamTrans.h"
#include "Functions/PtzTrace.h"
#include "System/UserManager.h"
#include "System/User.h"
#include "APIs/CommonBSP.h"
#include "Functions/MP3Play.h"
#include "APIs/CommonBSP_Battery.h"
#if defined(FUNC_WDT) 
#include "Functions/WatchDogTime.h"
#endif
#ifdef HMSOTA
#include "Configs/ConfigNet.h"
#include "Functions/HMSOTA/HMSManager.h"
#include "Functions/HMSOTA/OTAManager.h"
#endif

#ifdef IPC_JZ
#include "APIs/ExtDev.h"
#endif

#ifdef DOOR_LOCK
#include "APIs/LockApi.h"
#endif
#ifdef  SUNING
#include "Intervideo/RecordTs/RecordTsManager.h"
#include "Intervideo/RecordTs/SNDeviceAttr.h"
#endif
//#ifdef _cplusplus
extern "C" {
//#endif
SYSTEM_TIME *TimeOnChange(SYSTEM_TIME *timeout,struct tm *timein)
{
	timeout->year = timein->tm_year;
	timeout->month = timein->tm_mon;
	timeout->day = timein->tm_mday;
	timeout->hour = timein->tm_hour;
	timeout->minute = timein->tm_min;
	timeout->second = timein->tm_sec;
	timeout->isdst = timein->tm_isdst;
	timeout->wday = timein->tm_wday;

	return timeout;
}

char * TaskSearchListDirFileName(char *path)  
{  
    int               i 	= 0; 
    DIR              *pDir 	= NULL;  
    struct dirent    *ent 	= NULL;   
    char              childpath[512] = "";  

    pDir = opendir(path);  

	if (pDir == NULL)
	   return NULL;
	
    memset(childpath,0,sizeof(childpath));  


    while((ent=readdir(pDir))!=NULL)  
    {  
        if (strstr(ent->d_name,".jpg") != NULL)
  		{
  			closedir(pDir);
  			return ent->d_name;
        }
    }  

	closedir(pDir);
	return NULL;
} 

int TaskSearchImageFilePath(char *filename,char *path)
{
	sprintf(path,"%s/%s",IMAGE_DIR,filename);
	if(access(path,F_OK) == 0)
		return 0;
	else
		return -1;	
}


int TransFileListPerPage(uint8_t channel,RTFileListPerPage *filelist,int NumsOfPerPage,char LastFileName[64])
{
	struct  stat fbuf;
	char 	FileName[64] 	= {0};
	char 	*p 		= NULL;
	char 	PreFileName[64] = {0};
	int  	mCnt 	= 0;
	int 	tmp 	= 0;
	int		n 		= 1;
	int 	i       = 0;
	
	SYSTEM_TIME StartTime;
	SYSTEM_TIME EndTime;
	VideoFiles  List;
	
	if(access(VIDEO_DIR,F_OK) != 0)
		return -1;

	if( 0 == strcmp(LastFileName, "0") || LastFileName == NULL || LastFileName[0] == '0' || LastFileName[0] == 0 || LastFileName[0] == '\0')
	{
		tmp = file_get_all_videolist_bigsort(&List,NULL,NULL);
	}
	else
	{
    	p = strtok(LastFileName,"_");
		if(p != NULL)
		{
			printf("LastFileName:%s\n",p);
			sscanf(p,"%04d%02d%02d%02d%02d%02d",&EndTime.year,&EndTime.month,&EndTime.day,
				&EndTime.hour,&EndTime.minute,&EndTime.second);
			memcpy(&StartTime,&EndTime,sizeof(SYSTEM_TIME));
			StartTime.year -= 1;
			printf("~~~~~~~~~~%4d-%02d-%02d %02d:%02d:%02d",StartTime.year,StartTime.month,StartTime.day,
				StartTime.hour,StartTime.minute,StartTime.second);
		}
		else	return -1;
		
		p = NULL;
	
		tmp = file_get_all_videolist_bigsort(&List,&StartTime,&EndTime);
	}
	printf("FileCount:%d\n",tmp);
	if(tmp < 0)
	{
		filelist->FileCount = 0;
		return -1;	
	}	
		
	if(NumsOfPerPage <= 8)	NumsOfPerPage = 8;
			
	n = tmp > NumsOfPerPage? NumsOfPerPage:tmp;

	for(i = 0;i < n;i ++)
	{
		memset(FileName,0,64);
		strcpy(FileName,List.FilesList[i].FileName);
		
		if (NULL != strstr(FileName,".mp4"))
		{
			memset(PreFileName,0,64);
			memcpy(PreFileName,FileName,strlen(FileName)-strlen(".mp4"));
		}
		if (NULL == strstr(FileName,"_"))
			break;

    	p = strtok(PreFileName,"_");
		mCnt = 0;
		while (p != NULL)
		{
			mCnt++;

			if (mCnt > 4)
				break;

			if (mCnt == 1)
			 	strcpy(filelist->fileinfo[i].FileCreateTime ,p);
			else if (mCnt == 2)
			{
				int tmp = atoi(p);
				filelist->fileinfo[i].RecordDuration = ((tmp / 100) * 60) + ( tmp %100 );
			}	

			p = strtok(NULL,"_");				 
		}
		p = NULL;
		
		memset(filelist->fileinfo[i].FileName, 0, RT_VIDEO_NANME_LEN);
		strcpy(filelist->fileinfo[i].FileName, FileName);

		memset(FileName,0,64);
		sprintf(FileName,"%s/%s",List.FilesList[i].DirPath,List.FilesList[i].FileName);
		memset(&fbuf,0,sizeof(struct stat));
		stat(FileName,&fbuf);
		filelist->fileinfo[i].FileSize		= fbuf.st_size;
		filelist->fileinfo[i].FileTypeMask  = 1;			
	}
	
	filelist->FileCount = i;	

	return 0;
}

int GetEarilestVideoFile(RTFileListPerPage *filelist)
{
	char *p = NULL;
	int tmp,dur;
	struct stat fbuf;
	VideoFiles  List;
	char FileName[128] = {0};
	
	if(access(VIDEO_DIR,F_OK) != 0 || filelist == NULL)
		return -1;

	tmp = file_get_earlist_videolist(&List,NULL,NULL);

	if(tmp < 0)
	{
		printf("FileCount:%d\n",tmp);
		return -1;	
	}		

	filelist->FileCount = 1;
	strncpy(filelist->fileinfo[0].FileName,List.FilesList[0].FileName,64);
	strncpy(FileName,List.FilesList[0].FileName,64);
	p = strtok(FileName,"_");
	
	if(p != NULL)
	{
		strcpy(filelist->fileinfo[0].FileCreateTime ,p);
		p = strtok(NULL,"_");	
		if (p != NULL)
		{
			dur = atoi(p);
			filelist->fileinfo[0].RecordDuration = ((dur / 100) * 60) + ( dur %100 );
		}	
	}
	p = NULL;

	memset(FileName,0,128);
	memset(&fbuf,0,sizeof(struct stat));
	sprintf(FileName,"%s/%s",List.FilesList[0].DirPath,List.FilesList[0].FileName);
	stat(FileName,&fbuf);
	filelist->fileinfo[0].FileSize		= fbuf.st_size;
	filelist->fileinfo[0].FileTypeMask  = 1;			

//	printf("FileName:%s,RecordDuration:%d,FileSize:%d\n",FileName,
//		filelist->fileinfo[0].RecordDuration,filelist->fileinfo[0].FileSize);
	
	return 0;
}


int32_t RealTimeGetUUID_Callback(char uuid[RT_UUID_LEN])
{
	if(uuid == NULL)	
		return -1;
	memset(uuid, 0, RT_UUID_LEN);		

	if( 0 > GetUuid(uuid) || uuid[0] == '0' || uuid[0] == 0 )
		return -1;
	return 0;
}


#define BR_HDMODE 0     //??
#define BR_SDMODE 1     //??		
#define BR_LUMODE 2     //??

#ifdef IPC_JZ_NEW
#ifdef FAC_CHECK
const int Bitrate_T10[3] = {1000,512,256};
const int Bitrate_T20[3] = {2000,800,256};
#else
#ifdef IPC_JZ_T21
const int Bitrate_T10[3] = {800,512,256};
const int Bitrate_T20[3] = {900,800,256};
#else
const int Bitrate_T10[3] = {800,512,256};
const int Bitrate_T20[3] = {1000,800,256};
#endif
#endif
#endif

#ifdef IPC_JZ
const int Bitrate_100W[3] = {800,512,256};
const int Bitrate_200W[3] = {1200,800,256};
#endif

int32_t RealTimeVideoOpen_Callback(uint8_t channel,RT_EncodeMode eCodeMode,uint32_t SuggestBitRate,RealTimeVideoDataFormat* videoinfo)
{	
	int Ret 		 = 0;
	int nfps   = 0;
	int igop   = 0;
	
	uint32_t BitRate = SuggestBitRate;
	
	CConfigEncode* pCfgEncode = new CConfigEncode();
	pCfgEncode->update();
	CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig();

	Get_Video_Nfps_Igop(&nfps, &igop);

	videoinfo->codec 									= RT_CODEC_H264;
	videoinfo->framerate 								= nfps;
	videoinfo->frameInterval 							= igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iGOP = igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = nfps;

	
	videoinfo->colorDepth		= 24;

	//_printd("eCodeMode:%d",eCodeMode);
	//CAPTURE_CAPS Caps;
	//CaptureGetCaps(&Caps);
	
	#ifdef FAC_CHECK  //fac_check  720P=1M  1080P=2M
	eCodeMode = RT_HDMODE;
	#endif

	switch(eCodeMode)
	{
		case RT_HDMODE:
		{
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_1080P;
				BitRate = Bitrate_T20[BR_HDMODE];//800;
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;				
			}
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_T10[BR_HDMODE];//800;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				 _printd("RT_HDMODE 720P Mode");
				 cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution  = CAPTURE_SIZE_720P;
				 BitRate = Bitrate_100W[BR_HDMODE];
				 videoinfo->width  = 1280;
				 videoinfo->height = 720;
				 cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("1080P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_1080P;
				BitRate =  Bitrate_200W[BR_HDMODE];
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;					
			}			
			#else
			 _printd("RT_HDMODE 720P Mode");
			 cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution  = CAPTURE_SIZE_720P;
			 BitRate = 800;
			 videoinfo->width  = 1280;
			 videoinfo->height = 720;
			 cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;			
			#endif
		}
		break;
		case RT_SDMODE:
		{
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_T20[BR_SDMODE];//600;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_SDMODE];//512;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("640x360P RT_SDMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_SDMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;		
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("720P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_200W[BR_SDMODE];
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			}
			#else
			_printd("640x360P RT_SDMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
			BitRate = 512;
			videoinfo->width  = 640;
			videoinfo->height = 360;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			#endif
		}
		break;
		case RT_LUMODE:
		{
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_LUMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T20[BR_LUMODE];//600;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;				
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_LUMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_LUMODE];//600;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_LUMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;	
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_200W[BR_LUMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;		
			}
			#else
			_printd("RT_LUMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
			BitRate = 256;
			videoinfo->width  = 640;
			videoinfo->height = 360;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;		
			#endif
		}
		break;
		default:
		{
			Ret = -1;
		}
		break;
	}
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate 	= BitRate;
	videoinfo->bitrate =  BitRate;
	if(Ret != -1)	pCfgEncode->commit();

	delete pCfgEncode;

	
	return 0;
}

int32_t RealTimeSetVideoInfo_Callback(uint8_t channel,RT_EncodeMode eCodeMode,uint32_t SuggestBitRate, RealTimeVideoDataFormat* videoinfo)
{	
	int Ret 		 = 0;
	uint32_t BitRate = SuggestBitRate;
	
	CConfigEncode* pCfgEncode = new CConfigEncode();
	pCfgEncode->update();
	CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig();

	int nfps   = 0;
	int igop   = 0;
	Get_Video_Nfps_Igop(&nfps, &igop);
		
	videoinfo->framerate 								= nfps;
	videoinfo->frameInterval 							= igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iGOP = igop;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = nfps;	
	
	videoinfo->codec = RT_CODEC_H264;
	videoinfo->colorDepth = 24;

	//CAPTURE_CAPS Caps;
	//CaptureGetCaps(&Caps);
	//_printd("eCodeMode:%d",eCodeMode);
	#ifdef FAC_CHECK  //fac_check  720P=1M  1080P=2M
	eCodeMode = RT_HDMODE;
	#endif
	
	switch(eCodeMode)
	{
		case RT_HDMODE:
		{			
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_1080P;
				BitRate = Bitrate_T20[BR_HDMODE];//800;
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;				
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_HDMODE");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_T10[BR_HDMODE];//800;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_HDMODE 720P Mode");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_100W[BR_HDMODE];
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_HDMODE 1080P Mode");
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_1080P;
				BitRate = Bitrate_200W[BR_HDMODE];
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			}
			#else
			_printd("RT_HDMODE 720P Mode");
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
			BitRate = 800;
			videoinfo->width  = 1280;
			videoinfo->height = 720;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 1;
			#endif
		}
		break;
		case RT_SDMODE:
		{			
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_T20[BR_SDMODE];//600;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_SDMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_SDMODE];//512;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("640x360P RT_SDMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_SDMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("720P RT_SDMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_720P;
				BitRate = Bitrate_200W[BR_SDMODE];
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;
			}
			#else
			_printd("640x360P RT_SDMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
			BitRate = 512;
			videoinfo->width  = 640;
			videoinfo->height = 360;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 2;			
			#endif
		}
		break;
		case RT_LUMODE:
		{
			#ifdef IPC_JZ_NEW
			if (SensorSC2235 == PlatformGetHandle()->Sensor &&
				ProductM_A3  != PlatformGetHandle()->ProductModel){
				_printd("1080P RT_LUMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T20[BR_LUMODE];//600;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;				
			}			
			else if (SensorJXH62 == PlatformGetHandle()->Sensor ||
				     ProductM_A3 == PlatformGetHandle()->ProductModel){
				_printd("720P RT_LUMODE");	
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_T10[BR_LUMODE];//600;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;				
			}
			#elif IPC_JZ
			if (BAT_SensorOV9732 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_100W[BR_LUMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;
			} else if (BAT_SensorSC2235 == BAT_PlatformGetHandle()->Sensor) {
				_printd("RT_LUMODE");			
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
				BitRate = Bitrate_200W[BR_LUMODE];
				videoinfo->width  = 640;
				videoinfo->height = 360;
				cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;			}
			#else
			_printd("RT_LUMODE");			
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution	= CAPTURE_SIZE_360P;
			BitRate = 256;
			videoinfo->width  = 640;
			videoinfo->height = 360;
			cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = 3;			
			#endif
		}
		break;
		default:
		{
			Ret = -1;
		}
		break;
	}
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate = BitRate;
	videoinfo->bitrate =  BitRate;
	if(Ret != -1)	pCfgEncode->commit();

	delete pCfgEncode;

	return Ret ;
}
int32_t RealTimeGetVideoCurInfo_Callback(uint8_t channel,RT_EncodeMode *pCodeMode, RealTimeVideoDataFormat* videoinfo)
{	
	CConfigEncode* pCfgMainEncode = new CConfigEncode();
	pCfgMainEncode->update();
	CONFIG_ENCODE& ConfigEncode = pCfgMainEncode->getConfig();

	int curMainFmt = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
	int BitRate    = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
	int nfps   = 0;
	int igop   = 0;

	int curQuality = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality;
	Get_Video_Nfps_Igop(&nfps, &igop);


#if 0
	//#if defined(IPC_R2S2)||defined(IPC_A3S2)||defined(IPC_P2)||defined(IPC_R2P2)
	if (curMainFmt == CAPTURE_SIZE_1080P)
	{
		*pCodeMode 	= RT_HDMODE;
		videoinfo->width  = 1920;
		videoinfo->height = 1080;
	}
	else if  (curMainFmt == CAPTURE_SIZE_720P)
	{
		*pCodeMode 	= RT_SDMODE;
		videoinfo->width  = 1280;
		videoinfo->height = 720;
	}
	else if (curMainFmt == CAPTURE_SIZE_360P)
	{
		*pCodeMode = RT_LUMODE;
		videoinfo->width  = 640;
		videoinfo->height = 360;
	}
#endif
	#ifdef IPC_JZ_NEW   

	if(SensorSC2235 == PlatformGetHandle()->Sensor &&
	   ProductM_A3  != PlatformGetHandle()->ProductModel) 
	{
		switch (curQuality)
		{
			case 1: 
				*pCodeMode = RT_HDMODE;
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				break;
			case 2:
				*pCodeMode = RT_SDMODE;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				break;
			case 3:
				*pCodeMode = RT_LUMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			default : 
				break;
		}
	}
	else if(SensorJXH62 == PlatformGetHandle()->Sensor ||
			ProductM_A3 == PlatformGetHandle()->ProductModel)
	{
		switch (curQuality)
		{
			case 1: 
				*pCodeMode = RT_HDMODE;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				break;
			case 2:
				*pCodeMode = RT_SDMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			case 3:
				*pCodeMode = RT_LUMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			default : 
				break;
		}
	}
	#elif IPC_JZ
	if (BAT_SensorOV9732== BAT_PlatformGetHandle()->Sensor) 
	{
		switch (curQuality)
		{
			case 1: 
				*pCodeMode = RT_HDMODE;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				break;
			case 2:
				*pCodeMode = RT_SDMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			case 3:
				*pCodeMode = RT_LUMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			default : 
				break;
		}
	} 
	else if (BAT_SensorSC2235== BAT_PlatformGetHandle()->Sensor)
	{
		switch (curQuality)
		{
			case 1: 
				*pCodeMode = RT_HDMODE;
				videoinfo->width  = 1920;
				videoinfo->height = 1080;
				break;
			case 2:
				*pCodeMode = RT_SDMODE;
				videoinfo->width  = 1280;
				videoinfo->height = 720;
				break;
			case 3:
				*pCodeMode = RT_LUMODE;
				videoinfo->width  = 640;
				videoinfo->height = 360;
				break;
			default : 
				break;
		}
	}
	#endif
	
	videoinfo->codec 			= RT_CODEC_H264;
	videoinfo->framerate 		= nfps;
	videoinfo->frameInterval 	= igop;

	videoinfo->colorDepth = 24;
	videoinfo->bitrate = ConfigEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
	delete pCfgMainEncode;
	return 0;
}

int32_t RealTimeVideoClose_Callback(uint8_t channel)
{	
	g_LiveCamTrans.StopVideo(0,0,0);
	return 0;
}

int32_t RealTimeAudioOpen_CallBack(uint8_t channel,RealTimeAudioDataFormat* audioinfo)
{
	audioinfo->samplesRate = 16000;
	audioinfo->bitrate = 16;
	audioinfo->waveFormat = RT_WAVE_FORMAT_G711;
	audioinfo->channelNumber = channel;	
	audioinfo->blockAlign = 1;
	audioinfo->bitsPerSample =16;
	audioinfo->frameInterval = 62;

	g_LiveCamTrans.StartAudio(0);
	return 0;
}

int32_t RealTimeAudioClose_CallBack(uint8_t channel)
{
	g_LiveCamTrans.StopAudio(0);
	return 0;
}

int32_t RealTimeFileListPerPage_Callback(uint8_t channel,RTFileListPerPage *FilePage,int NumsOfPerPage,char LastFileName[64])
{
    TransFileListPerPage(0,FilePage,NumsOfPerPage,LastFileName);
	return 0;
}

int32_t RealTimeQueryRecordFile_Callback(uint8_t channel,uint8_t ReType,QueryRecordTime FileTime,RTFileListPerPage_2 *FilePage)
{
	int i,mCnt;
	int yy,nn,dd,hh,mm,ss,len;
	char *p = NULL;
	struct tm timetmp;
	FILE_INFO tupuinfo[LIST_SIZE];
	uint tupnum = 0;

	memset(tupuinfo,0,sizeof(tupuinfo));
	
	file_sys_getlist(0, (SYSTEM_TIME*)(&(FileTime.StartTime)), (SYSTEM_TIME*)(&(FileTime.EndTime)), ReType, &tupnum, tupuinfo, 0, 0);
	

	//if(tupnum > LIST_SIZE)	tupnum = LIST_SIZE;
	if(tupnum > 80)	tupnum = 80;
	printf("FileNums = %d\n", tupnum);
	
	FilePage->FileCount = tupnum;
	for(i = 0; i < tupnum; i++)
	{				
		memset(FilePage->fileinfo[i].FileName, 0, 64);

		strcpy(FilePage->fileinfo[i].FileName, tupuinfo[i].base_name);
		FilePage->fileinfo[i].FileSize 		= tupuinfo[i].file_length;

		FilePage->fileinfo[i].FileStartTime.year 	= tupuinfo[i].start_time.year + 2000;
		FilePage->fileinfo[i].FileStartTime.month	= tupuinfo[i].start_time.month;
		FilePage->fileinfo[i].FileStartTime.day  	= tupuinfo[i].start_time.day;
		FilePage->fileinfo[i].FileStartTime.hour 	= tupuinfo[i].start_time.hour;
		FilePage->fileinfo[i].FileStartTime.minute 	= tupuinfo[i].start_time.minute;
		FilePage->fileinfo[i].FileStartTime.second 	= tupuinfo[i].start_time.second;
		
		FilePage->fileinfo[i].RecordDuration		= tupuinfo[i].dur_time;

		FilePage->fileinfo[i].FileEndTime.year 		= tupuinfo[i].end_time.year + 2000;
		FilePage->fileinfo[i].FileEndTime.month		= tupuinfo[i].end_time.month;
		FilePage->fileinfo[i].FileEndTime.day  		= tupuinfo[i].end_time.day;
		FilePage->fileinfo[i].FileEndTime.hour 		= tupuinfo[i].end_time.hour;
		FilePage->fileinfo[i].FileEndTime.minute 	= tupuinfo[i].end_time.minute;
		FilePage->fileinfo[i].FileEndTime.second 	= tupuinfo[i].end_time.second;		
		//_printd("file[%d].name:%s \nfilezie[%d]",i,FilePage->fileinfo[i].FileName,tupuinfo[i].file_length);
	}	

	return 0;
}

int32_t TimeCompare(RTDateTime sTime,RTDateTime tTime)
{
	if(sTime.year 	!= tTime.year  	 ||
	   sTime.month	!= tTime.month 	 ||
	   sTime.day  	!= tTime.day   	 ||
	   sTime.hour 	!= tTime.hour  	 ||
	   sTime.minute	!= tTime.minute  ||
	   sTime.second != tTime.second  
	   )
	   return -1;

	return 0;
}
int32_t RealTimeQueryRecordPage_Callback(uint8_t channel,uint8_t ReType,uint32_t StartStamp,uint32_t EndStamp
	,uint32_t Page,uint32_t PageNum,RTFileListPerPage_3 *FilePage)
{
	int i,cnt;
	uint tupnum ;
	FILE_INFO tupuinfo[LIST_SIZE] = {0};

	static RTFileListPerPage_3 mFilePage = {0};
	static uint32_t mStartStamp = 0;
	static uint32_t mEndStamp   = 0;
	static time_t lastTime = 0;
	time_t curTime = time(NULL);

	uint32_t	tmpStamp	= 0;
	long		TimeOff		= 0;
	SYSTEM_TIME cStartTime 	= {0};
	SYSTEM_TIME cEndTime 	= {0};
	
	struct tm pTime			= {0};
	struct tm tmpTime		= {0};
	
	if( NULL == FilePage){
		return -1;
	}
	if(0 > file_sys_get_linkstat()){
		memset(FilePage,0,sizeof(RTFileListPerPage_3));
		return 0;
	}
//同一时间 30s才更新一次列表
	if(lastTime == 0 || curTime > lastTime + 30 ||
	   mStartStamp != StartStamp || mEndStamp   != EndStamp)

	{
		memset(tupuinfo,0,sizeof(tupuinfo));	
		
		RealTimeGetTimeZone_Callback(&TimeOff,NULL);

		tmpStamp = TimeOff + StartStamp;
		if(NULL != gmtime_r((time_t*)&tmpStamp,&pTime)){
			cStartTime.year 	= pTime.tm_year + 1900;
			cStartTime.month	= pTime.tm_mon + 1;
			cStartTime.day  	= pTime.tm_mday;
			cStartTime.hour 	= pTime.tm_hour;
			cStartTime.minute 	= pTime.tm_min;
			cStartTime.second 	= pTime.tm_sec;			

			printf("%d,%d,%d,%d,%d,%d\n",cStartTime.year ,cStartTime.month,
				cStartTime.day ,cStartTime.hour,cStartTime.minute,
				cStartTime.second);
		}	

		tmpStamp = TimeOff + EndStamp;
		if(NULL != gmtime_r((time_t*)&tmpStamp,&pTime)){
			cEndTime.year 	= pTime.tm_year + 1900;
			cEndTime.month	= pTime.tm_mon + 1;
			cEndTime.day  	= pTime.tm_mday;
			cEndTime.hour 	= pTime.tm_hour;
			cEndTime.minute = pTime.tm_min;
			cEndTime.second = pTime.tm_sec;	

			printf("%d,%d,%d,%d,%d,%d\n",cEndTime.year ,cEndTime.month,
				cEndTime.day ,cEndTime.hour,cEndTime.minute,
				cEndTime.second);			
		}		
		
		file_sys_getlist(0, &cStartTime, &cEndTime, ReType, &tupnum, tupuinfo, 0, 0);
		

		if(tupnum > LIST_SIZE)	tupnum = LIST_SIZE;
		printf("FileNums = %d\n", tupnum);
		
		mFilePage.FileCount = tupnum;
		for(i = 0; i < tupnum; i++)
		{				
			if(tupuinfo[i].file_length <= 1){
				_printd("this file(%s) is too small(%dkb)",tupuinfo[i].base_name,
					tupuinfo[i].file_length);
				mFilePage.FileCount --;
				continue;
			}
			
			memset(mFilePage.fileinfo[i].FileName, 0, 64);

			strcpy(mFilePage.fileinfo[i].FileName, tupuinfo[i].base_name);
			mFilePage.fileinfo[i].FileSize 		= tupuinfo[i].file_length;
			
			tmpTime.tm_year = tupuinfo[i].start_time.year + 2000 - 1900;
			tmpTime.tm_mon	= tupuinfo[i].start_time.month - 1;
			tmpTime.tm_mday = tupuinfo[i].start_time.day;
			tmpTime.tm_hour = tupuinfo[i].start_time.hour;
			tmpTime.tm_min 	= tupuinfo[i].start_time.minute;
			tmpTime.tm_sec 	= tupuinfo[i].start_time.second;

			
			mFilePage.fileinfo[i].FileStartStamp 	= mktime(&tmpTime) - TimeOff;
			
			mFilePage.fileinfo[i].RecordDuration	= tupuinfo[i].dur_time;

			mFilePage.fileinfo[i].FileEndStamp 		= mFilePage.fileinfo[i].FileStartStamp + tupuinfo[i].dur_time;		
			//_printd("file[%d].name:%s \nfilezie[%d]",i,FilePage->fileinfo[i].FileName,tupuinfo[i].file_length);
		}

		lastTime 	= curTime;
		mStartStamp = StartStamp;
		mEndStamp	= EndStamp;
	}	
	else{
		printf("same..........\n");
	}
	
	memset(FilePage,0,sizeof(RTFileListPerPage_3));
	
	cnt = (Page-1) * PageNum;

//	printf("Page:%d,PageNum:%d,cnt:%d,FileCount:%d..........\n",
//		Page,PageNum,cnt,mFilePage.FileCount);
	
	if( mFilePage.FileCount < cnt ){
		return -1;
	}

	i = mFilePage.FileCount - cnt;
	
	FilePage->FileCount = i > PageNum? PageNum : i;


	
	memcpy((void*)(FilePage->fileinfo),(void*)&(mFilePage.fileinfo[cnt]),
		FilePage->FileCount * sizeof(mFilePage.fileinfo[cnt]));
	


	return 0;
}
/*
timeList 由用户自己申请，
返回值为 timeList有效(实际有数据的)数组长度
*/
int32_t RealTimeQueryRecordTimeList_Callback(uint8_t channel,uint32_t startTime, uint32_t endTime, uint32_t **timeList)
{
	long		TimeOff		= 0;
	SYSTEM_TIME cStartTime 	= {0};
	SYSTEM_TIME cEndTime 	= {0};	
	uint32_t mStartStamp 	= startTime;
	uint32_t mEndStamp   	= endTime;	

	VideoTimeList tList		= {0};
	struct tm pTime			= {0};
	
	RealTimeGetTimeZone_Callback(&TimeOff,NULL);

	if(startTime <= 0 && endTime <= 0){
		file_get_video_timelist(&tList,NULL,NULL);
	}
	else{
		startTime += TimeOff;
		endTime	  += TimeOff;

		if(NULL != gmtime_r((time_t*)&startTime,&pTime)){
			cStartTime.year 	= pTime.tm_year + 1900;
			cStartTime.month	= pTime.tm_mon  + 1;
			cStartTime.day  	= pTime.tm_mday;
			cStartTime.hour 	= pTime.tm_hour;
			cStartTime.minute 	= pTime.tm_min;
			cStartTime.second 	= pTime.tm_sec;			

			_printd("Start:%d,%d,%d,%d,%d,%d\n",cStartTime.year ,cStartTime.month,
				cStartTime.day ,cStartTime.hour,cStartTime.minute,
				cStartTime.second);
		}	

		if(NULL != gmtime_r((time_t*)&endTime,&pTime)){
			cEndTime.year 	= pTime.tm_year + 1900;
			cEndTime.month	= pTime.tm_mon  + 1;
			cEndTime.day  	= pTime.tm_mday;
			cEndTime.hour 	= pTime.tm_hour;
			cEndTime.minute = pTime.tm_min;
			cEndTime.second = pTime.tm_sec;	

			_printd("End:%d,%d,%d,%d,%d,%d\n",cEndTime.year ,cEndTime.month,
				cEndTime.day ,cEndTime.hour,cEndTime.minute,
				cEndTime.second);			
		}
		file_get_video_timelist(&tList,&cStartTime,&cEndTime);
	}

	if(tList.TimeCount > 0){
		uint32_t *pTimeList = NULL;
		
		if( NULL == (*timeList = (uint32_t *)malloc(tList.TimeCount*sizeof(uint32_t) + 1))){
			_printd("timeList malloc error");
			return 0;
		}

		pTimeList = *timeList;
		for(int i = 0; i < tList.TimeCount ; i ++){
			pTimeList[i] = tList.TimeList[i];
			_printd("i:%d,timestamp:%lu\n",i,pTimeList[i]);
		}
	}
	else{
		*timeList = NULL;
	}
	return tList.TimeCount;
}

VD_INT32 QueryFileTimes(char *pfilename)
{
	int		 	i 		   = 0;
	int 		mtime      = 0;
	char 		buf[64]	   = {0};
	char		*p 		   = "_";
	char 		*pp        = NULL;
	if(pfilename == NULL){
		return -1;
	}
	strncpy(buf,pfilename,strlen(pfilename));
	pp = strtok(buf,p);
	while((pp = strtok(NULL,"_")))//20170607123030_1001_008.mp4
	{
		i++;
		if(i == 1)
		{
			mtime = atoi(pp);
			mtime = ((mtime / 100) * 60) + ( mtime %100 );
			printf("mtime = %d \n",mtime);
			break;
		}
	}
	return mtime;
}
// I = 1; P = 2 error = -1;
static int Mp4FindFrameType(unsigned char *Buffer, int Bufferlen)
{
	int i = 0; 
	int Bufferlenlocal = (Bufferlen > 512) ? 512 : Bufferlen;
	for (i = 0; i < Bufferlenlocal; i++){
		if (Buffer[i + 0] == 0x00 && Buffer[i + 1] == 0x00 &&
			Buffer[i + 2] == 0x00 && Buffer[i + 3] == 0x01)
		{
			if((Buffer[i + 4] & 0x1f) == 7 || (Buffer[i + 4] & 0x1f) == 5){
				return 1;
			}
			else if((Buffer[i + 4] & 0x1f) == 1){
				return 2;
			}
		}
		i +=1;
	}
	return -1;
}

static int GetKeyFramIndex(int *pFrameIndex,  unsigned int seektime)
{
	int kk					= 0;
	int VideoTimeDur 		= MP4Reader_GetVFrameDur();
	int maxVideonub 		= MP4Reader_GetVFrameNums(); 
	int KeyFrameNums		= MP4Reader_GetKeyFrameNums();
	int curVideoindex 		= 0;

	if(VideoTimeDur < 0 || maxVideonub < 0 || KeyFrameNums < 0){
		printf("read videoInfo error \n");
		return -1;
	}


//特殊考虑15(30)帧时间戳 66 66 67 这种情况
	if(VideoTimeDur % 33 == 0 ){
		curVideoindex = (seektime*10)/(VideoTimeDur*10+3);
	}
	else if(VideoTimeDur % 67 == 0 || VideoTimeDur % 34 == 0){
		curVideoindex = (seektime*10)/((VideoTimeDur-1)*10+3);
	}
	else{
		curVideoindex = seektime/VideoTimeDur;
	}

	printf("curVideoindex:%d,KeyFrameNums:%d,maxVideonub:%d\n",
		curVideoindex,KeyFrameNums,maxVideonub);
	
	if(curVideoindex > maxVideonub){
		*pFrameIndex = MP4Reader_Get_I_FrameNum(KeyFrameNums - 1);
		return -1;
	}

	kk = curVideoindex * KeyFrameNums / maxVideonub + 1;
	if(kk > KeyFrameNums ) kk = KeyFrameNums - 1; 
	if(kk <= 0)	kk = 1;	
	printf("seek iFrame:%d\n",kk);
	*pFrameIndex = MP4Reader_Get_I_FrameNum(kk);

	return 0;
}

typedef struct
{
	uint32_t        n_value;      //参数
	CONTROLTYPE      n_Control;    //状态标志

}P2PHAND;
static P2PHAND play_value = {NULL}; //录像播放策略
static bool RecordSta = false;

void* RecordPopData(void* arg)
{
	bool* RecSta = (bool*)arg;

	unsigned char Buf[1024*1024] = {0};
	int firstFlag		= 0;
	int Len  			= 0;
	int SleepTime       = 0;
	uint64 OldTime 		= 0;
	uint64 CurTime 		= 0;
	
	uint VideoTimeStamp = 0;
	uint AudioTimeStamp	= 0;
	int VideoCurFrame   = 1;
	int AudioCurFrame	= 1;
	int FrameRate		= 0;
	int AudioFrames 	= MP4Reader_GetAFrameNums();
	int VideoFrames 	= MP4Reader_GetVFrameNums();
	int AudioFrameDur	= MP4Reader_GetAFrameDur();
	int VideoFrameDur 	= MP4Reader_GetVFrameDur();

	

	pthread_detach(pthread_self());
	SET_THREAD_NAME("RecordPopData");
	
	printf("VideoFrames:%d\n",VideoFrames);
	printf("AudioFrames:%d\n",AudioFrames);
	printf("VideoFrameDur:%d\n",VideoFrameDur);
	printf("AudioFrameDur:%d\n",AudioFrameDur);
	
	if(VideoFrameDur <= 0) VideoFrameDur = 50;
	if(AudioFrameDur <= 0) AudioFrameDur = 64;

	if(VideoFrameDur > AudioFrameDur) SleepTime = AudioFrameDur;
	else							  SleepTime = VideoFrameDur;
	
	printf("RecSta:  =====  [%d  %d]\n",*RecSta,play_value.n_Control);

	OldTime 	= SystemGetMSCount64();

//粗劣估算，1000%VideoFrameDur的余数 大于VideoFrameDur的一半时则FrameRate加一
	if(1000%VideoFrameDur != 0){
		FrameRate	= (1000+VideoFrameDur/2)/VideoFrameDur;
	}
	else{
		FrameRate	= 1000/VideoFrameDur;
	}
		
	while(VideoCurFrame <= VideoFrames && *RecSta == true)
	{
		VideoTimeStamp = MP4Reader_GetVideoTimeStamp(VideoCurFrame);
		AudioTimeStamp = MP4Reader_GetAudioTimeStamp(AudioCurFrame);
		switch(play_value.n_Control)
		{
			case RT_CONTINUE://继续播放
			{
				CurTime = SystemGetMSCount64();				
				if(CurTime < ( OldTime + VideoTimeStamp - 5*VideoFrameDur))
				{
					//usleep(VideoFrameDur*1000);
					usleep(5*1000);
					continue;
				}
			
				if(VideoTimeStamp <= AudioTimeStamp || AudioCurFrame >= AudioFrames)
				{
					if((Len = MP4Reader_VideoData(Buf,VideoCurFrame))> 0)
					{
						int iret = Mp4FindFrameType(Buf,Len);
						if( 1 == iret )
						{
							RealTimeSendRecordVideoData(0, 1,Buf,Len,VideoTimeStamp);
							firstFlag = 1;		//发送第一帧是 I 帧
							_printd("I : [%02x%02x%02x%02x%02x] TotalVideoNub = %d, CurVideoIndex = %d ,VideoTimestamp =%d threadId = %lu\n",Buf[0],Buf[1],Buf[2],Buf[3],Buf[4],VideoFrames,VideoCurFrame,VideoTimeStamp,pthread_self());

						}else if(2 == iret && firstFlag == 1){
							//_printd("P : [%02x%02x%02x%02x%02x] TotalVideoNub = %d, CurVideoIndex = %d ,VideoTimestamp =%d threadId = %lu\n",Buf[0],Buf[1],Buf[2],Buf[3],Buf[4],VideoFrames,VideoCurFrame,VideoTimeStamp,pthread_self());
							RealTimeSendRecordVideoData(0, 0,Buf,Len,VideoTimeStamp);

						}else{
							printf("Read I P fream error [%d]!\n",iret);
							for(int i = 0;i < 16;i++)
								printf("%x,",Buf[i]);
							printf("\n");
						}
					}	
					else
					{
						printf("Read Video Error\n");
					}
					VideoCurFrame ++;
					
#if 0
					static FILE   *pfile = NULL;
					static int	   Flag  = 0;
			
					if(pfile == NULL)
					{
						if( NULL == (pfile=fopen("/home/record.h264","w+b")))
						{
							printf("fopen /home/record.h264   error~ \n");
							return NULL;
						}
					}
					if(NULL != pfile){
						fwrite(Buf,Len,1,pfile);
						fflush(pfile);
						
						Flag++;
						if(Flag >= 10000)
						{
							printf("write aac end!");
							fclose(pfile);
							pfile = NULL;
						}
					}
											
#endif		
				}
				else
				{
					if((Len = MP4Reader_AudioData(Buf,AudioCurFrame)) - 7 > 0)
					{
						RealTimeSendRecordAudioData(0,Buf,Len,AudioTimeStamp);	
						_printd("AudioTimeStamp:%d\n",AudioTimeStamp);
					}	
					else
					{
						printf("Read Audio Error\n");
					}

					AudioCurFrame ++;	
				}			
#if 0
				CurTime = SystemGetMSCount64();
				if(CurTime < ( OldTime + VideoTimeStamp - 5*VideoFrameDur))
				{
					usleep(VideoFrameDur*1000);
				}
#endif				
			}
				break;
			case RT_PAUSE://暂停发送
				usleep(5* 1000);
				printf("  >>>>>>>>>PauseSendStream	 \n");
				continue;	
			case RT_STOP: //停止播放
				goto END_EXIT;
			case RT_JUMP:
				
				printf("JumpFrame SendStream  CurVideoIndex=%d value=%d\n", VideoCurFrame,play_value.n_value);
				GetKeyFramIndex(&VideoCurFrame,play_value.n_value);
				
				VideoTimeStamp = MP4Reader_GetVideoTimeStamp(VideoCurFrame);
				AudioCurFrame = VideoTimeStamp / AudioFrameDur + 1;
				
				play_value.n_Control = RT_CONTINUE;
				printf("JumpFrame to  VideoIndex=%d,AudioIndex=%d\n", VideoCurFrame,AudioCurFrame);
				printf("VideoTimeStamp=%d,VideoFrameDur=%d\n", VideoTimeStamp,VideoFrameDur);
				OldTime = SystemGetMSCount64() - VideoTimeStamp + VideoFrameDur;
				break;
			default:
				break;
		}	
		usleep(4000);
	} 
	RealTimeStopSendPlayBackData(0);
END_EXIT:
	_printd(">>>>>>>>>>>  Play PushStream File  End ~ \n");
	play_value.n_Control = RT_PAUSE;
	MP4Reader_Close();
	RecordSta = false;
	//usleep(5*1000);
	pthread_exit(0);
	
	return NULL;
	
}


int32_t RealTimeRecordOpen_CallBack(uint8_t channel,char*  recordname,RealTimeVideoDataFormat* videoinfo, RealTimeAudioDataFormat* audioinfo,uint32_t* FileTotalTime)
{
	_printd("-------------------->>>>>>>>>>>>>>>>RealTimeRecordOpen_CallBack");

	pthread_t vtid_t;
	char Path[256];
	char VPath[256],TPath[256],APath[256];	

	int year,mon,day,hour,min,sec;
	int timelen,snum;
	
	sscanf(recordname,"%04d%02d%02d%02d%02d%02d_%d_%d.mp4",
		&year,&mon,&day,&hour,&min,&sec,&timelen,&snum);
				
		
	snprintf(VPath,128,"%s/%04d-%02d-%02d/%02d/%s",VIDEO_DIR,year,mon,day,hour,recordname);
	snprintf(TPath,128,"%s/%04d-%02d-%02d/%02d/%s",TIMER_DIR,year,mon,day,hour,recordname);
	snprintf(APath,128,"%s/%04d-%02d-%02d/%02d/%s",ALARM_DIR,year,mon,day,hour,recordname);

	if(access(VPath,F_OK) == 0)
	{
		memcpy(Path,VPath,128);
	}
	else if(access(TPath,F_OK) == 0)
	{
		memcpy(Path,TPath,128);
	}
	else if(access(APath,F_OK) == 0)
	{
		memcpy(Path,APath,128);
	}
	else
	{
		printf("The file(%s) is'n exist\n",recordname);
		return -1;
	}
	
	videoinfo->codec         = 0;
	videoinfo->bitrate       = 640  * 1024;
	videoinfo->width         = 640;
	videoinfo->height        = 320;
	videoinfo->framerate     = 20;
	videoinfo->colorDepth    = 24;
	videoinfo->frameInterval = 50;

	audioinfo->samplesRate	 = 16000;
	audioinfo->bitrate		 = 16;
	audioinfo->waveFormat    = 0;
	audioinfo->channelNumber = 0;
	audioinfo->blockAlign    = 1;
	audioinfo->bitsPerSample = 16;
	audioinfo->frameInterval = 64;
	
	*FileTotalTime = QueryFileTimes(Path) * 1000;
	_printd("MP4 File Name is [%s] FileTotalTime = %d - -\n",Path, *FileTotalTime);

	sec = 0;
	while(sec ++ < 50 && RecordSta  == true )
	{
		play_value.n_Control = RT_STOP;
		usleep(20 * 1000);
	}
	
	if( RecordSta == false && MP4Reader_Open(Path))
	{	
		play_value.n_Control = RT_PAUSE; 
		RecordSta = true;
		_printd("MP4 File Name is opened successly\n");
		pthread_create (&vtid_t, NULL, RecordPopData,(void*)&RecordSta);
	}
	
	return 0;
}

int32_t RealTimeRecordCotrol_CallBack(uint8_t channel,CONTROLTYPE type,uint32_t value)
{
	_printd("RealTimeRecordCotrol_CallBack ------------->>>>>> value = %d, type = %d \n",value,type);

	play_value.n_value   = value;
	play_value.n_Control = type;

	return 0;
}

int32_t RealTimeRecordFileDelete_CallBack(char RecordFileName[64])
{
	char Path[256];
	char VPath[256],TPath[256],APath[256];	

	int year,mon,day,hour,min,sec;
	int timelen,snum;
	
	sscanf(RecordFileName,"%04d%02d%02d%02d%02d%02d_%d_%d.mp4",
		&year,&mon,&day,&hour,&min,&sec,&timelen,&snum);
				
		
	snprintf(VPath,128,"%s/%04d-%02d-%02d/%02d/%s",VIDEO_DIR,year,mon,day,hour,RecordFileName);
	snprintf(TPath,128,"%s/%04d-%02d-%02d/%02d/%s",TIMER_DIR,year,mon,day,hour,RecordFileName);
	snprintf(APath,128,"%s/%04d-%02d-%02d/%02d/%s",ALARM_DIR,year,mon,day,hour,RecordFileName);


	if(access(VPath,F_OK) == 0)
	{
		memcpy(Path,VPath,128);
	}
	else if(access(TPath,F_OK) == 0)
	{
		memcpy(Path,TPath,128);
	}
	else if(access(APath,F_OK) == 0)
	{
		memcpy(Path,APath,128);
	}
	else
	{
		printf("The file(%s) is'n exist\n",RecordFileName);
		return -1;
	}		

	remove(Path);
	printf("remove %s\n",Path);

	return 0;
}

int32_t RealTimeUpgrade_Callback(char UpgradeFile[128])
{
#ifdef SUNING
	int RebootFlag = 0;
	char pre_version[64] = {0};
	char cur_version[64] = {0};
	Get_Version_Path(cur_version,64);
	Get_PreVersion_Path(pre_version, 64);

	if(0 == access(pre_version, 0))
	{
		remove(pre_version);
	}
	if(rename(cur_version, pre_version))
	{
		 perror("rename");;
	}
	RebootFlag = 2;
	g_Cmos.Write(CMOS_REBOOTFLAG,&RebootFlag,4);
#endif

#if defined(FUNC_WDT) 
	//关看门狗
	g_WatchDogTime.Stop(); 
#endif
	char UpgradeFilePath[150] = {0};
	sprintf(UpgradeFilePath,"/app/upgradeTool  %s",UpgradeFile);
	printf("%s\n",UpgradeFilePath);
	execl("/bin/sh","sh","-c",UpgradeFilePath,(char*)0);	


	return 0;
}

int32_t RealTimeGetDevVersion_Callback(char Version[64])
{
	char  strVerName[16] = {0};
	char* ReadVersion = NULL;
	
	JudgeFileIsExis(0,0);

	Get_Version_Name(strVerName, sizeof(strVerName));

	if (NULL != (ReadVersion = ReadVersionToFile()) && NULL  != strstr(ReadVersion, strVerName))
	{
		strncpy(Version , ReadVersion,64);
	}
	else
	{
		char strVerPath[64] = {0};
		Get_Version_Path(strVerPath, sizeof(strVerPath));
		remove(strVerPath);
		JudgeFileIsExis(0,0);
		if(NULL != (ReadVersion = ReadVersionToFile()))
		{
			strncpy(Version , ReadVersion,64);
		}
	}
	return 0;
}
extern char g_DelFileFlag;

int32_t RealTimeGetSDInfo_Callback(RTSDInfo *info) //SD???????????????
{
	int Remainmem 		= 0;
	int Totalmem 		= 0;
	int ret 			= -1;
	RTFileListPerPage filelist;
	static char EarlyFileTime[64] ={0};

	static unsigned long CurTime = 0;
	static unsigned long LastTime= 0;
	
	ret = file_sys_get_cap(&Totalmem,&Remainmem);
	
	CurTime = time(NULL);
#if 1	
	//删除过文件再查询
	if(g_DelFileFlag)
	{
		//FFFile.cpp 删除过SD卡文件
		memset(EarlyFileTime,0,64);
	}
#else	
	//per 30s
	if(CurTime > LastTime + 30 || CurTime < LastTime - 30){
		LastTime = CurTime;
		memset(EarlyFileTime,0,64);
	}
#endif
	if(ret != -1)
	{
		if(FS_WRITE_ERROR == g_DriverManager.GetSDStatus()){
			info->SDExist 		= 2;
		}
		else{
			info->SDExist 		= 1;	
		}
		info->SDTotalSize 	= Totalmem/1024; //MB
        info->SDFreeSize 	= Remainmem/1024;//MB	
		if(EarlyFileTime[0] == 0){
			GetEarilestVideoFile(&filelist);
			if(filelist.FileCount > 0){
				memset(info->EarlyFileName,0,sizeof(info->EarlyFileName));
				strncpy(info->EarlyFileName,filelist.fileinfo[filelist.FileCount - 1].FileCreateTime,64);
				strncpy(EarlyFileTime,info->EarlyFileName,64);
				g_DelFileFlag = 0;
			}
		}
		else{
			strncpy(info->EarlyFileName,EarlyFileTime,64);
		}		
	}
	else
	{
		if(file_sys_get_linkstat() < 0){
			info->SDExist 		= 0;
		}
		else{ //卡异常
			info->SDExist 		= 2;
		}
	}
//_printd("SDExist:%d",info->SDExist);
	return 0;
}

int32_t RealTimeGetFileSavePath_Callback(RTSystemFilePath *path) //返回存储目录
{
	char dir[256] = {0};
	switch(path->type)
	{
		case RTMPADDR:
			  strncpy(path->FilePath,"/mnt/temp",128);//流地址保存目录
		break;
		case STREAMID:
			  strncpy(path->FilePath,"/mnt/temp",128);//StreamId保存目录
		break;
		case UPGRADEPATH:
			  strncpy(path->FilePath,"/tmp/upgrape",128);//升级目录
		break;
		case LiveMuteSta:
			  strncpy(path->FilePath,"/mnt/temp",128);//声音开关保存目录
		break;
		case AiPhotoDir:
			if(0 == g_AiUpload.GetStoreDir(dir)){
				strncpy(path->FilePath,dir,128);
			}
		break;
		case AlarmRecFile:
			 strncpy(path->FilePath,ALARM_DIR,128);//报警录像临时存储目录
			 // strncpy(path->FilePath,"/tmp/zdk",128);
		break;
	}
	return 0;
}

int32_t RealTimePTZOpen_Callback(uint8_t channel)
{
	return 0;
}

int32_t RealTimePTZCmd_Callback(uint8_t channel, RT_PTZControlCmd ptzcmd, RT_ControlArgData* arg)
{

#ifdef MOTOR

#ifdef IPC_JZ_NEW

#if 0
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();
	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();
#endif

	PTZ_OPT_STRUCT m_Ptz_Opt_Struct;
	memset(&m_Ptz_Opt_Struct, 0, sizeof(PTZ_OPT_STRUCT));
	//printf("=====>>>>Ver=%d Hor=%d\n",cfgCamera.VerReverse, cfgCamera.HorReverse);
	switch(ptzcmd)
	{
	#if 0
		case RT_PTZ_MV_STOP:
			PtzWrite(NULL, 0);
			return 0;
		case RT_PTZ_MV_UP:
			if(1 == cfgCamera.VerReverse){
				PtzWrite(NULL, 2);
			}else{
				PtzWrite(NULL, 1);
			}
			return 0;
		case RT_PTZ_MV_DOWN:
			if(1 == cfgCamera.VerReverse){  //
				PtzWrite(NULL, 1);
			}else{
				PtzWrite(NULL, 2);
			}
			return 0;
		case RT_PTZ_MV_LEFT://R2P2 Q1  right|left
			if(1 == cfgCamera.HorReverse){
				PtzWrite(NULL, 4);
			}else{
				PtzWrite(NULL, 3);
			}
			return 0;
		case RT_PTZ_MV_RIGHT:
			if(1 == cfgCamera.HorReverse){
				PtzWrite(NULL, 3);
			}else{
				PtzWrite(NULL, 4);
			}
			return 0;
		#else
		case RT_PTZ_MV_STOP:
			g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
			return 0;
		case RT_PTZ_MV_UP:
			g_PtzTrace.PtzRunCmd(PTZRUN_UP);
			return 0;
		case RT_PTZ_MV_DOWN:
			g_PtzTrace.PtzRunCmd(PTZRUN_DOWN);
			return 0;
		case RT_PTZ_MV_LEFT://R2P2 Q1  right|left
			g_PtzTrace.PtzRunCmd(PTZRUN_LEFT);
			return 0;
		case RT_PTZ_MV_RIGHT:
			g_PtzTrace.PtzRunCmd(PTZRUN_RIGHT);
			return 0;
		#endif
		
		case RT_PTZ_ZOOM_DEC:
			m_Ptz_Opt_Struct.cmd  = PTZ_OPT_ZOOM_WIDE;    
			m_Ptz_Opt_Struct.arg1 = 16;//
			g_Ptz.Start(channel, &m_Ptz_Opt_Struct);
			return 0;
		case RT_PTZ_ZOOM_INC:
			m_Ptz_Opt_Struct.cmd  = PTZ_OPT_ZOOM_TELE;    
			m_Ptz_Opt_Struct.arg1 = 16;//
			g_Ptz.Start(channel, &m_Ptz_Opt_Struct);
			break;
		case RT_PTZ_AUTO_CRUISE://????
			break;
		case RT_PTZ_GOTO_PRESET://?????
		#if 0
			m_Ptz_Opt_Struct.cmd 	= PTZ_OPT_GOTOPRESET;
			m_Ptz_Opt_Struct.arg1 	= arg->arg1;
			g_Ptz.Start(channel, &m_Ptz_Opt_Struct);
		#else
			g_PtzTrace.PtzRunCmd(PTZRUN_GOTO_PRESET,arg->arg1);
		#endif
			return 0;
		case RT_PTZ_SET_PRESET://??????
			m_Ptz_Opt_Struct.cmd 	= PTZ_OPT_SETPRESET;
			m_Ptz_Opt_Struct.arg1 	= arg->arg1;
			g_Ptz.Start(channel, &m_Ptz_Opt_Struct);
			return 0;
		case RT_PTZ_CLEAR_PRESET://??????
			m_Ptz_Opt_Struct.cmd 	= PTZ_OPT_CLEARPRESET;
			m_Ptz_Opt_Struct.arg1 	= arg->arg1;
			g_Ptz.Start(channel, &m_Ptz_Opt_Struct);
			return 0;
		case RT_PTZ_CLEAR_TOUR:
			break;
		default:
			break;
	}
#elif defined(IPC_FH)
	int  rotate_state = 0;
	if(ptzcmd == RT_PTZ_MV_UP)
		rotate_state = 1;
	else if(ptzcmd == RT_PTZ_MV_DOWN)
		rotate_state = 2;
	else if(ptzcmd == RT_PTZ_MV_LEFT)
		rotate_state = 3;
	else if(ptzcmd == RT_PTZ_MV_RIGHT)
		rotate_state = 4;
	else
		rotate_state = 0;
	GpioPwmMotoCtrl(rotate_state);
#endif

	return 0;

#else
	return 1;
#endif
}

int32_t RealTimePTZClose_Callback(uint8_t channel)
{

	return 0;
}

static int talkbackPri = -1;
int32_t RealTimeAudioPlayStart_Callback(AUDIOPLY_TYPE EncordType,int32_t SampleRate,
	int32_t BitWidth,uint32_t Volume,int Priority/*the biger ,the higher*/)
{
#ifdef 	MP3PLAYWITHURL
	int  mp3PlayStatus 	 = g_MP3Play.GetMP3PlayStatus(NULL);
	int  mp3PlayPriority = g_MP3Play.GetMp3PlayCurPriority();

	talkbackPri = Priority;
//音乐处于播放中的状态
	if(3 == mp3PlayStatus || 1 == mp3PlayStatus){//
		if(talkbackPri >= mp3PlayPriority){
			g_MP3Play.SetMP3PlayStatus(0);
			//usleep(500*1000);
		}
		else{
			
			return 0;
		}
	}
#endif	
	static time_t oldplaytime = 0;
	time_t playtime = time(NULL);


	printf("playtime is %ld\n",playtime);
	printf("oldplaytime is %ld\n",oldplaytime);
#ifndef IPC_JZ  //对讲之前先播放一段铃声-------for YiDong
	if (PlatformGetHandle()->ClientCode == Client_YD_DEV || 
		PlatformGetHandle()->ClientCode == Client_YD_SLD){
		if (0 == access(AUDIO_START_TALK,F_OK)){
			if ((playtime - oldplaytime) >= 2){
				int r_play = AudioPlayAudioStart(AUDIO_START_TALK, (enum audio_encode_type)16, 0);
				if (r_play == -1){
					AudioPlayAudioStop();
					usleep(20*1000);
					AudioPlayAudioStart(AUDIO_START_TALK, (enum audio_encode_type)16, 0);
				}
				AudioPlayForceSet();//
				//sleep(3);				
			}			
		}
	}
	oldplaytime = playtime; 
#endif

	return AudioTalkPlaySet(EncordType ,SampleRate ,BitWidth ,Volume);
}

int32_t RealTimeAudioPlayProGress_Callback(uint8_t* buf, int32_t size)
{
#ifdef 	MP3PLAYWITHURL
//	char url[1024] = {0};
	static int iCnt = 0;
	int  mp3PlayStatus 	 = g_MP3Play.GetMP3PlayStatus(NULL);
	int  mp3PlayPriority = g_MP3Play.GetMp3PlayCurPriority();

//音乐处于播放中的状态
	if(3 == mp3PlayStatus || 1 == mp3PlayStatus){//
		if(talkbackPri >= mp3PlayPriority){
			g_MP3Play.SetMP3PlayStatus(0);
			usleep(500*1000);
		}
		else{
			if((iCnt++%10) == 0)
				_printd("The priority of playing music is more than this,so dump the frame");
			return 0;
		}
	}
#endif	
	return AudioTalkPlayFrame(buf,size);
}
int32_t RealTimeAudioPlayStop_Callback(void)
{
	talkbackPri = -1;
	return AudioTalkPlayFinish();
}

int32_t RealTimeImageMirrorFlip_Callback(MirrorFlip direct)
{
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();

	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();

	switch (direct){
	case NORMAL:
		cfgCamera.HorReverse = 0;
		cfgCamera.VerReverse = 0;

		printf("The direction of image is NORMAL\n");
		break;
	case HORFLIP:
		cfgCamera.HorReverse = 1;
		cfgCamera.VerReverse = 0;

		printf("The direction of image is HORFLIP\n");
		break;
	case VERFLIP:
		#if 0 //在APP 改之前，垂直翻转暂时用 水平垂直一起翻转，以解决机器倒装镜像bug
		cfgCamera.HorReverse = 0;
		cfgCamera.VerReverse = 1;

		printf("The direction of image is VERFLIP\n");
		break;
		#endif //----20181210  LHC add
	case HORVERFLIP:
		cfgCamera.HorReverse = 1;
		cfgCamera.VerReverse = 1;

		printf("The direction of image is HORVERFLIP\n");
		break;
	default :
		cfgCamera.HorReverse = 0;
		cfgCamera.VerReverse = 0;
		break;
	}


	ConfigCamera.commit();

	return 0;
}


int32_t RealTimeImageGetMirrorFlip_Callback(MirrorFlip *pdirect)
{
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();

	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();
	if (0 == cfgCamera.HorReverse && 0 == cfgCamera.VerReverse)
	{	
		*pdirect = NORMAL;
	}
	else if (cfgCamera.HorReverse && 0 == cfgCamera.VerReverse)
	{
		*pdirect = HORFLIP;
	}
	else if (0 == cfgCamera.HorReverse && cfgCamera.VerReverse)
	{
		*pdirect = VERFLIP;
	}
	else if(cfgCamera.HorReverse && cfgCamera.VerReverse)
	{
		*pdirect = HORVERFLIP;
	}
	
	return 0;
}
int32_t RealTimeSleepIpcam()
{
#if defined(IPC_JZ) 
	g_DevExternal.Dev_EnableSleepMode();
	g_DevExternal.PaassCMDSleepMode();	
	//g_DevExternal.EnterSleepMode();
#endif
	return 0;
}
int32_t RealTimeSetSleepStatu(int32_t enable)
{	
#if defined(IPC_JZ) 
	if(enable)
		g_DevExternal.Dev_EnableSleepMode();
	else
		g_DevExternal.Dev_UnableSleepMode();
#endif
	return 0;

}

int32_t RealTimeGetTimePIRWorkSheet(RT_WORKSHEET *pPirWorkSheet)
{
#if defined(IPC_JZ) 
	CConfigAwakeTime* pAwe = new CConfigAwakeTime();
	int hour,min,sec;

	pAwe->update();

	for(int i = 0;i < WDAYS && i < RT_WEEKS ; i++)
	{
		CONFIG_AWAKETIME& m_cfg = pAwe->getConfig(i);
		for(int j = 0;j < UNITS && j < RT_TSECT; j++)
		{	
			pPirWorkSheet->tsSchedule[i][j].enable = m_cfg.DayPlan[j].Enable;
			if(m_cfg.DayPlan[j].Enable == TRUE)
			{
				hour = (m_cfg.DayPlan[j].StartTime >> 16)&0xff;
				min  = (m_cfg.DayPlan[j].StartTime >>  8)&0xff;
				sec  =  m_cfg.DayPlan[j].StartTime&0xff;

				_printd("Start[%02d:%02d:%02d]",hour,min,min);
				pPirWorkSheet->tsSchedule[i][j].startHour  = hour;
				pPirWorkSheet->tsSchedule[i][j].startMinute= min;
				pPirWorkSheet->tsSchedule[i][j].startSecond= sec;

				hour = (m_cfg.DayPlan[j].EndTime >> 16)&0xff;
				min  = (m_cfg.DayPlan[j].EndTime >>  8)&0xff;
				sec  =  m_cfg.DayPlan[j].EndTime&0xff;		
				
				_printd("End[%02d:%02d:%02d]",hour,min,min);
				pPirWorkSheet->tsSchedule[i][j].endHour  = hour;
				pPirWorkSheet->tsSchedule[i][j].endMinute= min;
				pPirWorkSheet->tsSchedule[i][j].endSecond= sec;
			}
		}
	}
#endif
	return 0;
}
int32_t RealTimeSetTimePIRWorkSheet(RT_WORKSHEET PirWorkSheet)
{
#if defined(IPC_JZ) 
	CConfigAwakeTime* pAwe = new CConfigAwakeTime();
	int hour,min,sec;

	_printd("RealTimeSetTimePIRWorkSheet");

	pAwe->update();

	for(int i = 0;i < WDAYS && i < RT_WEEKS ; i++)
	{
		CONFIG_AWAKETIME& m_cfg = pAwe->getConfig(i);
		for(int j = 0;j < UNITS && j < RT_TSECT; j++)
		{	
			m_cfg.DayPlan[j].Enable = PirWorkSheet.tsSchedule[i][j].enable;
	#if 0
			_printd("%d %d %d - %d %d %d  [%d]",hour = PirWorkSheet.tsSchedule[i][j].startHour,
				PirWorkSheet.tsSchedule[i][j].startMinute,PirWorkSheet.tsSchedule[i][j].startSecond,
				PirWorkSheet.tsSchedule[i][j].endHour,PirWorkSheet.tsSchedule[i][j].endMinute,
				PirWorkSheet.tsSchedule[i][j].endSecond,PirWorkSheet.tsSchedule[i][j].enable
				);
	#endif
			if(PirWorkSheet.tsSchedule[i][j].enable == TRUE)
			{	
				hour = PirWorkSheet.tsSchedule[i][j].startHour;
				min  = PirWorkSheet.tsSchedule[i][j].startMinute;
				sec  = PirWorkSheet.tsSchedule[i][j].startSecond;

				m_cfg.DayPlan[j].StartTime = (hour << 16) | (min << 8) | sec;
				
				hour = PirWorkSheet.tsSchedule[i][j].endHour;
				min  = PirWorkSheet.tsSchedule[i][j].endMinute;
				sec  = PirWorkSheet.tsSchedule[i][j].endSecond;
				
				m_cfg.DayPlan[j].EndTime   = (hour << 16) | (min << 8) | sec;

				_printd("StartTime :%d EndTime :%d",m_cfg.DayPlan[j].StartTime,m_cfg.DayPlan[j].EndTime);
			}
			else
			{
				PirWorkSheet.tsSchedule[i][j].enable = FALSE;
			}
		}
	}

	pAwe->commit();
#endif	
	return 0;
}

int32_t RealTimeGetElectricity(RT_BAT_Status *pEle)
{
#if defined(IPC_JZ) 
	ZRT_CAM_BAT_Status Status;
	
	memset(&Status, 0, sizeof(Status));

	if(0 != g_DevExternal.Dev_GetElecQuantity(&Status))
		return -1;
	
	pEle->capacity 			= Status.capacity;
	pEle->chargingStatus 	= Status.chargingStatus;
#endif	
	return 0;
}

int32_t RealTimeSetDevSleepSta(int32_t Enable,int32_t Val)
{
	_printd("\n\nEnable %d, Val:%d\n\n",Enable,Val);
#ifdef IPC_JZ
	if(Enable)
	{
		g_DevExternal.Dev_EnableSleepMode();
		g_DevExternal.Dev_SetSleepTime(Val);
	}
	else
	{
		g_DevExternal.Dev_UnableSleepMode();	
		g_DevExternal.Dev_SetSleepTime(60);
	}
#endif	
		
}
int32_t RealTimeReBoot_CallBack()
{
	int RebootFlag = 0;
	g_Cmos.Write(CMOS_REBOOTFLAG,&RebootFlag,4);
	printf("reboot -f \n\n\n");
	char ReStartStion[150] = {0};
	sprintf(ReStartStion,"reboot");
	printf("%s\n",ReStartStion);
	execl("/bin/sh","sh","-c",ReStartStion,(char*)0);
	return 0;
}

int32_t RealTimeGetDeviceInfo(RT_DeviceInfo *pDevInfo)
{
	memset(pDevInfo, 0, sizeof(RT_DeviceInfo));

	GetDevModel(pDevInfo->DevType,sizeof(pDevInfo->DevType));
	GetSystemVer(pDevInfo->SystemVersion,sizeof(pDevInfo->SystemVersion));
	GetConnWifi(pDevInfo->WifiName,sizeof(pDevInfo->WifiName));
	GetWifiValue(&(pDevInfo->WifiValue));
	GetDeviceNum(pDevInfo->DeviceNum,sizeof(pDevInfo->DeviceNum));
	GetDevIp(pDevInfo->IpAddr,sizeof(pDevInfo->IpAddr));
	GetDevMac(pDevInfo->MacAddr,sizeof(pDevInfo->MacAddr));

	return 0;
}

int32_t RealTimeSetTimeZone_Callback(long TimeSec,int DaylightEnable)
{
	_printd("--->>>>>Settimezone = %d DaylightEnable:%d\n",TimeSec,DaylightEnable);
	//if(timezone < 0) 	return -1;
	
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();

	_printd("--->>>>>cfgnetntp.TimeZone = %d \n",cfgnetntp.TimeZone);

	int dtimezone = (TimeSec * 10)/3600;

	_printd("--->>>>>dtimezone = %d ,%d,%d\n",dtimezone,TimeSec * 10,(TimeSec * 10)/3600);
	
	cfgnetntp.TimeZone 		= dtimezone;
	cfgnetntp.DaylightEnable= DaylightEnable;
		
	m_cCfgNtp.commit();
	return 0;
}
int32_t RealTimeGetTimeZone_Callback(long *pTimeSec,int *pDaylightEnable)
{
	CConfigNetNTP m_cCfgNtp;
	m_cCfgNtp.update();
	CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();

	if(NULL != pTimeSec){
		*pTimeSec = (((float)cfgnetntp.TimeZone)/10) * 3600;
		_printd("--->>>>>Gettimezone = %d(%f)\n",*pTimeSec,(*pTimeSec/3600.0));
	}
	if(NULL != pDaylightEnable){
		*pDaylightEnable = cfgnetntp.DaylightEnable;
	}

	return 0;
}

//#ifdef IPC_P2
int32_t RealTimeGet4GInfo(RT_4GInfo *p4GInfo)
{
	return 0;
}



int32_t RealTimeSet4GFlowLimit(RT_FlowLimit Limit)
{
	return 0;
}

int32_t RealTimeGet4GFlowLimit(RT_FlowLimit *Limit)
{
	return 0;
}
//#endif

#ifdef NVR_TAOSHI
int32_t RealTimeSystemTimeSynchronization_Callback(int32_t timestamps)
{
	return 0;
}
#endif
int32_t RealTimeSetSDCardFormat_Callback()
{
	_printd("format SD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	if(g_RecordStatus)
	{
		_printd("#############StopManulRecord##############");
		g_Record.StopRec(REC_CLS, 0);
	}	

	return file_sys_format();
}

int32_t RealTimeConnectWifi_Callback(char* Ssid,char* Passwd)
{
	_printd("#Ssid:%s ,Passwd:%s",Ssid,Passwd);

	if(FALSE == CheckSsid(Ssid,strlen(Ssid)))
		return 1;
	
	Store_Cfgfile(STORE_WIFI_CONFIG,Ssid,Passwd);

	return 0;
}

int32_t RealTimeForceIFrame_Callback()
{
	_printd("ForceIFrame...");

	#ifdef IPC_JZ_NEW
	if(ChipT10 == PlatformGetHandle()->Chip){
		int nWidth  = 0;
		int nHeight = 0;
		CaptureGetCurHeightWidth(&nWidth, &nHeight);
		if(nWidth == 640 && nHeight == 360){
			_printd("T10 640X360 ForceIFrame...\n");
			return CaptureForceIFrame(0,1);   //双码流   强制次码流
		}
	}
	#endif
	return CaptureForceIFrame(0,0);
}

int32_t RealTimePwdAuth_Callback(char* username, char* pwd)
{
#ifdef FAC_CHECK
	return 0;
#endif	
	int ret = 0;
	if(6 != strlen(pwd))
	{
		printf("size is not 6!\n");
		return -1;
	}
	ret = g_userManager.isPasswdValid("guest",pwd,needEncrypt,0);
	if(ret)
	{
		printf("pwd ok!\n");
		return 0;
	}
	else
	{
		printf("fail ok!\n");
		return -1;
	}
}
int32_t RealTimeResetUsrPsword_Callback(char* username, char* oldpwd, char* newpwd)
{
	int ret = 0;
	
	if(0 == strcmp("6666superpw6666",oldpwd))
	{
		//应pass要求重新绑定修改为默认密码
		g_userManager.modPassword("guest",newpwd,needEncrypt);
		return 0;
	}
	if((6 != strlen(oldpwd)) || (6 != strlen(newpwd)))
	{
		printf("size is not 6!\n");
		return -1;
	}
	ret = g_userManager.isPasswdValid("guest",oldpwd,needEncrypt,0);
	if(ret)
	{
		g_userManager.modPassword("guest",newpwd,needEncrypt);
	}
	else
	{
		_printd("old pwd novalid!!!!");
		return -1;
	}
	return 0;
	
}
#ifdef HMSOTA
int32_t RealTimeGetSDTimeWorkSheet(RT_WORKSHEET *PirWorkSheet)
{
	return 0;
}
int32_t	RealTimeSetSDTimeWorkSheet(RT_WORKSHEET PirWorkSheet)
{
	return 0;
}

int32_t RealTimeGetHmsConfig(RT_CONFIGNETHMS *pHmsConfig)
{
	CConfigNetHms    configHmsServer;
	configHmsServer.update();
	CONFIG_NET_HMS &confighms = configHmsServer.getConfig();
	memcpy(pHmsConfig,&confighms,sizeof(CONFIG_NET_HMS));
	return 0;
}
int32_t	RealTimeSetHmsConfig(RT_CONFIGNETHMS HmsConfig)
{
	int ret = 0;
	if(HmsConfig.Enable == 2)
	{
		ret = g_HMS_Indian.TestSendDataToHmsServer(HmsConfig.ServerName,HmsConfig.Password);		
	}
	else
	{
		CConfigNetHms    configHmsServer;
		configHmsServer.update();
		CONFIG_NET_HMS &confighms = configHmsServer.getConfig();
		memcpy(&confighms,&HmsConfig,sizeof(CONFIG_NET_HMS));
		configHmsServer.commit();
	}
	return ret;
}
int32_t RealTimeGetOTAConfig(RT_CONFIGNETOTA *pOtaConfig)
{
	CConfigNetOta configOtaMode;
	configOtaMode.update();
	CONFIG_NET_OTA &configOta = configOtaMode.getConfig();
	pOtaConfig->Mode     = configOta.Mode;
	pOtaConfig->Interval = configOta.Interval;
	
	//memcpy(pOtaConfig,&configOta,sizeof(CONFIG_NET_OTA));
	return 0;
}
int32_t RealTimeSetOTAConfig(RT_CONFIGNETOTA OtaConfig)
{
	if((OtaConfig.Interval <= 0) || (OtaConfig.Mode < 0) || (OtaConfig.Mode > 1))
	{
		_printd("paragram error!!!\n");
		return 1;
	}
	CConfigNetOta configOtaMode;
	configOtaMode.update();
	CONFIG_NET_OTA &configOta = configOtaMode.getConfig();
	configOta.Mode     = OtaConfig.Mode;
	configOta.Interval = OtaConfig.Interval;
//	memcpy(&configOta,&OtaConfig,sizeof(CONFIG_NET_OTA));
	configOtaMode.commit();
	return 0;
}
int32_t RealTimeGetOTANewVersionInfo(RT_UPLOADNEWVERINFO *pNewVerInfo)
{
	int ret  =	g_OTA_Indian.QueryVersionInfo(pNewVerInfo);
	return ret;
}
int32_t RealTimeOTAUpgradeNewVersion(int ignore,char Filename[128])
{
	int ret  =	0;
	if(0 == ignore)
	{
		ret  =	g_OTA_Indian.CallBackUpgrade(Filename);
	}
	else
	{
		CConfigNetOta configOtaMode;
		configOtaMode.update();
		CONFIG_NET_OTA &configOta = configOtaMode.getConfig();
		strncpy(configOta.IgnoreVersion,Filename,128); 
		configOtaMode.commit();
	}
	return ret;
}

#endif
int32_t RealTimeNVRAddFaceInfo_Callback(RT_ModNewFaceInfo FaceInfo)
{
	printf("RealTimeNVRAddFaceInfo_Callback!!!!\n");
	
	return g_AiUpload.AddUserFace(FaceInfo);
}

int32_t RealTimeNVRDelFaceInfo_Callback(RT_ModifyFaceInfo FaceInfo)
{
	printf("RealTimeNVRDelFaceInfo_Callback!!!!\n");
	
	return g_AiUpload.DelUserFace(FaceInfo);
}
int32_t RealTimeNVRChangeFaceInfo_Callback(RT_ModifyFaceInfo ModifyFaceInfo,RT_ModNewFaceInfo ModNewFaceInfo)
{
	printf("RealTimeNVRChangeFaceInfo_Callback!!!!\n");
	
	return g_AiUpload.ModUserFace(ModifyFaceInfo,ModNewFaceInfo);
}
int32_t RealTimeNVRQueryFaceInfo_Callback(RT_QueryFaceInfo QueryFaceInfo,RT_QueryRespFaceInfo *RespFaceInfo)
{
#if 0
	int i;
	printf("RealTimeNVRQueryFaceInfo_Callback!!!!\n");
	RespFaceInfo->count = 50;
	for(i = 0;i < 50; i++)
	{
		RespFaceInfo->faceinfo[i].age = i;
		RespFaceInfo->faceinfo[i].glass = i%2;
		sprintf(RespFaceInfo->faceinfo[i].strName,"url_%d",i);
		strcpy(RespFaceInfo->faceinfo[i].strsex,i%2?"F":"M");
		sprintf(RespFaceInfo->faceinfo[i].strUrl,"%s/%d.jpeg","http://ali-face-tmp.oss-cn-shanghai.aliyuncs.com",i);
	}
	return 0;
#else
	return g_AiUpload.QueryUserFace(QueryFaceInfo,RespFaceInfo);
#endif
}

int32_t RealTimeSetMp3Url(char url[1024],int priority/*the biger ,the higher*/)
{
#ifdef 	MP3PLAYWITHURL
//	char url[1024] = {0};
	int  mp3PlayStatus 	 = g_MP3Play.GetMP3PlayStatus(NULL);
	int  mp3PlayPriority = g_MP3Play.GetMp3PlayCurPriority();

	printf("mp3PlayStatus:%d,mp3PlayPriority:%d\n",mp3PlayStatus,mp3PlayPriority);

//处于对讲中
	if(priority <= talkbackPri){
		_printd("The priority of talkback(%d) is more than this,so dump the music(%d)",
			talkbackPri,priority);
		return 1;
	}
	
//音乐处于播放中或者下载中的状态
	if((3 == mp3PlayStatus || 1 == mp3PlayStatus) && priority < mp3PlayPriority){
		_printd("The priority of playing music is more than this,so dump the music");
		return 1;
	}
	
	printf("MP3 URL:%s,priority:%d\n",url,priority);
	g_MP3Play.SetMP3Url(url);
	g_MP3Play.SetMp3PlayCurPriority(priority);
	
	return 0;
#else
	return 1;
#endif
}
int32_t  RealTimeMp3PlayCtrl(RT_Mp3PlayCtrl ctrl)
{
#ifdef 	MP3PLAYWITHURL
	if(MP3_CLOSE == ctrl){
		printf("MP3 Play STOP \n");
		g_MP3Play.SetMP3PlayStatus(0);
	}else if(MP3_PAUSE == ctrl){
		printf("MP3 Play PAUSE \n");
		g_MP3Play.MP3PlayPause();
	}else if(MP3_RESUME == ctrl){
		printf("MP3 Play RESUME \n");
		g_MP3Play.MP3PlayResume();
	}
	else{
		printf("Other val:%d \n",ctrl);
		g_MP3Play.SetMP3PlayStatus(0);
	}
#else
	return 1;
#endif
	return 0;
}
int32_t  RealTimeGetMp3PlayStatus(char url[1024])
{
#ifdef 	MP3PLAYWITHURL
	return g_MP3Play.GetMP3PlayStatus(url);
#endif

	return 0;
}

int32_t  RealTimeSetLedStatus(int val /*0:open 1:close*/)
{
	printf("SetLedStatus :%d\n",val);
	
	return g_ledstat.SetLedSwitch(val,1);
}

/*0:open 1:close*/
int32_t  RealTimeGetLedStatus()
{
	printf("GetLedStatus.銆傘�傘�俓n");
	
	return g_ledstat.GetLedSwitch();
}

int32_t  RealTimeSetAudioOutVolume(int val)
{
#ifdef 	MP3PLAYWITHURL
	printf("..RealTimeSetAudioOutVolume :%d\n",val);
	
	return g_MP3Play.SetMP3PlayVoiceVolume(val);
#else
	return 1;
#endif	
}
int32_t RealTimeGetAudioOutVolume()
{
#ifdef 	MP3PLAYWITHURL
	printf("RealTimeGetAudioOutVolume.銆傘�傘�俓n");

	
	return g_MP3Play.GetMP3PlayVoiceVolume();
#else
	return 1;
#endif	
}
int32_t RealTimeResetAllConfigure()
{
	printf("RealTimeResetAllConfigure.銆傘�傘�俓n");

	g_WifiLink.ResetAllCfg();
	
	return 0;
}
int32_t RealTimeResetNetWork()
{
	printf("RealTimeResetNetWork.銆傘�傘�俓n");

	g_WifiLink.ResetNetwork();

#ifdef 	MP3PLAYWITHURL
	g_MP3Play.SetMP3PlayStatus(0);
#endif
#ifdef MOTOR
	SetMotorTopPos();
#endif
	return 0;
}

int32_t RealTimeSetAlarmSwtich(RTAlarmSwitch Switch)
{
	CConfigAlarmSwitch cfgAlarmSwitch;
	cfgAlarmSwitch.update();
	CONFIG_ALARMSWITCH &cfgSwitch = cfgAlarmSwitch.getConfig();
	
	if(Switch.AlarmSwitch_Cry != RT_ALARM_REMAIN){
		cfgSwitch.bCryEnable = !Switch.AlarmSwitch_Cry;
	}
	if(Switch.AlarmSwitch_Motion != RT_ALARM_REMAIN){
		cfgSwitch.bMotionEnable = !Switch.AlarmSwitch_Motion;
	}
	if(Switch.AlarmSwitch_Human != RT_ALARM_REMAIN){
		cfgSwitch.bHumanEnable = !Switch.AlarmSwitch_Human;
	}
	if(Switch.AlarmSwitch_Voice != RT_ALARM_REMAIN){
		cfgSwitch.bVoiceEnable = !Switch.AlarmSwitch_Voice;
	}
	if(Switch.AlarmSwitch_Trace != RT_ALARM_REMAIN){
		cfgSwitch.bTraceEnable = !Switch.AlarmSwitch_Trace;
	}	

	cfgAlarmSwitch.commit();
	return 0;
}
int32_t RealTimeGetAlarmSwtich(RTAlarmSwitch *pSwitch)
{
	if( NULL == pSwitch){
		return 1;
	}

	CConfigAlarmSwitch cfgAlarmSwitch;
	cfgAlarmSwitch.update();
	CONFIG_ALARMSWITCH &cfgSwitch = cfgAlarmSwitch.getConfig();

	memset(pSwitch,0,sizeof(RTAlarmSwitch));
	if(FALSE == cfgSwitch.bCryEnable){
		pSwitch->AlarmSwitch_Cry = RT_ALARM_CLOSE;
	}
	if(FALSE == cfgSwitch.bMotionEnable){
		pSwitch->AlarmSwitch_Motion = RT_ALARM_CLOSE;
	}
	if(FALSE == cfgSwitch.bHumanEnable){
		pSwitch->AlarmSwitch_Human = RT_ALARM_CLOSE;
	}
	if(FALSE == cfgSwitch.bVoiceEnable){
		pSwitch->AlarmSwitch_Voice = RT_ALARM_CLOSE;
	}
	if(FALSE == cfgSwitch.bTraceEnable){
		pSwitch->AlarmSwitch_Trace = RT_ALARM_CLOSE;
	}	
	
	return 0;
}

int32_t  RealTimeSetAiUploadFreq(RT_UpLoadFaceFreqinfo FreqInfo)
{
	CConfigAiUpload configAiUpload;
	configAiUpload.update();
	CONFIG_AIUPLOADFREQ &configAi = configAiUpload.getConfig();

	if(FreqInfo.FaceUploadFreq > 0){
		configAi.iFaceFreq		= FreqInfo.FaceUploadFreq;
	}
	if(FreqInfo.StrangerUploadFreq > 0){
		configAi.iStrngerFreq	= FreqInfo.StrangerUploadFreq;
	}
	if(FreqInfo.CryUploadFreq > 0){
		configAi.iCryFreq		= FreqInfo.CryUploadFreq;
	}	
_printd("faceUpFreq:%ds,strangerUpFreq:%ds,cryUpFreq:%ds",
		FreqInfo.FaceUploadFreq,FreqInfo.StrangerUploadFreq,FreqInfo.CryUploadFreq);	
	configAiUpload.commit();

	return 0;
}
int32_t  RealTimeGetAiUploadFreq(RT_UpLoadFaceFreqinfo *FreqInfo)
{
	if(NULL == FreqInfo){
		_printd("FreqInfo NULL");
		return -1;
	}
	
	CConfigAiUpload configAiUpload;
	configAiUpload.update();
	CONFIG_AIUPLOADFREQ &configAi = configAiUpload.getConfig();

	FreqInfo->FaceUploadFreq		= configAi.iFaceFreq;
	FreqInfo->StrangerUploadFreq	= configAi.iStrngerFreq;
	FreqInfo->CryUploadFreq			= configAi.iCryFreq;

	return 0;
}

int32_t  RealTimeSuspend_Callback(RTSystemStatus mode)
{
	_printd("RealTimeSuspend_Callback [%d]\n",mode);

	CConfigGeneral cCfgGeneral;

	cCfgGeneral.update();
	CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
#ifdef  SUNING
		g_RecordTsManager.SetSuspendMode(mode); 
#endif

	if(mode == RT_SYSTEM_SUSPEND){
		config.iSuspendMode = 1;

#ifdef 	MP3PLAYWITHURL
		//stop mp3 playing
		g_MP3Play.SetMP3PlayStatus(0);
#endif
	}
	else{
		if( 1 == config.iSuspendMode ){
#ifdef MOTOR
			SetMotorMidPos();
			sleep(2);
#endif
			
		}
		config.iSuspendMode = 0;
	}

	cCfgGeneral.commit();

	if(mode == RT_SYSTEM_SUSPEND){
#ifdef MOTOR
		SetMotorTopPos();
#endif
	}
	else{
		RealTimeForceIFrame_Callback();
	}

	return 0;
}
RTSystemStatus RealTimeGetCurSystemStatus_Callback()
{
	printf("RealTimeGetCurSystemStatus_Callback\n");

	CConfigGeneral cCfgGeneral;
	RTSystemStatus mode;

	cCfgGeneral.update();
	CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
	if(config.iSuspendMode == 1){
		mode = RT_SYSTEM_SUSPEND;
	}
	else{
		mode = RT_SYSTEM_NORMAL;
	}

	return mode;
}

int32_t  RealTimeSetCurToPtzPoint_Callback(int id,int opt/*0:add,1:del*/)
{
	printf("RealTimeSetCurToPtzPoint_Callback\n");

	if(id < 0 || id >= MAXNUM_POINT){
		return 1;
	}
#ifdef MOTOR	
	CConfigPtzTrace cCfgPtzTrace;

	cCfgPtzTrace.update();
	CONFIG_TRACE &config = cCfgPtzTrace.getConfig();

	if(!opt){
		config.sPoint[id].Enable = 1;
		GetMotorPresetPos(&(config.sPoint[id].x),&(config.sPoint[id].y));
		_printd("id:%d...enable:%d,x:%d,y:%d",id,
			config.sPoint[id].Enable,config.sPoint[id].x,config.sPoint[id].y);
	}
	else{
		config.sPoint[id].Enable = 0;
	}
	
	cCfgPtzTrace.commit();
	
	return 0;
#else
	return 1;
#endif
}

int32_t  RealTimeGetAllPtzPoint_Callback(RT_ALLPresetPointInfo *pInfo)
{
	printf("RealTimeGetAllPtzPoint_Callback\n");

	if(NULL == pInfo){
		return 1;
	}

	int i = 0;
	CConfigPtzTrace cCfgPtzTrace;

	cCfgPtzTrace.update();
	CONFIG_TRACE &config = cCfgPtzTrace.getConfig();

	for(i = 0; i < 6 && i < MAXNUM_POINT; i ++){
		if( 1 == config.sPoint[i].Enable){
			pInfo->pointInfo[i].enable 	= RT_ENABLE;
		}
		else{
			pInfo->pointInfo[i].enable 	= RT_UNABLE;
		}
		
		pInfo->pointInfo[i].x 		= config.sPoint[i].x;
		pInfo->pointInfo[i].y 		= config.sPoint[i].y;
	}

	return 0;
}

int32_t  RealTimeSetPtzTraceRoute_Callback(RT_SetTraceRoute route)
{
	printf("RealTimeSetPtzTraceRoute_Callback\n");

	int i = 0;
	int arraySize = sizeof(route.traceInfo)/sizeof(route.traceInfo[0]);
	
	CConfigPtzTrace cCfgPtzTrace;

	cCfgPtzTrace.update();
	CONFIG_TRACE &config = cCfgPtzTrace.getConfig();

	if( RT_ENABLE ==  route.Enable){
		config.bRunTraceEnable = VD_TRUE;
	}
	else{
		config.bRunTraceEnable = VD_FALSE;
	}
		
	for(i = 0; i < arraySize && i < MAXNUM_POINT; i ++){
		if(route.traceInfo[i].iPointNum >= 0 || route.traceInfo[i].iPointNum < MAXNUM_POINT ){
			config.sPlan.iPointQueue[i] = route.traceInfo[i].iPointNum;
			config.sPlan.iStayTime[i]   = route.traceInfo[i].iStayTime;
		}
		else{
			config.sPlan.iPointQueue[i] = -1;
			config.sPlan.iStayTime[i]	= 0;
		}
	}

	arraySize = sizeof(route.runTime)/sizeof(route.runTime[0]);
	for(i = 0 ; i < arraySize && i < RUNPERIOD_NUM; i ++){
		config.sRunTime[i].startHour = route.runTime[i].Start.hour;
		config.sRunTime[i].startMin	 = route.runTime[i].Start.minute;
		config.sRunTime[i].startSec	 = route.runTime[i].Start.second;
		config.sRunTime[i].endHour 	 = route.runTime[i].End.hour;
		config.sRunTime[i].endMin 	 = route.runTime[i].End.minute;
		config.sRunTime[i].endSec 	 = route.runTime[i].End.second;
	}
	
	cCfgPtzTrace.commit();
	
	return 0;
}

int32_t RealTimeGetPtzTraceRoute_Callback(RT_SetTraceRoute *pRoute)
{
	printf("RealTimeGetPtzTraceRoute_Callback\n");

	if(NULL == pRoute){
		return 1;
	}

	int i = 0;
	int arraySize = sizeof(pRoute->traceInfo)/sizeof(pRoute->traceInfo[0]);
	CConfigPtzTrace cCfgPtzTrace;

	cCfgPtzTrace.update();
	CONFIG_TRACE &config = cCfgPtzTrace.getConfig();

	if( VD_TRUE == config.bRunTraceEnable){
		pRoute->Enable = RT_ENABLE;
	}
	else{
		pRoute->Enable = RT_UNABLE;
	}
	
	for(i = 0; i < arraySize && i < MAXNUM_POINT; i ++){
		pRoute->traceInfo[i].iPointNum = config.sPlan.iPointQueue[i];
		pRoute->traceInfo[i].iStayTime = config.sPlan.iStayTime[i];
	}
	
	arraySize = sizeof(pRoute->runTime)/sizeof(pRoute->runTime[0]);
	for(i = 0 ; i < arraySize && i < RUNPERIOD_NUM; i ++){
		pRoute->runTime[i].Start.hour	= config.sRunTime[i].startHour;
		pRoute->runTime[i].Start.minute	= config.sRunTime[i].startMin;
		pRoute->runTime[i].Start.second = config.sRunTime[i].startSec;
		pRoute->runTime[i].End.hour		= config.sRunTime[i].endHour;
		pRoute->runTime[i].End.minute	= config.sRunTime[i].endMin;
		pRoute->runTime[i].End.second	= config.sRunTime[i].endSec;
	}
	return 0;
}

int32_t RealTimeAKeyToShelter_Callback()
{
#ifdef MOTOR
	SetMotorTopPos();
#endif
	
	return 0;
}
/*-------------------------------------------------------------*/
//lock
int32_t RealTimeGetLockInfo_Callback(RT_DoorLockinfo *LockInfo)
{
#ifdef DOOR_LOCK
	if( NULL == LockInfo){
		return -1;
	}

	int ret = 1 ;
	lockInfo Info = {0};
		
	ret = GetLockStatus(&Info);

	#if 0
	LockInfo->SignalStrength= Info.SignalStrength;
	#else
	ZRT_CAM_WIFI_Status WifiStatu;
	
	g_DevExternal.Dev_GetWiFi(&WifiStatu);

	if(WifiStatu.connectionStatus != WIFI_CONNECT_FAILED &&
		WifiStatu.connectionStatus != WIFI_DISCONNECTED){
		LockInfo->SignalStrength = 100 - abs(WifiStatu.signalLevel);
	}
	else{
		LockInfo->SignalStrength= 0;
	}

	#endif
	LockInfo->Electric		= Info.Electric;

	if(1 == Info.DoorStatus){
		LockInfo->DoorStatus = 1;
	}
	else{
		LockInfo->DoorStatus = 0;
	}
	if(1 == Info.LockStatus){
		LockInfo->LockStatus = 1;
	}
	else{
		LockInfo->LockStatus = 0;
	}	

	return ret;
#else
	return 1;
#endif
}
int32_t RealTimeSetLockTempPsw_Callback(RT_DoorLockTempPasswd LockInfo)
{
#ifdef DOOR_LOCK
	printf("time:%d,psd:%s,num:%d\n",LockInfo.Time,LockInfo.Passwd,LockInfo.ValidNums);
	
	return SetLockTempPsw(LockInfo.Passwd,strlen(LockInfo.Passwd),LockInfo.Time);
#else
	return 1;
#endif
}
int32_t RealTimeGetLockTime_Callback(RT_DateTime *TimeInfo)
{
	if( NULL == TimeInfo){
		return -1;
	}
#ifdef DOOR_LOCK
	int ret = 0;
	SYSTEM_TIME Time = {0};
	
	ret = GetLockTime(&Time);
	
	TimeInfo->m_year 	= Time.year;
	TimeInfo->m_month	= Time.month;
	TimeInfo->m_day		= Time.day;
	TimeInfo->m_hour	= Time.hour;
	TimeInfo->m_minute	= Time.minute;
	TimeInfo->m_second	= Time.second;
	
	return ret;
#else
	return 1;
#endif
}
int32_t RealTimeSetLockTime_Callback(RT_DateTime TimeInfo)
{
	printf("%04d-%02d-%02d\n",TimeInfo.m_year,TimeInfo.m_month,TimeInfo.m_day);
#ifdef DOOR_LOCK
	SYSTEM_TIME Time = {0};

	Time.year		= TimeInfo.m_year;
	Time.month		= TimeInfo.m_month;
	Time.day		= TimeInfo.m_day;
	Time.hour		= TimeInfo.m_hour;
	Time.minute		= TimeInfo.m_minute;
	Time.second		= TimeInfo.m_second;
	
	SetLockTime(Time);	
	return 0;
#else
	return 1;
#endif
}
int32_t RealTimeLockOperate_Callback(RT_DoorLockOperate LockInfo)
{
	printf("time:%d,psd:%s,Action:%d\n",LockInfo.Time,LockInfo.Passwd,LockInfo.Action);
#ifdef DOOR_LOCK
	int ret = 1;

	if(1 == LockInfo.Type){
		if( 0 == LockInfo.Action || 1 == LockInfo.Action){
			ret = OpenLockByRemote();
		}
	}
	else{
		if( 0 == LockInfo.Action || 1 == LockInfo.Action){
			ret = OpenLockByPsw(LockInfo.Passwd,strlen(LockInfo.Passwd));
		}		
	}
	return ret;
#else
	return 1;
#endif
}
int32_t RealTimeGetLockVersion_Callback(RT_DoorLockVersion *VerInfo)
{
	if( NULL == VerInfo){
		return -1;
	}
#ifdef DOOR_LOCK
	strcpy(VerInfo->MacType,"11111");
	strcpy(VerInfo->FirmwareVer,"22222");
	strcpy(VerInfo->HardwareVer,"33333");
	strcpy(VerInfo->ZigbeeVer,"44444");

	return GetLockSoftVersion(VerInfo->HardwareVer);	
#else
	return 1;
#endif
}

int32_t RealTimeSetExtLedSwitch(RT_ABLE val /*0:enable 1:unable*/)
{
	_printd("SetExtLedStatus :%d\n",val);

#ifdef IPC_JZ
	if (RT_ENABLE) {
		BCAM_LedSetState(ED_LED_TYPE_WHITE_LED, 1);
	} else {
		BCAM_LedSetState(ED_LED_TYPE_WHITE_LED, 0);
	}
#endif
	return 0;
}

/*0:enable 1:unable*/
RT_ABLE RealTimeGetExtLedSwitch()
{
	_printd("GetExtLedStatus\n");
	
#if 0
#ifdef IPC_JZ
	int ret = 0;
	int ledstate;
	BCAM_LedGetState(ED_LED_TYPE_WHITE_LED, &ledstate);
	if (ledstate)
{
		ret = RT_ENABLE;
	} else {
		ret = RT_UNABLE;		
	}
	return ret;
#endif
#else
	return RT_ENABLE;
#endif
}

#ifdef  SUNING
int32_t  RealTimeSetTsUpLoadEnable_Callback(int type,int Enable)
{
	_printd("type:%d Enable:%d",type, Enable);	
	g_RecordTsManager.SetEnable(type, Enable);	
	return 0;
}	
int32_t  RealTimeUpgradeFail_Callback()
{
	//失败才调用，成功则重启后调用
	g_SN_DevAttr_OTA.SendOtaResult(1);	
	return 0;
}	

#endif

int32_t  RealTimeGetDeviceCMEI_Callback(char cmei[RT_CMEI_LEN])
{
	char DevKey[72] = {0};
	GetDevKey(DevKey);
	_printd("device DevKey:%s\n",DevKey);
	
	if(cmei == NULL)	
		return -1;
	
	memset(cmei, 0, RT_CMEI_LEN);		
	if( 0 > GetCmei(cmei) || cmei[0] == '0' || cmei[0] == 0 )
		return -1;

	_printd("device cmei:%s\n",cmei);

	return 0;
}
/*-------------------------------------------------------------*/
}
