
/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              NetIo.cpp
**Version:	              Version 0.01
**Author:	              
**Created:                
**Modified:               yang_shukui,2011-5-17   16:19
**Modify Reason:         
**Description:            
*********************************************************************/

#include <unistd.h>
#include <time.h>
#include <APIs/System.h>
#include <Net/NetIo/NetIo.h>
#include <MultiTask/Guard.h>

int NetIoGetSequence()
{
	static int seq = 0;
	if (seq >= 0x7fffffff)
	{
		seq = 0;
	}
	return ++seq;
}

CDemultipLexer::CDemultipLexer(unsigned int maxNum)
{
    m_MaxSize = maxNum;
}

CDemultipLexer::~CDemultipLexer()
{
	Clear();
}

void CDemultipLexer::Clear()
{
	HandlerList::iterator it;
	CGuard guard(m_HandlerListMutex);
	
	for (it = m_HandlerList.begin(); it != m_HandlerList.end(); it++)
	{
	    RemoveHandler((*it).second, VD_TRUE);
	}
	
#if 0
ITERATOR:
	for (it = m_HandlerList.begin(); it != m_HandlerList.end(); )
	{
		CEventHandler *handler = (*it).second;
		delete(handler);
        m_HandlerList.erase(it);
		goto ITERATOR;
	}
#endif
}

int CDemultipLexer::RegisterHandler(CEventHandler *pHandler, VD_BOOL force)
{
	HandlerList::iterator it;

	if (!pHandler)
	{
		return NETIO_RET_ERROR;
	}
    if(m_HandlerList.size() > m_MaxSize)
    {
        printf("CDemultipLexer::RegisterHandler, reach max size [%d]", m_MaxSize);
        return NETIO_RET_ERROR;
    }
	
	it = m_HandlerList.find((int)pHandler);
	if(it != m_HandlerList.end())
	{
		return NETIO_RET_ERROR;
	}
    
	m_HandlerList[(int)pHandler] = pHandler;
	printf("%p CDemultipLexer::RegisterHandler, size [%d]\n", this, m_HandlerList.size());
	return NETIO_RET_OK;
}

int CDemultipLexer::RemoveHandler(CEventHandler *pHandler, VD_BOOL force)
{
	if(force)
	{
	    Clear();
	    return NETIO_RET_OK;
	}
	//tracepoint();
	HandlerList::iterator it = m_HandlerList.find((int)pHandler);
	if(it != m_HandlerList.end())
	{
	    pHandler->m_Event = IO_EVENT_NR;
		return NETIO_RET_OK;
	}
	return NETIO_RET_ERROR;
}

void CDemultipLexer::Proccess()
{
	HandlerList::iterator it;
	int maxfd = 0;
	fd_set readfds;
    fd_set writefds;
	int fds;
	struct timeval SelectTimev;
    CEventHandler *handler = NULL;
    int fdsFlag = 0;

    CGuard guard(m_HandlerListMutex);
	
#if 0
	{
	    static int procNo = 1;
	    static time_t begin = time(NULL);
		time_t off = time(NULL) - begin;

		if (off == 0)
		{
		   off = 1;
		}
		printf("m_HandlerList.size() [%d], speed[%f]\n", m_HandlerList.size(), ((float)(procNo))/((float)(off)));
		procNo++;
	}
#endif
	{
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);

		//这里没有什么好办法了,用goto是目前能想到的最好办法了
	    ITERATOR:
		for (it = m_HandlerList.begin(); it != m_HandlerList.end(); )
		{
			handler = (*it).second;
			if (IO_EVENT_NR == handler ->m_Event)
			{
				handler = (*it).second;
				delete(handler);
                m_HandlerList.erase(it);
				//printf("%p CDemultipLexer::RemoveHandler, size [%d]\n", this, m_HandlerList.size());
				goto ITERATOR;
			}
	        
			handler->Process();
			
            if (handler->m_Handler <= 0)
            {
                continue;
            }
			if (handler->m_Handler > maxfd)
			{
				maxfd = handler->m_Handler;
			}
			
			FD_SET(handler->m_Handler, &readfds);
			
			{
				CGuard guard(handler->m_BufferListMutex);
				if (handler->m_BufferList.size() > 0)
				{
					FD_SET(handler->m_Handler, &writefds);
				}
			}

			it++;
		}

		SelectTimev.tv_usec = 10*1000;
		SelectTimev.tv_sec = 0;
		fds = select(maxfd + 1, &readfds, &writefds, NULL, &SelectTimev);
		if(fds < 0)
		{
			printf("Proccess^_^select error\n");
			usleep(1000);
		}
		else if(fds == 0)
		{
		    usleep(1000);
		}
		else
		{
		    fdsFlag = 0;
			it = m_HandlerList.begin();
			while(fds && (it != m_HandlerList.end()))
			{
				handler = (*it).second;   
				if(FD_ISSET(handler->m_Handler, &readfds) )
				{
					fdsFlag = 1;
					if(handler->m_Event & (IO_EVENT_READ|IO_EVENT_ACCEPT|IO_EVENT_CONNECT))
					{
						handler->Handler((void *)NULL, 0, handler->m_Event);
					}
				}

				if(FD_ISSET(handler->m_Handler, &writefds) )
				{
					fdsFlag = 1;
					if(handler->m_Event & IO_EVENT_WRITE)
					{
						handler->Handler((void *)NULL, 0, IO_EVENT_WRITE);
					}
				}

				if (fdsFlag)
				{
					fds--;
					fdsFlag = 0;
				}

				it++;
			}
		}
	}
}

CEventHandler::CEventHandler(CIOReactor *reactor, unsigned short port)
{
	m_Reactor = reactor;
	m_Handler = INVALID_SOCKET;
	m_Event = IO_EVENT_NR;
	m_LocalPort = port;
	m_ErrorCount = 0;
	m_Proc = NULL;
}

CEventHandler::~CEventHandler()
{
    CGuard guard(m_BufferListMutex);
	CEventHandler::BufferList::iterator it;

	for (it = m_BufferList.begin(); it != m_BufferList.end(); )
	{
	
		if((*it).ptk)
		{
		    (*it).ptk->Release();
			(*it).ptk = NULL;
		}
		it++;
	}
	m_BufferList.clear();

	if(m_Handler > 0)
	{
	    close(m_Handler);
		m_Handler = -1;
	}
}

int CEventHandler::SendMsg(void *data, int len, VD_BOOL newFlag)
{
	MSG_NODE node;
	
	CGuard guard(m_BufferListMutex);
    if (m_BufferList.size() >= MAX_BUFFER_NODE_NUM)
    {
        printf("m_BufferList [%d] is full - -\n", len);
		return NETIO_RET_ERROR;
    }

	if(!data || len < 0 || len > 1024 * 128)
	{
	    tracepoint();
	    return NETIO_RET_ERROR;
	}
	
	if (newFlag)
	{
		node.ptk = g_PacketManager.GetPacket(len);
		if(!node.ptk)
		{
		    printf("SendMsg error, g_PacketManager.GetPacket len = %d\n", len);
			return NETIO_RET_ERROR;
		}
		memcpy((void *)(node.ptk->GetBuffer()), data, len);
	    node.ptk->SetLength(len);
	}
	else
	{
	    node.ptk = (CPacket *)data;
	}
	
	node.seq = NetIoGetSequence();
	//VDDumpHex((const char * )(node.ptk->GetBuffer()), len);
    m_BufferList.push_back(node);
	return NETIO_RET_OK;
}

int CEventHandler::SendMsgNoDelay(void *data, int len)
{
    if (m_Handler < 0 || !data || len <= 0 || IO_EVENT_NR == m_Event)
    {
        printf("m_Handler %d, data %p, len %d\n", m_Handler, data, len);
        return NETIO_RET_ERROR;
    }

	fd_set writefds;
	int fds;
	struct timeval SelectTimev;

	FD_ZERO(&writefds);
	FD_SET(m_Handler, &writefds);

	SelectTimev.tv_usec = 500*1000;
	SelectTimev.tv_sec = 0;
	fds = select(m_Handler + 1, NULL, &writefds, NULL, &SelectTimev);
	if(fds <= 0)
	{
		printf("SendMsgNoDelay ^_^ select error\n");
		m_Reactor->RemoveHandler(this, VD_FALSE);
		return NETIO_RET_ERROR;
	}
	else
	{
	    if(FD_ISSET(m_Handler, &writefds) )
		{
		    if (((char *)data)[0] != '$')
		    {
		        printf("_________________________________\n%s___________________________________\n", (char *)data);
		    }
		    send(m_Handler, data, len, 0);
		}
	}
	
	return NETIO_RET_OK;
}


int CEventHandler::RecvMsg(MSG_NODE *node)
{
    if(!node)
    {
        return NETIO_RET_ERROR;
    }
	
	CGuard guard(m_BufferListMutex);
	BufferList::iterator it = m_BufferList.begin();
    if(m_BufferList.size() > 0 && it != m_BufferList.end())	
   	{
   	    node->ptk = (*it).ptk;
   	    //node->data = (*it).data;
		//node->len = (*it).len;
		node->seq = (*it).seq;
		m_BufferList.erase(it);
		return NETIO_RET_OK;
    }
	
	return NETIO_RET_ERROR;
}

void CEventHandler::SetHandlerProc(HandlerProc proc)
{
    m_Proc = proc;
}

CUdpEventHandler::CUdpEventHandler(CIOReactor *reactor, unsigned short port)
	 : CEventHandler(reactor, port)
{
	struct sockaddr_in s_addr;
	if ((m_Handler = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
	{
		perror("socket");
	}

	if(m_Handler > 0)
	{
        const int yes = 1;

        if (INVALID_SOCKET == setsockopt(m_Handler, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)))
        {
            perror("setsockopt\n");
            close(m_Handler);
			m_Handler = -1;
        }
		else
		{
	        //设置非阻塞
	        if (fcntl(m_Handler, F_SETFL, O_NONBLOCK) == -1)
	        {
	            printf("fcntl(F_SETFL, O_NONBLOCK) Error\n");
				close(m_Handler);
			    m_Handler = -1;
	        }
			else
			{				
				memset(&s_addr, 0, sizeof(struct sockaddr_in));
				s_addr.sin_family = AF_INET;
				s_addr.sin_addr.s_addr = INADDR_ANY;
				s_addr.sin_port = htons(m_LocalPort);
				if ((bind(m_Handler, (struct sockaddr *) &s_addr, sizeof(s_addr))) == -1) 
				{
					perror("bind");
					close(m_Handler);
					m_Handler = -1;
				}
				else
				{
				    printf("new CUdpEventHandler instance\n");		
				}
			}
		}
	}
}

CUdpEventHandler::~CUdpEventHandler()
{
    if( m_Handler > 0)
    {
        close(m_Handler);
		m_Handler = -1;
    }
}

int CUdpEventHandler::Handler(void *para, int paraLen, int event)
{
	socklen_t  addr_len = sizeof(m_C_addr);
	if (event & IO_EVENT_READ)
	{
		m_RecvLen = recvfrom(m_Handler, m_Buffer, sizeof(m_Buffer) - 1, 0, (struct sockaddr *)&m_C_addr, &addr_len);		
		if (m_RecvLen > 0)
		{
		    m_Buffer[m_RecvLen] = '\0';
		    if (m_Proc)
		    {
		        (this->*m_Proc)(this);
		    }
			m_ErrorCount = 0;
		}
		else if(0 <= m_RecvLen)
		{
		    CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_NR);
			printf("Proccess recvfrom data error! [len:%d], [errno : %d]\n", m_RecvLen, errno);
		}
	}
	
	if (event & IO_EVENT_WRITE)
	{
	    MSG_NODE node;
		int ret = RecvMsg(&node);
		while(NETIO_RET_OK == ret)
		{
		    m_RecvLen = sendto(m_Handler, (char *)(node.ptk->GetBuffer()), node.ptk->GetLength(), 0, (struct sockaddr *)(&m_C_addr), addr_len);
			//len = sendto(m_Handler, node.data, node.len, 0, (struct sockaddr *)(&c_addr), addr_len);
			if(-1 == m_RecvLen)
			{
			    CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_NR);
			    printf("Proccess sendto data error! [len:%d], [errno : %d]\n", m_RecvLen, errno);
				return NETIO_RET_ERROR;
			}
			//delete [] node.data;
			node.ptk->Release();
			node.ptk = NULL;
			break;
			ret = RecvMsg(&node);
		}
	}

	return NETIO_RET_OK;
}

CIoEventHandler::CIoEventHandler(CIOReactor *reactor, unsigned short port)
	 : CEventHandler(reactor, port)
{}

CIoEventHandler::~CIoEventHandler()
{
}

int CIoEventHandler::Handler(void *para, int paraLen, int event)
{
	if (event & IO_EVENT_READ)
	{
		m_RecvLen = recv(m_Handler , m_Buffer, sizeof(m_Buffer) - 1, 0);
		if (m_RecvLen > 0)
		{
		    m_Buffer[m_RecvLen] = '\0';
            if (m_Proc)
		    {
		        (this->*m_Proc)(this);
		    }
            m_ErrorCount = 0;
            
		}
		else if(0 == m_RecvLen)
		{
			printf("close by peer\n");
			m_Event = IO_EVENT_NR;
		}
		else
		{
		    CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_NR);
			printf("Proccess recv data error! [len:%d], [errno : %d]\n", m_RecvLen, errno);
		}
	}
	if (event & IO_EVENT_WRITE)
	{
		MSG_NODE node;
		int ret = RecvMsg(&node);
		while(NETIO_RET_OK == ret)
		{
			m_RecvLen = send(m_Handler, (char *)(node.ptk->GetBuffer()), node.ptk->GetLength(), 0);
			if (m_RecvLen > 0)
			{		
			    m_ErrorCount = 0;
			}
			else //-1
			{
			    CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_NR);
				return NETIO_RET_ERROR;
			}
			node.ptk->Release();
			node.ptk = NULL;
			break;
			
		    ret = RecvMsg(&node);
		}
		
	}

	return NETIO_RET_OK;
}

CEventHandlerAccept::CEventHandlerAccept(CIOReactor *reactor, unsigned short port,unsigned int maxListenNum) 
	: CEventHandler(reactor, port)
{
	struct sockaddr_in s_addr;
	if ((m_Handler = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
		perror("socket");
	}

	if (m_Handler > 0)
	{
        const int yes = 1;

        if (INVALID_SOCKET == setsockopt(m_Handler, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)))
        {
            perror("setsockopt\n");
            close(m_Handler);
			m_Handler = -1;
        }
		else
		{
	        //设置非阻塞
	        if (fcntl(m_Handler, F_SETFL, O_NONBLOCK) == -1)
	        {
	            perror("bind");
				close(m_Handler);
				m_Handler = -1;
	        }
			else
			{				
				memset(&s_addr, 0, sizeof(struct sockaddr_in));
				s_addr.sin_family = AF_INET;
				s_addr.sin_addr.s_addr = INADDR_ANY;
				s_addr.sin_port = htons(m_LocalPort);
				if ((bind(m_Handler, (struct sockaddr *) &s_addr, sizeof(s_addr))) == -1) 
				{
					perror("bind");
					close(m_Handler);
					m_Handler = -1;
				}
				else if (-1 == listen(m_Handler, maxListenNum))
		        {
		            perror("bind");
					close(m_Handler);
					m_Handler = -1;
		        }
				else
				{
				    printf("new [%d %s] CEventHandlerAccept instance\n", m_Handler, m_Reactor->m_Name);
				}
			}
		}
	}
}

CEventHandlerAccept::~CEventHandlerAccept()
{
}

int CEventHandlerAccept::Handler(void *para, int paraLen, int event)
{
	AcceptPara acceptPara;
	
	socklen_t sockLen = sizeof(struct sockaddr);
	if (event & IO_EVENT_ACCEPT)
	{
		acceptPara.sockFd = accept(m_Handler,(struct sockaddr*)&m_C_addr, &sockLen);
        if(acceptPara.sockFd <= 0)
        {
            return NETIO_RET_ERROR;
        }

		acceptPara.handler = this;
		if (m_Proc)
	    {
	        (this->*m_Proc)(&acceptPara);
	    }
		else
		{
		    close(acceptPara.sockFd);
		}
	}

	return NETIO_RET_OK;
}

CIOConnector::CIOConnector(CIOReactor *reactor, unsigned short port)
	 : CEventHandler(reactor, port)
{	 
	 memset(m_ServerIp, 0, sizeof(m_ServerIp));
	 strcpy(m_ServerIp, "192.168.1.39");
	 m_ServerPort = 6000;

	 m_Interval = 1;
	 m_LastRegistTime = 0;

	 m_Event = IO_EVENT_NR;
	 m_Handler = INVALID_SOCKET;
	 m_State = RSTATE_INIT;
}

CIOConnector::~CIOConnector()
{
    if(m_Handler > 0)
	{
		close(m_Handler);
		m_Handler = INVALID_SOCKET;
	}
}

int CIOConnector::Handler(void *para, int paraLen, int event)
{	
	//printf("m_State = %d\n", m_State);

	if(RSTATE_REGIST == m_State || RSTATE_KEEPALIVE == m_State)
	{
		if (event & IO_EVENT_READ)
		{
			m_RecvLen = recv(m_Handler, m_Buffer, sizeof(m_Buffer) - 1, 0);
			if (m_RecvLen > 0)
			{
			    m_Buffer[m_RecvLen] = '\0';
				if (m_Proc)
			    {
			        (this->*m_Proc)(this);
			    }
				m_ErrorCount = 0;
			}
			else if(0 == m_RecvLen)
			{
				printf("close by peer\n");
				m_State = RSTATE_INIT;
			}
			else
			{
				CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_READ|IO_EVENT_WRITE|IO_EVENT_CONNECT);
				printf("Proccess recv data error! [len:%d], [errno : %d]\n", m_RecvLen, errno);
			}
		}
		
		if (event & IO_EVENT_WRITE)
		{	
			MSG_NODE node;
			int ret = RecvMsg(&node);
			while(NETIO_RET_OK == ret)
			{
				m_RecvLen = send(m_Handler, (char *)(node.ptk->GetBuffer()), node.ptk->GetLength(), 0);
				if (m_RecvLen > 0)
				{
					m_ErrorCount = 0;
					//printf("send [len :%d]\n",  len);
					//VDDumpHex((const char *)(node.ptk->GetBuffer()), len);
				}
				else //-1
				{
					CHECK_SOCKET_ERROR(m_ErrorCount, IO_EVENT_READ|IO_EVENT_WRITE|IO_EVENT_CONNECT);
					return NETIO_RET_ERROR;
				}
				node.ptk->Release();
				break;
				ret = RecvMsg(&node);
			}
			
		}
	}

	return NETIO_RET_OK;
}
int CIOConnector::SetServer(char *ip, unsigned short port)
{
    if (!ip || (strlen(ip) >= sizeof(m_ServerIp)))
    {
        return NETIO_RET_ERROR;
    }
	
	memset(m_ServerIp, 0, sizeof(m_ServerIp));
	strcpy(m_ServerIp, "192.168.1.39");
    m_ServerPort = port;
    return NETIO_RET_OK;
}

int CIOConnector::Connect()
{
	time_t now = time(NULL);
	if(RSTATE_INIT == m_State)
	{
		if(now - m_LastRegistTime >= m_Interval * 2)
		{
			if(m_Handler > 0)
			{
				close(m_Handler);
				m_Handler = INVALID_SOCKET;
			}
			
			m_Handler = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);	 
			if(m_Handler <= 0)
			{
				return NETIO_RET_ERROR;
			}
			
			struct sockaddr_in serverAddr;
			memset(&serverAddr, 0, sizeof(serverAddr));
			serverAddr.sin_family = AF_INET;
			serverAddr.sin_addr.s_addr = inet_addr(m_ServerIp);
			serverAddr.sin_port = htons(m_ServerPort);
		   
			int flags = 0;
			if ((flags = fcntl(m_Handler, F_GETFL, 0)) == -1)
			{
				printf("fcntl(F_GETFL, O_NONBLOCK)");
			}
			if (fcntl(m_Handler, F_SETFL, flags | O_NONBLOCK) == -1)
			{
				printf("fcntl(F_SETFL, O_NONBLOCK)");
			}
			int ret = connect(m_Handler, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr));
			m_State = (ret > 0 ? RSTATE_REGIST : RSTATE_CONNECT);
			if(RSTATE_REGIST == m_State)
			{
			    printf("m_ServerAddr [%d %s %d] connet success!\n",
					m_Handler, m_ServerIp, m_ServerPort);
			}
			//printf("ret  %d m_State %d\n", ret, m_State);
			m_LastRegistTime = now;
		}
	}
	else if(RSTATE_CONNECT == m_State)
	{
		fd_set wds;
		FD_ZERO(&wds);
		FD_SET(m_Handler, &wds);

		struct timeval tv;
		tv.tv_sec = 0;
		tv.tv_usec = 10000; /*10 ms*/
		int iRet = select(m_Handler + 1, NULL, &wds, NULL, &tv);
		if(iRet>0 && FD_ISSET(m_Handler, &wds))
		{
			int slen = -1;
			int error = -1;
			slen = sizeof(error);
			getsockopt(m_Handler, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&slen);
			m_State = (0 == error ? RSTATE_REGIST : RSTATE_INIT);
			if(RSTATE_REGIST == m_State)
			{
			    printf("m_ServerAddr [%d %s %d] connet success!\n",
					m_Handler, m_ServerIp, m_ServerPort);
			}
			else
			{
				printf("m_ServerAddr [%s %d] connet failed!\n",
					m_ServerIp, m_ServerPort);
			}
		}
		else
		{
			tracepoint();
			m_State = RSTATE_INIT;
		}
		m_LastRegistTime = now;
	}
	
	return (int)now;
}

int CIOConnector::Process()
{
	int now = 0;
	now = Connect();
#if 0
	if(RSTATE_REGIST == m_State)
	{
		m_State = RSTATE_REGISTD;
		m_LastRegistTime = now;
	}
	else if(RSTATE_REGISTD == m_State)
	{
		if(now - m_LastRegistTime >= m_Interval)
		{
			m_State = RSTATE_INIT;
			m_LastRegistTime = now;
		} 
	}

	if(NETIO_RET_ERROR == ret)
	{
	    return NETIO_RET_ERROR;
	}


	int now = (time_t)ret;
    if(RSTATE_KEEPALIVE == m_State)
	{
		if(now - m_LastKeepAliveTime >= m_Interval)
		{
			SendKeepAliveReq();
			m_LastKeepAliveTime = now;
		} 
	}
#endif
	return NETIO_RET_OK;
}

CIOReactor::CIOReactor(const char *name, unsigned int maxNum) : CThread(name, TP_DEFAULT, 64)
{
    InitDemultipLexer(maxNum);
	m_Para = NULL;
	strcpy(m_Name, name);
}

CIOReactor::~CIOReactor()
{	
	if(m_MultipLexer)
	{
		delete m_MultipLexer;
	}
}

void CIOReactor::InitDemultipLexer(unsigned int maxNum)
{
	m_MultipLexer = new CDemultipLexer(maxNum);
}

HandlerList *CIOReactor::GetHandlerList()
{
    return &(m_MultipLexer->m_HandlerList);
}

void CIOReactor::Lock()
{
	m_MultipLexer->m_HandlerListMutex.Enter();
}

void CIOReactor::UnLock()
{
	m_MultipLexer->m_HandlerListMutex.Leave();
}

int CIOReactor::RegisterHandler(CEventHandler *pHandler, VD_BOOL force)
{
	return m_MultipLexer->RegisterHandler(pHandler, force);
}

int CIOReactor::RemoveHandler(CEventHandler *pHandler, VD_BOOL force)
{
	return m_MultipLexer->RemoveHandler(pHandler, force);
}

void CIOReactor::IoPool()
{
	m_MultipLexer->Proccess();
}

VD_BOOL CIOReactor::Run(CEventHandler *eventHandler, int event)
{
	if(eventHandler/* && eventHandler->m_Handler > 0*/)
	{
	    printf("eventHandler->m_Handler = %d\n", eventHandler->m_Handler);
		eventHandler ->m_Event = event;
		RegisterHandler(eventHandler, VD_FALSE);
	}

	if(m_bLoop)
	{
	    //printf("Thread has been started!\n");
	    return TRUE;
	}
	
	printf("CIOReactor [addr %p] Start()>>>>>>>>>>>>>>>>>>>>>\n", this);
    if (CreateThread() != TRUE)
    {
        trace("CIOReactor::Start -- Create thread error!\n");
        return FALSE;
    }
	
    return TRUE;
}

VD_BOOL CIOReactor::Stop(CEventHandler *eventHandler)
{
    if(eventHandler)
    {
        RemoveHandler(eventHandler, VD_FALSE);
		return VD_TRUE;
    }
	
    printf("CIOReactor [addr %p] Stop()>>>>>>>>>>>>>>>>>>>>>\n", this);
	if(m_bLoop)
	{
        RemoveHandler((CEventHandler *)NULL, VD_TRUE);
	}
    DestroyThread();
	
    return TRUE;
}

void CIOReactor::ThreadProc(void)
{	
	while(m_bLoop)
	{
	    IoPool();
	}
}

