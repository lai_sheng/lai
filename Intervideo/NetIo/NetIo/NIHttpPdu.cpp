#include <assert.h>
#include <Net/NetIo/HttpPdu.h>

char *g_NIHttpFiled[eHttpNr] =
{
	"Method",                 /*eHttpMethod*/
	"GET",                    /*eHttpGet*/
	"POST",                   /*eHttpPost*/
	"OPTIONS",                /*eHttpOptions*/
	"DESCRIBE",               /*eHttpDescribe*/
	"ANNOUNCE",               /*eHttpAnnounce*/
	"SETUP",                  /*eHttpSetup*/
	"PLAY",                   /*eHttpPlay*/
	"PAUSE",                  /*eHttpPause*/
	"GET_PARAMETER",          /*eHttpGetParameter*/          
	"TEARDOWN",               /*eHttpTeardown*/
	"HTTP/1.0",               /*eHttpRspHttp*/
	"HTTP/1.1",               /*eHttpRspHttp11*/
	"RTSP/1.0",               /*eHttpRspRtsp*/
	"SIP/2.0",                /*eHttpRspSip*/
    "REGISTER",               /*eHttpRegister*/
    "INVITE",                 /*eHttpInvite*/
    "CANCEL",                 /*eHttpCancel*/
    "ACK",                    /*eHttpAck*/
    "BYE",                    /*eHttpBye*/
    "INFO",                   /*eHttpInfo*/
    "MESSAGE",                /*eHttpMessage*/
    "SUBSCRIBE",              /*eHttpSubscribe*/
    "NOTIFY",                 /*eHttpNotify*/
	
    "From",                   /*eHttpFrom*/
	"To",                     /*eHttpTo*/
	"CSeq",                   /*eHttpCseq*/
	"Call-ID",                /*eHttpCallID*/
	"Contact",                /*eHttpContact*/
	"Content-Type",           /*eHttpContentType*/
	"Content-Length",         /*eHttpContentLength*/
	"URL",                    /*eHttpUrl*/
    "Via",                    /*eHttpVia*/
	"VontentTypeString",      /*eHttpVontentTypeString*/
	"Accept",                 /*eHttpAccept*/
	"User-Agent",             /*eHttpUserAgent*/
	"Host",                   /*eHttpHost*/
	"ClientAddress",          /*eHttpXClientAddress*/
	"XTransactionID",         /*eHttpXTransactionID*/
	"Set-Cookie",             /*eHttpSetCookie*/
	"Date",                   /*eHttpDate*/
	"Server",                 /*eHttpServer*/
    "Public",                 /*eHttpPublic*/
    "Cookie",                 /*eHttpCookie*/
    "Accept-Encoding",        /*eHttpAcceptEncoding*/
	"Accept-Language",        /*eHttpAcceptLanguage*/
	"Allow",                  /*eHttpAllow*/
	"Bandwidth",              /*eHttpBandwidth*/
	"Blocksize",              /*eHttpBlockSize*/
	"Scale",                  /*eHttpScale*/
	"Speed",                  /*eHttpSpeed*/
    "Conference",             /*eHttpConference*/
	"Connection",             /*eHttpConnection*/
	"Content-Base",           /*eHttpContentBase*/
	"Content-Encoding",       /*eHttpContentEncoding*/
	"Content-Language",       /*eHttpContentLanguage*/
    "Range",                  /*eHttpRange*/
    "RTP-Info",               /*eHttpRtpInfo*/
	"Session",                /*eHttpSession*/
	"Timestamp",              /*eHttpTimestamp*/
    "Transport",              /*eHttpTransport*/
	"WWW-Authenticate",       /*eHttpWwwAuthenticate*/
	"Unsupported",            /*eHttpUnsupport*/
	"Vary",                   /*eHttpVary*/
    "Expires",                /*eHttpExpires*/
	"Last-Modified",          /*eHttpLastModified*/
    "Cache-Control",          /*eHttp_x_Cache_Control*/
	"x-Accept-Retransmit",    /*eHttp_x_Accept_Retransmit*/
	"x-Accept-Dynamic-Rate",  /*eHttp_x_Accept_Dynamic_Rate*/
	"x-Dynamic-Rate",         /*eHttp_x_Dynamic_Rate*/
	"X-Sequence",             /*eHttpXSequence*/
	"x-sessioncookie",        /*eHttpx_sessioncookie*/
	"SOAPAction",             /*eHttpSoapAction*/
	"Supported",              /*eHttpSupported*/
	"Min-SE",                 /*eHttpMin_SE*/
	"Event",                  /*eHttpEvent*/
	"Max-Forwards",           /*eHttpMax_Forwards*/
	"Session-Expires",        /*eHttpSession_Expire*/
};

CNIHttpPdu::CNIHttpPdu()
{
	m_Valid = 1;
	memset(m_Method, 0, sizeof(m_Method));
	m_HeaderLength = 0;
	m_BodyLength = 0;
	m_HttpCfg.fieldNum = HTTP_FIELD_NUM;
	m_HttpCfg.fieldLen = HTTP_FIELD_LEN;
	m_HttpCfg.bodyLen = HTTP_BODY_MAX_LEN;
	m_FieldBuffer = HTTP_NULL;
	m_Header = HTTP_NULL;
	m_Body = HTTP_NULL;
	m_Stream = HTTP_NULL;

	SetParsePara(&m_HttpCfg);
}
CNIHttpPdu::CNIHttpPdu(CNIHttpPdu &pdu)
{
	m_Valid = 1;
	memset(m_Method, 0, sizeof(m_Method));
	m_HeaderLength = 0;
	m_BodyLength = 0;
	m_HttpCfg.fieldNum = HTTP_FIELD_NUM;
	m_HttpCfg.fieldLen = HTTP_FIELD_LEN;
	m_HttpCfg.bodyLen = HTTP_BODY_MAX_LEN;
	m_FieldBuffer = HTTP_NULL;
	m_Header = HTTP_NULL;
	m_Body = HTTP_NULL;
	m_Stream = HTTP_NULL;

	SetParsePara(&GetHttpCfg());

	char *field = HTTP_NULL;
	int fType = eHttpGet;
	do
	{
		field = pdu.GetField((eFieldType)fType);
		if(HTTP_NULL != field)
		{
			SetField((eFieldType)fType, field, strlen(field));
		}
	}while(fType++ < eHttpNr );

	int len = pdu.GetBodyLength();
	if(len > 0)
	{
		SetBody(pdu.GetBody(), len);
	}
}
CNIHttpPdu::~CNIHttpPdu()
{
	if(m_FieldBuffer)
	{
		delete [] m_FieldBuffer;
	}
}
int CNIHttpPdu::Copy(CNIHttpPdu *pdu)
{
    if (!pdu)
    {
        return RET_ERROR;
    }
	Reset();

	char *field = HTTP_NULL;
	int fType = eHttpGet;
	do
	{
		field = pdu->GetField((eFieldType)fType);
		if(HTTP_NULL != field)
		{
			SetField((eFieldType)fType, field, strlen(field));
		}
	}while(fType++ < eHttpNr );

	int len = pdu->GetBodyLength();
	if(len > 0)
	{
		SetBody(pdu->GetBody(), len);
	}
	return RET_OK;
}

void CNIHttpPdu::Reset(void)
{
	memset(m_FieldBuffer, 0, m_HttpCfg.fieldNum * m_HttpCfg.fieldLen * 2 + m_HttpCfg.bodyLen);
	m_Pos = eHttpMethod;
	do
	{
		m_Field[m_Pos] = m_HttpCfg.fieldNum * m_HttpCfg.fieldLen;
	}while(m_Pos++ < eHttpNr);
	m_Pos = 0;
	m_HeaderLength = 0;
	m_BodyLength = 0;
	m_NeedRePacketFlag = 1;
}
int CNIHttpPdu::IsValid(void)
{
	return m_Valid;
}
char *CNIHttpPdu::GetMethod(void)
{
	return m_Method;
}
sHttpCfg & CNIHttpPdu::GetHttpCfg(void)
{
	return m_HttpCfg;
}
int CNIHttpPdu::GetLength(void)
{
	return (m_HeaderLength + m_BodyLength);
}
char * CNIHttpPdu::GetBody(void)
{
	return m_Body;
}
int CNIHttpPdu::GetBodyLength(void)
{
	return m_BodyLength;
}
char * CNIHttpPdu::GetHeader(void)
{
	return m_Header;
}
int CNIHttpPdu::GetHeaderLength(void)
{
	return m_HeaderLength;
}
char * CNIHttpPdu::GetField(eFieldType fieldType)
{
	int fType = fieldType;
	if(eHttpMethod == fieldType)
	{
		do
		{
			if(m_HttpCfg.fieldNum * m_HttpCfg.fieldLen != m_Field[fType])
			{
				break;
			}
		}while(++fType < eHttpNr);
	}

	return (m_Field[fType] < m_HttpCfg.fieldNum * m_HttpCfg.fieldLen ? &m_FieldBuffer[m_Field[fType]] : HTTP_NULL);
}

int CNIHttpPdu::SetField(eFieldType fieldType, char *value, int len)
{
    if (!value)
    {
        return RET_ERROR;
    }
	
	int iLen = (len > 0 ? len : strlen(value));
	if(fieldType < eHttpNr)
	{
		if(fieldType >= eHttpGet && fieldType <= eHttpRspSip)
		{
			//printf("value = %d\n", fieldType);
			//一个http包只能有一个Method,要么为GET(OPTIONS...),要么为回应HTTP/1.1(RTSP/1.0...)
			int fType = eHttpGet;
			do
			{
				if(m_HttpCfg.fieldNum * m_HttpCfg.fieldLen != m_Field[fType] )
				{
					m_Field[fType] = m_HttpCfg.fieldNum * m_HttpCfg.fieldLen;
					//m_Pos--;
					//printf("value = %s\n", value);
					//break;
				}
			}while(fType++ < eHttpRspSip );
			
			memset(m_Method, 0, sizeof(m_Method));
			strcpy(m_Method, g_NIHttpFiled[fieldType]);
		}
		//printf("m_Pos = %d\n", m_Pos);
		HTTP_CHECK_RET(true, m_Pos <= m_HttpCfg.fieldNum, RET_ERROR);
		m_Field[fieldType] = m_Pos * m_HttpCfg.fieldLen;
		HTTP_CHECK_RET(true, iLen < m_HttpCfg.fieldLen, RET_ERROR);
		strncpy(&m_FieldBuffer[m_Field[fieldType]], value, iLen);
		m_FieldBuffer[m_Field[fieldType] + iLen] = '\0';
		m_NeedRePacketFlag = 1;
		m_Pos++;
		return RET_OK;
	}
	return RET_ERROR;
}

int CNIHttpPdu::SetField(eFieldType fieldType, char *value)
{
	return SetField(fieldType, value, strlen(value));
}

int CNIHttpPdu::SetBody(char *body, int len)
{
	int iLen = (len >= 0 ? len : 0);

	HTTP_CHECK_RET(true, (HTTP_NULL != body && iLen >= 0), RET_ERROR);
	m_BodyLength = iLen;
	m_Body = m_FieldBuffer + m_HttpCfg.fieldNum * m_HttpCfg.fieldLen * 2;
	HTTP_CHECK_RET(true, iLen < m_HttpCfg.bodyLen, RET_ERROR);

        if (iLen>0)
        {
            strncpy(m_Body, body, m_BodyLength);
        }
        
	m_Body[m_BodyLength] = '\0';
	m_NeedRePacketFlag = 1;
	
	char contentLenStr[16];
	memset(contentLenStr, 0, sizeof(contentLenStr));
	sprintf(contentLenStr, "%d", m_BodyLength);
	SetField(eHttpContentLength, contentLenStr, strlen(contentLenStr));
    
	return RET_OK;
}
int CNIHttpPdu::FromStream(char *data, int len)
{
	HTTP_CHECK_RET(true, (data && len >= 0), RET_ERROR);
	char *end = strstr(data, HTTP_HEADER_END);
    
	HTTP_CHECK_RET(true, HTTP_NULL != end, RET_ERROR);
	HTTP_CHECK_RET(true, data + len -4 >= end, RET_ERROR);
	HTTP_CHECK_RET(true, ((int)(end - data + strlen(HTTP_HEADER_END)) <= m_HttpCfg.fieldNum * m_HttpCfg.fieldLen), RET_ERROR);

	Reset();

	HTTP_CHECK_RET(true, (end - data + strlen(HTTP_HEADER_END) < (unsigned int)(m_HttpCfg.fieldNum * m_HttpCfg.fieldLen)), RET_ERROR);
	strncpy(m_Header, data, end - data + strlen(HTTP_HEADER_END));
	m_HeaderLength = end - data + strlen(HTTP_HEADER_END);

	HTTP_CHECK_RET(true, RET_OK == ParseHeader(), RET_ERROR);
	char *field = GetField(eHttpContentLength);
	if(field)
	{
		int bodyLen = atoi(field);
		if (32767 == bodyLen) //Rtsp over Http
		{
		    return RET_OK;
		}
		
		//printf("bodyLen <%d>, len<%d> %d\n", bodyLen, len, len - (int)( end - data + strlen(HTTP_HEADER_END)));
		if(len - bodyLen >= (int)( end - data + strlen(HTTP_HEADER_END)))
		{
			SetBody(end + strlen(HTTP_HEADER_END), bodyLen);
		}
		else
		{
		    return RET_NO_WHOLE_PACKET;
		}
	}
	return RET_OK;
}
char * CNIHttpPdu::ToStream( void )
{
	//HTTP_CHECK_RET(true, (1 == m_NeedRePacketFlag), m_Stream);
	if(0 == m_NeedRePacketFlag)
	{
		return m_Stream;
	}
	HTTP_CHECK_RET(true, RET_OK == Packet(), HTTP_NULL);	
	return m_Stream;
}
int CNIHttpPdu::SetParsePara(sHttpCfg *cfg)
{
	HTTP_CHECK_RET(true, HTTP_NULL != (char *)cfg, -1);

	int needAlloc = 0;
	int iRet = 3;

	if(cfg->fieldNum > 2 && cfg->fieldNum <= eHttpNr)
	{
		needAlloc = (m_HttpCfg.fieldNum == cfg->fieldNum ? 0 : 1);
		m_HttpCfg.fieldNum = cfg->fieldNum;
		--iRet;
	}
	if(cfg->fieldLen > 64 && cfg->fieldLen < 1024)
	{

		needAlloc |= (m_HttpCfg.fieldLen == cfg->fieldLen ? 0 : 1);
		m_HttpCfg.fieldLen = cfg->fieldLen;
		--iRet;
	}
	if(cfg->bodyLen > 1024 && cfg->bodyLen < 1024*1024)
	{
		needAlloc |= (m_HttpCfg.bodyLen == cfg->bodyLen ? 0 : 1);
		m_HttpCfg.bodyLen = cfg->bodyLen;
		--iRet;
	}

	if(1 == needAlloc)
	{
		if(m_FieldBuffer)
		{
			delete [] m_FieldBuffer;
		}
		m_FieldBuffer = new char [m_HttpCfg.fieldNum * m_HttpCfg.fieldLen * 2 + m_HttpCfg.bodyLen];
		assert(m_FieldBuffer);
		if(!m_FieldBuffer)
		{
			m_Valid = 0;
		}
		else
		{
			m_Header = m_FieldBuffer + m_HttpCfg.fieldNum * m_HttpCfg.fieldLen;
			m_Stream = m_Header;
		}
	}
	else
	{
		if(!m_FieldBuffer)
		{
			m_FieldBuffer = new char [m_HttpCfg.fieldNum * m_HttpCfg.fieldLen * 2 + m_HttpCfg.bodyLen];
			assert(m_FieldBuffer);
			if(!m_FieldBuffer)
			{
				m_Valid = 0;
			}
			else
			{
				m_Header = m_FieldBuffer + m_HttpCfg.fieldNum * m_HttpCfg.fieldLen;
				m_Stream = m_Header;
			}
		}
	}
	Reset();
	return iRet;
}
void CNIHttpPdu::DumpField()
{
	int fType = eHttpGet;
	do
	{
		printf("HttpField %d %d\n",fType, m_Field[fType]);

	}while(++fType < eHttpNr);

	printf("\n");
}

int CNIHttpPdu::ParseHeader(void)
{
	char *end = strstr(m_Header, CRLF);
	if(!end)
	{
		return RET_ERROR;
	}
	char pTitle[HTTP_FIELD_LEN] = {0};
	char pValue[HTTP_FIELD_LEN] = {0};
	HTTP_CHECK_RET(true, RET_OK == ReadName(m_Header, pTitle, sizeof(pTitle), " "), RET_ERROR);
	HTTP_CHECK_RET(true, RET_OK == ReadValue(m_Header, pValue, sizeof(pValue), " ", 0), RET_ERROR);
	HTTP_CHECK_RET(true, RET_OK == FillField(pTitle, pValue), RET_ERROR);
	//printf("##########<%s>#<%s>###########\n", pTitle, pValue);
	strcpy(m_Method, pTitle);
	return Parse(end + strlen(CRLF));
}

int CNIHttpPdu::ReadName(char *begin,char *pTitle, int len, char *seperate)
{
	HTTP_CHECK_RET(true, begin && pTitle && seperate, RET_ERROR);
	char *end = strstr(begin, seperate);
	HTTP_CHECK_RET(true, HTTP_NULL != end, RET_ERROR);
	HTTP_CHECK_RET(true, end - begin < len, RET_ERROR);
	strncpy(pTitle, begin, end - begin);
	pTitle[end - begin] = '\0';
	//printf("%s\n", pTitle);
	return RET_OK;
}

int CNIHttpPdu::ReadValue(char *begin, char *pValue, int len, char *seperate, int white)
{
	HTTP_CHECK_RET(true, white >=0 && white <= 1, RET_ERROR);
	HTTP_CHECK_RET(true, begin && pValue && seperate, RET_ERROR);
	char *end1 = strstr(begin, seperate);
	char *end2 = strstr(begin,"\r\n");
	HTTP_CHECK_RET(true, end1 && end2, RET_ERROR);
	HTTP_CHECK_RET(true, end2 - end1 > 1 && end2 - end1 -1 < len, RET_ERROR);
	strncpy(pValue, end1 + white + 1, end2 - end1 - white);
	pValue[end2 - end1 -1] = '\0';
	//printf("%s\n", pValue);
	return RET_OK;
}

int CNIHttpPdu::FillField(char *pTitle, char *pValue)
{
	HTTP_CHECK_RET(true, pTitle && pValue, RET_ERROR);
	int fType = 0;
	do
	{
		if(0 == strcmp( pTitle, g_NIHttpFiled[fType]))
		{
			return SetField((eFieldType)fType, pValue, strlen(pValue));
		}
	}while(++fType < eHttpNr);
	return RET_ERROR;
}

int CNIHttpPdu::Parse(const char* data)
{
    int methodLine = 1;
	char pTitle[128] = {0};	//seperate之前的子串
	char pValue[128] = {0};	//seperate之后的子串

	char *begin = (char *)data;
	char *end = strstr(begin,CRLF);
	while ( end )
	{
		if(RET_OK != ReadName(begin,  pTitle, sizeof(pTitle), ":") ||
				RET_OK != ReadValue(begin, pValue,sizeof(pValue), ":", 0))
		{
			return RET_ERROR;
		}
		FillField(pTitle, pValue);
		begin = end + strlen(CRLF);
		end = strstr(begin,CRLF);

		if(end == begin)
		{
			return RET_OK;
		}
		if (1 == methodLine)
		{
		    
		}
	}
	return RET_OK;
}

int CNIHttpPdu::Packet()
{
	m_HeaderLength = 0;
	int fType = eHttpGet;
	char *field = HTTP_NULL;
	int len = 0;
	do
	{
		field = GetField((eFieldType)fType);
		//printf("%d\n", fType);
		if(field)
		{
			//printf("#####%s####\n",field);
			len = strlen(g_NIHttpFiled[fType]);
			strncpy(m_Header + m_HeaderLength, g_NIHttpFiled[fType], len);
			m_HeaderLength += len;

			const char* sep = ((fType >= eHttpGet && fType < eHttpFrom) ? " " : ": ") ;
			len = strlen(sep);
			strncpy(m_Header + m_HeaderLength, sep, len);
			m_HeaderLength += len;

			len = strlen(field);
			strncpy(m_Header + m_HeaderLength, field, len);
			m_HeaderLength += len;

			len = strlen(CRLF);
			strncpy(m_Header + m_HeaderLength, CRLF, len);
			m_HeaderLength += len;

			m_Header[m_HeaderLength] = '\0';
		}
	}while(++fType < eHttpNr);
	len = strlen(CRLF);
	
	strncpy(m_Header + m_HeaderLength, CRLF, len);
	m_HeaderLength += len;
	if(m_BodyLength > 0)
	{	
		memmove(m_Header + m_HeaderLength, m_Body, m_BodyLength);
		m_Header[m_HeaderLength + m_BodyLength] = '\0';
	}
	m_NeedRePacketFlag = 0;
	return RET_OK;
}

int HttpChange(char *src, int srcLen, char *dst, int &dstLen)
{
	if(!src || srcLen <= 0 || srcLen > 1024*4 || !dst)
	{
		return -1;
	}
    memset(dst, 0, dstLen);
	
	char *tSrc = src;
	char *tDst = dst;
    int tDstLen = 0;
	char preChar = '\0';
	while (*tSrc != '\0' && (tSrc - src) < srcLen )
	{
        if ('\n' == *tSrc && '\r' != preChar)
        {
			if (dstLen <= tDst - dst)
			{
				return -1;
			}
			*tDst = '\r';
			tDst++;
			tDstLen++;
        }
		
		if (dstLen <= tDst - dst)
		{
			return -1;
		}
		*tDst = *tSrc;
		tDst++;
		tDstLen++;

		preChar = *tSrc;
		tSrc++;
	}

	printf("%d %02x %02x %02x %02x\n", tDstLen,
		(unsigned char)dst[tDstLen - 4],
		(unsigned char)dst[tDstLen - 3],
		(unsigned char)dst[tDstLen - 2],
		(unsigned char)dst[tDstLen - 1]);

    int crlfAddNo = 0;
	if ('\r' != dst[tDstLen - 2] || '\n' != dst[tDstLen - 1])
	{
		crlfAddNo = 2;
	}
	else if ('\r' != dst[tDstLen - 4] || '\n' != dst[tDstLen - 3])
    {
		crlfAddNo = 1;
    }
	for (int iCrlfAddNo = 0; iCrlfAddNo < crlfAddNo; iCrlfAddNo++)
	{
		if (dstLen - 1 <= tDst - dst)
		{
			return -1;
		}
		*tDst = '\r';
		tDst++;
		tDstLen++;
		
		*tDst = '\n';
		tDst++;
		tDstLen++;
	}
	
    dstLen = tDstLen;
	
	return 0;
}
