#include <Net/NetIo/NetType.h>
#include <Net/NetIo/NIPdu.h>
#include <stdio.h>
#include <string.h>

CNIPdu::CNIPdu(void)
{
    memset(&m_Info, 0, sizeof(m_Info));
	m_Hdr = NULL;
}

CNIPdu::~CNIPdu(void)
{
    if (m_Info.info)
    {
        delete [] m_Info.info;
		m_Info.info = NULL;
    }
}

bool CNIPdu::parseBody(char* pBuff, int iLen, int &outLen)
{	
	return true;
}

bool CNIPdu::packetBody(char* &pData, int &iLen)
{    
	return true;
}

bool CNIPdu::GetPduInfo(char *& info, int &len) 
{
    if (m_Info.info)
    {
        info = m_Info.info;
        len = m_Info.len;
        return true;
    }
    return false; 
}

bool CNIPdu::SetPduInfo(char *info, int len) 
{ 
    if (!info)
    {
        return false;
    }

	if (m_Info.info)
	{
	    delete [] m_Info.info;
		m_Info.info = NULL;
	}
    m_Info.info = new char [len];
	memcpy(m_Info.info, info, len);
	m_Info.len = len;

	return true;
}

void *CNIPdu::GetPdu()
{
    return m_Hdr;
}

void CNIPdu::SetPdu(void *hdr)
{
	if (!hdr)
	{
	    return;
	}

	m_Hdr = hdr;
}

