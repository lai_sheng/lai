#include <Net/NetIo/NetType.h>
#include <Net/NetIo/NIProc.h>
#include <Configs/ConfigBase.h>
#include <Main.h>

CNIProc::CNIProc()
{
}

CNIProc::~CNIProc(void)
{
}

int CNIProc::Processor(CNIMsg *msg, CNIPdu *pReq, CNIPdu *& pRsp)
{
	if(!msg || !pReq)
	{
		return FAILURE_RET;
	}
    int iRet = FAILURE_RET;
 	iRet = Process(msg->iCmd, pReq, pRsp);
	if(FAILURE_RET != iRet & CONFIG_APPLY_REBOOT)
	{
 	    SystemSleep(1000);
		g_Challenger.Reboot();
	}
    return iRet;
}


bool CNIProc::RegCmd(SigProc pProc, CObject *pObj, int iCmd)
{
    NICmdProc proc;
	proc.proc = pProc;
	proc.obj = pObj;
	
    std::pair<NICmdTable::iterator, bool> ret = m_NICmdTable.insert(NICmdTable::value_type(iCmd, proc));
    if (!ret.second)
    {
        trace("CNIProc::RegisterCmd [iCmd = %d]had been register!!\n",iCmd);
        return false;
    }

    return true;
}

bool CNIProc::UnRegCmd( int iCmd)
{   
    if(m_NICmdTable.erase(iCmd))
    {
        return true;
    }
    else
    {
        trace("CNIProc::UnRegisterCmd [iCmd = %d] failed!!\n",iCmd);
    }

    return false;
}

int CNIProc::Process(int iCmd, CNIPdu* pReq, CNIPdu*& pResp)
{
    NICmdTable::iterator iter = m_NICmdTable.find(iCmd);
    int iRet = FAILURE_RET;

    if (iter != m_NICmdTable.end())
    {
        NICmdProc &cmdProc = (*iter).second;
		iRet = (cmdProc.obj->*cmdProc.proc)(pReq, pResp);
    }
	else
	{
	    NITRIP();
	}

    return iRet;
}

