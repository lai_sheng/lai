#include <Net/NetIo/SipPdu.h>

#define xmlError(x) printf("xmlError, line<%d> <%s>\n", __LINE__, x)

CSipPdu::CSipPdu()
{
	m_pHttpHdr = NULL;
	m_pXmlDocs = NULL;
    Init();
}

CSipPdu::~CSipPdu()
{
    Init();
	if (m_pHttpHdr)
	{
		delete m_pHttpHdr;
	}
}

void CSipPdu::Init(void)
{
	m_pRootElement = NULL; 
    m_psubRootElement = NULL;
    m_xmlStr.clear();
	if (m_pXmlDocs)
	{
	    delete m_pXmlDocs;
		m_pXmlDocs = NULL;
	}
}


/*主动上报的打包处理*/
CSipPdu* CSipPdu::createPdu()
{
    char* field = 0;

    CSipPdu* pPdu = new CSipPdu();
	assert(pPdu);
    if (!pPdu)
    {
        return NULL;
    }
    CNIHttpPdu* pObj = new CNIHttpPdu();
	assert(pObj);
    if (!pObj)
    {
        delete pPdu;
        return NULL;
    }
    
	
    field = "*/*";
    pObj->SetField(eHttpAccept, field, strlen(field));
    field = "192.168.177";
    pObj->SetField(eHttpUrl, field, strlen(field));
	field = "application/x-www-form-urlencoded";
    pObj->SetField(eHttpContentType, field, strlen(field));
	field = "VISIONDIGI DVR/DVS";
    pObj->SetField(eHttpUserAgent, field, strlen(field));
	field = "192.168.39";
    pObj->SetField(eHttpHost, field, strlen(field));
    pPdu->setHdr(pObj);
    
    pPdu->packetBefore(NULL, 0, false);
    
    return pPdu;
}

CSipPdu* CSipPdu::createAck(CSipPdu* pReqPdu, int iResult)
{
    if (!pReqPdu || !pReqPdu->m_pHttpHdr)
    {
        return NULL;
    }
    CSipPdu* pRspPdu = new CSipPdu();
	assert(pRspPdu);
    if (!pRspPdu)
    {
        return NULL;
    }
    CNIHttpPdu* pObj = new CNIHttpPdu(*pReqPdu->m_pHttpHdr);
	assert(pObj);
    if (!pObj)
    {
        delete pRspPdu;
        return NULL;
    }

    char* feild = 0;
    int len = 0;

    feild = "200 OK";
    len = strlen(feild);
    pObj->SetField(eHttpRspSip, feild, len);
    feild = "text/xml; charset=gb2312";
    len = strlen(feild);
    pObj->SetField(eHttpContentType, feild, len);
    pRspPdu->setHdr(pObj);
    
    pRspPdu->packetBefore(NULL, iResult, false);

    return pRspPdu;
}

char* CSipPdu::getNode(TiXmlElement* pRootElement, char* pTagName)
{
	m_xmlStr.clear();
    if (pRootElement)
    {
        TiXmlNode* Child = pRootElement->FirstChild( pTagName);
        if (Child)
        {
            TiXmlNode* Child1 = Child->FirstChild();
            if (Child1 && Child1->Value())
            {
				m_xmlStr += Child1->Value();
                pRootElement->RemoveChild(Child);
            }
        }
    }
    return ((m_xmlStr.size() > 0) ? (char* )m_xmlStr.c_str() : NULL);
}

TiXmlElement* CSipPdu::addNode(TiXmlElement* pRootElement, 
                               const char* subTitle, 
                               const char* value)
{      
    if (!pRootElement)
    {
        return NULL;
    }
	TiXmlElement subItem(subTitle);
	TiXmlText txt(value);
	
    subItem.InsertEndChild(txt);

    TiXmlNode* child = pRootElement->InsertEndChild(subItem);
    if (child)
    {
        return child->ToElement();
    }
    return NULL;
}

TiXmlElement* CSipPdu::findSubRoot(TiXmlElement* pRootElement,const char* tagName, int iIndex)
{
    TiXmlNode* Child = 0;
    int iCount = 0;

    if (pRootElement)
    {
        for( Child = pRootElement->FirstChild(tagName); Child; Child = Child->NextSibling(tagName) )        
        {            
            if (iCount ==iIndex)
            {
                TiXmlElement* pTemElement =  Child->ToElement();
                if (pTemElement)
                {
                    return pTemElement;
                }

                return NULL;
            }
            iCount++;        
        }
    }

    return NULL;
}

/* 
分析a(num)/b(num)/,内部构造字符串为分析和系列化包做准备
*/
bool CSipPdu::parseString(const char* strNodePath,
                          size_t& iStartPos,
                          char* strNodeName, 
                          int& iNodeNum)
{
	char* posDivStr = NULL;
	char* posSubStr = NULL;
    int len = 0;

    iNodeNum = -1;

    len = strlen(strNodePath);
	if((int)iStartPos >= len)
	{
		return false;
	}
//LE0 Amba
    posDivStr = strstr((char *)(strNodePath+iStartPos), "/");
    posSubStr = strstr((char *)(strNodePath+iStartPos), "(");
    /* 存在/时候 */
    if (posDivStr)
    {
        if (posSubStr && posSubStr < posDivStr)/* tagA(n)/tagB----*/
        {
			len = posSubStr - strNodePath + iStartPos;
			if(len >= XML_NODE_LEN)
			{
				return false;
			}
            memcpy(strNodeName, strNodePath+iStartPos, len);

            char strNodeNum[16] = {0};
			char* tmpSubStr = strstr(posSubStr, ")");
			if(!tmpSubStr)
			{
				return false;
			}
            memcpy(strNodeNum, posSubStr+1, tmpSubStr-posSubStr-1);
            iNodeNum = atoi(strNodeNum);
            iStartPos = posDivStr - strNodePath +1;
            return true;
        }
        else
        {
            /* tagA/tagB/--- 或者tagA/*/
			len = posDivStr - strNodePath + iStartPos;
			if(len >= XML_NODE_LEN)
			{
				return false;
			}
			
            memcpy(strNodeName, strNodePath+iStartPos, len);
            iNodeNum = 0; 
            iStartPos = posDivStr - strNodePath +1;
            return true;
        }
    }

    return false;
}

int CSipPdu::parseBody(const char* strData, int len, int &iLen)
{
	char* str = (char* )strData;

	if(!m_pHttpHdr)
	{
        m_pHttpHdr = new CNIHttpPdu();
		assert(m_pHttpHdr);
		if(!m_pHttpHdr)
		{
		    return false;
		}
	}
    int iRet = m_pHttpHdr->FromStream(str, len);
	if (CNIHttpPdu::RET_OK != iRet)
	{
		if(CNIHttpPdu::RET_NO_WHOLE_PACKET == iRet)
		{
			return CNIHttpPdu::RET_NO_WHOLE_PACKET;
		}
		printf("FromStream Error!\n%s\n", str);
		return CNIHttpPdu::RET_ERROR;
	}
	iLen = m_pHttpHdr->GetLength();
	
	bool bRet = true;
	bRet = parseXml();
	
	return true == bRet ? CNIHttpPdu::RET_OK : CNIHttpPdu::RET_ERROR;
}

bool CSipPdu::parseXml()
{
    char* strBody = m_pHttpHdr->GetBody();
	int len = m_pHttpHdr->GetBodyLength();
    if (len > 0 )
    {
        Init();
        if(!m_pXmlDocs)
        {
	        m_pXmlDocs = new TiXmlDocument;
	        if (!m_pXmlDocs)
	        {
	            return false;
	        }
        }
		m_pXmlDocs->Parse(strBody);
		if ( !m_pXmlDocs->Error())
		{
			m_pRootElement = m_pXmlDocs->RootElement();;
			m_psubRootElement = m_pRootElement;
			return true;
		}
		printf("XMLPduBody::deserialize in %s: %s\n", m_pXmlDocs->Value(), m_pXmlDocs->ErrorDesc() );
    }
	

    return false;
}

void CSipPdu::packetBefore(char* strCmd, int iResult, bool bReq) 
{   
    Init();
	
 	if(!m_pXmlDocs)
 	{
	    m_pXmlDocs = new TiXmlDocument;
		assert(m_pXmlDocs);
	    if (!m_pXmlDocs)
	    {
	        return;
	    }
 	}
    m_pXmlDocs->Parse( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
    if (!m_pXmlDocs->Error())
    {
    }
}

char* CSipPdu::packetBody()
{
	char* str = NULL;
    if (m_pHttpHdr)
    {
        str = packetXml(); 
		if (str)
		{
			m_pHttpHdr->SetBody(str, strlen(str));
		}
        str = m_pHttpHdr->ToStream();
    }
    return str;
}

char* CSipPdu::packetXml()
{
	m_xmlStr.clear();
    if (m_pXmlDocs)
    {
    #if 0
		TiXmlPrinter printer;
		//格式化输出，即加入了Tab和space
		//printer.SetStreamPrinting();
		m_pXmlDocs->Accept( &printer );
	    m_xmlStr = printer.CStr();
	#endif
	    std::ostringstream strStream;
        strStream<<(*m_pXmlDocs);
        m_xmlStr = strStream.str();
    }

    return (m_xmlStr.size() > 0 ? (char* )m_xmlStr.c_str() : NULL);
}

bool CSipPdu::changeRoot(char* pTagPath, bool bAddSubRoot)
{
    /*入参为空时回到根节点*/
    if (NULL == pTagPath)
    {
        m_psubRootElement = m_pRootElement;
        return true;
    }
	
	TiXmlElement* pTemElement = m_pRootElement;
    TiXmlElement* pTemElementChild = NULL;
	char strNodePath[XML_NODE_LEN];
    char strNodeName[XML_NODE_LEN];
    size_t index = 0;
    int iNodeNum = -1;
    int len = 0;
	
	memset(strNodePath, 0, sizeof(strNodePath));
	memset(strNodeName, 0, sizeof(strNodeName));
	len = strlen(pTagPath);
	if(XML_NODE_LEN > len + 1)
	{
		memcpy(strNodePath, pTagPath, len);
        strcat(strNodePath, "/");/* 添加一个好分析 */
	}
	else
	{
	    return false;
	}
	
    if (!m_pRootElement)
    {
        if (!parseString(strNodePath, index, strNodeName, iNodeNum))
        {
            return false;
        }

		TiXmlElement itemRoot(strNodeName);
		if (!m_pXmlDocs)
		{
		    return false;
		}
		
		m_pXmlDocs->InsertEndChild(itemRoot); 
		TiXmlNode* node = m_pXmlDocs->FirstChild(strNodeName);
		if (node)
		{
			TiXmlElement* itemRoot = node->ToElement();
			if (!itemRoot)
			{
    			return false;
			}
			
			m_pRootElement = itemRoot;
			m_psubRootElement = m_pRootElement;
		}
    }
	
	{
		memset(strNodePath, 0, sizeof(strNodePath));
		memset(strNodeName, 0, sizeof(strNodeName));
	    while(1)
	    {
	        if (!parseString(strNodePath, index, strNodeName, iNodeNum))
	        {
	            break;
	        }

	        pTemElementChild = findSubRoot(pTemElement, strNodeName, iNodeNum);
	        if (NULL == pTemElementChild && true ==bAddSubRoot)
	        {
	            pTemElementChild = addNode(pTemElement, strNodeName, "");
	        }    

	        if (pTemElementChild)
	        {
	            pTemElement =  pTemElementChild;
	            m_psubRootElement = pTemElement;
	            continue;
	        }

	        printf("changeRoot:Error subRoot [%s]\n", strNodeName);
	        m_psubRootElement = NULL;
	        break;
	    }

	    if (m_psubRootElement) 
		{
		    return true;
	    }
	}

    return false;
}


/****************************************/
//添加字符串
bool CSipPdu::addData(char* tagName,  char* tagValue, int iLen)
{
    if (!m_psubRootElement || !tagName || !tagValue || (int)strlen(tagValue) != iLen)
    {
        return false;
    }

    addNode(m_psubRootElement, (const char* )tagName, (const char* )tagValue);
    return true;
}

//添加整型
bool CSipPdu::addData(char* tagName,  int iValue)
{
    if (!m_psubRootElement || !tagName)
    {
        return false;
    }

    /* 转化成字串*/
    char strTmp[32];
    memset(strTmp,0,sizeof(strTmp));
    sprintf(strTmp,"%d",iValue);
            
    addNode(m_psubRootElement, (const char *)tagName, strTmp);
    return true;
}

bool CSipPdu::addAttrData(char* tagName,  int iValue)
{
    if (!m_psubRootElement || !tagName)
    {
        return false;
    }

    m_psubRootElement->SetAttribute(tagName,iValue);

    return true;
}

bool CSipPdu::addAttrData(char* tagName, char* tagValue, int iLen)
{
    if (!m_psubRootElement || !tagName)
    {
        return false;
    }

    const char* pValue = tagValue;
    m_psubRootElement->SetAttribute(tagName, pValue);

    return true;
}

char* CSipPdu::getData(char* pTagName)
{
    if (m_psubRootElement)
    {
    	return getNode(m_psubRootElement, pTagName);
    }

     return NULL;
}


int CSipPdu::getData(char* pTagName,int& iValue)
{
    iValue = -2;
    getData(pTagName);
    if (m_xmlStr.size() > 0)
    {
        iValue = atoi(m_xmlStr.c_str());
    }

    return iValue;
}

int CSipPdu::getAttrData(char * pTagName, int & iValue)
{
    if (m_psubRootElement)
    {
        m_psubRootElement->Attribute(pTagName,&iValue);
    }
    
    return iValue;
}

const char* CSipPdu::getAttrData(char* pTagName)
{
    if (m_psubRootElement)
    {
        return m_psubRootElement->Attribute(pTagName);
    }

    return NULL;
}

