
#ifndef NET_IO_H__
#define NET_IO_H__
#include <ez_libs/ez_socket/ez_socket.h>
#include <Multitask/Thread.h>
#include <Multitask/Mutex.h>
#include <Multitask/Guard.h>
#include <System/Packet.h>
#include <System/ABuffer.h>
#include <map>
#include <vector>

using namespace std;

class CIOReactor;
class CEventHandler;
class CUdpEventHandler;

#ifndef WIN32
#define INVALID_SOCKET -1
#define CLOSESOCKET close
#else
#define CLOSESOCKET closesocket
#define socklen_t int

#endif

#define MAX_RTP_PAYLOAD_LEN 1400

typedef enum
{
	IO_EVENT_READ     = 0x2,
	IO_EVENT_WRITE    = 0x4,
	IO_EVENT_ACCEPT   = 0x8,
	IO_EVENT_CONNECT  = 0x10,
	IO_EVENT_NR,
}IO_EVENT;

typedef enum
{
	NETIO_RET_OK = 0,
	NETIO_RET_ERROR,
}ENetIO;

typedef enum
{
    RSTATE_INIT       = 0,
	RSTATE_CONNECT,
	RSTATE_REGIST,
	RSTATE_KEEPALIVE,
}RSTATE;

typedef enum
{
    EVENT_STATE_NORMAL = 0,
	EVENT_STATE_RELEASE,
}eEventState;

typedef struct
{
	CPacket *ptk;
	int seq;
	int cmd;
}MSG_NODE;

typedef struct
{
	unsigned char csrccount:4;
	unsigned char extension:1;
	unsigned char padding:1;
	unsigned char version:2;
	unsigned char payloadtype:7;
	unsigned char marker:1;
	unsigned short seqnum;
	unsigned int timestamp;
	unsigned int ssrc;
}RTP_HDR;
#define RTP_HDR_SIZE (sizeof(RTP_HDR))

#define CHECK_SOCKET_ERROR(errorCount, event) do\
{                                               \
    if(++errorCount > 100)                      \
    {                                           \
        if (IO_EVENT_NR == event)				\
        {										\
            tracepoint();                       \
            m_Event = IO_EVENT_NR;				\
		}										\
	}                                           \
}while(0);

#define CHECK_SOCKET_ERROR_FUNC(errorCount, event, func) do\
{                                               \
    if(++errorCount > 100)                      \
	{                                           \
		if (IO_EVENT_NR == event)				\
		{										\
		    tracepoint();                       \
			m_Event = IO_EVENT_NR;				\
		}										\
		func;								    \
	}                                           \
}while(0);

#define CHECK_RELEASE(x) do                                  \
{                                                            \
	if (x->m_Reactor->m_CurrentTime - x->m_KeepAlive > 15000)\
	{                                                        \
	    if (x->m_Event != IO_EVENT_ACCEPT)                   \
		{                                                    \
		    printf("%u %u\n", x->m_KeepAlive, x->m_Reactor->m_CurrentTime);\
	        VD_TRACE("Time out %p\n", x);                    \
	        x->m_Event = IO_EVENT_NR;                        \
		}                                                    \
	}                                                        \
}while(0)

typedef std::map<int, CEventHandler* > HandlerList; 
class CDemultipLexer
{
public:
	CDemultipLexer(unsigned int maxNum);
	virtual ~CDemultipLexer();
	
	int RegisterHandler(CEventHandler *pHandler, VD_BOOL force);
	int RemoveHandler(CEventHandler *pHandler, VD_BOOL force);
	virtual void Proccess();
	
public:
	HandlerList m_HandlerList;
	CMutex m_HandlerListMutex;
	void Clear();
	
private:
	
protected:
	unsigned int m_MaxSize;

private:
    int m_SelectErrorNo;
};

typedef void (CObject::*HandlerProc)(void *para);
#define MAX_BUFFER_NODE_NUM 640

class CEventHandler : public CObject
{
public:
	typedef std::vector<MSG_NODE> BufferList; 

public:
	CEventHandler(CIOReactor *reactor, unsigned short port);
	virtual ~CEventHandler();

	virtual int Handler(void *para, int paraLen, int event) = 0;
    virtual int SendMsg(void *data, int len, VD_BOOL newFlag = VD_TRUE);	
	virtual int SendMsgNoDelay(void *data, int len);
	virtual int RecvMsg(MSG_NODE *node);

    /*各handler的特性处理*/
	virtual int Process(){ return NETIO_RET_OK; }
	virtual void SetHandlerProc(HandlerProc proc);
	virtual void Notify(int msg, int flag){}
	void SetExpire(uint expire);
	
public:
	int m_Handler;
	int m_Event;
	int m_State;
	CIOReactor *m_Reactor;
	BufferList m_BufferList;
	CMutex m_BufferListMutex;
	CABuffer m_RecvBuf;
	struct sockaddr_in m_C_addr;
	HandlerProc m_Proc;

	char m_Buffer[2048];
	int m_RecvLen;
	int m_ErrorCount;
	bool m_Lock;
	uint m_KeepAlive;
	uint m_Expire;
	unsigned short m_LocalPort;

protected:
	int m_SendNo;
};

class CUdpEventHandler : public CEventHandler
{
public:
	CUdpEventHandler(CIOReactor *reactor, unsigned short port = 8081);
	virtual ~CUdpEventHandler();

	int Handler(void *para, int paraLen, int event);
	
private:
};

class CIoEventHandler : public CEventHandler
{
public:
	CIoEventHandler(CIOReactor *reactor, unsigned short port = 0);
	virtual ~CIoEventHandler();

	int Handler(void *para, int paraLen, int event);

private:
};

class CEventHandlerAccept : public CEventHandler
{
public:
	typedef struct 
	{
		CEventHandler *handler;
		int sockFd;
	}AcceptPara;
	
public:
	CEventHandlerAccept(CIOReactor *reactor, unsigned short port = 8080, unsigned int maxListenNum = 128);
	virtual ~CEventHandlerAccept();
	
	int Handler(void *para, int paraLen, int event);

private:
};

class CIOConnector : public CEventHandler
{
public:
	CIOConnector(CIOReactor *reactor, unsigned short port = 0);
	virtual ~CIOConnector();

	int Handler(void *para, int paraLen, int event);
	int Process();
	int SetServer(char *ip, unsigned short port);
	
private:
	int Connect();
		
private:
	char m_ServerIp[32];
	unsigned short m_ServerPort;
	
	/*上一次连接或注册时间*/
	time_t m_LastRegistTime;
	int m_Interval;
};


class CIOReactor : public CThread
{	
public:
	CIOReactor(const char *name = "IOReactor", unsigned int maxNum = 128);
	virtual ~CIOReactor();

	VD_BOOL Run(CEventHandler *eventHandler, int event);
	VD_BOOL Stop(CEventHandler *eventHandler);

	int RegisterHandler(CEventHandler *pHandler, VD_BOOL force);
	int RemoveHandler(CEventHandler *pHandler, VD_BOOL force);
	
	void IoPool();
	void InitDemultipLexer(unsigned int maxNum);
	HandlerList *GetHandlerList();
	void Lock();
	void UnLock();
	
	void ThreadProc();
	
public:
	void *m_Para;
	char m_Name[32];
	uint m_CurrentTime;
	
protected:
	CDemultipLexer *m_MultipLexer;
};

extern void InitSocket();
extern void ReleaseSocket();
extern  int NetIoGetSequence();
extern void VDDumpHex(const char *pData,int len);

#ifdef WIN32
#define NET_ERRNO WSAGetLastError()
#else 
#define NET_ERRNO errno
#endif

#endif

