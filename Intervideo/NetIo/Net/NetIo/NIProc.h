#ifndef _NI_CMD_PROC_H_
#define _NI_CMD_PROC_H_

#include <System/Object.h>
#include <Net/NetIo/NIPdu.h>
#include <map>

using namespace std;

class CNIMsg
{
public:
	CNIMsg()
	{
	    iCmd = 0; 
        iSessionId = 0;
        iDataLen = 0;
        pData = NULL;
	}
	virtual ~CNIMsg(){}
	
	virtual void *GetInfo(){ return NULL; }
	
public:
    int           iCmd; 
    unsigned int  iSessionId;
    unsigned int  iDataLen;
    void          *pData;
};

typedef int (CObject::*SigProc)(CNIPdu*, CNIPdu*&);

class CNIProc
{	
public:
    CNIProc(void);
    virtual ~CNIProc(void);

    int Processor(CNIMsg *msg, CNIPdu *pReq, CNIPdu *& pRsp);
	
	/* 注册命令函数 */
	bool RegCmd(SigProc pProc, CObject *pObj, int iCmd);

	/* 注销命令函数 */
	bool UnRegCmd(int iCmd);

	/* 检查Processor处理的前提条件,返回非0, Processor才处理相应的信令,Processor要用到返回的信息.
	 */
	virtual int CheckProcessor(void *para, CNIPdu* pReq, void *retInfo) = 0;
	
private:
	/** 查找命令处理 */
    int Process(int iCmd, CNIPdu* pReq, CNIPdu *& pResp);
    
private:
	/// 管理控制函数的列表
	typedef struct
	{
		SigProc proc;
		CObject *obj;
	}NICmdProc;	
	typedef std::map<int, NICmdProc> NICmdTable;
	NICmdTable	m_NICmdTable;
};

#endif

