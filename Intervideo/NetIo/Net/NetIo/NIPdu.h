#ifndef NIPDU_H__
#define NIPDU_H__

#include <list>
#include <System/Packet.h>

class CNIPdu
{
public:
	typedef struct 
	{
	    char *info;
		int len;
	}NIPduInfo;
	
public:
	CNIPdu(void);
	virtual ~CNIPdu(void);
    
	/* 分析缓冲由应用层传入，拷贝保存*/
	virtual bool parseBody(char* pBuff, int iLen, int &outLen);

	/* 打包缓冲pdu传入包体数据 */
	virtual bool packetBody(char* &pBuff, int &iLen);

    /* 获取pdu的一些信息 */
    bool GetPduInfo(char *& info, int &len);
	bool SetPduInfo(char *info, int len);

	void *GetPdu();
	void SetPdu(void *hdr);
	
private:
	NIPduInfo m_Info;
	void * m_Hdr;
};

#endif

