
#ifndef _CCAPTURE_H_
#define _CCAPTURE_H_

#include "Media/ICapture.h"
#include "MultiTask/Mutex.h"
#include "Configs/ConfigDigiChManager.h"

#include <map>

/// 负责捕获对象的动态管理，组合了CDevCapture\CNetCapture的对象
class CCapture : public ICapture
{
	//typedef std::map<ICapture*, int> MAP_ICAPTUER;	///< 捕获对象
	
public: 
	/// 构造函数
	CCapture();

	/// 析构函数
	~CCapture();
	
};

/// 管理捕获对象动态更改
class CCaptureObjectManager : public CObject
{
	class CaptureDouble
	{
	public:
		CaptureDouble(LOGIC_CHN channel)
		{
			pDev = NULL;
			pNet = NULL;
			m_local_channel = channel;
			m_analog_chn = -1;
			m_digital_chn = -1;
		}

		ICapture* GetCapture( ENUM_CHANNEL_TYPE type );
		//!返回0-成功
		int Create(ENUM_CHANNEL_TYPE type, int chn);
	protected:
		ICapture* pDev;
		ANALOG_CHN m_analog_chn;

		ICapture* pNet;
		DIGITAL_CHN m_digital_chn;

		LOGIC_CHN m_local_channel;
	};
	typedef std::map<LOGIC_CHN, CaptureDouble*> MAP_ICAPTUER;	///< 捕获对象
	
public:
	PATTERN_SINGLETON_DECLARE(CCaptureObjectManager);

	CCaptureObjectManager();

	//!此函数不支持析构，此处不可实现
	~CCaptureObjectManager();
	
	int Start(void);
	int Stop(void);

	/// 获取捕获对象
	/// \param[in] iChannel 显示通道号
	/// \return 返回捕获对象
	ICapture* getCaptureObject(int iChannel);

private:
	
	void OnConfigNetCamera(CConfigLocalDigiChCfg* pConfigNetCamera, int &ret);

	void  ChangeRecMod(int iChannel, ENUM_CHANNEL_TYPE type);	
private:	
	CMutex			m_CaptureChangedMutex;		///< 修改捕获对象时候的互斥锁
	//int 			m_iDecodeNum;				///< 数字解码通道总数
	MAP_ICAPTUER	m_mapCapture;				///< 捕获对象容器

	//int 			m_iChannelType[N_SYS_CH];	///< 捕获通道类型，是数字通道还是模拟通道

    CConfigLocalDigiChCfg m_CfgNetCamera;

	ICaptureManager* m_pCaptureM;
};

/**
 * \brief : 98 系列产品，16 个模拟通道，4个数字通道
 */
class CVisiondigiProductAnalog16 : public ICaptureManager
{
	friend class ICaptureManager;

private:
	virtual void CreateProduct();
	CVisiondigiProductAnalog16() {}
	~CVisiondigiProductAnalog16() {}
};

/**
 * \brief : 98 系列产品，16 个模拟通道，4个数字通道
 */
class CVisiondigiProduct98Logic20 : public ICaptureManager
{
	friend class ICaptureManager;
private:
	virtual void CreateProduct();
private:
	CVisiondigiProduct98Logic20() {}
	~CVisiondigiProduct98Logic20() {}
};


/**
 * \brief : 98 系列产品，16 个模拟通道，0个数字通道
 */
class CVisiondigiProductLogic16Decode0_Win32 : public ICaptureManager
{
	friend class ICaptureManager;
private:
	virtual void CreateProduct();
private:
	CVisiondigiProductLogic16Decode0_Win32() {}
	~CVisiondigiProductLogic16Decode0_Win32() {}
};


/**
 * \brief : 98 系列产品，16 个模拟通道，4个数字通道
 */
class CVisiondigiProductLogic20_Win32 : public ICaptureManager
{
	friend class ICaptureManager;
private:
	virtual void CreateProduct();
private:
	CVisiondigiProductLogic20_Win32() {}
	~CVisiondigiProductLogic20_Win32() {}
};

/**
 * \brief : 解码器 系列产品，0 个模拟通道，4个数字通道
 */
class CVisiondigiProductDecoder : public ICaptureManager
{
	friend class ICaptureManager;
private:
	virtual void CreateProduct();
private:
	CVisiondigiProductDecoder() {}
	~CVisiondigiProductDecoder() {}
};

#endif//_CCAPTURE_H_

