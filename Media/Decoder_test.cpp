//
//  "$Id$"
//
//  Copyright (c)1992-2007, Shanghai VisionDigi Technology CO.LTD.
//  All Rights Reserved.
//
//	Description:	
//	Revisions:		2010-01-12 eric.yu created.
//
//
//zhaohy debug 20130225

#if !defined(GUI_DEVICE_REFRESH) || defined(WIN32)

#ifdef __cplusplus
extern "C" {
#endif

#include "APIs/Decoder.h"

int DecoderGetDspInfo(int iChannel, DECODER_DSPINFO *pDspInfo){return 0;};
//
int DecoderGetChannels(void){return 0;};
//
int DecoderCreate(int iChannel){return 0;}; 
//
int DecoderDestroy(int iChannel){return 0;};
//
int DecoderSetPolicy(int iChannel, int iPolicy){return 0;};
//
int DecoderGetImageSize(int iChannel){return 0;};
//
int DecoderStart(int iChannel){return 0;};
//
int DecoderStop(int iChannel){return 0;};
//
int DecoderPutBuffer(int iChannel, void* pBuffer, int iLength, int iTimeOut){return 0;};
//
int DecoderMapChannel(int channel, int targetDisplayChannel){return 0;};
//
int DecoderUnmapChannel(int channel){return 0;};
//
int DecoderGetMappedChannel(int channel){return 0;};
//
int DecoderSetAVEnable(int iChannel,int iAudioEnable,int iVideoEnable){return 0;};
//
int DecoderGetAVEnable(int iChannel,int *piAEnable,int *piVEnable){return 0;};


int DecoderFlush(int iChannel) {return 0;};
int DecodePutMultiBuf(int iChannel, DECODER_BUF_GROUP* pDecoderBuf, int iTimeOut) {return 0;};
int DecoderGetBuffer(int channel, DECODER_BLOCK *pBlock) {return 0;};

#ifdef __cplusplus
}
#endif

#endif


