/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              MultiMediaProc.cpp
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2011-1-14   8:40
**Modified:               
**Modify Reason: 
**Description:            测试HIS解码器是否能解码第和第三方码流
*********************************************************************/
#include <APIs/System.h>
#include <Media/MultiMediaProc.h>
#include <Media/IFrameParse.h>
#include "../Net/RtspApp/RtspMedia.h"

PATTERN_SINGLETON_IMPLEMENT(CMultiMedia);

CMultiMedia::CMultiMedia() : CThread("CMultiMediaProc", TP_CAPTURE)
{
    m_MediaType = MULTI_MEDIA_TYPE_VDH264;
    m_Parser = NULL;
	
    strcpy(m_FileName, "test.dav");
    m_Fp = NULL;

	m_MediaInfo.bFistIFrame = false;
	m_MediaInfo.iFPS = 25;
	m_MediaInfo.Resolution = CAPTURE_SIZE_D1;	
	m_MediaInfo.RecTimeOff = 0;        //记录录像文件的偏移时间，即从开始回放到当前，录像一共偏了多少时间，豪秒级。
	m_MediaInfo.BeginTime = 0;
	m_MediaInfo.Speed = 4;             // SPEED_NORMAL
	m_MediaInfo.SleepTime = 0;

	m_Sig = new TMultiMedia(8);
	m_SigAttachNum = 0;
	
	SetMediaPara("audio_video_d1.dav", MULTI_MEDIA_TYPE_VDH264);
	//SetMediaPara("vilar.264", MULTI_MEDIA_TYPE_VIMIC);
	
	Start();
}
	
CMultiMedia::~CMultiMedia()
{
    if(m_Parser)
	{
	    delete m_Parser;
		m_Parser = NULL;
	}

	if(m_Fp)
	{
	    fclose(m_Fp);
		m_Fp = NULL;
	}
	if(m_Sig)
	{
	    delete m_Sig;
	}
}
int CMultiMedia::Attach(CObject * pObj, TMultiMedia_PROC pProc)
{
    trace("CMultiMedia::Attach ............................\n");
	
	if (IsThreadOver())
	{
	    Start();
	}
	
	int ret = m_Sig->Attach(pObj, pProc);
	if(ret >= 0)
	{
	    m_SigAttachNum += 1;
	}
	return ret;
}

int CMultiMedia::Detach(CObject * pObj, TMultiMedia_PROC pProc)
{
    trace("CMultiMedia::Detach ............................\n");
	
	int ret = m_Sig->Detach(pObj, pProc);
	if(ret >= 0 )
	{
	    m_SigAttachNum -= 1;
	}
	return ret;
}

bool CMultiMedia::Start()
{

	if (!IsThreadOver())
	{
		return true;
	}
	if(!CreateThread())
	{
		trace("CMultiMedia Start, CreateThread failed!\n");
		return false;
	}
	
	m_MediaInfo.BeginTime = SystemGetMSCount();
	trace("CMultiMedia Start Successful ...\n");
	return true;
}

bool CMultiMedia::Stop()
{
	if(IsThreadOver())
	{
		return true;
	}
	
	trace("CMultiMedia Stop ...\n");
	return DestroyThread();
}

bool CMultiMedia::SetMediaPara(char* fileName, MULTI_MEDIA_TYPE type)
{
    bool ret = false;
	
    if(!fileName || strlen(fileName) >= sizeof(m_FileName))
    {
        return false;
    }
	strcpy(m_FileName, fileName);

	ret = (type >= MULTI_MEDIA_TYPE_VDH264 && type <= MULTI_MEDIA_TYPE_VIMIC);
    m_MediaType = (ret ? type : MULTI_MEDIA_TYPE_VDH264);

	
	if(m_Parser)
	{
		delete m_Parser; 
		m_Parser = NULL;
	}
	if((m_Parser = IFrameParse::CreatNew(m_MediaType)) == NULL)
	{
		ret = false;
	}
	else
	{
		m_Parser->Init();
	}

	if(m_Fp)
	{
		fclose(m_Fp);
		m_Fp = NULL;
	}
	if((m_Fp = fopen(m_FileName, "r")) == NULL)
	{
	   ret = false;
	}
	
    if(!ret)
    {
	    if(m_Parser)
		{
		    delete m_Parser;
			m_Parser = NULL;
		}

		if(m_Fp)
		{
		    fclose(m_Fp);
			m_Fp = NULL;
		}
    }
	return ret;
}

void CMultiMedia::ThreadProc()
{    
    char* ptr = NULL;
    FRAMEINFO *dPktHeader = NULL;
    bool lastNal = false;
	char *sPtk = new char[MULTI_MEDIA_BUF_LEN];
	char *dPtk = NULL;
	int sPtkLen = 0;
	int dPtkLen = 0;

    SystemSleep(15);
    while(!IsThreadOver())
	{
	    if(!m_Fp || !m_Parser || m_SigAttachNum <= 0)
	    {
	        SystemSleep(100);
			continue;
	    }
		
	    memset(sPtk,0,MULTI_MEDIA_BUF_LEN);
		sPtkLen = fread(sPtk, 1, MULTI_MEDIA_BUF_LEN, m_Fp);		
		lastNal = (MULTI_MEDIA_BUF_LEN != sPtkLen ? true : false);

		trace("reead len = %d\n", sPtkLen);
	    while(sPtkLen > 0 && m_SigAttachNum > 0)
	    {
			m_Parser->AddPacket(sPtk, sPtkLen);
			while(0 == m_Parser->Parse(dPtk, dPtkLen, lastNal))
			{		
				if(dPtk)
				{	
					dPktHeader = (FRAMEINFO *)dPtk;
					int iFrameType = dPktHeader->FrameType;
					ptr = dPtk + sizeof(FRAMEINFO);
					if(PACK_TYPE_FRAME_I == iFrameType)
					{											
						if(ptr[5] > 1 && ptr[5] <= 25)
						{
							m_MediaInfo.iFPS = ptr[5];
							//VD_TRACE("FPS <%d>, m_Speed <%d>, m_SleepTime <%d>\n", m_MediaInfo.iFPS, m_MediaInfo.Speed, m_MediaInfo.SleepTime);
						}
					}	
					CPacket *pPtk = g_PacketManager.GetPacket(MULTI_MEDIA_BUF_LEN);
					if(pPtk)
					{
					    pPtk->PutBuffer(ptr, dPtkLen);
					    (*m_Sig)(8, 0, pPtk);
					    pPtk->Release();
					}
					
					dPtk = NULL;
					if(!m_MediaInfo.bFistIFrame && PACK_TYPE_FRAME_AUDIO != iFrameType)
					{
						m_MediaInfo.RecTimeOff += 1000/m_MediaInfo.iFPS;
						uint curTime = SystemGetMSCount();
						m_MediaInfo.SleepTime = m_MediaInfo.RecTimeOff - (unsigned int)(g_PlaySpeed[m_MediaInfo.Speed]*(curTime-m_MediaInfo.BeginTime)) - 10;
						if(m_MediaInfo.SleepTime >= 0)
						{   
						    trace("m_MediaInfo.SleepTime = %d\n", m_MediaInfo.SleepTime);
							SystemSleep(m_MediaInfo.SleepTime);  
							//SystemSleep(180);  
						}
					}
				}	
			}
			
			memset(sPtk,0,MULTI_MEDIA_BUF_LEN);
			sPtkLen = fread(sPtk, 1, MULTI_MEDIA_BUF_LEN, m_Fp);		
			lastNal = (MULTI_MEDIA_BUF_LEN != sPtkLen ? true : false);

			trace("reead len = %d\n", sPtkLen);
	    }  
		fseek(m_Fp, 0, SEEK_SET);
		//m_bLoop = false;//exit
	}
	assert(0);
}


