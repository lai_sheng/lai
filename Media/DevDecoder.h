
#ifndef _DEV_DECODER_H_
#define _DEV_DECODER_H_

#include "Media/IDevDecoder.h"

/// \defgroup IDevDecoder接口调用方式
/// \code
///    =================================================|
///							   |						|
///					   GetDecoderChannels				|
///														|                         
///			IDevDecoder::instance(iChannel)->Start()    |  
///    =================================================|

#define MAX_DECODER_CHANNEL_NUM	32	///< 最大解码通道数

class CDevDecoder : public IDevDecoder
{
public:
    /// 构造函数
    /// \param[in] 通道号
    CDevDecoder(int iChannel);

    /// 析构函数
    ~CDevDecoder();

    /// 每个解码通道均创建单一的实例
    /// \param[in] iChannel 通道号
    /// \return 返回设备对象
    static CDevDecoder* instance(int iChannel);

    /// 开启数字解码器
    /// \return 成功返回0　失败返回-1
    int Start(void);

    /// 关闭数字解码器
    /// \return 成功返回0　失败返回-1
    int Stop(void);

    /// 获取解码的分辨率，注意：此函数的返回和当前分割有关
    /// \return 分辨率，参照capture_size_t。
    int GetImageSize(void);

    /// 设置解码策略
    /// \param[in] iPolicy 策略
    /// \		   目前一共有5档(-2 -1 0 1 2),值越大表示越流畅但延迟越大
    /// \		   -2表示实时性最好，2表示流畅性最好，0默认
    /// \return　成功返回0　失败返回-1
    int SetPolicy(int iPolicy);

    /// 往DSP送数据	
    /// \param[in] pvData 码流数据
    /// \param[in] iLength 码流长度
    /// \param[in] iTimeOut 超时时间,默认为5ms
    /// \return 成功返回0　失败返回-1
    int PutBuffer(void *pvData, int iLength, int iTimeOut = 5);

    int PutMultiBuf(DECODER_BUF_GROUP* pDecoderBuf, int iTimeOut);

    /// 获取发送的解码器数据长度，获取后长度复位
    /// \param[in] iTimeOut 超时时间,默认为5ms
    /// \return 成功返回长度　失败返回0
    int GetPutLength() ;

    /// 设置数字通道对应的预览通道号
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \param[in] targetDisplayChannel　显示通道号，取值0~15
    /// \retval 0  成功。
    /// \retval !=0  失败。
    int MapChannel(int channel, int targetDisplayChannel);

    /// 取消数字通道和显示通道的对应关系
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \retval 0  成功。
    /// \retval !=0  失败。
    int UnmapChannel(int channel);

    /// 返回数字通道对应的显示通道，如果数字通道未被map，或者其它错误，返回-1
    /// 否则返回显示通道号(>=0)
    /// \param[in] channel 数值通道号，取值0~DecoderGetChannels()
    /// \return 成功返回显示通道号，失败返回-1
    int GetMappedChannel(int channel);

    /// 清空解码缓冲，重新搜寻I帧。上层丢数据时会调用
    /// 
    /// \param [in] channel 通道号。
    /// \retval 0  清空成功
    /// \retval <0  清空失败
    int Flush(int iChannel);

private:
    int     m_iChannel;     ///< 通道号
    int     m_iPutLen;
    static  CDevDecoder* _instance[MAX_DECODER_CHANNEL_NUM]; ///< 数字解码器通道对象
};

#endif//_DEV_DECODER_H_

