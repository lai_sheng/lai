
#include "CCapture.h"
#include <assert.h>
#include "Devices/DevCapture.h"
//#include "Net/NetClient/NetCapture.h"
//#include "Functions/Preview.h"
#include "Functions/Record.h"
#include "Media/ICapture.h"

CCapture::CCapture()
{

}
CCapture::~CCapture()
{

}

ICapture::~ICapture()
{

}

ICapture* ICapture::instance(int iChannel)
{
    return CCaptureObjectManager::instance()->getCaptureObject(iChannel);
}

int ICapture::GetChannels ()
{
    return CDevCapture::instance(0)->GetChannels();
}

void ICapture::Synchronize()
{
    return CDevCapture::instance(0)->Synchronize();
}

/////////////////////////////////////////////////////////////////////
/// CCaptureObjectManager

PATTERN_SINGLETON_IMPLEMENT(CCaptureObjectManager);
//extern int g_nDecoders;

CCaptureObjectManager::CCaptureObjectManager()
:m_CaptureChangedMutex(MUTEX_RECURSIVE)
{
    int i = 0;
    m_mapCapture.clear();

    //m_iDecodeNum = g_nDecoders;

    m_pCaptureM = ICaptureManager::instance();
    Start();

}

CCaptureObjectManager::~CCaptureObjectManager()
{
    assert(0);
}

//!TODO:此处应该把解码通道做到可以任意指定, 暂时只能前n路
int CCaptureObjectManager::Start()
{
    trace(">>>>>>>>>CCaptureObjectManager start\r\n");
    m_CfgNetCamera.update();
    m_CfgNetCamera.attach(this, (TCONFIG_PROC)&CCaptureObjectManager::OnConfigNetCamera);

    for( LOGIC_CHN i = 0; i < m_pCaptureM->GetLogicChnNum(); i++ )
    {        
        LOCAL_CH_CFG& cam_info = m_CfgNetCamera.getConfig(i);
        if( i >= m_mapCapture.size() )
        {
            m_mapCapture[i] = new CaptureDouble(i);

            if( m_pCaptureM->IsSupportAnalog(i) )
            {
                m_mapCapture[i]->Create( ENUM_CHANNEL_TYPE_ANALOG, m_pCaptureM->GetAnalogChnNo(i) );
            }
            else
            {
                if ( cam_info.iDeviceChannelType == ENUM_CHANNEL_TYPE_ANALOG )
                {
                    cam_info.iDeviceChannelType = ENUM_CHANNEL_TYPE_DATA;
                }
            }

            if( m_pCaptureM->IsSupportDigital(i) )
            {
                m_mapCapture[i]->Create( ENUM_CHANNEL_TYPE_DATA, m_pCaptureM->GetDigitalChnNo(i) );
            }
            else
            {
                if ( cam_info.iDeviceChannelType == ENUM_CHANNEL_TYPE_DATA )
                {
                    cam_info.iDeviceChannelType = ENUM_CHANNEL_TYPE_ANALOG;
                }
            }
        }
    }

    //!对配置中的类型与实际能力的不一致，要改回来，否则会出错
    m_CfgNetCamera.commit();

    return 0;
}

int CCaptureObjectManager::Stop()
{
    m_CfgNetCamera.detach(this, (TCONFIG_PROC)&CCaptureObjectManager::OnConfigNetCamera);

    return 0;
}

ICapture* CCaptureObjectManager::getCaptureObject(int iChannel)
{
    MAP_ICAPTUER::iterator pi;

    assert( ( iChannel < m_pCaptureM->GetLogicChnNum() ) && (iChannel >= 0) );
    int chn_type = m_CfgNetCamera.getConfig(iChannel).iDeviceChannelType;

    return m_mapCapture[iChannel]->GetCapture(static_cast<ENUM_CHANNEL_TYPE>(chn_type) );
}

///当数字通道处于切换状态的时候，用开关录像模式来控制录像的开启
void  CCaptureObjectManager::ChangeRecMod(int iChannel, ENUM_CHANNEL_TYPE type)
{
#if 0 // eric.yu 2010-01-12
    CConfigRecordMode CfgRecMod;
    CfgRecMod.update();
    CONFIG_RECORDMODE& newcfg = CfgRecMod.getConfig(iChannel);
    CONFIG_RECORDMODE oldcfg = newcfg;
    if(oldcfg.RecordMode != CLOSED_REC)
    {
        newcfg.RecordMode = CLOSED_REC;
        newcfg.b_log = FALSE;
        CfgRecMod.commit();
    }
#endif //if 0


#if 0 // eric.yu 2010-01-12
    if(oldcfg.RecordMode != CLOSED_REC)
    {
        newcfg.RecordMode = oldcfg.RecordMode;
        CfgRecMod.commit();
    }
#endif //if 0

    return;
}

void CCaptureObjectManager::OnConfigNetCamera(CConfigLocalDigiChCfg * pConfigNetCamera, int & ret)
{
    trace(">>>>>>>>>>>CCaptureObjectManager::OnConfigNetCamera\n");
#if 0
    for (int i = 0; i < g_nCapture + m_iDecodeNum; i++)
    {
        LOCAL_CH_CFG &cfgOld = m_CfgNetCamera.getConfig(i);
        LOCAL_CH_CFG &cfgNew = pConfigNetCamera->getConfig(i);

        if ((cfgOld.iDeviceChannelType == ENUM_CHANNEL_TYPE_ANALOG)
            && (cfgNew.iDeviceChannelType == ENUM_CHANNEL_TYPE_DATA))
        {

            ChangeRecMod(i, ENUM_CHANNEL_TYPE_DATA);
        }
        else if ((cfgOld.iDeviceChannelType == ENUM_CHANNEL_TYPE_DATA)
                && (cfgNew.iDeviceChannelType == ENUM_CHANNEL_TYPE_ANALOG))
        {
            ChangeRecMod(i, ENUM_CHANNEL_TYPE_ANALOG);
        }
    }
#endif
    m_CfgNetCamera.update();
}

int CCaptureObjectManager::CaptureDouble::Create( ENUM_CHANNEL_TYPE type, int chn )
{
    assert( type == ENUM_CHANNEL_TYPE_ANALOG || type == ENUM_CHANNEL_TYPE_DATA);
    if ( type == ENUM_CHANNEL_TYPE_ANALOG )
    {
        assert(m_analog_chn == -1);
        m_analog_chn = static_cast<ANALOG_CHN>(chn);
        pDev = CDevCapture::instance(m_local_channel);
    }
    else
    {
    	assert(0);
    }
    return 0;
}

ICapture* CCaptureObjectManager::CaptureDouble::GetCapture( ENUM_CHANNEL_TYPE type )
{
    assert( type == ENUM_CHANNEL_TYPE_ANALOG || type == ENUM_CHANNEL_TYPE_DATA);

    if ( type == ENUM_CHANNEL_TYPE_ANALOG )
    {
        
        if ( pDev == NULL )
        {
            trace("not support devcapture!\n");
            assert(pNet != NULL);
            return pNet;
        }
        return pDev;
    }

    if ( pNet == NULL )
    {
        trace("not support digital type!\n");
        assert(pDev != NULL);
        return pDev;
    }
    return pNet;
}

ICaptureManager * ICaptureManager::instance()
{
    static ICaptureManager* s_intance = NULL;

    if( s_intance == NULL )
    {
#ifdef WIN32
        s_intance = new CVisiondigiProductLogic16Decode0_Win32();
#else
        s_intance = new CVisiondigiProductAnalog16();
#endif
        s_intance->start();
    }

    return s_intance;
}

ICaptureManager::ICaptureManager()
{
    m_logic_chn_num = -1;
    m_analog_chn_num = -1;
    m_digital_chn_num = -1;
}

std::string& ICaptureManager::GetLogicChnStr()
{
    return m_logic_chn_str;
}

int ICaptureManager::GetLogicChnNum()
{
    return m_logic_chn_num;
}
int ICaptureManager::GetAnalogChnNum()
{
    return m_analog_chn_num;
}
int ICaptureManager::GetDigitalChnNum()
{
    return m_digital_chn_num;
}

ANALOG_CHN ICaptureManager::GetAnalogChnNo( LOGIC_CHN chn )
{
    if( chn >= m_logic_chn_num || chn < 0 )
    {
        return -1;
    }

    ANALOG_CHN a_chn= m_logic_chn_map[chn].m_analog_chn;

    return a_chn;
}

DIGITAL_CHN ICaptureManager::GetDigitalChnNo( LOGIC_CHN chn )
{

    if ( chn >= m_logic_chn_num || chn < 0 )
    {
        return -1;
    }

    DIGITAL_CHN d_chn= m_logic_chn_map[chn].m_digital_chn;

    return d_chn;
}

bool ICaptureManager::IsSupportAnalog( LOGIC_CHN chn )
{
    if( chn >= m_logic_chn_num || chn < 0 )
    {
        return false;
    }

    ANALOG_CHN a_chn= m_logic_chn_map[chn].m_analog_chn;

    if( a_chn == -1 )
    {
        return false;
    }
    return true;
}

bool ICaptureManager::IsSupportDigital( LOGIC_CHN chn )
{
    if( chn >= m_logic_chn_num || chn < 0 )
    {
        return false;
    }

    DIGITAL_CHN d_chn= m_logic_chn_map[chn].m_digital_chn;

    if( d_chn == -1 )
    {
        return false;
    }
    return true;
}

void ICaptureManager::start()
{
    int i;
    char buf_digital[16] = {0};

    CreateProduct();

    trace("logic chn:%d, max:%d\n", m_logic_chn_num, N_SYS_CH);
    assert(N_SYS_CH >= m_logic_chn_num );
    assert( m_logic_chn_num > 0 );

    m_logic_chn_str += " 1";
    for ( i = 1; i < m_logic_chn_num; i++ )
    {
        m_logic_chn_str += "|";
        sprintf(buf_digital, " %d", i+1);
        m_logic_chn_str += buf_digital;
    }

    m_logic_chn_str += "|";
    m_logic_chn_str += LOADSTR("comstring.comall");
}

void CVisiondigiProductAnalog16::CreateProduct()
{
    int i;
    m_analog_chn_num = CaptureGetChannels();
    m_digital_chn_num = 0;

    infof("digital chn num:%d\n",  m_digital_chn_num);
    //!N_SYS_CH-此处如小于，可能会有很多数组访问越界隐患

    m_logic_chn_num = 0;
    for( i = 0; i < m_analog_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = i;
        chn_pair.m_digital_chn = -1;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }

}

void CVisiondigiProduct98Logic20::CreateProduct()
{
    int i;
    m_analog_chn_num = CaptureGetChannels();
    m_digital_chn_num = 0;

//    assert(m_digital_chn_num <=16);
    infof("==>analog chn:%d, digital chn num:%d\n",  m_analog_chn_num, m_digital_chn_num);
    //!N_SYS_CH-此处如小于，可能会有很多数组访问越界隐患

    m_logic_chn_num = 0;
    for( i = 0; i < m_analog_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = i;
        chn_pair.m_digital_chn = -1;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }

    for( i = 0; i < m_digital_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = -1;
        chn_pair.m_digital_chn = i;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }
}

void CVisiondigiProductLogic20_Win32::CreateProduct()
{
    int i;
    m_analog_chn_num = 16;
    m_digital_chn_num = 4;

    infof("digital chn num:%d\n",  m_digital_chn_num);
    //!N_SYS_CH-此处如小于，可能会有很多数组访问越界隐患

    m_logic_chn_num = 0;
    for( i = 0; i < m_analog_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = i;
        chn_pair.m_digital_chn = -1;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }

    for( i = 0; i < m_digital_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = -1;
        chn_pair.m_digital_chn = i;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }
}

void CVisiondigiProductLogic16Decode0_Win32::CreateProduct()
{
    int i;
    m_analog_chn_num = 16;
    m_digital_chn_num = 0;

    infof("digital chn num:%d\n",  m_digital_chn_num);
    //!N_SYS_CH-此处如小于，可能会有很多数组访问越界隐患

    m_logic_chn_num = 0;
    for( i = 0; i < m_analog_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = i;
        chn_pair.m_digital_chn = -1;

        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }
}

void CVisiondigiProductDecoder::CreateProduct()
{
    int i;
    m_analog_chn_num = 0;
    m_digital_chn_num = 0;
    infof("digital chn num:%d\n",  m_digital_chn_num);

    /* 如果ID不对,不让死机,默认按照一路解码器实现 */
    if (0 >= m_digital_chn_num ||N_SYS_CH <= m_digital_chn_num)
    {
        printf("\r\n####################CVisiondigiProductDecoder::CreateProduct():error!!!!! m_digital_chn_num = %d\r\n\r\n",m_digital_chn_num);
        m_digital_chn_num = 1;
    }
    
    m_logic_chn_num = 0;
    for( i = 0; i < m_digital_chn_num; i++ )
    {
        LOGIC_CHN_PAIR_T chn_pair;
        chn_pair.m_analog_chn = -1;
        chn_pair.m_digital_chn = i;
        m_logic_chn_map[m_logic_chn_num] = chn_pair;
        m_logic_chn_num++;
    }
}

