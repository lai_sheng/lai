
#include "DevDecoder.h"
#include <assert.h>
#include <stdio.h>
#include "Configs/ConfigDigiChManager.h"

CDevDecoder* CDevDecoder::_instance[MAX_DECODER_CHANNEL_NUM] = {NULL, };

int GetDecoderChannels()
{
	//return 0;
	return DecoderGetChannels();
}

CDevDecoder::CDevDecoder(int iChannel):m_iChannel(iChannel),m_iPutLen(0)
{    
	trace(">>>>>>>>CDevDecoder, m_iChannel:%d!\n", m_iChannel);
	assert((m_iChannel >=0) && (m_iChannel < MAX_DECODER_CHANNEL_NUM));
	DecoderCreate(m_iChannel);

    CConfigLocalDigiChCfg *pCfg = new CConfigLocalDigiChCfg();
    assert(pCfg);

    pCfg->update();
    
    LOCAL_CH_CFG &cfgInfo = pCfg->getConfig(iChannel);

    (VD_VOID)DecoderSetAVEnable(iChannel,(0 == cfgInfo.iAVEnable)?0:1,(1 == cfgInfo.iAVEnable)?0:1);

    (VD_VOID)DecoderSetPolicy(iChannel,cfgInfo.iDecodePolicy);
    delete pCfg;
}

CDevDecoder::~CDevDecoder()
{
	printf("~CDevDecoder>>>>>>>>!\n");
	assert((m_iChannel >=0) && (m_iChannel < MAX_DECODER_CHANNEL_NUM));
	//return 0;
	DecoderUnmapChannel(m_iChannel);
	DecoderDestroy(m_iChannel);
}

CDevDecoder* CDevDecoder::instance(int iChannel)
{
	if (NULL == _instance[iChannel])
	{
		_instance[iChannel] = new CDevDecoder(iChannel);
	}	

	return _instance[iChannel];
}

int CDevDecoder::GetImageSize(void)
{
		//return 0;
	return DecoderGetImageSize(m_iChannel);
}

int CDevDecoder::SetPolicy(int iPolicy)
{
	return DecoderSetPolicy(m_iChannel, iPolicy);		
}

int CDevDecoder::Start()
{
	printf("################### DecoderStart iChannel = %d \n\n", m_iChannel);
	m_iPutLen = 0;

	//MapChannel(m_iChannel, m_iChannel);

	return DecoderStart(m_iChannel);
		
	//printf("---------------------- DecoderStart Down! iChannel = %d \n\n", m_iChannel);
	
	//return 0;
}

int CDevDecoder::Stop()
{
	//UnmapChannel(m_iChannel);
	return DecoderStop(m_iChannel);
}

int CDevDecoder::PutBuffer(void * pvData, int iLength, int iTimeOut/* = 5*/)
{
	int ret = 0;
	if (NULL == pvData)
	{
		//printf("\n\n#####################################\n\n");
		return -1;
	}
	m_iPutLen += iLength;
	ret = DecoderPutBuffer(m_iChannel, pvData, iLength, iTimeOut);
	return ret;

}

int CDevDecoder::PutMultiBuf(DECODER_BUF_GROUP* pDecoderBuf, int iTimeOut)
{
	int i;
	if( pDecoderBuf->count == 0 )
	{
		return -1;
	}

	for( i = 0; i < pDecoderBuf->count; i++ )
	{
		m_iPutLen += pDecoderBuf->u32Len[i];
	}
//#ifdef DECODER_EX //!扩展解码器接口
	return DecodePutMultiBuf(m_iChannel, pDecoderBuf, iTimeOut);
//#else
//	return 0;
//#endif

}

int CDevDecoder::GetPutLength()
{
	int iLen = m_iPutLen;
	m_iPutLen = 0;
	return iLen;
}

int CDevDecoder::MapChannel(int channel, int targetDisplayChannel)
{
	/*if ((channel < 0) 
		|| (channel >= GetDecoderChannels())
		|| (targetDisplayChannel < 0)
		|| (targetDisplayChannel >= 16))
	{
		printf("MapChannel Input param error channel = %d targetChannel = %d \n\n", 
			channel, targetDisplayChannel);
		return -1;
	}*/
	return DecoderMapChannel(channel, targetDisplayChannel);
}

int CDevDecoder::UnmapChannel(int channel)
{
	/*if ((channel < 0) 
		|| (channel >= GetDecoderChannels()))
	{
		printf("UnmapChannel Input param error channel = %d\n", channel); 
		return -1;
	}*/
	return DecoderUnmapChannel(channel);
}

int CDevDecoder::GetMappedChannel(int channel)
{
	/*if ((channel < 0) 
		|| (channel >= GetDecoderChannels()))
	{
		printf("GetMappedChannel Input param error channel = %d\n", channel); 
		return -1;
	}*/
	return DecoderGetMappedChannel(channel);	
}

/// 清空解码缓冲，重新搜寻I帧
/// 
/// \param [in] channel 通道号。
/// \retval 0  清空成功
/// \retval <0  清空失败
int CDevDecoder::Flush(int iChannel)
{
	return DecoderFlush(m_iChannel);
}

IDevDecoder *IDevDecoder::instance(int iChannel)
{	
	return CDevDecoder::instance(iChannel);
}

IDevDecoder::~IDevDecoder()
{
}

