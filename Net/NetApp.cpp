/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * NetConfig.cpp - _explain_
 *
 * Copyright (C) 2005 Technologies, All Rights Reserved.
 *
 *
 * _detail_explain_.
 * 
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#include "APIs/Net.h"
#include "Net/NetWorkService.h"
#include "System/Log.h"
#include "Net/NetApp.h"
#include "Configs/ConfigNet.h"
#include "Net/NetConfig.h"
//#include "Net/Dlg/DlgDDNSCli.h"
#include "System/Console.h"
#include "Net/INet/INet.h"
#include "RtspApp/PortPool.h"
#include "Main.h"
//Le0
#ifdef ONVIF
#include "Onvif242/OnvifApp.h"
#endif

//add langzi 2010-3-24
#ifndef WIN32
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <string.h>
#include <stdio.h>
#endif
//end langzi

#define MAXINTERFACES 16   

#if defined(FUNCTION_NET_ALARM_CENTER)
//#include "Net/Dlg/Cmds_AlarmToCenter.h"
#endif

#ifdef _USE_SMTP_MODULE
#include "Net/Dlg/DlgSmtpCli.h"
#endif

#if defined(FUNC_FTP_UPLOAD)
//#include "Net/Dlg/FtpClient.h"
#endif

#ifdef _NET_USE_DHCPCLIENT/*是否使用dhcp*/
#include "Net/Dlg/DlgDhcpCli.h"
#endif

#if defined(_FUNC_UPNP_MOUDLE)
11111
#include "Net/Dlg/upnp/UpnpClient.h"
#endif

#if defined(USING_PROTOCOL_NTP)
#include "Net/Dlg/DlgNtpCli.h"
#endif

#if defined(FUNCTION_SUPPORT_RTSP)
#include "./RtspApp/RtspThread.h"
#endif

#if defined (ONVIF_SERVER)
#include "OnvifApp.h"
#endif

//route信息,网关信息
 typedef struct _route_str_metric{	
 	char 	name[20];	
	unsigned int	des;	
	unsigned int 	gateway;	
	unsigned int 	flag;
	unsigned int 	val[7];
} ROUTE_STR_METRIC;

std::string g_strDefaultHostIp;
std::string g_strDefaultNetMask; 
std::string g_strDefaultGateway;
int g_iFirstUseDefaultIp;
	

PATTERN_SINGLETON_IMPLEMENT(CNetApp);

CNetApp::CNetApp():m_TimerCheckNet("CheckNet")
{
//modified by ilena zhou 2010-06-13, 使用reset设置默认ip时，会判断错误导致系统不作修改，因此这里初始值改为0
	m_IPInfo.HostIP.l = 0;//Str2Ip(g_strDefaultHostIp.c_str());
	m_IPInfo.SubMask.l = 0;//Str2Ip(g_strDefaultNetMask.c_str());
	m_IPInfo.GateWay.l = 0;//Str2Ip(g_strDefaultGateway.c_str());
	m_IPInfo.iDHCP = FALSE;
	trace("CNetApp::CNetApp()>>>>>>>>>\n");
}
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

CNetApp::~CNetApp()
{
}

//设置网络IP信息
int CNetApp::SetNetIPInfo(const char *pEthName, const IPInfo *pInfo, VD_BOOL update)
{
	char strHostIp[IP4_STR_LEN+1]={0};
	char strSubmask[IP4_STR_LEN+1]={0};
	char strGateway[IP4_STR_LEN+1]={0};

	int iRet = SUCCESS_RET;

	if(NULL == pInfo)
	{_printd();
		return FAILURE_RET;
	}
	if (pInfo->GateWay.l == m_IPInfo.GateWay.l &&
		pInfo->HostIP.l == m_IPInfo.HostIP.l &&
		pInfo->SubMask.l == m_IPInfo.SubMask.l)
	{_printd();
		return SUCCESS_RET;
	}


	// 如果主机地址与网关地址不匹配时, 不更改设置
	if ((pInfo->HostIP.l & pInfo->SubMask.l)
		!= (pInfo->GateWay.l & pInfo->SubMask.l)) 
	{_printd();
		return FAILURE_RET;
	}

    //限制不能设置尾数为255的IP
    	//if(pInfo->HostIP.c[3] == 255) //zsliu change
	if(pInfo->HostIP.c[3] == 255 || pInfo->HostIP.c[3] == 0)
	{_printd();
		return FAILURE_RET;
	}   
   
	Ip2Str(pInfo->HostIP.l, strHostIp);
	Ip2Str(pInfo->SubMask.l, strSubmask);
	Ip2Str(pInfo->GateWay.l, strGateway);
	iRet |= NetSetHostIP(pEthName, strHostIp, strSubmask);
	iRet |= NetSetGateway(pEthName, strGateway);
	
	if(SUCCESS_RET == iRet)
	{_printd();
		m_IPInfo.HostIP.l = pInfo->HostIP.l;
		m_IPInfo.SubMask.l = pInfo->SubMask.l;
		m_IPInfo.GateWay.l = pInfo->GateWay.l;
		m_IPInfo.iDHCP = pInfo->iDHCP;
	}
	else
	{_printd();
		Ip2Str(m_IPInfo.HostIP.l, strHostIp);
		Ip2Str(m_IPInfo.SubMask.l, strSubmask);
		Ip2Str(m_IPInfo.GateWay.l , strGateway);
		iRet |= NetSetHostIP(pEthName, strHostIp, strSubmask);
		iRet |= NetSetGateway(pEthName, strGateway);
	}
	return iRet;
}

//获取网络IP信息
int CNetApp::GetNetIPInfo(const char *pEthName, IPInfo *pInfo, VD_BOOL update)
{
	char strHostIp[IP4_STR_LEN+1]={0};
	char strSubmask[IP4_STR_LEN+1]={0};
	char strGateway[IP4_STR_LEN+1]={0};
	int iRet = SUCCESS_RET;
	int iEth = -1;
	char EthName[8]  = {0};

	int fd;         
	int if_len;     
	struct ifreq buf[MAXINTERFACES];    
	struct ifconf ifc;
	
	ROUTE_STR_METRIC metric;
	char gw_addr[16] = {0};
	char  buf_gw[255] = {0};
	FILE *fp       = NULL;
	
	if(NULL == pInfo)
	{
		return FAILURE_RET;
	}
	//_printd("******************Search 2**********************pEthName==%s\n", pEthName);
	// iEth = get_netlink_status("eth0");
	//if (1 == iEth)
	//	strcpy(EthName , "eth0");
	//if (0 == iEth)
	//strcpy(EthName , "wlan0");
	
	if(NULL != pEthName)
	{
		strcpy(EthName , pEthName);
		//_printd("********************Search 2.1******************************************\n");
		if(0 == strcmp(EthName, pEthName))	//是否从环境变量中提取IP
		{	
		//	printf("\e[42mEthName=%s222222222222\e[0m\n",pEthName);
			if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
			{
				perror("socket(AF_INET, SOCK_DGRAM, 0)");
				return -1;
			}
		 
			
			ifc.ifc_len = sizeof(buf);
			ifc.ifc_buf = (caddr_t) buf;
		 
			
			if (ioctl(fd, SIOCGIFCONF, (char *) &ifc) == -1)
			{
				perror("SIOCGIFCONF ioctl");
				close(fd);
				return -1;
			}
		 
			if_len = ifc.ifc_len / sizeof(struct ifreq); 
			//printf("接口数量:%d\n\n", if_len);
		 
		
			//printf("接口：%s\n", buf[if_len].ifr_name); 
			while(0 != strcmp(EthName, buf[if_len].ifr_name))
			{
				if (-1 == if_len)
				{
					break;
				}
				 
				if_len--;
			}
		 
			if(0 <= if_len)
			{
				if (!(ioctl(fd, SIOCGIFFLAGS, (char *) &buf[if_len])))
				{
					
					if (buf[if_len].ifr_flags & IFF_UP)
					{
						//printf("接口状态: UP\n");
					}
					else
					{
						//printf("接口状态: DOWN\n");
					}
				}
				else
				{
					char str[256];
					sprintf(str, "SIOCGIFFLAGS ioctl %s", buf[if_len].ifr_name);
					perror(str);
				}
		 
		 
				
				if (!(ioctl(fd, SIOCGIFADDR, (char *) &buf[if_len])))
				{
					/*printf("IP地址:%s\n",
							(char*)inet_ntoa(((struct sockaddr_in*) (&buf[if_len].ifr_addr))->sin_addr));*/
					 inet_pton(AF_INET, (char*)inet_ntoa(((struct sockaddr_in*) (&buf[if_len].ifr_addr))->sin_addr), &pInfo->HostIP.l);
				
				}
				else
				{
					char str[256];
					sprintf(str, "SIOCGIFADDR ioctl %s", buf[if_len].ifr_name);
					perror(str);
				}
		 
				
				if (!(ioctl(fd, SIOCGIFNETMASK, (char *) &buf[if_len])))
				{
					/*printf("子网掩码:%s\n",
							(char*)inet_ntoa(((struct sockaddr_in*) (&buf[if_len].ifr_addr))->sin_addr));*/
					 inet_pton(AF_INET, (char*)inet_ntoa(((struct sockaddr_in*) (&buf[if_len].ifr_addr))->sin_addr), &pInfo->SubMask.l );
				}
				else
				{
					char str[256];
					sprintf(str, "SIOCGIFADDR ioctl %s", buf[if_len].ifr_name);
					perror(str);
				}
		 
				
				if (!(ioctl(fd, SIOCGIFBRDADDR , (char *) &buf[if_len])))
				{
					/*printf("广播地址:%s\n",
							(char*)inet_ntoa(((struct sockaddr_in*) (&buf[if_len].ifr_addr))->sin_addr));*/
				}
				else
				{
					char str[256];
					sprintf(str, "SIOCGIFADDR ioctl %s", buf[if_len].ifr_name);
					perror(str);
				}
		 
				
				if (!(ioctl(fd, SIOCGIFHWADDR, (char *) &buf[if_len])))
				{
					/*printf("MAC地址:%x:%x:%x:%x:%x:%x\n\n",
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[0],
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[1],
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[2],
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[3],
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[4],
							(unsigned char) buf[if_len].ifr_hwaddr.sa_data[5]);*/
				}
				else
				{
					char str[256];
					sprintf(str, "SIOCGIFHWADDR ioctl %s", buf[if_len].ifr_name);
					perror(str);
				}

				/*if(0 == strcmp("eth0", buf[if_len].ifr_name))
				{
					break;
				}*/
				
			}//–while end
		 
			//关闭socket
			close(fd);
			
			if(NULL == (fp=fopen("/proc/net/route", "r"))) 
				return 0;
			while(!feof(fp)){
				fgets((char *)buf_gw, 254, fp);
				sscanf(buf_gw, "%s %x %x %x %d %d %d %d %d %d %d" , metric.name, 
					&metric.des, &metric.gateway, &metric.flag, 
					&metric.val[0], &metric.val[1], &metric.val[2], &metric.val[3], &metric.val[4], 
					&metric.val[5], &metric.val[6]);
				
				if (metric.flag == 3){
					snprintf(gw_addr, 19,  "%d.%d.%d.%d", 
						metric.gateway & 0xff, metric.gateway>>8 & 0xff,
						metric.gateway>>16 & 0xff, metric.gateway>>24 & 0xff);	
					break;
				}
			}
			//_printd("*****默认网关:%s\n", gw_addr);
			inet_pton(AF_INET, (char *)gw_addr, &pInfo->GateWay.l);
			fclose(fp);
			pInfo->iDHCP 		=	m_IPInfo.iDHCP;			
			return SUCCESS_RET;	
		}
	}
	//_printd("********************搜索2.3******************************************\n");
	if(update)
	{
		iRet |= NetGetHostIP(pEthName, strHostIp,sizeof(strHostIp), strSubmask,sizeof(strSubmask));
		iRet |= NetGetGateway(pEthName, strGateway,sizeof(strGateway));

//#ifdef WIN32
//		iRet = FAILURE_RET;
//#endif
		if(SUCCESS_RET == iRet)
		{	
			m_IPInfo.HostIP.l = Str2Ip(strHostIp);
			m_IPInfo.SubMask.l = Str2Ip(strSubmask);
			m_IPInfo.GateWay.l = Str2Ip(strGateway);
		}
	}
	pInfo->HostIP.l 	=	m_IPInfo.HostIP.l ;
	pInfo->SubMask.l 	=	m_IPInfo.SubMask.l ;
	pInfo->GateWay.l 	=	m_IPInfo.GateWay.l ;
	pInfo->iDHCP 		=	m_IPInfo.iDHCP;
	return iRet;
}

//!网络功能启动
VD_BOOL CNetApp::Start()
{
	int iRet = 0;
	iRet = GetNetIPInfo(NULL, &m_IPInfo,TRUE);
	if(SUCCESS_RET == iRet)
	{
		trace("NetApp Start GetIP 0x%x\n", m_IPInfo.HostIP.l);
	}
	m_ConfigNetCommon.update();
	m_ConfigNetCommon.attach(this,(TCONFIG_PROC)&CNetApp::OnNetCommApp);
	m_ConfigNetIPFilter.update();
	m_ConfigNetIPFilter.attach(this,(TCONFIG_PROC)&CNetApp::OnNetIPFilter);

#if 0//defined(USING_PROTOCOL_NTP) && !defined(_WIN32)
    g_NtpClient.Start();
#endif

#ifdef _USE_SMTP_MODULE
    g_SmtpClient.Start();
#endif

#ifdef _NET_USE_DHCPCLIENT/*是否使用dhcp*/
    g_DhcpClient.Start();
#endif

#if defined(FUNCTION_NET_ALARM_CENTER)
    //g_AlarmToCenter.Start();
#endif


#ifdef ONVIF
	//Le0
	_printd("Le0++++++++++++++Onvif Start+++++++++++++++++++++\n");
	g_Onvif.Start();
#endif

#ifdef ONVIF_SERVER
	g_SMPortPool.InitPortPool(20000, 1000);
	g_Onvif.Start();
#endif

#if FUNCTION_SUPPORT_RTSP
#ifdef BASE2HIGH
    CConfigNetRtspSet rtspConfig;
    rtspConfig.update();
    RTSP_SET &RtspSet = rtspConfig.getConfig();
    SetRtspPort((RTSP_CONFIG*)&RtspSet);
    SartVencRtsp();
#else
	//启动端口号为554的RTSP服务 Le0
  //  g_RtspSvr.Start();
#endif
#endif
	g_Console.registerCmd (CConsole::Proc( &CNetApp::onNet, this ), "net", "Net set!");//注册控制台关于网路的操作函数

//网络状态检测
#ifdef CHECK_NET_STAT
    m_TimerCheckNet.Start(this, (VD_TIMERPROC)&CNetApp::checkNetStat, 0, 10000);
#endif
 
	return true;
}
//!网络功能停止
VD_BOOL CNetApp::Stop()
{
	m_ConfigNetCommon.detach(this,(TCONFIG_PROC)&CNetApp::OnNetCommApp);
    m_TimerCheckNet.Stop();
	return true;
}
//!网络配置更改生效回调函数
void CNetApp::OnNetCommApp(CConfigNetCommon* pConfigNetCommon, int &ret)
{

	CONFIG_NET_COMMON &oldCfgComm = m_ConfigNetCommon.getConfig();
	CONFIG_NET_COMMON &newCfgComm = pConfigNetCommon->getConfig();

	//端口修改需要重启系统
	if ( newCfgComm.HttpPort != oldCfgComm.HttpPort
		|| newCfgComm.TCPPort != oldCfgComm.TCPPort )
	{
		ret |= CONFIG_APPLY_REBOOT;
		return;
	}
}

//IP过滤配置修改回调,
void CNetApp::OnNetIPFilter(CConfigNetIPFilter* pConfigNetIPFilter, int &ret)
{
	CONFIG_NET_IPFILTER &oldCfgIPFilter = m_ConfigNetIPFilter.getConfig();
	 oldCfgIPFilter =pConfigNetIPFilter->getConfig();
	 ret |=CONFIG_APPLY_OK;
}

#ifdef _NET_USE_DHCPCLIENT
//!DHCP实际状态回调, 当状态改变时处理
//void CNetApp::OnSetDHCP(VD_BOOL iUseDHCP, const IPInfo *pIpInfo, const char *pEthName) //delect langzi 2010-3-24
void CNetApp::OnSetDHCP(VD_BOOL iUseDHCP, const IPInfo *pIpInfo, const char *pEthName, const unsigned long *pDNS) //add langzi 2010-3-24
{
	if(iUseDHCP)                   //获取dhcp,设置IP
	{
	
		SetNetIPInfo(pEthName, pIpInfo, FALSE);
		//add langzi 修改DNS 且生效
		#if 0
		{
			char oldPrimaryDNS[24] ={0};
            		char oldSecondDNS[24] ={0};
			char strNewDNS[24]={0};
            		int bret = 0;
			
            		bret = NetGetDNSAddress(oldPrimaryDNS, sizeof(oldPrimaryDNS), oldSecondDNS, sizeof(oldSecondDNS));
            		if ( 0 == bret )
            		{
						Ip2Str(*pDNS, strNewDNS);
                		if (strcmp(strNewDNS, oldPrimaryDNS) && strcmp(strNewDNS, oldSecondDNS)) 
                		{
                    			NetSetDNSAddress(strNewDNS, oldPrimaryDNS);
		    			res_init();  //add langzi 2010-3-24 初始化 DNS 2010-3-24
					printf("\n==line[%d]=file[%s]===get dhcp true!==DNS[%s]=\n", __LINE__, __FILE__, strNewDNS);
                		}
            		}
            		else
            		{
                		NetSetDNSAddress(strNewDNS, "0.0.0.0");
				res_init();  //add langzi 2010-3-24 初始化 DNS 2010-3-24
            		}
		}
		//end langzi
		#endif
	}
	else                   //没获取到或关闭dhcp,重新设置设定默认IP
	{
		m_IPInfo.iDHCP = iUseDHCP;
	}

}
#endif

bool CNetApp::IsTrustIP(char *ip)
{
	//added by wyf on 091221
	// 检查IP地址是否有效，具体规则如下
	// 如果白名单为空，那么黑名单内的将不能访问
	// 否则只允许白名单内的ip地址登陆		
	IPADDR *pTempIP;
	int trustIpNum = 0;
	int i = 0;
	CONFIG_NET_IPFILTER &CfgIPFilter = m_ConfigNetIPFilter.getConfig();

	//统计白名单个数
	for (i = 0; i < MAX_FILTERIP_NUM; ++i)
	{
		pTempIP = (IPADDR *)&CfgIPFilter.TrustList[i];
		if(0 != pTempIP->l)
		{
			trustIpNum++;
		}
	}

	unsigned long l = Str2Ip(ip);
	if(CfgIPFilter.Enable)
	{
		if(0 != trustIpNum)
		{
			for(i = 0; i < MAX_FILTERIP_NUM; i++)
			{
				if (CfgIPFilter.TrustList[i].l == l) 
				{
				return true;
				}
			}
			return false;
		}
		else
		{
			for (i = 0; i < MAX_FILTERIP_NUM; ++i)
			{
				if (CfgIPFilter.BannedList[i].l == l) 
				{
					return false;
				}
			}
			return true;
		}	
		return false;
	}
	//不使能，全部通过
	return true;
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
int CNetApp::onNet(int argc, char **argv)
{
	char *pszCmd0;
	char * pArg1, *pArg2,*pArg3;
   
	 CConsoleArg arg(argc, argv);
     pszCmd0 = arg.getOptions();//获得命令选项
	 if(pszCmd0==NULL)//解决用户只输入net命令时，程序崩溃的问题
	 {
		trace("net command:\n");
		trace("net  -a <ip>[submsk][gateway]: set ip\n");
		trace("net  -b : run net module\n");
		trace("net  -c : stop net module\n");
		trace("net  -d : dump net config\n");
		trace("net  -e : change port\n");

		#ifdef _DBG_ATMTCPRECEIVER
			trace("net  -i : start atmtcp receiver.\n");
			trace("net  -j : stop atmtcp receiver.\n");
		#endif
		return 0;
	 }

	pArg1  = arg.GetArg(0);
    pArg2 = arg.GetArg(1);
	pArg3 = arg.GetArg(2);
    
	switch(pszCmd0[0])
	{
		case 'a':  // set ip
		{
			char strIp[32];
			IPInfo info;
			g_NetApp.GetNetIPInfo(NULL, &info);
			if (NULL == pArg1)
			{
				break;
			}
			Ip2Str(info.HostIP.l, strIp);

			info.HostIP.l = Str2Ip(pArg1);
			trace("---> NewHostIP:%s\n", pArg1);
			
			if (NULL != pArg2)
			{
				info.SubMask.l = Str2Ip(pArg2);
				trace("---> NewSubmask:%s\n", pArg2);
			}
			if (NULL != pArg3)
			{
				info.GateWay.l= Str2Ip(pArg3);
				trace("---> NewGateway:%s\n", pArg3);
			}
			
			// 如果主机地址与网关地址不匹配时, 不更改设置
			int iRet = g_NetApp.SetNetIPInfo(NULL, &info);

			if (iRet !=SUCCESS_RET)
			{
				trace("save&set IP error \n");
			}
			else
			{
				trace("set OK \n");
			}
			break;
		}
		case 'b':
		{
			g_Net.Run(0, NULL); //start net module
			trace("g_Net.Run\n");

			break;
		}
		case 'c':
		{
			g_Net.Stop(0, NULL); // stop
			trace("g_Net.Stop\n");
			break;
		}
		case 'd':
		{
			g_NetFactory.CreateNet()->Dump();

			break;
		}
		case 'e':
		{
			int iPort = 0;
			CConfigNetCommon cfgNetComm;
			cfgNetComm.update();
			if (NULL == pArg1)
			{
				break;
			}

			iPort = atoi(pArg1);
			if (iPort <= 1024)
			{
				trace("Error input:%d, should > 1024\n", iPort);
			}
			else
			{
				trace("Chg tcp listen port %d->%d\n", cfgNetComm.getConfig().TCPPort, iPort);
				cfgNetComm.getConfig().TCPPort = iPort;
				cfgNetComm.commit();
			}

			break;
		}
		case 'f':
		{
			break;
		}
        case 's':
		{
			trace("\tNo Vsp Config,Operate No Meaning!\n");
			break;
		}
		case 'g':
		{
			break;
		}
#ifdef _DBG_ATMTCPRECEIVER
		case 'i':
		{
			trace("g_DevATMTCPReciver.Start()\n");
			g_DevATMTCPReciver.Start();
			break;
		}
		case 'j':
		{
			trace("g_DevATMTCPReciver.Stop()\n");
			g_DevATMTCPReciver.Stop();
			break;
		}
#endif

		default:
		{
			trace("net command:\n");
			trace("net  -a <ip>[submsk][gateway]: set ip\n");
			trace("net  -b : run net module\n");
			trace("net  -c : stop net module\n");
			trace("net  -d : dump net config\n");
			trace("net  -e : change port\n");

#ifdef _DBG_ATMTCPRECEIVER
			trace("net  -i : start atmtcp receiver.\n");
			trace("net  -j : stop atmtcp receiver.\n");
#endif
			break;
		}
	}
    return 0;

}


#ifndef WIN32
/* Function: parseTcp
 * Description: Parse a tcp line information.
 * Input:   line - line information
 * Output:  stat - tcp state statistics
 * Return:  0 if success, -1 if fail
 */
int CNetApp::parseTcp(char *line, TCP_STATE_STAT *stat)
{
    unsigned long rxq, txq, time_len, retr, inode;
    int num, local_port, rem_port, d, state, uid, timer_run, timeout;
    char rem_addr[128], local_addr[128], more[512];
    num = sscanf(line,
    "%d: %64[0-9A-Fa-f]:%X %64[0-9A-Fa-f]:%X %X %lX:%lX %X:%lX %lX %d %d %ld %512s\n",
    &d, local_addr, &local_port, rem_addr, &rem_port, &state,
    &txq, &rxq, &timer_run, &time_len, &retr, &uid, &timeout, &inode, more);
    if (num < 11) 
    {
        warnf("warning: got bogus tcp line.\n");
        return -1;
    }

    switch(state)
    {
        case TCP_ESTABLISHED: 
            stat->establisheds++;
            break;
        case TCP_SYN_SENT: 
            stat->syn_sents++;
            break;
        case TCP_SYN_RECV: 
            stat->syn_recvs++;
            break;
        case TCP_FIN_WAIT1: 
            stat->fin_wait1s++;
            break;
        case TCP_FIN_WAIT2: 
            stat->fin_wait2s++;
            break;
        case TCP_TIME_WAIT: 
            stat->time_waits++;
            break;
        case TCP_CLOSE: 
            stat->closes++;
            break;
        case TCP_CLOSE_WAIT: 
            stat->close_waits++;
            break;
        case TCP_LAST_ACK: 
            stat->last_acks++;
            break;
        case TCP_LISTEN: 
            stat->listens++;
            break;
        case TCP_CLOSING: 
            stat->closings++;
            break;
        default:
            warnf("warning: got unknown tcp state: %d\n", state);
            break;
    }
    
    return 0;
}

/* Function: getTcpConnections
 * Description: Get tcp connection count and state statistics in the system.
 * Input:   file - system tcp information path
 * Output:  stat - tcp state statistics
 * Return:  tcp connection count if success, -1 if fail
 */
int CNetApp::getTcpConnections(char *file, TCP_STATE_STAT *stat)
{
    FILE *procinfo;
    int connections = 0;
    char buffer[200];
    memset(stat, 0, sizeof(TCP_STATE_STAT));
    procinfo = fopen(file, "r"); 
    if (procinfo == NULL) 
    { 
        perror(file); 
        return -1; 
    }
    else
    { 
       do { 
            if (fgets(buffer, sizeof(buffer), procinfo))
            {
                if (parseTcp(buffer, stat) == 0)
                {
                    connections++;
                }
            }
        } while (!feof(procinfo)); 

        fclose(procinfo); 
        return connections;
    }
}


/* Function: checkNetStat
 * Description: Check system net stat to see if the net is working normally.
 * We will reboot the system if something becomes abnormal.
 * Input:   none
 * Output:  none
 * Return:  none
 */
void CNetApp::checkNetStat()
{
    static int itimewiatsnmb = 0;
    int connections;
    TCP_STATE_STAT stat;
    connections = getTcpConnections(PATH_PROCNET_TCP, &stat);

    #if 0
    tracef("tcp connections is %d, they are:\n", connections);
    tracef("ESTABLISHED: %d\n", stat.establisheds);
    tracef("SYN_SENT: %d\n", stat.syn_sents);
    tracef("SYN_RECV: %d\n", stat.syn_recvs);
    tracef("FIN_WAIT1: %d\n", stat.fin_wait1s);
    tracef("FIN_WAIT2: %d\n", stat.fin_wait2s);
    tracef("TIME_WAIT: %d\n", stat.time_waits);
    tracef("CLOSE: %d\n", stat.closes);
    tracef("CLOSE_WAIT: %d\n", stat.close_waits);
    tracef("LAST_ACK: %d\n", stat.last_acks);
    tracef("LISTEN: %d\n", stat.listens);
    tracef("CLOSING: %d\n", stat.closings);
    #endif
    
    if (stat.syn_recvs >= SYN_FLOOD
    || stat.close_waits >= TOO_MANY_CONNECTIONS_NOT_CLOSED*g_nLogicNum)
    {
        errorf("the net is abnormal!\n");
        g_Challenger.Reboot();
    }
    else if(stat.time_waits >= TOO_MANY_CONNECTIONS_IN_TIME_WAIT_STATE)
    {
        itimewiatsnmb++;
        if(itimewiatsnmb > 10)
        {
            g_Challenger.Reboot();  
        }
    }
    else
    {
        itimewiatsnmb = 0;
    }
}
#endif

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)//add by jwd 20160128
int CNetApp::ReStartWifi()
{
	char cmd[100];
	memset(&cmd,0,sizeof(cmd));
	sprintf(cmd,"sed -i \"s/ssid=.*$/ssid=%s/\" /mnt/mtd/Config/hostapd.conf",CConfigWlanInfo::getLatest().szSsid);
	system(cmd); 
	
	memset(&cmd,0,sizeof(cmd));
	sprintf(cmd,"sed -i \"s/wpa_passphrase=.*$/wpa_passphrase=%s/\" /mnt/mtd/Config/hostapd.conf",CConfigWlanInfo::getLatest().szPassword);
	system(cmd); 

	system("killall hostapd");
	system("/app/ipcam/hostapd /mnt/mtd/Config/hostapd.conf -B");	
	return 0;
}
#endif


