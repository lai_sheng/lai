#include "nal_decode.h"


void analyze_nal(unsigned char* pcSrc, int* piConsumed, int iLength, int* piType)
{
    int iIndex = 0;
    int iStart = 0;

    //pcSrc[0]&0x80;		//forbidden bit
    *piType= pcSrc[0]&0x1F;

    pcSrc++;
    iLength--;
    
    for (iIndex = 0; iIndex + 1 < iLength; iIndex += 2)
	{
        if( NULL != pcSrc[iIndex])
        {
            continue;
        }
        
        if ( (iIndex > 0) && (pcSrc[iIndex-1]==0) )
        {
            iIndex--;
        }
        
        if ( (iIndex + 2 < iLength)
          && (pcSrc[iIndex+1] == 0)
          && (pcSrc[iIndex+2]<=3) )
		{
            if ( pcSrc[iIndex+2] != 3 )
		{
                /* startcode, so we must be past the end */
                iLength=iIndex;
            }
            break;
        }
    }

    if ( iIndex >= iLength-1 )
	{ //no escaped 0
        *piConsumed = iLength+1; //+1 for the header
        return; 
    }
    
    iStart=0;
    while ( iStart < iLength )
	{ 
        //remove escapes (very rare 1:2^22)
        if ( (iStart+2 < iLength) 
          && (pcSrc[iStart] == 0) 
          && (pcSrc[iStart+1] == 0) 
          && (pcSrc[iStart+2] <= 3) )
		{
            if ( pcSrc[iStart+2] == 3 )
			{ //escape
                iStart+=3;
                continue;
            }
			else //next start code
            {
                break;
			}
        }
    iStart ++;
    }
    
    *piConsumed= iStart + 1;//+1 for the header
}

/*
	返回值: -1 ------ 参数错误
		否则 返回已经分析的数据长度。
*/
int decode_nal_info(unsigned char* pcBuf, int iBufSize, NAL_INFO* pszNalInfo)
{
	int iBufIndex = 0;
	int iConsumed = 0;
	int iType = 0;
	int iNr = 0;

	if ( (NULL ==pszNalInfo) || (NULL ==pcBuf) )
	{
		return -1;
	}

	do{
		for(; iBufIndex + 3 < iBufSize; iBufIndex++)
		{
			/* this should allways succeed in the first iteration */
			if ( (pcBuf[iBufIndex] == 0) 
			  && (pcBuf[iBufIndex+1] == 0)
			  && (pcBuf[iBufIndex+2] == 1) )
			{
				break;
			}
		}
		
		if (iBufIndex + 3 >= iBufSize)
		{
			return iBufIndex;
		}
		
		iBufIndex += 3;		
		iNr++;
		
		analyze_nal(pcBuf+iBufIndex, &iConsumed, iBufSize - iBufIndex, &iType);
		
		pszNalInfo->pucAddress[iNr] = pcBuf+iBufIndex-4;
		pszNalInfo->iSize[iNr] = iConsumed + 4;
		pszNalInfo->iType[iNr] = iType;		
        	pszNalInfo->iNr = iNr;
		iBufIndex += iConsumed;
		
		//printf("decode_nal_info Size[%d], Type[%d], Nr [%d]\r\n",pszNalInfo->iSize[iNr] ,  pszNalInfo->iType[iNr] , pszNalInfo->iNr);
		
	}while ( iNr < NAL_MAX );
	
	return iBufIndex;
}


