/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              RtspMedia.cpp
**Version:                  Version 0.01
**Author:                  yang_shukui
**Created:                2010-5-6   16:40
**Modified:               
**Modify Reason: 
**Description:            处理视频和音频数据
*********************************************************************/
#include "RtspMedia.h"
#include "MultiTask/Guard.h"
#include "Devices/DevCapture.h"
#include "nal_decode.h"
#include "BaseFunc/VDDebug.h"
#include "BaseFunc/VDDelete.h"
#include "RtspSession.h"
#include "APIs/Net.h"
#include "../RtspApp/RtspThread.h"
#include "PortPool.h"
#include "Functions/jpeg.h"

extern int g_nCapture;

#define H264_STEAM_HEADER_LEN 4   //00000001
#define NAL_RTP_PACKET_SIZE 1300  //此大小必须小于rtp封包大小
#define UDP_PACKET_SIZE 2*1024
#define R_RTP_ID    101
#define RTP_JPEG_RESTART           0x40
#define db_printf  fprintf 

/*附录 C
下面这段代码用来阐明RTP/JPEG数据包分片和头的生成过程。

   For clarity and brevity, the structure definitions are only valid for
   32-bit big-endian (most significant octet first) architectures. Bit
   fields are assumed to be packed tightly in big-endian bit order, with
   no additional padding. Modifications would be required to construct a
   portable implementation.


?    RTP data header from RFC1889
 */
typedef struct {
        unsigned int version:2;   /* protocol version */
        unsigned int p:1;         /* padding flag */
        unsigned int x:1;         /* header extension flag */
        unsigned int cc:4;        /* CSRC count */
        unsigned int m:1;         /* marker bit */
        unsigned int pt:7;        /* payload type */
        ushort seq;              /* sequence number */
        uint ts;               /* timestamp */
        uint ssrc;             /* synchronization source */
        uint csrc[1];          /* optional CSRC list */
} rtp_hdr_t;

#define RTP_HDR_SZ 12

/* The following definition is from RFC1890 */
#define RTP_PT_JPEG             26

struct jpeghdr {
        unsigned int tspec:8;   /* type-specific field */
        unsigned int off:24;    /* fragment byte offset */
        uchar type;            /* id of jpeg decoder params */
        uchar q;               /* quantization factor (or table id) */
        uchar width;           /* frame width in 8 pixel blocks */
        uchar height;          /* frame height in 8 pixel blocks */
};

struct jpeghdr_rst {
        ushort dri;
        unsigned int f:1;
        unsigned int l:1;
        unsigned int count:14;
};

struct jpeghdr_qtable {
        uchar  mbz;
        uchar  precision;
        ushort length;
};

ushort CRtspMedia::SendFrame( uint ts, uint ssrc,
                   uchar *jpeg_data, int len, uchar type,
                   uchar typespec, int width, int height, int dri,
                   uchar q, uchar *lqt, uchar *cqt) 
{
    rtp_hdr_t rtphdr;
    struct jpeghdr jpghdr;
    struct jpeghdr_rst rsthdr;

    struct jpeghdr_qtable qtblhdr;
    uchar packet_buf[NAL_RTP_PACKET_SIZE];
    uchar *ptr;
    int bytes_left = len;
    int data_len;

    /* Initialize RTP header
     */
    rtphdr.version = 2;
    rtphdr.p = 0;
    rtphdr.x = 0;
    rtphdr.cc = 0;
    rtphdr.m = 0;
    rtphdr.pt = RTP_PT_JPEG;
    rtphdr.seq = ntohs((unsigned short)getSequenceNum());
    rtphdr.ts = ts;
    rtphdr.ssrc = ssrc;

    /* Initialize JPEG header
     */
    jpghdr.tspec = typespec;
    jpghdr.off = 0;
    jpghdr.type = type | ((dri != 0) ? RTP_JPEG_RESTART : 0);
    jpghdr.q = q;
    jpghdr.width = width / 8;
    jpghdr.height = height / 8;

    /* Initialize DRI header
     */
    if (dri != 0) 
    {
        rsthdr.dri = dri;
        rsthdr.f = 1;        /* This code does not align Ris */
        rsthdr.l = 1;
        rsthdr.count = 0x3fff;
    }

    /* Initialize quantization table header
     */
    if (q >= 128) 
    {
        qtblhdr.mbz = 0;
        qtblhdr.precision = 0; /* This code uses 8 bit tables only */
        qtblhdr.length = 128;  /* 2 64-byte tables */
    }

    while (bytes_left > 0) 
    {
        ptr = packet_buf + RTP_HDR_SZ;
        memcpy(ptr, &jpghdr, sizeof(jpghdr));

        ptr += sizeof(jpghdr);

        if (dri != 0) 
        {
            memcpy(ptr, &rsthdr, sizeof(rsthdr));
            ptr += sizeof(rsthdr);
        }

        if (q >= 128 && jpghdr.off == 0) 
        {
            memcpy(ptr, &qtblhdr, sizeof(qtblhdr));
            ptr += sizeof(qtblhdr);
            memcpy(ptr, lqt, 64);
            ptr += 64;
            memcpy(ptr, cqt, 64);
            ptr += 64;
        }

        data_len = NAL_RTP_PACKET_SIZE - (ptr - packet_buf);
        if (data_len >= bytes_left)
        {
            data_len = bytes_left;
            rtphdr.m = 1;
        }

        memcpy(packet_buf, &rtphdr, RTP_HDR_SZ);
        memcpy(ptr, jpeg_data + jpghdr.off, data_len);

        //send_packet(packet_buf, (ptr – packet_buf) + data_len);


        CPacket* pPacket = g_PacketManager.GetPacket(UDP_PACKET_SIZE);
        if (pPacket)
        {    
            memcpy(pPacket->GetBuffer() + 4, packet_buf, (ptr - packet_buf) + data_len);
            SET_MEDIA_TYPE(pPacket, RtspVedio);
            SendPacket(pPacket, HanderVRtp);
        }
        
        jpghdr.off += data_len;
        bytes_left -= data_len;
        rtphdr.seq = ntohs((unsigned short)getSequenceNum());
    }
    return rtphdr.seq;
}

VD_BOOL Sps8Bytes(int resolution)
{
    if( CAPTURE_SIZE_D1 == resolution 
        || CAPTURE_SIZE_720P == resolution 
        || CAPTURE_SIZE_HD1 == resolution
        || CAPTURE_SIZE_SXGA == resolution
        || CAPTURE_SIZE_XVGA == resolution )
    {
        return VD_TRUE;
    }

    return VD_FALSE;
}

CRtspMedia::CRtspMedia(CRtspSession *rtspsession) : m_RtspSession(rtspsession)
{
    int index = 0;
    
    m_iChannel = 0;
    m_iStreamType = 0;
    m_Magic = 0;
    m_bAudioOn = VD_FALSE;    
    m_iSplitNo = 1;
    m_flag = false;
    m_Resolution = CAPTURE_SIZE_NR;
    m_cabRtpPacket.Append(NULL, 10*1024);
    m_cabRtpPacket.Reset(); 
    m_cabRtpPacket.SetIncreaseSize(1024);
    m_bFistIFrame = true;
    m_bMonitor = false;
    
    m_iFPS = 25;    
    m_TaskTimer.SetMaxPeriodTime(TIME_PERIOD);
    m_MediaType = MEDIA_VEDIO;
    m_QueueReleaseFlag = VD_TRUE;
    m_rtpModel = RTP_OVER_UDP;
    
    for(index = 0; index < RtspMediaNr; index++)
    {
        m_usSeq[index] = 0;
        m_ulSSRC[index] = 0;
        m_ulRtpTimestamp[index] = 0;
    }
    
    for(index = HanderVRtp; index < HanderRtspNr; index++)
    {
        m_iDataLen[index] = 0;
        ClearQueue(index);
    }
}

CRtspMedia::~CRtspMedia()
{
    int index = HanderVRtp;
    
    m_iChannel = -1;
    m_iStreamType = -1;
    m_iSplitNo = 1;
    m_bFistIFrame = true;
    m_cabRtpPacket.Reset();

    for(; index < HanderRtspNr; index++)
    {
        ClearQueue(index);
    }
}
//媒体回调
void CRtspMedia::OnCapture(int iChannel, uint iCodeType, CPacket *pPacket)
{
    if (!pPacket)
    {
        VD_TRACE("CVapVideo::OnRealData : pPacket is NULL\n");
        return ;
    }
    pPacket->AddRef();
    
   if (iCodeType == CHL_JPEG_T)
   {
        PrcocessJpegVideo(iChannel, pPacket,iCodeType);
        pPacket->Release();
        return;
    }
    
    if (RTP_OVER_MULTI_CAST != m_rtpModel)
    {
        /* 数字通道目前和模拟通道分开处理先 */
        if (iChannel >= g_nCapture)
        {
            PrcocessDigiChVideo(iChannel, pPacket,iCodeType);
        }
        else
        {
            PrcocessVideo(iChannel, pPacket,iCodeType);
        }
    }
    else
    {
        PrcocessMultiVideo(iChannel, pPacket,iCodeType);
    }

    pPacket->Release();
    return;
}

VD_VOID CRtspMedia::PrcocessMultiVideo(int iCh, CPacket *pPacket,uint iCodeType)
{
    printf("CRtspMedia::PrcocessMultiVideo()in\r\n");
    return;
}

VD_BOOL CRtspMedia::DoMediaTask(int media)
{
    if(media < HanderVRtp || media > HanderARtcp)
    {
        VD_TRACE("CRtspMediaVedio::GetPacket error, media <%d>\n",media);
        return VD_FALSE;
    }
    CGuard guard(m_szMutex[media]);
    CPacket *ptk = NULL;

    int totalSize = m_RtpQueue[media].size();
    int index = 0;
    int iRet = 0;
    while(index < totalSize)
    {
        ptk = m_RtpQueue[media].front();
        iRet = m_RtspSession->DoRtpPacket(ptk, media);
        if(iRet < 0)
        {
            if(m_QueueReleaseFlag)
            {
                m_iDataLen[media] -= ptk->GetLength();
                ptk->Release();
                m_RtpQueue[media].pop_front();
            }
            break;
        }
        m_iDataLen[media] -= ptk->GetLength();
        ptk->Release();
        m_RtpQueue[media].pop_front();
        
        index++;
        //VD_TRACE("m_iDataLen[media] <%d>\n",m_iDataLen[media]);
        if(m_TaskTimer.IsOutTimePeriod(iRet))
        {
            break;
        }
    }
    return VD_TRUE;
}
void CRtspMedia::SetQueueReleaseFlag(VD_BOOL flag)
{
    m_QueueReleaseFlag = flag;
}
int CRtspMedia::GetChannel()
{
    return m_iChannel;
}

int CRtspMedia::GetStreamType()
{
    return m_iStreamType;
}

VD_BOOL CRtspMedia::IsAudioOn()
{
    return m_bAudioOn;
}

//处理视频会话的数据包
void CRtspMedia::PrcocessJpegVideo(int iCh, CPacket *pPacket,uint iCodeType)
{
    //在这个函数里面要处理丢帧的操作
    PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)pPacket->GetHeader();
    uchar *pBufSend = NULL;
    uint uiWillSendLen = 0; /* 本次发送的数据长度 */
   
    // 共FRAME_MAX_NUM个定位信息
    for (int iFrameId=0; iFrameId<FRAME_MAX_NUM; iFrameId++)
    {    
        uchar iFrameType = pPacketHead->FrameInfo[iFrameId].FrameType;//祯类型
        int iLocation = pPacketHead->FrameInfo[iFrameId].FramePos;//帧开始位置
        uint iFrameDataLen = pPacketHead->FrameInfo[iFrameId].DataLength;///*!< 帧在本块中的长度 */
        uchar iFrameFlag = pPacketHead->FrameInfo[iFrameId].FrameFlag;//帧头帧尾标识

        //VD_TRACE("iFrameId <%d>, iFrameDataLen <%d>\n", iFrameId, iFrameDataLen);
        if( PACK_TYPE_FRAME_NULL == iFrameType) 
        {
            break;
        }   

        pBufSend = pPacket->GetBuffer();
        pBufSend += iLocation;

        //VD_TRACE("*******************iFrameTotalLen = %d,iFrameDataLen = %d\n",iFrameTotalLen,iFrameDataLen);

        /* 如果包含帧头和帧尾的帧 */
        if (PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag) 
        {   
//            SetTimestamp(RtspVedio, 90000/m_iFPS);
            m_cabRtpPacket.Reset();
            m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
            uiWillSendLen = m_cabRtpPacket.Size();
        }
        /* 只包含帧头的帧 */
        else if ( PACK_CONTAIN_FRAME_HEAD == iFrameFlag )
        {
//            SetTimestamp(RtspVedio, 90000/m_iFPS);
            m_cabRtpPacket.Reset();
            m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
        }
        /* 只包含帧尾的帧 */
        else if ( PACK_CONTAIN_FRAME_TRAIL == iFrameFlag )
        {
            m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
            uiWillSendLen = m_cabRtpPacket.Size();
        }
        /* 不包含帧头和帧尾的帧 */
        else if ( PACK_CONTAIN_FRAME_NONHT == iFrameFlag )
        {
            m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
        }
        else
        {
            m_cabRtpPacket.Reset();
            continue;
        }

        if(uiWillSendLen > 0)
        {   
        #if 0
            static int fileNo = 0;
            char fileName[32];

            memset(fileName, 0, sizeof(fileName));
            sprintf(fileName,"/tmp/onvfi%d.jpg", ++fileNo);
            
            CDFile file(fileName, "w");
            file.Write((unsigned char * )m_cabRtpPacket.Buf(), m_cabRtpPacket.Size());
            file.Close();
    #endif
        
            jpeg_packet_rtp((char *)m_cabRtpPacket.Buf()+16,m_cabRtpPacket.Size()-16);
#if 0         
            SendFrame(ntohl(m_ulRtpTimestamp[RtspVedio]), ntohs(m_ulSSRC[RtspVedio]),
                (unsigned char * )m_cabRtpPacket.Buf()+16,
                m_cabRtpPacket.Size()-16, 
                1, 0, 352, 288, 0x18C, 79, NULL, NULL);
#endif

            /* 回收空间 */                 
            m_cabRtpPacket.Pop(NULL,uiWillSendLen);
        }
    }
}

/* spi等信息的最大长度 */
#define MAX_SPS_PPS_SEI_LEN 512
#define MAX_H264_HEAD_LEN 4

const char g_acH264SliceFlag[MAX_H264_HEAD_LEN] = {0x00, 0x00, 0x00, 0x01};

//处理视频会话的数据包
int CRtspMedia::PrcocessSliceInfo()
{
    int index = 16;/* 私有头跳过 */
    char *pBufSend = (char *)m_cabRtpPacket.Buf();
    uint uiWillSendLen = m_cabRtpPacket.Size();

    m_iFrameHeadLen = 0;
    
    int iLastindex = 0;

    m_iFPS = ((pBufSend[5] & 0x1F)<=35)?(pBufSend[5] & 0x1F):m_iFPS;
    
    while (index < uiWillSendLen)
    {
        if (g_acH264SliceFlag[0] == pBufSend[index] 
            && uiWillSendLen - index > MAX_H264_HEAD_LEN
            && !memcmp(&pBufSend[index], g_acH264SliceFlag, MAX_H264_HEAD_LEN))
        {
                    index += MAX_H264_HEAD_LEN;
            
                    /* I帧数据内容 */
                    if ( (pBufSend [index] &0x1f) == 5)
                    {
                        if (iLastindex !=0 )
                        {
                            PacketFU_A(&pBufSend[iLastindex] , index - iLastindex - MAX_H264_HEAD_LEN, false,0,1);
                        }

                        m_cabRtpPacket.Pop(NULL,index);
                        m_iFrameHeadLen = index;

                         /* I帧数据不在这里发 ，统一在外面发送*/
                         return 0;
                    }
                    else
                    {
                        if (iLastindex !=0 )
                        {
                            PacketFU_A(&pBufSend[iLastindex],  index - iLastindex - MAX_H264_HEAD_LEN, false,0,1);
                            iLastindex = index;
                       }
                        else
                        {
                            iLastindex = index;
                        }
                    }
                    
                    continue;
        }
              
        index++;
    }

      return -1;
    
}


//数字通道的rtsp现在先独立处理 
void CRtspMedia::PrcocessDigiChVideo(int iCh, CPacket *pPacket,uint iCodeType)
{
    //在这个函数里面要处理丢帧的操作
    PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)pPacket->GetHeader();
    uchar *pBufSend = NULL;
    uint uiWillSendLen = 0; /* 本次发送的数据长度 */
    int iSplitNums = 0;
    bool bLastSlice = false;
    static uchar stFrameType = 0; 

    // 共FRAME_MAX_NUM个定位信息
    for (int iFrameId=0; iFrameId<FRAME_MAX_NUM; iFrameId++)
    {    
        uchar iFrameType = pPacketHead->FrameInfo[iFrameId].FrameType;//祯类型
        int iLocation = pPacketHead->FrameInfo[iFrameId].FramePos;//帧开始位置
        uint iFrameDataLen = pPacketHead->FrameInfo[iFrameId].DataLength;///*!< 帧在本块中的长度 */
        uint iFrameTotalLen = pPacketHead->FrameInfo[iFrameId].FrameLength;
        uchar iFrameFlag = pPacketHead->FrameInfo[iFrameId].FrameFlag;//帧头帧尾标识

        //VD_TRACE("iFrameId <%d>, iFrameDataLen <%d>\n", iFrameId, iFrameDataLen);
        if( PACK_TYPE_FRAME_NULL == iFrameType) 
        {
            break;
        }

        if (0 == iFrameDataLen)
        {
            unsigned char nal_header = 0x61;
            if(PACK_TYPE_FRAME_I == stFrameType)
            {
                 nal_header = 0x65;
            }
            pBufSend = (uchar *)m_cabRtpPacket.Buf(); 
            uiWillSendLen =  m_cabRtpPacket.Size();
            bLastSlice = true;

            VD_TRACE(" CVapVideo::PrcocessVideo ERROR: iFrameDataLen is zero !\n");
            PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice , nal_header , iSplitNums);    
            /* 回收空间 */ 
            m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            continue;
        }
        ////没有有效数据则退出
        if ( (PACK_TYPE_FRAME_I != iFrameType) 
            && (PACK_TYPE_FRAME_P != iFrameType)  )
        {
            continue;
        }

        if (m_bFistIFrame)
        {    
            //为了尽快将I祯送出
            if (PACK_TYPE_FRAME_I!= iFrameType)
            {
                VD_TRACE("it is not the PACK_TYPE_FRAME_I !\n");
                continue;
            }
            m_bFistIFrame = false;
        }

        stFrameType = iFrameType;
        pBufSend = pPacket->GetBuffer();
        pBufSend += iLocation;

        //VD_TRACE("*******************iFrameTotalLen = %d,iFrameDataLen = %d\n",iFrameTotalLen,iFrameDataLen);

        if(PACK_TYPE_FRAME_I == iFrameType)
        {
            unsigned char nal_header = 0x65;
            
            /* 如果包含帧头和帧尾的帧 */
            if (PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag) 
            {   
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);

                /* 不用处理,就这样发出去 */
                m_cabRtpPacket.Reset();
                //VDDumpHex((const char * )pBufSend,16);
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                PrcocessSliceInfo();
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen = m_cabRtpPacket.Size();
                m_iSplitNo = 1;
                bLastSlice = true;
            }
            /* 只包含帧头的帧 */
            else if ( PACK_CONTAIN_FRAME_HEAD == iFrameFlag )
            {
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);

                /* 只发送VIDEO_DEVITION_LENTH长度,不足放到下次发送 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if(m_cabRtpPacket.Size() <  MAX_SPS_PPS_SEI_LEN)
                {
                    m_flag = true;
                    continue;
                }
                
                PrcocessSliceInfo();
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen = (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                m_iSplitNo = 1;
                bLastSlice = false;
            }
            /* 只包含帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_TRAIL == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if (true == m_flag)
                {
                    PrcocessSliceInfo();
                    m_flag = false;
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();   
                uiWillSendLen = m_cabRtpPacket.Size();
                bLastSlice = true;                        
            }
            /* 不包含帧头和帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_NONHT == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if (true == m_flag )
                {
                    if(m_cabRtpPacket.Size() <  MAX_SPS_PPS_SEI_LEN)
                    {
                        m_flag = true;
                        continue;
                    }

                    PrcocessSliceInfo();
                    m_flag = false;       
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen =  (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                bLastSlice = false;
            }
            else
            {
                m_cabRtpPacket.Reset();
                continue;
            }

            int iFrameLen = iFrameTotalLen - m_iFrameHeadLen;
            iSplitNums = (iFrameLen + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;

            //需要去除1字节nal_header,防止数据刚好为1301
            if(iSplitNums > 1)
            {
                iSplitNums = (iFrameLen - 1 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            }

            if ((iSplitNums != 1) && (m_iSplitNo == 1))
            {
                m_cabRtpPacket.Pop(NULL,1);
                pBufSend = (uchar *)m_cabRtpPacket.Buf();   
                m_iFrameHeadLen += 1;
                iFrameLen -= 1;
                uiWillSendLen -= 1;
            }
            
            if(uiWillSendLen > 0)
            {                
                PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice,nal_header,iSplitNums);
            
                /* 回收空间 */                 
                m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            }
        }
        else
        {
            unsigned char nal_header = 0x61;
            iSplitNums = (iFrameTotalLen - 12 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            if(iSplitNums > 1)
            {
                iSplitNums = (iFrameTotalLen - 13 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            }            

            /* 如果包含帧头和帧尾的帧 */
            if (PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag) 
            {
                /* 不用处理,就这样发出去 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(iFrameDataLen <= 13)
                {
                    m_flag = true;
                    continue;
                }
                
                if(1 == iSplitNums)
                {
                    m_cabRtpPacket.Pop(NULL, 12);
                }
                else
                {
                    m_cabRtpPacket.Pop(NULL, 13);
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen = m_cabRtpPacket.Size();
                m_iSplitNo = 1;
                bLastSlice = true;
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);
            }
            /* 只包含帧头的帧 */
            else if ( PACK_CONTAIN_FRAME_HEAD == iFrameFlag )
            {               
                /* 只发送NAL_RTP_PACKET_SIZE长度,不足放到下次发送 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(iFrameDataLen <= 13)
                {
                    m_flag = true;
                    continue;
                }
                if(1 == iSplitNums)
                {
                    m_cabRtpPacket.Pop(NULL, 12);
                }
                else
                {
                    m_cabRtpPacket.Pop(NULL, 13);
                }                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();

                uiWillSendLen = (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                m_iSplitNo = 1;        
                bLastSlice = false;

                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);
            }
            /* 只包含帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_TRAIL == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(true == m_flag )
                {
                    if(1 == iSplitNums)
                    {
                        m_cabRtpPacket.Pop(NULL, 12);
                    }
                    else
                    {
                        m_cabRtpPacket.Pop(NULL, 13);
                    }
                    m_flag = false;
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();

                uiWillSendLen = m_cabRtpPacket.Size();
                bLastSlice = true;
            }
            /* 不包含帧头和帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_NONHT == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                
                if(true == m_flag )
                {
                    if(1 == iSplitNums)
                    {
                        m_cabRtpPacket.Pop(NULL, 12);
                    }
                    else
                    {
                        m_cabRtpPacket.Pop(NULL, 13);
                    }
                    m_flag = false;
                }

                uiWillSendLen =  (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                bLastSlice = false;
            }
            else
            {
                m_cabRtpPacket.Reset();
                continue;
            }

            if(uiWillSendLen > 0)
            {
                PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice,nal_header,iSplitNums);                
                /* 回收空间 */
                m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            }            
        }
    }
}

//处理视频会话的数据包
void CRtspMedia::PrcocessVideo(int iCh, CPacket *pPacket,uint iCodeType)
{
    //在这个函数里面要处理丢帧的操作
    PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)pPacket->GetHeader();
    uchar *pBufSend = NULL;
    uint uiWillSendLen = 0; /* 本次发送的数据长度 */
    int iSplitNums = 0;
    bool bLastSlice = false;
    static uchar stFrameType = 0; 

    // 共FRAME_MAX_NUM个定位信息
    for (int iFrameId=0; iFrameId<FRAME_MAX_NUM; iFrameId++)
    {    
        uchar iFrameType = pPacketHead->FrameInfo[iFrameId].FrameType;//祯类型
        int iLocation = pPacketHead->FrameInfo[iFrameId].FramePos;//帧开始位置
        uint iFrameDataLen = pPacketHead->FrameInfo[iFrameId].DataLength;///*!< 帧在本块中的长度 */
        uint iFrameTotalLen = pPacketHead->FrameInfo[iFrameId].FrameLength;
        uchar iFrameFlag = pPacketHead->FrameInfo[iFrameId].FrameFlag;//帧头帧尾标识
        
        //VD_TRACE("iFrameId <%-3d>, iFrameType <%-3d>, iFrameFlag <%-3d>, iFrameDataLen <%-6d>\n", 
            //iFrameId, iFrameType, iFrameFlag, iFrameDataLen);
        if( PACK_TYPE_FRAME_NULL == iFrameType) 
        {
            break;
        }

        if (0 == iFrameDataLen)
        {
            unsigned char nal_header = 0x61;
            if(PACK_TYPE_FRAME_I == stFrameType)
            {
                 nal_header = 0x65;
            }
            pBufSend = (uchar *)m_cabRtpPacket.Buf(); 
            uiWillSendLen =  m_cabRtpPacket.Size();
            bLastSlice = true;

            //VD_TRACE(" CVapVideo::PrcocessVideo ERROR: iFrameDataLen is zero !\n");
            PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice , nal_header , iSplitNums);    
            /* 回收空间 */ 
            m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            continue;
        }
        
        if (m_bFistIFrame)
        {    
            //为了尽快将I祯送出
            if (PACK_TYPE_FRAME_I!= iFrameType)
            {
                VD_TRACE("it is not the PACK_TYPE_FRAME_I !\n");
                continue;
            }
            m_bFistIFrame = false;
        }

        stFrameType = iFrameType;
        pBufSend = pPacket->GetBuffer();
        pBufSend += iLocation;

        //VD_TRACE("*******************iFrameTotalLen = %d,iFrameDataLen = %d\n",iFrameTotalLen,iFrameDataLen);

        if(PACK_TYPE_FRAME_I == iFrameType)
        {
            unsigned char nal_header = 0x65;
            
            /* 如果包含帧头和帧尾的帧 */
            if (PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag) 
            {   
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);

                /* 不用处理,就这样发出去 */
                m_cabRtpPacket.Reset();
                //VDDumpHex((const char * )pBufSend,16);
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                PrcocessSliceInfo();

                m_iSplitNo = 1;
            }
            /* 只包含帧头的帧 */
            else if ( PACK_CONTAIN_FRAME_HEAD == iFrameFlag )
            {
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);

                /* 只发送VIDEO_DEVITION_LENTH长度,不足放到下次发送 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if(m_cabRtpPacket.Size() <  MAX_SPS_PPS_SEI_LEN)
                {
                    m_flag = true;
                    continue;
                }
                
                PrcocessSliceInfo();
                m_iSplitNo = 1;
            }
            /* 只包含帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_TRAIL == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if (true == m_flag)
                {
                    PrcocessSliceInfo();
                    m_flag = false;
                }
                                      
            }
            /* 不包含帧头和帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_NONHT == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);

                if (true == m_flag )
                {
                    if(m_cabRtpPacket.Size() <  MAX_SPS_PPS_SEI_LEN)
                    {
                        m_flag = true;
                        continue;
                    }

                    PrcocessSliceInfo();
                    m_flag = false;       
                }

            }
            else
            {
                m_cabRtpPacket.Reset();
                continue;
            }

            int iFrameLen = iFrameTotalLen - m_iFrameHeadLen;
            iSplitNums = (iFrameLen + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;

            //需要去除1字节nal_header,防止数据刚好为1301
            if(iSplitNums > 1)
            {
                iSplitNums = (iFrameLen - 1 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            }

            if ((iSplitNums != 1) && (m_iSplitNo == 1))
            {
                m_cabRtpPacket.Pop(NULL,1);
                pBufSend = (uchar *)m_cabRtpPacket.Buf();   
                m_iFrameHeadLen += 1;
                iFrameLen -= 1;
                uiWillSendLen -= 1;
            }

            if(( PACK_CONTAIN_FRAME_NONHT == iFrameFlag ) ||( PACK_CONTAIN_FRAME_HEAD == iFrameFlag ))
            {
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen =  (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                bLastSlice = false;
            }
            else if(( PACK_CONTAIN_FRAME_TRAIL== iFrameFlag ) ||( PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag ))
            {
                pBufSend = (uchar *)m_cabRtpPacket.Buf();   
                uiWillSendLen = m_cabRtpPacket.Size();
                bLastSlice = true;  
            }
            
            if(uiWillSendLen > 0)
            {                
                PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice,nal_header,iSplitNums);
            
                /* 回收空间 */                 
                m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            }
        }
        else if (PACK_TYPE_FRAME_P == iFrameType)
        {
            unsigned char nal_header = 0x61;
            iSplitNums = (iFrameTotalLen - 12 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            if(iSplitNums > 1)
            {
                iSplitNums = (iFrameTotalLen - 13 + NAL_RTP_PACKET_SIZE - 1) / NAL_RTP_PACKET_SIZE;
            }            

            /* 如果包含帧头和帧尾的帧 */
            if (PACK_CONTAIN_FRAME_HEADTRAIL == iFrameFlag) 
            {
                /* 不用处理,就这样发出去 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(iFrameDataLen <= 13)
                {
                    m_flag = true;
                    continue;
                }
                
                if(1 == iSplitNums)
                {
                    m_cabRtpPacket.Pop(NULL, 12);
                }
                else
                {
                    m_cabRtpPacket.Pop(NULL, 13);
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                uiWillSendLen = m_cabRtpPacket.Size();
                m_iSplitNo = 1;
                bLastSlice = true;
                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);
            }
            /* 只包含帧头的帧 */
            else if ( PACK_CONTAIN_FRAME_HEAD == iFrameFlag )
            {               
                /* 只发送NAL_RTP_PACKET_SIZE长度,不足放到下次发送 */
                m_cabRtpPacket.Reset();
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(iFrameDataLen <= 13)
                {
                    m_flag = true;
                    continue;
                }
                if(1 == iSplitNums)
                {
                    m_cabRtpPacket.Pop(NULL, 12);
                }
                else
                {
                    m_cabRtpPacket.Pop(NULL, 13);
                }                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();

                uiWillSendLen = (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                m_iSplitNo = 1;        
                bLastSlice = false;

                //rtp时间戳设置
                SetTimestamp(RtspVedio, 90000/m_iFPS);
            }
            /* 只包含帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_TRAIL == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                if(true == m_flag )
                {
                    if(1 == iSplitNums)
                    {
                        m_cabRtpPacket.Pop(NULL, 12);
                    }
                    else
                    {
                        m_cabRtpPacket.Pop(NULL, 13);
                    }
                    m_flag = false;
                }
                
                pBufSend = (uchar *)m_cabRtpPacket.Buf();

                uiWillSendLen = m_cabRtpPacket.Size();
                bLastSlice = true;
            }
            /* 不包含帧头和帧尾的帧 */
            else if ( PACK_CONTAIN_FRAME_NONHT == iFrameFlag )
            {
                m_cabRtpPacket.Append(pBufSend,iFrameDataLen);
                pBufSend = (uchar *)m_cabRtpPacket.Buf();
                
                if(true == m_flag )
                {
                    if(1 == iSplitNums)
                    {
                        m_cabRtpPacket.Pop(NULL, 12);
                    }
                    else
                    {
                        m_cabRtpPacket.Pop(NULL, 13);
                    }
                    m_flag = false;
                }

                uiWillSendLen =  (m_cabRtpPacket.Size()/NAL_RTP_PACKET_SIZE) * NAL_RTP_PACKET_SIZE;
                bLastSlice = false;
            }
            else
            {
                m_cabRtpPacket.Reset();
                continue;
            }

            if(uiWillSendLen > 0)
            {
                PacketFU_A((char *)pBufSend ,  uiWillSendLen , bLastSlice,nal_header,iSplitNums);                
                /* 回收空间 */
                m_cabRtpPacket.Pop(NULL,uiWillSendLen);
            }            
        }
        else if(PACK_TYPE_FRAME_AUDIO == iFrameType && iFrameDataLen > 8)
        {
        #if 0
            static uint t1 = SystemGetMSCount();
            static uint no = 0;
            uint t2 = SystemGetMSCount();

            no++;
            t2 = 8 * (t2 -t1)/no;
            if (t2 > 481 || t2 < 479)
            {
               t2 = 480;
            }
            printf("======== [%d]\n", t2);
            SetTimestamp(RtspAudio, t2);
        #endif
            SetTimestamp(RtspAudio, 480);
            sendAudio(iCh, (char *)&pBufSend[8], iFrameDataLen - 8, true);
        }
    }
}

void CRtspMedia::PacketFU_A(char* pBuffer, int iLen, bool bLastSlice,unsigned char nal_header,int Nums)
{  

    if ( NULL == pBuffer)
    {
        return ;
    }
    if ((iLen <= NAL_RTP_PACKET_SIZE ) &&(Nums == 1))
    {        
        //每一桢打marker
        sendFU_A(m_iChannel,NULL,NULL, (char *)(pBuffer ),iLen , bLastSlice); 
        //VD_TRACE(" ilen = %d \n",iLen);
        //trace("======= RTP SEND BUFFERSIZE nal_header[%X] len %d\n", *(pBuffer+H264_STEAM_HEADER_LEN), iLen- H264_STEAM_HEADER_LEN);
    }
    else
    {
        int total = iLen;// 去掉header
        if(Nums > 200)//一般I/P帧最多算200k，超过这个值说明异常了
        {
            //trace("SO MUCH iAckNum = %d,error\n",Nums);
            //return;
        }

        int curr = 0;
        unsigned char* fu_buf = (unsigned char *)pBuffer; 
        int fu_len = 0;
        bool marker = false;
        //如果分片nal，去除00000001后直接发送，marker标志根据bFlags是否最后一个slice，同时判断下是否最后一个包
        while( curr < total )
        {
            //printf("--------m_iSplitNo = %d ,",m_iSplitNo);
            unsigned char fui, fuh;
            fui = 28;
            fui = ( fui | (nal_header & 0x60) );
            if( m_iSplitNo == 1 )
            {
                // 第一个包
                fuh = ( 0x80 | (nal_header & 0x1F) );
                fu_len = NAL_RTP_PACKET_SIZE;
                marker = false;
                m_iSplitNo++;
            }
            else if((m_iSplitNo == Nums) ||( total - curr < NAL_RTP_PACKET_SIZE ))
            {
                // 最后一个包
                fuh = ( 0x40 | (nal_header & 0x1F) );
                fu_len = total - curr ;
                marker = true; //还需要看是否最后一个nalu
                m_iSplitNo = 1;
            }
            else
            {
                // 中间的包
                fuh = (nal_header & 0x1F);
                fu_len = NAL_RTP_PACKET_SIZE ;
                marker = false;
                m_iSplitNo++;
            }
            sendFU_A(m_iChannel, (char *)&fui, (char *)&fuh, (char *)fu_buf,fu_len,marker);
            curr = curr + fu_len;
            fu_buf = fu_buf + fu_len;
        } // while( curr < total )
    }//end if (nLength < 1400)
}

int CRtspMedia::sendFU_A(int iCh,char* fui,char* fuh,char* data, int len, bool bMarker)
{
    if (len <= 0 || NULL == data || len > NAL_RTP_PACKET_SIZE)
    {
        return -1;
    }
       
    CPacket* pPacket = g_PacketManager.GetPacket(len + RTPHDR_SIZE + m_Magic + 2);
    if (pPacket)
    {
        char *pRtpBuf = (char *)(pPacket->GetBuffer() + m_Magic);

        RTPHDR* pszHdr = (RTPHDR* )(pRtpBuf ); 
        memset((char *)pszHdr, 0, RTPHDR_SIZE);
        pszHdr->version = 2;
        pszHdr->payloadtype = 96;
        
        pszHdr->marker = bMarker;
        pszHdr->seqnum = ntohs((unsigned short)getSequenceNum(RtspVedio));
        pszHdr->timestamp = ntohl(m_ulRtpTimestamp[RtspVedio]);
        pszHdr->ssrc = ntohl(m_ulSSRC[RtspVedio]);
        if(NULL == fui)
        {
            memcpy(pRtpBuf + RTPHDR_SIZE, data, len);
            pPacket->SetLength(RTPHDR_SIZE + m_Magic + len);
        }
        else
        {
           /*
            padding 添加方式，如果需要填充字节的话
            1)pszHdr->padding = 1;
            2)最后一个字节，如果长度为1，只需要填充0x01
               如果2个字节，填充 00 0x02
               如果3字节，填充 00 00 0x03
               一般4字节对齐，填充方式比较简单
            */
            memcpy(pRtpBuf + RTPHDR_SIZE, fui, 1);
            memcpy(pRtpBuf + RTPHDR_SIZE + 1, fuh, 1);
            memcpy(pRtpBuf + RTPHDR_SIZE + 2, data, len);
            pPacket->SetLength(RTPHDR_SIZE + 2 + m_Magic + len);
        }  
        if(RTP_OVER_TCP == m_rtpModel)
        {
            int len = pPacket->GetLength() - m_Magic;
            uchar *pMagic = (uchar *)(pPacket->GetBuffer());
            pMagic[0] = '$';
            pMagic[1] = m_Interleaved[HanderVRtp];
            pMagic[2] = (len & 0x0000FF00) >> 8;
            pMagic[3] = len & 0xff;
            //VDDumpHex((const char*)pMagic,16);
        }
        SendPacket(pPacket, HanderVRtp);
    
        return 0;
    }
    else
    {
        VD_TRACE("##NO PACKET^^^^^^^%d^^^^^^^^^^^\r\n", len);
    }
    return -1;
}

int CRtspMedia::sendAudio(int iCh, char* data, int len, bool bMarker)
{
    if (len <= 0 || NULL == data || len > NAL_RTP_PACKET_SIZE)
    {
        return -1;
    }
    /*
    static CDFile fp;
    if (!fp.IsValid())
    {
        fp.Open("vlc.g711a", "w");
    }
    fp.Write((unsigned char *)data, len);
    */
    CPacket* pPacket = g_PacketManager.GetPacket(len + RTPHDR_SIZE + m_Magic);
    if (pPacket)
    {
        char *pRtpBuf = (char *)(pPacket->GetBuffer() + m_Magic);

        RTPHDR* pszHdr = (RTPHDR* )(pRtpBuf ); 
        memset((char *)pszHdr, 0, RTPHDR_SIZE);
        pszHdr->version = 2;
        pszHdr->payloadtype = 8;
        
        pszHdr->marker = (true == bMarker ? 1 : 0);
        pszHdr->seqnum = ntohs((unsigned short)getSequenceNum(RtspAudio));
        pszHdr->timestamp = ntohl(m_ulRtpTimestamp[RtspAudio]);
        pszHdr->ssrc = ntohl(m_ulSSRC[RtspAudio]);
       
        memcpy(pRtpBuf + RTPHDR_SIZE, data, len);
        pPacket->SetLength(RTPHDR_SIZE + m_Magic + len);
          
        if(RTP_OVER_TCP == m_rtpModel)
        {
            int len = pPacket->GetLength() - m_Magic;
            uchar *pMagic = (uchar *)(pPacket->GetBuffer());
            pMagic[0] = '$';
            pMagic[1] = m_Interleaved[HanderARtp];
            pMagic[2] = (len & 0x0000FF00) >> 8;
            pMagic[3] = len & 0xff;
            //VDDumpHex((const char*)pMagic,16);
        }
        SendPacket(pPacket, HanderARtp);
    
        return 0;
    }
    else
    {
        VD_TRACE("##sendAudio, NO PACKET^^^^^^^%d^^^^^^^^^^^\r\n", len);
    }
    return -1;
}

void CRtspMedia::SetTimestamp(int mediaType, int timeOff)
{
    m_ulRtpTimestamp[mediaType] += timeOff;
    //VD_TRACE("timestamp %u\n",m_ulRtpTimestamp);
}
void CRtspMedia::SetSSRC(unsigned long ssrc, int mediaType)
{
    m_ulSSRC[mediaType] = ssrc;
}
unsigned short CRtspMedia::getSequenceNum(int mediaType)
{
    if (++m_usSeq[mediaType] >= 65535)
    {
        m_usSeq[mediaType] = 0;
    }
    return m_usSeq[mediaType];
}

VD_BOOL CRtspMedia::SendPacket(CPacket *ptk, int mediaType)
{
    if(!ptk || m_bFistIFrame)
    {
        VD_TRACE("CRtspMediaVedio::SendPacket error,ptk is [%p %d]\n", ptk, m_bFistIFrame);
        return VD_FALSE;
    }
    
    if(m_iDataLen[mediaType] >= MAX_BUFFER_LEN)
    {
        ClearQueue(mediaType);
        return VD_FALSE;
    }
//test for exception,
/*    
    static int count = 0;
    if(count++ >= 1500)
    {
        ClearQueue(media);
        count = 0;
        return VD_FALSE;
    }
    VD_TRACE("count = %d\n",count);
*/
    CGuard guard(m_szMutex[mediaType]); //这句不要放到ClearQueue前
    m_RtpQueue[mediaType].push_back(ptk);
    m_iDataLen[mediaType] += ptk->GetLength();    
    return VD_TRUE;
}

//media是enum,具体指的是视频还是音频
void CRtspMedia::ClearQueue(int media)
{
    if(media < HanderVRtp|| media > HanderARtcp)
    {
        VD_TRACE("CRtspMediaVedio::ClearQueue error, media <%d>\n",media);
        return;
    }
    CGuard guard(m_szMutex[media]);
    
    int totalSize = m_RtpQueue[media].size();
    int index = 0;
    CPacket *ptk = NULL;
    while(index < totalSize)
    {
        ptk = m_RtpQueue[media].front();
        m_iDataLen[media] -= ptk->GetLength();
        ptk->Release();
        m_RtpQueue[media].pop_front();
        index++;
    }
    
    if(0 != m_iDataLen[media])
    {
        VD_TRACE("m_iDataLen != 0! m_iDataLen[%d] <%d>\n",media, m_iDataLen[media]);
        m_iDataLen[media] = 0;
    }
    m_bFistIFrame = true;
}

VD_BOOL CRtspMedia::SetInterleaved(eRtspHandler type,int value)
{
    if(type < HanderRtspNr && type >= HanderVRtp)
    {
        m_Interleaved[type] = value;
        return VD_TRUE;
    }
    return VD_FALSE;
}

void CRtspMedia::OnConfig(char *cmd, char *para)
{
}

VD_BOOL CRtspMedia::SetRtspModel(eRtpModle mode)
{
    if(mode>= RTP_OVER_TCP && mode <= RTP_OVER_MULTI_CAST)
    {
        m_rtpModel = mode;
        m_Magic = (mode == RTP_OVER_TCP) ? 4 : 0;
        return VD_TRUE;
    }
    return VD_FALSE;
}

eRtpModle CRtspMedia::GetRtspModel()
{
    return m_rtpModel;
}
VD_BOOL CRtspMedia::SetVedioTime(char *strTime)
{
    if(!strTime)
    {
        return VD_FALSE;
    }
    //sprintf
    sscanf(strTime, "%04d-%02d-%02d-%02d-%02d-%02d_%04d-%02d-%02d-%02d-%02d-%02d",
        &m_StartTime.year,
        &m_StartTime.month,
        &m_StartTime.day,
        &m_StartTime.hour,
        &m_StartTime.minute,
        &m_StartTime.second,

        &m_EndTime.year,
        &m_EndTime.month,
        &m_EndTime.day,
        &m_EndTime.hour,
        &m_EndTime.minute,
        &m_EndTime.second);
        
    return VD_TRUE;
}

int CRtspMedia::SetChannel(int Channel)
{
    if(Channel < 0 || Channel >= g_nLogicNum)
    {
        return -1;
    }
    m_iChannel = Channel;
    return m_iChannel;
}
int CRtspMedia::SetStreamType(int dwStreamType)
{
    if( dwStreamType < 0 || dwStreamType > EXTRA_STREAM_NUM)
    {
        return -1;
    }
    m_iStreamType = dwStreamType;
    return m_iStreamType;
}

VD_BOOL CRtspMedia::SetMediaType(eMediaType mediaType)
{
    if(mediaType >= MEDIA_VEDIO && mediaType <= MEDIA_DOWNLOAD)
    {
        m_MediaType = mediaType;
        return VD_TRUE;
    }
    VD_TRACE("SetMediaType error,mediaType<%d>",mediaType);
    return VD_FALSE;
}
eMediaType CRtspMedia::GetMediaType()
{
    return m_MediaType;
}

int CRtspMedia::parse_DQT( struct rtp_jpeg *out, char *d, int len )
{
    int i;
    for( i = 0; i < len; i += 65 )
    {
        if( ( d[i] & 0xF0 ) != 0 )
        {
            db_printf(0, "Unsupported quant table precision 0x%X!\n",
                    d[i] & 0xF0 );
            return -1;
        }
        out->quant[d[i] & 0xF] = (unsigned char *)(d + i + 1);
    }
    
    return 0;
}

int CRtspMedia::parse_SOF( struct rtp_jpeg *out, char *d, int len )
{
    int c;

    out->chroma_table = -1;
    if( d[0] != 8 )
    {
        db_printf(0,  "Invalid precision %d in SOF0\n", d[0] );
        return -1;
    }
    out->height = GET_16( d + 1 );
    out->width = GET_16( d + 3 );
    if( ( out->height & 0x7 ) || ( out->width & 0x7 ) )
    {
        db_printf(0,  "Width/height not divisible by 8!\n" );
        //return -1;
    }
    out->width >>= 3;
    out->height >>= 3;
    if( d[5] != 3 )
    {
        db_printf(0, "Number of components is %d, not 3!\n", d[5] );
        return -1;
    }

    #if 0
    out->type = 1;
    out->luma_table = 0;
    out->chroma_table = 1;
    #endif
    
   // 01 21 00 02 11 01 03 11 01
    
    /* Loop over the parameters for each component */
    for( c = 6; c < 6 + 3 * 3; c += 3 )
    {
        if( d[c + 2] >= 16 || ! out->quant[d[c + 2]] )
        {
            db_printf(0, "Component %d specified undefined quant table %d!\n", d[c], d[c + 2] );
            return -1;
        }
        switch( d[c] ) /* d[c] contains the component ID */
        {
        case 1: /* Y */
            if( d[c + 1] == 0x21 )
            {
                out->type = 0;
            }
            else if( d[c + 1] == 0x22 ) 
            {
                out->type = 1;
            }
            else
            {
                db_printf(0, "Invalid sampling factor 0x%02X in Y component!\n", d[c + 1] );
                return -1;
            }
            out->luma_table = d[c + 2];
            break;
        case 2: /* Cb */
        case 3: /* Cr */
            if( d[c + 1] != 0x11 )
            {
                db_printf(0, "Invalid sampling factor 0x%02X in %s component!\n", d[c + 1], d[c] == 2 ? "Cb" : "Cr" );
                return -1;
            }
            if( out->chroma_table < 0 )
                out->chroma_table = d[c + 2];
            else if( out->chroma_table != d[c + 2] )
            {
                db_printf(0, "Cb and Cr components do not share a quantization table!\n" );
                return -1;
            }
            break;
        default:
            db_printf(0,  "Invalid component %d in SOF!\n", d[c] );
            return -1;
        }
    }
    
    
    return 0;
}

int CRtspMedia::parse_DHT( struct rtp_jpeg *out, char *d, int len )
{
    /* We should verify that this coder uses the standard Huffman tables */
    out->dht = d-4;
    out->dht_len= len+4;
    return 0;
}

int CRtspMedia::jpeg_process_frame( struct frame *f, void *d )
{
    struct rtp_jpeg *out = (struct rtp_jpeg *)d;
    int i, blen;

    for( i = 0; i < 16; ++i ) out->quant[i] = NULL;

    for( i = 0; i < f->length; i += blen + 2 )
    {
        if( f->d[i] != 0xFF )
        {
            db_printf(0, "Found %02X at %d, expecting FF\n", f->d[i], i );
            out->scan_data_len = 0;
            return 0;
        }

        /* SOI (FF D8) is a bare marker with no length field */
        if( f->d[i + 1] == 0xD8 ) 
        {   
            blen = 0;
        }
        else 
        {
            blen = GET_16( f->d + i + 2 );
        }
        
      //  db_printf(0, "jpeg_process_frame   tab :%2x  len :%d \n", f->d[i + 1] , blen );
        switch( f->d[i + 1] )
        {
            case 0xDB: /* Quantization Table */
                if( parse_DQT( out, f->d + i + 4, blen - 2 ) < 0 )
                {
                    out->scan_data_len = 0;
                    return 0;
                }
            break;
            case 0xC0: /* Start of Frame */
                if( parse_SOF( out, f->d + i + 4, blen - 2 ) < 0 )
                {
                    out->scan_data_len = 0;
                    return 0;
                }
            break;
            case 0xC4: /* Huffman Table */
                //if( out->init_done ) break; /* only parse DHT once */
                if( parse_DHT( out, f->d + i + 4, blen - 2 ) < 0 )
                {
                    out->scan_data_len = 0;
                    return 0;
                }
            //    out->init_done = 1;
            break;
            case 0xDA: /* Start of Scan */
                out->scan_data =(unsigned char *) f->d + i + 2 + blen;
                out->scan_data_len = f->length - i - 2 - blen;
            return out->init_done;
        }
    }

//    db_printf(0, "Found no scan data!\n" );
    out->scan_data_len = 0;
    return 0;
}

void CRtspMedia::rtp_jpeg_init(void * _p)
{
    struct rtp_jpeg *p = (struct rtp_jpeg *)_p;
    p->init_done = 0;
    p->timestamp = 0;
}

void CRtspMedia::jpeg_packet_rtp(char *_pInputBuffer, int _iLength)
{
    int iIType = 1;
    struct frame JpegFrame;
//    media_t * pMedia =(media_t *)_pMedia;
//    struct rtp_jpeg *out = &pMedia->m_MVideo.m_RtpJepg;

    struct rtp_jpeg *out =  new struct rtp_jpeg;
    memset(out,0,sizeof(struct rtp_jpeg));

    int i, plen, vcnt, hdr_len;
    static char rtpbuffer[JPEG_MAX_PAYLOAD_LEN];
    rtp_packet_header_t * pRtpH = (rtp_packet_header_t *)rtpbuffer;

    char * vhdr = rtpbuffer+sizeof(rtp_packet_header_t);
    char * qhdr = vhdr+8;
    char * lqtable = qhdr+4;
    char * cqtable = lqtable+64;
    char * cData = cqtable+64;

    int iMaxSize = 0;
    int liv_TimeStep = 90000 *2;
    JpegFrame.d = _pInputBuffer;
    JpegFrame.length = _iLength;
//    JpegFrame.length = _iLength - 2;
    jpeg_process_frame(&JpegFrame,out);
    if(out->scan_data_len==0)
    {
        return;
    }

//    printf( "jpeg_packet_rtp _iLength:%d  scanflen:%d!\n" ,_iLength,out->scan_data_len);
    
    /* Main JPEG header */
    vhdr[0] = 0; /* type-specific, value 0 -> non-interlaced frame */
    /* vhdr[1..3] is the fragment offset */
    vhdr[4] = out->type; /* type */
    vhdr[5] = 255; /* Q, value 255 -> dynamic quant tables */
    vhdr[6] = out->width;
    vhdr[7] = out->height;


    /* Quantization table header */
    qhdr[0] = 0; /* MBZ */
    qhdr[1] = 0; /* precision */
    PUT_16( qhdr + 2, 2 * 64 ); /* length */

    /* Luma quant table */
    memcpy(lqtable,out->quant[out->luma_table],64);
    /* Chroma quant table */
    memcpy(cqtable,out->quant[out->chroma_table],64);
    
    hdr_len = 140 + sizeof(rtp_packet_header_t);

    /* scan_data_len大小的RTP包被分若干包发送，最后一个分包为实际大小 */
    for( i = 0; i < out->scan_data_len; i += plen )
    {
              iMaxSize = JPEG_MAX_PAYLOAD_LEN;

        /* plen为每个预发送rtp的大小*/
        plen = out->scan_data_len - i;
        
        if( plen > iMaxSize - hdr_len )
              {      
            plen = iMaxSize - hdr_len;
              }

            
        /* No hay PUT_24 macro... */
        vhdr[1] = i >> 16;
        vhdr[2] = ( i >> 8 ) & 0xff;
        vhdr[3] = i & 0xff;
        
            memcpy(rtpbuffer+hdr_len,out->scan_data + i,plen);

            pRtpH->version = 2;
            pRtpH->p = 0;
            pRtpH->x = 0;
            pRtpH->cc = 0;
            pRtpH->marker = 0;
            pRtpH->pt = RTP_PT_JPEG;

            VD_UINT16 seqno = ntohs((unsigned short)getSequenceNum());
            memcpy(rtpbuffer+2,&seqno,sizeof(seqno));
            
            VD_UINT32 ts = ntohl(m_ulRtpTimestamp[RtspVedio]);
            memcpy(rtpbuffer+4,&ts,sizeof(ts));
            
            VD_UINT32 SSRC = ntohl(m_ulSSRC[RtspVedio]);
            memcpy(rtpbuffer+8,&SSRC,sizeof(SSRC));

//        rtpsession_makeheader(pRtpH,&pMedia->m_MVideo.m_RtpVideo.m_RtpHeader,plen + i == out->scan_data_len);
        
//        pMedia->send_Rtpdata(pMedia,rtpbuffer,plen+hdr_len,0,iIType);
            if (plen == out->scan_data_len - i)
            {
                pRtpH->marker = 1;
            }

        CPacket* pPacket = g_PacketManager.GetPacket(UDP_PACKET_SIZE);
        if (pPacket)
        {    
                    memcpy(pPacket->GetBuffer(), rtpbuffer, hdr_len  + plen);
//                    printf("hdr_len[%d]  + plen[%d] = %d\r\n",hdr_len,plen,hdr_len + plen);
                    
                    //SET_MEDIA_TYPE(pPacket, RtspVedio);
                    pPacket->SetLength(hdr_len  + plen);
                    SendPacket(pPacket, HanderVRtp);
        }

        /* Done with all headers except main JPEG header */
        hdr_len = 8+sizeof(rtp_packet_header_t);
//        pMedia->m_MVideo.m_RtpVideo.m_RtpHeader.seqno++;
        iIType = 0;
    }
//    pMedia->m_MVideo.m_RtpVideo.m_RtpHeader.ts += liv_TimeStep;
        m_ulRtpTimestamp[RtspVedio]+=liv_TimeStep;        

    return ;
}


void CRtspMediaVedio::OnEncodeChange(CConfigEncode *pConfigEncode, int &ret)
{   
    int temp = 0;

    int iChannel = GetChannel();
    int dwStreamType = GetStreamType();

    /* 数字通道分辨率不在这里处理  */
    if (iChannel >= g_nCapture)
    {
        return;
    }

    CONFIG_ENCODE& szCfg = pConfigEncode->getConfig((iChannel>=0&&iChannel<g_nCapture)?iChannel:0);
    
    temp = (CHL_MAIN_T == dwStreamType ? szCfg.dstMainFmt[0].vfFormat.nFPS : szCfg.dstExtraFmt[dwStreamType-1].vfFormat.nFPS);
    m_iFPS = (temp <= 0 || temp > 25) ? m_iFPS : temp;
    temp = (CHL_MAIN_T == dwStreamType ? szCfg.dstMainFmt[0].vfFormat.iResolution : szCfg.dstExtraFmt[dwStreamType-1].vfFormat.iResolution);
    if(temp >= CAPTURE_SIZE_D1 && temp < CAPTURE_SIZE_NR && temp != m_Resolution)
    {
        m_Resolution = temp;
        //m_RtspSession->SetReleaseFlag(CRtspSession::RELEASE_NEED);
        
    }
    
    VD_TRACE("Encode change,m_iFPS <%d>, m_Resolution <%d>\n",m_iFPS,temp);
}

CRtspMediaVedio::CRtspMediaVedio(CRtspSession *rtspsession) : CRtspMedia(rtspsession)
{
   m_pCfgEncode = new CConfigEncode();
   m_pCfgEncode->update();
   m_pCfgEncode->attach(this,(TCONFIG_PROC)&CRtspMediaVedio::OnEncodeChange);
   GetResolution();
}

CRtspMediaVedio::~CRtspMediaVedio()
{
   StopMedia();
   m_pCfgEncode->detach(this,(TCONFIG_PROC)&CRtspMediaVedio::OnEncodeChange);
   VD_DELETE(m_pCfgEncode);
}

int CRtspMediaVedio::GetResolution()
{
    if(m_Resolution >= CAPTURE_SIZE_D1 && m_Resolution < CAPTURE_SIZE_NR)
    {
        return m_Resolution;
    }
    
    int iChannel = GetChannel();
    int dwStreamType = GetStreamType();

    /*数字通道组播时分辨率写成D1*/   
    if (iChannel >= g_nCapture &&iChannel < g_nLogicNum)
    {
        if(CHL_MAIN_T == dwStreamType)
        {
            m_Resolution = CAPTURE_SIZE_CIF;
        }
        else
        {
            m_Resolution = CAPTURE_SIZE_QCIF;
        }
        return m_Resolution;
    }
    
    m_pCfgEncode->update();
    CONFIG_ENCODE& szCfg = m_pCfgEncode->getConfig(iChannel);
    if(CHL_MAIN_T == dwStreamType)
    {
        if(!szCfg.dstMainFmt[0].bVideoEnable)
        {
            return CAPTURE_SIZE_NR; 
        }
        m_Resolution = szCfg.dstMainFmt[0].vfFormat.iResolution;
        m_iFPS =  szCfg.dstMainFmt[0].vfFormat.nFPS;
        m_bAudioOn = szCfg.dstMainFmt[0].bAudioEnable;
    }
    else
    {
        if(!szCfg.dstExtraFmt[dwStreamType-1].bVideoEnable)
        {
            return CAPTURE_SIZE_NR; 
        }
        m_Resolution = szCfg.dstExtraFmt[dwStreamType-1].vfFormat.iResolution;
        m_iFPS = szCfg.dstExtraFmt[dwStreamType-1].vfFormat.nFPS;
        m_bAudioOn = szCfg.dstExtraFmt[dwStreamType-1].bAudioEnable;
    }
    if (m_iFPS <= 0 || m_iFPS > 25)
    {
        m_iFPS = 25;
    }
    return m_Resolution;
}

bool CRtspMediaVedio::StartMedia()
{
    int iChannel = GetChannel();
    int dwStreamType = GetStreamType();
    m_iFPS = 25;
    printf("CRtspMediaVedio::StartMedia():dwStreamType = %d\r\n",dwStreamType);
    
    if (iChannel < 0 || iChannel >= g_nLogicNum || dwStreamType < 0 || dwStreamType > EXTRA_STREAM_NUM)
    {
        VD_TRACE("StartVideoCapture error, Channel <%d>, dwStreamType <%d>\n",iChannel,dwStreamType);
        return false;
    }

    if (true == m_bMonitor)
    {
        VD_TRACE("CRtspMediaVedio::StartMedia(): m_bMonitor = true !!! Channel <%d>, dwStreamType <%d>\n",iChannel,dwStreamType);
         return true;
   }    
//    dwStreamType = CHL_JPEG_T;

    if (CHL_JPEG_T == dwStreamType)
    {
        m_bFistIFrame = false;
    }
    
    //CDevCapture *pCapture = CDevCapture::instance(iChannel);
    ICapture *pCapture = ICapture::instance(iChannel);
    if (NULL != pCapture)
    {
        /*
        * 做强制I帧操作,以便在客户端能够马上看到图像
        */
        pCapture->SetIFrame(dwStreamType);
        VD_BOOL bRet = pCapture->Start(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CRtspMediaVedio::OnCapture, DATA_MONITOR,dwStreamType);
        if(bRet == VD_TRUE) 
        {
            VD_TRACE("StartVideoCapture :[Channel = %d],[dwStreamType = %d]\r\n",iChannel,dwStreamType);
            m_bMonitor = true;
            return true;
        }
    }
    else
    {
        VD_TRACE("GetCapture failed!!\n");
    }

    return false;
}

bool CRtspMediaVedio::StopMedia()
{
    if( 0 == m_iFPS || false == m_bMonitor )
    {
        VD_TRACE("Media has not been started or stopped!\n");
        return true;
    }
    int iChannel = GetChannel();
    int dwStreamType = GetStreamType();

    /* 没有开启成功时不进行停止，避免底层assert失败 */
    if (-1 == iChannel || -1 == dwStreamType)
    {
        tracepoint();
        return true;
    }
    //dwStreamType = CHL_JPEG_T;
    
    VD_BOOL bRet = VD_FALSE;
    //CDevCapture *pCapture = CDevCapture::instance(iChannel);
    ICapture *pCapture = ICapture::instance(iChannel);
    if (NULL != pCapture)
    {
        bRet = pCapture->Stop(this,(CDevCapture::SIG_DEV_CAP_BUFFER)&CRtspMedia::OnCapture, DATA_MONITOR,dwStreamType);
    }
    m_iFPS = 25;
    m_bMonitor = false;
    
    return bRet == VD_TRUE;
}

CRtspMediaRecord::CRtspMediaRecord(CRtspSession *rtspsession) : CRtspMedia(rtspsession)
{
     m_FrameParse = new CFrameGeneral();
     if(m_FrameParse && m_FrameParse->GetState() > 0)
     {
         m_DiskReader = new CDevHDiskReader();
         if(!m_DiskReader)
         {
             VD_TRACE("CRtspMediaRecord,new CDevHDiskReader error!\n");
         }
     }
}

CRtspMediaRecord::~CRtspMediaRecord()
{
   StopMedia();
   VD_DELETE(m_FrameParse);
   VD_DELETE(m_DiskReader);
}

#ifdef RTSP_RECORD_TEST

#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#define FRAME_GENERAL_BUFFER_LEN 1024*32

static int TestVideoFileExitFlag = 0;

void CRtspMediaRecord::TestVideoFile()
{
    CFrameGeneral test;
    FILE *fp = fopen("test.dav","r");
    
    CPacket *sPtk = g_PacketManager.GetPacket(FRAME_GENERAL_BUFFER_LEN);
    CPacket *dPtk = NULL;
    char *buf = (char *)sPtk->GetBuffer();
    
    
    int len = fread(buf,1,FRAME_GENERAL_BUFFER_LEN,fp);
    sPtk->SetLength(len);
    //VD_TRACE("Read len < %d>\n",len);
    
    time_t tTimeStamp = 0;
    time_t tCurTime = time(NULL);
    VD_BOOL bTransNeedSleep = VD_TRUE;

    m_Speed = SPEED_QUICK_T2;
    while(len > 0)
    {
        int pos = 0;
        test.AddPacket(sPtk);
        while(CFrameGeneral::IRET_SUCCESS == test.Parse(dPtk,pos,pos))
        {
            if(!dPtk)
            {
                break;
            }
            if(bTransNeedSleep)
            {
                SystemSleep(70);
            }
            
            PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)dPtk->GetHeader();
            uchar iFrameType = pPacketHead->FrameInfo[0].FrameType;
            
            //快放与慢放时，只发I帧
            if (g_PlaySpeed[m_Speed] > (double)1.0)     //快放
            {
                /*
                if(PACK_TYPE_FRAME_I == iFrameType)
                {    
                    tTimeStamp = GetTimeStamp(dPtk);
                    if(0 == m_RecTimeStamp)
                    {
                        m_RecTimeStamp = tTimeStamp;
                    }
                    
                    tCurTime = time(NULL);
                    if((tTimeStamp-m_RecTimeStamp) > 2*(tCurTime-m_CurrentTime))
                    {  
                        bTransNeedSleep = VD_TRUE;
                        VD_TRACE("SystemSleep 100 ms\n");
                    }
                    else
                    {
                        bTransNeedSleep = VD_FALSE;
                    }
                }
                
                else
                {
                    dPtk->Release();
                    dPtk = NULL;
                    continue;
                }
                */
                
            }
            else if (g_PlaySpeed[m_Speed] < (double)1.0)//慢放
            {
                //m_SleepTime
            }
            else if (g_PlaySpeed[m_Speed] == (double)1.0)//正常速度
            {
                if(PACK_TYPE_FRAME_I == iFrameType)
                {                       
                    tTimeStamp = GetTimeStamp(dPtk);
                    if(0 == m_RecTimeStamp)
                    {
                        m_RecTimeStamp = tTimeStamp;
                    }
                }
                tCurTime = time(NULL);
                if((tTimeStamp-m_RecTimeStamp) > (tCurTime-m_CurrentTime))
                {  
                    bTransNeedSleep = VD_TRUE;
                    //VD_TRACE("SystemSleep 100 ms\n");
                }
                else
                {
                    bTransNeedSleep = VD_FALSE;
                }
            }
            else
            {
                VD_TRACE("g_PlaySpeed[m_Speed] is error,%f\n",g_PlaySpeed[m_Speed]);
                dPtk->Release();
                dPtk = NULL;
                continue;
            }
            
            if(0 == TestVideoFileExitFlag)
            {
                dPtk->Release();
                dPtk = NULL;
				fclose(fp);
                return;
            }
            
            OnCapture(0,0,dPtk);
            dPtk->Release();
            dPtk = NULL;
        }        
        memset(buf,0,FRAME_GENERAL_BUFFER_LEN);
        len = fread(buf,1,FRAME_GENERAL_BUFFER_LEN,fp);
        sPtk->SetLength(len);
        //VD_TRACE("Read len < %d>\n",len);
    }
    fclose(fp);
    sPtk->Release();
    StopMedia();
    //m_RtspSession->SetReleaseFlag(CRtspSession::RELEASE_NEED);
    //assert(0);
}

void *test(void *para)
{
    if(!para)
    {
        VD_TRACE("ThreadBody test,para is NULL\n!");
    }
    CRtspMediaRecord *record = (CRtspMediaRecord *)para;
    record->TestVideoFile();
}
#endif

uint CRtspMediaRecord::GetTimeStamp(CPacket *ptk)
{
    uint tmpTime =  *((uint *)(ptk->GetBuffer() + 8));
    uint iTime = 0;
    SYSTEM_TIME st;

    st.year = tmpTime>>26&0x3f;
    st.month = tmpTime>>22&0xf;
    st.day = tmpTime>>17&0x1f;
    st.hour = tmpTime>>12&0x1f;
    st.minute = tmpTime>>6&0x3f;
    st.second = tmpTime&0x3f;

    iTime = st.hour*60*60 + st.minute*60 + st.second;

    return iTime;
}

bool CRtspMediaRecord::StartMedia()
{
    m_Speed = SPEED_NORMAL;
    m_BeginTime = VD_GetMSCount();
    m_RecTimeOff = 0;
    m_iFPS = 25;
    m_SleepTime = 1000/m_iFPS - 10;

#ifdef RTSP_RECORD_TEST
    if(1 == TestVideoFileExitFlag)
    {
        return false;
    }
    TestVideoFileExitFlag = 1;
    pthread_t thread;
    if((pthread_create(&thread, NULL, test, this)) != 0)
    {
        VD_TRACE("线程1创建失败!\n");
    }
    else
    {
        VD_TRACE("线程1创建成功!\n");
    }                
    return true;
#endif
    

    int iChannel = GetChannel();
    if(m_FrameParse)
    {
        m_FrameParse->Init();
    }
    VD_BOOL bRet = VD_FALSE;
    if(m_DiskReader)
    {
        m_DiskReader->NewLocate(iChannel, &m_StartTime, &m_EndTime);
    #ifdef YUANYANG
        bRet = m_DiskReader->Start(this,
                 (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnRecordYY, iChannel);
    #else
        bRet = m_DiskReader->Start(this,
                 (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnRecord, iChannel);
    #endif
    }
    return bRet == VD_TRUE ? true : false;
}

bool CRtspMediaRecord::StopMedia()
{
#ifdef RTSP_RECORD_TEST
    TestVideoFileExitFlag = 0;
    return true;
#endif

    if(0 == m_iFPS)
    {
        //VD_TRACE("Media has not been started or stopped!\n");
        return true;
    }
    VD_BOOL bRet = VD_FALSE;
    if(m_DiskReader)
    {
    #ifdef YUANYANG
        bRet = m_DiskReader->Stop(this,
                     (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnRecordYY) ;
    #else
        bRet = m_DiskReader->Stop(this,
                     (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnRecord) ;
    #endif
    }    
    m_iFPS = 25;
    return bRet == VD_TRUE ? true : false;
}
int CRtspMediaRecord::GetResolution()
{
    //m_Resolution = CAPTURE_SIZE_D1;
    //return m_Resolution;
    if(m_Resolution >= CAPTURE_SIZE_D1 && m_Resolution < CAPTURE_SIZE_NR)
    {
        return m_Resolution;
    } 
    int iChannel = GetChannel();
    
//for test
/*
    m_StartTime.year = 2010;
    m_StartTime.month = 5;
    m_StartTime.day = 17;
    m_StartTime.hour = 10;
    m_StartTime.minute = 18;
    m_StartTime.second = 49;

    m_EndTime.year = 2010;
    m_EndTime.month = 5;
    m_EndTime.day = 17;
    m_EndTime.hour = 10;
    m_EndTime.minute = 24;
    m_EndTime.second = 12;
*/
    VD_TRACE("\n%04d-%02d-%02d-%02d-%02d-%02d_%04d-%02d-%02d-%02d-%02d-%02d\n",
        m_StartTime.year,
        m_StartTime.month,
        m_StartTime.day,
        m_StartTime.hour,
        m_StartTime.minute,
        m_StartTime.second,

        m_EndTime.year,
        m_EndTime.month,
        m_EndTime.day,
        m_EndTime.hour,
        m_EndTime.minute,
        m_EndTime.second);

    if(m_DiskReader)
    {
        m_DiskReader->NewLocate(iChannel, &m_StartTime, &m_EndTime);
        m_DiskReader->Start(this,
            (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnReSolution, iChannel);
        int iGetResolutionWaitCount = 2;
        while(iGetResolutionWaitCount-- > 0)
        {
            if(m_iFPS > 1 && m_iFPS <= 25 && m_bAudioOn)
            {
                break;
            }
            SystemSleep(100);
        }
        m_DiskReader->Stop(this,
            (CDevHDiskReader::SIG_DEV_HDISK_READER)&CRtspMediaRecord::OnReSolution) ;
    }
    return m_Resolution;
}
void CRtspMediaRecord::OnRecordYY(int iCMD, CPacket *pPacket, int iCh)
{
    if(!pPacket || !m_FrameParse)
    {
        StopMedia();
        m_RtspSession->SetReleaseFlag(CRtspSession::RELEASE_MEDIA_MSG);
        VD_TRACE("CRtspMediaRecord::OnReSolution,pPacket (or m_FrameParse) is null, Maybe encounter feof(GetPacket error)!\n");
        return ;
    }
    pPacket->AddRef();
    
    //Add the PGSP Stream Process..............................
    
    pPacket->Release();
}

void CRtspMediaRecord::OnRecord(int iCMD, CPacket *pPacket, int iCh)
{
    if(!pPacket || !m_FrameParse)
    {
        StopMedia();
        m_RtspSession->SetReleaseFlag(CRtspSession::RELEASE_MEDIA_MSG);
        VD_TRACE("CRtspMediaRecord::OnReSolution,pPacket (or m_FrameParse) is null, Maybe encounter feof(GetPacket error)!\n");
        return ;
    }
    pPacket->AddRef();
    
    m_FrameParse->AddPacket(pPacket);
    int pos = 0;
    CPacket *dPtk = NULL;
    while(CFrameGeneral::IRET_SUCCESS == m_FrameParse->Parse(dPtk ,pos,pos))
    {
        if(!dPtk)
        {
            break;
        }
        uint SystemGetMSCount(void);
        PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)dPtk->GetHeader();
        uchar iFrameType = pPacketHead->FrameInfo[0].FrameType;

        if(PACK_TYPE_FRAME_I == iFrameType)
        {                               
            uchar *ptr =  dPtk->GetBuffer();
    
            if(ptr[5] > 1 && ptr[5] <= 25)
            {
                m_iFPS = ptr[5];
                VD_TRACE("FPS <%d>, m_Speed <%d>, m_SleepTime <%d>\n", m_iFPS, m_Speed, m_SleepTime);
            }
        }
        
        static CDFile fp;
        if (!fp.IsValid())
        {
            fp.Open("v.dav", "w");
        }
        fp.Write((unsigned char *)dPtk->GetBuffer(), dPtk->GetLength());
        
        PrcocessVideo(0,dPtk,0);
        dPtk->Release();
        dPtk = NULL;
        
        if(!m_bFistIFrame && PACK_TYPE_FRAME_AUDIO != iFrameType)
        {
            m_RecTimeOff += 1000/m_iFPS;
            uint curTime = VD_GetMSCount();
            m_SleepTime = m_RecTimeOff - (uint)(g_PlaySpeed[m_Speed]*(curTime-m_BeginTime)) - 10;
            if(m_SleepTime >= 0)
            {  
                SystemSleep(m_SleepTime);  
            }
            /*
            VD_TRACE("m_RecTimeOff<%d>, -----<%d>, m_SleepTime<%d>\n",
                m_RecTimeOff, (uint)(g_PlaySpeed[m_Speed]*(curTime-m_BeginTime)), m_SleepTime);
                */
        }
    }
    pPacket->Release();
}

//还需要修放
void CRtspMediaRecord::OnReSolution(int iCMD, CPacket *pPacket, int iCh)
{
    int index = 0;
    int pos = 0;
    CPacket *dPtk = NULL;
    if(!pPacket || !m_FrameParse)
    {
        m_RtspSession->SetReleaseFlag(CRtspSession::RELEASE_MEDIA_MSG);
        VD_TRACE("CRtspMediaRecord::OnReSolution,pPacket or m_FrameParse is null!\n");
        return ;
    }
    
    //VDDumpHex((const char * )pPacket->GetBuffer(),16);
    pPacket->AddRef();
    m_FrameParse->AddPacket(pPacket);
    while(CFrameGeneral::IRET_SUCCESS == m_FrameParse->Parse(dPtk,pos,pos))
    {
        if(dPtk)
        {
            //VDDumpHex((const char * )dPtk->GetBuffer(),16);
            
            PKT_HEAD_INFO *dPktHeader = (PKT_HEAD_INFO *)dPtk->GetHeader();
            if(PACK_TYPE_FRAME_I == dPktHeader->FrameInfo[0].FrameType)
            {
                uchar *ptr =  dPtk->GetBuffer();
                
                //获取视频帧率
                if(ptr[5] > 1 && ptr[5] <=25)
                {
                    m_iFPS = ptr[5];
                    VD_TRACE("FPS <%d>\n", m_iFPS);
                }
                else
                {
                    VD_TRACE("FPS error <%d>,use default <%d>\n", ptr[5],25);
                    m_iFPS = 25;
                }
                
                //获取视频分辨率
                if(0x58 == ptr[6] && 0x48 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_D1;
                }
                else if(0x2C == ptr[6] && 0x48 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_HD1;
                }
                else if(0x2C == ptr[6] && 0x24 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_CIF;
                }
                else  if(0x16 == ptr[6] && 0x12 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_QCIF;
                }
                else  if(0xA0 == ptr[6] && 0x5A == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_720P;
                }
                else if(0xA0 == ptr[6] && 0x80 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_SXGA;
                }
                else if(0x80 == ptr[6] && 0x60 == ptr[7])
                {
                    m_Resolution = CAPTURE_SIZE_XVGA;
                }
                else
                {
                    VD_TRACE("Not surported Vedio resolution,width<0x%x>, high<0x%x>\n", ptr[6], ptr[7]);
                }
                
                if (m_bAudioOn || index >= 3)
                {
                    dPtk->Release();
                    break;
                }
                index++;
            }
            else if (PACK_TYPE_FRAME_AUDIO == dPktHeader->FrameInfo[0].FrameType)
            {
                m_bAudioOn = VD_TRUE;
            }
            dPtk->Release();
        }
    }  
    pPacket->Release();
}

VD_BOOL CRtspMediaRecord::SetSpeed(ePlaySpeed speed)
{
    if(speed <= SPEED_QUICK_T4 && speed >= SPEED_SLOW_T4)
    {
        m_SleepTime = (int)((1000/m_iFPS) / g_PlaySpeed[speed] - 10);
        //快慢放只发I帧
        if(speed != SPEED_NORMAL)
        {
            //m_SleepTime *= (m_SleepTime+10)*m_iFPS - 10;
            VD_TRACE("Not SPEED_NORMAL,Send IFrame only!\n");
        }
        VD_TRACE("Set Speed <%d %f> m_SleepTime <%d>\n",speed, g_PlaySpeed[speed], m_SleepTime);
    }
    else
    {
        VD_TRACE("Speed not surport <%f>\n",g_PlaySpeed[speed]);
    }
    return VD_TRUE;
}
void CRtspMediaRecord::OnConfig(char *cmd, char *para)
{
    if(!cmd || !para)
    {
        return ;
    }
    if(!strncmp(cmd, "speed",strlen("speed")))
    {
        int speed = atoi(para);
        
        printf("%d \n", speed);
        
        if(speed > 0)
        {
            for(int iSpeed = SPEED_NORMAL; iSpeed < SPEED_NR; iSpeed++)
            {
                if(speed == g_PlaySpeed[iSpeed])
                {
                    m_Speed = iSpeed;
                    SetSpeed((ePlaySpeed)m_Speed);    
                    break;
                }
            }
        }
        else
        {
            if(-2 == speed)
            {
                m_Speed = SPEED_SLOW_T2;
                SetSpeed((ePlaySpeed)m_Speed);    
            }
            else if(-4 == speed)
            {
                m_Speed = SPEED_SLOW_T4;
                SetSpeed((ePlaySpeed)m_Speed);    
            }
        }
    }
    else if(!strncmp(cmd, "speed",strlen("speed")))
    {
    }
}

/* 组播 */
CRtspMediaVedioMulti::CRtspMediaVedioMulti(CRtspSession *rtspsession) :CRtspMediaVedio(rtspsession)
{
    m_iSocket = -1;
    memset(m_acPeerIp,0,sizeof(m_acPeerIp));
    m_usRemotePort = 0;
    m_usLocalPort = 0;
}

CRtspMediaVedioMulti::~CRtspMediaVedioMulti() 
{
    if (m_iSocket > 1)
    {        
       ez_close(m_iSocket);
       m_iSocket = -1;
    }
    
    g_SMPortPool.ReleasePortPair(m_usLocalPort);
}

VD_BOOL CRtspMediaVedioMulti::GetLocalPort(VD_UINT16 &usLocalPort)
{
    usLocalPort = m_usLocalPort;
    
    if (0 == m_usLocalPort)
    {
        return VD_FALSE;
    }

    return VD_TRUE;
}


VD_VOID CRtspMediaVedioMulti::SetLocalPort(VD_UINT16 usLocalPort)
{
    m_usLocalPort = usLocalPort;
}

VD_BOOL CRtspMediaVedioMulti::CreateUdp()
{
    char pIp[16] = {0};
    char pMask[16] = {0};

    NetGetHostIP("eth0",pIp,sizeof(pIp), pMask,sizeof(pMask));
    m_iSocket = VDCreateUdpSocket(pIp,m_usLocalPort);

    IPADDR stIp;
    stIp.l = CConfigNetRtspSet::getLatest().stMulticastIp.l;
    
    sprintf(m_acPeerIp,"%d.%d.%d.%d",stIp.c[0],stIp.c[1],stIp.c[2],stIp.c[3]);

    m_usRemotePort = CConfigNetRtspSet::getLatest().usPort + m_iChannel*4+m_iStreamType*2;
    
    printf ("###########CRtspMediaVedioMulti::CreateUdp():\r\nm_usRemotePort = %d stIp.l = %d m_acPeerIp = %s\r\n",m_usRemotePort,stIp.l,m_acPeerIp);
    
    return VD_TRUE;
}

VD_VOID CRtspMediaVedioMulti::PrcocessMultiVideo(int iCh, CPacket *pPacket,uint iCodeType)
{
#define MULTICAST_PACKET_SIZE 1300

    uchar *  pData  = pPacket->GetBuffer();
    int iLen = pPacket->GetLength();
    
    int iLeftLen = iLen;
    int iSendLen = 0;
    
    while (iLeftLen > 0)
    {
        CPacket *pSendPacket = g_PacketManager.GetPacket(MULTICAST_PACKET_SIZE+RTPHDR_SIZE);

        if (NULL == pSendPacket)
        {
            return;
        }

        char *pRtpBuf = (char *)(pSendPacket->GetBuffer());
        memset(pRtpBuf,0,MULTICAST_PACKET_SIZE+RTPHDR_SIZE);
        
        RTPHDR* pszHdr = (RTPHDR* )(pRtpBuf ); 
        memset((char *)pszHdr, 0, RTPHDR_SIZE);
        pszHdr->version = 2;
        pszHdr->payloadtype = 96;
        
        pszHdr->marker = 1;
        pszHdr->seqnum = ntohs((unsigned short)getSequenceNum(RtspVedio));

        m_ulRtpTimestamp[RtspVedio]+=1000;        
        pszHdr->timestamp = ntohl(m_ulRtpTimestamp[RtspVedio]);
        pszHdr->ssrc = ntohl(m_ulSSRC[RtspVedio]);

        if (iLeftLen > MULTICAST_PACKET_SIZE )
        {
            iSendLen = MULTICAST_PACKET_SIZE;
        }
        else
        {
            iSendLen = iLeftLen;
        }

        memcpy(pRtpBuf + RTPHDR_SIZE, pData + iLen -iLeftLen, iSendLen);
        iLeftLen -= iSendLen;
        
        int iSendLenth = VDSocketUdpSend(m_iSocket, 
            pRtpBuf,
            iSendLen+RTPHDR_SIZE,
            m_acPeerIp,
            m_usRemotePort);
         
        if (iSendLenth != iSendLen + (int)RTPHDR_SIZE)
        {
            printf("iSendLenth = %d iSendLen = %d\r\n",iSendLenth,iSendLen);
        }

        pSendPacket->Release();
    }
}
    
PATTERN_SINGLETON_IMPLEMENT(CRtspMediaVedioMultiManager);

CRtspMediaVedioMultiManager::CRtspMediaVedioMultiManager()
{
    memset(m_apRtspMedia,0,sizeof(m_apRtspMedia));
    memset(m_iRefNum,0,sizeof(m_iRefNum));
}

CRtspMediaVedioMultiManager::~CRtspMediaVedioMultiManager()
{

}

/* 入参: ich 通道 iFlag 0 主码流 1 副码流 */
CRtspMediaVedioMulti *CRtspMediaVedioMultiManager::GetRtspMedia(CRtspSession *rtspsession,VD_INT32 iCh,VD_INT32 iFlag)
{
    if (iCh < 0 || iCh>=g_nLogicNum)
    {
        return NULL;
    }
    
    if (iFlag < 0 || iFlag > 1 )
    {
        return NULL;
    }

    CGuard guard(m_Mutex);
    
    if (m_apRtspMedia[iCh+g_nLogicNum*iFlag])
    {
        m_iRefNum[iCh+g_nLogicNum*iFlag]++;

        printf("CRtspMediaVedioMultiManager::GetRtspMedia():111m_iRefNum[%d] = %d pMedia = %p\r\n",
        iCh+g_nLogicNum*iFlag,m_iRefNum[iCh+g_nLogicNum*iFlag],m_apRtspMedia[iCh+g_nLogicNum*iFlag]);

        return m_apRtspMedia[iCh+g_nLogicNum*iFlag];
    }

    CRtspMediaVedioMulti *pMedia = new CRtspMediaVedioMulti(rtspsession);
    
    if (NULL == pMedia)
    {
        return NULL;
    }
    
    pMedia->SetMediaType(MEDIA_VEDIO);
    pMedia->SetStreamType(iFlag);
    pMedia->SetChannel(iCh);
    pMedia->SetRtspModel(RTP_OVER_MULTI_CAST);

    m_apRtspMedia[iCh+g_nLogicNum*iFlag] = pMedia;
    m_iRefNum[iCh+g_nLogicNum*iFlag]++;
    
    printf("CRtspMediaVedioMultiManager::GetRtspMedia():222m_iRefNum[%d] = %d pMedia = %p\r\n",
            iCh+g_nLogicNum*iFlag,m_iRefNum[iCh+g_nLogicNum*iFlag],pMedia);
    return m_apRtspMedia[iCh+g_nLogicNum*iFlag];
}

VD_BOOL CRtspMediaVedioMultiManager::ReleaseRtspMedia(CRtspMediaVedioMulti * pRtspMediaVedio)
{
    if (NULL == pRtspMediaVedio)
    {
        return VD_FALSE;
    }

    printf("CRtspMediaVedioMultiManager::ReleaseRtspMedia():pRtspMediaVedio = %p\r\n",pRtspMediaVedio);

    CGuard guard(m_Mutex);

    for (VD_INT32 i=0;i<N_SYS_CH*2;i++)
    {
        if (m_apRtspMedia[i] == pRtspMediaVedio)
        {
  //          printf("CRtspMediaVedioMultiManager::ReleaseRtspMedia():111m_iRefNum[%d] = %d\r\n",i,m_iRefNum[i]);
            
            m_iRefNum[i]--;
            
            if (m_iRefNum[i]<=0)
            {
                printf("CRtspMediaVedioMultiManager::ReleaseRtspMedia():222m_iRefNum[%d] = %d\r\n",i,m_iRefNum[i]);
                printf("========&&&&&&&&&&======\n");

                delete m_apRtspMedia[i];
                m_apRtspMedia[i] = NULL;
                m_iRefNum[i] = 0;
            }

            return VD_TRUE;
        }
    }
    
    return VD_FALSE;
}

