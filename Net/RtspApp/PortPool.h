#pragma once

#include <vector>

#include "System/Object.h"

///< Port pair  in rtsp setup message 
typedef std::pair<unsigned short, unsigned short> PortPair;

class CPortPool:public CObject
{
public:
	PATTERN_SINGLETON_DECLARE(CPortPool);
protected:
    ///< Port pool element;the first elem stands for the used flag
    typedef std::pair<bool, PortPair> PortElem;
    typedef std::vector<PortElem>     PortPool;

public:

    /**
    * @brief
    * Init the port pool with a init start port
    * 
    * @param [in] firstPort The first port in the pool
    * @param [in] MaxPairNum How many port pair it can provide
    *
    * @remarks
    * Inner port validity is not checked. 
    */
    void InitPortPool(unsigned short firstPort, int MaxPairNum);

    /**
    * @brief
    * Get an unused port pair and pass out
    * 
    * @param [out] pair The first unused port pair in the pool
    *
    * @returns
    * -true On success,and the port is passed out
    * -false No port pair is available now
    */
    bool GetPortPair(PortPair& pair);

    /** 
    * Release port pair;If not exist,return false
    */
    bool ReleasePortPair(PortPair pair);

    /**
    * Release port pair overloads with [first, first+1];
    */
    bool ReleasePortPair(unsigned short first);

private:
	CPortPool();
	virtual ~CPortPool();

    PortPool   m_pool;     
};

#define g_SMPortPool (*CPortPool::instance())


