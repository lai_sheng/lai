/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              TPAckManager.h
**Version:	              Version 0.01
**Author:	              
**Created:                
**Modified:               yang_shukui,2010-4-28   9:29
**Modify Reason:         
**Description:            重新整理rtsp部分的代码时添加，主要是提供TPlayer异步缓冲的管理，提高代码的共用性
*********************************************************************/

#ifndef __TPLAYER_ACK_MANAGER_H__
#define __TPLAYER_ACK_MANAGER_H__

#include "BaseFunc/HashTable.h"
#include "BaseFunc/VDDelete.h"
#include "BaseFunc/VDDebug.h"

template <class T> class CTPAckManager
{
public:
	#define MAX_CONN_NUM 4
	#define MAX_TNUM     128
	
public:
    CTPAckManager()
    {
		m_ConnId = NULL;
		SetPara((1<<MAX_CONN_NUM), MAX_TNUM);
		m_HashSvrAck = HashTable::create(ONE_WORD_HASH_KEYS);
		VD_ASSERT(m_HashSvrAck);
        Clear();
    }
	
	~CTPAckManager()
	{
	    Clear();
		//zsliu add
		if(m_ConnId)
		{
		    VD_DELETE_ARRAY(m_ConnId);
			m_ConnId = NULL;
		}
		if(m_HashSvrAck)		//zsliu add
		{
			VD_DELETE(m_HashSvrAck);
		}
	}
	
	int SetPara(int connIdNum, int TNum)
	{
	    int iRet = -1;
	    if(connIdNum > 0 && connIdNum <= (1<<MAX_CONN_NUM) && TNum > 0 && TNum <= MAX_TNUM)
	    {
	        m_ConnIdNum = connIdNum;
		    m_TNum = TNum;
			iRet = 0;
	    }
		if(m_ConnId)
		{
		    Clear();
		    VD_DELETE_ARRAY(m_ConnId);
			m_ConnId = NULL;
		}
			
	    m_ConnId = new int[m_ConnIdNum];
		for(int i = 0; i < m_ConnIdNum; i++)
		{
		    m_ConnId[i] = -1;
		}
		
		return iRet;
	}
	
	int GetPara(int &connIdNum, int &TNum)
	{
	    connIdNum = m_ConnIdNum;
		TNum = m_TNum;
		return 0;
	}
	
	int Add(int connId,int seq, T* Node)
	{
		unsigned key = GetKey(connId,seq);
	    m_HashSvrAck->Add((char const*)key,(void*)Node);
		return 0;
	}
	int Delete(int connId,int seq)
	{
	    unsigned key = GetKey(connId,seq);
		if(-1 == (int)key)
		{
		    return -1;
		}
		
		T* pHashNode = (T*) m_HashSvrAck->Lookup((char const*)key);
		VD_DELETE(pHashNode);
		
		VD_BOOL bRet = m_HashSvrAck->Remove((char const*)key);
		return bRet == VD_TRUE ? 0 : -1 ;
	}
	
	void Clear(void)
	{
	    T* pHashNode = (T*) (m_HashSvrAck->RemoveNext());
		while(NULL != pHashNode)
		{
		    VD_DELETE(pHashNode);
			pHashNode = (T*) (m_HashSvrAck->RemoveNext());
		}
	}
private:
	int AddIndex(int connId)
	{
	    for(int i = 0; i <m_ConnIdNum; i++)
	    {
	        if(-1 == m_ConnId[i])
	        {
	            m_ConnId[i] = connId;
	            return i;
	        }
	    }
		return -1;
	}
	int GetIndex(int connId)
	{
	    for(int i = 0; i <m_ConnIdNum; i++)
	    {
	        if(connId == m_ConnId[i])
	        {
	            return i;
	        }
	    }
		return -1;
	}

	//此处的seq的值是应用传来的，已经保证了不大于65535
	unsigned GetKey(int connId,int seq)
	{
	    unsigned key = 0;
		int index = GetIndex(connId);

		if(-1 == index)
		{
		    index =  AddIndex(connId);
		    if(-1 == index)
		    {
		        return -1;
		    }
		}
		
		key += ((index<<(31-MAX_CONN_NUM)) + seq);
		return key;
	}
	
private:
	//MapTPSvrAck m_MapSvrAck;
	HashTable* m_HashSvrAck;
	int *m_ConnId;
	
	int m_ConnIdNum;//缓冲连接的最大个数
	int m_TNum;     //缓冲T的最大个数
};

#endif

