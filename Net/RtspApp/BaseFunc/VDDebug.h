/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              VDDebug.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-4-27   9:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/
#ifndef __VD_DEBUG_H__
#define __VD_DEBUG_H__

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#define __VD_DEBUG__               1
#define __VD_ASSERT__              1

extern unsigned int g_VDDebugInfo;

extern void VDDumpHex(const char *pData,int len);

#if defined(_MSC_VER) || defined(_WINDOWS_) || defined(WIN32)
#define VD_TRACE printf
#else
#define VD_TRACE(fmt, args ... )	fprintf(stdout, fmt, ## args)
#endif

#ifdef __VD_ASSERT__
    #define VD_ASSERT(x) assert(x)
#else
    #define VD_ASSERT(x)
#endif

#ifdef __VD_DEBUG__
	#define VD_DEBUGINFO(x)   if ((g_VDDebugInfo >> 0) & 0x1) {x; } else { ; }        /* 普通打印信息开关 */
	#define VD_DEBUGALARM(x)  if ((g_VDDebugInfo >> 1) & 0x1) {x; } else { ; }        /* 告警调试 */
	#define VD_DEBUGREQ(x)    if ((g_VDDebugInfo >> 2) & 0x1) {x; } else { ; }        /* 命令请求调试开关 */
	#define VD_DEBUGVSP(x)    if ((g_VDDebugInfo >> 3) & 0x1) {x; } else { ; }        /* 视频调试开关 */
	#define VD_DEBUGRH(x)     if ((g_VDDebugInfo >> 4) & 0x1) {x; } else { ; }        /* 注册保活调试开关 */
	#define VD_DEBUGTHR(x)    if ((g_VDDebugInfo >> 5) & 0x1) {x; } else { ; }        /* 检测线程是否活动 */
	#define VD_DEBUGDATA(x)   if ((g_VDDebugInfo >> 6) & 0x1) {x; } else { ; }        /* 发送数据信息 */
	#define VD_DEBUGVASP(x)   if ((g_VDDebugInfo >> 7) & 0x1) {x; } else { ; }        /* 音频调试开关 */
	#define VD_DEBUGBIT(x)    if ((g_VDDebugInfo >> 8) & 0x1) {x; } else { ; }        /* 码流信息 */
	#define VD_DEBUGERR(x)    if ((g_VDDebugInfo >> 0) & 0x1) {x; __trip;} else { ; } /* 错误信息调试开关 */

	//#define VD_PRINTF(fmt, args ... )	fprintf(stderr, fmt, ## args)
#else
	#define VD_DEBUGINFO(x)   if ((g_VDDebugInfo >> 0) & 0x1) {x; } else { ; } 
	#define VD_DEBUGALARM(x)  if ((g_VDDebugInfo >> 1) & 0x1) {x; } else { ; }
	#define VD_DEBUGREQ(x)    if ((g_VDDebugInfo >> 2) & 0x1) {x; } else { ; } 
	#define VD_DEBUGVSP(x)    if ((g_VDDebugInfo >> 3) & 0x1) {x; } else { ; } 
	#define VD_DEBUGRH(x)     if ((g_VDDebugInfo >> 4) & 0x1) {x; } else { ; } 
	#define VD_DEBUGTHR(x)    if ((g_VDDebugInfo >> 5) & 0x1) {x; } else { ; }
	#define VD_DEBUGDATA(x)   if ((g_VDDebugInfo >> 6) & 0x1) {x; } else { ; }
	#define VD_DEBUGVASP(x)   if ((g_VDDebugInfo >> 7) & 0x1) {x; } else { ; }
	#define VD_DEBUGBIT(x)    if ((g_VDDebugInfo >> 8) & 0x1) {x; } else { ; }
	#define VD_DEBUGERR(x)    if ((g_VDDebugInfo >> 0) & 0x1) {x; __trip;} else { ; }

	#define VD_PRINTF(fmt, args ... )
#endif

class CDFile
{
public:
	  #define CDFILE_FILE_NAME_LEN 128
	  
public:
      CDFile()
	  {
	       m_Fp = NULL;
		   m_Valid = 0;
	  }
	  CDFile(char *fileName,char *mode)
	  {
	      m_Fp = NULL;
	  	  Open(fileName,mode);
	  }
	  ~CDFile()
	  {
	  	  Close();
	  }
	  int Open(char *fileName,char *mode)
	  {
	  	  m_Valid = 1;
	  	  
	  	  if(fileName && strlen(fileName) < CDFILE_FILE_NAME_LEN - 1)
	  	  {
	  	  	  strcpy(m_FileName, fileName);
	  	  }
	  	  else
	  	  {
	  	  	  time_t iTime = time(NULL);
	  	  	  memset(m_FileName, 0, CDFILE_FILE_NAME_LEN);
	  	  	  sprintf(m_FileName,"/home/%d.jpg",(int)iTime);
	  	  }
	  	  if(!m_Fp)
	  	  {
	  	      if(NULL == (m_Fp = fopen(m_FileName,mode)))
	  	      {
	  	  	      m_Valid = 0;
	  	      }
			  else
		      {    
		          printf("open file <%s>\n",m_FileName);
		      }
	  	  }
		  
		  return 0;
	  }	
	  int Close()
	  {
	      if(m_Fp)
	  	  {
	  	  	  fclose(m_Fp);
			  m_Fp = NULL;
	  	  }
		  return 0;
	  }
	  int Write(unsigned char *buf, int len)
	  {
	  	  return (buf && len > 0 && IsValid()) ? fwrite((char *)buf, 1, len, m_Fp) : 0;
	  }
	  int IsValid()
	  {
	  	  return m_Valid;
	  }
	  
	  void Flush()
	  {
	      fflush(m_Fp);
	  }
	  
private:
	  FILE* m_Fp;
	  char m_FileName[CDFILE_FILE_NAME_LEN];
	  int m_Valid;
};

#endif

