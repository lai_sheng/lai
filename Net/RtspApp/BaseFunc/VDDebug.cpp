/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              VDDebug.cpp
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-4-27   9:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/
#include "VDDebug.h"

unsigned int g_VDDebugInfo = 0xffffffff;//默认调试信息全部打开

void VDDumpHex(const char *pData,int len)
{
	if(!pData)
	{
		return ;
	}

	int count = 0;
	char *ptr = (char *)pData;

	printf("%06d) ", count);
	for (int i = 0; i < len; i++) 
	{
		printf("%02x ", (unsigned char)*ptr++);
		if((i+1)%16 == 0)
		{	   
			printf("\n");
			if((len-1) != i)
			{
				printf("%06d) ", ++count);
			}
		}
		else if((i+1)%8 == 0)
		{	   
			printf("   ");
		}
	}
	printf("\n\n");
}


