/*!*******************************************************************
**                  VD Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              VDDelet.h
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-4-27   9:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/
#ifndef __VD_DELETE_H__
#define __VD_DELETE_H__

#define VD_DELETE(x) \
	if(x)\
	{\
		delete x;\
		x = NULL;\
	}

#define VD_DELETE_ARRAY(x) \
	if(x)\
	{\
		delete [] x;\
		x = NULL;\
	}
#endif


