/*
* Copyright (c) 2009, visiondigi
* All rights reserved.
*
* 文件名称：DH_HTTP.h
* 文件标识：参见配置管理计划书
* 摘　　要：对vd_http的继承，实现vd_http 中没有实现的rtsp一些功能
*
* 当前版本：1.0
* 作　　者：zdx
* 完成日期：2009年12月23日
*
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "RtspHttp.h"

#if _MSC_VER >= 1400   //VC8
#pragma warning(disable:4996)
#endif

RtspHTTPCommon::RtspHTTPCommon(CommonLib::MSG_TYPE enType):CommonLib::HTTPCommon(enType)
{
    RtspHTTPCommonInit();
}

RtspHTTPCommon::~RtspHTTPCommon()
{
}

void RtspHTTPCommon::RtspHTTPCommonInit(void)
{
    method.clear();

    viaList.clear();
    from.clear();
    to.clear();
    cseq.clear();
    callID.clear();
    contact.clear();

    xClientAddress.clear();
    xTransactionID.clear();
    setCookie.clear();
    cookie.clear();
    date.clear();
    server.clear();
    acceptEncoding.clear();
    acceptLanguage.clear();
    allow.clear();
    conference.clear();
    connection.clear();
    contentBase.clear();
    contentEncoding.clear();
    contentLanguage.clear();

    range.clear();
    rtpInfo.clear();
    session.clear();
    timestamp.clear();
    transport.clear();
    wwwAuthenticate.clear();
    strauthor.clear();
    scale.clear();
    speed.clear();
    unsupport.clear();
    vary.clear();
    lastModified.clear();
    expires.clear();
    contentTypeString.clear();

    contentLength = 0;
    contentType = -1;
    maxForwards = -1;
    blockSize = -1;
    bandwidth = -1;

    _x_Cache_Control.clear();
    _x_Accept_Retransmit.clear();
    _x_Accept_Dynamic_Rate.clear();
    _x_Dynamic_Rate.clear();
    _x_Support_Unicast.clear();
    _x_PlayInfo.clear();
    _x_Play_Control.clear();
    _x_Vod_Url.clear();
    _str_Public.clear();
    _x_Vod.clear();

    return;
}

int RtspHTTPCommon::parseLine(const std::string& data)
{
        size_t fieldBegin = 0;
        std::string  linesting,via;

        //PARSE_HEAD_VIA_FIELD(data, via, STR_VIA)
        if ( (fieldBegin = data.find(STR_VIA))                          
            != std::string::npos)                                           
        {                                                                   
            fieldBegin = data.find_first_of(':', fieldBegin);
            fieldBegin = data.find_first_not_of(" ", fieldBegin + 1);       
            if (fieldBegin != std::string::npos)
            {                                                               
                via.assign(data, fieldBegin, data.size() - fieldBegin);  
                viaList.push_back(via);
            }                                                               
        }

        PARSE_HEAD_FIELD(data, from, STR_FROM)
        PARSE_HEAD_FIELD(data, to, STR_TO)
        PARSE_HEAD_FIELD(data, callID, STR_CALLID)
        PARSE_HEAD_FIELD(data, cseq, STR_CSEQ)
        PARSE_HEAD_FIELD(data, cseq, STR_ZTE_CSEQ)


        /* 这两个变量和Accept的区别在httpcommon中已经处理 */
        PARSE_HEAD_FIELD(data, acceptEncoding, STR_ACCEPT_ENCODING)
        PARSE_HEAD_FIELD(data, acceptLanguage, STR_ACCEPT_LANGUAGE)

      
        PARSE_HEAD_FIELD_INT(data, maxForwards, STR_MAX_FORWARDS)
        PARSE_HEAD_FIELD(data, xClientAddress, STR_X_CLIENT_ADDRESS)
        PARSE_HEAD_FIELD(data, xTransactionID, STR_X_TRANSACTION_ID)
        PARSE_HEAD_FIELD(data, setCookie, STR_SET_COOKIE)
        PARSE_HEAD_FIELD(data, cookie, STR_COOKIE)
        PARSE_HEAD_FIELD(data, date, STR_DATE)
        PARSE_HEAD_FIELD(data, server, STR_SERVER)
        PARSE_HEAD_FIELD(data, allow, STR_ALLOW)
        PARSE_HEAD_FIELD(data, conference, STR_CONFERENCE)
        PARSE_HEAD_FIELD(data, connection, STR_CONNECTION)
        PARSE_HEAD_FIELD(data, contentBase, STR_CONTENT_BASE)
        PARSE_HEAD_FIELD(data, contentEncoding, STR_CONTENT_ENCODING)
        PARSE_HEAD_FIELD(data, contentLanguage, STR_CONTENT_LANGUAGE)
        PARSE_HEAD_FIELD(data, range, STR_RANGE)
        PARSE_HEAD_FIELD(data, rtpInfo, STR_RTP_INFO)
        PARSE_HEAD_FIELD(data, session, STR_SESSION)
        PARSE_HEAD_FIELD(data, timestamp, STR_TIMESTAMP)
        PARSE_HEAD_FIELD(data, transport, STR_TRANSPORT)
        PARSE_HEAD_FIELD(data, wwwAuthenticate, STR_WWW_AUTHENTICATE)
        PARSE_HEAD_FIELD(data, strauthor, STR_AUTHOR)
        
        PARSE_HEAD_FIELD(data, scale, STR_SCALE)
        PARSE_HEAD_FIELD(data, speed, STR_SPEED)
        PARSE_HEAD_FIELD(data, unsupport, STR_UNSUPPORT)
        PARSE_HEAD_FIELD(data, vary, STR_VARY)
        PARSE_HEAD_FIELD(data, lastModified, STR_LAST_MODIFIED)
        PARSE_HEAD_FIELD(data, expires, STR_EXPIRES)

        PARSE_HEAD_FIELD_INT(data, bandwidth, STR_BANDWIDTH)
        PARSE_HEAD_FIELD_INT(data, blockSize, STR_BLOCKSIZE)

        PARSE_HEAD_FIELD(data, _x_Cache_Control, STR_CACHE_CONTROL_X)
        PARSE_HEAD_FIELD(data, _x_Accept_Retransmit, STR_ACCEPT_RETRANSMIT_X)
        PARSE_HEAD_FIELD(data, _x_Accept_Dynamic_Rate, STR_ACCEPT_DYNAMIC_RATE_X)
        PARSE_HEAD_FIELD(data, _x_Dynamic_Rate, STR_DYNAMIC_RATE_X)
        PARSE_HEAD_FIELD(data, _x_Support_Unicast, STR_SUPPORT_UNICAST)
        PARSE_HEAD_FIELD(data, _x_Play_Control, STR_PLAY_CONTROL)
        PARSE_HEAD_FIELD(data, _x_Vod_Url, STR_X_VOD_URL)
        PARSE_HEAD_FIELD(data, _str_Public, STR_PUBLIC)
        PARSE_HEAD_FIELD(data, _x_Vod, STR_X_VOD)

        return 0;
}    

int RtspHTTPCommon::packetLine(void)
{
    char str[512] = {0};
  
    memset(str, 0, sizeof(str));
    
    //via域
    for (std::list<std::string>::iterator sIt = viaList.begin(); sIt != viaList.end(); sIt++)
    {
        PACKET_HEAD_FIELD_STR((*sIt), STR_VIA)
    }

    PACKET_HEAD_FIELD_STR(from, STR_FROM)

    PACKET_HEAD_FIELD_STR(to, STR_TO)

    PACKET_HEAD_FIELD_STR(callID, STR_CALLID)

    PACKET_HEAD_FIELD_STR(cseq, STR_CSEQ)
    
    PACKET_HEAD_FIELD_STR(contact, STR_CONTACT)

    PACKET_HEAD_FIELD_INT(maxForwards, STR_MAX_FORWARDS)
  
    PACKET_HEAD_FIELD_STR(expires, STR_EXPIRES)
     
    //date域
    PACKET_HEAD_FIELD_STR(date, STR_DATE)

    //X-Client-Address域    
    PACKET_HEAD_FIELD_STR(xClientAddress, STR_X_CLIENT_ADDRESS)

    //X-Transaction-ID域
    PACKET_HEAD_FIELD_STR(xTransactionID, STR_X_TRANSACTION_ID)

    //server域    
    PACKET_HEAD_FIELD_STR(server, STR_SERVER)

    //set-cookie域    
    PACKET_HEAD_FIELD_STR(setCookie, STR_SET_COOKIE)

    //cookie域
    PACKET_HEAD_FIELD_STR(cookie, STR_COOKIE)

    PACKET_HEAD_FIELD_STR(acceptEncoding, STR_ACCEPT_ENCODING)
    PACKET_HEAD_FIELD_STR(acceptLanguage, STR_ACCEPT_LANGUAGE)
    PACKET_HEAD_FIELD_STR(allow, STR_ALLOW)
    PACKET_HEAD_FIELD_STR(conference, STR_CONFERENCE)
    PACKET_HEAD_FIELD_STR(connection, STR_CONNECTION)
    PACKET_HEAD_FIELD_STR(contentBase, STR_CONTENT_BASE)
    PACKET_HEAD_FIELD_STR(contentEncoding, STR_CONTENT_ENCODING)
    PACKET_HEAD_FIELD_STR(contentLanguage, STR_CONTENT_LANGUAGE)
    PACKET_HEAD_FIELD_STR(range, STR_RANGE)
    PACKET_HEAD_FIELD_STR(rtpInfo, STR_RTP_INFO)
    PACKET_HEAD_FIELD_STR(session, STR_SESSION)
    PACKET_HEAD_FIELD_STR(timestamp, STR_TIMESTAMP)
    PACKET_HEAD_FIELD_STR(transport, STR_TRANSPORT)
    PACKET_HEAD_FIELD_STR(wwwAuthenticate, STR_WWW_AUTHENTICATE)
    PACKET_HEAD_FIELD_STR(strauthor, STR_AUTHOR)
    PACKET_HEAD_FIELD_STR(scale, STR_SCALE)
    PACKET_HEAD_FIELD_STR(speed, STR_SPEED)
    PACKET_HEAD_FIELD_STR(unsupport, STR_UNSUPPORT)
    PACKET_HEAD_FIELD_STR(lastModified, STR_LAST_MODIFIED)
    PACKET_HEAD_FIELD_STR(vary, STR_VARY)

    PACKET_HEAD_FIELD_INT(bandwidth, STR_BANDWIDTH)
    PACKET_HEAD_FIELD_INT(blockSize, STR_BLOCKSIZE)

    PACKET_HEAD_FIELD_STR(_x_Cache_Control, STR_CACHE_CONTROL_X)
    PACKET_HEAD_FIELD_STR(_x_Accept_Retransmit, STR_ACCEPT_RETRANSMIT_X)
    PACKET_HEAD_FIELD_STR(_x_Accept_Dynamic_Rate, STR_ACCEPT_DYNAMIC_RATE_X)
    PACKET_HEAD_FIELD_STR(_x_Dynamic_Rate, STR_DYNAMIC_RATE_X)
    PACKET_HEAD_FIELD_STR(_x_Support_Unicast, STR_SUPPORT_UNICAST)
    PACKET_HEAD_FIELD_STR(_x_PlayInfo, STR_X_PLAYINFO)
    PACKET_HEAD_FIELD_STR(_x_Play_Control, STR_PLAY_CONTROL)
    PACKET_HEAD_FIELD_STR(_x_Vod_Url, STR_X_VOD_URL)
    PACKET_HEAD_FIELD_STR(_str_Public, STR_PUBLIC)
    PACKET_HEAD_FIELD_STR(_x_Vod, STR_X_VOD)
    //头和体的空行不在头中加，包括包体最后的空行都不在包体中加，在上层PDU类中加。
    
    return 0;
}


