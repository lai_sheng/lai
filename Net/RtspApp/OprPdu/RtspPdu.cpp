#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "RtspPdu.h"

/*"RTSP"头*/
const char rtspStrFlag[RTSP_METHOD_NUM][16] = 
{
    "OPTIONS","DESCRIBE" ,"SETUP", "PLAY","RECORD","PAUSE", "GET_PARAMETER","SET_PARAMETER","TEARDOWN"
};

CRtspPdu::CRtspPdu()
{
    m_pHttpHdr = NULL;
    m_nodelist.clear();
}

CRtspPdu::~CRtspPdu()
{
    if (m_pHttpHdr)
    {
        delete m_pHttpHdr;
    }
    m_pHttpHdr = 0;

    m_nodelist.clear();
}

CRtspPdu* CRtspPdu::createPduWith(std::string strMethod)
{
    CRtspPdu* pPdu = new CRtspPdu();
    if (!pPdu)
    {
        return NULL;
    }

    RtspHTTPCommon* pObj = new RtspHTTPCommon(CommonLib::MODEL_REQUEST);
    if (!pObj)
    {
        delete pPdu;
        pPdu = 0;
        return NULL;
    }

    pObj->method = strMethod;
    pObj->userAgent = "VISION RTSP SERVER";

    pObj->setProtocol(CommonLib::PROTOCOL_RTSP);
    pPdu->SetHdr(pObj);
    pPdu->SetMethod(strMethod);

    return pPdu;
}

CRtspPdu* CRtspPdu::createAck(CRtspPdu* pReqPdu)
{
    if ( !pReqPdu )
    {
        return NULL;
    }

    CRtspPdu* pAckPdu = new CRtspPdu();
    if ( !pAckPdu )
    {
        return NULL;
    }

    RtspHTTPCommon* pObj = new RtspHTTPCommon(CommonLib::MODEL_RESPONSE);
    if ( !pObj )
    {
        delete pAckPdu;
        return NULL;
    }

    pObj->cseq = pReqPdu->GetHdr()->cseq;

    pObj->setProtocol(CommonLib::PROTOCOL_RTSP);
    pAckPdu->SetHdr(pObj);

    return pAckPdu;
}

CRtspPdu* CRtspPdu::createAckWithPdu(CRtspPdu* pReqPdu, std::string strMethod)
{
    if ( !pReqPdu )
    {
        return NULL;
    }

    CRtspPdu* pPdu = CRtspPdu::createPduWith(strMethod);
    if (!pPdu)
    {
        return NULL;
    }

    int iSeq = atoi(pReqPdu->GetHdr()->cseq.c_str());
    iSeq+=1;
    char strSeq[16]={0};
    sprintf(strSeq, "%d", iSeq);
    pPdu->GetHdr()->cseq = strSeq;
    pPdu->SetMethod(strMethod);
    
    return pPdu;
}

bool CRtspPdu::parseBody(const std::string strData)
{
    //也有可能是rtcp的数据，现在还没有处理
    RtspHTTPCommon* pObj = NULL;

    size_t posFlag = strData.find("\r\n");
    bool bReq = false;

    if (posFlag != std::string::npos)
    {
        size_t posSig = 0;

        for (int iIndex = 0 ; iIndex < RTSP_METHOD_NUM; iIndex++)
        {
            posSig = strData.find(rtspStrFlag[iIndex]);
            if (posSig != std::string::npos)
            {
                if (posSig < posFlag)
                {
                    bReq = true;
                    m_strMethod = rtspStrFlag[iIndex];
                    break;
                }
            }
        }

        if (bReq)
        {
            pObj = new RtspHTTPCommon(CommonLib::MODEL_REQUEST);
        }
        else
        {
            if (strData.find("RTSP") != std::string::npos 
                || strData.find("rtsp") != std::string::npos)
            {
                pObj = new RtspHTTPCommon(CommonLib::MODEL_RESPONSE);
            }
        }

        if (pObj)
        {
            if (CommonLib::ERR_SUCCESS != pObj->fromStream(strData))
            {
                printf("CRtspPdu::parseBody@@@@@@fromStream Error\n%s\n", strData.c_str());
                m_pHttpHdr = pObj;
                return false;
            }

            m_pHttpHdr = pObj;
            ParseLine();
            return true;
        }
        else
        {
            printf("CRtspPdu::parseBody Create HttpCommon Failed!\n");
            return false;
        }
    }
    else
    {
        printf("CRtspPdu::parseBody HEADER Failed!\n");
    }

    return false;
}

/*
v=0
o=MediaSIP 0344450000009195 0 IN IP4 10.18.34.100
s=MediaSIP
c=IN IP4 10.18.34.100
t=0 0
a=framerate:25
m=video 26666 RTP/AVP 96
a=rtpmap:96 MP4V-ES/90000
a=fmtp:96 config=00000100000001200006c400670c1610485180
*/
void CRtspPdu::ParseLine(const char* tagSplit)
{
    if(NULL == m_pHttpHdr->getBody())
    {
        return;
    }

    std::string strLineData(m_pHttpHdr->getBody());

    strLineData += tagSplit; /* 尾部再添加，好做分析 */

    size_t posAnd, posEqu,index;
    posAnd = posEqu = index = 0;

    LINE_NODE tmpNode;

    while(1)
    {
        posEqu = strLineData.find("=", index);
        posAnd = strLineData.find(tagSplit, index);

        if (posAnd == std::string::npos
            || posEqu == std::string::npos)
        {
            //no more
            break;
        }

        if (posEqu <= posAnd )
        {
            tmpNode.szNodeName.assign(strLineData, index, posEqu-index);
            tmpNode.szContent.assign(strLineData, posEqu+1, posAnd-posEqu-1);
            m_nodelist.push_back(tmpNode);
        }
        else
        {
            break;
        }

        index = posAnd + strlen(tagSplit);
    }
}

std::string& CRtspPdu::packetBody()
{
    m_lineStr.clear();

    if (m_pHttpHdr)
    {
        if (m_nodelist.size() > 0 )
        {
            for (std::list<LINE_NODE>::iterator ListItr = m_nodelist.begin();
                ListItr != m_nodelist.end();
                ListItr++)
            {
                m_lineStr += (*ListItr).szNodeName;
                m_lineStr += "=";
                m_lineStr += (*ListItr).szContent;

                m_lineStr += "\r\n"; /* &类型需要修改*/
            }

            m_pHttpHdr->setBody(m_lineStr.c_str(), m_lineStr.length());
        }

        m_lineStr.clear();
        m_lineStr = m_pHttpHdr->toStream();
    }

    return     m_lineStr;
}



std::string CRtspPdu::getLineData(char * pTagName)    
{
    if (!pTagName)
    {
        return NULL;
    }

    std::string strName = pTagName;
    m_lineValue.clear();

    for (std::list<LINE_NODE>::iterator ListItr= m_nodelist.begin();
        ListItr != m_nodelist.end();
        ListItr++)
    {
        if ((*ListItr).szNodeName== strName)
        {
            m_lineValue = (*ListItr).szContent;

            /* 采取分析结束后进行删除操作，上层应用层需要注意 */
            m_nodelist.erase(ListItr);
            break;

        }
    }

    return m_lineValue;
}

int CRtspPdu::getLineData(char* pTagName,int& iValue)
{
      iValue = -2;
    getLineData(pTagName);
    if (m_lineValue.length() > 0)
    {
        iValue = atoi(m_lineValue.c_str());
    }

    return iValue;
}

//添加字符串
bool CRtspPdu::addLineData(char* tagName,  char* tagValue, int iLen)
{
    if ( tagName && tagValue && iLen > 0 )
    {
        LINE_NODE tmpNode;
        tmpNode.szNodeName = tagName;
        tmpNode.szContent.assign(tagValue,iLen);
        m_nodelist.push_back(tmpNode);
    }
    return true;
}
//添加整型
bool CRtspPdu::addLineData(char* tagName,  int iValue)
{
    if (!tagName)
    {
        return false;
    }

    /* 转化成字串*/
    char strTmp[32];
    memset(strTmp,0,sizeof(strTmp));
    sprintf(strTmp,"%d",iValue);

    LINE_NODE tmpNode;
    tmpNode.szNodeName = tagName;
    tmpNode.szContent = strTmp;
    m_nodelist.push_back(tmpNode);
    return true;
}

