#pragma once

#include <iostream>
#include <sstream>
#include <map>
#include <list>
#include <string>

using namespace std;

#include "RtspHttp.h"

#define VAP_INVALID_VALUE   -2

#define RTSP_METHOD_NUM 9
extern const char rtspStrFlag[RTSP_METHOD_NUM][16];

typedef struct _tagLineNode
{
	std::string szNodeName;
	std::string szContent;
} LINE_NODE;

class CRtspPdu
{
public:
	CRtspPdu();
	~CRtspPdu();
	static CRtspPdu* createPduWith(std::string strMethod);
	static CRtspPdu* createAck(CRtspPdu* pReqPdu);
	static CRtspPdu* createAckWithPdu(CRtspPdu* pReqPdu, std::string strMethod);

	/*反系列化--->参数*/
	bool parseBody(const std::string strData);

	/*系列化参数--->*/
	std::string& packetBody();

	//获取A=xx&B=xx...格式下的值,或者A=xx\r\nB=xx...
	std::string getLineData(char * pTagName);
	int getLineData(char* pTagName,int& iValue);
	//添加A=xx格式
	bool addLineData(char* tagName,  char* tagValue, int iLen);
	bool addLineData(char* tagName,  int iValue);

	/* Rtsp头*/
	std::string GetMethod(){return m_strMethod;}
	void SetMethod(std::string strMethod){m_strMethod = strMethod;}

	RtspHTTPCommon* GetHdr(){return m_pHttpHdr;}
	void SetHdr(RtspHTTPCommon* pHdr){m_pHttpHdr = pHdr;}

private:
	void ParseLine(const char* tagSplit="\r\n");

	std::string m_strMethod;	/* 命令*/
	RtspHTTPCommon* m_pHttpHdr;

	/* tagA=xx&tagB=xx,或者tagA=xx\r\ntagB=xx格式包体*/
	std::list<LINE_NODE> m_nodelist; /* A=xx&B=xx 包体内容*/
	std::string m_lineStr;
	std::string m_lineValue;
};

