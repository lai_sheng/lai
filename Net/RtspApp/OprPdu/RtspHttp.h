#ifndef __RTSP_HTTP_H__
#define __RTSP_HTTP_H__
/*
* Copyright (c) 2009, visiondigi
* All rights reserved.
*
* 文件名称：DH_HTTP.h
* 文件标识：参见配置管理计划书
* 摘　　要：对vd_http的继承，实现vd_http 中没有实现的rtsp一些功能
*
* 当前版本：1.0
* 作　　者：zdx
* 完成日期：2009年12月23日
*
*/

#include "httpxml/http/VD_HTTP.h"


/////////////////////////////////////////////////////////////
class RtspHTTPCommon : public CommonLib::HTTPCommon
{

    public:
    
    RtspHTTPCommon(CommonLib::MSG_TYPE enType);

    virtual ~RtspHTTPCommon(void);

    int maxForwards;
    std::string contact;

    StrList viaList;
    std::string from;
    std::string to;

    std::string cseq;
    std::string callID;
    std::string date;
    std::string xClientAddress;
    std::string xTransactionID;
    std::string setCookie;
    std::string server;
    std::string cookie;
    std::string acceptEncoding;
    std::string acceptLanguage;
    std::string allow;
    int bandwidth;
    int blockSize;
    std::string scale;
    std::string speed;
    std::string conference;
    std::string connection;
    std::string contentBase;
    std::string contentEncoding;
    std::string contentLanguage;
    std::string range;
    std::string rtpInfo;
    std::string session;
    std::string timestamp;
    std::string transport;
    std::string wwwAuthenticate;
    std::string strauthor;
    std::string unsupport;
    std::string vary;
    std::string expires;
    std::string lastModified;

    std::string _x_Cache_Control;
    std::string _x_Accept_Retransmit;
    std::string _x_Accept_Dynamic_Rate;
    std::string _x_Dynamic_Rate;
    std::string _x_Support_Unicast;
    std::string _x_PlayInfo;
    std::string _x_Play_Control;
    std::string _x_Vod_Url;
    std::string _str_Public;
    std::string _x_Vod;

private:

    int parseLine(const std::string& data);/*对扩展字段进行解包*/
    int packetLine(void);/*对扩展字段进行打包*/
    void RtspHTTPCommonInit(void);;/*对扩展字段初始化*/
    
};

#endif //head
