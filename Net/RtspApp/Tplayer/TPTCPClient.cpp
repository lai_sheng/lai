/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPTCPClient.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：TCP客户端实现类。使用标准Socket实现TCP客户模式。因为使用标准Socket，所以理论上可以运行在各种支持BSD Socket的操作系统。
目前我们的目标是支持：Windows各版本、Linux、ucLinux、PSOS。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年6月28日
* 修订记录： 1. 接口重新改过了
2. 发送修改了，使用队列，然后在Heartbeat中检查socket是否可写，可写时进行发送。 

*
* 取代版本：0.2
* 原作者　：祝晓妮
* 完成日期：2007年4月28日
* 修订记录：创建

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年4月23日
* 修订记录：设计

*/
#include "TPTCPClient.h"

#include "ez_libs/ez_socket/ez_socket.h"

#include "ITPListener.h"


namespace CommonLib
{
    TPTCPClient::TPTCPClient(ITPListener * tcpclientapp, int engineId):ITPObject(tcpclientapp, engineId)
    {
    }

    TPTCPClient::~TPTCPClient()
    {
        Close();
    }

    int TPTCPClient::Connect(const char* ip, int port)
    {
        CGuard guard(_mutex);

        _ip = inet_addr((char *)ip);
        _port = htons(port);

        if (INVALID_SOCKET == _socket)
        {
            _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if((_localIp != INADDR_ANY) &&(_localPort != 0))
            {
                struct sockaddr_in local_addr;
                memset(&local_addr, 0, sizeof(local_addr));
                local_addr.sin_family = AF_INET;
                local_addr.sin_port = _localPort;
                local_addr.sin_addr.s_addr = _localIp;//MY IP

                if (INVALID_SOCKET == bind(_socket, (struct sockaddr *)&local_addr, sizeof(struct sockaddr)) )
                {
                    printf("bind local address error\n");
                    closeInside();
                    return TP_INVALID_ADDRESS;
                }
            }
        }

        struct sockaddr_in my_addr;
        memset(&my_addr, 0, sizeof(my_addr));

        my_addr.sin_family = AF_INET;
        my_addr.sin_addr.s_addr = _ip;
        my_addr.sin_port = _port;

        //将socket设置为非阻塞
        int flags = 0;
#ifdef WIN32
        unsigned long l = 1;
        int n = ioctlsocket(_socket, FIONBIO, &l);
        if (n != 0)
        {
            int errcode = WSAGetLastError();
            return errcode;
        }
#else
        if ((flags = fcntl(_socket, F_GETFL, 0)) == -1)
        {
            printf("fcntl(F_GETFL, O_NONBLOCK)");
        }

        if (fcntl(_socket, F_SETFL, flags | O_NONBLOCK) == -1)
        {
            printf("fcntl(F_SETFL, O_NONBLOCK)");
        }
#endif

        //设置接收和发送缓冲大小
        setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_recvBuffSize, sizeof(_recvBuffSize));
        setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_sendBufferSize, sizeof(_sendBufferSize));

        //connect
        int ret = connect(_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr));
        if(INVALID_SOCKET == ret)
        {
            fd_set wds;
            FD_ZERO(&wds);
            FD_SET(_socket,&wds);

            struct timeval tv;
            tv.tv_sec = 0;
            tv.tv_usec = 1500 * 1000; /*1500 ms*/
            int iRet = select(_socket + 1, NULL, &wds, NULL, &tv);
            if(iRet>0 && FD_ISSET(_socket,&wds))
            {
                int len = -1;
                int error = -1;
                len = sizeof(error);
#ifndef WIN32
                getsockopt(_socket, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&len);
#else
                getsockopt(_socket, SOL_SOCKET, SO_ERROR, (char *)&error, (int *)&len);
#endif
                if(error == 0) 
                {    
                    //printf("---------connect successful\n"); 
                    if (_listener)
                    {
                        //告知上层连接OK
                    _mutex.Leave();
                    _listener->onConnect(0, _socket, ip, port);
                    _mutex.Enter();
                }
                return _socket;
                }
                else
                {
                    printf("TPTCPClient::Connect connect failed\n");
                    closeInside();
                    return INVALID_SOCKET;
                }
            
                
            }
            else
            {
                closeInside();
                return INVALID_SOCKET;
            }
        }

        return _socket;
    }

    int TPTCPClient::Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort)
    {
        _mutex.Enter();
        _localIp = inet_addr((char *)localIp);
        _localPort = htons(localPort);
        _mutex.Leave();

        return Connect(remoteIp, remotePort);
    }

    int TPTCPClient::ConnectPeer(int iSocket, unsigned int iIp, unsigned short usPort)
    {
        if (iSocket <= 0)
        {
            return -1;
        }

         CGuard guard(_mutex);
         _socket = iSocket;
         _ip = iIp;
         _port= usPort;

         //设置接收和发送缓冲大小
         setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_recvBuffSize, sizeof(_recvBuffSize));
         setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_sendBufferSize, sizeof(_sendBufferSize));

         return _socket;
    }

    int TPTCPClient::Close()
    {
        CGuard guard(_mutex);

        return closeInside();
    }

    int TPTCPClient::closeInside()
    {
        int iRet = 0;
        if(_socket != INVALID_SOCKET)
        {
#ifndef WIN32
            shutdown(_socket, SHUT_RDWR);
#endif
            iRet = closesocket(_socket);
            _socket = INVALID_SOCKET;
        }
        
        //TODO:发送队列也要清一下.
        for (DataDequeIt it = _dataList.begin(); it != _dataList.end(); ++it)
        {
            DataRow& row = *it;
            _listener->onSendDataFail(0, _socket, row.sequence);
        }
        return iRet;
    }

    //心跳操作，维护本连接的收发操作，并通知上层调用者
    int TPTCPClient::Heartbeat()
    {
        CGuard guard(_mutex);

        if(INVALID_SOCKET == _socket)
        {
            //printf("==TPTCPClient:Heartbeat== Invalid socket!\n ");
            return TP_ERROR_BASE;
        }

        //select操作准备
        fd_set readfds;
        fd_set writefds;
        FD_ZERO(&readfds);
        FD_ZERO(&writefds);
        FD_SET(_socket,&readfds);

        ///仅仅需要发送数据的时候，才检查发送
        if (!_dataList.empty())
        {
            FD_SET(_socket,&writefds);
        }

        //check conn status
        struct timeval SelectTimev; 
        size_t milliSec = GetSelectTimer();
        SelectTimev.tv_usec = 1000 * (milliSec%1000);
        SelectTimev.tv_sec = milliSec/1000;

        int fdnums = select(_socket+1, &readfds, &writefds, NULL, &SelectTimev);
        if (fdnums < 0)
        {
            printf("==TPTCPClient:Heartbeat== client select error\n");
            return TP_ERROR_BASE;
        }
#ifndef WIN32
        else if(fdnums == 0)
        {
            //  printf("client^_^select error=%d\n",fdnums);
            return TP_NORMAL_RET;
        }
        else
        {
            //printf("client select result = %d\n",fdnums);
        }
#else
        else if(fdnums ==0)
        {
            //超时
            //printf("select timeout!\n");
            return 0;
        }
        else
        {
        }
#endif

        //检查对方发送来的数据
        if(fdnums && FD_ISSET(_socket, &readfds))
        {
            fdnums--;

            int iRevLen = recv(_socket, _buffer, BUF_SIZE, 0);
            if ( ((iRevLen < 0) && (errno != EINTR))
              || (iRevLen == 0) )//如果接收错误，但是错误是EAGAIN，表明仅仅是没有收到数据   
            {
                //是先通知上层还是先关闭需要再研究,因为可能会影响上层应用程序.
                if ( _listener != NULL )
                {    
                    printf("==TPTCPClient:Heartbeat[engineId %d]== Recv data<%d> error,ErrorNo<%d>!\n",_engineId,iRevLen,errno);
                    perror("recv");
                    _listener->onClose(_engineId, _socket);
                }

                //关闭，则不再做后处理
                closeInside();
                return 0;
            }
            {
                if(_listener != NULL)
                {
                    //这里客户方回调可能调用其他接口，防止自己锁自己
                    _mutex.Leave();
                    this->_listener->onData(_engineId, _socket, _buffer, iRevLen);
                    _mutex.Enter();
                }
            }
        }
        
        //检查内部缓冲待发送的数据，单连接模式仅仅发送队列头
        if (fdnums && !_dataList.empty())
        {
            DataRow row = _dataList.front();
            int iRst = sendInside(row.id, row.data, row.len);

            ///这里要考虑在确认里边关闭了连接的情况,所以先清理容器
            _dataList.pop_front();
            if(_listener != NULL)
            {
                if (iRst >= 0)
                {
                   _mutex.Leave();
                    _listener->onSendDataAck(_engineId, row.id, row.sequence);
                   _mutex.Enter();
                }
                else
                {
                    _listener->onSendDataFail(_engineId, row.id, row.sequence);
                }
            }
        }

        return TP_NORMAL_RET;
    }

    int TPTCPClient::Send(int id, const char *pBuf, size_t iBufLen)
    {
        DataRow row;
        row.id = id;
        row.data = pBuf;
        row.len = iBufLen;

        CGuard guard(_mutex);

        ///检查状态
        if (INVALID_SOCKET == _socket)
        {
            printf("===TPTCPClient:Send==Invalid socket!\n");
            return TP_INVALID_ADDRESS;
        }

        //TODO:检查队列是否满，不能无限制发
        row.sequence = getSequence();
        _dataList.push_back(row);
        return row.sequence;
    }

    //内部数据发送接口实现
    int TPTCPClient::sendInside(int id, const char *pBuf, size_t iBufLen)
    {
        if (INVALID_SOCKET == _socket)
        {
            printf("===TPTCPClient:sendInside===:socket invalid\n");
            return TP_ERROR_BASE;
        }

        if ((0 == iBufLen) || (NULL == pBuf))
        {
            return 0;
        }

        int iSend = 0;
        unsigned int iSended = 0;

        while (iSended < iBufLen)
        {
            iSend = send(_socket, pBuf+iSended, iBufLen-iSended, 0);

            if (iSend < 0)
            {
                break;
            }

            iSended += iSend;
            continue;
        }

        return (iBufLen);
    }

    int TPTCPClient::SendRawStream(int id, const char *pBuf, size_t iBufLen)
    {
        if ( !pBuf || iBufLen == 0)
        {
            return TP_ERROR_BASE;
        }
        CGuard guard(_mutex);

        if(INVALID_SOCKET == _socket )
        {
            //printf("==TPTCPClient:SendRawStream== Invalid socket!\n ");
            return TP_ERROR_BASE;
        }

        //select操作准备
        fd_set writefds;
        FD_ZERO(&writefds);
        FD_SET(_socket,&writefds);

        //check conn status
        struct timeval SelectTimev; 
        size_t milliSec = GetSelectTimer();
        SelectTimev.tv_usec = 1000 * (milliSec%1000);
        SelectTimev.tv_sec = milliSec/1000;

        int fdnums = select(_socket+1, NULL, &writefds, NULL, &SelectTimev);
        if (fdnums < 0)
        {
            printf("==TPTCPClient:Heartbeat== client select error\n");
            return TP_ERROR_BASE;
        }
#ifndef WIN32
        else if(fdnums == 0)
        {
            //  printf("client^_^select error=%d\n",fdnums);
            return TP_NORMAL_RET;
        }
        else
        {
            //printf("client select result = %d\n",fdnums);
        }
#else
        else if(fdnums ==0)
        {
            //超时
            //printf("select timeout!\n");
            return 0;
        }
        else
        {
        }
#endif

        //检查内部缓冲待发送的数据，单连接模式仅仅发送队列头
        if (fdnums )
        {
            /*int iRst = */sendInside(0, pBuf, iBufLen);
        }

        return TP_NORMAL_RET;
    }

    void TPTCPClient::DumpBufferdData()
    {
        CGuard guard(_mutex);

        DumpDataList(_dataList);
    }

    size_t TPTCPClient::GetBufferDataLen()
    {
        CGuard guard(_mutex);

        return GetDataListLen(_dataList);
    }
}
