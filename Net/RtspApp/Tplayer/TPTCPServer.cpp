/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPTCPServer.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：TCP服务端实现类。使用标准Socket实现TCP服务器模式。因为使用标准Socket，所以理论上可以运行在各种支持BSD Socket的操作系统。
目前我们的目标是支持：Windows各版本、Linux、ucLinux、PSOS。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年6月28日
* 修订记录： 1. 接口重新改过了
2. 发送修改了，使用队列，然后在Heartbeat中检查socket是否可写，可写时进行发送。 

*
* 取代版本：0.2
* 原作者　：祝晓妮
* 完成日期：2007年4月28日
* 修订记录：创建

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年4月23日
* 修订记录：设计

*/
#include "TPTCPServer.h"

#include "ez_libs/ez_socket/ez_socket.h"

#include "ITPListener.h"

namespace CommonLib
{
    TPTCPServer::TPTCPServer(ITPListener * TCPserverapp, int engineId) : ITPObject(TCPserverapp, engineId)
    {
    }

    TPTCPServer::~TPTCPServer()
    {
        Close();
    }

    int TPTCPServer::Listen(char* ip, int port)
    {
        CGuard guard(_mutex);

        if(ip == NULL)
        {
            _ip = INADDR_ANY;
        }
        else
        {
            _ip = inet_addr(ip);
        }

        _port = htons(port);
        if(INVALID_SOCKET == _socket)
        {
            _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        }

        struct sockaddr_in my_addr;
        const int yes = 1;

        //设置SO_REUSEADDR属性
        if (INVALID_SOCKET == setsockopt(_socket,
            SOL_SOCKET,
            SO_REUSEADDR,
            (char *) &yes,
            sizeof(yes)) )
        {
            goto failure_ez_TCP_listen_port_t;
        }

        //设置接收和发送缓冲大小
        setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_recvBuffSize, sizeof(_recvBuffSize));
        setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_sendBufferSize, sizeof(_sendBufferSize));

        memset(&my_addr, 0, sizeof(my_addr));
        my_addr.sin_family = AF_INET;
        my_addr.sin_port = _port;
        my_addr.sin_addr.s_addr = _ip;

        if (INVALID_SOCKET == bind(_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) )
        {
            goto failure_ez_TCP_listen_port_t;
        }

        if (INVALID_SOCKET == listen(_socket, 5))
        {
            goto failure_ez_TCP_listen_port_t;
        }

		_mutex.Leave();
        return _socket;

failure_ez_TCP_listen_port_t:
        closeInside();
        _mutex.Leave();
        return INVALID_SOCKET;
    }

    ///外部接口
    int TPTCPServer::CloseClient(int id)
    {
        CGuard guard(_mutex);
        return closeClientInside(id);
    }

    ///关闭给定的客户方，这里ID等于SOCKET
    int TPTCPServer::closeClientInside(int id)
    {
        ClientQueueIt it = _clients.find(id);
        if (it == _clients.end())
        {
            return TP_ERROR_BASE;
        }
        
        ///查找对应的socket关联的数据表
        ClientInfo& info = it->second;
        info.online = 0; ///下次可复用
        DataQueueIt iter = _queue.find(info.socket);
        if (iter != _queue.end())
        {
			///处理数据回调，并清理空间
			DataList& list = _queue[iter->first];
			for (DataList::iterator DataIt = list.begin(); DataIt != list.end(); ++DataIt)
			{
				if (_listener)
				{
					///通知上层删除空间，否则内存泄露,这里的数据并没有发送出去
					_listener->onSendDataFail(_engineId, id, DataIt->sequence);
				}
			}
			list.clear();
        }

        ///关闭对应socket
#ifndef WIN32
        shutdown(info.socket, SHUT_RDWR);
#endif
        int iRet = closesocket(info.socket);
        if (iRet < 0)
        {
            return iRet;
        }

        return TP_NORMAL_RET;
    }

    int TPTCPServer::closeInside()
    {
        int iRet = 0;
        ClientQueueIt it = _clients.begin();
        for (; it != _clients.end(); it++)
        {
            ///关闭各个子连接,连接表空间在稍后全部清理
            closeClientInside(it->first);
        }

        if(_socket != INVALID_SOCKET)
        {
#ifndef WIN32
			shutdown(_socket, SHUT_RDWR);
#endif
            iRet =  closesocket(_socket);
            _socket = INVALID_SOCKET;
        }

        _clients.clear();
        _queue.clear();
        return iRet;
    }

    int TPTCPServer::Close()
    {
        CGuard guard(_mutex);
        return closeInside();
    }

    int TPTCPServer::Heartbeat()
    {
        //printf("____________heartbeat__________\n");
        CGuard guard(_mutex);
        if(INVALID_SOCKET == _socket)
        {
            printf("_________socket error!\n");
            return TP_ERROR_BASE;
        }

        ///准备待监听的文件描述符
        fd_set readfds;
        fd_set writefds;
        FD_ZERO(&readfds);
        FD_ZERO(&writefds);

        FD_SET(_socket,&readfds);
        int maxfd = _socket; //起始监听socket

#define MERGE_SOCKFD(sock, fdset, maxfdnum) FD_SET(sock, &fdset); if (sock > maxfdnum) maxfdnum = sock; 
        //check conn status
        for (ClientQueueCIt iter = _clients.begin(); iter != _clients.end(); iter++)
        {
            if( (1 == iter->second.online) && (INVALID_SOCKET != iter->second.socket) )
            {
                MERGE_SOCKFD(iter->second.socket, readfds, maxfd);
            }
        }

        ///检查发数据接口,队列表以socke为id
        for (DataQueueIt wIt = _queue.begin(); wIt != _queue.end(); wIt++)
        {
            assert(wIt->first != INVALID_SOCKET);
            if (!wIt->second.empty())
            {
                MERGE_SOCKFD(wIt->first, writefds, maxfd);
            }
        }
#undef  MERGE_SOCKFD

        struct timeval SelectTimev; 
        size_t milliSec = GetSelectTimer();
        SelectTimev.tv_usec = 1000 * (milliSec%1000);
        SelectTimev.tv_sec = milliSec/1000;
        int fds = select(maxfd + 1, &readfds, &writefds, NULL, &SelectTimev);

        if(fds < 0)
        {
            printf("TPTCPServer^_^select error\n");
            return TP_ERROR_BASE;
        }

#ifndef WIN32
        else if(fds == 0)
        {
            //printf("==TPLayer== select timeout!\n");
            return TP_NORMAL_RET;
        }
        else
        {
            //printf("==TPLayer== select ret:<%d>\n",fds);
        }
#else
        else if(fds ==0)
        {
            //超时
            return TP_NORMAL_RET;
        }
        else
        {
            //这个是读出数据的分枝
            //
        }
#endif

        //accept new coming conn
        //

        if(fds && FD_ISSET(_socket,&readfds))
        {
            fds--;     
            //记录对方地址信息
            struct sockaddr_in addr;
            int addr_len = sizeof(struct sockaddr_in);
            int newSock = INVALID_SOCKET;
#ifndef WIN32
            newSock = accept(_socket,(struct sockaddr *)&addr, (socklen_t*)&addr_len);
#else
            newSock = accept(_socket,(struct sockaddr *)&addr, &addr_len);
#endif
            printf("==TPLayer== Accept on sock:<%d>\n", newSock);
            if(INVALID_SOCKET != newSock)
            {
                ///检验队列有效性和内部状态稳定
                assert((_clients.find(newSock) == _clients.end()) 
                    || (_clients[newSock].online == 0));
               
                //onConnect
                if(_listener == NULL)
                {
                    printf("======TPLayer=== No appropriate Listener!\n");
                    return TP_ERROR_BASE;
                }   
                ///这里传回的是本地字节序的IP和端口
                const char *ip = inet_ntoa(addr.sin_addr);
                int port = ntohs(addr.sin_port);
                int iRet = this->_listener->onConnect(_engineId, newSock, ip, port);
               
                if(0 == iRet)
                {
                    ClientInfo& info = _clients[newSock];

                    ///初始化该连接的相关属性
                    info.ip = addr.sin_addr.s_addr;
                    info.port = addr.sin_port;
                    info.online = 1;
                    info.socket = newSock;
                    info.timemark = 0;

                    const int optval = 1;
                    if (setsockopt(newSock, IPPROTO_TCP, TCP_NODELAY,    //No delay
                        (char *)&optval, sizeof(optval)) < 0)
                    {
                        printf("error setsockopt nodelay");
                    }
								
					//将socket设置为非阻塞
					int flags = 0;
#ifdef WIN32
					unsigned long l = 1;
					int n = ioctlsocket(newSock, FIONBIO, &l);
					if (n != 0)
					{
						int errcode = WSAGetLastError();
						return errcode;
					}
#else
					if ((flags = fcntl(newSock, F_GETFL, 0)) == -1)
					{
						printf("fcntl(F_GETFL, O_NONBLOCK)");
					}

					if (fcntl(newSock, F_SETFL, flags | O_NONBLOCK) == -1)
					{
						printf("fcntl(F_SETFL, O_NONBLOCK)");
					}
#endif


                }
                else
                {
                    printf("client error, didn't accept!\n");
#ifndef WIN32
					shutdown(newSock, SHUT_RDWR);
#endif
                    closesocket(newSock);
                }
            }
            else
            {
                printf("invalid dlg socket\n");
            }
        }

        //recieve data from clients
        ClientQueueIt it = _clients.begin();
        int iRevLen = 0;
        while(fds && (it != _clients.end()))
        {
            assert(it->first >= 0);
            //必须先判断online是否有效，如果online无效不能操作socket,FD_ISSET会出错        
            if( (1 == it->second.online) && FD_ISSET(it->first, &readfds) )
            {
                assert(it->first != INVALID_SOCKET);
                fds--;
				memset(_buffer,0,BUF_SIZE);
                iRevLen = recv(it->first , _buffer, BUF_SIZE, 0);
                if(iRevLen <= 0)
                {
                    if(_listener != NULL)
                    {
                    	printf("===TPTCPServer:Heartbeat==recv data error!\n");
                        //这里的锁操作是为了防止死锁,应用层可能在回调接口中调用其他操作
						_mutex.Leave();
                        _listener->onClose(_engineId, it->first); //通知应用层
						_mutex.Enter();
                    }
                    //自动关闭
                    closeClientInside(it->first);
                }
                else
                {
                    _buffer[iRevLen] = 0;
                    if(_listener != NULL)
                    {
                        _mutex.Leave();
                        this->_listener->onData(_engineId, it->first,_buffer,iRevLen);
                        _mutex.Enter();
                    }
                }
            }
            it++;
        }

        //遍历发送队列,看看哪些fd有数据要发送,然后与writefds对比,可以的则发送.
        for (DataQueueIt sIt = _queue.begin(); sIt != _queue.end(); sIt++)
        {
            if(fds && FD_ISSET(sIt->first, &writefds) )
            {
                DataList& list = _queue[sIt->first];
                DataList::iterator dataIt = list.begin();
                while (dataIt != list.end())
                {
                    if (0 == dataIt->data)
                    {
                        printf("===TPTCPServer===Invalid Data row!\n");
                        continue;
                    }
                    ///发送数据，清理异步空间
                    int iRet = sendInside( dataIt->id, dataIt->data, dataIt->len);
                    if (iRet < TP_NORMAL_RET)
                    {
                        ///错误，继续下一个
                        printf("===TPTCPServer===send data <%d> failed!\n", dataIt->id);
                        if (_listener)
                        {
                            _listener->onSendDataFail(_engineId, dataIt->id, dataIt->sequence);
                        }
                        dataIt = list.erase(dataIt);
                    }
                    else
                    {
                        //先改变容器状态，空间后续删除,防止两次操作容器
                        DataRow row = *dataIt;
                        dataIt = list.erase(dataIt);

                        if (_listener)
                        {
                            _mutex.Leave();
                            _listener->onSendDataAck(_engineId, row.id, row.sequence);
                            _mutex.Enter();
                        }
                    } 

                    ///VC7的erase操作返回值在删除容器最后一个元素的时候有问题，这里再判断一次
                    if (list.empty())
                    {
                        ///VC7在empty情况下，如果不Clear,返回的结果有问题！
                        list.clear();
                        dataIt = list.end();
                    }
                }  
            }
        }
        return TP_NORMAL_RET;
    }

    int TPTCPServer::Send(int id, const char *pBuf, size_t iBufLen)
    {
        DataRow row;
        row.id = id;
        row.data = pBuf;
        row.len = iBufLen;

        CGuard guard(_mutex);
        if ( (_clients.find(id) == _clients.end()) || (0 == _clients[id].online) )
        {
            ///sanity check,没有此活跃客户
            printf("===TPTCPServer::Send===No such client!\n");
            return -1;
        }

        row.sequence = getSequence();
        _queue[id].push_back(row);
        return row.sequence;
    }

    ///查找对应的客户ID，并向其发送数据;这里客户id就是其socket
    int TPTCPServer::sendInside(int id, const char *pBuf, size_t iBufLen)
    {
        if (INVALID_SOCKET == id || _queue.find(id) == _queue.end())
        {
            printf("TPTCPServer^_^id %d error!\n",id);
            return TP_INVALID_PARAM;
        }

        if ((0 == iBufLen) || (NULL == pBuf))
        {
            return TP_NORMAL_RET;
        }

        int nwritten = 0;
        unsigned int nleft = iBufLen;

        while (nleft > 0)
        {
            if ( (nwritten = send(id, pBuf, nleft, 0)) <= 0)
            {
                if (nwritten < 0 && errno == 4)
                {
                    nwritten = 0;       
                }
                else
                {
                    break;
                }
            }

            nleft -= nwritten;
            pBuf += nwritten;
        }

        return (iBufLen);
    }

    ///获取某个连接的地址信息
    bool TPTCPServer::GetPeerAddr(int clientId, unsigned long& ip, unsigned short& port)
    {
        CGuard guard(_mutex);
        if (_clients.find(clientId) == _clients.end())
        {
            return false;
        }
        else
        {
            ip = _clients[clientId].ip;
            port = _clients[clientId].port;
            return true;
        }
    }

    void TPTCPServer::DumpBufferdData()
    {
        CGuard guard(_mutex);
        DumpDataQueue(_queue);
    }

    size_t TPTCPServer::GetBufferDataLen()
    {
        CGuard guard(_mutex);
        return GetDataQueueLen(_queue);
    }
}
