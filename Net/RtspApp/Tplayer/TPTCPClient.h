/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPTCPClient.h
* 文件标识：参见配置管理计划书
* 摘　　要：TCP客户端功能类
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/
#ifndef _TPTCPClient_H_
#define _TPTCPClient_H_

#include "ITPObject.h"

namespace CommonLib
{
    class ITPListener;

    class TPTCPClient : public ITPObject
    {
    public:
        TPTCPClient(ITPListener *tcpclientapp, int engineId = 0);
        virtual ~TPTCPClient();

    public:
        /** 关闭该传输链路 */
        virtual int Close(void);

        /** 
        * 初始化socket，并用给定的地址和端口建立传输链路
        * 内部采用50ms定时器非阻塞方式建立连接
        * 如果连接成功则返回socket，否则返回INVALID_SOCKET
        */
        virtual int Connect(const char* ip, int port);    

        /** 附带本地地址和端口的重载版本 */
        virtual int Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort);

		//此接口新加，暂时用于tcpserver中的client子连接，传递套接字给connection
		virtual int ConnectPeer(int iSocket, unsigned int iIp, unsigned short usPort);

        /** 异步数据发送接口，真正的数据发送在心跳中完成 */
        virtual int Send(int id, const char *pBuf, size_t iBufLen);

        /** 心跳操作，用一个select周期检查链路层收发操作，并通知上层 */
        virtual int Heartbeat(void);

        ///implementation
        virtual size_t GetBufferDataLen();
        virtual void DumpBufferdData();

		virtual int SendRawStream(int id, const char *pBuf, size_t iBufLen);
    private:

        int sendInside(int id, const char *pBuf, size_t iBufLen);
        int closeInside();

        DataDeque    _dataList;    /**< 异步队列表 */
    };

}

#endif

