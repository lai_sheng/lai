/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPUDPClient.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：UDP客户端实现类。使用标准Socket实现UDP客户模式。因为使用标准Socket，所以理论上可以运行在各种支持BSD Socket的操作系统。
目前我们的目标是支持：Windows各版本、Linux、ucLinux、PSOS。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年6月28日
* 修订记录： 1. 接口重新改过了
2. 发送修改了，使用队列，然后在Heartbeat中检查socket是否可写，可写时进行发送。 

*
* 取代版本：0.2
* 原作者　：祝晓妮
* 完成日期：2007年4月28日
* 修订记录：创建

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年4月23日
* 修订记录：设计

*/

#include "TPUDPClient.h"

#include "ez_libs/ez_socket/ez_socket.h"

#include "ITPListener.h"

namespace CommonLib
{

    TPUDPClient::TPUDPClient(ITPListener* callback, int engineId):ITPObject(callback, engineId)
    {
    }

    TPUDPClient::~TPUDPClient()
    {
        closeInside();
    }

    int TPUDPClient::Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort)
    {
		_mutex.Enter();
        _localIp = inet_addr((char *)localIp);
        _localPort = htons(localPort);
		_mutex.Leave();

        return Connect(remoteIp, remotePort);
    }

    int TPUDPClient::Connect(const char * ip, int port)
    {
		CGuard guard(_mutex);

        ///校验并保存对方IP和对方端口
        if(ip == NULL)
        {
            ///目的端口为ANY可能是个错误？
            assert(!"Bad remote ip!\n");
            _ip = INADDR_ANY;
        }
        else
        {
            _ip = inet_addr((char *)ip);
        }

        if(port > 0)
        {
            _port = htons(port);
        }
        else
        {
            port = 0;
        }

        //建立socket
        if(INVALID_SOCKET == _socket)
        {
            _socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        }

        struct sockaddr_in my_addr;
        const int yes = 1;

        //Reuse addr option
        if (INVALID_SOCKET
            == setsockopt(_socket,
            SOL_SOCKET,
            SO_REUSEADDR,
            (char *) &yes,
            sizeof(yes)) )
        {
            printf("setsockopt SO_REUSEADDR error~!\n");
            Close();
            return INVALID_SOCKET;
        }

        //设置接收和发送缓冲大小
        setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_recvBuffSize, sizeof(_recvBuffSize));
        setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_sendBufferSize, sizeof(_sendBufferSize));

		struct timeval _time;
    	memset(&_time,0,sizeof(struct timeval));
    	_time.tv_sec = 0;
    	_time.tv_usec = 5000;
		setsockopt(_socket,SOL_SOCKET, SO_SNDTIMEO, (char *)&_time, sizeof(struct timeval));

#ifdef WIN32
        unsigned long l = 1;
        int n = ioctlsocket(_socket, FIONBIO, &l);
        if (n != 0)
        {
            int errcode = WSAGetLastError();
            return -1;
        }
#else
        if (fcntl(_socket, F_SETFL, O_NONBLOCK) == -1)
        {
            printf("fcntl(F_SETFL, O_NONBLOCK)Error\n");
            return -1;
        }
#endif

        memset(&my_addr, 0, sizeof(my_addr));
        my_addr.sin_family = AF_INET;
        my_addr.sin_port = _localPort;
        my_addr.sin_addr.s_addr = _localIp;//MY IP

        if (INVALID_SOCKET
            == bind(_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) )
        {
            printf("udp client bind error\n");
            Close();
            return INVALID_SOCKET;
        }

        return _socket;
    }

    int TPUDPClient::Close()
    {
		CGuard guard(_mutex);

        return closeInside();
    }

    int TPUDPClient::closeInside()
    {
        int iRet = 0;
        if(_socket == INVALID_SOCKET)
        {
            return TP_NORMAL_RET;
        }
		
        iRet = closesocket(_socket);
        _socket = INVALID_SOCKET;
		
		DataDequeIt it = _dataList.begin();
		for (;it != _dataList.end();it ++)
		{
			if (_listener)
			{
				_listener->onSendDataFail(_engineId, it->id, it->sequence);
			}
		}
		
        PurgeData(_dataList);
  
        return iRet;
    }

    //Modified by yanf,10811;070906;Add error detect
    //这里仅仅对一个socket进行select
    int TPUDPClient::Heartbeat()
    {
		CGuard guard(_mutex);
        if(INVALID_SOCKET == _socket)
        {
            return TP_ERROR_BASE;
        }
#if 1	
        /*
        * Select的阻塞操作会导致RtpSession中heartbeat中的锁不能释放，从而
        * 导致调用sendFragmentFrame的线程被挂起，如(回放线程，监视视频的回调
        * 线程等)，下面的修改直接使用sendto发送，发送失败的数据被丢掉，不会
        * 阻塞线程
        */
	    struct sockaddr_in addr;
        int addr_len = sizeof(struct sockaddr_in);
        #ifndef WIN32 
        int iRevLen = recvfrom(_socket,_buffer, BUF_SIZE, 0,(struct sockaddr*)&addr, (socklen_t*)&addr_len);
        #else
        int iRevLen = recvfrom(_socket,_buffer, BUF_SIZE, 0,(struct sockaddr*)&addr, &addr_len);
        #endif

        if(iRevLen < 0)
        {
            //printf("TPUDPClient rev error!\n");
        }
        else if(iRevLen == 0)
        {
            //在UDP环境下不会发生这种情况
            //printf("TPUDPServer revlen=0 want to close\n");
        }
        else
        {
            if(_listener != NULL)
            {
				//tracepoint();
                //这里客户方回调可能调用其他接口，防止自己锁自己
				_mutex.Leave();
                this->_listener->onData(_engineId, _socket, _buffer, iRevLen);
				_mutex.Enter();
            }
        }
		
		DataDequeIt it = _dataList.begin();
        if (it != _dataList.end())
        {
            //拷贝出来，先清理容器，避免回调中重复清导致上层被重复调用
            DataRow row = *it;
            assert( row.data && (row.len > 0) );
			/*
			printf("-----video data seq is %d---------    \n",row.sequence);
			printf("\n");
			*/
       	    int ret = sendInside(row.id, row.data, row.len);		
			
       	    if (ret != row.len)
            {
                //error occurs
                //perror("==TPLayer:TPUDPLClient:heartbeat:sendInside==");
                //tracepoint();
                if(_listener)
                {
                    _listener->onSendDataFail(_engineId, row.id, row.sequence);
                }
            }
            else
            {
				//tracepoint();
                if(_listener)
                {
					_mutex.Leave();
                    _listener->onSendDataAck(_engineId, row.id, row.sequence);
					_mutex.Enter();

                }
            }
			_dataList.pop_front();
        }
		else
		{
			//printf("tplayer has no n\eed data to send,return now==========\n\n");
			return TP_NODATA_TRANS;
		}
		return TP_NORMAL_RET;

#else
        fd_set readfds,writefds;
        FD_ZERO(&writefds);
        FD_ZERO(&readfds);
        FD_SET(_socket, &readfds);
        if (!_dataList.empty())
        {
            FD_SET(_socket, &writefds);
        }

        //check conn status
        struct timeval SelectTimev; 
        size_t milliSec = GetSelectTimer();
        SelectTimev.tv_usec = 1000 * (milliSec%1000);
        SelectTimev.tv_sec = milliSec/1000;

        //锁放在这里更合适
		CGuard guard(_mutex);

        int fdnums = select(_socket+1, &readfds, &writefds, NULL, &SelectTimev);
        if (fdnums < 0)
        {
            printf("client select error\n");
            return TP_NORMAL_RET;
        }
        else if(fdnums == 0)
        {
            //printf("======Timeout cannot write now=====\n");
            return TP_NORMAL_RET;
        }

        //先接收数据
        if (FD_ISSET(_socket, &readfds))
        {
            --fdnums;

            ///接收数据操作，这里并不需要保存对方的IP和端口
            struct sockaddr_in addr;
            int addr_len = sizeof(struct sockaddr_in);
            #ifndef WIN32 
            int iRevLen = recvfrom(_socket,_buffer, BUF_SIZE, 0,(struct sockaddr*)&addr, (socklen_t*)&addr_len);
            #else
            int iRevLen = recvfrom(_socket,_buffer, BUF_SIZE, 0,(struct sockaddr*)&addr, &addr_len);
            #endif

            if(iRevLen < 0)
            {
                printf("TPUDPClient rev error!\n");
            }
            else if(iRevLen == 0)
            {
                //在UDP环境下不会发生这种情况
                printf("TPUDPServer revlen=0 want to close\n");
            }
            else
            {
                if(_listener != NULL)
                {
                    //这里客户方回调可能调用其他接口，防止自己锁自己
					_mutex.Leave();
                    this->_listener->onData(_engineId, _socket, _buffer, iRevLen);
					_mutex.Enter();
                }
            }
        }

        if (fdnums > 0 && FD_ISSET(_socket, &writefds))
        {
            //发数据，还是修改回原来的策略，只发队列头，调度在外部
            DataDequeIt it = _dataList.begin();
            if (it != _dataList.end())
            {
                //拷贝出来，先清理容器，避免回调中重复清导致上层被重复调用
                DataRow row = *it;
                assert( row.data && (row.len > 0) );
                _dataList.pop_front();

                int ret = sendInside(row.id, row.data, row.len);

                if ( static_cast<size_t>(ret) < row.len)
                {
                    //error occurs
                    //perror("==TPLayer:TPUDPLClient:heartbeat:sendInside==");
                    if(_listener)
                    {
                        _listener->onSendDataFail(_engineId, row.id, row.sequence);
                    }
                }
                else
                {
                    if(_listener)
                    {
						_mutex.Leave();
                        _listener->onSendDataAck(_engineId, row.id, row.sequence);
						_mutex.Enter();
                    }
                }
            }
            else
            {
                //空队列
            }
        }
        
        return TP_NORMAL_RET;
#endif
    }

    int TPUDPClient::Send(int id, const char *pBuf, size_t iBufLen)
    {

		CGuard guard(_mutex);
        DataRow row;
        row.id = id;
        row.data = pBuf;
        row.len = iBufLen;

        //如果数据一直来不及发送，就先在这里清理一下，防止队列过度拥挤
        if (_dataList.size() > 100)
        {
            printf("==TPLayer:TPUDPClient:Send== Data queue larger than 100!\n");
            //暂时超过100个数据包就强迫释放,立刻清空缓冲，保证不会一直拥堵
            for (DataDequeIt it = _dataList.begin(); it != _dataList.end(); ++it)
            {
                ///先通知上层才删除,这里是异常删除
                if (_listener)
                {
                    _listener->onSendDataFail(_engineId, id, it->sequence);
                }
            }
            _dataList.clear();
        }

        row.sequence = getSequence();
        _dataList.push_back(row);
        return row.sequence;
    }



	int TPUDPClient::SendRawStream(int id, const char *pBuf, size_t iBufLen)
	{
		if ( !pBuf || iBufLen == 0)
		{
			return TP_ERROR_BASE;
		}
		
		CGuard guard(_mutex);

		if(INVALID_SOCKET == _socket )
		{
			//printf("==TPUDPClient:SendRawStream== Invalid socket!\n ");
			return TP_ERROR_BASE;
		}

		//select操作准备
		fd_set writefds;
		FD_ZERO(&writefds);
		FD_SET(_socket,&writefds);

		//check conn status
		struct timeval SelectTimev; 
		size_t milliSec = 40;
		SelectTimev.tv_usec = 1000 * (milliSec%1000);
		SelectTimev.tv_sec = milliSec/1000;

		int fdnums = select(_socket+1, NULL, &writefds, NULL, &SelectTimev);
		if (fdnums < 0)
		{
			printf("==TPTCPClient:Heartbeat== client select error\n");
			return TP_ERROR_BASE;
		}
#ifndef WIN32
		else if(fdnums == 0)
		{
			//  printf("client^_^select error=%d\n",fdnums);
			return TP_NORMAL_RET;
		}
		else
		{
			//printf("client select result = %d\n",fdnums);
		}
#else
		else if(fdnums ==0)
		{
			//超时
			printf("TPUDPClient::SendRawStream select timeout!\n");
			return 0;
		}
		else
		{
		}
#endif

		//检查内部缓冲待发送的数据，单连接模式仅仅发送队列头
		if (fdnums )
		{
			/*int iRst = */sendInside(0, pBuf, iBufLen);
		}

		return TP_NORMAL_RET;
	}
		
    int TPUDPClient::sendInside(int id, const char *pBuf, size_t iBufLen)
    {
        if (INVALID_SOCKET == _socket)
        {
        	printf("===TPUDPClient::sendInside===bad socket!\n");
            return INVALID_SOCKET;
        }

        if ((0 == iBufLen) || (NULL == pBuf))
        {
        	printf("===TPUDPClient::sendInside===Bad data!\n");
            return TP_NORMAL_RET;
        }

        struct sockaddr_in  ClientAddr;
        memset(&ClientAddr, 0, sizeof(ClientAddr));
        ClientAddr.sin_family = AF_INET;
        ClientAddr.sin_addr.s_addr = _ip;
        ClientAddr.sin_port = _port;

        //if (iBufLen == 8+12 || iBufLen == 5+12)
    	//{
	 //   	printf("###############TPUDPClient::sendInside===0x%x!\n", *(pBuf + 12));
    	//}
		
        return sendto(_socket, (char *)pBuf, iBufLen, 0, (struct sockaddr*)&ClientAddr, sizeof(struct sockaddr));
    }

    void TPUDPClient::DumpBufferdData()
    {
 		CGuard guard(_mutex);

        DumpDataList(_dataList);
    }

    size_t TPUDPClient::GetBufferDataLen()
    {
		CGuard guard(_mutex);
        return GetDataListLen(_dataList);
    }
}
