/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：ITPObject.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：传输层接口抽象类实现。传输层不管是UDP还是TCP，接口完全一样，基类相同。所以可以完全替换，上层代码不用修改。
服务端和客户端接口也很相似，接口风格一样，只是创建时不同。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年4月28日

*
* 取代版本：1.0
* 原作者　：
* 完成日期：
* 修订记录：
*/

#include <iostream>
#include <iomanip>

#include "ITPObject.h"

#include "ez_libs/ez_socket/ez_socket.h"

#ifdef WIN32
//#include <WinSock2.h>
#endif

#include "ITPListener.h"

namespace CommonLib
{
    ITPObject::ITPObject(ITPListener* instance, int engineId)
    {
        this->_listener = instance;

        _buffer = new char[BUF_SIZE];
        assert(_buffer);
        memset(_buffer, 0, BUF_SIZE);

#ifdef WIN32
         WORD version_requested = MAKEWORD (2, 2);
         WSADATA wsa_data;
         int error = WSAStartup (version_requested, &wsa_data);
#endif

        _socket = INVALID_SOCKET;
        _ip     = INADDR_ANY;
        _port   = 0;

        _localPort = 0;
        _localIp = INADDR_ANY;

        _engineId = engineId;
        _sequence = 0;

        //默认50毫秒一个周期
        _selectTimer = 50;

        ///默认32KB缓冲
        _sendBufferSize = 32*1024;
        _recvBuffSize = 32*1024;
    }

    ITPObject::~ITPObject()
    {
        if(_buffer != NULL)
        {
            delete []_buffer;
            _buffer = 0;
        }
#ifdef WIN32
       WSACleanup ();
#endif
    }

    int ITPObject::Close(void)
    {
        return 0;
    }

    int ITPObject::CloseClient(int id)
    {
        return TP_ERROR_UNSUPPORT;
    }

    int ITPObject::Listen(char* ip, int port)
    {
        return TP_ERROR_UNSUPPORT;
    }

    int ITPObject::Connect(const char* ip, int port)
    {
        return TP_ERROR_UNSUPPORT;
    }		

    int ITPObject::Connect(const char* localIp, int localPort, const char* remoteIp, int remotePort)
    {
        return TP_ERROR_UNSUPPORT;
    }

    void ITPObject::SetListener(ITPListener* listener)
    {
        assert(listener != NULL);
        this->_listener = listener;
    }

    int ITPObject::getSequence(void)
    {
        if (_sequence ++ >= 65535)
        {
            _sequence = 0;
        }

        return _sequence;
    }

    ///缓冲类型
    ERR_TYPE ITPObject::SetSocketBufferSize(TPType type, size_t size)
    {
		CGuard guard(_mutex);

        if(size > 64*1024)
        {
            return TP_OUTOF_RANGE;
        }

        if (type == TP_SEND)
        {
            _sendBufferSize = size;
        }
        else if (type == TP_RECEIVE)
        {
            _recvBuffSize = size;
        }
        else
        {
            return TP_INVALID_PARAM;
        }

        return TP_NORMAL_RET;
    }

    ///获取当前的缓冲大小
    size_t ITPObject::GetSocketBufferSize(TPType type)const
    {
        if (type == TP_SEND)
        {
            return _sendBufferSize;
        }
        else if (type == TP_RECEIVE)
        {
            return _recvBuffSize;
        }
        else
        {
            ///never run here
            assert(!"Bad buffer type!\n");
            return 0;
        }
    }

    ERR_TYPE ITPObject::SetSelectTimeout(unsigned long sec, unsigned long usec)
    {
		CGuard guard(_mutex);

        if (sec > 75 || usec > 1000000)
        {
            return TP_OUTOF_RANGE;
        }

        return TP_NORMAL_RET;
    }

    ///Select 操作时间周期
    bool ITPObject::SetSelectTimer(size_t milliSec)
    {
		CGuard guard(_mutex);

        if ( milliSec > 75 * 1000 )
        {
            fprintf(stderr, "==ITPObject:SetSelectTimer== Out of Range!\n");
            return false;
        }
        else
        {
            _selectTimer = milliSec;
            return true;
        }
    }

    //设置接收缓冲大小
    ERR_TYPE ITPObject::SetRecvTPBuffSize(size_t size)
    {
        if (size > 1024*64)
        {
            return TP_OUTOF_RANGE;
        }

		CGuard guard(_mutex);

        _tpRcvBufSize = size;
        char* p = new char[_tpRcvBufSize];
        if (p)
        {
            delete []_buffer;
            _buffer = p;
            return TP_NORMAL_RET;
        }
        else
        {
            return TP_MEM_ERROR;
        }
    }

    ////制定使用外部指针作为缓冲？
    ERR_TYPE ITPObject::SetTPRecvBuffer(char* buff, int size)
    {
        ///这里的外部指针如何释放？应该遵循谁申请，谁释放的原则，否则底层如何释放size大小的内存？
		CGuard guard(_mutex);

        delete []_buffer;
        _buffer = buff;
        _tpRcvBufSize = size;

        return TP_NORMAL_RET;
    }

    ///获取多连接队列数据长度
    size_t GetDataQueueLen(const ITPObject::DataQueue& queue)
    {
        size_t num = 0;
        for (ITPObject::DataQueueCIt it = queue.begin(); it != queue.end(); ++it)
        {
            for (ITPObject::DataList::const_iterator itData = it->second.begin(); itData != it->second.end(); ++itData)
            {
                if (itData->len >= 0) 
                {
                    num += itData->len;
                }
            }
        }
        return num;
    }

    ///获取单连接队列长度
    size_t GetDataListLen(const ITPObject::DataDeque& list)
    {
        size_t num = 0;
        for (ITPObject::DataDeque::const_iterator it = list.begin(); it != list.end(); ++it)
        {
            num += it->len;
        }
        //std::cerr<<"-=-=-=List len:"<<num<<std::endl;
        return num;
    }

    void DumpDataQueue(const ITPObject::DataQueue& queue)
    {
        using std::cerr;
        using std::endl;
        using std::setw;

        cerr<<"################# Data queue data Dump ###############"<<endl;
        if (queue.empty())
        {
            cerr<<"########## Empty internal buffer!\n"<<endl;
            return;
        }

        cerr<<"====id======data addr====len=====seq==socket"<<endl;
        for (ITPObject::DataQueueCIt it = queue.begin(); it != queue.end(); ++it)
        {
            for (ITPObject::DataList::const_iterator itData = it->second.begin();
                itData != it->second.end(); ++itData)
            {
                DataRow pElem = *itData;
                cerr<<setw(6)<<pElem.id;
                cerr<<setw(15)<<(unsigned long)pElem.data;
                cerr<<setw(7)<<pElem.len<<setw(8)<<pElem.sequence<<setw(8)<<it->first<<endl;
            }
        }
        cerr<<"################################"<<endl;
    }

    void DumpDataList(const ITPObject::DataDeque& list)
    {
        using std::cerr;
        using std::endl;
        using std::setw;

        cerr<<"################# Data List data Dump ###############"<<endl;
        if (list.empty())
        {
            cerr<<"########## Empty internal buffer!\n"<<endl;
            return;
        }

        cerr<<"====id======data addr====len=====seq=="<<endl;
        for (ITPObject::DataDeque::const_iterator it = list.begin(); it != list.end(); ++it)
        {
            DataRow pElem = *it;
            cerr<<setw(6)<<pElem.id;
            cerr<<setw(15)<<(unsigned long)pElem.data;
            cerr<<setw(7)<<pElem.len<<setw(8)<<pElem.sequence<<setw(8)<<endl;
        }
        cerr<<"################################"<<endl;
    }


    void PurgeData(ITPObject::DataDeque& list)
    {
        //for (ITPObject::DataDeque::const_iterator it = list.begin(); it != list.end(); ++it)
        //{
        //    delete *it;
        //}
        list.clear();
    }   
}
