/*
* Copyright (c) 2007, 浙江大华技术股份有限公司
* All rights reserved.
*
* 文件名称：TPUDPServer.cpp
* 文件标识：参见配置管理计划书
* 摘　　要：UDP客户端实现类。使用标准Socket实现UDP服务器模式。因为使用标准Socket，所以理论上可以运行在各种支持BSD Socket的操作系统。
目前我们的目标是支持：Windows各版本、Linux、ucLinux、PSOS。
*
* 当前版本：1.0
* 作　　者：李明江
* 完成日期：2007年6月28日
* 修订记录： 1. 接口重新改过了
2. 发送修改了，使用队列，然后在Heartbeat中检查socket是否可写，可写时进行发送。 

*
* 取代版本：0.2
* 原作者　：祝晓妮
* 完成日期：2007年4月28日
* 修订记录：创建

*
* 取代版本：0.1
* 原作者　：李明江
* 完成日期：2007年4月23日
* 修订记录：设计

*/

#include "TPUDPServer.h"

#include "ez_libs/ez_socket/ez_socket.h"
#include "ITPListener.h"

namespace CommonLib
{
    TPUDPServer::TPUDPServer(ITPListener * UDPserverapp, int engineId) : ITPObject(UDPserverapp, engineId)
    {
    }

    TPUDPServer::~TPUDPServer()
    {
        closeInside();
    }

    ///UDP本身是无连接的，因此这里仅仅是绑定socket到相关的端口
    int TPUDPServer::Listen(char* ip, int port)
    {
		CGuard guard(_mutex);

        if(ip == NULL)
        {
            _ip = INADDR_ANY;
        }
        else
        {
            _ip = inet_addr(ip);
        }
        _port = htons(port);

        if(INVALID_SOCKET == _socket)
        {
            _socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        }

        struct sockaddr_in my_addr;
        const int yes = 1;

        if (INVALID_SOCKET
            == setsockopt(_socket,
            SOL_SOCKET,
            SO_REUSEADDR,
            (char *) &yes,
            sizeof(yes)) )
        {
            printf("setsockopt error\n");
            closeInside();
            return INVALID_SOCKET;
        }

        //设置接收和发送缓冲大小
        setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_recvBuffSize, sizeof(_recvBuffSize));
        setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_sendBufferSize, sizeof(_sendBufferSize));

        //设置非阻塞
        #ifdef WIN32
        unsigned long l = 1;
        int n = ioctlsocket(_socket, FIONBIO, &l);
        if (n != 0)
        {
            int errcode;
            errcode = WSAGetLastError();
            return -1;
        }
        #else
        if (fcntl(_socket, F_SETFL, O_NONBLOCK) == -1)
        {
            printf("fcntl(F_SETFL, O_NONBLOCK) Error\n");
        }
        #endif

        memset(&my_addr, 0, sizeof(my_addr));
        my_addr.sin_family = AF_INET;
        my_addr.sin_port = _port;
        my_addr.sin_addr.s_addr = _ip;

        if (INVALID_SOCKET
            == bind(_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) )
        {
            printf("UDP server bind error\n");
            closeInside();
            return INVALID_SOCKET;
        }

        return _socket;
    }

    ///udp 仅仅使用一个socket
    int TPUDPServer::CloseClient(int id)
    {    
		CGuard guard(_mutex);

        if (_clients.find(id) == _clients.end())
        {
            return TP_NORMAL_RET;
        }

        return TP_NORMAL_RET;
    }

    int TPUDPServer::Close()
    {
  		CGuard guard(_mutex);

        return closeInside();
    }

    int TPUDPServer::closeInside()
    {
        int iRet = 0;
        if(_socket == INVALID_SOCKET)
        {
            return TP_NORMAL_RET;
        }

        iRet = closesocket(_socket);
        _socket = INVALID_SOCKET;

        ///清理异步队列数据
        PurgeData(_dataList);
        _dataList.clear();
        _clients.clear();

        return iRet;
    }

    ///这里的实现策略是多个连接都用一个socket来收发
    int TPUDPServer::Heartbeat()
    {
        if(INVALID_SOCKET == _socket)
        {
            return TP_ERROR_BASE;
        }

        //填入select监听集
        fd_set readfds;
        fd_set writefds;
        FD_ZERO(&readfds);
        FD_ZERO(&writefds);
        FD_SET(_socket, &readfds);

        if (!_dataList.empty())
        {
            FD_SET(_socket, &writefds);
        }

		CGuard guard(_mutex);

        int maxFd = _socket;

        //select时间
        struct timeval SelectTimev; 
        size_t milliSec = GetSelectTimer();
        SelectTimev.tv_usec = 1000 * (milliSec%1000);
        SelectTimev.tv_sec = milliSec/1000;
        int fds = select(maxFd + 1, &readfds, &writefds, NULL, &SelectTimev);

        if (fds < 0)
        {
            printf("select < 0, error\n");
            return -1;
        }
        else if (fds == 0)
        {//
            //printf("TPUDPServer  select = 0\n");
            return TP_NORMAL_RET;
        }
        else
        {
        }

        if(fds && FD_ISSET(_socket,&readfds))
        {
            fds--;

            struct sockaddr_in addr;
            int addr_len = sizeof(struct sockaddr_in);

            #ifndef WIN32 
            int iRevLen = recvfrom(_socket,_buffer,BUF_SIZE, 0,(struct sockaddr*)&addr, (socklen_t*)&addr_len);
            #else
            int iRevLen = recvfrom(_socket,_buffer,BUF_SIZE, 0,(struct sockaddr*)&addr, &addr_len);
            #endif

            if(iRevLen < 0)
            {
                printf("TPUDPServer rev error!\n");
            }
            else if(iRevLen == 0)
            {
                //在UDP环境下不会发生这种情况
                printf("TPUDPServer revlen=0 want to close\n");
            }
            else
            {
                unsigned int peerPort = addr.sin_port;
                unsigned long peerIp = addr.sin_addr.s_addr;
                size_t clientId = 0;
          
                //查看是否是第一次上来的连接,UDP本身没有连接的概念
				ClientQueueIt it = _clients.begin();
				for(; it != _clients.end(); ++it)
				{
					if( (it->second.ip == peerIp) && (it->second.port == peerPort) && it->second.online )
					{
						//无需查找了,为某一个子连接
                        clientId = it->first;
						break;
					}
				}

				//client 队列不存在这样的地址，应该是一个新连接上来了;查找或生成一个合适的ID
				if (it == _clients.end())
				{				 
					for(it = _clients.begin(); it != _clients.end(); ++it)
					{
						//先查找以前曾被关闭的该地址的连接复用
						if(it->second.online == 0)
						{
							printf("the id %d is invalid ,use it again\n",it->first);
							clientId = it->first;
						}
					}

					if (it == _clients.end())
					{
						//放在队列尾部
                        //printf("====== New client now at:[%#x:%u] , add at the end\n",
						//	peerIp, peerPort);
						//直接放到最后
						clientId = _clients.size();
					}
                    
                    ///回调处理连接请求
                    int iRet = 0;
                    if(_listener != NULL)
                    {
                        char* remIp = inet_ntoa(addr.sin_addr);
                        iRet = this->_listener->onConnect(_engineId, clientId, remIp, ntohs(peerPort));
                    }

                    ///初始化
                    if(0 == iRet)
                    {
                        ClientInfo& info = _clients[clientId];
                        info.ip = peerIp;
                        info.port = peerPort;
                        info.online = 1;
                        info.socket = _socket;
                        info.timemark = 0;
                    }
                }

				//处理来自该客户方的数据
				if(_listener != NULL)
				{
                    //如果OnData中客户端发数据，可能导致线程循环锁问题
					_mutex.Leave();
					this->_listener->onData(_engineId, clientId,_buffer, iRevLen);
					_mutex.Enter();
				}
            }
        }
		
		if (fds > 0)
		{
            //现处理队列中所有客户的发送请求
            for (DataDequeIt sIt = _dataList.begin(); sIt != _dataList.end(); ++sIt)
            {
                DataRow row = *sIt;
                sIt = _dataList.erase(sIt);
                int ilen = sendInside(row.id, row.data, row.len);
                if (static_cast<size_t>(ilen) < row.len)
                {
#ifdef WIN32
                    printf("======TPLayer=== udp sendto error:<%d>\n", WSAGetLastError());
#else
                    perror("======TPLayer=== udp sendto error");
#endif
                    if (_listener)
                    {
                        _listener->onSendDataFail(_engineId, row.id, row.sequence);
                    }
                }
                else
                {
                    if(_listener)
                    {
						_mutex.Leave();
                        _listener->onSendDataAck(_engineId, row.id, row.sequence);
						_mutex.Enter();
                    }
                }

                ///VC7 need this sanity check
                if (_dataList.empty())
                {
                    //_dataList.clear();
                    //sIt = _dataList.end();
                    break;
                }
            }
		}
        
        return TP_NORMAL_RET;
    }

    int TPUDPServer::Send(int id,const char * pBuf, size_t iBufLen)
    {
		CGuard guard(_mutex);

        DataRow row;
        row.id = id;
        row.data = pBuf;
        row.len = iBufLen;
        row.sequence = getSequence();
        _dataList.push_back(row);
        return row.sequence;
    }

    int TPUDPServer::sendInside(int id, const char *pBuf, size_t iBufLen)
    {
        if( _clients.find(id) == _clients.end() )
        {
            printf("id %d error!\n",id);
            return TP_ERROR_BASE;
        }

        ClientInfo& info = _clients[id];

        struct sockaddr_in  peerAddr;
        memset(&peerAddr, 0, sizeof(peerAddr));

        peerAddr.sin_family = AF_INET;
        peerAddr.sin_addr.s_addr = info.ip;
        peerAddr.sin_port = info.port;

        unsigned int addr_len = sizeof(struct sockaddr);

        return sendto(_socket, (char *)pBuf, iBufLen, 0, (struct sockaddr *)(&peerAddr), addr_len);
    }

    void TPUDPServer::DumpBufferdData()
    {
 		CGuard guard(_mutex);

        DumpDataList(_dataList);
    }

    size_t TPUDPServer::GetBufferDataLen()
    {
		CGuard guard(_mutex);

        return GetDataListLen(_dataList);
    }
}
