/*!********************************************************************************************************
**                  Rtsp Module for General Network
***********************************************************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              FrameGeneral.cpp
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-5-13   8:33
**Modified:               
**Modify Reason: 
**Description:           录像加入,从底层上来的数据没有包头,目前是每次上来的packet都是大小为32K数据包,为了方
**                       便组包,先把这些没头的录像数据组帧,然后封装成有头的packet包,为了方便,第个packet只有
**                       一帧.
**********************************************************************************************************/

#include "FrameGeneral.h"

int CFrameGeneral::AddPacket(CPacket *sPtk)
{
    if (!sPtk)
    {
        VD_TRACE("sPtk is NULL!");
        return IRET_ERROR;
    }
	
	if (m_PtkLeftLen + sPtk->GetLength() > FRAME_PACKET_LEN)
	{
	    Init();
	    VD_TRACE("addPacket drop data,m_PtkLeftLen = %d,sPtk->GetLength() = %d\n", m_PtkLeftLen, sPtk->GetLength());
	    return IRET_ERROR;
	}
	
    char *pareBuf = (char *)m_parsePtk->GetBuffer();
	int offset = m_parsePtk->GetLength() - m_PtkLeftLen;
	
	memmove(pareBuf, pareBuf + offset, m_PtkLeftLen);
	memcpy(pareBuf + m_PtkLeftLen, sPtk->GetBuffer(), sPtk->GetLength());
	
	m_PtkLeftLen += sPtk->GetLength();	
	m_parsePtk->SetLength(m_PtkLeftLen);
	
    return IRET_SUCCESS;
}


int CFrameGeneral::Parse(CPacket *& dPtk, int beginPos, int &dstPos)
{
    char *pareBuf = (char *)m_parsePtk->GetBuffer();
	int ptkLen = m_parsePtk->GetLength();
	if (beginPos >= (FRAME_PACKET_LEN - FRAME_HEADER_LEN))
	{
	    VD_TRACE("Buffer is not enough,need reset!\n");
	    Init();
	    return IRET_ERROR;
	}
	if ( m_PtkLeftLen < FRAME_HEADER_LEN)
	{
	    //VD_TRACE("Need add data,m_PtkLeftLen <%d>\n",m_PtkLeftLen);
	    return IRET_ERROR;
	}
	
	
    int frameType = FindFrameHeader(pareBuf + beginPos, ptkLen - beginPos, dstPos);
	if (PACK_TYPE_FRAME_NULL == frameType)
	{
	    VD_TRACE("FindFrameHeader error,LINE %d, FILE %s\n",__LINE__,__FILE__);
		Init();
	    return IRET_ERROR;
	}
    
	int frameLen = FrameAtoi(&((pareBuf + beginPos + dstPos)[frameLenOff[frameType]]), frameType) + frameHeaderLen[frameType];
	/*
	VD_TRACE("dstPos <%d> frameType<%d> off<%d> frameLen<%d>\n",
		dstPos,frameType,frameLenOff[frameType],frameLen);
	*/
	if(frameLen > FRAME_PACKET_LEN/2)
	{
	    //帧长度错误
	    VD_TRACE("Frame Len is too much!<%d> <%d>\n", frameLen, frameType);
		VDDumpHex(&((pareBuf + beginPos + dstPos)[frameLenOff[frameType]]), 16);
	    Init();
	    return IRET_ERROR;
	}
	int curLeftLen = ptkLen - beginPos - dstPos;
	//VD_TRACE("curLeftLen = %d,frameLen = %d\n", curLeftLen, frameLen);
	if (curLeftLen < frameLen)
	{
	    //VD_TRACE("frameLen is not enough!\n");
		return IRET_ERROR;
	}
	dPtk = g_PacketManager.GetPacket(frameLen);
	if(!dPtk)
	{
	
	    VD_TRACE("g_PacketManager.GetPacket error,Line <%d>!\n",__LINE__);
	    Init();
		return IRET_ERROR;
	}
	
	dPtk->ClearHeader();
	PKT_HEAD_INFO *dPktHeader = (PKT_HEAD_INFO *)dPtk->GetHeader();
	for (int ii = 0; ii < FRAME_MAX_NUM; ii++)
	{
		dPktHeader->FrameInfo[ii].FrameType = PACK_TYPE_FRAME_NULL;
	}
	dPktHeader->FrameInfo[0].FrameType = frameType;
	dPktHeader->FrameInfo[0].FrameFlag = 3;//包含帧头和帧尾
    dPktHeader->FrameInfo[0].FramePos = 0;
	dPktHeader->FrameInfo[0].FrameLength = frameLen ;
	dPktHeader->FrameInfo[0].DataLength = frameLen ;

	memset(dPtk->GetBuffer(),0,dPtk->GetSize());
	memcpy(dPtk->GetBuffer(), pareBuf + beginPos + dstPos, frameLen);
	
	dPtk->SetLength(frameLen );
	m_PtkLeftLen = ptkLen - beginPos - dstPos - frameLen ;
	dstPos += (beginPos + frameLen);
	
	//VD_TRACE("PtkLeftLen = %d,size = %d nStart = %d\n\n",m_PtkLeftLen,dPtk->GetLength(),dstPos);

	return IRET_SUCCESS;
}

enum PackTypeFlag CFrameGeneral::FindFrameHeader(char *buf, int len, int &pos)
{
    if (!buf)
    {
        return PACK_TYPE_FRAME_NULL;
    }
    //VDDumpHex(buf,32);
    char *ptr = buf;
	int curPos = 3;
	
	while (curPos < len)
	{		
		for(int iFrameType = 0; iFrameType < FRAME_TYPE_NUM; iFrameType++)
		{
		    //VDDumpHex(Frame_Key_Word[iFrameType],4);
			//VDDumpHex(ptr,5);
			char *keyWord = (char *)(&Frame_Key_Word[iFrameType]);
		    if (keyWord[0] == ptr[0])
	        {
	            if (!memcmp(&ptr[1], &keyWord[1], KEY_WORD_LEN - 1))
	            {
	                pos = curPos - 3;
	                return (enum PackTypeFlag)iFrameType;
	            }
	        }
			
		}
		
		ptr++;
		curPos++ ;
	}
	return PACK_TYPE_FRAME_NULL;
}
#if 0
#include <stdio.h>
#include <assert.h>

#define FRAME_GENERAL_BUFFER_LEN 1024*32
void TestFrameGeneral()
{
    CFrameGeneral test;
    FILE *fp = fopen("test.dav","r");
    FILE *wFp = fopen("write.dav","w");
	
	CPacket *sPtk = g_PacketManager.GetPacket(FRAME_GENERAL_BUFFER_LEN);
	CPacket *dPtk = NULL;
	char *buf = (char *)sPtk->GetBuffer();
	
	
	int len = fread(buf,1,FRAME_GENERAL_BUFFER_LEN,fp);
	sPtk->SetLength(len);
	VD_TRACE("Read len < %d>\n",len);
	
	while(len > 0)
	{
	    int pos = 0;
	    test.AddPacket(sPtk);
		while(CFrameGeneral::IRET_SUCCESS == test.Parse(dPtk,pos,pos))
		{
		    if(dPtk)
			{
			    fwrite(dPtk->GetBuffer(),1,dPtk->GetLength(),wFp);
			    dPtk->Release();
				dPtk = NULL;
			}
		}
		
		memset(buf,0,FRAME_GENERAL_BUFFER_LEN);
		len = fread(buf,1,FRAME_GENERAL_BUFFER_LEN,fp);
		sPtk->SetLength(len);
		VD_TRACE("Read len < %d>\n",len);
	}
	fclose(fp);
	fclose(wFp);
	sPtk->Release();
	assert(0);
}

#endif

