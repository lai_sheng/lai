/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              RtspThread.h
**Version:                  Version 0.01
**Author:                  
**Created:                
**Modified:               yang_shukui,2010-4-27   9:40
**Modify Reason:          重新整理rtsp部分的代码，以提高通用rtsp服务的可维护性；拟加入rtspClient端的支持。
**Description:            CRtspSrv提供rtsp server/client 端的服务。
*********************************************************************/

#include "RtspThread.h"
#include "OprPdu/RtspPdu.h"
#include "BaseFunc/VDDebug.h"
#include "BaseFunc/VDDelete.h"


PATTERN_SINGLETON_IMPLEMENT(CRtspSrv);

CRtspSrv::CRtspSrv() : CThread("RtspSrv", TP_NET)
{
    m_RtspMediaService = new CRtspMediaService();
    //VD_ASSERT(m_RtspMediaService);
    m_TPServer = NULL;

    CConfigNetRtspSet rtspConfig;
    rtspConfig.update();
    RTSP_SET &RtspSet = rtspConfig.getConfig();
    Start(RtspSet.usListernPort);
}
    
CRtspSrv::~CRtspSrv()
{
    Stop();
	if(m_RtspMediaService)
	{
	   	VD_DELETE(m_RtspMediaService);
		m_RtspMediaService = NULL; //zsliu
	}
#ifdef WIN32
    WSACleanup();
#endif
}
    
VD_BOOL CRtspSrv::Start(unsigned short port)
{
    CConfigNetRtspSet rtspConfig;
    rtspConfig.update();
    RTSP_SET &RtspSet = rtspConfig.getConfig();
    if(!RtspSet.usEnable)
    {
        VD_TRACE("Rtsp module is disEnable\n");
        return VD_TRUE;
    }
    
    //保证不多次重建线程
    if (m_bLoop)
    {
        return TRUE;
    }
    
    int iSocket = INVALID_SOCKET;
    if(!m_TPServer)
    {
        //engineId 设置为0,只有一个应用，engineId的值不用关心
        m_TPServer = new CommonLib::TPTCPServer(this,0);
        if(!m_TPServer)
        {
            VD_TRACE("CRtspSrv create TcpServer error!\n");
            return VD_FALSE;
        }

        //用设备自身的IP
        iSocket = m_TPServer->Listen(NULL,port);
        if(INVALID_SOCKET  == iSocket)
        {
            VD_TRACE("CRtspSrv Listen port <%d> failed!\n",port);
            VD_DELETE(m_TPServer);
            return FALSE;
        }
    }
    
    VD_BOOL bRet = FALSE;
    bRet = CreateThread();
    if(!bRet)
    {
        VD_TRACE("CRtspSrv Start, CreateThread failed!\n");
        VD_DELETE(m_TPServer);
        return FALSE;
    }
    if(m_RtspMediaService)
    {
        m_RtspMediaService->SetMaxConn(RTSP_CONN_NUM);
    }
    m_RtspSvrAck.SetPara(RTSP_CONN_NUM,16);
    trace("CRtspSrv::CRtspSrv Start Successful,<%d> is listennig on <%d>\n", iSocket,port);
    return TRUE;
}

VD_BOOL CRtspSrv::Stop()
{
    VD_DELETE(m_TPServer);
    if(m_RtspMediaService)
    {
        m_RtspMediaService->Stop(VD_FALSE);
    }
    m_RtspSvrAck.Clear();

    if (!m_bLoop)
    {
        return true;
    }
    DestroyThread();
    
    return true;
}
int CRtspSrv::CloseClient(int connId)
{
    if(!m_TPServer)
    {
        VD_TRACE("m_TPServer is NULL\n");
        return -1;
    }
    return m_TPServer->CloseClient(connId);
}

void CRtspSrv::ThreadProc()
{
    while( m_bLoop)
    {    
        m_TPServer->Heartbeat();
        /* 
            m_TPServer->Heartbeat()中使用了select，cpu的占用率不会很高，但在这里面sleep 100ms不会影响消息交互,
            却可以明显降低cpu的占用率
        */
        SystemSleep(10);
    }
}

int CRtspSrv::onData(int engineId, int connId, const char* data, size_t len)
{
    if (len > 0 && data[0] >= 'A' && data[0] <= 'z')
    {
        VD_TRACE("\n========================OnData<%d %d>============================\n%s\n", connId, len, data);
    }

    if(m_RtspMediaService)
    {
        m_RtspMediaService->DoMsg(engineId, connId, data, len);
    }
    return 0;
}
int CRtspSrv::onClose(int engineId, int connId) 
{
    if(m_RtspMediaService)
    {
        m_RtspMediaService->DelRtspSession(connId,CRtspSession::RELEASE_NEED);
    }
    return 0;
}
int CRtspSrv::onConnect(int engineId, int connId, const char* ip, int port) 
{
    VD_TRACE("RtspConneted,engineId <%d>, connId <%d>, ip <%s>, port <%d>\n", engineId, connId, ip, port);
    CRtspSession *rtspSession = new CRtspSessionSvr();
    if(!rtspSession)
    {
        VD_TRACE("Create RtspSession failed!\n");
        return -1;
    }
    rtspSession->SetRtspSocket(connId);
    rtspSession->SetPeerIp(ip);
    
    if(m_RtspMediaService)
    {
        m_RtspMediaService->AddRtspSession(rtspSession);
    }
    
    return 0;
}

int CRtspSrv::onSendDataAck(int engineId, int connId, int id)
{
    m_RtspSvrAck.Delete(connId,id);
    return 0;
}

int CRtspSrv::onSendDataFail(int engineId, int connId, int id)
{
    m_RtspSvrAck.Delete(connId,id);
    return 0;
}

int CRtspSrv::TPSendPdu(int connId, CRtspPdu *rtspPdu)
{
    if(!rtspPdu)
    {
        return -1;
    }
    
    std::string& strBody = rtspPdu->packetBody();
    char* pStream = (char *)strBody.c_str();
    int iStreamLen = strBody.length();
    int seq = m_TPServer->Send(connId,pStream,iStreamLen);
    if(seq > 0)
    {
        m_RtspSvrAck.Add(connId,seq,rtspPdu);
        VD_TRACE("#######################TPRtspOutput####################\n%s",pStream);
    }
    else
    {
        delete rtspPdu;
    }
    return 0;
}

/*  对于异步发送数据的缓冲内存，只要是用到了TPLayer,都必须对缓冲内存进行管理，即调用TpLayte的send后，将发送缓冲保存，
**  再在onSendDataFail和onSendDataAck里面释放。为了提高代码的共用性，最好是在TPLayer里面做。也可以用Send，直接发,这
**  时可以空实现onSendDataFail和onSendDataAck。
*/
int CRtspSrv::SendPdu(int connId,CRtspPdu *rtspPdu)
{
    if(!rtspPdu)
    {
        return -1;
    }
    std::string& strBody = rtspPdu->packetBody();
    char* pStream = (char *)strBody.c_str();
    int iStreamLen = strBody.length();

    VD_TRACE("#######################RtspOutput####################\n%s",pStream);

    return VDSocketTcpSend(connId, pStream, iStreamLen);
}

int CRtspSrv::SendMsg(int connId, const char *data, int len)
{
    if (!data || len <= 0)
    {
        return 0;
    }
	
	char* pStream = (char *)data;
	int iStreamLen = len;
	VD_TRACE("#######################TPSendMsgOutput####################\n%s",pStream);
	return VDSocketTcpSend(connId, pStream, iStreamLen);
}


int VDSocketTcpSend(int connId,const char* pData,int len)
{
    int ret = -1;
    int sendlen = 0;
    struct timeval timetmp;
    fd_set writefd;
    
    char *ptr = (char*) pData;

    while( sendlen < len )
    {
        FD_ZERO(&writefd);
        if(connId <= 0)
        {
            return -1;
            VD_TRACE("CRtspSrv::Send,connId <= 0\n");
        }
        FD_SET(connId, &writefd);

        timetmp.tv_sec = 0;
        timetmp.tv_usec = 200000;

        ret = select(connId+1, NULL, &writefd, NULL, &timetmp);
        if(ret > 0)
        {
            ret = send(connId, ptr+sendlen, len-sendlen, 0);
            sendlen += ret;
        }
        else
        {
            return -1;
        }
    }
    return sendlen;
}

int VDSocketUdpSend(int connId,const char* pData,int len,char *ip,unsigned short port)
{
    int sendLen = -1;
    fd_set writefds;
    FD_ZERO(&writefds);    
    
    FD_SET(connId, &writefds);  
    struct timeval SelectTimev; 
    SelectTimev.tv_usec = 200000 ;
    SelectTimev.tv_sec = 0;
    int fds = select(connId + 1, NULL, &writefds, NULL, &SelectTimev);
    if(fds < 0)
    {
        VD_TRACE("VDSocketUdpSend^_^select error\n");
        return -1;
    }
    else if(fds == 0)
    {
        return -1;
    } 
    
    if( FD_ISSET(connId, &writefds) )
    {
        struct sockaddr_in  ClientAddr;
        memset(&ClientAddr, 0, sizeof(ClientAddr));
        ClientAddr.sin_family = AF_INET;
        ClientAddr.sin_addr.s_addr = inet_addr(ip);
        ClientAddr.sin_port = htons(port);
        sendLen = sendto(connId, pData, len, 0, (struct sockaddr*)&ClientAddr, sizeof(struct sockaddr));
    }
    
    return (sendLen > 0 ? sendLen : -1);
}

int VDCreateUdpSocket(char * ip,unsigned short port)
{
    int _socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    struct sockaddr_in my_addr;
    const int yes = 1;
    int _socketBuffSize = 32 * 1024;

    //Reuse addr option
    if (INVALID_SOCKET
        == setsockopt(_socket,
        SOL_SOCKET,
        SO_REUSEADDR,
        (char *) &yes,
        sizeof(yes)) )
    {
        VD_TRACE("setsockopt SO_REUSEADDR error~!\n");
        closesocket(_socket);
        return INVALID_SOCKET;
    }
    //设置接收和发送缓冲大小
    setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&_socketBuffSize, sizeof(_socketBuffSize));
    setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&_socketBuffSize, sizeof(_socketBuffSize));

    #ifdef WIN32
        unsigned long l = 1;
        int n = ioctlsocket(_socket, FIONBIO, &l);
        if (n != 0)
        {
            int errcode = WSAGetLastError();
            return -1;
        }
    #else
        //设置非阻塞属性
        if (fcntl(_socket, F_SETFL, O_NONBLOCK) == -1)
        {
            VD_TRACE("fcntl(F_SETFL, O_NONBLOCK)Error\n");
            return -1;
        }
    #endif
    

    memset(&my_addr, 0, sizeof(my_addr));
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr = inet_addr(ip);;//MY IP

    if (INVALID_SOCKET
        == bind(_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) )
    {
        VD_TRACE("udp client bind error\n");
        close(_socket);
        return INVALID_SOCKET;
    }
    VD_TRACE("Create UDP socket <%d>, ip<%s>, port<%d>\n",_socket,ip,port);
    return _socket;
}


