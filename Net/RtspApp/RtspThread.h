/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              RtspThread.h
**Version:	              Version 0.01
**Author:	              
**Created:                
**Modified:               yang_shukui,2010-4-27   9:40
**Modify Reason:          重新整理rtsp部分的代码，以提高通用rtsp服务的可维护性；拟加入rtspClient端的支持。
**Description:            CRtspSrv提供rtsp server/client 端的服务。
*********************************************************************/

#ifndef __VD_RTSP_SVR_H__
#define __VD_RTSP_SVR_H__

#include "MultiTask/Thread.h"
#include "MultiTask/Mutex.h"
#include "System/Object.h"
#include "System/ABuffer.h"
#include "RtspMediaService.h"
#include "OprPdu/RtspPdu.h"
#include "RtspSession.h"
#include "BaseFunc/VDDebug.h"
#include "TPAckManager.h"
#include "Tplayer/ITPListener.h"
#include "Tplayer/TPTCPServer.h"

#define RTSP_LISTEN_PORT 554    //rtsp服务端lister的端口

#ifdef FUNCTION_SUPPORT_RTSP_MULTICAST
#define RTSP_CONN_NUM    160     //多播情况下需要建立很多的rtsp连接，虽然每个通道只是发送1分码流
#else
#define RTSP_CONN_NUM    10     //rtsp会话的最大个数，包括server端和client端的实时监视和录像回放
#endif

//直接调用socket函数发送
int VDSocketTcpSend(int connId,const char* pData,int len);
int VDSocketUdpSend(int connId,const char* pData,int len,char *ip,unsigned short port);
int VDCreateUdpSocket(char * ip,unsigned short port);

class CRtspSrv : public CThread, public CommonLib::ITPListener 
{
public:
	PATTERN_SINGLETON_DECLARE(CRtspSrv);
	
public:
	virtual void ThreadProc();

	/* 开启线程 */
	VD_BOOL Start(unsigned short port = RTSP_LISTEN_PORT);

	/* 停止线程 */
	VD_BOOL Stop();

    //异步缓冲发送 
    int TPSendPdu(int connId, CRtspPdu *rtspPdu);
	
	//不通过异步缓冲，直接发送
    int SendPdu(int connId,CRtspPdu *rtspPdu);
	int SendMsg(int connId, const char *data, int len);	
	
	CRtspSrv();
	~CRtspSrv();
	int CloseClient(int connId);
	int onClose(int engineId, int connId); 
	int onConnect(int engineId, int connId, const char* ip, int port);
	int onSendDataAck(int engineId, int connId, int id);
	int onSendDataFail(int engineId, int connId, int id);
	int onData(int engineId, int connId, const char* data, size_t len);
    
private:	
    //用于异步RTSP信令发送缓冲管理
	CTPAckManager<CRtspPdu> m_RtspSvrAck;
	
	CRtspMediaService *m_RtspMediaService;	
    CommonLib::TPTCPServer *m_TPServer;
};

#define g_RtspSvr (*CRtspSrv::instance())

#endif


