/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**		(c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**						  All Rights Reserved
**
**File Name:              TaskTimer.cpp
**Version:	              Version 0.01
**Author:	              yang_shukui
**Created:                2010-5-7   8:40
**Modified:               
**Modify Reason: 
**Description:            为媒体数据的发送做时间处理，分配给本任务的时间，
**                        就退出本任务的循环
*********************************************************************/

#include "TaskTimer.h"

#if defined(_MSC_VER) || defined(_WINDOWS_)
   #include   <windows.h>
   int gettimeofday(struct timeval* tv,void *tz) 
   {
      union {
         long long ns100;
         FILETIME ft;
      } now;
     
      GetSystemTimeAsFileTime (&now.ft);
      tv->tv_usec = (long) ((now.ns100 / 10LL) % 1000000LL);
      tv->tv_sec = (long) ((now.ns100 - 116444736000000000LL) / 10000000LL);
     return (0);
   }
#endif

unsigned int VD_GetMSCount(void)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec*1000 + tv.tv_usec/1000;
}

