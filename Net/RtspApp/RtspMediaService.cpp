/*!*******************************************************************
**                  Debug Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              CRtspMediaService.cpp
**Version:                  Version 0.01
**Author:                  yang_shukui
**Created:                2010-4-27   13:50
**Modified:               
**Modify Reason: 
**Description:            处理rtsp server/client 媒体服务
*********************************************************************/
#include "RtspMediaService.h"
#include "BaseFunc/VDDebug.h"
#include "BaseFunc/VDDelete.h"
#include "RtspThread.h"
#include "PortPool.h"
#include "System/Console.h"

PATTERN_SINGLETON_IMPLEMENT(CRtspMediaService);

CRtspMediaService::CRtspMediaService():CThread("RtspMediaService", TP_NET)
{
    m_ConnIdNum = -1;
    m_RtspSvrClt = NULL;
    
    m_RtspConfig = new CConfigNetRtspSet();
    VD_ASSERT(m_RtspConfig);
    m_RtspConfig->update();
    m_RtspConfig->attach(this,(TCONFIG_PROC)&CRtspMediaService::OnRtspConfigChange);
    RTSP_SET &rtspSet = m_RtspConfig->getConfig();
    memcpy(&m_OldRtspSet,&rtspSet,sizeof(RTSP_SET));
    g_SMPortPool.InitPortPool(m_OldRtspSet.usUdpStartPort, m_OldRtspSet.usUdpPortNum);
    //注册控制台关于rtsp的操作函数
    g_Console.registerCmd (CConsole::Proc( &CRtspMediaService::OnRtspConfig, this ), "rtsp", "OnRtspConfig");
}

CRtspMediaService::~CRtspMediaService()
{
    Stop();    
    m_RtspConfig->detach(this,(TCONFIG_PROC)&CRtspMediaService::OnRtspConfigChange);
    VD_DELETE(m_RtspConfig);
	m_RtspConfig = NULL;//zsliu
    g_Console.unregisterCmd("rtsp");
}
//这个函数只在程序起来后调用一次
int CRtspMediaService::SetMaxConn(int connIdNum)
{
    CGuard guard(m_RtspSvrCltMutex);
//    if(connIdNum < 0 || connIdNum > 32)
    if(connIdNum < 0 )
    {
        return -1;
    }
    
    if(-1 != m_ConnIdNum && m_RtspSvrClt)
    {
        Stop(VD_FALSE);
        if(m_ConnIdNum > connIdNum)
        {
            for(int i = connIdNum; i < m_ConnIdNum; i++)
            {
                m_RtspSvrClt[i].connId = -1;
                VD_DELETE(m_RtspSvrClt[i].rtspSession);
            }
            
            m_ConnIdNum = connIdNum;
        }
        else if(m_ConnIdNum < connIdNum)
        {
            sRtspConn *tRtspConn = new sRtspConn[connIdNum];
            if(!tRtspConn)
            {
                 return -1;
            }
            
            for(int i = 0; i < m_ConnIdNum; i++)
            {
                tRtspConn[i].connId = m_RtspSvrClt[i].connId;
                tRtspConn[i].rtspSession = m_RtspSvrClt[i].rtspSession;
            }
            
            delete m_RtspSvrClt;
            
            m_RtspSvrClt = tRtspConn;
            m_ConnIdNum = connIdNum;
            
            return 0;
        }
        else
        {
            return 0;
        }
    }
    
    m_ConnIdNum = connIdNum;
    m_RtspSvrClt = new sRtspConn[m_ConnIdNum];
    for(int i = 0; i < m_ConnIdNum; i++)
    {
        m_RtspSvrClt[i].connId = -1;
        m_RtspSvrClt[i].rtspSession = NULL;
    }
    return 0;
}

VD_BOOL CRtspMediaService::Start(void)
{
    //保证不多次重建线程
    if (m_bLoop)
    {
        return TRUE;
    }

	if(!CreateThread())
	{
		VD_TRACE("CRtspMediaService Start, CreateThread failed!\n");
        return VD_FALSE;
	}
	
    VD_TRACE("CRtspMediaService Start Successful...\n");
    return VD_TRUE;
}

VD_BOOL CRtspMediaService::Stop(VD_BOOL bClear)
{
    if(!m_bLoop)
    {
        return VD_TRUE;
    }
    
    for (int i = 0; i < m_ConnIdNum; i++)
    {
        if(-1 != m_RtspSvrClt[i].connId)
        {
            m_RtspSvrClt[i].rtspSession->SetReleaseFlag(CRtspSession::RELEASE_NEED);
        }
    }
    
    if(bClear)
    {
        DestroyThread();
    }
    return VD_TRUE;
}

void CRtspMediaService::ThreadProc()
{
    while(m_bLoop)
    {
        m_RtspSvrCltMutex.Enter();

        for (int i = 0; i < m_ConnIdNum; i++)
        {
            //VD_TRACE("i <%d> m_RtspSvrClt[i].connId <%d>\n",i, m_RtspSvrClt[i].connId);
            if(-1 != m_RtspSvrClt[i].connId)
            {
                if(CRtspSession::RELEASE_NEED == m_RtspSvrClt[i].rtspSession->GetReleaseFlag())
                {
                    //tracepoint();
                    //采用异步关rtspSession,以免出现对已经释放的对象进行引用
                    InnerDelRtspSession(m_RtspSvrClt[i].connId,CRtspSession::RELEASE_NEED);
                    //tracepoint();
                    continue;
                }
                m_RtspSvrClt[i].rtspSession->DoMediaTask();
            }
            else
            {
                break;
            }
        }
        m_RtspSvrCltMutex.Leave();
            
        SystemSleep(10);
    }   
}
int CRtspMediaService::GetValidIndex()
{
    for(int i = 0; i < m_ConnIdNum; i++)
    {
        if(-1 == m_RtspSvrClt[i].connId)
        {
            return i;
        }
    }
    if(-1 == m_ConnIdNum)
    {
        SetMaxConn(RTSP_CONN_NUM);
    }
    return -1;
}
VD_BOOL CRtspMediaService::AddRtspSession(CRtspSession *rtspSession)
{
    CGuard guard(m_RtspSvrCltMutex);
    int index = GetValidIndex();
    if(!rtspSession || -1 == index)
    {
        VD_TRACE("Reach Max Rtsp Connetion <%d>, rtspSession == NULL <%d>\n", RTSP_CONN_NUM, NULL == rtspSession);
        return VD_FALSE;
    }
    m_RtspSvrClt[index].connId = rtspSession->GetRtspSocket();
    m_RtspSvrClt[index].rtspSession = rtspSession;

    if(!m_bLoop/*0 == index*/)
    {
        Start();
    }
    VD_TRACE("AddRtspSession <%d>\n", m_RtspSvrClt[index].connId);
    return VD_TRUE;
}

VD_BOOL CRtspMediaService::InnerDelRtspSession(int connId,CRtspSession::eRtspState flag)
{
    for (int i = 0; i < m_ConnIdNum; i++)
    {
        if(connId == m_RtspSvrClt[i].connId && -1 != m_RtspSvrClt[i].connId)
        {
            int tReleaseFlag = m_RtspSvrClt[i].rtspSession->GetReleaseFlag();
            m_RtspSvrClt[i].rtspSession->SetReleaseFlag(flag);
            if(tReleaseFlag == CRtspSession::RELEASE_NEED)
            {
                VD_DELETE(m_RtspSvrClt[i].rtspSession);
                m_RtspSvrClt[i].connId = -1;
                VD_TRACE("Delete RtspSession <%d>\n",connId);
                TransPreRtspSession(i);
                if(-1 == m_RtspSvrClt[0].connId)
                {
                    __trip;
                    Stop(VD_TRUE);
                }
                return VD_TRUE ;
            }
            return VD_TRUE;
        }
    }
    
    VD_TRACE("connId <%d>,No such rtspSession \n",connId);
    return VD_FALSE;
}

VD_BOOL CRtspMediaService::DelRtspSession(int connId,CRtspSession::eRtspState flag)
{
    CGuard guard(m_RtspSvrCltMutex);
    return InnerDelRtspSession(connId,flag);
}

//将已经存在的rtspSession移到数组m_RtspSvrClt的前面，以便ThreadProc中while的break退出.
VD_BOOL CRtspMediaService::TransPreRtspSession(int index)
{ 
    if(index < 0)
    {
        return VD_FALSE;
    }
    
    int iIndex = index;
    int tConnIdNum = m_ConnIdNum - 1;
    while(iIndex < tConnIdNum)
    {
        if(-1 != m_RtspSvrClt[iIndex+1].connId)
        {
            m_RtspSvrClt[iIndex].connId = m_RtspSvrClt[iIndex+1].connId;
            m_RtspSvrClt[iIndex].rtspSession = m_RtspSvrClt[iIndex+1].rtspSession;
            m_RtspSvrClt[iIndex+1].connId = -1;
            m_RtspSvrClt[iIndex+1].rtspSession = NULL;
        }
        else
        {
            break;
        }
        iIndex++;
    }
    
    return VD_TRUE;
}

VD_BOOL CRtspMediaService::DoMsg(int engineId, int connId, const char* data, size_t len)
{
    CGuard guard(m_RtspSvrCltMutex);
    CRtspSession* rtspSession = GetRtspSession(connId);
    if(!rtspSession)
    {
        VD_TRACE("No such rtspSession <%d>\n",connId);
        return VD_FALSE;
    }
    rtspSession->DoMsgTask(engineId, connId, data, len);
    if(CRtspSession::RELEASE_MEDIA_MSG == rtspSession->GetReleaseFlag())
    {
        g_RtspSvr.CloseClient(connId);
        rtspSession->SetReleaseFlag(CRtspSession::RELEASE_NEED);
    }
    return VD_TRUE ;
}

CRtspSession* CRtspMediaService::GetRtspSession(int connId)
{
    for (int i = 0; i < m_ConnIdNum; i++)
    {
        if(connId == m_RtspSvrClt[i].connId)
        {
            return m_RtspSvrClt[i].rtspSession;
        }
    }
    return (CRtspSession*)NULL;
}

void CRtspMediaService::Clear()
{
    for (int i = 0; i < m_ConnIdNum; i++)
    {
        if(-1 != m_RtspSvrClt[i].connId)
        {
            m_RtspSvrClt[i].connId = -1;
            VD_DELETE(m_RtspSvrClt[i].rtspSession);
        }
    }
}

/*
*rtsp串口相关配制及设置
*/
int CRtspMediaService::OnRtspConfig(int argc, char **argv)
{
    char *pszCmd0;
    char * pArg1;

    CConsoleArg arg(argc, argv);
    pszCmd0 = arg.getOptions();//获得命令选项
    pArg1  = arg.GetArg(0);

    //VD_TRACE("argc = %d\n",argc);
    //VD_TRACE("pszCmd0 <%s> pArg1 <%s> pArg1 <%s> pArg1 <%s>\n",pszCmd0, pArg1,arg.GetArg(1),arg.GetArg(2));
    if(!pszCmd0 || !pArg1)
    {
        VD_TRACE("use \"rtsp -help\" for help please!\n");
        return 0;
    }
    if(!strncmp(pszCmd0,"help",strlen("help")))
    {
        VD_TRACE("trans command usage:\n");
        VD_TRACE("rtsp -help: print help message\n");
        VD_TRACE("rtsp -udpPort port : set rtp/udp over tcp port(unsigned short,must > 20000)\n");
        VD_TRACE("rtsp -rtspPort port : set rtsp listenning port(unsigned short,must > 20000)\n");
        VD_TRACE("rtsp -speed sock speed(-4、-1、1、2、4):set playback speed\n");
        VD_TRACE("rtsp -play sock TimeStr(y-m-d-h-m-s_y-m-d-h-m-s,StartTiem_EndTime)\n");
    }
    else if(!strncmp(pszCmd0,"udpPort",strlen("udpPort")))
    {
        VD_TRACE("rtsp -udpPort port\n");
    }
    else if(!strncmp(pszCmd0,"rtspPort",strlen("rtspPort")))
    {
        VD_TRACE("rtsp -rtspPort port\n");
    }
    else if(!strncmp(pszCmd0,"play",strlen("play")))
    {
        VD_TRACE("rtsp -play sock TimeStr\n");
        int sock = atoi(pArg1);
        CRtspSession *rtspSession = GetRtspSession(sock);
        if(rtspSession)
        {
            rtspSession->OnConfig("play",arg.GetArg(1));
        }
        else
        {
            VD_TRACE("No such rtspSession\n");
        }
    }
    else if(!strncmp(pszCmd0,"speed",strlen("speed")))
    {
        VD_TRACE("set playback speed\n");
        int sock = atoi(pArg1);
        CRtspSession *rtspSession = GetRtspSession(sock);
        if(rtspSession)
        {
            rtspSession->OnConfig("speed",arg.GetArg(1));
        }
        else
        {
            VD_TRACE("No such rtspSession\n");
        }
    }
    
    return 0;
}

void CRtspMediaService::OnRtspConfigChange(CConfigNetRtspSet *cRtspConfig,int &ret)
{
    RTSP_SET &newRtspSet = cRtspConfig->getConfig();
    
    VD_TRACE("rtspListernPort %d %d %d ----> %d %d %d \n",
        m_OldRtspSet.usListernPort,
        m_OldRtspSet.usUdpStartPort, 
        m_OldRtspSet.usUdpPortNum,
        newRtspSet.usListernPort,
        newRtspSet.usUdpStartPort, 
        newRtspSet.usUdpPortNum);
    
    if(m_OldRtspSet.usEnable != newRtspSet.usEnable)
    {
        //ret |= CONFIG_APPLY_REBOOT;
        m_OldRtspSet.usEnable = newRtspSet.usEnable;
        
        if(newRtspSet.usEnable)
        {
            g_RtspSvr.Start(newRtspSet.usListernPort);
        }
        else
        {
            g_RtspSvr.Stop();
        }
        
    }
    if(m_OldRtspSet.usListernPort != newRtspSet.usListernPort)
    {
        m_OldRtspSet.usListernPort = newRtspSet.usListernPort;
        
        g_RtspSvr.Stop();
        g_RtspSvr.Start(newRtspSet.usListernPort);
        //ret |= CONFIG_APPLY_REBOOT;
    }

    if(m_OldRtspSet.usUdpStartPort != newRtspSet.usUdpStartPort || m_OldRtspSet.usUdpPortNum != newRtspSet.usUdpPortNum)
    {
        VD_TRACE("UdpStartPort or UdpPortNum have been Chaneged,Need Init rtpPortPool\n");
        //ret |= CONFIG_APPLY_REBOOT;
        if(newRtspSet.usUdpPortNum <= 1000 && 
            newRtspSet.usUdpStartPort >= 10000 && 
            newRtspSet.usUdpStartPort <= (unsigned short)0xffff - newRtspSet.usUdpPortNum)
        {
            m_OldRtspSet.usUdpStartPort = newRtspSet.usUdpStartPort;
            m_OldRtspSet.usUdpPortNum = newRtspSet.usUdpPortNum;
            Stop(VD_FALSE);
            g_SMPortPool.InitPortPool(newRtspSet.usUdpStartPort, newRtspSet.usUdpPortNum);
        }
    }

    /* 简单处理，组播配置修改后停止全部rtsp 会话*/
    if (m_OldRtspSet.stMulticastIp.l != newRtspSet.stMulticastIp.l || m_OldRtspSet.usPort != newRtspSet.usPort)
    {
        Stop(VD_FALSE);
    }
}

