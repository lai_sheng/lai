/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              RtspSession.h
**Version:                  Version 0.01
**Author:                  yang_shukui
**Created:                2010-4-26   13:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/

#include "ez_libs/ezutil/str_opr.h"
#include "ez_libs/ezutil/base64.h"
#include "System/UserManager.h"
#include "APIs/Capture.h"
#include "APIs/System.h"
#include "APIs/Net.h"
#include "Devices/DevCapture.h"
#include "RtspSession.h"
#include "BaseFunc/VDDebug.h"
#include "RtspThread.h"
#include "PortPool.h"
#include "OprPdu/RtspPdu.h"

extern const char rtspStrFlag[RTSP_METHOD_NUM][16];
extern int VDSocketTcpSend(int connId,const char* pData,int len);

CRtspSession::CRtspSession()
{
    m_NeedRelease = RELEASE_NO_NEED;
    m_RecvBuf.Reset();
    
    m_pCheckOption = new CTimer("RTSPOPTION_CHECKER");
    VD_ASSERT(m_pCheckOption);
        
    m_tmLastOption = time(NULL);
    m_iOptionCounts = -1;//从1开始计数过滤掉rtsp会话的第一次Option，这次的Option不是从中保活Option
    MethodFuncInit();
    
    for(int i = 0; i < HanderRtspNr; i++)
    {
        m_RtspSocket[i] = INVALID_SOCKET;
        m_RtspRemotePort[i] = 0;
        m_RtspLocalPort[i] = 0;
    }
    m_Media = NULL;
	m_OverHttp = VD_FALSE;
}

CRtspSession::~CRtspSession()
{
    for(int i = HanderVRtp; i < HanderRtspNr; i++)
    {
        if(INVALID_SOCKET != m_RtspSocket[i] && m_RtspSocket[HanderRtsp] != m_RtspSocket[i])
        {
            closesocket(m_RtspSocket[i]);
        }
    }
    if(m_pCheckOption)
    {
        if(m_pCheckOption->IsStarted())
        {
            m_pCheckOption->Stop();
        }
        VD_DELETE(m_pCheckOption);
    }
    g_SMPortPool.ReleasePortPair(m_RtspLocalPort[HanderVRtp]);
    if(m_Media)
    {
        printf("CRtspSession::~CRtspSession():m_Media = %p \r\n",m_Media);
        
        if(m_Media->IsAudioOn())
        {
            g_SMPortPool.ReleasePortPair(m_RtspLocalPort[HanderARtp]);
        }
        
        if (RTP_OVER_MULTI_CAST == m_Media->GetRtspModel())
        {
            g_RtspMediaVedioMultiManager.ReleaseRtspMedia((CRtspMediaVedioMulti*)m_Media);
            m_Media = NULL;
        }
        else
        {
            delete m_Media;
            m_Media = NULL;
        }
    }
}
void CRtspSession::MethodFuncInit()
{
    m_MethoFunc[0] = &CRtspSession::OptionsReq;
    #ifdef YUANYANG
    m_MethoFunc[1] = &CRtspSession::DescribeReqYY;
    #else
    m_MethoFunc[1] = &CRtspSession::DescribeReq;
    #endif
    m_MethoFunc[2] = &CRtspSession::SetupReq;
    m_MethoFunc[3] = &CRtspSession::PlayReq;
    m_MethoFunc[4] = &CRtspSession::RecordReq;
    m_MethoFunc[5] = &CRtspSession::PauseReq;
    m_MethoFunc[6] = &CRtspSession::GetparameterReq;
    m_MethoFunc[7] = &CRtspSession::SetparameterReq;
    m_MethoFunc[8] = &CRtspSession::TeardownReq;
    m_MethoFunc[9] = &CRtspSession::ErrorReq;   
}
void CRtspSession::SetPeerIp(const char *ip)
{
    if(!ip)
    {
        return ;
    }
    strncpy(m_szPeerIp,ip,sizeof(m_szPeerIp));
}

VD_BOOL CRtspSession::DoMediaTask()
{
    if(m_Media)
    {
        m_Media->DoMediaTask(HanderVRtp);
        if(m_Media->IsAudioOn())
        {
            m_Media->DoMediaTask(HanderARtp);
        }
    }
    return VD_TRUE;   
}
void CRtspSession::OnConfig(char *cmd, char *para)
{
    if(m_Media)
    {
        m_Media->OnConfig(cmd, para);
    }
}

int CRtspSession::DoRtpPacket(CPacket *packet, int mediaType)
{
    if(!packet )
    {    
        return 0;
    }
    
    return (this->*m_RtspTrans[mediaType])(m_RtspSocket[mediaType], 
        (const char *)packet->GetBuffer(), 
        (int)packet->GetLength(),
        m_szPeerIp,
        m_RtspRemotePort[mediaType]);
}

CRtspSession::eRtspState CRtspSession::GetReleaseFlag()
{
    return m_NeedRelease;
}

CRtspSession::eRtspState CRtspSession::SetReleaseFlag(eRtspState flag)
{
    if(flag >= RELEASE_NO_NEED && flag <= RELEASE_NEED)
    {
        m_NeedRelease = flag;
    }
    return m_NeedRelease;
}

int CRtspSession::GetRtspSocket(void)
{
    return m_RtspSocket[HanderRtsp];
}

int CRtspSession::SetRtspSocket(int socket)
{
    if(INVALID_SOCKET != socket)
    {
        m_RtspSocket[HanderRtsp] = socket;
    }
    return m_RtspSocket[HanderRtsp];
}

int CRtspSession::SendToPeerMsg(CRtspPdu*& pReqPdu)
{
    if (!pReqPdu )
    {
        return 0;
    }
    return g_RtspSvr.TPSendPdu(m_RtspSocket[HanderRtsp], pReqPdu);
}

int CRtspSession::GetIndexOfMethod(std::string method)
{
    for(int i = 0; i < RTSP_METHOD_NUM; i++)
    {
        if(method  == rtspStrFlag[i])
        {
            return i;
        }
    }
    return RTSP_METHOD_NUM;
}
void CRtspSession::CheckOptionsState(unsigned int arg)
{
    /*
    client是5秒一个options，假如60秒内都没收到过options，说明连接断开，
    为防止其他公司的client不发option，因此有个曾经收到过3次option的判断
    */

    time_t tTime = time(NULL);
    if((tTime - m_tmLastOption > OPTION_TIMER_INTERVAL*12)
        &&(tTime - m_tmLastOption < OPTION_TIMER_INTERVAL*18)//防止编码器系统时间被改
        &&(m_iOptionCounts > 3))
    {
        VD_TRACE("CRtspClient::CheckOptionsState Has not recv options for a long time!!!\n");
        if(!m_pCheckOption)
        {
            //m_pCheckOption->Stop();//m_pCheckOption=NULL
            trace("CRtspClient::CheckOptionsState , stop the options checker timer!!\n");
        }
        m_iOptionCounts = 0;
        m_tmLastOption = 0;
        //m_NeedRelease = RELEASE_MEDIA_MSG; 
    #ifndef YUANYANG
        SetReleaseFlag(RELEASE_NEED);
    #endif
    }
    
}
void CRtspSession::GetUrlLineData(std::string strUrl,std::string strKey,std::string &strValue)
{
    size_t posF = 0;
    size_t posQ = 0;

    strValue.clear();
    posF = strUrl.find(strKey); 
    if (posF != std::string::npos) 
    {
        posQ = strUrl.find("&", posF);
        if (posQ == std::string::npos)
        {  
            posQ = strUrl.find(" ", posF);  
            if (posQ == std::string::npos) 
            {  
                posQ = strUrl.end() - strUrl.begin();
            }  
        } 
        strValue.assign(strUrl.begin() + posF + strKey.size(), strUrl.begin() + posQ); 
    }
}

VD_BOOL CRtspSession::HttpGet(const char *data, int &len)
{
    char *httpMsg = "HTTP/1.0 200 OK\r\n"
					"Connection: close\r\n"
					"Date: Tue, Aug 09 2011 09:38:56 GMT\r\n"
					"Cache-Control: no-store\r\n"
					"Pragma: no-cache\r\n"
					"Server: meida server\r\n"
					"Content-Type: application/x-rtsp-tunnelled\r\n"
					"Cache-Control: no-cache\r\n\r\n";
	if (!data)
	{
	    return VD_FALSE;
	}
	
	char *httpEnd = (char*)strstr(data, "\r\n\r\n");
	if (!httpEnd)
	{
	    return VD_FALSE;
	}
	
	len = (int)(httpEnd - data) + 4;
    g_RtspSvr.SendMsg(m_RtspSocket[HanderRtsp], httpMsg, strlen(httpMsg));
	
	return VD_TRUE;
}

VD_BOOL CRtspSession::HttpPost(const char *data, int &len)
{
    if (!data)
	{
	    return VD_FALSE;
	}
	
	char *httpEnd = (char*)strstr(data, "\r\n\r\n");
	if (!httpEnd)
	{
	    return VD_FALSE;
	}
	
	len = (int)(httpEnd - data) + 4;
    m_OverHttp = VD_TRUE;
	
	return VD_TRUE;
}

VD_BOOL CRtspSession::DoMsgTask(int engineId, int connId, const char * pData, size_t len)
{
	char *msg = (char *)pData;
	int iLen = len;
	VD_BOOL bRet = VD_FALSE;
	
    if(!pData || len <= 0)
    {
        return VD_FALSE;
    }

    //RTSP报文长度限制
    if(m_RecvBuf.Size() + len > MAX_RTSP_RECV_LEN)
    {
        m_RecvBuf.Reset();
        return VD_TRUE;
    }

    int httpLen = 0;
    char *httpHeader = (char *)pData;
	while (msg = strstr(httpHeader, "\r\n\r\n"))
	{
	    httpLen = (int)(msg - httpHeader) + 4;
		m_RecvBuf.Append((unsigned char*)httpHeader, httpLen);
		httpHeader += httpLen;
	}
	
	httpLen = (int)(httpHeader - pData);
	if (httpLen < (int)len && httpLen > 64 &&'$' != *httpHeader)
	{
		CPacket *ptk = g_PacketManager.GetPacket((((len - httpLen) * 4) / 3) + 5);
		if (!ptk)
		{
		    return VD_FALSE;
		}
		
		char *rtspMsgBuffer = (char *)ptk->GetBuffer();
	    iLen = ez_base64decode(rtspMsgBuffer, httpHeader);
		m_RecvBuf.Append((unsigned char*)rtspMsgBuffer, iLen);
		ptk->Release();
	}
    
    while(m_RecvBuf.Size() > 0)
    {
        msg = (char *)(m_RecvBuf.Buf());
		iLen = m_RecvBuf.Size();
		
		if('$' == *msg)
        {
            //rtp over tcp方式下的rtp or rtcp packet (depend of rtspChaId/Transport: RTP/AVP/TCP;unicast;interleaved=0-1)
            if(m_RecvBuf.Size() > 4)
            {
                //int rtspChaId =  (int)(*(msg+1));//以备后用
                int payLoadLen = ((int)(*(msg+2)) <<8) + ((int)(*(msg+3)));
                if ( (4 + payLoadLen) >(int)(m_RecvBuf.Size()) )
                {
                    return VD_FALSE;
                }
                m_RecvBuf.Pop(NULL, 4 + payLoadLen);
            }    
            else
            {
                break;
            }
        }
		else if (!strncmp(msg, "GET ", 4))
		{
		    //rtsp over http/GET
		    bRet = HttpGet((const char *)msg, iLen);
			if (bRet)
			{
			    m_RecvBuf.Pop(NULL, iLen);
			}
			else
			{
			    m_RecvBuf.Reset();
			}
		}
		else if (!strncmp(msg, "POST ", 5))
		{
		    //rtsp over http/POST
		    bRet = HttpPost((const char *)msg, iLen);
			if (bRet)
			{
			    m_RecvBuf.Pop(NULL, iLen);
			}
			else
			{
			    m_RecvBuf.Reset();
			}
		}
        else
        {
            //RTSP消息,分析msg
            CRtspPdu* pRtspPdu = new CRtspPdu();
            VD_ASSERT(pRtspPdu);
			
	        //VDDumpHex((const char *)msg, iLen);
            std::string strBuff(msg, iLen);
                
            bool bRet = pRtspPdu->parseBody(strBuff);
            if (!bRet)
            {
                if ( pRtspPdu->GetHdr() != NULL )
                {
                    int errcode = pRtspPdu->GetHdr()->GetLastError();
                    if (pRtspPdu->GetHdr() && 
                        ( CommonLib::ERR_BODYINCOMPLETE ==  errcode||
                        CommonLib::ERR_HDRINCOMPLETE == errcode ) )
                    {
                    }
                    else
                    {
                        VD_TRACE("errcode:%d\n", errcode);
                        //assert(0);
                        m_RecvBuf.Reset();
                    }
                }
		//zsliu change
		if(pRtspPdu)
		{
                	delete pRtspPdu;
			pRtspPdu = NULL;
		}
                break;
            }
            //区分请求和响应，调用不同的处理
            if (pRtspPdu->GetHdr()->getType() == CommonLib::MODEL_REQUEST)
            {
                //此处为服务器模式的操作,监视请求
                (this->*m_MethoFunc[GetIndexOfMethod(pRtspPdu->GetMethod())])(engineId, connId, pRtspPdu);
            }
            //解析ok，需要弹出分析的缓冲大小
            if(pRtspPdu->GetHdr()->getLength() > m_RecvBuf.Size())
            {
                m_RecvBuf.Reset();
            }
            else
            {
                m_RecvBuf.Pop(NULL, pRtspPdu->GetHdr()->getLength());
            }
		//zsliu change
		//delete pRtspPdu;
		if(pRtspPdu)
		{
                	delete pRtspPdu;
			pRtspPdu = NULL;
		}
         }
     }
    return 0;
}

int CRtspSessionSvr::OptionsReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }

    int iResult = 200;
#if 0
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;
        m_NeedRelease = RELEASE_MEDIA_MSG;  
    }
#endif

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;
    pRspPdu->GetHdr()->_str_Public = "";
    for(int i = 0; i < RTSP_METHOD_NUM; i++)
    {
        if(i != 0)
        {
            pRspPdu->GetHdr()->_str_Public += " ";
        }
        pRspPdu->GetHdr()->_str_Public += rtspStrFlag[i];
    }

    SendToPeerMsg(pRspPdu);

    m_tmLastOption = time(NULL);
    if(++m_iOptionCounts == 0 && m_pCheckOption)
    {   
        //开启5秒定时器
        m_pCheckOption->Start(this, (VD_TIMERPROC)&CRtspSession::CheckOptionsState, 10*1000, OPTION_TIMER_INTERVAL*1000);
        //VD_TRACE("Start rtsp OptionReq timer check......\n");
    }
    return 0;
}
int CRtspSessionSvr::DescribeReqYY(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }
    
    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "DVR 1.1";
    pRspPdu->GetHdr()->result = 200;
    pRspPdu->GetHdr()->message = "OK";
    pRspPdu->GetHdr()->accept = pReqPdu->GetHdr()->accept;
    pRspPdu->GetHdr()->acceptLanguage = pReqPdu->GetHdr()->acceptLanguage;
    
    int iChannel = -1;
    int iResolution = CAPTURE_SIZE_D1;
    
    std::string strUrl = pReqPdu->GetHdr()->url;
    int iResult = 200;
    std::string strUName;
    std::string strUPwd;
    std::string strType;
    size_t posF = 0;
    size_t posQ = 0;

    posF = strUrl.find("RTSP://");
    if ( posF == std::string::npos )
    {
        posF = strUrl.find("rtsp://");
    }
    if (posF != std::string::npos)
    {
        strUrl.erase(strUrl.begin() , strUrl.begin() + posF + strlen("rtsp://") );

        posQ = strUrl.find("/");
        if (posQ != std::string::npos)
        {
            strUrl.erase(strUrl.begin() , strUrl.begin() + posQ + 1);
        }
        //远洋录像查询时设备发给服务器的录像文件名构格式,需要根据文件名获取iCodeType和iChannel。
        //"%02d_%04d-%02d-%02d-%02d-%02d-%02d_%04d-%02d-%02d-%02d-%02d-%02d",
        //"%02d"表示通道号，"%04d-%02d-%02d-%02d-%02d-%02d_%04d-%02d-%02d-%02d-%02d-%02d"对应时间    
        std::string strType;
        GetUrlLineData(pReqPdu->GetHdr()->url,"record_",strType);
        VD_TRACE("record para <%s>\n",strType.c_str());
        if(strType.size() > 0)
        {
            char *recordPara = (char*)strstr(strType.c_str(), "_");
            if(recordPara)
            {
                iChannel = atoi(strType.c_str()) ;    
                if(iChannel >= 0 && iChannel < g_nCapture)
                {
                    tracepoint();
                    iChannel = 0;
                }
		//zsliu add
                if(m_Media)
		{
              		delete m_Media;
			m_Media = NULL;
			printf("repat new m_Media file = %s, fun = %s(), line = %d\n", __FILE__, __FUNCTION__, __LINE__);
                }
		//
                m_Media = new CRtspMediaRecord(this);
                VD_ASSERT(m_Media);
                m_Media->SetMediaType(MEDIA_PLAY);
                if(VD_FALSE == m_Media->SetVedioTime(&recordPara[1]))//to pass '-'
                {
                    iResult = 451;
                }
                else
                {
                    m_Media->SetChannel(iChannel);
                    iResolution = m_Media->GetResolution();
                    if(iResolution < CAPTURE_SIZE_D1 || iResolution >= CAPTURE_SIZE_NR)
                    {
                        iResult = 451;
                    }
                }
            }
        }        
    }    

    if (iResult != 200)
    {
        pRspPdu->GetHdr()->result = 451;
        pRspPdu->GetHdr()->message = "CHANNEL_PARA_ERROR";     
        SendToPeerMsg(pRspPdu);

        VD_TRACE("####CRtspClient::DescribeReq CHANNEL_ERROR or USER PASSWORD ERROR\r\n");
        m_NeedRelease = RELEASE_MEDIA_MSG;

        return 0;
    }
    
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->contentBase = pReqPdu->GetHdr()->url;

    //填充sdp信息
    pRspPdu->GetHdr()->contentTypeString="application/sdp";


    if(m_Media && m_Media->IsAudioOn())
    {
        //在此加入音频的描述
    }
    SendToPeerMsg(pRspPdu);

    return 0;
}

//目前支持D1/CIF/QCIF三种分辨率
/*
    RTSP url 规则
    rtsp://ip:554/user=xxx&password=xxx&id=xx&type=xx
    type=1表示子码流，其他值都按照主码流来解
    id 通道号
    user,password 用户名和密码，请求视频前需要验证
*/

int CRtspSessionSvr::DescribeReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }
	
    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "DVR 1.1";
    pRspPdu->GetHdr()->result = 200;
    pRspPdu->GetHdr()->message = "OK";
    pRspPdu->GetHdr()->accept = pReqPdu->GetHdr()->accept;
    pRspPdu->GetHdr()->acceptLanguage = pReqPdu->GetHdr()->acceptLanguage;
    
    int iChannel = -1;
    int iCodeType = 0;
    int iResolution = CAPTURE_SIZE_D1;
    
    std::string strUrl = pReqPdu->GetHdr()->url;
    int iResult = 200;
    std::string strUName;
    std::string strUPwd;
    std::string strType;
    size_t posF = 0;
    size_t posQ = 0;

    posF = strUrl.find("RTSP://");
    if ( posF == std::string::npos )
    {
        posF = strUrl.find("rtsp://");
    }
    if (posF != std::string::npos)
    {
        strUrl.erase(strUrl.begin() , strUrl.begin() + posF + strlen("rtsp://") );

        posQ = strUrl.find("/");
        if (posQ != std::string::npos)
        {
            strUrl.erase(strUrl.begin() , strUrl.begin() + posQ + 1);
        }

        GetUrlLineData(strUrl,"user=",strUName);
        GetUrlLineData(strUrl,"password=",strUPwd);
        if (g_userManager.isPasswdValid(strUName, strUPwd))
        {
#ifdef KOTI    
            if(strUrl.find("v1.3gp") != std::string::npos)
            {
                iCodeType = 0;
            }
            else
            {
                iCodeType = 1;
            }
#else
            GetUrlLineData(strUrl,"type=",strType);
            iCodeType = atoi((char*)strType.c_str());
#endif
            
            GetUrlLineData(strUrl,"id=",strType);
            iChannel = atoi((char*)strType.c_str()) - 1 ;

            if(iCodeType >= 0 && iCodeType < CHL_FUNCTION_NUM && iChannel >= 0 && iChannel < g_nLogicNum)
            {            
                std::string strType;
                GetUrlLineData(pReqPdu->GetHdr()->url,"record=",strType);
                VD_TRACE("record para <%s>\n",strType.c_str());

                if(strType.size() > 0)
                {
                	//zsliu add
	                if(m_Media)
			{
				delete m_Media;
 				m_Media = NULL;
				printf("zsliu repat new m_Media file = %s, fun = %s(), line = %d\n", __FILE__, __FUNCTION__, __LINE__);
			}
			 //
                    m_Media = new CRtspMediaRecord(this);
                    VD_ASSERT(m_Media);
                    m_Media->SetMediaType(MEDIA_PLAY);
                    if(VD_FALSE == m_Media->SetVedioTime((char *)(strType.c_str())))
                    {
                        iResult = 451;
                    }
                }
                else
                {
                	//zsliu add
	                if(m_Media)
			{
				delete m_Media;
 				m_Media = NULL;
				printf("zsliu repat new m_Media file = %s, fun = %s(), line = %d\n", __FILE__, __FUNCTION__, __LINE__);
			}
			 //
                    m_Media = new CRtspMediaVedio(this);
                    VD_ASSERT(m_Media);
                    m_Media->SetMediaType(MEDIA_VEDIO);
                    m_Media->SetStreamType(iCodeType);
                }

                m_Media->SetChannel(iChannel);
                iResolution = m_Media->GetResolution();
                if(iResolution < CAPTURE_SIZE_D1 || iResolution >= CAPTURE_SIZE_NR)
                {
                    iResult = 451;
                }
            }
            else
            {
                iResult = 451;
            }    
        }    
        else
        {
            iResult = 451;
        }
    }    

    if (iResult != 200)
    {
        pRspPdu->GetHdr()->result = 451;
        pRspPdu->GetHdr()->message = "CHANNEL_PARA_ERROR";     
        SendToPeerMsg(pRspPdu);

        VD_TRACE("####CRtspClient::DescribeReq CHANNEL_ERROR or USER PASSWORD ERROR\r\n");
        m_NeedRelease = RELEASE_MEDIA_MSG;

        return 0;
    }
    
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->contentBase = pReqPdu->GetHdr()->url;

    //填充sdp信息
    pRspPdu->GetHdr()->contentTypeString="application/sdp";

    pRspPdu->addLineData("v", 0);

    char pIp[16] = {0};
    char pTmpBuf[256] = {0};
    NetGetHostIP("eth0",pIp,sizeof(pIp), pTmpBuf,sizeof(pTmpBuf));

    std::string strTm = "- 3331435948 1116907222000 IN IP4 ";
    strTm += pIp;
    pRspPdu->addLineData("o", (char* )strTm.c_str(),strTm.length());

    pRspPdu->addLineData("s", "Session",strlen("Session"));

    strTm = "IN IP4 ";
    strTm += pIp;
    pRspPdu->addLineData("c", (char* )strTm.c_str(),strTm.length());

    pRspPdu->addLineData("t", "0 0",strlen("0 0"));
    pRspPdu->addLineData("a", "range:npt=now-",strlen("range:npt=now-"));

    if (iCodeType == CHL_JPEG_T)
    {
       pRspPdu->addLineData("a", "x-broadcastcontrol:TIME",strlen("x-broadcastcontrol:TIME"));
       pRspPdu->addLineData("a", "x-copyright: Vimicro",strlen("x-copyright: Vimicro"));
       pRspPdu->addLineData("m", "video 0 RTP/AVP 26",strlen("video 0 RTP/AVP 26"));
       pRspPdu->addLineData("a", "recvonly",strlen("recvonly"));
       pRspPdu->addLineData("a", "control:trackID=0",strlen("control:trackID=0"));
       SendToPeerMsg(pRspPdu);
       return 0;
    }
    
    //支持黑莓手机播放
    pRspPdu->addLineData("a", "control:*",strlen("control:*"));
 
    pRspPdu->addLineData("m", "video 0 RTP/AVP 96",strlen("video 0 RTP/AVP 96"));
    pRspPdu->addLineData("a", "rtpmap:96 H264/90000",strlen("rtpmap:96 H264/90000"));

	memset(pTmpBuf,0,256);
	sprintf(pTmpBuf, "control:trackID=%d", RtspVedio);
    pRspPdu->addLineData("a", (char* )pTmpBuf, strlen(pTmpBuf));

    strTm = "fmtp:96 profile-level-id=";
	
	memset(pTmpBuf,0,256);
    if(iResolution == CAPTURE_SIZE_D1)
    {
        strncpy(pTmpBuf, "42E01E", 6);
    }
    if(iResolution == CAPTURE_SIZE_HD1)
    {
        strncpy(pTmpBuf, "42E015", 6);
    }
    else if (iResolution == CAPTURE_SIZE_CIF )
    {
        strncpy(pTmpBuf, "42E014", 6);
    }
    else if(iResolution == CAPTURE_SIZE_720P)
    {
        strncpy(pTmpBuf, "000042", 6);
    }
	else if(iResolution == CAPTURE_SIZE_1080P)
    {
        strncpy(pTmpBuf, "42002A", 6);
    }
    else/* QCIF*/
    {
        strncpy(pTmpBuf, "42E00A", 6);      
    }

    strTm += pTmpBuf;
    strTm += ";packetization-mode=1;sprop-parameter-sets="; //edit by jeckeanchan 091127  packetization-mode=0 quicktime

    memset(pTmpBuf,0,256);

    uchar sps[32] = {0x67,0x42,0xe0,0x0a,0xdb,0x0b,0x13,0x10,};/* QCIF*/
    if (iResolution == CAPTURE_SIZE_CIF )
    {
        sps[3] = 0x14;
        sps[4] = 0xDA;
        sps[5] = 0x05;
        sps[6] = 0x82;
        sps[7] = 0x51;
        sps[8]='\0';
    }
    else if(iResolution == CAPTURE_SIZE_D1)
    {
        sps[3] = 0x1E;
        sps[4] = 0xDB;
        sps[5] = 0x02;
        sps[6] = 0xC0;
        sps[7] = 0x49;
        sps[8] = 0x10;
        sps[9]='\0';
    }
    else if(iResolution == CAPTURE_SIZE_HD1)
    {
        sps[3] = 0x15;
        sps[4] = 0xDB;
        sps[5] = 0x05;
        sps[6] = 0x81;
        sps[7] = 0x24;
        sps[8] = 0x40;
        sps[9]='\0';
    }
    else if(iResolution == CAPTURE_SIZE_720P)
    {
        sps[3] = 0x1F;
        sps[4] = 0xDA;
        sps[5] = 0x01;
        sps[6] = 0x40;
        sps[7] = 0x16;
        sps[8] = 0xC4;
        sps[9]='\0';
    }
    else if(iResolution == CAPTURE_SIZE_SXGA)
    {
        sps[3] = 0x20;
        sps[4] = 0xDB;
        sps[5] = 0x01;
        sps[6] = 0x40;
        sps[7] = 0x08;
        sps[8] = 0x11;
        sps[9]='\0';
    }
    else if(iResolution == CAPTURE_SIZE_XVGA)
    {
        sps[3] = 0x1F;
        sps[4] = 0xDB;
        sps[5] = 0x01;
        sps[6] = 0x00;
        sps[7] = 0x18;
        sps[8] = 0x44;
        sps[9]='\0';
    }
	else if(iResolution == CAPTURE_SIZE_1080P)
    {
        sps[3] = 0x95;
        sps[4] = 0xA8;
        sps[5] = 0x1E;
        sps[6] = 0x00;
        sps[7] = 0x89;
        sps[8] = 0xF9;
        sps[9] = 0x50;
		sps[10] = '\0';
    }
    
    /* sps(base64),pps(base64)*/
    uchar pps[32] = {0x68,0xce,0x30,0xa4,0x80,};
    memset(pTmpBuf,0,256);
    ez_base64encode(pTmpBuf, (char *)sps, strlen((char *)sps));
    strTm += pTmpBuf ;
    
    strTm += ",";

    memset(pTmpBuf,0,256);
    ez_base64encode(pTmpBuf, (char *)pps, strlen((char *)pps));
    strTm += pTmpBuf ;
    pRspPdu->addLineData("a", (char* )strTm.c_str(),strTm.length());

    if(m_Media && m_Media->IsAudioOn())
    {
        //当前只支持G711a, 若为PCM，"a=rtpmap:8 PCMA/8000/1"改为"a=rtpmap:8 PCMU/8000"
		strTm = "audio 0 RTP/AVP 8";
		pRspPdu->addLineData("m", (char* )strTm.c_str(),strTm.length());
		strTm = "AS:48";
		pRspPdu->addLineData("b", (char* )strTm.c_str(),strTm.length());
		strTm = "rtpmap:8 PCMA/8000/1";
		pRspPdu->addLineData("a", (char* )strTm.c_str(),strTm.length());
		memset(pTmpBuf,0,256);
	    sprintf(pTmpBuf, "control:trackID=%d", RtspAudio);
		pRspPdu->addLineData("a", (char* )pTmpBuf, strlen(pTmpBuf));
    }
    SendToPeerMsg(pRspPdu);
    return 0;
}

int CRtspSessionSvr::SetupReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu )
    {
        return -1;
    }
	
	long pos = 0;
    int iResult = 451;
    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    if(!m_Media)
    {
        goto END_DESCRIBE;
    }
	
    if (pReqPdu->GetHdr()->url.find("rtsp://") != std::string::npos
        || pReqPdu->GetHdr()->url.find("RTSP://") != std::string::npos)
    {
	    pos = pReqPdu->GetHdr()->url.find("trackID=", pos) + strlen("trackID=");;
	    int trackId = strtol(pReqPdu->GetHdr()->url.c_str() + pos, 0, 0);
		if (trackId == 3)
		{
		    trackId = RtspVedio;
		}
		
	    if (trackId < RtspVedio || trackId > RtspAudio) //分别对应于视频和音频
	    {
	        goto END_DESCRIBE;
	    }
		int type = (RtspVedio == trackId ? HanderVRtp : HanderARtp);

		//SSRC填充
        uint uiSSRC = SystemGetMSCount();
        m_Media->SetSSRC(uiSSRC, trackId);
		
		char strSession[64]={0};
        sprintf(strSession, "%08X", uiSSRC);//ssrc,session标志一样
        pRspPdu->GetHdr()->session = strSession;
        pRspPdu->GetHdr()->transport = pReqPdu->GetHdr()->transport;
		
        if(std::string::npos != pRspPdu->GetHdr()->transport.find("RTP/AVP/TCP"))
        {
            if (m_Media && (m_Media->GetChannel() >= g_nLogicNum))
            {
                iResult = 452;
            }
            else
            {
                m_Media->SetRtspModel(RTP_OVER_TCP);
				
                m_RtspTrans[type] = &CRtspSession::TransRtpOverTcp;
                m_RtspSocket[type] = m_RtspSocket[HanderRtsp];
                m_RtspTrans[type + 1] = &CRtspSession::TransRtpOverTcp;
                m_RtspSocket[type + 1] = m_RtspSocket[HanderRtsp];
				
                int pos = pRspPdu->GetHdr()->transport.find_first_of("=", 0);
                int magic = strtol(pRspPdu->GetHdr()->transport.c_str() + pos + 1, 0, 0);
                m_Media->SetInterleaved((eRtspHandler)type, magic);
                magic = strtol(pRspPdu->GetHdr()->transport.c_str() + pos + 3, 0, 0);
                m_Media->SetInterleaved((eRtspHandler)(type + 1), magic);
				iResult = 200;
            }
        }
        else
        {
            pos = pReqPdu->GetHdr()->transport.find("multicast");
            if ((unsigned)pos != std::string::npos)
            {
                /* 组播 功能*/
                int iCh = 0;
                int iStreamType = 0;

                /* 先删除原先分配的media对象，重新分配组播对象 */
                if (m_Media)
                {
                    iCh = m_Media->GetChannel();
                    iStreamType = m_Media->GetStreamType();
                }
                else
                {
                    __trip;
                }

                if (RTP_OVER_MULTI_CAST != m_Media->GetRtspModel())
                {
                	//delete m_Media;
	                //m_Media = NULL;

                	//zsliu change
                	if(m_Media)
                	{
	                    delete m_Media;
	                    m_Media = NULL;
                	}
                }
                
                if (NULL == m_Media)
                {
                    m_Media = g_RtspMediaVedioMultiManager.GetRtspMedia(this,iCh,iStreamType);
                }
                
                if (NULL == m_Media)
                {
                    iResult = 452;/* 随便写的错误码 */
                }
                else
                {

                    /* 如果端口已经分配，则不重复分配 */
                    VD_UINT16 usLocalPort = 0;
                    if (((CRtspMediaVedioMulti*)m_Media)->GetLocalPort(usLocalPort))
                    {
                        m_RtspRemotePort[type] = CConfigNetRtspSet::getLatest().usPort + iCh*4 +iStreamType*2;
                        m_RtspRemotePort[type] = CConfigNetRtspSet::getLatest().usPort + iCh*4 + iStreamType*2+1;
                            
                        char strSDP[64]={0};
                        char pIp[16] = {0};
                        char pMask[16] = {0};
                        NetGetHostIP("eth0",pIp,sizeof(pIp), pMask,sizeof(pMask));
                                                        
                        pRspPdu->GetHdr()->transport = "RTP/AVP;multicast;destination=";
                        pRspPdu->GetHdr()->transport += m_szPeerIp;
                        pRspPdu->GetHdr()->transport += ";source=";
                        pRspPdu->GetHdr()->transport += pIp;
                        pRspPdu->GetHdr()->transport += ";server_port=";

                        sprintf(strSDP,"%d-%d;client_port=%d-%d;ssrc=%08X",
                            usLocalPort, usLocalPort+1,
                            m_RtspRemotePort[type],m_RtspRemotePort[type + 1], uiSSRC);
                        
                        pRspPdu->GetHdr()->transport += strSDP;
                        iResult = 200;
                    }
                    else
                    {
                        PortPair szPort;
                        if(g_SMPortPool.GetPortPair(szPort))                        
                        {                                
                            char strSDP[64]={0};
                            char pIp[16] = {0};
                            char pMask[16] = {0};
                            NetGetHostIP("eth0",pIp,sizeof(pIp), pMask,sizeof(pMask));
                            
                            ((CRtspMediaVedioMulti*)m_Media)->SetLocalPort(szPort.first);
                            ((CRtspMediaVedioMulti*)m_Media)->CreateUdp();
                            
                            pRspPdu->GetHdr()->transport = "RTP/AVP;multicast;destination=";
                            pRspPdu->GetHdr()->transport += m_szPeerIp;
                            pRspPdu->GetHdr()->transport += ";source=";
                            pRspPdu->GetHdr()->transport += pIp;
                            pRspPdu->GetHdr()->transport += ";server_port=";

                            sprintf(strSDP,"%d-%d;client_port=%d-%d;ssrc=%08X",
                                szPort.first, szPort.second,
                                m_RtspRemotePort[type],m_RtspRemotePort[type + 1], uiSSRC);
                            
                            pRspPdu->GetHdr()->transport += strSDP;
                            iResult = 200;
                        }
                    }
                }
            }
            else
            {
                if (m_Media && (m_Media->GetChannel() >= g_nLogicNum))
                {
                    iResult = 452;
                }
                else
                {
                    /* 单播 功能*/
                    pos = pReqPdu->GetHdr()->transport.find("client_port");
                    if ((unsigned)pos != std::string::npos)
                    {
                        pos = pReqPdu->GetHdr()->transport.find_first_of("= ", pos) + 1;
                        m_RtspRemotePort[type] = (unsigned short)strtol(pReqPdu->GetHdr()->transport.c_str() + pos, 0, 0);
                        m_RtspRemotePort[type + 1] = (unsigned short)strtol(pRspPdu->GetHdr()->transport.c_str() + pos + 2, 0, 0);
                        
                        PortPair szPort;
                        if(g_SMPortPool.GetPortPair(szPort))
                        {
                            m_RtspLocalPort[type] = szPort.first;
                            m_RtspLocalPort[type + 1] = szPort.second;
                            
                            char strSDP[64]={0};
                            char pIp[16] = {0};
                            char pMask[16] = {0};
                            NetGetHostIP("eth0",pIp,sizeof(pIp), pMask,sizeof(pMask));
                            if(m_Media)
                            {
                                m_Media->SetRtspModel(RTP_OVER_UDP);
                            }
                            m_RtspTrans[type] = &CRtspSession::TransRtpOverUdp;
							
							if (INVALID_SOCKET == m_RtspSocket[type])
							{
                                m_RtspSocket[type] = VDCreateUdpSocket(pIp,m_RtspLocalPort[type]);
							}
							
							if (INVALID_SOCKET == m_RtspSocket[type + 1])
							{
                                m_RtspSocket[type + 1] = VDCreateUdpSocket(pIp,m_RtspLocalPort[type + 1]);
							}
                            pRspPdu->GetHdr()->transport = "RTP/AVP;unicast;destination=";
                            pRspPdu->GetHdr()->transport += m_szPeerIp;
                            pRspPdu->GetHdr()->transport += ";source=";
                            pRspPdu->GetHdr()->transport += pIp;
                            pRspPdu->GetHdr()->transport += ";server_port=";

                            sprintf(strSDP,"%d-%d;client_port=%d-%d;ssrc=%08X",
                                m_RtspLocalPort[type], m_RtspLocalPort[type + 1],
                                m_RtspRemotePort[type],m_RtspRemotePort[type + 1], uiSSRC);
                            pRspPdu->GetHdr()->transport += strSDP;
                            iResult = 200;
                        }
                    }  
                }
             }
        }
    }
	
END_DESCRIBE:
    pRspPdu->GetHdr()->server = "DVR /1.1";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    if(451 == iResult)
    {
        m_NeedRelease = RELEASE_MEDIA_MSG;
    }

    SendToPeerMsg(pRspPdu);
    
    return 0;
}

int CRtspSessionSvr::RecordReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }

    int iResult = 200;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;
        m_NeedRelease = RELEASE_MEDIA_MSG;    
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;

    SendToPeerMsg(pRspPdu);
    return 0;
}

int CRtspSessionSvr::PlayReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }
    
    int iResult = 200;

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = "OK";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    
    std::string strUrl = pReqPdu->GetHdr()->url;
        
    if (strUrl.find("RTSP://") == std::string::npos &&
        strUrl.find("rtsp://") == std::string::npos)
    {
        iResult = 451;
    }

    if (iResult != 200 || 
        true != m_Media->StartMedia())
    {
        pRspPdu->GetHdr()->result = 451;
        pRspPdu->GetHdr()->message = "CHANNEL_ERROR";    
        SendToPeerMsg(pRspPdu);
        m_NeedRelease = RELEASE_MEDIA_MSG;

        return -1;
    }
    pRspPdu->GetHdr()->range = pReqPdu->GetHdr()->range;
    pRspPdu->GetHdr()->rtpInfo = "url=";
    pRspPdu->GetHdr()->rtpInfo += pReqPdu->GetHdr()->url;
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;
    SendToPeerMsg(pRspPdu);

    return 0;
}

int CRtspSessionSvr::PauseReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }

    int iResult = 200;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;
        m_NeedRelease = RELEASE_MEDIA_MSG;    
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;    

    SendToPeerMsg(pRspPdu);
    return 0;
}


int CRtspSessionSvr::GetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }
    int iResult = 200;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;
        m_NeedRelease = RELEASE_MEDIA_MSG;
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;
    pRspPdu->GetHdr()->_str_Public = "";
    for(int i = 0; i < RTSP_METHOD_NUM; i++)
    {
        if(i != 0)
        {
            pRspPdu->GetHdr()->_str_Public += " ";
        }
        pRspPdu->GetHdr()->_str_Public += rtspStrFlag[i];
    }

    SendToPeerMsg(pRspPdu);

    return 0;
}
int CRtspSessionSvr::SetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }

    int iResult = 200;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;
        m_NeedRelease = RELEASE_MEDIA_MSG;
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;

    SendToPeerMsg(pRspPdu);

    return 0;
}

int CRtspSessionSvr::TeardownReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }
    
    int iResult = 200;
    m_NeedRelease = RELEASE_MEDIA_MSG;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        iResult = 451;    
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = (iResult == 200)?"OK":"ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;

    /* 组播情况下直接释放对象*/
    if (m_Media && RTP_OVER_MULTI_CAST != m_Media->GetRtspModel())
    {
        m_Media->StopMedia();
    }
    else
    {
        g_RtspMediaVedioMultiManager.ReleaseRtspMedia((CRtspMediaVedioMulti*)m_Media);
        m_Media = NULL;
        SetReleaseFlag(RELEASE_NEED);
    }
    
    //SendToPeerMsg(pRspPdu);
    //这里一定要直接发
    g_RtspSvr.SendPdu(m_RtspSocket[HanderRtsp], pRspPdu);
    
    return 0;
}

int CRtspSessionSvr::ErrorReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    if (NULL == pReqPdu)
    {
        return -1;
    }

    int iResult = 451;
    if (pReqPdu->GetHdr()->url.find("rtsp://") == std::string::npos
        && pReqPdu->GetHdr()->url.find("RTSP://") == std::string::npos)
    {
        m_NeedRelease = RELEASE_MEDIA_MSG;  
    }

    CRtspPdu* pRspPdu = CRtspPdu::createAck(pReqPdu);
    pRspPdu->GetHdr()->server = "VISION 1.0";
    pRspPdu->GetHdr()->result = iResult;
    pRspPdu->GetHdr()->message = "ERROR";
    pRspPdu->GetHdr()->userAgent = "VISIONDIGI RTSP Server";
    pRspPdu->GetHdr()->session = pReqPdu->GetHdr()->session;
    pRspPdu->GetHdr()->_str_Public = "";
    for(int i = 0; i < RTSP_METHOD_NUM; i++)
    {
        if(i != 0)
        {
            pRspPdu->GetHdr()->_str_Public += " ";
        }
        pRspPdu->GetHdr()->_str_Public += rtspStrFlag[i];
    }

    SendToPeerMsg(pRspPdu);
    return 0;
}

int CRtspSessionSvr::TransRtpOverTcp(int connId,const char* pData,int len,char *ip,unsigned short port)
{
    return VDSocketTcpSend(connId, pData, len);
}
int CRtspSessionSvr::TransRtpOverUdp(int connId,const char* pData,int len,char *ip,unsigned short port)
{
    return VDSocketUdpSend (connId, pData, len, ip, port);
}

int CRtspSessionClt::OptionsReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::DescribeReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}


int CRtspSessionClt::SetupReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}


int CRtspSessionClt::RecordReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::PlayReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::PauseReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::GetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::SetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::TeardownReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::ErrorReq(int engineId, int connId, CRtspPdu* pReqPdu)
{
    return 0;
}

int CRtspSessionClt::TransRtpOverTcp(int connId,const char* pData,int len,char *ip,unsigned short port)
{
    return 0;
}

int CRtspSessionClt::TransRtpOverUdp(int connId,const char* pData,int len,char *ip,unsigned short port)
{
    return 0;
}


