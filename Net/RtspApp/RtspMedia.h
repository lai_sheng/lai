/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              RtspMedia.h
**Version:                  Version 0.01
**Author:                  yang_shukui
**Created:                2010-5-6   16:40
**Modified:               
**Modify Reason: 
**Description:            处理视频和音频数据;部分代码是从原先的CRtspRtp
**                        和CVapVedio中拷备过来的。
*********************************************************************/

#ifndef __RTSP_MEDIA_VEDIO_H__
#define __RTSP_MEDIA_VEDIO_H__

#include <deque>
#include "Devices/DevHDiskReader.h"
#include "System/Object.h"
#include "MultiTask/Mutex.h"
#include "MultiTask/Thread.h"
#include "System/Packet.h"
#include "Configs/ConfigEncode.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "System/ABuffer.h"
#include "TaskTimer.h"
#include "FrameGeneral.h"

class CRtspSession;

#define EXTRA_STREAM_NUM CHL_FUNCTION_NUM  //现在只有主码流和辅码流两种

typedef enum
{
    HanderRtsp   = 0,
	HanderVRtp,
    HanderVRtcp,
    HanderARtp,
    HanderARtcp,
    HanderRtspNr,
}eRtspHandler;

typedef enum
{
    RtspVedio   = 0,
    RtspAudio   = 1,
    RtspMediaNr = 2,
}eRtspMedia;

typedef enum
{
    RTP_OVER_TCP = 0,
    RTP_OVER_UDP = 1,
    RTP_OVER_MULTI_CAST = 2,
    RTP_OVER_NR  = 3,
}eRtpModle;

typedef enum
{   
    MEDIA_VEDIO = 0,
    MEDIA_PLAY = 1,
    MEDIA_DOWNLOAD =2,
}eMediaType;

typedef struct _RTPHDR_
{
    // little endian
    unsigned char csrccount:4;
    unsigned char extension:1;
    unsigned char padding:1;
    unsigned char version:2;
    
    unsigned char payloadtype:7;
    unsigned char marker:1;
    
    unsigned short seqnum;

    unsigned int timestamp;
    unsigned int ssrc;

}RTPHDR;
#define RTPHDR_SIZE (sizeof(RTPHDR))

#define GET_MEDIA_TYPE(x)           (*((uchar *)(x->GetBuffer())))
#define SET_MEDIA_TYPE(x,y)         (*((uchar *)(x->GetBuffer())) = y)
#define GET_MEDIA_BUF(x)            (x->GetBuffer() + MEDIA_TYPE_OFF)  
#define GET_MEDIA_BUF_LEN(x)        (x->GetLength() - MEDIA_TYPE_OFF)
#define MEDIA_TYPE_OFF 4
#define BEGIN_VRTP(x)  (x-1)
#define END_VRTP(x)    (x+1)


typedef enum 
{
    SPEED_SLOW_T16    = 0,
    SPEED_SLOW_T8     = 1,
    SPEED_SLOW_T4     = 2,
    SPEED_SLOW_T2     = 3,
    SPEED_NORMAL      = 4,
    SPEED_QUICK_T2    = 5,
    SPEED_QUICK_T4    = 6,
    SPEED_QUICK_T8    = 7,
    SPEED_QUICK_T16   = 8,
    SPEED_NR          = 9,
}ePlaySpeed;

const double g_PlaySpeed[SPEED_NR] =
{
    0.0625,
    0.125,
    0.25,
    0.5,// 不能写成1/2
    1,
    2,
    4,
    8,
    16,
};

class CRtspMedia : public CObject
{
public:
    #define MAX_BUFFER_LEN (1024 * 1024)     //暂时设置为1M
    typedef std::deque<CPacket *> RtpQueue;
    
public:
    CRtspMedia(CRtspSession *rtspsession);
    virtual ~CRtspMedia();
    
    virtual bool StartMedia() = 0;
    virtual bool StopMedia() = 0;
    
    virtual void OnCapture(int iChannel, uint iCodeType, CPacket *pPacket);
    virtual VD_BOOL DoMediaTask(int media);
    virtual int GetChannel();
    virtual int SetChannel(int Channel);
    virtual int GetStreamType();
    virtual int SetStreamType(int dwStreamType);
    virtual VD_BOOL IsAudioOn();
    virtual void SetSSRC(unsigned long ssrc, int mediaType = RtspVedio);//eRtspMedia
    virtual int GetResolution() = 0;
    virtual VD_BOOL SetInterleaved(eRtspHandler type,int value);
    virtual VD_BOOL SetRtspModel(eRtpModle mode);
    virtual eRtpModle GetRtspModel();

    virtual VD_BOOL SetMediaType(eMediaType mediaType);
    virtual eMediaType GetMediaType();
    virtual VD_BOOL SetVedioTime(char *strTime);
    virtual void SetQueueReleaseFlag(VD_BOOL flag);
    
    virtual void OnConfig(char *cmd, char *para);
	
    ushort SendFrame( uint ts, uint ssrc,
                   uchar *jpeg_data, int len, uchar type,
                   uchar typespec, int width, int height, int dri,
                   uchar q, uchar *lqt, uchar *cqt);
protected:
    virtual void PrcocessVideo(int iCh, CPacket *pPacket,uint iCodeType);
	//处理视频会话的数据包
    virtual void PrcocessJpegVideo(int iCh, CPacket *pPacket,uint iCodeType);

    virtual void PrcocessMultiVideo(int iCh, CPacket *pPacket,uint iCodeType);
    virtual void PrcocessDigiChVideo(int iCh, CPacket *pPacket,uint iCodeType);
    virtual int PrcocessSliceInfo();
       
    int m_iFPS;
    int m_Resolution ;
    SYSTEM_TIME m_StartTime;
    SYSTEM_TIME m_EndTime;

    bool m_bMonitor;    //视频开启标志位
    bool m_bFistIFrame;
    //rtsp会话，负责整个rtsp会话的信令解析和响应及媒体数据处理的调查度
    CRtspSession *m_RtspSession;  
    
protected:
    virtual void PacketFU_A(char* pBuffer, int iLen, bool bFlags , unsigned char nal_header ,int Nums);
    virtual int sendFU_A(int iCh,char* fui,char* fuh,char* data, int len, bool bMarker);
    virtual void SetTimestamp(int mediaType, int timeOff);
    virtual VD_BOOL SendPacket(CPacket *ptk, int mediaType);
    virtual void ClearQueue(int media);
    virtual unsigned short getSequenceNum(int mediaType = RtspVedio);
    virtual int sendAudio(int iCh, char* data, int len, bool bMarker);

    virtual int parse_DQT( struct rtp_jpeg *out, char *d, int len );
    virtual int parse_SOF( struct rtp_jpeg *out, char *d, int len );
    virtual int parse_DHT( struct rtp_jpeg *out, char *d, int len );
    virtual int jpeg_process_frame( struct frame *f, void *d );
    
    virtual void rtp_jpeg_init(void * _p);
    virtual void jpeg_packet_rtp(char *_pInputBuffer, int _iLength);
    
    bool m_flag;
    int m_iSplitNo;


    unsigned long m_ulRtpTimestamp[RtspMediaNr];
    unsigned long m_ulSSRC[RtspMediaNr];
    unsigned short m_usSeq[RtspMediaNr];
    
    int m_iChannel;
    int m_iStreamType;
    VD_BOOL m_bAudioOn;    

    //rtp over rtsp(TCP) 1,UDP 2
    eRtpModle m_rtpModel;
    
    //RTSP over tcp的魔数年对应的ID
    int m_Interleaved[HanderRtspNr];
    int m_Magic;
    
    //Rtp构造包
    CABuffer  m_cabRtpPacket;
    
    RtpQueue m_RtpQueue[HanderRtspNr];
    CMutex m_szMutex[HanderRtspNr];
    
    //缓冲区数据的大小，将根据此值来处理丢帧操作
    unsigned long   m_iDataLen[HanderRtspNr]; 
    
    //时间分配器，给每个rtspSession分配最多不超过x(ms)媒体数据传输时间表，时间用完就只有等到下一个时间片才再处理数据发送.以免多个
    //rtspSesion之间的阻塞，这个时间可以用SetMaxPeriodTime来设置.
    CTaskTimer m_TaskTimer;

    VD_BOOL m_QueueReleaseFlag;//数据发送失败后是否清除队列中相应的节点,下载时需要将此值设置为VD_FALSE;
    eMediaType m_MediaType;

    int m_iFrameHeadLen;
};

class CRtspMediaVedio : public CRtspMedia
{
public:
    CRtspMediaVedio(CRtspSession *rtspsession) ;
    virtual ~CRtspMediaVedio();
    
    virtual bool StartMedia();
    virtual bool StopMedia();
    virtual int GetResolution();
private:
    CConfigEncode* m_pCfgEncode;
    virtual void OnEncodeChange(CConfigEncode *pConfigEncode, int &ret);
};

/* 多播的视频请求 */
class CRtspMediaVedioMulti : public CRtspMediaVedio
{
public:
    CRtspMediaVedioMulti(CRtspSession *rtspsession);
    virtual ~CRtspMediaVedioMulti();
    
    VD_BOOL GetLocalPort(VD_UINT16 &usLocalPort);
    VD_VOID SetLocalPort(VD_UINT16 usLocalPort);
    VD_BOOL CreateUdp();
    
protected:
    void PrcocessMultiVideo(int iCh, CPacket *pPacket,uint iCodeType);

private:
    VD_INT32 m_iSocket;
    VD_INT8  m_acPeerIp[16];
    
    VD_UINT16 m_usRemotePort;
    VD_UINT16 m_usLocalPort;
};

class CRtspMediaVedioMultiManager:public CObject
{
public:
    PATTERN_SINGLETON_DECLARE(CRtspMediaVedioMultiManager);

    CRtspMediaVedioMultiManager();
    virtual ~CRtspMediaVedioMultiManager();

    /* 入参: ich 通道 iFlag 0 主码流 1 副码流 */
    CRtspMediaVedioMulti *GetRtspMedia(CRtspSession *rtspsession,VD_INT32 iCh,VD_INT32 iFlag);
    VD_BOOL  ReleaseRtspMedia(CRtspMediaVedioMulti * pRtspMediaVedio);

private:
    CMutex m_Mutex;
    CRtspMediaVedioMulti *m_apRtspMedia[N_SYS_CH*2];
    VD_INT32 m_iRefNum[N_SYS_CH*2];

};

#define g_RtspMediaVedioMultiManager (*CRtspMediaVedioMultiManager::instance())
   
class CRtspMediaRecord : public CRtspMedia
{
public:
    CRtspMediaRecord(CRtspSession *rtspsession);
    virtual ~CRtspMediaRecord();
    
    bool StartMedia();
    bool StopMedia();
    int GetResolution();    
    void TestVideoFile();
    VD_BOOL SetSpeed(ePlaySpeed speed);
    
private:
    void OnRecord(int iCMD, CPacket *pPacket, int iCh);
    void OnRecordYY(int iCMD, CPacket *pPacket, int iCh);
    void OnReSolution(int iCMD, CPacket *pPacket, int iCh);
    void OnConfig(char *cmd, char *para);
    
    CFrameGeneral *m_FrameParse;
    CDevHDiskReader *m_DiskReader;
    uint GetTimeStamp(CPacket *ptk);
    
    uint m_RecTimeOff;        //记录录像文件的偏移时间，即从开始回放到当前，录像一共偏了多少时间，豪秒级。
    uint m_BeginTime;
    int m_Speed; // > 1快放，< 0,慢放，= 0，暂停 = 1正常播放
    int m_SleepTime;
};

#endif

