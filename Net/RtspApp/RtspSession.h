/*!*******************************************************************
**                  Rtsp Module for General Network
*********************************************************************
**
**        (c)Copyright 2010,Visiondigi Technology Stock Co.Ltd.
**                          All Rights Reserved
**
**File Name:              RtspSession.h
**Version:                  Version 0.01
**Author:                  yang_shukui
**Created:                2010-4-26   13:40
**Modified:               
**Modify Reason: 
**Description:
*********************************************************************/
#ifndef __RTSP_SESSION_H__
#define __RTSP_SESSION_H__

#include <time.h>
#include <string>
#include "MultiTask/Mutex.h"
#include "MultiTask/Guard.h"
#include "MultiTask/Timer.h"
#include "System/Packet.h"
#include "System/ABuffer.h"
#include "OprPdu/RtspPdu.h"
#include "Tplayer/TPTypedef.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "RtspMedia.h"

class CRtspSession : public CObject
{
public:
    typedef int (CRtspSession::*MethodFunc)(int engineId, int connId, CRtspPdu* pReqPdu);
    typedef int (CRtspSession::*RtspTrans)(int connId,const char* pData,int len,char *ip,unsigned short port);
    
    #define MAX_RTSP_RECV_LEN           (4 * 1024)
    #define OPTION_TIMER_INTERVAL       5
    typedef enum
    {
        RELEASE_NO_NEED = 1,
        RELEASE_MEDIA_MSG,
        RELEASE_MSG_MEDIA,
        RELEASE_NEED,
    }eRtspState;
    
public:
    CRtspSession();
    virtual ~CRtspSession();

    VD_BOOL DoMsgTask(int engineId, int connId, const char * pData, size_t len);
    VD_BOOL DoMediaTask();    
    eRtspState GetReleaseFlag();
    eRtspState SetReleaseFlag(eRtspState flag);
    int GetRtspSocket(void);
    int SetRtspSocket(int socket);
    void SetPeerIp(const char *ip);
    int DoRtpPacket(CPacket *packet, int mediaType);
    void CheckOptionsState(unsigned int arg);
    
    virtual int TransRtpOverTcp(int connId,const char* pData,int len,char *ip,unsigned short port) = 0;
    virtual int TransRtpOverUdp(int connId,const char* pData,int len,char *ip,unsigned short port) = 0;

    void OnConfig(char *cmd, char *para);
protected:    
    int SendToPeerMsg(CRtspPdu*& pReqPdu);
    void GetUrlLineData(std::string strUrl,std::string strKey,std::string &strValue);
	virtual VD_BOOL HttpGet(const char *data, int &len);
	virtual VD_BOOL HttpPost(const char *data, int &len);
	
protected:     
    eRtspState  m_NeedRelease;          //RtspSession异步释放标志
    char m_szPeerIp[16];                //远程IP地址
    int m_RtspSocket[HanderRtspNr];
    unsigned short m_RtspRemotePort[HanderRtspNr];
    unsigned short m_RtspLocalPort[HanderRtspNr];
    CRtspMedia *m_Media;
    CTimer* m_pCheckOption;
    time_t m_tmLastOption;
    int m_iOptionCounts;
    CABuffer m_RecvBuf;
    MethodFunc m_MethoFunc[RTSP_METHOD_NUM+1];
    RtspTrans m_RtspTrans[HanderRtspNr];
	VD_BOOL m_OverHttp;
    
private:    
    int GetIndexOfMethod(std::string method);
    void MethodFuncInit();

    //以下成员函数中的参数engineId和connId暂时没用，以备扩展
    virtual int OptionsReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int DescribeReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int DescribeReqYY(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int SetupReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int RecordReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int PlayReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int PauseReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int GetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int SetparameterReq(int engineId,int connId, CRtspPdu * pReqPdu) = 0;
    virtual int TeardownReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0;
    virtual int ErrorReq(int engineId, int connId, CRtspPdu* pReqPdu) = 0; 
};

class CRtspSessionSvr : public CRtspSession
{
public:
    CRtspSessionSvr() : CRtspSession()
    {}
    ~CRtspSessionSvr(){}
    
private:
    //以下成员函数中的参数engineId和connId暂时没用，以备扩展
    int OptionsReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int DescribeReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int DescribeReqYY(int engineId, int connId, CRtspPdu* pReqPdu);
    int SetupReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int RecordReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int PlayReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int PauseReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int GetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int SetparameterReq(int engineId,int connId, CRtspPdu * pReqPdu);
    int TeardownReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int ErrorReq(int engineId, int connId, CRtspPdu* pReqPdu);
    
    int TransRtpOverTcp(int connId,const char* pData,int len,char *ip,unsigned short port);
    int TransRtpOverUdp(int connId,const char* pData,int len,char *ip,unsigned short port);
};

class CRtspSessionClt : public CRtspSession
{
public:
    CRtspSessionClt() : CRtspSession()
    {}
    ~CRtspSessionClt(){}
    
private:
    //以下成员函数中的参数engineId和connId暂时没用，以备扩展
    int OptionsReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int DescribeReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int SetupReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int RecordReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int PlayReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int PauseReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int GetparameterReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int SetparameterReq(int engineId,int connId, CRtspPdu * pReqPdu);
    int TeardownReq(int engineId, int connId, CRtspPdu* pReqPdu);
    int ErrorReq(int engineId, int connId, CRtspPdu* pReqPdu);

    int TransRtpOverTcp(int connId,const char* pData,int len,char *ip,unsigned short port);
    int TransRtpOverUdp(int connId,const char* pData,int len,char *ip,unsigned short port);

};

#endif

