#include "PortPool.h"

#include <algorithm>

PATTERN_SINGLETON_IMPLEMENT(CPortPool)
CPortPool::CPortPool()
{
}

CPortPool::~CPortPool()
{
}

void CPortPool::InitPortPool(unsigned short firstPort, int MaxPairNum)
{
    m_pool.reserve(MaxPairNum);
    m_pool.clear();
    PortElem elem;
    for (size_t i = 0; i < (size_t)MaxPairNum; ++i)
    {
        elem.first = true;   //unused flag
        elem.second.first = short(firstPort + 2 * i);
        elem.second.second = short(firstPort + 2 * i + 1);
        m_pool.push_back(elem);
    }
}

bool CPortPool::GetPortPair(PortPair &pair)
{
    //Not Inited yet?
    if (m_pool.empty())
    {
        InitPortPool(20000, 1000);
        return false;
    }

    PortPool::iterator it = m_pool.begin();
    for (; it != m_pool.end(); ++it)
    {
        if (it->first)
        {
            pair = it->second;
            it->first = false;
            return true;
        }
    }

    // tracepoint();
    return false;
}

bool CPortPool::ReleasePortPair(PortPair pair)
{
    PortPool::iterator it = find(m_pool.begin(), m_pool.end(), std::make_pair(false, pair));
    if (it == m_pool.end())
    {
        //Not found?
        // tracepoint();
        return false;
    }

    //release port
    it->first = true;
    return true;
}

///Release port pair overloads with [first, first+1];
bool CPortPool::ReleasePortPair(unsigned short first)
{
    return ReleasePortPair(std::make_pair(first, first+1));
}



