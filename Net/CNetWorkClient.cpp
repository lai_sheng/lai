#ifdef WIN32#pragma warning( disable : 4786)#endif#include "Net/CNetWorkClient.h"#include "Devices/DevCapture.h"#include "Configs/ConfigNet.h"#include "Configs/ConfigConversion.h"#include "Configs/ConfigLocation.h"#include "Net/NetApp.h"#include "Functions/DriverManager.h"#include "APIs/Ide.h"#include "System/AppConfig.h"#include "OprPdu/FilterHelper.h"#include "OprPdu/IVnPdu.h"#include "VideoMonitor.h"#include "System/Console.h"//#include "Net/FtpServer/FtpServerWQ.h"#include "Net/NetConfig.h"#include "System/File.h" CClientUpgrate::CClientUpgrate():m_ifilecurrent(0),m_ifiletotal(0),m_istatus(0) { 	m_file = NULL; } CClientUpgrate::~CClientUpgrate() {	if(NULL != m_file)	{		delete m_file;	} } void CClientUpgrate::Start(int iFilelen) {	//m_file.Open("/home/updata.bin",);	if(0 == iFilelen)	{		return;	}	if( 0 == access("/home/updata.bin", 0))	{		system("rm /home/updata.bin -rf");	}	m_file = new CFile();	assert(m_file);	if(m_file->Open("/home/updata.bin", CFile::modeCreate|CFile::modeReadWrite | CFile::modeNoTruncate))	{		m_ifiletotal = iFilelen;		m_istatus = 1;		m_file->Seek(0, CFile::begin);		m_ctimer.Start(this, (VD_TIMERPROC)&CClientUpgrate::TimerCheck, 60000, 60000);	}	else	{		delete m_file;		m_file = NULL;	}}int CClientUpgrate::UpgrateStatus(){	return  m_istatus;}void CClientUpgrate::Stop(){	if(NULL != m_file)	{		m_file->Close();		system("rm /home/updata.bin -rf");		delete m_file;		m_file = NULL;	}	m_ifilecurrent = 0;	m_ifiletotal = 0;	m_istatus = 0;}void CClientUpgrate::AppendData(char * buf,int len){	if(NULL == buf || len == 0 || NULL == m_file)	{		_printd("data no invaile!!!");		return;	}	m_ctimer.Stop();	m_file->Write(buf,len);	m_file->Flush();	m_ifilecurrent += len;		_printd("m_ifilecurrent = [%d]", m_ifilecurrent);	if(m_ifilecurrent == m_ifiletotal)	{		m_file->Close();		Upgrate();		Stop();	}	else	{		m_ctimer.Start(this, (VD_TIMERPROC)&CClientUpgrate::TimerCheck, 60000, 60000);	}}int CClientUpgrate::TimerCheck(int arg){	Stop();//1分钟无数据就升级失败}bool CClientUpgrate::Upgrate(){	_printd("tar start....\n\n\n\n");	return 0;	char Command[128];	system("rm -rf /home/hisi/app/*");	#ifdef CHIP_S2L	sprintf (Command, "tar -xvf %s/%s -C %s", "/home", "updata.bin", "/home/amba/app");	#else	sprintf (Command, "tar -xzvf %s/%s -C %s", "/home", "updata.bin", "/home/hisi/app");	#endif	if (0 != system(Command)){//tar -c new 		return FALSE;	}	_printd("tar Ok Ok!!!@@@@@####");	/*if (0 == access(RECV_IPNCPATH, F_OK)){		_printd ("#####Move ipnc to /appex!!!!");		system("rm -rf /appex/ipnc");		sprintf (Command, "mv %s %s -f", RECV_IPNCPATH, IPNCPATH);		system(Command);		sprintf (Command, "chmod 0777 -R %s", IPNCPATH);		system(Command);	}	UpdateSendProgress(lpCliInfo->client_sock, 80);	fsync(lpCliInfo->client_sock);	Update_DelBackUp(*pUpdateFileType);//删除备份	UpdateSendProgress(lpCliInfo->client_sock, 90);	fsync(lpCliInfo->client_sock);*/	return TRUE;	} CClientSnap::CClientSnap(){	m_ichanel = 0;	m_iStart  = 0;	m_iSock = -1;}CClientSnap::~CClientSnap(){}void CClientSnap::StartSnap(int sock, int ichanel /*= 0*/){	m_iSock = sock;	m_ichanel = ichanel;     if (CDevCapture::instance(m_ichanel)->Start(this,         (CDevCapture::SIG_DEV_CAP_BUFFER)&CClientSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))    {		_printd("start snap.....!");    }}void CClientSnap::StopSnap(){     if (CDevCapture::instance(m_ichanel)->Stop(this,      (CDevCapture::SIG_DEV_CAP_BUFFER)&CClientSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))    {       _printd("stop snap.....!");    }	m_iSock = -1;}void CClientSnap::OnSnap(int iChannel, uint dwStreamType, CPacket *pPacket){    //! 判断是否为抓图的辅码流    if ((dwStreamType != CHL_JPEG_T) || (!pPacket))    {        return;    }    CGuard l_Guard(m_SnapMutex);        CPacket* pPkt = pPacket;    PKT_HEAD_INFO    *pkthead = (PKT_HEAD_INFO *)(pPkt->GetHeader());    uchar byFlag = (uchar)pkthead->FrameInfo[0].FrameFlag;	DHTIME dhtime;	if(!m_iStart && byFlag != PACK_CONTAIN_FRAME_HEAD && byFlag != PACK_CONTAIN_FRAME_HEADTRAIL)	{		return;	}	if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))    {    	m_iStart = 1;		//取码流里的时间        getFirstFrameTime(pPkt, &dhtime, PACK_TYPE_FRAME_JEPG);        //!一张图片的第一个数据包的前16个字节去掉        pPkt = g_PacketManager.GetPacket(pPacket->GetLength());        if( NULL == pPkt )        {            errorf("alloc packet failed!\n");            return;        }                //去掉数据前的12字节(8空字节+ 4 DHTIME)#if	defined(CHIP_S2L) || defined(DOME_S2L)        pPkt->SetLength(pPacket->GetLength() - 20);     	memcpy(pPkt->GetHeader(), pPacket->GetHeader(), PKT_HDR_SIZE);        memcpy(pPkt->GetBuffer(), pPacket->GetBuffer() + 20, pPkt->GetLength());#else		pPkt->SetLength(pPacket->GetLength() - 16);      	memcpy(pPkt->GetHeader(), pPacket->GetHeader(), PKT_HDR_SIZE);        memcpy(pPkt->GetBuffer(), pPacket->GetBuffer() + 16, pPkt->GetLength());#endif     }	SendPic(pPkt ,  byFlag, &dhtime);    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))    {        pPkt->Release();    }	if ((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))	{		m_iStart = 0;		StopSnap();	}}int connt =0;int CClientSnap::SendPic(CPacket *pPacket,uchar byFlag, DHTIME *pDHTIME){	int  sendSock = -1;    	connt++;	sendSock = m_iSock;	if( -1 != sendSock )	{	   if(byFlag == PACK_CONTAIN_FRAME_HEAD		||byFlag == PACK_CONTAIN_FRAME_HEADTRAIL)		{			connt =1;		}					pPacket->AddRef();		struct iovec msghdr[2];		DVRIP ip;							memset(&ip, 0, sizeof(ip));		memset(&msghdr, 0, sizeof(msghdr));		ip.dvrip_cmd= RESP_DEV_SNAP;		ip.dvrip_extlen= pPacket->GetLength();				ip.dvrip_p[0]	= 0;		ip.dvrip_p[1]	= 0x00; 		//ip.dvrip_p[2] = 0x40;		//!合入帧头尾标志信息		ip.dvrip_p[2] =  byFlag;		ip.dvrip_p[5] = connt;		ip.dvrip_p[16]	= CHL_JPEG_T;		if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))		{	 			memcpy(&ip.dvrip_p[20],pDHTIME,4);	 		}		msghdr[0].iov_base = &ip;		msghdr[0].iov_len  = sizeof(ip);		msghdr[1].iov_base = pPacket->GetBuffer();		msghdr[1].iov_len  = pPacket->GetLength();		_printd("\n\nsnap size is %d\n\n",pPacket->GetLength());        writev(sendSock, msghdr, 2);		//增加等待时间,否则会导致send 过快 网络发送失败问题		pPacket->Release();		SystemSleep(10); 			}	return 0;	}//PATTERN_SINGLETON_IMPLEMENT(CNetWorkClient);CNetWorkClient::CNetWorkClient():CThread("NetWorkClient", TP_NET), m_bIsNetRunning(false),m_snap(NULL){    m_iSock = -1;	m_bRegisterFlag = false;	m_iflags = 0;	memset(&m_remote, 0, sizeof(struct io));	m_snap = new CClientSnap();	m_upgrate = new CClientUpgrate();	m_ctimer.Start(this, (VD_TIMERPROC)&CNetWorkClient::TimerCheckBuf, 60000, 60000);}CNetWorkClient::~CNetWorkClient(){	if(NULL != m_snap)	{		delete m_snap;	}	m_ctimer.Stop();}bool CNetWorkClient::ConnectRemoteServer(const char *szServer, const unsigned short nPort_){	unsigned short nPort = 0;	LPSERVENT lpServEnt;	SOCKADDR_IN sockAddr;	fd_set fdwrite,fdexcept;	timeval timeout;	int res = 0;	bool conected = false;	m_iSock = INVALID_SOCKET;	if((m_iSock = socket(PF_INET, SOCK_STREAM,0)) == INVALID_SOCKET)	{		return false;	}	if(nPort_ != 0)	{		nPort = htons(nPort_);	}			sockAddr.sin_family = AF_INET;	sockAddr.sin_port = nPort;	if((sockAddr.sin_addr.s_addr = inet_addr(szServer)) == INADDR_NONE)	{		LPHOSTENT host;					host = gethostbyname(szServer);		if (host)			memcpy(&sockAddr.sin_addr,host->h_addr_list[0],host->h_length);		else		{			close(m_iSock);			return false;		}					}	// start non-blocking mode for socket:	/*if(ioctl(m_iSock,FIONBIO, (unsigned long*)&ul) == SOCKET_ERROR)	{		close(m_iSock);		return false;	}*/	#ifdef LINUX			int keepAlive = 1; // 开启keepalive属性	int keepIdle = 60; // 如该连接在60秒内没有任何数据往来,则进行探测	int keepInterval = 5; // 探测时发包的时间间隔为5 秒	int keepCount = 3; // 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.	if( setsockopt(m_iSock, SOL_SOCKET, SO_KEEPALIVE, (void *)&keepAlive, sizeof(keepAlive)) < 0)	{		tracef("setsockopt keepalive failed..\n");	}	 	else if(  setsockopt(m_iSock, SOL_TCP, TCP_KEEPIDLE, (void*)&keepIdle, sizeof(keepIdle)) < 0 )	{		tracef("setsockopt keepidle failed..\n");	}	else if( setsockopt(m_iSock, SOL_TCP, TCP_KEEPINTVL, (void *)&keepInterval, sizeof(keepInterval)) < 0 )	{		tracef("setsockopt keepintvl failed..\n");	}	else if( setsockopt(m_iSock, SOL_TCP, TCP_KEEPCNT, (void *)&keepCount, sizeof(keepCount)) < 0 )	{		tracef("setsockopt keepcnt failed..\n");	}					//gettimeofday(&current_time, NULL);#else			//current_time.tv_sec = 0;			//current_time.tv_usec = SystemGetMSCount()*1000; #endif	if( connect(m_iSock,(LPSOCKADDR)&sockAddr,sizeof(sockAddr)) == SOCKET_ERROR )	{		if(errno != EINPROGRESS)		{			close(m_iSock);			return false;		}	}	#if defined(WIN32)				unsigned long	 ulON = 1;				ioctlsocket(m_iSock, FIONBIO, &ulON);#else				int    flags = fcntl(m_iSock, F_GETFL, 0);				if (flags != -1)				{					fcntl(m_iSock, F_SETFL, flags | O_NONBLOCK) ;				}				int nSendBuf = 128*1024;//设置为128K				setsockopt(m_iSock,SOL_SOCKET,SO_SNDBUF,(const char*)&nSendBuf,sizeof(int));			#endif /* _WIN32 */	DVRIP szHead;	memset(&szHead, 0 , sizeof(DVRIP));	szHead.dvrip_cmd = DEV_REGISTER;	memcpy(&szHead.dvrip_p[0],"1234567890",10);	int count = 0;	int nbyte = 0;	sleep(10);//延时10秒  要不不成功//	nbyte = send(m_iSock, &szHead, sizeof(DVRIP),0);//	_printd("====nbyte = [%d]\n",nbyte);	while(1)	{	    nbyte = 0;		nbyte = send(m_iSock, &szHead, sizeof(DVRIP),0);		sleep(10);		if(nbyte == 32)		{			break;		}		else		{			_printd("send.....");			count++;			if(count >= 3)			{				return false;			}		}	}		sleep(10);	return true;}int CNetWorkClient::Run(const int argc, char *argv[]){    tracef("NetWorkService Run=======>>>>>>>>>>>>>>>>>\n");    assert(m_bIsNetRunning == false);    m_bExitNetThread = false;        if(CreateThread() == FALSE)    {        assert(0);        return FAILURE_RET;    }    return SUCCESS_RET;}void CNetWorkClient::svc(){       while (!m_bExitNetThread)    {		while(!ConnectRemoteServer("172.16.2.98", 6009))		//while(!ConnectRemoteServer("27.18.17.108", 5555))		{			_printd("Failed to connect to the server!!! ");			_printd("IO_DATALEN(m_remote)  = [%d]",IO_DATALEN(&m_remote));			sleep(5); 		}		m_bIsNetRunning = true;		_printd("m_bIsNetRunning = true;");		while(m_bIsNetRunning)		{			_printd("connect to server successfull!");			NetClientPoll(5000);			_printd("======test----- m_bIsNetRunning = [%d]",m_bIsNetRunning);		}    }}void CNetWorkClient::ThreadProc(){    CNetWorkClient *pClientDlgProc = (CNetWorkClient *)this;    assert(pClientDlgProc);    pClientDlgProc->svc();        return ;}int CNetWorkClient::Stop(const int argc, char *argv[]){    tracef("CNetWorkClient Stop=======>>>>>>>>>>>>>>>>>\n");    if( m_bExitNetThread == false)     {       	m_bExitNetThread = TRUE;        DestroyThread();        m_bIsNetRunning = false;    }    return SUCCESS_RET;}/*struct sockaddr_in *CNetWorkClient::GetRemoteAddr(int iDlgNo){    static struct sockaddr_in remoteaddr;    struct conn *pNetConnection = GetConn(iDlgNo);    if(pNetConnection)     {        memcpy(&remoteaddr, &pNetConnection->sa.u.sin, sizeof(struct sockaddr_in));    }    return &remoteaddr;}*/int CNetWorkClient::NetClientRecv(){	if(-1 == m_iSock)    {        return 0;    }        int n = 0;    if (m_iflags & FLAG_SOCK_READABLE)     {        const unsigned long len = IO_SPACELEN(&m_remote);        n = recv(m_iSock, m_remote.buf + m_remote.head, len, 0);		_printd("recv n = %d\n",n);		_printd("m_remote.done = [%d].....!",m_remote.done);        if (n == 0 || (n < 0 && ERRNO != EWOULDBLOCK))        {        	_printd("m_remote.done = [%d].....!",m_remote.done);            perror("readremote");            m_remote.done = 1;            _printd("ddddddddddddddddd");            tracef("$$$$$$$$$$$ socket is %d, ret: %d\n",m_iSock, n);        }                if (n > 0)        {            m_remote.head += n;            m_remote.buf[m_remote.head] = '\0';        }         else if (m_remote.done)         {            m_iflags |= FLAG_FINISHED;            m_iflags &= ~FLAG_IO_READY;        }    }        return n;	}int CNetWorkClient::NetClientPoll(int milliseconds){	_printd("struct timeval    tv; ");    struct timeval    tv;            /* Timeout for select() */    fd_set        read_set, write_set;    int        max_fd = 0, msec = milliseconds;        FD_ZERO(&read_set);    FD_ZERO(&write_set);    int error=-1, len;	len = sizeof(error);	int     ret = getsockopt(m_iSock, SOL_SOCKET, SO_ERROR, (char*)&error, (socklen_t *)&len);	if( (ret != 0) || (error == EPIPE) )	{		printf("NetCorePoll():ret= %d error = %d\r\n",ret,error);	    CloseSocket();		return 0;	}#define    MERGEFD(fd,set)    \    do {FD_SET(fd, set); if (fd > max_fd) max_fd = fd; } while (0)    	/* If there is space to read, do it */	if (IO_SPACELEN(&m_remote))		{MERGEFD(m_iSock, &read_set);}    tv.tv_sec = msec / 1000;    tv.tv_usec = msec % 1000;    /* Check IO readiness */    if (select(max_fd + 1, &read_set, NULL, NULL, &tv) < 0)     {        tracef("select: %s\r\n", strerror(errno));        return 0;    }	_printd(" m_iflags |= FLAG_SOCK_READABLE;");    /* Set IO readiness flags */	if (FD_ISSET(m_iSock, &read_set)) 	{		_printd("aaccccsdeqerwew");		m_iflags |= FLAG_SOCK_READABLE;	}	if (FD_ISSET(m_iSock, &write_set))	{		m_iflags |= FLAG_SOCK_WRITABLE;		_printd(" m_iflags |= FLAG_SOCK_READABLE;");	}    /* handle if IO ready */	if (m_iflags & FLAG_SOCK_READABLE)    {    	_printd("cccccccc");		_printd("m_iFlag = [%d]",m_iflags);		NetClientIoProcess();		_printd("m_iFlag = [%d]",m_iflags);	}	if (m_iflags & FLAG_FINISHED)	{		_printd("FLAG_FINISHED");		CloseSocket();		return false;	}	return 0;    }int CNetWorkClient::TimerCheckBuf(int arg){	int len = IO_DATALEN(&m_remote);	if(len && (m_iRecvLen == len))	{		m_iRecvFlag++;		if(m_iRecvFlag >= 5)		{			m_iRecvFlag = 0;			memset(&m_remote, 0, sizeof(struct io));//清空数据		}	}	else	{		m_iRecvFlag = 0;		m_iRecvLen = len;		_printd("m_iRcvLen = [%d]",len);	}}void CNetWorkClient::CloseSocket(){	(void) shutdown(m_iSock, 2);	(void) close(m_iSock);	m_bIsNetRunning = false;	m_bRegisterFlag = false;	m_iflags = 0;	m_iSock = INVALID_SOCKET;	memset(&m_remote, 0, sizeof(struct io));//	_printd("\n\n\nm_remote.done = [%d]\n\n\n\n",m_remote.done);}void CNetWorkClient::NetClientIoProcess(){    assert(m_iSock != -1);     unsigned long len;	_printd("NetClientRecv();");    NetClientRecv();    len = IO_DATALEN(&m_remote);    while(len >= sizeof(DVRIP))     {    	struct dvrip szHead;        memset(&szHead, 0 , sizeof(struct dvrip));        memcpy(&szHead,(char *)(m_remote.buf + m_remote.tail),sizeof(struct dvrip));		_printd("====len[%d] szHead.dvrip_extlen[%d]=====drip.cmd = %d\n", len, szHead.dvrip_extlen,szHead.dvrip_cmd);		if (len >= (szHead.dvrip_extlen + sizeof(DVRIP)))         {       		_printd("====MsgProcess((char *)m_remote.buf + m_remote.tail, szHead.dvrip_extlen + sizeof(DVRIP));===== drip.cmd = %d\n", szHead.dvrip_cmd);	            MsgProcess((char *)m_remote.buf + m_remote.tail, szHead.dvrip_extlen + sizeof(DVRIP));            m_remote.tail += szHead.dvrip_extlen + sizeof(DVRIP);            len = IO_DATALEN(&m_remote);        }         else         {            break;        }    }    if ( len > 0 )    {        memcpy(m_remote.buf, m_remote.buf + m_remote.tail, len);        m_remote.head = len;         m_remote.tail = 0;    }	_printd("m_remote.tail = %d;m_remote.head = %d;\n",m_remote.tail,m_remote.head);}int CNetWorkClient::MsgProcess(char* buf, int len){		CPacket *pPacker = NULL;	int iSendFlag = 0;	int itotalen = 0;	DVRIP szHead;	memset(&szHead, 0 , sizeof(DVRIP));	memcpy(&szHead,buf,sizeof(DVRIP));		_printd("memcpy(&szHead,buf,sizeof(DVRIP));");	if(false == m_bRegisterFlag && DEV_REGISTER != szHead.dvrip_cmd)	{		_printd("false == m_bRegisterFlag && DEV_REGISTER != szHead.dvrip_cmd");		CloseSocket();  	}	else if(false == m_bRegisterFlag && DEV_REGISTER == szHead.dvrip_cmd)	{		if(szHead.dvrip_r0)		{			m_bRegisterFlag = true;			_printd("Register successfull!\n");			return 0;		}		else		{			_printd("aaaaaaaa");			CloseSocket();			return -1;		}	}		CIVnPdu * pReq = new CIVnPdu();	CIVnPdu * pResp = 0;	bool have_msg_out = false;	int iRet = 0;    if (pReq && pReq->parseBody(buf, len))    {                tracef("@@@@CNetWorkClient::MsgProcess@@@  cmd:<%d>\n", pReq->m_pIpHdr->dvrip_cmd);    		switch(pReq->m_pIpHdr->dvrip_cmd)		{		case QUERY_DEV_STATUS:		{				QueryDevStatus(pReq, pResp, have_msg_out);			/*struct devstatus _dev_status;			memset(&_dev_status,0,sizeof(struct devstatus));			_dev_status.iDev4gStatus = 1;			_dev_status.iDevStatus  = 1;			_dev_status.iDevWifiStatus = 1;			_dev_status.iDiskCap = 10000;			_dev_status.iDiskReCap = 5000;			_dev_status.iDiskStatus = 1;			memcpy(_dev_status.verdata,"IPCamer_V4.8.10",15);			itotalen = sizeof(struct devstatus) + sizeof(DVRIP);			szHead.dvrip_extlen = sizeof(struct devstatus);						pPacker = g_PacketManager.GetPacket( itotalen );			char* pBuffer = (char* )pPacker->GetBuffer();			memcpy(pBuffer, &szHead, sizeof(DVRIP));			memcpy(pBuffer + sizeof(DVRIP), &_dev_status, sizeof(struct devstatus));			iSendFlag = 1;*/		}			break;		case QUERY_DEV_WIFI:		{			QueryDevWifiInfo(pReq, pResp, have_msg_out);			/*			struct WIFI_Info _wifi_info;			memset(&_wifi_info,0,sizeof(struct WIFI_Info));			_wifi_info.iEnable = 1;			memcpy(_wifi_info.szSsid, "wja_ipcam", 10);			memcpy(_wifi_info.szPswd, "1234567890", 11);			itotalen = sizeof(struct WIFI_Info) + sizeof(DVRIP);			szHead.dvrip_extlen = sizeof(struct WIFI_Info);						pPacker = g_PacketManager.GetPacket( itotalen );			char* pBuffer = (char* )pPacker->GetBuffer();			memcpy(pBuffer, &szHead, sizeof(DVRIP));			memcpy(pBuffer + sizeof(DVRIP), &_wifi_info, sizeof(struct WIFI_Info));			iSendFlag = 1;*/		}		break;		case MOD_DEV_WIFI:		{			ModDevWifiInfo(pReq, pResp, have_msg_out);			/*struct WIFI_Info _wifi_info;			memcpy(&_wifi_info,buf + sizeof(DVRIP),sizeof(struct WIFI_Info));			_printd("wifi_info.iEnable = [%d]",_wifi_info.iEnable);			_printd("wifi_info.ssid = [%s]",_wifi_info.szSsid);			_printd("wifi_info.pwsd = [%s]",_wifi_info.szPswd);*/		}		break;		case QUERY_DEV_LOG:		{			QueryDevLog(pReq, pResp, have_msg_out);		/*	int iRet;		    int index = 0;		    uint uiLogPos = 0;		    NET_QUERY_SYSYTEM_LOG Cfg;		    memset(&Cfg,0,sizeof(NET_QUERY_SYSYTEM_LOG));   		    NET_LOG_INFO LogInfoCfg;		    memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));		    LOG_ITEM szLogBuffer;			char szLogstr[LOG_LINE_MAX] = {0};		    	//	    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );			memcpy(&Cfg ,buf + sizeof(DVRIP),sizeof(NET_QUERY_SYSYTEM_LOG));			_printd("Cfg.ucQueryType = [%d]",Cfg.ucQueryType);		    if(Cfg.ucQueryType == 0)		    {		        tracef("-----QueryLogs QueryType is wrong!\n");		        return 0;		    }		    int iType = Cfg.ucLogType;		    g_Log.SetRange(&Cfg.stStartTime, &Cfg.stEndTime);						_printd("starttime y[%d]-[%d]-[%d]",Cfg.stStartTime.year, Cfg.stStartTime.month, Cfg.stStartTime.day);			_printd("starttime d[%d]-[%d]-[%d]",Cfg.stStartTime.hour, Cfg.stStartTime.minute, Cfg.stStartTime.second);			_printd("endtime y[%d]-[%d]-[%d]",Cfg.stEndTime.year, Cfg.stEndTime.month, Cfg.stEndTime.day);			_printd("endtime d[%d]-[%d]-[%d]",Cfg.stEndTime.hour, Cfg.stEndTime.minute, Cfg.stEndTime.second);		    uint iTotalItem = g_Log.GetCount();			_printd("iTotalItem = %d", iTotalItem);		    uint iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);		    if (iTotalItem >= 1000)		    {		        iTotalItem = 1000;		        iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);		    }						    pPacker = g_PacketManager.GetPacket(iTotalLen + sizeof(DVRIP));		    assert( pPacker != NULL );			szHead.dvrip_cmd = RESP_DEV_LOG;			szHead.dvrip_extlen = iTotalLen;				itotalen = iTotalLen + sizeof(DVRIP);			int iFlag = 1;		    //转化成对应的类型		    _printd("iType = %d",iType);		    if(iType < 0)		    {		        tracef("-----QueryLogs iType is wrong!\n");		        //pPacker->Release();		        iFlag = 0;		        iTotalLen = 0;				itotalen = sizeof(DVRIP);		    }		    else if(iType == 0)		    {		        iType = LOG_TYPE_NR;		    }		    else		    {		        iType--;		    }			pPacker->PutBuffer((unsigned char *)&szHead, sizeof(DVRIP));		    while(iFlag && (iRet = g_Log.GetContext(iType, &uiLogPos, szLogstr, &szLogBuffer)) != -1 && (index < iTotalItem))		    {		        memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));		        memset(szLogstr, 0, sizeof(szLogstr));		        		        LogInfoCfg.usType = szLogBuffer.type;		        LogInfoCfg.ucFlag= szLogBuffer.flag;		        LogInfoCfg.ucData = szLogBuffer.data;		        timedh2sys(&LogInfoCfg.stTime, &szLogBuffer.time);        		        //strcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context);  				memcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context,8);		        pPacker->PutBuffer((unsigned char *)&LogInfoCfg, sizeof(NET_LOG_INFO));		        uiLogPos++;//指向下一个日志项		        index++;		    }			itotalen = pPacker->GetLength();			szHead.dvrip_cmd = RESP_DEV_LOG;			szHead.dvrip_extlen = pPacker->GetLength() - sizeof(DVRIP);				memcpy(pPacker->GetBuffer(), &szHead, sizeof(DVRIP));			_printd("szHead.dvrip_extlen = %d",szHead.dvrip_extlen);			_printd("itotalen = %d", itotalen);			iSendFlag = 1;*/		}		break;		case QUERY_DEV_SNAP:		{			m_snap->StartSnap(m_iSock);		}		break;		case UPDATA_VESION:		{			int filelen = 0;			memcpy(&filelen, &szHead.dvrip_p[0], 4);			_printd("filelen = [%d]", filelen);			m_upgrate->Start(filelen);				}		break;		case UPDATA_NOW:		{			if(m_upgrate->UpgrateStatus())			{				m_upgrate->AppendData(buf + sizeof(DVRIP), szHead.dvrip_extlen);			}		}		break;		default : 						break;		}    }	if(pResp && have_msg_out)	{		_printd("send sock.....");		send(m_iSock, (char* )pResp->GetPacket(), pResp->GetPacketLen(), 0);		delete pResp;	}	if(NULL != pReq)	{		delete pReq;	}	}int  CNetWorkClient::GetSock(){	return m_iSock;}static char* cmd_system(const char* command){    char* result = NULL;    FILE *fpRead;    fpRead = popen(command, "r");	if(fpRead)	{	    char buf[1024];	    memset(buf,'\0',sizeof(buf));	    while(fgets(buf,1024-1,fpRead) != NULL)	    { 			result = buf;	    }        pclose(fpRead);	}    return result;}int CNetWorkClient::QueryDevStatus(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU, bool &have_msg_out){    pRespPDU = new CIVnPdu();	if(!pRespPDU)    {        return -1;    }	struct devstatus _dev_status;	memset(&_dev_status,0,sizeof(struct devstatus));	_dev_status.iDev4gStatus = 1;	_dev_status.iDevStatus  = 1;	_dev_status.iDevWifiStatus = 1;	_dev_status.iDiskCap = 10000;	_dev_status.iDiskReCap = 5000;	_dev_status.iDiskStatus = 1;	memcpy(_dev_status.verdata, "IPCamer_V4.8.10", 15);    pRespPDU->packetBody((char *)&_dev_status, sizeof(struct devstatus));        pRespPDU->m_pIpHdr->dvrip_cmd = QUERY_DEV_STATUS;	have_msg_out = true;		return 0;}int CNetWorkClient::QueryDevWifiInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU, bool &have_msg_out){    pRespPDU = new CIVnPdu();	if(!pRespPDU)    {        return -1;    }	struct WIFI_Info _wifi_info;	memset(&_wifi_info,0,sizeof(struct WIFI_Info));/* //普通IPC暂时不能测试	char *ssid = NULL;	char *pswd = NULL;//	WLAN_INFO wifi_info;//	memset(&wifi_info,0,sizeof(WLAN_INFO));	ssid = cmd_system("grep \"ssid\"  /mnt/mtd/Config/hostapd.conf | sed \"s/ssid=//g\"");	int n;	if( NULL != ssid)	{		n = strlen(ssid);		if(n > 0 && (ssid[n-1] == '\n'))		{			ssid[n-1] = '\0';		}		strncpy(_wifi_info.szSsid, ssid, strlen(ssid));	}	else	{		return -1;	}	pswd = cmd_system("grep \"wpa_passphrase\"  /mnt/mtd/Config/hostapd.conf | sed \"s/wpa_passphrase=//g\"");	if(NULL != pswd)	{		n = strlen(pswd);		if(n > 0 && (pswd[n-1] == '\n'))		{			pswd[n-1] = '\0';		}		strncpy(_wifi_info.szPswd,pswd,strlen(pswd));	}	else	{		return -1;	}*/	_wifi_info.iEnable = 1;	memcpy(_wifi_info.szSsid, "wja_ipcam", 10);	memcpy(_wifi_info.szPswd, "1234567890", 11);	pRespPDU->packetBody((char *)&_wifi_info, sizeof(struct WIFI_Info));        pRespPDU->m_pIpHdr->dvrip_cmd = QUERY_DEV_WIFI;	have_msg_out = true;		return 0;}int CNetWorkClient::ModDevWifiInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU, bool &have_msg_out){	if( !pReqPDU )	{		return -1;	}		struct WIFI_Info _wifi_info;	memcpy(&_wifi_info, pReqPDU->GetPacket() + sizeof(DVRIP),sizeof(struct WIFI_Info));	_printd("wifi_info.iEnable = [%d]", _wifi_info.iEnable);	_printd("wifi_info.ssid = [%s]", _wifi_info.szSsid);	_printd("wifi_info.pwsd = [%s]", _wifi_info.szPswd);/*	CConfigWlanInfo* pWlanInfo = new CConfigWlanInfo();    pWlanInfo->update();	CONFIG_WLAN_INFO& cfgWlanInfo = pWlanInfo->getConfig(0);	sprintf(cfgWlanInfo.szSsid,"%s",_wifi_info->ssid);	sprintf(cfgWlanInfo.szPassword,"%s",_wifi_info->password);	pWlanInfo->commit();	delete pWlanInfo;	g_NetApp.ReStartWifi();*/	return 0;}int CNetWorkClient::QueryDevLog(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU, bool &have_msg_out){	int iRet;	int index = 0;	uint uiLogPos = 0;	CPacket *pPacker = NULL;	NET_QUERY_SYSYTEM_LOG Cfg;	memset(&Cfg,0,sizeof(NET_QUERY_SYSYTEM_LOG));   	NET_LOG_INFO LogInfoCfg;	memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));	LOG_ITEM szLogBuffer;	char szLogstr[LOG_LINE_MAX] = {0};	//	    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );	memcpy(&Cfg ,pReqPDU->GetPacket()+ sizeof(DVRIP),sizeof(NET_QUERY_SYSYTEM_LOG));	_printd("Cfg.ucQueryType = [%d]",Cfg.ucQueryType);	if(Cfg.ucQueryType == 0)	{	    tracef("-----QueryLogs QueryType is wrong!\n");	    return 0;	}	int iType = Cfg.ucLogType;	g_Log.SetRange(&Cfg.stStartTime, &Cfg.stEndTime);	_printd("starttime y[%d]-[%d]-[%d]",Cfg.stStartTime.year, Cfg.stStartTime.month, Cfg.stStartTime.day);	_printd("starttime d[%d]-[%d]-[%d]",Cfg.stStartTime.hour, Cfg.stStartTime.minute, Cfg.stStartTime.second);	_printd("endtime y[%d]-[%d]-[%d]",Cfg.stEndTime.year, Cfg.stEndTime.month, Cfg.stEndTime.day);	_printd("endtime d[%d]-[%d]-[%d]",Cfg.stEndTime.hour, Cfg.stEndTime.minute, Cfg.stEndTime.second);	uint iTotalItem = g_Log.GetCount();	_printd("iTotalItem = %d", iTotalItem);	uint iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);	if (iTotalItem >= 1000)	{	    iTotalItem = 1000;	    iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);	}			pPacker = g_PacketManager.GetPacket(iTotalLen);	assert( pPacker != NULL );	int iFlag = 1;	//转化成对应的类型	_printd("iType = %d",iType);	if(iType < 0)	{	    tracef("-----QueryLogs iType is wrong!\n");	    //pPacker->Release();	    iFlag = 0;	    iTotalLen = 0;	}	else if(iType == 0)	{	    iType = LOG_TYPE_NR;	}	else	{	    iType--;	}	while(iFlag && (iRet = g_Log.GetContext(iType, &uiLogPos, szLogstr, &szLogBuffer)) != -1 && (index < iTotalItem))	{	    memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));	    memset(szLogstr, 0, sizeof(szLogstr));	    	    LogInfoCfg.usType = szLogBuffer.type;	    LogInfoCfg.ucFlag= szLogBuffer.flag;	    LogInfoCfg.ucData = szLogBuffer.data;	    timedh2sys(&LogInfoCfg.stTime, &szLogBuffer.time);        	    //strcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context);  		memcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context,8);	    pPacker->PutBuffer((unsigned char *)&LogInfoCfg, sizeof(NET_LOG_INFO));	    uiLogPos++;//指向下一个日志项	    index++;	}	    pRespPDU = new CIVnPdu();	if(!pRespPDU)    {    	pPacker->Release();        return -1;    }	pRespPDU->packetBody((char *)pPacker->GetBuffer(), pPacker->GetLength());        pRespPDU->m_pIpHdr->dvrip_cmd = RESP_DEV_LOG;    pPacker->Release();		have_msg_out = true;	return 0;}CNetWorkClient * CNetWorkClientFactory::instance(){										static CNetWorkClient * _instance = NULL;			if( NULL == _instance)							{		_instance = new CNetWorkClient;		}												return _instance;						}	