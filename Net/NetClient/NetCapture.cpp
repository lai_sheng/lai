#include "Net/NetClient/NetCapture.h"
#include "System/Packet.h"
#include "Devices/DevCapture.h"
//#include "Functions/Preview.h"
#include "Net/NetClient/INetConListener.h"
#include "Net/NetClient/NetCaptureManager.h"
#include "../VideoMonitor.h"
//#include "Net/Dlg/FtpUpgrade.h"
#include "Devices/DevCapBuf.h"


VD_UINT32 g_uiDecodeDataPer10s[MAX_NET_CHANNEL] = {0};

#define vd_assert(x) if(!(x)) {char*p=NULL; *p=2;}

CNetChannelStream::CNetChannelStream( CNetChannelCapture * parent_channel,
        NET_CAPTURE_CHANNEL_T stream_type )
:m_stream_type( stream_type )
,m_parent_channel( parent_channel )
,m_streamCalculate(0)
,data_len_per_10second(0)
,tmp_start_time(0)
,m_lastending_time(0)
,m_Config(NULL)
,m_bEnable(true)
{
    m_psigMonitor = new TSignal3<int, uint, CPacket *>(32);
    vd_assert( m_psigMonitor != NULL );

    m_sigRecord =   new TSignal3<int, uint, CPacket *>(32);
    vd_assert( m_sigRecord != NULL );

    m_psigPBProc = new TSignal3<int, ulong, ulong>(32);
    vd_assert( m_psigPBProc != NULL);

    m_psigAlarm     = new TSignal3<appEventCode, ulong, int>(32);
    vd_assert( m_psigAlarm != NULL );

    m_montior_count = 0;
    m_pRemoteChn = NULL;
    m_RecType = 0;
    m_preSeconds = 0;
    m_preBufferState = false;
}
CNetChannelStream::~CNetChannelStream()
{
    Stop();
    if(m_psigMonitor)
    {
        delete m_psigMonitor;
        m_psigMonitor = NULL;
    }

	if(m_sigRecord)
    {
        delete m_sigRecord;
        m_sigRecord = NULL;
    }

    if(m_psigAlarm)
    {
        delete m_psigAlarm;
        m_psigAlarm = NULL;
    }
    if( m_Config != NULL )
    {
        delete m_Config;
    }

    if(m_pRemoteChn != NULL )
    {
    	delete m_pRemoteChn;
    	m_pRemoteChn = NULL;
    }

}

VD_BOOL CNetChannelStream::Bind( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,uint dwFlags,uint dwStreamType )
{
    int iRet;
    printf( "bind stream[%d], local channel[%d], this [%#x], m_bEnable:%d, m_RecType:%#x, dwFlags:%#x, dwStreamType:%d\n",
    		m_stream_type, m_parent_channel->GetLogicChn(),reinterpret_cast<int>(this), m_bEnable, m_RecType, dwFlags, dwStreamType );

//    printf("CNetChannelStream::Bind():m_RecType = %x\r\n",m_RecType);

    uint old_rectype = m_RecType;	 

    if( (dwFlags & DATA_RECORD) || (dwFlags & DATA_2SND) )
    {
	        iRet = m_sigRecord->Attach(pObj, pProc);
			if(iRet == 1)
			{
				m_RecType |= DATA_RECORD;
				adjustPreBuffer();
				printf("xql:: bind m_RecType=%x \n",m_RecType);
			}    
    }

    if( dwFlags & DATA_BUFFER )
    {
        m_RecType |= DATA_BUFFER;
    }

    if( dwFlags & DATA_MONITOR )
    {
        iRet = m_psigMonitor->Attach(pObj, pProc);
        if( iRet <= 0 && iRet != -2)
        {
            warnf("CNetChannelStream::Bind-attach monitor failed, stream:%d, local channel[%d]!\n", 
                m_stream_type, m_parent_channel->GetLogicChn());
            return FALSE;
        }
        m_RecType |= DATA_MONITOR;
    }  

#ifdef  _ZHONGKONG
        if( (old_rectype == 0 && m_RecType != 0 && m_stream_type != NET_CHL_MAIN_T) && m_bEnable )
        {
            StartMonitor(NULL);
        }
#else        
        if( old_rectype == 0 && m_RecType != 0  && m_bEnable )
        {
        	StartMonitor(NULL);
        }
#endif 
//    printf("CNetChannelStream::Bind()2222:m_RecType = %x\r\n",m_RecType);

    return TRUE;
}
VD_BOOL CNetChannelStream::Unbind( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,uint dwFlags,uint dwStreamType )
{

    int iRet;
    printf( "unbind stream[%d], local channel[%d], this[%#x]\n",
        m_stream_type, m_parent_channel->GetLogicChn(), reinterpret_cast<int>(this) );

//    printf("CNetChannelStream::Unbind():m_RecType = %x\r\n",m_RecType);

    if( (dwFlags & DATA_RECORD) || (dwFlags & DATA_2SND) )
    {
		iRet = m_sigRecord->Detach(pObj, pProc);
	    if( iRet < 0 )
	    {
	        errorf("unbind failed, dwFlags:%x,stream[%d], local channel[%d], this[%#x]\n",
	            dwFlags,m_stream_type, m_parent_channel->GetLogicChn(), reinterpret_cast<int>(this) );
	        return FALSE;
	    }
	    if( iRet == 0)
	 	    m_RecType &= ~DATA_RECORD;
//	    printf("xql:: m_RecType=%x \n",m_RecType);    
    }

    if(dwFlags & DATA_BUFFER)
    {
        m_RecType &= ~DATA_BUFFER;
    }

    if( dwFlags & DATA_MONITOR )
    {
		iRet = m_psigMonitor->Detach(pObj, pProc);
	    if( iRet < 0 )
	    {
	        errorf("2unbind failed, dwFlags:%x,stream[%d], local channel[%d], this[%#x]\n",
	            dwFlags,m_stream_type, m_parent_channel->GetLogicChn(), reinterpret_cast<int>(this) );
	        return FALSE;
	    }
	    if( iRet == 0)
	 	    m_RecType &= ~DATA_MONITOR;
//	    printf("2xql:: m_RecType=%x \n",m_RecType);     
    }
    
#ifdef  _ZHONGKONG
    if( (m_RecType == 0 && m_stream_type != NET_CHL_MAIN_T)|| !m_bEnable )
    {
    	StopMonitor();
    }
#else
    if( m_RecType == 0 || !m_bEnable )
    {
    	StopMonitor();
    }
#endif

//    printf("CNetChannelStream::unBind()2222:m_RecType = %x\r\n",m_RecType);

    return TRUE;
}

VD_BOOL CNetChannelStream::BindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc )
{
    return TRUE;
}

VD_BOOL CNetChannelStream::UnBindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc )
{
    return TRUE;
}

VD_BOOL CNetChannelStream::BindPlayBackProc(CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc)
{
    return TRUE;
}

VD_BOOL CNetChannelStream::UnBindPlayBackProc(CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc)
{
    return TRUE;
}

int CNetChannelStream::SetConfigAsync( INetCliConfig * config )
{
    return 0;
}

VD_BOOL CNetChannelStream::adjustPreBuffer()
{
    VD_BOOL bRet = FALSE;
    if (m_preSeconds)
    {
        // 只有预录开的情况下才有用
        m_preBufferState = TRUE;
        CDevBufManager::instance()->setBuffer(m_preSeconds, m_parent_channel->GetLogicChn());  
//		printf("xql:: CNetChannelStream::adjustPreBuffer()\n");
        bRet = TRUE;
    }
    else
    {
        m_preBufferState = FALSE;
    }
    
    return bRet;    

}

void CNetChannelStream::SetIFrameNum(int number)
{
	printf("xql-1::set net pre record number:%d \n",number);
    m_preSeconds = number;
    if (m_preSeconds == 0)
    {
        CDevBufManager::instance()->clear(m_parent_channel->GetLogicChn());
    }
}

int CNetChannelStream::StartMonitor( INetChannel* channel )
{
    return 0;
}
int CNetChannelStream::StopMonitor()
{
    return 0;
}

VD_BOOL CNetChannelStream::Start( )
{
    return TRUE;
}
VD_BOOL CNetChannelStream::Stop( )
{
    return TRUE;
}

int CNetChannelStream::GetLoginChannel( )
{
	if( m_pRemoteChn == NULL )
	{
		return -1;
	}
    return m_pRemoteChn->GetChannelNO();
}

void CNetChannelStream::OnMonitor( ulong dwDataType, CPacket* pVideoPacket )
{
    //! 调试信息
    ulong tmp_end_time = 0;

//#ifdef _PURE_DECODE
    int iCh = m_parent_channel->GetLocalChannel();
    g_uiDecodeDataPer10s[iCh] += pVideoPacket->GetLength();
    tmp_end_time = SystemGetMSCount();    
    m_lastending_time = tmp_end_time; 

    if( tmp_end_time - tmp_start_time > 4*1000 )    //!< 4秒一统计
    {
//        printf("total recv len 4 second [%u], local channel[%d], remote channel[%d[, stream[%d], end[%u], start[%u]\n",
//            g_uiDecodeDataPer10s[iCh], m_parent_channel->GetLocalChannel(), m_remote_channel, m_stream_type, tmp_end_time, tmp_start_time);
        tmp_start_time = tmp_end_time;
        g_uiDecodeDataPer10s[iCh] = 0;
    }

    m_streamCalculate += pVideoPacket->GetLength();

    if (m_RecType != 0 )
    {
//    __trip;
        if (m_preBufferState)
        {
//        	printf("xql-3::set net pre record number: \n");
            //说明需要取录像数据
            CPacket *pPkt = NULL;
            uint iLength = 0;
            int iRet = 0;
            while ((iRet = CDevBufManager::instance()->popBuffer(&pPkt, m_parent_channel->GetLogicChn())) == 0)
            {
//            	printf("xql-4::set net pre record number: len:%d, m_preSeconds:%d \n", iLength, m_preSeconds);
                //取到的数据写往硬盘
                (*(m_sigRecord))(m_parent_channel->GetLogicChn(), m_stream_type, pPkt);
                iLength += pPkt->GetLength();
                pPkt->Release();
                if (iLength >= pVideoPacket->GetLength() * 4)
                {
                    break;
                }
            }

            if (iRet != 0)
            {
                //说明已经取空了
                m_preBufferState = FALSE;
            }
        }
        
        if (m_preBufferState == FALSE && (m_RecType & DATA_RECORD))
        {
            //说明空了
//            printf("xql-5::set net pre record number: \n");
            (*(m_sigRecord))(m_parent_channel->GetLogicChn(), m_stream_type, pVideoPacket);
        }
        
        if (m_preSeconds)
        {
//        	printf("xql-2::set net pre record number: \n");
            //说明需要预录
            if (CDevBufManager::instance()->pushBuffer(pVideoPacket, m_parent_channel->GetLogicChn()))
            {
                // 回调存在异常，m_dwBytes会变大，且是死循环，导致嵌入深入死机。
                // OnData(pPacket, dwType);
                
                // 执行到这里只有一个情况：即缓冲区满且数据全是录像数据。
                // 但是由于取录像数时长度大于当前包的长度，所以不应该执行到此，否则有异常。
                tracepoint();
            }
        }
    }  
    
//    debugf("local channel[%d], stream[%d], len:%d, this[%#x]\n",
//            m_parent_channel->GetLocalChannel(), m_stream_type, pVideoPacket->GetLength(), this );
//    tracepoint();
    (*(m_psigMonitor))(m_parent_channel->GetLogicChn(), m_stream_type, pVideoPacket );
//    tracepoint();
}

VD_BOOL CNetChannelStream::IsOnMonitorRunning()
{
    ulong dwMS, dwIntervalMS;

    dwMS = SystemGetMSCount();
    dwIntervalMS = dwMS - m_lastending_time;
    if(dwIntervalMS > 5*1000)
    {
        return FALSE; 
    }
    return TRUE; 
}

void CNetChannelStream::OnPBProcess(ulong dwTotalSize, ulong dwCurSize)
{
    (*(m_psigPBProc))(m_parent_channel->GetLogicChn(), dwTotalSize, dwCurSize); 
}


/*
 *type 有如下意义，详见网络协议
1	ALARM_IN	外部报警
2	ALARM_MOTION	动态检测
3 	ALARM_LOSS	视频丢失
4 	ALARM_BLIND	视频遮挡
 *
 */
void CNetChannelStream::OnAlarm( int channel, uint type, int state )
{
//    printf("CNetChannelStream::OnAlarm channel[%d], type[%d], state[%d] GetLoginChannel() = %d\n",
//    		channel, type, state,GetLoginChannel());

    if( channel > NET_MAX_CHAN )
    {
        printf( "error alarm, channel[%d]\n", m_parent_channel->GetLogicChn());
        return ;
    }

    /* 不是当前通道的报警不处理 */
    if (channel != (GetLoginChannel() - 1)  )
    {
        return ;
    }

    if( m_stream_type == CHL_2END_T)
    {
        //!报警注册到多个码流中去，如果主码流在用，就过虑掉辅助码流来的报警
        if( m_parent_channel->HaveBitRate(CHL_MAIN_T) )
        {
            return;
        }
    }
    
    enum app_event_code code;
    if(type == 1)
    {
    	code = appEventAlarmLocal;
    }
    else if(type == 2)
    {
    	code = appEventVideoMotion;
				printf ("[%s][%d]GetMotionDetectState iStatus\n", __FILE__, __LINE__);

    }
    else if(type == 3)
    {
    	code = appEventVideoLoss;
    }
    else if(type == 4)
    {
    	code = appEventVideoBlind;
    }

    (*m_psigAlarm)( code, state, m_parent_channel->GetLogicChn() );
}

int CNetChannelStream::GetStream()
{
    return m_streamCalculate;
}

void CNetChannelStream::ClearStream()
{
    m_streamCalculate = 0;
}

int  CNetChannelStream::SetDbg( int dbg )
{
    return 0;
}

int  CNetChannelStream::SetReconnect( int iFlag )
{
    return 0;
}


VD_VOID  CNetChannelStream::SendPtzData(VD_VOID *pData,VD_UINT32 uiLen)
{
    return;
}

int CNetChannelStream::GetParam(int TypeMain, int index,int subIndex,void* buf,  int size)
{
    return -1;
}
    
int CNetChannelStream::SetParam(int TypeMain, int index, int subIndex,void* buf,  int size)
{
    return -1;
}

 bool  CNetChannelStream::IsLogin()
 {
    return -1;
 }

void CNetChannelStream::dump()
{

}

void CNetChannelStream::SetIFrametoNetStream()
{
}

VD_BOOL CNetChannelStream::GetState()
{
	if( m_RecType== 0 )
	{
		return VD_FALSE;
	}

	return VD_TRUE;
}

void	CNetChannelStream::Enable(bool flag)
{
	m_bEnable = flag;
}

/*!
   \class  CNetChannelCapture
 */
CNetChannelCapture::CNetChannelCapture( LOGIC_CHN logic_chn, DIGITAL_CHN local_channle )
:m_local_channel( local_channle )
,m_dbg( 1 )
{
	m_bEnable = true;
    
#if 0
//!NVR24采取帧模式
    if ( ICaptureManager::instance()->GetDigitalChnNum()  == 24 )
    {
        printf("CNetChannelCapture::NVR24  use GET_FRONT_FLOW\n");
#ifndef WIN32
        CVirtualNetCon::NetCliSetDataMode(GET_FRONT_FLOW);
#endif
    }
#endif

    m_logic_chn = logic_chn;
    for( int i = 0; i < NET_CHL_FUNCTION_NUM; i++ )
    {
        m_stream[i] = NULL;
#ifdef _DECODER_
        m_dwNetBytes[i] = 0;
        m_NetBitRate[i] = 0;
#endif
    }
    
#ifdef _DECODER_    
    m_streamtype = 0; 
#else
    m_dwNetBytes = 0;
    m_NetBitRate = 0;
    m_dwNetMS = 0;
#endif
}

CNetChannelCapture::~CNetChannelCapture()
{
    printf("CNetChannelCapture::~ Logic chn:%d\n", m_logic_chn);
    Stop( NET_CHL_FUNCTION_NUM );
}

int CNetChannelCapture::Start( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,
        uint dwFlags,uint dwStreamType /*= CHL_MAIN_T*/ )
{
    if( dwStreamType >= NET_CHL_FUNCTION_NUM )
    {
        trace("1-CNetChannelCapture::Start-failed, dwStreamType:%d\n", dwStreamType);
        return FALSE;
    }

    if( m_stream[dwStreamType] == NULL)
    {
//    	assert(0);
        Start(dwStreamType);
    }
    m_stream[dwStreamType]->Enable(m_bEnable);
    return m_stream[dwStreamType]->Bind( pObj, pProc, dwFlags, dwStreamType );
}

int CNetChannelCapture::Stop( CObject * pObj, SIG_DEV_CAP_BUFFER pProc,
        uint dwFlags, uint dwStreamType /*= CHL_MAIN_T*/ )
{
    if( dwStreamType >= NET_CHL_FUNCTION_NUM )
    {
        return FALSE;
    }
    if( m_stream[dwStreamType] == NULL )
    {
        return FALSE;
    }
    return m_stream[dwStreamType]->Unbind( pObj, pProc, dwFlags, dwStreamType );
}

VD_BOOL CNetChannelCapture::BindPlayBackProc( CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc, ulong dwFlags,ulong dwStreamType /*= CHL_PLAYBACK_T*/ )
{
    if( m_stream[NET_CHL_Play_T] == NULL )
    {
        return FALSE;
    }
    return m_stream[NET_CHL_Play_T]->BindPlayBackProc( pObj, pProc );
}

VD_BOOL CNetChannelCapture::UnBindPlayBackProc( CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc, ulong dwFlags,ulong dwStreamType /*= CHL_PLAYBACK_T*/ )
{
    if( m_stream[NET_CHL_Play_T] == NULL )
    {
        return FALSE;
    }
    return m_stream[NET_CHL_Play_T]->UnBindPlayBackProc( pObj, pProc );
}

VD_BOOL CNetChannelCapture::BindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc )
{
    for(int i = 0; i < NET_CHL_FUNCTION_NUM; i++)
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->BindAram( pObj, pProc );
        }
    }
    return VD_TRUE;
}

VD_BOOL CNetChannelCapture::UnBindAram( CObject * pObj, NET_DEV_ALARM_DELEGATE pProc )
{
    for(int i = 0; i < NET_CHL_FUNCTION_NUM; i++)
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->UnBindAram( pObj, pProc );
        }
    }
    return VD_TRUE;
}

int CNetChannelCapture::Start( uint dwStreamType /*= CHL_MAIN_T*/ )
{
    if( dwStreamType < NET_CHL_FUNCTION_NUM )
    {
        if( m_stream[dwStreamType] == NULL )
        {
            m_stream[dwStreamType] = new CNetChannelStream( this,
                    (NET_CAPTURE_CHANNEL_T)dwStreamType);
            vd_assert( m_stream[dwStreamType] != NULL );

            //m_stream[dwStreamType]->Start();
        }
        m_stream[dwStreamType]->Start(); //up 为什么移到外面了
        //,虽然我每次在开启回放前，去关闭了先前的回放，但是由于中间网络模块是异步的，导致在真正detach回调函数的时候还没执行到
    }
    else
    {
        for( int i = 0 ; i < NET_CHL_FUNCTION_NUM ; i++ )
        {
            if( m_stream[i] == NULL )
            {
                m_stream[i] = new CNetChannelStream( this, (
                        NET_CAPTURE_CHANNEL_T)dwStreamType);
                vd_assert( m_stream[i] != NULL );

                //m_stream[i]->Start();

            }
            m_stream[i]->Start(); //up

        }
    }

    return TRUE;
}

int CNetChannelCapture::Stop( uint dwStreamType /*= CHL_MAIN_T*/ )
{
    if( dwStreamType < NET_CHL_FUNCTION_NUM )
    {
        if( m_stream[dwStreamType] != NULL )
        {
            m_stream[dwStreamType]->Stop();
            delete m_stream[dwStreamType];
            m_stream[dwStreamType] = NULL;
        }
    }
    else
    {
        for( int i = 0 ; i < NET_CHL_FUNCTION_NUM ; i++ )
        {
            if( m_stream[i] != NULL )
            {
                m_stream[i]->Stop();
                delete m_stream[i];
                m_stream[i] = NULL;
            }
        }
    }

    return TRUE;
}

int CNetChannelCapture::SetConfigAsync( ulong dwStreamType, INetCliConfig * config )
{
    int iret = 0;

    if( dwStreamType < NET_CHL_FUNCTION_NUM )
    {
        if( m_stream[dwStreamType] != NULL )
        {
            m_stream[dwStreamType]->SetConfigAsync( config );
        }
        else
        {
            return -1;
        }
    }
    else
    {
        for( int i = 0; i < NET_CHL_FUNCTION_NUM; i++ )
        {
            if( m_stream[i] != NULL )
            {
                m_stream[i]->SetConfigAsync( config );
            }
            else
            {
                iret = -1;
            }
        }
    }
    return iret;
}

int CNetChannelCapture::StartMonitor( INetChannel* channel )
{
    //int iret = 0;
    vd_assert( channel != NULL );

    for( int i = 0; i < NET_CHL_FUNCTION_NUM; i++ )
    {
        if(i == NET_CHL_Play_T )//监视配置下来的信息，不要覆盖了回放的配置
        {
            continue;
        }

        if( m_stream[i] != NULL )
        {
        	infof("CNetChannelCapture::StartMonitor, i:%d, state:%d, m_bEnable:%d\n",
        			i, m_stream[i]->GetState(), m_bEnable );
            INetChannel *tmpChannel = channel->clone();

            /* 本地解码使用的配置中已经包含了主辅码流信息，这里不应该再进行
               配置 */
            if ( i != NET_CHL_MAIN_T)
            {
                tmpChannel->SetStream( (NET_CAPTURE_CHANNEL_T)i );
            }
            
            if( m_bEnable )
            {
            	m_stream[i]->StartMonitor( tmpChannel );
            }
            delete tmpChannel;
        }
    }

    return 0;
}
int CNetChannelCapture::StopMonitor( INetChannel* channel )
{
    //int iret = 0;

    NET_CAPTURE_CHANNEL_T dwStreamType;
        
    for( int i = 0; i < NET_CHL_FUNCTION_NUM; i++ )
    {
        if( m_stream[i] != NULL )
        {
        	infof("CNetChannelCapture::StopMonitor, i:%d, state:%d\n", i, m_stream[i]->GetState());
            
#ifdef  _ZHONGKONG
            if ( 0 == i )
            {
                m_stream[i]->StopMonitor( );
            }
            else  if( m_stream[i]->GetState() )
            {
                m_stream[i]->StopMonitor( );
            }
#else        
            if( m_stream[i]->GetState() )
            {
                m_stream[i]->StopMonitor( );
            }
#endif        

        }
    }

    return 0;
}

int CNetChannelCapture::SetFormat( CAPTURE_FORMAT *pFormat, uint dwType /*= CHL_MAIN_T*/ )
{
    return TRUE;
}

const CAPTURE_FORMAT * CNetChannelCapture::GetFormat( uint dwType /*= CHL_MAIN_T*/ ) const
{
    return NULL;
}

int CNetChannelCapture::SetIFrame( uint dwStreamType /*= CHL_MAIN_T*/ )
{
    return TRUE;
}

int CNetChannelCapture::adjustPreBuffer()
{
    return m_stream[CHL_MAIN_T]->adjustPreBuffer();
}

int CNetChannelCapture::SetTime(SYSTEM_TIME * pSysTime)
{
    return TRUE;
}

int CNetChannelCapture::SetVstd(uint dwStandard)
{
    return TRUE;
}
int CNetChannelCapture::SetCover(VD_RECT *pRect, uint Color, int Enable, int index)
{
    return TRUE;
}
int CNetChannelCapture::SetTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect,
        VD_PCSTR str, FONTSIZE fontsize)
{
    return TRUE;
}

int CNetChannelCapture::GetState(uint dwStreamType)
{
    return TRUE;
}

uint CNetChannelCapture::GetBufferLength()
{
    return TRUE;
}

void CNetChannelCapture::SetIFrameNum(int number)
{
    m_stream[CHL_MAIN_T]->SetIFrameNum(number);
    return;
}

void CNetChannelCapture::CalculateExBitRate()
{
    return;
}

void CNetChannelCapture::OnData(CPacket *pPacket,uint dwType)
{
    return;
}
void CNetChannelCapture::OnRecData(CPacket ** pPacket)
{
    return;
}

int CNetChannelCapture::GetVolume(int *pVolume)
{
    return TRUE;
}

ENUM_CHANNEL_STREAM_TYPE CNetChannelCapture::GetCaptureSreamType()
{
#ifndef WIN32
    if( CVirtualNetCon::NetCliGetDataMode() == GET_FRONT_FRAME )
    {
        return ENUM_STREAM_TYPE_8K;
    }
#endif

    return ENUM_STREAM_TYPE_Stream;
}

ulong CNetChannelCapture::GetChannelStreamType()
{
#ifdef _DECODER_
    return m_streamtype; 
#else
    return 0; 
#endif 
}

//extern void VDDumpHex(const char *pData,int len);

int CNetChannelCapture::getImageSize(capture_size_t* pSize, uint dwType)
{
    /*
        从I帧信息中获取图像大小（分辨率），目前尚未发现有一个packet中的帧数据长度小于I帧头的长度，就算有，下面的操作是
        只读性质的，不会引起异常，顶多是返回一个错误的分辨率，上层若发现是错误的分辨，在录像打包时可能功能不正常。但这
        种情况应该是不存在的
   */
    if(!pSize)
    {
        return CAPTURE_SIZE_D1;
    }
    
    *pSize = CAPTURE_SIZE_NR;

    const unsigned char *pdata = (const unsigned char *)dwType;
    unsigned int framehead = pdata[0]|pdata[1]<<8|pdata[2]<<16|pdata[3]<<24;
    int iWidth = 0;
    int iHeight = 0;
    
    //VDDumpHex(vedioDataPtr, 16);
    if ( framehead == 0xFD010000 || framehead == 0xFC010000 || framehead == 0xF0010000)
    {
        iWidth = pdata[6]*8;
        iHeight = pdata[7]*8;
    }
    else if (framehead == 0xED010000 ||framehead == 0xEC010000 ||framehead == 0xEA010000)
    {
        /* 针对svac修改的24字节头 */
        iWidth = pdata[8] + (pdata[9]<<8);
        iHeight = pdata[10] + (pdata[11]<<8);

    }
    else
    {
        return CAPTURE_SIZE_D1;
    }
    
    //PAL制式
    if(704 == iWidth && 576 == iHeight)
    {
        *pSize = CAPTURE_SIZE_D1;
    }
    else if(352 == iWidth && 576 == iHeight)
    {
        *pSize = CAPTURE_SIZE_HD1;
    }
    else if(720 == iWidth && 288 == iHeight)
    {
        *pSize = CAPTURE_SIZE_BCIF;
    }    
    else if(352 == iWidth && 288 == iHeight)
    {
        *pSize = CAPTURE_SIZE_CIF;
    }
    else  if(0x16*8 == iWidth && 0x12*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_QCIF;
    }
    else  if(0x50*8 == iWidth && 0x3c*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_VGA;
    }
    else  if(0x28*8 == iWidth && 0x1E*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_QVGA;
    }
    else  if(480 == iWidth && 480== iHeight)
    {
        *pSize = CAPTURE_SIZE_SVCD;
    }
    else  if(160== iWidth && 128 == iHeight)
    {
        *pSize = CAPTURE_SIZE_QQVGA;
    }    
    else  if(1280 == iWidth && 720 == iHeight)
    {
        *pSize = CAPTURE_SIZE_720P;
    }
    else  if(1280 == iWidth && 704 == iHeight)//svac
    {
        *pSize = CAPTURE_SIZE_720P;
    }
    else  if(1920 == iWidth && 1080 == iHeight)
    {
        *pSize = CAPTURE_SIZE_1080P;
    }
    else  if(1920 == iWidth && 1072 == iHeight)//svac
    {
        *pSize = CAPTURE_SIZE_1080P;
    }
    else  if(800 == iWidth && 592 == iHeight)
    {
        *pSize = CAPTURE_SIZE_SVGA;
    }
    else  if(1024 == iWidth && 768 == iHeight)
    {
        *pSize = CAPTURE_SIZE_XVGA;
    }
	 else  if(1280 == iWidth && 960 == iHeight)
    {
        *pSize = CAPTURE_SIZE_1280_960;   
    }
    else  if(1280 == iWidth && 800 == iHeight)
    {
        *pSize = CAPTURE_SIZE_WXGA;
    }
    else  if(1280 == iWidth && 1024 == iHeight)
    {
        *pSize = CAPTURE_SIZE_SXGA;
    }
    else  if(1600 == iWidth && 1024 == iHeight)
    {
        *pSize = CAPTURE_SIZE_WSXGA;
    }
    else  if(1600 == iWidth && 1200 == iHeight)
    {
        *pSize = CAPTURE_SIZE_UXGA;
    }
    else  if(1920 == iWidth && 1200 == iHeight)
    {
        *pSize = CAPTURE_SIZE_WUXGA;
    }
	else  if(2048 == iWidth && 1536 == iHeight)
	{
		*pSize = CAPTURE_SIZE_300W; //2048*1536
	}
	else  if(2592 == iWidth && 1944 == iHeight)
	{
		*pSize = CAPTURE_SIZE_500W; //2592*1944
	}
	else if (2688 == iWidth && 1520 == iHeight)
	{
		*pSize = CAPTURE_SIZE_400W;
	}
	else if(3072 == iWidth && 2048 == iHeight)    
	{
		*pSize = CAPTURE_SIZE_600W; //3072*2048
	}
    //need modify
    //NTSC制式
    if(CAPTURE_SIZE_NR != *pSize)
    {
        return *pSize;
    }
    if(0x58*8 == iWidth && 0x3C*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_D1;
    }
    else if(0x2C*8 == iWidth && 0x3C*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_HD1;
    }
    else if(720 == iWidth && 240 == iHeight)
    {
        *pSize = CAPTURE_SIZE_BCIF;
    }    
    else if(0x2C*8 == iWidth && 0x1E*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_CIF;
    }
    else  if(0x16*8 == iWidth && 0xE*8 == iHeight)
    {
        *pSize = CAPTURE_SIZE_QCIF;
    }

    if(CAPTURE_SIZE_NR == *pSize)
    {
        return CAPTURE_SIZE_D1;
    }

    return *pSize;
    //return *pSize = (capture_size_t)m_captureFormat[dwType].ImageSize;

}
DIGITAL_CHN CNetChannelCapture::GetLocalChannel()
{
    return m_local_channel;
}
int CNetChannelCapture::GetLoginChannel( ulong dwStreamType /*= CHL_MAIN_T*/ )
{
    vd_assert( m_stream[dwStreamType] != NULL );
    return m_stream[dwStreamType]->GetLoginChannel();
}

uint CNetChannelCapture::GetBitRate(int dwType)
{
#ifdef _DECODER_
    if(dwType == NET_CHL_MAIN_T )//|| dwType == NET_CHL_Play_T || dwType == NET_CHL_2END_T)
    {
        //trace("m_local_channel = %d, m_NetBitRate = %d \n", m_local_channel, m_NetBitRate);
        return m_NetBitRate[NET_CHL_MAIN_T];
    }
    else if(dwType == NET_CHL_2END_T)
    {
        return m_NetBitRate[NET_CHL_2END_T];
    }
    else if(dwType == NET_CHL_Play_T)
    {
        return m_NetBitRate[NET_CHL_Play_T];
    }
    else if(dwType == NET_CHL_FUNCTION_NUM)
    {
        ulong dwTotalNetBit = 0;
        for(int i = 0; i < NET_CHL_FUNCTION_NUM; i++)
        {
            dwTotalNetBit += m_NetBitRate[i]; 
        }
        return dwTotalNetBit; 
    }
    return 0;
#else
    if ((dwType == CHL_MAIN_T)||(dwType == NET_CHL_FUNCTION_NUM))
    {
        return m_NetBitRate;
    }
    else if(dwType == CHL_2END_T)
    {
    }
    return 0;
#endif
}

void CNetChannelCapture::CalculateBitRate()
{
#ifdef _DECODER_
    CGuard tmpGuard(m_calcMutex);

        //以Kb/s为单位
        //以字节为单位，先除后乘，解决超出范围，使码流变小的问题
        if (NULL != m_stream[NET_CHL_MAIN_T])
        { 
            if(m_stream[NET_CHL_MAIN_T]->GetLoginChannel()!= -1)
            {
                if(m_stream[NET_CHL_MAIN_T]->IsOnMonitorRunning())
                {
                    m_dwNetBytes[NET_CHL_MAIN_T] = m_stream[NET_CHL_MAIN_T]->GetStream();
                    m_NetBitRate[NET_CHL_MAIN_T] = m_dwNetBytes[NET_CHL_MAIN_T] * 8 / 1024 ;
                }
                else
                {
                    //m_stream[NET_CHL_MAIN_T]->ClearStream();
                    m_NetBitRate[NET_CHL_MAIN_T] = 0; 
                }
            }
            else
            {
                m_stream[NET_CHL_MAIN_T]->ClearStream();
                m_NetBitRate[NET_CHL_MAIN_T] = 0; 
            }
        }
        if (NULL != m_stream[NET_CHL_2END_T])
        {
            if(m_stream[NET_CHL_2END_T]->GetLoginChannel()!= -1)
            {
                if(m_stream[NET_CHL_2END_T]->IsOnMonitorRunning())
                {
                    m_dwNetBytes[NET_CHL_2END_T]=  m_stream[NET_CHL_2END_T]->GetStream(); 
                    m_NetBitRate[NET_CHL_2END_T] = m_dwNetBytes[NET_CHL_2END_T] * 8 / 1024 ;
                }                
                else
                {
                    //m_stream[NET_CHL_2END_T]->ClearStream();
                    m_NetBitRate[NET_CHL_2END_T] = 0; 
                }
            }
            else
            {
                m_stream[NET_CHL_2END_T]->ClearStream();
                m_NetBitRate[NET_CHL_2END_T] = 0; 
            }
        }
        
        if (NULL != m_stream[NET_CHL_Play_T])
        {
            if(m_stream[NET_CHL_Play_T]->GetLoginChannel()!= -1)
            {
                if(m_stream[NET_CHL_Play_T]->IsOnMonitorRunning())
                {
                    m_dwNetBytes[NET_CHL_Play_T]= m_stream[NET_CHL_Play_T]->GetStream();
                    m_NetBitRate[NET_CHL_Play_T] = m_dwNetBytes[NET_CHL_Play_T] * 8 / 1024 ;
                }
                else
                {
                    //m_stream[NET_CHL_Play_T]->ClearStream();
                    m_NetBitRate[NET_CHL_Play_T] = 0;     
                }

            }
            else
            {
                m_stream[NET_CHL_Play_T]->ClearStream();
                m_NetBitRate[NET_CHL_Play_T] = 0;         
            }            
        }
        //m_NetBitRate = m_dwNetBytes * 8 / 1024 ; //* 1000 / dwIntervalMS;
    for(int i = 0; i < NET_CHL_FUNCTION_NUM; i++)
    {
        m_dwNetBytes[i] = 0;
    }

#else
    ulong dwMS, dwIntervalMS;

    dwMS = SystemGetMSCount();
    dwIntervalMS = dwMS - m_dwNetMS;

    if (dwIntervalMS > 0)
    {
        //以Kb/s为单位
        //以字节为单位，先除后乘，解决超出范围，使码流变小的问题
        if (NULL != m_stream[CHL_MAIN_T])
        {
            m_dwNetBytes = m_stream[CHL_MAIN_T]->GetStream();
        }

        if (NULL != m_stream[CHL_2END_T])
        {
            m_dwNetBytes += m_stream[CHL_2END_T]->GetStream();
        }

        m_NetBitRate = m_dwNetBytes * 8 / 1024 * 1000 / dwIntervalMS;
    }

    m_dwNetBytes = 0;
    if (NULL != m_stream[CHL_MAIN_T])
    {
        m_stream[CHL_MAIN_T]->ClearStream();
    }
    
    if (NULL != m_stream[CHL_2END_T])
    {
        m_stream[CHL_2END_T]->ClearStream();
    }

    m_dwNetMS = dwMS;
#endif
}

bool CNetChannelCapture::HaveBitRate(int dwType)
{
    if( m_stream[dwType]->GetStream() > 0 )
    {
        return true;
    }
    return false;
}

CNetChannelCapture * CNetChannelCapture::instance( LOGIC_CHN channle )
{
    return CNetCaptureManager::instance()->GetDevInstance( channle );
}
void CNetChannelCapture::dump()
{
    for( int i = 0; i < NET_CHL_FUNCTION_NUM ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->dump();
        }
    }
}

int  CNetChannelCapture::SetDbg( int dbg )
{
    m_dbg = dbg ;
    for( int i = 0; i < NET_CHL_FUNCTION_NUM ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->SetDbg(m_dbg);
        }
    }
    return 0;
}

int  CNetChannelCapture::SetReconnect( int iFlag )
{
    for( int i = 0; i < NET_CHL_FUNCTION_NUM ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->SetReconnect(iFlag);
        }
    }
    
    return 0;
}

int  CNetChannelCapture::GetDbg()
{
    return m_dbg;
}

int& CNetChannelCapture::GetLogicChn()
{
    return m_logic_chn;
}

VD_VOID CNetChannelCapture::SendPtzData(VD_VOID *pData,VD_UINT32 uiLen)
{
    //目前同时只有一个码流
    for( int i = 0; i < NET_CHL_FUNCTION_NUM ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->SendPtzData(pData,uiLen);
            return;
        }
    }
}

int CNetChannelCapture::GetParam(int TypeMain, int index,int subIndex,void* buf,  int size)
{
	int iRet =-1;
    for( int i = 0; i <= NET_CHL_2END_T; i++ )
    {
        if( m_stream[i] != NULL )
        {
            iRet = m_stream[i]->GetParam(TypeMain,index,subIndex,buf,size);
	     if(iRet >= 0)
	     {
 		    return iRet;
	     }	
        }
    }
    
    return -1;
}
    
int CNetChannelCapture::SetParam(int TypeMain, int index,int subIndex,void* buf,  int size)
{
	int iRet =-1;
    for( int i = 0; i <= NET_CHL_2END_T ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            iRet = m_stream[i]->SetParam(TypeMain,index,subIndex,buf,size);
	     if(iRet >= 0)
	     {
 		    return iRet;
	     }				
        }
    }
    
    return -1;
}
 bool  CNetChannelCapture::IsLogin()
 {
    for( int i = 0; i <= NET_CHL_MAIN_T ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            return m_stream[i]->IsLogin();
        }
    }
    
    return -1;
 }

void	CNetChannelCapture::Enable(bool flag)
{
	m_bEnable = flag;
    for( int i = 0; i < NET_CHL_FUNCTION_NUM ; i++ )
    {
        if( m_stream[i] != NULL )
        {
            m_stream[i]->Enable(m_bEnable);
            return;
        }
    }
}

