#include "Net/NetClient/NetCaptureManager.h"
#include "Net/NetClient/NetCapture.h"
#include "Net/NetClient/NetCliSimple.h"
#include "System/Packet.h"
#include "Devices/DevCapture.h"
//#include "Functions/Preview.h"
#include "Net/NetClient/NetCli_V8.h"
#include "Net/NetClient/NetCli_ZK.h"
#include "Net/NetClient/INetConListener.h"
#include "Net/NetClient/INetClient.h"
#include "System/AppEvent.h"
#include "System/Console.h"
#include "../VideoMonitor.h"
#include "Rtsp/Dec/decSPS.h"
#include   <sstream>   
#include "APIs/Net.h"

extern VD_UINT32 g_uiDecodeDataPer10s[16];


CNetCaptureManager::CNetCaptureManager():m_NetBitRateTimer("NETCAP_BitRate")
,/*m_pNetDevAlarm( NULL ),*/m_isDump(false)
{

    m_pCaptureM = ICaptureManager::instance();

    for( int i = 0; i < MAX_NET_CHANNEL; i++ )
    {
        m_net_channel[i] = NULL;
        m_CurDecVideoCaps[i] = 0;
        m_StreamType[i] = ST_MONITOR;
        m_iMode[i] = 0; //0:directconn, 1:transmitconn
        m_iplaybackway[i] = 0; 
        m_tmpDownLoadSize[i] = 0; 
        m_bSeeking[i] = FALSE; 
        m_pbpObj[i] = NULL;        
        memset(&m_dvrinfo[i], 0, sizeof(DVRINFO)); 
       // memset(&m_fileinfo[i], 0, sizeof(FILE_INFO)); 
    }
    m_CurTotalVideoCaps = 0;
    memset(&m_chanErrInfo, 0, sizeof(ERRCHANNELINFO));
    m_dspInfoMask = 0;

    //m_pNetDevAlarm = new CNetDevAlarm(  );
    //assert(m_pNetDevAlarm);

    g_Console.registerCmd(CConsole::Proc(&CNetCaptureManager::ConsoleHelp, this), "netcap", "netcapture operation!");

    m_UpdateTimer.Start(this, (VD_TIMERPROC)&CNetCaptureManager::OnTimer, 0, 1000);
    m_uiDecodeState= 0;
    
    CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CNetCaptureManager::onAppEvent);
}

CNetCaptureManager::~CNetCaptureManager()
{
    Stop();
}

CNetCaptureManager * CNetCaptureManager::instance(  )
{
    static CNetCaptureManager * g_NetCaptureManager = NULL;
    if( g_NetCaptureManager == NULL )
    {
        g_NetCaptureManager = new CNetCaptureManager();
    }

    return g_NetCaptureManager;
}

//CNetDevAlarm * CNetCaptureManager::GetNetDevAlarm()
//{
//    return m_pNetDevAlarm;
//}

CNetChannelCapture * CNetCaptureManager::GetDevInstance( LOGIC_CHN channel, VD_BOOL create /*= true*/ )
{
    if( channel < 0 || channel >= MAX_NET_CHANNEL )
    {
        return NULL;
    }
    if( m_net_channel[channel] == NULL )
    {
        if( create )
        {
            m_net_channel[channel] = new CNetChannelCapture( channel, m_pCaptureM->GetDigitalChnNo(channel) );
        }

    }
    return m_net_channel[channel];
}

void CNetCaptureManager::ReStartChannel()
{
    for( int i =0 ; i < MAX_NET_CHANNEL; i++ )
    {
        if( m_net_channel[i] == NULL )
        {
            continue;
        }
        
        //
    }
}

void CNetCaptureManager::SetConfigNet( LOGIC_CHN channel, VD_BOOL BEnable, REMOTE_CH_CFG &stRemoteChCfg)
{
}

int  CNetCaptureManager::SetReconnect(LOGIC_CHN iCh, int iFlag )
{
    infof(" CNetCaptureManager::SetReconnect-iCh:%d, iFlag:%d\n", iCh, iFlag);
    if(iCh >= MAX_NET_CHANNEL)
    {
        return -1;
    }

    if (NULL == m_net_channel[iCh])
    {
        return -1;
    
    }

    return  m_net_channel[iCh]->SetReconnect( iFlag );
}

int CNetCaptureManager::Start()
{
    ICaptureManager* pCaptureM = ICaptureManager::instance();
    printf("init net capture....\n");

    for ( LOGIC_CHN i = 0; i < pCaptureM->GetLogicChnNum(); i++)
    {
        if( pCaptureM->IsSupportDigital(i) )
        {
            if( m_net_channel[i] == NULL )
            {
                m_net_channel[i] = new CNetChannelCapture(i,
                    m_pCaptureM->GetDigitalChnNo(i) );
            }

            m_net_channel[i]->Start( NET_CHL_MAIN_T ); //!启动线程
            m_net_channel[i]->Start( NET_CHL_2END_T ); //!启动线程
            m_net_channel[i]->SetDbg( 0 );
        }
    }

    //m_NetTour.Start();
    m_NetBitRateTimer.Start(this, (VD_TIMERPROC)&CNetCaptureManager::OnNetCap, 0, 3*1000); //OnCap中需要判断设备指针是否有效

    return 0;
}
int CNetCaptureManager::Stop()
{
    ICaptureManager* pCaptureM = ICaptureManager::instance();
    for ( LOGIC_CHN i = 0; i < pCaptureM->GetLogicChnNum(); i++)
    {
        if( pCaptureM->IsSupportDigital(i) )
        {
            if( m_net_channel[i] == NULL )
            {
                m_net_channel[i] = new CNetChannelCapture(i,
                    m_pCaptureM->GetDigitalChnNo(i) );
            }

            m_net_channel[i]->Stop( NET_CHL_MAIN_T ); //!启动线程
    #ifndef _PURE_DECODE
            m_net_channel[i]->Stop( NET_CHL_2END_T ); //!启动线程
    #endif
            printf("stop channel:%d,value:%p success, this:%p\n", i, m_net_channel[i],this);
        }
    }

    return 0;
}

#ifdef ONVIF_SERVER
#ifdef ONVIF_CLIENT
extern int VD__GetVideoEncoderConfigurationOptions(int iCh,int iStream);
extern int VD__GetVideoEncoderConfiguration(int iCh,int iStream);
extern int VD__GetImagingSettings(int iCh);
#endif
#endif
void CNetCaptureManager::onAppEvent(appEventCode code, int index, appEventAction action,
        EVENT_HANDLER *param, const CConfigTable* data /*= NULL*/)
{
#ifdef ONVIF_SERVER
#ifdef ONVIF_CLIENT

    //视频一连接上，先读取前端配置
    if (code == appEventVideoLoss && action == appEventStop)
    {
       if (index>= g_nCapture && CNetCaptureManager::instance()->GetCurrentConfigType(index) == NCPT_ONVIF_CLIENT)
       {
           __trip;
            VD__GetVideoEncoderConfigurationOptions(index,0);
            VD__GetVideoEncoderConfigurationOptions(index,1);

            VD__GetVideoEncoderConfiguration(index,0);
            VD__GetVideoEncoderConfiguration(index,1);
            VD__GetImagingSettings(index);
           __trip;
        }
    }
#endif
#endif
    if( code != appEventUpgrade )
    {
        return ;
    }

    //!24路nvr需要升级时，关闭数字通道，否则，
    //!网络资源点用太多，升级会失败
    ICaptureManager* pCaptureM = ICaptureManager::instance();
    if ( pCaptureM->GetDigitalChnNum() == 24)
    {
            //m_NetTour.Stop();
            for ( LOGIC_CHN i = 0; i < pCaptureM->GetLogicChnNum(); i++)
            {
                if( pCaptureM->IsSupportDigital(i) )
                {
                    SetReconnect(i, 0);
                    StopMonitor(i);
                }
            }
    }
}

int CNetCaptureManager::StartAlarm( )
{
    //return m_pNetDevAlarm->Start();
	return 0;
}
int CNetCaptureManager::StopAlarm( )
{
    //return m_pNetDevAlarm->Stop();
    return 0;
}

VD_VOID CNetCaptureManager::SendPtzData(VD_INT32 iCh,VD_VOID *pData,VD_UINT32 uiLen)
{   
    if (iCh < 0 || iCh >= MAX_NET_CHANNEL )
    {
        __trip;
        return;
    }
    
    if ( ! m_net_channel[iCh] )
    {
        return;
    }

    m_net_channel[iCh]->SendPtzData(pData,uiLen);
    return;
}

int CNetCaptureManager::GetParam(VD_INT32 iCh,int TypeMain, int index, int subIndex,void* buf,  int size)
{   
    if (iCh < 0 || iCh >= MAX_NET_CHANNEL )
    {
        __trip;
        return -1;
    }
    
    if ( ! m_net_channel[iCh] )
    {
        return -1;
    }

    return m_net_channel[iCh]->GetParam(TypeMain,index,subIndex,buf,size);
}

int CNetCaptureManager::SetParam(VD_INT32 iCh,int TypeMain, int index, int subIndex,void* buf,  int size)
{   
    if (iCh < 0 || iCh >= MAX_NET_CHANNEL )
    {
        __trip;
        return -1;
    }
    
    if ( ! m_net_channel[iCh] )
    {
        return -1;
    }

    return m_net_channel[iCh]->SetParam(TypeMain,index,subIndex,buf,size);
}
bool CNetCaptureManager::IsLogin(VD_INT32 iCh)
{   
    if (iCh < 0 || iCh >= MAX_NET_CHANNEL )
    {
        __trip;
        return -1;
    }
    
    if ( ! m_net_channel[iCh] )
    {
        return -1;
    }

    return m_net_channel[iCh]->IsLogin();
}
 
VD_INT32 CNetCaptureManager::GetRemoteCh(LOGIC_CHN iCh)
{
    //return m_NetTour.GetRemoteCh(iCh);
	return iCh;
}

VD_INT32 CNetCaptureManager::GetCurrentConfigType(LOGIC_CHN iCh)
{
    //return m_NetTour.GetCurrentConfigType(iCh);
    return iCh;
}

/* 返回值按位置1表示视频丢失，置0表示解码正常 */
VD_UINT32 CNetCaptureManager::GetDecodeState( )
{
    return ~m_uiDecodeState;
}

VD_VOID   CNetCaptureManager::OnTimer(VD_UINT32 arg)
{
//    arg = arg;   
    
    static VD_UINT32 uiOldData[MAX_NET_CHANNEL] = {0};
    static VD_UINT32 uiTime[MAX_NET_CHANNEL] = {0};

    for(int i=0;i<ICaptureManager::instance()->GetDigitalChnNum();i++)
    {
        /* 原先在解码 */
        if (m_uiDecodeState & (1<<i))
        {
            if (uiOldData[i] == g_uiDecodeDataPer10s[i])
            {
                uiTime[i]++;
            }
            else
            {
                uiTime[i] = 0;
            }
            
            if (uiTime[i]>3)//3s
            {
                m_uiDecodeState &= ~(1<<i);
            }
        }
        /* 原先不在解码 */
        else
        {
            if (uiOldData[i] != g_uiDecodeDataPer10s[i])
            {
                m_uiDecodeState |= (1<<i);
                uiTime[i] = 0;
            }
        }
        
        uiOldData[i] = g_uiDecodeDataPer10s[i];
    }    
}

void CNetCaptureManager::NotifyDecCaps(int iCifCaps)
{

}

void CNetCaptureManager::NotifyDSPInfo(int dwState, VD_BOOL bList)
{
}


int CNetCaptureManager::StopMonitor( LOGIC_CHN channel )
{
    assert( channel < MAX_NET_CHANNEL );
    if( channel <= -1 )
    {
        for( int i = 0 ;i < MAX_NET_CHANNEL; i++ )
        {
            if ( !m_pCaptureM->IsSupportDigital(i) )
            {
                continue;
            }
            if( m_net_channel[i] != NULL )
            {
                m_net_channel[i]->StopMonitor(NULL);
            }
        }
    }
    else
    {
        if( m_net_channel[channel] != NULL )
        {
            m_net_channel[channel]->StopMonitor(NULL);
        }
    }
    
    return 0;
}

int  CNetCaptureManager::GetDecChannelState(unsigned int i, CHANNELSTATE *chnste)
{
    return 0; 
}

//定时统计码流和检测dsp解码信息，3秒钟检测一次
void CNetCaptureManager::OnNetCap(uint arg)
{
    int cifNum = 0;
    DECODER_DSPINFO dspInfo;

    m_CurTotalVideoCaps = 0;
    ulong dwState = 0;

    clearChannelInfo();  ///清空一下通道出错信息
    
    for (int i = 0; i < MAX_NET_CHANNEL; i++)
    {
        if (NULL != m_net_channel[i])
        {
            m_net_channel[i]->CalculateBitRate();
            if(m_isDump)
            {
                trace("net[%d]", m_net_channel[i]->GetBitRate(NET_CHL_FUNCTION_NUM));
            }
#if 0
            if( 0 == m_net_channel[i]->GetBitRate(CHL_MAIN_T) )
            {
        //        CAppEventManager::instance()->notify(appEventAlarmDecoder_AV, i,
        //                appEventStart, NULL, NULL, NULL);
            }
            else
            {
                CAppEventManager::instance()->notify(appEventAlarmDecoder_AV, i,
                        appEventStop, NULL, NULL, NULL);
            }
#endif            
        }
        memset(&dspInfo, 0 ,sizeof(DECODER_DSPINFO));
        int dspState = DecoderGetDspInfo(i, &dspInfo);

        cifNum = (dspInfo.iVideoWidth *dspInfo.iVideoHeight)/(352*288);
        m_CurDecVideoCaps[i] = 0;
        if(dspInfo.iKBPS >0)
        {
            m_CurDecVideoCaps[i] =  cifNum *dspInfo.iVideoFramesPerSecond;
            m_CurTotalVideoCaps += m_CurDecVideoCaps[i];
        }
        if(m_isDump)
        {
            trace(" %d : cap[%d ], F[%d], BPS[%d]\n",i, cifNum, dspInfo.iVideoFramesPerSecond,dspInfo.iKBPS);
        }
        
        if(!dspState)
        {/*
            cifNum = (dspInfo.iVideoWidth *dspInfo.iVideoHeight)/(352*288);
            m_CurDecVideoCaps[i] = 0;
            if(dspInfo.iKBPS >0)
            {
                m_CurDecVideoCaps[i] =  cifNum *dspInfo.iVideoFramesPerSecond;
                m_CurTotalVideoCaps += m_CurDecVideoCaps[i];
            }
            if(m_isDump)
            {
                trace(" %d : cap[%d ], F[%d], BPS[%d]\n",i, cifNum, dspInfo.iVideoFramesPerSecond,dspInfo.iKBPS);
            }
            */
        }
        else
        { 
            switch(dspState)                    ///依据底层接口返回值进行判断
            {
                case dspError:
                    dwState |= BITMSK(0);
                    break;
                case fmtError: 
                    dwState |= BITMSK(4);
                    m_chanErrInfo.errChannel[fmtErrChan] |= BITMSK(i);
                    break;
                case manuError:
                    dwState |= BITMSK(2);
                    m_chanErrInfo.errChannel[manuErrChan] |= BITMSK(i);
                    break;
                case reslError:
                    dwState |= BITMSK(3);
                    m_chanErrInfo.errChannel[reslErrChan] |= BITMSK(i);
                    break;
                case ifraError:
                    dwState |= BITMSK(5);
                    m_chanErrInfo.errChannel[ifraErrChan] |= BITMSK(i);
                    break;
                default:
                    assert(0);
                    break;
            }
                        
        }
    }
    if(m_isDump)
    {
        trace("dsp total cif25Num = %d \n", m_CurTotalVideoCaps/25);
        if (m_chanErrInfo.errChannel[fmtErrChan] != 0)
        {
            trace("DSP is not support this fmt\n"); 
        }
        if (m_chanErrInfo.errChannel[manuErrChan] != 0)
        {
            trace("DSP is not support this manu\n");
        }
        if (m_chanErrInfo.errChannel[reslErrChan] != 0)
        {
            trace("DSP is not support this resl\n");
        }
        if (m_chanErrInfo.errChannel[ifraErrChan] != 0)
        {
            trace("DSP is not find I frame\n");  
        }
        if (dwState & BITMSK(0))
        {
            trace("Load DSP error\n"); 
        }
    }
    NotifyDecCaps(m_CurTotalVideoCaps);
    NotifyDSPInfo(dwState, static_cast<VD_BOOL>(arg));
}

ERRCHANNELINFO CNetCaptureManager::getErrChanInfo()
{
    return m_chanErrInfo;
}

int  CNetCaptureManager::GetMode(int channelID)
{
    if(channelID < 0 || channelID > 15)
    {
        return -1; 
    }
    return m_iMode[channelID]; 
}
int  CNetCaptureManager::SetDownLoadSize(int i, uint uisize, VD_BOOL bSeeking)
{
    m_tmpDownLoadSize[i] = uisize; 
    m_bSeeking[i] = bSeeking; 
    return 0; 
}

uint  CNetCaptureManager::GetDownLoadSize(int i)
{ 
    //uint tmpsize = m_tmpDownLoadSize[i];
    //m_tmpDownLoadSize[i] = 0; 
    return m_tmpDownLoadSize[i]; 
}

VD_BOOL CNetCaptureManager::GetChannelisSeeking(int i)
{
    return m_bSeeking[i]; 
}

int    CNetCaptureManager::GetChannelStreamType(int channelID)
{
    assert((channelID >= 0) && (channelID < 16));

    if(m_net_channel[channelID] == NULL)
    {
        __trip;
        return -1;
    }
    NET_CAPTURE_CHANNEL_T tStream = (NET_CAPTURE_CHANNEL_T)m_net_channel[channelID]->GetChannelStreamType(); 
    return tStream; 
}

VD_BOOL CNetCaptureManager::SetPlayBackProc(int channel,  CObject * pObj, SIG_DEV_PLAYBACKPROC_DELEGATE pProc)
{
    assert(pObj != NULL);
    m_pbpObj[channel] = pObj; 
    m_sigProc[channel] = pProc; 
    return TRUE;     
}


VD_BOOL CNetCaptureManager::CtrlRecFile(int iCh, int offset)
{ 
    return TRUE; 
}
void CNetCaptureManager::clearChannelInfo()
{
    memset(&m_chanErrInfo, 0, sizeof(ERRCHANNELINFO));
}

int CNetCaptureManager::ConsoleHelp(int argc, char **argv)
{
    char *pszCmd0, *pArg2;
    int local_channel = -1;

    CConsoleArg arg(argc, argv);

    pszCmd0 = arg.getOptions();

    if (pszCmd0 == NULL || argc < 2 )
    {
        //PrintHelp(NULL);
        return 0;
    }
    trace("argc:%d, pszCmd0:%s\n", argc, pszCmd0);

    local_channel = arg.getInt(0);
    if(argc >= 2)
    {
        pArg2 = arg.GetArg(1);
    }
    else
    {
        pArg2 = NULL;
    }

    return 0;
}

