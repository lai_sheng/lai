//    文 件 名： ServerDlgProc.cpp
//    描    述: 用于实现网络视频实时传输

//                               删除原有的网络自适应算法

#ifdef WIN32
#pragma warning( disable : 4786)
#endif

#include "Net/NetWorkService.h"
#include "Devices/DevCapture.h"

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#include "Configs/ConfigNet.h"
#include "Configs/ConfigConversion.h"
#include "Configs/ConfigLocation.h"

#include "Net/NetApp.h"
#include "Functions/DriverManager.h"
#include "APIs/Ide.h"
#include "System/AppConfig.h"

#include "OprPdu/FilterHelper.h"
#include "OprPdu/IVnPdu.h"
#include "VideoMonitor.h"
#include "System/Console.h"
//#include "Net/FtpServer/FtpServerWQ.h"


NET_TRANS_POLICY_CFG g_stNetTransPolicy ;


CNetWorkService::CNetWorkService():CThread("NetWorkService", TP_NET), m_bIsNetRunning(false)
{
    CFilterHelper::instance();
    m_iPrtfperiad = 20;
    m_bTransPrint = false;

    g_Console.registerCmd(CConsole::Proc(&CNetWorkService::OnConsoleNetUser, this),
        "netUser", "useronline!");
}

CNetWorkService::~CNetWorkService()
{
    Stop();
}

void NetInitQualityRateCfg()
{
    CConfigConversion CCfgCVS;
    memset(&g_stNetTransPolicy, 0 ,sizeof(NET_TRANS_POLICY_CFG));
    g_stNetTransPolicy.bNetTransPolicy = 0;
    g_stNetTransPolicy.emNetTransState = NET_TRANS_RATE;
    g_stNetTransPolicy.bHaveDisk = 0;
    g_stNetTransPolicy.bExtStream = 0;
    g_stNetTransPolicy.bHighDownload = 0;

    CConfigNetCommon *pCConfigNetCommon = new CConfigNetCommon;
    pCConfigNetCommon->update();
    CONFIG_NET_COMMON& cfg = pCConfigNetCommon->getConfig();    

    if(TRUE == cfg.bUseTransferPlan )
    {
        g_stNetTransPolicy.bNetTransPolicy = 1;
    }

    if(TRUE == cfg.bUseHSDownLoad )
    {
        g_stNetTransPolicy.bHighDownload = 1;
    }
    
    if(IMAGE_TRANSFER_QUAILITY_PRIOR == cfg.TransferPlan )
    {
        g_stNetTransPolicy.emNetTransState= NET_TRANS_QUALITY;
    }
    delete pCConfigNetCommon;

    tracef("@@@@@@@ enble %d state %d\n",g_stNetTransPolicy.bNetTransPolicy , g_stNetTransPolicy.emNetTransState);

    CConfigEncode* pCfgEncode = new CConfigEncode();

    pCfgEncode->update(); 

    int iChNum = ICaptureManager::instance()->GetLogicChnNum();
    
    for(int curChannel=0;curChannel<iChNum;curChannel++)
    {
        CONFIG_ENCODE& cfgNew = pCfgEncode->getConfig(curChannel);
        tracef("main chn %d Change fps is %d iResol %d rate %d \n",curChannel, cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS,
            cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution,cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate);
        tracef("extra chn %d Change fps is %d iResol %d  rate %d \n",curChannel, cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS,
            cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution,cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate);
        
        g_stNetTransPolicy.usFps[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
        g_stNetTransPolicy.usFps[curChannel + iChNum] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
        
        g_stNetTransPolicy.usResolution[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
        g_stNetTransPolicy.usResolution[curChannel + iChNum] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;

        g_stNetTransPolicy.usDspRate[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
        g_stNetTransPolicy.usDspRate[curChannel + iChNum] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
        
    }
    
    delete pCfgEncode;
    
    CAPTURE_EXT_STREAM CfgExt;
    CaptureGetExtCaps(&CfgExt);
    if(CfgExt.ExtraStream & BITMSK(1))
    {
        g_stNetTransPolicy.bExtStream = 1;
        tracef("@@@@@@@@@@@have extra stream\n");
    }
    else
    {
        tracef("@@@@@@@@@@@dont have extra stream\n");
    }

#ifndef    VN_IPC
    IDE_INFO64    ide_info;
    memset(&ide_info,0, sizeof(IDE_INFO64));
    g_DriverManager.GetIdeInfo(&ide_info);
    if(ide_info.ide_num > 0)
    {
        g_stNetTransPolicy.bHaveDisk = 1;
        tracef("!!!!!!!!!! have hdisk %d\n",ide_info.ide_num );
    }
#endif
}
/*
*网络模块启动
*/
int CNetWorkService::Run(const int argc, char *argv[])
{
    tracef("NetWorkService Run=======>>>>>>>>>>>>>>>>>\n");
    assert(m_bIsNetRunning == false);

    m_bExitNetThread = false;
    
#ifdef RECORD_NET_THREAD_MULTI    
    CaptureHelperManager::instance();
#endif

    if(CreateThread() == FALSE)
    {
        assert(0);
        return FAILURE_RET;
    }

    g_Console.registerCmd (CConsole::Proc( &CNetWorkService::onTrans, this ), "trans", "Trans Printf!");//注册控制台关于网路的操作函数

    return SUCCESS_RET;
}

/*
*传输策略打印控制 
*/
int CNetWorkService::onTrans(int argc, char **argv)
{
    char *pszCmd0;
    char * pArg1;

    CConsoleArg arg(argc, argv);
         pszCmd0 = arg.getOptions();//获得命令选项
         pArg1  = arg.GetArg(0);
     if(pszCmd0 == NULL ||NULL == pArg1)//解决用户只输入net命令时，程序崩溃的问题
     {
        trace("trans command usage:\n");
        trace("trans -p   <open/close>  : printf tans info   open:printf     close: no printf \n");
        trace("trans -t    <20>              : set print Interval 1-60 defalut  20 second    \n");
        return 0;
     }
    
    if(0 == strcmp(pszCmd0,"p"))
    {
        if(0==strcmp(pArg1,"open"))
        {
            m_bTransPrint = true;
        }else if(0 == strcmp(pArg1,"close"))
        {    
            m_bTransPrint = false; 
        }else
        {
            tracef("parg error!\n");
        }
                
    }
    else if(0 == strcmp(pszCmd0,"t"))
    {
        int tmpTm = atoi(pArg1);
        if(tmpTm >0 && tmpTm <61)
        {
            m_iPrtfperiad = tmpTm;
        }
    }
	else
    {
        tracef("cmd error!\n");
    }
    
    trace("New m_bTransPrint %d  m_iPrtfperiad %d\n",m_bTransPrint,m_iPrtfperiad);

    return 0;

}

/*
*网络模块入口
*/
void CNetWorkService::svc()
{
    m_bIsNetRunning = true;
    /*
    *为上下文申请空间
    */
    NetCoreContext *pNetCtx = NULL;
    pNetCtx = NetCoreInit(0, NULL);

    NetCoreIO_t io;
    io.init = &CNetWorkService::NetCoreIoInit;
    io.fini = &CNetWorkService::NetCoreIoFini;
    io.process= &CNetWorkService::NetCoreIoProcess;

    io.port = CConfigNetCommon::getLatest().TCPPort;
    io.udp_port = CConfigNetCommon::getLatest().UDPPort;

    io.context = NULL;

    NetInitQualityRateCfg();
    /*
    * 建立第一个conn加入ctx中，其作用do_accept接受连接
    */
    NetCoreAddHandler(pNetCtx, &io);
    
    while (!m_bExitNetThread)
    {
        NetCorePoll(pNetCtx, 5000);
        struct conn    *c, *nc;

        for (c = pNetCtx->connections; c != NULL; c = nc) 
        {
            nc = c->next;
            if((1 == c->iChangMainToExtraFlag
                || 2 == c->iChangMainToExtraFlag)
                && NULL != c->pCapture)
            {
                c->pCapture->FilterCallback(c, 0);
            }
            
            c->iChangMainToExtraFlag = 0;
        }
    }

    /*
    *网络模块结束
    */
    NetCoreFini(pNetCtx);
    m_bIsNetRunning = false;
}


void CNetWorkService::ThreadProc()
{
    CNetWorkService *pServerDlgProc = (CNetWorkService *)this;
    assert(pServerDlgProc);
    pServerDlgProc->svc();
    
    return ;
}

int CNetWorkService::Stop(const int argc, char *argv[])
{
    tracef("NetWorkService Stop=======>>>>>>>>>>>>>>>>>\n");
    if(m_bIsNetRunning == true) 
    {
        m_bExitNetThread = TRUE;
        DestroyThread();//线程退出
        m_bIsNetRunning = false;
    }
    return SUCCESS_RET;
}


struct conn *CNetWorkService::GetConn(int iDlgNo)
{
    return  g_NetMsgProcesser.GetNetConn(iDlgNo);
}

struct sockaddr_in *CNetWorkService::GetRemoteAddr(int iDlgNo)
{
    static struct sockaddr_in remoteaddr;
    struct conn *pNetConnection = GetConn(iDlgNo);
    if(pNetConnection) 
    {
        memcpy(&remoteaddr, &pNetConnection->sa.u.sin, sizeof(struct sockaddr_in));
    }
    return &remoteaddr;
}

CUser * CNetWorkService::GetUser(int iDlgNo)
{
    static CUser defaultUser;
    struct conn *pNetConnection = GetConn(iDlgNo);

    if (NULL != pNetConnection && NULL != pNetConnection->context)
    {
        return ((CUser *)(pNetConnection->context));
    }
    return &defaultUser;
}

int CNetWorkService::KickOffUser(int iDlgNo)
{
    struct conn *pNetConnection = GetConn( iDlgNo);
    if (NULL != pNetConnection)
    {
        pNetConnection->flags |= FLAG_FINISHED;
    GetUser(iDlgNo)->logout();
    }

    return 0;
}

int CNetWorkService::SetBlockPeer(IPDEF ipPeer, uint iPeriod, time_t LastValid)
{
    g_NetMsgProcesser.SetBlockPeer(ipPeer, iPeriod, LastValid);
    return 0;
}


/*
*每个conn连接的操作初始化，处理以及结束
*/
int CNetWorkService::NetCoreIoInit(struct conn *c)
{
    if (0 != g_NetMsgProcesser.RegisterConn(c))
    {
        return -1;
    }
    return 0;
}


void CNetWorkService::NetCoreIoFini(struct conn * c)
{
    g_NetMsgProcesser.UnRegisterConn(c);
    return;    
}

void CNetWorkService::NetCoreIoProcess(struct conn *c)
{
    assert(c->sock != -1); 
    unsigned long len;

    NetCoreRecv(c);
    len = IO_DATALEN(&c->remote);
    while(len >= sizeof(DVRIP)) 
    {
        struct dvrip szHead;
         memset(&szHead, 0 , sizeof(struct dvrip));
         memcpy(&szHead,(char *)(c->remote.buf + c->remote.tail),sizeof(struct dvrip));
        if (len >= (szHead.dvrip_extlen + sizeof(DVRIP))) 
        {
            g_NetMsgProcesser.MsgProcess(c, (char *)c->remote.buf + c->remote.tail, szHead.dvrip_extlen + sizeof(DVRIP));

            c->remote.tail += szHead.dvrip_extlen + sizeof(DVRIP);
            len = IO_DATALEN(&c->remote);
        } 

        else 
        {
            break;
        }
    }

    /* 放到循环外面搬移更合理 */
    if ( len > 0 )
    {
        memcpy(c->remote.buf, c->remote.buf + c->remote.tail, len);
        c->remote.head = len; 
        c->remote.tail = 0;
    }
}

int CNetWorkService::OnConsoleNetUser(int argc, char **argv)
{
    char *pszCmd0, *pArg1 = NULL, *pArg2 = NULL;
    CConsoleArg arg(argc, argv);
    pszCmd0 = arg.getOptions();

    if (pszCmd0 == NULL )
    {
        OnHelp( "help", NULL, NULL );
        return 0;
    }

    if( argc >= 2  )
    {
        pArg1 = arg.GetArg(0);
    }
    else if( argc >= 3 )
    {
        pArg2 = arg.GetArg(1);
    }
    OnHelp( pszCmd0, pArg1, pArg2 );
}

void CNetWorkService::OnHelp( char* opr, char* value1, char* value2)
{
    if( opr == NULL )
    {
            OnHelp( "help", NULL, NULL );
    }

    if( 0 == strcmp(opr, "help") )
    {
        int jj = 0;

        for (int ii=0; ii<C_MAX_TCP_CONN_NUM; ii++)
        {
                struct conn *pNetConnection = GetConn(ii);

                printf("#######ii=%d\r\n",ii);
                
                if (NULL != pNetConnection )
                {
                    printf("sock = %d\r\n",pNetConnection->sock);
                    printf("iSubConnChn = %d\r\n",pNetConnection->iSubConnChn);
                    printf("iSubConnType = %d\r\n",pNetConnection->iSubConnType);
                    printf("iChangMainToExtraFlag = %d\r\n",pNetConnection->iChangMainToExtraFlag);
                    printf("iSocketSendFlag = %d\r\n",pNetConnection->iSocketSendFlag);
                    printf("iSocketNeedChgFlag = %d\r\n",pNetConnection->iSocketNeedChgFlag);
                    printf("iExtraCfgChangFlag = %d\r\n",pNetConnection->iExtraCfgChangFlag);
                    printf("iClientFlag = %d\r\n",pNetConnection->iClientFlag);
                    printf("iRecDownNormalEndFlag = %d\r\n",pNetConnection->iRecDownNormalEndFlag);
                    printf("ch_no = %d\r\n",pNetConnection->netSTtateMonitor.ch_no);
                    
                    for(int jj=0;jj<pNetConnection->netSTtateMonitor.ch_no;jj++)
                    {
                        printf("ch_no_func[%d] = %d\r\n",jj,pNetConnection->netSTtateMonitor.ch_no_func[jj]);
                    }
                    
                    printf("iOnlineFlag = %d\r\n",pNetConnection->iOnlineFlag);
                    printf("bVodFlag = %d\r\n",pNetConnection->bVodFlag);
                    printf("iVodChn = %d\r\n",pNetConnection->iVodChn);

                    if (pNetConnection->context)
                    {
                        printf("user name = %s\r\n",g_Net.GetUser(ii)->getName().c_str());
                        printf("sin_addr = %s\r\n",inet_ntoa(g_Net.GetRemoteAddr(ii)->sin_addr));
                        printf("Type = %s\r\n",g_Net.GetUser(ii)->getType().c_str());
                    }
                }
            }            
    }
    else if( 0 == strcmp(opr, "debug") )
    {
    }
}

CNetWorkService * CNetWorkServiceFactory::instance()
{									
	static CNetWorkService * _instance = NULL;		
	if( NULL == _instance)						
	{
		#ifdef __BELL_QZTEL__
		_instance = new CBellNetWorkService;
		#else
		_instance = new CNetWorkService;	
		#endif			
	}											
	return _instance;						
}	


