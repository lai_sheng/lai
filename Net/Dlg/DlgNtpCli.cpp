#if defined(USING_PROTOCOL_NTP)

#include "Net/Dlg/DlgNtpCli.h"

#include "Configs/ConfigConversion.h"

#include "ez_libs/ez_socket/ez_socket.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"

#ifdef IPC_JZ
#include "APIs/ExtDev.h"
#endif
#include <sys/time.h>
#include <unistd.h>
#include "APIs/CommonBSP.h"

#ifdef SUNING
#include "Intervideo/RecordTs/RecordTsManager.h"
#include "Intervideo/RecordTs/SNDeviceAttr.h"
#endif
#include "Functions/Record.h"
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#endif
#ifdef		MOBILE_COUNTRY
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#include "Intervideo/MobileCountry/MobileCountryHttpsUpgrade.h"
#endif
#define TIME_20170101 (1483200000)
#define TIME_20180201 (1517443200)

#define TIME_20180426 (1524672000)


typedef struct _TIME_ZONE
{
	int index;
	int zone_time;
	char *str;
}TIME_ZONE_T;

TIME_ZONE_T gTimeZone[] =
{
	{0, 0*3600,"GMT+00:00"},
	{1, 1*3600,"GMT+01:00"},
	{2, 2*3600,"GMT+02:00"},
	{3, 3*3600,"GMT+03:00"},
	{4, 3*3600+1800,"GMT+03:30"},
	{5, 4*3600,"GMT+04:00"},
	{6, 4*3600+1800,"GMT+04:30"},
	{7, 5*3600,"GMT+05:00"},
	{8, 5*3600+1800,"GMT+05:30"},
	{9, 5*3600+1800+900,"GMT+05:45"},
	{10, 6*3600,"GMT+06:00"},
	{11, 6*3600+1800,"GMT+06:30"},
	{12, 7*3600,"GMT+07:00"},
	{13, 8*3600,"GMT+08:00"},
	{14, 9*3600,"GMT+09:00"},
	{15, 9*3600+1800,"GMT+09:30"},
	{16, 10*3600,"GMT+10:00"},
	{17, 11*3600,"GMT+11:00"},
	{18, 12*3600,"GMT+12:00"},
	{19, 13*3600,"GMT+13:00"},
	{20, -1*3600,"GMT-01:00"},
	{21, -2*3600,"GMT-02:00"},
	{22, -3*3600,"GMT-03:00"},
	{23, -3*3600-1800,"GMT-03:30"},
	{24, -4*3600,"GMT-04:00"},
	{25, -5*3600,"GMT-05:00"},
	{26, -6*3600,"GMT-06:00"},
	{27, -7*3600,"GMT-07:00"},
	{28, -8*3600,"GMT-08:00"},
	{29, -9*3600,"GMT-09:00"},
	{30, -10*3600,"GMT-10:00"},
	{31, -11*3600,"GMT-11:00"},
	{32, -12*3600,"GMT-12:00"},
};
static int ChangeNetDns(void)
{
	size_t size=-1;
	FILE *fp;
	char *pname;
	char DnsName[2][64];
	char line[128];
	char *Dnsback="119.29.29.29";
	char *Dnsback1 ="114.114.114.114";
	char *Dnsback2 = "8.8.8.8"; 
	fp = fopen("/etc/resolv.conf","w");
	if(NULL ==fp)
	{
		fprintf(stderr,"fopen DNS config file error !\n");
		return -1;
	}
	fseek(fp,0,SEEK_SET);
	fprintf(fp,"options timeout:1\n");
	fprintf(fp,"nameserver %s\n",Dnsback);
	fprintf(fp,"nameserver %s\n",Dnsback1);
	fprintf(fp,"nameserver %s\n",Dnsback2);
	fclose(fp);
	return 0;
}

int GetServerIpAddrFromDns(const char *Server, char *Ipaddr)
{
	struct addrinfo * res, *pt;   
	struct sockaddr_in *sinp;	
	struct addrinfo hints;
	const char *addr;  
	int succ=0,i=0; 
	char abuf[INET_ADDRSTRLEN];  


	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET; /* Allow IPv4 */
	hints.ai_flags = AI_PASSIVE; /* For wildcard IP address */
	hints.ai_protocol = 0; /* Any protocol */
	hints.ai_socktype = SOCK_STREAM;
	succ = getaddrinfo(Server, NULL, &hints, &res);   
	if(succ != 0)	
	{	
		if(-2 == succ)
		{
			//getaddrinfo 对于数据的格式是挑剔的，
			//在解析配置文件时确保没有额外的
			//空格通常在主机名参数中尾随空格，
			//否则你会得到这个错误
			ChangeNetDns();
		}
		_printd("Can't get address info! error code = %d,info : %s\n", succ,gai_strerror(succ));  
		return -1;
	}
	else
	{
		if (res->ai_addr->sa_family == AF_INET)
		{
			if (Ipaddr != NULL)
			{
				struct sockaddr_in* pServAddr = (struct sockaddr_in *)res->ai_addr;
				inet_ntop(AF_INET, &(pServAddr->sin_addr), Ipaddr, INET_ADDRSTRLEN);
			}

		}
	}
	_printd("Ipaddr:%s",Ipaddr);

	freeaddrinfo(res);

	return 0;
}


#define JAN_1970    0x83aa7e80 					/* 2208988800 seconds (1970 - 1900)*/
#define NTPFRAC(x) ( 4294*(x) + ( (1981*(x))>>11 ) )
#define USEC(x) (((x) >> 12 ) - 759 * ((( (x) >> 10 ) + 32768 ) >> 16 ))

PATTERN_SINGLETON_IMPLEMENT(CDlgNtpCli);
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CDlgNtpCli::CDlgNtpCli() :CThread("NtpCli", TP_DEFAULT)
,m_update_time("ntp"),m_iupdate_time_out(0)
{
	this->update_count 	= 0;
	m_NtpUpdataTime    	= 0;
	DaylightStart		= VD_FALSE;

	#ifdef IPC_JZ
	
	SYSTEM_TIME szSystemTime;
	struct tm *ptmTime;		
	struct tm       ntime;
	struct timeval 	tv;		
	
	setenv("TZ", "GMT", 1);//CST-8 GMT-8
	tzset();  
	
	GetMcuTime(&ntime);

	m_NtpUpdataTime = mktime(&ntime);
	_printd("Get McuTime:%ld\n",mktime(&ntime));

	if(m_NtpUpdataTime < TIME_20180426)
		m_NtpUpdataTime = TIME_20180426;
			
//设置默认时区 8
    struct timezone tz;
	gettimeofday(NULL, &tz);
	tz.tz_minuteswest = 8 * 60;
    if(0 == settimeofday(NULL, &tz))
		m_NtpUpdataTime += tz.tz_minuteswest*60;

	ptmTime = gmtime(&m_NtpUpdataTime);
	szSystemTime.second  = (int) ptmTime->tm_sec;
	szSystemTime.minute  = (int) ptmTime->tm_min;
	szSystemTime.hour    = (int) ptmTime->tm_hour;
	szSystemTime.day     = (int) ptmTime->tm_mday;
	szSystemTime.month   = (int) (ptmTime->tm_mon + 1);
	szSystemTime.year    = (int) (ptmTime->tm_year + 1900);/*current year-1900,调整下*/
	szSystemTime.wday    = (int) ptmTime->tm_wday;


	SystemSetCurrentTime(&szSystemTime);		
	#elif defined(IPC_JZ_NEW)
	//IPC_R2S2 IPC_A3S2 IPC_P2 IPC_R2S1 IPC_R2P2 IPC_G3S2
	setenv("TZ", "GMT", 1);//CST-8 GMT-8
	tzset(); 	
	#endif

	trace("CDlgNtpCli Enter--------\n");
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CDlgNtpCli::~CDlgNtpCli()
{
}

/*	$FXN :	Start()
==	======================================================================
==	$DSC :	Ntp功能的启动函数，创建了线程
==	$ARG :	
==		 :	
==	$RET :	TURE成功	 FALSE失败
==	$MOD :	
==	======================================================================
*/
VD_BOOL CDlgNtpCli::Start()
{
	int iRet = 0;
	
	m_cCfgNtp.attach(this, (TCONFIG_PROC)&CDlgNtpCli::OnCfgNtpCfg);
	m_cCfgNtp.update();
	OnCfgNtpCfg(m_cCfgNtp,iRet);
	
	m_update_time.Start(this, (VD_TIMERPROC)&CDlgNtpCli::AdjustTimeWithNTP, 0, m_cCfgNtp.getConfig().UpdatePeriod*1000*60);

	m_daylight_time.Start(this, (VD_TIMERPROC)&CDlgNtpCli::DaylightCheck, 1000, 2*1000);
	
	return TRUE;//CreateThread();
}
/*	$FXN :	ThreadProc()
==	======================================================================
==	$DSC :	Start()函数创建的线程的执行体
==	$ARG :	
==		 :	
==	$RET :
==	$MOD :
==	======================================================================
*/
void CDlgNtpCli::ThreadProc()
{
//	while (1)
	{
		//tracepoint();
		//AdjustTimeWithNTP();
	}
}

int CDlgNtpCli::TimeWithNtp(char *pServieName, int nPort, int nTimeZone, DAYLIGHT_TIME sDayLight)
{	
	int  iSocket = -1;
	int  iRet    = -1;
	uint uiDataBuffer[64] = {0};/*收发包缓冲*/
	
	SYSTEM_TIME szSystemTime;
	struct tm *ptmTime;	

	fd_set ReadSet;
	struct timeval tvTimeout;	

	if(NULL == pServieName)
	{
		return -1;
	}
	if(0 == nPort)
	{
		nPort = 123;
	}

	time_t stNow,stSetTime;
	char host[16] = {0};
//	printf("=====>>>>>>ServerName =%s\n", pServieName);
	
	
  if(inet_addr(pServieName) == INADDR_NONE)
  { 
	  if(GetServerIpAddrFromDns(pServieName,host))
  	//if(RealTimeNetDns(pServieName,host,16))
  	{
		struct hostent* mhost = gethostbyname(pServieName);
		if(mhost == NULL)
		{
			strcpy( host, pServieName);
			return -1;
		}
		else
		{
			Ip2Str(*(unsigned long *)mhost->h_addr_list[0], host);
		}
  	}
  }
	//trace("host[%s], port[%d]\n", host, nPort) ; 
	iSocket = ez_udp_connect(host, nPort);
	if (iSocket < 0)
	{
		//tracepoint();
		_printd("host[%s] port[%d]",host,nPort);
		m_iupdate_time_out ++;
		return -1;
	}	


	/*发包*/
	memset(uiDataBuffer,0,sizeof(uiDataBuffer));
	uiDataBuffer[0] = htonl ( ( 0 << 30 ) | ( 3 << 27 ) | ( 3 << 24 ) | ( 0 << 16) | ( 4 << 8 ) | (( -6) & 0xff ) );
	uiDataBuffer[1] = htonl(1<<16); 
	uiDataBuffer[2] = htonl(1<<16); 
	time(&stNow);								/* 发包时间since (00:00:00), January 1, 1970*/
	uiDataBuffer[10] = htonl((unsigned long)stNow + JAN_1970); 	/*Timestamp,since 1900*/
	uiDataBuffer[11] = htonl(0);
	
	iRet = ez_writen(iSocket,uiDataBuffer,48);
	if (iRet <= 0)
	{
		tracepoint();
		ez_close(iSocket);
		return -1;
	}
	
	/*收包*/
	struct timeval  tvout;
	long  start_select_time = 0 ,wait_time = 0;
	gettimeofday(&tvout,NULL);
	start_select_time = tvout.tv_sec;
	tvTimeout.tv_sec = 8;
	tvTimeout.tv_usec = 0;
	FD_ZERO(&ReadSet);
	FD_SET(iSocket, &ReadSet);
	iRet = select(iSocket + 1, &ReadSet, NULL, NULL, &tvTimeout);
	if (iRet <= 0)
	{
		trace("SNTP Rec Packet Timeout!\n");
		ez_close(iSocket);
		m_iupdate_time_out ++;
		return -1;
	}
	
	if (FD_ISSET(iSocket, &ReadSet))
	{
		gettimeofday(&tvout,NULL);
		wait_time =   tvout.tv_sec - start_select_time; 
		if(wait_time < 0 || wait_time > 8 )
		{
			wait_time = 0;
		}
		
		memset(uiDataBuffer,0,sizeof(uiDataBuffer));
		iRet = recv(iSocket,(char *)uiDataBuffer,sizeof(uiDataBuffer)*sizeof(uint), 0);
		if (iRet >= 48 && iRet < (int)(sizeof(uiDataBuffer)*sizeof(uint)))
		{
			/*记录收包时间*/
			time(&stNow);

#define DataOffset(i) ntohl(((uint *)uiDataBuffer)[i])	
			if ((DataOffset(0) >> 27 & 0x07)< 3 
				|| ((DataOffset(0) >> 24 & 0x07) != 4 
						&&(DataOffset(0) >> 24 & 0x07) != 5))
			{
				trace("SNTP Rec Packet error!\n");
				ez_close(iSocket);
				return -1;
			}

			/*网络上传输偏差调整，当地时区调整*/
			stSetTime  = DataOffset(10) - JAN_1970;
			stSetTime += ( (stNow + JAN_1970 - DataOffset(6))-(DataOffset(10) - DataOffset(8)) )/2 ;
#if	defined(IPC_JZ)|| defined(IPC_P2)|| defined(IPC_JZ_NEW)
			struct timeval  tv;
			struct timezone tz;

			gettimeofday(&tv, &tz);
			stSetTime += tz.tz_minuteswest*60;	
#else
//夏令时判断
			if(sDayLight.Enable == VD_TRUE &&
			   stSetTime >= sDayLight.StartTime && 
			   stSetTime <= sDayLight.EndTime){
				_printd("%ld s in (%ld,%ld),so enable daylight-time",stSetTime,
					sDayLight.StartTime,sDayLight.EndTime);
				stSetTime += 3600;
			}
			
			stSetTime  += (nTimeZone*3600);
#endif
#undef DataOffset
			stSetTime += 1;
			stSetTime += wait_time/2;
			ptmTime = gmtime(&stSetTime);	

			szSystemTime.second  = (int) ptmTime->tm_sec;
			szSystemTime.minute  = (int) ptmTime->tm_min;
			szSystemTime.hour    = (int) ptmTime->tm_hour;
			szSystemTime.day     = (int) ptmTime->tm_mday;
			szSystemTime.month   = (int) (ptmTime->tm_mon + 1);
			szSystemTime.year    = (int) (ptmTime->tm_year + 1900);/*current year-1900,调整下*/
			szSystemTime.wday    = (int) ptmTime->tm_wday;

			_printd("SNTP UpdateTime --%d:%d:%d:%02d:%02d:%02d\n",
				szSystemTime.year,szSystemTime.month,szSystemTime.day,
				szSystemTime.hour,szSystemTime.minute,szSystemTime.second);


			SYSTEM_TIME getSystemTime;
			SystemGetCurrentTime(&getSystemTime);
			
			_printd("current time ---%d:%d:%d:%02d:%02d:%02d\n",
							getSystemTime.year,getSystemTime.month,getSystemTime.day,
							getSystemTime.hour,getSystemTime.minute,getSystemTime.second);

			struct tm update_tm; //同步时间  
			struct tm cur_tm;	//当前系统时间
			update_tm.tm_year = szSystemTime.year - 1900;
			update_tm.tm_mon  = szSystemTime.month - 1;
			update_tm.tm_mday = szSystemTime.day;
			update_tm.tm_hour = szSystemTime.hour ;
			update_tm.tm_min  = szSystemTime.minute;
			update_tm.tm_sec  = szSystemTime.second;

			cur_tm.tm_year = getSystemTime.year - 1900;
			cur_tm.tm_mon  = getSystemTime.month - 1;
			cur_tm.tm_mday = getSystemTime.day;
			cur_tm.tm_hour = getSystemTime.hour ;
			cur_tm.tm_min  = getSystemTime.minute;
			cur_tm.tm_sec  = getSystemTime.second;
			
			time_t t_start =  mktime(&update_tm);
			time_t t_end   =  mktime(&cur_tm);	
			
			_printd("updatetime:%ld currenttime:%ld ",t_start,t_end);
			uint result = abs(t_end - t_start);
			//一秒以内可能存在网络偏差暂时不修改
			if(result >= 2)
			{
				SystemSetCurrentTime(&szSystemTime);
			}
			time(&m_NtpUpdataTime);

			#ifdef IPC_JZ
			SetCurrentTimeSyncMcu();
			#endif
			#ifdef SUNING
			g_RecordTsManager.SetNtpSuccess();
			g_SN_DevAttr_OTA.SetNtpSuccess();
			
			#endif
			g_Record.SetNtpSuccess();
			#ifdef HEMU_CLOUD
			g_HeMuHttpsCom.SetNtpSuccess();
			#endif
#ifdef		MOBILE_COUNTRY
			g_MobileCountrySdkApi.SetNtpSuccess();
			g_MobileCountryUpgrade.SetNtpSuccess();
#endif
			update_count ++;

			ez_close(iSocket);
			return 0;
		}
	}
	ez_close(iSocket);
	
	return -1;
}


void CDlgNtpCli::AdjustTimeWithNTP(uint arg)
{	
	int  	ii    = 0;
	int  	nPort = 0;
	double  nTimeZone = 0;
	//int		nDayLight = 0;
	DAYLIGHT_TIME sDayLightTime = {0};
	
	char 	strHostName[32] = {0};


	for(ii=0; ii<3; ii++)
	{
		CONFIG_NET_NTP &cfgNtp = m_cCfgNtp.getConfig();

		nPort	  	=	cfgNtp.Server.Port;
		nTimeZone 	= 	((float)cfgNtp.TimeZone)/10;
	
	//	nDayLight  	= 	cfgNtp.DaylightEnable;
		sDayLightTime.StartTime = cfgNtp.DaylightTime.StartTime;
		sDayLightTime.EndTime 	= cfgNtp.DaylightTime.EndTime;
		sDayLightTime.Enable    = cfgNtp.DaylightTime.Enable;
	//		if(nDayLight) nTimeZone+=1;
		_printd("nTimeZone:%f",nTimeZone);
		
		if(0 == nPort)
		{
			//nPort = 123;
			_printd("nTimeZone:%f,nPort[%d]",nTimeZone,nPort);
		}
	#if 1
		if(m_iupdate_time_out > 5)
		{
			//另两个域名同步超时5次则用windows域名同步
			//modify by jwd on 20190225
			//根据测试结果 time.windows.com 域名同步偶尔导致时间相差
			//10秒以上的偏差 
			memset(strHostName, 0, sizeof(strHostName));
			strcpy(strHostName, cfgNtp.Server.ServerName);  //默认
			if(0 == TimeWithNtp(strHostName, nPort, nTimeZone,sDayLightTime))
			{
				m_iupdate_time_out = 0;
				return;
			}
			//m_iupdate_time_out = 0;
		}
	#endif	
		memset(strHostName, 0, sizeof(strHostName));
		strcpy(strHostName, "cn.ntp.org.cn");
		if(0 == TimeWithNtp(strHostName, nPort, nTimeZone,sDayLightTime))
		{
			m_iupdate_time_out = 0;
			return;
		}
	
		memset(strHostName, 0, sizeof(strHostName));
		strcpy(strHostName, "jp.pool.ntp.org");  //日本
		if(0 == TimeWithNtp(strHostName, nPort, nTimeZone,sDayLightTime))
		{
			m_iupdate_time_out = 0;
			return;
		}
		
		if(ii >= 2) //获取失败则不退出，一直尝试更新
		{
			if(0 >= m_NtpUpdataTime) //世界未更新
			{
				time(&m_NtpUpdataTime);
		//		if(m_NtpUpdataTime < TIME_20180201) 
				{
					SYSTEM_TIME szSystemTime;
					struct tm *ptmTime;	

					#ifdef IPC_JZ
					
					struct tm       ntime;
					struct timeval 	tv;		
			
					GetMcuTime(&ntime);
					m_NtpUpdataTime = mktime(&ntime) + nTimeZone*3600;
					_printd("Get McuTime:%ld\n",mktime(&ntime));

					#else
					m_NtpUpdataTime = TIME_20180426 + nTimeZone*3600;
					//m_NtpUpdataTime = TIME_20180201 + gTimeZone[nTimeZone].zone_time;
					#endif				

					ptmTime = gmtime(&m_NtpUpdataTime);
					szSystemTime.second  = (int) ptmTime->tm_sec;
					szSystemTime.minute  = (int) ptmTime->tm_min;
					szSystemTime.hour    = (int) ptmTime->tm_hour;
					szSystemTime.day     = (int) ptmTime->tm_mday;
					szSystemTime.month   = (int) (ptmTime->tm_mon + 1);
					szSystemTime.year    = (int) (ptmTime->tm_year + 1900);/*current year-1900,调整下*/
					szSystemTime.wday    = (int) ptmTime->tm_wday;

					trace("SNTP UpdateTime --%d:%d:%d:%d:%d:%d\n",
						szSystemTime.year,szSystemTime.month,szSystemTime.day,
						szSystemTime.hour,szSystemTime.minute,szSystemTime.second);

					SystemSetCurrentTime(&szSystemTime);
				
				}
			}
	
			sleep(1);
			ii = 0;
		}
	}
}
void CDlgNtpCli::DaylightCheck()
{
	CGuard guard(m_Mutex);
	VD_BOOL SetTimeFlag   = VD_FALSE;
	
	CONFIG_NET_NTP &cfgNtp = m_cCfgNtp.getConfig();
	int	timezoneSec = (cfgNtp.TimeZone * 60)/10;
#if	defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)	
	struct timezone tz;
    struct timeval  tv;
	gettimeofday(&tv, &tz);
	
	if(VD_TRUE == cfgNtp.DaylightTime.Enable &&
	   tv.tv_sec >= cfgNtp.DaylightTime.StartTime && 
	   tv.tv_sec <= cfgNtp.DaylightTime.EndTime){
		DaylightStart = VD_TRUE;
	}	
	else{
		DaylightStart = VD_FALSE;
		
	}

	if(VD_FALSE == DaylightStart){
		if(tz.tz_minuteswest > timezoneSec){
			_printd("%ld is out of (%ld,%ld),so unable daylight-time",tv.tv_sec,
				cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);
		
			tz.tz_minuteswest -= 60;
			SetTimeFlag = VD_TRUE;
		}
	}
	else if(VD_TRUE == DaylightStart){
		if(tz.tz_minuteswest <= timezoneSec){
			_printd("%ld is in (%ld,%ld),so enable daylight-time",tv.tv_sec,
				cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);

			tz.tz_minuteswest += 60;
			SetTimeFlag = VD_TRUE;
		}
	}

	if(VD_TRUE == SetTimeFlag){
		settimeofday(&tv, &tz);
		#ifdef FeedDog    //checktime 夏令时变更
		UartManage_SetMcutime();  
		#endif
	}
	
#else
	VD_BOOL mDaylightStart = VD_FALSE;
	SYSTEM_TIME szSystemTime;
	struct tm *ptmTime;	
				
	time(&m_NtpUpdataTime);
	if(VD_TRUE == DaylightStart){
		m_NtpUpdataTime -= 3600;
	}

	if(VD_TRUE == cfgNtp.DaylightTime.DaylightTime &&
	   m_NtpUpdataTime >= cfgNtp.DaylightTime.StartTime && 
	   m_NtpUpdataTime <= cfgNtp.DaylightTime.EndTime){
		mDaylightStart = VD_TRUE;
	}
	else{
		mDaylightStart = VD_FALSE;
		
	}	

	if(mDaylightStart != DaylightStart){
		if(VD_FALSE == mDaylightStart){
			_printd("%ld is out of (%ld,%ld),so unable daylight-time",m_NtpUpdataTime,
				cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);
		}
		else{
			_printd("%ld is in (%ld,%ld),so enable daylight-time",m_NtpUpdataTime,
				cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);
			m_NtpUpdataTime += 3600;
		}
		
		SetTimeFlag 	= VD_TRUE;
		DaylightStart	= mDaylightStart;
	}
	

	if(VD_TRUE == SetTimeFlag){
		m_NtpUpdataTime  += ((cfgNtp.TimeZone*3600)/10);

		ptmTime = gmtime(&m_NtpUpdataTime);	

		szSystemTime.second  = (int) ptmTime->tm_sec;
		szSystemTime.minute  = (int) ptmTime->tm_min;
		szSystemTime.hour    = (int) ptmTime->tm_hour;
		szSystemTime.day     = (int) ptmTime->tm_mday;
		szSystemTime.month   = (int) (ptmTime->tm_mon + 1);
		szSystemTime.year    = (int) (ptmTime->tm_year + 1900);/*current year-1900,调整下*/
		szSystemTime.wday    = (int) ptmTime->tm_wday;

		trace("SetTime --%d:%d:%d:%02d:%02d:%02d\n",
			szSystemTime.year,szSystemTime.month,szSystemTime.day,
			szSystemTime.hour,szSystemTime.minute,szSystemTime.second);

		SystemSetCurrentTime(&szSystemTime)
	}
#endif
	
}
void CDlgNtpCli::OnCfgNtpCfg(CConfigNetNTP& cfg_in_tmp, int &ret)
{	
	CGuard guard(m_Mutex);
	CONFIG_NET_NTP &cfgNtp = cfg_in_tmp.getConfig();
	
#if	defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)
    struct timezone tz;
    struct timeval  tv;
	_printd("Set TimeZone:%d,",cfgNtp.TimeZone); 
	gettimeofday(&tv, &tz);
	tz.tz_minuteswest = (cfgNtp.TimeZone * 60)/10;

	if(VD_TRUE == cfgNtp.DaylightTime.Enable &&
	   tv.tv_sec >= cfgNtp.DaylightTime.StartTime && 
	   tv.tv_sec <= cfgNtp.DaylightTime.EndTime){
		_printd("%ld is in (%ld,%ld),so enable daylight-time",tv.tv_sec,
			cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);
		tz.tz_minuteswest += 60;
		DaylightStart	= VD_TRUE;
	}
	else{
		DaylightStart	= VD_FALSE;
	}

    settimeofday(&tv, &tz);

	#ifdef FeedDog
	UartManage_SetMcutime();  
	#endif
	
#else
	SYSTEM_TIME szSystemTime;
	struct tm *ptmTime;	
				
	time(&m_NtpUpdataTime);
	if(VD_TRUE == DaylightStart){
		m_NtpUpdataTime -= 3600;
	}
		
	if(VD_TRUE == cfgNtp.DaylightTime.DaylightTime &&
	   m_NtpUpdataTime >= cfgNtp.DaylightTime.StartTime && 
	   m_NtpUpdataTime <= cfgNtp.DaylightTime.EndTime){
		_printd("%ld is in (%ld,%ld),so enable daylight-time",m_NtpUpdataTime,
			cfgNtp.DaylightTime.StartTime,cfgNtp.DaylightTime.EndTime);
		m_NtpUpdataTime += 3600;
		DaylightStart	= VD_TRUE;
	}
	else{
		DaylightStart 	= VD_FALSE;
	}

	m_NtpUpdataTime  += ((cfgNtp.TimeZone*3600)/10);
	ptmTime = gmtime(&m_NtpUpdataTime);	

	szSystemTime.second  = (int) ptmTime->tm_sec;
	szSystemTime.minute  = (int) ptmTime->tm_min;
	szSystemTime.hour    = (int) ptmTime->tm_hour;
	szSystemTime.day     = (int) ptmTime->tm_mday;
	szSystemTime.month   = (int) (ptmTime->tm_mon + 1);
	szSystemTime.year    = (int) (ptmTime->tm_year + 1900);/*current year-1900,调整下*/
	szSystemTime.wday    = (int) ptmTime->tm_wday;

	trace("SetTime --%d:%d:%d:%02d:%02d:%02d\n",
		szSystemTime.year,szSystemTime.month,szSystemTime.day,
		szSystemTime.hour,szSystemTime.minute,szSystemTime.second);

	SystemSetCurrentTime(&szSystemTime);	
#endif	
	//只有时间不同时才更新定时器
	if (cfgNtp.UpdatePeriod != m_cCfgNtp.getConfig().UpdatePeriod) 
	{
		if( m_update_time.IsStarted() )
		{
			m_update_time.Stop();
		}
		m_update_time.Start(this, (VD_TIMERPROC)&CDlgNtpCli::AdjustTimeWithNTP, 0, 60*60*1000);//cfgNtp.UpdatePeriod*
	}
	
	m_cCfgNtp.update();
	
	ret |= CONFIG_APPLY_OK;
}

int CDlgNtpCli::GetNtpZoneIndex( ZONE_TIME_E zone )
{
	return (int)zone;
}
int CDlgNtpCli::GetNtpUpdateCount()
{
	return this->update_count;
}
int NetStr2Int(char * str)
{
	assert(str != NULL);

	int mul=1, value=0;
	for(unsigned int i=1; i<strlen(str); i++)
	    mul*=10;

	for(unsigned int j=0; j<strlen(str); j++)
	{
	    value += (str[j] - 48) * mul;
	    mul/=10;
	}
	return value;
}

char * NetInt2Str( int value )
{
	static char tmpBuf[32];	//!始终驻留

	memset(tmpBuf, 0, sizeof(tmpBuf) );
	ez_snprintf( tmpBuf, sizeof(tmpBuf), "%d" , value );

	return tmpBuf;
}

int CDlgNtpCli::LoadNtpCfg( char * pBuf, int BufLen )
{
	CONFIG_NTP_PROTO_SET tmp_ntp_cfg;
	memset(&tmp_ntp_cfg, 0, sizeof(CONFIG_NTP_PROTO_SET));
	std::string tmpStr;
	tracepoint();

	CConfigNetNTP *pCfgNetNtp = new CConfigNetNTP();
	pCfgNetNtp->update();
	CONFIG_NET_NTP& cfgNtp = pCfgNetNtp->getConfig();
	tmp_ntp_cfg.isEnable = cfgNtp.Enable ;
	strcpy(tmp_ntp_cfg.HostIP, cfgNtp.Server.ServerName);
	tmp_ntp_cfg.iTimeZone = cfgNtp.TimeZone ;
	tmp_ntp_cfg.nUpdateInterval = cfgNtp.UpdatePeriod ;
	tmp_ntp_cfg.unHostPort = cfgNtp.Server.Port;
	delete pCfgNetNtp;
	
	tracepoint();
	tmpStr = "Enable:";
	//printf("===>tmp_ntp_cfg.isEnable[%d]\n", tmp_ntp_cfg.isEnable);
	tmpStr += (tmp_ntp_cfg.isEnable == 0?"false":"true");
	tmpStr += "\r\n";

	tmpStr += "Host:";
	tmpStr += tmp_ntp_cfg.HostIP;
	tmpStr += "\r\n";

	tmpStr += "Port:";
	tmpStr += NetInt2Str( tmp_ntp_cfg.unHostPort );
	tmpStr += "\r\n";

	tmpStr += "UpdateInterval:";
	tmpStr += NetInt2Str( tmp_ntp_cfg.nUpdateInterval );
	tmpStr += "\r\n";

	tmpStr += "TimeZone:";
	tmpStr += NetInt2Str( tmp_ntp_cfg.iTimeZone );
	tmpStr += "\r\n";

	tmpStr += "\r\n";

	if( strlen( tmpStr.c_str() ) >= (size_t)BufLen )
	{
		trace("LoadNtpCfg>: BufLen[%d] is too short!\n", BufLen);
		return -1;
	}

	strcpy( pBuf, tmpStr.c_str() );

	trace("LoadNtpCfg>: %s\n", pBuf);
	return 0;
}

//!分析出data中的name和value
int GetNameValue( const char * data , const char * name,   char * value, const int value_len )
{
	const char *pStart;
	const char *pEnd;
	
	pStart = strstr( data, name);
	if( NULL == pStart)
	{
		return -1;
	}

	pStart += strlen(name);
	
	pStart = strchr(pStart, ':');
	if(pStart == NULL)
	{
		tracepoint();
		return -1;
	}

	pStart++;
	while( *pStart == ' ' ) { pStart++; }

	pEnd = strstr(pStart, "\r\n");
	if( pEnd == NULL )
	{
		tracepoint();
		return -2;
	}

	if( (pEnd - pStart) >= value_len )
	{
		tracepoint();
		return -3;
	}

	memcpy( value, pStart,  (pEnd - pStart) );
	value[pEnd - pStart] = 0;
	return 0;
}

int CDlgNtpCli::SaveNtpCfg( char * pBuf )
{
	trace("SaveNtpCfg>: %s\n", pBuf);
	
	CONFIG_NTP_PROTO_SET tmp_ntp_cfg;
	memset(&tmp_ntp_cfg, 0 , sizeof(CONFIG_NTP_PROTO_SET));
	char value[256] = {0};

	memset( &tmp_ntp_cfg, 0, sizeof(tmp_ntp_cfg) );

	if( GetNameValue(pBuf, "Enable",  value, sizeof(value) ) < 0 )
	{
		tracepoint();
		return -1;
	}
	tmp_ntp_cfg.isEnable = (strcmp("true", value) == 0)?1:0;
	//printf("value[%d],  tmp_ntp_cfg.isEnable\n", value, tmp_ntp_cfg.isEnable);
	
	if( GetNameValue(pBuf, "Host",  value, sizeof(value) ) < 0 )
	{
		tracepoint();
		return -1;
	}
	strcpy( tmp_ntp_cfg.HostIP, value);

	if( GetNameValue(pBuf, "Port",  value, sizeof(value) ) < 0 )
	{
		tracepoint();
		return -1;
	}
	tmp_ntp_cfg.unHostPort = (unsigned short)NetStr2Int(value);

	if( GetNameValue(pBuf, "UpdateInterval",  value, sizeof(value) ) < 0 )
	{
		tracepoint();
		return -1;
	}
	tmp_ntp_cfg.nUpdateInterval= NetStr2Int(value);

	if( GetNameValue(pBuf, "TimeZone",  value, sizeof(value) ) < 0 )
	{
		tracepoint();
		return -1;
	}
	tmp_ntp_cfg.iTimeZone= NetStr2Int(value);	


	CConfigNetNTP *pCfgNetNtp = new CConfigNetNTP();
	pCfgNetNtp->update();
	CONFIG_NET_NTP& cfgNtp = pCfgNetNtp->getConfig();
	cfgNtp.Enable = tmp_ntp_cfg.isEnable;
	strcpy(cfgNtp.Server.ServerName, tmp_ntp_cfg.HostIP);
	cfgNtp.TimeZone = tmp_ntp_cfg.iTimeZone;
	cfgNtp.UpdatePeriod = tmp_ntp_cfg.nUpdateInterval;
	cfgNtp.Server.Port = tmp_ntp_cfg.unHostPort;
	pCfgNetNtp->commit();
	delete pCfgNetNtp;
	
	return 0;
}
int CDlgNtpCli::GetCurrentTimeStamp()
{
	CGuard guard(m_Mutex);

	m_cCfgNtp.update();
	CONFIG_NET_NTP &cfgNtp = m_cCfgNtp.getConfig();
	
#if	defined(IPC_JZ)|| defined(IPC_JZ_NEW)|| defined(IPC_P2)
    struct timezone tz;
    struct timeval  tv;
	gettimeofday(&tv, &tz);


	if(VD_TRUE == cfgNtp.DaylightTime.Enable &&
	   tv.tv_sec >= cfgNtp.DaylightTime.StartTime && 
	   tv.tv_sec <= cfgNtp.DaylightTime.EndTime){
		 tv.tv_sec += 3600;
	}
	
	tv.tv_sec  += (cfgNtp.TimeZone * 3600)/10;

    return tv.tv_sec ;
#else
	SYSTEM_TIME szSystemTime;
	struct tm *ptmTime;	
				
	time(&m_NtpUpdataTime);
	if(VD_TRUE == DaylightStart){
		m_NtpUpdataTime -= 3600;
	}
		
	if(VD_TRUE == cfgNtp.DaylightTime.DaylightTime &&
	   m_NtpUpdataTime >= cfgNtp.DaylightTime.StartTime && 
	   m_NtpUpdataTime <= cfgNtp.DaylightTime.EndTime){
		m_NtpUpdataTime += 3600;
	}

	m_NtpUpdataTime  += ((cfgNtp.TimeZone*3600)/10);

	return m_NtpUpdataTime;
#endif	
}
#endif

