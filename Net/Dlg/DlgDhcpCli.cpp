
#ifdef WIN32
  #pragma warning( disable : 4786)
#endif

#ifdef _NET_USE_DHCPCLIENT/*是否使用dhcp*/

#include "Net/NetWorkService.h"
#include "Net/Dlg/DlgDhcpCli.h"
#include "Net/NetApp.h"

#include "Net/INet/NetInterFace.h"
#include "APIs/Net.h"

#ifndef ASSERT
#define ASSERT(x) \
	if(!(x)) \
	printf("ASSERT: %s,%d\n",__FILE__,__LINE__);
#endif

static char ETH_NAME[DHCP_MAX_NUM][8] = {"eth0", "wlan0"};

PATTERN_SINGLETON_IMPLEMENT(CDlgDhcpCli);

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CDlgDhcpCli::CDlgDhcpCli() :CThread("DhcpCli", TP_NET)
,m_iEnable(0),m_iWifiStatus(0)
{
	trace("CDlgDhcpCli Enter--------\n");
	for(int i = 0; i < DHCP_MAX_NUM; i++)
	{
		m_dhcp_protocol[i] = NULL;
	}

}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
CDlgDhcpCli::~CDlgDhcpCli()
{	//delete m_dhcp_protocol;
	std::vector<CDhcpProtocol*>::iterator con_iterator;
	for(int i = 0; i < DHCP_MAX_NUM; i++)
	{
		delete m_dhcp_protocol[i];
	}

}

/*	$FXN :	Start()
==	======================================================================
==	$DSC :	Dhcp功能的启动函数，创建了线程
==	$ARG :	
==		 :	
==	$RET :	TURE成功	 FALSE失败
==	$MOD :	
==	======================================================================
*/
VD_BOOL CDlgDhcpCli::Start()
{
	if (m_bLoop)
	{
		return TRUE;
	}

	NetGetDhcpEnable(&m_iEnable);
	
	int ret = CreateThread();

	return ret;
}
/*	$FXN :	ThreadProc()
==	======================================================================
==	$DSC :	Start()函数创建的线程的执行体
==	$ARG :	
==		 :	
==	$RET :
==	$MOD :
==	======================================================================
*/
void CDlgDhcpCli::ThreadProc()
{
	int iret = -1;
	int iCurDhcp = 0; //当前DHCP的网卡号，多网卡时只有一个网卡可以使用DHCP功能

	trace("dhcp>: start loop.......\n");

	m_netConfig.update();
	
	m_dhcp_protocol[0] = new CDhcpProtocol(ETH_NAME[0], m_netConfig.getConfig().HostName);
	m_dhcp_protocol[1] = new CDhcpProtocol(ETH_NAME[1], m_netConfig.getConfig().HostName);

	while (m_bLoop)
	{
		NetGetDhcpEnable(&m_iEnable);
		for(int i = 0; i < DHCP_MAX_NUM; i++)
		{
			//eth0 暂时不考虑
		    //使能配置暂时不考虑
			if (i == 0)
			continue;
			if(0 == m_iWifiStatus)
			{
				if(NULL != m_dhcp_protocol[1])
				{
					delete m_dhcp_protocol[1];
					m_dhcp_protocol[1] = NULL;
				}
				sleep(3);
				_printd("wait wifi connect successful!!!");
				continue;
			}
			if(NULL == m_dhcp_protocol[1])
			{				
				m_dhcp_protocol[1] = new CDhcpProtocol(ETH_NAME[1], m_netConfig.getConfig().HostName);
			}
			#if 0 
			if( !(m_iEnable >> i & 0x01) )
			{
				//!dhcp 尚未使能
				if( m_dhcp_protocol[i]->DhcpGetValid() )
				{
					m_dhcp_protocol[i]->DhcpRelease();
				}
				if (i == iCurDhcp)
				{
					g_NetApp.OnSetDHCP(FALSE);
				}
				continue;
			}
			#endif
			//dhcp 使能开启
			iret = OpenDhcp(m_dhcp_protocol[i]);
			_printd("ETH:%s========%d=============",ETH_NAME[i],iret);
			if(iret >= 0)
			{
				DHCP_PROTO_CONFIG szDhcpCfg = {0};
				IPInfo szIpInfo = {0};
				IPInfo szCurIpInfo = {0};
				char strHostIp[IP4_STR_LEN+1]={0};
				
				if(LoadDHCPConfig(&szDhcpCfg, ETH_NAME[i]))
				{
					Ip2Str(szDhcpCfg.my_ip, strHostIp);
					_printd("\n\nDHCP=====IP:%s\n\n\n", strHostIp);
					continue;
				}
				szIpInfo.HostIP.l = szDhcpCfg.my_ip;
				szIpInfo.SubMask.l = szDhcpCfg.my_netmask;
				szIpInfo.GateWay.l = szDhcpCfg.my_gateway;
				szIpInfo.iDHCP = szDhcpCfg.isUseDhcp;
				Ip2Str(szDhcpCfg.my_ip, strHostIp);
				_printd("DHCP=====IP:%s", strHostIp);
				
				g_NetApp.GetNetIPInfo(ETH_NAME[i], &szCurIpInfo);
				if(0 != szCurIpInfo.HostIP.l && ((szCurIpInfo.HostIP.l != szCurIpInfo.HostIP.l )
					|| (szCurIpInfo.SubMask.l !=  szIpInfo.SubMask.l)
					|| (szCurIpInfo.GateWay.l !=  szIpInfo.GateWay.l)))
				{
					//IP信息 相同就不设置IP
					//g_NetApp.OnSetDHCP(TRUE, &szIpInfo, ETH_NAME[i]); //delete langzi 2010-3-24
					//g_NetApp.OnSetDHCP(TRUE, &szIpInfo, ETH_NAME[i], &szDhcpCfg.server_DNS); //addd langzi 2010-3-24
				}	
	
			}			
			else
			{
				//g_NetApp.OnSetDHCP(FALSE);
			}
			iCurDhcp = i;
		}
		SystemSleep(5000);
	}
}

int CDlgDhcpCli::OpenDhcp(CDhcpProtocol * const pdhcp_in)
{
	int iret = -1;
	unsigned long ip, mask, gateway;
	unsigned long dns_ip; //add langzi 2010-3-24
	char strIP[16]={""};
	char strMask[16]={""};
	char strGateway[16]={""};
	char strDnsIP[16]={""}; //add langzi 2010-3-24

	_printd("pdhcp_in->DhcpGetValid()[%d]", pdhcp_in->DhcpGetValid());
	if ( pdhcp_in->DhcpGetValid() )
	{
		iret = pdhcp_in->DhcpTestValid();
		if(iret >= DHCP_OK) {SystemSleep(10000);}
		return 1;
    }
	else
	{
		_printd("get ip ");
		//iret = pdhcp_in->DhcpGetIP(ip, mask, gateway); //delect langzi 2010-3-24
		iret = pdhcp_in->DhcpGetIP(ip, mask, gateway, dns_ip); //add langzi 2010-3-24
		if(iret < DHCP_OK)
		{
			trace("CDlgDhcpCli>: dhcp get ip failed....5 second retry!\n");
			return -1;
		}
	}

	Ip2Str(ip, strIP);
	Ip2Str(mask, strMask);
	Ip2Str(gateway, strGateway);
	Ip2Str(dns_ip, strDnsIP); //add langzi 2010-3-24
	//Ip2Str(m_dhcpConfig->GetConfig().server_ip, strServerIp);
	_printd("strIP:%s",strIP);
	
	_printd("DHCPCLIENT: RequestIP[%s],NetMask[%s],GateWay[%s],SerVerIP[%s],LEASE[%u],DNS[%s]\n", //add langzi 2010-3-24
		strIP,strMask,strGateway,/*strServerIp*/"test",/*m_dhcpConfig->GetConfig(i).lease*/0, strDnsIP);

    if ( (ip & mask != gateway& mask) || (gateway == 0) || (gateway == 0xffffffff) ) 
	{/*冲突 ，如果主机地址与网关地址不匹配时, 不更改设置*/
		_printd("Get IP NetMask GateWay Error !\n");
		pdhcp_in->DhcpRelease();
		return -2;	
	}	

	return 0;
}
int CDlgDhcpCli::SaveDHCPConfig(DHCP_PROTO_CONFIG *pDhcpProtoSet, const char *pEthName)
{
	return SaveDhcp(pEthName, pDhcpProtoSet->isUseDhcp);
}

int CDlgDhcpCli::LoadDHCPConfig(DHCP_PROTO_CONFIG *pDhcpProtoSet, const char *pEthName)
{

	return LoadDhcp(pEthName, pDhcpProtoSet);
}

int CDlgDhcpCli::SaveDhcp(const char *eth_name, bool enable )
{
	VD_BOOL finded = FALSE;
	int iEnable = 0;
	int i;

	if(NULL == eth_name)
	{
		trace("eth_name can't be NULL\n");
		return -1;
	}

	for(i = 0; i < DHCP_MAX_NUM; i++)
	{
		if( 0 == strcmp(ETH_NAME[i], eth_name) )
		{
			finded = TRUE;
			iEnable = (enable) ? (0x1 >> i) : (0);
			break;
		}
	}

	trace("name[%s], enable[%d], i[%d]\n", eth_name, iEnable, i);

	if(finded)
	{
		//只能有一块网卡处于DHCP状态
		NetSetDhcpEnable(iEnable);
	}
	else
	{
		trace("can't setting eth[%s]\n", eth_name);
		return -1;
	}

	return 0;
}

int CDlgDhcpCli::LoadDhcp(const char *eth_name, DHCP_PROTO_CONFIG *pDhcpProtoSet)
{
	int iret, i, enable = 0;
	if(NULL == eth_name || pDhcpProtoSet == NULL)
	{
		trace("eth_name or pDhcpProtoSet can't be NULL\n");
		return -1;
	}

	NetGetDhcpEnable(&enable);
	for(i = 0; i < DHCP_MAX_NUM; i++)
	{
		//trace("==>i[%d], enable[%d]\n", i, enable);
		#if 0
		if(!((enable >> i) & 0x01))
		{
			continue;
		}
		#endif
		if( 0 == strcmp(ETH_NAME[i], eth_name) )
		{
			if( m_dhcp_protocol[i]->DhcpGetValid() )
			{
				iret = m_dhcp_protocol[i]->DhcpGetIP(pDhcpProtoSet->my_ip, pDhcpProtoSet->my_netmask, 
//					pDhcpProtoSet->my_gateway);  //delect langzi 2010-3-24
					pDhcpProtoSet->my_gateway, pDhcpProtoSet->server_DNS);  //add langzi 2010-3-24 增加DNS
				ASSERT(iret>=0);
				pDhcpProtoSet->isUseDhcp = TRUE;
				return 0;
			}
			else
			{
				trace("dhcp can't getting ip, try for a moment\n");
				return -1;
			}			
		}
		else
		{
			tracepoint();
		}
	}

	return -1;
}

int CDlgDhcpCli::SaveDhcpStrNet(const char * dhcp_str_in )
{
	if( NULL == dhcp_str_in)
	{
		return -1;
	}

	int enable = 0;
	CParse tmp_parse;
	tmp_parse.setSpliter("&&");
	tmp_parse.setTrim(true);
	tmp_parse.Parse(dhcp_str_in);

	if(tmp_parse.Size() > DHCP_MAX_NUM)
	{
		trace("wrong dhcp str net [%s]\n", dhcp_str_in);
		return -2;
	}

	for(int i = 0; i < tmp_parse.Size(); i++ )
	{
		CParse tmp_dhcp;
		tmp_dhcp.setSpliter("::");
		tmp_dhcp.setTrim(true);
		tmp_dhcp.Parse( tmp_parse.getWord(i) );

		for(int j = 0; j < DHCP_MAX_NUM; j++)
		{
			if( 0 == strcmp(ETH_NAME[i], tmp_dhcp.getWord(0).c_str() ) )
			{
				enable |= atoi(tmp_dhcp.getWord(1).c_str()) << i;
				break;
			}
		}
	}

	return NetSetDhcpEnable(enable);
}

int CDlgDhcpCli::LoadDhcpStrNet(char * dhcp_str_out , const unsigned int len)
{
	int i, enable = 0;
	char str_temp[250] = {0};

	NetGetDhcpEnable(&enable);
	for(i = 0; i < DHCP_MAX_NUM; i++)
	{
		if(i > 0)
		{
			ez_snprintf(str_temp+strlen(str_temp), sizeof(str_temp)-strlen(str_temp), "&&%s::%d", ETH_NAME[i], 
				(enable >> i & 0x01));
			continue;
		}
		ez_snprintf(str_temp+strlen(str_temp), sizeof(str_temp)-strlen(str_temp), "%s::%d", ETH_NAME[i], 
			(enable >> i & 0x01));
	}

	if(strlen(str_temp) >= len)
	{
		trace("len is too short\n");
		return -2;
	}

	trace("CDlgDhcpCli::LoadDhcpStrNet-->[%s]\n", str_temp);
	strcpy(dhcp_str_out, str_temp);

	return 0;
}

#endif


