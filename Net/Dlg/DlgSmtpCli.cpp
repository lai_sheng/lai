
#ifdef _USE_SMTP_MODULE 

#include <iostream>

#include "System/BaseTypedef.h"
#include "System/Locales.h"

#include "ez_libs/ez_socket/ez_socket.h"
#include "VDtinyRFC/smtp/DHSmtpApi.h"
#include "ez_libs/ezutil/Unicode.h"

#include "System/Parse.h" /*paser*/
//#include "Net/Operation.h"
#include "Net/Dlg/DlgSmtpCli.h"
#include "APIs/Net.h"

#include "Configs/ConfigConversion.h"

#include "Functions/Snap.h"

#include "Csmtp/CSmtpExport.h"

PATTERN_SINGLETON_IMPLEMENT(CDlgSmtpCli);

/*!
    \b Description        :    CDlgSmtpCli()构造函数，进行变量初始化\n
    \b Revisions        :    
*/
CDlgSmtpCli::CDlgSmtpCli()    : CThread("SMTP", TP_DEFAULT, 64)
,pSnapBuffer(NULL)
{
    trace("CDlgSmtpCli::CSmtpClient()>>>>>>>>>\n");
    m_bUpdateSmtpConfig = TRUE;
    m_bTest = FALSE;

    /*防止过度频繁邮件*/
    memset(m_iLastSendTime,0,sizeof(m_iLastSendTime));
    memset(m_HaveSendMask,0,sizeof(m_HaveSendMask));
    memset(m_OldSendMask,0,sizeof(m_OldSendMask));

    p_m_snap_smtp_queue = new CSmtpQueue(64);
}

/*!
    \b Description        :    ~CDlgSmtpCli()析构函数\n
    \b Revisions        :    
*/
CDlgSmtpCli::~CDlgSmtpCli()
{
    if (p_m_snap_smtp_queue)
    {
        delete p_m_snap_smtp_queue;
    }
}

/*!
    \b Description        :    Start()线程启动，绑定报警回调函数\n
    \b Revisions        :    
*/
VD_BOOL CDlgSmtpCli::Start(void)
{
    VD_BOOL bRet = FALSE;

    trace("CDlgSmtpCli::Start()>>>>>>>>>>>>>>>>>>>>>\n");

    // 创建发送邮件线程
    bRet = CreateThread();
    if (TRUE != bRet)
    {
        trace("CDlgSmtpCli::Start -- Create thread error!\n");
        return FALSE;
    }

    //向事件中心注册处理类
    CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CDlgSmtpCli::onAppEvent);
    m_CCfgNetServers.update();
    m_Config = m_CCfgNetServers.getConfig();
    m_CCfgNetServers.attach(this,(TCONFIG_PROC)&CDlgSmtpCli::OnNetConfigMail);
    return TRUE;
}

VD_BOOL CDlgSmtpCli::Stop(void)
{
    trace("CDlgSmtpCli::Stop()>>>>>>>>>>>>>>>>>>>>>\n");
    DestroyThread();

    m_CCfgNetServers.detach(this,(TCONFIG_PROC)&CDlgSmtpCli::OnNetConfigMail);
    CAppEventManager::instance()->detach(this, (SIG_EVENT::SigProc)&CDlgSmtpCli::onAppEvent);    

    return TRUE;
}
/*!
    \b Description        :    TestSendMail()测试配置是否正确，是否可以发邮件\n
    \b Argument            :    CONFIG_SMTP *pConfig
    \paramp        Config    :    测试的smtp配置
    \return        SUCCESS_RET：成功        FAILURE_RET：失败

    \b Revisions        :    
*/
int CDlgSmtpCli::TestSendMail(CONFIG_NET_EMAIL *pConfig)
{
    CGuard tmpGuard(m_Mutex);
    if (!m_bTest)
    {
        m_TestConfig = *pConfig;
        m_bTest = true;
        return SUCCESS_RET;
    }

    return FAILURE_RET;
}

int CDlgSmtpCli::ToString(char *data, int iSize,CONFIG_NET_EMAIL *cfg)
{
    int iLen = 0;
    char strIp[16]={""};
    Ip2Str(cfg->Server.ip.l,strIp);

    // 地址
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s:%d&&", 
                    cfg->Server.ServerName, cfg->Server.Port);

    // 收件人
    for (int i = 0; i < 3; ++i)
    {
        if (i < 3)
        {
            if( 0 == strcmp(cfg->Recievers[i], "none") )
            {
                iLen += ez_snprintf(data + iLen, iSize - iLen, "&&");
            }
            else
            {
            iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                            cfg->Recievers[i]);
            }
        }
        else
        {
            iLen += ez_snprintf(data + iLen, iSize - iLen, "&&");
        }
    }

    // 发件人
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                    cfg->SendAddr);
    
    // 用户名密码
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                    cfg->Server.UserName);
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                    cfg->Server.Password);

    // 主题
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                    cfg->Title);
    
    // 邮件体
    iLen += ez_snprintf(data + iLen, iSize - iLen, "&&");

    // 附件名称
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%s&&", 
                    "smtptest.txt");

    // 附件大小
    iLen += ez_snprintf(data + iLen, iSize - iLen, "0&&");

    //加密方式
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%d&&", cfg->bUseSSL);

    //加密使能
    iLen += ez_snprintf(data + iLen, iSize - iLen, "%d", cfg->Enable);
    return iLen;
}

int CDlgSmtpCli::ParseString(const char *data, int iSize, CONFIG_NET_EMAIL *cfg)
{
    char *pstr = new char[iSize + 1];

    strncpy(pstr, data, iSize);
    pstr[iSize] = '\0';

    CParse parse;
    parse.setSpliter("&&");        // 第一次分解
    parse.Parse((const char *)pstr);
    //parse.Parse(str);
    if (parse.Size() < 7)
    {
        delete pstr;
        return FAILURE_RET;
    }
    
    trace("Set MailCFG:\n[%s]\n", data);
    std::string strAddress;
    strAddress = parse.getWord(0);/*ip:port*/
    
    for (int i = 0; i < 3; ++i)/*recipient*/
    {
        if( parse.getWord(i + 1).length() )
        {
            memcpy(cfg->Recievers[i], parse.getWord(i + 1).c_str(), MIN(parse.getWord(i + 1).length(), EMAIL_ADDR_LEN-1) );
        }
        else
        {
            memset( cfg->Recievers[i], 0, 64*3 );
            memcpy( cfg->Recievers[i], "none@xxx.com", strlen("none@xxx.com") );
        }
        trace("[mail recver[%d:%s]]\n", i, cfg->Recievers[i]);
    }

    memset(cfg->SendAddr, 0, sizeof(cfg->SendAddr));
    memcpy(cfg->SendAddr, parse.getWord(4).c_str(), MIN(parse.getWord(4).length(), sizeof(cfg->SendAddr)-1) ); /*sender*/

    memset(cfg->Server.UserName, 0, sizeof(cfg->Server.UserName));
    memcpy(cfg->Server.UserName, parse.getWord(5).c_str(), MIN(parse.getWord(5).length(), sizeof(cfg->Server.UserName)-1) );   /*username*/

    memset(cfg->Server.Password, 0, sizeof(cfg->Server.Password    ) );
    memcpy(cfg->Server.Password    , parse.getWord(6).c_str(), MIN(parse.getWord(6).length(), sizeof(cfg->Server.Password    )-1) );    /*password*/

    memset(cfg->Title, 0, sizeof(cfg->Title));
    memcpy(cfg->Title, parse.getWord(7).c_str(), MIN(parse.getWord(7).length(), sizeof(cfg->Title)-1) ); /*subject*/

    cfg->bUseSSL = atoi(parse.getWord(11).c_str());
    cfg->Enable = atoi(parse.getWord(12).c_str());

    parse.setSpliter("|");        // 第二次分解
    parse.Parse(strAddress);
    std::string strFirstAddress;
    strFirstAddress = parse.getWord(0);

    parse.setSpliter(":");        // 第三次分解
    parse.Parse(strFirstAddress);
    //cfg->Server.ip.l = (uint)Str2Ip(parse.getWord(0).c_str());
    strcpy( cfg->Server.ServerName, parse.getWord(0).c_str() );
    cfg->Server.Port = atoi(parse.getWord(1).c_str());

    delete pstr;
    return SUCCESS_RET;
}


/*事件处理*/
void CDlgSmtpCli::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
    if(!param)
    {
        if ( !(appEventAll == code ) )
        {
            return;
        }
    }

    switch(action) 
    {
    case appEventStart:
        if (param->bMail)
        {
            VD_SendMessage(index,code,action);/*通道，类型，事件*/
        }
        break;
    case appEventStop:
        if (param->bMail)
        {
            //动态检测事件，硬盘空间不足，无盘可写只在事件开始时发送邮件，结束时不发邮件
            if ((code != appEventVideoMotion) && (code != appEventStorageLowSpace)
                && (code != appEventStorageFailure) && (code != appEventStorageNotExist))
            {
                VD_SendMessage(index,code,action);/*通道，类型，事件*/
            }
        }
        break;
    case appEventLatch:
        break;
    default:
        trace("appEventUnknown\n");
        break;
    }
}

/*!
    \b Description        :    ThreadProc(void)线程执行主体函数\n
    \return                

    \b Revisions        :    
*/
void CDlgSmtpCli::ThreadProc(void)
{
    while (m_bLoop)
    {
        //以阻塞方式接收消息
        Process();
        //SystemSleep(1000);
    }
}

/*!
    \b Description        :    Process()刷新配置，邮件收到报警（或测试），发送邮件\n
    \return                

    \b Revisions        :    
*/
void CDlgSmtpCli::Process()
{
    // 测试SMTP配置
    //OnTestSendMail();

    // 读取报警信息, 并发送邮件
    OnHandleAlarm();
}

/*!
    \b Description        :    OnTestSendMail()测试发邮件，检查配置\n
    \return                

    \b Revisions        :    
*/
int CDlgSmtpCli::OnTestSendMail(CONFIG_NET_EMAIL *pConfig)
{
    CGuard tmpGuard(m_Mutex);
    int iRet = 0;
    char AttachBuffer[]="abcdefghijklmnopqrstuvwxyz--JuFeng Tech";
    int BufferLen = strlen(AttachBuffer);

    iRet =  SendMailAttach(pConfig,0xFF, 0xFF,appEventStart,FALSE,"smtptest.txt",AttachBuffer,BufferLen);

    return iRet;
    // 标记为非测试状态, 等待下一次测试
  
}

/*!
    \b Description        :    OnHandleAlarm()处理报警发邮件，从消息队列去消息，然后生成信笺内容，发送邮件\n
    \return                

    \b Revisions        :    
*/
void CDlgSmtpCli::OnHandleAlarm()
{
    int result = 0;
    VD_MSG msg;
    memset(&msg, 0, sizeof(VD_MSG));
    if (FALSE == VD_RecvMessage(&msg, TRUE))
    {
        return ;
    }

    if( msg.wpa == appEventAll + 1 )
    {
        SMTP_SNAP_T tmp;
        VD_BOOL ret;
        ret = p_m_snap_smtp_queue->VD_RecvMessage( &tmp, FALSE) ;
        assert( ret );

        result = DoSnap( &tmp );
        if( result < 0 )
        {
            //数据出错
            tracepoint();
            pSnapBuffer->Release( );
            pSnapBuffer = NULL;
        }
        else if( result > 0 )
        {
            SendSnap(tmp.strName);
            pSnapBuffer->Release( );
            pSnapBuffer = NULL;
        }

        tmp.pBuffer->Release();
        return;
    }
    
    if (m_Config.Enable)
    {
        char AttachBuffer[]="abcdefghijklmnopqrstuvwxyz--JuFeng";
        int BufferLen = strlen(AttachBuffer);
        SendMailAttach(&m_Config,msg.msg,msg.wpa,(appEventAction)msg.lpa,FALSE,"smtptest.txt",AttachBuffer,BufferLen);
    }
}


/************************************************************/

/*!
    \b Description        :    SendMailAttach()发送邮件\n
    \return            0成功，1失败

    \b Revisions        :    
*/
int CDlgSmtpCli::SendMailAttach( CONFIG_NET_EMAIL *pConfigSmtp,
                                int iChannel, unsigned int iAlarmType,appEventAction action,
                                VD_BOOL bHaveAttach,char *strAttachname,
                                char *AttachBuffer,int BufferLen )

{

	ENU_MAIL_EnMode EncryptionMode = ENU_MAIL_EnMode_NULL;

	if (pConfigSmtp->bUseSSL) {
		EncryptionMode = ENU_MAIL_EnMode_SSL;
	}else{
		EncryptionMode = ENU_MAIL_EnMode_NULL;
	}

    //增加邮件主题
    /* 邮件标题库内认为输入的是澹清Ｂ２３１２，进行了一次GB2312到UTF8的转换 */
    /* 这里送入Gb2312格式 ,先不改库*/
    char acTitle[MAX_EMAIL_TITLE_LEN];
    memset(acTitle,0,MAX_EMAIL_TITLE_LEN);
    utf8TGb2312((unsigned char*)pConfigSmtp->Title,acTitle,MAX_EMAIL_TITLE_LEN,MAX_EMAIL_TITLE_LEN);
	
    //增加邮件接收者
    int iCount = 0;
	char * mailto[3] = {NULL,NULL,NULL};
    for (iCount = 0; iCount < 3; iCount++)
    {
        trace("[mail recver[%d:%s]]\n", iCount, pConfigSmtp->Recievers[iCount]);

		if ( 0 == strlen(pConfigSmtp->Recievers[iCount]))
		{
			printf("Recivers [%d] is NULL [%s]\n",iCount, pConfigSmtp->Recievers[iCount]);
			break;
		}
		
		if ( strcmp(pConfigSmtp->Recievers[iCount], "none@xx.com") 
            || strcmp(pConfigSmtp->Recievers[iCount], "none@xxx.com") )
        {
	  		mailto[iCount] = pConfigSmtp->Recievers[iCount];		
			printf("Add Recivers [%d] is [%s]\n",iCount, mailto[iCount]);

        }
    }

    char                 m_szMail[SMTP_MAIL_SIZE + 1];             /*!< 邮件体缓冲 */
    GetMailText( iChannel, action, iAlarmType, m_szMail, SMTP_MAIL_SIZE + 1);    //取得待发送内容

    trace("smtp>: m_szMail = %s\n", m_szMail);

    //增加邮件内容
    if(pConfigSmtp->dwAccePic & BITMSK(iChannel) && action == 0)	
    {    
		//发送3张图片附件
		for(int times=3; times>0; times--)
		{
			//每张图片附件间隔3s
			//sleep(3);
			SystemSleep(5000);

			if(DoSnap(iChannel) >= 0)
			{
				SendMailSSL(pConfigSmtp->Server.ServerName,pConfigSmtp->Server.Port,  EncryptionMode,
								pConfigSmtp->SendAddr, mailto[0],
								mailto[1],  mailto[2], 
				                pConfigSmtp->Server.UserName, pConfigSmtp->Server.Password,
				                pConfigSmtp->Title,
				                m_szMail,
				                m_SnapName,
				                (char *)pSnapBuffer->GetBuffer(),
				                pSnapBuffer->GetLength());

				//每次添加完一张图片附件清空下图片缓存
				pSnapBuffer->Release( );
				pSnapBuffer = NULL;
			}
		}
    }
	
    return 0;
}

/*
 * Function:       GetMailText
 * Description:    取得想使用邮件发送的文本信息
 * Calls:          
 * Called By:      
 * Table Accessed: 
 * Table Updated:  
 * Input:          
 * Output:         
 * Return:         
 * Others:         
 */
int CDlgSmtpCli::GetMailText(  int iChannel, uint action, unsigned int iAlarmType, char * m_szMail, int buflen  )
{
#define TMP_BUF_LEN 64
    int iLen = 0;
    char strTmp[TMP_BUF_LEN+1]={""};
    memset(m_szMail, 0, buflen);
     // 邮件信体 -- 报警内容
    // 报警事件

    switch (iAlarmType)
    {
    case appEventAlarmLocal:
    {
        strncpy(strTmp,LOADSTR("appEventAlarmLocal"),TMP_BUF_LEN);
        break;
    }case appEventAlarmNet:
    {
        strncpy(strTmp,LOADSTR("appEventAlarmNet"),TMP_BUF_LEN);
        break;
    }
    case appEventVideoMotion:
    {
        strncpy(strTmp,LOADSTR("appEventVideoMotion"),TMP_BUF_LEN);
        break;
    }
    case appEventVideoLoss:
    {
        strncpy(strTmp,LOADSTR("appEventVideoLoss"),TMP_BUF_LEN);
        break;
    }
    case appEventVideoBlind:
    {
        strncpy(strTmp,LOADSTR("appEventVideoBlind"),TMP_BUF_LEN);
        break;
    }
    case appEventVideoTitle:
    {
        strncpy(strTmp,LOADSTR("appEventVideoTitle"),TMP_BUF_LEN);                
        break;
    }
    case appEventVideoSplit:
    {
        strncpy(strTmp,LOADSTR("appEventVideoSplit"),TMP_BUF_LEN);                
        
        break;
    }
    case appEventVideoTour:
    {
        strncpy(strTmp,LOADSTR("appEventVideoTour"),TMP_BUF_LEN);                        
        break;
    }
    case appEventNetAbort:
    {
        strncpy(strTmp,LOADSTR("appEventNetAbort"),TMP_BUF_LEN);    
        break;
    }
    case appEventStorageNotExist:
    {
        strncpy(strTmp,LOADSTR("appEventStorageNotExist"),TMP_BUF_LEN);            
        break;
    }
    case appEventStorageFailure:
    {
        strncpy(strTmp,LOADSTR("appEventStorageFailure"),TMP_BUF_LEN);            
        break;
    }
    case appEventStorageLowSpace:
    {
        strncpy(strTmp,LOADSTR("appEventStorageLowSpace"),TMP_BUF_LEN);
        break;    
    }
    case appEventEncoderError:
    {
        strncpy(strTmp,LOADSTR("appEventEncoderError"),TMP_BUF_LEN);
        break;    
    }
    case appEventCDNoSpace:
    {
        strncpy(strTmp,LOADSTR("appEventCDNoSpace"),TMP_BUF_LEN);
        break;    
    }
    case appEventComm:
    {
        strncpy(strTmp,LOADSTR("appEventComm"),TMP_BUF_LEN);        
        break;    
    }
    case appEventStorageReadErr:
    {
        strncpy(strTmp,LOADSTR("appEventStorageReadErr"),TMP_BUF_LEN);                
        break;    
    }
    case appEventStorageWriteErr:
    {
        strncpy(strTmp,LOADSTR("appEventStorageWriteErr"),TMP_BUF_LEN);
        break;    
    }
    case appEventNetArp:
    {
        strncpy(strTmp,LOADSTR("appEventNetArp"),TMP_BUF_LEN);        
        break;
    }
    default:
        memcpy(strTmp,"Mail Test",strlen("Mail Test"));
        break;
    }

    if ((iAlarmType != appEventVideoMotion) && (iAlarmType != appEventStorageLowSpace)
        && (iAlarmType != appEventStorageNotExist)&& (iAlarmType != appEventStorageFailure))
    {
        if (action== appEventStop)
        {
            if (strlen(LOADSTR("alarm_smtp.matterend"))+strlen(strTmp)  <= TMP_BUF_LEN)
            {
                strcat(strTmp,  LOADSTR("alarm_smtp.matterend"));
            }
            else
            {
                printf("CDlgSmtpCli::GetMailText():111 len too big %d > %d\r\n",
                    strlen(LOADSTR("alarm_smtp.matterend"))+strlen(strTmp),TMP_BUF_LEN);
            }
        }
        else
        {
            if (strlen(LOADSTR("alarm_smtp.matterbegin"))+strlen(strTmp)  <= TMP_BUF_LEN)
            {
                strcat(strTmp,  LOADSTR("alarm_smtp.matterbegin"));
            }
            else
            {
                printf("CDlgSmtpCli::GetMailText():222 len too large %d > %d\r\n",
                    strlen(LOADSTR("alarm_smtp.matterbegin"))+strlen(strTmp),TMP_BUF_LEN);
            }        
        }
    }
    iLen += ez_snprintf(m_szMail + iLen, buflen - iLen,
                     "%s: %s\r\n", LOADSTR("alarm_smtp.matter"),
                     strTmp);

    // 报警输入通道号
    iLen += ez_snprintf( m_szMail + iLen, buflen - iLen,
                     "%s: %d",LOADSTR("alarm_smtp.inchannel"), iChannel+1 );

    // 报警开始时间
    SYSTEM_TIME m_spantime;
    SystemGetCurrentTime(&m_spantime);
    iLen += ez_snprintf(m_szMail + iLen, buflen - iLen,
                     "\r\n%s: %d/%d/%d %d:%d:%d\r\n",LOADSTR("alarm_smtp.starttime"),
                     m_spantime.day, m_spantime.month, m_spantime.year, m_spantime.hour, m_spantime.minute, m_spantime.second);

    // 报警设备名称
    CONFIG_NET configNet;
    CConfigConversion cCfgCVS;
    cCfgCVS.LoadNetConfigOld(&configNet);
	
	unsigned char pUtf8[MAX_EMAIL_TITLE_LEN];
	memset(pUtf8,0,MAX_EMAIL_TITLE_LEN);
	Gb2312TUtf8(configNet.HostName,pUtf8, MAX_EMAIL_TITLE_LEN);
    //gethostname(strTmp, sizeof(strTmp));//该函数暂时未能实现，暂时使用设备的网络名作为报警邮件的名称
    
    iLen += ez_snprintf(m_szMail + iLen, buflen - iLen,
                     "%s: %s\r\n",LOADSTR("alarm_smtp.devicename"), pUtf8);

    // 报警IP地址
    char szHostIp[IP4_STR_LEN + 1] = { 0 };
    char szNetMask[IP4_STR_LEN + 1] = { 0 };
    NetGetHostIP(NULL, szHostIp, sizeof(szHostIp), szNetMask, sizeof(szNetMask));
    iLen += ez_snprintf(m_szMail + iLen, buflen - iLen,
                     "%s: %s\r\n",LOADSTR("alarm_smtp.senderIP"), szHostIp);
    return 1;
}

void CDlgSmtpCli::OnSnap(CPacket *pBuffer,char *strName, int iLen)
{
    SMTP_SNAP_T tmp;

    if( strlen( strName ) >= VD_MAX_PATH )
    {
        tracepoint();
        return ;
    }

    tmp.pBuffer = pBuffer;
    strcpy(tmp.strName, strName);
    tmp.iLen = iLen;

    pBuffer->AddRef( );

    if(! p_m_snap_smtp_queue->VD_SendMessage( &tmp ) )
    {
        pBuffer->Release( );
        return ;
    }

    if( !VD_SendMessage(0, appEventAll + 1) )
    {
        tracepoint();
    }
    
    return;
}
//zanglei added 0518
extern void releaseSnapRef(Snap_Data* pSnap);

int CDlgSmtpCli::DoSnap(int iChn )
{
   	uint iRet = 0;
	Snap_Data SnapData;
	memset(&SnapData,0,sizeof(Snap_Data));
	memset(m_SnapName,0,sizeof(m_SnapName));
	int iCount = 4;
	do
	{
		iRet = g_Snap.getSnapDataByDir(&SnapData,iChn,CSnapBuffer::snapForward);

		if(iRet == 0)
		{
			break;
		}
		else if(iRet < 0 ||iCount < 1)
		{
			return -1;
		}
		else if(iRet == 1)
		{
			SystemSleep(500);
			iCount--;
		}		

	}while(1);
	if(SnapData.iLength <= 0 )
	{
		return -1;
	}
	if( NULL == pSnapBuffer )
	{
	    pSnapBuffer = g_PacketManager.GetPacket(SnapData.iLength);
	    if( NULL == pSnapBuffer )
	    {
	        tracepoint();
	        return -1;
	    }
	    pSnapBuffer->SetLength(0);
	}
	for(int i=0;i < SnapData.iPktNum;i++)
	{
	    iRet = pSnapBuffer->PutBuffer(SnapData.pPkt[i]->GetBuffer(), SnapData.pPkt[i]->GetLength());
	    if( iRet < SnapData.pPkt[i]->GetLength() )
	    {
	        tracepoint();
	        return -1;
	    }
	}
	if(pSnapBuffer->GetLength() >=(uint)SnapData.iLength)
	{
		sprintf(m_SnapName,"%4d%02d%02d%02d%02d%02d_%d.jpg",SnapData.sys_Createtime.year,SnapData.sys_Createtime.month,
			SnapData.sys_Createtime.day,SnapData.sys_Createtime.hour,SnapData.sys_Createtime.minute,SnapData.sys_Createtime.second,iChn);
		releaseSnapRef(&SnapData);//zanglei added for free memory 20130518
	    	return 1;    //图片数据已取完整
	}
	return -1;
}

int CDlgSmtpCli::DoSnap( const SMTP_SNAP_T * tmp_in )
{
    uint ret = 0;
    
    if( NULL == pSnapBuffer )
    {
        pSnapBuffer = g_PacketManager.GetPacket(tmp_in->iLen);
        if( NULL == pSnapBuffer )
        {
            tracepoint();
            return -1;
        }
        pSnapBuffer->SetLength( 0 );
    }

    
    if( pSnapBuffer->GetLength() < tmp_in->iLen )
    {
        ret = pSnapBuffer->PutBuffer( tmp_in->pBuffer->GetBuffer(), tmp_in->pBuffer->GetLength() );
        if( ret < tmp_in->pBuffer->GetLength() )
        {
            tracepoint();
            return -1;
        }
    }

    if(  pSnapBuffer->GetLength() >= tmp_in->iLen )
    {
        return 1;    //图片数据已取完整
    }
    
    return 0;
}

int CDlgSmtpCli::SendSnap(const char * strName)
{
    if(!m_Config.Enable) return -1;

	ENU_MAIL_EnMode EncryptionMode = ENU_MAIL_EnMode_NULL;

	if (m_Config.bUseSSL) {
		EncryptionMode = ENU_MAIL_EnMode_SSL;
	}else{
		EncryptionMode = ENU_MAIL_EnMode_NULL;
	}

    int iCount = 0;
	char * mailto[3] = {NULL,NULL,NULL};
    for (iCount = 0; iCount < 3; iCount++)
    {
        trace("[mail recver[%d:%s]]\n", iCount,  m_Config.Recievers[iCount]);
        if (strcmp( m_Config.Recievers[iCount], "none") )
        {
	  		mailto[iCount] = m_Config.Recievers[iCount];		
        }
    }

#if 0
	SendMailSSL(m_Config.Server.ServerName,m_Config.Server.Port,  EncryptionMode,
					m_Config.SendAddr, mailto[0],
					mailto[1],  mailto[2], 
	                m_Config.Server.UserName, m_Config.Server.Password,
	                m_Config.Title, 
	                "test mail",
	                strName, 
	                (char *)pSnapBuffer->GetBuffer(),
	                pSnapBuffer->GetLength());
#endif
    return 0;
}

void CDlgSmtpCli::OnNetConfigMail(CConfigNetEmail* pNetConfigMail, int &ret)
{
    m_CCfgNetServers.update();
    m_Config = m_CCfgNetServers.getConfig();
}

#endif // _USE_SMTP_MODULE 

