#ifdef _NET_USE_DHCPCLIENT

#include "Net/Dlg/dhcp/DhcpProtocol.h"
#include <memory.h>
#include <time.h>
#include <unistd.h>

#define trace printf

CDhcpProtocol::CDhcpProtocol(const char * eth_name, const char * host_name)
:eth_name_valid(true)
,m_iRawsocket(-1)
,m_starttime(0)
,ip_valid(false)
{
	int ret;
	
	memset(&m_dhcp_client, 0, sizeof(m_dhcp_client));

	if(NULL != eth_name)
	{
		memset(m_eth_name, 0, sizeof(m_eth_name));
		strcpy(m_eth_name, eth_name);
		ret = Dhcp_InitSessionCtx( &(m_dhcp_client.dhcp_session_ctx), m_eth_name );		
		if(ret < 0)
		{
			eth_name_valid =false;
		}
	}

	if(NULL != host_name )
	{
		strcpy(m_dhcp_client.dhcp_host_name, host_name);
	}
	else
	{
		strcpy(m_dhcp_client.dhcp_host_name, "no_host_name");
	}

}

CDhcpProtocol::~CDhcpProtocol()
{
	DhcpRelease();
}

//int CDhcpProtocol::DhcpGetIP( unsigned long &ip_out, unsigned long& mask_out, unsigned long& gate_way_out ) //delete langzi 2010-3-24
int CDhcpProtocol::DhcpGetIP( unsigned long &ip_out, unsigned long& mask_out, unsigned long& gate_way_out, unsigned long &server_DNS ) //add langzi 2010-3-24 增加DNS
{
	int ret;
	
	TEST_ETH_NAME_VALID();
	trace("ip_valid[%d]", ip_valid);
	if(ip_valid)//! ip已经获取到
	{
		ip_out = m_dhcp_client.dhcp_proto_config.my_ip;
		mask_out = m_dhcp_client.dhcp_proto_config.my_netmask;
		gate_way_out = m_dhcp_client.dhcp_proto_config.my_gateway;
		server_DNS = m_dhcp_client.dhcp_proto_config.server_DNS; //add langzi 2010-3-24
		return DHCP_OK;
	}
	trace("m_iRawsocket[%d]", m_iRawsocket);
	if (m_iRawsocket<0)
	{
		m_iRawsocket = UdpRawSocket(m_dhcp_client.dhcp_session_ctx.IfIndex);
		if(m_iRawsocket<0)
		{
			trace("DHCP:Creat raw socket failed\n");
			return DHCP_ERROR;
		}
	}

	ret = DhcpClient(m_iRawsocket, &m_dhcp_client);
	if(ret < 0)
	{
		ret = DHCP_ERROR;
		m_starttime = 0;
		ip_valid = false;
	}
	else
	{
		ip_out = m_dhcp_client.dhcp_proto_config.my_ip;
		mask_out = m_dhcp_client.dhcp_proto_config.my_netmask;
		gate_way_out = m_dhcp_client.dhcp_proto_config.my_gateway;
		server_DNS = m_dhcp_client.dhcp_proto_config.server_DNS; //add langzi 2010-3-24
		time(&m_starttime);
		ip_valid = true;
		ret = DHCP_OK;
	}

	//close(m_iRawsocket);
    	//m_iRawsocket = -1;	
	return ret;
}

int CDhcpProtocol::DhcpTestValid()
{
	int ret;
	time_t now_time;
	
	TEST_ETH_NAME_VALID();

	if (m_iRawsocket<0)
	{
		m_iRawsocket = UdpRawSocket(m_dhcp_client.dhcp_session_ctx.IfIndex);
		if(m_iRawsocket<0)
		{
			trace("DHCP:Creat raw socket failed\n");
			return DHCP_ERROR;
		}
	}
	
	time(&now_time);
	time_t delta = now_time-m_starttime;
	if(delta > (time_t)m_dhcp_client.dhcp_proto_config.lease)
	{
		ip_valid = false;
		DHCP_Release( &m_dhcp_client );
		return DHCP_OUT_LEASE;
	}
	else if(delta > (time_t)m_dhcp_client.dhcp_proto_config.lease/2)
	{
		ret = DHCP_AddLease( m_iRawsocket, &m_dhcp_client);
		if(ret >= DHCP_OK)
		{
			time(&m_starttime);
		}
	}

	return DHCP_OK;
}

int CDhcpProtocol::DhcpRelease()
{
	TEST_ETH_NAME_VALID();
	
	if(ip_valid)
	{
		ip_valid = false;
		DHCP_Release( &m_dhcp_client );
		close(m_iRawsocket);
    	m_iRawsocket = -1;	
	}

	return DHCP_OK;
}

bool CDhcpProtocol::DhcpGetValid()
{
	TEST_ETH_NAME_VALID();
	
	return ip_valid;
}

int CDhcpProtocol::GetLease()
{
	TEST_ETH_NAME_VALID();
	
	return m_dhcp_client.dhcp_proto_config.lease;
}

#endif

