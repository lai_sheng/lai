
#include "Net/INet/NetInterFace.h"
#include "APIs/Net.h"
#include "Net/NetConfig.h"
#include "ez_libs/ez_socket/ez_socket.h"

int GET_MACADDR2(unsigned char *pHardwareId, int len)/*get Dvr Mac Address*/
{
#ifndef WIN32

#ifdef __uClinux_

    int *iRet = NULL;
    if ((getbenv("HWADDR0")) == NULL)
    {
        trace("ERROR:Cannot find in armboot\n");
        return -1;
    }
    strncpy(pHardwareId, iRet, len);
    return 0;
    
#else

    int iSockfd = -1;
    int iRet = -1;
    
    int temp = 0;
    int i = 0;
    int j = 0;
    int k = 0;
    
    struct ifreq ifr;
    
    char Macaddr[32] = {0};
    char MacTmp[32] = {0};

    iSockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (iSockfd < 0)
    {
        trace("Sockets_open error!\n");
		return -1;
    }

    strcpy(ifr.ifr_name, "eth0");
    iRet = ioctl(iSockfd, SIOCGIFHWADDR, &ifr);
    if (iRet < 0)
    {
        trace("Get HWADDR error!\n");
		close(iSockfd);
        return -1;
    }
    else
    {
        memcpy(MacTmp, ifr.ifr_hwaddr.sa_data, 32);
    }

    for(i = 0; i < 6; i ++) 
    {
        if (0 == MacTmp[i])
        {
            Macaddr[k] = '0';
            k++;
            Macaddr[k] = '0';
            k++;
            Macaddr[k] = ':';
            k++;
        }
        else
        {
            for (j = 0; j < 2; j++)
            {
                temp = (MacTmp[i] >> ((1 - j) * 4)) & 0x0f;
                if ((temp >= 0) && (temp <= 9))
                {
                    Macaddr[k] = temp + 0x30;
                    k++;
                }
                else if ((temp >= 10) && (temp <= 15))
                {
                    Macaddr[k] = temp + 0x57;
                    k++;
                }
                else
                {
                	close(iSockfd);
                    return -1;
                }
            }
            if (5 == i)
            {
                break;
            }
            Macaddr[k] = ':';
            k++;
        }
    }
    Macaddr[k] = '\0';
    
    memset(pHardwareId, 0, len);
	if( len < (int)sizeof(Macaddr) )
	{
		trace("CAutoSearchIP>:mac buf is samll!\n");
		close(iSockfd);
		return -1;
	}
    memcpy(pHardwareId,Macaddr,sizeof(Macaddr));
    
    close(iSockfd);
    return k;
    
#endif
#else
return -1;

#endif
}
/****************
//INet 接口
*****************/
PATTERN_SINGLETON_IMPLEMENT(CNetFactory);
CNetConfigInterFace * CNetFactory::CreateNet()
{
	static CNetConfigInterFace * m_instance = NULL;
	if(m_instance == NULL)
	{
		m_instance = new CConfigCenterNet();
	}
	
	return m_instance;
}

CConfigCenterNet::CConfigCenterNet()
{
	CConfigConversion CCfgCVS; //此类无成员变量，占用栈很小
	CCfgCVS.LoadNetConfigOld(&m_ConfigNet);
}

CConfigCenterNet::~CConfigCenterNet()
{
}
char * 	CConfigCenterNet::GetHostName( char * host_name, int& name_len )
{
	if( host_name == NULL )
	{
		return NULL;
	}
	strcpy( host_name, m_ConfigNet.HostName);

	name_len = strlen(host_name);

	return host_name;
}

CONFIG_NET * CConfigCenterNet::GetConfigNet( CONFIG_NET * pConfigNet_out, bool update )
{
	if( update )
	{
		CConfigConversion CCfgCVS; 
		CCfgCVS.LoadNetConfigOld(&m_ConfigNet);
	}

	if( NULL ==  pConfigNet_out )
	{
		CONFIG_NET * p = &m_ConfigNet;
		return p;
	}
	else
	{
		memcpy( pConfigNet_out, &m_ConfigNet, sizeof(CONFIG_NET) );
		return pConfigNet_out;
	}
}

int	CConfigCenterNet::SaveConfigNet( CONFIG_NET * pConfigNet_in )
{
	if( 0 == memcmp( pConfigNet_in, &m_ConfigNet, sizeof(CONFIG_NET) ) )
	{
		return -1;
	}

	memcpy( &m_ConfigNet, pConfigNet_in, sizeof(CONFIG_NET) );
	CConfigConversion CCfgCVS; 
	CCfgCVS.SaveNetConfigOld(&m_ConfigNet, -1);
	return 0;
}

int	CConfigCenterNet::GetMacByName(const char *pEthName, char *pEthMac, const int iEthMacLen)
{
	return GET_MACADDR2((unsigned char*)pEthMac, iEthMacLen);
}
int	CConfigCenterNet::Dump()
{
	char tmpStr[32];

	CONFIG_NET * m_pNetConfig;
	m_pNetConfig = GetConfigNet();

	trace("Net Config :::\n");
	trace("---------------\n");
	trace("\tHostName  : %s\n", m_pNetConfig->HostName);
	//trace("\tUseDHCP   : %d\n", m_bUseDHCP);
	Ip2Str(m_pNetConfig->HostIP.l, tmpStr);
	trace("\tHostIP    : %s\n", tmpStr);
	Ip2Str(m_pNetConfig->Submask.l, tmpStr);
	trace("\tSubmask   : %s\n", tmpStr);
	Ip2Str(m_pNetConfig->GateWayIP.l, tmpStr);
	trace("\tGateWay   : %s\n", tmpStr);
	trace("\tTCPPort   : %d\n", m_pNetConfig->TCPPort);
	trace("\tTCPMaxConn: %d\n", m_pNetConfig->TCPMaxConn);

	return 0;
}

int CConfigCenterNet::GetIpByName( const char * eth_name, char * ip_out, char * mask_out )
{
	int iRet;
	char strHostIp[15+1]={0};
	char strSubmask[15+1]={0};

	iRet = NetGetHostIP(eth_name, strHostIp,sizeof(strHostIp), strSubmask,sizeof(strSubmask));
	if(NULL != ip_out)
	{
		strcpy(ip_out, strHostIp);
	}

	if (NULL != mask_out)
	{
		strcpy(mask_out, strSubmask);
	}
	return iRet;
}

int	CConfigCenterNet::SetIpByName (const char * eth_name, const char * ip_in, const char * mask_in ) 
{
	int ret = 0;
	if( 
		(eth_name == NULL) 
		|| (0 == strcmp(eth_name, ""))
		|| (0 == strcmp(ip_in, ""))
		|| (0 == strcmp(mask_in, ""))
		)
	{
		return NI_FAIL;
	}

	if( 0 != strcmp(eth_name, "eth0") )
	{
		tracef("don't support [%s], only support eth0\n", eth_name );
		return NI_FAIL;
	}

	ret = NetSetHostIP(eth_name, ip_in, mask_in);
	return ret;
}

int	CConfigCenterNet::GetGateByName( const char * eth_name,  char * gate_out, int &gate_len ) 
{
	char eth_gate[16] = {0};
	int ret = 0;

	if( 
		(eth_name == NULL) 
		|| (0 == strcmp(eth_name, ""))
		|| (NULL == gate_out )
		)
	{
		return NI_FAIL;
	}
	
	if( 0 != strcmp(eth_name, "eth0") )
	{
		tracef("don't support [%s], only support eth0\n", eth_name );
		return NI_FAIL;
	}
	
	ret = NetGetGateway(eth_name, eth_gate, sizeof(eth_gate) );
	strcpy(gate_out, eth_gate);
	gate_len = strlen(eth_gate);
	return ret;
}
int	CConfigCenterNet::SetGateByName( const char * eth_name,  const char * gate_in, const int gate_len )
{
	int ret = 0;

	if( 
		(eth_name == NULL) 
		|| (0 == strcmp(eth_name, ""))
		|| (0 == strcmp(gate_in, ""))
		)
	{
		return NI_FAIL;
	}

	if( 0 != strcmp(eth_name, "eth0") )
	{
		tracef("don't support [%s], only support eth0\n", eth_name );
		return NI_FAIL;
	}

	NET_PPPOE_CONFIG ppoe_cfg;	
	NetGetPPPoEConfig(&ppoe_cfg);

	if (1 == ppoe_cfg.enable)		//!pppoe开启的情况下, 不允许修改网关地址
	{
		return NI_FAIL_PPPOE_ENABLE;
	}
	
	ret = NetSetGateway(eth_name, gate_in);
	return ret;	
}


bool CConfigCenterNet::GetNetCurrentHostIP(IPDEF *pCurHostIp, int *pError)
{
	if ((NULL == pCurHostIp) || (NULL == pError))
	{
		return false;
	}
	
	enum
	{
		MAX_IP_LENGTH = 16,
	};
	char strHostIp[MAX_IP_LENGTH] = {0, };
	char strNetMask[MAX_IP_LENGTH] = {0,};
	char strDialIp[MAX_IP_LENGTH] = {0, };
	
	NET_PPPOE_CONFIG ppoe_cfg;	
	NetGetPPPoEConfig(&ppoe_cfg);	
	
	if (1 == ppoe_cfg.enable)
	{
		/* PPPoE拨号使能，但是还没有获得IP时就等一会儿 */
		for (int iTry = 0; iTry < 20; iTry++)
		{
			if (0 != NetGetDialIpAddress(strDialIp))
			{
				SystemSleep(2 * 1000);
			}
			else
			{
				strncpy(strHostIp, strDialIp, MAX_IP_LENGTH);
				trace ("GetNetCurrentHostIP::ppoe dial-ip %s\n", strDialIp);
				break;
			}
		}		
	}
	else
	{
		if (0 != NetGetHostIP(NULL, strHostIp, MAX_IP_LENGTH, strNetMask, MAX_IP_LENGTH))
		{
			trace ("GetNetCurrentHostIP::Con't Get IP!\n");
			return false;
		}
	}
	pCurHostIp->l = Str2Ip(strHostIp);
	*pError = 0;
	
	return true;
}

