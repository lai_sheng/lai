#pragma once

#include "System/Object.h"

class CIVnPdu;
#include <map>

class CVNUser
{
public:
	CVNUser();
	~CVNUser();
	int ReqUserAuth(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int CtrlAuthSwitch(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);	
};


