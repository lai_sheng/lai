
#ifndef _IVNSYSCONFIG_H_
#define _IVNSYSCONFIG_H_


#include <map>

class ICaptureManager;
class CTlv;
class IVNSysConfig;

typedef int (IVNSysConfig::*TLV_SYSCFG_FUN)( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out);

typedef struct _tlv_sysconfig_proc
{
    TLV_SYSCFG_FUN pFuncGet;
    TLV_SYSCFG_FUN pFuncSet;
}tlv_sysconfig_proc;

class IVNSysConfig
{
public:
    IVNSysConfig();
    ~IVNSysConfig();

    int SetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
    int GetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

protected:
    int OprConfig(bool bGet, CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU );

    int GetSplitCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int SetSplitCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int GetTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int SetTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int GetOutOSDCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int SetOutOSDCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    
    int SetRecordCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int GetRecordCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QuerySnapCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModSnapCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryGeneralCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModGeneralCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryCurTimeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModCurTimeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryDSTCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModDSTCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
	
    int QueryNetAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModNetAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int GetTVCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int SetTVCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryAlarmInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryNetInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryMotionCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryLossCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryBindCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int ModAlarmInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModNetInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModMotionCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModLossCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModBindCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );


    int GetAlarmCfg(CTlv& tlv_in, CTlv& tlv_out,CONFIG_GENERIC_EVENT& cfg);
    
    int QueryNoDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryDiskErrCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryNetBrokenCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryIPConfictCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int SetAlarmCfg(CTlv& tlv_in, CTlv& tlv_out,CONFIG_GENERIC_EVENT& cfg);
    
    int ModNoDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModDiskErrCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModNetBrokenCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );    
    int ModIPConfictCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );    

    int QueryDiskFullCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModDiskFullCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryIntervideoCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModIntervideoCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryGeneralNetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModGeneralNetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryNetAPPCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModNetAPPCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );

    int QueryEncodeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModEncodeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryPtzPresetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModPTZPresetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryPtzTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModPtzTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int PtzControlRawData( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int QueryChnName(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
    int ModChnName(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

	int QueryLogCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );		
	int ModLogCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
	
	int QueryLogInfoCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

	int QueryLocalFilesCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

	int QueryGooLink( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModGooLink( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );	

    int QueryAudioCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
    int ModAudioCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

    int QueryLocalChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
    int ModLocalChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

    int QueryRemoteChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
    int ModRemoteChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);

    int QueryKBDConfig(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);
    int ModKBDConfig(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out);


#if defined (_USE_720P_MODULE_)
    int QueryCameraCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    int ModCameraCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );	
    int QueryCameraCfgExt( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
	int ModCameraCfgExt( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
#endif


#ifdef _2761_EXCEPTION_CHK_
int QueryEncoderDetectCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
int ModEncoderDetectCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
#endif

	int QueryManualSnap( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
	int ManualSnap( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out );
    typedef std::map<int, tlv_sysconfig_proc> MAP_LIST;
    MAP_LIST m_map;
private:
    ICaptureManager* m_pCaptureM;
	void* m_pConn;
};

#endif
