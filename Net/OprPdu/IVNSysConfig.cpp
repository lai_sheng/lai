#include "IVnPdu.h"
#include "APIs/Split.h"
#include "APIs/Net.h"
#include "APIs/Audio.h"
#include "Net/NetConfig.h"
#include "Net/NetApp.h"
#include "Net/NetCore.h"

#include "System/Tlv.h"
#include "System/AppConfig.h"
#include "System/User.h"
#include "Configs/ConfigMonitorTour.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigTVAdjust.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigVideoWidget.h"
#include "Configs/ConfigChannelTitle.h"
#include "Configs/ConfigVideoColor.h"
#include "Configs/ConfigCamera.h"
#include "Devices/DevSplit.h"
#include "Devices/DevAudioIn.h"
#include "IVNSysConfig.h"
#include "FilterHelper.h"
#include "Functions/General.h"
#include "WFS/adapt.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Intervideo/DevSearch/DevSearchGlobal.h"
extern INT WriteFacInfo(SAVENANDINFO_T *pInFacInfo);
extern INT ReadFacInfo(SAVENANDINFO_T *pOutFacInfo);

#include "Main.h"
#include "Functions/Snap.h"
#include "Functions/DriverManager.h"

#include "Configs/ConfigDigiChManager.h"
#include "Net/NetClient/NetCaptureManager.h"
#include "Net/NetWorkService.h"

//#include "Functions/Display.h"
#ifndef WIN32
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#endif

#include "Media/ICapture.h"

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
#include "Configs/ConfigPlay.h"
#include "APIs/Ide.h"
#endif


int CONFIG_MONITORTOUR_2_SPLIT_COMBINE(enum split_combine_t sct, CONFIG_MONITORTOUR& tour_in,
        SPLIT_COMBINE& combine_out);
int SPLIT_COMBINE_2_CONFIG_MONITORTOUR( SPLIT_COMBINE& combine_in,
        CONFIG_MONITORTOUR& tour_out);

extern int getImageBitRate(int imageSize, int frames, int &minBitRate, int&maxBitRate);

IVNSysConfig::IVNSysConfig()
{
    m_pCaptureM = ICaptureManager::instance();
    assert(m_pCaptureM);
    m_pConn = NULL;
    tlv_sysconfig_proc proc;
    {
        //!增加type-DEV_SYSTEM_INFO_DISPLAY_SPLIT
        proc.pFuncGet = &IVNSysConfig::GetSplitCfg;
        proc.pFuncSet = &IVNSysConfig::SetSplitCfg;

        m_map[DEV_SYSTEM_INFO_DISPLAY_SPLIT] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_INFO_DISPLAY_TOUR
        proc.pFuncGet = &IVNSysConfig::GetTourCfg;
        proc.pFuncSet = &IVNSysConfig::SetTourCfg;

        m_map[DEV_SYSTEM_INFO_DISPLAY_TOUR] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_VIDEO_OUT_OSD_CFG
        proc.pFuncGet = &IVNSysConfig::GetOutOSDCfg;
        proc.pFuncSet = &IVNSysConfig::SetOutOSDCfg;

        m_map[DEV_SYSTEM_VIDEO_OUT_OSD_CFG] = proc;
    }    
    {
        //!增加type-DEV_SYSTEM_RECCTL_CFG 
        proc.pFuncGet = &IVNSysConfig::GetRecordCfg;
        proc.pFuncSet = &IVNSysConfig::SetRecordCfg;

        m_map[DEV_SYSTEM_RECCTL_CFG] = proc;
    }    
    {
        //!增加type-ENCODE_SNAP_STREAM_TYPE
        proc.pFuncGet = &IVNSysConfig::QuerySnapCfg;
        proc.pFuncSet = &IVNSysConfig::ModSnapCfg;

        m_map[DEV_CONFIG_TYPE_SNAP] = proc;
    }
    {
        //!增加type-DEV_ALARM_DECODE_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryAlarmCfg;
        proc.pFuncSet = &IVNSysConfig::ModAlarmCfg;

        m_map[DEV_ALARM_DECODE_TYPE] = proc;
    }
    {
        //!增加type-DEV_ALARM_DECODE_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryNetAlarmCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAlarmCfg;

        m_map[DEV_ALARM_NET_ALARMIN] = proc;
    }
	
    {
        //!增加type-DEV_SYSTEM_INFO_VIDEOUT_CFG - TV输出
        proc.pFuncGet = &IVNSysConfig::GetTVCfg;
        proc.pFuncSet = &IVNSysConfig::SetTVCfg;

        m_map[DEV_SYSTEM_INFO_VIDEOUT_CFG] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_INFO_GENERAL
        proc.pFuncGet = &IVNSysConfig::QueryGeneralCfg;
        proc.pFuncSet = &IVNSysConfig::ModGeneralCfg;

        m_map[DEV_SYSTEM_INFO_GENERAL] = proc;
    }    
    {
        //!增加type-DEV_SYSTEM_CUR_TIME
        proc.pFuncGet = &IVNSysConfig::QueryCurTimeCfg;
        proc.pFuncSet = &IVNSysConfig::ModCurTimeCfg;

        m_map[DEV_SYSTEM_CUR_TIME] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_CUR_TIME
        proc.pFuncGet = &IVNSysConfig::QueryDSTCfg;
        proc.pFuncSet = &IVNSysConfig::ModDSTCfg;

        m_map[DEV_SYSTEM_DST_TIME] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_QUERYLOG
        proc.pFuncGet = &IVNSysConfig::QueryLogInfoCfg;

        m_map[DEV_SYSTEM_QUERYLOG] = proc;
    } 
    {
        //!增加type-DEV_SYSTEM_OPRLOG
        proc.pFuncGet = &IVNSysConfig::QueryLogCfg;
        proc.pFuncSet = &IVNSysConfig::ModLogCfg;

        m_map[DEV_SYSTEM_OPRLOG] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_OPRLOG
        proc.pFuncGet = &IVNSysConfig::QueryLocalFilesCfg;
        //proc.pFuncSet = &IVNSysConfig::ModLogCfg;

        m_map[DEV_SYSTEM_QUERY_REC_PIC] = proc;
    }
    {
        //!增加type-DEV_SYSTEM_INFO_DISPLAY_SPLIT
        proc.pFuncGet = &IVNSysConfig::QueryAlarmInCfg;
        proc.pFuncSet = &IVNSysConfig::ModAlarmInCfg;

        m_map[DEV_ALARM_ALARMIN_TYPE] = proc;
    }
/////////////////////////////////////////////////    
    {
        //!增加type-DEV_ALARM_LOSS_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryLossCfg;
        proc.pFuncSet = &IVNSysConfig::ModLossCfg;

        m_map[DEV_ALARM_LOSS_TYPE] = proc;
    }
    {
        //!增加type-DEV_ALARM_MOTION_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryMotionCfg;
        proc.pFuncSet = &IVNSysConfig::ModMotionCfg;

        m_map[DEV_ALARM_MOTION_TYPE] = proc;
    }
    {
        //!增加type-DEV_ALARM_BLIND_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryBindCfg;
        proc.pFuncSet = &IVNSysConfig::ModBindCfg;

        m_map[DEV_ALARM_BLIND_TYPE] = proc;
    }
 
    {
        //!增加type-DEV_ALARM_NODISK_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryNoDiskCfg;
        proc.pFuncSet = &IVNSysConfig::ModNoDiskCfg;

        m_map[DEV_ALARM_NODISK_TYPE] = proc;
    }
    {
        //!增加type-DEV_ALARM_DISKERROR_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryDiskErrCfg;
        proc.pFuncSet = &IVNSysConfig::ModDiskErrCfg;

        m_map[DEV_ALARM_DISKERROR_TYPE] = proc;
    }
    {
        //!增加type-DEV_ALARM_DISKFULL_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryDiskFullCfg;
        proc.pFuncSet = &IVNSysConfig::ModDiskFullCfg;

        m_map[DEV_ALARM_DISKFULL_TYPE] = proc;
    }    
    {
        //!增加type-DEV_ALARM_NETBROKEN_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryNetBrokenCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetBrokenCfg;

        m_map[DEV_ALARM_NETBROKEN_TYPE] = proc;
    }    
    {
        //!增加type-DEV_ALARM_IPCONFICT_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryIPConfictCfg;
        proc.pFuncSet = &IVNSysConfig::ModIPConfictCfg;

        m_map[DEV_ALARM_IPCONFICT_TYPE] = proc;
    }        
/////////////////////DISK//////////////////////////////
    {
        //!增加type-DEV_ALARM_IPCONFICT_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryDiskCfg;
        proc.pFuncSet = &IVNSysConfig::ModDiskCfg;

        m_map[DEV_SYSTEM_INFO_DISK_DRIVER] = proc;
    }    
/////////////////////INTERVIDEO//////////////////////////////
    {
        //!增加type-DEV_ALARM_IPCONFICT_TYPE
        proc.pFuncGet = &IVNSysConfig::QueryIntervideoCfg;
        proc.pFuncSet = &IVNSysConfig::ModIntervideoCfg;

        m_map[DEV_CONFIG_TYPE_INTERVIDEO] = proc;
    }
///////////////////////GeneralNet////////////////////////////////////
    {
        //!增加type-DEV_CONFIG_TYPE_NET
        proc.pFuncGet = &IVNSysConfig::QueryGeneralNetCfg;
        proc.pFuncSet = &IVNSysConfig::ModGeneralNetCfg;

        m_map[DEV_CONFIG_TYPE_NET] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_ETHERNET
        proc.pFuncGet = &IVNSysConfig::QueryGeneralNetCfg;
        proc.pFuncSet = &IVNSysConfig::ModGeneralNetCfg;

        m_map[DEV_CONFIG_TYPE_ETHERNET] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_DNS
        proc.pFuncGet = &IVNSysConfig::QueryGeneralNetCfg;
        proc.pFuncSet = &IVNSysConfig::ModGeneralNetCfg;

        m_map[DEV_CONFIG_TYPE_DNS] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_PPPOE
        proc.pFuncGet = &IVNSysConfig::QueryGeneralNetCfg;
        proc.pFuncSet = &IVNSysConfig::ModGeneralNetCfg;

        m_map[DEV_CONFIG_TYPE_PPPOE] = proc;
    }    
///////////////////////NetApp////////////////////////////////////
    {
        //!增加type-DEV_CONFIG_TYPE_MAIL
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_MAIL] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_NTP
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_NTP] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_UPNP
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_UPNP] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_MULTI_DDNS
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_MULTI_DDNS] = proc;
    }
    {
        //!增加type-DEV_CONFIG_TYPE_AUTO_REGISTER
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_AUTO_REGISTER] = proc;
    }   
    {
        //!增加type-DEV_CONFIG_TYPE_AUTOREG_EX
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CONFIG_TYPE_AUTOREG_EX] = proc;
    }      
    {
        //!增加type-DEV_CFG_TYPE_DECODE
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CFG_TYPE_DECODE] = proc;
    }
    {
        //!增加type-DEV_CFG_TYPE_DECODE_EX
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CFG_TYPE_DECODE_EX] = proc;
    }
//////////////////////Encode////////////////////////////
    {
        //!增加type-DEV_CFG_TYPE_DECODE
        proc.pFuncGet = &IVNSysConfig::QueryEncodeCfg;
        proc.pFuncSet = &IVNSysConfig::ModEncodeCfg;

        m_map[DEV_CHANNEL_ENCODE_TYPE] = proc;
    }

#ifdef FUNC_FTP_UPLOAD
1111
    {
        //!增加type-DEV_CFG_TYPE_NET_FTP_SERVER
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CFG_TYPE_NET_FTP_SERVER] = proc;
    }
    {
        //!增加type-DEV_CFG_TYPE_NET_FTP_APP_TIME
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CFG_TYPE_NET_FTP_APP_TIME] = proc;
    }
#endif

#ifdef FUNCTION_SUPPORT_RTSP
111
    {
        //!增加type-DEV_CFG_TYPE_NET_RTSP
        proc.pFuncGet = &IVNSysConfig::QueryNetAPPCfg;
        proc.pFuncSet = &IVNSysConfig::ModNetAPPCfg;

        m_map[DEV_CFG_TYPE_NET_RTSP] = proc;
    }
#endif

///////////////////////PTZ TOUR////////////////////////////////////
    {
        proc.pFuncGet = &IVNSysConfig::QueryPtzPresetCfg;
        proc.pFuncSet = &IVNSysConfig::ModPTZPresetCfg;

        m_map[DEV_PTZ_PRESET_TYPE] = proc;
    }
    {
        proc.pFuncGet = &IVNSysConfig::QueryPtzTourCfg;
        proc.pFuncSet = &IVNSysConfig::ModPtzTourCfg;
        m_map[DEV_PTZ_TOUR_TYPE] = proc;
    }    
    {
        proc.pFuncGet = &IVNSysConfig::PtzControlRawData;
        proc.pFuncSet = &IVNSysConfig::PtzControlRawData;
        m_map[PTZ_RAW_DATA] = proc;
    }    
/////////////////////////获取通道名称//////////////////////////////////////////
    {
        proc.pFuncGet = &IVNSysConfig::QueryChnName;
        proc.pFuncSet = &IVNSysConfig::ModChnName;
        m_map[DEV_SYSTEM_CHANNAME_CFG] = proc;
    }

/////////////////////////音频配置/////////////////////////////////////////
    {
        proc.pFuncGet = &IVNSysConfig::QueryAudioCfg;
        proc.pFuncSet = &IVNSysConfig::ModAudioCfg;
        m_map[DEV_SYSTEM_AUDIO_CFG] = proc;
    }   

///////////////////////////////数字通道配置/////////////////////////////////////
    {
        proc.pFuncGet = &IVNSysConfig::QueryLocalChCfg;
        proc.pFuncSet = &IVNSysConfig::ModLocalChCfg;
        m_map[DEV_CFG_TYPE_NET_LOCAL_CH_CFG] = proc;
    }   

    {
        proc.pFuncGet = &IVNSysConfig::QueryRemoteChCfg;
        proc.pFuncSet = &IVNSysConfig::ModRemoteChCfg;
        m_map[DEV_CFG_TYPE_NET_REMOTE_CH_CFG] = proc;
    }
///////////////////////////////控制键盘配置/////////////////////////////////////
    {
        proc.pFuncGet = &IVNSysConfig::QueryKBDConfig;
        proc.pFuncSet = &IVNSysConfig::ModKBDConfig;
        m_map[DEV_CONFIG_TYPE_KBD_TYPE] = proc;
    }
    {
        proc.pFuncGet = &IVNSysConfig::ManualSnap;
        //proc.pFuncGet = &IVNSysConfig::QueryManualSnap;
        //proc.pFuncSet = &IVNSysConfig::ManualSnap;
        m_map[DEV_MANUAL_SNAP] = proc;
    }

#if defined (_USE_720P_MODULE_) 
    {
        proc.pFuncGet = &IVNSysConfig::QueryCameraCfg;
        proc.pFuncSet = &IVNSysConfig::ModCameraCfg;
        m_map[DEV_CONFIG_CAMERA] = proc;
    }
	{
		proc.pFuncGet = &IVNSysConfig::QueryCameraCfgExt;
		proc.pFuncSet = &IVNSysConfig::ModCameraCfgExt;
		m_map[DEV_CONFIG_CAMERA_EXT] = proc;
	}

#endif


#ifdef _2761_EXCEPTION_CHK_
    {
        proc.pFuncGet = &IVNSysConfig::QueryEncoderDetectCfg;
        proc.pFuncSet = &IVNSysConfig::ModEncoderDetectCfg;

        m_map[DEV_ENCODER_DETECT_CFG] = proc;
    } 
#endif
}

IVNSysConfig::~IVNSysConfig()
{
    m_map.clear();
}

int IVNSysConfig::SetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    m_pConn = pReqPDU->m_pConn;
    int ret = OprConfig(false, pReqPDU, pRespPDU);
    return ret;
}

int IVNSysConfig::GetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    return OprConfig(true, pReqPDU, pRespPDU);
}

int IVNSysConfig::OprConfig(bool bGet, CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    trace("IVNSysConfig::OprConfig: bGet:%d\n", bGet);

    int iRet = 0;
    pRespPDU = new CIVnPdu();
    MAP_LIST::iterator it;
    int tmp_len = 0;
    int ret = 0;
    bool no_fit = true;    //!请求的一个都没有匹配上,返回错误

        
    CPacket* pPaket = NULL;
    const int increase_size = 2048;//!如果packet大小不够，每次增加2048
    int increase_times = 1;
    pPaket = g_PacketManager.GetPacket( increase_size* increase_times );

    pRespPDU->m_pIpHdr->dvrip_cmd = pReqPDU->m_pIpHdr->dvrip_cmd;
    try
    {
        while((unsigned int)tmp_len < pReqPDU->m_pIpHdr->dvrip_extlen )
        {
            CTlv tlv;

            ret = tlv.Parse( (unsigned char*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE+tmp_len),
                    pReqPDU->m_pIpHdr->dvrip_extlen  - tmp_len );
            if( ret < 0 )
            {
                throw -1;
            }

            trace("tlv type:%d\n", tlv.GetType());
                
            it = m_map.find( tlv.GetType() );
            if( it == m_map.end() )
            {
                //!此id不存在
            }
            else
            {
                no_fit = false;

                bool have_tlv_out = false;
                CTlv tlv_sub;
                if( bGet )
                {_printd("second.pFuncGet !@#$#@!");
                     iRet = (this->*(it->second.pFuncGet))( tlv, tlv_sub, have_tlv_out );
                }
                else
                {_printd("second.pFuncSet !@#$#@!");
                   iRet = (this->*(it->second.pFuncSet))( tlv, tlv_sub, have_tlv_out );
                    /*if( ret < 0 )
                    {
                        tracepoint();
                        throw -1;
                    }*/
                }

                if( have_tlv_out )    //!应答有tlv数据
                {

                    if( tlv_sub.GetTotalLen()  > pPaket->GetLeft() )
                    {
                        while( 1 )
                        {
                            increase_times++;
                            if((increase_times*increase_size) > (int)( tlv_sub.GetTotalLen() + pPaket->GetLength() ))
                            {
                                CPacket* tmp = g_PacketManager.GetPacket( increase_size* increase_times );
                                assert(tmp != NULL);
                                tmp->PutBuffer( pPaket->GetBuffer(), pPaket->GetLength() );
                                pPaket->Release();
                                pPaket = tmp;
                                break;
                            }
                        }
                    }
                    int tmp_len;
                    uchar* p;
                    tlv_sub.Pack(p, tmp_len);

                    ret = pPaket->PutBuffer(p, tmp_len);

                    if( ret != tmp_len )
                    {
                        trace( "packet-size:%d, len:%d\n", pPaket->GetSize(), pPaket->GetLength() );
                        trace("tlv total_len:%d\n", tlv_sub.GetTotalLen() );
                    }
                    assert(ret == tmp_len);
                }
            }

            tmp_len += tlv.GetTotalLen();
        }
    }
    catch( int error )
    {
        if( error == -1 )
        {
            if(pRespPDU != NULL )
            {
                delete pRespPDU;
                pRespPDU = NULL;
            }
        }
    }

    if(pPaket)
    {
        if(pRespPDU)
        { 
            pRespPDU->packetBody( (char*)pPaket->GetBuffer(), pPaket->GetLength() );
        }
        pPaket->Release();
    }
    else
    {
        pRespPDU->packetBody(NULL, 0);
    }
    return iRet;
}


typedef struct _GooLinkConf
{
	unsigned int 	IsEnable;/*0 disable 1 enable*/
	unsigned char 	udid[32];
}GooLinkConf;
int IVNSysConfig::QueryGooLink( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	GooLinkConf GlConf = {0};
	SAVENANDINFO_T 		FacInfo = {0};
	ReadFacInfo(&FacInfo);
	strcpy((char *)GlConf.udid, FacInfo.PTPUUID);

	tlv_out.SetType(DEV_GOOLINK_TYPE);
    tlv_out.SetValue( (uchar*)&GlConf, sizeof(GlConf), false );

    have_tlv_out = true;
    return 0;
}
int IVNSysConfig::ModGooLink( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	if(sizeof(GooLinkConf) != tlv_in.GetLength())
	{
		_printd("sizeof(GooLinkConf) Length error!");
		return 0;
	}

	GooLinkConf *pTemp = NULL;
	pTemp = (GooLinkConf *)tlv_in.GetValueBuf();
	_printd("set enable = %d, str = [%s]", pTemp->IsEnable, pTemp->udid);

	SAVENANDINFO_T FacInfo = {0};
	ReadFacInfo(&FacInfo);
	strcpy(FacInfo.PTPUUID, (char *)pTemp->udid);
	WriteFacInfo(&FacInfo);
	//memcpy((char *)&GlConf, (char *)pTemp, sizeof(GooLinkConf));
	return 0;
}



int IVNSysConfig::GetSplitCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::GetSplitCfg\n");
    CConfigSplitCombine sc;
    sc.update();

    tlv_out.SetType(DEV_SYSTEM_INFO_DISPLAY_SPLIT);
    tlv_out.SetValue( (uchar*)&sc.getConfig(), sizeof( sc.getConfig() ), false );

    have_tlv_out = true;
    return 0;
}

int IVNSysConfig::SetSplitCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::SetSplitCfg\n");
    CConfigSplitCombine sc;
    sc.update();


    DISPLAY_SPLIT_CFG cfg;
    tlv_in.GetValue( (uchar*)&cfg, sizeof(cfg) );

    sc.getConfig().stSplitMode = cfg.stSplitMode;
    sc.getConfig().stCombine.iChMask = cfg.stCombine.iChMask;
    sc.getConfig().stCombine.ucSpecialCh = cfg.stCombine.ucSpecialCh;

    sc.commit();
    have_tlv_out = false;
    return 0;
}

int IVNSysConfig::GetTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::GetTourCfg\n");
    have_tlv_out = true;
    uchar* p = NULL;
    int tmp_len = 0;
    int ret = 0;

    CConfigMonitorTour tour;
    tour.update();

    CONFIG_MONITORTOUR& cfg = tour.getConfig();

    CPacket* pPacket = g_PacketManager.GetPacket( 8+sizeof(DISPLAY_TOUR_CFG) + SPLIT_NR* (8+sizeof(SPLIT_COMBINE)) );
    assert( pPacket != NULL );

    {
        //!添加轮巡参数
        DISPLAY_TOUR_CFG dis_cfg;

        dis_cfg.bTourEnable = cfg.bEnable;
        dis_cfg.usIntervalTime = cfg.iInterval;

        //trace("tour=======>get cfg:%d\n", dis_cfg.bTourEnable);
        CTlv tlv;
        tlv.SetType(DEV_SYSTEM_INFO_TOUR_CFG);
        tlv.SetValue((uchar*)&dis_cfg, sizeof(dis_cfg), false );
        tlv.Pack(p, tmp_len);

        ret = pPacket->PutBuffer(p, tmp_len);
        assert(ret == tmp_len);
    }

    {
        SPLIT_COMBINE sc;
        int i = 0;

        for( i = 0; i < SPLIT_NR; i++ )
        {
            ret = CONFIG_MONITORTOUR_2_SPLIT_COMBINE((enum split_combine_t)i, cfg, sc );
            if( ret < 0 )
            {
                continue;
            }

            CTlv tlv;

            tlv.SetType(DEV_SYSTEM_INFO_SPLIT_COMBINE);
            tlv.SetValue((uchar*)&sc, sizeof(sc), false );
            tlv.Pack(p, tmp_len);

            ret = pPacket->PutBuffer(p, tmp_len);
            assert(ret == tmp_len);
        }
    }

    tlv_out.SetType(DEV_SYSTEM_INFO_DISPLAY_TOUR);
    tlv_out.SetValue((uchar*)pPacket->GetBuffer(), pPacket->GetLength(), true );

    pPacket->Release();
    return 0;
}

int IVNSysConfig::SetTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::SetTourCfg\n");
    CConfigMonitorTour tour;
    tour.update();
    int ret = 0;

    CONFIG_MONITORTOUR& tour_cfg = tour.getConfig();

    have_tlv_out = false;

    CPacket* pPacket = g_PacketManager.GetPacket( tlv_in.GetLength() );
    assert( pPacket != NULL );

    tlv_in.GetValue( pPacket->GetBuffer(), pPacket->GetSize() );
    pPacket->SetLength( tlv_in.GetLength() );

    uint used_len = 0;

    while( used_len < pPacket->GetLength() )
    {
        CTlv tlv;

        ret = tlv.Parse(pPacket->GetBuffer()+used_len, pPacket->GetLength()-used_len );
        if( ret < 0 )
        {
            break;
        }

//        trace( "type:%d\n", tlv.GetType() );
        if( tlv.GetType() == DEV_SYSTEM_INFO_TOUR_CFG )
        {
            DISPLAY_TOUR_CFG Cfg;
            ret = tlv.GetValue((uchar*)&Cfg, sizeof(Cfg));
            assert(ret == 0 );

            //tlv.dump();

            tour_cfg.bEnable = Cfg.bTourEnable;
            tour_cfg.iInterval = Cfg.usIntervalTime;

            //("enable:%d, time:%d, size:%d\n",
            //        Cfg.bTourEnable, Cfg.usIntervalTime, sizeof(Cfg) );

        }
        else if( tlv.GetType() == DEV_SYSTEM_INFO_SPLIT_COMBINE )
        {
            SPLIT_COMBINE Cfg;
//            int ucCombinNums;
            tlv.GetValue( (uchar*)&Cfg, sizeof(Cfg) );

            SPLIT_COMBINE_2_CONFIG_MONITORTOUR(Cfg, tour_cfg);
        }

        used_len += tlv.GetTotalLen();
    }

    //trace("============>set tour cfg:%d\n", tour_cfg.bEnable);
    tour.commit();
    //trace("==================3=====>set time:%d\n", SystemGetMSCount());
    pPacket->Release();

    return 0;
}

int IVNSysConfig::GetRecordCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    RV_RECCTL  cfg;
    memset(&cfg,0,sizeof(cfg));
    
    uint64 recmask = g_Record.LoadRecordControl();
    for (int i = 0; i < g_nLogicNum; i++)
    {    
        cfg.ucState[i] = (0x3 & (recmask >> (2 * i)));
    }
    
    cfg.ucChannelCount = g_nLogicNum;
    
    tlv_out.SetType(DEV_SYSTEM_RECCTL_CFG);
    tlv_out.SetValue( (uchar*)&cfg, sizeof(RV_RECCTL), false );

    have_tlv_out = true;

    return 0;
}

int IVNSysConfig::SetRecordCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    RV_RECCTL  cfg;
    
    tlv_in.GetValue( (uchar*)&cfg, sizeof(cfg) );
    if(cfg.ucChannelCount != g_nLogicNum)
    {
        trace("RecordCfg, Channel num error!\n");
        return 0;
    }

    uint64 recmask = 0;
    for (int i = 0; i < g_nLogicNum; i++)
    {    
        if(cfg.ucState[i])
        {
            recmask |= ((uint64)cfg.ucState[i] << (2 * i));
        }
    }
    g_Record.SaveRecordControl(recmask, TRUE);
    have_tlv_out = false;

    return 0;
}

int IVNSysConfig::GetOutOSDCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{	
    CConfigGUISet GuiSet;
    GuiSet.update();

    CONFIG_GUISET &set = GuiSet.getConfig();
    NET_VIDEO_OUT_OSD_CFG  cfg;
    memset(&cfg,0,sizeof(cfg));

#define CHECH_OSD_SHOW(enable, mask) {cfg.ucOSDShow |= ((VD_TRUE == enable) ?  BITMSK(mask) : 0);}
	CHECH_OSD_SHOW(set.bTimeTitleEn, 0);
	CHECH_OSD_SHOW(set.bChannelTitleEn, 1);
	CHECH_OSD_SHOW(set.bAlarmStatus, 2);
	CHECH_OSD_SHOW(set.bRecordStatus, 3);	
	CHECH_OSD_SHOW(set.bDigiTimeTitleEn, 4);
	CHECH_OSD_SHOW(set.bDigiChannelTitleEn, 5);
	CHECH_OSD_SHOW(set.bDigiAlarmStatus, 6);
	CHECH_OSD_SHOW(set.bDigiRecordStatus, 7);
#undef CHECH_OSD_SHOW
   
    tlv_out.SetType(DEV_SYSTEM_VIDEO_OUT_OSD_CFG);
    tlv_out.SetValue( (uchar*)&cfg, sizeof(NET_VIDEO_OUT_OSD_CFG), false );

    have_tlv_out = true;

    return 0;
}

int IVNSysConfig::SetOutOSDCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigGUISet GuiSet;
    GuiSet.update();

    CONFIG_GUISET &set = GuiSet.getConfig();
    NET_VIDEO_OUT_OSD_CFG  cfg;
    tlv_in.GetValue( (uchar*)&cfg, sizeof(cfg) );
	
#define CHECH_OSD_SHOW(enable, mask) {enable = (cfg.ucOSDShow & BITMSK(mask)) ? 1 : 0;}
    CHECH_OSD_SHOW(set.bTimeTitleEn, 0);
	CHECH_OSD_SHOW(set.bChannelTitleEn, 1);
	CHECH_OSD_SHOW(set.bAlarmStatus, 2);
	CHECH_OSD_SHOW(set.bRecordStatus, 3);	
	CHECH_OSD_SHOW(set.bDigiTimeTitleEn, 4);
	CHECH_OSD_SHOW(set.bDigiChannelTitleEn, 5);
	CHECH_OSD_SHOW(set.bDigiAlarmStatus, 6);
	CHECH_OSD_SHOW(set.bDigiRecordStatus, 7);
#undef CHECH_OSD_SHOW
   
    GuiSet.commit();
    have_tlv_out = false;

    return 0;
}

int IVNSysConfig::QuerySnapCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::GetSnapCfg\n");
    
    CTlv tlv_sub;
    tlv_sub.Parse(tlv_in.GetValueBuf(),tlv_in.GetLength());
    
    CPacket* pPacket = NULL;
    uchar *p =NULL;
    int tmp_len = 0;
    int iTlvType =tlv_sub.GetType();
    CTlv tlv;

    if(ENCODE_SNAP_STREAM_TYPE == iTlvType)
    {
        DEV_CHANNEL devChn;
        memcpy(&devChn, tlv_sub.GetValueBuf(), sizeof(DEV_CHANNEL));

        CConfigSnap* pCfgSnap = new CConfigSnap();
        pCfgSnap->update();
        CONFIG_SNAP_NET SnapNetCfg;

        if(devChn.usTotal == devChn.usIndex)
        {
            tmp_len = devChn.usTotal*sizeof(CONFIG_SNAP_NET);
            pPacket = g_PacketManager.GetPacket(tmp_len);
            assert( pPacket != NULL );
        
            for(int i = 0;i< g_nCapture;i++)
            {
                memset(&SnapNetCfg,0,sizeof(CONFIG_SNAP_NET));
                
                CONFIG_SNAP& cfgSnap= pCfgSnap->getConfig(i);
                SnapNetCfg.iChannel = i;
                SnapNetCfg.Enable = cfgSnap.Enable;
                SnapNetCfg.SnapCount = 1;
                SnapNetCfg.SnapQuality = cfgSnap.SnapQuality;
                if(cfgSnap.SnapInterval >= 1000)
                {
                    SnapNetCfg.usSnapMode = 1;
                    SnapNetCfg.usCounts = cfgSnap.SnapInterval/1000;
                }
                else
                {
                    SnapNetCfg.usSnapMode = 0;
                    SnapNetCfg.usCounts = 1000/cfgSnap.SnapInterval;
                }
                
                pPacket->PutBuffer(&SnapNetCfg,sizeof(CONFIG_SNAP_NET));
            }       
        }
        else if(devChn.usTotal > devChn.usIndex)
        {
            tmp_len = sizeof(CONFIG_SNAP_NET);
            pPacket = g_PacketManager.GetPacket(tmp_len);
            assert( pPacket != NULL );

            memset(&SnapNetCfg,0,sizeof(CONFIG_SNAP_NET));
            
            CONFIG_SNAP& cfgSnap= pCfgSnap->getConfig(devChn.usIndex);
            SnapNetCfg.iChannel = devChn.usIndex;
            SnapNetCfg.Enable = cfgSnap.Enable;
            SnapNetCfg.SnapCount = 1;
            SnapNetCfg.SnapQuality = cfgSnap.SnapQuality;

            if(cfgSnap.SnapInterval >= 1000)
            {
                SnapNetCfg.usSnapMode = 1;
                SnapNetCfg.usCounts = cfgSnap.SnapInterval/1000;
            }
            else
            {
                SnapNetCfg.usSnapMode = 0;
                SnapNetCfg.usCounts = 1000/cfgSnap.SnapInterval;
            }
            
            pPacket->PutBuffer(&SnapNetCfg,sizeof(CONFIG_SNAP_NET));    
            
        }
        else
        {
            tracef("wpl GetSnapCfg error:  usTotal < usIndex\n");
        }

        delete pCfgSnap;
        
        tlv.SetType(ENCODE_SNAP_STREAM_TYPE);
        tlv.SetValue(pPacket->GetBuffer(),tmp_len, false);
        tlv.Pack(p, tmp_len);    
            
    }

    tlv_out.SetType(DEV_CONFIG_TYPE_SNAP);
    tlv_out.SetValue(p, tmp_len, true );

    pPacket->Release();
    have_tlv_out = true;
    return 0;
}

int IVNSysConfig::ModSnapCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::SetSnapCfg\n");
    
    CTlv tlv_sub;
    uchar* pData = tlv_in.GetValueBuf();
    if(NULL == pData)
    {
        return -1;
    }
    
    tlv_sub.Parse(pData,tlv_in.GetLength());

    uint iTotalLen = tlv_sub.GetLength();
    int iTlvType = tlv_sub.GetType();
    pData = tlv_sub.GetValueBuf();
    
    if(ENCODE_SNAP_STREAM_TYPE == iTlvType)
    {
        if ((iTotalLen< sizeof(CONFIG_SNAP_NET)) || (iTotalLen%sizeof(CONFIG_SNAP_NET)))
        {
            have_tlv_out = false;
            return 0;
        }

        CConfigSnap* pCfgSnap = new CConfigSnap();
        pCfgSnap->update();
        CONFIG_SNAP_NET SnapNetCfg;
        uint tmp_len =0;
        int totolSnapNum = 0;
        while(tmp_len < iTotalLen)
        {            
            memcpy(&SnapNetCfg, pData+tmp_len,sizeof(CONFIG_SNAP_NET));
            
            if(SnapNetCfg.iChannel >= 0 && SnapNetCfg.iChannel < g_nCapture)
            {
                CONFIG_SNAP& cfgSnap= pCfgSnap->getConfig(SnapNetCfg.iChannel);
                cfgSnap.Enable = SnapNetCfg.Enable;
                if(cfgSnap.Enable)
                {
                    if(++totolSnapNum > MAX_SNAP_NUM)
                    {
                        cfgSnap.Enable = 0;
                    }
                }
                cfgSnap.SnapCount = 1;
                cfgSnap.SnapQuality= SnapNetCfg.SnapQuality;
                if(SnapNetCfg.usCounts <= 0)
                {
                    SnapNetCfg.usCounts =1;
                }
                
                if(0 == SnapNetCfg.usSnapMode)
                {
                    cfgSnap.SnapInterval = 1000/(SnapNetCfg.usCounts);
                }
                else if(1 == SnapNetCfg.usSnapMode)
                {
                    cfgSnap.SnapInterval = SnapNetCfg.usCounts*1000;
                }

            }
            
            tmp_len += sizeof(CONFIG_SNAP_NET);
            
        }

        pCfgSnap->commit();
        delete pCfgSnap;
        
        
    }
    
    have_tlv_out = false;
    return 0;
}

int IVNSysConfig::QueryAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::GetAlarmCfg\n");
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    CConfigDecoderAlarm *pAlarm = new CConfigDecoderAlarm();
    pAlarm->update();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

//        trace("list node-type:%d\n", pTlv->GetType() );
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                //!取单通道信息
                ALARM_GUARD Alarm_guard;

                Alarm_guard.iChannel = pChannel->usIndex;
                Alarm_guard.iEnable = pAlarm->getConfig(pChannel->usIndex).enable;
                Alarm_guard.iGeneral = 3;

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)&Alarm_guard, sizeof(Alarm_guard), false );
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(ALARM_GUARD) );
                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    ALARM_GUARD Alarm_guard;

                    Alarm_guard.iChannel = i;
                    Alarm_guard.iEnable = pAlarm->getConfig(i).enable;
                    Alarm_guard.iGeneral = 3;

                    pPacket->PutBuffer( &Alarm_guard, sizeof(Alarm_guard) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

                pPacket->Release();
            }
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;
                pNetEH->iChannel = pChannel->usIndex;
                //memcpy( &pNetEH->stEventHandler, &pAlarm->getConfig(pNetEH->iChannel).hEvent, sizeof(EVENT_HANDLER));
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pAlarm->getConfig(pNetEH->iChannel).handler, pNetEH->stEventHandler);

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_EVENT_HANDLER), false );
                delete pNetEH;
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(NET_EVENT_HANDLER) );
                NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;

                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    pNetEH->iChannel =i;
                    //memcpy( &pNetEH->stEventHandler, &pAlarm->getConfig(pNetEH->iChannel).hEvent, sizeof(EVENT_HANDLER));
                    EVENT_HANDLER_2_EVENT_HANDLER_NET(pAlarm->getConfig(pNetEH->iChannel).handler, pNetEH->stEventHandler);
                    pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                //trace("====================>total:%d, size:%d, packetlen:%d\n",
                //        pChannel->usTotal, sizeof(NET_EVENT_HANDLER), pPacket->GetLength() );

                delete pNetEH;
                pPacket->Release();
            }
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {
            CConfigDecAlmWorksheet * pWorksheet = new CConfigDecAlmWorksheet;
            pWorksheet->update();

            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                NET_WORKSHEET* pNetEH = new NET_WORKSHEET;
                //pNetEH->iChannel = pChannel->usIndex;
                //pNetEH->iName = pWorksheet->getConfig( pNetEH->iChannel ).iName;
                //memcpy( &pNetEH->tsSchedule, &pWorksheet->getConfig(pNetEH->iChannel).tsSchedule,
                //        sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
                CONFIG_WORKSHEET_2_NET_WORKSHEET(  pChannel->usIndex, pWorksheet->getConfig(pChannel->usIndex), *pNetEH);

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_WORKSHEET), false );
                delete pNetEH;
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(NET_WORKSHEET) );
                NET_WORKSHEET* pNetEH = new NET_WORKSHEET;

                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    //pNetEH->iChannel =i;
                    //pNetEH->iName = pWorksheet->getConfig( pNetEH->iChannel ).iName;
                    //memcpy( &pNetEH->tsSchedule, &pWorksheet->getConfig(pNetEH->iChannel).tsSchedule, sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
                    CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                    pPacket->PutBuffer( pNetEH, sizeof(NET_WORKSHEET) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

                delete pNetEH;
                pPacket->Release();
            }

            delete pWorksheet;
        }

        out_list.push_back(pTlvOut);
    }

    delete pAlarm;


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}

int IVNSysConfig::ModAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::SetAlarmCfg\n");
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

//        trace("list type:%d\n", pTlv->GetType());
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            CConfigDecoderAlarm *pAlarm = new CConfigDecoderAlarm();
            pAlarm->update();

            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pNetAlarm = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pAlarm->getConfig(pNetAlarm->iChannel).enable = pNetAlarm->iEnable;
       //         pAlarm->getConfig(pNetAlarm->iChannel).iSensorType = pNetAlarm->iGeneral;
            }

            ret = pAlarm->commit();
            delete pAlarm;
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            CConfigDecoderAlarm *pAlarm = new CConfigDecoderAlarm();
            pAlarm->update();

            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pNetAlarm = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                //memcpy(&pAlarm->getConfig(pNetAlarm->iChannel).hEvent, &pNetAlarm->stEventHandler, sizeof(EVENT_HANDLER) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pNetAlarm->stEventHandler, pAlarm->getConfig(pNetAlarm->iChannel).handler);
            }

            ret = pAlarm->commit();
            delete pAlarm;
        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            CConfigDecAlmWorksheet * pWorksheet = new CConfigDecAlmWorksheet;
            pWorksheet->update();

            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pNetAlarm = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                //pWorksheet->getConfig(pNetAlarm->iChannel).iName = pNetAlarm->iName;
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pNetAlarm->iChannel, *pNetAlarm, pWorksheet->getConfig(pNetAlarm->iChannel) );
                //memcpy(pWorksheet->getConfig(pNetAlarm->iChannel).tsSchedule, pNetAlarm->tsSchedule,
                //        sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
            }

            ret = pWorksheet->commit();
            delete pWorksheet;
        }

        trace("===============>commit ret:%#x\n", ret);
    }

    return 0;
}
int IVNSysConfig::QueryNetAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::QueryNetAlarmCfg\n");
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    CConfigNetAlarm *pAlarm = new CConfigNetAlarm();
    pAlarm->update();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

//        trace("list node-type:%d\n", pTlv->GetType() );
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                //!取单通道信息
                ALARM_GUARD Alarm_guard;

                Alarm_guard.iChannel = pChannel->usIndex;
                Alarm_guard.iEnable = pAlarm->getConfig(pChannel->usIndex).bEnable;
                Alarm_guard.iGeneral = 3;

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)&Alarm_guard, sizeof(Alarm_guard), false );
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(ALARM_GUARD) );
                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    ALARM_GUARD Alarm_guard;

                    Alarm_guard.iChannel = i;
                    Alarm_guard.iEnable = pAlarm->getConfig(i).bEnable;
                    Alarm_guard.iGeneral = 3;

                    pPacket->PutBuffer( &Alarm_guard, sizeof(Alarm_guard) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

                pPacket->Release();
            }
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;
                pNetEH->iChannel = pChannel->usIndex;
                //memcpy( &pNetEH->stEventHandler, &pAlarm->getConfig(pNetEH->iChannel).hEvent, sizeof(EVENT_HANDLER));
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pAlarm->getConfig(pNetEH->iChannel).hEvent, pNetEH->stEventHandler);

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_EVENT_HANDLER), false );
                delete pNetEH;
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(NET_EVENT_HANDLER) );
                NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;

                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    pNetEH->iChannel =i;
                    //memcpy( &pNetEH->stEventHandler, &pAlarm->getConfig(pNetEH->iChannel).hEvent, sizeof(EVENT_HANDLER));
                    EVENT_HANDLER_2_EVENT_HANDLER_NET(pAlarm->getConfig(pNetEH->iChannel).hEvent, pNetEH->stEventHandler);
                    pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                //trace("====================>total:%d, size:%d, packetlen:%d\n",
                //        pChannel->usTotal, sizeof(NET_EVENT_HANDLER), pPacket->GetLength() );

                delete pNetEH;
                pPacket->Release();
            }
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {
            CConfigNetAlmWorksheet * pWorksheet = new CConfigNetAlmWorksheet;
            pWorksheet->update();

            DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
            if( pChannel->usIndex < pChannel->usTotal )
            {
                NET_WORKSHEET* pNetEH = new NET_WORKSHEET;
                //pNetEH->iChannel = pChannel->usIndex;
                //pNetEH->iName = pWorksheet->getConfig( pNetEH->iChannel ).iName;
                //memcpy( &pNetEH->tsSchedule, &pWorksheet->getConfig(pNetEH->iChannel).tsSchedule,
                //        sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
                CONFIG_WORKSHEET_2_NET_WORKSHEET(  pChannel->usIndex, pWorksheet->getConfig(pChannel->usIndex), *pNetEH);

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_WORKSHEET), false );
                delete pNetEH;
            }
            else
            {
                //!取所有通道信息
                CPacket* pPacket = g_PacketManager.GetPacket( pChannel->usTotal*sizeof(NET_WORKSHEET) );
                NET_WORKSHEET* pNetEH = new NET_WORKSHEET;

                for( int i = 0; i < pChannel->usTotal; i++ )
                {
                    //pNetEH->iChannel =i;
                    //pNetEH->iName = pWorksheet->getConfig( pNetEH->iChannel ).iName;
                    //memcpy( &pNetEH->tsSchedule, &pWorksheet->getConfig(pNetEH->iChannel).tsSchedule, sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
                    CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                    pPacket->PutBuffer( pNetEH, sizeof(NET_WORKSHEET) );
                }

                pTlvOut->SetType( pTlv->GetType() );
                pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

                delete pNetEH;
                pPacket->Release();
            }

            delete pWorksheet;
        }

        out_list.push_back(pTlvOut);
    }

    delete pAlarm;


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}


int IVNSysConfig::ModNetAlarmCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
//    trace("IVNSysConfig::SetAlarmCfg\n");
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

//        trace("list type:%d\n", pTlv->GetType());
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            CConfigNetAlarm *pAlarm = new CConfigNetAlarm();
            pAlarm->update();

            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pNetAlarm = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pAlarm->getConfig(pNetAlarm->iChannel).bEnable = pNetAlarm->iEnable;
       //         pAlarm->getConfig(pNetAlarm->iChannel).iSensorType = pNetAlarm->iGeneral;
            }

            ret = pAlarm->commit();
            delete pAlarm;
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            CConfigNetAlarm *pAlarm = new CConfigNetAlarm();
            pAlarm->update();

            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pNetAlarm = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                //memcpy(&pAlarm->getConfig(pNetAlarm->iChannel).hEvent, &pNetAlarm->stEventHandler, sizeof(EVENT_HANDLER) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pNetAlarm->stEventHandler, pAlarm->getConfig(pNetAlarm->iChannel).hEvent);
            }

            ret = pAlarm->commit();
            delete pAlarm;
        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            CConfigNetAlmWorksheet * pWorksheet = new CConfigNetAlmWorksheet;
            pWorksheet->update();

            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pNetAlarm = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                //pWorksheet->getConfig(pNetAlarm->iChannel).iName = pNetAlarm->iName;
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pNetAlarm->iChannel, *pNetAlarm, pWorksheet->getConfig(pNetAlarm->iChannel) );
                //memcpy(pWorksheet->getConfig(pNetAlarm->iChannel).tsSchedule, pNetAlarm->tsSchedule,
                //        sizeof(NETSECTION)*NET_N_WEEKS*NET_N_UI_TSECT );
            }

            ret = pWorksheet->commit();
            delete pWorksheet;
        }

        trace("===============>commit ret:%#x\n", ret);
    }

    return 0;
}


int IVNSysConfig::QueryGeneralCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    NET_GENERAL NetGenCfg;
    memset(&NetGenCfg,0,sizeof(NET_GENERAL));
    
    CConfigGeneral* pCfgGeneral = new CConfigGeneral();
    CConfigLocation* pCfgLocation = new CConfigLocation();
    CConfigAudioInFormat *pConfigAudioInFormat = new CConfigAudioInFormat();

    pCfgGeneral->update();
    pCfgLocation->update();
    pConfigAudioInFormat->update();

    CONFIG_GENERAL& cfgGeneral = pCfgGeneral->getConfig();
    CONFIG_LOCATION& cfgLocation = pCfgLocation->getConfig();
    CONFIG_AUDIOIN_FORMAT &cfgAudio = pConfigAudioInFormat->getConfig(0);
    
    if(0 == cfgGeneral.iOverWrite)
    {
        NetGenCfg.iCtrlMask &=~BITMSK(0);
    }else
    {
        NetGenCfg.iCtrlMask |= BITMSK(0);
    }
    if(0 == cfgAudio.Silence)
    {
        NetGenCfg.iCtrlMask &=~BITMSK(1);
    }else
    {
        NetGenCfg.iCtrlMask |= BITMSK(1);
    }
    
    NetGenCfg.iLocalNo =cfgGeneral.iLocalNo;
    NetGenCfg.usAutoLogout = cfgGeneral.iAutoLogout;
    NetGenCfg.usScreenSaveTime = cfgGeneral.iScreenSaveTime;

    NetGenCfg.ucTimeFmt = cfgLocation.iTimeFormat;
    NetGenCfg.ucDateSprtr = cfgLocation.iDateSeparator;
    NetGenCfg.ucDateFormat = cfgLocation.iDateFormat;
    NetGenCfg.ucCurVideoFmt = cfgLocation.iVideoFormat;
#if defined(DEF_RESOLUTION_ADJUST)	
    NetGenCfg.ucResolution =  cfgGeneral.iFixType;
#endif

#ifdef _FUN_SUPPORT_DOUBLE_OUTPUT    
    NetGenCfg.ucMenuLayer = cfgGeneral.iVideoStartOutPut;
#endif
    NetGenCfg.uiCurLanguage = cfgLocation.iLanguage;

    NetGenCfg.ucSupportVideoFmt = 0x3;

    NetGenCfg.uiSupportLanguage = (uint)AppConfig::instance()->getNumber("Global.Language", 0x87); 

    tlv_out.SetType(DEV_SYSTEM_INFO_GENERAL);
    tlv_out.SetValue((uchar*)&NetGenCfg, sizeof(NET_GENERAL), false);    

    have_tlv_out = true;

    delete pCfgGeneral;
    delete pCfgLocation;
    delete pConfigAudioInFormat;
    
    return 0;

}

int IVNSysConfig::QueryCurTimeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    SYSTEM_TIME NetTimeCfg;
    memset(&NetTimeCfg,0,sizeof(SYSTEM_TIME));
    
    SystemGetCurrentTime(&NetTimeCfg);

    tlv_out.SetType(DEV_SYSTEM_CUR_TIME);
    tlv_out.SetValue((uchar*)&NetTimeCfg, sizeof(SYSTEM_TIME), false);    

    have_tlv_out = true;

    return 0;

}

int IVNSysConfig::ModGeneralCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	_printd("IVNSysConfig::ModGeneralCfg");
    have_tlv_out = false;
    bool bNeedReboot = false;
    
    NET_GENERAL NetGenCfg;
    memset(&NetGenCfg,0,sizeof(NET_GENERAL));
    tlv_in.GetValue( (uchar*)&NetGenCfg, sizeof(NET_GENERAL) );
        
    CConfigLocation* pCfgLocation = new CConfigLocation();
    pCfgLocation->update();
    CONFIG_LOCATION &cfgLocation = pCfgLocation->getConfig();
      
    CConfigGeneral* pCfgGeneral = new CConfigGeneral();
    pCfgGeneral->update();
    CONFIG_GENERAL &cfgGeneral = pCfgGeneral->getConfig();

    CConfigAudioInFormat* pConfigAudioInFormat = new CConfigAudioInFormat();
    pConfigAudioInFormat->update();
    CONFIG_AUDIOIN_FORMAT &cfgAudio = pConfigAudioInFormat->getConfig(0);


#if defined(DEF_RESOLUTION_ADJUST)   
        int nowType = 0;
	 VD_BOOL bNeedSet = FALSE;	
	 int iVGAPixelFromPage = NetGenCfg.ucResolution;
	 if((iVGAPixelFromPage >= ENUM_VIDOUT_1920x1080p60)
	 	&& (iVGAPixelFromPage < ENUM_VIDOUT_NUM))
	 {
		nowType = cfgGeneral.iFixType;
		if(nowType != iVGAPixelFromPage)
		{                     
	        cfgGeneral.iFixType = iVGAPixelFromPage;
	        SystemSetVGAPixel(iVGAPixelFromPage);
		}

        }
#endif

    cfgGeneral.iLocalNo = NetGenCfg.iLocalNo;
    cfgGeneral.iOverWrite = 0x1 & NetGenCfg.iCtrlMask;
    cfgGeneral.iAutoLogout = NetGenCfg.usAutoLogout;            
    cfgGeneral.iScreenSaveTime = NetGenCfg.usScreenSaveTime;
    cfgAudio.Silence = (0x2 & NetGenCfg.iCtrlMask)>>1;

    cfgLocation.iDateFormat = NetGenCfg.ucDateFormat;
    cfgLocation.iDateSeparator =NetGenCfg.ucDateSprtr;
    cfgLocation.iTimeFormat = NetGenCfg.ucTimeFmt;

    if(NetGenCfg.ucCurVideoFmt  != cfgLocation.iVideoFormat)
    {
        //cfgLocation.iVideoFormat = NetGenCfg.ucCurVideoFmt;

        //!制式对有采集的编码通道才有意义
        if( ICaptureManager::instance()->GetAnalogChnNum() > 0 )
        {
        	//NVR没有视频制式，故将其挪下来。暂时解决针对NVR网络客户端发过的数据不对以致commit时验证失败的问题
        	//至于弹出"设备重启是否继续"的提示需要PC端修改
        	cfgLocation.iVideoFormat = NetGenCfg.ucCurVideoFmt;
        	
            //针对720P球机做特殊处理
            if (0 == SystemSetVideoMode((video_standard_t)(cfgLocation.iVideoFormat)) )
            {
                bNeedReboot = true;
            }
        }
    }

    if(NetGenCfg.uiCurLanguage != (uint)cfgLocation.iLanguage)
    {
        cfgLocation.iLanguage = NetGenCfg.uiCurLanguage;
        FILE *fp = fopen(CONFIG_DIR"/lang", "w+b");        /*更改通道名称以适应语言变化*/

        if(fp != NULL)
        {
            int lang = 1;
            fseek(fp, 0, SEEK_SET);
            fwrite(&lang, sizeof(int), 1, fp);
            fclose(fp);

            /* 针对部分产品可能写不成功的问题，多写几次 */
            for (int i=0;i<10;i++)
            {
                SystemSetUILanguage((ui_language_t)(cfgLocation.iLanguage));
                if ((ui_language_t)(cfgLocation.iLanguage) != SystemGetUILanguage())
                {
                    printf("IVNSysConfig::ModGeneralCfg():SystemSetUILanguage failed,cfgLocation.iLanguage  =  %d\r\n",cfgLocation.iLanguage);
                    SystemSleep(10);
                }
                else
                {
                    break;
                }
            }
        }

        bNeedReboot = true;
    }


    pCfgGeneral->commit();
    delete pCfgGeneral;
    
    pCfgLocation->commit();
    delete pCfgLocation;

    pConfigAudioInFormat->commit();
    delete pConfigAudioInFormat;
    
    if(bNeedReboot)
    {
        g_Challenger.Reboot();
    }
    
    return 0;
}


int IVNSysConfig::ModCurTimeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    SYSTEM_TIME NetTimeCfg;
    memset(&NetTimeCfg,0,sizeof(SYSTEM_TIME));

    tlv_in.GetValue( (uchar*)&NetTimeCfg, sizeof(SYSTEM_TIME) );    

    g_General.UpdateSystemTime(&NetTimeCfg, 2);

    have_tlv_out = false;

    return 0;

}

//前端日志查询
int IVNSysConfig::QueryLogInfoCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int iRet;
    int index = 0;
    uint uiLogPos = 0;
    NET_QUERY_SYSYTEM_LOG Cfg;
    memset(&Cfg,0,sizeof(NET_QUERY_SYSYTEM_LOG));   
    NET_LOG_INFO LogInfoCfg;
    memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));

    LOG_ITEM szLogBuffer;
	char szLogstr[LOG_LINE_MAX] = {0};
    
    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );
    if(Cfg.ucQueryType == 0)
    {
        tracef("-----QueryLogs QueryType is wrong!\n");
        return 0;
    }

    int iType = Cfg.ucLogType;
    g_Log.SetRange(&Cfg.stStartTime, &Cfg.stEndTime);

    uint iTotalItem = g_Log.GetCount();
    uint iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);

    if (iTotalItem >= 1000)
    {
        iTotalItem = 1000;
        iTotalLen = iTotalItem * sizeof(NET_LOG_INFO);
    }

    CPacket* pPacker = g_PacketManager.GetPacket(iTotalLen);
    assert( pPacker != NULL );

    //转化成对应的类型
    if(iType < 0)
    {
        tracef("-----QueryLogs iType is wrong!\n");
        pPacker->Release();
        return 0;
    }
    else if(iType == 0)
    {
        iType = LOG_TYPE_NR;
    }
    else
    {
        iType--;
    }

    while((iRet = g_Log.GetContext(iType, &uiLogPos, szLogstr, &szLogBuffer)) != -1 && (index < iTotalItem))
    {
        memset(&LogInfoCfg,0,sizeof(NET_LOG_INFO));
        memset(szLogstr, 0, sizeof(szLogstr));
        
        LogInfoCfg.usType = szLogBuffer.type;
        LogInfoCfg.ucFlag= szLogBuffer.flag;
        LogInfoCfg.ucData = szLogBuffer.data;
        timedh2sys(&LogInfoCfg.stTime, &szLogBuffer.time);        
        //strcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context);  
		memcpy((char *)LogInfoCfg.ucContext, (char *)szLogBuffer.context,8);
        pPacker->PutBuffer((unsigned char *)&LogInfoCfg, sizeof(NET_LOG_INFO));
        uiLogPos++;//指向下一个日志项
        index++;
    }

    tlv_out.SetType(DEV_SYSTEM_QUERYLOG);
    tlv_out.SetValue( pPacker->GetBuffer(), pPacker->GetLength(), false );    

    have_tlv_out = true;

    pPacker->Release();

    return 0;

}


//前端日志配置
int IVNSysConfig::QueryLogCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int i = 0;
    int	LogSavedEn[LOG_TYPE_NR+1];
    NET_OPR_SYSYTEM_LOG Cfg;
    memset(&Cfg,0,sizeof(NET_OPR_SYSYTEM_LOG));   
    
    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );
    if(0 == Cfg.ucOprType)  //日志清楚
    {
       // g_Log.Clear(g_localUser.getName().c_str());
       g_Log.Clear(CFilterHelper::m_userName.c_str());
    }
    else if(1 == Cfg.ucOprType)  //日志规则查询
    {
        g_Log.GetLogFilter(LogSavedEn);
        for(i = 0; i < LOG_TYPE_NR; i++)
        {
            Cfg.uiIfRegLog[i] = LogSavedEn[i];
        }
        //最后一位标示日志是否覆盖
        Cfg.ucFullPolicy = LogSavedEn[LOG_TYPE_NR];
        
        tlv_out.SetType(DEV_SYSTEM_OPRLOG);
        tlv_out.SetValue((unsigned char*)&Cfg, sizeof(NET_OPR_SYSYTEM_LOG), false );  

        have_tlv_out = true;
    }
    
    return 0;

}

int IVNSysConfig::ModLogCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int i = 0;
    int	LogSavedEn[LOG_TYPE_NR+1];
    NET_OPR_SYSYTEM_LOG Cfg;
    memset(&Cfg,0,sizeof(NET_OPR_SYSYTEM_LOG));   
    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );

    if(2 == Cfg.ucOprType)  //日志规则设置
    {
        for(i = 0; i < LOG_TYPE_NR; i++)
        {
            LogSavedEn[i] = Cfg.uiIfRegLog[i];
        }

        LogSavedEn[LOG_TYPE_NR] = Cfg.ucFullPolicy;
        
        g_Log.SetLogFilter(LogSavedEn);
    }

    return 0;
}

int IVNSysConfig::QueryLocalFilesCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	FFFile dhFile; 
	
	uint chan, ItemNum, type;
    int driver_type = NORMAL_DRIVER_TYPE;
    int iFileType = 0;
    FILE_INFO tmpfileInfo;
    memset(&tmpfileInfo,0,sizeof(FILE_INFO)); 
    NET_FILEINFO Cfg;
    memset(&Cfg,0,sizeof(NET_FILEINFO));       
    tlv_in.GetValue( (uchar*)&Cfg, sizeof(Cfg) );

    iFileType = Cfg.ucFileType;
    if (0 == iFileType)
    {
        driver_type = NORMAL_DRIVER_TYPE;
    }
    else if(1 == iFileType)
    {
        driver_type = SNAP_DRIVER_TYPE;
    }
    chan = Cfg.ucChannel;

    // 查询所有录像类型为0, 外部报警1, 动态检测2, 所有报警3 4- 按卡号查询
    switch(Cfg.ucSubType)
    {
        case 1:
        {
            type = (1<<F_ALERT);
            break;
        }
        case 2: // 动态检测
        {
            type = (1<<F_DYNAMIC);
            break;
        }
        case 3: // 所有报警
        {
            type  = (1<<F_ALERT);
            type |= (1<<F_DYNAMIC);
			type |= (1<<F_CARD);
            break;
        }
        case 4:    // 手动录像,刘阳提供
        {
            type = (1<<F_HAND);
            break;
        }
		case 5: // 定时录像
        {
            type  = (1<<F_COMMON);
            break;
        }  
        case 6: // 卡号录像
        {
            type = (1<<F_CARD);
            break;
        }
		case 7:
		{
			type = (1<<F_2SND);
			break;
		}
        case 0:
        default:
        {
            type  = (1<<F_COMMON);
            type |= (1<<F_ALERT);
            type |= (1<<F_DYNAMIC);
            type |= (1<<F_CARD);
            type |= (1<<F_HAND);
			type |= (1<<F_2SND);
            break;
        }
    }
    
    ItemNum = MAX_FILE_NUM_ONCE;
    uchar pdat[sizeof(FILE_INFO) * MAX_FILE_NUM_ONCE];
//    printf("%d %d %d %d %d\n", chan, Cfg.ucSubType, type, ItemNum, driver_type);
    dhFile.GetList(chan, &Cfg.stStartTime, &Cfg.stEndTime, type, &ItemNum, (FILE_INFO *)pdat, 0,driver_type);
    FILE_INFO *fileInfo;
    uint iTotalLen = ItemNum * sizeof(NET_FILEINFO);
    CPacket* pPacker = g_PacketManager.GetPacket(iTotalLen);
    assert( pPacker != NULL );

    for(uint ii = 0; ii < ItemNum; ii++)
    {
        fileInfo = (FILE_INFO *)(pdat + ii * sizeof(FILE_INFO));
		if (fileInfo->channel >= g_nLogicNum)
		{
		    fileInfo->channel -= g_nLogicNum;
		}
		
        memset(&Cfg,0,sizeof(NET_FILEINFO));  
        memcpy(&tmpfileInfo, fileInfo, sizeof(FILE_INFO));
        Cfg.ucFileType = iFileType;
        Cfg.ucSubType = tmpfileInfo.alarm;
        Cfg.ucChannel = chan;
        timedh2sys(&Cfg.stStartTime, &tmpfileInfo.start_time);   
        timedh2sys(&Cfg.stEndTime, &tmpfileInfo.end_time);   

	   /*	文件格式:通道号_开始时间(yyyy-MM-dd-HH-mm-ss)_结束时间(yyyy-MM-dd-HH-mm-ss)_盘号_分区号_簇号_文件序号_文件类型(00录像 01图片)_文件长度*/
	   sprintf((char *)Cfg.ucFileName, "%02d_%04d-%02d-%02d-%02d-%02d-%02d_%04d-%02d-%02d-%02d-%02d-%02d_%02d_%02d_%04d_%02d_%02d_%02d_%04d\n",
			        tmpfileInfo.channel,
			        tmpfileInfo.start_time.year + 2000,
			        tmpfileInfo.start_time.month,
			        tmpfileInfo.start_time.day,
			        tmpfileInfo.start_time.hour,
			        tmpfileInfo.start_time.minute,
			        tmpfileInfo.start_time.second,
			        tmpfileInfo.end_time.year + 2000,
			        tmpfileInfo.end_time.month,
			        tmpfileInfo.end_time.day,
			        tmpfileInfo.end_time.hour,
			        tmpfileInfo.end_time.minute,
			        tmpfileInfo.end_time.second,
			        0,//tmpfileInfo.disk_no,
			        0,//tmpfileInfo.disk_part,
			        0,//tmpfileInfo.first_clus_no,
			        0,//tmpfileInfo.hint,
			        iFileType,
			        tmpfileInfo.alarm,
					tmpfileInfo.file_length);

       pPacker->PutBuffer((unsigned char *)&Cfg, sizeof(NET_FILEINFO));
    }

    tlv_out.SetType(DEV_SYSTEM_QUERY_REC_PIC);
    tlv_out.SetValue( pPacker->GetBuffer(), pPacker->GetLength(), false );    

    have_tlv_out = true;
    pPacker->Release();

    return 0;
}

//夏令时查询
int IVNSysConfig::QueryDSTCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    NET_DST_CFG NetDstCfg;
    memset(&NetDstCfg,0,sizeof(NET_DST_CFG));

    CConfigLocation *pCfg = new CConfigLocation();
    assert(pCfg);
    pCfg->update();
    
    CONFIG_LOCATION &cfgLc = pCfg->getConfig();

    NetDstCfg.ucEnable = cfgLc.iDSTRule?1:0;//不区分不同地方的夏令时规则，统一进行配置

    NetDstCfg.iDST[0] = cfgLc.iDST[0];
    NetDstCfg.iDST[1] = cfgLc.iDST[1];
    
    tlv_out.SetType(DEV_SYSTEM_DST_TIME);
    tlv_out.SetValue((uchar*)&NetDstCfg, sizeof(NET_DST_CFG), false);    

    delete pCfg;
    pCfg = NULL;
    
    have_tlv_out = true;

    return 0;

}

//夏令时配置修改
int IVNSysConfig::ModDSTCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	_printd ("IVNSysConfig::ModDSTCfg");
    NET_DST_CFG NetDstCfg;
    memset(&NetDstCfg,0,sizeof(NET_DST_CFG));

    tlv_in.GetValue( (uchar*)&NetDstCfg, sizeof(NET_DST_CFG) );    

    CConfigLocation *pCfg = new CConfigLocation();
    assert(pCfg);
    pCfg->update();
    
    CONFIG_LOCATION &cfgLc = pCfg->getConfig();

    cfgLc.iDSTRule = (int)NetDstCfg.ucEnable;
    cfgLc.iDST[0] = NetDstCfg.iDST[0];
    cfgLc.iDST[1] = NetDstCfg.iDST[1];

    pCfg->commit();
    delete pCfg;
    pCfg = NULL;

    have_tlv_out = false;

    return 0;

}

int IVNSysConfig::QueryAlarmInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    CConfigAlarm *pAlarm = new CConfigAlarm();
    pAlarm->update();

    TLV_LIST& tmp_list = reader.GetTlvList();
            
    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
        
//        trace("list node-type:%d\n", pTlv->GetType() );
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(ALARM_GUARD) );
            
            for( int i = iChn; i < iTotal; i++ )
            {
                ALARM_GUARD Alarm_guard;

                Alarm_guard.iChannel = i;
                Alarm_guard.iEnable = pAlarm->getConfig(i).bEnable;
                Alarm_guard.iGeneral = pAlarm->getConfig(i).iSensorType;

                pPacket->PutBuffer( &Alarm_guard, sizeof(Alarm_guard) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_EVENT_HANDLER) );
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;

            for( int i = iChn; i < iTotal; i++ )
            {
                pNetEH->iChannel =i;
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pAlarm->getConfig(i).hEvent, pNetEH->stEventHandler);
                pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            delete pNetEH;
            pPacket->Release();
            
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_WORKSHEET) );
            NET_WORKSHEET* pNetEH = new NET_WORKSHEET;

            CConfigALMWorksheet * pWorksheet = new CConfigALMWorksheet;
            pWorksheet->update();

            for( int i = iChn; i < iTotal; i++ )
            {
                CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                pPacket->PutBuffer( pNetEH, sizeof(NET_WORKSHEET) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            delete pNetEH;
            pPacket->Release();

            delete pWorksheet;
            
        }

        out_list.push_back(pTlvOut);
    }

    delete pAlarm;


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true);

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}

int IVNSysConfig::QueryLossCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    CConfigLossDetect *pCfg = new CConfigLossDetect();
    pCfg->update();
    
    CConfigVLTWorksheet * pWorksheet = new CConfigVLTWorksheet;
    pWorksheet->update();

    TLV_LIST& tmp_list = reader.GetTlvList();
    
//    trace("IVNSysConfig::QueryLossCfg->list num:%d\n", tmp_list.size() );
    TLV_LIST::iterator it;
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

//        trace("list node-type:%d\n", pTlv->GetType() );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
        
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(ALARM_GUARD) );
            for( int i = iChn; i < iTotal; i++ )
            {
                ALARM_GUARD Alarm_guard;
                memset(&Alarm_guard,0,sizeof(ALARM_GUARD));
                
                Alarm_guard.iChannel = i;
                Alarm_guard.iEnable = pCfg->getConfig(i).enable;
                Alarm_guard.iGeneral = 3;
                pPacket->PutBuffer( &Alarm_guard, sizeof(ALARM_GUARD));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;

            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_EVENT_HANDLER) );

            for( int i = iChn; i < iTotal; i++ )
            {
                pNetEH->iChannel =i;
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pCfg->getConfig(i).handler, pNetEH->stEventHandler);
                pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            

            delete pNetEH;
            
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {

            NET_WORKSHEET* pNetEH = new NET_WORKSHEET;
            
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_WORKSHEET) );

            for( int i = iChn; i < iTotal; i++ )
            {
                CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                pPacket->PutBuffer(pNetEH, sizeof(NET_WORKSHEET) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();
            
            delete pNetEH;
            
        }

        out_list.push_back(pTlvOut);
    }

    delete pCfg;
    delete pWorksheet;

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}

int IVNSysConfig::QueryMotionCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    CConfigMotionDetect *pCfg = new CConfigMotionDetect();
    pCfg->update();
    
    CConfigMTDWorksheet * pWorksheet = new CConfigMTDWorksheet;
    pWorksheet->update();

    TLV_LIST& tmp_list = reader.GetTlvList();
    
    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
        
//        trace("list node-type:%d\n", pTlv->GetType() );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
        
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(ALARM_GUARD) );
            for( int i = iChn ; i < iTotal; i++)
            {
                ALARM_GUARD Alarm_guard;
                memset(&Alarm_guard,0,sizeof(ALARM_GUARD));
                
                Alarm_guard.iChannel = i;
                Alarm_guard.iEnable = pCfg->getConfig(i).bEnable;
                Alarm_guard.iGeneral = pCfg->getConfig(i).iLevel;
                
                pPacket->PutBuffer( &Alarm_guard, sizeof(ALARM_GUARD));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;
            
            CPacket* pPacket = g_PacketManager.GetPacket( (iTotal - iChn)*sizeof(NET_EVENT_HANDLER) );

            for( int i = iChn ; i < iTotal; i++)
            {
                pNetEH->iChannel =i;
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pCfg->getConfig(i).hEvent, pNetEH->stEventHandler);
                pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            
            delete pNetEH;
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {
            NET_WORKSHEET* pNetEH = new NET_WORKSHEET;
            

            CPacket* pPacket = g_PacketManager.GetPacket( (iTotal - iChn)*sizeof(NET_WORKSHEET) );

            for( int i = iChn ; i < iTotal; i++)
            {
                CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                pPacket->PutBuffer(pNetEH, sizeof(NET_WORKSHEET) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
        
            delete pNetEH;
        }
        else if(pTlv->GetType() == ALARM_MOTION_REGION_TYPE)
        {
            NET_REGION *pNetEH = new NET_REGION;

            CPacket* pPacket = g_PacketManager.GetPacket( (iTotal - iChn)*sizeof(NET_REGION) );

            for( int i = iChn ; i < iTotal; i++)
            {
                pNetEH->iChannel = i;
                pNetEH->uiRowColNum = (18<<16)|22;
                memcpy(pNetEH->iRegion,pCfg->getConfig(i).mRegion,MD_REGION_ROW*sizeof(int));                
                pPacket->PutBuffer( pNetEH, sizeof(NET_REGION) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();
            
            delete pNetEH;
        }

        out_list.push_back(pTlvOut);
    }

    delete pCfg;
    delete pWorksheet;

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}

int IVNSysConfig::QueryBindCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    CConfigBlindDetect*pCfg = new CConfigBlindDetect();
    pCfg->update();
    
    CConfigBLDWorksheet * pWorksheet = new CConfigBLDWorksheet;
    pWorksheet->update();

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
        
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(ALARM_GUARD) );
            
            for( int i = iChn ; i < iTotal; i++)
            {
                ALARM_GUARD Alarm_guard;

                Alarm_guard.iChannel = i;
                Alarm_guard.iEnable = pCfg->getConfig(i).bEnable;
                Alarm_guard.iGeneral =pCfg->getConfig(i).iLevel;
                
                pPacket->PutBuffer( &Alarm_guard, sizeof(ALARM_GUARD));
            }

            pTlvOut->SetType( pTlv->GetType());
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;

            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_EVENT_HANDLER) );

            for( int i = iChn ; i < iTotal; i++)
            {
                pNetEH->iChannel =i;
                EVENT_HANDLER_2_EVENT_HANDLER_NET(pCfg->getConfig(i).hEvent, pNetEH->stEventHandler);
                pPacket->PutBuffer( pNetEH, sizeof(NET_EVENT_HANDLER) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            
            pPacket->Release();

            delete pNetEH;
        }
        else if( pTlv->GetType() == ALARM_WORKSTREET_TYPE)
        {
            NET_WORKSHEET* pNetEH = new NET_WORKSHEET;

            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_WORKSHEET) );

            for( int i = iChn ; i < iTotal; i++)
            {
                CONFIG_WORKSHEET_2_NET_WORKSHEET(i, pWorksheet->getConfig(i), *pNetEH);
                pPacket->PutBuffer( pNetEH, sizeof(NET_WORKSHEET) );
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

            pPacket->Release();
            
            delete pNetEH;
        }

        out_list.push_back(pTlvOut);
    }

    delete pCfg;
    delete pWorksheet;


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

//    trace("tlv_out-len:%d\n", tlv_out.GetLength() );
    return 0;
}
int IVNSysConfig::ModAlarmInCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    CConfigAlarm *pAlarm = new CConfigAlarm();
    pAlarm->update();    
    
    CConfigALMWorksheet * pWorksheet = new CConfigALMWorksheet;
    pWorksheet->update();
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pNetAlarm = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pAlarm->getConfig(pNetAlarm->iChannel).bEnable = pNetAlarm->iEnable;
                pAlarm->getConfig(pNetAlarm->iChannel).iSensorType = pNetAlarm->iGeneral;
            }    

        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pNetAlarm = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i));
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pNetAlarm->stEventHandler, pAlarm->getConfig(pNetAlarm->iChannel).hEvent);
            }

        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            
            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pNetAlarm = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pNetAlarm->iChannel, *pNetAlarm, pWorksheet->getConfig(pNetAlarm->iChannel));
            }    
        }

    }

    pAlarm->commit();
    ret = pWorksheet->commit();
    delete pAlarm;
    
    delete pWorksheet;
    
    return 0;
}


int IVNSysConfig::ModLossCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    CConfigLossDetect *pCfgLoss = new CConfigLossDetect();
    pCfgLoss->update();
    
    CConfigVLTWorksheet * pWorksheet = new CConfigVLTWorksheet;
    pWorksheet->update();
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pCfg = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pCfgLoss->getConfig(pCfg->iChannel).enable = pCfg->iEnable;
            }

        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pCfg = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pCfg->stEventHandler, pCfgLoss->getConfig(pCfg->iChannel).handler);
            }
        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pCfg = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pCfg->iChannel, *pCfg, pWorksheet->getConfig(pCfg->iChannel) );
            }
        }

    }

    pCfgLoss->commit();
    pWorksheet->commit();

    delete pCfgLoss;

    delete pWorksheet;    
    
    return 0;
}


int IVNSysConfig::ModMotionCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    CConfigMotionDetect *pCfgMtd = new CConfigMotionDetect();

    CConfigMTDWorksheet * pWorksheet = new CConfigMTDWorksheet;

    pCfgMtd->update();
    pWorksheet->update();
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pCfg = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );

                pCfgMtd->getConfig(pCfg->iChannel).bEnable = pCfg->iEnable;
                pCfgMtd->getConfig(pCfg->iChannel).iLevel = pCfg->iGeneral;
            }
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pCfg = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pCfg->stEventHandler, pCfgMtd->getConfig(pCfg->iChannel).hEvent);
            }
            
        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pCfg = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pCfg->iChannel, *pCfg, pWorksheet->getConfig(pCfg->iChannel) );
            }
            
        }
        else if(pTlv->GetType() == ALARM_MOTION_REGION_TYPE)
        {
            num = pTlv->GetLength()/sizeof(NET_REGION);
            for(int i = num; i > 0; i-- )
            {
                NET_REGION* pCfg = (NET_REGION*)( pTlv->GetValueBuf()+sizeof(NET_REGION)*(num-i) );
                memcpy(pCfgMtd->getConfig(pCfg->iChannel).mRegion,pCfg->iRegion,MD_REGION_ROW*sizeof(int));
            }
        }

    }

    ret = pCfgMtd->commit();
    pWorksheet->commit();
    delete pCfgMtd;
    
    delete pWorksheet;    
    
    return 0;
}    

    
int IVNSysConfig::ModBindCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    CConfigBlindDetect *pCfgBind = new CConfigBlindDetect();
    pCfgBind->update();
    CConfigBLDWorksheet * pWorksheet = new CConfigBLDWorksheet;
    pWorksheet->update();

    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pCfg = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pCfgBind->getConfig(pCfg->iChannel).bEnable = pCfg->iEnable;
                pCfgBind->getConfig(pCfg->iChannel).iLevel = pCfg->iGeneral;
            }
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pCfg = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pCfg->stEventHandler, pCfgBind->getConfig(pCfg->iChannel).hEvent);
            }
        }
        else if(pTlv->GetType() == ALARM_WORKSTREET_TYPE )
        {
            
            num = pTlv->GetLength()/sizeof(NET_WORKSHEET);
            for(int i = num; i > 0; i-- )
            {
                NET_WORKSHEET* pCfg = (NET_WORKSHEET*)( pTlv->GetValueBuf()+sizeof(NET_WORKSHEET)*(num-i) );
                NET_WORKSHEET_2_CONFIG_WORKSHEET( pCfg->iChannel, *pCfg, pWorksheet->getConfig(pCfg->iChannel) );
            }
        }

    }

    pCfgBind->commit();
    pWorksheet->commit();
    delete pCfgBind;
    delete pWorksheet;     
    return 0;
}

int IVNSysConfig::GetAlarmCfg(CTlv& tlv_in, CTlv& tlv_out,CONFIG_GENERIC_EVENT& cfg)
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
                
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {

            ALARM_GUARD Alarm_guard;
            memset(&Alarm_guard,0,sizeof(ALARM_GUARD));
            
            Alarm_guard.iEnable = cfg.enable;

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)&Alarm_guard, sizeof(Alarm_guard), false );
            

        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;
            
            pNetEH->iChannel =0;
            EVENT_HANDLER_2_EVENT_HANDLER_NET(cfg.handler, pNetEH->stEventHandler);

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_EVENT_HANDLER), false );
            
            delete pNetEH;
        }
        else 
        {
            tracef("NO worksheet\n");
        }

        out_list.push_back(pTlvOut);
    }


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;

}
int IVNSysConfig::QueryNoDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out)
{    
    CConfigStorageNotExist *pStNotExist = new CConfigStorageNotExist();
    assert(pStNotExist);
    pStNotExist->update();
    CONFIG_GENERIC_EVENT& cfg = pStNotExist->getConfig();
    
    GetAlarmCfg(tlv_in, tlv_out,cfg);

    delete pStNotExist;
    
    have_tlv_out = true;

    return 0;
}    
    

int IVNSysConfig::QueryDiskErrCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigStorageFailure *pStDikErr = new CConfigStorageFailure();
    assert(pStDikErr);
    pStDikErr->update();
    CONFIG_GENERIC_EVENT& cfg = pStDikErr->getConfig();
    
    GetAlarmCfg(tlv_in, tlv_out,cfg);

    delete pStDikErr;

    have_tlv_out = true;

    return 0;
}
    

int IVNSysConfig::QueryNetBrokenCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{    
    CConfigNetAbort *pStNetAbort = new CConfigNetAbort();
    assert(pStNetAbort);
    pStNetAbort->update();
    CONFIG_GENERIC_EVENT& cfg = pStNetAbort->getConfig();
    
    GetAlarmCfg(tlv_in, tlv_out,cfg);

    delete pStNetAbort;

    have_tlv_out = true;

    return 0;
}


int IVNSysConfig::QueryIPConfictCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigNetArp*pStNetARP = new CConfigNetArp();
    assert(pStNetARP);
    pStNetARP->update();
    CONFIG_GENERIC_EVENT& cfg = pStNetARP->getConfig();
    
    GetAlarmCfg(tlv_in, tlv_out,cfg);

    delete pStNetARP;

    have_tlv_out = true;
    return 0;
}

//int CmdsDefaultOperation::OnReqPTZCfg(const int iThrdIdx)  
//!请求云台通道配置
int IVNSysConfig::QueryPtzPresetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{    
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;    

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }
    have_tlv_out = true;

    TLV_LIST& tmp_list = reader.GetTlvList();
    
    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = pChannel->usTotal;
            iTotal = pChannel->usTotal;
        }
        
        if( pTlv->GetType() == PTZ_QUERY_PRESET_TYPE )
        {
            if(pChannel->usIndex == pChannel->usTotal )//返回所有通道预置点信息
            {
                int i = 0;                                
                //如果下载预置点信息文件成功
                if(true == g_Ptz.PTZ_LoadPresetInfoFile())
                {
                    NET_PRESET_INFOR *pPresetInfoArray = new NET_PRESET_INFOR[PTZ_PRESETNUM * g_nCapture];//存放所有通道下的所有预置点信息的数组            
                    memset(pPresetInfoArray, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM * g_nCapture);
                    unsigned short iTotalPresetNums = 0;//所有通道总的有效预置点数
                    for(i = 0; i < g_nCapture; i++)
                    {                                                
                        NET_PRESET_INFOR onePresetInfoArray[PTZ_PRESETNUM];//存放该通道下的所有预置点信息的数组            
                        memset(onePresetInfoArray, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);
                        
                        memcpy(onePresetInfoArray, g_Ptz.PTZ_GetPreset(i), sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);

                        if(NULL != onePresetInfoArray)//获取该通道下的所有预置点信息成功
                        {
                            //统计第i通道下的预置点数
                            for(i = 0; i < PTZ_PRESETNUM; i++)
                            {
                                int iPresetID = 0;//预置点号
                                iPresetID = onePresetInfoArray[i].ucPresetId; 
                                if((iPresetID > 0) && (iPresetID <= PTZ_PRESETNUM))  
                                {
                                    memcpy(&pPresetInfoArray[iTotalPresetNums], &onePresetInfoArray[i], sizeof(NET_PRESET_INFOR));
                                    iTotalPresetNums++;
                                }
                            }                
                        }
                    }
                    
                    CPacket* pPacket = g_PacketManager.GetPacket(iTotalPresetNums*sizeof(NET_PRESET_INFOR) );
                    pPacket->PutBuffer( (char *)pPresetInfoArray, iTotalPresetNums*sizeof(NET_PRESET_INFOR));
                    pTlvOut->SetType( pTlv->GetType() );
                    pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                    pPacket->Release();

                    delete []pPresetInfoArray;
                }        
            }
            else//返回通道usIndexChannel下的所有预置点信息
            {            
                //如果下载预置点信息文件成功
                if(true == g_Ptz.PTZ_LoadPresetInfoFile())
                {
                    unsigned short iPresetNums = 0;//该通道下的有效预置点数
                    NET_PRESET_INFOR actualPresetInfoArray[PTZ_PRESETNUM];//存放该通道下的有效预置点信息的数组    
                    memset(actualPresetInfoArray, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);
                    
                    NET_PRESET_INFOR PresetInfoArray[PTZ_PRESETNUM];//存放该通道下的所有预置点信息的数组            
                    memset(PresetInfoArray, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);                
                    memcpy(PresetInfoArray, g_Ptz.PTZ_GetPreset(pChannel->usIndex), sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);
                    
                    if(NULL != PresetInfoArray)//获取该通道下的所有预置点信息成功
                    {    
                        int iPresetID = 0;//预置点号
                        //统计第usIndexChannel通道下的预置点数
                        for(int i = 0; i < PTZ_PRESETNUM; i++)
                        {
                            iPresetID = PresetInfoArray[i].ucPresetId; 
                            if((iPresetID > 0) && (iPresetID <= PTZ_PRESETNUM))  
                            {
                                memcpy(&actualPresetInfoArray[iPresetNums], &PresetInfoArray[i], sizeof(NET_PRESET_INFOR));
                                iPresetNums++;
                            }
                        }
                    }
                    
                    CPacket* pPacket = g_PacketManager.GetPacket(iPresetNums*sizeof(NET_PRESET_INFOR) );
                    pPacket->PutBuffer( (char *)actualPresetInfoArray, iPresetNums*sizeof(NET_PRESET_INFOR));
                    pTlvOut->SetType( pTlv->GetType() );
                    pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                    pPacket->Release();
                }            
            }                            
        }        
        out_list.push_back(pTlvOut);
    }

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}

//!设置云台通道配置    
int IVNSysConfig::ModPTZPresetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        if( pTlv->GetType() == PTZ_ADD_PRESET_TYPE )
        {
            NET_PRESET_INFOR pszPresetInfo;
            memset(&pszPresetInfo, 0, sizeof(NET_PRESET_INFOR));
            pTlv->GetValue((unsigned char*)&pszPresetInfo, sizeof(NET_PRESET_INFOR));

            //设置预置点
            if(false == g_Ptz.PTZ_AddPreset(pszPresetInfo.ucChannel,&pszPresetInfo))
            {
                //m_DVRIP.dvrip_p[4] = ENM_ERROR;
                //SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
                return -1;
            }
            
            g_Ptz.PTZ_SavePresetInfoFile(pszPresetInfo.ucChannel);//保存预置点信息文件
            g_Ptz.PTZ_SaveTourInfoFile(pszPresetInfo.ucChannel);//保存巡航线路信息文件
            return 0;
        }
        else if( pTlv->GetType() == PTZ_CLEAR_PRESET_TYPE )
        {
            char cTmpClearPresetArray[(PTZ_PRESETNUM + 1)* sizeof(int)];
            memset(cTmpClearPresetArray, 0, sizeof(int) * (PTZ_PRESETNUM + 1));
            pTlv->GetValue((unsigned char*)cTmpClearPresetArray, sizeof(int) * (PTZ_PRESETNUM + 1));

            int iClearPresetArray[PTZ_PRESETNUM];
            memset(iClearPresetArray, 0, sizeof(int) * PTZ_PRESETNUM);

            unsigned short usChannel = 0;
            memcpy(&usChannel, cTmpClearPresetArray, 2);
            memcpy(iClearPresetArray, cTmpClearPresetArray + 4, sizeof(int) * PTZ_PRESETNUM);
                        
            int iClearPresetNums = 0;
            iClearPresetNums = ((pTlv->GetLength()) - 4) / sizeof(int);
            for(int i = 0; i < iClearPresetNums; i++)
            {
                int iPresetID = 0;
                memcpy(&iPresetID, &iClearPresetArray[i], 4);    
                if(false == g_Ptz.PTZ_DeletePreset(usChannel, iPresetID))
                {
                    /*delete pParsePdu;
                    m_DVRIP.dvrip_p[4] = ENM_ERROR;
                    SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
                    */
                    return -1;
                }
            }
            
            g_Ptz.PTZ_SavePresetInfoFile(usChannel);//保存预置点信息文件
            g_Ptz.PTZ_SaveTourInfoFile(usChannel);//保存巡航线路信息文件
        }        
    }
    
    return 0;
}
int IVNSysConfig::QueryPtzTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;    

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }
    have_tlv_out = true;

    TLV_LIST& tmp_list = reader.GetTlvList();
    
    TLV_LIST::iterator it;    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = pChannel->usTotal;
            iTotal = pChannel->usTotal;
        }
        
        if( pTlv->GetType() == PTZ_QUERY_TOUR_TYPE)
        {
            if(pChannel->usIndex == pChannel->usTotal)//返回所有通道下的所有巡航线路信息
            {
                int i = 0;
                int j = 0;
                int iTotalTourNums = 0;//所有通道下总的有效巡航线路数
                //如果下载巡航线路信息成功
                if(true == g_Ptz.PTZ_LoadTourInfoFile())
                {                    
                    NET_TOUR_INFOR *pTourInfoArray = new NET_TOUR_INFOR[PTZ_PRESETNUM * g_nCapture];//存放所有通道下的所有预置点信息的数组            
                    memset(pTourInfoArray, 0, sizeof(NET_TOUR_INFOR) * PTZ_CHANNELS * g_nCapture);
                    for(i = 0; i < g_nCapture; i++)
                    {
                        for(j = 0; j < PTZ_CHANNELS; j++)
                        {
                            NET_TOUR_INFOR stTourInfo;
                            memset(&stTourInfo, 0, sizeof(NET_TOUR_INFOR));
                            memcpy(&stTourInfo, g_Ptz.PTZ_GetTourInfo(i, j), sizeof(NET_TOUR_INFOR));
                            if(0 != stTourInfo.ucPresetCnt)//该巡航线路下有预置点
                            {
                                memcpy(&pTourInfoArray[iTotalTourNums], &stTourInfo, sizeof(NET_TOUR_INFOR));
                                iTotalTourNums++;
                            }
                        }
                    }
                    CPacket* pPacket = g_PacketManager.GetPacket(iTotalTourNums*sizeof(NET_TOUR_INFOR) );
                    pPacket->PutBuffer( (char *)pTourInfoArray, iTotalTourNums*sizeof(NET_TOUR_INFOR));
                    pTlvOut->SetType( pTlv->GetType() );
                    pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                    pPacket->Release();

                    delete []pTourInfoArray;
                }
            }            
            else//返回通道usIndexTourChannel下的有效巡航线路信息
            {
                NET_TOUR_INFOR TourInfoArray[PTZ_CHANNELS];//存放该通道下的所有有效巡航线路信息             
                memset(TourInfoArray, 0, sizeof(NET_TOUR_INFOR) * PTZ_CHANNELS);
        
                //如果下载巡航线路信息成功
                if(true == g_Ptz.PTZ_LoadTourInfoFile())
                {
                    int actualTourNums = 0;
                    for(int i = 0; i < PTZ_CHANNELS; i++)
                    {
                        NET_TOUR_INFOR oneTourInfo;
                        memset(&oneTourInfo, 0, sizeof(NET_TOUR_INFOR));
                        memcpy(&oneTourInfo, g_Ptz.PTZ_GetTourInfo(pChannel->usIndex, i), sizeof(NET_TOUR_INFOR));
                        if(0 != oneTourInfo.ucPresetCnt)//该巡航线路下有预置点
                        {
                            memcpy(&TourInfoArray[actualTourNums], &oneTourInfo, sizeof(NET_TOUR_INFOR));
                            actualTourNums++;
                        }
                    }
                    CPacket* pPacket = g_PacketManager.GetPacket(actualTourNums*sizeof(NET_TOUR_INFOR) );
                    pPacket->PutBuffer( (char *)TourInfoArray, actualTourNums*sizeof(NET_TOUR_INFOR));
                    pTlvOut->SetType( pTlv->GetType() );
                    pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );
                    pPacket->Release();
                }        
            }                            
        }        
        out_list.push_back(pTlvOut);
    }

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}

int IVNSysConfig::ModPtzTourCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        PTZ_OPT_STRUCT ptzOptionCmd;
        memset(&ptzOptionCmd, 0, sizeof(PTZ_OPT_STRUCT));
        int iRet = 0;
        if( pTlv->GetType() == PTZ_ADD_TOUR_PRESET_TYPE )
        {
            unsigned short usChannel = 0;
            unsigned short usIndex = 0;

            char addTourPresetBuf[sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM + 4];
            memset(addTourPresetBuf, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM + 4);
            NET_PRESET_INFOR addTourPresetArray[PTZ_PRESETNUM];
            memset(addTourPresetArray, 0, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);
            pTlv->GetValue((unsigned char*)addTourPresetBuf, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM + 4);
            memcpy(&usChannel, addTourPresetBuf, 2);
            memcpy(&usIndex, addTourPresetBuf + 2, 2);
            memcpy(addTourPresetArray, addTourPresetBuf + 4, sizeof(NET_PRESET_INFOR) * PTZ_PRESETNUM);

            int iAddTourPresetNums = 0;
            iAddTourPresetNums = ((pTlv->GetLength()) - 4) / sizeof(NET_PRESET_INFOR);
            for(int i = 0; i < iAddTourPresetNums; i++)
            {
                ptzOptionCmd.cmd = PTZ_OPT_ADDTOUR;
                ptzOptionCmd.arg1 = usIndex - 1;
                ptzOptionCmd.arg2 = addTourPresetArray[i].ucPresetId;
                ptzOptionCmd.arg3 = addTourPresetArray[i].ucDWellTime;
                ptzOptionCmd.reserved = addTourPresetArray[i].ucSpeed;

                iRet = g_Ptz.PTZ_OnPtzTour(ptzOptionCmd, usChannel);
                if(0 == iRet)
                {
                    g_Ptz.PTZ_SaveTourInfoFile(usChannel);
                    g_Ptz.PTZ_SavePresetInfoFile(usChannel);
                }
                else//失败
                {
                /*
                    delete pParsePdu;
                    m_DVRIP.dvrip_p[4] = ENM_ERROR;
                    SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
                */
                    return -1;            
                }
            }
            /*
            delete pParsePdu;
            m_DVRIP.dvrip_p[4] = ENM_SUCESS;
            int iRet = SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
            if(iRet < 0)
            {
                tracef("file[%s] line[%d]============= SendBackMessage fail!\r\n",__FILE__,__LINE__);
            }
            tracef("file[%s] line[%d]============= SendBackMessage success!\r\n",__FILE__,__LINE__);
            */
            return 0;
        }
        else if( pTlv->GetType() == PTZ_CLEAR_TOUR_PRESET_TYPE )
        {
            unsigned short usChannel = 0;
            unsigned short usIndex = 0;
            
            char cClearTourPresetBuf[sizeof(int) * PTZ_PRESETNUM + 4];
            memset(cClearTourPresetBuf, 0, sizeof(int) * PTZ_PRESETNUM + 4);
            int iClearTourPresetArray[PTZ_PRESETNUM];
            memset(iClearTourPresetArray, 0, sizeof(int) * PTZ_PRESETNUM);
            pTlv->GetValue((unsigned char*)cClearTourPresetBuf, sizeof(int) * PTZ_PRESETNUM + 4);        
            memcpy(&usChannel, cClearTourPresetBuf, 2);
            memcpy(&usIndex, cClearTourPresetBuf + 2, 2);
            memcpy(iClearTourPresetArray, cClearTourPresetBuf + 4, sizeof(int) * PTZ_PRESETNUM);

            int iClearTourPresetNums = 0;
            iClearTourPresetNums = ((pTlv->GetLength())- 4) / sizeof(int);
            for(int i = 0; i < iClearTourPresetNums; i++)
            {
                ptzOptionCmd.cmd = PTZ_OPT_DELTOUR;
                ptzOptionCmd.arg1 = usIndex - 1;
                memcpy(&ptzOptionCmd.arg2, &iClearTourPresetArray[i], 4);
                iRet = g_Ptz.PTZ_OnPtzTour(ptzOptionCmd, usChannel);
                if(0 == iRet)
                {
                    g_Ptz.PTZ_SaveTourInfoFile(usChannel);
                    g_Ptz.PTZ_SavePresetInfoFile(usChannel);
                }
                else//失败
                {
                /*
                    delete pParsePdu;
                    m_DVRIP.dvrip_p[4] = ENM_ERROR;
                    SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
                */
                    return -1;            
                }
            }
            /*
            delete pParsePdu;
            m_DVRIP.dvrip_p[4] = ENM_SUCESS;
            int iRet = SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
            if(iRet < 0)
            {
                tracef("file[%s] line[%d]============= SendBackMessage fail!\r\n",__FILE__,__LINE__);
            }
            tracef("file[%s] line[%d]============= SendBackMessage success!\r\n",__FILE__,__LINE__);
            */
            return 0;
        }
        else if( pTlv->GetType() == PTZ_CLEAR_TOUR_TYPE)
        {
            unsigned short usChannel = 0;
            unsigned short usIndex = 0;
            
            DEV_PTZ_CHANNEL* pPTZChannel = (DEV_PTZ_CHANNEL*)pTlv->GetValueBuf();
            memcpy(&usChannel, &pPTZChannel->usChannel, 2);
            memcpy(&usIndex, &pPTZChannel->usIndex, 2);
            ptzOptionCmd.cmd = PTZ_OPT_CLEARTOUR;
            ptzOptionCmd.arg1 = usIndex - 1;
            iRet = g_Ptz.PTZ_OnPtzTour(ptzOptionCmd, usChannel);
            //清除巡航线路成功，则保存文件
            if(0 == iRet)
            {
                g_Ptz.PTZ_SaveTourInfoFile(usChannel);
                g_Ptz.PTZ_SavePresetInfoFile(usChannel);
                /*
                delete pParsePdu;
                m_DVRIP.dvrip_p[4] = ENM_SUCESS;
                int iRet = SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
                if(iRet < 0)
                {
                    tracef("file[%s] line[%d]============= SendBackMessage fail!\r\n",__FILE__,__LINE__);
                }
                tracef("file[%s] line[%d]============= SendBackMessage success!\r\n",__FILE__,__LINE__);
                */
                return 0;
            } 
            else//失败
            {
            /*
                delete pParsePdu;
                m_DVRIP.dvrip_p[4] = ENM_ERROR;
                SendBackMessage(iThrdIdx, m_pMsg->iDlgNo, (uchar *)&m_DVRIP, DVRIP_HEAD_T_SIZE);
            */
                return -1;            
            }
        }
        else
        {
            tracef("no this sub command!\r\n");
        }
        
    }
    
    return 0;
}
int IVNSysConfig::SetAlarmCfg(CTlv& tlv_in, CTlv& tlv_out,CONFIG_GENERIC_EVENT& cfg)
{
    int ret = 0 ;
    TlvReader reader;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    

    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pNetAlarm = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                cfg.enable = pNetAlarm->iEnable;
            }

        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pNetAlarm = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pNetAlarm->stEventHandler,cfg.handler);
            }
        }


    }

    return 0;
}

int IVNSysConfig::ModNoDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{    
    CConfigStorageNotExist *pStNotExist = new CConfigStorageNotExist();
    assert(pStNotExist);
    pStNotExist->update();
    CONFIG_GENERIC_EVENT& cfg = pStNotExist->getConfig();
    
    SetAlarmCfg(tlv_in, tlv_out,cfg);
    
    pStNotExist->commit();

    delete pStNotExist;

    have_tlv_out = false;

    return 0;

}
int IVNSysConfig::ModDiskErrCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigStorageFailure *pStDikErr = new CConfigStorageFailure();
    assert(pStDikErr);
    pStDikErr->update();
    CONFIG_GENERIC_EVENT& cfg = pStDikErr->getConfig();
    
    SetAlarmCfg(tlv_in, tlv_out,cfg);

    pStDikErr->commit();

    delete pStDikErr;

    have_tlv_out = false;

    return 0;

}
int IVNSysConfig::ModNetBrokenCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigNetAbort *pStNetAbort = new CConfigNetAbort();
    assert(pStNetAbort);
    pStNetAbort->update();
    CONFIG_GENERIC_EVENT& cfg = pStNetAbort->getConfig();
    
    SetAlarmCfg(tlv_in, tlv_out,cfg);

    pStNetAbort->commit();
    delete pStNetAbort;

    have_tlv_out = false;
    
    return 0;

}
int IVNSysConfig::ModIPConfictCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigNetArp*pStNetARP = new CConfigNetArp();
    assert(pStNetARP);
    pStNetARP->update();
    CONFIG_GENERIC_EVENT& cfg = pStNetARP->getConfig();
    
    SetAlarmCfg(tlv_in, tlv_out,cfg);
    
    pStNetARP->commit();
    delete pStNetARP;

    have_tlv_out = false;

    return 0;
}

int IVNSysConfig::QueryDiskFullCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    CConfigStorageLowSpace *pCfg = new CConfigStorageLowSpace();
    pCfg->update();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
                
        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            ALARM_GUARD Alarm_guard;
            memset(&Alarm_guard,0,sizeof(ALARM_GUARD));
            
            Alarm_guard.iEnable = pCfg->getConfig().enable;
            Alarm_guard.iGeneral = pCfg->getConfig().lowerLimit;

            pTlvOut->SetType( pTlv->GetType());
            pTlvOut->SetValue( (uchar*)&Alarm_guard, sizeof(Alarm_guard), false );
            
        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            NET_EVENT_HANDLER* pNetEH = new NET_EVENT_HANDLER;
                        
            pNetEH->iChannel =0;
            EVENT_HANDLER_2_EVENT_HANDLER_NET(pCfg->getConfig().handler, pNetEH->stEventHandler);

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pNetEH, sizeof(NET_EVENT_HANDLER), false );
            
            delete pNetEH;
        }

        out_list.push_back(pTlvOut);
    }

    delete pCfg;

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
//        trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}    

int IVNSysConfig::ModDiskFullCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    CConfigStorageLowSpace *pCfg = new CConfigStorageLowSpace();
    pCfg->update();

    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;

        if( pTlv->GetType() == ALARM_ENABLE_TYPE )
        {
            num = pTlv->GetLength()/sizeof(ALARM_GUARD);
            for(int i = num; i > 0; i-- )
            {
                ALARM_GUARD* pNetAlarm = (ALARM_GUARD*)( pTlv->GetValueBuf()+sizeof(ALARM_GUARD)*(num-i) );
                pCfg->getConfig().enable = pNetAlarm->iEnable;
                pCfg->getConfig().lowerLimit = pNetAlarm->iGeneral;
            }

        }
        else if( pTlv->GetType() == ALARM_EVENT_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_EVENT_HANDLER);
            for(int i = num; i > 0; i-- )
            {
                NET_EVENT_HANDLER* pNetAlarm = (NET_EVENT_HANDLER*)( pTlv->GetValueBuf()+sizeof(NET_EVENT_HANDLER)*(num-i) );
                EVENT_HANDLER_NET_2_EVENT_HANDLER(pNetAlarm->stEventHandler, pCfg->getConfig().handler);
            }
        }

    }

    pCfg->commit();
    delete pCfg;

    return 0;
}    

int IVNSysConfig::QueryDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true ;
    
    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
                
        if( pTlv->GetType() == DEV_SYSTEM_INFO_DISKINFO )
        {
#if 0
            IDE_INFO64 ideinfor64; //协议需要修改，同时要考虑兼容性！！！！
            
            g_DriverManager.GetIdeInfo(&ideinfor64);

            NET_DISK_INFO NetDiskInfo;
            CPacket* pPacket = g_PacketManager.GetPacket(ideinfor64.ide_num*sizeof(NET_DISK_INFO) );

            for(int iIndex = 0; iIndex <ideinfor64.ide_num;iIndex++)
            {
                memset(&NetDiskInfo,0, sizeof(NET_DISK_INFO));

                DRIVER_INFO drivinfor;
                
                NetDiskInfo.ucLocalStorage = 0;   
                NetDiskInfo.ucDiskType =0;
                NetDiskInfo.ucPartMethod =0x3 ;
                NetDiskInfo.ucSupportPartionMax = 2;
                NetDiskInfo.ucSupportFileSystem = BITMSK(0);

                NetDiskInfo.ucSpaceType = BITMSK(1)|BITMSK(5);

                uint uidriver_type = 0;      
                g_DriverManager.GetDriverType(iIndex,0,&uidriver_type);

                if(3 == uidriver_type)
                {
                    NetDiskInfo.ucAttr = 2;  // 冗余盘
                }else if(4 == uidriver_type)
                {
                    NetDiskInfo.ucAttr = 0 ;
                }
                else
                {
                    NetDiskInfo.ucAttr = uidriver_type;
                }
                int iTotal = 0,iRemain = 0;;
                
                for(int i = 0;i <MAX_PARTITIONS;i++)
                {
                    int ret = g_DriverManager.GetDriverInfo(iIndex, i, &drivinfor);
                    if(0 == i)
                    {
                        NetDiskInfo.ucDiskNo = iIndex;//drivinfor.index;
                        NetDiskInfo.ucCurrentUse =  drivinfor.is_current;
                        NetDiskInfo.ucErrorFlag = drivinfor.error_flag;
                    }

                 
                    if(ret)
                    {
                        g_DriverManager.GetDriverType(iIndex,i,&uidriver_type);    

                         if(4 == uidriver_type)
                        {
                            NetDiskInfo.szPartion[i].ucPartType = 1;
                        }
                         else
                        {
                            NetDiskInfo.szPartion[i].ucPartType = 0;
                        }

                        iTotal += drivinfor.total_space;
                        iRemain += drivinfor.remain_space;
                        
                        NetDiskInfo.szPartion[i].ucUse = 1;
                        NetDiskInfo.szPartion[i].ucPartion = i;
                        NetDiskInfo.szPartion[i].ucFSType = 0;
                         
                        if(drivinfor.total_space > (50 * BITMSK(10)))
                        {
                            NetDiskInfo.szPartion[i].uiTotalSpace = drivinfor.total_space/BITMSK(10);
                            NetDiskInfo.szPartion[i].ucSpaceType = 2<<4;
                        }
                         else
                        {
                            NetDiskInfo.szPartion[i].uiTotalSpace =drivinfor.total_space;
                            NetDiskInfo.szPartion[i].ucSpaceType = 1<<4;
                        }
                        
                          if(drivinfor.remain_space> (50 * BITMSK(10)))
                        {
                            NetDiskInfo.szPartion[i].uiRemainSpace = drivinfor.remain_space/BITMSK(10);
                            NetDiskInfo.szPartion[i].ucSpaceType |= 2;
                        }
                         else
                        {
                            NetDiskInfo.szPartion[i].uiRemainSpace = drivinfor.remain_space;
                            NetDiskInfo.szPartion[i].ucSpaceType |= 1;
                        }

                    }
                    else
                    {
                        break;
                    }

                }

                if ( iTotal> (50 * BITMSK(10)))
                {
                    NetDiskInfo.uiTotalSpace = iTotal/BITMSK(10);
                    NetDiskInfo.ucSpaceType = 2<<4;
                }
                else
                {
                    NetDiskInfo.uiTotalSpace = iTotal;
                    NetDiskInfo.ucSpaceType  = 1<<4;
                }
                
                  if(iRemain > (50 * BITMSK(10)))
                {
                    NetDiskInfo.uiRemainSpace = iRemain/BITMSK(10);
                    NetDiskInfo.ucSpaceType |=2;
                }
                else
                {
                    NetDiskInfo.uiRemainSpace = iRemain;
                    NetDiskInfo.ucSpaceType |= 1;
                }
                
                pPacket->PutBuffer(&NetDiskInfo,sizeof(NET_DISK_INFO));
            }
            
            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( pPacket->GetBuffer(),pPacket->GetLength(), false );

            pPacket->Release();
#else
            NET_DISK_INFO NetDiskInfo;
            CPacket* pPacket = g_PacketManager.GetPacket(sizeof(NET_DISK_INFO) );

            for(int iIndex = 0; iIndex < 1;iIndex++)
            {
                memset(&NetDiskInfo,0, sizeof(NET_DISK_INFO));

                DRIVER_INFO drivinfor;
                
                NetDiskInfo.ucLocalStorage = 0;   
                NetDiskInfo.ucDiskType =0;
                NetDiskInfo.ucPartMethod =0x3 ;
                NetDiskInfo.ucSupportPartionMax = 2;
                NetDiskInfo.ucSupportFileSystem = BITMSK(0);

                NetDiskInfo.ucSpaceType = BITMSK(1)|BITMSK(5);

                uint uidriver_type = 0;      
                g_DriverManager.GetDriverType(iIndex,0,&uidriver_type);

                if(3 == uidriver_type)
                {
                    NetDiskInfo.ucAttr = 2;  // 冗余盘
                }else if(4 == uidriver_type)
                {
                    NetDiskInfo.ucAttr = 0 ;
                }
                else
                {
                    NetDiskInfo.ucAttr = uidriver_type;
                }
                int iTotal = 0,iRemain = 0;;
                
 
                int ret = g_DriverManager.GetDriverInfo(iIndex, 0, &drivinfor);
     
                {
                    NetDiskInfo.ucDiskNo = iIndex;//drivinfor.index;
                    NetDiskInfo.ucCurrentUse =  1;//drivinfor.is_current;
                    NetDiskInfo.ucErrorFlag = drivinfor.error_flag;
                }

             
                if(ret)
                {
                    g_DriverManager.GetDriverType(iIndex,0,&uidriver_type);    

                    iTotal += drivinfor.total_space;
                    iRemain += drivinfor.remain_space;
                }
                else
                {
                    break;
                }

          

                if ( iTotal> (50 * BITMSK(10)))
                {
                    NetDiskInfo.uiTotalSpace = (iTotal)/BITMSK(10);
				//	printf("uiTotalSpace:%f",NetDiskInfo.uiTotalSpace);
                    NetDiskInfo.ucSpaceType = 2<<4;
                }
                else
                {
                    NetDiskInfo.uiTotalSpace = iTotal;
                    NetDiskInfo.ucSpaceType  = 1<<4;
                }
                
                  if(iRemain > (50 * BITMSK(10)))
                {
                    NetDiskInfo.uiRemainSpace = (iRemain)/BITMSK(10);
					//printf("uiRemainSpace:%f",NetDiskInfo.uiRemainSpace);
                    NetDiskInfo.ucSpaceType |=2;
                }
                else
                {
                    NetDiskInfo.uiRemainSpace = iRemain;
                    NetDiskInfo.ucSpaceType |= 1;
                }
                
                pPacket->PutBuffer(&NetDiskInfo,sizeof(NET_DISK_INFO));
            }
            
            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( pPacket->GetBuffer(),pPacket->GetLength(), false );

            pPacket->Release();
#endif

        }

        out_list.push_back(pTlvOut);
    }


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}
int IVNSysConfig::ModDiskCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;

	//CUser* pUser  = (CUser* )((struct conn *)m_pConn)->context;
#ifdef __BELL_QZTEL__
	SesionInfo_t *sess = (SesionInfo_t *)((struct conn *)m_pConn)->context;
	CUser *pUser = (CUser*)sess->user;
#else	
	CUser* pUser  = (CUser* )((struct conn *)m_pConn)->context;
#endif
    if(!pUser->isValidAuthority("StorageManager") )
    {
    	trace("Usr have no Authority to operate harddisk!!!!!!!!!!\n");
		return -1;
    }
    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        if( pTlv->GetType() == DEV_SYSTEM_CTRL_DISKINFO )
        {
            NET_CTRL_DISK_INFO* pNetCfg = (NET_CTRL_DISK_INFO*)(pTlv->GetValueBuf());
            
            IDE_INFO64 harddriver_tmp;
            g_DriverManager.GetIdeInfo(&harddriver_tmp);
            int iSubDev = pNetCfg->ucDiskNo;        

            DRIVER_INFO di;
            memset(&di, 0 ,sizeof(DRIVER_INFO));
            g_DriverManager.GetDriverInfo(iSubDev,0,&di);

            if(di.is_current)
            {                
                if(!g_Record.IsThreadOver())
                {
                    g_Record.Stop();
                }

                g_Snap.stop();    
            }
            
            switch(pNetCfg->ucCtrlType)
            {
                case 1://设置为只读盘
                {
                    g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_ONLY);
                    break;
                }
                case 2://设置为冗余盘
                {
                    IDE_INFO64 ideinfor64; //协议需要修改，同时要考虑兼容性！！！！   
                    g_DriverManager.GetIdeInfo(&ideinfor64);
                    int  iRWDiskNum = 0;
                    for(int iIndex = 0;iIndex <ideinfor64.ide_num;iIndex++)
                    {
                        uint uidriver_type = 0;      
                        g_DriverManager.GetDriverType(iIndex,0,&uidriver_type);
                        if(DRIVER_READ_WRITE == uidriver_type)
                        {
                            iRWDiskNum ++;
                        }
                    }
                    if(iRWDiskNum > 1)
                    {
                        if(g_DriverManager.SetDriverType(iSubDev,0,DRIVER_REDUNDANT))
                        {
                            g_Log.Append(LOG_HDD_TYPE, iSubDev);
                        }
                    }                 
                    break;
                }
                case 3://清楚数据
                {
                    if (g_DriverManager.FormatDisk(iSubDev))
                    {
                        g_Log.Append(LOG_HDD_FORMAT, iSubDev);
                    }
                    break;                    
                }
                case 4://恢复错误
                {
                    g_DriverManager.ParoleDriver(iSubDev,0);
                    break;
                }
                case 5:  //分区
                {
                    g_DriverManager.FormatDisk(iSubDev);    //为什么VS100 发送的是设备 1? 需要修改
                    if(0 == pNetCfg->ucPartMethod)
                    {
                        if(0 == pNetCfg->uiPartSpace[0])
                        {
                            g_DriverManager.SetDriverType(iSubDev,0,DRIVER_SNAPSHOT);
                            ret = 2;

                            break;
                        }

                        //对发过了的分区数据进行处理
                        if(pNetCfg->uiPartSpace[0] >= 100)
                        {
                            pNetCfg->uiPartSpace[0] = 100;
                            pNetCfg->uiPartSpace[1] = 0;
                            pNetCfg->uiPartSpace[2] = 0;
                            pNetCfg->uiPartSpace[3] = 0;
                        }
                        else if(pNetCfg->uiPartSpace[0] < 100)
                        {
                            pNetCfg->uiPartSpace[1] = 100 -pNetCfg->uiPartSpace[0];
                            pNetCfg->uiPartSpace[2] = 0;
                            pNetCfg->uiPartSpace[3] = 0;                                
                        }                
                        
                        uint64 sectors = 0;    //每一个分区占硬盘总容量的百分比
                        PARTITION_INFO strPartitionInfo[MAX_PARTITIONS]; //分区信息                
                        memset(strPartitionInfo, 0, sizeof(PARTITION_INFO) * MAX_PARTITIONS);

                        IDE_INFO64 strIdeInfo;
                        ide_getinfo64(&strIdeInfo);
                        uint64 idecap = strIdeInfo.ide_cap[iSubDev];    
                        int bool_snap = 0;    //增加一块快照分区

                        for(int i = 0; i < MAX_PARTITIONS; i++)
                        {
                            if(0 < pNetCfg->uiPartSpace[i] && 100 >= pNetCfg->uiPartSpace[i])
                            {
                                strPartitionInfo[i].start_sector = sectors;
                                strPartitionInfo[i].total_sector = idecap * (pNetCfg->uiPartSpace[i])/ 100;
                                if(i>=1)
                                    bool_snap = 1;
                            }
                            else
                            {
                                strPartitionInfo[i].start_sector = 0;
                                strPartitionInfo[i].total_sector = 0;
                            }
                            sectors += strPartitionInfo[i].total_sector;
                        }
                        //wfs_write_partitions(iSubDev, strPartitionInfo, MAX_PARTITIONS); 

                        infof("dev is %d, type %d, %d  %d , bool_snap = %d.\n\n", 
                            iSubDev, pNetCfg->ucPartType, pNetCfg->uiPartSpace[0], pNetCfg->uiPartSpace[1], bool_snap);
                        if (bool_snap)
                        {
                            g_DriverManager.FormatDriver(iSubDev, 0);
                            g_DriverManager.FormatDriver(iSubDev, 1);
                        }
                        else
                        {
                            g_DriverManager.FormatDisk(iSubDev);
                        }

                        if(bool_snap)
                        {
                            g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_WRITE);
                            g_DriverManager.SetDriverType(iSubDev,1,DRIVER_SNAPSHOT);//第2块分区设置为快照分区
                        }
                        else
                        {
                            if(pNetCfg->ucPartType == 1)
                            {
                                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_SNAPSHOT);
                            }
                            else if(pNetCfg->ucPartType == 0)
                            {
                                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_WRITE);
                            }
                        }

                       // wfs_array_driver();       
                        ret = 2;
#ifdef SDK_3512	 //3512设备SD卡分区失败					
		  SystemSleep(30*1000);	
#endif
                    }
                    else
                    {                    
                        tracepoint();
                        return -1;
                    }                    
                    break;
                }
                case 0://设置为读写盘
                default:
                {
                    tracepoint();
                    g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_WRITE);
                    break;                    
                }
            }            

            g_DriverManager.IsSnapExist();

            if(di.is_current && (pNetCfg->ucCtrlType != 5))
            {
                if(g_Record.IsThreadOver())
                {
                    g_Record.Start(); // 重新开始录像
                }
		#ifdef ENC_SUPPORT_SNAPSHOT
			#ifdef SNAP_NET_THREAD_MULTI
			g_Snap.SynSnapCfg();	
			#else
			g_Snap.start();	
			#endif
		#endif
            }
        }
        else if( pTlv->GetType() == DEV_SYSTEM_CTRL_DISKPARTION)
        {
            NET_CTRL_DISK_PARTION* pNetCfg = (NET_CTRL_DISK_PARTION*)(pTlv->GetValueBuf());
            if(pNetCfg->ucPartCtrlType == 0)
            {
                g_DriverManager.FormatDriver(pNetCfg->ucDiskNo,pNetCfg->ucPartNo);
            }
        }

    }

    return ret;
}

int IVNSysConfig::QueryIntervideoCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;
    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
        out_list.push_back(pTlvOut);
    }


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}
int IVNSysConfig::ModIntervideoCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int iRet = 0 ;
    TlvReader reader;

    have_tlv_out = false;

    iRet = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( iRet < 0 )
    {
        return -1;
    }

    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;

    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
    }
    return iRet;  //返回2 重启设备
}


int IVNSysConfig::QueryGeneralNetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    uint uiTlvType = tlv_in.GetType();

    have_tlv_out = true;    

    if(DEV_CONFIG_TYPE_NET == uiTlvType)
    {
        CONFIG_NET_APP NetAppCfg;

        CConfigNetCommon *pNetCfg = new CConfigNetCommon;
        pNetCfg->update();
        CONFIG_NET_COMMON& cfg = pNetCfg->getConfig();

        memcpy(NetAppCfg.HostName,cfg.HostName,sizeof(cfg.HostName) -1);
        NetAppCfg.HttpPort = cfg.HttpPort;
        NetAppCfg.TCPPort = cfg.TCPPort;
        NetAppCfg.SSLPort = cfg.SSLPort;
        NetAppCfg.UDPPort = cfg.UDPPort;
        NetAppCfg.MaxConn = cfg.MaxConn;
        NetAppCfg.ucMonMode = cfg.MonMode;
        NetAppCfg.ucUseTransPlan = cfg.bUseTransferPlan;
        NetAppCfg.ucTransPlan = cfg.TransferPlan;
        NetAppCfg.ucHighSpeedDownload = cfg.bUseHSDownLoad;
        
        tlv_out.SetType(DEV_CONFIG_TYPE_NET);
        tlv_out.SetValue((uchar*)&NetAppCfg, sizeof(CONFIG_NET_APP),false);

        delete pNetCfg;
        
    }        
    else if( DEV_CONFIG_TYPE_ETHERNET == uiTlvType )
    {
        CONFIG_NET_ETH NetEthCfg;
        memset(&NetEthCfg,0,sizeof(NetEthCfg));

        strncpy(NetEthCfg.strEthName,"eth0",16);
        NetGetMAC("eth0", NetEthCfg.strMacAddr,32);
    
        IPInfo info;
        g_NetApp.GetNetIPInfo(NULL, &info,TRUE);
        NetEthCfg.HostIP.l= info.HostIP.l;
        NetEthCfg.Submask.l =info.SubMask.l;
        NetEthCfg.Gateway.l = info.GateWay.l;
        NetEthCfg.ucTranMedia = 0;
        NetEthCfg.ucDefaultEth = 1;
        NetEthCfg.ucValid = 1;
#ifdef _NET_USE_DHCPCLIENT
        NetEthCfg.ucValid |=BITMSK(2);
        int enable = 0;
        NetGetDhcpEnable(&enable);
        if(enable)
        {
            NetEthCfg.ucValid &= ~BITMSK(0);
            NetEthCfg.ucValid |= BITMSK(1);
        }

#endif                            
        tlv_out.SetType(DEV_CONFIG_TYPE_ETHERNET);
        tlv_out.SetValue((uchar*)&NetEthCfg,sizeof(CONFIG_NET_ETH), false);        
        
    }
    else if( DEV_CONFIG_TYPE_DNS == uiTlvType)
    {
        DNS_IP_LIST DNSIPListCfg;

#ifndef WIN32
        NetGetDNSAddress(DNSIPListCfg.strPrimaryIp, 16,DNSIPListCfg.strSecondIp, 16);
#endif
        tracef("[Print by vfware] ip1 = %s,ip2 = %s\n",DNSIPListCfg.strPrimaryIp,DNSIPListCfg.strSecondIp);
    
        tlv_out.SetType(DEV_CONFIG_TYPE_DNS);
        tlv_out.SetValue((uchar*)&DNSIPListCfg,sizeof(DNS_IP_LIST), false);        
        
    }    
    else if( DEV_CONFIG_TYPE_PPPOE ==uiTlvType)
    {
        PPPOE_IP_SRV PppoeIPSRVCfg;
        memset(&PppoeIPSRVCfg, 0, sizeof(PPPOE_IP_SRV));

        NET_PPPOE_CONFIG pppoeCfg;
        memset(&pppoeCfg, 0, sizeof(NET_PPPOE_CONFIG));
        NetGetPPPoEConfig(&pppoeCfg);

        PppoeIPSRVCfg.iEnable = pppoeCfg.enable;
        strncpy(PppoeIPSRVCfg.strUser,pppoeCfg.username,NAME_PASSWORD_LEN);
        strncpy(PppoeIPSRVCfg.strPwd,pppoeCfg.password,NAME_PASSWORD_LEN);
        
        NetGetDialIpAddress(PppoeIPSRVCfg.strip);
        
        tlv_out.SetType(DEV_CONFIG_TYPE_PPPOE);
        tlv_out.SetValue((uchar*)&PppoeIPSRVCfg,sizeof(PPPOE_IP_SRV), false );    
        
    }
    else
    {
        have_tlv_out = false;
    }
	
    return 0;

}

extern INT ReadFacInfo(SAVENANDINFO_T *pOutFacInfo);
extern INT WriteFacInfo(SAVENANDINFO_T *pInFacInfo);
int IVNSysConfig::ModGeneralNetCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    uint uiTlvType = tlv_in.GetType();
    int iRet = 0;//-1;
    if(DEV_CONFIG_TYPE_NET == uiTlvType)
    {
	CONFIG_NET_APP* pNetConfig = (CONFIG_NET_APP*)tlv_in.GetValueBuf();
	CConfigNetCommon *pCfgNetCommon = new CConfigNetCommon();
	pCfgNetCommon->update();    
	CONFIG_NET_COMMON& cfgComm = pCfgNetCommon->getConfig();
	_printd("==============devname = %s\n",pNetConfig->HostName);
	strcpy(cfgComm.HostName , pNetConfig->HostName);        
	if(pNetConfig->HttpPort)
	    cfgComm.HttpPort = pNetConfig->HttpPort;
	if(pNetConfig->TCPPort)
	    cfgComm.TCPPort = pNetConfig->TCPPort;
	if(pNetConfig->SSLPort)
	    cfgComm.SSLPort = pNetConfig->SSLPort;
	if(pNetConfig->UDPPort)
	    cfgComm.UDPPort = pNetConfig->UDPPort;
	cfgComm.MaxConn = pNetConfig->MaxConn;
	cfgComm.MonMode = pNetConfig->ucMonMode;
	cfgComm.bUseTransferPlan= pNetConfig->ucUseTransPlan;
	cfgComm.TransferPlan = pNetConfig->ucTransPlan;
	cfgComm.bUseHSDownLoad = pNetConfig->ucHighSpeedDownload;
        
	pCfgNetCommon->commit();
        
	SAVENANDINFO_T FacInfo = {0};
	ReadFacInfo(&FacInfo);
	strncpy(FacInfo.devName, cfgComm.HostName, 23);
	WriteFacInfo(&FacInfo );
	delete pCfgNetCommon;
    }
    else if( DEV_CONFIG_TYPE_ETHERNET == uiTlvType )
    {

        CONFIG_NET_ETH* pNetConfig = (CONFIG_NET_ETH*)tlv_in.GetValueBuf();
        
        IPInfo netipinfo;
        if(pNetConfig->HostIP.l)
        {
            netipinfo.HostIP.l = pNetConfig->HostIP.l;
        }
        if(pNetConfig->Submask.l)
        {
            netipinfo.SubMask.l=pNetConfig->Submask.l;
        }
        if(pNetConfig->Gateway.l)
        {
            netipinfo.GateWay.l= pNetConfig->Gateway.l;
        }

        iRet =g_NetApp.SetNetIPInfo(NULL, &netipinfo,2);

        /* 返回-1会导致重启，保存失败时返回校验出错 */
        if (iRet < 0)
        {
            iRet = CONFIG_APPLY_VALIT_ERROR;    
        }
                
#if _NET_USE_DHCPCLIENT
        if( (pNetConfig->ucValid & BITMSK(2))
            && (pNetConfig->ucValid & BITMSK(1)))
        {
            NetSetDhcpEnable(1);
        }else
        {
            NetSetDhcpEnable(0);
        }
#endif        
    }
    else if( DEV_CONFIG_TYPE_DNS == uiTlvType)
    {    
#ifndef WIN32
        DNS_IP_LIST* pNetConfig = (DNS_IP_LIST*)tlv_in.GetValueBuf();

        DNS_IP_LIST stOldIpList;
        memset(&stOldIpList,0,sizeof(stOldIpList));
        
        NetGetDNSAddress(stOldIpList.strPrimaryIp,16,stOldIpList.strSecondIp,16);
        
        if (strncmp(stOldIpList.strPrimaryIp, pNetConfig->strPrimaryIp, 16) 
            ||strncmp(stOldIpList.strSecondIp, pNetConfig->strSecondIp, 16) )
        {
            NetSetDNSAddress(pNetConfig->strPrimaryIp,pNetConfig->strSecondIp);
            res_init();
            //iRet |=CONFIG_APPLY_REBOOT;
        }
        
#endif        
    }
    else if( DEV_CONFIG_TYPE_PPPOE ==uiTlvType)
    {
        PPPOE_IP_SRV* pNetConfig = (PPPOE_IP_SRV*)tlv_in.GetValueBuf();

        NET_PPPOE_CONFIG OldpppoeCfg;
        memset(&OldpppoeCfg,0,sizeof(OldpppoeCfg));
        NetGetPPPoEConfig(&OldpppoeCfg); 

    int iRet_usr = 0;
    int iRet_pwd = 0;
    iRet_usr = strncmp(OldpppoeCfg.username,pNetConfig->strUser, NAME_PASSWORD_LEN);
    iRet_pwd = strncmp(OldpppoeCfg.password, pNetConfig->strPwd, NAME_PASSWORD_LEN);
    if(OldpppoeCfg.enable && (!pNetConfig->iEnable))
    {
        printf("pppoe stop...\r\n");
        system("setsid /usr/sbin/pppoe-stop &");//转移到后台 by ilena zhou 2010-04-08
        system("setsid /usr/sbin/pppoe-stop &");
    }
    else if((!OldpppoeCfg.enable) && pNetConfig->iEnable)
    {
        printf("pppoe start...\r\n");
        system("setsid /usr/sbin/pppoe-start &");
    }
    else if((OldpppoeCfg.enable && pNetConfig->iEnable) && ((iRet_usr != 0) || (iRet_pwd != 0)) )
    {//enable状态下，只是改变用户名或者密码 by ilena zhou 2010-04-08
        printf("\r\nCall Restart...\r\n");
        system("setsid /usr/sbin/pppoe-restart &");
    } 
    else
    {
        printf("whassat????\r\n");
    }
  
        if(OldpppoeCfg.enable != pNetConfig->iEnable 
            || strcmp(pNetConfig->strUser, OldpppoeCfg.username)
            || strcmp(pNetConfig->strPwd,OldpppoeCfg.password))
        {

            NET_PPPOE_CONFIG pppoeCfg;
            memset(&pppoeCfg,0,sizeof(pppoeCfg));
            pppoeCfg.enable = pNetConfig->iEnable;
            strncpy(pppoeCfg.username,pNetConfig->strUser,NAME_PASSWORD_LEN);
            strncpy(pppoeCfg.password,pNetConfig->strPwd,NAME_PASSWORD_LEN);

            if (NetSetPPPoEConfig(&pppoeCfg) != 0)
            {
                    iRet |= CONFIG_APPLY_VALIT_ERROR;
            }
        }
        
    }

    return iRet;

}

int IVNSysConfig::QueryNetAPPCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    uint uiTlvType = tlv_in.GetType();

    have_tlv_out = true;    

    if(DEV_CONFIG_TYPE_MAIL == uiTlvType)
    {
        CONFIG_EMAIL NetAppCfg;
            
        CConfigNetEmail ConfigNetserver;
        ConfigNetserver.update();
        CONFIG_NET_EMAIL &configNetserver = ConfigNetserver.getConfig();

        NetAppCfg.Server.Enable = configNetserver.Enable;
        memcpy(NetAppCfg.Server.Server.ServerName,configNetserver.Server.ServerName,sizeof(configNetserver.Server.ServerName)-1);
        memcpy(NetAppCfg.Server.Server.UserName,configNetserver.Server.UserName,sizeof(configNetserver.Server.UserName)-1);
        memcpy(NetAppCfg.Server.Server.Password,configNetserver.Server.Password,sizeof(configNetserver.Server.Password)-1);
        NetAppCfg.Server.Server.ip = configNetserver.Server.ip;
        NetAppCfg.Server.Server.Port = configNetserver.Server.Port;
        NetAppCfg.Server.Server.Anonymity = configNetserver.Server.Anonymity;
        NetAppCfg.bUseSSL = configNetserver.bUseSSL;
        memcpy(NetAppCfg.SendAddr,configNetserver.SendAddr,sizeof(configNetserver.SendAddr)-1);
        memcpy(NetAppCfg.Title,configNetserver.Title,sizeof(NetAppCfg.Title)-1);

        for(int i =0; i < 3; i++)
        {
            memcpy(NetAppCfg.Recievers[i],configNetserver.Recievers[i],EMAIL_ADDR_LEN-1);

        }

	 NetAppCfg.uiPicturesChs = configNetserver.dwAccePic;
		
        tlv_out.SetType(DEV_CONFIG_TYPE_MAIL);
        tlv_out.SetValue((uchar*)&NetAppCfg, sizeof(CONFIG_EMAIL),false);
        
    }        
    else if( DEV_CONFIG_TYPE_NTP == uiTlvType )
    {
        CONFIG_NTP NetAppCfg;
        
        CConfigNetNTP *pCfgNetNtp = new CConfigNetNTP();
        pCfgNetNtp->update();
        CONFIG_NET_NTP& cfgNtp = pCfgNetNtp->getConfig();    

        NetAppCfg.szNtpServer.Enable = cfgNtp.Enable;
        strcpy(NetAppCfg.szNtpServer.Server.ServerName,cfgNtp.Server.ServerName);
        strcpy(NetAppCfg.szNtpServer.Server.UserName,cfgNtp.Server.UserName);
        strcpy(NetAppCfg.szNtpServer.Server.Password,cfgNtp.Server.Password);
        NetAppCfg.szNtpServer.Server.Port = cfgNtp.Server.Port;
        NetAppCfg.szNtpServer.Server.ip = cfgNtp.Server.ip;
        NetAppCfg.TimeZone = cfgNtp.TimeZone;
        NetAppCfg.UpdatePeriod = cfgNtp.UpdatePeriod;
        
        tlv_out.SetType(DEV_CONFIG_TYPE_NTP);
        tlv_out.SetValue((uchar*)&NetAppCfg,sizeof(CONFIG_NTP), false);        

        delete pCfgNetNtp;
        
    }
    else if( DEV_CONFIG_TYPE_UPNP == uiTlvType)
    {
        CONFIG_UPNP NetAppCfg;
        
        CConfigNetUPNP ConfigUpnp;
        ConfigUpnp.update();
        CONFIG_NET_UPNP &cfgnetupnp = ConfigUpnp.getConfig();        

        NetAppCfg.Enable = cfgnetupnp.Enable;
        NetAppCfg.TCPPort = cfgnetupnp.TCPPort;
        NetAppCfg.WebPort = cfgnetupnp.WebPort;
    
        tlv_out.SetType(DEV_CONFIG_TYPE_UPNP);
        tlv_out.SetValue((uchar*)&NetAppCfg,sizeof(CONFIG_UPNP), false);        
        
    }    
    else if( DEV_CONFIG_TYPE_MULTI_DDNS ==uiTlvType)
    {

    }
    else if( DEV_CONFIG_TYPE_AUTO_REGISTER ==uiTlvType)
    {
            
    }
    else if( DEV_CONFIG_TYPE_AUTOREG_EX ==uiTlvType)
    {
    
    }    
    else if( DEV_CFG_TYPE_DECODE ==uiTlvType)
    {
        CPacket* pPacket = g_PacketManager.GetPacket( m_pCaptureM->GetDigitalChnNum()*sizeof(CFG_DECODE_T) );
        if( pPacket == NULL )
        {
            trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
            return CONFIG_APPLY_VALIT_ERROR;
        }

        CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
        CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
        assert(ptmpLocal);
        assert(ptmpRemote);
        
        ptmpLocal->update();
        ptmpRemote->update();
        
        for( int iIndex = 0; iIndex < m_pCaptureM->GetLogicChnNum(); iIndex++ )
        {
            if( !m_pCaptureM->IsSupportDigital(iIndex) )
            {
                continue;
            }

            LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(iIndex);
            REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(iIndex);

            unsigned int uiIndex = stLocalCfg.uiNotTourCfgIndex;
            uiIndex  = (uiIndex >= MAX_TOUR_NUM)?0:uiIndex;
        
            CFG_DECODE_T NetAppCfg;
            memset( &NetAppCfg, 0, sizeof(CFG_DECODE_T) );

            NetAppCfg.channel = iIndex;
            NetAppCfg.device_type = stRemoteCfg.stRemoteCfg[uiIndex].iDevType;
            NetAppCfg.device_port = stRemoteCfg.stRemoteCfg[uiIndex].iDevicePort;
            NetAppCfg.device_ip =stRemoteCfg.stRemoteCfg[uiIndex].DeviceIP.l;

            NetAppCfg.device_channel = stRemoteCfg.stRemoteCfg[uiIndex].iRemoteChannelNo;
            NetAppCfg.enable = stLocalCfg.BEnable;
            NetAppCfg.stream_type = stRemoteCfg.stRemoteCfg[uiIndex].iStreamType;

            memcpy(NetAppCfg.username, stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName, sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName));
            memcpy(NetAppCfg.password,stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword, sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword));

            pPacket->PutBuffer(&NetAppCfg,sizeof(CFG_DECODE_T));
            
        } 

        delete ptmpLocal;
        ptmpLocal = NULL;

        delete ptmpRemote;
        ptmpRemote = NULL;
            
        tlv_out.SetType(DEV_CFG_TYPE_DECODE);
        tlv_out.SetValue(pPacket->GetBuffer(),pPacket->GetLength(), false );    

        pPacket->Release();
        
    }
    else if( DEV_CFG_TYPE_DECODE_EX == uiTlvType)
    {
        CPacket* pPacket = g_PacketManager.GetPacket( m_pCaptureM->GetDigitalChnNum()*sizeof(CFG_DECODE_EX) );
        if( pPacket == NULL )
        {
            trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
            return CONFIG_APPLY_VALIT_ERROR;
        }

        CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
        CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
        assert(ptmpLocal);
        assert(ptmpRemote);
        
        for( int iIndex = 0; iIndex < m_pCaptureM->GetLogicChnNum(); iIndex++ )
        {
            if( !m_pCaptureM->IsSupportDigital(iIndex) )
            {
                continue;
            }

            LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(iIndex);
            REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(iIndex);

            VD_UINT32 uiIndex = (stLocalCfg.uiNotTourCfgIndex>=MAX_TOUR_NUM)?0:stLocalCfg.uiNotTourCfgIndex;
            
            CFG_DECODE_EX NetAppCfg;
            memset( &NetAppCfg, 0, sizeof(CFG_DECODE_EX) );

            NetAppCfg.ucChannel = (uchar)iIndex;
            NetAppCfg.ucAVEnable = (VD_UINT8)stLocalCfg.iAVEnable;
            NetAppCfg.ucRemotePtzPreset = (VD_UINT8)stRemoteCfg.stRemoteCfg[uiIndex].iRemotePtzPreset;
            NetAppCfg.ucRemotePtzPresetEnable = (VD_UINT8)stRemoteCfg.stRemoteCfg[uiIndex].iRemotePtzPresetEnable;
            NetAppCfg.ucConType = (VD_UINT8)stRemoteCfg.stRemoteCfg[uiIndex].iConType;
            NetAppCfg.iDecodePolicy = stLocalCfg.iDecodePolicy;
            NetAppCfg.ucLocalPtz = (uchar)stLocalCfg.BPTZLocal;
            pPacket->PutBuffer(&NetAppCfg,sizeof(CFG_DECODE_EX));
        }
        
        delete ptmpLocal;
        ptmpLocal = NULL;

        delete ptmpRemote;
        ptmpRemote = NULL;
        
        tlv_out.SetType(DEV_CFG_TYPE_DECODE_EX);
        tlv_out.SetValue(pPacket->GetBuffer(),pPacket->GetLength(), false );    

        pPacket->Release();
    }
#ifdef FUNC_FTP_UPLOAD
    else if( DEV_CFG_TYPE_NET_FTP_SERVER == uiTlvType)
    {
        CConfigFTPServer cConfig;
        cConfig.update();
        
        CPacket* pPacket = g_PacketManager.GetPacket( 2*sizeof(FTP_SERVER_SET) );
        if( pPacket == NULL )
        {
            trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
            return CONFIG_APPLY_VALIT_ERROR;
        }

        for( int iIndex = 0; iIndex < 2; iIndex++ )
        {
            CONFIG_FTP_SERVER &cfg = cConfig.getConfig(iIndex);
            FTP_SERVER_SET stFtpSet;
            memset(&stFtpSet,0,sizeof(FTP_SERVER_SET));
            
            stFtpSet.stServer.Enable = cfg.Enable;
            stFtpSet.stServer.Server.Anonymity = cfg.Server.Anonymity,
            stFtpSet.stServer.Server.Port = cfg.Server.Port;
            stFtpSet.iType = iIndex;
            
            memcpy(stFtpSet.stServer.Server.ServerName,cfg.Server.ServerName,NAME_PASSWORD_LEN);
            memcpy(stFtpSet.cDirName,cfg.RemotePathName,VD_MAX_PATH);
            memcpy(stFtpSet.stServer.Server.UserName,cfg.Server.UserName,MAX_FTP_USERNAME_LEN);
            memcpy(stFtpSet.stServer.Server.Password,cfg.Server.Password,MAX_FTP_PASSWORD_LEN);

            stFtpSet.iFileLen = cfg.FileMaxLen;
            //stFtpSet.iInterval = cfg.;
            
            pPacket->PutBuffer(&stFtpSet,sizeof(FTP_SERVER_SET));
        }
        
        tlv_out.SetType(DEV_CFG_TYPE_NET_FTP_SERVER);
        tlv_out.SetValue(pPacket->GetBuffer(),pPacket->GetLength(), false );
        pPacket->Release();
    }
    else if( DEV_CFG_TYPE_NET_FTP_APP_TIME == uiTlvType)
    {
        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)tlv_in.GetValueBuf();

        if ( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }
        else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
        
        CConfigFTPApplication cConfig;
        cConfig.update();
        
        CPacket* pPacket = g_PacketManager.GetPacket((iTotal-iChn)*sizeof(FTP_SERVER_SET));

        if( pPacket == NULL )
        {
            trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
            return CONFIG_APPLY_VALIT_ERROR;
        }
    
        for(int iIndex = iChn;(iIndex < iTotal) && (iIndex < g_nLogicNum);iIndex++)
        {
            for(int ii=0;ii<2;ii++)			
            {
                CONFIG_FTP_APPLICATION &cfg = cConfig.getConfig(iIndex + ii*N_SYS_CH);
                    
                FTP_APP_TIME_SET  stFtpAppTime;
                memset(&stFtpAppTime,0,sizeof(FTP_APP_TIME_SET));

                stFtpAppTime.usCh   = iIndex;
                stFtpAppTime.usType = ii;

                for(int j = 0; j < N_WEEKS; j ++)
                {
                    for(int k = 0; k < N_MIN_TSECT; k++)
                    {
                        stFtpAppTime.stSect[j][k].StartHour = (char)cfg.RecordPeriodSet[j][k].Tsect.startHour;
                        stFtpAppTime.stSect[j][k].StartMin  = (char)cfg.RecordPeriodSet[j][k].Tsect.startMinute;
                        stFtpAppTime.stSect[j][k].StartSec  = (char)cfg.RecordPeriodSet[j][k].Tsect.startSecond;
                        stFtpAppTime.stSect[j][k].EndHour   = (char)cfg.RecordPeriodSet[j][k].Tsect.endHour;
                        stFtpAppTime.stSect[j][k].EndMin    = (char)cfg.RecordPeriodSet[j][k].Tsect.endMinute;
                        stFtpAppTime.stSect[j][k].EndSec    = (char)cfg.RecordPeriodSet[j][k].Tsect.endSecond;

                        if(cfg.RecordPeriodSet[j][k].StateMask.En[EnGeneral])
                            stFtpAppTime.stSect[j][k].State |= 1<<1;
                        if(cfg.RecordPeriodSet[j][k].StateMask.En[EnAlarm])
                            stFtpAppTime.stSect[j][k].State |= 1<<3;
                        if(cfg.RecordPeriodSet[j][k].StateMask.En[EnMotion])
                            stFtpAppTime.stSect[j][k].State |= 1<<2;
                    }
                }

                pPacket->PutBuffer(&stFtpAppTime,sizeof(FTP_APP_TIME_SET));
            }
        }

        tlv_out.SetType(DEV_CFG_TYPE_NET_FTP_APP_TIME);
        tlv_out.SetValue(pPacket->GetBuffer(),pPacket->GetLength(), false );
        pPacket->Release();
    }
#endif    
#ifdef FUNCTION_SUPPORT_RTSP
    else if( DEV_CFG_TYPE_NET_RTSP == uiTlvType)
    {
        CPacket* pPacket = g_PacketManager.GetPacket( sizeof(RTSP_SET) );
        if( pPacket == NULL )
        {
            trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
            return CONFIG_APPLY_VALIT_ERROR;
        }
        
        CConfigNetRtspSet cRtspConfig;
        cRtspConfig.update();
        RTSP_SET &stRtspSet = cRtspConfig.getConfig();
               
        pPacket->PutBuffer(&stRtspSet,sizeof(RTSP_SET));
        tlv_out.SetType(DEV_CFG_TYPE_NET_RTSP);
        tlv_out.SetValue(pPacket->GetBuffer(),pPacket->GetLength(), false );    

        pPacket->Release();
    }
#endif
    else
    {
        have_tlv_out = false;
    }

    return 0;
}

int IVNSysConfig::ModNetAPPCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    uint uiTlvType = tlv_in.GetType();

    if(DEV_CONFIG_TYPE_MAIL == uiTlvType)
    {
        CONFIG_EMAIL* pNetConfig = (CONFIG_EMAIL*)tlv_in.GetValueBuf();

        CConfigNetEmail cConfigNetserver ;
        cConfigNetserver.update();
        CONFIG_NET_EMAIL &cfgold = cConfigNetserver.getConfig();

        cfgold.Enable = pNetConfig->Server.Enable;
        memcpy(cfgold.Server.ServerName,pNetConfig->Server.Server.ServerName,NAME_PASSWORD_LEN);
        memcpy(cfgold.Server.UserName,pNetConfig->Server.Server.UserName,NAME_PASSWORD_LEN);
        memcpy(cfgold.Server.Password,pNetConfig->Server.Server.Password,NAME_PASSWORD_LEN);
        cfgold.Server.ip = pNetConfig->Server.Server.ip;
        cfgold.Server.Port = pNetConfig->Server.Server.Port;
        cfgold.Server.Anonymity = pNetConfig->Server.Server.Anonymity;
        cfgold.bUseSSL = pNetConfig->bUseSSL;
        memcpy(cfgold.SendAddr,pNetConfig->SendAddr,EMAIL_ADDR_LEN);
        memcpy(cfgold.Title,pNetConfig->Title,MAX_EMAIL_TITLE_LEN);

	 int iIndex = 0;
        for (int i = 0; i < 3; ++i)/*recipient*/
        {
        	 memset( cfgold.Recievers[i], 0, EMAIL_ADDR_LEN);
            if(strlen(pNetConfig->Recievers[i]))
            {
                memcpy(cfgold.Recievers[iIndex],pNetConfig->Recievers[i], EMAIL_ADDR_LEN-1);
		  iIndex++;
            }
        }

	cfgold.dwAccePic = pNetConfig->uiPicturesChs;

        cConfigNetserver.commit();
    
    }
    else if( DEV_CONFIG_TYPE_NTP == uiTlvType )
    {
        CONFIG_NTP* pNetAppCfg = (CONFIG_NTP*)tlv_in.GetValueBuf();
        
        CConfigNetNTP cCfgNetNtp ;
        cCfgNetNtp.update();
        CONFIG_NET_NTP& cfgNtp = cCfgNetNtp.getConfig();    

        cfgNtp.Enable = pNetAppCfg->szNtpServer.Enable;
        strcpy(cfgNtp.Server.ServerName,pNetAppCfg->szNtpServer.Server.ServerName);
        strcpy(cfgNtp.Server.UserName,pNetAppCfg->szNtpServer.Server.UserName);
        strcpy(cfgNtp.Server.Password,pNetAppCfg->szNtpServer.Server.Password);
        cfgNtp.Server.Port =pNetAppCfg->szNtpServer.Server.Port;
        cfgNtp.Server.ip= pNetAppCfg->szNtpServer.Server.ip;
        cfgNtp.TimeZone = pNetAppCfg->TimeZone;
        cfgNtp.UpdatePeriod = pNetAppCfg->UpdatePeriod;

        cCfgNetNtp.commit();
        
    }
    else if( DEV_CONFIG_TYPE_UPNP == uiTlvType)
    {    

        CONFIG_UPNP* pNetAppCfg = (CONFIG_UPNP*)tlv_in.GetValueBuf();;
        
        CConfigNetUPNP cConfigUpnp;
        cConfigUpnp.update();
        CONFIG_NET_UPNP &cfgnetupnp = cConfigUpnp.getConfig();        

        cfgnetupnp.Enable = pNetAppCfg->Enable;
        cfgnetupnp.TCPPort = pNetAppCfg->TCPPort;
        cfgnetupnp.WebPort = pNetAppCfg->WebPort;        

        cConfigUpnp.commit();
    }
    else if( DEV_CONFIG_TYPE_MULTI_DDNS ==uiTlvType)
    {
    }
    else if( DEV_CONFIG_TYPE_AUTO_REGISTER ==uiTlvType)
    {    
    }
    else if( DEV_CONFIG_TYPE_AUTOREG_EX ==uiTlvType)
    {    
    }    
    else if( DEV_CFG_TYPE_DECODE ==uiTlvType)
    {
        int num = tlv_in.GetLength()/sizeof(CFG_DECODE_T);

        CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
        CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
        assert(ptmpLocal);
        assert(ptmpRemote);
        
        ptmpLocal->update();
        ptmpRemote->update();
        
        for(int iIndex = 0;iIndex < num;iIndex++)
        {
            CFG_DECODE_T* pNetConfig = (CFG_DECODE_T*)(tlv_in.GetValueBuf()+iIndex*sizeof(CFG_DECODE_T));

            if ( pNetConfig->channel >= m_pCaptureM->GetLogicChnNum() )
            {
                return 0;
            }

            LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(pNetConfig->channel);
            REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(pNetConfig->channel);

            VD_UINT32 uiIndex = (stLocalCfg.uiNotTourCfgIndex>=MAX_TOUR_NUM)?0:stLocalCfg.uiNotTourCfgIndex;
            stLocalCfg.uiNotTourCfgIndex = uiIndex;
            
            stRemoteCfg.stRemoteCfg[uiIndex].iDevType = pNetConfig->device_type;
            stRemoteCfg.stRemoteCfg[uiIndex].iDevicePort = pNetConfig->device_port;
            stRemoteCfg.stRemoteCfg[uiIndex].DeviceIP.l = pNetConfig->device_ip;

            stRemoteCfg.stRemoteCfg[uiIndex].iRemoteChannelNo = pNetConfig->device_channel;
            
            stLocalCfg.BEnable = pNetConfig->enable;
            stRemoteCfg.stRemoteCfg[uiIndex].iStreamType = pNetConfig->stream_type;

            memcpy(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName,pNetConfig->username,sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName));
            memcpy(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword,pNetConfig->password,sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword));
        }
        
        ptmpLocal->commit();
        ptmpRemote->commit();

        delete ptmpLocal;
        ptmpLocal = NULL;

        delete ptmpRemote;
        ptmpRemote = NULL;
        
    }
    else if( DEV_CFG_TYPE_DECODE_EX == uiTlvType)
    {
        int num = tlv_in.GetLength()/sizeof(CFG_DECODE_EX);

        CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
        CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
        assert(ptmpLocal);
        assert(ptmpRemote);
        
        ptmpLocal->update();
        ptmpRemote->update();
        
        for(int iIndex = 0;iIndex < num;iIndex++)
        {
            CFG_DECODE_EX* pNetConfig = (CFG_DECODE_EX*)(tlv_in.GetValueBuf()+iIndex*sizeof(CFG_DECODE_EX));

            LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(pNetConfig->ucChannel);
            REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(pNetConfig->ucChannel);

            VD_UINT32 uiIndex = (stLocalCfg.uiNotTourCfgIndex>=MAX_TOUR_NUM)?0:stLocalCfg.uiNotTourCfgIndex;
            stLocalCfg.uiNotTourCfgIndex = uiIndex;
            

            stLocalCfg.iAVEnable = pNetConfig->ucAVEnable;
            stRemoteCfg.stRemoteCfg[uiIndex].iRemotePtzPreset = pNetConfig->ucRemotePtzPreset;
            stRemoteCfg.stRemoteCfg[uiIndex].iRemotePtzPresetEnable = pNetConfig->ucRemotePtzPresetEnable;

            /* 老的协议会把上报的保留字改成0下发 */
            if (CON_PROTOCOL_TCP == pNetConfig->ucConType || CON_PROTOCOL_UDP == pNetConfig->ucConType)
            {
                stRemoteCfg.stRemoteCfg[uiIndex].iConType = (int)pNetConfig->ucConType;                
            }
            
            stLocalCfg.iDecodePolicy = pNetConfig->iDecodePolicy;
            stLocalCfg.BPTZLocal = pNetConfig->ucLocalPtz;
        }

        ptmpRemote->commit();
        ptmpLocal->commit();

        delete ptmpLocal;
        ptmpLocal = NULL;

        delete ptmpRemote;
        ptmpRemote = NULL;
    }
#ifdef FUNC_FTP_UPLOAD
    else if( DEV_CFG_TYPE_NET_FTP_SERVER == uiTlvType)
    {
        //长度不对
        if ( tlv_in.GetLength()%sizeof(FTP_SERVER_SET))
        {
            return 0;
        }
        
        int num = tlv_in.GetLength()/sizeof(FTP_SERVER_SET);

        CConfigFTPServer cConfig;
        cConfig.update();
        
        for(int iIndex = 0;iIndex < num;iIndex++)
        {
            //printf("iIndex = %d\r\n",iIndex);
            
            FTP_SERVER_SET* pNetConfig = (FTP_SERVER_SET*)(tlv_in.GetValueBuf()+iIndex*sizeof(FTP_SERVER_SET));

            if (pNetConfig->iType >= 2)
            {
                printf("pNetConfig->iType = %d\r\n",pNetConfig->iType);
                continue;
            }
            
            CONFIG_FTP_SERVER &cfg = cConfig.getConfig(pNetConfig->iType);

            cfg.Enable = pNetConfig->stServer.Enable;
            memcpy(cfg.Server.ServerName,pNetConfig->stServer.Server.ServerName,NAME_PASSWORD_LEN);
            
            cfg.Server.Port = pNetConfig->stServer.Server.Port;
            cfg.Server.Anonymity = pNetConfig->stServer.Server.Anonymity;

            memcpy(cfg.RemotePathName,pNetConfig->cDirName,VD_MAX_PATH);
            memcpy(cfg.Server.UserName,pNetConfig->stServer.Server.UserName,MAX_FTP_USERNAME_LEN);
            memcpy(cfg.Server.Password,pNetConfig->stServer.Server.Password,MAX_FTP_PASSWORD_LEN);
            cfg.FileMaxLen = pNetConfig->iFileLen;

        }

        cConfig.commit();        
    }
    else if( DEV_CFG_TYPE_NET_FTP_APP_TIME == uiTlvType)
    {
        //长度不对
        if ( tlv_in.GetLength()%sizeof(FTP_APP_TIME_SET))
        {
            return 0;
        }
            
        int num = tlv_in.GetLength()/sizeof(FTP_APP_TIME_SET);

        CConfigFTPApplication cConfig;
        cConfig.update();
        
        for(int iIndex = 0;iIndex < num;iIndex++)
        {
            FTP_APP_TIME_SET* pNetConfig = (FTP_APP_TIME_SET*)(tlv_in.GetValueBuf()+iIndex*sizeof(FTP_APP_TIME_SET));

            if (pNetConfig->usType >= 2)
            {
                continue;
            }
            
            CONFIG_FTP_APPLICATION &cfg = cConfig.getConfig(pNetConfig->usCh + pNetConfig->usType*N_SYS_CH);

            for(int j = 0; j < N_WEEKS; j ++)
            {
                for(int k = 0; k < N_MIN_TSECT; k++)
                {
                    cfg.RecordPeriodSet[j][k].Tsect.startHour = (int)pNetConfig->stSect[j][k].StartHour;
                    cfg.RecordPeriodSet[j][k].Tsect.startMinute = (int)pNetConfig->stSect[j][k].StartMin;
                    cfg.RecordPeriodSet[j][k].Tsect.startSecond = (int)pNetConfig->stSect[j][k].StartSec;
                    cfg.RecordPeriodSet[j][k].Tsect.endHour = (int)pNetConfig->stSect[j][k].EndHour;
                    cfg.RecordPeriodSet[j][k].Tsect.endMinute = (int)pNetConfig->stSect[j][k].EndMin;
                    cfg.RecordPeriodSet[j][k].Tsect.endSecond = (int)pNetConfig->stSect[j][k].EndSec;
                    
                    cfg.RecordPeriodSet[j][k].StateMask.En[EnGeneral] = (pNetConfig->stSect[j][k].State &(1<<1))?1:0;
                    cfg.RecordPeriodSet[j][k].StateMask.En[EnAlarm] = (pNetConfig->stSect[j][k].State &(1<<3))?1:0;
                    cfg.RecordPeriodSet[j][k].StateMask.En[EnMotion] = (pNetConfig->stSect[j][k].State &(1<<2))?1:0;
                }
            }
        }
        
        cConfig.commit();
    }
#endif

#ifdef FUNCTION_SUPPORT_RTSP
    else if( DEV_CFG_TYPE_NET_RTSP == uiTlvType)
    {
        //长度不对
        if ( tlv_in.GetLength() != sizeof(RTSP_SET))
        {
            return 0;
        }
        
        CConfigNetRtspSet cRtspConfig;
        cRtspConfig.update();

        RTSP_SET &stRtspSet = cRtspConfig.getConfig();
        RTSP_SET *pstNetRtspSet  = (RTSP_SET *)tlv_in.GetValueBuf();
        
        IPADDR   stMulticastIp;
        ushort    usPort; 
        
        stMulticastIp.l = stRtspSet.stMulticastIp.l;
        usPort = stRtspSet.usPort;
        
        memcpy(&stRtspSet,tlv_in.GetValueBuf(),sizeof(RTSP_SET));
        if (0 == pstNetRtspSet->stMulticastIp.l)
        {
            stRtspSet.stMulticastIp.l = stMulticastIp.l;
        }
        
        if (0 == pstNetRtspSet->usPort)
        {
            stRtspSet.usPort = usPort;
        }
#ifdef BASE2HIGH
     SetRtspPort((RTSP_CONFIG*)&stRtspSet);
#endif
        cRtspConfig.commit();
    }
#endif

    return 0;
}


int IVNSysConfig::QueryEncodeCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }

    CConfigVideoWidget* pccfgVW = new CConfigVideoWidget;
    pccfgVW->update();
    
    CConfigEncode* pCfgEncode = new CConfigEncode();
    pCfgEncode->update();

    TLV_LIST& tmp_list = reader.GetTlvList();
    
//    trace("IVNSysConfig::QueryLQueryEncodeCfgossCfg->list num:%d\n", tmp_list.size() );
    TLV_LIST::iterator it;
    
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );

//        trace("list node-type:%d\n", pTlv->GetType() );

        int iChn = 0,iTotal = 0;
        DEV_CHANNEL* pChannel = (DEV_CHANNEL*)pTlv->GetValueBuf();
        
        if( pChannel->usIndex < pChannel->usTotal )
        {
            iChn = pChannel->usIndex;
            iTotal = iChn +1;
        }else
        {
            iChn = 0;
            iTotal = pChannel->usTotal;
        }
//        trace("DEV_CHANNEL_ENCODE_TYPE-tlv type:%d\n", pTlv->GetType());
        if(pTlv->GetType() == ENCODE_CHANNEL_NAME_TYPE)
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(CHANNEL_NAME) );
            
            CConfigChannelTitle* pTitle = new CConfigChannelTitle();
            pTitle->update();

            for( int i = iChn; i < iTotal; i++ )
            {
                CHANNEL_NAME ChannelName;
                memset(&ChannelName,0,sizeof(CHANNEL_NAME));        
                ChannelName.iChannel = i;
                        memcpy(ChannelName.strChannelName,pTitle->getConfig(i).strName, CHANNEL_NAME_SIZE -1);
            
                pPacket->PutBuffer(&ChannelName, sizeof(CHANNEL_NAME));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();        

                delete pTitle;

        }
        else if(pTlv->GetType() == ENCODE_MAIN_STREAM_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_ENCODE) );

            for( int i = iChn; i < iTotal; i++ )
            {
                NET_ENCODE EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_ENCODE));    

                CONFIG_ENCODE& ConfigEncode = pCfgEncode->getConfig(i);
                EncodeCfg.iChannel = i;
                memcpy(&EncodeCfg.dstFmt,&(ConfigEncode.dstMainFmt[0]),sizeof(MEDIA_FORMAT));
                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_ENCODE));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();        
            
        }
        else if( pTlv->GetType() == ENCODE_SUB_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_3IRD_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_4RTH_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_5FTH_STREAM_TYPE )
        {
        	int stream = 0;
        	if( pTlv->GetType() == ENCODE_SUB_STREAM_TYPE )
        	{
        		stream = 0;
        	}
        	else if( pTlv->GetType() == ENCODE_3IRD_STREAM_TYPE )
        	{
        		stream = 1;
        	}
        	else if( pTlv->GetType() == ENCODE_4RTH_STREAM_TYPE )
			{
        		stream = 2;
			}
        	else if( pTlv->GetType() == ENCODE_5FTH_STREAM_TYPE )
			{
        		stream = 3;
			}

        	if( stream >= EXTRATYPES )
        	{
        		continue;
        	}

            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_ENCODE) );

            for( int i = iChn; i < iTotal; i++ )
            {
                NET_ENCODE EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_ENCODE));    

                CONFIG_ENCODE& ConfigEncode = pCfgEncode->getConfig(i);
                EncodeCfg.iChannel = i;
                memcpy(&EncodeCfg.dstFmt,&(ConfigEncode.dstExtraFmt[stream]),sizeof(MEDIA_FORMAT));
		
                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_ENCODE));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();    
            
        }
        else if( pTlv->GetType() == ENCODE_SNAP_STREAM_TYPE)
        {                        
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_ENCODE) );

            for( int i = iChn; i < iTotal; i++ )
            {
                NET_ENCODE EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_ENCODE));    

                CONFIG_ENCODE& ConfigEncode = pCfgEncode->getConfig(i);
                EncodeCfg.iChannel = i;
                    memcpy(&EncodeCfg.dstFmt,&(ConfigEncode.dstSnapFmt[0]),sizeof(MEDIA_FORMAT));

                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_ENCODE));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();    
            
        }
        else if( pTlv->GetType() == ENCODE_COLOR_TYPE )
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_VIDEOCOLOR) );

            CConfigVideoColor* pCfgVideoColor = new CConfigVideoColor();
            pCfgVideoColor->update();
            
            for( int i = iChn; i < iTotal; i++ )
            {
                NET_VIDEOCOLOR EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_VIDEOCOLOR));    

                CONFIG_VIDEOCOLOR& cfgVideoColor = pCfgVideoColor->getConfig(i);
                EncodeCfg.iChannel = i;

                memcpy(&EncodeCfg.stVideoColor,&cfgVideoColor,sizeof(CONFIG_VIDEOCOLOR));

                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_VIDEOCOLOR));
            }

            pTlvOut->SetType( pTlv->GetType());
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();

            delete pCfgVideoColor;
        }  
        else if(pTlv->GetType() == ENCODE_COVER_TYPE)   
        {
            CAPTURE_CAPS caps;
                  GetCaps(&caps);
                int iCoverNum = (caps.CoverBlocks < COVERNUM ? caps.CoverBlocks : COVERNUM);
            
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_VIDEO_COVER) );

            for( int i = iChn; i < iTotal; i++ )
            {
                NET_VIDEO_COVER EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_VIDEO_COVER));    

                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(i);
                EncodeCfg.usChannel = i;
                for(int i = 0; i < iCoverNum; i++)
                {
                    EncodeCfg.dstCovers[i].rcRelativePos= cfgvw_t.dstCovers[i].rcRelativePos;
                    EncodeCfg.dstCovers[i].rgbaFrontground= cfgvw_t.dstCovers[i].rgbaFrontground;
                    EncodeCfg.dstCovers[i].bEncodeBlend= cfgvw_t.dstCovers[i].bEncodeBlend;
                    EncodeCfg.dstCovers[i].bPreviewBlend= cfgvw_t.dstCovers[i].bPreviewBlend;
                    EncodeCfg.dstCovers[i].rgbaBackground= cfgvw_t.dstCovers[i].bEncodeBlend; /*黑色*/
                    if(true == cfgvw_t.dstCovers[i].bEncodeBlend)            
                    {
                      EncodeCfg.usCoverNum    ++;
                    }
                }            

                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_VIDEO_COVER));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();    
            
            
        }   
        else if( pTlv->GetType() == ENCODE_TIMEOSD_TYPE)
        {                        
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_VIDEO_WIDGET) );
            
            for( int i = iChn; i < iTotal; i++ )
            {
                NET_VIDEO_WIDGET EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_VIDEO_WIDGET));    

                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(i);

                EncodeCfg.usChannel = i;
                EncodeCfg.dstWidget.bEncodeBlend = cfgvw_t.TimeTitle.bEncodeBlend;
                EncodeCfg.dstWidget.bPreviewBlend = cfgvw_t.TimeTitle.bPreviewBlend;
                EncodeCfg.dstWidget.rcRelativePos.left = cfgvw_t.TimeTitle.rcRelativePos.left;
                EncodeCfg.dstWidget.rcRelativePos.top = cfgvw_t.TimeTitle.rcRelativePos.top;
                EncodeCfg.dstWidget.rcRelativePos.right = cfgvw_t.TimeTitle.rcRelativePos.right;
                EncodeCfg.dstWidget.rcRelativePos.bottom= cfgvw_t.TimeTitle.rcRelativePos.bottom;
				EncodeCfg.dstWidget.rgbaFrontground = cfgvw_t.TimeTitle.rgbaFrontground;
				EncodeCfg.dstWidget.rgbaBackground = cfgvw_t.TimeTitle.rgbaBackground;
                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_VIDEO_WIDGET));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();    
            
        }
        else if(pTlv->GetType() == ENCODE_TITLEOSD_TYPE)
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(NET_VIDEO_WIDGET) );

            for( int i = iChn; i < iTotal; i++ )
            {
                NET_VIDEO_WIDGET EncodeCfg;
                memset(&EncodeCfg,0,sizeof(NET_VIDEO_WIDGET));    

                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(i);

                EncodeCfg.usChannel = i;
                EncodeCfg.dstWidget.bEncodeBlend = cfgvw_t.ChannelTitle.bEncodeBlend;
                EncodeCfg.dstWidget.bPreviewBlend = cfgvw_t.ChannelTitle.bPreviewBlend;
                EncodeCfg.dstWidget.rcRelativePos.left = cfgvw_t.ChannelTitle.rcRelativePos.left;
                EncodeCfg.dstWidget.rcRelativePos.top = cfgvw_t.ChannelTitle.rcRelativePos.top;
                EncodeCfg.dstWidget.rcRelativePos.right = cfgvw_t.ChannelTitle.rcRelativePos.right;
                EncodeCfg.dstWidget.rcRelativePos.bottom= cfgvw_t.ChannelTitle.rcRelativePos.bottom;            
		  EncodeCfg.dstWidget.rgbaFrontground = cfgvw_t.ChannelTitle.rgbaFrontground;
		  EncodeCfg.dstWidget.rgbaBackground = cfgvw_t.ChannelTitle.rgbaBackground;
                pPacket->PutBuffer(&EncodeCfg, sizeof(NET_VIDEO_WIDGET));
            }

            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

            pPacket->Release();    
            
        }
        else if(pTlv->GetType() == ENCODE_OSD_SIZE)
        {
            CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(CONFIG_OSDPARAM));
            CConfigOSDParam* pOSD = new CConfigOSDParam();
            pOSD->update();
            for( int i = iChn; i < iTotal; i++ )
            {         
                CONFIG_OSDPARAM OSD_Zoom;
                memset(&OSD_Zoom,0,sizeof(CONFIG_OSDPARAM));        
                OSD_Zoom.size = pOSD->getConfig(i).size;
                pPacket->PutBuffer(&OSD_Zoom, sizeof(CONFIG_OSDPARAM));
               }
            pTlvOut->SetType( pTlv->GetType() );
            pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);
            pPacket->Release();        
               delete pOSD;
        }
        out_list.push_back(pTlvOut);
    }

    delete pccfgVW;
    delete pCfgEncode;

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}


int IVNSysConfig::ModEncodeCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    int ret = 0 ;
    int flag  =0;
    TlvReader reader;
    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }
    TLV_LIST& tmp_list = reader.GetTlvList();
    TLV_LIST::iterator it;

    CConfigVideoWidget* pccfgVW = new CConfigVideoWidget;
    pccfgVW->update();
    
    CConfigEncode* pCfgEncode = new CConfigEncode();
    pCfgEncode->update();
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);
        int num = 0;
        
    //    trace("DEV_CHANNEL_ENCODE_TYPE-set-tlv type:%d\n", pTlv->GetType());
        if(pTlv->GetType() == ENCODE_CHANNEL_NAME_TYPE)
        {
            num = pTlv->GetLength()/sizeof(CHANNEL_NAME);
            
            CConfigChannelTitle* pTitle = new CConfigChannelTitle();
            pTitle->update();
            
            for(int i = num; i > 0; i-- )
            {
                CHANNEL_NAME* pCfg = (CHANNEL_NAME*)( pTlv->GetValueBuf()+sizeof(CHANNEL_NAME)*(num-i) );               
                 memcpy(pTitle->getConfig(pCfg->iChannel).strName,pCfg->strChannelName,CHANNEL_NAME_SIZE -1);
            }
            pTitle->commit();
            delete pTitle;
            
        }
        else if(pTlv->GetType() == ENCODE_MAIN_STREAM_TYPE )
        {_printd("ENCODE_MAIN_STREAM_TYPE");
			
#ifndef FIXED_ENCODEING_PARA
_printd("mark");
            int iMinBitRate = 0;
            int iMaxBitRate = 0;
            num = pTlv->GetLength()/sizeof(NET_ENCODE);
         _printd("mark");
            for(int i = num; i > 0; i-- )
            {_printd("mark");
                NET_ENCODE* pCfg = (NET_ENCODE*)( pTlv->GetValueBuf()+sizeof(NET_ENCODE)*(num-i) );
                CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig(pCfg->iChannel);
_printd("mark");
#ifdef _USE_720P_MODULE_  //豆干机设置要求支持1600*1200  15帧率	
				if(pCfg->dstFmt.vfFormat.iResolution == CAPTURE_SIZE_UXGA
					&&pCfg->dstFmt.vfFormat.nFPS >15)		
				{_printd("mark");
					pCfg->dstFmt.vfFormat.nFPS = 15;
				}
_printd("mark");
				if((CAPTURE_SIZE_UXGA == pCfg->dstFmt.vfFormat.iResolution) && (CAPTURE_SIZE_UXGA != cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.iResolution))
				{_printd("mark");
					flag = 2;
				}
				if((CAPTURE_SIZE_UXGA !=  pCfg->dstFmt.vfFormat.iResolution) && (CAPTURE_SIZE_UXGA == cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.iResolution))
				{_printd("mark");
					flag = 2;
				}		
#endif
#ifdef SDK_3516
_printd("mark");
         //34041要求动态切换分辨率，获取产品ID然后重启下
                CAPTURE_CAPS Caps;
                memset(&Caps, 0, sizeof(CAPTURE_CAPS));
                CaptureGetCaps(&Caps);
                if( Caps.Reserved & BITMSK(0) )
                {
                    //!各通道能力可能不一样
                    memset(&Caps, 0, sizeof(CAPTURE_CAPS));
                    GetChnCaps(pCfg->iChannel, &Caps);
                }
                 if (Caps.ImageSize & BITMSK(pCfg->dstFmt.vfFormat.iResolution))
                 {
					if(cfgEncode.dstMainFmt[0].vfFormat.iResolution !=pCfg->dstFmt.vfFormat.iResolution)
					{
						cfgEncode.dstMainFmt[0].vfFormat.iResolution =  pCfg->dstFmt.vfFormat.iResolution;
			    		ret |= pCfgEncode->commit();
						if(pCfg->dstFmt.vfFormat.iResolution == CAPTURE_SIZE_720P)
						{
							VideoInSetSensorMode(0,VD_SENSOR_1080P_720P);
						}
						else if(pCfg->dstFmt.vfFormat.iResolution == CAPTURE_SIZE_1080P)
						{
							VideoInSetSensorMode(0,VD_SENSOR_NORM);
						}else if (pCfg->dstFmt.vfFormat.iResolution == CAPTURE_SIZE_1280_960){
							VideoInSetSensorMode(0,VD_SENSOR_NORM);
						}
						//_printd("befor start reboot ");
						//g_Challenger.Reboot();
						//_printd ("end reboot");
					}
				}
#endif	
                memcpy(&(cfgEncode.dstMainFmt[pCfg->iSteamIndex]),&(pCfg->dstFmt),sizeof(MEDIA_FORMAT));
				_printd("Set res = %d", cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.iResolution);
                getImageBitRate(pCfg->dstFmt.vfFormat.iResolution, pCfg->dstFmt.vfFormat.nFPS, iMinBitRate, iMaxBitRate);
                if(cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.nBitRate < iMinBitRate)
                {
                    cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.nBitRate = iMinBitRate;
                }
                else if(cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.nBitRate > iMaxBitRate)
                {
                    cfgEncode.dstMainFmt[pCfg->iSteamIndex].vfFormat.nBitRate = iMaxBitRate;
                }
            }
#endif
        }
        else if( pTlv->GetType() == ENCODE_SUB_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_3IRD_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_4RTH_STREAM_TYPE
        		|| pTlv->GetType() == ENCODE_5FTH_STREAM_TYPE )
        {
#ifndef FIXED_ENCODEING_PARA
        	int stream = 0;
        	if( pTlv->GetType() == ENCODE_SUB_STREAM_TYPE )
        	{
        		stream = 0;
        	}
        	else if( pTlv->GetType() == ENCODE_3IRD_STREAM_TYPE )
        	{
        		stream = 1;
        	}
        	else if( pTlv->GetType() == ENCODE_4RTH_STREAM_TYPE )
			{
        		stream = 2;
			}
        	else if( pTlv->GetType() == ENCODE_5FTH_STREAM_TYPE )
			{
        		stream = 3;
			}

        	if( stream >= EXTRATYPES )
        	{
        		continue;
        	}

 //           trace("DEV_CHANNEL_ENCODE_TYPE-set-stream:%d\n", stream);
            int iMinBitRate = 0;
            int iMaxBitRate = 0;
            num = pTlv->GetLength()/sizeof(NET_ENCODE);
            for(int i = num; i > 0; i-- )
            {
                NET_ENCODE* pCfg = (NET_ENCODE*)( pTlv->GetValueBuf()+sizeof(NET_ENCODE)*(num-i));
                pCfg->iSteamIndex = stream;
                CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig(pCfg->iChannel);
                memcpy(&(cfgEncode.dstExtraFmt[stream]),&(pCfg->dstFmt),sizeof(MEDIA_FORMAT));
            
                getImageBitRate(pCfg->dstFmt.vfFormat.iResolution, pCfg->dstFmt.vfFormat.nFPS, iMinBitRate, iMaxBitRate);
                if(cfgEncode.dstExtraFmt[pCfg->iSteamIndex].vfFormat.nBitRate < iMinBitRate)
                {
                    cfgEncode.dstExtraFmt[pCfg->iSteamIndex].vfFormat.nBitRate = iMinBitRate;
                }
                else if(cfgEncode.dstExtraFmt[pCfg->iSteamIndex].vfFormat.nBitRate > iMaxBitRate)
                {
                    cfgEncode.dstExtraFmt[pCfg->iSteamIndex].vfFormat.nBitRate = iMaxBitRate;
                }
            }    
#endif            
        }
        else if( pTlv->GetType() == ENCODE_SNAP_STREAM_TYPE)
        {         
            num = pTlv->GetLength()/sizeof(NET_ENCODE);
            for(int i = num; i > 0; i-- )
            {
                NET_ENCODE* pCfg = (NET_ENCODE*)( pTlv->GetValueBuf()+sizeof(NET_ENCODE)*(num-i) );
                CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig(pCfg->iChannel);

                memcpy(&(cfgEncode.dstSnapFmt[pCfg->iSteamIndex]),&(pCfg->dstFmt),sizeof(MEDIA_FORMAT));    

            }
            
        }
        else if( pTlv->GetType() == ENCODE_COLOR_TYPE )
        {
            num = pTlv->GetLength()/sizeof(NET_VIDEOCOLOR);
            
            CConfigVideoColor* pCfgVideoColor = new CConfigVideoColor();
            pCfgVideoColor->update();
            
            for(int i = num; i > 0; i-- )
            {
                NET_VIDEOCOLOR* pCfg = (NET_VIDEOCOLOR*)( pTlv->GetValueBuf()+sizeof(NET_VIDEOCOLOR)*(num-i) );
                CONFIG_VIDEOCOLOR& cfgVideoColor = pCfgVideoColor->getConfig(pCfg->iChannel);
                memcpy(&cfgVideoColor,&(pCfg->stVideoColor), sizeof(CONFIG_VIDEOCOLOR));                
            }
            pCfgVideoColor->commit();
            delete pCfgVideoColor;
            
        }        
        else if(pTlv->GetType() == ENCODE_COVER_TYPE)
        {
        	_printd("ENCODE_COVER_TYPE");
            num = pTlv->GetLength()/sizeof(NET_VIDEO_COVER);
            for(int i = num; i > 0; i-- )
            {
                NET_VIDEO_COVER* pCfg = (NET_VIDEO_COVER*)( pTlv->GetValueBuf()+sizeof(NET_VIDEO_COVER)*(num-i) );
                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(pCfg->usChannel);
                if ( pCfg->usCoverNum> COVERNUM) 
                {
                    cfgvw_t.iCoverNum = COVERNUM;
                }else
                {
                     cfgvw_t.iCoverNum = pCfg->usCoverNum;
                }
                int iIndex;
                for(iIndex = 0 ;iIndex < COVERNUM ;iIndex++)
                {
                    //if(iIndex < pCfg->usCoverNum)
                    if(iIndex < 4)
                    {
                        cfgvw_t.dstCovers[iIndex].rcRelativePos.left= pCfg->dstCovers[iIndex].rcRelativePos.left;
                        cfgvw_t.dstCovers[iIndex].rcRelativePos.right= pCfg->dstCovers[iIndex].rcRelativePos.right;
                        cfgvw_t.dstCovers[iIndex].rcRelativePos.top= pCfg->dstCovers[iIndex].rcRelativePos.top;
                        cfgvw_t.dstCovers[iIndex].rcRelativePos.bottom= pCfg->dstCovers[iIndex].rcRelativePos.bottom;
                        cfgvw_t.dstCovers[iIndex].rgbaFrontground = pCfg->dstCovers[iIndex].rgbaFrontground;
                        cfgvw_t.dstCovers[iIndex].bEncodeBlend = pCfg->dstCovers[iIndex].bEncodeBlend;
                        cfgvw_t.dstCovers[iIndex].bPreviewBlend = pCfg->dstCovers[iIndex].bPreviewBlend;
                        cfgvw_t.dstCovers[iIndex].rgbaBackground = pCfg->dstCovers[iIndex].rgbaBackground; /*黑色*/
                    }else
                    {
                        cfgvw_t.dstCovers[iIndex].rcRelativePos.left=1024;
                           cfgvw_t.dstCovers[iIndex].rcRelativePos.right= 2048;
                           cfgvw_t.dstCovers[iIndex].rcRelativePos.top= 1024;
                           cfgvw_t.dstCovers[iIndex].rcRelativePos.bottom= 2048;
                           cfgvw_t.dstCovers[iIndex].bEncodeBlend = false;
                          cfgvw_t.dstCovers[iIndex].bPreviewBlend = false;
                    }
                }
            }

        }
        else if( pTlv->GetType() == ENCODE_TIMEOSD_TYPE)
        {                        
            num = pTlv->GetLength()/sizeof(NET_VIDEO_WIDGET);
            for(int i = num; i > 0; i-- )
            {
                NET_VIDEO_WIDGET* pCfg = (NET_VIDEO_WIDGET*)( pTlv->GetValueBuf()+sizeof(NET_VIDEO_WIDGET)*(num-i) );

                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(pCfg->usChannel);

                cfgvw_t.TimeTitle.rcRelativePos.left= pCfg->dstWidget.rcRelativePos.left;
                cfgvw_t.TimeTitle.rcRelativePos.right= pCfg->dstWidget.rcRelativePos.right;
                cfgvw_t.TimeTitle.rcRelativePos.top= pCfg->dstWidget.rcRelativePos.top;
                cfgvw_t.TimeTitle.rcRelativePos.bottom= pCfg->dstWidget.rcRelativePos.bottom;
                cfgvw_t.TimeTitle.bEncodeBlend = pCfg->dstWidget.bEncodeBlend;
                cfgvw_t.TimeTitle.bPreviewBlend = pCfg->dstWidget.bPreviewBlend;
                cfgvw_t.TimeTitle.rgbaBackground = pCfg->dstWidget.rgbaBackground;  
                cfgvw_t.TimeTitle.rgbaFrontground = pCfg->dstWidget.rgbaFrontground;


            }
            
        }
        else if(pTlv->GetType() == ENCODE_TITLEOSD_TYPE)
        {
            num = pTlv->GetLength()/sizeof(NET_VIDEO_WIDGET);

            for(int i = num; i > 0; i-- )
            {
                NET_VIDEO_WIDGET* pCfg = (NET_VIDEO_WIDGET*)( pTlv->GetValueBuf()+sizeof(NET_VIDEO_WIDGET)*(num-i) );

                CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(pCfg->usChannel);
                
                cfgvw_t.ChannelTitle.rcRelativePos.left= pCfg->dstWidget.rcRelativePos.left;
                cfgvw_t.ChannelTitle.rcRelativePos.right= pCfg->dstWidget.rcRelativePos.right;
                cfgvw_t.ChannelTitle.rcRelativePos.top= pCfg->dstWidget.rcRelativePos.top;
                cfgvw_t.ChannelTitle.rcRelativePos.bottom= pCfg->dstWidget.rcRelativePos.bottom;
                cfgvw_t.ChannelTitle.bEncodeBlend = pCfg->dstWidget.bEncodeBlend;
                cfgvw_t.ChannelTitle.bPreviewBlend = pCfg->dstWidget.bPreviewBlend;
				
                cfgvw_t.ChannelTitle.rgbaBackground = pCfg->dstWidget.rgbaBackground;  
                cfgvw_t.ChannelTitle.rgbaFrontground = pCfg->dstWidget.rgbaFrontground;
            }
        }
        else if(pTlv->GetType() == ENCODE_OSD_SIZE)
        {
            num = pTlv->GetLength()/sizeof(CONFIG_OSDPARAM);
            CConfigOSDParam* pOSD = new CConfigOSDParam();
            pOSD->update();
            for(int i = num; i > 0; i-- )
            {
                CONFIG_OSDPARAM* pCfg = (CONFIG_OSDPARAM*)( pTlv->GetValueBuf()+sizeof(CONFIG_OSDPARAM)*(num-i) );
                
                if (pCfg->index >= 0 && pCfg->index <  N_SYS_CH)
                {
                    pOSD->getConfig(pCfg->index).size = pCfg->size;
                }            

            }
            pOSD->commit();
            delete pOSD;
                
        }
    }

	int iRet = 0;
	_printd("Before Encode commit");
	ret |= (iRet = pCfgEncode->commit());
	_printd ("End Encode commit iRet = %d", iRet);
    ret |= flag;
    trace("###########ret:%#x\n", ret);
    pccfgVW->commit();
    delete pccfgVW;
    delete pCfgEncode;
  	//重启系统
  	_printd("#@#@#@#@");
	if (ret & CONFIG_APPLY_REBOOT)
	{
		g_Challenger.Reboot();
	}	  
    return 0;
}

int IVNSysConfig::QueryChnName(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    int iChn = 0,iTotal = 0;
    DEV_CHANNEL* pChannel = (DEV_CHANNEL*)tlv_in.GetValueBuf();
    
    if( pChannel->usIndex < pChannel->usTotal )
    {
        iChn = pChannel->usIndex;
        iTotal = iChn +1;
    }
    else if( pChannel->usIndex > pChannel->usTotal )
   {
      	iChn = pChannel->usTotal;
		
   	iTotal = pChannel->usIndex;;
    }
    else
    {
        iChn = 0;
        iTotal = pChannel->usTotal;
    }
    CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(CHANNEL_NAME) );
    
    CConfigChannelTitle* pTitle = new CConfigChannelTitle();
    pTitle->update();

    for( int i = iChn; i < iTotal; i++ )
    {
        CHANNEL_NAME ChannelName;
        memset(&ChannelName,0,sizeof(CHANNEL_NAME));        
        ChannelName.iChannel = i;
        memcpy(ChannelName.strChannelName,pTitle->getConfig(i).strName, CHANNEL_NAME_SIZE -1);
    
        pPacket->PutBuffer(&ChannelName, sizeof(CHANNEL_NAME));
    }

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

    pPacket->Release();        

    delete pTitle;

    have_tlv_out = true;
    return 0;
}

int IVNSysConfig::ModChnName(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    int num = tlv_in.GetLength()/sizeof(CHANNEL_NAME);
    
    CConfigChannelTitle* pTitle = new CConfigChannelTitle();
    pTitle->update();
    
    for(int i = num; i > 0; i-- )
    {
        CHANNEL_NAME* pCfg = (CHANNEL_NAME*)(tlv_in.GetValueBuf()+sizeof(CHANNEL_NAME)*(num-i) );

        if (pCfg->iChannel >= 0 && pCfg->iChannel < g_nLogicNum)
        {
            memcpy(pTitle->getConfig(pCfg->iChannel).strName,pCfg->strChannelName,CHANNEL_NAME_SIZE -1);
        }
        else
        {
            tracepoint();
        }
    }
    
    pTitle->commit();
    delete pTitle;

    have_tlv_out = FALSE;
    return 0;            
}

int IVNSysConfig::QueryAudioCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    CConfigAudioInFormat* pcConfig = new CConfigAudioInFormat();
    assert(pcConfig);
    
    pcConfig->update();
    
    CONFIG_AUDIOIN_FORMAT &audio_format = pcConfig->getConfig();

    CONFIG_AUDIO_FORMAT_NET stData;
    memset(&stData,0,sizeof(CONFIG_AUDIO_FORMAT_NET));
    
    AUDIOIN_CAPS stCaps;
    memset(&stCaps,0,sizeof(stCaps));
    CDevAudioIn::GetCaps(&stCaps);
    if (0x03 == stCaps.ucAudioInType)
    {
        stData.uiTalkAudioSourceType = audio_format.AudioSourceType;
    }
    else
    {
        stData.uiTalkAudioSourceType = stCaps.ucAudioInType - 1;
    }

    stData.uiOutSilence = audio_format.Silence;

    printf("IVNSysConfig::QueryAudioCfg():audio_format.Silence = %d\r\n",audio_format.Silence);
    
    stData.uiOutLAudioVolumn = audio_format.LAudioVolumn;
    stData.uiOutRAudioVolumn = audio_format.RAudioVolumn;
    stData.uiLongtimeBeepEnable = audio_format.LongtimeBeepEnable;

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)&stData, sizeof(CONFIG_AUDIO_FORMAT_NET), false);

    delete pcConfig;

    have_tlv_out = true;
    return 0;
}

int IVNSysConfig::ModAudioCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    

    CConfigAudioInFormat* pcConfig = new CConfigAudioInFormat();
    assert(pcConfig);
    
    pcConfig->update();
    
    CONFIG_AUDIO_FORMAT_NET* pCfg = (CONFIG_AUDIO_FORMAT_NET*)tlv_in.GetValueBuf();
    CONFIG_AUDIOIN_FORMAT &audio_format = pcConfig->getConfig();

	audio_format.AudioSourceType = pCfg->uiTalkAudioSourceType;
    audio_format.Silence = pCfg->uiOutSilence;
    audio_format.LAudioVolumn = pCfg->uiOutLAudioVolumn;
    audio_format.RAudioVolumn = pCfg->uiOutRAudioVolumn;
    audio_format.LongtimeBeepEnable = pCfg->uiLongtimeBeepEnable;
	
    pcConfig->commit();
    delete pcConfig;

    have_tlv_out = FALSE;
    return 0;            
}

int IVNSysConfig::QueryLocalChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    
    int iChn = 0,iTotal = 0;
    DEV_CHANNEL* pChannel = (DEV_CHANNEL*)tlv_in.GetValueBuf();
    
    if( pChannel->usIndex < pChannel->usTotal )
    {
        iChn = pChannel->usIndex;
        iTotal = iChn +1;
    }
    else
    {
        iChn = 0;
        iTotal = pChannel->usTotal;
    }
    
    CPacket* pPacket = g_PacketManager.GetPacket((iTotal - iChn)*sizeof(LOCAL_CH_CFG) );
    
    CConfigLocalDigiChCfg* pcConfig = new CConfigLocalDigiChCfg();
    pcConfig->update();

    for( int i = iChn; i < iTotal; i++ )
    {
        pPacket->PutBuffer(&pcConfig->getConfig(i), sizeof(LOCAL_CH_CFG));
    }

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

    pPacket->Release();

    delete pcConfig;
    pcConfig = NULL;
    
    have_tlv_out = true;

    return 0;
}

int IVNSysConfig::ModLocalChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{
    if ( 0!= (tlv_in.GetLength()%sizeof(LOCAL_CH_CFG)))
    {
        have_tlv_out = FALSE;
        printf("IVNSysConfig::ModLocalChCfg():out1!\r\n");        
        return -1; 
    }
    
    int num = tlv_in.GetLength()/sizeof(LOCAL_CH_CFG);
    CConfigLocalDigiChCfg* pcConfig = new CConfigLocalDigiChCfg();
    pcConfig->update();
    
    for(int i = 0; i < num; i++ )
    {
        LOCAL_CH_CFG* pCfg = (LOCAL_CH_CFG*)(tlv_in.GetValueBuf()+sizeof(LOCAL_CH_CFG)*(i) );
                
        if ((pCfg->iLocalChannelNo < 0) || (pCfg->iLocalChannelNo >=  m_pCaptureM->GetLogicChnNum()))
        {
            continue;
        }
        
        LOCAL_CH_CFG &stLocalCfg = pcConfig->getConfig(pCfg->iLocalChannelNo);
        memcpy(&stLocalCfg,pCfg,sizeof(LOCAL_CH_CFG));       
    }
    
    pcConfig->commit();
    delete pcConfig;
    pcConfig = NULL;
    
    have_tlv_out = FALSE;
    
    return 0;            
}

int IVNSysConfig::QueryRemoteChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    
    int iChn = 0,iTotal = 0;
    DEV_CHANNEL* pChannel = (DEV_CHANNEL*)tlv_in.GetValueBuf();
    
    if( pChannel->usIndex < pChannel->usTotal )
    {
        iChn = pChannel->usIndex;
        iTotal = iChn +1;
    }
    else
    {
        /* 这个只允许一个通道一个通道取 */
        printf("IVNSysConfig::QueryRemoteChCfg():pChannel->usIndex = %d pChannel->usTotal = %d\r\n",
        pChannel->usIndex,pChannel->usTotal);
        
        have_tlv_out = false;
        printf("IVNSysConfig::QueryRemoteChCfg():out 1!\r\n");
        
        return -1;
    }
    
    CPacket* pPacket = g_PacketManager.GetPacket(sizeof(REMOTE_DIGI_CH_CFG));    
    CConfigRemoteDigiChCfg* pcConfig = new CConfigRemoteDigiChCfg();
    pcConfig->update();
    
    pPacket->PutBuffer((char *)&pcConfig->getConfig(iChn), sizeof(REMOTE_DIGI_CH_CFG));

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

    pPacket->Release();

    delete pcConfig;
    pcConfig = NULL;
    
    have_tlv_out = true;

    return 0;
}

int IVNSysConfig::ModRemoteChCfg(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    
    if ( 0 != ( tlv_in.GetLength()%sizeof(NET_EMOTE_CH_CFG) ) )
    {
        have_tlv_out = FALSE;
        printf("IVNSysConfig::ModRemoteChCfg():len err tlv_in.GetLength() = %d!\r\n",tlv_in.GetLength());        
        return -1; 
    }

    int num = tlv_in.GetLength()/sizeof(NET_EMOTE_CH_CFG);

    CConfigRemoteDigiChCfg* pcConfig = new CConfigRemoteDigiChCfg();
    pcConfig->update();
    
    for(int i = 0; i < num; i++ )
    {
        NET_EMOTE_CH_CFG* pCfg = (NET_EMOTE_CH_CFG*)(tlv_in.GetValueBuf() + sizeof(NET_EMOTE_CH_CFG)*i );
        if (pCfg->iLocalChannelNo < 0 || pCfg->iLocalChannelNo >=  m_pCaptureM->GetLogicChnNum())
        {
            continue;
        }

        if ( !m_pCaptureM->IsSupportDigital(pCfg->iLocalChannelNo) )
        {
            continue;
        }

        if ( pCfg->iCfgIndex < 0 || pCfg->iCfgIndex >= MAX_TOUR_NUM)
        {
            continue;
        }

        int iContype = pcConfig->getConfig(pCfg->iLocalChannelNo).stRemoteCfg[pCfg->iCfgIndex].iConType;
        memcpy(&pcConfig->getConfig(pCfg->iLocalChannelNo).stRemoteCfg[pCfg->iCfgIndex],pCfg,sizeof(NET_EMOTE_CH_CFG));

        /* 老的web 会把上报的保留字改成0下发,这里进行兼容 */
        if (CON_PROTOCOL_TCP != pCfg->iConType && CON_PROTOCOL_UDP != pCfg->iConType)
        {
            pcConfig->getConfig(pCfg->iLocalChannelNo).stRemoteCfg[pCfg->iCfgIndex].iConType = iContype;                
        }
    }
    
    pcConfig->commit();
    delete pcConfig;
    pcConfig = NULL;
    
    have_tlv_out = FALSE;
    
    return 0;            
}

static const uint g_acBaudTable[10] = {    //波特率参数表
    300,  600,
    1200,  2400,   4800, 
    9600, 19200, 38400, 57600, 115200,
};
static int GetBaudBaseSelNet(uint baudbase)
{
    for(int i=0;i<10;i++){
        if(g_acBaudTable[i]==baudbase)
            return i;
    }
    return -1;
}

int IVNSysConfig::QueryKBDConfig(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    
    CPacket* pPacket = g_PacketManager.GetPacket(sizeof(KBD_CONFIG));    
    CConfigKbd* pcConfig = new CConfigKbd();
    pcConfig->update();

    KBD_CONFIG stConfigNet;
    memset(&stConfigNet,0,sizeof(stConfigNet));

    stConfigNet.Function = pcConfig->getConfig().iProtocol;

    switch(pcConfig->getConfig().iCommAttri.nDataBits)
    {
    case 5:
        stConfigNet.DataBits  = 0;
        break;
    case 6:
        stConfigNet.DataBits  = 1;
        break;
    case 7:
        stConfigNet.DataBits  = 2;
        break;        
    case 8:
    default:    
        stConfigNet.DataBits  = 3;
        break;
    }

    stConfigNet.StopBits = pcConfig->getConfig().iCommAttri.iStopBits;

    if(pcConfig->getConfig().iCommAttri.iStopBits == 1)
    {
        stConfigNet.StopBits = 0;
    }
    else
    {
        stConfigNet.StopBits = 2;
    }
    
    stConfigNet.Parity = pcConfig->getConfig().iCommAttri.iParity;
    stConfigNet.BaudBase = GetBaudBaseSelNet(pcConfig->getConfig().iCommAttri.nBaudBase);

    pPacket->PutBuffer((char *)&stConfigNet, sizeof(KBD_CONFIG));

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false);

    pPacket->Release();

    delete pcConfig;
    pcConfig = NULL;
    
    have_tlv_out = true;
    return 0;
}

int IVNSysConfig::ModKBDConfig(CTlv & tlv_in, CTlv & tlv_out, bool & have_tlv_out)
{    
    if ( sizeof(KBD_CONFIG) != tlv_in.GetLength() )
    {
        have_tlv_out = false;
        printf("IVNSysConfig::ModKBDConfig():len err tlv_in.GetLength() = %d!\r\n",tlv_in.GetLength());        
        return -1; 
    }

    KBD_CONFIG *pstConfigNet = (KBD_CONFIG *)tlv_in.GetValueBuf();

    if (pstConfigNet == 0)
    {
        __trip;
        return -1;
    }
        
    CConfigKbd* pcConfig = new CConfigKbd();
    pcConfig->update();

    pcConfig->getConfig().iProtocol = pstConfigNet->Function;

    switch(pstConfigNet->DataBits)
    {
    case 0:
        pcConfig->getConfig().iCommAttri.nDataBits  = 5;
        break;
    case 1:
        pcConfig->getConfig().iCommAttri.nDataBits  = 6;
        break;
    case 2:
        pcConfig->getConfig().iCommAttri.nDataBits  = 7;
        break;        
    case 3:
    default:    
        pcConfig->getConfig().iCommAttri.nDataBits  = 8;
        break;
    }

    pcConfig->getConfig().iCommAttri.iParity = pstConfigNet->Parity;

    if(pstConfigNet->StopBits == 0)
    {
        pcConfig->getConfig().iCommAttri.iStopBits = 1;
    }
    else
    {
        pcConfig->getConfig().iCommAttri.iStopBits = 2;
    }

    if(pstConfigNet->BaudBase < 10)
    {
        pcConfig->getConfig().iCommAttri.nBaudBase = g_acBaudTable[pstConfigNet->BaudBase];
    }
    else
    {
        __trip;
    }

    pcConfig->commit();
    
    have_tlv_out = false;
    return 0;
}

int CONFIG_MONITORTOUR_2_SPLIT_COMBINE(enum split_combine_t sct, CONFIG_MONITORTOUR& tour_in,
        SPLIT_COMBINE& combine_out)
{
    //!判断此分割是否支持
    SPLIT_CAPS caps;

    if( !CDevSplit::instance(0)->GetCaps( &caps ) )
    {
        trace("can't get split caps!\n");
        return -1;
    }


    if( BITMSK(static_cast<int>(sct)) & caps.dwCombine )
    {
        //!支持此分割模式
    }
    else
    {
        trace("don't support this caps:%d\n", sct);
        return -1;
    }

    int i = 0;

    memset(&combine_out, 0, sizeof(combine_out) );
    return 0;
}

int SPLIT_COMBINE_2_CONFIG_MONITORTOUR( SPLIT_COMBINE& combine_in,
        CONFIG_MONITORTOUR& tour_out)
{
    //!判断此分割是否支持
    SPLIT_CAPS caps;

    if( !CDevSplit::instance(0)->GetCaps( &caps ) )
    {
        trace("can't get split caps!\n");
        return -1;
    }


    if( BITMSK(static_cast<int>(combine_in.ucSplitMode)) & caps.dwCombine )
    {
        //!支持此分割模式
    }
    else
    {
        trace("don't support this caps:%d\n", combine_in.ucSplitMode);
        return -1;
    }
    return 0;
}

int IVNSysConfig::GetTVCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }


    TLV_LIST& tmp_list = reader.GetTlvList();

    //trace("IVNSysConfig::GetTVCfg->list num:%d\n", tmp_list.size() );
    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        if( (*it)->GetType() == VIDEOUT_CFG_TV )
        {
            CConfigTVAdjust* pCfgTV = new CConfigTVAdjust;
            pCfgTV->update();

            CTlv* pTmpTlv = new CTlv;

            pTmpTlv->SetType(VIDEOUT_CFG_TV);
            pTmpTlv->SetValue( (uchar*)&pCfgTV->getConfig(), sizeof(pCfgTV->getConfig()), false );

            out_list.push_back(pTmpTlv);

            delete pCfgTV;
        }
    }


    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }

    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        //trace("type:%d, len:%d", (*it)->GetType(), (*it)->GetLength() );
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }

    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );

    pTotalPacket->Release();

    return 0;
}

int IVNSysConfig::SetTVCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    TlvReader reader;
    TLV_LIST out_list;
    int ret = 0;

    have_tlv_out = false;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }


    TLV_LIST& tmp_list = reader.GetTlvList();

    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        if( (*it)->GetType() == VIDEOUT_CFG_TV )
        {
            CConfigTVAdjust* pCfgTV = new CConfigTVAdjust;
            pCfgTV->update();

            (*it)->GetValue( (uchar*)&pCfgTV->getConfig(), sizeof(pCfgTV->getConfig()) );
            ret = pCfgTV->commit();
            if( ret != CONFIG_APPLY_OK || ret != CONFIG_APPLY_DELAY_SAVE )
            {
                trace("SetTVCfg: subtype:%d, commit error:%#x\n", (*it)->GetType(), ret);
            }
            delete pCfgTV;
        }
    }

    return 0;
}

int IVNSysConfig::PtzControlRawData( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int iLen = tlv_in.GetLength();
    PTZ_RAW_DATA_S *pData = (PTZ_RAW_DATA_S *)tlv_in.GetValueBuf();

    if (NULL == pData  || iLen != sizeof(PTZ_RAW_DATA_S))
    {
        __trip;
        return 0;
    }
    
    if (pData->iLen <= 0 || pData->iLen > 32)
    {
        __trip;
        return 0;
    }
    
    g_Ptz.WritePtzRawData((uchar*)pData->acData,(uint)pData->iLen,pData->iCh);
    
    return 0;
}

int IVNSysConfig::QueryManualSnap( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    
    return 0;
}
int IVNSysConfig::ManualSnap( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    if(tlv_in.GetLength() != sizeof(MANUAL_SNAP))
    {
        return -1;
    }
    MANUAL_SNAP* pConfig = (MANUAL_SNAP*)tlv_in.GetValueBuf();
    tracepoint();

    /*数字通道目前不支持抓图*/
    if (pConfig->Channel>= 0 && pConfig->Channel<g_nCapture)
    {
        g_Snap.startSnap(CSnap::snapTriggerManual,pConfig->Channel);
        g_Snap.stopSnap(CSnap::snapTriggerManual,pConfig->Channel);
    }

    if(pConfig->acTransferNet)
    {
   	 return DEV_MANUAL_SNAP;
    }

   return 0;	
}

#if defined (_USE_720P_MODULE_) 
int IVNSysConfig::QueryCameraCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();

	tlv_out.SetType( tlv_in.GetType());
	tlv_out.SetValue((uchar*)&(ConfigCamera.getConfig()), sizeof(CONFIG_CAMERA), false);    
	have_tlv_out = true;

	return 0;
}

int IVNSysConfig::ModCameraCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	if(tlv_in.GetLength() != sizeof(CONFIG_CAMERA))
	{
		return 0;
	}

	CONFIG_CAMERA* pConfig = (CONFIG_CAMERA*)tlv_in.GetValueBuf();
	CConfigCamera ConfigCamera ;
	ConfigCamera.update();
	CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();
	memcpy(&cfgCamera, pConfig, sizeof(CONFIG_CAMERA));
	ConfigCamera.commit();
	
	return 0;
}
int IVNSysConfig::QueryCameraCfgExt( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    int ret = 0 ;
    TlvReader reader;
    TLV_LIST out_list;

    have_tlv_out = true;

    ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
    if( ret < 0 )
    {
        return -1;
    }
    
    TLV_LIST& tmp_list = reader.GetTlvList();
    
    TLV_LIST::iterator it;
    for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
    {
        CTlv* pTlv = (*it);

        CTlv *pTlvOut = new CTlv;
        assert( pTlvOut != NULL );
        
//        trace("list node-type:%d\n", pTlv->GetType() );
        
        if( pTlv->GetType() == CAMERA_WHITEBALANCE_TYPE )
        {
  		 CConfigWhiteBalance *pCfg = new CConfigWhiteBalance();
    		pCfg->update();            
		CPacket* pPacket = g_PacketManager.GetPacket(sizeof(CONFIG_WHITEBALANCE) );

		CONFIG_WHITEBALANCE ConfigCfg;

		memset(&ConfigCfg,0,sizeof(CONFIG_WHITEBALANCE));

		CONFIG_WHITEBALANCE& tmpConfigCfg = pCfg->getConfig();

		memcpy(&ConfigCfg,&tmpConfigCfg,sizeof(CONFIG_WHITEBALANCE));

		pPacket->PutBuffer( &ConfigCfg, sizeof(CONFIG_WHITEBALANCE));

		pTlvOut->SetType( pTlv->GetType() );
		pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

		pPacket->Release();

		delete pCfg;
            
        }
	else if(CAMERA_RESOLUTION_TYPE ==pTlv->GetType())
	{
  		 CConfigSenSor *pCfg = new CConfigSenSor();
    		pCfg->update();            
		CPacket* pPacket = g_PacketManager.GetPacket(sizeof(CONFIG_SENSOR) );

		CONFIG_SENSOR ConfigCfg;

		memset(&ConfigCfg,0,sizeof(CONFIG_SENSOR));

		CONFIG_SENSOR& tmpConfigCfg = pCfg->getConfig();

		memcpy(&ConfigCfg,&tmpConfigCfg,sizeof(CONFIG_SENSOR));

		pPacket->PutBuffer( &ConfigCfg, sizeof(CONFIG_SENSOR));

		pTlvOut->SetType( pTlv->GetType() );
		pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

		pPacket->Release();

		delete pCfg;
	}
	else if(CAMERA_DRC_TYPE ==pTlv->GetType())	
	{
  		 CConfigCameraExt*pCfg = new CConfigCameraExt();
    		pCfg->update();            
		CPacket* pPacket = g_PacketManager.GetPacket(sizeof(CONFIG_CAMERA_EXT) );

		CONFIG_CAMERA_EXT ConfigCfg;

		memset(&ConfigCfg,0,sizeof(CONFIG_CAMERA_EXT));

		CONFIG_CAMERA_EXT& tmpConfigCfg = pCfg->getConfig();

		memcpy(&ConfigCfg,&tmpConfigCfg,sizeof(CONFIG_CAMERA_EXT));

		pPacket->PutBuffer( &ConfigCfg, sizeof(CONFIG_CAMERA_EXT));

		pTlvOut->SetType( pTlv->GetType() );
		pTlvOut->SetValue( (uchar*)pPacket->GetBuffer(), pPacket->GetLength(), false );

		pPacket->Release();

		delete pCfg;
	}
#ifdef SDK_3516	
	else if(pTlv->GetType() == CAMERA_SUPPER_CONFIG_TYPE)
	{
		CONFIG_SUPPER ConfigCfg ;
		memset(&ConfigCfg,0,sizeof(CONFIG_SUPPER));
		
		ConfigCfg.bProfile = GetProfileType();
		 ConfigCfg.bSensortype = VideoInGetSensorType(0);	 

		pTlvOut->SetType( pTlv->GetType() );
		pTlvOut->SetValue( (uchar*)&ConfigCfg, sizeof(CONFIG_SUPPER), false );
	 
	}
#ifdef FUN_SUPPORT_ISP
	else if(pTlv->GetType() == CAMERA_BADPIXEL_DETECT)
	{
		VideoBadPixelDetect();
		pTlvOut->SetType( pTlv->GetType() );
		pTlvOut->SetValue(NULL ,0 , false );		
	}
#endif

#endif
        out_list.push_back(pTlvOut);
    }

    uint total_len = 0;
    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        total_len += (*it)->GetTotalLen();
    }
    CPacket* pTotalPacket = g_PacketManager.GetPacket(total_len);
    assert( pTotalPacket != NULL );

    for( it = out_list.begin(); it != out_list.end(); it++ )
    {
        uchar *p;
        int len = 0;
        (*it)->Pack(p, len );
        pTotalPacket->PutBuffer(p, len);

        delete (*it);
    }
    out_list.clear();

    tlv_out.SetType( tlv_in.GetType() );
    tlv_out.SetValue( pTotalPacket->GetBuffer(), pTotalPacket->GetLength(), true );
    pTotalPacket->Release();
    return 0;
}

int IVNSysConfig::ModCameraCfgExt( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	int ret = 0 ;
	TlvReader reader;

	ret = reader.Parse( tlv_in.GetValueBuf(), tlv_in.GetLength() );
	if( ret < 0 )
	{
		return -1;
	}

	TLV_LIST& tmp_list = reader.GetTlvList();

	TLV_LIST::iterator it;
	

	for( it = tmp_list.begin(); it != tmp_list.end(); it++ )
	{
		CTlv* pTlv = (*it);

		if( pTlv->GetType() == CAMERA_WHITEBALANCE_TYPE)
		{
			CConfigWhiteBalance cConfig ;
			cConfig.update();
			
			CONFIG_WHITEBALANCE& tmpConfigCfg = cConfig.getConfig();
			CONFIG_WHITEBALANCE* pConfigCfg = (CONFIG_WHITEBALANCE*)(pTlv->GetValueBuf());

			memcpy(&tmpConfigCfg,pConfigCfg,sizeof(CONFIG_WHITEBALANCE));

			VideoInSetWhiteBalance(0,(CAMERA_WHITEBALANCE*)&tmpConfigCfg);
			int iret = cConfig.commit();

		}
#ifdef SDK_3516				
		else if(CAMERA_RESOLUTION_TYPE == pTlv->GetType())
		{
			CConfigSenSor cConfig ;
			cConfig.update();
			
			CONFIG_SENSOR& tmpConfigCfg = cConfig.getConfig();
			CONFIG_SENSOR* pConfigCfg = (CONFIG_SENSOR*)(pTlv->GetValueBuf());
			
	  		    CConfigEncode* pCfgEncode = new CConfigEncode();
	  		    pCfgEncode->update();
			     CONFIG_ENCODE& cfgEncode = pCfgEncode->getConfig(0);
				 
			if(tmpConfigCfg.iResolution != pConfigCfg->iResolution)
			{
				memcpy(&tmpConfigCfg,pConfigCfg,sizeof(CONFIG_SENSOR));

				if(tmpConfigCfg.iResolution ==0
					|| tmpConfigCfg.iResolution == 1)
				{
					VideoInSetSensorType(0,VISIONDIGI_SONY_EH4300_1080P);
					cfgEncode.dstMainFmt[0].vfFormat.iResolution =  CAPTURE_SIZE_1080P;
				}
				else
				{
					VideoInSetSensorType(0,VISIONDIGI_HITACHI_110_720P30);
					cfgEncode.dstMainFmt[0].vfFormat.iResolution =  CAPTURE_SIZE_720P;
				}
				
				CConfigLocation* pCfgLocation = new CConfigLocation();
				pCfgLocation->update();
				CONFIG_LOCATION &cfgLocation = pCfgLocation->getConfig();

				if(tmpConfigCfg.iResolution == 0
					||tmpConfigCfg.iResolution == 2
					||tmpConfigCfg.iResolution == 4)
				{
					cfgLocation.iVideoFormat = VIDEO_STANDARD_NTSC;
				}
				else
				{
					cfgLocation.iVideoFormat = VIDEO_STANDARD_PAL;
				}			
		        SystemSetVideoMode((video_standard_t)(cfgLocation.iVideoFormat));
  			    pCfgLocation->commit();
  			    delete pCfgLocation;
				
				int iret = cConfig.commit();
				pCfgEncode->commit();
				ret =2;
			}
		}
		else if( pTlv->GetType() == CAMERA_DRC_TYPE)
		{
			CConfigCameraExt cConfig ;
			cConfig.update();
			uchar oldsnrWDRenable, newsnrWDRenable;
			
			CONFIG_CAMERA_EXT& tmpConfigCfg = cConfig.getConfig();
			oldsnrWDRenable = tmpConfigCfg.snrWDREnable;
			CONFIG_CAMERA_EXT* pConfigCfg = (CONFIG_CAMERA_EXT*)(pTlv->GetValueBuf());
			newsnrWDRenable = pConfigCfg->snrWDREnable;

			memcpy(&tmpConfigCfg,pConfigCfg,sizeof(CONFIG_CAMERA_EXT));

			int iret = cConfig.commit();

			if (oldsnrWDRenable != newsnrWDRenable)
			{
				//设置S2L宽动态不重启，其它平台重启
				#ifdef CHIP_S2L
				ret = 0;
				#else
				trace("change the sensor wdr enable, need reboot!");
				ret = 2;
				#endif
			}

		}
		else if(pTlv->GetType() == CAMERA_SUPPER_CONFIG_TYPE)
		{
			CONFIG_SUPPER *pConfigCfg = (CONFIG_SUPPER*)(pTlv->GetValueBuf());
			
			SetProfileType((int)pConfigCfg->bProfile);
			VideoInSetSensorType(0,(VD_SENSOR_TYPE_E)pConfigCfg->bSensortype);

			ret =2;
		}
		
#endif		
	}

	return ret;
}
#endif



#ifdef _2761_EXCEPTION_CHK_
int IVNSysConfig::QueryEncoderDetectCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
    CConfigEncoderDetect* pcConfig = new CConfigEncoderDetect();
    assert(pcConfig);
    
    pcConfig->update();
    
    ENCODER_DETCETE &config = pcConfig->getConfig();

    tlv_out.SetType( tlv_in.GetType());
    tlv_out.SetValue( (uchar*)&config, sizeof(ENCODER_DETCETE), false);

    delete pcConfig;

    have_tlv_out = true;
    return 0;
}
int IVNSysConfig::ModEncoderDetectCfg( CTlv& tlv_in, CTlv& tlv_out, bool& have_tlv_out )
{
	if(tlv_in.GetLength() != sizeof(ENCODER_DETCETE))
	{
		return 0;
	}

	ENCODER_DETCETE* pConfig = (ENCODER_DETCETE*)tlv_in.GetValueBuf();

      CConfigEncoderDetect* pCfgEncoder = new CConfigEncoderDetect();
      assert(pCfgEncoder);
    
      pCfgEncoder->update();
    
      ENCODER_DETCETE &config = pCfgEncoder->getConfig();

	config.bEnable = pConfig->bEnable;
      pCfgEncoder->commit();

	return 0;
}

#endif


