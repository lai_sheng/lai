#pragma once

#include "System/Object.h"
#include "Functions/Ptz.h"
class CIVnPdu;

class CVNControl:public CObject
{
public:

	CVNControl();
	~CVNControl();

	int SetOsdTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int SetSysTime(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QueryChanTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int ModChanTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int CtrlMonSw(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int CtrlFrontBoard(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int SetExtAlarm(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int SimuAlarmIn(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int SimuAlarmOut(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int OprPTZCom(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	
	int CtrlPtz(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int ForceIFrame(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

private:
	int m_PreCmd;
	PTZ_OPT_STRUCT	m_Ptz_Opt_Struct;
	void PtzStopTimer(uint iChannel);

	void OnHumanStop(uint arg);
};

