#pragma once

#include "System/Object.h"
#include "System/Function.h"
#include "MultiTask/Timer.h"
#include "System/User.h"
#include "System/AppEvent.h"
//#include "Functions/NetKeyBoard.h"
#include <map>
#include <vector>
class CIVnPdu;
class CVNUser;
class CVNControl;
class CVNConfig;
class CVNLogState;
//class CVNAlmState;

class IVNSysConfig;
class IVNRemoteConfig;

struct conn;
class CUser;

#include "Net/NetCore.h"

typedef struct __ip_list_node
{
    IPDEF ipIP;
    time_t    ttLastValid;
    uint    iValidity;
}IP_LIST_NODE;

typedef std::vector<IP_LIST_NODE> IP_LIST;

class CFilterHelper:public CObject
{
public:
    PATTERN_SINGLETON_DECLARE(CFilterHelper);

    CFilterHelper();
    virtual ~CFilterHelper();


    int MsgProcess(struct conn *pConnect, char *buf, int len);

    struct conn *GetNetConn(int iDlgNo);
    int SetBlockPeer(IPDEF ipPeer, uint iPeriod, time_t LastValid);
    int  RegisterConn(struct conn *c);
    void UnRegisterConn(struct conn *c);
	static std::string m_userName;
private:
    /// 控制函数定义
    typedef TFunction2<int, CIVnPdu*&, CIVnPdu*&> FileterProc;

    CVNUser*      m_VapUser;/** 处理模块对象实例 */
    CVNControl*      m_VapControl;
    CVNConfig*      m_VapConfig;
    CVNLogState*      m_VapLogState;
    //CVNAlmState*      m_VapAlmState;

    IVNSysConfig*     m_VapSysConfig;
    IVNRemoteConfig* m_VapRemoteConfig;	
	//CNetKeyBoard* m_NetKeyBoard;

    //权限列表
    typedef std::map<int, std::string> AuthStringList;
    typedef std::map<int, std::string>::iterator Authiter;
    AuthStringList m_AuthStringList;

    /// 注册命令函数
    bool registerFilter(FileterProc proc, int enmCommand);    

    /// 注销命令函数
    bool unregisterFilter(int enmCommand);

    /// 管理控制函数的列表
    typedef std::map<int, FileterProc> FileterTable;
    FileterTable    m_sFilterTable;



    CTimer *m_pExpireTimer;
    IP_LIST m_NetBlockIp;/*!<屏蔽ip列表*/
    struct conn *m_NetConnList[C_MAX_TCP_CONN_NUM];/*!<多客户端连接的信息*/
    int CheckPeerIP(in_addr in);

    void onConnTimer(uint arg);    

    int m_iAlarmState[appEventAll]; /*用于记录告警类型*/    
    void onAppEvent(appEventCode code, int index,    appEventAction action, 
                    EVENT_HANDLER *param, const CConfigTable* data);

    int OprSubConn(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
    int RegQuest(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
    int ReqBookAlm(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
    int ReqBookPic(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);	

    int FilterCompat(int enmCommand, CIVnPdu*& pReq, CIVnPdu*& pResp);
    void AckNomoral(struct conn *pConnect , uchar iLastOpr, uchar iRetCode);

};

#define g_NetMsgProcesser (*CFilterHelper::instance())

