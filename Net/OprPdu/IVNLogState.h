
#pragma once

#include "System/Object.h"

class ICaptureManager;
class CIVnPdu;

class CVNLogState
{
public:

	CVNLogState();
	~CVNLogState();

	int QuerySysInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int CtrlRecord(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryAlarmRecordState(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QuerySystemOpr(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryLogs(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int CtrlDevice(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryDevice(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QueryAlarmRecord(struct conn *c,CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryRecordList(struct conn *c,CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
protected:
	int AckAblityTlv(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	ICaptureManager* m_pCaptureM;
};





