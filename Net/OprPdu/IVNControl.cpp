
#include "IVNControl.h"
#include "IVnPdu.h"
#include "Functions/Encode.h"
#include "Functions/General.h"
#include "Functions/Alarm.h"
#include "Functions/Comm.h"
#include "Devices/DevAlarm.h"

#include "System/BaseTypedef.h"
#include "Main.h"
#include "Net/NetClient/NetCaptureManager.h"
#include "APIs/CommonBSP.h"
#include "Functions/PtzTrace.h"
#include "Configs/ConfigCamera.h"

extern int g_nAlarmOut;
extern int g_nAlarmIn;
extern int g_nCapture;
enum EnFrontKey
{
    NKEY_POWER = 0,
    NKEY_ENTER,
    NKEY_ESC,
    NKEY_UP,
    NKEY_DOWN,
    NKEY_LEFT,
    NKEY_RIGHT,
    NKEY_0,
    NKEY_1,
    NKEY_2,
    NKEY_3,
    NKEY_4,
    NKEY_5,
    NKEY_6,
    NKEY_7,
    NKEY_8,
    NKEY_9,
    NKEY_10,
    NKEY_11,
    NKEY_12,
    NKEY_13,
    NKEY_14,
    NKEY_15,
    NKEY_16,
    NKEY_SPLT,
    NKEY_DIV1,
    NKEY_DIV9,
    NKEY_ADDR,
    NKEY_INFO,
    NKEY_REC,
    NKEY_FN1,
    NKEY_FN2,
    NKEY_PLAY,
    NKEY_STOP,
    NKEY_SLOW,
    NKEY_FAST,
    NKEY_PREV,
    NKEY_NEXT,
    NKEY_JMPUP,
    NKEY_JMPDN,
    NKEY_10PLUS,
    NKEY_SHIFT,
    NKEY_BACK,
};
typedef struct _net_comm_name
{
    char        ProtocolName[12];    // 12字节的协议名称
    uint            BaudRate;             // 4字节的波特率
    uchar            DataBits;             // 1字节的数据位
    uchar            StopBits;             // 1字节的停止位
    uchar            Parity;             // 1字节的奇偶校验
    uchar            Resove;                // 1字节保留
}NET_COMM_NAME;

CVNControl::CVNControl()
{
	m_PreCmd  = PTZ_OPT_NONE;
}

CVNControl::~CVNControl()
{
}

/*REQ_INFO_PROC cmd + subcmd*/
int CVNControl::SetOsdTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

    if (1 == pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        g_Encode.setOSDTitle(pReqPDU->m_pIpHdr->dvrip_p[4],                    //第几行
            pReqPDU->m_pIpHdr->dvrip_p[16],                                    // 叠加位置
            pReqPDU->m_pIpHdr->dvrip_p[12],                                    // 显示或隐藏
            (VD_PCSTR)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE),    // 数据内容
            pReqPDU->m_pIpHdr->dvrip_p[8]);//通道号
    }
    return 0;
}

int CVNControl::SetSysTime(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

    SYSTEM_TIME szTime;
    if (1 == pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        szTime.year    = pReqPDU->m_pIpHdr->dvrip_p[8] + 2000;
        szTime.month    = pReqPDU->m_pIpHdr->dvrip_p[9];
        szTime.day        = pReqPDU->m_pIpHdr->dvrip_p[10];
        szTime.hour    = pReqPDU->m_pIpHdr->dvrip_p[11];
        szTime.minute    = pReqPDU->m_pIpHdr->dvrip_p[12];
        szTime.second    = pReqPDU->m_pIpHdr->dvrip_p[13];

        g_General.UpdateSystemTime(&szTime, 2);
    }
    else if (0 == pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        
    }
    else
    {
        tracef("Wrong time opr(%d)\n", pReqPDU->m_pIpHdr->dvrip_p[0]);
    }
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(pRspPdu)
    {
        pRspPdu->packetBody();
        pRspPdu->m_pIpHdr->dvrip_cmd = REQ_SET_DTIME;
        pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
        
        SystemGetCurrentTime(&szTime);
        
        pRspPdu->m_pIpHdr->dvrip_p[8]    = (unsigned char) ((szTime.year-2000)&0xff);
        pRspPdu->m_pIpHdr->dvrip_p[9]    = (unsigned char) (szTime.month&0xff);
        pRspPdu->m_pIpHdr->dvrip_p[10]    = (unsigned char) (szTime.day&0xff);
        pRspPdu->m_pIpHdr->dvrip_p[11]    = (unsigned char) (szTime.hour&0xff);
        pRspPdu->m_pIpHdr->dvrip_p[12]    = (unsigned char) (szTime.minute&0xff);
        pRspPdu->m_pIpHdr->dvrip_p[13]    = (unsigned char) (szTime.second&0xff);

        pRespPDU = pRspPdu;
    }
    return 0;

}

int CVNControl::QueryChanTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

    std::string strTitle ="";
    if(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        CConfigChannelTitle cfgChnTitle;
        cfgChnTitle.update();

        for(int i=0;i<g_nCapture;i++)
        {
            strTitle+=cfgChnTitle[i].strName;
            if (i != g_nCapture - 1)
            {
                strTitle+="&&";
            }
        }
        
        //应答
        CIVnPdu* pRspPdu = new CIVnPdu();
        if(pRspPdu)
        {
            pRspPdu->packetBody((char*)strTitle.c_str(), strTitle.length());
            pRspPdu->m_pIpHdr->dvrip_cmd = ACK_CHAN_TITLE;        
            pRspPdu->m_pIpHdr->dvrip_p[0] = 1;
             
            pRespPDU = pRspPdu;
        }        
    } 
    
    return 0;

}


int CVNControl::ModChanTitle(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    std::string strTitle;
    strTitle.assign(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE, pReqPDU->m_pIpHdr->dvrip_extlen);


    if(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        CConfigChannelTitle cfgChnTitle;
        cfgChnTitle.update();
        strTitle+="&&";
        size_t szQot = 0;
        std::string strTmp;
        int iCount = 0;
        
        for (int i=0; i< g_nCapture; i++)
        {
            szQot = strTitle.find("&&");
            
            if ( szQot == std::string::npos)
            {
                break;
            }

            strTmp.assign(strTitle.begin(), strTitle.begin()+szQot);
            memset(cfgChnTitle[i].strName,0,CHANNEL_NAME_SIZE);
            strncpy(cfgChnTitle[i].strName, strTmp.c_str(), (strTmp.length() > CHANNEL_NAME_MAX_LEN)?CHANNEL_NAME_MAX_LEN-1:strTmp.length());
            iCount++;
            strTitle.erase(strTitle.begin(),strTitle.begin()+szQot+2);
        }

        if ( iCount == g_nCapture)
        {
            cfgChnTitle.commit();
        }
    }

    //无应答
    return 0;
}


/*画面分割切换*/
int CVNControl::CtrlMonSw(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    return 0;
}

int CVNControl::CtrlFrontBoard(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

    unsigned int key = 0;

    switch(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        case NKEY_POWER:
        {
            g_Challenger.Restart();
            return 0;
        }
        case NKEY_ENTER:
        {
            key = KEY_RET;
            break;
        }
        case NKEY_ESC:
        {
            key = KEY_ESC;
            break;
        }
        case NKEY_UP:
        {
            key = KEY_UP;
            break;
        }
        case NKEY_DOWN:
        {
            key = KEY_DOWN;
            break;
        }
        case NKEY_LEFT:
        {
            key = KEY_LEFT;
            break;
        }
        case NKEY_RIGHT:
        {
            key = KEY_RIGHT;
            break;
        }
        case NKEY_0:
        case NKEY_1:
        case NKEY_2:
        case NKEY_3:
        case NKEY_4:
        case NKEY_5:
        case NKEY_6:
        case NKEY_7:
        case NKEY_8:
        case NKEY_9:
        case NKEY_10:
        case NKEY_11:
        case NKEY_12:
        case NKEY_13:
        case NKEY_14:
        case NKEY_15:
        case NKEY_16:
        {
            key = KEY_0 + pReqPDU->m_pIpHdr->dvrip_p[0] - NKEY_0;
            break;
        }
        case NKEY_SPLT:
        {
            key = KEY_SPLIT;
            break;
        }
        case NKEY_DIV1:
        {
            key = KEY_SPLIT1;
            break;
        }
        case NKEY_DIV9:
        {
            key = KEY_SPLIT9;
            break;
        }
        case NKEY_ADDR:
        {
            key = KEY_ADDR;
            break;
        }
        case NKEY_INFO:
        {
            key = KEY_INFO;
            break;
        }
        case NKEY_REC:
        {
            key = KEY_RIGHT;
            break;
        }
        case NKEY_FN1:
        {
            key = KEY_FUNC;
            break;
        }
        case NKEY_PLAY:
        {
            key = KEY_PLAY;
            break;
        }
        case NKEY_STOP:
        {
            key = KEY_STOP;
            break;
        }
        case NKEY_SLOW:
        {
            key = KEY_SLOW;
            break;
        }
        case NKEY_FAST:
        {
            key = KEY_FAST;
            break;
        }
        case NKEY_PREV:
        {
            key = KEY_PREV;
            break;
        }
        case NKEY_NEXT:
        {
            key = KEY_NEXT;
            break;
        }
        case NKEY_JMPUP:
        {
            key = KEY_PGUP;
            break;
        }
        case NKEY_JMPDN:
        {
            key = KEY_PGDN;
            break;
        }
        case NKEY_10PLUS:
        {
            key = KEY_10PLUS;
            break;
        }
        case NKEY_FN2:
        {
            key = KEY_SHIFT;
            break;
        }
        case NKEY_BACK:
        {
            key = KEY_BACK;
            break;
        }
        default:
        {
            return 0;
        }
    }
    static CTimer tmHumantimer("Net_SimuKey");
    tmHumantimer.Stop();
    tmHumantimer.Start(this, (VD_TIMERPROC)&CVNControl::OnHumanStop, 350, 0, key);
    return 0;
}

void CVNControl::OnHumanStop(uint arg)
{

}

enum enmSetExtAlarm
{
    QueryExtAlmInSetState = 0,
    QueryExtAlmOutSetState,
    SetExtAlmInSetState,
    //SetExtAlmOutSetState,

    SetExtAlmOutSetType=3,    //设置外部报警输出的类型  add by ilena zhou 2010-01-25
   GetExtAlmOutSetType=5,    //获取外部报警输出的类型  add by ilena zhou 2010-01-25
    
};

int CVNControl::SetExtAlarm(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_STAT_ALARM;
    pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
    
    pRespPDU = pRspPdu;

    int ii = 0;

    switch(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        case QueryExtAlmInSetState:
        {
            memset(&pRspPdu->m_pIpHdr->dvrip_p[4], 2, 20);

            //读取外部报警输入配置
            CConfigAlarm *pAlarm = new CConfigAlarm();
            pAlarm->update();

            for (ii=0; ii<g_nAlarmIn; ii++)
            {
                CONFIG_ALARM &cfgAlarm = pAlarm->getConfig(ii);
                if (cfgAlarm.bEnable)
                {
                    pRspPdu->m_pIpHdr->dvrip_p[ii+4] = 1;
                }
                else
                {
                    pRspPdu->m_pIpHdr->dvrip_p[ii+4] = 0;
                }
            } 
            delete pAlarm;

            break;
        }
        case QueryExtAlmOutSetState:
        {
            memset(&pRspPdu->m_pIpHdr->dvrip_p[4], 2, 20);

            uint uiStatus = CDevAlarm::instance()->GetOutState();
            for (ii=0; ii<g_nAlarmOut; ii++)
            {
                //外部报警状态 
                if(uiStatus & (1<<ii))
                {
                    pRspPdu->m_pIpHdr->dvrip_p[ii+4] = 1;
                }
                else
                {
                    pRspPdu->m_pIpHdr->dvrip_p[ii+4] = 0;
                }

            } 

            break;
        }
        case SetExtAlmInSetState:
        {
            CConfigAlarm *pAlarm = new CConfigAlarm;
            pAlarm->update();

            for (ii = 0; ii < g_nAlarmIn; ii++)
            {
                CONFIG_ALARM &cfgAlarm = pAlarm->getConfig(ii);

                if(pReqPDU->m_pIpHdr->dvrip_p[ii+4] == 1)
                {
                    cfgAlarm.bEnable = TRUE;
                }
                else if (pReqPDU->m_pIpHdr->dvrip_p[ii+4] == 0)
                {
                    cfgAlarm.bEnable = FALSE;
                }
            }

            pAlarm->commit();
            delete pAlarm;

            break;
        }
        case SetExtAlmOutSetType:
        {
            uint dwAlmOutTypeii = 0;
            uint dwAlmOutii = 0;
            for(ii=0; ii<g_nAlarmOut; ii++)
            {
                int tmp_outtype = pReqPDU->m_pIpHdr->dvrip_p[ii+4];
                if(tmp_outtype == 3)    
                {
                    dwAlmOutTypeii  = dwAlmOutTypeii +  0;
                }
                else
                {
                if(tmp_outtype == 1)
                    {
                        dwAlmOutTypeii = dwAlmOutTypeii + (1<<(ii*2));//power(2,(ii * 2));
                        dwAlmOutii |= (1<<ii);
                    }
                else 
                    {
                    if (tmp_outtype == 4)
                        {
                        dwAlmOutTypeii  = dwAlmOutTypeii + (1<<(ii * 2 + 1)); //power(2,(ii * 2 + 1));
                        }
                    }
                }
            }
            g_Alarm.SetAlarmOutType(dwAlmOutTypeii);
            g_Alarm.SetAlarmOut(dwAlmOutii, ALARM_OUT_MANUAL);

            break;
        }
        case GetExtAlmOutSetType:
        {
               // 外部报警类型。3为配置，1为始终，4为关闭(2同时用为默认填零值) add by ilena zhou
            memset(&pRespPDU->m_pIpHdr->dvrip_p[4], 2, 20);
            int OutTypei= g_Alarm.GetAlarmOutType();
            for(ii=0; ii<g_nAlarmOut; ii++)
            {
                if(OutTypei & (1<<ii*2))    
                {
                    pRespPDU->m_pIpHdr->dvrip_p[ii+4] = 1;//为始终，
                }
                else 
                {
                    if(OutTypei & (2<<ii*2))
                    {
                        pRespPDU->m_pIpHdr->dvrip_p[ii+4] = 4;
                        //为关闭
                    }
                    else//((OutTypei & (0<<ii*2)) == 0)
                    {
                        pRespPDU->m_pIpHdr->dvrip_p[ii+4] = 3;
                        // 3为配置
                    }
                }
                // end add by ilena zhou    
            }
        }
            
        default:
        break;
    }

    return 0;
}

int CVNControl::SimuAlarmIn(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = pReqPDU->m_pIpHdr->dvrip_cmd;
    pRespPDU = pRspPdu;
    
    static uint uiAlarmInState = 0;
    for (int i = 0; i < g_nAlarmIn; i++)
    {
        if(1 == pReqPDU->m_pIpHdr->dvrip_p[i+4])
        {
            uiAlarmInState |= BITMSK(i);
        }
        else if(0 == pReqPDU->m_pIpHdr->dvrip_p[i+4])
        {
            if (uiAlarmInState & BITMSK(i))
            {
                uiAlarmInState &= ~(BITMSK(i));
            }
        }
    }

    g_Alarm.VD_SendMessage(appEventAlarmLocal, uiAlarmInState, appEventAlarmNet);

    return 0;
}

int CVNControl::SimuAlarmOut(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = pReqPDU->m_pIpHdr->dvrip_cmd;
    pRespPDU = pRspPdu;
    return 0;
}


int CVNControl::OprPTZCom(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    
    if ( pReqPDU->m_pIpHdr->dvrip_p[0] == 0)
    {
        int iProtocolNums = g_Ptz.GetProtocolCount() + 1;
        if ( iProtocolNums > 0)
        {
            CPacket* pPacker = g_PacketManager.GetPacket(iProtocolNums * sizeof(NET_COMM_NAME));
            if (pPacker)
            {
                char* pBuffer = (char* )pPacker->GetBuffer();
                for (int i = 0; i < iProtocolNums; i++)
                {
                    NET_COMM_NAME* pComName = (NET_COMM_NAME*)(pBuffer+i*sizeof(NET_COMM_NAME));
                    memset(pComName, 0, sizeof(NET_COMM_NAME));
                    if(i == 0)
                    {
                        strncpy(pComName->ProtocolName, "NONE", sizeof(pComName->ProtocolName));
                    }
                    else    
                    {
                        PTZ_OPT_ATTR PtzAttr;
                        memset(&PtzAttr, 0, sizeof(PtzAttr));
                        g_Ptz.GetProtocolAttr(&PtzAttr, i-1);
                        strncpy(pComName->ProtocolName, PtzAttr.Name, 12);
                    }
                }
                
                pRspPdu->packetBody((char* )pPacker->GetBuffer(), iProtocolNums * sizeof(NET_COMM_NAME));

                pPacker->Release();
            }
            
        }
        else
        {
            pRspPdu->packetBody();
        }
        
        pRspPdu->m_pIpHdr->dvrip_cmd = ACK_PROT_TITLE;
        pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
        pRspPdu->m_pIpHdr->dvrip_p[1] = iProtocolNums;
        
    }
    else if ( pReqPDU->m_pIpHdr->dvrip_p[0] == 1)
    {
    
        int iProtocolNums = g_Comm.GetProtocolCount();
        tracef("The Comm Ports:%d\n",iProtocolNums);

        uint dwMask = g_Comm.GetProtocolMask();    // 掩码表示0~31个协议是否有效

        if ( iProtocolNums > 0)
        {
            CPacket* pPacker = g_PacketManager.GetPacket(iProtocolNums * sizeof(NET_COMM_NAME));
            if (pPacker)
            {
                char* pBuffer = (char* )pPacker->GetBuffer();

                for (int i = 0; i < iProtocolNums; i++)
                {
                    NET_COMM_NAME* pComName = (NET_COMM_NAME*)(pBuffer+i*sizeof(NET_COMM_NAME));
                    memset(pComName, 0, sizeof(NET_COMM_NAME));
                    strncpy(pComName->ProtocolName, g_Comm.GetProtocolName(i), 12);
                }
                
                pRspPdu->packetBody((char* )pPacker->GetBuffer(), iProtocolNums * sizeof(NET_COMM_NAME));

                pPacker->Release();
            }
            
        }
        else
        {
            pRspPdu->packetBody();
        }
        
        pRspPdu->m_pIpHdr->dvrip_cmd = ACK_PROT_TITLE;
        pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
        pRspPdu->m_pIpHdr->dvrip_p[1] = iProtocolNums;
        memcpy((void *)&pRspPdu->m_pIpHdr->dvrip_p[2], &dwMask, sizeof(uint));

    }
    else if (pReqPDU->m_pIpHdr->dvrip_p[0] == 2)
    {
        PTZ_OPT_ATTR PtzAttr;
        int iProtocolIndex = pReqPDU->m_pIpHdr->dvrip_p[1] - 1;    // 协议序号
        if (iProtocolIndex < 0)
        {
            pRspPdu->packetBody();
            pRspPdu->m_pIpHdr->dvrip_cmd = ACK_PROT_TITLE;
            pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
            pRspPdu->m_pIpHdr->dvrip_p[1] = pReqPDU->m_pIpHdr->dvrip_p[1];
        }
        else
        {
            memset(&PtzAttr, 0, sizeof(PtzAttr));
            g_Ptz.GetProtocolAttr(&PtzAttr, iProtocolIndex);
            
            pRspPdu->packetBody((char *)&PtzAttr, sizeof(PtzAttr));
            pRspPdu->m_pIpHdr->dvrip_cmd = ACK_PROT_TITLE;
            pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
            pRspPdu->m_pIpHdr->dvrip_p[1] = pReqPDU->m_pIpHdr->dvrip_p[1];
        }
    }

    pRespPDU = pRspPdu;
    return 0;
}
static uchar NET_PTZ_CMD[] =
{
    //0x0
    PTZ_OPT_UP, PTZ_OPT_DOWN, PTZ_OPT_LEFT, PTZ_OPT_RIGHT, PTZ_OPT_ZOOM_TELE, PTZ_OPT_ZOOM_WIDE,
    //0x6
    PTZ_OPT_NONE, PTZ_OPT_FOCUS_FAR, PTZ_OPT_FOCUS_NEAR,PTZ_OPT_IRIS_LARGE, PTZ_OPT_IRIS_SMALL,
    //0xb
    PTZ_OPT_NONE,  PTZ_OPT_NONE,  PTZ_OPT_NONE,  PTZ_OPT_LIGHT_ON,  PTZ_OPT_STARTTOUR,
    //0x10
    PTZ_OPT_GOTOPRESET, PTZ_OPT_SETPRESET, PTZ_OPT_CLEARPRESET, PTZ_OPT_NONE, PTZ_OPT_NONE,
    //0x15
    PTZ_OPT_NONE,  PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE,
    //0x1b
    PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE,
    //0x20
    PTZ_OPT_LEFTUP, PTZ_OPT_RIGHTUP, PTZ_OPT_LEFTDOWN, PTZ_OPT_RIGHTDOWN,
    //0x24
    PTZ_OPT_ADDTOUR, PTZ_OPT_DELTOUR, PTZ_OPT_CLEARTOUR,
    //0x27
    PTZ_OPT_AUTOPANON, PTZ_OPT_AUTOPANOFF, PTZ_OPT_SETLEFTLIMIT, PTZ_OPT_SETRIGHTLIMIT, PTZ_OPT_AUTOSCANON, PTZ_OPT_AUTOSCANOFF,
    //0x2d
    PTZ_OPT_SETPATTERNBEGIN, PTZ_OPT_SETPATTERNEND, PTZ_OPT_STARTPATTERN, PTZ_OPT_STOPPATTERN, PTZ_OPT_CLEARPATTERN,
    //0x32
    PTZ_OPT_ALARM_SEARCH, PTZ_OPT_POSITION, PTZ_OPT_AUXON, PTZ_OPT_AUXOFF,
    //0x36
    PTZ_OPT_MENU, PTZ_OPT_EXIT, PTZ_OPT_ENTER, PTZ_OPT_ESC, PTZ_OPT_MENUUP, PTZ_OPT_MENUDOWN, PTZ_OPT_MENULEFT, PTZ_OPT_MENURIGHT,
    //0x3e
    PTZ_OPT_SWITCH,PTZ_OPT_SETPRESETNAME,
    //0x40
    PTZ_OPT_ALARMPTZ,PTZ_MATRIX_SWITCH,PTZ_LIGHT_CONTROLLER,
};

static inline int FindIndex(uchar cmd)
{
    int i = 0;
    int ret = -1;
    while(NET_PTZ_CMD[i] <= PTZ_OPT_MENURIGHT)
    {
        if (NET_PTZ_CMD[i] == cmd)
        {
            ret = i;
            break;
        }
        else
        {
            i++;
        }
    }

    return ret;
}

int CVNControl::CtrlPtz(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    int 		iChannel    = pReqPDU->m_pIpHdr->dvrip_p[1];    //第10字节：  通道号M
    uchar    	command     = pReqPDU->m_pIpHdr->dvrip_p[2];    //第11字节： 命令 
    uchar     	arg1        = pReqPDU->m_pIpHdr->dvrip_p[3];
    uchar    	arg2        = pReqPDU->m_pIpHdr->dvrip_p[4];  // 速度
    uchar    	arg3        = pReqPDU->m_pIpHdr->dvrip_p[8];

  

#ifdef IPC_JZ_NEW   //仅用于君正方案摇头机web端临时调试用


#if 1 //lhc
   printf("iChannel is [%d]\n",iChannel); 		
	printf("command is [%d]\n",command);
	printf("arg1 is [%d]\n",arg1);
	printf("arg2 is [%d]\n",arg2);
	printf("arg3 is [%d]\n",arg3);
#endif
	static uchar old_command = 4;// up:0  down:1  left:2  right:3  none(stop):4
	if (command >= 4 || arg2 > 8)
	{	
		trace("para command [%d]  agr2 [%d]  error \n ",command,arg2);
		return -1;
	}

	if (arg2 == 0)
	{
#ifdef MOTOR	
		//PtzWrite(NULL, 0);
		g_PtzTrace.PtzRunCmd(PTZRUN_STOP);
#endif
		old_command = 4;
	}
	else if (old_command != command)
	{
#ifdef MOTOR
		#if 0
		CConfigCamera ConfigCamera ;
		ConfigCamera.update();
		CONFIG_CAMERA& cfgCamera = ConfigCamera.getConfig();
		#endif
		
		old_command = command;
		#if 0
		switch(command)
		{
			case 0://up
				if(1 == cfgCamera.VerReverse){
					PtzWrite(NULL, 2);
				}else{
					PtzWrite(NULL, 1);
				}
				break;
			case 1://down
				if(1 == cfgCamera.VerReverse){
					PtzWrite(NULL, 1);
				}else{
					PtzWrite(NULL, 2);
				}
				break;
			case 2://left
				if(1 == cfgCamera.HorReverse){
					PtzWrite(NULL, 4);
				}else{
					PtzWrite(NULL,3);
				}
				break;
			case 3://right
				if(1 == cfgCamera.HorReverse){
					PtzWrite(NULL, 3);
				}else{
					PtzWrite(NULL,4);
				}
				break;
			default:
				break;
		}
		#else
		switch(command)
		{
			case 0://up
				g_PtzTrace.PtzRunCmd(PTZRUN_UP);
				break;
			case 1://down
				g_PtzTrace.PtzRunCmd(PTZRUN_DOWN);
				break;
			case 2://left
				g_PtzTrace.PtzRunCmd(PTZRUN_LEFT);
				break;
			case 3://right
				g_PtzTrace.PtzRunCmd(PTZRUN_RIGHT);
				break;
#if 0 //产测归位，自动巡航
			case 32://goback
				g_PtzTrace.PtzRunCmd(PTZRUN_GOTO_MID);
				break;
			case 33://cruise
				g_PtzTrace.PtzRunCmd(PTZRUN_CRUISE);	
				break;	
#endif
			default:
				break;
		}
		#endif
#endif	
	}
	
	return 0;
#endif

    CConfigPTZ    cCfgPtz;
    cCfgPtz.update();
    CONFIG_PTZ& cfg = cCfgPtz.getConfig(iChannel);

    int index = g_Ptz.GetIndex(cfg.strProtocolName);
    if (index == 0)
    {
        return 0;
    }

    PTZ_OPT_ATTR attr;
    g_Ptz.GetProtocolAttr(&attr, index - 1);

    if (command < sizeof(NET_PTZ_CMD))
    {
        if (command <= FindIndex(PTZ_OPT_CLEARPRESET))
        {
            if (NET_PTZ_CMD[command] == PTZ_OPT_LIGHT_ON)
            {
                if (arg1 == 1)
                {
                    m_Ptz_Opt_Struct.cmd = PTZ_OPT_LIGHT_ON;
                }
                else if (arg1 == 0)
                {
                    m_Ptz_Opt_Struct.cmd = PTZ_OPT_LIGHT_OFF;
                }
                else
                {
                    return FAILURE_RET;
                }
            }
            else if (NET_PTZ_CMD[command] == PTZ_OPT_STARTTOUR)
            {
                if (arg3 == 76)
                {
                    m_Ptz_Opt_Struct.cmd = PTZ_OPT_STARTTOUR;
                }
                else if (arg3 == 96)
                {
                    m_Ptz_Opt_Struct.cmd = PTZ_OPT_STOPTOUR;
                }
                else
                {
                    return FAILURE_RET;
                }
                m_Ptz_Opt_Struct.arg1 = arg1;
#ifdef DEF_SOFT_TOUR
                m_Ptz_Opt_Struct.arg1 = arg1 - 1;
                g_Ptz.PTZ_OnPtzTour(m_Ptz_Opt_Struct, iChannel);
                return 0;
#endif    
            }
            else
            {
                m_Ptz_Opt_Struct.cmd = NET_PTZ_CMD[command];
                if (command <= FindIndex(PTZ_OPT_DOWN))
                {
                    m_Ptz_Opt_Struct.arg1 = attr.PanSpeedMin + ((arg2==1)?0:arg2) * (attr.PanSpeedMax - attr.PanSpeedMin)/8 + ((arg2==1)?1:0);
                }
                else    if (command <=FindIndex(PTZ_OPT_RIGHT))
                {
                    m_Ptz_Opt_Struct.arg1 = attr.TileSpeedMin + ((arg2==1)?0:arg2) * (attr.TileSpeedMax - attr.TileSpeedMin)/8 + ((arg2==1)?1:0);
                }
                else
                {
                    m_Ptz_Opt_Struct.arg1 = arg2;
                }

                if (arg2 == 0)
                {
                    m_Ptz_Opt_Struct.arg1 = 0;
                }
            }
        }
        else if ((command>= FindIndex(PTZ_OPT_LEFTUP)) && (command <= FindIndex(PTZ_OPT_RIGHTDOWN)))
        {
            m_Ptz_Opt_Struct.cmd = NET_PTZ_CMD[command];
            m_Ptz_Opt_Struct.arg1 = attr.TileSpeedMin +  ((arg1==1)?0:arg1) * (attr.TileSpeedMax - attr.TileSpeedMin)/8 + ((arg1==1)?1:0);
            m_Ptz_Opt_Struct.arg2 = attr.PanSpeedMin +  ((arg2==1)?0:arg2) * (attr.PanSpeedMax - attr.PanSpeedMin)/8 + ((arg2==1)?1:0);

            if (arg1 == 0)
            {
                m_Ptz_Opt_Struct.arg1 = 0;
            }
            
            if (arg2 == 0)
            {
                m_Ptz_Opt_Struct.arg2 = 0;
            }
        }
        else
        {
        //巡航，线扫，巡迹等操作云台，停止下巡航操作
        #ifdef DEF_SOFT_TOUR
            m_Ptz_Opt_Struct.cmd = PTZ_OPT_STOPTOUR;
            m_Ptz_Opt_Struct.arg1 = PTZ_ARG_POSITIVE - 1;
            g_Ptz.PTZ_OnPtzTour(m_Ptz_Opt_Struct, iChannel);
        #endif
            if (NET_PTZ_CMD[command] == PTZ_OPT_POSITION)
	        {
	            m_Ptz_Opt_Struct.cmd = NET_PTZ_CMD[command];
	            m_Ptz_Opt_Struct.arg1 = *((int *)&(pReqPDU->m_pIpHdr->dvrip_p[12]));
	            m_Ptz_Opt_Struct.arg2 = *((int *)&(pReqPDU->m_pIpHdr->dvrip_p[16]));
	            m_Ptz_Opt_Struct.arg3 = *((int *)&(pReqPDU->m_pIpHdr->dvrip_p[20]));

	        }
	        else if(NET_PTZ_CMD[command] == PTZ_MATRIX_SWITCH)
	        {
	            return 0;
	        }
	        else if (NET_PTZ_CMD[command] == PTZ_LIGHT_CONTROLLER)//针对特殊的灯光控制器命令
	        {
	            uchar pdat[4] = {0x00,0x18,0x00,0x00};
	            pdat[0] = arg1;
	            pdat[2] = arg2;
	            pdat[3] = arg3;
	            g_Ptz.WritePtz((uchar *)pdat, 4 , iChannel);
	            return 0;
	        }
	        else
	        {
	            m_Ptz_Opt_Struct.cmd = NET_PTZ_CMD[command];
	            m_Ptz_Opt_Struct.arg1 = arg1;
	            m_Ptz_Opt_Struct.arg2 = arg2;
	            m_Ptz_Opt_Struct.arg3 = arg3;
	        }
        }
        
        //在连接600时对停止命令不做处理（变倍，聚焦，光圈），否者会造成一变到底的问题
        if((m_Ptz_Opt_Struct.cmd >= PTZ_OPT_ZOOM_WIDE) 
            && (m_Ptz_Opt_Struct.cmd <= PTZ_OPT_IRIS_SMALL)
            &&(m_Ptz_Opt_Struct.arg1 == 0)
            &&(m_Ptz_Opt_Struct.arg2 == 0)
            &&(m_Ptz_Opt_Struct.arg3 == 0))
        {
            return 0;
        }

		if (m_Ptz_Opt_Struct.cmd == PTZ_OPT_ZOOM_WIDE) {
			m_Ptz_Opt_Struct.arg1 *= 2;
		//	printf("~~~PTZ_OPT_ZOOM_WIDE [%d,%d,%d]\n", m_Ptz_Opt_Struct.arg1, m_Ptz_Opt_Struct.arg2,m_Ptz_Opt_Struct.arg3);
		}
		if (m_Ptz_Opt_Struct.cmd == PTZ_OPT_ZOOM_TELE) {
			m_Ptz_Opt_Struct.arg1 *= 2;
		//	printf("~~~PTZ_OPT_ZOOM_TELE [%d,%d,%d]\n", m_Ptz_Opt_Struct.arg1, m_Ptz_Opt_Struct.arg2,m_Ptz_Opt_Struct.arg3);
		}

		
	//方向控制时速度为0 代表停止命令
        if ((m_Ptz_Opt_Struct.cmd >= PTZ_OPT_LEFTUP) 
            && (m_Ptz_Opt_Struct.cmd <= PTZ_OPT_RIGHTDOWN)
            &&(m_Ptz_Opt_Struct.arg1 == 0))
    	{
		g_Ptz.Stop(iChannel, &m_Ptz_Opt_Struct);
    	}
    	else
    	{
	        //网络上送来的是开始命令，DVR主动停下来
	        static CTimer TimrPtz("TimrPtz");
	        TimrPtz.Stop();
	        g_Ptz.Start(iChannel, &m_Ptz_Opt_Struct);
	        if ((m_Ptz_Opt_Struct.cmd >= PTZ_OPT_LEFTUP) 
	            && (m_Ptz_Opt_Struct.cmd <= PTZ_OPT_IRIS_SMALL))
	        {            
	            TimrPtz.Start(this, (VD_TIMERPROC)&CVNControl::PtzStopTimer, attr.Internal, 0, iChannel);
	        }
    	}
    }
    return 0;
}


void CVNControl::PtzStopTimer(uint iChannel)
{
    g_Ptz.Stop(iChannel, &m_Ptz_Opt_Struct);
	m_PreCmd  = PTZ_OPT_NONE;
}

int CVNControl::ForceIFrame(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

	int iChn = pReqPDU->m_pIpHdr->dvrip_p[0];
	int StreamType = pReqPDU->m_pIpHdr->dvrip_p[1];

	if((iChn >= 0 && iChn < g_nCapture)
		&& (StreamType >= CHL_MAIN_T && StreamType <CHL_3IRD_T))
	{
			ICapture::instance(iChn)->SetIFrame(StreamType);
	}
        //应答
        CIVnPdu* pRspPdu = new CIVnPdu();
        if(pRspPdu)
        {
		pRspPdu->packetBody();
		pRspPdu->m_pIpHdr->dvrip_cmd = REQ_USER_FORCEIFRAME;        
             
              pRespPDU = pRspPdu;
        }    

    return 0;
}
