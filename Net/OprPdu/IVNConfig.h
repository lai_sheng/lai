#pragma once
#include "System/Object.h"
#include "APIs/Types.h"
#include "Configs/ConfigWorksheet.h"

//工作表类型
enum _worksheet_type_
{
    WORKSHEET_TYPE_RECORD = 1,    //普通录像
    WORKSHEET_TYPE_ALARM,        //本地报警
    WORKSHEET_TYPE_NETALARM,    //网络报警
    WORKSHEET_TYPE_MOTION,        //动态检测
    WORKSHEET_TYPE_BLIND,        //遮挡检测
    WORKSHEET_TYPE_LOSS,        //视频丢失
    WORKSHEET_TYPE_NOHDD,        //无硬盘
    WORKSHEET_TYPE_ERRHDD,        //硬盘出错
    WORKSHEET_TYPE_NOSPACEHDD,    //硬盘无空间
    WORKSHEET_TYPE_NETABORT,    //断网
    WORKSHEET_TYPE_DECALARM,    //解码器报警
    WORKSHEET_TYPE_SNAP,
    WORKSHEET_TYPE_ALL,            //一般工作表类型在这之前添加
};

enum _event_type_
{
    EVENT_ALARM = 1,    //本地报警事件
    EVENT_NETALARM,        //网络报警事件
    EVENT_DECALARM,        //解码器报警事件
    EVENT_MOTION,        //动态检测事件
    EVENT_BLIND = 5,        //遮挡检测事件
    EVENT_LOSS,            //视频丢失事件
    EVENT_NOHDD,        //无硬盘事件
    EVENT_ERRHDD,        //硬盘出错事件
    EVENT_NOSPACEHDD,    //硬盘空间不足
    EVENT_NETABORT = 10,     //断网事件
    EVENT_CODER_ALARM,    //编码器事件
    EVENT_INFRARED_ALARM = 12,//红外报警事件
    EVENT_IP_CONFLICT = 14,//IP冲突报警事件added by wyf on 20100310    
    EVNET_ALL,            //确保放在最后
};

class CIVnPdu;
class CPacket;
class ICaptureManager;

class CVNConfig
{
public:

	CVNConfig();
	~CVNConfig();

	int SetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int GetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int ModMac(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int ImportCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int ExportCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	
private:
	int ModGeneralCfg(CIVnPdu*& pReqPDU);
	int ModCommCfg(CIVnPdu*& pReqPDU);
	int ModNetCfg(CIVnPdu*& pReqPDU);	
	int ModTitleCfg(CIVnPdu*& pReqPDU);
	int ModMailCfg(CIVnPdu*& pReqPDU);
	int ModDisplayCfg(CIVnPdu*& pReqPDU);
	int ModAutoMaintain(CIVnPdu*& pReqPDU);
	int ModPTZCfg(CIVnPdu*& pReqPDU);
	int ModExCapture(CIVnPdu*& pReqPDU);
	
	int ModNtpCfg(CIVnPdu*& pReqPDU);
	int ModPPPOECfg(CIVnPdu*& pReqPDU);
	int ModIPFilterCfg(CIVnPdu*& pReqPDU);
	int ModDDNSCfg(CIVnPdu*& pReqPDU);
	int ModRegServer(CIVnPdu*& pReqPDU);
	int ModDHCPCfg(CIVnPdu*& pReqPDU);
	int ModDNSCfg(CIVnPdu*& pReqPDU);
	int ModVSPCfg(CIVnPdu*& pReqPDU);
	int ModDspBitRate(CIVnPdu*& pReqPDU);
	
	int ModTransferPolicy(CIVnPdu*& pReqPDU);
	int ModHighDownload(CIVnPdu*& pReqPDU);

	int ModRecordCfg(CIVnPdu*& pReqPDU);
	int ModEncodeCfg(CIVnPdu*& pReqPDU);
	int ModColorCfg(CIVnPdu*& pReqPDU);
	int ModEventCfg(CIVnPdu*& pReqPDU);
	int ModWorkSheet(CIVnPdu*& pReqPDU);

	int ModVideCover(CIVnPdu*& pReqPDU);
	int ModVideoMatrix(CIVnPdu*& pReqPDU);
	int ModAudioFormat(CIVnPdu*& pReqPDU);

	int ModDecode(CIVnPdu*& pReqPDU);
	int ModGooLink(CIVnPdu*& pReqPDU);

//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
	int QueryGeneralCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryCommCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryNetCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryPTZCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryTitleCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryDisplay(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryExCapture(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryMailCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	
	int QueryPPPOE(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryIPFilter(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);	
	int QueryDDNS(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryVspCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryUpnpCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryFTPCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QureyNtpCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryDHCPCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryDNSCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryTransferPolicy(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryRegServer(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);


	int QueryVideoMatrix(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryAudioFormat(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QueryDSPInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryAutoMaintain(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryRecord(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryEncode(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryColor(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QueryHighDownload(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryVideoCover(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryWorkSheet(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryEvent(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);

	int QueryDecode(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
	int QueryGooLink(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU);
private:
	int		m_InportRevPacketNum;		///< 配置导入时候接收到的总包数
	int		m_InportRevCfgTotalBytes;	///< 接收到的总字节数
	CPacket	*m_pPacket;					///< 配置导入包缓冲
    int     PacketWorkSheet(CIVnPdu*& pRespPDU,const CONFIG_WORKSHEET *stWS);
    ICaptureManager* m_pCaptureM;
protected:
	typedef struct
	{
	   int channel;//!本地解码通道号, 从0开始
	   ushort device_type;//!设备类型, 0—威乾设备
	   ushort device_port;//连接前端设备端口
	   uint device_ip;//网络字节序储存, 连接的前端设备ip
	   uint device_channel;//!远程通道号
	   uint enable;//!使能开关
	   uint stream_type; //!连接码流类型,0-主码流
	   char username[128]; //!连接前端用户名
	   char password[128]; //!连接密码
	}CFG_DECODE_T;

};


