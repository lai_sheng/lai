#include "IVNConfig.h"
#include "IVnPdu.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "Functions/Encode.h"
#include "Functions/DriverManager.h"
#include "Functions/Record.h"
#include "Functions/Comm.h"
#include "Functions/General.h"
//#include "Functions/Display.h"
#include "Configs/ConfigConversion.h"
#include "Configs/ConfigManager.h"
#include <netinet/in.h>
#ifdef WIN32
#undef OEM_TYPE
#define OEM_TYPE "VS"
#endif
#include "Functions/InExport.h"
#include "System/AppConfig.h"
#include "System/File.h"
#include "Configs/ConfigNet.h"

#include "System/BaseTypedef.h"
#include "System/Tlv.h"
#include "Main.h"
#include "APIs/Net.h"
#include "APIs/ExCapture.h"

#ifdef _USE_SMTP_MODULE
#include "Net/Dlg/DlgSmtpCli.h"
#endif

#ifdef _NET_USE_DHCPCLIENT
#include "Net/Dlg/DlgDhcpCli.h"
#endif

#ifdef USING_PROTOCOL_NTP
#include "Net/Dlg/DlgNtpCli.h"
#endif

#include "Configs/ConfigDigiChManager.h"

extern INT WriteFacInfo(SAVENANDINFO_T *pInFacInfo);
extern INT ReadFacInfo(SAVENANDINFO_T *pOutFacInfo);


typedef struct tagCONFIG_INTELLIGENTDETECT_NET
{
	int iIntelEnable;							/*智能联动使能*/
	IPADDR IntelIPaddr;			/*智能通道IP地址 */	
	EVENT_HANDLER_NET hEvent;					/*智能联动 */
} CONFIG_INTELLIGENTDETECT_NET;

typedef struct __config_ex_capture_t
{
    uchar         iQuality;        // 画质
    uchar         iReserved[31];    // 保留
} CONFIG_EX_CAPTURE_T;

typedef struct {
    uchar    Ptz_Version[8];        /*!< 版本号 */
    PTZ_ATTR PTZAttr;            /*!< 串口属性　*/
    ushort    DestAddr;            /*!< 目的地址 0-255 */
    ushort      Protocol;            /*!< 协议类型 保存协议的下标，动态变化 */
    ushort    MonitorAddr;        /*!< 监视器地址 0-64 */
    uchar    Reserved[10];        /*!< 保留 */
} OLD_CONFIG_PTZ;

// 为了兼容以前 sigh...
typedef struct {
    uchar    Ptz_Version[8];        /*!< 版本号 */
    uint    DestAddr;            /*!< 目的地址 */
    uint    Protocol;            /*!< 协议类型 */
    PTZ_ATTR PTZAttr;            /*!< 串口属性 */
} __NET_CONFIG_PTZ;


CVNConfig::CVNConfig():m_InportRevPacketNum(0), m_InportRevCfgTotalBytes(0), m_pPacket(NULL)
{
	m_pCaptureM = ICaptureManager::instance();
}

CVNConfig::~CVNConfig()
{
}

int CVNConfig::SetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }
    
    enum modifyConfigCMD
    {
        mofifySuccess = 0,
        modifySaveFailed,
        modifyValidFailed,
        modifyNotSupport,
        modifyNoPower,
    };
    
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    pRspPdu->packetBody();    
    pRspPdu->m_pIpHdr->dvrip_p[8] = pReqPDU->m_pIpHdr->dvrip_p[8];
    pRspPdu->m_pIpHdr->dvrip_cmd = REQ_CONF_UPDAT;

    pRspPdu->m_pIpHdr->dvrip_p[0] = mofifySuccess;

    
    pRespPDU = pRspPdu;    
    int iRet = 0;
    switch(pReqPDU->m_pIpHdr->dvrip_p[8])
    {
        case CFG_GENERAL:
        {
			_printd("SetConfig CFG_GENERAL");
            iRet = ModGeneralCfg(pReqPDU);
            break;
        }
        case CFG_COMM:
        {_printd("SetConfig CFG_COMM");
            iRet = ModCommCfg(pReqPDU);
            break;
        }
        case CFG_NET:
        {_printd("SetConfig CFG_NET");
            iRet = ModNetCfg(pReqPDU);
            break;
        }
        case CFG_TITLE:
        {_printd("SetConfig CFG_TITLE");
            iRet = ModTitleCfg(pReqPDU);
            break;
        }
        case CFG_MAIL:
        {_printd("SetConfig CFG_MAIL");
            iRet = ModMailCfg(pReqPDU);
            break;
        }
        case CFG_DISPLAY:
        {_printd("SetConfig CFG_DISPLAY");
            iRet = ModDisplayCfg(pReqPDU);
            break;
        }
        case CFG_AUTOMAINTAIN:
        {_printd("SetConfig CFG_AUTOMAINTAIN");
            iRet = ModAutoMaintain(pReqPDU);
            break;
        }
        case CFG_PTZ:
        {_printd("SetConfig CFG_PTZ");
            iRet = ModPTZCfg(pReqPDU);
            break;
        }
        case CFG_EXCAPTURE:
        {_printd("SetConfig CFG_EXCAPTURE");
            iRet = ModExCapture(pReqPDU);
            break;
        }
        case CFG_PPPOE:
        {_printd("SetConfig CFG_PPPOE");
            iRet = ModPPPOECfg(pReqPDU);
            break;
        }
		case CFG_NET_IPFILTER:
		{_printd("SetConfig CFG_NET_IPFILTER");
			iRet = ModIPFilterCfg(pReqPDU);
			break;
		}
        case CFG_DDNS:
        {_printd("SetConfig CFG_DDNS");
            iRet = ModDDNSCfg(pReqPDU);

            break;
        }
        case CFG_VSP:
        {_printd("SetConfig CFG_VSP");
            iRet = ModVSPCfg(pReqPDU);
            break;
        }
        case CFG_DSPBITRATE:
        {_printd("SetConfig CFG_DSPBITRATE");
            iRet = ModDspBitRate(pReqPDU);
            break;
        }
        case CFG_NET_UPNP:
        case CFG_FTP:
            break;

		case CFG_NET_NTP:
        {_printd("SetConfig CFG_NET_NTP");
            iRet =  ModNtpCfg(pReqPDU);
            break;
        }
        case CFG_REG_SERVER:
        {_printd("SetConfig CFG_REG_SERVER");
            iRet =  ModRegServer(pReqPDU);
            break;
        }
        case CFG_DHCP:
        {_printd("SetConfig CFG_DHCP");
            iRet = ModDHCPCfg(pReqPDU);
            break;
        }        
        case CFG_DNS:
        {_printd("SetConfig CFG_DNS");
            iRet = ModDNSCfg(pReqPDU);
            break;
        }
        case CFG_TRANSFER_POLICY:
        {_printd("SetConfig CFG_TRANSFER_POLICY");
            iRet = ModTransferPolicy(pReqPDU);
            break;
        }        
        case CFG_RECDDOWNLOAD:
        {_printd("SetConfig CFG_RECDDOWNLOAD");
            iRet = ModHighDownload(pReqPDU);
            break;
        }
        case CFG_NEWRECORD:
        {_printd("SetConfig CFG_NEWRECORD");
            iRet = ModRecordCfg(pReqPDU);
            break;
        }
        case CFG_CURRENTCAPTURE:
        {_printd("SetConfig CFG_CURRENTCAPTURE");
            iRet = ModEncodeCfg(pReqPDU);
            break;
        }
        
        case CFG_COLOR:
        {_printd("SetConfig CFG_COLOR");
            iRet = ModColorCfg(pReqPDU);
            break;
        }
        case CFG_EVENT:
        {_printd("SetConfig CFG_EVENT");
            iRet = ModEventCfg(pReqPDU);
            break;
        }
        case CFG_WORKSHEET:
        {_printd("SetConfig CFG_WORKSHEET");
            iRet = ModWorkSheet(pReqPDU);
            break;
        }

        case CFG_VIDEOCOVER:
        {_printd("SetConfig CFG_VIDEOCOVER");
            iRet = ModVideCover(pReqPDU);
            break;
        }
        case CFG_DECODE:
        {_printd("SetConfig CFG_DECODE");
            iRet = ModDecode(pReqPDU);
            break;
        }
#if 0
        case CFG_MATRIX:
        {
            iRet = ModVideoMatrix(pReqPDU);
            break;
        }
    
        case CFG_AUDIOFORMAT:
        {
            iRet = ModAudioFormat(pReqPDU);
            break;
        }
#endif    
		case CFG_GOOLINK:
		{
			iRet = ModGooLink(pReqPDU);
			break;
		}
        default:
        {
            tracef("cmd[%#x]  modifyNotSupport \n", pReqPDU->m_pIpHdr->dvrip_p[8]);
            pRspPdu->m_pIpHdr->dvrip_p[0] = modifyNotSupport;
        }

    }

    if (iRet & CONFIG_APPLY_VALIT_ERROR)
    {
        pRspPdu->m_pIpHdr->dvrip_p[0] = modifyValidFailed;
    }
    else if (iRet & (CONFIG_APPLY_FILE_ERROR | CONFIG_APPLY_NOT_EXSIST))
    {
        pRspPdu->m_pIpHdr->dvrip_p[0] = modifySaveFailed;
    }
    else if (iRet & (CONFIG_APPLY_REBOOT | CONFIG_APPLY_RESTART))
    {
        //置重启标志
        pRspPdu->m_pIpHdr->dvrip_p[1] = 1;
    }
    
    return iRet;
}

int CVNConfig::ModGeneralCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;

    if (sizeof(CONFIG_GENERAL_OLD) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        /* add by Ilena 用于增加语言和视频制式变更功能 2009-09-27  */
        CONFIG_GENERAL_OLD *pConfig = (CONFIG_GENERAL_OLD *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
        
        CConfigLocation* pCfgLocation = new CConfigLocation();
        pCfgLocation->update();
        CONFIG_LOCATION &cfgLocation = pCfgLocation->getConfig();
        
        int old_Language = cfgLocation.iLanguage;    
        int old_VideoFormat = cfgLocation.iVideoFormat;    
    
        //转换数据
        CConfigGeneral* pCfgGeneral = new CConfigGeneral();
        pCfgGeneral->update();
        CONFIG_GENERAL &cfgGeneral = pCfgGeneral->getConfig();
        
        cfgGeneral.iLocalNo = pConfig->LocalNo;
        cfgGeneral.iOverWrite = pConfig->OverWrite;
        cfgGeneral.iAutoLogout = pConfig->StandbyTime;            
        //XXXX Fix
        // cfgGeneral.iPacketLength = pConfig->RecLen;            
#ifndef VN_IPC
        cfgLocation.iDSTRule = (int)AppConfig::instance()->getNumber("Global.DaylightSavingTime", DST_OFF);
#else
        cfgLocation.iDSTRule = (int)pConfig->DST;
#endif
        cfgLocation.iDateFormat = pConfig->DateFmt;
        cfgLocation.iDateSeparator =pConfig->DateSprtr;
        cfgLocation.iLanguage = pConfig->Language;
        cfgLocation.iTimeFormat = pConfig->TimeFmt;
        cfgLocation.iVideoFormat = pConfig->VideoFmt;            

        iRet |= pCfgGeneral->commit() | pCfgLocation->commit();

        if((old_VideoFormat != cfgLocation.iVideoFormat) || (old_Language != cfgLocation.iLanguage))
        {
            bool bNeedReboot = false;

            if(old_VideoFormat != cfgLocation.iVideoFormat)
            {
                //针对720P球机做特殊处理，设置制式成功才重启
                if (0 == SystemSetVideoMode((video_standard_t)(cfgLocation.iVideoFormat)))
                {
                     bNeedReboot = true;
                }
            }
            
            if(old_Language != cfgLocation.iLanguage)
            {
                FILE *fp = fopen(CONFIG_DIR"/lang", "w+b");        /*更改通道名称以适应语言变化*/
                if(fp != NULL)
                {
                    int lang = 1;
                    fseek(fp, 0, SEEK_SET);
                    fwrite(&lang, sizeof(int), 1, fp);
                    fclose(fp);

                    /* 连续写两次eeprom还是有问题,这里还是要稍微sleep一下 */
                    if (old_VideoFormat != cfgLocation.iVideoFormat)
                    {
                        SystemSleep(10);
                    }

                    SystemSetUILanguage((ui_language_t)(cfgLocation.iLanguage));
                }
                bNeedReboot = true;
            }
			
            if (bNeedReboot == true)
            {
                g_Challenger.Reboot();
            }
        }
        delete pCfgGeneral;
        delete pCfgLocation;
        /* add by Ilena __End */
    }
    return 0;
}

int CVNConfig::ModCommCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;

    if (sizeof(CONFIG_COMM) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        CONFIG_COMM* pConfig = (CONFIG_COMM *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);

        CConfigComm* pCfgCom = new CConfigComm();
        pCfgCom->update();

        CONFIG_COMM_X& cfgCom = pCfgCom->getConfig();

        cfgCom.iPortNo            = 1;
        cfgCom.iProtocolName    = (int)(pConfig->Function);
        cfgCom.iCommAttri.iParity    = (int)(pConfig->Parity);
        //对停止位做处理
        if(pConfig->StopBits == 0)
        {
            cfgCom.iCommAttri.iStopBits = 1;
        }
        else
        {
            cfgCom.iCommAttri.iStopBits = 2;
        }
        cfgCom.iCommAttri.nBaudBase = (int)(pConfig->BaudBase);
        cfgCom.iCommAttri.nDataBits = (int)(pConfig->DataBits);
        
        iRet = pCfgCom->commit();

        delete pCfgCom;

        
    }

    return iRet;
}

int CVNConfig::ModNetCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;
    if (sizeof(CONFIG_NET) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        CConfigConversion CCfgCVS;
        iRet |= CCfgCVS.SaveNetConfigOld((CONFIG_NET *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE), -1,2);

        if(CONFIG_APPLY_REBOOT & iRet)
        {
            g_Challenger.Reboot();        //open by ilena zhou
        }
    }
    else
    {
        return -1;
    }
    return iRet;
}

int CVNConfig::ModExCapture(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_VALIT_ERROR;
    if (sizeof(CONFIG_EX_CAPTURE_T) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        CONFIG_EX_CAPTURE_T *pExCap = (CONFIG_EX_CAPTURE_T *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
        ExCaptureSetQuality(0, pExCap->iQuality);
        iRet = CONFIG_APPLY_OK;
    }

    return iRet;
}

int CVNConfig::ModTitleCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;

    if ( (N_SYS_CH * NAME_LEN) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        CConfigChannelTitle* pTitle = new CConfigChannelTitle();
        if (pTitle)
        {
            pTitle->update();
            char* pTmp = (char *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
            for (int i = 0; i < N_SYS_CH; i++)
            {
                memcpy(pTitle->getConfig(i).strName, (pTmp + (i * NAME_LEN)), NAME_LEN);
            }
            iRet = pTitle->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
            delete pTitle;
        }
    }
    return iRet;
}

int CVNConfig::ModMailCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;

#ifdef _USE_SMTP_MODULE

    CConfigNetEmail ConfigNetserver ;
    ConfigNetserver.update();
    CONFIG_NET_EMAIL &cfgold = ConfigNetserver.getConfig();
    TIMESECTION section1 = cfgold.Schedule[0];
    TIMESECTION section2 = cfgold.Schedule[1];
    memset(&cfgold, 0, sizeof(CONFIG_NET_EMAIL));
    
    g_SmtpClient.ParseString((char *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE),
                             pReqPDU->m_pIpHdr->dvrip_extlen, &ConfigNetserver.getConfig());
    cfgold.Schedule[0] = section1;
    cfgold.Schedule[1] = section2;
    iRet = ConfigNetserver.commit();

#endif

    return iRet;
}

int CVNConfig::ModDisplayCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;

    CConfigConversion CCfgCVS; 

    if (sizeof(CONFIG_DISPLAY_OLD) == pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        iRet = CCfgCVS.SaveOldDisplay((CONFIG_DISPLAY_OLD *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE));
    }

    return iRet;
}

int CVNConfig::ModAutoMaintain(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;
    if (pReqPDU->m_pIpHdr->dvrip_extlen == sizeof(CONFIG_AUTO_OLD_CLIENT))
    {
        CONFIG_AUTO_OLD cfgOldAuto;
        memset(&cfgOldAuto, 0, sizeof(CONFIG_AUTO_OLD));
        CONFIG_AUTO_OLD_CLIENT* pCfg = (CONFIG_AUTO_OLD_CLIENT *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
            
        cfgOldAuto.AutoDeleteFilesTime = (int)pCfg->AutoDeleteFilesTime;
        cfgOldAuto.AutoRebootDay = (int)pCfg->AutoRebootDay;
        cfgOldAuto.AutoRebootTime = (int)pCfg->AutoRebootTime;
        CConfigConversion CCfgCVS; 
        iRet = CCfgCVS.SaveOldAutoMain(&cfgOldAuto);
    }

    return iRet;
}



int CVNConfig::ModPTZCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_OK;
    // 云台协议修改, 网络模块延用以前的协议
    CONFIG_PTZ_OLD            ConfigPtz[N_SYS_CH];            // 云台配置结构
    PTZ_OPT_ATTR        PtzAttr;
    CConfigConversion CCfgCVS; //此类无成员变量，占用栈很小
    memset(&ConfigPtz, 0, sizeof(ConfigPtz));
    CCfgCVS.LoadOldPtzConfig(ConfigPtz);
    if (pReqPDU->m_pIpHdr->dvrip_extlen == N_SYS_CH*sizeof(__NET_CONFIG_PTZ))// 更老的云台网络配置结构
    {
        for (int ii=0; ii<N_SYS_CH; ii++)
        {
            __NET_CONFIG_PTZ* pOldPtzCfg = (__NET_CONFIG_PTZ*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + ii*sizeof(__NET_CONFIG_PTZ));
            ConfigPtz[ii].DestAddr = pOldPtzCfg->DestAddr;
            memset(&PtzAttr, 0, sizeof(PtzAttr));
            if (pOldPtzCfg->Protocol != 0)
            {
                g_Ptz.GetProtocolAttr(&PtzAttr, pOldPtzCfg->Protocol - 1);
                strncpy(ConfigPtz[ii].Name, PtzAttr.Name, sizeof(ConfigPtz[ii].Name));
            }
            else
            {
                strcpy(ConfigPtz[ii].Name, "NONE");
            }
            
            if (0 != memcmp(&ConfigPtz[ii].PTZAttr, &pOldPtzCfg->PTZAttr, sizeof(PTZ_ATTR)))
            {
#ifndef KBD_ON_DEV_PTZ
          if ( (ii>=0 && ii <g_nCapture)
               || 1 == CConfigLocalDigiChCfg::getLatest(ii).BPTZLocal)
                {
                    CDevPtz::instance()->SetAttribute(&pOldPtzCfg->PTZAttr);
                }
#endif
            }
            
            memcpy(&ConfigPtz[ii].PTZAttr, &pOldPtzCfg->PTZAttr, sizeof(PTZ_ATTR));
        }
    }
    else if (pReqPDU->m_pIpHdr->dvrip_extlen == N_SYS_CH*sizeof(OLD_CONFIG_PTZ))
    {

        for (int ii=0; ii<N_SYS_CH; ii++)
        {
            OLD_CONFIG_PTZ* pOldPtzCfg = (OLD_CONFIG_PTZ*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + ii*sizeof(OLD_CONFIG_PTZ));
        
            ConfigPtz[ii].DestAddr = pOldPtzCfg->DestAddr;
            ConfigPtz[ii].MonitorAddr = pOldPtzCfg->MonitorAddr;
            memset(&PtzAttr, 0, sizeof(PtzAttr));
            if (pOldPtzCfg->Protocol != 0)
            {
                g_Ptz.GetProtocolAttr(&PtzAttr, pOldPtzCfg->Protocol - 1);
                strncpy(ConfigPtz[ii].Name, PtzAttr.Name, sizeof(ConfigPtz[ii].Name));
            }
            else
            {
                strcpy(ConfigPtz[ii].Name, "NONE");
            }
            
            if (0 != memcmp(&ConfigPtz[ii].PTZAttr, &pOldPtzCfg->PTZAttr, sizeof(PTZ_ATTR)))
            {
                //当设置改变时，设置新的属性到PTZ设备
                CDevPtz::instance()->SetAttribute(&pOldPtzCfg->PTZAttr);
            }

            memcpy(&ConfigPtz[ii].PTZAttr, &pOldPtzCfg->PTZAttr, sizeof(PTZ_ATTR));
        }
        //当web  端的目的地址,    波特率,  协议名称发生改变时向球机发送一串字节
    }
    else
    {
        return iRet;
    }

    iRet = CCfgCVS.SaveOldPtzConfig(ConfigPtz);

    return iRet;
}

int CVNConfig::ModPPPOECfg(CIVnPdu*& pReqPDU)
{
    int iRet = 0;

    NET_PPPOE_CONFIG pppoeCfg;
    memset(&pppoeCfg,0,sizeof(NET_PPPOE_CONFIG));
    pppoeCfg.enable = (~pReqPDU->m_pIpHdr->dvrip_p[12]) & 1; // 是否启用, 0为启用, 1为不启用
    std::string strData((char *)&pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen);

    std::string strUser;
    std::string strPwd;
    size_t pos = strData.find("&&");
    if ( pos != std::string::npos)
    {
        strUser.assign(strData.begin(), strData.begin()+pos);
        strPwd.assign(strData.begin()+pos+2, strData.end());
        strncpy(pppoeCfg.username, strUser.c_str(), sizeof(pppoeCfg.username)-1);
        strncpy(pppoeCfg.password, strPwd.c_str(), sizeof(pppoeCfg.password)-1);
    }
    
    NET_PPPOE_CONFIG OldpppoeCfg;
    memset(&OldpppoeCfg,0,sizeof(OldpppoeCfg));
    NetGetPPPoEConfig(&OldpppoeCfg);    //存取旧的配置, 以待比较

	int iRet_usr = 0;
	int iRet_pwd = 0;
	iRet_usr = strncmp(OldpppoeCfg.username, pppoeCfg.username, 64);
	iRet_pwd = strncmp(OldpppoeCfg.password, pppoeCfg.password, 64);
	if(OldpppoeCfg.enable && (!pppoeCfg.enable))
	{
		printf("pppoe stop...\r\n");
		system("setsid /usr/sbin/pppoe-stop &");//转移到后台 by ilena zhou 2010-04-08
		system("setsid /usr/sbin/pppoe-stop &");
	}
	else if((!OldpppoeCfg.enable) && pppoeCfg.enable)
	{
		printf("pppoe start...\r\n");
		system("setsid /usr/sbin/pppoe-start &");
	}
	else if((OldpppoeCfg.enable && pppoeCfg.enable) && ((iRet_usr != 0) || (iRet_pwd != 0)) )
    {//enable状态下，只是改变用户名或者密码 by ilena zhou 2010-04-08
		printf("\r\nCall Restart...\r\n");
		system("setsid /usr/sbin/pppoe-restart &");
	} 
	else
	{
		printf("whassat????\r\n");
	}

        
    if(pppoeCfg.enable != OldpppoeCfg.enable 
        || strcmp(pppoeCfg.username , OldpppoeCfg.username)
        || strcmp(pppoeCfg.password ,OldpppoeCfg.password))
    {

        if (NetSetPPPoEConfig(&pppoeCfg) != 0)
        {
            iRet |= CONFIG_APPLY_VALIT_ERROR;
        }
    }
    
    return iRet;
}

//added by wyf on 091229
int CVNConfig::ModIPFilterCfg(CIVnPdu*& pReqPDU)
{
	//接收更新后的IP权限设置信息，包括使能值、黑名单和白名单
	int i = 0;
	int j = 0;
	IPADDR TempIP;
	IPADDR *pTempIP = NULL;

	CConfigNetIPFilter *pConfigNetIPFilter = new CConfigNetIPFilter();
	pConfigNetIPFilter->update();
	CONFIG_NET_IPFILTER &cfgNetIPFilter = pConfigNetIPFilter->getConfig();

	//在更新IP权限配置之前，先将原来的黑白名单清空
	IPADDR *pDelBannedIP = NULL;
	IPADDR *pDelTrustIP = NULL;
	for(i = 0; i < MAX_FILTERIP_NUM; i++)
	{
		pDelBannedIP = (IPADDR *)&cfgNetIPFilter.BannedList[i];
		pDelTrustIP = (IPADDR *)&cfgNetIPFilter.TrustList[i];

		pDelBannedIP->l =0;
		pDelTrustIP->l =0;
	}

	int* iIpFilterEnable = (int*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE );
	cfgNetIPFilter.Enable = *iIpFilterEnable & 1;
	int *iBlackIPNums = (int*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + sizeof(int) );//黑名单数
	int *iTrustIPNums = (int*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + 2*sizeof(int) );//黑名单数
	int iIPNums = *iBlackIPNums + *iTrustIPNums;

	uint *IPList =(uint*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + 3*sizeof(uint) ) ;

	for(i = 0; i < *iBlackIPNums; i++)
	{
        uint uiIp = ntohl(IPList[i]);
		memcpy(&(TempIP.l), &uiIp, 4);
		for(j = 0; j < MAX_FILTERIP_NUM; j++)
		{
			pTempIP = (IPADDR *)&cfgNetIPFilter.BannedList[j];
			if(pTempIP->c[0] == 0 && pTempIP->c[1] == 0 && pTempIP->c[2] == 0 && pTempIP->c[3] == 0)
		    {
				pTempIP->c[0] = TempIP.c[0];
		        pTempIP->c[1] = TempIP.c[1];
		        pTempIP->c[2] = TempIP.c[2];
		        pTempIP->c[3] = TempIP.c[3];
		        break;
		    }
		}
	}

	for(i = *iBlackIPNums; i < iIPNums; i++)
	{
        uint uiIp = ntohl(IPList[i]);
		memcpy(&(TempIP.l), &uiIp, 4);

		for(j = 0; j < MAX_FILTERIP_NUM; j++)
		{
			pTempIP = (IPADDR *)&cfgNetIPFilter.TrustList[j];
		
			if(pTempIP->c[0] == 0 && pTempIP->c[1] == 0 && pTempIP->c[2] == 0 && pTempIP->c[3] == 0)
		    {
		        pTempIP->c[0] = TempIP.c[0];
		        pTempIP->c[1] = TempIP.c[1];
		        pTempIP->c[2] = TempIP.c[2];
		        pTempIP->c[3] = TempIP.c[3];
		        break;
		    }
		}
	}

	pConfigNetIPFilter->commit();
	delete pConfigNetIPFilter;

	return 0;
}
//end added by wyf on 091229

int CVNConfig::ModDDNSCfg(CIVnPdu*& pReqPDU)
{
//add by ilena zhou 2009-11-20
}


int CVNConfig::ModRegServer(CIVnPdu*& pReqPDU)
{
    tracef("not support ModRegServer\n");
    return -1;
}

int CVNConfig::ModDHCPCfg(CIVnPdu*& pReqPDU)
{
#ifdef _NET_USE_DHCPCLIENT
    int iRet  = 0;
    tracef("ModDHCPCfg\n");
    std::string strData((char *)&pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen);
    
    g_DhcpClient.SaveDhcpStrNet(strData.c_str());

    CConfigDHCP configDHCP ;
    configDHCP.update();

    std::string strCfg;
    size_t pos = strData.find("&&");
    if ( pos != std::string::npos)
    {
        strCfg.assign(strData.begin(), strData.begin()+pos);
        strData.erase(strData.begin(), strData.begin()+pos+2);
    }
    else
    {
        strCfg = strData;
        strData.clear();
    }

    if(strCfg.size() > 0)
    {
        pos = strCfg.find("::");
        if ( pos != std::string::npos)
        {
            CONFIG_DHCP_CFG &dhcpcfg = configDHCP.getConfig(0);
            std::string strTmp;
            strTmp.assign(strCfg.begin(), strCfg.begin()+pos);
            strcpy(dhcpcfg.ifName,(char *)strTmp.c_str());
    
            strCfg.erase(strCfg.begin(), strCfg.begin()+pos+2);
            dhcpcfg.enable = atoi((char *)strCfg.c_str());

            tracef("dhcp:%d,%s\n",dhcpcfg.enable,dhcpcfg.ifName);            
        }        
    }

    if(strData.size() > 0)
    {
        pos = strData.find("::");
        if ( pos != std::string::npos)
        {
            CONFIG_DHCP_CFG &dhcpcfg = configDHCP.getConfig(1);
            std::string strTmp;
            strTmp.assign(strData.begin(), strData.begin()+pos);
            strcpy(dhcpcfg.ifName,(char *)strTmp.c_str());
                
            strData.erase(strData.begin(), strData.begin()+pos+2);
            dhcpcfg.enable = atoi((char *)strData.c_str());

            tracef("dhcp1:%d,%s\n",dhcpcfg.enable,dhcpcfg.ifName);            
        }
    }

    iRet = configDHCP.commit();
    return iRet;
#else
    return 0;
#endif
}

int CVNConfig::ModNtpCfg(CIVnPdu*& pReqPDU)
{
#if defined(USING_PROTOCOL_NTP)
    char Buf[256] = {0};

    if(pReqPDU->m_pIpHdr->dvrip_extlen > sizeof(Buf) )
    {
        tracef("ModNtpCfg Len error\r\n");
        return -1;
    }
    memcpy(Buf, &pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen);
    if( g_NtpClient.SaveNtpCfg(Buf) < 0)
    {
        return -1;
    }
#endif
    return 0;
}

int CVNConfig::ModDNSCfg(CIVnPdu*& pReqPDU)
{
#ifdef _DNS_FUNCTION
    int ret = 0;
    tracef("OnModDNSCfg\n");
    std::string strData((char *)&pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen);

    std::string strPrimaryIp;
    std::string strSecondryIp;
    size_t pos = strData.find("::");
    if ( pos != std::string::npos)
    {
        strPrimaryIp.assign(strData.begin(), strData.begin()+pos);
        strSecondryIp.assign(strData.begin()+pos+2, strData.end());        

        pos = strPrimaryIp.find("=");
        if ( pos != std::string::npos)
        {
            strPrimaryIp.erase(strPrimaryIp.begin(), strPrimaryIp.begin()+pos+1);
        }
        pos = strSecondryIp.find("=");
        if ( pos != std::string::npos)
        {
            strSecondryIp.erase(strSecondryIp.begin(), strSecondryIp.begin()+pos+1);
        }
        tracef("[Print by vfware] PrimaryIpValue = %s,SecondaryIpValue = %s\n",strPrimaryIp.c_str(),strSecondryIp.c_str());
        
        ret = NetSetDNSAddress(strPrimaryIp.c_str(),strSecondryIp.c_str());
        if( ret < 0)
        {
            tracef("NetSetDNSAddress failed\n");
            return -1;
        }

        //修改DNS后系统重启 by ilena zhou 2010-01-06
        return CONFIG_APPLY_REBOOT;
                
    }
    return 0;
#else

    tracef("CFG_DNS no support\n");
    return -1;
#endif
}

int CVNConfig::ModHighDownload(CIVnPdu*& pReqPDU)
{
    trace("int CmdsDefaultOperation::CFG_RECDDOWNLOAD(CS32 iThrdIdx)\n");
    std::string strData((char *)&pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen%32);

    CConfigNetCommon cCfgNetComm;
    cCfgNetComm.update();
    cCfgNetComm.getConfig().bUseHSDownLoad = atoi(strData.c_str());

    tracef("cCfgNetComm.getConfig().bUseHSDownLoad :%d\n",cCfgNetComm.getConfig().bUseHSDownLoad );
    int iRet = cCfgNetComm.commit();

    tracef("iRet:%d\n",iRet);
    return 0;
}

int CVNConfig::ModTransferPolicy(CIVnPdu*& pReqPDU)
{
    tracef("int CVNConfig::ModTransferPolicy\n");

    std::string strData((char *)&pReqPDU->m_pIpHdr->dvrip_p[24], pReqPDU->m_pIpHdr->dvrip_extlen);
    std::string strEn;
    std::string strPolicy;
    size_t pos = strData.find("::");
    if ( pos != std::string::npos)
    {
        strEn.assign(strData.begin(), strData.begin()+pos);
        strPolicy.assign(strData.begin()+pos+2, strData.end());    
    }

    CConfigNetCommon *pNetTransfer = new CConfigNetCommon;
    pNetTransfer->update();
    CONFIG_NET_COMMON& cfg = pNetTransfer->getConfig();

    tracef("cfg.MaxConn:%d\n", cfg.MaxConn);
    if(strEn == "1")
    {
        cfg.bUseTransferPlan = true;
    }
    else
    {
        cfg.bUseTransferPlan = false;
    }
    
    cfg.TransferPlan = 2;
    if(strPolicy == "ImgQlty-First")
    {
        cfg.TransferPlan = 0;
    }
    else if(strPolicy == "Fluency-First")
    {
        cfg.TransferPlan = 1;
    }

    tracef("cfg.MaxConn:%d\n", cfg.MaxConn);
    int iRet =  pNetTransfer->commit();
    tracef("iRet:%d\n",iRet);
    delete pNetTransfer;
    return iRet;
    
}
int CVNConfig::ModVSPCfg(CIVnPdu*& pReqPDU)
{
    return 0;
}

int CVNConfig::ModDspBitRate(CIVnPdu*& pReqPDU)
{
    return 0;
}

int CVNConfig::ModRecordCfg(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_VALIT_ERROR;

    if (pReqPDU->m_pIpHdr->dvrip_extlen == N_SYS_CH * sizeof(CONFIG_RECORD))
    {
        CConfigRecord *pCfgRecord = new CConfigRecord();
        if (pCfgRecord)
        {
            pCfgRecord->update();

            //此处把抓图配置给覆盖掉了，需要先保存起来
            VD_BOOL iSnapShot[N_SYS_CH]= {0};
            for (int i  = 0; i < N_SYS_CH; i++)
            {
                iSnapShot[i] = pCfgRecord->getConfig(i).bSnapShot;
            }

            memcpy(&pCfgRecord->getConfig(), (CONFIG_RECORD *)(&pReqPDU->m_pIpHdr->dvrip_p[24]),
                   N_SYS_CH * sizeof(CONFIG_RECORD));

            for (int i = 0; i < N_SYS_CH; i++)
            {
                pCfgRecord->getConfig(i).bSnapShot = iSnapShot[i];
            }

            iRet = pCfgRecord->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
            delete pCfgRecord;
        }
    }

    return iRet;
}


int CVNConfig::ModEncodeCfg(CIVnPdu*& pReqPDU)
{
    int iRet = 0;

    if (pReqPDU->m_pIpHdr->dvrip_extlen == N_SYS_CH * sizeof(CONFIG_CAPTURE_OLD))
    {
        CConfigConversion CCfgCVS; 
        CONFIG_CAPTURE_OLD* pCaptureCfg = (CONFIG_CAPTURE_OLD*)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE);
        iRet = CCfgCVS.SaveOldEncode(pCaptureCfg);
    }
    return iRet;
}

int CVNConfig::ModColorCfg(CIVnPdu*& pReqPDU)
{
    int iRet = 0;

    if(pReqPDU->m_pIpHdr->dvrip_extlen == N_SYS_CH * sizeof(CONFIG_COLOR_OLD))
    {
        CConfigConversion CCfgCVS; 
        CONFIG_COLOR_OLD* pTmpCfg = (CONFIG_COLOR_OLD*)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE);
        CONFIG_COLOR_OLD* pColorCfg = pTmpCfg;
        
        for (int i = 0; i < g_nCapture; i++)
        {
            for (int j = 0; j < N_COLOR_SECTION; j++)
            {
                //网络上搞反了，纠正一下
                if (pTmpCfg->Color[j].Gain & BITMSK(7))
                {
                    pTmpCfg->Color[j].Gain = pTmpCfg->Color[j].Gain & 0x7f;
                }
                else
                {
                    pTmpCfg->Color[j].Gain |= BITMSK(7);
                }
            }
            pTmpCfg++;
        }

        iRet = CCfgCVS.SaveOldColor(pColorCfg);

    }

    return iRet;
}

int CVNConfig::ModEventCfg(CIVnPdu*& pReqPDU)
{
    int iRet = 0;

    switch(pReqPDU->m_pIpHdr->dvrip_p[16])
    {
        case EVENT_ALARM:
        if (sizeof(CONFIG_ALARM) * N_ALM_IN == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigAlarm* pAlarm = new CConfigAlarm();
            if (pAlarm)
            {
                pAlarm->update();
                memcpy(&pAlarm->getConfig(), 
                    (CONFIG_ALARM *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_ALARM) * N_ALM_IN);
                //保存配置
                iRet = pAlarm->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pAlarm;
            }
        }
        break;

        case EVENT_NETALARM:
        if (sizeof(CONFIG_ALARM) * N_ALM_IN == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigNetAlarm* pNetAlarm = new CConfigNetAlarm();
            if (pNetAlarm)
            {
                pNetAlarm->update();
                memcpy(&pNetAlarm->getConfig(), 
                    (CONFIG_ALARM *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_ALARM) * N_ALM_IN);
                //保存配置
                iRet = pNetAlarm->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pNetAlarm;
            }
        }
        break;
        case EVENT_MOTION:
        if (sizeof(CONFIG_MOTIONDETECT) * N_SYS_CH == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigMotionDetect* pMotion = new CConfigMotionDetect();
            if (pMotion)
            {
                pMotion->update();
                memcpy(&pMotion->getConfig(), 
                    (CONFIG_MOTIONDETECT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_MOTIONDETECT) * N_SYS_CH);
                //保存配置
                iRet = pMotion->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pMotion;
            }
        }
        break;
        case EVENT_LOSS:
        if (sizeof(CONFIG_GENERIC_EVENT) * N_SYS_CH == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigLossDetect* pLoss = new CConfigLossDetect();
            if (pLoss)
            {
                pLoss->update();
                memcpy(&pLoss->getConfig(), 
                    (CONFIG_GENERIC_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_GENERIC_EVENT) * N_SYS_CH);
                //保存配置
                iRet = pLoss->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pLoss;
            }
        }
        break;
        case EVENT_BLIND:
        if (sizeof(CONFIG_BLINDDETECT) * N_SYS_CH == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigBlindDetect* pBlind = new CConfigBlindDetect();
            if (pBlind)
            {
                pBlind->update();
                memcpy(&pBlind->getConfig(), 
                    (CONFIG_BLINDDETECT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_BLINDDETECT) * N_SYS_CH);
                //保存配置
                iRet = pBlind->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pBlind;
            }
        }
        break;
        case EVENT_NOHDD:
        if (sizeof(CONFIG_GENERIC_EVENT) == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigStorageNotExist *pStNotExist = new CConfigStorageNotExist();
            if (pStNotExist)
            {
                pStNotExist->update();
                memcpy(&pStNotExist->getConfig(), 
                    (CONFIG_GENERIC_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE),
                    sizeof(CONFIG_GENERIC_EVENT));

                iRet = pStNotExist->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pStNotExist;
            }
        }
        break;
        case EVENT_ERRHDD:
        if (sizeof(CONFIG_GENERIC_EVENT) == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigStorageFailure *pStFailure = new CConfigStorageFailure();
            if (pStFailure)
            {
                pStFailure->update();
                memcpy(&pStFailure->getConfig(), 
                    (CONFIG_GENERIC_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE),
                    sizeof(CONFIG_GENERIC_EVENT));

                iRet = pStFailure->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pStFailure;
            }
        }
        break;
        case EVENT_NOSPACEHDD:
        if (sizeof(CONFIG_STORAGE_SPACE_EVENT) == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigStorageLowSpace *pStNoSpace = new CConfigStorageLowSpace();
            if (pStNoSpace)
            {
                pStNoSpace->update();
                memcpy(&pStNoSpace->getConfig(),
                    (CONFIG_STORAGE_SPACE_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_STORAGE_SPACE_EVENT));

                iRet = pStNoSpace->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pStNoSpace;
            }
        }
        break;
        case EVENT_NETABORT:
        if (sizeof(CONFIG_GENERIC_EVENT) == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            CConfigNetAbort* pNetAbort = new CConfigNetAbort();
            if (pNetAbort)
            {
                pNetAbort->update();
                memcpy(&pNetAbort->getConfig(), 
                    (CONFIG_GENERIC_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_GENERIC_EVENT));
                //保存配置
                iRet = pNetAbort->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pNetAbort;
            }
        }
	 case EVENT_IP_CONFLICT:
        if (sizeof(CONFIG_GENERIC_EVENT) == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
	     CConfigNetArp* pIPConflict = new CConfigNetArp();
            if (pIPConflict)
            {
                pIPConflict->update();
                memcpy(&pIPConflict->getConfig(), 
                    (CONFIG_GENERIC_EVENT *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), 
                    sizeof(CONFIG_GENERIC_EVENT));
                iRet = pIPConflict->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pIPConflict;
            }
        }
        break;
        default:
        break;
    }

    return iRet;
}

int CVNConfig::ModWorkSheet(CIVnPdu*& pReqPDU)
{
    int iRet = CONFIG_APPLY_VALIT_ERROR;

    if (sizeof(UI_WORKSHEET) != pReqPDU->m_pIpHdr->dvrip_extlen)
    {
        return iRet;
    }

    int iChannel = pReqPDU->m_pIpHdr->dvrip_p[17] - 1;
    if (iChannel >= g_nLogicNum)
    {
        return -1;
    }
    
    switch(pReqPDU->m_pIpHdr->dvrip_p[16])
    {
        case WORKSHEET_TYPE_RECORD:
        {
            CConfigRECWorksheet* pWorksheet = new CConfigRECWorksheet();
            if (pWorksheet)
            {
                pWorksheet->update();
                CONFIG_WORKSHEET& cfgWorksheet = pWorksheet->getConfig(iChannel);
                //memcpy(&cfgWorksheet, (CONFIG_WORKSHEET *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), sizeof(CONFIG_WORKSHEET));
                (void)SetConfigWSSchedule(& cfgWorksheet,(UI_WORKSHEET_SCHEDULE *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE + sizeof(cfgWorksheet.iName)));

                cfgWorksheet.iName = iChannel;

                CConfigRECState *configRecState = new CConfigRECState();
                configRecState->update();
                CONFIG_RECSTATE& cfgRecStat = configRecState->getConfig(iChannel);
                for(int i=0; i<N_WEEKS; i++)
                {
                    for(int j=0; j<N_UI_TSECT; j++)
                    {
                        int enable = cfgWorksheet.tsSchedule[i][j].enable;
                        cfgWorksheet.tsSchedule[i][j].enable = ((enable&BITMSK(2)) == 0)?0:1;
                        cfgRecStat.recstate[i][j] = (3&enable);
                    }
                }

                iRet = configRecState->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                iRet |= pWorksheet->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete configRecState;
                delete pWorksheet;

            }
            break;
        }
        case WORKSHEET_TYPE_ALARM:
        case WORKSHEET_TYPE_NETALARM:
        {
            CConfigALMWorksheet* pWorksheet = new CConfigALMWorksheet();
            if (pWorksheet)
            {
                pWorksheet->update();
                CONFIG_WORKSHEET& cfgWorksheet = pWorksheet->getConfig(iChannel);
                //memcpy(&cfgWorksheet, (CONFIG_WORKSHEET *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), sizeof(CONFIG_WORKSHEET));
                (void)SetConfigWSSchedule(& cfgWorksheet,(UI_WORKSHEET_SCHEDULE *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE + sizeof(cfgWorksheet.iName)));
                cfgWorksheet.iName = iChannel;
                iRet = pWorksheet->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pWorksheet;
            }
            break;
        }
        case WORKSHEET_TYPE_MOTION:
        {
            CConfigMTDWorksheet* pWorksheet = new CConfigMTDWorksheet();
            if (pWorksheet)
            {
                pWorksheet->update();
                CONFIG_WORKSHEET& cfgWorksheet = pWorksheet->getConfig(iChannel);
                //memcpy(&cfgWorksheet, (CONFIG_WORKSHEET *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), sizeof(CONFIG_WORKSHEET));
                (void)SetConfigWSSchedule(& cfgWorksheet,(UI_WORKSHEET_SCHEDULE *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE + sizeof(cfgWorksheet.iName)));
                cfgWorksheet.iName = iChannel;
                iRet = pWorksheet->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pWorksheet;
            }
            break;
        }
        case WORKSHEET_TYPE_BLIND:
        {
            CConfigBLDWorksheet* pBlindsheet = new CConfigBLDWorksheet();
            if (pBlindsheet)
            {
                pBlindsheet->update();
                CONFIG_WORKSHEET& cfgWorksheet = pBlindsheet->getConfig(iChannel);
                //memcpy(&cfgWorksheet, (CONFIG_WORKSHEET *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), sizeof(CONFIG_WORKSHEET));
                (void)SetConfigWSSchedule(& cfgWorksheet,(UI_WORKSHEET_SCHEDULE *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE + sizeof(cfgWorksheet.iName)));
                cfgWorksheet.iName = iChannel;
                iRet = pBlindsheet->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pBlindsheet;
            }
            break;
        }
        case WORKSHEET_TYPE_LOSS:
        {
            CConfigVLTWorksheet* pVLTsheet = new CConfigVLTWorksheet();
            if (pVLTsheet)
            {
                pVLTsheet->update();
                CONFIG_WORKSHEET& cfgWorksheet = pVLTsheet->getConfig(iChannel);
                //memcpy(&cfgWorksheet, (CONFIG_WORKSHEET *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE), sizeof(CONFIG_WORKSHEET));
                (void)SetConfigWSSchedule(& cfgWorksheet,(UI_WORKSHEET_SCHEDULE *)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE + sizeof(cfgWorksheet.iName)));
                cfgWorksheet.iName = iChannel;
                iRet = pVLTsheet->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE);
                delete pVLTsheet;
            }
            break;
        }
        case WORKSHEET_TYPE_NOHDD:
        case WORKSHEET_TYPE_ERRHDD:
        case WORKSHEET_TYPE_NOSPACEHDD:
        case WORKSHEET_TYPE_NETABORT:
        default:
        break;
    }

    return iRet;
}

int CVNConfig::ModVideCover(CIVnPdu*& pReqPDU)
{
    tracef("CVNConfig::ModVideCover\n");
    VIDEO_COVER_T * pConfigVCT;
    VIDEO_COVER_ATTR_T * pConfigVCAT;

    pConfigVCT = (  VIDEO_COVER_T*)( (char*)(pReqPDU->GetPacket()+ DVRIP_HEAD_T_SIZE) );
    pConfigVCAT =(VIDEO_COVER_ATTR_T*) (    (char*)pConfigVCT+ sizeof(VIDEO_COVER_T)     );

    CConfigVideoWidget* pccfgVW = new CConfigVideoWidget;
    pccfgVW->update();

    CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(pConfigVCT->iChannel-1);    

    CAPTURE_CAPS caps;
	GetCaps(&caps);
		
    cfgvw_t.iCoverNum = (caps.CoverBlocks < pConfigVCT->iCoverNum ? caps.CoverBlocks : pConfigVCT->iCoverNum);
    for(int i = 0; i < pConfigVCT->iCoverNum; i++)
    {
        cfgvw_t.dstCovers[i].rcRelativePos = pConfigVCAT[i].tBlock ;
        cfgvw_t.dstCovers[i].rgbaFrontground = pConfigVCAT[i].Color ;
        
        // 注意此处的两个使能值应该由网络上传过来的，不应该写死的。待
        // 网络协议更新后同步修改
        cfgvw_t.dstCovers[i].bEncodeBlend = pConfigVCAT[i].Encode;
        cfgvw_t.dstCovers[i].bPreviewBlend = pConfigVCAT[i].Priview;
    }
    
    pccfgVW->commit();
    delete pccfgVW;

    return 0;
}

int CVNConfig::ModVideoMatrix(CIVnPdu*& pReqPDU)
{
    tracef("CVNConfig::ModVideoMatrix\n");
    int iRet = CONFIG_APPLY_NOT_EXSIST;

    if(pReqPDU->m_pIpHdr->dvrip_p[10] != 1)
    {
        return iRet;
    }

    return iRet;
}

int CVNConfig::ModAudioFormat(CIVnPdu*& pReqPDU)
{
    int iRet  = 0;
    return iRet;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int CVNConfig::GetConfig(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }


    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;
    switch(pReqPDU->m_pIpHdr->dvrip_p[8])
    {
        case CFG_GENERAL:
        {_printd("GetConfig CFG_GENERAL");
            QueryGeneralCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_COMM:
        {_printd("GetConfig CFG_COMM");
            QueryCommCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_NET:
        {_printd("GetConfig CFG_NET");
            QueryNetCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_PTZ:
        {_printd("GetConfig CFG_PTZ");
            QueryPTZCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_DISPLAY:
        {_printd("GetConfig CFG_DISPLAY");
            QueryDisplay(pReqPDU, pRespPDU);
            break;
        }
        case CFG_TITLE:
        {_printd("GetConfig CFG_TITLE");
            QueryTitleCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_MAIL:
        {_printd("GetConfig CFG_MAIL");
            QueryMailCfg(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_EXCAPTURE:
        {_printd("GetConfig CFG_PPPOE");
            QueryExCapture(pReqPDU, pRespPDU);
            break;
        }
        case CFG_PPPOE:
        {
			_printd("GetConfig CFG_PPPOE");
            QueryPPPOE(pReqPDU, pRespPDU);
            break;
        }
        case CFG_NET_IPFILTER:
        {_printd("GetConfig CFG_NET_IPFILTER");
            QueryIPFilter(pReqPDU, pRespPDU);
            break;
        }
        case CFG_DDNS:
        {_printd("GetConfig CFG_DDNS");
            QueryDDNS(pReqPDU, pRespPDU);
            break;
        }
        case CFG_DSPINFO:
        {_printd("GetConfig CFG_DSPINFO");
            QueryDSPInfo(pReqPDU, pRespPDU);
            break;
        }
        case CFG_AUTOMAINTAIN:
        {_printd("GetConfig CFG_AUTOMAINTAIN");
            QueryAutoMaintain(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_NET_NTP:
        {_printd("GetConfig CFG_NET_NTP");
            QureyNtpCfg(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_NET_UPNP:
        {_printd("GetConfig CFG_NET_UPNP");
            QueryUpnpCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_MATRIX:
        {_printd("GetConfig CFG_MATRIX");
            QueryVideoMatrix(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_VIDEOCOVER:
        {_printd("GetConfig CFG_VIDEOCOVER");
            QueryVideoCover(pReqPDU, pRespPDU);
            break;
        }
        case CFG_DHCP:
        {_printd("GetConfig CFG_DHCP");
            QueryDHCPCfg(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_FTP:
        {_printd("GetConfig CFG_FTP");
            QueryFTPCfg(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_DNS:
        {_printd("GetConfig CFG_DNS");
            QueryDNSCfg(pReqPDU, pRespPDU);
            break;
        }
        case CFG_RECDDOWNLOAD:
        {_printd("GetConfig CFG_RECDDOWNLOAD");
            QueryHighDownload(pReqPDU, pRespPDU);
            break;
        }    
        case CFG_AUDIOFORMAT:
        {_printd("GetConfig CFG_AUDIOFORMAT");
            QueryAudioFormat(pReqPDU, pRespPDU);
            break;
        }            
        case CFG_NEWRECORD:
        {_printd("GetConfig CFG_NEWRECORD");
            QueryRecord(pReqPDU, pRespPDU);
            break;
        }
        case CFG_EVENT:
        {_printd("GetConfig CFG_EVENT");
            QueryEvent(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_WORKSHEET:
        {_printd("GetConfig CFG_WORKSHEET");
            QueryWorkSheet(pReqPDU, pRespPDU);
            break;
        }
        case CFG_COLOR:
        {_printd("GetConfig CFG_COLOR");
            QueryColor(pReqPDU, pRespPDU);
            break;
        }
        case CFG_CURRENTCAPTURE:
        {_printd("GetConfig CFG_CURRENTCAPTURE");
            QueryEncode(pReqPDU, pRespPDU);
            break;
        }
        case CFG_TRANSFER_POLICY:
        {_printd("GetConfig CFG_NET_IPFILTER");
            QueryTransferPolicy(pReqPDU, pRespPDU);
            break;
        }
        case CFG_VSP:
        {_printd("GetConfig CFG_VSP");
            QueryVspCfg(pReqPDU, pRespPDU);
            break;
        }        
        case CFG_REG_SERVER:
        {_printd("GetConfig CFG_REG_SERVER");
            QueryRegServer(pReqPDU, pRespPDU);
            break;
        }
        case CFG_DECODE:
        {_printd("GetConfig CFG_DECODE");
            QueryDecode(pReqPDU, pRespPDU);
            break;
        }
		case CFG_GOOLINK:
		{
			_printd ("GetConfig CFG_REG_GOOLINK");
			QueryGooLink(pReqPDU, pRespPDU);
			break;
		}
        default:
        {
			_printd("GetConfig default [%x]", pReqPDU->m_pIpHdr->dvrip_p[8]);
            pRspPdu->packetBody();
            break;
        }
    }
    
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_CONF_PARAM;
    pRspPdu->m_pIpHdr->dvrip_p[8] = pReqPDU->m_pIpHdr->dvrip_p[8];

    return 0;
}

int CVNConfig::QueryGeneralCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    int iLength = sizeof(CONFIG_GENERAL_OLD);
    CPacket *pPkt = g_PacketManager.GetPacket(iLength);
    pPkt->SetLength(iLength);

    CONFIG_GENERAL_OLD *pConfig = (CONFIG_GENERAL_OLD *)pPkt->GetBuffer();
    
    //转换数据
    CConfigGeneral* pCfgGeneral = new CConfigGeneral();
    CConfigLocation* pCfgLocation = new CConfigLocation();

    pCfgGeneral->update();
    pCfgLocation->update();

    CONFIG_GENERAL& cfgGeneral = pCfgGeneral->getConfig();
    CONFIG_LOCATION& cfgLocation = pCfgLocation->getConfig();
    
    pConfig->LocalNo = cfgGeneral.iLocalNo;
    pConfig->OverWrite = cfgGeneral.iOverWrite;
    //XXX
    //pConfig->RecLen = cfgGeneral.iPacketLength;
    
    pConfig->VideoFmt = cfgLocation.iVideoFormat;
    pConfig->Language = cfgLocation.iLanguage;
    pConfig->DateFmt = cfgLocation.iDateFormat;
    pConfig->DateSprtr = cfgLocation.iDateSeparator;
    pConfig->TimeFmt = cfgLocation.iTimeFormat;
    pConfig->DST = cfgLocation.iDSTRule;

    pConfig->StandbyTime = cfgGeneral.iAutoLogout;

    delete pCfgGeneral;
    delete pCfgLocation;

    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);
    
    pPkt->Release();

    return 0;
}

int CVNConfig::QueryCommCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryCommCfg");
    int iLength = sizeof(CONFIG_COMM);
    CPacket *pPkt = g_PacketManager.GetPacket(iLength);
    pPkt->SetLength(iLength);

    CONFIG_COMM*pConfig = (CONFIG_COMM *)pPkt->GetBuffer();
    CConfigComm* pCfgCom = new CConfigComm();
    pCfgCom->update();
    CONFIG_COMM_X& cfgCom = pCfgCom->getConfig();

    memset(pConfig->Com_Version, 0, sizeof(pConfig->Com_Version));
    pConfig->Function    = (uchar)(cfgCom.iProtocolName);
    pConfig->BaudBase    = (uint)(cfgCom.iCommAttri.nBaudBase);
    pConfig->Parity        = (uchar)(cfgCom.iCommAttri.iParity);
    if(cfgCom.iCommAttri.iStopBits == 1)
    {
        pConfig->StopBits = 0;
    }
    else
    {
        pConfig->StopBits = 2;
    }

    pConfig->DataBits    = (uchar)(cfgCom.iCommAttri.nDataBits);

    delete pCfgCom;

    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);
    pPkt->Release();

    return 0;
}

int CVNConfig::QueryNetCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    _printd("CVNConfig::QueryNetCfg");
//追加MAC地址 add by Ilena zhou
    char MacAddr[32];        
    char szMacAddr[20] = {0};
    memset(MacAddr, '\0', 32);
    //用来储存获取的原始MAC地址，形式如"ce:d5:aa:09:84:0f"
    NetGetMAC("eth0", MacAddr, sizeof(MacAddr));
    strncpy(szMacAddr,MacAddr,strlen(MacAddr));
// add_end 

    int iLength = sizeof(CONFIG_NET)+sizeof(szMacAddr);
    CPacket *pPkt = g_PacketManager.GetPacket(iLength);
    pPkt->SetLength(iLength);

    char* pTmp = (char *)pPkt->GetBuffer();
    CONFIG_NET* pCfgNet = (CONFIG_NET*)pTmp;
    memset(pCfgNet, 0, sizeof(CONFIG_NET));
    
    CConfigConversion CCfgCVS; 
    CCfgCVS.LoadNetConfigOld(pCfgNet);
    memcpy((char *)(pTmp+sizeof(CONFIG_NET)), szMacAddr, sizeof(szMacAddr));
    
    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);
    pPkt->Release();

    return 0;
}


int CVNConfig::QueryPTZCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryPTZCfg");
	int num = N_SYS_CH;
    CConfigConversion CCfgCVS; 
    CPacket *pPkt = g_PacketManager.GetPacket(num * sizeof(CONFIG_PTZ_OLD));
    CONFIG_PTZ_OLD *pCfg = (CONFIG_PTZ_OLD *)pPkt->GetBuffer();
    CCfgCVS.LoadOldPtzConfig(pCfg);

    // 云台协议修改, 网络模块延用以前的协议
    CPacket *pOldPkt = g_PacketManager.GetPacket(num * sizeof(OLD_CONFIG_PTZ));
    OLD_CONFIG_PTZ *pOldCfg = (OLD_CONFIG_PTZ *)pOldPkt->GetBuffer();

    memset(pOldCfg, 0, num * sizeof(OLD_CONFIG_PTZ));
    for (int ii=0; ii < num; ii++)
    {
        memcpy(&pOldCfg[ii].Ptz_Version, &pCfg[ii].Ptz_Version, sizeof(pOldCfg[ii].Ptz_Version));
        pOldCfg[ii].DestAddr = pCfg[ii].DestAddr;
        pOldCfg[ii].Protocol = g_Ptz.GetIndex(pCfg[ii].Name);
        pOldCfg[ii].MonitorAddr = pCfg[ii].MonitorAddr;
        memcpy(&pOldCfg[ii].PTZAttr, &pCfg[ii].PTZAttr, sizeof(PTZ_ATTR));
    }

    pRespPDU->packetBody((char *)pOldCfg, num * sizeof(OLD_CONFIG_PTZ));
    
    pOldPkt->Release();
    pPkt->Release();
    return 0;
}


int CVNConfig::QueryTitleCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryTitleCfg");
    char strName[NAME_LEN * N_SYS_CH]={0};

    for (int i=0; i<N_SYS_CH; i++)
    {
        memcpy((char *)&strName[i*NAME_LEN],(char*)CConfigChannelTitle::getLatest(i).strName, NAME_LEN);
    }
    pRespPDU->packetBody((char *)strName, N_SYS_CH * NAME_LEN);
    return 0;
}

int CVNConfig::QueryDisplay(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryDisplay");
    CConfigConversion CCfgCVS; 
    int iLength = sizeof(CONFIG_DISPLAY_OLD);
    CPacket *pPkt  = g_PacketManager.GetPacket(iLength);
    pPkt->SetLength(iLength);

    CCfgCVS.LoadOldDisplay((CONFIG_DISPLAY_OLD *)pPkt->GetBuffer());

    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);

    pPkt->Release();

    return 0;
}



int CVNConfig::QueryExCapture(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryExCapture");
    CONFIG_EX_CAPTURE_T excapCfg;
    excapCfg.iQuality = 0;
    pRespPDU->packetBody((char *)&excapCfg, sizeof(CONFIG_EX_CAPTURE_T));    
    return 0;
}

int CVNConfig::QueryMailCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryMailCfg");
#ifndef _USE_SMTP_MODULE
    pRespPDU->packetBody();
#else
    CConfigNetEmail mmConfigNetserver;
    mmConfigNetserver.update();

    CONFIG_NET_EMAIL &configNetserver = mmConfigNetserver.getConfig();
    
    CPacket *pPkt = g_PacketManager.GetPacket(1024);
    
    char* pstrMail = (char *)pPkt->GetBuffer();    /*!<邮件*/
    memset(pstrMail, 0, 1024);
    
    int iLen = g_SmtpClient.ToString(pstrMail, 1024 - 1,&configNetserver);

    tracef("SMTP Config:%s\n", pstrMail);
    pRespPDU->packetBody((char *)pstrMail, iLen + 1);
    pPkt->Release();

#endif

    return 0;
}

int CVNConfig::QueryPPPOE(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryPPPOE");
    NET_PPPOE_CONFIG pppoeCfg;
    memset(&pppoeCfg, 0, sizeof(pppoeCfg));
    NetGetPPPoEConfig(&pppoeCfg);
    std::string strPPP = pppoeCfg.username;
    strPPP += "&&";
    strPPP += pppoeCfg.password;

    char szIp[16] = { 0 };
    NetGetDialIpAddress(szIp);
    strPPP += "&&";
    strPPP += szIp;
    strPPP += "&&";	//和VS100统一，增加结束符 by ilena zhou 2010-05-12

    pRespPDU->packetBody((char *)strPPP.c_str(), strPPP.length());

    pRespPDU->m_pIpHdr->dvrip_p[12] = (~pppoeCfg.enable) & 1;

    return 0;
}

//added by wyf on 091229
int CVNConfig::QueryIPFilter(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	//将IP权限设置的使能值、黑名单和白名单发送给SDK
	_printd("CVNConfig::QueryIPFilter");
	CConfigNetIPFilter ConfigNetIPFilter;
	ConfigNetIPFilter.update();
	CONFIG_NET_IPFILTER &cfgNetIPFilter = ConfigNetIPFilter.getConfig();

	uint bufdata[3 + MAX_FILTERIP_NUM * 2] = {0};
		
	int iQIPListIndex = 0;
	int iQBannedIPNums = 0;
	int iQTrustIPNums = 0;
	int j ;	
	IPADDR *pTempIP = NULL;
	for(j = 0; j < MAX_FILTERIP_NUM; j++)
	{
		pTempIP = (IPADDR *)&cfgNetIPFilter.BannedList[j];
		if(pTempIP->l != 0 )
        {
            uint uiIp = htonl(pTempIP->l);
        	memcpy(&bufdata[iQIPListIndex+3],&uiIp,4);
			iQBannedIPNums++;
			iQIPListIndex++;
        }
	}

	for(j = 0; j < MAX_FILTERIP_NUM; j++)
	{
		pTempIP = (IPADDR *)&cfgNetIPFilter.TrustList[j];
		if(pTempIP->l != 0 )
        {
            uint uiIp = htonl(pTempIP->l);
        	memcpy(&bufdata[iQIPListIndex+3],&uiIp,4);
			iQTrustIPNums++;
			iQIPListIndex++;
        }
	}

	bufdata[0] = cfgNetIPFilter.Enable;
	bufdata[1] = iQBannedIPNums;
	bufdata[2] = iQTrustIPNums;

	pRespPDU->packetBody((char *)bufdata, sizeof(int)*(3+iQIPListIndex));    

	return 0;	
}
//end added by wyf on 091229

int CVNConfig::QueryDDNS(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryDDNS");
    pRespPDU->packetBody();
    return 0;

}
int CVNConfig::QueryVspCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    trace("CFG_VSP\n");
	_printd("CVNConfig::QueryVspCfg");
    switch (pReqPDU->m_pIpHdr->dvrip_p[9])
    {
        case 15:
        {
            break;
        }
        default:
        break;
    }
    return 0;
}
int CVNConfig::QueryUpnpCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryUpnpCfg");
    char strUpnp[32]={0};
    CONFIG_NET_UPNP* pUpnp = (CONFIG_NET_UPNP*)strUpnp;
    CConfigNetUPNP m_getConfigUpnp;
     m_getConfigUpnp.update();
    CONFIG_NET_UPNP &cfgnetupnp = m_getConfigUpnp.getConfig();
    memcpy(pUpnp, &cfgnetupnp, sizeof(CONFIG_NET_UPNP));
    pRespPDU->packetBody((char *)strUpnp, sizeof(CONFIG_NET_UPNP) + 4);    

    return 0;
}

int CVNConfig::QueryFTPCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	_printd("CVNConfig::QueryFTPCfg");
#if defined(FUNC_FTP_UPLOAD)

    CONFIG_FTP_PROTO_SET cfgFTP;
    memset(&cfgFTP, 0, sizeof(CONFIG_FTP_PROTO_SET));

    CConfigFTPServer *pCfgFTPServer = new CConfigFTPServer();
    CConfigFTPApplication *pCfgFTPApp = new CConfigFTPApplication();

    pCfgFTPServer->update(0);
    pCfgFTPApp->update();
        
    CONFIG_FTP_SERVER& cfgFtp= pCfgFTPServer->getConfig(0);
    cfgFTP.m_isEnable = cfgFtp.Enable;
    cfgFTP.m_unHostIP.l=cfgFtp.Server.ip.l;
    strcpy(cfgFTP.m_cUserName,cfgFtp.Server.UserName );
    strcpy(cfgFTP.m_cPassword,cfgFtp.Server.Password );
    cfgFTP.m_nHostPort =cfgFtp.Server.Port ;
    strcpy(cfgFTP.m_cDirName, cfgFtp.RemotePathName);
    cfgFTP.m_iFileLen = cfgFtp.FileMaxLen;


    for(int i= 0; i < N_SYS_CH; i ++)
    {
        CONFIG_FTP_APPLICATION& cfgFtpApp = pCfgFTPApp->getConfig(i);
        for(int j = 0; j < N_WEEKS; j ++)
        {
            for(int k = 0; k < N_MIN_TSECT; k++)
            {
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.StartHour = cfgFtpApp.RecordPeriodSet[j][k].Tsect.startHour;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.StartMin = cfgFtpApp.RecordPeriodSet[j][k].Tsect.startMinute;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.StartSec = cfgFtpApp.RecordPeriodSet[j][k].Tsect.startSecond;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.EndHour = cfgFtpApp.RecordPeriodSet[j][k].Tsect.endHour;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.EndMin = cfgFtpApp.RecordPeriodSet[j][k].Tsect.endMinute;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_tSect.EndSec = cfgFtpApp.RecordPeriodSet[j][k].Tsect.endSecond;
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_GeneralEn = cfgFtpApp.RecordPeriodSet[j][k].StateMask.En[EnGeneral];
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_AlarmEn = cfgFtpApp.RecordPeriodSet[j][k].StateMask.En[EnAlarm];
                cfgFTP.m_stMdAlarmSet[i][j].m_Period[k].m_MdEn = cfgFtpApp.RecordPeriodSet[j][k].StateMask.En[EnMotion] ;
            }
        }
    }

    delete pCfgFTPServer;
    delete pCfgFTPApp;

    pRespPDU->packetBody((char *)&cfgFTP, sizeof(CONFIG_FTP_PROTO_SET) );    
#else
    pRespPDU->packetBody();
#endif

    return 0;
}


int CVNConfig::QureyNtpCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
#if defined(USING_PROTOCOL_NTP)
    char strBuf[256];
    int ret;
    ret = g_NtpClient.LoadNtpCfg(strBuf, sizeof(strBuf) );
    if( ret < 0)
    {
        tracef("g_NtpClient.LoadNtpCfg Failed!\n");
        return -1;
    }
    pRespPDU->packetBody((char *)strBuf, strlen(strBuf)  );    

#else
    pRespPDU->packetBody();
#endif
    return 0;
}


int CVNConfig::QueryDHCPCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
#ifdef _NET_USE_DHCPCLIENT
    char strPath[256] ={0};
    g_DhcpClient.LoadDhcpStrNet(strPath, sizeof(strPath));
    tracef("----------------------OnQueryDHCPCfg---[in] = %s\n",strPath);

    pRespPDU->packetBody((char *)strPath, strlen(strPath)  );
#else
    pRespPDU->packetBody();
#endif
    
    return 0;
}

int CVNConfig::QueryDNSCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    int iRet = 0;

    std::string strDNS;

    char ip1[16] = {0};
    char ip2[16] = {0};
    
#ifndef WIN32
    iRet =  NetGetDNSAddress(ip1, 16, ip2, 16);
#endif
    if(iRet < 0)
    {
        tracef("NetGetDNSAddress failed\n");
        pRespPDU->packetBody();
        return -1;
    }

    tracef("[Print by vfware] ip1 = %s,ip2 = %s\n",ip1,ip2);

    std::string PrimaryIp;
    PrimaryIp += ip1;
    std::string SecondaryIp;
    SecondaryIp += ip2;
    strDNS += "PrimaryIp=";
    strDNS += PrimaryIp;
    strDNS += "::";
    strDNS += "SecondaryIp=";
    strDNS += SecondaryIp;

    pRespPDU->packetBody((char *)strDNS.c_str(), strDNS.length() );
    return 0;
}

int CVNConfig::QueryTransferPolicy(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CConfigNetCommon *pNetComm = new CConfigNetCommon;
    pNetComm->update();
    CONFIG_NET_COMMON& cfg = pNetComm->getConfig();
    char strData[128] = {0};
    switch(cfg.TransferPlan)
    {
        case IMAGE_TRANSFER_QUAILITY_PRIOR:
            sprintf(strData, "%d::ImgQlty-First", cfg.bUseTransferPlan);
            break;
        case IMAGE_FLUENCY_PRIOR:
            sprintf(strData, "%d::Fluency-First", cfg.bUseTransferPlan);
            break;
        default:
            sprintf(strData, "%d::Auto", cfg.bUseTransferPlan);
            break;
    }
    pRespPDU->packetBody((char *)strData, strlen(strData) );

    delete pNetComm;
    pNetComm = NULL;

    return 0;
}

int CVNConfig::QueryRegServer(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    tracef("not support QueryRegServer!\n");
    return -1;
}

int CVNConfig::QueryVideoMatrix(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    return 0;
}
int CVNConfig::QueryAudioFormat(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    return 0;
}


int CVNConfig::QueryDSPInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CAPTURE_DSPINFO dspInfo;
    GetDspInfo(&dspInfo);

    /// 捕获编码能力网络上使用的结构结构
    typedef struct CAPTURE_OLD_DSPINFO
    {
        unsigned int    nMaxEncodePower;    ///< DSP 支持的最高编码能力。
        unsigned short    nMaxSupportChannel;    ///< DSP 支持最多输入视频通道数。
        unsigned short    bChannelMaxSetSync;    ///< DSP 每通道的最大编码设置是否同步 0-不同步, 1 -同步。
    }CAPTURE_OLD_DSPINFO;

    //为了使网络能够获取到准确长度的配置,此处取老的配置结构长度
    pRespPDU->packetBody((char *)&dspInfo, sizeof(CAPTURE_OLD_DSPINFO) );
    
    return 0;
}

int CVNConfig::QueryAutoMaintain(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CConfigConversion CCfgCVS;

    CONFIG_AUTO_OLD cfgOldAuto;
    CONFIG_AUTO_OLD_CLIENT cfgOldAutoClient;
    memset(&cfgOldAuto, 0, sizeof(CONFIG_AUTO_OLD));
    memset(&cfgOldAutoClient, 0, sizeof(CONFIG_AUTO_OLD_CLIENT));

    CCfgCVS.LoadOldAutoMain(&cfgOldAuto);
    cfgOldAutoClient.AutoDeleteFilesTime = (uchar)cfgOldAuto.AutoDeleteFilesTime;
    cfgOldAutoClient.AutoRebootDay = (uchar)cfgOldAuto.AutoRebootDay;
    cfgOldAutoClient.AutoRebootTime = (uchar)cfgOldAuto.AutoRebootTime;

    pRespPDU->packetBody((char *)&cfgOldAutoClient,  sizeof(CONFIG_AUTO_OLD_CLIENT) );
    return 0;
}

int CVNConfig::QueryRecord(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    int iLen = sizeof(CONFIG_RECORD) * N_SYS_CH;
    CPacket *pPkt = g_PacketManager.GetPacket(iLen);
    
    for (int i = 0; i < N_SYS_CH; i++)
    {
        CONFIG_RECORD* pRecord = (CONFIG_RECORD*)(pPkt->GetBuffer()+i*sizeof(CONFIG_RECORD));  
        
        memcpy(pRecord, (char *)&CConfigRecord::getLatest(i), sizeof(CONFIG_RECORD));
    }
    
    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLen);
    pPkt->Release();

    return 0;
}

int CVNConfig::QueryEncode(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CConfigConversion CCfgCVS; 
    int iLength = N_SYS_CH * sizeof(CONFIG_CAPTURE_OLD);

    CPacket *pPkt = g_PacketManager.GetPacket(iLength);
    assert(pPkt != NULL);
    pPkt->SetLength(iLength);

    CCfgCVS.LoadOldEncode((CONFIG_CAPTURE_OLD *)pPkt->GetBuffer());
    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);
    
    pPkt->Release();

    return 0;
}

int CVNConfig::QueryColor(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CConfigConversion CCfgCVS; 
    int iLength = N_SYS_CH * sizeof(CONFIG_COLOR_OLD);
    CPacket *pPkt = g_PacketManager.GetPacket(iLength);
    pPkt->SetLength(iLength);

    CONFIG_COLOR_OLD *pColor = (CONFIG_COLOR_OLD *)pPkt->GetBuffer();

    CCfgCVS.LoadOldColor(pColor);

    for (int ii = 0; ii < g_nCapture; ii++)
    {
        for (int jj = 0; jj < N_COLOR_SECTION; jj++)
        {
            //网络上搞反了，纠正一下
            if (pColor->Color[jj].Gain & BITMSK(7))
            {
                pColor->Color[jj].Gain = pColor->Color[jj].Gain & 0x7f;
            }
            else
            {
                pColor->Color[jj].Gain |= BITMSK(7);
            }
        }
        pColor++;
    }

    pRespPDU->packetBody((char *)pPkt->GetBuffer(), iLength);
    pPkt->Release();
    return 0;
}

int CVNConfig::QueryHighDownload(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CConfigNetCommon CCfgNetComm;
    CCfgNetComm.update();

    bool bUse = CCfgNetComm.getConfig().bUseHSDownLoad ? true : false;
    char strData[32]={0};

    sprintf(strData, "%d", bUse);

    tracef("strData:%s\n",strData);
    pRespPDU->packetBody((char *)strData, strlen(strData));

    return 0;
}

int CVNConfig::QueryVideoCover(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    tracef("query multi video cover\n");

    VIDEO_COVER_T * pConfigVCT;
    VIDEO_COVER_ATTR_T * pConfigVCAT;

    int cha = pReqPDU->m_pIpHdr->dvrip_p[9];  //通道号

    int iLength = sizeof(VIDEO_COVER_T)+sizeof(VIDEO_COVER_ATTR_T)*COVERNUM;
    CPacket *pPacket = g_PacketManager.GetPacket(iLength);
    assert(pPacket);

    pConfigVCT = (VIDEO_COVER_T*)pPacket->GetBuffer();
    pConfigVCAT =(VIDEO_COVER_ATTR_T*) ((char*)pConfigVCT+ sizeof(VIDEO_COVER_T) );

    CConfigVideoWidget* pccfgVW = new CConfigVideoWidget;
    pccfgVW->update();

    CONFIG_VIDEOWIDGET&  cfgvw_t = pccfgVW->getConfig(cha-1);

    pConfigVCT->iChannel = cha;
	
	CAPTURE_CAPS caps;
	GetCaps(&caps);
		
    pConfigVCT->iCoverNum = (caps.CoverBlocks < cfgvw_t.iCoverNum ? caps.CoverBlocks : cfgvw_t.iCoverNum);
    //pConfigVCT->iCoverNum = cfgvw_t.iCoverNum;
    if ( pConfigVCT->iCoverNum > COVERNUM) 
    {
        pConfigVCT->iCoverNum= COVERNUM;
    }
    
    tracef("cover_num[%d], channel[%d]\n", cfgvw_t.iCoverNum, cha);
    
    memset(pConfigVCT->iRev, 0, sizeof(pConfigVCT->iRev) );

    for(int i = 0; i < pConfigVCT->iCoverNum; i++)
    {
        pConfigVCAT[i].tBlock = cfgvw_t.dstCovers[i].rcRelativePos;
        pConfigVCAT[i].Color = cfgvw_t.dstCovers[i].rgbaFrontground;
        pConfigVCAT[i].Encode = cfgvw_t.dstCovers[i].bEncodeBlend;
        pConfigVCAT[i].Priview = cfgvw_t.dstCovers[i].bPreviewBlend;
        pConfigVCAT[i].iBlockType = 0; /*黑色*/
    }

    delete pccfgVW;

    iLength = sizeof(VIDEO_COVER_T)+sizeof(VIDEO_COVER_ATTR_T)*(pConfigVCT->iCoverNum);
    
    pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
    
    pPacket->Release( );
    return 0;
}


int CVNConfig::QueryEvent(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    switch( pReqPDU->m_pIpHdr->dvrip_p[16])
    {
        case EVENT_ALARM:    //本地报警
        {
            int iLength = 0;
            CPacket *pPacket = 0;            
            iLength = sizeof(CONFIG_ALARM) * N_ALM_IN;
            
            pPacket = g_PacketManager.GetPacket(iLength);
            
            for (int i = 0; i < N_ALM_IN; i++)
            {
                memcpy((char *)(pPacket->GetBuffer()+i*sizeof(CONFIG_ALARM)), 
                    (char *)&(CConfigAlarm::getLatest(i)), 
                    sizeof(CONFIG_ALARM));
            }
            pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
            pPacket->Release( );        

            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_ALARM;

        }
        return 0;
        case EVENT_NETALARM:    //网络报警
        {
            int iLength = 0;
            CPacket *pPacket = 0;            
            
            iLength = sizeof(CONFIG_ALARM) * N_ALM_IN;
            pPacket = g_PacketManager.GetPacket(iLength);
            for (int i = 0; i < N_ALM_IN; i++)
            {
                memcpy((char *)(pPacket->GetBuffer()+i*sizeof(CONFIG_ALARM)), 
                    (char *)&(CConfigNetAlarm::getLatest(i)), 
                    sizeof(CONFIG_ALARM));            
            }

            pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
            pPacket->Release( );        

            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_NETALARM;
        }
        return 0;
        case EVENT_MOTION:    //动态检测
        {
            int iLength = 0;
            CPacket *pPacket = 0;            
            
            iLength = sizeof(CONFIG_MOTIONDETECT) * N_ALM_IN;
            pPacket = g_PacketManager.GetPacket(iLength);
            for (int i = 0; i < N_ALM_IN; i++)
            {
                memcpy((char *)(pPacket->GetBuffer()+i*sizeof(CONFIG_MOTIONDETECT)), 
                    (char *)&(CConfigMotionDetect::getLatest(i)), 
                    sizeof(CONFIG_MOTIONDETECT));            
            }

            pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
            pPacket->Release( );        

            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_MOTION;
        }
        return 0; 
        case EVENT_LOSS:    //视频丢失
        {
            int iLength = 0;
            CPacket *pPacket = 0;            
            
            iLength = sizeof(CONFIG_GENERIC_EVENT) * N_ALM_IN;
            pPacket = g_PacketManager.GetPacket(iLength);
            for (int i = 0; i < N_ALM_IN; i++)
            {
                memcpy((char *)(pPacket->GetBuffer()+i*sizeof(CONFIG_GENERIC_EVENT)), 
                    (char *)&(CConfigLossDetect::getLatest(i)), 
                    sizeof(CONFIG_GENERIC_EVENT));            
            }

            pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
            pPacket->Release( );        

            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_LOSS;
        }
        return 0;

        case EVENT_BLIND:    //黑屏
        {
            int iLength = 0;
            CPacket *pPacket = 0;            
            
            iLength = sizeof(CONFIG_BLINDDETECT) * N_ALM_IN;
            pPacket = g_PacketManager.GetPacket(iLength);
            for (int i = 0; i < N_ALM_IN; i++)
            {
                memcpy((char *)(pPacket->GetBuffer()+i*sizeof(CONFIG_BLINDDETECT)), 
                    (char *)&(CConfigBlindDetect::getLatest(i)), 
                    sizeof(CONFIG_BLINDDETECT));            
            }

            pRespPDU->packetBody((char *)pPacket->GetBuffer(), iLength);
            pPacket->Release( );        

            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_BLIND;
        }
        return 0;

        case EVENT_NOHDD:    //无硬盘
        {
            pRespPDU->packetBody((char *)&(CConfigStorageNotExist::getLatest()), sizeof(CONFIG_GENERIC_EVENT));
            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_NOHDD;
        }
        return 0;

        case EVENT_ERRHDD:    //硬盘出错
        {
            pRespPDU->packetBody((char *)&(CConfigStorageFailure::getLatest()), sizeof(CONFIG_GENERIC_EVENT));
            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_ERRHDD;
        }
        return 0;

        case EVENT_NOSPACEHDD:    //硬盘无空间
        {
            pRespPDU->packetBody((char *)&(CConfigStorageLowSpace::getLatest()), sizeof(CONFIG_STORAGE_SPACE_EVENT));
            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_NOSPACEHDD;
        }
        return 0;

        //---------------------------------------------------------------------------
        case EVENT_NETABORT:    //断网
        {
            pRespPDU->packetBody((char *)&(CConfigNetAbort::getLatest()), sizeof(CONFIG_GENERIC_EVENT));
            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_NETABORT;
        }
        return 0;

		 case EVENT_IP_CONFLICT:    
        {
            pRespPDU->packetBody((char *)&(CConfigNetArp::getLatest()), sizeof(CONFIG_GENERIC_EVENT));
            pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_EVENT;
            pRespPDU->m_pIpHdr->dvrip_p[16] = EVENT_IP_CONFLICT;
        }
		return 0;
        default:
        pRespPDU->packetBody();
        return 0;
    }
    return 0;
}

typedef struct _GooLinkConf
{
	unsigned int 	IsEnable;/*0 disable 1 enable*/
	unsigned char 	udid[32];
}GooLinkConf;
int CVNConfig::QueryGooLink(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
	GooLinkConf GlConf = {0};

	SAVENANDINFO_T 		FacInfo = {0};
	ReadFacInfo(&FacInfo);
	strcpy((char *)GlConf.udid, FacInfo.PTPUUID);
	pRespPDU->packetBody((char *)&GlConf, sizeof(GlConf));
    return 0;
}
int CVNConfig::ModGooLink(CIVnPdu*& pReqPDU)
{
	GooLinkConf *pTemp = NULL;
	GooLinkConf GlConf = {0};
	pTemp = (GooLinkConf *)( (char*)(pReqPDU->GetPacket() + DVRIP_HEAD_T_SIZE) );
	_printd("set enable = %d, str = [%s]", pTemp->IsEnable, pTemp->udid);
#if 0
	if (strlen((char *)pTemp->udid) != 10){
		sprintf ((char *)pTemp->udid, "zz00000000");
		pTemp->IsEnable = 0;
		_printd("set udid [%s] enable = %d", pTemp->udid, pTemp->IsEnable);
	}
#endif
#ifdef LANGTAO
	g_GooLink.SetConf(pTemp->IsEnable, pTemp->udid);
#else
	SAVENANDINFO_T FacInfo = {0};
	ReadFacInfo(&FacInfo);
	strcpy(FacInfo.PTPUUID, (char *)pTemp->udid);
	WriteFacInfo(&FacInfo);
#endif
	//memcpy((char *)&GlConf, (char *)pTemp, sizeof(GooLinkConf));
	return 0;
}
int CVNConfig::QueryDecode(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    CPacket* pPacket = g_PacketManager.GetPacket( m_pCaptureM->GetDigitalChnNum()*sizeof(CFG_DECODE_T) );
    if( pPacket == NULL )
    {
        trace("CVNConfig::QueryDecode-can't alloc  packet!\n");
        return CONFIG_APPLY_VALIT_ERROR;
    }

    /* 这里简单点转化，因为多连接不可能完全对应 */
    CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
    CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
    assert(ptmpLocal);
    assert(ptmpRemote);
    
    ptmpLocal->update();
    ptmpRemote->update();
    
    for( int i = 0; i < m_pCaptureM->GetLogicChnNum(); i++ )
    {
    	if( !m_pCaptureM->IsSupportDigital(i) )
    	{
    		continue;
    	}
        
        CFG_DECODE_T tmpCfg;
        memset( &tmpCfg, 0, sizeof(CFG_DECODE_T) );
        tmpCfg.channel = i;

        LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(tmpCfg.channel);
        REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(tmpCfg.channel);
        
        unsigned int uiIndex = (stLocalCfg.uiNotTourCfgIndex >= MAX_TOUR_NUM)?0:stLocalCfg.uiNotTourCfgIndex;
            
        tmpCfg.device_type = stRemoteCfg.stRemoteCfg[uiIndex].iDevType;
        tmpCfg.device_port = stRemoteCfg.stRemoteCfg[uiIndex].iDevicePort;
        tmpCfg.device_ip = stRemoteCfg.stRemoteCfg[uiIndex].DeviceIP.l;
        //char *tp = (char*)&tmpCfg.device_ip;
        //trace("%#x:%#x:%#x:%#x\n", *tp, *(tp+1), *(tp+2), *(tp+3));
        tmpCfg.device_channel = stRemoteCfg.stRemoteCfg[uiIndex].iRemoteChannelNo;
        
        tmpCfg.enable = stRemoteCfg.stRemoteCfg[uiIndex].BEnable && stLocalCfg.BEnable;
        tmpCfg.stream_type = stRemoteCfg.stRemoteCfg[uiIndex].iStreamType;

        int min_len = std::min(sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName),
                sizeof(tmpCfg.username) );
        memcpy(tmpCfg.username, stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName, min_len);

        min_len = std::min(sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword),
                    sizeof(tmpCfg.password) );
        memcpy(tmpCfg.password, stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword, min_len);
        
        CTlv tlv;

        tlv.SetType( 1 );
        tlv.SetValue((unsigned char*)&tmpCfg, sizeof(CFG_DECODE_T), false );

        unsigned char * pBuf = NULL;
        int total_len = 0;
        int ret = 0;

        ret = tlv.Pack(pBuf, total_len);
        if( ret < 0 )
        {
            trace("CVNConfig::ModDecode-tlv pack error!\n");
            delete ptmpLocal;
            ptmpLocal = NULL;

            delete ptmpRemote;
            ptmpRemote = NULL;

			pPacket->Release();
			
            return CONFIG_APPLY_VALIT_ERROR;
        }

        pPacket->PutBuffer(pBuf, total_len);
    }

    delete ptmpLocal;
    ptmpLocal = NULL;

    delete ptmpRemote;
    ptmpRemote = NULL;
    
    pRespPDU->packetBody( (char *)pPacket->GetBuffer(), pPacket->GetLength() );
    pPacket->Release();
    return 0;
}

int CVNConfig::ModDecode(CIVnPdu*& pReqPDU)
{
    int ret= 0 ;
    uint analyse_len = 0;

    /* 这里简单点转化，因为多连接不可能完全对应 */
    CConfigLocalDigiChCfg *ptmpLocal = new CConfigLocalDigiChCfg();
    CConfigRemoteDigiChCfg *ptmpRemote = new CConfigRemoteDigiChCfg();
    assert(ptmpLocal);
    assert(ptmpRemote);    
    
    ptmpLocal->update();
    ptmpRemote->update();
        
    while( analyse_len < pReqPDU->m_pIpHdr->dvrip_extlen )
    {
        CTlv tlv;

        ret = tlv.Parse( (unsigned char * )(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE + analyse_len),
                pReqPDU->m_pIpHdr->dvrip_extlen-analyse_len );
        if( ret < 0 )
        {
            trace("CVNConfig::ModDecode-tlv parse error!\n");
            ret = CONFIG_APPLY_VALIT_ERROR;
            break;
        }

        analyse_len+= tlv.GetTotalLen();

        CFG_DECODE_T tmpCfg;
        memset( &tmpCfg, 0, sizeof(CFG_DECODE_T) );
        ret = tlv.GetValue((unsigned char*)&tmpCfg, sizeof(CFG_DECODE_T));
        assert(ret == 0 );

        if( tmpCfg.channel >= m_pCaptureM->GetLogicChnNum() )
        {
            trace("CVNConfig::ModDecode-channel[%dd] error!\n", tmpCfg.channel);
            ret = CONFIG_APPLY_VALIT_ERROR;
            break;
        }

        LOCAL_CH_CFG &stLocalCfg= ptmpLocal->getConfig(tmpCfg.channel);
        REMOTE_DIGI_CH_CFG &stRemoteCfg = ptmpRemote->getConfig(tmpCfg.channel);

        unsigned int uiIndex = stLocalCfg.uiNotTourCfgIndex;
        uiIndex  = (uiIndex >= MAX_TOUR_NUM)?0:uiIndex;
                
        stLocalCfg.BEnable = (VD_BOOL)tmpCfg.enable;
        stLocalCfg.iMode = DIGI_CH_MODE_SINGLE;

        stRemoteCfg.stRemoteCfg[uiIndex].iDevType = tmpCfg.device_type;
        stRemoteCfg.stRemoteCfg[uiIndex].iDevicePort = tmpCfg.device_port;
        stRemoteCfg.stRemoteCfg[uiIndex].DeviceIP.l = tmpCfg.device_ip;

        stRemoteCfg.stRemoteCfg[uiIndex].iRemoteChannelNo = tmpCfg.device_channel;
        stRemoteCfg.stRemoteCfg[uiIndex].BEnable = tmpCfg.enable;
        stRemoteCfg.stRemoteCfg[uiIndex].iStreamType = tmpCfg.stream_type;

        int min_len = std::min(sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName),
                sizeof(tmpCfg.username) );
        memcpy(stRemoteCfg.stRemoteCfg[uiIndex].cDeviceUserName, tmpCfg.username, min_len);

        min_len = std::min(sizeof(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword),
                    sizeof(tmpCfg.password));
        memcpy(stRemoteCfg.stRemoteCfg[uiIndex].cDevicePassword, tmpCfg.password, min_len);
     }

    if( ret == 0 && analyse_len == pReqPDU->m_pIpHdr->dvrip_extlen )
    {
        ptmpLocal->commit();
        ptmpRemote->commit();
    }
    else
    {
        trace("CVNConfig::ModDecode-error,analyse_len:%d\n", analyse_len);
        ret = CONFIG_APPLY_VALIT_ERROR;
    }

    delete ptmpLocal;
    ptmpLocal = NULL;

    delete ptmpRemote;
    ptmpRemote = NULL;
    return ret;
}

int CVNConfig::PacketWorkSheet(CIVnPdu*& pRespPDU,const CONFIG_WORKSHEET * stWS)
{
    CPacket* pPacket = NULL;

    assert(stWS);

    pPacket = g_PacketManager.GetPacket(sizeof(UI_WORKSHEET));
    assert(pPacket);

    int len = sizeof(int)+N_UI_TSECT*sizeof(TIMESECTION);
    pPacket->PutBuffer((unsigned char *)stWS,len);

    for(int ii = 1; ii<N_WEEKS;ii++)
    {
        pPacket->PutBuffer((unsigned char *)stWS->tsSchedule[ii],N_UI_TSECT*sizeof(TIMESECTION));
    }
    
    pRespPDU->packetBody( (char *)pPacket->GetBuffer(), sizeof(UI_WORKSHEET));
    pPacket->Release();
    
    return 0;
}

int CVNConfig::QueryWorkSheet(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    int iWorkSheet = pReqPDU->m_pIpHdr->dvrip_p[17] - 1;
    switch(pReqPDU->m_pIpHdr->dvrip_p[16])
    {
        case WORKSHEET_TYPE_RECORD:
        {
            //录像工作表从0开始,0--15

            CConfigRECWorksheet *configRecWorksheet = new CConfigRECWorksheet();
            configRecWorksheet->update();
            CONFIG_WORKSHEET& cfgWorkSheet = configRecWorksheet->getConfig(iWorkSheet);

            CConfigRECState *configRecState = new CConfigRECState();
            configRecState->update();
            CONFIG_RECSTATE& cfgRecStat = configRecState->getConfig(iWorkSheet);

            for(int i=0; i<N_WEEKS; i++)
            {
                for(int j=0; j<N_TSECT; j++)
                {
                    cfgWorkSheet.tsSchedule[i][j].enable = (cfgWorkSheet.tsSchedule[i][j].enable == 0)?0:4;
                    cfgWorkSheet.tsSchedule[i][j].enable |= cfgRecStat.recstate[i][j]&BITMSK(0);
                    cfgWorkSheet.tsSchedule[i][j].enable |= cfgRecStat.recstate[i][j]&BITMSK(1);
                }
            }
            delete configRecState;
            
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&cfgWorkSheet);
            delete configRecWorksheet;

            break;
        }
        case WORKSHEET_TYPE_ALARM:
            //pRespPDU->packetBody((char *)&(CConfigALMWorksheet::getLatest(iWorkSheet)), sizeof(CONFIG_WORKSHEET));
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&CConfigALMWorksheet::getLatest(iWorkSheet));

            break;
        case WORKSHEET_TYPE_NETALARM:
            //pRespPDU->packetBody((char *)&(CConfigALMWorksheet::getLatest(iWorkSheet)), sizeof(CONFIG_WORKSHEET)); 
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&CConfigALMWorksheet::getLatest(iWorkSheet));

            break;
        case WORKSHEET_TYPE_MOTION:
            //pRespPDU->packetBody((char *)&(CConfigMTDWorksheet::getLatest(iWorkSheet)), sizeof(CONFIG_WORKSHEET));            
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&CConfigMTDWorksheet::getLatest(iWorkSheet));

            break;
        case WORKSHEET_TYPE_BLIND:
            //pRespPDU->packetBody((char *)&(CConfigBLDWorksheet::getLatest(iWorkSheet)), sizeof(CONFIG_WORKSHEET));
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&CConfigBLDWorksheet::getLatest(iWorkSheet));
            break;
        case WORKSHEET_TYPE_LOSS:
            //pRespPDU->packetBody((char *)&(CConfigVLTWorksheet::getLatest(iWorkSheet)), sizeof(CONFIG_WORKSHEET));
            PacketWorkSheet(pRespPDU,(const CONFIG_WORKSHEET *)&CConfigVLTWorksheet::getLatest(iWorkSheet));
            break;
        default:
            pRespPDU->packetBody();
            break;
    }


    pRespPDU->m_pIpHdr->dvrip_p[8] = CFG_WORKSHEET;
    pRespPDU->m_pIpHdr->dvrip_p[16] = pReqPDU->m_pIpHdr->dvrip_p[16];
    pRespPDU->m_pIpHdr->dvrip_p[17] = pReqPDU->m_pIpHdr->dvrip_p[17];

    return 0;
}

int CVNConfig::ModMac(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    if(pReqPDU->m_pIpHdr->dvrip_p[0] == 0
        && (pReqPDU->m_pIpHdr->dvrip_extlen > 0
            && pReqPDU->m_pIpHdr->dvrip_extlen <= 17))
    {
        char pEthMacAddr[18] = {0};
        memcpy(pEthMacAddr,
            (char *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE),
            pReqPDU->m_pIpHdr->dvrip_extlen);
        pEthMacAddr[pReqPDU->m_pIpHdr->dvrip_extlen] = '\0';

        if((pEthMacAddr[0] == '0') &&( pEthMacAddr[1] == '0'))
        {
            for(int ii = 2; ii < 17; ii++ )
            {
                if(pEthMacAddr[ii] == ':')
                {
                    continue;
                }
                else if((pEthMacAddr[ii] >= '0' && pEthMacAddr[ii] <= '9')||
                    (pEthMacAddr[ii] >= 'a' && pEthMacAddr[ii] <='f') ||
                    (pEthMacAddr[ii] >= 'A'&& pEthMacAddr[ii] <= 'F'))
                {
                }
                else
                {
                    tracef("Mac Enter is wrong\n");
                    return 0;
                }
            }
            #ifndef WIN32
            NetSetMAC("eth0", pEthMacAddr);
            #endif
        }
    }


    return 0;
}


int CVNConfig::ExportCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    int iRetLen = 0;
    int fileType = pReqPDU->m_pIpHdr->dvrip_p[1];
    CPacket *pPacket = g_PacketManager.GetPacket(CInExport::enum_configbuf_len);    
    if (NULL == pPacket)
    {
        errorf("CVNConfig::ExportCfg pPacket == NULL.\n");
        return -1;
    }
    if ((iRetLen = CInExport::instance()->ExportConfig(fileType, pPacket)) <= 0)
    {
        pPacket->Release();
        errorf("CVNConfig::ExportCfg Export %d Config error.\n", fileType);
        return -1;
    }    

    CIVnPdu* pRsp = new CIVnPdu();
    pRespPDU = pRsp;
    pRespPDU->packetBody((char*)pPacket->GetBuffer(),iRetLen);
    pRespPDU->m_pIpHdr->dvrip_p[0] = ACK_CONF_EXPORT;
    pRespPDU->m_pIpHdr->dvrip_p[1] = fileType;
    pRespPDU->m_pIpHdr->dvrip_cmd = ACK_CONF_EXPORT;
    pPacket->Release();

    return 0;
}

int CVNConfig::ImportCfg(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{ 
    uint uiLen = 0;

    //当接收到配置导入信令的第一个包，则分配CONFIG_BUF_LEN大小的内存
    if(1 == pReqPDU->m_pIpHdr->dvrip_r0)
    {
        m_InportRevPacketNum = 0;
        m_InportRevCfgTotalBytes = 0;
        if (NULL == m_pPacket)
        {
            tracef("CVNConfig::InportCfg GetPacket Startr\n");
            m_pPacket = g_PacketManager.GetPacket(CInExport::enum_configbuf_len);        
        }
        if(NULL == m_pPacket)
        {        
            tracef("CVNConfig::InportCfg GetPacket Failed\r\n");
            return 0;
        }
    }
    m_InportRevPacketNum++;    
    
    uiLen = pReqPDU->m_pIpHdr->dvrip_extlen;    //获取到待导入文件的长度

    memcpy(m_pPacket->GetBuffer()+ m_InportRevCfgTotalBytes,(char *)pReqPDU->GetPacket() + DVRIP_HEAD_T_SIZE, uiLen);
    m_InportRevCfgTotalBytes += uiLen;
    
    //infof("nsequence[%d] packetNum[%d] packetsize[%d] m_iRecvCfgPkgNum[%d] m_iRecvCfgTotalBytes[%d]\r\n ",
    //    m_pDVRIPHead->dvrip_r0,m_pDVRIPHead->dvrip_r1,uiLen,m_InportRevPacketNum,m_InportRevCfgTotalBytes);

    //接收完所有的配置导入包，则开始导入配置参数
    int tmpRecvPkgNum = pReqPDU->m_pIpHdr->dvrip_r1;
    if(m_InportRevPacketNum == tmpRecvPkgNum)
    {            
        //added by wyf on 20100113    解决导入非法配置文件DVR死机问题
        int iInExportRet = CInExport::instance()->InportConfig(CInExport::InExportFileAll, m_pPacket, m_InportRevCfgTotalBytes);
        m_pPacket->Release();
        
        if(0 != iInExportRet)
        {
            return 0;
        }
        //end 
        
        CIVnPdu* pRsp = new CIVnPdu();
        pRespPDU = pRsp;
        pRespPDU->m_pIpHdr->dvrip_cmd = ACK_CONF_IMPORT;
        pRespPDU->m_pIpHdr->dvrip_p[0] = ACK_CONF_IMPORT;
        pRespPDU->packetBody();

        return CONFIG_APPLY_CFG_REBOOT;//modified by wyf on 20101008 
    }
    return 1; 
}

