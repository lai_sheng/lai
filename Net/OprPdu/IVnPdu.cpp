
#include <string.h>

#include "IVnPdu.h"
#include "System/BaseTypedef.h"

CIVnPdu::CIVnPdu(void)
{
	m_pBuffer = 0;
	m_iTotalLen = 0;
	m_pPacker = 0;
	memset(m_stDVRIPHdr, 0 , sizeof(m_stDVRIPHdr));
	m_pIpHdr = (DVRIP *)m_stDVRIPHdr;
	m_pBuffer = (char *)m_stDVRIPHdr;

	m_MainTlvList.clear();
	m_SubTlvList.clear();

	m_MainTypeLen = 0;
	m_strMainTypePtr = 0;
	m_pConn = 0;
}

CIVnPdu::~CIVnPdu(void)
{
	if (m_pPacker)
	{
		m_pPacker->Release();
	}
	m_MainTlvList.clear();
	m_SubTlvList.clear();
}

bool CIVnPdu::parseBody(char* pBuff, int iLen)
{
	if ( NULL == pBuff || iLen < 32 )
	{
		return false;
	}

#if 0
	///////协议外已经处理
	DVRIP* pTmp = (DVRIP* )pBuff;
	if (pTmp->dvrip_extlen + DVRIP_HEAD_T_SIZE < iLen)
	{
		return false;
	}
#endif

	if (iLen == DVRIP_HEAD_T_SIZE)
	{
		memcpy(m_stDVRIPHdr, pBuff, DVRIP_HEAD_T_SIZE);
		m_pBuffer = (char *)m_stDVRIPHdr;
		m_pIpHdr = (DVRIP* )m_pBuffer;
		m_iTotalLen = DVRIP_HEAD_T_SIZE;
	}
	else
	{
	    if (!m_pPacker)
	    {
	        m_pPacker = g_PacketManager.GetPacket(iLen+4);
		    if (!m_pPacker)
		    {
			    return false;
		    }
	    }
		

		m_pBuffer = (char *)m_pPacker->GetBuffer();
		memcpy(m_pBuffer, pBuff, iLen);
		m_pIpHdr = (DVRIP* )m_pBuffer;
		m_pBuffer[iLen] = '\0';
		m_iTotalLen = iLen;
	}

	return true;
}

int CIVnPdu::packetBody(char* pData, int iLen)
{
	if (iLen > 0)
	{
	   //有扩展数据
	    if (!m_pPacker)
	    {
	        m_pPacker = g_PacketManager.GetPacket(iLen+DVRIP_HEAD_T_SIZE);
			if (!m_pPacker)
			{
				return -1;
			}
	    }
		
		m_pPacker->SetLength(iLen+DVRIP_HEAD_T_SIZE);
		m_pBuffer = (char *)m_pPacker->GetBuffer();
		memset(m_pBuffer,0,m_pPacker->GetLength());

		memcpy(m_pBuffer, m_pIpHdr, DVRIP_HEAD_T_SIZE);
		m_pIpHdr = (DVRIP* )m_pBuffer;
		m_pIpHdr->dvrip_extlen = iLen;
		m_iTotalLen = DVRIP_HEAD_T_SIZE+iLen;
        // 针对有扩展数据填充时直接拷贝
		if (pData)
		{
			memcpy(m_pBuffer+DVRIP_HEAD_T_SIZE, pData, iLen);
			m_pPacker->SetLength(m_iTotalLen);
		}
		else//在没有填充扩展数据前只存在有效长度为32字节，后续进行数据添加
		{
			m_pPacker->SetLength(DVRIP_HEAD_T_SIZE); 
		}
	}
	else
	{
		//只有头部32字节
		m_pBuffer = (char *)m_stDVRIPHdr;
		m_pIpHdr = (DVRIP* )m_pBuffer;
		m_iTotalLen = DVRIP_HEAD_T_SIZE;
	}

	//添加版本信息
	m_pIpHdr->dvrip_hl = DVRIP_HEAD_T_SIZE/4;
	m_pIpHdr->dvrip_v = DVRIP_VERSION;		
	return m_iTotalLen;
}

//bMainType 用于区分是主还是副tlv
IVN_TLV_NODE* CIVnPdu::getTlvNode(int ieType, bool bMainType)
{
	if ( bMainType)
	{
		for (std::list<IVN_TLV_NODE>::iterator ListItr = m_MainTlvList.begin();
			ListItr != m_MainTlvList.end();
			ListItr++)
		{
			if (ieType != (*ListItr).stHdr.IeType || !(*ListItr).pData || (*ListItr).stHdr.IeLengh <= 0)
			{
				continue;
			}

			(*ListItr).stHdr.IeType = -1;//指定无效，多次查询避免重复

			return &(*ListItr);
		}
	}
	else
	{
		for (std::list<IVN_TLV_NODE>::iterator ListItr = m_SubTlvList.begin();
			ListItr != m_SubTlvList.end();
			ListItr++)
		{
			if (ieType != (*ListItr).stHdr.IeType || !(*ListItr).pData || (*ListItr).stHdr.IeLengh <= 0)
			{
				continue;
			}

			(*ListItr).stHdr.IeType = -1;//指定无效，多次查询避免重复

			return &(*ListItr);
		}
	}


	return NULL;
}

bool CIVnPdu::parseTlv(char* pBuff, int iLen, bool bMainType)
{
	if ( pBuff == NULL || iLen <= 0)
	{
		return true;
	}

	if( !bMainType)
	{
		m_SubTlvList.clear();
	}

	int iBodyLen = 0;
	char* pBodyPtr = pBuff;

	while( iBodyLen < iLen)
	{
		/* 检查长度,是否满足够一个ie头长度分析	*/
		if (iLen < iBodyLen + IVN_TLV_HEADER)
		{
			return false;
		}

		IVN_TLV_NODE tmpTlvNode;
		memset(&tmpTlvNode, 0 , sizeof(tmpTlvNode));

		memcpy(&tmpTlvNode.stHdr, (char *)pBodyPtr, IVN_TLV_HEADER);

		/* 是否够一个ie分析*/
		if (iLen < iBodyLen +(tmpTlvNode.stHdr.IeLengh + IVN_TLV_HEADER))
		{
			return false;
		}

		char* pIeData = (char*)(pBodyPtr + IVN_TLV_HEADER);

		/* 各个子节点添加到list中，注意只是引用，包体缓冲不能删除*/
		tmpTlvNode.pData = pIeData;

		if (bMainType)
		{
			m_MainTlvList.push_back(tmpTlvNode);
		}
		else
		{
			m_SubTlvList.push_back(tmpTlvNode);
		}

		iBodyLen += (tmpTlvNode.stHdr.IeLengh + IVN_TLV_HEADER);	//分析数据长度增加
		pBodyPtr += (tmpTlvNode.stHdr.IeLengh + IVN_TLV_HEADER);	//缓冲后移
	}

	return true;
}


bool CIVnPdu::addTlvNode(int ieType, int ieLength, char* pValue)
{
	if (!m_pPacker)
	{
		return false;
	}

	int iLen  = -1;

     /* 当数据为空或者 长度为0时候，只填充TL*/
    if (NULL == pValue || ieLength == 0)
	{
		//需要判断下缓冲长度,临时保存下主TLV头地址
		m_strMainTypePtr = (char *)(m_pPacker->GetBuffer()+m_pPacker->GetLength());
		//两层TLV模式此统计扩展L所有长度
		m_MainTypeLen = 0; 

       iLen = m_pPacker->PutBuffer((char *)&ieType, 4);
	   
	   iLen = m_pPacker->PutBuffer((char *)&ieLength, 4);

	   return true;
	}
	else if ( ieLength < 0)
	{
		return false;
	}

	//需要判断下缓冲长度
	iLen = m_pPacker->PutBuffer((char *)&ieType, 4);

	iLen = m_pPacker->PutBuffer((char *)&ieLength, 4);
	iLen = m_pPacker->PutBuffer((char *)pValue, ieLength);

	//多层TLV 保留长度
	m_MainTypeLen += IVN_TLV_HEADER+ieLength;

	return true;
}

void CIVnPdu::changeLength(bool bMainType)
{
	if (!m_pPacker)
	{
		return ;
	}

   if ( bMainType)
   {
	   m_pBuffer = (char *)m_pPacker->GetBuffer();
	   m_pIpHdr = (DVRIP* )m_pBuffer;
	   m_pIpHdr->dvrip_extlen = m_pPacker->GetLength() - DVRIP_HEAD_T_SIZE;
	   m_iTotalLen = m_pPacker->GetLength();
      
   }
   else
   { 
	   if ( m_strMainTypePtr )
	   {
		   memcpy((char *)(m_strMainTypePtr+4),(char *)&m_MainTypeLen, 4);
	   }
   }
}


