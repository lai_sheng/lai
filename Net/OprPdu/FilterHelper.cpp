
#include "FilterHelper.h"
#include "IVNUser.h"
#include "IVNControl.h"
#include "IVNConfig.h"

#include "IVNLogState.h"
//#include "IVNAlmState.h"

#include "IVnPdu.h"
#include "IVNSysConfig.h"
//#include "IVNRemoteConfig.h"

#include "Configs/ConfigNet.h"
#include "Configs/ConfigLocation.h"
#include "Net/NetConfig.h"

#include "../VideoMonitor.h"
#include "APIs/DVRDEF.H"
#include "System/BaseTypedef.h"
#include "System/Version.h"
#include "System/UserManager.h"
#include "System/Tlv.h"
#include "ez_libs/ez_socket/ez_socket.h"
#include "Main.h"
#include "System/AppConfig.h"


extern int g_SystemAuthFail;


PATTERN_SINGLETON_IMPLEMENT(CFilterHelper);
std::string CFilterHelper::m_userName;

enum EnLoginFailedErrorId
{
    EnLoginFailed_PasswdNotValid = 0, // 密码不正确
    EnLoginFailed_AccountNotValid,// 帐号不存在
    EnLoginFailed_AccountLogined = 3,//已经登录
    EnLoginFailed_AccountLocked,//被锁定
    EnLoginFailed_AccountInBlackList,//黑名单
};

//!报警类型
enum _alarm_type_t
{
    ALARM_IN     = 1,    //外部报警
    ALARM_MOTION    ,    //动态监测报警
    ALARM_LOSS      ,    //视频丢失报警
    ALARM_BLIND     ,    //视频遮挡
    ALARM_AUDIO     ,    //音频报警
    ALARM_DISKLOW   ,    //硬盘低容量报警
    ALARM_DISKERROR ,    //硬盘错误报警
    ALARM_NODISK    ,    //无硬盘报警
    
    ALARM_ENCODER = 10,       //编码器报警
    ALARM_EMERGENCY = 11,     //紧急报警事件
    ALARM_INFRARED = 12, //红外报警事件
    ALARM_IPCONFLICT = 13,     //IP冲突报警    
    ALARM_ALL       ,    //一般报警如果需要添加，这前添加
    
    //下面是特殊的报警，从151开始
    ALARM_COMM  = 151,  //串口故障
    ALARM_DECODER = 156,//解码器报警
    ALARM_DECODER_AV = 157, //!本地音视频解码器报警
 };

//报警操作
enum EnAlarmOpr
{
    OpStQueryAlarmInfo = 1,        //查询
    OpStBookAlarmInfo     ,        //订阅
    OpStCanselAlarmInfo   ,        //取消
};
enum EnLoginResult
{
    EnLoginResult_Succeeded = 0,    // login succeeded
    EnLoginResult_Failed = 1,        // login failed
    EnLoginResult_Logined = 3,        // login yet
};

//!普通信息返回码 对应命令 0x64
enum GeneralMsgRetCode
{
    GeneralMsgRetCode_Success = 0, // 无错
    GeneralMsgRetCode_Busy = 1, // 系统忙
    GeneralMsgRetCode_NoAuth = 2, // 无权限
    GeneralMsgRetCode_ConnFull = 3, // 连接满
    GeneralMsgRetCode_IpNotAllowed = 4, // IP 不被容许
};

typedef struct _User_Auth_List
{
    unsigned int uiMonitorMask;
    unsigned int uiPlayMask;
    unsigned int uiNetPreviewMask;
    unsigned int uiControlConfigMask;
    unsigned int uiReserved[4];
} USER_AUTH_LIST, *pUSER_AUTH_LIST;

typedef struct _Super_User_Auth_List
{
    unsigned int uiFunMask;   //
	/*按位表示支持的功能
	0 : 支持多种机芯设置 
       1:  多种编码类型
       2: 支持设置坏点检查
	*/
    unsigned int uiSensorTypeL;   /*机芯类型掩码低32位*/
    unsigned int uiSensorTypeH;	
    unsigned char ucProfileType;  /* 掩码表示2 hp  1 M   0  Baseline*/
    unsigned char uiReserved[63];
} SUPER_USER_AUTH_LIST, *pSUPER_USER_AUTH_LIST;


bool CheckAuth( CConfigTable& user, char* auth )
{
    for( int j = 0; j < user["AuthorityList"].size(); j++ )
    {
        if(user["AuthorityList"][j].asString().compare(auth) == 0 )
        {
            return true;
        }
    }

    return false;
}

bool GetUserAuth(std::string& user, USER_AUTH_LIST* user_auth)
{

    CConfigTable config_user;

    if( !g_userManager.getUser(user, config_user) )
    {
        return false;
    }

    memset( user_auth, 0, sizeof(USER_AUTH_LIST) );

    char tmp[128] = {0};
    int i = 0; 
    memset(tmp, 0, sizeof(tmp));
    //!回放权限
    for( i = 0; i < g_nCapture; i++ )
    {
        ez_snprintf(tmp, sizeof(tmp), "Replay_%2d", i);
        if( CheckAuth(config_user, tmp) )
        {
            user_auth->uiPlayMask |= BITMSK(i);
        }
    }

    memset(tmp, 0, sizeof(tmp));
    //!预览权限
    for( i = 0; i < g_nCapture; i++ )
    {
        ez_snprintf(tmp, sizeof(tmp), "NetPreview_%2d", i);
        if( CheckAuth(config_user, tmp) )
        {
            user_auth->uiNetPreviewMask |= BITMSK(i);
        }
    }

    //!控制配置权限
#define SET_AUTH(x)    \
    if( CheckAuth(config_user, x) )    \
    {    \
        user_auth->uiControlConfigMask |= BITMSK(i);    \
    }

    i = 0;
    SET_AUTH("CtrlPanel");

    i++;
    SET_AUTH("ShutDown");

    i++;
    SET_AUTH("RecordMode");

    i++;
    SET_AUTH("Backup");

    i++;
    SET_AUTH("StorageManager");

    i++;
    SET_AUTH("PTZControl");

    i++;
    SET_AUTH("Account");

    i++;
    SET_AUTH("SysInfo");

    i++;
    SET_AUTH("AlarmIO");

    i++;
    SET_AUTH("QueryLog");

    i++;
    SET_AUTH("DelLog");

    i++;
    SET_AUTH("SysUpgrade");

    i++;
    SET_AUTH("AutoMaintain");

    i++;
    SET_AUTH("GeneralConfig");

    i++;
    SET_AUTH("EncodeConfig");

    i++;
    SET_AUTH("RecordConfig");

    i++;
    SET_AUTH("CommConfig");

    i++;
    SET_AUTH("NetConfig");

    i++;
    SET_AUTH("VideoConfig");

    i++;
    SET_AUTH("PtzConfig");

    i++;
    SET_AUTH("OutputConfig");

    i++;
    SET_AUTH("DefaultConfig");

#undef SET_AUTH

    return true;
}


CFilterHelper::CFilterHelper()
{
    m_VapUser = new CVNUser();
    m_VapControl = new CVNControl();
    m_VapConfig = new CVNConfig();
    m_VapLogState = new CVNLogState();
        
    m_VapSysConfig = new IVNSysConfig();
#ifdef GUI_DEVICE_REFRESH
    //m_VapRemoteConfig = new IVNRemoteConfig();
#endif

    //m_NetKeyBoard = new CNetKeyBoard();

    registerFilter( CFilterHelper::FileterProc(&CVNUser::ReqUserAuth, 		m_VapUser), 	REQ_USER_PARAM);
    registerFilter( CFilterHelper::FileterProc(&CVNUser::CtrlAuthSwitch, 	m_VapUser), 	REQ_CTRL_SWTCH);

    registerFilter( CFilterHelper::FileterProc(&CVNConfig::SetConfig, 		m_VapConfig), 	REQ_CONF_UPDAT);
    registerFilter( CFilterHelper::FileterProc(&CVNConfig::GetConfig, 		m_VapConfig), 	REQ_CONF_PARAM);
    registerFilter( CFilterHelper::FileterProc(&CVNConfig::ModMac, 			m_VapConfig), 	REQ_MODIFY_MAC);
    registerFilter( CFilterHelper::FileterProc(&CVNConfig::ExportCfg, 		m_VapConfig), 	REQ_CONF_EXPORT);
    registerFilter( CFilterHelper::FileterProc(&CVNConfig::ImportCfg, 		m_VapConfig), 	REQ_CONF_IMPORT);

    registerFilter( CFilterHelper::FileterProc(&IVNSysConfig::SetConfig, 	m_VapSysConfig), SET_SYSTEM_CONFIG);
    registerFilter( CFilterHelper::FileterProc(&IVNSysConfig::GetConfig, 	m_VapSysConfig), REQ_SYSTEM_CONFIG);

#ifdef GUI_DEVICE_REFRESH
    //registerFilter( CFilterHelper::FileterProc(&IVNRemoteConfig::SetConfig, m_VapRemoteConfig), SET_REMOTE_SYSTEM_CONFIG);
    //registerFilter( CFilterHelper::FileterProc(&IVNRemoteConfig::GetConfig, m_VapRemoteConfig), REQ_REMOTE_SYSTEM_CONFIG);
#endif

    registerFilter( CFilterHelper::FileterProc(&CVNControl::SetOsdTitle, 	m_VapControl), REQ_INFO_PROC);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::SetSysTime, 	m_VapControl), REQ_SET_DTIME);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::QueryChanTitle, m_VapControl), REQ_CHAN_TITLE);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::ModChanTitle, 	m_VapControl), REQ_CHAN_UPDAT);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::CtrlMonSw, 		m_VapControl), REQ_CTRL_MONSW);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::SetOsdTitle, 	m_VapControl), REQ_BOARD_CTRL);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::CtrlFrontBoard, m_VapControl), REQ_INFO_PROC);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::SetExtAlarm, 	m_VapControl), REQ_ALARM_INFO);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::SimuAlarmIn, 	m_VapControl), REQ_CTRL_ALARM);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::SimuAlarmOut, 	m_VapControl), REQ_CTRL_LAMP);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::OprPTZCom, 		m_VapControl), REQ_PROT_TITLE);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::CtrlPtz, 		m_VapControl), REQ_CTRL_PANTZ);
    registerFilter( CFilterHelper::FileterProc(&CVNControl::ForceIFrame, 	m_VapControl), REQ_USER_FORCEIFRAME);

    //registerFilter( CFilterHelper::FileterProc(&CVNAlmState::QueryStatInfo, m_VapAlmState), REQ_STAT_QUERY);

    registerFilter( CFilterHelper::FileterProc(&CVNLogState::CtrlRecord, 	m_VapLogState), REQ_CTRL_RECRD);
//    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QueryRecordList, m_VapLogState), REQ_RECD_QUERY);
//    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QueryAlarmRecord, m_VapLogState), REQ_FILE_ALARM);
    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QuerySystemOpr,m_VapLogState), REQ_SYSTEM_OPTQUERY);
    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QueryLogs, 	m_VapLogState), REQ_LOGS_PARAM);
    registerFilter( CFilterHelper::FileterProc(&CVNLogState::CtrlDevice, 	m_VapLogState), REQ_DEVICE_CTRL);
    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QueryDevice, 	m_VapLogState), REQ_DEVICE_QUERY);
    registerFilter( CFilterHelper::FileterProc(&CVNLogState::QuerySysInfo, m_VapLogState), REQ_INFO_SYSTM);

    m_AuthStringList.insert(AuthStringList::value_type(REQ_CONF_UPDAT, "CtrlPanel"));
    m_AuthStringList.insert(AuthStringList::value_type(REQ_MODIFY_MAC, "CtrlPanel"));
    m_AuthStringList.insert(AuthStringList::value_type(REQ_CONF_IMPORT, "CtrlPanel"));

    m_AuthStringList.insert(AuthStringList::value_type(REQ_CTRL_PANTZ, "PTZControl"));
    m_AuthStringList.insert(AuthStringList::value_type(REQ_CTRL_RECRD, "RecordMode"));
    m_AuthStringList.insert(AuthStringList::value_type(REQ_LOGS_PARAM, "QueryLog"));
    
    //m_AuthStringList.insert(AuthStringList::value_type(REQ_DEVICE_CTRL, "DefaultConfig"));    //恢复默认配置
    //m_AuthStringList.insert(AuthStringList::value_type(REQ_USER_PARAM, "Account"));    



    memset(m_NetConnList, 0, sizeof(m_NetConnList));
    m_NetBlockIp.clear();   //屏蔽ip列表

    m_pExpireTimer = new CTimer("ConnState");
    

    //初始化报警状态变量
    for (int i = 0; i < appEventAll; i++)
    {
        m_iAlarmState[i] = appEventInit;
    }
    
    //向事件中心注册处理类
    CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CFilterHelper::onAppEvent);
    m_pExpireTimer->Start(this, (VD_TIMERPROC)&CFilterHelper::onConnTimer,0, 60 * 1000);

    
}

CFilterHelper::~CFilterHelper()
{
    CAppEventManager::instance()->detach(this, (SIG_EVENT::SigProc)&CFilterHelper::onAppEvent);
}


bool CFilterHelper::registerFilter(FileterProc proc, int enmCommand)
{
    std::pair<FileterTable::iterator, bool> ret = 
        m_sFilterTable.insert(FileterTable::value_type(enmCommand, proc));
    if (!ret.second)
    {
        tracef("CFilterHelper::registerFilter had been register!!\n");
        return false;
    }

    return true;
}


bool CFilterHelper::unregisterFilter(int enmCommand)
{
    if(m_sFilterTable.erase(enmCommand))
    {
        return true;
    }
    return false;
}



///处理解析好的消息,返回要处理的响应消息，如果成功返回seq，否则-1
int CFilterHelper::FilterCompat(int enmCommand, CIVnPdu*& pReq, CIVnPdu*& pResp)
{
    FileterTable::iterator iter = m_sFilterTable.find(enmCommand);
    int iRet  = 0;
    if (iter != m_sFilterTable.end())
    {
        iRet = (*iter).second(pReq, pResp);
    }
    else
    {
        tracef("CFilterHelper::FilterCompat CMD<%x> doesnot exist!\r\n", enmCommand);
    }

    return iRet;
}


//!睿威协议处理
int CFilterHelper::MsgProcess(struct conn *pConnect, char *buf, int len)
{
	//CUser *pUser = (CUser*)pConnect->context;	
	
#ifdef __BELL_QZTEL__
	SesionInfo_t *sess = (SesionInfo_t *)pConnect->context;
	CUser *pUser = (CUser*)sess->user;
#else	
	CUser *pUser = (CUser*)pConnect->context;
#endif
	CFilterHelper::m_userName=pUser->getName();
    if(pConnect->bVodFlag)
    {
        
        DVRIP szHdr;
        memset(&szHdr, 0, sizeof(szHdr));
        
        szHdr.dvrip_cmd= ACK_RECD_PLAY;
        szHdr.dvrip_r0= 0x11;
        szHdr.dvrip_r1= ~(szHdr.dvrip_r0);
        szHdr.dvrip_p[0] = pConnect->iVodChn+ 1;    //协议中是从1开始的
        szHdr.dvrip_p[7] = 0x12; // ???
        tracef("sendStopDownloadCMD=======>>>>>>>>>>>>>>>\n");
       
        msghdr_t msghdr;
        memset(&msghdr, 0, sizeof(msghdr));
        msghdr.buf     = (void*)&szHdr;
        msghdr.buf_len = sizeof(szHdr);
        
        DvrNetSendMsg(pConnect, &msghdr);

        pConnect->bVodFlag =false;
    }
    
    CIVnPdu * pReq = new CIVnPdu();
    int iRet = 0;
    if (pReq && pReq->parseBody(buf, len))
    {
        CIVnPdu * pResp = 0;
        
//        tracef("@@@@CFilterHelper::FilterCompat@@@  cmd:<%d>\n", pReq->m_pIpHdr->dvrip_cmd);    

        switch(pReq->m_pIpHdr->dvrip_cmd)
        {
            case REQ_USER_LOGIN:
            {
                int count = 0;
				_printd ("cmd [REQ_USER_LOGIN]");
                for(int iIndex = 0; iIndex< C_MAX_TCP_CONN_NUM; iIndex++) 
                {
                    if(NULL!=m_NetConnList[iIndex])
                    {
                        count++;
                    } 
                }

                if(count > CConfigNetCommon::getLatest().MaxConn)
                {
                    pResp = new CIVnPdu();
                    if(pResp)
                    {
                        pResp->packetBody();

                        pResp->m_pIpHdr->dvrip_cmd = ACK_USER_LOGIN;
                        pResp->m_pIpHdr->dvrip_p[0] = EnLoginResult_Failed;
                        pResp->m_pIpHdr->dvrip_p[1] = 2;    //其他情况，协议未定义
                    }
                }
                else
                {
                    RegQuest(pConnect,pReq,pResp);
                }

                break;
            }	
			case REQ_STAT_QUERY:
	        {
				pResp = new CIVnPdu();
                if(pResp)
                {
                    pResp->packetBody();

                    pResp->m_pIpHdr->dvrip_cmd = ACK_STAT_QUERY;
                }
				break;
			}			
            case REQ_ALARM_STAT:
            {
				printf ("[%s][%d] cmd [REQ_ALARM_STAT]\n", __FILE__, __LINE__);
                ReqBookAlm(pConnect,pReq,pResp);
                break;
            } 
            case REQ_UPLOAD_PIC_STAT:
            {
				printf ("[%s][%d] cmd [REQ_UPLOAD_PIC_STAT]\n", __FILE__, __LINE__);
                ReqBookPic(pConnect,pReq,pResp);
                break;
            } 			
            case REQ_SUB_CONN:
            {
				printf ("[%s][%d] cmd [REQ_SUB_CONN]\n", __FILE__, __LINE__);
				OprSubConn(pConnect,pReq,pResp);
                break;
            }
            case REQ_CHAN_SWTCH:
            {
				printf ("[%s][%d] cmd [REQ_CHAN_SWTCH]\n", __FILE__, __LINE__);
                if (pConnect->pCapture)
                {                
                    int iRet = pConnect->pCapture->FilterCallback(pConnect, pReq->m_pIpHdr);
 #if 0
                    if (iRet==1)
                    {
                        pResp = new CIVnPdu();
                        if(pResp)
                        {
                            pResp->packetBody();

                            pResp->m_pIpHdr->dvrip_cmd = ACK_CHAN_MONIT;
                            pResp->m_pIpHdr->dvrip_p[0] = GeneralMsgRetCode_ConnFull;
                            //pResp->m_pIpHdr->dvrip_p[1] = ;    //其他情况，协议未定义
                        }
                     }
#endif                    
                }
                else
                {
                    __trip;
                }
                break;
            }
            case REQ_RECD_DOWN:
            case REQ_RECD_PLAY:
            case REQ_PLAY_STOP:
            case REQ_PLAY_PAUSE:
            case REQ_CTRL_PLAY:                
            {
				printf ("[%s][%d] cmd [REQ_RECD_DOWN]\n", __FILE__, __LINE__);
                if (pConnect->pVod)
                {
                    pConnect->pVod->FilterCallback(pConnect, pReq->m_pIpHdr);
                }
                break;
            }
            case REQ_AUDIO_DATA:
            case ACK_REQ_AUDIO_DATA:
            {
//				printf ("[%s][%d] cmd [REQ_AUDIO_DATA]\n", __FILE__, __LINE__);
                if (pConnect->pAudio)
                {
                    pConnect->pAudio->FilterCallback(pConnect,pReq->m_pIpHdr);
                }
                break;
            }   
            case REQ_NET_KEYBOARD_CTRL:
            {
				printf ("[%s][%d] cmd [REQ_NET_KEYBOARD_CTRL]\n", __FILE__, __LINE__);
                //m_NetKeyBoard->ParseHeader(pConnect,pReq,pResp);
                break;
            }
            case REQ_RECD_QUERY:
            {
				printf ("[%s][%d] cmd [REQ_RECD_QUERY]\n", __FILE__, __LINE__);
                m_VapLogState->QueryRecordList(pConnect,pReq,pResp);
                break;
            }
            case REQ_FILE_ALARM:
            {
				printf ("[%s][%d] cmd [REQ_FILE_ALARM]\n", __FILE__, __LINE__);
                m_VapLogState->QueryAlarmRecord(pConnect,pReq,pResp);
                break;
            }            
            default:
            {
				//if (pReq->m_pIpHdr->dvrip_cmd == 0xcd) return 2;
				_printd (" cmd [default[%x]]\n", pReq->m_pIpHdr->dvrip_cmd);
                std::string strAuth;
                bool bAuth =  false;
                Authiter it = m_AuthStringList.find((int)(pReq->m_pIpHdr->dvrip_cmd));
                if ( it != m_AuthStringList.end())
                {
                    strAuth = (*it).second;
                    bAuth = true;
                }

                if ( bAuth)
                {
                    //CUser* pUser = (CUser* )(pConnect->context);
				#ifdef __BELL_QZTEL__
					SesionInfo_t *sess = (SesionInfo_t *)pConnect->context;
					CUser *pUser = (CUser*)sess->user;
				#else	
					CUser *pUser = (CUser*)pConnect->context;
				#endif
                    bAuth = pUser->isValidAuthority((const char *)strAuth.c_str());
                }
                else
                {
                    bAuth = true;
                }
                
                if ( bAuth){
                    pReq->m_pConn = (void* )pConnect;
                    iRet = FilterCompat((int)(pReq->m_pIpHdr->dvrip_cmd), pReq, pResp);

					 //此处通过返回值判断此链接进行了手动抓拍上传图片
					if(iRet == DEV_MANUAL_SNAP) 
					{
						pConnect->bUpPicFlag = TRUE;
						iRet = 0;
					}
                }else{
                    //简单回复操作无权限
                    //应答
                    AckNomoral(pConnect, pReq->m_pIpHdr->dvrip_cmd, GeneralMsgRetCode_NoAuth);
                }
                break;
            }
        }
    
        if ( pResp)
        {
            msghdr_t msghdr;
            memset(&msghdr, 0, sizeof(msghdr));
            msghdr.buf     = (void*)pResp->GetPacket();
            msghdr.buf_len = pResp->GetPacketLen();
            DvrNetSendMsg(pConnect, &msghdr);

            delete pResp;
            pResp = NULL;
        }

        if( iRet & CONFIG_APPLY_REBOOT && iRet > 0)
        {
        	SystemSleep(500);
            g_Challenger.Reboot();
        }  

        //added by wyf on 20101008		
        if( iRet & CONFIG_APPLY_CFG_REBOOT && iRet > 0)
        {        
            g_Challenger.RebootForUsbConfig();
        } 
        //end added by wyf on 20101008	 
    }

    delete pReq;

    return 2;
}


void CFilterHelper::onConnTimer(uint arg)
{
//    tracef("CFilterHelper::onConnTimer-----------------\n");
    
    for(int i=0;i<C_MAX_TCP_CONN_NUM;i++)
    {
        if(m_NetConnList[i] != NULL)
        {
            if ((m_NetConnList[i]->iOnlineFlag!=0)&&(m_NetConnList[i]->iOnlineFlag!=1)&&(m_NetConnList[i]->iOnlineFlag!=-1))
            {
                printf("CFilterHelper::onConnTimer():m_NetConnList[%d]->iOnlineFlag = %d\r\n",i,m_NetConnList[i]->iOnlineFlag);
            }
            
            if((m_NetConnList[i]->iOnlineFlag < -1)  || (m_NetConnList[i]->iOnlineFlag >= 1) )
            {
                m_NetConnList[i]->iOnlineFlag = 0;
            }
            else if (m_NetConnList[i]->iOnlineFlag == 0)
            {
                m_NetConnList[i]->iOnlineFlag = -1;
            }
        }
    }
}


/**********************************************/
int CFilterHelper::SetBlockPeer(IPDEF ipPeer, uint iPeriod, time_t LastValid)
{
    IP_LIST_NODE Node;

    Node.ipIP.l = ipPeer.l;
    Node.iValidity = iPeriod;

    if (0 == LastValid)
    {
        time(&Node.ttLastValid);
    }
    else
    {
        Node.ttLastValid = LastValid;
    }

    m_NetBlockIp.push_back(Node);

    return 0;
}

int CFilterHelper::CheckPeerIP(in_addr in)
{
    tracef("The New Conn IP(%s)...\n", inet_ntoa(in));
    time_t ttNow;
    time(&ttNow);

    // 检查非法IP
    for (IP_LIST::iterator ii=m_NetBlockIp.begin();
        ii!=m_NetBlockIp.end();
        ii++)
    {
        if (in.s_addr == ii->ipIP.l)
        {
            tracef("Blocked at:%ld, %ld sec. Now:%ld\n", 
                (unsigned long)ii->ttLastValid, 
                (unsigned long)ii->iValidity, 
                (unsigned long)ttNow);

            // 限制过期了，容许now
            if (ttNow > (time_t)(ii->ttLastValid+ii->iValidity))
            {
                m_NetBlockIp.erase(ii);
                break;
            }
            else
            {
                tracef("Blocked now, should wait %ld sec.\n",(unsigned long)(ii->ttLastValid+ii->iValidity)-ttNow);
                return -1;
            }

            break;
        }
    }

    return 0;
}

/*+-+-+-+-+-+-+-+-+-+-注册和反注册操作连接,在多连接操作中存在问题+-+-+-+-+-+-+-+-+-+-*/

int CFilterHelper::RegisterConn(struct conn *c)
{
    assert(c);
    /*
    *初始化会话用户
    */    

#ifdef __BELL_QZTEL__

    char *p = (char*)malloc(sizeof(SesionInfo_t) + sizeof(BELL_NET_FLAG) );
	
    SesionInfo_t *sess = (SesionInfo_t *)p;
    sess->user = new CUser();

    BELL_NET_FLAG * pflag = NULL;
    pflag =  ( BELL_NET_FLAG* )( p + sizeof(SesionInfo_t) );
    pflag ->flag = CONN_UNKOWN;

    c->context = (void *) p;
#else
	CUser* pUser = new CUser();
    c->context = (void *) pUser;	
#endif

    c->pCapture = g_CaptureHelperManager.AllocCaptureHelper(c);//new CaptureHelper(c);
   // c->pCapture = new CaptureHelper(c);
   // c->pVod = new VodHelper(c);
   // c->pAudio = new AudioHelper(c);
   c->pVod = g_CaptureHelperManager.AllocCaptureVodHelper(c);
   c->pAudio = g_CaptureHelperManager.AllocCaptureAudioHelper(c);
    /*
    *处理连接数目限制
    *处理屏蔽的ip
    *ip是否有效
    */
    char szIp[16] = { 0 };
    Ip2Str(c->sa.u.sin.sin_addr.s_addr, szIp);
    
    if (SUCCESS_RET != CheckPeerIP(c->sa.u.sin.sin_addr))
    {
        AckNomoral(c, 0, GeneralMsgRetCode_IpNotAllowed);
        return -1;
    }
    
     if (!g_NetApp.IsTrustIP(szIp))
     {
          AckNomoral(c, 0, GeneralMsgRetCode_IpNotAllowed);
         return -1;
     }

    /*
    *保存连接配置信息
    */
    int IsIdle = -1;
    int ii = 0;
    int count = 0;

    for( ii = 0; ii < C_MAX_TCP_CONN_NUM; ii ++) 
    {
        if(NULL!=m_NetConnList[ii])
        {
//            tracef("Full address[%0lX],nameaddress[%0lX]\n",
//               (unsigned long)m_NetConnList[ii],
 //               (unsigned long)m_NetConnList[ii]->context);        

 //           tracef("List Full Id[%d],sock[%d],username[%s]\n",
//                ii,
 //               m_NetConnList[ii]->sock,
 //               (((CUser *)(m_NetConnList[ii]->context))->getName().c_str())
//                );

            count++;
        } 
        else if( IsIdle == -1)
        {
            tracef("List Idle Id[%d]\n",ii);
            IsIdle = ii;
            break;
         }
    }

    if((count < CConfigNetCommon::getLatest().MaxConn+1)
        && (IsIdle != -1))
    {
        m_NetConnList[IsIdle]=c;
    } 
    else
    {
        tracef("Too many connection\n");
        AckNomoral(c, 0, GeneralMsgRetCode_ConnFull);
        return -1;    
    }

    return 0;
}

void CFilterHelper::UnRegisterConn(struct conn *c)
{
    assert(c);
#ifdef __BELL_QZTEL__
    SesionInfo_t *sess = (SesionInfo_t *)c->context;
    CUser *pUser = (CUser*)sess->user;
#else	
    CUser *pUser = (CUser*)c->context;
#endif

    g_CaptureHelperManager.FreeCaptureHelper(c->pCapture);
    c->pCapture = 0;
#if 1
    g_CaptureHelperManager.FreeCaptureVodHelper(c->pVod);
    c->pVod = 0;
    g_CaptureHelperManager.FreeCaptureAudioHelper(c->pAudio);
    c->pAudio = 0;	
#else
    if(c->pVod)
    {
        delete c->pVod;
        c->pVod = NULL;
    }

    if(c->pAudio)
    {
        delete c->pAudio;
        c->pAudio = NULL;
    }
#endif
    if ( pUser)
    {
        pUser->logout();
        //delete pUser;
#ifdef __BELL_QZTEL__
        //tracepoint();
        delete pUser;
        free(sess);
#else
        delete pUser;
#endif
    }
    
    for(int ii = 0; ii < C_MAX_TCP_CONN_NUM; ii ++) 
    {
        if (c == m_NetConnList[ii])
        {
            tracef("Release ID[%d],sock[%d]\n",
                ii,m_NetConnList[ii]->sock);

            m_NetConnList[ii] = NULL;
            break;
        }
    }

}

/*
*得到连接对象指针conn*
*/
struct conn *CFilterHelper::GetNetConn(int iDlgNo)
{
    assert(iDlgNo >= 0 && iDlgNo < C_MAX_TCP_CONN_NUM);
        return  m_NetConnList[iDlgNo];
}

//////////////////////////////////////////////////////////////////////////////////////////
int CFilterHelper::OprSubConn(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
//    unsigned long ulSequencFlag = 0;
//    char cTmp[8] = {0};
//    memcpy(cTmp,pReqPDU->m_pIpHdr->dvrip_p,4);
//    sscanf(cTmp, "%ld", &ulSequencFlag);

    pConnect->iSubConnChn = pReqPDU->m_pIpHdr->dvrip_p[5];
    pConnect->iSubConnType = pReqPDU->m_pIpHdr->dvrip_p[4];
    memcpy((char *)&pConnect->ulConnSequenceFlag,pReqPDU->m_pIpHdr->dvrip_p,4);
    pConnect->iSubConnFlag = 1;
    return 0;
}

int CFilterHelper::RegQuest(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    

    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    //spRspPdu->packetBody();
    
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_USER_LOGIN;

    // 第2字节：1：具有多画面预览功能；0：没有
    pRspPdu->m_pIpHdr->dvrip_r0 = 0;
    pRspPdu->m_pIpHdr->dvrip_r1 = pReqPDU->m_pIpHdr->dvrip_r1;

    // 0: 表示登录成功 1: 表示登录失败 3: 用户已登录
    pRspPdu->m_pIpHdr->dvrip_p[0] = EnLoginResult_Succeeded;

    // 通道数
    pRspPdu->m_pIpHdr->dvrip_p[2] = g_nCapture;

    // 视频编码方式
    pRspPdu->m_pIpHdr->dvrip_p[3] = 9; // 新的码流H264
    pRspPdu->m_pIpHdr->dvrip_p[4] = 11; //DVR_NVS_C

    //转成字符串会导致序列号超过10000时出错
    char __strTmp[9]= {0};
//    sprintf(__strTmp, "%ld", pConnect->ulConnSequenceFlag);
//    printf("************** sequence is %ld\n",pConnect->ulConnSequenceFlag);

    /*第17--20四个字节保存序列号*/
    //memcpy(pRspPdu->m_pIpHdr->dvrip_p + 8,__strTmp ,sizeof(unsigned long ));
    memcpy(pRspPdu->m_pIpHdr->dvrip_p + 8,(char *)&pConnect->ulConnSequenceFlag ,sizeof(unsigned long ));
    
    //    第25字节: 主机主版本号
    //    第26字节: 主机版本号
    //    第27字节: 第25字节的反码
    pRspPdu->m_pIpHdr->dvrip_p[16] = 6;
    pRspPdu->m_pIpHdr->dvrip_p[17] = 0;
    pRspPdu->m_pIpHdr->dvrip_p[18] = ~pRspPdu->m_pIpHdr->dvrip_p[16];

    //第29字节: 主机的视频制式
    // 非实时
    pRspPdu->m_pIpHdr->dvrip_p[19] = 0;
    pRspPdu->m_pIpHdr->dvrip_p[20] = CConfigLocation::getLatest().iVideoFormat;
    pRspPdu->m_pIpHdr->dvrip_p[21] = g_nLogicNum; //通道数
    pRspPdu->m_pIpHdr->dvrip_p[22] = 0x64;    //视通标志
    pRspPdu->m_pIpHdr->dvrip_p[23] = 0x02; //

    //     第9-16字节: 用户名
    //    第17-24字节: 密码
    std::string strUName;
    std::string strUPwd;
    memset(__strTmp, 0, 9);
    memcpy(__strTmp, &(pReqPDU->m_pIpHdr->dvrip_p[0]), 8);
    __strTmp[8] = '\0';
    strUName = __strTmp;
    
    memset(__strTmp, 0, 9);
    memcpy(__strTmp, &(pReqPDU->m_pIpHdr->dvrip_p[8]), 8);
    __strTmp[8] = '\0';
    strUPwd = __strTmp;    

    if (pReqPDU->m_pIpHdr->dvrip_extlen > 0 
        && pReqPDU->m_pIpHdr->dvrip_extlen < 1024)
    {
        std::string strTmp;
        
        strTmp.assign((char *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE), 
            pReqPDU->m_pIpHdr->dvrip_extlen);
        
        tracef("USER PASSWORD<%s>\r\n", strTmp.c_str());
        
        size_t pos = strTmp.find("&&");
        if (pos != std::string::npos)
        {
            strUName.assign(strTmp.begin(), strTmp.begin()+pos);
            strUPwd.assign(strTmp.begin()+pos+strlen("&&"), strTmp.end());
        }
    }

#if 1//zhaohy add 20130327
	int pwd_no=0;
	for(pwd_no=0;pwd_no<6;pwd_no++)
	{
		strUPwd[pwd_no]=~strUPwd[pwd_no];
	}
#endif

    bool bIsSuccess = false;
    int iErrorId = 0;

    if(pReqPDU->m_pIpHdr->dvrip_v < DVRIP_VERSION)
    {
        pConnect->iClientFlag = 0;
        infof("old client verison %d \n",pReqPDU->m_pIpHdr->dvrip_v);
    }
    else
    {
        pConnect->iClientFlag = 1;
        infof("net  client verison %d \n",pReqPDU->m_pIpHdr->dvrip_v);
    }

    //CUser* pUser = (CUser* )(pConnect->context);
	#ifdef __BELL_QZTEL__
		SesionInfo_t *sess = (SesionInfo_t *)pConnect->context;
		CUser *pUser = (CUser*)sess->user;
	#else	
		CUser *pUser = (CUser*)pConnect->context;
	#endif
   // if(pReqPDU->m_pIpHdr->dvrip_p[19])
    {
        bIsSuccess = pUser->login(strUName, strUPwd, "DVRIP-Web",NoEncrypt);
    }
    if (!bIsSuccess)	
    {
        bIsSuccess = pUser->login(strUName, strUPwd, "DVRIP-Web",needEncrypt);
    }
    tracef("Login([%s], [%s]).\n ", strUName.c_str(), strUPwd.c_str());

    // 有此用户，且状态正常才行
    if (bIsSuccess)
    {
        tracef("succeeded\n");
        pRspPdu->m_pIpHdr->dvrip_p[0] = EnLoginResult_Succeeded;
        pRspPdu->m_pIpHdr->dvrip_p[1] = 2;        //统一设置为管理员
        //pRspPdu->m_pIpHdr->dvrip_p[19] = 1;

        if( pReqPDU->m_pIpHdr->dvrip_r1 & BITMSK(0) )
        {
            //!支持tlv分析

            //!属性tlv
            CTlv tlv_sys_attr;

            SYSATTR_EX_T* pSysattr = new SYSATTR_EX_T;
	     memset(pSysattr,0,sizeof(SYSATTR_EX_T));		
            GetSysAttrEx(pSysattr);
            tlv_sys_attr.SetType(DEV_SYSTEM_INFO_DEV_ATTR);
            tlv_sys_attr.SetValue( (unsigned char*)pSysattr, sizeof(SYSATTR_EX_T), false );
            delete pSysattr;

            //!序列号tlv
            char seq[48] = {0};
		if(g_SystemAuthFail > 0)
		{
			sprintf(seq,"Version Faile Code : 0x%x ",g_SystemAuthFail);
		}else
		{
            		CVersion::instance()->GetSerialNumber(seq, sizeof(seq));
		}
            CTlv tlv_seq;

            tlv_seq.SetType(DEV_SYSTEM_INFO_DEV_ID);
			//vc2700获取mac地址时，m_nested值被修改为true，导致无法获取到mac地址，移到pack之前处理
            //tlv_seq.SetValue((unsigned char*)seq, sizeof(seq), false );

            char version[64] = {0};
            CVersion::instance()->GetVersion(version, sizeof(version) );

            char pubdate[32] = {0};
            SYSTEM_TIME tm;
            CVersion::instance()->GetBuildDate(&tm);
            FormatTimeString(&tm,pubdate,FT_NORMAL);
            sprintf(version+strlen(version), "Build: %s", pubdate);

            CTlv tlv_version;
            tlv_version.SetType(DEV_SYSTEM_INFO_DEV_VER);
            tlv_version.SetValue((uchar*)version, sizeof(version), false );

            //!权限tlv
            USER_AUTH_LIST auth;
            GetUserAuth(strUName, &auth);
            CTlv tlv_auth;
            tlv_auth.SetType(DEV_SYSTEM_INFO_USER_AUTH);
            tlv_auth.SetValue( (unsigned char*)&auth, sizeof(USER_AUTH_LIST), false );

#if 0
            //!能力tlv
            CTlv tlv_ability;

            CPacket *pPacket_ability;
            GetAblility(pPacket_ability);
            tlv_ability.SetType(DEV_SYSTEM_INFO_DEV_ALL);
            tlv_ability.SetValue(pPacket_ability->GetBuffer(), pPacket_ability->GetLength());
            pPacket_ability->Release();
#endif
//add by ilena zhou 2010-06-03
            //设备类型tlv
            int DevType;
#ifdef VN_IPC
            char chType[128] = {0};
            SystemGetDeviceType(chType);
            if (0 == strcmp(chType, "VP26M2H"))
                DevType = VD_DOME; 
            else
                DevType = VD_IPC; 
#else
            /* 下面的SystemGetDeviceTyoe主要针对9304的各个
                  改造版本处理 */                                 
            char chType[128] = {0};
            SystemGetDeviceType(chType);
            if (0 == strcmp(chType, "DVS"))
            {
                DevType = VD_DVS; 
            }
            else if(0 == strcmp(chType, "DECODER"))
            {
                DevType = VD_DVS; 
            }
            else if(0 == strcmp(chType, "IPC"))
            {
                DevType = VD_IPC; 
            }
            else
            {
                DevType = VD_DVR;    
            }
#endif

            CTlv tlv_driver_type;
            tlv_driver_type.SetType(DEV_SYSTEM_INFO_DEV_TYPE);
            tlv_driver_type.SetValue( (unsigned char*)&DevType, sizeof(DevType), false );
//add end

#ifdef SDK_3516 
            CTlv tlv_SuperAuth;

	     SUPER_USER_AUTH_LIST  super_auth;

	    super_auth.uiFunMask = 0x3;

#ifdef FUN_SUPPORT_ISP
	   super_auth.uiFunMask |= BITMSK(2);
#endif		
	    super_auth.ucProfileType = 0x7;

	    VD_SENSOR_CAP_T Caps;		
	     VideoInGetSensorCap(&Caps);
	    super_auth.uiSensorTypeL =Caps.sensor_cap;

            tlv_SuperAuth.SetType(DEV_SYSTEM_INFO_SUPERUSER_AUTH);
            tlv_SuperAuth.SetValue( (unsigned char*)&super_auth, sizeof(SUPER_USER_AUTH_LIST), false );
#endif
            //!汇总，给扩展数据
            int ext_len = tlv_sys_attr.GetTotalLen()+
                    tlv_seq.GetTotalLen()
                    +tlv_auth.GetTotalLen();

#ifdef SDK_3516 
	if(pUser->isSupper())
	{
		ext_len += tlv_SuperAuth.GetTotalLen();
	}
#endif
            CPacket* pAllPacket = g_PacketManager.GetPacket(ext_len);

            uchar* p = NULL;
            int tmp_len = 0;
            tlv_sys_attr.Pack(p, tmp_len);
            pAllPacket->PutBuffer(p, tmp_len);

			tlv_seq.SetValue((unsigned char*)seq, sizeof(seq), false );
            tlv_seq.Pack(p, tmp_len);
            pAllPacket->PutBuffer(p, tmp_len);

            tlv_version.Pack(p, tmp_len);
            pAllPacket->PutBuffer(p, tmp_len);

            tlv_auth.Pack(p, tmp_len);
            pAllPacket->PutBuffer(p, tmp_len);

#ifdef SDK_3516 
	if(pUser->isSupper())
	{
            tlv_SuperAuth.Pack(p, tmp_len);
            pAllPacket->PutBuffer(p, tmp_len);
	}
#endif

            //tlv_ability.Pack(p, tmp_len);
            //pAllPacket->PutBuffer(p, tmp_len);

			tlv_driver_type.Pack(p, tmp_len);	//by ilena zhou
            pAllPacket->PutBuffer(p, tmp_len);

            pRspPdu->packetBody((char*)pAllPacket->GetBuffer(), pAllPacket->GetLength());

            pAllPacket->Release();
        }
        else
        {
            pRspPdu->packetBody();
        }
    }
    else // 登录失败
    {
        pRspPdu->m_pIpHdr->dvrip_p[0] = EnLoginResult_Failed; // 失败

	//给VS100返回具体登录错误的返回值 added by wyf on 20100528
//	iErrorId = GetOperator(m_pMsg->iDlgNo)->getErrorNo(); 
	//end added by wyf on 20100528
       iErrorId =  pUser->getErrorNo();

        switch (iErrorId)
        {
            case -2: // 密码错误
            {
				_printd("EnLoginFailed_PasswdNotValid");
                pRspPdu->m_pIpHdr->dvrip_p[1] = EnLoginFailed_PasswdNotValid;        //0: 表示密码错误
                break;
            }
            case -4: // 用户不存在
            {
				_printd("EnLoginFailed_AccountNotValid");
                pRspPdu->m_pIpHdr->dvrip_p[1] = EnLoginFailed_AccountNotValid;
                break;
            }
            case -5: // 锁定
            {
				_printd("EnLoginFailed_AccountLocked");
                pRspPdu->m_pIpHdr->dvrip_p[1] = EnLoginFailed_AccountLocked;
                break;
            }
            case -6: // 黑名单
            {
				_printd("EnLoginFailed_AccountInBlackList");
                pRspPdu->m_pIpHdr->dvrip_p[1] = EnLoginFailed_AccountInBlackList;
                break;
            }
            case -7: // 帐号已经登录
            {
				_printd("EnLoginResult_Logined");
                pRspPdu->m_pIpHdr->dvrip_p[0] = EnLoginResult_Logined; // 失败
                pRspPdu->m_pIpHdr->dvrip_p[1] = EnLoginFailed_AccountLogined;
                break;
            }
            default: // 其他情况：用户名不存在、帐号锁定、帐号停用等
            {
				_printd("LoginFailed_default");
                pRspPdu->m_pIpHdr->dvrip_p[1] = 2;    //其他情况，协议未定义
                break;
            }
        }// switch
        pRspPdu->packetBody();
    }
	pRspPdu->m_pIpHdr->dvrip_v  = pReqPDU->m_pIpHdr->dvrip_v; 

    pRespPDU = pRspPdu;
    return 0;
}
int CFilterHelper::ReqBookPic(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
	
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    int tmp = pReqPDU->m_pIpHdr->dvrip_p[1];

	pConnect->iUpPicMsk = tmp<<8 |pReqPDU->m_pIpHdr->dvrip_p[0];

    pRespPDU = pRspPdu;

    pRspPdu->m_pIpHdr->dvrip_extlen = 0;
    pRspPdu->m_pIpHdr->dvrip_p[8] = 0;
  
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_UPLOAD_PIC_STAT;

    
    return  0;
}
/* 客户端订阅告警信息，由于不同的客户端可能订阅不同的告警，所以每个客户端单独处理*/
int CFilterHelper::ReqBookAlm(struct conn *pConnect , CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;

    int  iEventType  = 0;     
    int  iAlmType = pReqPDU->m_pIpHdr->dvrip_p[4];
    pRspPdu->m_pIpHdr->dvrip_p[4]  = pReqPDU->m_pIpHdr->dvrip_p[4];
    pRspPdu->m_pIpHdr->dvrip_extlen = 0;
    pRspPdu->m_pIpHdr->dvrip_p[8] = 0;

    switch(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        case OpStQueryAlarmInfo: //查询报警状态
        {
            tracef("wpl  no suport\n");
            break;
        }
        case OpStBookAlarmInfo: //订阅报警状态
        {
            switch(iAlmType)
            {
                case ALARM_IN:
                    iEventType = appEventAlarmLocal;
                    break;
                case ALARM_MOTION:
                    iEventType = appEventVideoMotion;
					_printd ("appEventVideoMotion");

                    break;
                case ALARM_LOSS:
                    iEventType = appEventVideoLoss;
                    break;
                case ALARM_BLIND:
                    iEventType = appEventVideoBlind;
                    break;
                case ALARM_DISKLOW:
                    iEventType = appEventStorageLowSpace;
                    break;
                case ALARM_DISKERROR:
                    iEventType = appEventStorageFailure;
                    break;
                    //增加IP冲突报警added by wyf on 20100310
                case ALARM_IPCONFLICT:
                    iEventType = appEventNetArp;
                    break;
                    //end added by wyf on 20100310    
                    
                case ALARM_ENCODER:
                    iEventType = appEventAlarmEncoder;
                    break;
                case ALARM_DECODER:
                    iEventType = appEventAlarmDecoder;
                    break;
                case ALARM_COMM:
                    iEventType = appEventComm;
                    break;
                case ALARM_DECODER_AV:
                    iEventType = appEventAlarmDecoder_AV;
                    break;
                default:
                    iEventType = appEventInit;
                    break;
            }

            if(iAlmType > 0) 
            {
                pConnect->iUserBookAlarm|= BITMSK(iEventType);
            }
            else
            {
                pRespPDU->m_pIpHdr->dvrip_p[8] = 1;
            }

            break;
        }
        case OpStCanselAlarmInfo:    // 取消订阅报警状态
        {
            switch(iAlmType)
            {
                case ALARM_IN:
                    iEventType = appEventAlarmLocal;
                    break;
                case ALARM_MOTION:
                    iEventType = appEventVideoMotion;
							printf ("[%s][%d]appEventVideoMotion\n", __FILE__, __LINE__);

                    break;
                case ALARM_LOSS:
                    iEventType = appEventVideoLoss;
                    break;
                case ALARM_BLIND:
                    iEventType = appEventVideoBlind;
                    break;
                case ALARM_DISKLOW:
                    iEventType = appEventStorageLowSpace;
                    break;
                case ALARM_DISKERROR:
                    iEventType = appEventStorageFailure;
                    break;
                //增加IP冲突报警added by wyf on 20100310
                case ALARM_IPCONFLICT:
                    iEventType = appEventNetArp;
                    break;
                //end added by wyf on 20100310                        
                case ALARM_ENCODER:
                    iEventType = appEventAlarmEncoder;
                    break;
                case ALARM_DECODER:
                    iEventType = appEventAlarmDecoder;
                    break;
                case ALARM_COMM:
                    iEventType = appEventComm;
                    break;
                case ALARM_DECODER_AV:
                    iEventType = appEventAlarmDecoder_AV;
                    break;
                default:
                    iEventType = appEventInit;
                    break;
            }

            if(iAlmType > 0) 
            {
                pConnect->iUserBookAlarm&= (~ BITMSK(iEventType));
            }
            else
            {
                pRspPdu->m_pIpHdr->dvrip_p[8] = 1;
            }

            break;
        }
        default:
        {
            pRspPdu->m_pIpHdr->dvrip_p[8] = 1;
            
            break;
        }
    }
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_ALARM_STAT;
    pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
    
    return  0;
}

void CFilterHelper::AckNomoral(struct conn *pConnect , uchar iLastOpr, uchar iRetCode)
{
    //应答
    CIVnPdu * pResp = new CIVnPdu();
    if(pResp)
    {
        pResp->packetBody();
        
        pResp->m_pIpHdr->dvrip_cmd = ACK_INFO_NORMAL;
        pResp->m_pIpHdr->dvrip_p[0] = iLastOpr;
        pResp->m_pIpHdr->dvrip_p[4] = iRetCode;
        
        msghdr_t msghdr;
        memset(&msghdr, 0, sizeof(msghdr));
        msghdr.buf     = (void*)pResp->GetPacket();
        msghdr.buf_len = pResp->GetPacketLen();
        DvrNetSendMsg(pConnect, &msghdr);

        delete pResp;
    }
}

//事件处理函数 ，此种做法报警存在问题
void CFilterHelper::onAppEvent(appEventCode code, int index,
                                      appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
	_printd("CFilterHelper::onAppEvent appEventCode[%d]", code);
    if ((code < appEventInit && code >= appEventAll))
    {
        return;
    }

#if defined(FUNCTION_SUPPORT_MESSAGETONET)
    //增加对外部报警信息是否上传网络的判断 2008-05-15
    if ((code == appEventAlarmLocal) && (!param->bMessagetoNet))
    {
        trace("MessagetoNet::%d code ::%d \n", param->bMessagetoNet, code);
        return;
    }
#endif

    switch(action)
    {
        case appEventStart:
        m_iAlarmState[(int)code] |= BITMSK(index);
        break;
        case appEventStop:
        m_iAlarmState[(int)code] &= ~(BITMSK(index));
        break;
        case appEventLatch:
        break;
        default:
        break;
    }

    bool bNeedNotify = false;
    for(int ii = 0; ii < C_MAX_TCP_CONN_NUM; ii ++) 
    {
        if (NULL !=  m_NetConnList[ii] &&
            (m_NetConnList[ii]->sock != INVALID_SOCKET))
        {
            if ((m_NetConnList[ii]->iUserBookAlarm & BITMSK(code)) != 0)
            {
                bNeedNotify = true;
                break;
            }
        }
    }

    if (!bNeedNotify)
    {
    	_printd("bNeedNotify == false");
        return ;
    }

    int iEventType = -1;
    switch(code)
    {
    case appEventAlarmLocal:
        iEventType = ALARM_IN;
        break;
    case appEventAlarmInfrared:
        iEventType = ALARM_INFRARED;
        break;
    case appEventVideoMotion:
        iEventType = ALARM_MOTION;
        break;
    case appEventVideoLoss:
        iEventType = ALARM_LOSS;
        break;
    case appEventVideoBlind:
        iEventType = ALARM_BLIND;
        break;
    case appEventStorageLowSpace:
        iEventType = ALARM_DISKLOW;
        break;
    case appEventStorageFailure:
        iEventType = ALARM_DISKERROR;
        break;
    case appEventAlarmEncoder:
        iEventType = ALARM_ENCODER;
        break;
        //增加IP冲突报警added by wyf on 20100310
    case appEventNetArp:
        iEventType = ALARM_IPCONFLICT;
        break;
        //end added by wyf on 20100310
        
    case appEventAlarmEmergency:
        iEventType = ALARM_EMERGENCY;
        break;
    case appEventAlarmDecoder:
        iEventType = ALARM_DECODER;
        break;
    case appEventAlarmDecoder_AV:
        iEventType = ALARM_DECODER_AV;
        break;
    default:
        tracef("\nno support alarm !!!!!!!!!!\n");
        return ;
    }

    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
		return ;
    }

    pRspPdu->packetBody((char*)&m_iAlarmState[code],(uint)sizeof(int));
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_ALARM_STAT;
    pRspPdu->m_pIpHdr->dvrip_p[4] =iEventType;

    msghdr_t msghdr;
    memset(&msghdr, 0, sizeof(msghdr));
    msghdr.buf     = (void*)pRspPdu->GetPacket();
    msghdr.buf_len = pRspPdu->GetPacketLen();

    for(int ii = 0; ii < C_MAX_TCP_CONN_NUM; ii ++) 
    {
        if (NULL !=  m_NetConnList[ii] &&
            (m_NetConnList[ii]->sock != INVALID_SOCKET))
        {
            if ((m_NetConnList[ii]->iUserBookAlarm & BITMSK(code)) != 0)
            {
            	//_printd("DvrNetSendMsg");
                DvrNetSendMsg(m_NetConnList[ii], &msghdr);
            } 
        }
    }
    delete pRspPdu;
}


