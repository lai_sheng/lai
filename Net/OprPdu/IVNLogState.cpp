#include "IVNLogState.h"
#include "IVnPdu.h"
#include "Devices/DevCapture.h"
#include "Functions/DriverManager.h"
#include "Functions/Record.h"
#include "Configs/ConfigManager.h"
#include "System/User.h"
#include "System/UserManager.h"
#include "System/AppConfig.h"
#include "Functions/Comm.h"

#include "System/Log.h"
#include "System/Version.h"
#include "System/Tlv.h"
#include "Devices/DevAudioIn.h"
#include "Configs/ConfigManager.h"
#include "Configs/ConfigConversion.h"
#include "Configs/ConfigDigiChManager.h"

#include "APIs/MotionDetect.h"

#include "Functions/Alarm.h"
#include "APIs/Split.h"
#include "Devices/DevSplit.h"
#include "Devices/DevDetect.h"
#include "Media/ICapture.h"
#include "Net/NetCore.h"
#include "Net/NetClient/NetCliConfig-x.h"
#include "Net/NetWorkService.h"
//#include "Main.h"
#include "APIs/Audio.h"
#include "Devices/DevAudioIn.h"
//#define _ZXW_VILAR 1


 //!查询系统属性信息
typedef struct __sysattr_t
{
    unsigned char iVerMaj;          // BOIS主版本号(软件)
    unsigned char iVerSub;          // BOIS次版本号(软件)
    unsigned char iVideoInCaps;     // 视频输入接口数量
    unsigned char iAudioInCaps;     // 音频输入接口数量
    unsigned char iAlarmInCaps;     // 报警输入接口数
    unsigned char iAlarmOutCaps;    // 报警输出接口数
    unsigned char iIdePortNum;      // IDE接口数
    unsigned char iAetherNetPortNum;// 网络接口数
    unsigned char iUsbPortNum;      // USB接口数
    unsigned char iComPortNum;      // 串口数
    unsigned char iParallelPortNum; // 并口口数
    unsigned char iReserved_NULL0;  // 空字节, 为了对齐
    unsigned short year;            // 编译日期: 年
    unsigned char  month;           // 编译日期: 月
    unsigned char  day;             // 编译日期: 日
    unsigned char iVerUser;         // BOIS用户版本号(软件)
    unsigned char iVerModify;       // BOIS修改版本号(软件)
    unsigned char iWebVersion[4];   // WEB版本
    unsigned char WebYear;          // 预留
    unsigned char WebMonth;         // 预留
    unsigned char WebDay;           // 预留
    unsigned char iDecodeChanNum;     // 本地解码(回放)路数
    unsigned char iSpeechInCaps;     // 对讲输入接口数量
    unsigned char iSpeechOutCaps;     // 对讲输出接口数量
    unsigned char iVGAPortNum;     // VGA接口数量
    unsigned char iTVOutPortNum;     // TV输出接口数量
    unsigned char iReserved_06;     // 预留
    unsigned char iReserved_07;     // 预留
}SYSATTR_T;
//!查询视频属性信息
typedef struct __sysattr_video_caps
{
    unsigned int iCifCaps;         // 大小
    unsigned int iTypeCaps;        // 编码
    unsigned int iFrameCaps;    // 帧率
    unsigned int iQualityCaps;    // 画质
    unsigned char iCifFrames;    // 每个芯片编码总帧数 单位：cif/s
    unsigned char _iRev_00;        // 保留
    unsigned char _iRev_01;        // 保留
    unsigned char _iRev_02;        // 保留
    unsigned int iReserved_01;    // 保留
    unsigned int iReserved_02;    // 保留
    unsigned int iReserved_03;    // 保留
    unsigned char iStandard;    // 制式
    unsigned char iRev_00;        // 保留
    unsigned char iRev_01;        // 保留
    unsigned char iRev_02;        // 保留
}SYSATTR_VIDEO_CAPS_T;


//!查询字符集属性信息
typedef struct __sys_string_support
{
    uchar  Version;        // 本配置的版本信息
    uint Type;           // 支持的类型， 掩码表示， 从低到高依表示：自定义|UTF-8
    uchar  iReserved_00;    // 预留
    uchar  iReserved_01;    // 预留
    uchar  iReserved_02;    // 预留
    uchar  iReserved_03;    // 预留
    uchar  iReserved_04;    // 预留
    uchar  iReserved_05;    // 预留
    uchar  iReserved_06;    // 预留
    uchar  iReserved_07;    // 预留
} SYS_STRING_SUPPORT;


/* 登陆属性 */
typedef struct __login_attribute
{
    /* 是否必须登陆设备方可使用， 0－要登陆，1－不必登陆 */
    uchar    iShouldLogin;
    /* 支持的登陆用户名最大长度 */
    uchar    iUNameMaxLen;
    /* 支持的密码最大长度 */
    uchar    iUPwdMaxLen;
    /* 支持的加密方式 */
    uchar    iEncryptType;/* 从低到高，按位表示加密方式：0-3des, */
    uchar  iReserved[28];/* 保留 */
}LOGIN_ATTRIBUTE;


//! 查询语音对讲属性
typedef struct __dialog_caps_t
{
    // 支持的音频数据个数的数量
    uchar iAudioTypeNum;        /*!< 是否支持语音对讲 */
    // 保留位
    uchar iRev[31];
} DIALOG_CAPS;

typedef struct __audio_attr_t
{
    // 支持的编码类型
    ushort iAudioType;            // 1:PCM, 2:G711a
    // 位
    ushort iAudioBit;            // 用实际的值表示， 如8位 则填值为8
    // 支持的采样率
    uint dwSampleRate;            // 用实际的值表示， 如8k 则填值为8000
    // 保留位
    uchar    iRev[64];
}AUDIO_ATTR_T;


/// 捕获辅助码流支持特性结构
typedef struct NET_CAPTURE_EXT_STREAM
{
    uint ExtraStream;                        ///< 用channel_t的位来表示支持的功能。
    uint CaptureSizeMask[64];    ///< 每一个值表示对应分辨率支持的辅助码流。
}NET_CAPTURE_EXT_STREAM;

enum EnDeviceCtrl
{
    OpStReboot = 1,
    OpStShutdown,
    OpStHdd,
    OpStTestDevice=5,
    OpSpringSnap=8,
    OpDelLog = 9,//日志清除
    OpStRecall = 10,    //恢复设备的默认设置　2007-11-28
};

CVNLogState::CVNLogState()
{
    m_pCaptureM = ICaptureManager::instance();
}

CVNLogState::~CVNLogState()
{
}


/*!
    \b Description        :    查询系统信息\n
*/
int CVNLogState::QuerySysInfo(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;
    switch(pReqPDU->m_pIpHdr->dvrip_p[0] )
    {
        /*设置属性信息*/
        case 0:
        {
            tracef("SYS_USE_INFO");
            SYS_USE_INFO useInfo;
            g_DriverManager.GetFSInfo(&useInfo);
            pRspPdu->packetBody((char *) &useInfo,  sizeof(SYS_USE_INFO));
            break;
        }        
        case 0x01:  /*设备属性信息*/
        {
            SYSATTR_T inform;
            memset(&inform, 0, sizeof(SYSATTR_T));

            int maj = 0;
            int min = 0;

            CVersion::instance()->GetVersion(&maj,&min);

            inform.iVerMaj = (unsigned char)maj;
            inform.iVerSub = (unsigned char)(min / 10);
            inform.iVideoInCaps = g_nCapture;

            if (g_CapsEx.HasAudioBoard)
            {
                // 特殊的设备，音频输入数量不能用视频通道来表示，两者不一样
                #ifdef ENC_SPECIAL_AUDIO_CHANNELS
                inform.iAudioInCaps = g_CapsEx.HasAudioBoard;
                #else
                inform.iAudioInCaps = g_nCapture;
                #endif
            }
            else
            {
                inform.iAudioInCaps = 0;
            }

            inform.iAlarmInCaps = g_nAlarmIn;
            inform.iAlarmOutCaps = g_nAlarmOut;
            inform.iUsbPortNum = 2;
            inform.iIdePortNum = 4;
            inform.iAetherNetPortNum = 0x01;
            inform.iComPortNum = 0x01;

            SYSTEM_TIME stBuildTime;
            /* use build time to represent */
            CVersion::instance()->GetBuildDate(&stBuildTime);
            inform.year  = (unsigned short)stBuildTime.year;
            inform.month = (unsigned char)stBuildTime.month;
            inform.day   = (unsigned char)stBuildTime.day;
            pRspPdu->packetBody((char *) &inform,  sizeof(SYSATTR_T));
            break;
        }
        //!查询视频属性
        case 0x04:
        {
            SYSATTR_VIDEO_CAPS_T VideoInfo;
            memset(&VideoInfo, 0, sizeof(VideoInfo));

            CAPTURE_CAPS tmpCapCaps;
            memset((void*)&tmpCapCaps,0,sizeof(CAPTURE_CAPS));
            GetCaps(&tmpCapCaps);

            VideoInfo.iCifCaps    = tmpCapCaps.ImageSize;
            VideoInfo.iTypeCaps   = tmpCapCaps.Compression;
            VideoInfo.iFrameCaps  = 0xff;
            VideoInfo.iQualityCaps= 0x3f;
            VideoInfo.iCifFrames  = 100;//25*4
            VideoInfo.iStandard   = CConfigLocation::getLatest().iVideoFormat;

            pRspPdu->packetBody((char *) &VideoInfo,  sizeof(SYSATTR_VIDEO_CAPS_T));
            break;
           }        
        case 0x05:
        {
            SYS_STRING_SUPPORT encoding;
            memset(&encoding, 0, sizeof(encoding));
            encoding.Type = 0x00000001;//ENCODING_UTF8 = 0x00000001,
            pRspPdu->packetBody((char *) &encoding,  sizeof(SYS_STRING_SUPPORT));
            break; 
        }      
        case 0x09:
        {
            LOGIN_ATTRIBUTE login_attribute_tmp;
            memset(&login_attribute_tmp, 0, sizeof(login_attribute_tmp));

            login_attribute_tmp.iShouldLogin = 0;
            login_attribute_tmp.iUNameMaxLen = 8;
            login_attribute_tmp.iUPwdMaxLen = 8;
            login_attribute_tmp.iEncryptType = 0;
            pRspPdu->packetBody((char *) &login_attribute_tmp,  sizeof(LOGIN_ATTRIBUTE));
            break; 
        }     
        case 0x0e:
        {
            tracef("MOTION_DETECT_CAPS");
            MOTION_DETECT_CAPS   detectCapInfo;
            memset(&detectCapInfo,0,sizeof(MOTION_DETECT_CAPS));

            MotionDetectGetCaps(&detectCapInfo);
            pRspPdu->packetBody((char *) &detectCapInfo,  sizeof(MOTION_DETECT_CAPS));

            break;
        }   
        //!查询系统语言集信息
        case 0x14:
        {
                
            int iLMask = (int)AppConfig::instance()->getNumber("Global.Language", 0x87);
            int iDefaultLmask = (int)AppConfig::instance()->getNumber("Global.LanguageDefault", ENGLISH);
            ENUM_MAP emLanguage[] =
            {
                {"English", ENGLISH},
                {"SimpChinese", CHINESE_S},
                {"TradChinese", CHINESE_T},
                {"Italian", ITALIAN},
                {"Spanish", SPANISH},
                {"Japanese", JAPANESE},
                {"Russian", RUSSIAN},
                {"French", FRENCH},
                {"German", GERMAN},
                {"Portugal",PORTUGAL},
                {"Turkey",TURKEY},
                {"Poland",POLAND},
                {"Romanian",ROMANIAN},
                {"Hungarian",HUNGARIAN},
                {"Finnish",FINNISH},
                {"Estonian",ESTONIAN},
                {"Korean",KOREAN},
                {"Farsi",FARSI},
                {"Dansk",DANSK},
	         {"Bulgaria",BULGARIA},
	         {"Arabic",ARABIC},	     
                	{"HEBREW",HEBREW},
                {NULL,    },
            };

            unsigned int iFlag = 0x0001;
            std::string szBuffer;

            for(int k = 0; k < LANG_NR; k++)
            {
                if(iLMask &(iFlag<<k))
                {
                    if(0 != k)
                    {
                        szBuffer.append("|");
                    }
                    szBuffer.append(emLanguage[k].name);
                }
            }

            if(iDefaultLmask >= 0 && iDefaultLmask < LANG_NR)
            {
                szBuffer.append("&&");
                szBuffer.append(emLanguage[iDefaultLmask].name);
            }

            tracef("support language:\n%s\n",szBuffer.c_str());

            pRspPdu->packetBody((char *) szBuffer.c_str(),  szBuffer.length());

            break;
           }    
        case 0x1a://设备功能集
        {
            if( pReqPDU->m_pIpHdr->dvrip_r1 == 1 )
            {
                AckAblityTlv(pReqPDU, pRespPDU);
            }
            else
            {
                std::string funcs;
                //AutoMaintain
                funcs.append("AutoMaintain:1:Reboot,DeleteFiles");
                //FTP
                #ifdef FUNC_FTP_UPLOAD
                funcs.append("&&");
                funcs.append("FTP:1:Record,Snap");
                #endif
                //SMTP
                #ifdef _USE_SMTP_MODULE
                funcs.append("&&");
                funcs.append("SMTP:1:AlarmText,AlarmSnap");
                #endif
                //NTP
                #ifdef USING_PROTOCOL_NTP
                funcs.append("&&");
                funcs.append("NTP:2:AdjustSysTime");
                #endif
                //VideoCover
                #ifdef MULTI_COVER
                funcs.append("&&");
                funcs.append("VideoCover:1:MutiCover");
                #endif
                #ifdef _NET_USE_DHCPCLIENT
                funcs.append("&&");
                funcs.append("DHCP:1:RequestIP");
                #endif

                tracef("Support Functions :\n%s\n",funcs.c_str());
                pRspPdu->packetBody((char *) funcs.c_str(),  funcs.length());
            }
            break;
        }
        case 0x1d:
        {
            //OEM厂商和类型定义
            #if defined(OEM_General_VENDOR)
            #define    OEM_VENDOR_NAME        "General"
            #define    OEM_TYPE_NAME            "<NULL>"
            #else
            #define    OEM_VENDOR_NAME        "<NULL>"
            #define    OEM_TYPE_NAME            "<NULL>"
            #endif

            std::string strOem;
            strOem = OEM_VENDOR_NAME;
            strOem +=  "::" ;
            strOem += OEM_TYPE_NAME;
            pRspPdu->packetBody((char *) strOem.c_str(),  strOem.length());

            break;
        }        
        case 0x1e:   /* 查询网络状态信息，如登录了哪些用户*/
        {
            break;
        }
        default:
        {
            pRspPdu->packetBody();
            pRspPdu->m_pIpHdr->dvrip_p[8] = 2;
            break;
        }
        
    }
       
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_INFO_SYSTM;
    pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0] ;

    return 0;
}

int CVNLogState::CtrlRecord(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    //isValidAuthority("RecordMode")
    if (!pReqPDU)
    {
        return -1;
    }    

    int i =0;
    uint recmask = 0;
    if (pReqPDU->m_pIpHdr->dvrip_p[20] == 0)
    {
        // 控制录像
        for (i=0; i < g_nCapture; i++)
        {
            if (pReqPDU->m_pIpHdr->dvrip_p[i])
            {
                recmask |= pReqPDU->m_pIpHdr->dvrip_p[i] << (2 * i);
            }
        }
        g_Record.SaveRecordControl(recmask, TRUE);
    }

    pReqPDU->m_pIpHdr->dvrip_p[0] = 1;/*传递到查询报警录像状态函数处理，私有转化*/

    //应答处理
    return QueryAlarmRecordState(pReqPDU, pRespPDU);
}

int CVNLogState::QueryAlarmRecordState(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;
    switch(pReqPDU->m_pIpHdr->dvrip_p[0] )
    {
        case 1:    /*查询录像工作及报警状态*/
        {
            char strRecordStatus[32]={0};
            uint allStatus = g_Record.LoadRecordControl();
            for (int i = 0; i < g_nCapture; i++)
            {
                // 取得通道i的录像状态值, 每二位表示一个通道, iAllStatus最多表示16通道
                strRecordStatus[i] = ((allStatus & (0x3 << 2 * i)) >> (2 * i));
            }    
            pRspPdu->packetBody(strRecordStatus, g_nCapture);
            
            break;
        }
        case 3:    /*查询扩展报警状态*/
        {
            char extAlarm[N_PTZ_ALARM] = { 0 };
            for (int i = 0; i < N_PTZ_ALARM; i++)
            {
                extAlarm[i] = (uchar)(g_Alarm.GetAlarmPtzState(i) & 0xFF);
            }
            pRspPdu->packetBody(extAlarm, N_PTZ_ALARM);                
            break;
        }
        default:
        {
            pRspPdu->packetBody();
            break;
        }
    }
        

    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_STAT_QUERY;
    pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];

    uint iStatues;
    iStatues = g_Record.GetState(0);
    memcpy(&pRspPdu->m_pIpHdr->dvrip_p[1], &iStatues, sizeof(uint) );

    pRspPdu->m_pIpHdr->dvrip_p[5]=0xff;

    //外部报警
    if (g_Alarm.GetAlarmState())
    //if (m_iAlarmState[appEventAlarmLocal])
    {
        pRspPdu->m_pIpHdr->dvrip_p[5] = pRspPdu->m_pIpHdr->dvrip_p[5] & (~(1<<0));
        iStatues = g_Alarm.GetAlarmState();
        //iStatues = m_iAlarmState[appEventAlarmLocal];
        memcpy(&pRspPdu->m_pIpHdr->dvrip_p[8], &iStatues, sizeof(uint) );
    }

    //视频丢失
    if (g_Alarm.GetLossDetectState())
    //if (m_iAlarmState[appEventVideoLoss])
    {
        pRspPdu->m_pIpHdr->dvrip_p[5] = pRspPdu->m_pIpHdr->dvrip_p[5] & (~(1<<1));
        iStatues = g_Alarm.GetLossDetectState();
        //iStatues = m_iAlarmState[appEventVideoLoss];

        memcpy(&pRspPdu->m_pIpHdr->dvrip_p[12], &iStatues, sizeof(uint) );
    }

    //动态检测
    if (g_Alarm.GetMotionDetectState())
    //if (m_iAlarmState[appEventVideoMotion])
    {
        pRspPdu->m_pIpHdr->dvrip_p[5] = pRspPdu->m_pIpHdr->dvrip_p[5]& (~(1<<2));
        iStatues = g_Alarm.GetMotionDetectState();
        //iStatues = m_iAlarmState[appEventVideoMotion];
        printf ("[%s][%d]GetMotionDetectState iStatus= %d\n", __FILE__, __LINE__, iStatues);

        memcpy(&pRspPdu->m_pIpHdr->dvrip_p[16], &iStatues, sizeof(uint) );
    }

    //由于在g_Comm.GetCommState()做了清除操作，要用值保存一下
    uchar comstate = (uchar)g_Comm.GetCommState();
    if (comstate)
    {
        pRspPdu->m_pIpHdr->dvrip_p[5] = pRspPdu->m_pIpHdr->dvrip_p[5]& (~(1<<4));
        pRspPdu->m_pIpHdr->dvrip_p[20] = comstate;
    }

    return 0;
}

int CVNLogState::QueryDevice(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;

    switch(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        case 0:
        {
            char strStatus[4+16+16]={0};
            strStatus[0]=0;
            strStatus[1]=g_nAlarmIn;
            strStatus[2]=g_nAlarmOut;
            int iPos      =  3;
            for(int iTmp=0; iTmp<g_nAlarmIn; iTmp++)
            {
                int iAlarmStatus = (BITMSK(iTmp)&g_Alarm.GetAlarmState());
                if(0 != iAlarmStatus)
                {
                    iAlarmStatus = 0x01;
                }
                else
                {
                    iAlarmStatus = 0x00;
                }
                strStatus[iPos] = iAlarmStatus;
                iPos++;
            }
            for(int iTmp=0; iTmp<g_nAlarmOut; iTmp++)
            {
                int iAlarmStatus = (BITMSK(iTmp)&g_Alarm.GetAlarmOut());
                if(0 != iAlarmStatus)
                {
                    iAlarmStatus = 0x01;
                }
                else
                {
                    iAlarmStatus = 0x00;
                }
                strStatus[iPos] = iAlarmStatus;
                iPos++;
            }
            strStatus[iPos]=0;
            pRspPdu->packetBody(strStatus, 4+g_nAlarmIn+g_nAlarmOut);
            break;
        }
        case 0x01:
        {
            int iChannel = pReqPDU->m_pIpHdr->dvrip_p[1];
            if ( iChannel >= 0 && iChannel < g_nCapture)
            {
                pRspPdu->packetBody();
                pRspPdu->m_pIpHdr->dvrip_p[1] = iChannel;
                pRspPdu->m_pIpHdr->dvrip_p[12] = 0x01;
                pRspPdu->m_pIpHdr->dvrip_p[13] = 0x01;
                if((BITMSK(iChannel)&(g_Alarm.GetLossDetectState())))
                {
                    pRspPdu->m_pIpHdr->dvrip_p[14] = 0x01;
                }
                pRspPdu->m_pIpHdr->dvrip_p[15] = 0x00;
            }
            else
            {
                char strStatus[16+16]={0};
                for(int iTmp=0; iTmp<g_nCapture; iTmp++)
                {
                    if((BITMSK(iTmp)&(g_Alarm.GetLossDetectState())))
                    {
                        strStatus[iTmp] = 0x01;
                    }
                }
        
                pRspPdu->packetBody(strStatus, g_nCapture+g_nCapture);
                pRspPdu->m_pIpHdr->dvrip_p[1] = pReqPDU->m_pIpHdr->dvrip_p[1];
                pRspPdu->m_pIpHdr->dvrip_p[12] = g_nCapture;
                pRspPdu->m_pIpHdr->dvrip_p[13] = g_nCapture;
            }
            
            break;
        }
        default:
        {
            pRspPdu->packetBody();
            break;
        }
    }

    pRspPdu->m_pIpHdr->dvrip_p[0] = pReqPDU->m_pIpHdr->dvrip_p[0];
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_DEVICE_QUERY;

    return 0;
}

int CVNLogState::QuerySystemOpr(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;

    bool bSuccess = false;
    int iChannel = pReqPDU->m_pIpHdr->dvrip_p[0];

    if(pReqPDU->m_pIpHdr->dvrip_p[1] == 0x00)
    {
        trace("**********OLD OPT QUERY**********\n");
        if(iChannel >= 0 && iChannel < g_nCapture)/*单通道查询*/
        {
            CAPTURE_CAPS tmpCapCaps;
            memset((void*)&tmpCapCaps,0,sizeof(CAPTURE_CAPS));/*得到选项信息*/
            bSuccess = (GetCaps(&tmpCapCaps))?true:false;

            char strBuff[8]={0};
            memcpy((char*)strBuff, (uchar *)&tmpCapCaps.Compression, 4*sizeof(char));
            memcpy((char*)(strBuff+4), (uchar *)&tmpCapCaps.ImageSize, 4*sizeof(char));

            pRspPdu->packetBody(strBuff, 8);

        }
    }
    else if(pReqPDU->m_pIpHdr->dvrip_p[1] == 0x01)
    {
        tracef("**********NEW OPT QUERY**********\n");
        CAPTURE_CAPS tmpCapCaps;
        NET_CAPTURE_EXT_STREAM CaptureStream;
        memset((void*)&tmpCapCaps, 0, sizeof(CAPTURE_CAPS));
        memset(&CaptureStream, 0, sizeof(NET_CAPTURE_EXT_STREAM));
        bSuccess = (GetCaps(&tmpCapCaps) && GetExtCaps((CAPTURE_EXT_STREAM *)&CaptureStream))?true:false;//获取压缩模式
        if(!bSuccess)
        {
            tracef("CaptureGetExtCaps Failed1\n");
            pRspPdu->packetBody();
        }
        else
        {
            pRspPdu->packetBody((char *)&CaptureStream, sizeof(NET_CAPTURE_EXT_STREAM));
            pRspPdu->m_pIpHdr->dvrip_p[7] = 1;
            memcpy((uchar *)(&pRspPdu->m_pIpHdr->dvrip_p[8]),(uchar *)&tmpCapCaps.Compression,4*sizeof(char));
        }

    }
    
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_SYSTEM_OPTQUERY;
    pRspPdu->m_pIpHdr->dvrip_p[2] = pReqPDU->m_pIpHdr->dvrip_p[0];/*通道号*/
    pRspPdu->m_pIpHdr->dvrip_p[3] = pReqPDU->m_pIpHdr->dvrip_p[1];//查询选项:0x00 查询旧的 0x01 新的查询
    pRspPdu->m_pIpHdr->dvrip_p[0] = (bSuccess) ? 0:1;/*判断操作是否成功*/
    return 0;
}

int CVNLogState::QueryLogs(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }

    pRespPDU = pRspPdu;
    
    if (0x01 != pReqPDU->m_pIpHdr->dvrip_p[1] )
    {
        tracef("QueryLogs param from client is wrong!\n");
        pRspPdu->packetBody();
        pRspPdu->m_pIpHdr->dvrip_cmd = ACK_LOGS_PARAM;        
        return 0;
    }

    uint iTotalItem = g_Log.GetCount();
    uint iTotalLen = iTotalItem * sizeof(struct LOG_ITEM);

    if (iTotalItem == 0)
    {
        pRspPdu->packetBody();
        pRspPdu->m_pIpHdr->dvrip_cmd = ACK_LOGS_PARAM;    
        return 0;
    }
    else if (iTotalItem >= 1000)
    {
        iTotalItem = 1000;
        iTotalLen = iTotalItem * sizeof(struct LOG_ITEM);
    }

    CPacket* pPacker = g_PacketManager.GetPacket(iTotalLen);
    if ( !pPacker)
    {
        pRspPdu->packetBody();
        pRspPdu->m_pIpHdr->dvrip_cmd = ACK_LOGS_PARAM;    
        return 0;        
    }
    
    char* pBuffer = (char* )pPacker->GetBuffer();    
    int iCount = 0;
    for (ushort ii=0; ii<iTotalItem; ii++)
    {
        struct LOG_ITEM* pLogs= (struct LOG_ITEM*)(pBuffer+ii*sizeof(struct LOG_ITEM));
        memset(pLogs, 0, sizeof(struct LOG_ITEM));
            
        if (1 != g_Log.GetItem(ii, pLogs, 1))
        {
            break;
        }
        iCount++;
    }

    iTotalLen = iCount * sizeof(struct LOG_ITEM);
    pRspPdu->packetBody(pBuffer, iTotalLen);
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_LOGS_PARAM;
    memcpy(&pRspPdu->m_pIpHdr->dvrip_p[0], &iTotalLen, 4);
    
    pPacker->Release();

    return 0;
}

int CVNLogState::CtrlDevice(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    int iRet = 0;
    pRspPdu->packetBody();
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_DEVICE_CTRL;    
    pRespPDU = pRspPdu;

    pRspPdu->m_pIpHdr->dvrip_p[0]    =  pReqPDU->m_pIpHdr->dvrip_p[0];
    pRspPdu->m_pIpHdr->dvrip_p[4]    =  pReqPDU->m_pIpHdr->dvrip_p[4];    
    pRspPdu->m_pIpHdr->dvrip_p[8]    =  pReqPDU->m_pIpHdr->dvrip_p[8];    

    //CUser* pUser = (CUser* )(((struct conn* )pReqPDU->m_pConn)->context);
#ifdef __BELL_QZTEL__
	SesionInfo_t *sess = (SesionInfo_t *)(((struct conn* )pReqPDU->m_pConn)->context);
	CUser *pUser = (CUser*)sess->user;
#else	
	CUser* pUser = (CUser* )(((struct conn* )pReqPDU->m_pConn)->context);
#endif
    switch(pReqPDU->m_pIpHdr->dvrip_p[0])
    {
        case OpStReboot:
        case OpStShutdown://设备重启，设备关闭控制
        {
            if(pUser && pUser->isValidAuthority("ShutDown"))
            {
                pRspPdu->m_pIpHdr->dvrip_p[4] = 0;
                if (pRspPdu->m_pIpHdr->dvrip_p[0] == 0x01)
                {
                    //此处需要考虑先把数据发送出去后再重新启动
                    //g_Challenger.Reboot();
                    iRet = CONFIG_APPLY_REBOOT; 
                }
            }
            else
            {
                pRspPdu->m_pIpHdr->dvrip_p[4] = 2;
            }
            break;
        }
        case OpStHdd:       //硬盘控制
        {
//对硬盘操作前，先关闭录像
//add by ilena zhou 2009-11-03 临时解决问题: 如果只有一块硬盘挂在端口2上，客户端无法控制硬盘类型
            IDE_INFO64 harddriver_tmp;
            g_DriverManager.GetIdeInfo(&harddriver_tmp);
            int ideNum = harddriver_tmp.ide_num;
            int iSubDev = pReqPDU->m_pIpHdr->dvrip_p[4];
            if(ideNum <= iSubDev)
            {
                iSubDev--;
            }
//add end
            if(!g_Record.IsThreadOver())
            {
                g_Record.Stop();
            }
            
            switch(pReqPDU->m_pIpHdr->dvrip_p[8])//具体操作码
            {
                case 0://清楚数据
                if (g_DriverManager.FormatDisk(iSubDev))
                {
                    g_Log.Append(LOG_HDD_FORMAT, iSubDev);
                }
                break;
                case 1://设置为读写盘
                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_WRITE);
                break;
                case 2://设置为只读盘
                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_READ_ONLY);
                break;
                case 3://设置为冗余盘
                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_REDUNDANT);
                break;
                case 4://恢复错误
                g_DriverManager.ParoleDriver(iSubDev,0);
                break;
                case 5://设置为快照盘
                g_DriverManager.SetDriverType(iSubDev,0,DRIVER_SNAPSHOT);
                break;
            }

            g_DriverManager.IsSnapExist();
            pRspPdu->m_pIpHdr->dvrip_p[4]= 0; //操作成功,返回码
            break;
        }
        case OpStRecall:
        {
            //isValidAuthority("DefaultConfig")
            char* pBuff = (char*)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
            unsigned long dwFlags = 0;
            memcpy(&dwFlags , pBuff, sizeof(unsigned long));

            for (int k = 31; k >= 0; k--)
            {
                if (dwFlags & BITMSK(k))
                {
                    iRet |= g_Config.SetDefaultConfig(k);
			
                    tracef("OnRecallCfg::ret = %d k = %d \n", iRet, k);

                    //恢复全部
                    if (31 == k)
                    {
                        break;
                    }
                }
            }
            
            //恢复默认失败
            if ((iRet & CONFIG_APPLY_FILE_ERROR) || (iRet & CONFIG_APPLY_CAPS_ERROR)
                || (iRet & CONFIG_APPLY_VALIT_ERROR) || (iRet & CONFIG_APPLY_NOT_EXSIST))
            {
                pRspPdu->m_pIpHdr->dvrip_p[4] = 2;//操作失败,返回码
            }
            else
            {
                pRspPdu->m_pIpHdr->dvrip_p[4] = 0;//操作成功,返回码
            }

            //写文件
            g_Config.saveFile();
            
            if((iRet & CONFIG_APPLY_REBOOT) || (iRet & CONFIG_APPLY_RESTART))
            {
                //g_Challenger.Reboot();
                iRet = CONFIG_APPLY_REBOOT;
            }
            break;
        }
        case OpDelLog:
        {
            //CUser *user = (CUser *)(((struct SesionInfo*)(m_OprConn->context))->user);
            //g_Log.Clear(user->getName().c_str());
            break;
        }
        default:
        pRspPdu->m_pIpHdr->dvrip_p[2] = 2;

        break;
    }

    return iRet;
}




//结构信息
typedef struct _CARDINFO                
{
    unsigned char    channel        :4;                //    卡号
    unsigned char    video_audio    :2;                //    音/视频  01:音频,10:视频    
    unsigned char    alarm        :2;                //    报警     普通/报警/动态检测报警/媒体文件中断(0/1/2/3)
}
#ifdef LINUX
__attribute__ ((packed)) 
#endif 
CARDINFO;

//文件信息结构,网络使用
typedef struct _DIRINFO
{
    CARDINFO    DirInfo ;
    DHTIME        DirStartTime ;
    DHTIME        DirEndTime ;
    unsigned int    FileSize ;
    unsigned int    StartCluster ;
    unsigned char    DriveNo ;
    unsigned char    Hint;
    unsigned char PartNo;
}DIRINFO;

typedef struct _CARDDIRINFO
{
    uchar        SelfSize;              //本结构自身大小
    uchar        DriveNo ;
    uchar        Hint;
    uchar        SpecialUse;          //这个对应于硬盘下载器使用
    DHTIME        DirStartTime ;
    DHTIME        DirEndTime ;
    uint        FileSize ;            //文件内容大小
    uint        FileSpace ;          // 文件占用磁盘空间
    uint        StartCluster ;
    uchar        CardNo[20];
    CARDINFO    DirInfo ;
    uchar        Reserved[32];         //扩展用
}CARDDIRINFO;

int CVNLogState::QueryRecordList(struct conn *c, CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        printf("CVNLogState::QueryRecordList():pReqPDU= NULL\r\n");
        return -1;
    }    

    if(NULL == c)
    {
        printf("CVNLogState::QueryRecordList():c= NULL\r\n");
        return -1;
    }
    
    //应答
    CIVnPdu* pRspPdu = new CIVnPdu();
    if(!pRspPdu)
    {
        return -1;
    }
    pRespPDU = pRspPdu;
    
    FFFile dhFile;
    uint chan, ItemNum, type;
    SYSTEM_TIME start_time;
    SYSTEM_TIME end_time;
    memset(&start_time, 0, sizeof(SYSTEM_TIME));
    memset(&end_time, 0xff, sizeof(SYSTEM_TIME));

    chan = pReqPDU->m_pIpHdr->dvrip_p[0];
    chan = (chan==ALL_CHANNELS) ? chan : (chan-1);

    memcpy((uchar*)&start_time.year, (uchar*)&pReqPDU->m_pIpHdr->dvrip_p[1], 2); // year
    start_time.month = pReqPDU->m_pIpHdr->dvrip_p[3];
    start_time.day = pReqPDU->m_pIpHdr->dvrip_p[4];
    start_time.hour = pReqPDU->m_pIpHdr->dvrip_p[5];
    start_time.minute  = pReqPDU->m_pIpHdr->dvrip_p[6];
    start_time.second  = pReqPDU->m_pIpHdr->dvrip_p[7];
    uchar hint = pReqPDU->m_pIpHdr->dvrip_p[9];

    //if(6 == pReqPDU->m_pIpHdr->dvrip_p[8])
    {
        if((uint)7 == pReqPDU->m_pIpHdr->dvrip_extlen)
        {
            memset(&end_time, 0, sizeof(SYSTEM_TIME));/*Must be cleared!!*/
            char* pTmp = (char *)(pReqPDU->GetPacket()+DVRIP_HEAD_T_SIZE);
            
            memcpy((uchar*)&(end_time.year), (uchar*)pTmp, 2); // year
            end_time.month   = (uchar)*(pTmp + 2);
            end_time.day     = (uchar)*(pTmp + 3);
            end_time.hour    = (uchar)*(pTmp + 4);
            end_time.minute  = (uchar)*(pTmp + 5);
            end_time.second  = (uchar)*(pTmp + 6);
        }
    }

    // 查询所有录像类型为0, 外部报警1, 动态检测2, 所有报警3 4- 按卡号查询
    type = 0;
    switch(pReqPDU->m_pIpHdr->dvrip_p[8])
    {
        case 1:
        {
            type = (1<<F_ALERT);
            break;
        }
        case 2: // 动态检测
        {
            type = (1<<F_DYNAMIC);
            break;
        }
        case 3: // 所有报警
        {
            type  = (1<<F_ALERT);
            type |= (1<<F_DYNAMIC);
            break;
        }
        case 4:    // 手动录像,刘阳提供
        {
            type |= (1<<F_HAND);
            break;
        }
        case 5: // 交易类型
        {
            type = (1<<F_CARD);
            break;
        }    
        case 0:
        case 6:
        default:
        {
            type  = (1<<F_COMMON);
            type |= (1<<F_ALERT);
            type |= (1<<F_DYNAMIC);
            type |= (1<<F_CARD);
            type |= (1<<F_HAND);

            break;
        }
    } // switch


    // 卡号长度, 文件系统定为20
    ItemNum = MAX_FILE_NUM_ONCE;    //网络查询，一次最多16个文件
    uchar pdat[sizeof(FILE_INFO) * MAX_FILE_NUM_ONCE];
    memset(pdat, 0, sizeof(pdat));
    
    uchar driver_type = pReqPDU->m_pIpHdr->dvrip_p[17];
    if (driver_type == 0)
    {
        driver_type = NORMAL_DRIVER_TYPE;
    }else if(1 == driver_type)
    {
        driver_type = SNAP_DRIVER_TYPE;
    }
#ifndef SYS_NO_RECORD    



    if (chan==ALL_CHANNELS)
    {
        /* 目前不会进入这个逻辑 */
        dhFile.GetList(chan, &start_time, &end_time, type, &ItemNum, (FILE_INFO *)pdat, hint,driver_type);
    }
    else
    {
        /* 目前錠錝100 都是按通道一个一个传过来的，没有统一查所有通道的方式 */
        char auth[32];
        sprintf(auth, "Replay_%02d",  chan + 1);
        //CUser* pUser  = (CUser* )c->context;
    #ifdef __BELL_QZTEL__
		SesionInfo_t *sess = (SesionInfo_t *)c->context;
		CUser *pUser = (CUser*)sess->user;
	#else	
		CUser *pUser = (CUser*)c->context;
	#endif
        if(pUser && pUser->isValidAuthority(auth))
        {
            dhFile.GetList(chan, &start_time, &end_time, type, &ItemNum, (FILE_INFO *)pdat, hint,driver_type);
        }
        else
        {
            ItemNum = 0;
            tracef("Channel [%d] No Authority!\n",chan);
        }
    }
        
#else
    ItemNum = 0;
#endif

    if((6 == pReqPDU->m_pIpHdr->dvrip_p[8]) && (0 < ItemNum))
    {
        DHTIME      dhtmStart;
        DHTIME      dhtmEnd;

        dhFile.Open();
   //     tracef("Searched Files:%d\n",ItemNum);
        timesys2dh(&dhtmStart,&start_time);
        timesys2dh(&dhtmEnd,&end_time);

#if 0 
        if(!dhFile.Normalize((FILE_INFO *)(pdat) + (ItemNum -1),dhtmStart,dhtmEnd))
        {
            tracef("Truncted NO.%d error!!!!!!!\n",ItemNum);
        }        
        if(!dhFile.Normalize((FILE_INFO *)(pdat),dhtmStart,dhtmEnd))/*根据开始时间截取首段录像*/
        {
            tracef("Truncted NO.%d error!!!!!!!\n",ItemNum);
        }
#endif
        dhFile.Close();
    }

    // 记录日志
    LOG_ITEM_RECORD lirSearch;
    lirSearch.chan = chan+1;
    lirSearch.type = type;
    timesys2dh(&lirSearch.time, &start_time);
    g_Log.Append(LOG_SEARCH, 0, &lirSearch);

    FILE_INFO* pfile = (FILE_INFO*)pdat;
    DIRINFO tDirInfo;
    CARDDIRINFO tCarInfo;

    if ( ItemNum > 0)
    {
        int iTotalLen = 0;
        
        if (15 == pReqPDU->m_pIpHdr->dvrip_p[8])
        {
            iTotalLen = ItemNum * sizeof(CARDDIRINFO);
        }
        else
        {
            iTotalLen = ItemNum * sizeof(DIRINFO);
        }
        int iHint = (pfile + ItemNum - 1)->hint;
        
        CPacket* pPacker = g_PacketManager.GetPacket(iTotalLen);
        char* pTmp = (char *)pPacker->GetBuffer();
        int iCount = 0;
        
        if (15 == pReqPDU->m_pIpHdr->dvrip_p[8])
        {
            
            while(ItemNum > 0)
            {
                memset(&tCarInfo, 0, sizeof(CARDDIRINFO));
                tCarInfo.DirInfo.channel = pfile->channel;
                tCarInfo.DirInfo.video_audio = pfile->video_audio;
                tCarInfo.DirInfo.alarm = pfile->alarm;

                memcpy(&tCarInfo.DirStartTime, &pfile->start_time, sizeof(DHTIME));
                memcpy(&tCarInfo.DirEndTime, &pfile->end_time, sizeof(DHTIME));
                
                tracef("endtim %02d:%02d:%02d\n", tCarInfo.DirEndTime.hour, tCarInfo.DirEndTime.minute, tCarInfo.DirEndTime.second);
                
                tCarInfo.FileSize = pfile->file_length;
                tCarInfo.StartCluster = pfile->first_clus_no;

                tCarInfo.DriveNo = pfile->disk_no;


                // 精确定位参数
                tCarInfo.Hint = pfile->hint;
                pfile++;
                ItemNum--;
                memcpy((char *)(pTmp+iCount*sizeof(CARDDIRINFO)), &tCarInfo, sizeof(CARDDIRINFO));
                iCount++;
            }
            
        }
        else
        {
            while(ItemNum > 0)
            {
                memset( &tDirInfo, 0, sizeof(DIRINFO) );

                tDirInfo.DirInfo.channel = pfile->channel;
                tDirInfo.DirInfo.video_audio = pfile->video_audio;
                
                if(1 == pReqPDU->m_pIpHdr->dvrip_p[17])
                {
                    tDirInfo.DirInfo.alarm = 3;  //图片文件
                }else
                {
                    tDirInfo.DirInfo.alarm = pfile->alarm;
                }
                
                memcpy(&tDirInfo.DirStartTime, &pfile->start_time, sizeof(DHTIME));
                memcpy(&tDirInfo.DirEndTime, &pfile->end_time, sizeof(DHTIME));
                tDirInfo.FileSize = pfile->file_length;
                tDirInfo.StartCluster = pfile->first_clus_no;
                tDirInfo.PartNo = pfile->disk_part;
                tDirInfo.DriveNo = pfile->disk_no;

                // 精确定位参数
                tDirInfo.Hint = pfile->hint;
                pfile ++;
                ItemNum--;

                memcpy((char *)(pTmp+iCount*sizeof(DIRINFO)), &tDirInfo, sizeof(DIRINFO));
                iCount++;
            }
            
        }    
        pRspPdu->packetBody(pTmp, iTotalLen);
        pRspPdu->m_pIpHdr->dvrip_p[9] = iHint;
        pPacker->Release();
        
    }
    else
    {
        pRspPdu->packetBody();
    }


    pRspPdu->m_pIpHdr->dvrip_p[8] = pReqPDU->m_pIpHdr->dvrip_p[8];
    pRspPdu->m_pIpHdr->dvrip_p[17] = pReqPDU->m_pIpHdr->dvrip_p[17];
    pRspPdu->m_pIpHdr->dvrip_cmd = ACK_FILE_RECRD;
    return 0;
}
int CVNLogState::QueryAlarmRecord(struct conn *c,CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    if (!pReqPDU)
    {
        return -1;
    }    
    
    pReqPDU->m_pIpHdr->dvrip_cmd = REQ_RECD_QUERY;
    if (0 == pReqPDU->m_pIpHdr->dvrip_p[8])
    {
        pReqPDU->m_pIpHdr->dvrip_p[8] = 3;
    }

    return QueryRecordList(c,pReqPDU, pRespPDU);
}


void GetAblility(CPacket*& pPacket)
{
    unsigned char * p = NULL;
    int Len_p = 0;
    //int ablity = 0;
    int total_len = 8 + sizeof(DECODE_ABILITY) + 8 + sizeof(AUTO_REGISTER_ABILITY)
                    + 8 + sizeof(PTZ_TOUR_ABILITY) + 8 + sizeof(SPLIT_MODE)
                    + 8*(SPLIT_NR+sizeof(SPLIT_COMBINE) ) + 8 + sizeof(ABILITY_SNAP_T)
                    + 8 + sizeof(ABILITY_AlarmLink) + 8 + sizeof(TALK_ABILITY)
                    + 8 + sizeof(IVIDEO_ABILITY) + 8 + sizeof(ENCODE_CAPS_NET)
                    + 8 + sizeof(TIME_ABILITY) + 8 + sizeof(SAVIA_ABILITY)
                    + 8 + sizeof(FTP_ABILITY) + 8 + sizeof(BLACKWHITEIP_ABILITY)
                    + 8 + sizeof(RTSP_ABILITY) + 8 + sizeof(AUDIO_ABILITY)
                    + 8 + sizeof(KBD_ABILITY) + 8 + sizeof(OSD_ZOOM_ABILITY)+sizeof(ENCODE_CAPS_NET2)
                    + 8 + 32;//sizeof(COLOR_ABILITY);
	

	total_len += (8 + sizeof(NEW_NET_OPR_ABILITY));

#if defined (_USE_720P_MODULE_) 
	total_len += (8 + sizeof(VIDEOIN_CAPS));
#endif

    pPacket = g_PacketManager.GetPacket(total_len);

    {
        PTZ_TOUR_ABILITY ability;
        memset( &ability, 0, sizeof(PTZ_TOUR_ABILITY) );
        ability.usPresetNums = PTZ_PRESETNUM;
        ability.usTourNums = PTZ_CHANNELS;

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_PTZ_TOUR);
        tlv.SetValue( (uchar*)&ability, sizeof(PTZ_TOUR_ABILITY), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

    /* 黑白名单能力集 */
    {
        BLACKWHITEIP_ABILITY ability;
        memset( &ability, 0, sizeof(BLACKWHITEIP_ABILITY) );
        ability.usMaxBlackNums = MAX_FILTERIP_NUM;
        ability.usMaxWhiteNums = MAX_FILTERIP_NUM;
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_DEV_BLACKWHITEIP);
        tlv.SetValue( (uchar*)&ability, sizeof(BLACKWHITEIP_ABILITY),false);
        tlv.Pack(p, Len_p);
        pPacket->PutBuffer(p, Len_p);
    }
#ifdef ENC_SUPPORT_SNAPSHOT
    {
        ABILITY_SNAP_T ability;
        memset( &ability, 0, sizeof(ABILITY_SNAP_T) );
        ability.type_mask=BITMSK(SNAP_TYPE_TRIGGER)|BITMSK(SNAP_TYPE_TIMER)|BITMSK(SNAP_TYPE_ALAEM);
    	ability.uiCountsPerSecond = BITMSK(15);//|BITMSK(0); 

#ifdef SNAPSHOT_EXT  /*抓拍间隔扩大到7200 秒*/
	 	ability.uiMaxSeconds= 7200;
#else
        ability.uiMaxSeconds= 120;
#endif

//        ability.uiMinSeconds = 1;
        ability.uiMinSeconds = 2;

        ability.uiSnapMaxCount = 10; /*一次触发或手动抓拍最大张数*/
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_SNAP);
        tlv.SetValue( (uchar*)&ability, sizeof(ABILITY_SNAP_T),false);
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }    
#endif    

#ifdef __INTERVIDEO_
    {
        IVIDEO_ABILITY ability;
        memset( &ability, 0, sizeof(IVIDEO_ABILITY) );
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_IVIDEO);
        tlv.SetValue( (uchar*)&ability, sizeof(IVIDEO_ABILITY),false);
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }    
#endif
    {
        TALK_ABILITY ability;
        memset( &ability, 0, sizeof(ability) );
        ability.ucSurportTalkIn = 1;
        ability.ucSurportTalkOut = 1;
        //ability.usTourNums = PTZ_CHANNELS;
        
        ability.ucTalkInMask = 1;    //其他设备也取消PCM支持
        ability.ucTalkOutMask = 1;

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_TALK);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

    //音量调节能力集
    {
        AUDIO_ABILITY ability;
        memset( &ability, 0, sizeof(ability));

		//暂时通过是否为5路设备来判断VGA设备,屏蔽第五通道音频使能
		if(5 == g_nLogicNum)
		{
			ability.uiRes[0] |= BITMSK(4);
		}
        ability.ucAudioChannel = 1;
        
#ifdef _FUNC_ADJUST_VOLUME_   //add by ilena 音量控制 2010-07-01
        ability.uiAudioVolumn = 1;
        ability.ucTalk |= 1;//支持对讲音量输入控制
#ifdef _PF_DM365_SERIES_
        ability.ucAudioChannel = 1;    //单声道
#else
        ability.ucAudioChannel = 1;//    //hisi的接口没有实现 暂时还是单声道
//        ability.ucAudioChannel = 2;//双声道
#endif
#endif

        //!音频通道输入支持能力
        AUDIOIN_CAPS stCaps2;
        memset(&stCaps2,0,sizeof(stCaps2));
        CDevAudioIn::GetCaps(&stCaps2);
        ability.uAudioCaps |= stCaps2.ucRes[0];
        ability.uAudioCaps |= ( ((uint)stCaps2.ucRes[1]) << 8);
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_DEV_AUDIO);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

    {
        ENCODE_CAPS_NET ability;
        memset( &ability, 0, sizeof(ability) );

        CAPTURE_DSPINFO dspinfo;
        memset(&dspinfo,0,sizeof(CAPTURE_DSPINFO));
        GetDspInfo(&dspinfo);    

        ability.uiMaxEncodePower = dspinfo.nMaxEncodePower;
        ability.usSupportChannel = dspinfo.nMaxSupportChannel;
        ability.usChannelSetSync = dspinfo.bChannelMaxSetSync;
        
        CAPTURE_CAPS CapsInfo;
        memset(&CapsInfo,0,sizeof(CAPTURE_CAPS));
        GetCaps(&CapsInfo);
        CAPTURE_EXT_STREAM CapExtInfo;
        memset(&CapExtInfo,0,sizeof(CAPTURE_EXT_STREAM));
        GetExtCaps(&CapExtInfo);
        ability.ucVideoStandardMask = BITMSK(0)|BITMSK(1);
        ability.ucEncodeModeMask = CapsInfo.BitRateControl;
        
        ability.usStreamCap =BITMSK(0)|BITMSK(1);
        ability.uiImageSizeMask =CapsInfo.ImageSize;
        
        memcpy(ability.uiImageSizeMask_Assi,CapExtInfo.CaptureSizeMask,CAPTURE_SIZE_NR*sizeof(int));
        ability.ucSupportPolicy = 0;     
		//!利用这个保留字，为1表示，修改编码配置分辨率，需要重启		
		//!利堡客户需求，增加主码流的HD1和辅码流CIF，内存不够，采用这种策略
		//!CapsInfo.Reserved按位表示不同功能，第2位表示编码设置是否重启
		 ability.ucRes[0]=CapsInfo.Reserved&0xff;
		ability.ucRes[0] |= 0x2;

        ability.ucCompression = (1<<0);//按位表示，第0位表示是否支持264，第1位表示是否支持svac，第二位表示支持265
		if(CapsInfo.Compression & (1<<CAPTURE_COMP_H265)){
			ability.ucCompression |= (1<<2);
		}

#ifdef _2761_EXCEPTION_CHK_
	ability.ucEncoderDetect =1;
#endif

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_ENCODE);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false);
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);        
    }
    {
    	ENCODE_CAPS_NET2* pEncodeCaps = new ENCODE_CAPS_NET2();
    	memset( pEncodeCaps, 0, sizeof(ENCODE_CAPS_NET2) );

    	CAPTURE_STREAM_IMAGE tmpCaps;
    	memset(&tmpCaps, 0, sizeof(CAPTURE_STREAM_IMAGE) );

    	CaptureGetStreamImageCaps( &tmpCaps );

    	for( int i = 0; i < CAPTURE_SIZE_NR; i++)
    	{
    		for(int j=CHL_2END_T; j <= CHL_4RTH_T; j++)
    		{
    			if ( tmpCaps.CaptureSizeMask[i] & BITMSK(j) )
    			{
    				pEncodeCaps->ImageSizeMask_Stream[i] |= ( BITMSK(j-CHL_2END_T) );
    			}
    		}

    	}

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_ENCODE2);
        tlv.SetValue( (uchar*)pEncodeCaps, sizeof(ENCODE_CAPS_NET2), false);
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);

        delete pEncodeCaps;
    }

#ifndef SYS_NO_GUI

    //!设备分割能力
    {
        int split_num = 0;

        CPacket* pSplitPacket = g_PacketManager.GetPacket( 8+sizeof(SPLIT_MODE)+ SPLIT_NR*(8+sizeof(SPLIT_COMBINE) ) );
        assert( pSplitPacket != NULL );
        //!分割模式
        {
            //!判断此分割是否支持
            SPLIT_CAPS caps;

            if( !CDevSplit::instance(0)->GetCaps( &caps ) )
            {
                trace("can't get split caps!\n");
                assert(0);
            }

            SPLIT_MODE ability;
            memset( &ability, 0, sizeof(ability) );

            if( BITMSK(static_cast<int>(SPLIT1)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(0);    //!支持单画面能力
                split_num++;
            }

            if( BITMSK(static_cast<int>(SPLIT2)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(1);    //!支持2 画面能力
                split_num++;
            }

            if( BITMSK(static_cast<int>(SPLIT4)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(2);    //!支持4画面能力
                split_num++;
            }

            if( BITMSK(static_cast<int>(SPLIT8)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(3);    //!支持8画面能力
                split_num++;
            }
            if( BITMSK(static_cast<int>(SPLIT9)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(4);    //!支持9画面能力
                split_num++;
            }

            if( BITMSK(static_cast<int>(SPLIT16)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(5);    //!支持16画面能力
                split_num++;
            }
            if( BITMSK(static_cast<int>(SPLIT6)) & caps.dwCombine )
            {
                ability.iSplitMode |= BITMSK(7);    //!支持16画面能力
                split_num++;
            }

            CTlv tlv_split_mode;
            tlv_split_mode.SetType(DEV_SYSTEM_INFO_SPLIT_MODE);
            tlv_split_mode.SetValue( (uchar*)&ability, sizeof(ability), false );
            tlv_split_mode.Pack(p, Len_p);

            pSplitPacket->PutBuffer(p, Len_p);
            {
                SPLIT_COMBINE split_cfg;
                if( ability.iSplitMode & BITMSK(0) )
                {
                    //!支持单画面
                    memset( &split_cfg, 0, sizeof(split_cfg) );
                    split_cfg.ucSplitMode = SPLIT1;
                    split_cfg.ucSplitType = 0; //!任意组合
                    split_cfg.ucCombinNums = 0;

                    CTlv tlv;
                    tlv.SetType(DEV_SYSTEM_INFO_SPLIT_COMBINE);
                    tlv.SetValue( (uchar*)&split_cfg, sizeof(split_cfg), false );
                    tlv.Pack(p, Len_p);

                    //trace("==================>type:%#x, type2:%#x\n",
                    //        DEV_SYSTEM_INFO_SPLIT_COMBINE, (*(uint*)p));

                    pSplitPacket->PutBuffer(p, Len_p);
                }

                if( ability.iSplitMode & BITMSK(2) )
                {
                    //!支持4画面
                    memset( &split_cfg, 0, sizeof(split_cfg) );
                    split_cfg.ucSplitMode = SPLIT4;
                    split_cfg.ucSplitType = 1; //!包含组合
                    split_cfg.ucCombinNums = 0;

                    for( int i = 0; (4*i+4) <= g_nLogicNum; i++ )
                    {
                        split_cfg.ucCombinNums++;
                        split_cfg.stCombineCh[i].iChMask |= BITMSK(4*i);
                        split_cfg.stCombineCh[i].iChMask |= BITMSK(4*i+1);
                        split_cfg.stCombineCh[i].iChMask |= BITMSK(4*i+2);
                        split_cfg.stCombineCh[i].iChMask |= BITMSK(4*i+3);

                        split_cfg.stCombineCh[i].ucSpecialCh = 4*i;
                    }

                    CTlv tlv;
                    tlv.SetType(DEV_SYSTEM_INFO_SPLIT_COMBINE);
                    tlv.SetValue( (uchar*)&split_cfg, sizeof(split_cfg), false );
                    tlv.Pack(p, Len_p);

                    pSplitPacket->PutBuffer(p, Len_p);
                }
            }
        }

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_SPLIT_TYPE);
        tlv.SetValue( pSplitPacket->GetBuffer(), pSplitPacket->GetLength(), true );
		pSplitPacket->Release();
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }
#endif

    //!报警联运能力
    {
        ABILITY_AlarmLink ability;
        /* 消息处理方式,可以同时多种处理方式,包括
        * 0x00000001 - 网络:上传管理服务器
        * 0x00000002 - 录像:触发
        * 0x00000004 - 云台联动
        * 0x00000008 - 发送邮件
        * 0x00000010 - 设备本地报警轮巡
        * 0x00000020 - 设备提示使能
        * 0x00000040 - 设备报警输出使能
        * 0x00000080 - Ftp上传使能
        * 0x00000100 - 蜂鸣
        * 0x00000200 - 语音提示
        * 0x00000400 - 抓图使能*/
        memset(&ability, 0, sizeof(ABILITY_AlarmLink) );

        ability.dwActionMask = 0x00000010 |0x00000040 | 0x00000100 ;
        ability.dwActionMask |= 0x00000004 ;
        ability.dwActionMask |= 0x00000002 ;
        ability.dwActionMask |= 0x00000020 ;

#ifdef ENC_SUPPORT_SNAPSHOT
        CAPTURE_EXT_STREAM extStream; 
        GetExtCaps(&extStream);
        if(extStream.ExtraStream & BITMSK(CHL_JPEG_T))
        {
            ability.dwActionMask |= 0x00000400;
        }
#endif


#ifdef _USE_SMTP_MODULE
        ability.dwActionMask |= 0x00000008;
#endif

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_AlarmLink);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

    //时间能力集
    {
        TIME_ABILITY ability;
        memset( &ability, 0, sizeof(ability));

        ability.ucSupTimeAdjust |= 0x01;//夏令时
#ifdef USING_PROTOCOL_NTP
        ability.ucSupTimeAdjust |= 0x02;//SNTP
#endif
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_DEV_TIME);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

    //录像控制能力集
    {
        RECORD_ABILITY ability;
        memset( &ability, 0, sizeof(ability));

        ability.dwFlag |= 0x01;
#ifdef FUN_SNIFFER
		ability.ucSupportCardRecord = 1;
#endif

        /* 预录时间，目前写死最大10s */
        ability.dwFlag |= 0x02;
        ability.usPreRecordTime = 10;
		
        CTlv tlv;
        tlv.SetType(DEV_ABILITY_DEV_RECORD );
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);
    }

#if defined (_USE_720P_MODULE_) 
	{
		VIDEOIN_CAPS video_in_caps;
		memset(&video_in_caps, 0, sizeof(VIDEOIN_CAPS));
		VideoInGetChCaps(0, &video_in_caps);

		CTlv tlv;
		tlv.SetType(DEV_ABILITY_CAMERA);
		tlv.SetValue( (uchar*)&video_in_caps, sizeof(VIDEOIN_CAPS), false);
		tlv.Pack(p, Len_p);

		pPacket->PutBuffer(p, Len_p);
	}

#ifdef SDK_3516
   {
		SENSOR_ABILITY ability;
		memset( &ability, 0, sizeof(ability));

		ability.iMskList= 0x0;
		VD_SENSOR_TYPE_E tmptype = VideoInGetSensorType(0);

		//枪击支持区域遮挡
		if(tmptype != PANASONIC_34041_1080P_60FPS
		&& tmptype != PANASONIC_34041_1080P
		&& tmptype != PANASONIC_34041_720P
		&& tmptype != APTINA_9M034)
		{
			ability.iMskFun = BITMSK(0);  
		}

        CTlv tlv;
        tlv.SetType(DEV_ABILITY_SUPPORT_SENSOR_TYPE);
        tlv.SetValue( (uchar*)&ability, sizeof(ability), false );
        tlv.Pack(p, Len_p);

        pPacket->PutBuffer(p, Len_p);	
   }
#endif
#endif

{
    NEW_NET_OPR_ABILITY ability;
    memset( &ability, 0, sizeof(NEW_NET_OPR_ABILITY) );
#ifdef ENC_SUPPORT_SNAPSHOT
	ability.uiNetOprAbilty = 3;
#else
	ability.uiNetOprAbilty = 2;
#endif

    CTlv tlv;
    tlv.SetType(DEV_ABILITY_NEW_NET_OPR);
    tlv.SetValue( (uchar*)&ability, sizeof(NEW_NET_OPR_ABILITY), false );
    tlv.Pack(p, Len_p);

    pPacket->PutBuffer(p, Len_p);
}

	{
	    ALARM_SUPPORT_ABILITY ability;
	    memset( &ability, 0, sizeof(ALARM_SUPPORT_ABILITY) );
	    //ADDED BY ZANGLEI 20130407 去掉网络报警和视频丢失报警
	    ability.uiAlarmAbilty = 0x34;

	    CTlv tlv;
	    tlv.SetType(DEV_ABILITY_SUPPORT_ALARM);
	    tlv.SetValue( (uchar*)&ability, sizeof(ALARM_SUPPORT_ABILITY), false );
	    tlv.Pack(p, Len_p);

	    pPacket->PutBuffer(p, Len_p);
	}

	{
	    ABILITY_OEM_FUN_T ability;
	    memset( &ability, 0, sizeof(ABILITY_OEM_FUN_T) );
	    CTlv tlv;
	    tlv.SetType(DEV_ABILITY_OEM_FUN);
	    tlv.SetValue( (uchar*)&ability, sizeof(ABILITY_OEM_FUN_T), false );
	    tlv.Pack(p, Len_p);

	    pPacket->PutBuffer(p, Len_p);
	} 
}

int CVNLogState::AckAblityTlv(CIVnPdu*& pReqPDU, CIVnPdu*& pRespPDU)
{
    pRespPDU->m_pIpHdr->dvrip_r1 = 1;
    //!能力tlv
    CPacket *pPacket_ability;
    GetAblility(pPacket_ability);
    pRespPDU->packetBody( (char*)pPacket_ability->GetBuffer(), pPacket_ability->GetLength() );
    pPacket_ability->Release();
    return 0;
}



