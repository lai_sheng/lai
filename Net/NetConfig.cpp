#ifndef _NETCONFIG_H_
#define _NETCONFIG_H_

#include "APIs/Split.h"
#include "Devices/DevVideo.h"
//#include "Devices/DevPlay.h"
#include "Net/NetConfig.h"

template<> void exchangeTable<DISPLAY_SPLIT_CFG>(CConfigExchange& xchg, CConfigTable& table,
		DISPLAY_SPLIT_CFG& config, int index, int app)
{
	uint specialch = config.stCombine.ucSpecialCh;

    xchg.exchange(table, "stSplitMode", config.stSplitMode,0,SPLIT_NR,SPLIT16 );
	xchg.exchange(table["stCombine"], "iChMask", config.stCombine.iChMask, 0x00ff );

	xchg.exchange(table["stCombine"], "ucSpecialCh", specialch, 0x0000 );
    xchg.exchange(table, "ucRes", (char *)config.stCombine.ucRes, "\0" );
	config.stCombine.ucSpecialCh = specialch;
}


void GetSysAttrEx(SYSATTR_EX_T* psys_attr)
{
	psys_attr->iVideoInCaps = g_nCapture;
	psys_attr->iVideoOutCaps = CDevVideo::GetChannels();

	if (g_CapsEx.HasAudioBoard)
	{
		// 特殊的设备，音频输入数量不能用视频通道来表示，两者不一样
		#ifdef ENC_SPECIAL_AUDIO_CHANNELS
		psys_attr->iAudioInCaps = g_CapsEx.HasAudioBoard;
		#else
		psys_attr->iAudioInCaps = g_nCapture;
		#endif
	}else{
		psys_attr->iAudioInCaps = 0;
	}
	psys_attr->iAudioOutCaps = 1;

	psys_attr->iAlarmInCaps = g_nAlarmIn;
	psys_attr->iAlarmOutCaps = g_nAlarmOut;
	psys_attr->iDiskNum = 4;
	psys_attr->iAetherNetPortNum = 0x01;

	psys_attr->iUsbPortNum = 2;
	psys_attr->iDecodeChanNum = 1;//暂时写1  hyd 20161107
	psys_attr->iComPortNum = 0x01;
	psys_attr->iParallelPortNum = 0;

	psys_attr->iSpeechInCaps = 0;
	psys_attr->iSpeechOutCaps = 0;
}

void EVENT_HANDLER_NET_2_EVENT_HANDLER(EVENT_HANDLER_NET& ehn, EVENT_HANDLER& eh)
{
	eh.dwRecord = ehn.dwRecord;
	eh.iRecordLatch = ehn.iRecordLatch;
	eh.dwTour = ehn.dwTour;
	eh.dwSnapShot = ehn.dwSnapShot;
	eh.dwAlarmOut = ehn.dwAlarmOut;
	eh.iAOLatch = ehn.iAOLatch;
	memcpy(eh.PtzLink, ehn.PtzLink, sizeof(PTZ_LINK)*16 );
	eh.bRecordEn = ehn.bRecordEn;
	eh.bTourEn = ehn.bTourEn;
	eh.bSnapEn = ehn.bSnapEn;
	eh.bAlarmOutEn = ehn.bAlarmOutEn;
	eh.bPtzEn = ehn.bPtzEn;
	eh.bTip = ehn.bTip;
	eh.bMail = ehn.bMail;
	eh.bMessage = ehn.bMessage;
	eh.bBeep = ehn.bBeep;
	eh.bVoice = ehn.bVoice;
	eh.bFTP = ehn.bFTP;
	eh.iWsName = ehn.iWsName;
	eh.dwMatrix = ehn.dwMatrix;
	eh.bMatrixEn = ehn.bMatrixEn;
	eh.bLog = ehn.bLog;
	eh.iEventLatch = ehn.iEventLatch;
	eh.bMessagetoNet = ehn.bMessagetoNet;
}

void EVENT_HANDLER_2_EVENT_HANDLER_NET(EVENT_HANDLER& eh, EVENT_HANDLER_NET& ehn)
{
	ehn.dwRecord = eh.dwRecord;
	ehn.iRecordLatch = eh.iRecordLatch;
	ehn.dwTour = eh.dwTour;
	ehn.dwSnapShot = eh.dwSnapShot;
	ehn.dwAlarmOut = eh.dwAlarmOut;
	ehn.iAOLatch = eh.iAOLatch;
	memcpy(ehn.PtzLink, eh.PtzLink, sizeof(PTZ_LINK)*16 );
	ehn.bRecordEn = eh.bRecordEn;
	ehn.bTourEn = eh.bTourEn;
	ehn.bSnapEn = eh.bSnapEn;
	ehn.bAlarmOutEn = eh.bAlarmOutEn;
	ehn.bPtzEn = eh.bPtzEn;
	ehn.bTip = eh.bTip;
	ehn.bMail = eh.bMail;
	ehn.bMessage = eh.bMessage;
	ehn.bBeep = eh.bBeep;
	ehn.bVoice = eh.bVoice;
	ehn.bFTP = eh.bFTP;
	ehn.iWsName = eh.iWsName;
	ehn.dwMatrix = eh.dwMatrix;
	ehn.bMatrixEn = eh.bMatrixEn;
	ehn.bLog = eh.bLog;
	ehn.iEventLatch = eh.iEventLatch;
	ehn.bMessagetoNet = eh.bMessagetoNet;
}

void CONFIG_WORKSHEET_2_NET_WORKSHEET(int channel, CONFIG_WORKSHEET&cw, NET_WORKSHEET& ncw)
{
	ncw.iChannel = channel;
	ncw.iName = cw.iName;

	for( int i = 0; i < NET_N_WEEKS; i++ )
	{
		for(int j = 0; j < NET_N_UI_TSECT; j++ )
		{
			//memcpy(&ncw.tsSchedule[i][j], &cw.tsSchedule[i][j], sizeof(NETSECTION) );
			ncw.tsSchedule[i][j].enable = cw.tsSchedule[i][j].enable;
			//trace("channel:%d, i:%d,j:%d, enable:%d\n", channel, i, j, ncw.tsSchedule[i][j].enable);
			ncw.tsSchedule[i][j].startHour = cw.tsSchedule[i][j].startHour;
			ncw.tsSchedule[i][j].startMinute = cw.tsSchedule[i][j].startMinute;
			ncw.tsSchedule[i][j].startSecond = cw.tsSchedule[i][j].startSecond;

			ncw.tsSchedule[i][j].endHour = cw.tsSchedule[i][j].endHour;
			ncw.tsSchedule[i][j].endMinute = cw.tsSchedule[i][j].endMinute;
			ncw.tsSchedule[i][j].endSecond = cw.tsSchedule[i][j].endSecond;
		}
	}
}
void NET_WORKSHEET_2_CONFIG_WORKSHEET(int channel, NET_WORKSHEET& ncw, CONFIG_WORKSHEET&cw)
{
	//cw.iName = ncw.iName;
	for( int i = 0; i < NET_N_WEEKS; i++ )
	{
		for(int j = 0; j < NET_N_UI_TSECT; j++ )
		{
			//memcpy(&cw.tsSchedule[i][j], &ncw.tsSchedule[i][j], sizeof(NETSECTION) );
			cw.tsSchedule[i][j].enable = ncw.tsSchedule[i][j].enable;
			cw.tsSchedule[i][j].startHour = ncw.tsSchedule[i][j].startHour;
			cw.tsSchedule[i][j].startMinute = ncw.tsSchedule[i][j].startMinute;
			cw.tsSchedule[i][j].startSecond = ncw.tsSchedule[i][j].startSecond;

			cw.tsSchedule[i][j].endHour = ncw.tsSchedule[i][j].endHour;
			cw.tsSchedule[i][j].endMinute = ncw.tsSchedule[i][j].endMinute;
			cw.tsSchedule[i][j].endSecond = ncw.tsSchedule[i][j].endSecond;
		}
	}
}

#endif
