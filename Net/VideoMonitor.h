//    文 件 名： VideoMonitor.h
//    描    述: 用于实现网络视频实时传输

//                               删除原有的网络自适应算法
//
#ifndef __VIDEO_MONITOR_H__
#define __VIDEO_MONITOR_H__

#include "System/Object.h"
#include "System/Packet.h"
#include "Net/NetCore.h"
#include "Net/NetWorkService.h"
#include "Configs/ConfigEncode.h"
#include "Configs/ConfigRecord.h"
#include "Configs/ConfigNet.h"

#include "System/Signals.h"
#include "MultiTask/Thread.h"

#include "Net/NetApp.h"
#include "Media/ICapture.h"

/*
*窄带网络传输策结构体，请不要随意修改
*/
typedef enum _manager_enum_
{
    PFrame_all= 0,
    PFrame_DQuarter,
    PFrame_Quarter,
    PFrame_Half,
    PFrame_HHalf,
    PFrame_None,
    IFrame_None,
}Manager_enum;

typedef enum _net_adjust_state_enum_
{
    NET_ADJUST_STATE_NORMAL = 0,
    NET_ADJUST_STATE_DOWN,
    NET_ADJUST_STATE_UP,
    NET_ADJUST_STATE_CONFLICT,
}_NET_ADJUST_STATE_enum;


typedef struct _frames_adapter_a_
{
    int emAdjustState[N_SYS_CH*2];
    int iPFrameNums[N_SYS_CH*2];/*统计p帧数目，-1 wait to send i frame*/
    int iFpsAdapter[N_SYS_CH*2];/* 网络发帧调整0 :normal 1: adapter, normal期间数据是按包发, 如有整帧存的话按帧发*/
    struct timeval stLastTime[N_SYS_CH*2];
}PER_CONN_NET_FRAME_ADAPTER;


typedef struct  _quality_rate_adapter_a
{
    int bConnTransPolicy[N_SYS_CH*2];    /*是否需要网络策略传输*/
    int emConnTransState[N_SYS_CH*2];    /*网络传输状态NetTransferState_t结构*/
    
    int iUpCounts[N_SYS_CH*2];                  /*!码流改变次数*/
    int iDownCounts[N_SYS_CH*2];
    
    int bChangeStreamFlags[N_SYS_CH*2];    /*!切换到主辅码流标志，其中0切换到主码流，1辅*/
    
    ushort usOldDspRate[N_SYS_CH*2];     /*!码率，新码率，上调的码率*/
    ushort usNewDspRate[N_SYS_CH*2];    
    ushort usUpDspRate[N_SYS_CH*2];    

    ushort usOldFps[N_SYS_CH*2];        /*!帧率，新帧率，上调的帧率*/
    ushort usNewFps[ N_SYS_CH*2];    
    ushort usUpFps[N_SYS_CH*2];        
    
    ushort iConnResolution[N_SYS_CH*2]; /*!分辨率*/

    int emAdjustState[N_SYS_CH*2];    /*!调整策略, _NET_ADJUST_STATE_enum结构内容*/
    int bConfictFlag[N_SYS_CH*2];    
    
    int iFpsAdapter[N_SYS_CH*2];
    
    struct timeval stLastTime[N_SYS_CH*2];    /*记录某通道最后一次检测或处理时间*/
    struct timeval stAdjustTime[N_SYS_CH*2];    
}PER_CONN_NET_ADAPTER;

typedef struct
{
  uint ChannelCmd[32];//!0:关闭 1:打开, 2,:不改变
  uint ChannelStream[32];//!0-主码流,1:辅码流
}CHANNEL_OPR;

class CaptureHelper :public CObject
{
public:

    CaptureHelper(struct conn *c);
    virtual ~CaptureHelper();
    int FilterCallback(struct conn *c, DVRIP *pHead);

    void Reset();
    void Init();

    VD_BOOL GetAllocFlag();
    VD_VOID SetAllocFlag(VD_BOOL BFlag);
    VD_VOID SetCon(struct conn *c);

    char GetMonitorState(int chn,int stream);

#ifdef RECORD_NET_THREAD_MULTI 

    bool SendChnToNet();
#endif

    /* 供外部停止视频使用 */
    int stopAllVideoCapture();

    int GetMonitorNum();/* 正在监视的通道数目 */
    int GetMonitorRate();/* 正在监视的码流大小(K) */
    int CalcMonitorNumDiff(DVRIP *pHead);/*本次操作引起的码流监视数目变化*/
    int CalcMonitorRateDiff(DVRIP *pHead);/*本次操作引起的码流码率变化*/

private:
    void OnCapture(int chn,int stream, CPacket *pPacket);
#ifdef RECORD_NET_THREAD_MULTI 
    void OnCaptureEx(int chn,int stream, CPacket *pPacket);
#endif
    int startLocalVideoCapture(int chn,int stream);
    int stopLocalVideoCapture(int chn,int stream,int flag );
    int startNetVideoCapture(int chn,int stream);
    int stopNetVideoCapture(int chn,int stream,int flag );

    int startVideoCapture(int chn,int stream);
    int stopVideoCapture(int chn,int stream,int flag );

    /* 配置改变*/
    int EncodeCfgChange(CConfigEncode & cConfigEncode, int & iRet);
    int NetTransCfgChange(CConfigNetCommon* pConfigNetCommon, int &ret);

    /*发送前状态变迁，自适应处理函数*/
    int  NetStateDetect(int ch,int stream );
    int  NetTransAdpater(int ch,int stream );

    int  DigiChNetStateDetect(int ch,int stream );

    void NetChangeMainToExtra(int iChn, int iStream);
    void NetChangeStream(bool bExtraToMain);

    void DumpHelper(int chn,int stream);
    
private:
    CConfigEncode        m_cCfgEncode;                    
    CConfigRecord              m_cCfgRecord;
    CConfigNetCommon   m_cCfgNetCommon;
    
    ulong m_seq[N_SYS_CH*2];    //!数字溢出时，自动为0
    char m_MonitorState[N_SYS_CH*2];/*<存储当前通道对应的主辅码流的监视状态*/
    struct conn *m_conn;
    int m_iMaxFrameRate;/*当前的制式支持的最大帧率*/
    PER_CONN_NET_FRAME_ADAPTER m_stFPSAdapter;
    PER_CONN_NET_ADAPTER m_stTransAdapter;  /*<对于每个连接进行的网络传输策略调整参数*/

    uint64 m_iLastTime[N_SYS_CH*2]; //上次打印时间
    ICaptureManager* m_pCaptureM;

    VD_BOOL m_BAllocFlag;

    CMutex m_Mutex;

#ifdef RECORD_NET_THREAD_MULTI 

    typedef struct PACKET_CAPTURE
    {
        int chn;
        CPacket* pPacket;
        uint dwStreamType;
    }PACKET_CAPTURE;
    typedef std::list<PACKET_CAPTURE> PACKET_LIST;
    PACKET_LIST m_packet_list;
    uint m_packet_count; //!主要实时更新m_packet_list的数量，这样判断数量可以不用进入锁里去
//    uint m_total_len; //!缓存pakcet list总大小
    CMutex    m_mutex_list;

#endif
    
};


#if 1

#ifdef RECORD_NET_THREAD_MULTI 
class CaptureHelperManager :public CThread
#else
class CaptureHelperManager :public CObject
#endif
{
public:
    PATTERN_SINGLETON_DECLARE(CaptureHelperManager);

    CaptureHelperManager();
    
    virtual ~CaptureHelperManager();
#ifdef RECORD_NET_THREAD_MULTI 

    virtual void ThreadProc();


#endif

    typedef std::list< CaptureHelper* > CapHlp_List;

    typedef std::list< VodHelper* > CapVodHlp_List;

    typedef std::list< AudioHelper* > CapAudioHlp_List;

    CapHlp_List m_CHPool;
    CaptureHelper *AllocCaptureHelper(struct conn *c);
    bool FreeCaptureHelper(CaptureHelper* pstCH);

    CapVodHlp_List m_CHVodPool;
    VodHelper *AllocCaptureVodHelper(struct conn *c);
    bool FreeCaptureVodHelper(VodHelper* pstCH);
	
    CapAudioHlp_List m_CHAudioPool;
    AudioHelper *AllocCaptureAudioHelper(struct conn *c);
    bool FreeCaptureAudioHelper(AudioHelper* pstCH);	
	
    void StopAllMonitor();
    int GetMonitorNum();/* 正在监视的通道数目 */
    int GetMonitorRate();/* 正在监视的码流大小(K) */    
private:
    CMutex m_list_Pool_Mutex;
	CMutex m_Vodlist_Pool_Mutex;
	CMutex m_Audiolist_Pool_Mutex;
};


#define g_CaptureHelperManager (*CaptureHelperManager::instance())
#endif

int RegisterVideoMonitorFilter(struct conn *c);

class CDevHDiskReader;

class VodHelper :public CObject
{
public:
    enum netDownLoadState
    {
        netDownLoadStop = 0,
        netDownLoadStart,
        netDownLoadPause
    };
    VodHelper(struct conn *c);

    virtual ~VodHelper();

    int FilterCallback(struct conn *c, DVRIP *pHead);
    void Init();    
    VD_BOOL GetAllocFlag();
    VD_VOID SetAllocFlag(VD_BOOL BFlag);
    VD_VOID SetCon(struct conn *c);
    int stopAllVodCapture();	
private:
    void OnDownload(int iCMD, CPacket *pPacket, int iCh);
    bool startDiskReader(int chn);
    bool stopDiskReader(int chn);
    bool pauseDiskReader(int chn);    

    int m_iDownloadState[N_SYS_CH];

    CDevHDiskReader* m_pHDiskReader[N_SYS_CH];
    struct conn *m_conn;
    ICaptureManager* m_pCaptureM;

    VD_BOOL m_BAllocFlag;
	
};

class AudioHelper :public CObject
{
public:

    AudioHelper(struct conn *c);

    virtual ~AudioHelper();

    int FilterCallback(struct conn *c, DVRIP *pHead);
    void Init();    
    VD_BOOL GetAllocFlag();
    VD_VOID SetAllocFlag(VD_BOOL BFlag);
    VD_VOID SetCon(struct conn *c);
    int stopAllAudioCapture();
private:

    void OnAudioCapture(int iChannel, CPacket *pPacket);
    bool StartAudioCapture(int iChannel);
    bool StopAudioCapture(int iChannel);
    bool SetAudioInFormat(int iChannel, int iFormat);
 
    int m_bNeedTransAudioData[N_AUDIO_IN];
    int m_bIsChnOnAudioCapture[N_AUDIO_IN];
    bool m_bIsAudioStarted;
    struct conn *m_conn;

    VD_BOOL m_BAllocFlag;
    
};
#endif//__VIDEO_MONITOR_H__
//
//
