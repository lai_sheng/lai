//    文 件 名： VideoMonitor.cpp
//    描        述   ： 用于实现网络视频实时传输

// 包括动态检测网络状态，根据不同网络传输策略进行调整
// 包括用户的低延时，画面质量和流畅性，带宽利用率等


#include "System/BaseTypedef.h"
#include "Devices/DevExCapture.h"
//#include "Functions/Display.h"
#include "VideoMonitor.h"
#include "Devices/DevCapture.h"
#include "APIs/Split.h"
#include "Configs/ConfigConversion.h"
#include "Net/NetConfig.h"

#include "Functions/Record.h"
#include "Functions/Encode.h"
#ifdef LINUX
#include <sys/time.h>        /* Some linuxes put struct timeval there */
#include <linux/sockios.h>
#endif
#include "Net/NetApp.h"
#include "Configs/ConfigNet.h"
#include "System/AppConfig.h"

#include "Functions/AudioManager.h"
#include "Devices/DevAudioIn.h"
#include "Devices/DevHDiskReader.h"
#include "Configs/ConfigDigiChManager.h"
//#include "Functions/Upgrader.h"

//!通道监视切换
enum NetworkMonitorStatus
{
    CHANNEL_MONITOR_OFF=0,
    CHANNEL_MONITOR_ON=1,
    CHANNEL_MONITOR_NOCHANGE=2,
};
//!音频数据获取
enum GetAudioDataStatus
{
    CHANNEL_AUDIO_DATA_OFF=0,
    CHANNEL_AUDIO_DATA_ON=1,
};

extern struct timeval current_time;
extern NET_TRANS_POLICY_CFG g_stNetTransPolicy ;

int vfps_pal_all[]  = {1, 2, 3,4, 5,6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24, 25};
int vfps_ntsc_all[] ={1, 2, 3,4, 5,6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24, 25,26,27,28,29,30};
int g_stNetMinRate[]={600,384,384,160,32,512,400,40,64};
int g_stNetMaxRate[]={3000,1600,1600,400,2500,600,600,150,800};

int pframes_map[] = {200,40,30,20,10,0,0};
#define MAX_PACKET_SIZE 4200
#define MAX_PFRAME_DROP 200
#define NET_MIN_DSP_RATE 64

CaptureHelper::CaptureHelper(struct conn *c)
    :m_conn(c)
{
    m_BAllocFlag = VD_TRUE;
    
    m_pCaptureM = ICaptureManager::instance();
    assert(m_pCaptureM);

    Init();
}

CaptureHelper::~CaptureHelper() 
{
    for (int i = 0; i < (m_pCaptureM->GetLogicChnNum() ); i ++)
    {
        #if defined(DEF_UPDATE_VBR)
        if((1 == m_MonitorState[i]) && (0 == ICaptureManager::instance()->GetDigitalChnNum()))
        {
            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
            g_Encode.getCapFormat(format);
            UpdateVBRValue(2,2,BITMSK(i),format);    
            tracef("*******222******UpdateVBRValue,2,2[%d]***********\n",i);
        }
        #endif
        stopVideoCapture(i,0,0);
        stopVideoCapture(i,1,0);
    }
    m_cCfgEncode.detach(this, ((TCONFIG_PROC)&CaptureHelper::EncodeCfgChange));
    m_cCfgNetCommon.detach(this,((TCONFIG_PROC)&CaptureHelper::NetTransCfgChange));
}

void CaptureHelper::Init()
{
    CGuard tmpGuard(m_Mutex);

    memset(m_seq, 0, sizeof(m_seq) );
    memset(m_MonitorState, 0, sizeof(m_MonitorState));

    m_iMaxFrameRate = 24;

    CONFIG_GENERAL_OLD szGeneralCfg;
    memset(&szGeneralCfg, 0 , sizeof(CONFIG_GENERAL_OLD));
    
    //转换数据
    CConfigGeneral* pCfgGeneral = new CConfigGeneral();
    CConfigLocation* pCfgLocation = new CConfigLocation();

    pCfgGeneral->update();
    pCfgLocation->update();

    CONFIG_GENERAL& cfgGeneral = pCfgGeneral->getConfig();
    CONFIG_LOCATION& cfgLocation = pCfgLocation->getConfig();
    
    szGeneralCfg.LocalNo = cfgGeneral.iLocalNo;
    szGeneralCfg.OverWrite = cfgGeneral.iOverWrite;
    szGeneralCfg.VideoFmt = cfgLocation.iVideoFormat;
    szGeneralCfg.Language = cfgLocation.iLanguage;
    szGeneralCfg.DateFmt = cfgLocation.iDateFormat;
    szGeneralCfg.DateSprtr = cfgLocation.iDateSeparator;
    szGeneralCfg.TimeFmt = cfgLocation.iTimeFormat;
    szGeneralCfg.DST = cfgLocation.iDSTRule;

    szGeneralCfg.StandbyTime = cfgGeneral.iAutoLogout;

    delete pCfgGeneral;
    delete pCfgLocation;

    if(szGeneralCfg.VideoFmt == 1)
    {
        m_iMaxFrameRate = 29;
    }

    m_cCfgEncode.update();
    m_cCfgEncode.attach(this, ((TCONFIG_PROC)&CaptureHelper::EncodeCfgChange));

    m_cCfgNetCommon.update();
    m_cCfgNetCommon.attach(this,(TCONFIG_PROC)&CaptureHelper::NetTransCfgChange);

    memset(&m_stFPSAdapter,0,sizeof(PER_CONN_NET_FRAME_ADAPTER));
    memset(&m_stTransAdapter,0,sizeof(PER_CONN_NET_ADAPTER));    

    for(int iTmp = 0; iTmp < N_SYS_CH*2; iTmp ++)
    {
#ifdef LINUX
        gettimeofday(&m_stFPSAdapter.stLastTime[iTmp],0);
        gettimeofday(&m_stTransAdapter.stLastTime[iTmp],0);
        gettimeofday(&m_stTransAdapter.stAdjustTime[iTmp],0);
#else
        m_stFPSAdapter.stLastTime[iTmp].tv_sec = 0;
        m_stFPSAdapter.stLastTime[iTmp].tv_usec = SystemGetMSCount()*1000;        

        m_stTransAdapter.stLastTime[iTmp].tv_sec = 0;
        m_stTransAdapter.stLastTime[iTmp].tv_usec = SystemGetMSCount()*1000;        
        m_stTransAdapter.stAdjustTime[iTmp].tv_sec = 0;
        m_stTransAdapter.stAdjustTime[iTmp].tv_usec = SystemGetMSCount()*1000;    
#endif
    }

    for(int iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
    {        
        m_stTransAdapter.bConnTransPolicy[iChn] = g_stNetTransPolicy.bNetTransPolicy;
        m_stTransAdapter.emConnTransState[iChn] = g_stNetTransPolicy.emNetTransState;            
        m_stTransAdapter.emAdjustState[iChn] = 0;
        m_stTransAdapter.bConfictFlag[iChn] = 0;
        m_stTransAdapter.usOldDspRate[iChn] = g_stNetTransPolicy.usDspRate[iChn];

        int iMaxFps = (m_iMaxFrameRate == 24)?25:30;
        int iResolution = g_stNetTransPolicy.usResolution[iChn] ;
        if (g_stNetTransPolicy.usResolution[iChn] >= CAPTURE_SIZE_NR)
        {
            iResolution = CAPTURE_SIZE_CIF;
        }
        int iCfgFps = (g_stNetTransPolicy.usFps[iChn] >iMaxFps)?iMaxFps:(g_stNetTransPolicy.usFps[iChn]);
        int iMinRate = g_stNetMinRate[iResolution]*iCfgFps/iMaxFps;
        
        m_stTransAdapter.iConnResolution[iChn]  = iMinRate;

        if(m_stTransAdapter.usOldDspRate[iChn]  < (iMinRate +16))
        {
            m_stTransAdapter.usOldDspRate[iChn]  = iMinRate+ 16;            
            
        }
        m_stTransAdapter.usNewDspRate[iChn] =m_stTransAdapter.usOldDspRate[iChn] - 8;
    
        m_stTransAdapter.usUpDspRate[iChn] = 0;
        
        m_stTransAdapter.usUpFps[iChn] = 0;
        m_stTransAdapter.iUpCounts[iChn] = 0;
        m_stTransAdapter.iDownCounts[iChn] = 0;
        m_stTransAdapter.iFpsAdapter[iChn] = 0;        
        m_stTransAdapter.bChangeStreamFlags[iChn] = 0;

        m_stTransAdapter.usOldFps[iChn] =g_stNetTransPolicy.usFps[iChn];
        m_stTransAdapter.usNewFps[iChn] = g_stNetTransPolicy.usFps[iChn];

        #ifdef LINUX
        gettimeofday(&m_stTransAdapter.stLastTime[iChn],0);
        gettimeofday(&m_stTransAdapter.stAdjustTime[iChn],0);
        #else
        m_stTransAdapter.stLastTime[iChn].tv_sec = 0;
        m_stTransAdapter.stLastTime[iChn].tv_usec = SystemGetMSCount()*1000;        
        m_stTransAdapter.stAdjustTime[iChn].tv_sec = 0;
        m_stTransAdapter.stAdjustTime[iChn].tv_usec = SystemGetMSCount()*1000;    
        #endif
    }

#ifdef RECORD_NET_THREAD_MULTI
    m_packet_count = 0;
#endif
    
}

void CaptureHelper::Reset()
{
    CGuard tmpGuard(m_Mutex);
    printf("CaptureHelper::Reset():in\r\n");

    for (int i = 0; i < (m_pCaptureM->GetLogicChnNum() ); i ++)
    {
        #if defined(DEF_UPDATE_VBR)
        if((1 == m_MonitorState[i]) && (0 == ICaptureManager::instance()->GetDigitalChnNum()))
        {
            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
            g_Encode.getCapFormat(format);
            UpdateVBRValue(2,2,BITMSK(i),format);    
            tracef("*******222******UpdateVBRValue,2,2[%d]***********\n",i);
        }
        #endif
        stopVideoCapture(i,0,0);
        stopVideoCapture(i,1,0);
    }

#ifdef RECORD_NET_THREAD_MULTI

	/*清空网络发送中的队列数据，避免下次会发送不必要的数据出去*/
	if( m_packet_count > 0 )
	{
		printf("m_packet_count = %d\r\n",m_packet_count);
	}
	CPacket* pPacket = NULL;

	m_packet_count = 0;
	m_mutex_list.Enter();
	
	while(1)
	{
		PACKET_LIST::iterator it = m_packet_list.begin();
		if (it == m_packet_list.end())
		{
			break;
		}

		pPacket = it->pPacket;
		m_packet_list.pop_front();
		pPacket->Release();
	}

	m_mutex_list.Leave();

#endif

    m_conn = NULL;
    m_BAllocFlag = VD_FALSE;
}

void ReleasePacket(const struct tagMsgHdr *msg) 
{
    CPacket *pPacket = (CPacket *)msg->context;
    assert(pPacket);
    pPacket->Release();
}    

int CaptureHelper::NetTransCfgChange(CConfigNetCommon* pConfigNetCommon, int &ret)
{
    CGuard tmpGuard(m_Mutex);
    
    if (m_BAllocFlag == VD_FALSE)
    {
        return 0;
    }
    
    int iOldEnabe =  g_stNetTransPolicy.bNetTransPolicy;
    int iOldState =  g_stNetTransPolicy.emNetTransState;
    if(m_conn->sock <= 0)
    {
        return 0;
    }
    
    CONFIG_NET_COMMON &newCfgComm = pConfigNetCommon->getConfig();
    if(true == newCfgComm.bUseTransferPlan )
    {
        g_stNetTransPolicy.bNetTransPolicy = 1;
    }
    else
    {
        g_stNetTransPolicy.bNetTransPolicy = 0;
    }

    if(true == newCfgComm.bUseHSDownLoad )
    {
        g_stNetTransPolicy.bHighDownload = 1;
    }
    else
    {
        g_stNetTransPolicy.bHighDownload = 0;
    }
    
    if(IMAGE_TRANSFER_QUAILITY_PRIOR == newCfgComm.TransferPlan )
    {
        g_stNetTransPolicy.emNetTransState= NET_TRANS_QUALITY;
    }
    else
    {
        g_stNetTransPolicy.emNetTransState= NET_TRANS_RATE;
    }

    tracef("$$$$$$$$$ NetTransCfgChange enable %d state  %d hsdown %d \n",
        g_stNetTransPolicy.bNetTransPolicy,g_stNetTransPolicy.emNetTransState,
        g_stNetTransPolicy.bHighDownload);

    if((iOldEnabe != g_stNetTransPolicy.bNetTransPolicy) 
        ||(iOldState != g_stNetTransPolicy.emNetTransState))
    {    
        if(iOldEnabe != g_stNetTransPolicy.bNetTransPolicy) 
        {
            m_conn->iSocketNeedChgFlag = 1;
        }
        
        m_cCfgEncode.update();

        for(int curChannel=0;curChannel<m_pCaptureM->GetLogicChnNum();curChannel++)
        {
            CONFIG_ENCODE& cfgNew = m_cCfgEncode.getConfig(curChannel);

            tracef("111main chn %d Change fps is %d iResol %d \n",curChannel, 
                cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS,cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution);
            tracef("111extra chn %d Change fps is %d iResol %d \n",curChannel, 
                cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS,cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution);

            g_stNetTransPolicy.usFps[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
            g_stNetTransPolicy.usFps[curChannel + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;

            g_stNetTransPolicy.usResolution[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
            g_stNetTransPolicy.usResolution[curChannel + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;

            g_stNetTransPolicy.usDspRate[curChannel] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
            g_stNetTransPolicy.usDspRate[curChannel + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;

        }

        NetChangeStream(true);
        m_conn->iExtraCfgChangFlag = 1;

    }
        
    return 0;
}
    
int CaptureHelper::EncodeCfgChange(CConfigEncode & cConfigEncode, int & iRet)
{
    CGuard tmpGuard(m_Mutex);
    
    if (m_BAllocFlag == VD_FALSE)
    {
        return 0;
    }

    if(m_conn->sock <= 0)
    {
        return 0;
    }

    bool bNeedChange = false;
    int iChn = 0;
    for(iChn=0;iChn<m_pCaptureM->GetLogicChnNum();iChn++)
    {
        CONFIG_ENCODE& cfgNew = cConfigEncode.getConfig(iChn);
        VIDEO_FORMAT& cfgMainFmt = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat;
        VIDEO_FORMAT& cfgExtFmt = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat;
        
        tracef("main chn %d Change fps is %d iResol %d \n",iChn, cfgMainFmt.nFPS,cfgMainFmt.iResolution);
        tracef("extra chn %d Change fps is %d iResol %d \n",iChn, cfgExtFmt.nFPS,cfgExtFmt.iResolution);

        if((g_stNetTransPolicy.usFps[iChn] != cfgMainFmt.nFPS)
            ||(g_stNetTransPolicy.usFps[iChn + (m_pCaptureM->GetLogicChnNum() )] != cfgExtFmt.nFPS))
        {
            bNeedChange = true;

            tracef("&&&&&&&&&& chnl %d %d %d %d %d  \n",iChn, g_stNetTransPolicy.usFps[iChn]  ,
                cfgMainFmt.nFPS,    g_stNetTransPolicy.usFps[iChn + (m_pCaptureM->GetLogicChnNum() )] ,    cfgExtFmt.nFPS);
        }
        g_stNetTransPolicy.usFps[iChn] = cfgMainFmt.nFPS;
        g_stNetTransPolicy.usFps[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgExtFmt.nFPS;

        if((g_stNetTransPolicy.usResolution[iChn] != cfgMainFmt.iResolution)
            ||(g_stNetTransPolicy.usResolution[iChn + (m_pCaptureM->GetLogicChnNum() )] != cfgExtFmt.iResolution))
        {
            bNeedChange = true;

            tracef("&&&&&&&&&& 111 chnl %d %d %d %d %d  \n",
                iChn, g_stNetTransPolicy.usResolution[iChn],cfgMainFmt.iResolution,
                g_stNetTransPolicy.usResolution[iChn + (m_pCaptureM->GetLogicChnNum() )] ,    cfgExtFmt.iResolution);
        }
        g_stNetTransPolicy.usResolution[iChn] = cfgMainFmt.iResolution;
        g_stNetTransPolicy.usResolution[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgExtFmt.iResolution;

        g_stNetTransPolicy.usDspRate[iChn] = cfgMainFmt.nBitRate;
        g_stNetTransPolicy.usDspRate[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgExtFmt.nBitRate;
        
    }

    if(false == bNeedChange)
    {
           for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
        {  
            if(m_stTransAdapter.usOldDspRate[iChn] != g_stNetTransPolicy.usDspRate[iChn] )
            {
                bNeedChange = true;
                tracef("111&&&&&&&&&& chnl %d %d %d\n",iChn, m_stTransAdapter.usOldDspRate[iChn] ,
                    g_stNetTransPolicy.usDspRate[iChn] );
                break;
            }               
         }
    }

    if(false == bNeedChange)
    {
        return 0;
    }

    tracef("******CaptureHelper::EncodeCfgChange, start to refresh*****************\n");
    for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
    {        
        m_stTransAdapter.bConnTransPolicy[iChn] = g_stNetTransPolicy.bNetTransPolicy;
        
        m_stTransAdapter.emConnTransState[iChn] = g_stNetTransPolicy.emNetTransState;        
                    
        m_stTransAdapter.emAdjustState[iChn] = 0;
        m_stTransAdapter.bConfictFlag[iChn] = 0;
        
        m_stTransAdapter.usOldDspRate[iChn] = g_stNetTransPolicy.usDspRate[iChn];

        int iMaxFps = (m_iMaxFrameRate == 24)?25:30;
        int iResolution = (g_stNetTransPolicy.usResolution[iChn] >=CAPTURE_SIZE_NR)? CAPTURE_SIZE_CIF:(g_stNetTransPolicy.usResolution[iChn]);
        int iCfgFps = (g_stNetTransPolicy.usFps[iChn] >iMaxFps)?iMaxFps:(g_stNetTransPolicy.usFps[iChn]);
        int iMinRate = g_stNetMinRate[iResolution]*iCfgFps/iMaxFps;

        m_stTransAdapter.iConnResolution[iChn]  = iMinRate;

        if(m_stTransAdapter.usOldDspRate[iChn]  < (iMinRate +16))
        {
            m_stTransAdapter.usOldDspRate[iChn]  = iMinRate+ 16;            
        }
        m_stTransAdapter.usNewDspRate[iChn] =m_stTransAdapter.usOldDspRate[iChn] - 8;
        
        m_stTransAdapter.usUpDspRate[iChn] = 0;
        
        m_stTransAdapter.usUpFps[iChn] = 0;
        m_stTransAdapter.iUpCounts[iChn] = 0;
        m_stTransAdapter.iDownCounts[iChn] = 0;
        m_stTransAdapter.iFpsAdapter[iChn] = 0;        

        m_stTransAdapter.usOldFps[iChn] =g_stNetTransPolicy.usFps[iChn];
        m_stTransAdapter.usNewFps[iChn] = g_stNetTransPolicy.usFps[iChn];
    
        #ifdef LINUX
        gettimeofday(&m_stTransAdapter.stLastTime[iChn],0);
        gettimeofday(&m_stTransAdapter.stAdjustTime[iChn],0);
        #else
        m_stTransAdapter.stLastTime[iChn].tv_sec = 0;
        m_stTransAdapter.stLastTime[iChn].tv_usec = SystemGetMSCount()*1000;        
        m_stTransAdapter.stAdjustTime[iChn].tv_sec = 0;
        m_stTransAdapter.stAdjustTime[iChn].tv_usec = SystemGetMSCount()*1000;    
        #endif

        tracef("****CaptureHelper::EncodeCfgChange state change now chn %d dsprate %d,fps %d\n", 
                iChn ,m_stTransAdapter.usOldDspRate[iChn],m_stTransAdapter.usNewFps[iChn] );
    }    

    NetChangeStream(true);
    m_conn->iExtraCfgChangFlag = 1;
    
    return 0;
}


void CaptureHelper::NetChangeStream(bool bExtraToMain)
{
    struct timeval szCurTime;
#ifdef LINUX
    gettimeofday(&szCurTime, NULL);
#else
    szCurTime.tv_sec = 0;
    szCurTime.tv_usec = SystemGetMSCount()*1000; 
#endif
    
    for( int iTmpChn = 0 ; iTmpChn < m_pCaptureM->GetLogicChnNum(); iTmpChn ++)
    {        
        if (bExtraToMain)
        {
            if((1 == m_MonitorState[iTmpChn + (m_pCaptureM->GetLogicChnNum() )])
                &&(1 == m_stTransAdapter.bChangeStreamFlags[iTmpChn] ))
            {        
                stopVideoCapture(iTmpChn,1,1);

                startVideoCapture(iTmpChn,0);
            
                m_stTransAdapter.bChangeStreamFlags[iTmpChn] = 0;
            }
        }
        else
        {
            if(1 == m_MonitorState[iTmpChn])
            {        
                stopVideoCapture(iTmpChn,0,1);

                startVideoCapture(iTmpChn,1);
            
                m_stTransAdapter.bChangeStreamFlags[iTmpChn] = 1;
            }
        }

        memcpy(&m_stTransAdapter.stAdjustTime[iTmpChn],&szCurTime,sizeof(struct timeval));
        memcpy(&m_stTransAdapter.stAdjustTime[iTmpChn + (m_pCaptureM->GetLogicChnNum() )],&szCurTime,sizeof(struct timeval));
    }

    m_conn->iExtraCfgChangFlag = 1;
}


void CaptureHelper::NetChangeMainToExtra(int iChn, int iStream)
{
    if(0 ==  g_stNetTransPolicy.bExtStream 
        ||1 == m_conn->iChangMainToExtraFlag
        || (iChn + (m_pCaptureM->GetLogicChnNum() )*iStream >=  (m_pCaptureM->GetLogicChnNum() )))
    {
        return;
    }

    CAPTURE_DSPINFO szDspInfo;
    memset(&szDspInfo,0, sizeof(CAPTURE_DSPINFO));
    GetDspInfo(&szDspInfo);

    int iMaxFps =  (m_iMaxFrameRate == 24)?25 :30;
    m_cCfgEncode.update();

    uint uiGroupPower = 0; 
    uint uiCurPower  = 0;

    for (int ii = 0; ii < szDspInfo.nMaxSupportChannel; ii++)
    {
        uint uiTypePower = 0;
        CONFIG_ENCODE &cfg=m_cCfgEncode.getConfig( ii);

        for(int kk = ENCODE_TYPE_TIM; kk < ENCODE_TYPE_NUM; kk++)
        {        
            uiCurPower = calculatePower(cfg.dstMainFmt[kk].vfFormat.iResolution, cfg.dstMainFmt[kk].vfFormat.nFPS);
            if(uiTypePower < uiCurPower)    
            {
                uiTypePower = uiCurPower;
            }
            //tracef("!!!!!!!!power %ld\n",uiTypePower);
        }        

        uiGroupPower += uiTypePower;
    }
    uint uiLeftPower = 0;
    if(szDspInfo.nMaxEncodePower >  uiGroupPower)
    {
        uiLeftPower = szDspInfo.nMaxEncodePower -  uiGroupPower;
    }

    int iExtraRel = CAPTURE_SIZE_CIF;
    int iExtraFps  = 6;
    while( 1)
    {
        uiGroupPower = 0; //重新计算，初步认定dsp可以实现的编码策略
        
        for (int ii = 0; ii < szDspInfo.nMaxSupportChannel; ii++)
        {
            uiCurPower = calculatePower(CAPTURE_SIZE_CIF, iMaxFps);
            uiGroupPower += uiCurPower;
        }

        if(uiGroupPower < uiLeftPower )
        {
            iExtraRel = CAPTURE_SIZE_CIF;
            iExtraFps = iMaxFps;
            break;
        }

        if(iMaxFps <= 6)
        {
            iExtraRel = CAPTURE_SIZE_CIF;
            iExtraFps = iMaxFps;
            break;
        }

        iMaxFps = iMaxFps/2;
    }

    if(NET_TRANS_RATE == g_stNetTransPolicy.emNetTransState)
    {
        if(iExtraFps < 25)
        {            
            iExtraRel = CAPTURE_SIZE_QCIF;
            iExtraFps = (m_iMaxFrameRate == 24)?25:30;
        }
    }
            
    iMaxFps = (m_iMaxFrameRate == 24)?25:30;
    int iResolution = (iExtraRel < 0 ||iExtraRel>=CAPTURE_SIZE_NR)? CAPTURE_SIZE_CIF:(iExtraRel);
    int iCfgFps = (iExtraFps >iMaxFps)?iMaxFps:(iExtraFps);
    int iChangdRate = g_stNetMaxRate[iResolution]*iCfgFps/iMaxFps;

    tracef("!!!!!!rate %d fps %d rel %d !!!!\n",iChangdRate,iExtraFps,iExtraRel);

    int iStreamType = CHL_2END_T;
    for(int iTmpChn = 0 ; iTmpChn < m_pCaptureM->GetLogicChnNum(); iTmpChn ++)
    {
        g_stNetTransPolicy.usDspRate[iTmpChn + (m_pCaptureM->GetLogicChnNum() )*iStreamType] =  iChangdRate;
        g_stNetTransPolicy.usFps[iTmpChn + (m_pCaptureM->GetLogicChnNum() )*iStreamType ]  = iExtraFps;
        g_stNetTransPolicy.usResolution[iTmpChn + (m_pCaptureM->GetLogicChnNum() )*iStreamType]= iExtraRel;

        ICapture *pChnDevCapture = ICapture::instance(iTmpChn);
        const CAPTURE_FORMAT *pCfgFmtTmp = pChnDevCapture->GetFormat(iStreamType);
        if(NULL == pCfgFmtTmp) 
        {
            continue;
        }

        CAPTURE_FORMAT szCfgFmt ;
        memcpy(&szCfgFmt, pCfgFmtTmp, sizeof(CAPTURE_FORMAT));

        szCfgFmt.ImageSize = iExtraRel;
        szCfgFmt.FramesPerSecond = iExtraFps;
        szCfgFmt.BitRate = iChangdRate;
        szCfgFmt.BitRateControl = CAPTURE_BRC_CBR;

        szCfgFmt.AVOption    = 1;
        szCfgFmt.AVOption |= BITMSK(1);        

        pChnDevCapture->SetFormat(&szCfgFmt, iStreamType);     

    }

    m_conn->iChangMainToExtraFlag = 1;
}


/*
*数字通道网络检测函数，现在只做简单处理
*/
int  CaptureHelper::DigiChNetStateDetect(int chn,int stream )
{
    struct timeval szCurTime;
#ifdef LINUX
    gettimeofday(&szCurTime, NULL);
#else
    szCurTime.tv_sec = 0;
    szCurTime.tv_usec = SystemGetMSCount()*1000; 
#endif
    int iChannelType = chn + (m_pCaptureM->GetLogicChnNum() )*stream;
 
    int iDiffTime = (szCurTime.tv_sec - m_stFPSAdapter.stLastTime[iChannelType].tv_sec)*1000 
                    + (szCurTime.tv_usec - m_stFPSAdapter.stLastTime[iChannelType].tv_usec)/1000;
        
    if ( iDiffTime < 30 && iDiffTime >= 0)
    {
        return 0;
    }
    
    memcpy(&m_stFPSAdapter.stLastTime[iChannelType],&szCurTime,sizeof(struct timeval));

    /* 应该按照配置的码率和分辨率比较科学，这里简单处理 */
    int iListAdapt = 8;
    if (g_nLogicNum > g_nCapture + 8)
    {
         iListAdapt = 2;
    }
    else    if (g_nLogicNum > g_nCapture + 4)
    {
         iListAdapt = 4;
    }

    if( (int)(m_conn->DataTransferQueue[chn+1].totalSize) > 64*1024*iListAdapt)
    {
       if (m_stFPSAdapter.emAdjustState[iChannelType] != IFrame_None)
       {
            printf("DigiChNetStateDetect():change to ##IFrame_None chn = %d,totalSize = %lu\r\n",
                chn,m_conn->DataTransferQueue[chn+1].totalSize);
            m_stFPSAdapter.emAdjustState[iChannelType] = IFrame_None;
       }
    }
    else if( (int)(m_conn->DataTransferQueue[chn+1].totalSize) > 32*1024*iListAdapt)
    {
       if (m_stFPSAdapter.emAdjustState[iChannelType] != PFrame_None)
       {
            printf("DigiChNetStateDetect():change to ##PFrame_None chn = %d,totalSize = %lu\r\n",
                chn,m_conn->DataTransferQueue[chn+1].totalSize);
            m_stFPSAdapter.emAdjustState[iChannelType] = PFrame_None;
       }
    }
    else if( (int)(m_conn->DataTransferQueue[chn+1].totalSize) < 8*1024*iListAdapt )
    {   
        //为了减少cpu占用，尽量让数字通道只发I帧
        if (m_stFPSAdapter.emAdjustState[iChannelType] != PFrame_all)
        {
            printf("DigiChNetStateDetect():change to ##PFrame_all chn = %d,totalSize = %lu\r\n",
                chn,m_conn->DataTransferQueue[chn+1].totalSize);
            m_stFPSAdapter.emAdjustState[iChannelType] = PFrame_all;
        }
    }

    return 0;
}

/*
*该函数是网络状态自检函数，为网络调整基础函数
*窄带网络传输策略处理核心函数，请不要随意修改
*/
int  CaptureHelper::NetStateDetect(int chn,int stream )
{
    struct timeval szCurTime;
#ifdef LINUX
    gettimeofday(&szCurTime, NULL);
#else
    szCurTime.tv_sec = 0;
    szCurTime.tv_usec = SystemGetMSCount()*1000; 
#endif
    int iChannelType = chn + (m_pCaptureM->GetLogicChnNum() )*stream;
     int iDspRateComp =  0;

	if(m_MonitorState[chn] == TRUE)
	{
		iChannelType = chn;
		iDspRateComp = m_stTransAdapter.usNewDspRate[iChannelType];
	}

	int tmp = m_pCaptureM->GetLogicChnNum();
	if(m_MonitorState[chn + tmp] == TRUE)
	{
		iDspRateComp += m_stTransAdapter.usNewDspRate[tmp];
	}
	
    int iDiffTime = (szCurTime.tv_sec - m_stFPSAdapter.stLastTime[iChannelType].tv_sec)*1000 
                    + (szCurTime.tv_usec - m_stFPSAdapter.stLastTime[iChannelType].tv_usec)/1000;
    if(  (1 ==  g_stNetTransPolicy.bNetTransPolicy && (iDiffTime >= 0 && iDiffTime < 2000))
        || (0 ==  g_stNetTransPolicy.bNetTransPolicy &&  (iDiffTime >= 0 && iDiffTime < 300)))
    {
        return 0;
    }
    
    if(0 == iDspRateComp)
    {
        iDspRateComp =  m_stTransAdapter.usOldDspRate[iChannelType];
    }
#ifndef WIN32
    /* 内存不够的设备，队列长度小一点 */
    if (g_PacketManager.GetBufferSize() < 20000 && g_nCapture>=4)
    {
        iDspRateComp = iDspRateComp/4;
    }  
#endif   
    if(m_stTransAdapter.usNewFps[iChannelType]  < 12)
    {
        if(iDspRateComp < 128)
        {
            iDspRateComp = 128;
        }
    }

    int iNetDataReConflict = 0;      
    if(0 ==  g_stNetTransPolicy.bNetTransPolicy)
    {
//ZHY 窄带宽算法 增加对200万以上分辨率的支持 20150527
		//_printd("iChannelType=%d, usResolution=%d", iChannelType, g_stNetTransPolicy.usResolution[iChannelType]);
        //对720P ，暂时写成D1的2倍
        if(CAPTURE_SIZE_8K == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 36*64*8*1024;
        }        
        if(CAPTURE_SIZE_4K == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 10*64*8*1024;
        }        
        if(CAPTURE_SIZE_800W == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 9*64*8*1024;
        }        
        if(CAPTURE_SIZE_600W == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 7*64*8*1024;
        }        
        if(CAPTURE_SIZE_500W == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 6*64*8*1024;
        } 
		if (CAPTURE_SIZE_400W == g_stNetTransPolicy.usResolution[iChannelType])
		{
			iNetDataReConflict = 5*64*8*1024;
		}
        if(CAPTURE_SIZE_300W == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 3*64*8*1024;
        }        
        if(CAPTURE_SIZE_1080P == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 2*64*8*1024;
        }        
        if(CAPTURE_SIZE_1280_960 == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 2*64*8*1024;
        }        
        if(CAPTURE_SIZE_720P == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 64*8*1024;
        }
        if(CAPTURE_SIZE_D1 == g_stNetTransPolicy.usResolution[iChannelType])
        {            
            iNetDataReConflict = 32*8*1024;
        }
        else  if((CAPTURE_SIZE_HD1 == g_stNetTransPolicy.usResolution[iChannelType])
                 ||(CAPTURE_SIZE_BCIF == g_stNetTransPolicy.usResolution[iChannelType]))
        {             
            iNetDataReConflict = 16*8*1024;
        }
    }
#ifndef WIN32
    /* 内存不够的设备，队列长度小一点 */
    if (g_PacketManager.GetBufferSize() < 20000 && g_nCapture>=4)
    {
        iNetDataReConflict = iNetDataReConflict/4;
    }
#endif
    m_conn->DataTransferQueue[chn+1].CurrentDspRate  = iDspRateComp;

    //!队列缓冲长度大于dsp速率的3/10时
    if( (int)(m_conn->DataTransferQueue[chn+1].totalSize*8) > ((iDspRateComp*1024*3)/10) + iNetDataReConflict)
    {
        
        if( (int)(m_conn->DataTransferQueue[chn+1].totalSize*8) > ((iDspRateComp*1024*4)/10) + iNetDataReConflict)
        {
            m_stTransAdapter.bConfictFlag[iChannelType] = 1;    //!大于4/10时,要置位冲突标志
        }

        if( (int)(m_conn->DataTransferQueue[chn+1].totalSize*8) > ((iDspRateComp*1024*15)/10 + iNetDataReConflict))
        {
            printf(" !!!111 CaptureHelper::NetStateDetect():chn %d,manager is %d Qsize %lu iDspRateComp = %d\n\n",
                chn,m_stFPSAdapter.emAdjustState[iChannelType],m_conn->DataTransferQueue[chn+1].totalSize,iDspRateComp);

            m_stFPSAdapter.emAdjustState[iChannelType] = IFrame_None;    //!大于15/10时, 丢I帧
        }
        else  if( (int)(m_conn->DataTransferQueue[chn+1].totalSize*8) > ((iDspRateComp*1024*10)/10) + iNetDataReConflict)
        {
            printf(" !!!2222CaptureHelper::NetStateDetect():chn %d,manager is %d Qsize %lu iDspRateComp = %d\n\n",
                chn,m_stFPSAdapter.emAdjustState[iChannelType],m_conn->DataTransferQueue[chn+1].totalSize,iDspRateComp);
            m_stFPSAdapter.emAdjustState[iChannelType] = PFrame_None;
        }
        else if(m_stFPSAdapter.emAdjustState[iChannelType] == IFrame_None)
        {
            printf(" !!!333CaptureHelper::NetStateDetect():chn %d,manager is %d Qsize %lu iDspRateComp = %d\n\n",
                chn,m_stFPSAdapter.emAdjustState[iChannelType],m_conn->DataTransferQueue[chn+1].totalSize,iDspRateComp);
            return 0;
        }
        else if(m_stFPSAdapter.emAdjustState[iChannelType] == PFrame_None)
        {
            if( (int)(m_conn->DataTransferQueue[chn+1].totalSize*8) < ((iDspRateComp*1024*8)/10) + iNetDataReConflict)
            {
                return 0;
            }
            
            printf(" !!!444 CaptureHelper::NetStateDetect():chn %d,manager is %d Qsize %lu iDspRateComp = %d\n\n",
                chn,m_stFPSAdapter.emAdjustState[iChannelType],m_conn->DataTransferQueue[chn+1].totalSize,iDspRateComp);
            
            m_stFPSAdapter.emAdjustState[iChannelType] = IFrame_None;
        }
        else
        {
            //!计算权重:由当前的队列长度比上dsp速度的1/10的倍数表示
            unsigned int uiWeight = (unsigned int)((m_conn->DataTransferQueue[chn+1].totalSize*8)/((iDspRateComp*1024)/10));
            uiWeight = uiWeight<3?1:(uiWeight-2);
            int iChannels = 0;
            //!根据权重,调整发送模式应该处于的级别
            while(m_stFPSAdapter.emAdjustState[iChannelType] < PFrame_None)
            {
                m_stFPSAdapter.emAdjustState[iChannelType] += 1;
                iChannels++;
                if(iChannels >=  (int)uiWeight)
                {
                    break;
                }
            }
        }
    }
    else if((m_conn->DataTransferQueue[chn+1].totalSize  < 4*1024)    //!队列缓冲小于4k
        ||((m_conn->DataTransferQueue[chn+1].totalSize*8) <= (unsigned long)((iDspRateComp*1024*2)/10)))    //!队列缓冲小于dsp速率的2/10
    {    
        //!发送质量升高一个档次
        if(m_stFPSAdapter.emAdjustState[iChannelType] == PFrame_all)
        {
            return 0;
        }
        else if(m_stFPSAdapter.emAdjustState[iChannelType] > PFrame_all)
        {
            m_stFPSAdapter.emAdjustState[iChannelType] -= 1;
        }
    }
    else
    {
        return 0;
    }
    
    memcpy(&m_stFPSAdapter.stLastTime[iChannelType],&szCurTime,sizeof(struct timeval));    
    if (IFrame_None == m_stFPSAdapter.emAdjustState[iChannelType] )
    {
        tracef(" ###CaptureHelper::NetStateDetect  chn %d,emAdjustState is %d Qsize %d \n\n",
            chn,m_stFPSAdapter.emAdjustState[iChannelType], (int)(m_conn->DataTransferQueue[chn+1].totalSize) );
    }


    if((m_stTransAdapter.bConnTransPolicy[iChannelType] == g_stNetTransPolicy.bNetTransPolicy)
        &&(m_stTransAdapter.emConnTransState[iChannelType]  ==  g_stNetTransPolicy.emNetTransState)
        &&(0 ==m_conn->iExtraCfgChangFlag))
    {
    }
    else
    {
        if(m_stTransAdapter.bConnTransPolicy[iChannelType] != g_stNetTransPolicy.bNetTransPolicy)
        {
            m_conn->iSocketNeedChgFlag = 1;
        }
        m_conn->iExtraCfgChangFlag = 0;
        m_cCfgEncode.update();
        int iChn = 0;
        for(iChn=0;iChn<m_pCaptureM->GetLogicChnNum();iChn++)
        {
            CONFIG_ENCODE& cfgNew = m_cCfgEncode.getConfig(iChn);
            g_stNetTransPolicy.usFps[iChn] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
            g_stNetTransPolicy.usFps[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;

            g_stNetTransPolicy.usResolution[iChn] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
            g_stNetTransPolicy.usResolution[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;

            g_stNetTransPolicy.usDspRate[iChn] = cfgNew.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
            g_stNetTransPolicy.usDspRate[iChn + (m_pCaptureM->GetLogicChnNum() )] = cfgNew.dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
        }
        
        for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
        {        
            int  iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                int iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))?0 : 1;

            #ifdef NOT_SUPPORT_EXT_STREAM
            //not suport ext stream on vs2500 james.xu    
            if(iStreamType > 0)
            {
                tracef("#####not support extrat stream!!!! iStreamType:%d\r\n",iStreamType);
                continue;
            }
            #endif

            if (m_MonitorState[iChn] ==0)
            {
                continue;
            }
            
            m_stTransAdapter.bConnTransPolicy[iChn] = g_stNetTransPolicy.bNetTransPolicy;
            m_stTransAdapter.emConnTransState[iChn] = g_stNetTransPolicy.emNetTransState;            
            m_stTransAdapter.emAdjustState[iChn] = 0;
            m_stTransAdapter.bConfictFlag[iChn] = 0;

            ICapture *pChnDevCapture = ICapture::instance(iChnNex);
            const CAPTURE_FORMAT *pCfgFmtTmp = pChnDevCapture->GetFormat(iStreamType);
            if(NULL == pCfgFmtTmp) 
            {
                continue;
            }

            CAPTURE_FORMAT szCfgFmt ;
            memcpy(&szCfgFmt, pCfgFmtTmp, sizeof(CAPTURE_FORMAT));
            m_stTransAdapter.usOldDspRate[iChn] = g_stNetTransPolicy.usDspRate[iChn];

            int iMaxFps = (m_iMaxFrameRate == 24)?25:30;
            int iResolution = (g_stNetTransPolicy.usResolution[iChn] >=CAPTURE_SIZE_NR)? CAPTURE_SIZE_CIF:(g_stNetTransPolicy.usResolution[iChn]);
            int iCfgFps = (g_stNetTransPolicy.usFps[iChn] >iMaxFps)?iMaxFps:(g_stNetTransPolicy.usFps[iChn]);
            int iMinRate = g_stNetMinRate[iResolution]*iCfgFps/iMaxFps;
            m_stTransAdapter.iConnResolution[iChn]  = iMinRate;
            if(m_stTransAdapter.usOldDspRate[iChn]  < (iMinRate +16))
            {
                m_stTransAdapter.usOldDspRate[iChn]  = iMinRate+ 16;            
            }
            m_stTransAdapter.usNewDspRate[iChn] =m_stTransAdapter.usOldDspRate[iChn] - 8;
            m_stTransAdapter.usUpDspRate[iChn] = 0;
            m_stTransAdapter.usUpFps[iChn] = 0;
            m_stTransAdapter.iUpCounts[iChn] = 0;
            m_stTransAdapter.iDownCounts[iChn] = 0;
            m_stTransAdapter.iFpsAdapter[iChn] = 0;        
            m_stTransAdapter.usOldFps[iChn] =g_stNetTransPolicy.usFps[iChn];
            m_stTransAdapter.usNewFps[iChn] = g_stNetTransPolicy.usFps[iChn];

            #ifdef LINUX
            gettimeofday(&m_stTransAdapter.stLastTime[iChn],0);
            gettimeofday(&m_stTransAdapter.stAdjustTime[iChn],0);
            #else
            m_stTransAdapter.stLastTime[iChn].tv_sec = 0;
            m_stTransAdapter.stLastTime[iChn].tv_usec = SystemGetMSCount()*1000;        
            m_stTransAdapter.stAdjustTime[iChn].tv_sec = 0;
            m_stTransAdapter.stAdjustTime[iChn].tv_usec = SystemGetMSCount()*1000;    
            #endif

            if((1 == g_stNetTransPolicy.bHaveDisk)    &&(CHL_MAIN_T ==  iStreamType))
            {
                continue;
            }

            szCfgFmt.ImageSize = g_stNetTransPolicy.usResolution[iChn];
            szCfgFmt.BitRateControl = CAPTURE_BRC_CBR;
            szCfgFmt.FramesPerSecond =  m_stTransAdapter.usNewFps[iChn] ;
            szCfgFmt.BitRate= m_stTransAdapter.usNewDspRate[iChn] ;
            //szCfgFmt.AVOption    = 1;
            //szCfgFmt.AVOption |= BITMSK(1);    // 这里会自动打开现场声音，和网络组讨论后，需要屏蔽。        
            
            pChnDevCapture->SetFormat(&szCfgFmt, iStreamType);     

            tracef("CaptureHelper::NetStateDetect change now chn %d dsprate %d,fps %d\n", 
                iChn ,m_stTransAdapter.usOldDspRate[iChn],m_stTransAdapter.usNewFps[iChn] );

        }    
    }
    return 0;
}
/*
*网络自适应处理函数，与NetStateDetect 配套使用
*窄带网络传输策略处理核心函数，请不要随意修改
*/
int  CaptureHelper::NetTransAdpater(int chn,int stream )
{
    if(0 == g_stNetTransPolicy.bNetTransPolicy)
    {
        return 0;
    }

    struct timeval time_now;
#ifdef LINUX
    gettimeofday(&time_now, NULL);
#else
    time_now.tv_sec = 0;
    time_now.tv_usec = SystemGetMSCount()*1000; 
#endif
    int chnex = chn + (m_pCaptureM->GetLogicChnNum() )*stream;
    int difftime = (time_now.tv_sec - m_stTransAdapter.stLastTime[chnex].tv_sec)*1000 
    + (time_now.tv_usec - m_stTransAdapter.stLastTime[chnex].tv_usec)/1000;

    if(difftime < 6000)
    {
        return 0;
    }
    
    CAPTURE_FORMAT cptChnFmt ;
    const CAPTURE_FORMAT *cptChnFmtTmp = NULL;
    ICapture *pChnDevCapture =NULL;
    pChnDevCapture = ICapture::instance(chn);
    cptChnFmtTmp = pChnDevCapture->GetFormat(stream);
    if(NULL == cptChnFmtTmp) 
    {
        return 0;
    }
    
    int iChn= 0;    
    int iStreamType =  0;
    int iChnNex = 0;

    int iOldDspRate = 0;
    int iTmpState = 0;
    int iOldFps = 0;
    int iStateFlag = 0;
    int iCurrtDspRate = 0;
    int iCurrtDspFps = 0;
    int iResetDspFlag = 0;
    
    for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2;iChn++)
    {
        if(m_MonitorState[iChn] == 1)
            {                
            if((m_stFPSAdapter.emAdjustState[iChn]  != PFrame_all)
                ||(1 == m_stTransAdapter.bConfictFlag[iChn]))
            {
                iStateFlag = 1;
                break;
            }
        }
    }
/* -------------------流畅性优先---------------------------------*/
    if(NET_TRANS_RATE == m_stTransAdapter.emConnTransState[chnex] )
    {        
        if(1 == iStateFlag )
        {
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(m_MonitorState[iChn] == 1 
                && NET_TRANS_RATE == m_stTransAdapter.emConnTransState[iChn] )
                {
                    difftime = (time_now.tv_sec - m_stTransAdapter.stLastTime[iChn].tv_sec)*1000 
                    + (time_now.tv_usec - m_stTransAdapter.stLastTime[iChn].tv_usec)/1000;
                    if(difftime <6000)
                    {
                        return 0;
                    }
                    else
                    {
                        m_stTransAdapter.bConfictFlag[iChn] = 0;
                    }
                }
            }
            
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))? 0 : 1;

                if(m_MonitorState[iChn] == 0)
                {
                    continue;
                }
                /* --------DSP主副码流调整------------*/
                if((1 == g_stNetTransPolicy.bHaveDisk )
                &&(CHL_MAIN_T== iStreamType)
                &&(iChn < (m_pCaptureM->GetLogicChnNum() )))
                {
                    if(m_stFPSAdapter.emAdjustState[iChn] < PFrame_HHalf)
                    {
                        continue;
                    }
                            
                    difftime = (time_now.tv_sec - m_stTransAdapter.stAdjustTime[chnex].tv_sec)*1000 
                    + (time_now.tv_usec - m_stTransAdapter.stAdjustTime[chnex].tv_usec)/1000;

                    if(difftime < 30000)
                    {
                        continue;
                    }                    

                    NetChangeMainToExtra(iChnNex, iStreamType);
                    continue;
                }

                if(1 == m_conn->iExtraCfgChangFlag )
                {
                    continue;
                }

                /* --------DSP码率调整-----------*/
                if(NET_TRANS_RATE== m_stTransAdapter.emConnTransState[iChn] )
                {                
                    pChnDevCapture = ICapture::instance(iChnNex);
                    cptChnFmtTmp = pChnDevCapture->GetFormat(iStreamType);
                    if(NULL == cptChnFmtTmp) 
                    {
                        continue;
                    }
                    memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));
                    
                    iResetDspFlag = 0;
                    iOldDspRate =  m_stTransAdapter.usNewDspRate[iChn];
                    iCurrtDspRate = cptChnFmt.BitRate;

                    if(iOldDspRate != iCurrtDspRate)
                    {
                        iResetDspFlag = 1;
                    }

                    if((m_stTransAdapter.usNewDspRate[iChn] <=  m_stTransAdapter.iConnResolution[iChn] )
                        &&(0 == iResetDspFlag))
                    {
                        continue;
                    }

                    if(0 == iResetDspFlag )
                    {                    
                        m_stTransAdapter.iUpCounts[iChn] = 0;
                        if(NET_ADJUST_STATE_UP == m_stTransAdapter.emAdjustState[iChn] )
                        {
                            if(0 == m_stTransAdapter.usUpDspRate[iChn] )
                            {
                                m_stTransAdapter.usUpDspRate[iChn] =   m_stTransAdapter.usNewDspRate[iChn] - (m_stTransAdapter.usOldDspRate[iChn]*9)/100;
                                tracef("****************chn %d up dsprate is %d !!!!\n",iChn, m_stTransAdapter.usUpDspRate[iChn] );
                            }
                        }

                        if(m_stFPSAdapter.emAdjustState[chnex] >= PFrame_HHalf)
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  =  (m_stTransAdapter.usNewDspRate[iChn] *4)/10 ;    
                        }
                        else  if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_Half)
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  =  (m_stTransAdapter.usNewDspRate[iChn] *5)/10 ;    
                        }
                        else if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_Quarter)
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  =  (m_stTransAdapter.usNewDspRate[iChn] *6)/10 ;    
                        }
                        else  if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_DQuarter)
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  =  (m_stTransAdapter.usNewDspRate[iChn] *7)/10 ;    
                        }    
                        else
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  =  (m_stTransAdapter.usNewDspRate[iChn] *8)/10 ;    
                        }
                        
                        m_stTransAdapter.iDownCounts[iChn]++;
                        if(m_stTransAdapter.iDownCounts[iChn] >= 5)
                        {
                            m_stTransAdapter.usUpDspRate[iChn] = 0;
                        }
                        
                        if(m_stTransAdapter.usUpDspRate[iChn] > 0)
                        {
                            m_stTransAdapter.usNewDspRate[iChn]  = m_stTransAdapter.usUpDspRate[iChn] ;
                            tracef("*********************chn %d  downset dsprate to updsprate %d !!!!\n",
                                iChn,m_stTransAdapter.usNewDspRate[iChn]);
                        }
                    }

                    if(m_stTransAdapter.usNewDspRate[iChn] < m_stTransAdapter.iConnResolution[iChn] )
                    {
                        m_stTransAdapter.usNewDspRate[iChn] =  m_stTransAdapter.iConnResolution[iChn] ;
                    }

                    if(m_stTransAdapter.usNewDspRate[iChn] > m_stTransAdapter.usOldDspRate[iChn])
                    {
                        m_stTransAdapter.usNewDspRate[iChn]  = m_stTransAdapter.usOldDspRate[iChn];
                    }    
                    
                    if((iOldDspRate !=  m_stTransAdapter.usNewDspRate[iChn])
                        ||(1 == iResetDspFlag))
                    {
                        cptChnFmt.BitRate =  m_stTransAdapter.usNewDspRate[iChn] ;
                        pChnDevCapture->SetFormat(&cptChnFmt, iStreamType);     
                    }
                    
                    memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(struct timeval));
                    m_stTransAdapter.emAdjustState[iChn] = NET_ADJUST_STATE_DOWN;
                    m_stTransAdapter.bConfictFlag[iChn] = 0;    

                    tracef("*******chn %d start to just down  %d !!!!\n",iChn,m_stTransAdapter.usNewDspRate[iChn]  );    
                }
            }
        }
        //else if(m_stFPSAdapter.emAdjustState[chnex]  == PFrame_all)
        else if(0 == iStateFlag )
        {
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(m_MonitorState[iChn] == 1)
                {
                    if(NET_TRANS_RATE == m_stTransAdapter.emConnTransState[iChn] )
                    {
                        difftime = (time_now.tv_sec - m_stTransAdapter.stLastTime[iChn].tv_sec)*1000 
                        + (time_now.tv_usec - m_stTransAdapter.stLastTime[iChn].tv_usec)/1000;
                        if(difftime <12000)
                        {
                            return 0;
                        }
                        else
                        {
                            m_stTransAdapter.bConfictFlag[iChn] = 0;    
                        }
                    }
                }
            }

            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2;iChn++)
            {
                iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))? 0 : 1;
                if(m_MonitorState[iChn] == 1)
                    {                
                    if((m_conn->DataTransferQueue[iChnNex+1].totalSize*8) > (unsigned long)((m_stTransAdapter.usNewDspRate[iChn]*1024*1)/10))
                    {
                        tracef("CaptureHelper::NetQualityRateAdpate  chn %d,  buffer have data %lu.\n",iChn, m_conn->DataTransferQueue[iChnNex+1].totalSize);
                        break;
                    }
                }
            }

            if(iChn<(m_pCaptureM->GetLogicChnNum() )*2)
            {
                for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
                {
                    if(m_MonitorState[iChn] == 1)
                    {                                                
                        if(NET_TRANS_RATE== m_stTransAdapter.emConnTransState[iChn] )
                        {
                            memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(struct timeval));
                            m_stTransAdapter.bConfictFlag[iChn] = 0;                            
                        }
                    }
                }
                return 0;
            } 
            
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))?0 : 1;

                if(m_MonitorState[iChn] == 0)
                    {
                        continue;
                  }

                  if((1 == g_stNetTransPolicy.bHaveDisk )
                     &&(CHL_MAIN_T== iStreamType)
                     &&(iChn < (m_pCaptureM->GetLogicChnNum() )))
                       {
                    continue;
                  }

                /* --------DSP码率调整-----------*/
                if(NET_TRANS_RATE == m_stTransAdapter.emConnTransState[iChn] )
                {
                    pChnDevCapture = ICapture::instance(iChnNex);
                    cptChnFmtTmp = pChnDevCapture->GetFormat(iStreamType);
                    if(NULL == cptChnFmtTmp) 
                    {
                        continue;
                    }        

                    memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));

                    iResetDspFlag = 0;
                    iOldDspRate =  m_stTransAdapter.usNewDspRate[iChn];
                    iCurrtDspRate = cptChnFmt.BitRate;

                    if(iOldDspRate != iCurrtDspRate)
                    {
                        iResetDspFlag = 1;
                    }

                    if((m_stTransAdapter.usNewDspRate[iChn] >= m_stTransAdapter.usOldDspRate[iChn])
                            &&(0 == iResetDspFlag))
                    {
                        if((CHL_2END_T== iStreamType)
                             &&(iChn >=  (m_pCaptureM->GetLogicChnNum() )))
                                   {
                                       difftime = (time_now.tv_sec - m_stTransAdapter.stAdjustTime[chnex].tv_sec)*1000 
                                + (time_now.tv_usec - m_stTransAdapter.stAdjustTime[chnex].tv_usec)/1000;

                                if(difftime < 360000)
                                {
                                    continue;
                                }
                                       
                                
                                if(1 == m_stTransAdapter.bChangeStreamFlags[iChnNex])
                                {
                                    if(iChnNex + (m_pCaptureM->GetLogicChnNum() )*iStreamType >=  (m_pCaptureM->GetLogicChnNum() ))
                                    {
                                        m_conn->iChangMainToExtraFlag = 2;
                                    }    
                                }
                                
                                continue;
                             }
                        
                        continue;
                    }
                    
                    if(0 == iResetDspFlag )
                    {                    
                        m_stTransAdapter.iDownCounts[iChn] = 0;    
                    
                        m_stTransAdapter.usNewDspRate[iChn]  = m_stTransAdapter.usNewDspRate[iChn] + (m_stTransAdapter.usOldDspRate[iChn]*5)/100 ;    

                        m_stTransAdapter.iUpCounts[iChn] ++;
                        if(m_stTransAdapter.iUpCounts[iChn] >= 15)
                        {
                            m_stTransAdapter.usUpDspRate[iChn] = 0;
                        }
                        
                        if(m_stTransAdapter.usUpDspRate[iChn] > 0)
                        {
                            if(m_stTransAdapter.usNewDspRate[iChn]  > m_stTransAdapter.usUpDspRate[iChn] )
                            {
                                m_stTransAdapter.usNewDspRate[iChn]  = m_stTransAdapter.usUpDspRate[iChn];
                                tracef("*********************chn %d  upset dsprate to updsprate %d !!!!\n",iChn,m_stTransAdapter.usNewDspRate[iChn]);
                            }
                        }
                    }

                    if(m_stTransAdapter.usNewDspRate[iChn] < m_stTransAdapter.iConnResolution[iChn] )
                    {
                        m_stTransAdapter.usNewDspRate[iChn] =  m_stTransAdapter.iConnResolution[iChn] ;
                    }

                    if(m_stTransAdapter.usNewDspRate[iChn] > m_stTransAdapter.usOldDspRate[iChn])
                    {
                        m_stTransAdapter.usNewDspRate[iChn]  = m_stTransAdapter.usOldDspRate[iChn];
                    }    

                    if((iOldDspRate !=  m_stTransAdapter.usNewDspRate[iChn])
                        ||(1 == iResetDspFlag))
                    {
                           cptChnFmt.BitRate =  m_stTransAdapter.usNewDspRate[iChn] ;
                        pChnDevCapture->SetFormat(&cptChnFmt, iStreamType);     
                    }
                    
                    memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(struct timeval));
                    m_stTransAdapter.emAdjustState[iChn] = NET_ADJUST_STATE_UP;
                    m_stTransAdapter.bConfictFlag[iChn] = 0;
                         
                    tracef("*******chn %d start to just up  %d !!!!\n", iChn,m_stTransAdapter.usNewDspRate[iChn]  );
                
                }
            }
        }        
    }
/* -------------------质量优先---------------------------------*/
    else  if(NET_TRANS_QUALITY == m_stTransAdapter.emConnTransState[chnex] )
    {        
        if(1 == iStateFlag )
        {
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(m_MonitorState[iChn] == 1)
                {
                    if(NET_TRANS_QUALITY== m_stTransAdapter.emConnTransState[iChn] )
                    {
                        difftime = (time_now.tv_sec - m_stTransAdapter.stLastTime[iChn].tv_sec)*1000 
                        + (time_now.tv_usec - m_stTransAdapter.stLastTime[iChn].tv_usec)/1000;
                        if(difftime <6000)
                        {
                            return 0;
                        }
                        else
                        {
                            m_stTransAdapter.bConfictFlag[iChn] = 0;
                        }
                    }
                }
            }
        
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(NET_TRANS_QUALITY== m_stTransAdapter.emConnTransState[iChn] )
                {
                    iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                    iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))?0 : 1;
                    if(m_MonitorState[iChn] == 0)
                    {
                        continue;
                    }
                    
                    /* --------DSP主副码流调整-----------*/
                    if((1 == g_stNetTransPolicy.bHaveDisk )
                        &&(CHL_MAIN_T== iStreamType)
                        &&(iChn < (m_pCaptureM->GetLogicChnNum() )))
                    {
                        if(m_stFPSAdapter.emAdjustState[iChn] < PFrame_HHalf)
                        {
                            continue;
                        }

                        difftime = (time_now.tv_sec - m_stTransAdapter.stAdjustTime[chnex].tv_sec)*1000 
                        + (time_now.tv_usec - m_stTransAdapter.stAdjustTime[chnex].tv_usec)/1000;

                        if(difftime < 30000)
                        {
                            continue;
                        }
                           
                        NetChangeMainToExtra(iChnNex, iStreamType);
                        continue;
                    }

                    if(1 == m_conn->iExtraCfgChangFlag )
                    {
                        continue;
                    }
                    /* --------DSP帧率和码率调整-----------*/                
                    pChnDevCapture = ICapture::instance(iChnNex);
                    cptChnFmtTmp = pChnDevCapture->GetFormat(iStreamType);
                    if(NULL == cptChnFmtTmp) 
                    {
                        continue;
                    }

                    memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));

                    int iPNMaxFps =  (m_iMaxFrameRate == 24)?vfps_pal_all[m_iMaxFrameRate ] : vfps_ntsc_all[m_iMaxFrameRate];
                    int iNewFps = (m_stTransAdapter.usNewFps[iChn]  > 0 && m_stTransAdapter.usNewFps[iChn]  <= iPNMaxFps)?m_stTransAdapter.usNewFps[iChn]-1:0;
                    
                    m_stTransAdapter.iFpsAdapter[iChn] = m_iMaxFrameRate - iNewFps;    

                    iResetDspFlag = 0;
                    iOldFps =  m_stTransAdapter.usNewFps[iChn] ;
                    iCurrtDspFps = cptChnFmt.FramesPerSecond;

                    if(iOldFps!= iCurrtDspFps)
                    {
                        iResetDspFlag = 1;
                    }

                    if(0 == iResetDspFlag )
                    {
                        if( m_stTransAdapter.usNewFps[iChn]   <=2)
                        {
                            continue;
                        }

                        if( m_stTransAdapter.usOldFps[iChn]   <= 2)
                        {
                            continue;
                        }                         

                        if( m_stTransAdapter.iFpsAdapter[iChn] >= m_iMaxFrameRate)
                        {
                            continue;
                        }
                    }

                    tracef("$$$$$$$$$$$$$$ chn %d newfps %d upfps %d,state %d,index %d\n",iChn,m_stTransAdapter.usNewFps[iChn],
                        m_stTransAdapter.usUpFps[iChn] ,m_stTransAdapter.iFpsAdapter[iChn],iNewFps);
                    
                    if(0 == iResetDspFlag )
                    {            
                        m_stTransAdapter.iUpCounts[iChn] = 0;
                        
                        if(NET_ADJUST_STATE_UP == m_stTransAdapter.emAdjustState[iChn] )
                        {
                            if(0 == m_stTransAdapter.usUpFps[iChn] )
                            {
                                iTmpState = m_stTransAdapter.iFpsAdapter[iChn] +1;

                                iNewFps = m_iMaxFrameRate - iTmpState;
                                if(iNewFps >m_iMaxFrameRate)
                                {
                                       iNewFps = m_iMaxFrameRate ;
                                }
                                
                                m_stTransAdapter.usUpFps[iChn]  = (m_iMaxFrameRate == 24)?vfps_pal_all[iNewFps] : vfps_ntsc_all[iNewFps];
                            }
                            tracef("****************chn %d up fps is %d\n",iChn, m_stTransAdapter.usUpFps[iChn] );
                        }

                        if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_Half)
                        {
                            m_stTransAdapter.iFpsAdapter[iChn] += 4;    
                        }                    
                        else if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_Quarter)
                        {
                            m_stTransAdapter.iFpsAdapter[iChn] += 3;    
                        }
                        else  if(m_stFPSAdapter.emAdjustState[chnex] >=PFrame_DQuarter)
                        {
                            m_stTransAdapter.iFpsAdapter[iChn] += 2;
                        }    
                        else
                        {
                            m_stTransAdapter.iFpsAdapter[iChn] += 1;
                        }                    

                        if(m_stTransAdapter.iFpsAdapter[iChn]  > m_iMaxFrameRate)
                        {
                            m_stTransAdapter.iFpsAdapter[iChn]  = m_iMaxFrameRate ;
                        }        

                        iNewFps = m_iMaxFrameRate - m_stTransAdapter.iFpsAdapter[iChn] ;
                        if(iNewFps >m_iMaxFrameRate)
                        {
                            iNewFps = m_iMaxFrameRate ;
                        }
                        m_stTransAdapter.usNewFps[iChn]  =  (m_iMaxFrameRate == 24)?vfps_pal_all[iNewFps] : vfps_ntsc_all[iNewFps];

                        m_stTransAdapter.iDownCounts[iChn] ++;
                        if(m_stTransAdapter.iDownCounts[iChn] >=5)
                        {
                            m_stTransAdapter.usUpFps[iChn] = 0;
                        }
                        
                        if(m_stTransAdapter.usUpFps[iChn] > 0)
                        {
                            m_stTransAdapter.usNewFps[iChn]  = m_stTransAdapter.usUpFps[iChn] ;

                            int iPNMaxFps =  (m_iMaxFrameRate == 24)?vfps_pal_all[m_iMaxFrameRate ] : vfps_ntsc_all[m_iMaxFrameRate];
                            iNewFps = (m_stTransAdapter.usNewFps[iChn]  > 0 && m_stTransAdapter.usNewFps[iChn]  <= iPNMaxFps)?m_stTransAdapter.usNewFps[iChn]-1:0;
                            
                            m_stTransAdapter.iFpsAdapter[iChn] = m_iMaxFrameRate - iNewFps;    
                
                            tracef("*********************chn %d  downset dsprate to updsprate %d\n",iChn,m_stTransAdapter.usNewDspRate[iChn]);
                        }
                    }
                    
                    cptChnFmt.FramesPerSecond =  m_stTransAdapter.usNewFps[iChn] ;
                    m_stTransAdapter.usNewDspRate[iChn]  = (cptChnFmt.FramesPerSecond * m_stTransAdapter.usOldDspRate[iChn])/m_stTransAdapter.usOldFps[iChn]   +48 ; 
        
                    if(m_stTransAdapter.usNewDspRate[iChn] < NET_MIN_DSP_RATE )
                    {
                        m_stTransAdapter.usNewDspRate[iChn]   = NET_MIN_DSP_RATE ;
                    }

                    if(m_stTransAdapter.usNewDspRate[iChn]   >   m_stTransAdapter.usOldDspRate[iChn])
                    {
                        m_stTransAdapter.usNewDspRate[iChn]  =  m_stTransAdapter.usOldDspRate[iChn];
                    }

                    if((m_stTransAdapter.usNewFps[iChn]  != iOldFps)
                        ||(1 == iResetDspFlag))
                    {
                        cptChnFmt.BitRate = m_stTransAdapter.usNewDspRate[iChn] ;    
                        pChnDevCapture->SetFormat(&cptChnFmt, iStreamType);     
                    }                    

                    memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(timeval));
                    m_stTransAdapter.emAdjustState[iChn] = NET_ADJUST_STATE_DOWN;
                    m_stTransAdapter.bConfictFlag[iChn] = 0;
                    
                    tracef("*********chn %d  fps start to just down  %d Current dsprate is %d  \n",iChn,m_stTransAdapter.usNewFps[iChn],cptChnFmt.BitRate  );
                
                }
            }
        }
        else if(0 == iStateFlag )
        {
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(m_MonitorState[iChn] == 1)
                {
                    if(NET_TRANS_QUALITY== m_stTransAdapter.emConnTransState[iChn] )
                    {
                        difftime = (time_now.tv_sec - m_stTransAdapter.stLastTime[iChn].tv_sec)*1000 
                        + (time_now.tv_usec - m_stTransAdapter.stLastTime[iChn].tv_usec)/1000;
                        if(difftime <12000)
                        {
                            return 0;                            
                        }
                        else
                        {
                            m_stTransAdapter.bConfictFlag[iChn] = 0;
                        }
                    }
                }
            }

            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2;iChn++)
            {
                iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))? 0 : 1;
                if(m_MonitorState[iChn] == 1)
                {                
                    if((m_conn->DataTransferQueue[iChnNex+1].totalSize*8) > (unsigned long)((m_stTransAdapter.usNewDspRate[iChn]*1024*1)/10))
                    {
                        tracef("CaptureHelper::NetTransAdpater chn is %d, buffer have data %lu.\n", iChn,m_conn->DataTransferQueue[iChnNex+1].totalSize);
                        break;
                    }
                }
            }

            if(iChn<(m_pCaptureM->GetLogicChnNum() )*2)
            {
                for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
                {
                    if(m_MonitorState[iChn] == 1)
                    {                                                
                        if(NET_TRANS_QUALITY == m_stTransAdapter.emConnTransState[iChn] )
                        {
                            memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(struct timeval));
                            m_stTransAdapter.bConfictFlag[iChn] = 0;                            
                        }
                    }
                }

                return 0;
            } 
            /* --------DSP帧率码率调整-----------*/
            for(iChn = 0; iChn <(m_pCaptureM->GetLogicChnNum() )*2; iChn ++)
            {
                if(NET_TRANS_QUALITY == m_stTransAdapter.emConnTransState[iChn] )
                {
                    iChnNex =  (iChn < (m_pCaptureM->GetLogicChnNum() ))? iChn : (iChn-(m_pCaptureM->GetLogicChnNum() ));
                    iStreamType =  (iChn<(m_pCaptureM->GetLogicChnNum() ))?0 : 1;

                    if(m_MonitorState[iChn] == 0)
                    {
                        continue;
                    }

                    if((1 == g_stNetTransPolicy.bHaveDisk )
                     &&(CHL_MAIN_T== iStreamType)
                     &&(iChn < (m_pCaptureM->GetLogicChnNum() )))
                    {
                        continue;
                    }
                                    
                    pChnDevCapture = ICapture::instance(iChnNex);
                    cptChnFmtTmp = pChnDevCapture->GetFormat(iStreamType);
                    if(NULL == cptChnFmtTmp) 
                    {
                        continue;
                    }

                    memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));

                    int iPNMaxFps =  (m_iMaxFrameRate == 24)?vfps_pal_all[m_iMaxFrameRate ] : vfps_ntsc_all[m_iMaxFrameRate];
                    int iNewFps = (m_stTransAdapter.usNewFps[iChn]  > 0 && m_stTransAdapter.usNewFps[iChn]  <= iPNMaxFps)?m_stTransAdapter.usNewFps[iChn]-1:0;
                
                    m_stTransAdapter.iFpsAdapter[iChn] = m_iMaxFrameRate - iNewFps;    

                    iResetDspFlag = 0;
                    iOldFps =  m_stTransAdapter.usNewFps[iChn] ;
                    iCurrtDspFps = cptChnFmt.FramesPerSecond;
                    if(iOldFps!= iCurrtDspFps)
                    {
                        iResetDspFlag = 1;
                    }

                    if(0 == iResetDspFlag)
                    {
                        if(m_stTransAdapter.usNewFps[iChn]   >= m_stTransAdapter.usOldFps[iChn])
                        {
                            if((CHL_2END_T== iStreamType)
                                 &&(iChn >=  (m_pCaptureM->GetLogicChnNum() )))
                            {
                                difftime = (time_now.tv_sec - m_stTransAdapter.stAdjustTime[chnex].tv_sec)*1000 
                                            + (time_now.tv_usec - m_stTransAdapter.stAdjustTime[chnex].tv_usec)/1000;

                                if(difftime < 360000)
                                {
                                    continue;
                                }
                                           
                                //NetAdpaterExchangeExtraToMain(iChnNex,iStreamType);
                                if(1 == m_stTransAdapter.bChangeStreamFlags[iChnNex])
                                {
                                    if(iChnNex + (m_pCaptureM->GetLogicChnNum() )*iStreamType >=  (m_pCaptureM->GetLogicChnNum() ))
                                    {
                                        m_conn->iChangMainToExtraFlag = 2;
                                    }    
                                }
                                
                                continue;
                             }
                        
                             continue;    
                        }    

                        if(m_stTransAdapter.iFpsAdapter[iChn] < 1 )
                        {
                            continue;
                        }
            
                    }        
                    
                    tracef("$$$$$$$$$$$$$$222 chn %d newfps %d upfps %d,state %d,index %d\n",iChn,m_stTransAdapter.usNewFps[iChn],
                        m_stTransAdapter.usUpFps[iChn] ,m_stTransAdapter.iFpsAdapter[iChn],iNewFps);

                    if(0 == iResetDspFlag)
                    {
                        m_stTransAdapter.iDownCounts[iChn] = 0;
                        m_stTransAdapter.iFpsAdapter[iChn] -= 1;                    

                        iNewFps = m_iMaxFrameRate - m_stTransAdapter.iFpsAdapter[iChn] ;
                        if(iNewFps >m_iMaxFrameRate)
                        {
                               iNewFps = m_iMaxFrameRate ;
                        }
                        m_stTransAdapter.usNewFps[iChn]  =  (m_iMaxFrameRate == 24)?vfps_pal_all[iNewFps] : vfps_ntsc_all[iNewFps];
                    
                        m_stTransAdapter.iUpCounts[iChn] ++;
                        if(m_stTransAdapter.iUpCounts[iChn] >= 15)
                        {
                            m_stTransAdapter.usUpFps[iChn] = 0;
                        }
                        
                        if(m_stTransAdapter.usUpFps[iChn] > 0)
                        {
                            if(m_stTransAdapter.usNewFps[iChn]  > m_stTransAdapter.usUpFps[iChn] )
                            {
                                m_stTransAdapter.usNewFps[iChn]  = m_stTransAdapter.usUpFps[iChn];
                                int iPNMaxFps =  (m_iMaxFrameRate == 24)?vfps_pal_all[m_iMaxFrameRate ] : vfps_ntsc_all[m_iMaxFrameRate];
                                iNewFps = (m_stTransAdapter.usNewFps[iChn]  > 0 && m_stTransAdapter.usNewFps[iChn]  <= iPNMaxFps)?m_stTransAdapter.usNewFps[iChn]-1:0;

                                m_stTransAdapter.iFpsAdapter[iChn] = m_iMaxFrameRate - iNewFps;    
                                tracef("*********************chn %d  upset dsprate to fps %d\n",iChn,m_stTransAdapter.usNewFps[iChn]);
                            }
                        }
                    }

                    cptChnFmt.FramesPerSecond =  m_stTransAdapter.usNewFps[iChn] ;
                    m_stTransAdapter.usNewDspRate[iChn]  = (cptChnFmt.FramesPerSecond * m_stTransAdapter.usOldDspRate[iChn])/m_stTransAdapter.usOldFps[iChn]   +48 ;
                    if(m_stTransAdapter.usNewDspRate[iChn]  < NET_MIN_DSP_RATE )
                    {
                        m_stTransAdapter.usNewDspRate[iChn]  = NET_MIN_DSP_RATE ;
                    }
                    
                    if(m_stTransAdapter.usNewDspRate[iChn]  >   m_stTransAdapter.usOldDspRate[iChn])
                    {
                        m_stTransAdapter.usNewDspRate[iChn]  =  m_stTransAdapter.usOldDspRate[iChn];
                    }                    

                    if((m_stTransAdapter.usNewFps[iChn]  != iOldFps)
                        ||(1 == iResetDspFlag ))
                    {
                        cptChnFmt.BitRate = m_stTransAdapter.usNewDspRate[iChn] ;
                        pChnDevCapture->SetFormat(&cptChnFmt, iStreamType);     
                    }
            
                    memcpy(&m_stTransAdapter.stLastTime[iChn],&time_now,sizeof(timeval));
                    m_stTransAdapter.emAdjustState[iChn] = NET_ADJUST_STATE_UP;
                    m_stTransAdapter.bConfictFlag[iChn] = 0;
                                             
                    tracef("*******chn %d fps start to just up  %d,Current dsprate %d \n", iChn,m_stTransAdapter.usNewFps[iChn],cptChnFmt.BitRate);
                }
            }
         }
    }

    return 0;
    
}


/* 正在监视的通道数目 */
int CaptureHelper::GetMonitorNum()
{
    int i=0;
    int iMonitorNum=0;

    for(i=0;i<m_pCaptureM->GetLogicChnNum();i++)
    {
        if ( m_MonitorState[i] ==1  || m_MonitorState[i + (m_pCaptureM->GetLogicChnNum() )] ==1 )
        {
            iMonitorNum++;
        }
    }
    
    return iMonitorNum;
}

/* 正在监视的码流大小(K) */
int CaptureHelper::GetMonitorRate()
{
    int i=0;
    int iMonitorRate=0;
    
    for (i=0;i<m_pCaptureM->GetLogicChnNum();i++)
    {
        if (i<m_pCaptureM->GetAnalogChnNum())
        {
            if ( m_MonitorState[i] ==1 )
            {
                iMonitorRate+=m_cCfgEncode.getConfig(i).dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
            }

            if ( m_MonitorState[i+m_pCaptureM->GetLogicChnNum()] == 1 )
            {
                iMonitorRate+=m_cCfgEncode.getConfig(i).dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
            }
        }
        else /* 数字通道不大好统计,暂时只处理纯模拟设备*/
        {
             //   iMonitorRate+=1024;
        }
    }
    
    return iMonitorRate;
}


/*本次操作引起的码流监视数目变化*/
int CaptureHelper::CalcMonitorNumDiff(DVRIP *pHead)
{
    int idiff=0;

    if(pHead==NULL)
    {
        return 0;
    }
    
    /* 目前只处理纯模拟设备 */
    switch(pHead->dvrip_p[18])
    {
        case EnMonMode_Tcp:
        {
            /*
             * 视频监控命令,开启处理线程
             */
            int ChNum = m_pCaptureM->GetAnalogChnNum();
            if( ChNum <= 16 )
            {
                for(int i = 0; i < ChNum; i++)
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    if(0 != pHead->dvrip_extlen)//(len ==16)
                    {
                        char *stHead = NULL;
                        stHead = (char *)((char *)pHead+DVRIP_HEAD_T_SIZE+i);
                        if(stHead != NULL)
                        {
                            //码流类型检查
                            stream = *stHead;
                        }
                    }
                    
                    if ( CHL_MAIN_T != stream && CHL_2END_T != stream )
                    {
                        stream = CHL_MAIN_T;
                    }
                    
                    /*
                    * 监视状态发生改变,这里我们必须做相应的调整
                    */
                    if(pHead->dvrip_p[i] != m_MonitorState[i+m_pCaptureM->GetLogicChnNum()*stream])
                    {
                        if(pHead->dvrip_p[i] == CHANNEL_MONITOR_ON)
                        {                            
                            idiff++;
                        } 
                        else if(pHead->dvrip_p[i] == CHANNEL_MONITOR_OFF)
                        {
                            idiff--;
                        }
                    }
                } // for
            }
            else
            {
                assert(0 != pHead->dvrip_extlen);
                int tlv_type = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16));
                int tlv_len = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16+4));
                if( tlv_type != 1
                    || tlv_len != (pHead->dvrip_extlen-16-8) )
                {
                }
                else
                {
                    CHANNEL_OPR* pChannelOpr = NULL;
                    pChannelOpr = (CHANNEL_OPR *)((char *)pHead+DVRIP_HEAD_T_SIZE+16+8);
                    for( int i = 0; i < ChNum; i++ )
                    {
                        int stream = 0;//!<存储码流类型, 主码流或辅助码流
                        stream = pChannelOpr->ChannelStream[i];
                        if(stream != 0 && stream != 1)
                        {
                            continue;
                        }

                        if ( CHL_MAIN_T != stream  && CHL_2END_T != stream )
                        {
                            stream = CHL_MAIN_T;
                        }

                        if( pChannelOpr->ChannelCmd[i] != m_MonitorState[i+m_pCaptureM->GetLogicChnNum()*stream] )
                        {

                            if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_ON )
                            {
                                idiff++;
                            }
                            else if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_OFF )
                            {
                                idiff--;
                            }
                        }
                    }
                }            
            }

            break;
        }
        case EnMonMode_digital:
        {
            int ChNum = m_pCaptureM->GetDigitalChnNum();
            if( ChNum <= 16 )
            {
                for(int i = 0; i < m_pCaptureM->GetDigitalChnNum(); i++)
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    if(0 != pHead->dvrip_extlen)//(len ==16)
                    {
                        char *stHead = NULL;
                        stHead = (char *)((char *)pHead+DVRIP_HEAD_T_SIZE+i);
                        if(stHead != NULL)
                        {
                            //码流类型检查
                            stream = *stHead;
                            if(stream != 0 && stream != 1)
                            {
                                continue;
                            }
                        }
                    }

                    int tmpChn = i+m_pCaptureM->GetAnalogChnNum();
                    if(pHead->dvrip_p[i] != m_MonitorState[tmpChn+m_pCaptureM->GetLogicChnNum()*stream])
                    {
                        if(pHead->dvrip_p[i] == CHANNEL_MONITOR_ON)
                        {
                            idiff++;
                        }
                        else if(pHead->dvrip_p[i] == CHANNEL_MONITOR_OFF)
                        {
                            idiff--;
                        }
                    }
                }
            }
            else
            {
                assert(0 != pHead->dvrip_extlen);
                int tlv_type = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16));
                int tlv_len = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16+4));
                CHANNEL_OPR* pChannelOpr = NULL;
                pChannelOpr = (CHANNEL_OPR *)((char *)pHead+DVRIP_HEAD_T_SIZE+16+8);
                for( int i = 0; i < ChNum; i++ )
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    stream = pChannelOpr->ChannelStream[i];
                    if(stream != 0 && stream != 1)
                    {
                        continue;
                    }

                    if( pChannelOpr->ChannelCmd[i] != m_MonitorState[i+m_pCaptureM->GetAnalogChnNum()*stream] )
                    {
                        if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_ON )
                        {
                            idiff++;
                        }
                        else if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_OFF )
                        {
                            idiff--;
                        }
                    }
                }
            }
            break;
       }
        default:
            break;
    }
    
    printf("CaptureHelper::CalcMonitorNumDiff = %d\r\n",idiff);

    return idiff;
}

/*本次操作引起的码流码率变化*/
/* 只处理纯模拟的设备 */
int CaptureHelper::CalcMonitorRateDiff(DVRIP *pHead)
{
    int idiffRate=0;

    if(pHead==NULL)
    {
        return 0;
    }
    
    /* 目前只处理纯模拟设备 */
    switch(pHead->dvrip_p[18])
    {
        case EnMonMode_Tcp:
        {
            /*
             * 视频监控命令,开启处理线程
             */
            int ChNum = m_pCaptureM->GetAnalogChnNum();
            if( ChNum <= 16 )
            {
                for(int i = 0; i < ChNum; i++)
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    if(0 != pHead->dvrip_extlen)//(len ==16)
                    {
                        char *stHead = NULL;
                        stHead = (char *)((char *)pHead+DVRIP_HEAD_T_SIZE+i);
                        if(stHead != NULL)
                        {
                            //码流类型检查
                            stream = *stHead;
                        }
                    }

                    if ( stream != 0 && stream != 1)
                    {
                        stream = 0;
                    }
                    
                    /*
                    * 监视状态发生改变,这里我们必须做相应的调整
                    */
                    if(pHead->dvrip_p[i] != m_MonitorState[i+m_pCaptureM->GetLogicChnNum()*stream])
                    {
                        if(pHead->dvrip_p[i] == CHANNEL_MONITOR_ON)
                        { 
                            if  ( stream == 0 )
                            {
                                idiffRate+=m_cCfgEncode.getConfig(i).dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
                            }
                            else
                            {
                                idiffRate+=m_cCfgEncode.getConfig(i).dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
                            }
                        } 
                        else if(pHead->dvrip_p[i] == CHANNEL_MONITOR_OFF)
                        {                           
                            if  ( stream == 0 )
                            {
                                idiffRate -= m_cCfgEncode.getConfig(i).dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
                            }
                            else
                            {
                                idiffRate -= m_cCfgEncode.getConfig(i).dstExtraFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
                            }
                        }
                    }
                } // for
            }
        }
        default:
            break;
    }
    
    printf("CaptureHelper::CalcMonitorRateDiff = %d\r\n",idiffRate);
    return idiffRate;
}
    
void CaptureHelper::DumpHelper(int chn,int stream)
{
    if(!g_Net.m_bTransPrint)
    {
        return;
    }
    uint64 tmpCnt = SystemGetMSCount64();
    if(g_Net.m_iPrtfperiad*1000+m_iLastTime[chn]> tmpCnt)
    {
        return;
    }

    int iChnStream = chn+stream*m_pCaptureM->GetLogicChnNum();
    
    m_iLastTime[chn] = tmpCnt;

    //m_conn    
    { 
        printf("   @@@@@@@@@@@@@@@CaptureHelper::DumpHelper@@@@@@@@@@@@@\n");
        printf("   Chn   Stream  sock  subsock Main2ExtFlag  QueSize   \n");
        printf("   %d      %d      %d        %d       %d        %lu\n\n",
            chn,stream,m_conn->sock,m_conn->MultiSock[chn],m_conn->iChangMainToExtraFlag,m_conn->DataTransferQueue[chn+1].totalSize);
    }

    //g_stNetTransPolicy
    char tmpstr[16] = {0};
    {
        
        memset(tmpstr,0,sizeof(tmpstr));
        switch(g_stNetTransPolicy.emNetTransState)
        {
             case NET_TRANS_RATE:
                 strcpy(tmpstr,"RatePrior");
                break;
             case NET_TRANS_QUALITY:
                 strcpy(tmpstr,"QlityPrior");
                break;
            default:
                break;
        }
        printf("   -----------------g_stNetTransPolicy----------------\n");
        printf("   bTransPolicy     NetState    bHaveDisk bExtStrm bHDown DspRate Fps Resolution\n");
        printf("      %d             %s     %d         %d        %d    %d   %d     %d \n\n",
            g_stNetTransPolicy.bNetTransPolicy,    tmpstr,g_stNetTransPolicy.bHaveDisk,g_stNetTransPolicy.bExtStream,
            g_stNetTransPolicy.bHighDownload,g_stNetTransPolicy.usDspRate[iChnStream],
            g_stNetTransPolicy.usFps[iChnStream],g_stNetTransPolicy.usResolution[iChnStream]);
    }
    //m_stTransAdapter  
    {
    
        printf("   -----------------m_stTransAdapter-----------------\n");
        printf("   bConTraPolicy  ConnTranStat   AdjustState  bConfictFlag  iUpCounts iDownCounts \n");
        printf("       %d              %d              %d          %d                %d          %d      \n",
            m_stTransAdapter.bConnTransPolicy[iChnStream],m_stTransAdapter.emConnTransState[iChnStream],
            m_stTransAdapter.emAdjustState[iChnStream],m_stTransAdapter.bConfictFlag[iChnStream],
            m_stTransAdapter.iUpCounts[iChnStream],    m_stTransAdapter.iDownCounts[iChnStream]);
        
        printf("   OldFps  NewFps iFpsAdapter  UPFps  OldDspRate NewDspRate UPDspRate ConReslution \n");
        printf("      %d     %d     %d            %d       %d         %d        %d      %d \n\n",
            m_stTransAdapter.usOldFps[iChnStream],m_stTransAdapter.usNewFps[iChnStream],
            m_stTransAdapter.iFpsAdapter[iChnStream],m_stTransAdapter.usUpFps[iChnStream],
            m_stTransAdapter.usOldDspRate[iChnStream],m_stTransAdapter.usNewDspRate[iChnStream],
            m_stTransAdapter.usUpDspRate[iChnStream],m_stTransAdapter.iConnResolution[iChnStream]);
    }
    //m_stFPSAdapter
    {    
        memset(tmpstr,0,sizeof(tmpstr));
        switch(m_stFPSAdapter.emAdjustState[iChnStream])
        {
            case PFrame_all:
                strcpy(tmpstr,"PFrame_all");
                break;
            case PFrame_DQuarter:
                strcpy(tmpstr,"PFrame_DQuarter");
                break;
            case PFrame_Quarter:
                strcpy(tmpstr,"PFrame_Quarter");
                break;
            case PFrame_Half:
                strcpy(tmpstr,"PFrame_Half");
                break;
            case PFrame_HHalf:
                strcpy(tmpstr,"PFrame_HHalf");
                break;
            case PFrame_None:
                strcpy(tmpstr,"PFrame_None");
                break;            
            case IFrame_None:
                strcpy(tmpstr,"IFrame_None");
                break;
            default:
                memset(tmpstr,0,sizeof(tmpstr));
				break;
        }
        
        printf("   -----------------m_stFPSAdapter-----------------\n");
        printf("   AdjustState    pFrameNums     FpsAdapter \n");
        printf("   %s        %d             %d \n\n",tmpstr,m_stFPSAdapter.iPFrameNums[iChnStream],m_stFPSAdapter.iFpsAdapter[iChnStream]);
    }


    
}

char CaptureHelper::GetMonitorState(int chn,int stream)
{
        int iCh = chn%g_nLogicNum;
        int iStream = stream%2;
        
        return m_MonitorState[iCh+iStream*g_nLogicNum];
}

#ifdef RECORD_NET_THREAD_MULTI 
bool CaptureHelper::SendChnToNet()
{
    if( m_packet_count > 0 )
    {
        CPacket* pPacket = NULL;
        uint dwStreamType = 0;
        int chn;

        m_mutex_list.Enter();
        PACKET_LIST::iterator it = m_packet_list.begin();
        pPacket = it->pPacket;
        dwStreamType = it->dwStreamType;
        chn = it->chn;
        m_packet_list.pop_front();
        m_packet_count--;
        m_mutex_list.Leave();

        OnCaptureEx(chn, dwStreamType, pPacket);

        pPacket->Release();

        return true;
    }

    return false;
}

void CaptureHelper::OnCapture(int chn,int stream, CPacket *pPacket)
{
//fyj 20170227
    PACKET_CAPTURE packet_capture;
    packet_capture.chn = chn;
    packet_capture.pPacket = pPacket;
    packet_capture.dwStreamType = stream;


    int iCount = g_nLogicNum;    
#ifdef _USE_720P_MODULE_
    iCount = 60;
#endif
    if( m_packet_count > iCount*20 )
    {
        debugf("capture list is too large discard it, size:%d, chn:%d!\n", m_packet_count, chn);
        return ;
    }

    m_mutex_list.Enter();
    pPacket->AddRef();
    m_packet_list.push_back(packet_capture);
    m_packet_count++;
    m_mutex_list.Leave();
}
#endif

#ifdef RECORD_NET_THREAD_MULTI
void CaptureHelper::OnCaptureEx(int chn,int stream, CPacket *pPacket)
#else
void CaptureHelper::OnCapture(int chn,int stream, CPacket *pPacket)
#endif
{
    CGuard tmpGuard(m_Mutex);
    
    if (VD_FALSE == m_BAllocFlag)
    {
        printf("CaptureHelper::OnCapture():m_BAllocFlag = VD_FALSE\r\n");
        return;
    }
#if 0
	if (1 == stream) {
		PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());
	    
		int tCount = 0;
		for( ;tCount < 8; tCount++)
		{
		    uchar * pBufSend = NULL;
		    unsigned char ucFrameType = pPacketHead->FrameInfo[tCount].FrameType;  
		    unsigned char ucFrameFlag = pPacketHead->FrameInfo[tCount].FrameFlag;                  
		    unsigned short usFramePos = pPacketHead->FrameInfo[tCount].FramePos;                    
		    unsigned int usCFrameLength = pPacketHead->FrameInfo[tCount].DataLength;
			unsigned int usFrameLength = pPacketHead->FrameInfo[tCount].FrameLength;
			if (ucFrameType == 255)
			{
			    break;
			}
		_printd("web pack[%d] pos [%d], flag[%d], len[%lu] total[%lu], type[%d]", tCount=1,
			pPacketHead->FrameInfo[tCount].FramePos,
			pPacketHead->FrameInfo[tCount].FrameFlag,
			pPacketHead->FrameInfo[tCount].DataLength,
			pPacketHead->FrameInfo[tCount].FrameLength,
			pPacketHead->FrameInfo[tCount].FrameType);
		    //printf("tCount %d [%d %d %d %d %d %d]\n", tCount,ucFrameType, ucFrameFlag, usFramePos, usCFrameLength, usFrameLength, pPacket->GetLength());
		}
	}
#endif
#if 0
      PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)(pPacket->GetHeader());
	for(int j = 0; j < FRAME_MAX_NUM; j++) {
		 if(pPacketHead->FrameInfo[j].FrameFlag == PACK_TYPE_FRAME_NULL )
            		break;
		debugf("captureSend pos [%d], flag[%d], len[%lu] total[%lu], type[%d]\n\n", 
			pPacketHead->FrameInfo[j].FramePos,
			pPacketHead->FrameInfo[j].FrameFlag,
			pPacketHead->FrameInfo[j].DataLength,
			pPacketHead->FrameInfo[j].FrameLength,
			pPacketHead->FrameInfo[j].FrameType);
	}
#endif

    //!当前通道的对应码流的监视状态是否使能
    if(m_MonitorState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] == 0) return ;

    int chnex = chn + (m_pCaptureM->GetLogicChnNum() )*stream;

    DumpHelper(chn, stream);

    //数字通道的码流控制和模拟通道不同，不能直接读取配置的码流，也不能通过设置dsp参数来修改码率，也不能切到符码流等
    //目前简化处理
    if(chn>= g_nCapture && chn <g_nLogicNum)
    {
        DigiChNetStateDetect(chn,stream);
    }
    else
    {
        //NetStateDetect(chn,stream);
        //NetTransAdpater(chn,stream);

    }
    
    PKT_HEAD_INFO *pPacketHead = (PKT_HEAD_INFO *)pPacket->GetHeader();
    msghdr_t *msg = NULL;
    const unsigned long packetLen = pPacket->GetLength() - 13;//fyj 不要时间戳
    int iSendFullPktFlag = 0;
    
    if(m_stFPSAdapter.emAdjustState[chnex] == PFrame_all)
    {
        if(m_stFPSAdapter.iFpsAdapter[chnex] == 1 && pPacketHead->FrameInfo[0].FrameType== PACK_TYPE_FRAME_I )
        {
            m_stFPSAdapter.iFpsAdapter[chnex] = 0;
        }
    }
    else if(m_stFPSAdapter.iFpsAdapter[chnex] == 0)
    {
        m_stFPSAdapter.iFpsAdapter[chnex] = 1;
        m_stFPSAdapter.iPFrameNums[chnex] = MAX_PFRAME_DROP;
    }

    if(m_stFPSAdapter.iFpsAdapter[chnex] == 0)
    {
        iSendFullPktFlag = 1;
    }
    else
    {
        if((m_stFPSAdapter.emAdjustState[chnex] == IFrame_None)&&(m_stFPSAdapter.iPFrameNums[chnex] <= 0))
        {
            if(m_conn->DataTransferQueue[chn + 1].totalSize != 0)
            {
                NetDvrSendQueue(m_conn,chn,ACK_CHAN_MONIT);
            }    
            NetDvrSendOtherQueue(m_conn,chn,ACK_CHAN_MONIT);

            return ;
        }
    }

    /*
     * 最多八段信息，每段信息为1个字节，总8字节 (xxxx yyyy,其中xxxx标识 帧类型，定义见上；
     *具体是0000 I帧，0001 P帧，0010 B帧， 0011音频帧， 0100 图片， 0101 动检帧（为智能考虑），
     *1111结束无效帧（标识八段信息内此后的段信息内无效）yyyy 标识此包帧头尾标志，定义见上；）
     *采取ip.dvrip_p[2] ip.dvrip_p[3] ip.dvrip_p[4] ip.dvrip_p[5] ip.dvrip_p[6]
     *ip.dvrip_p[7] ip.dvrip_p[8] ip.dvrip_p[9] 填充上述8个分段信息；
     *采取 ip.dvrip_p[12] ip.dvrip_p[13] ip.dvrip_p[14] ip.dvrip_p[15]为网络发包的序号；
     */
    static uchar frame_local2net[] =
    {
        0x00,    //!PACK_TYPE_FRAME_P
        0x10,    //!PACK_TYPE_FRAME_I,
        0x20,    //!PACK_TYPE_FRAME_B,
        0x30,    //!PACK_TYPE_FRAME_AUDIO,
        0x40,    //!PACK_TYPE_FRAME_JEPG,
        0x50,    //!PACK_TYPE_FRAME_MOTION,
        0x60,    //!PACK_TYPE_FRAME_EE
        0x70,    //!PACK_TYPE_FRAME_EF
        0xf0,    //!PACK_TYPE_FRAME_NUM,
        0xf0,    //!PACK_TYPE_FRAME_NULL=0xff,
    };
    static int ptk_slice_len = 1024 * 32;

    iSendFullPktFlag = 1;	

    if(1 ==  iSendFullPktFlag)
    {		
#if 1
		DVRIP ip;
		msghdr_t msghdr[2] ;
		int num = packetLen / ptk_slice_len;
		int slice = packetLen % ptk_slice_len;
		int index = 0;
		
		while(index < num)
		{
	        memset(&ip, 0, sizeof(ip));
	        memset(&msghdr, 0, sizeof(msghdr));

	        ip.dvrip_cmd = ACK_CHAN_MONIT;
	        ip.dvrip_extlen= ptk_slice_len;
	        ip.dvrip_p[0]   = chn;
	        ip.dvrip_p[1]   = 0x00;
			ip.dvrip_p[2] = frame_local2net[pPacketHead->FrameInfo[0].FrameType];
			ip.dvrip_p[3] = 0xff;
	        ip.dvrip_p[16]  = (stream == 0)?0:stream+4;
			
            if (0 == slice)
            {
                if (1 == num)
                {
                    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEADTRAIL);
                }
				else if (0 == index)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEAD);
				}
				else if(index == num -1)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_TRAIL);
				}
				else
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_NONHT);
				}
            }
			else 
			{
				if (0 == index)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEAD);
				}
				else
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_NONHT);
				}
			}
			
	        m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
	        ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
	        (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

            pPacket->AddRef();
	        msghdr[0].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[0].chn      = chn;
	        msghdr[0].buf      = &ip;
	        msghdr[0].buf_len  = sizeof(ip);
	        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
	        msghdr[0].next     = &msghdr[1];

	        msghdr[1].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[1].chn      = chn;
	        msghdr[1].buf      = pPacket->GetBuffer() + index * ptk_slice_len;
	        msghdr[1].buf_len  = ptk_slice_len;
	        msghdr[1].msg_flags= 0;
	        msghdr[1].callback = ReleasePacket;
	        msghdr[1].context  = pPacket;
	        msghdr[1].iFrameFlag = PKT_FULL_FRAME;

	        DvrNetSendChain(m_conn, msghdr);
	        NetDvrSendOtherQueue(m_conn,chn, ACK_CHAN_MONIT);
			index++;
		}
		
		if (slice > 0)
		{
			memset(&ip, 0, sizeof(ip));
	        memset(&msghdr, 0, sizeof(msghdr));

	        ip.dvrip_cmd = ACK_CHAN_MONIT;
	        ip.dvrip_extlen= slice;
	        ip.dvrip_p[0]   = chn;
	        ip.dvrip_p[1]   = 0x00; 
			ip.dvrip_p[2] = frame_local2net[pPacketHead->FrameInfo[0].FrameType];
			ip.dvrip_p[3] = 0xff;
	        ip.dvrip_p[16]  = (stream == 0)?0:stream+4;

			if (0 == num)
			{
			    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEADTRAIL);
			}
			else
			{
			    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_TRAIL);
			}
			
	        m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
	        ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
	        (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

            pPacket->AddRef();
	        msghdr[0].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[0].chn      = chn;
	        msghdr[0].buf      = &ip;
	        msghdr[0].buf_len  = sizeof(ip);
	        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
	        msghdr[0].next     = &msghdr[1];

	        msghdr[1].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[1].chn      = chn;
	        msghdr[1].buf      = pPacket->GetBuffer() + num * ptk_slice_len;
	        msghdr[1].buf_len  = slice;
	        msghdr[1].msg_flags= 0;
	        msghdr[1].callback = ReleasePacket;
	        msghdr[1].context  = pPacket;
	        msghdr[1].iFrameFlag = PKT_FULL_FRAME;

	        DvrNetSendChain(m_conn, msghdr);
	        NetDvrSendOtherQueue(m_conn,chn, ACK_CHAN_MONIT);
		}	
	#else
	    {
	        pPacket->AddRef();
			
	        DVRIP ip;
	        msghdr_t msghdr[2] ;

	        memset(&ip, 0, sizeof(ip));
	        memset(&msghdr, 0, sizeof(msghdr));

	        ip.dvrip_cmd = ACK_CHAN_MONIT;
	        ip.dvrip_extlen= packetLen;
	        ip.dvrip_p[0]   = chn;
	        ip.dvrip_p[1]   = 0x00; 
	        ip.dvrip_p[16]  = (stream == 0)?0:stream+4;

	        //!统计帧信息
	        for (int ii=0; ii<FRAME_MAX_NUM; ii++)
	        {
	            uchar  iType     = pPacketHead->FrameInfo[ii].FrameType;
	            uchar  iFlag     = pPacketHead->FrameInfo[ii].FrameFlag;

	            if( iType == PACK_TYPE_FRAME_NULL )
	            {
	                ip.dvrip_p[2+ii] = 0xff;
	                break;
	            }

	            ip.dvrip_p[2+ii] = frame_local2net[iType];

	            //!合入帧头尾标志信息
	            ip.dvrip_p[2+ii] = (ip.dvrip_p[2+ii] | iFlag);
	        }

	        m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
	        //!写入序号
	        ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
	        (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

	        msghdr[0].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[0].chn      = chn;
	        msghdr[0].buf      = &ip;
	        msghdr[0].buf_len  = sizeof(ip);
	        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
	        msghdr[0].next     = &msghdr[1];

	        msghdr[1].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[1].chn      = chn;
	        msghdr[1].buf      = pPacket->GetBuffer();
	        msghdr[1].buf_len  = packetLen;
	        msghdr[1].msg_flags= 0;
	        msghdr[1].callback = ReleasePacket;
	        msghdr[1].context  = pPacket;
	        msghdr[1].iFrameFlag = PKT_FULL_FRAME;
//            printf("%d flag = %d, buf_len = %d\n", packetLen, ip.dvrip_p[2]&0x0f, msghdr[1].buf_len);
	        DvrNetSendChain(m_conn, msghdr);
	        NetDvrSendOtherQueue(m_conn,chn,ACK_CHAN_MONIT);
	    }
	#endif
        return ;     
    }
    

    for (int ii=0; ii<FRAME_MAX_NUM; ii++)
    {
        uchar  iType     = pPacketHead->FrameInfo[ii].FrameType;
        uchar  iFlag     = pPacketHead->FrameInfo[ii].FrameFlag;
        int iLocation = pPacketHead->FrameInfo[ii].FramePos;
        uint iLen      = pPacketHead->FrameInfo[ii].DataLength;


        if(iType == PACK_TYPE_FRAME_NULL)
        {
            break;
        }
        
        else if(iType == PACK_TYPE_FRAME_I)
        {
            m_stFPSAdapter.iPFrameNums[chnex] = 0;
        }
        else if(iType == PACK_TYPE_FRAME_P)
        {
            if(m_stFPSAdapter.iPFrameNums[chnex] == -1)
            {
                continue;
            }
            
            m_stFPSAdapter.iPFrameNums[chnex]++;

            if(m_stFPSAdapter.iPFrameNums[chnex] > pframes_map[m_stFPSAdapter.emAdjustState[chnex]])
            {
                if(iFlag == PACK_CONTAIN_FRAME_HEADTRAIL || iFlag == PACK_CONTAIN_FRAME_TRAIL)
                {
                     m_stFPSAdapter.iPFrameNums[chnex] = -1;
                }
            }
        }
        else if(iType == PACK_TYPE_FRAME_AUDIO)
        {
        }
        else
        {
            continue;
        }

        if(iLen > 0 && iLen <= packetLen)
        {
#if 1
		DVRIP ip;
		msghdr_t msghdr[2] ;
		int num = iLen / ptk_slice_len;
		int slice = iLen % ptk_slice_len;
		int index = 0;
		
		while(index < num)
		{
	        memset(&ip, 0, sizeof(ip));
	        memset(&msghdr, 0, sizeof(msghdr));

	        ip.dvrip_cmd = ACK_CHAN_MONIT;
	        ip.dvrip_extlen= ptk_slice_len;
	        ip.dvrip_p[0]   = chn;
	        ip.dvrip_p[1]   = 0x00;
			ip.dvrip_p[2] = frame_local2net[iType];
			ip.dvrip_p[3] = 0xff;
	        ip.dvrip_p[16]  = (stream == 0)?0:stream+4;
			
            if (0 == slice)
            {
                if (1 == num)
                {
                    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEADTRAIL);
                }
				else if (0 == index)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEAD);
				}
				else if(index == num -1)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_TRAIL);
				}
				else
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_NONHT);
				}
            }
			else 
			{
				if (0 == index)
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEAD);
				}
				else
				{
				    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_NONHT);
				}
			}
			
	        m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
	        ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
	        (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

            pPacket->AddRef();
	        msghdr[0].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[0].chn      = chn;
	        msghdr[0].buf      = &ip;
	        msghdr[0].buf_len  = sizeof(ip);
	        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
	        msghdr[0].next     = &msghdr[1];

	        msghdr[1].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[1].chn      = chn;
	        msghdr[1].buf      = pPacket->GetBuffer() + index * ptk_slice_len;
	        msghdr[1].buf_len  = ptk_slice_len;
	        msghdr[1].msg_flags= 0;
	        msghdr[1].callback = ReleasePacket;
	        msghdr[1].context  = pPacket;
	        msghdr[1].iFrameFlag = PKT_FULL_FRAME;

	        DvrNetSendChain(m_conn, msghdr);
	        NetDvrSendOtherQueue(m_conn,chn, ACK_CHAN_MONIT);
			index++;
		}
		
		if (slice > 0)
		{
			memset(&ip, 0, sizeof(ip));
	        memset(&msghdr, 0, sizeof(msghdr));

	        ip.dvrip_cmd = ACK_CHAN_MONIT;
	        ip.dvrip_extlen= slice;
	        ip.dvrip_p[0]   = chn;
	        ip.dvrip_p[1]   = 0x00; 
			ip.dvrip_p[2] = frame_local2net[iType];
			ip.dvrip_p[3] = 0xff;
	        ip.dvrip_p[16]  = (stream == 0)?0:stream+4;

			if (0 == num)
			{
			    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_HEADTRAIL);
			}
			else
			{
			    ip.dvrip_p[2] = (ip.dvrip_p[2] | PACK_CONTAIN_FRAME_TRAIL);
			}
			
	        m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
	        ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
	        (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

            pPacket->AddRef();
	        msghdr[0].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[0].chn      = chn;
	        msghdr[0].buf      = &ip;
	        msghdr[0].buf_len  = sizeof(ip);
	        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
	        msghdr[0].next     = &msghdr[1];

	        msghdr[1].msgCmd   = ACK_CHAN_MONIT;
	        msghdr[1].chn      = chn;
	        msghdr[1].buf      = pPacket->GetBuffer() + num * ptk_slice_len;
	        msghdr[1].buf_len  = slice;
	        msghdr[1].msg_flags= 0;
	        msghdr[1].callback = ReleasePacket;
	        msghdr[1].context  = pPacket;
	        msghdr[1].iFrameFlag = PKT_FULL_FRAME;

	        DvrNetSendChain(m_conn, msghdr);
	        NetDvrSendOtherQueue(m_conn,chn, ACK_CHAN_MONIT);
		}	
#else
            pPacket->AddRef();

            DVRIP ip;
            msghdr_t msghdr[2] ;
            memset(&ip, 0, sizeof(ip));
            memset(&msghdr, 0, sizeof(msghdr));

            ip.dvrip_cmd= ACK_CHAN_MONIT;
            ip.dvrip_extlen= iLen;
            ip.dvrip_p[0]   = chn;
            ip.dvrip_p[1]   = 0x00; 
            ip.dvrip_p[16]  = (stream == 0)?0:stream+4;

            ip.dvrip_p[2] = frame_local2net[iType];

            //!合入帧头尾标志信息
            ip.dvrip_p[2] = (ip.dvrip_p[2] | iFlag);

            ip.dvrip_p[3] = 0xff;//!按帧发送时，每个包都只有一帧

            m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream]++;
            //!写入序号
            ulong* pSeq = (ulong*)(&ip.dvrip_p[12]);
            (*pSeq) = m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream];

            msghdr[0].msgCmd   = ACK_CHAN_MONIT;
            msghdr[0].chn      = chn;
            msghdr[0].buf      = &ip;
            msghdr[0].buf_len  = sizeof(ip);
            msghdr[0].msg_flags= MSG_FLAGS_MORE ;
            msghdr[0].next     = &msghdr[1];

            msghdr[1].msgCmd   = ACK_CHAN_MONIT;
            msghdr[1].chn      = chn;
            msghdr[1].buf      = pPacket->GetBuffer()+iLocation;
            msghdr[1].buf_len  = iLen;
            msghdr[1].msg_flags= 0;
            msghdr[1].callback = ReleasePacket;
            msghdr[1].context  = pPacket;
            msghdr[1].iFrameFlag = PKT_FULL_FRAME;

            MutexEnter(m_conn->hMutex);    
            for(msg = msghdr;msg; msg = msg->next) 
            {
                NetInsertDataToQueue(m_conn, msg);
            }
            MutexLeave(m_conn->hMutex);
#endif
        }
    }

    if(m_conn->DataTransferQueue[chn + 1].totalSize != 0)
    {
        NetDvrSendQueue(m_conn,chn,ACK_CHAN_MONIT);
    }    

    NetDvrSendOtherQueue(m_conn,chn,ACK_CHAN_MONIT);
    return ;

}

/*
 * 该函数用于开启实时监视
 */
int CaptureHelper::startVideoCapture(int chn,int stream)
{
    tracef("....chn[%d]\n", chn );

    // 如果正在升级，则不进入
    //if (CUpgrader::instance()->getState())
    //{
    //    return 0;
    //}

    startLocalVideoCapture( chn, stream );
    return 0;
}

int CaptureHelper::stopAllVideoCapture()
{
    CGuard tmpGuard(m_Mutex);
    printf("CaptureHelper::stopAllVideoCapture():in\r\n");

    for (int i = 0; i < (m_pCaptureM->GetLogicChnNum() ); i ++)
    {
        stopVideoCapture(i,0,0);
        stopVideoCapture(i,1,0);
    }

    return 0;
}
    
int CaptureHelper::stopVideoCapture(int chn,int stream,int flag)
{
    stopLocalVideoCapture( chn, stream, flag );
    return 0;
}

int CaptureHelper::startLocalVideoCapture(int chn,int stream)
{
    if (m_MonitorState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] ==0)
    {
        ICapture *pCapture = ICapture::instance(chn);
        trace("\n >>>>>>>>>>>>>>>>>>>>>>>>> start Local startVideoCapture >>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
        assert(pCapture);
        
        tracef("Start chn %d,stream %d\n",chn,stream);
        VD_BOOL     bRet =    false;

#if 0    ///< 这里会自动打开辅码流的音视频,目前没有这样的需求;先关闭 2010-10-19
        if(( 1 == g_stNetTransPolicy.bNetTransPolicy )
            &&(NET_TRANS_RATE == g_stNetTransPolicy.emNetTransState )
            &&(1 == stream))
        {
            CAPTURE_FORMAT cptChnFmt ;
            const CAPTURE_FORMAT *cptChnFmtTmp = NULL;
            

            cptChnFmtTmp = NULL;
            cptChnFmtTmp = pCapture->GetFormat(stream);

            if(NULL == cptChnFmtTmp) 
            {
                return -1;
            }

            //!如果音频都没有开的话, 那么把音视频全部打开
            if(0 == cptChnFmtTmp->AVOption)
            {
                memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));

                cptChnFmt.AVOption    = 1;
                cptChnFmt.AVOption |= BITMSK(1);        

                bRet = pCapture->SetFormat(&cptChnFmt, stream);     
            }
        }
 #endif
 
        /*
        * 做强制I帧操作,以便在客户端能够马上看到图像
        */
        pCapture->SetIFrame(stream);
        bRet = pCapture->Start(this,
                            (CDevCapture::SIG_DEV_CAP_BUFFER)&CaptureHelper::OnCapture, DATA_MONITOR,stream);
        if(bRet == TRUE) 
        {
            m_conn->netSTtateMonitor.ch_no_func[chn]=MONITOR;
            m_MonitorState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = 1;

            struct timeval time_now;
#ifdef LINUX
            gettimeofday(&time_now, NULL);
#else
            time_now.tv_sec = 0;
            time_now.tv_usec = SystemGetMSCount()*1000; 
#endif    
            memcpy(&m_stTransAdapter.stLastTime[chn+(m_pCaptureM->GetLogicChnNum() )*stream],&time_now,sizeof(timeval));
            memcpy(&m_stTransAdapter.stAdjustTime[chn+(m_pCaptureM->GetLogicChnNum() )*stream],&time_now,sizeof(timeval));
            m_stTransAdapter.bConfictFlag[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = 0;

            m_stFPSAdapter.emAdjustState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = PFrame_all;
            m_stFPSAdapter.iFpsAdapter[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = 0;
            m_stFPSAdapter.iPFrameNums[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = MAX_PFRAME_DROP;
            memcpy(&m_stFPSAdapter.stLastTime[chn+(m_pCaptureM->GetLogicChnNum() )*stream],&time_now,sizeof(timeval));

            return 0;
        }
    }
    return -1;
}

int CaptureHelper::stopLocalVideoCapture(int chn,int stream,int flag )
{
    if (m_MonitorState[chn+m_pCaptureM->GetLogicChnNum()*stream])
    {
        ICapture *pCapture = ICapture::instance(chn);
        assert(pCapture);
        
        VD_BOOL bRet  = pCapture->Stop(this, 
                                    (CDevCapture::SIG_DEV_CAP_BUFFER)&CaptureHelper::OnCapture, DATA_MONITOR,stream);

        tracef("Stop chn %d,stream %d Ret %d\n",chn,stream, bRet);
        
        if(bRet == TRUE) 
        {
            /* 通道chn的状态指示为没有使用 add by yang_shukui*/
            m_conn->netSTtateMonitor.ch_no_func[chn]=NOUSE;
            m_MonitorState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = 0;
            m_seq[chn+m_pCaptureM->GetLogicChnNum()*stream] = 0;

            /*清除末发送的数据*/
            NetDvrFreeQueueData(m_conn,ACK_CHAN_MONIT,chn,1);

            m_stFPSAdapter.emAdjustState[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = PFrame_all;
            m_stFPSAdapter.iFpsAdapter[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = 0;
            m_stFPSAdapter.iPFrameNums[chn+(m_pCaptureM->GetLogicChnNum() )*stream] = MAX_PFRAME_DROP;

            if((1 == g_stNetTransPolicy.bHaveDisk)&&(CHL_MAIN_T ==    stream))
            {
            }
            else
            {
                CAPTURE_FORMAT cptChnFmt ;
                const CAPTURE_FORMAT *cptChnFmtTmp = NULL;            
                cptChnFmtTmp = pCapture->GetFormat(stream);
                if(cptChnFmtTmp) 
                {
                    memcpy(&cptChnFmt, cptChnFmtTmp, sizeof(CAPTURE_FORMAT));
                    cptChnFmt.BitRate =  g_stNetTransPolicy.usDspRate[chn+(m_pCaptureM->GetLogicChnNum() )*stream] ;
                    cptChnFmt.FramesPerSecond = g_stNetTransPolicy.usFps[chn+(m_pCaptureM->GetLogicChnNum() )*stream] ;
                    cptChnFmt.ImageSize = g_stNetTransPolicy.usResolution[chn+(m_pCaptureM->GetLogicChnNum() )*stream];
                    cptChnFmt.BitRateControl = CAPTURE_BRC_CBR;

                    //pCapture->SetFormat(&cptChnFmt, stream);  
                }
            }
            
            m_stTransAdapter.bChangeStreamFlags[chn]= 0;
            m_conn->DataTransferQueue[chn +1].SocketSendFailFlag =0;

            struct timeval stTime;
            #ifdef LINUX
            gettimeofday(&stTime, NULL);
            #else
            stTime.tv_sec = 0;
            stTime.tv_usec = SystemGetMSCount()*1000; 
            #endif
            memcpy(&m_conn->DataTransferQueue[chn +1].SocetSendSleepTime,&stTime,sizeof(timeval));    

            return 0;
        }
    }
    return -1;
}

int CaptureHelper::startNetVideoCapture(int chn,int stream)
{
    return 0;
}

int CaptureHelper::stopNetVideoCapture(int chn,int stream,int flag )
{
    return 0;
}

/*
 * 下面这个函数用于处理画面监视的控制协议
 */
int CaptureHelper::FilterCallback(struct conn *c, DVRIP *pHead)
{
    int iMaxVideoNum=0;
    int iMaxVideoRate=0;
    int iDiff = 0;
        
    assert(c == m_conn);
    if(1 == c->iChangMainToExtraFlag)
    {
        NetChangeStream(false);
        tracef("&&&&&&&&&&&&&&&&&111\n");
        return 2;
    }
    else if(2 == c->iChangMainToExtraFlag)
    {
        NetChangeStream(true);
        tracef("&&&&&&&&&&&&&&&&&222\n");
        return 2;
    }

    //!不是请求媒体数据, 返回
    //if(NULL == pHead  ||( pHead && pHead->dvrip_cmd != REQ_CHAN_SWTCH))   //dengxiao
	if(NULL ==pHead || pHead->dvrip_cmd != REQ_CHAN_SWTCH)
    {
        return 0;    
    }

    DVRIP ip;
    memset(&ip, 0, sizeof(DVRIP));

    ip.dvrip_cmd = ACK_CHAN_MONIT;
    ip.dvrip_p[0] = pHead->dvrip_p[0];

    trace("CaptureHelper::FilterCallback-mode:%d\n", pHead->dvrip_p[18]);

    /* 通过连接数量来限制，还是通过计算配置的码流来限制 */
    VD_BOOL BLimitByNum=VD_TRUE;

    /* 目前只有纯模拟设备通过码率来限制发送视频路数 */
    if ( m_pCaptureM->GetDigitalChnNum() > 0 )
    {
        BLimitByNum=VD_TRUE;
    }
    else
    {
        BLimitByNum=VD_FALSE;
    }  

    /* 目前先不根据码率进行配置 */

    BLimitByNum=VD_TRUE;
    /* 带数字通道的按照连接数量来限制码流 */
    if (BLimitByNum)
    {
        iMaxVideoNum = m_pCaptureM->GetLogicChnNum()*10;

        if (iMaxVideoNum >= 48)
        {
            iMaxVideoNum = 48;
        }
        
        iDiff = CalcMonitorNumDiff(pHead);

        if (iDiff > 0 && (g_CaptureHelperManager.GetMonitorNum() + iDiff > iMaxVideoNum))
        {
            __trip;
            printf("CaptureHelper::FilterCallback():num >iMaxVideoNum(%d)\r\n",iMaxVideoNum);
            
            return 1;
        }
    }
    else
    {
        /* 最大码率 ，目前按照16*2048来处理*/
        iMaxVideoRate = 16*2048;
        iDiff = CalcMonitorRateDiff(pHead);

        if (iDiff > 0 && (g_CaptureHelperManager.GetMonitorRate() + iDiff > iMaxVideoRate))
        {
            __trip;
            printf("CaptureHelper::FilterCallback():Rate >iMaxVideoRate(%d)\r\n",iMaxVideoRate);
            return 1;
        }
    }
    
    switch(pHead->dvrip_p[18])
    {
        case EnMonMode_Tcp:
        {
            /*
             * 视频监控命令,开启处理线程
             */
#ifdef _PURE_DECODE /* 目前解码器的数字通道走的老的模拟通道的协议 */
             int ChNum = m_pCaptureM->GetDigitalChnNum();
#else
             int ChNum = m_pCaptureM->GetAnalogChnNum();
#endif
            if( ChNum <= 16 )
            {
                for(int i = 0; i < ChNum; i++)
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    if(0 != pHead->dvrip_extlen)//(len ==16)
                    {
                        char *stHead = NULL;
                        stHead = (char *)((char *)pHead+DVRIP_HEAD_T_SIZE+i);
                        if(stHead != NULL)
                        {
                            //码流类型检查
                            stream = *stHead;
                            CAPTURE_EXT_STREAM CfgExt;
                            CaptureGetExtCaps(&CfgExt);
                            if( CfgExt.ExtraStream & BITMSK(stream) == 0 )
                            {
                                warnf("Do not support this stream %d!\n", stream);
                                continue;
                            }
                        }
                    }

                    if((1 == m_stTransAdapter.bChangeStreamFlags[i] )    &&(CHL_MAIN_T == stream ))
                    {
                        stream = CHL_2END_T;
                    }

                    /*
                    * 监视状态发生改变,这里我们必须做相应的调整
                    */
                    if(pHead->dvrip_p[i] != m_MonitorState[i+m_pCaptureM->GetLogicChnNum()*stream])
                    {
                        if(pHead->dvrip_p[i] == CHANNEL_MONITOR_ON)
                        {                            
                            /*
                            * 开启该通道的画面监视
                            */
                            #if defined(DEF_UPDATE_VBR)
                            //UpdateVBRValue(2,1,BITMSK(i));
                            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                            g_Encode.getCapFormat(format);
                            UpdateVBRValue(2,1,BITMSK(i),format);
                            //tracef("*************UpdateVBRValue,2,1[%d]***********\n",i);
                            #endif

                            char auth[32];
                            sprintf(auth, "NetPreview_%02d",  i + 1);
                            //CUser* pUser  = (CUser* )m_conn->context;
                            #ifdef __BELL_QZTEL__
                            SesionInfo_t *sess = (SesionInfo_t *)m_conn->context;
                            CUser *pUser = (CUser*)sess->user;
                            #else    
                            CUser *pUser = (CUser*)m_conn->context;
                            #endif
                            if(pUser->isValidAuthority(auth))
                            {
                                    startVideoCapture(i,stream);
                            }
                            else
                            {
                                tracef("Channel [%d] No Authority!\n",i);
                            }
                        } 
                        else if(pHead->dvrip_p[i] == CHANNEL_MONITOR_OFF)
                        {
                            #if defined(DEF_UPDATE_VBR)
                            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                            g_Encode.getCapFormat(format);
                            UpdateVBRValue(2,2,BITMSK(i),format);
                            tracef("*************UpdateVBRValue,2,2[%d]***********\n",i);
                            #endif
                            /*
                            * 用户关闭了该通道的监视
                            */
                            stopVideoCapture(i,stream,0);

                        }
                    }
                } // for
            }
            else //! 大于16路，使用扩展数据格式监视操作协议
            {
                assert(0 != pHead->dvrip_extlen);
                int tlv_type = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16));
                int tlv_len = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16+4));
                if( tlv_type != 1
                    || tlv_len != (pHead->dvrip_extlen-16-8) )
                {
                    trace("monitor error tlv type:%d, tlv_len:%d, extlen:%d!\n",
                            tlv_type, tlv_len, pHead->dvrip_extlen);
                }
                else
                {
                    trace("monitor right tlv type:%d, tlv_len:%d, extlen:%d!\n",
                            tlv_type, tlv_len, pHead->dvrip_extlen);
                    CHANNEL_OPR* pChannelOpr = NULL;
                    pChannelOpr = (CHANNEL_OPR *)((char *)pHead+DVRIP_HEAD_T_SIZE+16+8);
                    for( int i = 0; i < ChNum; i++ )
                    {
                        int stream = 0;//!<存储码流类型, 主码流或辅助码流
                        stream = pChannelOpr->ChannelStream[i];
                        if(stream != 0 && stream != 1)
                        {
                            warnf("Do not support this chn:%d, stream %d!\n", i, stream);
                            continue;
                        }

                        if((1 == m_stTransAdapter.bChangeStreamFlags[i] )    &&(CHL_MAIN_T == stream ))
                        {
                            stream = CHL_2END_T;
                        }

                        if( pChannelOpr->ChannelCmd[i] != m_MonitorState[i+m_pCaptureM->GetLogicChnNum()*stream] )
                        {

                            if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_ON )
                            {
                                /*
                                * 开启该通道的画面监视
                                */
                                #if defined(DEF_UPDATE_VBR)
                                //UpdateVBRValue(2,1,BITMSK(i));
                                CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                                g_Encode.getCapFormat(format);
                                UpdateVBRValue(2,1,BITMSK(i),format);
                                //tracef("*************UpdateVBRValue,2,1[%d]***********\n",i);
                                #endif

                                char auth[32];
                                sprintf(auth, "NetPreview_%02d",  i + 1);
                                //CUser* pUser  = (CUser* )m_conn->context;
                                #ifdef __BELL_QZTEL__
                                SesionInfo_t *sess = (SesionInfo_t *)m_conn->context;
                                CUser *pUser = (CUser*)sess->user;
                                #else    
                                CUser *pUser = (CUser*)m_conn->context;
                                #endif
                                if(pUser->isValidAuthority(auth))
                                {
                                        startVideoCapture(i,stream);
                                }
                                else
                                {
                                    tracef("Channel [%d] No Authority!\n",i);
                                }
                            }
                            else if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_OFF )
                            {
                                #if defined(DEF_UPDATE_VBR)
                                CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                                g_Encode.getCapFormat(format);
                                UpdateVBRValue(2,2,BITMSK(i),format);
                                tracef("*************UpdateVBRValue,2,2[%d]***********\n",i);
                                #endif
                                /*
                                * 用户关闭了该通道的监视
                                */
                                stopVideoCapture(i,stream,0);

                            }
                        }
                    }
                }
            }
            break;    
        }
        case EnMonMode_digital:
        {
            infof("opr digital chn\n");
            int ChNum = m_pCaptureM->GetDigitalChnNum();
            if( ChNum <= 16 )
            {
                for(int i = 0; i < m_pCaptureM->GetDigitalChnNum(); i++)
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    if(0 != pHead->dvrip_extlen)//(len ==16)
                    {
                        char *stHead = NULL;
                        stHead = (char *)((char *)pHead+DVRIP_HEAD_T_SIZE+i);
                        if(stHead != NULL)
                        {
                            //码流类型检查
                            stream = *stHead;
                            CAPTURE_EXT_STREAM CfgExt;
                            CaptureGetExtCaps(&CfgExt);

                            if(stream != 0 && stream != 1)
                            {
                                warnf("Do not support this stream %d!\n", stream);
                                continue;
                            }
                        }
                    }

                    int tmpChn = i+m_pCaptureM->GetAnalogChnNum();
                    if(pHead->dvrip_p[i] != m_MonitorState[tmpChn+m_pCaptureM->GetLogicChnNum()*stream])
                    {
                        trace("i:%d, m:%d\n", i, pHead->dvrip_p[i]);
                        if(pHead->dvrip_p[i] == CHANNEL_MONITOR_ON)
                        {
                            /*
                            * 开启该通道的画面监视
                            */
                            char auth[32];
                            sprintf(auth, "NetPreview_%02d",  tmpChn + 1);
                            //CUser* pUser  = (CUser* )m_conn->context;
                            #ifdef __BELL_QZTEL__
                            SesionInfo_t *sess = (SesionInfo_t *)m_conn->context;
                            CUser *pUser = (CUser*)sess->user;
                            #else    
                            CUser *pUser = (CUser*)m_conn->context;
                            #endif
                            if(pUser->isValidAuthority(auth) )
                            {
                                    startVideoCapture(tmpChn, stream);
                            }
                            else
                            {
                                tracef("Channel [%d] No Authority!\n",i + m_pCaptureM->GetAnalogChnNum());
                            }
                        }
                        else if(pHead->dvrip_p[i] == CHANNEL_MONITOR_OFF)
                        {
                            /*
                            * 用户关闭了该通道的监视
                            */
                            stopVideoCapture(tmpChn, stream, 0);
                        }
                    }

                }
            }
            else
            {
                assert(0 != pHead->dvrip_extlen);
                int tlv_type = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16));
                int tlv_len = (*(int*)((char *)pHead+DVRIP_HEAD_T_SIZE+16+4));
                trace("monitor right tlv type:%d, tlv_len:%d, extlen:%d!\n",
                        tlv_type, tlv_len, pHead->dvrip_extlen);
                CHANNEL_OPR* pChannelOpr = NULL;
                pChannelOpr = (CHANNEL_OPR *)((char *)pHead+DVRIP_HEAD_T_SIZE+16+8);
                for( int i = 0; i < ChNum; i++ )
                {
                    int stream = 0;//!<存储码流类型, 主码流或辅助码流
                    stream = pChannelOpr->ChannelStream[i];
                    if(stream != 0 && stream != 1)
                    {
                        warnf("Do not support this chn:%d, stream %d!\n", i, stream);
                        continue;
                    }

                    if((1 == m_stTransAdapter.bChangeStreamFlags[i] )    &&(CHL_MAIN_T == stream ))
                    {
                        stream = CHL_2END_T;
                    }

                    if( pChannelOpr->ChannelCmd[i] != m_MonitorState[i+m_pCaptureM->GetAnalogChnNum()*stream] )
                    {

                        if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_ON )
                        {
                            /*
                            * 开启该通道的画面监视
                            */
                            #if defined(DEF_UPDATE_VBR)
                            //UpdateVBRValue(2,1,BITMSK(i));
                            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                            g_Encode.getCapFormat(format);
                            UpdateVBRValue(2,1,BITMSK(i),format);
                            //tracef("*************UpdateVBRValue,2,1[%d]***********\n",i);
                            #endif

                            char auth[32];
                            sprintf(auth, "NetPreview_%02d",  i + 1);
                            //CUser* pUser  = (CUser* )m_conn->context;
                            #ifdef __BELL_QZTEL__
                            SesionInfo_t *sess = (SesionInfo_t *)m_conn->context;
                            CUser *pUser = (CUser*)sess->user;
                            #else    
                            CUser *pUser = (CUser*)m_conn->context;
                            #endif
                            if(pUser->isValidAuthority(auth))
                            {
                                    startVideoCapture(i+m_pCaptureM->GetAnalogChnNum(),stream);
                            }
                            else
                            {
                                tracef("Channel [%d] No Authority!\n",i);
                            }
                        }
                        else if( pChannelOpr->ChannelCmd[i] == CHANNEL_MONITOR_OFF )
                        {
                            #if defined(DEF_UPDATE_VBR)
                            CAPTURE_FORMAT format[m_pCaptureM->GetLogicChnNum()];
                            g_Encode.getCapFormat(format);
                            UpdateVBRValue(2,2,BITMSK(i),format);
                            tracef("*************UpdateVBRValue,2,2[%d]***********\n",i);
                            #endif
                            /*
                            * 用户关闭了该通道的监视
                            */
                            stopVideoCapture(i+m_pCaptureM->GetAnalogChnNum(),stream,0);

                        }
                    }
                }
            }

        break;
    }
        default:
            break;
    }
    

    return 2;
}

VD_BOOL CaptureHelper::GetAllocFlag()
{
    return m_BAllocFlag;
}

VD_VOID CaptureHelper::SetAllocFlag(VD_BOOL BFlag)
{
    m_BAllocFlag = BFlag;
}

VD_VOID CaptureHelper::SetCon(struct conn *c)
{
    m_conn = c;
}

VodHelper::VodHelper(struct conn *c)
    :m_conn(c)
{
    m_pCaptureM = ICaptureManager::instance();
    assert(m_pCaptureM);
    
    for (int ii=0; ii<N_SYS_CH; ii++)
    {
        m_iDownloadState[ii] = netDownLoadStop;

        if( ii >= m_pCaptureM->GetLogicChnNum() )
        {
            m_pHDiskReader[ii] = NULL;    
        }
        else
        {
            m_pHDiskReader[ii] = new CDevHDiskReader;
        }
    }
}


VodHelper::~VodHelper() 
{
    for (int ii=0; ii<m_pCaptureM->GetLogicChnNum(); ii++)
    {
        if(m_pHDiskReader[ii] != NULL)
        {
            //这边不要停止了，要不然析构函数里就不能阻塞了。由于对象被销毁，所以里面的状态可以不用维护了。
            delete m_pHDiskReader[ii];
            m_pHDiskReader[ii] = NULL;
        }
    } 
}
void VodHelper::Init()
{
    for (int ii=0; ii<N_SYS_CH; ii++)
    {
        m_iDownloadState[ii] = netDownLoadStop;
    }	
}
void VodHelper::OnDownload(int iCMD, CPacket *pPacket, int iCh)
{
    if (netDownLoadStop == m_iDownloadState[iCh])
    {
        return ;
    }

    int iOldQueueSize = 0;
    int iNewQueueSize = 0;
    int iSendQueueSize = 0 ;

    if (pPacket) 
    {
        DVRIP* head;
        msghdr_t msghdr[2];

        pPacket->AddRef(); 
        
        head         	 	= (DVRIP *)pPacket->GetHeader();
        head->dvrip_cmd 	= ACK_RECD_PLAY;
    
        memset(&msghdr, 0, sizeof(msghdr));
        
        msghdr[0].msgCmd   = ACK_RECD_PLAY;
        msghdr[0].chn      = iCh;
        msghdr[0].buf      = head;
        msghdr[0].buf_len  = sizeof(DVRIP);
        msghdr[0].msg_flags= MSG_FLAGS_MORE ;
        msghdr[0].next     = &msghdr[1];

        msghdr[1].msgCmd   = ACK_RECD_PLAY;
        msghdr[1].chn      = iCh;    
        msghdr[1].buf      = pPacket->GetBuffer();
        msghdr[1].buf_len  = head->dvrip_extlen;
        msghdr[1].msg_flags= 0;
        msghdr[1].callback = ReleasePacket;
        msghdr[1].context  = pPacket;
        msghdr[1].iFrameFlag = PKT_FULL_FRAME;

//        tracef("&&&&&&&&&recdown chn %d,len %d,total size %d\n\n",iCh,head->extLen,m_OprConn->DataTransferQueue[m_pCaptureM->GetLogicChnNum()+ 1].totalSize/1024);
        iOldQueueSize = m_conn->DataTransferQueue[m_pCaptureM->GetLogicChnNum()+ 1].totalSize/1024 + sizeof(DVRIP) +head->dvrip_extlen;
        DvrNetSendChain(m_conn, msghdr);

        if(0 == g_stNetTransPolicy.bHighDownload)
        {
            iNewQueueSize = m_conn->DataTransferQueue[m_pCaptureM->GetLogicChnNum()+ 1].totalSize/1024;
            iSendQueueSize = iOldQueueSize - iNewQueueSize;
            if(iSendQueueSize > 60*1024)
            {
                SystemSleep(20);
            }
            else if(iSendQueueSize > 48*1024)
            {
                SystemSleep(15);
            }
            else if(iSendQueueSize > 36*1024)
            {
                SystemSleep(10);
            }
            else if(iSendQueueSize > 24*1024)
            {
                SystemSleep(5);
            }
            else if(iSendQueueSize > 12*1024)
            {
                SystemSleep(5);
            }
        }

        static int iCnt = 0;
        while(NetQueueOverLimit(m_conn)) 
        {

            iOldQueueSize = m_conn->DataTransferQueue[m_pCaptureM->GetLogicChnNum()+ 1].totalSize/1024 + sizeof(DVRIP) +head->dvrip_extlen;
            iCnt++;
            NetDvrSendQueue(m_conn, iCh, ACK_RECD_PLAY);

            if(0 == g_stNetTransPolicy.bHighDownload)
            {
                iNewQueueSize = m_conn->DataTransferQueue[m_pCaptureM->GetLogicChnNum()+ 1].totalSize/1024;
                iSendQueueSize = iOldQueueSize - iNewQueueSize;
                if(iSendQueueSize > 60*1024)
                {
                    SystemSleep(20);
                }
                else if(iSendQueueSize > 48*1024)
                {
                    SystemSleep(15);
                }
                else if(iSendQueueSize > 36*1024)
                {
                    SystemSleep(10);
                }
                else if(iSendQueueSize > 24*1024)
                {
                    SystemSleep(5);
                }
                else if(iSendQueueSize > 12*1024)
                {
                    SystemSleep(5);
                }
            }

            // 如果当前状态已经停止，并且循环次数超过50,需要跳出此循环，防止死循环
            // 目前的原因应该是在数据量大，在发送的过程中网络状况很糟糕或者网络断开可能会造成这种情况。
            if (iCnt >= 100)
            {
                tracepoint();
                iCnt = 0;
                break;
            }

            SystemSleep(5);
        }
        
        iCnt = 0;
    }
    else 
    {
        /*
         * 如果我们收到的Packet为空值,表示文件读取结束
         */
        m_conn->iRecDownNormalEndFlag = 1;
        
        stopDiskReader(iCh);

        m_conn->iRecDownNormalEndFlag = 0;
/*        
        //发送下载结束命令
        DVRIP szHdr;
        memset(&szHdr, 0, sizeof(szHdr));
        
        szHdr.dvrip_cmd= ACK_RECD_PLAY;
        // 第2字节：编码模式，11H表示实时录像。3XH表示MJPEG模式录像，其中X表示帧率1-2-3-4-5
        szHdr.dvrip_r0= 0x11;
        // 第3字节：第2字节的反码。
        szHdr.dvrip_r1= ~(szHdr.dvrip_r0);
        // 第4字节：回放权限。为0时允许回放，非0时不允许回放。
        // dhProtocolHead.reserv[2] = 1; // 录像下载ing，不容许回放
        // 第9字节：通道号。
        szHdr.dvrip_p[0] = iCh + 1;    //协议中是从1开始的
        // 结束
        //m_dhProtocolHead[iThrdIdx].extLen = 0;
        szHdr.dvrip_p[7] = 0x12; // ???
           tracef("sendStopDownloadCMD=======>>>>>>>>>>>>>>>\n");
       
        msghdr_t msghdr;
        memset(&msghdr, 0, sizeof(msghdr));
        msghdr.buf     = (void*)&szHdr;
        msghdr.buf_len = sizeof(szHdr);

        DvrNetSendMsg(m_conn, &msghdr);
*/
        m_conn->bVodFlag = true;
        m_conn->iVodChn = iCh;
    
    }

#ifndef _USE_720P_MODULE_ 
    /*此处不能去掉，用于控制发送速度*/
    SystemSleep(20);
#endif
    return ;
}

bool VodHelper::startDiskReader(int iChannel)
{
    bool bRet = false;
    tracef("CmdsRecDownload::StartHDiskReader(%d)\n", iChannel);

    if (NULL == m_pHDiskReader[iChannel])
    {
        __trip;
        return bRet;
    }
    
    if (m_iDownloadState[iChannel] == netDownLoadStart)
    {
        __fline;
        trace("Has been started.\n");
        bRet = true;
    }
    else
    {
        m_pHDiskReader[iChannel]->ResetPacketIdx();
        int TmpStat = m_iDownloadState[iChannel] ;
        m_iDownloadState[iChannel] = netDownLoadStart;
        bRet = m_pHDiskReader[iChannel]->Start(this,
                (CDevHDiskReader::SIG_DEV_HDISK_READER)&VodHelper::OnDownload, 
                iChannel) == TRUE;

        if (!bRet)
        {
            m_iDownloadState[iChannel] = TmpStat;
        }
    }

    return bRet;
}
bool VodHelper::pauseDiskReader(int iChannel)
{
    bool bRet = false;
    tracef("CmdsRecDownload::pauseHDiskReader(%d)\n", iChannel);
    
    if (NULL == m_pHDiskReader[iChannel])
    {
        __trip;
        return bRet;
    }

    if (netDownLoadStart != m_iDownloadState[iChannel])
    {
        __fline;
        trace("Has NOT been started.\n");
    }
    else
    {
        bRet = m_pHDiskReader[iChannel]->Stop(this,
                (CDevHDiskReader::SIG_DEV_HDISK_READER)&VodHelper::OnDownload) == TRUE;
        if (bRet)
        {
            trace("!!!!!chn %d stop read disk success\n", iChannel);
            
            m_iDownloadState[iChannel] = netDownLoadPause;
        }

    }
    
    return bRet;
}

bool VodHelper::stopDiskReader(int iChannel)
{
    bool bRet = false;
    tracef("CmdsRecDownload::StopHDiskReader(%d)\n", iChannel);

    if (NULL == m_pHDiskReader[iChannel])
    {
        return bRet;
    }

    if (netDownLoadStop == m_iDownloadState[iChannel])
    {
        m_conn->netSTtateMonitor.ch_no_func[N_SYS_CH]=NOUSE;
    }
    else
    {
        if (m_iDownloadState[iChannel] == netDownLoadStart)
        {
            __trip;
            //开始直接到结束            
            bRet = m_pHDiskReader[iChannel]->Stop(this,
                (CDevHDiskReader::SIG_DEV_HDISK_READER)&VodHelper::OnDownload) == TRUE;
        }
        else
        {
            __trip;
            //暂停到结束            
            bRet = TRUE;
        }

        if (bRet)
        {
            m_iDownloadState[iChannel] = netDownLoadStop;
            m_pHDiskReader[iChannel]->ResetPacketIdx();
        }

        if(0  == m_conn->iRecDownNormalEndFlag )
        {
            /*清除末发送的数据*/
            NetDvrFreeQueueData(m_conn,ACK_RECD_PLAY,iChannel,1);

        }
    }

    return bRet;
}
int VodHelper::stopAllVodCapture()
{
    printf("VodHelper::stopAllVodCapture():in\r\n");

    for (int i = 0; i < (m_pCaptureM->GetLogicChnNum() ); i ++)
    {
        stopDiskReader(i);
    }

    return 0;
}
int VodHelper::FilterCallback(struct conn *c, DVRIP *pHead)
{

    //!不是请求录像数据, 返回

    assert(c == m_conn);

    int iCh = (int)pHead->dvrip_p[0] - 1;

    if (iCh >= m_pCaptureM->GetLogicChnNum())
    {
        return 0;
    }
    
    switch(pHead->dvrip_cmd)
    {
        case REQ_RECD_DOWN:
        case REQ_RECD_PLAY:
        {
            /// 权限判断
#if 1
            char auth[32];
            sprintf(auth, "Replay_%02d",  iCh + 1);
            //CUser* pUser  = (CUser* )m_conn->context;
            #ifdef __BELL_QZTEL__
            SesionInfo_t *sess = (SesionInfo_t *)m_conn->context;
            CUser *pUser = (CUser*)sess->user;
            #else    
            CUser *pUser = (CUser*)m_conn->context;
            #endif
            if(false == pUser->isValidAuthority(auth))
            {
                DVRIP head;
                msghdr_t msghdr;
                memset(&head, 0, sizeof(head));
                memset(&msghdr, 0, sizeof(msghdr));
                
                msghdr.msgCmd    	= ACK_INFO_NORMAL;
                msghdr.buf        	= &head;
                msghdr.buf_len   	= sizeof(DVRIP);                
                head.dvrip_cmd		= ACK_INFO_NORMAL;                
                head.dvrip_p[0] 	= 0;
                head.dvrip_p[4] 	= 2;//GeneralMsgRetCode_NoAuth
                DvrNetSendMsg(c,&msghdr);
                tracef("Channel [%d] No Replay Authority!\n",iCh);
                return 0;
            }
#endif
            
            if (m_iDownloadState[iCh] == netDownLoadStart)
            {
                __trip;
                break;
            }

            uint iDisk;
            uint iClus;
            uint iPart = 0;

            iDisk = pHead->dvrip_p[8];


            if(0 != pHead->dvrip_p[15])
            {
                iPart = pHead->dvrip_p[15];
            }
            memcpy(&iClus, &(pHead->dvrip_p[9]), sizeof(uint) );
            trace("iDisk(%d) part (%d) iClus(%d)\n", iDisk, iPart,iClus);

            SYSTEM_TIME st;
            ushort year;
            memcpy(&year, &pHead->dvrip_p[1], sizeof(ushort));
            st.year         = year;
            st.month     	= pHead->dvrip_p[3];
            st.day         	= pHead->dvrip_p[4];
            st.hour         = pHead->dvrip_p[5];
            st.minute     	= pHead->dvrip_p[6];
            st.second     	= pHead->dvrip_p[7];

            if((1 == pHead->dvrip_p[19])
                || (2 == pHead->dvrip_p[19]))/*按时间段回放录像时,需要结束时间*/
            {
                trace("Have EndTime!!!!!\n");
                DHTIME dhTime;
                memset(&dhTime,0,sizeof(DHTIME));
                memcpy(&dhTime,&pHead->dvrip_p[20],4);
                SYSTEM_TIME et;
                timedh2sys(&et,&dhTime);
                trace("et.year:%d %d %d %d %d %d \n", 
                    et.year,
                    et.month,
                    et.day,
                    et.hour,
                    et.minute,
                    et.second);
                if (0 == iDisk && 0 == iClus)
                {
                    m_pHDiskReader[iCh]->NewLocate(iCh, &st, &et);
                }
                else
                {
                    m_pHDiskReader[iCh]->NewLocate(iDisk,iPart, iClus, &st, &et);
                }
            }
            else/*兼容旧版本,单独查询录像文件*/
            {
                tracef("NO EndTime!!\n");
				
                if (0 == iDisk && 0 == iClus)
                {
                    m_pHDiskReader[iCh]->Locate(iCh, &st);
                }
                else
                {
                    if(st.year <= 0 
                        ||(st.month < 1 || st.month > 12)
                        ||(st.day < 1 ||st.day > 31)
                        ||(st.hour < 0 ||st.hour > 23)
                        ||(st.minute < 0 || st.minute > 59)
                        ||(st.second < 0 || st.second > 59))
                    {
                        m_pHDiskReader[iCh]->Locate(iDisk,iPart, iClus, NULL);
                    }
                    else
                    {
                        m_pHDiskReader[iCh]->Locate(iDisk, iPart,iClus, &st);
                    }
                }

            }
            //
            // 记录日志
            //
            LOG_ITEM_RECORD lirSearch;
            lirSearch.chan = iCh+1;
            timesys2dh(&lirSearch.time, &st);
    
            if (REQ_RECD_DOWN == pHead->dvrip_cmd)
            {
                /* 通道chn的状态指示为下载 add by yang_shukui*/
                m_conn->netSTtateMonitor.ch_no_func[N_SYS_CH]=DOWNLOAD;
                g_Log.Append(LOG_DOWNLOAD, 0, &lirSearch);
            }
            else
            {
                /* 通道chn的状态指示为回放 add by yang_shukui*/
                m_conn->netSTtateMonitor.ch_no_func[N_SYS_CH]=PLAYBACK;
                g_Log.Append(LOG_PLAYBACK, 0, &lirSearch);
            }

		//三所监控	
		#ifdef SHREG
			g_SHReg.SetState(SH_TYPE_NETPLAY);
		#endif
            startDiskReader(iCh);
			
            break;
        }
        case REQ_PLAY_STOP:
        {
            if (stopDiskReader(iCh))
            {
                m_conn->netSTtateMonitor.ch_no_func[N_SYS_CH]=NOUSE;
                if(m_conn->TransferQueue.totalSize != 0)
                {
                    NetDvrFreeQueueData(m_conn,ACK_RECD_PLAY,iCh,1);
                }
            }

            break;
        }
        case REQ_PLAY_PAUSE:
        {
            //
            // redo after pause
            //
            if (1 == pHead->dvrip_r0)
            {
                startDiskReader(iCh);
            }
            else
            {
                pauseDiskReader(iCh);
            }

            break;
        }
        case REQ_CTRL_PLAY:
        {
            if (netDownLoadStop == m_iDownloadState[iCh])
            {
                /* 通道chn的状态指示为没有使用 add by yang_shukui*/
                m_conn->netSTtateMonitor.ch_no_func[N_SYS_CH]=NOUSE;
                break;
            }
            //
            // 第17-20字节: 从起始时间开始文件偏移大小
            //
            uint iOffset = *(uint*)&pHead->dvrip_p[8];
            iOffset *= 1024; /* 传过来的是KB */

            //清一下缓存，保证拖动立即有效
            if(m_conn->TransferQueue.totalSize != 0)
            {
                NetDvrFreeQueueData(m_conn,ACK_RECD_PLAY,iCh,1);
            }
            m_pHDiskReader[iCh]->Seek(iOffset);

            break;
        }
        default:
        {
            tracef("Error cmd when download rec\n");
            break;
        }
    }// switch

    return 2;
    
}
VD_BOOL VodHelper::GetAllocFlag()
{
    return m_BAllocFlag;
}

VD_VOID VodHelper::SetAllocFlag(VD_BOOL BFlag)
{
    m_BAllocFlag = BFlag;
}
VD_VOID VodHelper::SetCon(struct conn *c)
{
    m_conn = c;
}
AudioHelper::AudioHelper(struct conn *c)
    :m_conn(c)
{
    for (int ii=0; ii<N_AUDIO_IN; ii++)
    {
        m_bNeedTransAudioData[ii] = false;
        m_bIsChnOnAudioCapture[ii] = false;
    }
}
AudioHelper::~AudioHelper( )
{
    for(int i = 0; i < g_nAudioIn; i ++) 
    {
        if(m_bIsChnOnAudioCapture[i] == true) 
        {
            this->StopAudioCapture(i);
            /*
             * 避免可能潜在的错误问题
             */
            SystemSleep(80);
        }
    }
}

void AudioHelper::Init()
{
    for (int ii=0; ii<N_AUDIO_IN; ii++)
    {
        m_bNeedTransAudioData[ii] = false;
        m_bIsChnOnAudioCapture[ii] = false;
    }

    m_bIsAudioStarted = false;
}
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
//extern "C"	int32_t GLNK_AudioDecode_CallBack(unsigned char channel, char* buffer, unsigned int length);

int AudioHelper::FilterCallback(struct conn *c, DVRIP *pHead)
{
    assert(c == m_conn);

    //应答
    
    switch(pHead->dvrip_cmd)
    {
        case REQ_SEND_AUDIO:
        {
            DVRIP head;
            msghdr_t msghdr;
            memset(&head, 0, sizeof(head));
            memset(&msghdr, 0, sizeof(msghdr));
            
            msghdr.msgCmd   = ACK_SEND_AUDIO;
            msghdr.chn      = pHead->dvrip_p[4];
            msghdr.buf      = &head;
            msghdr.buf_len  = sizeof(DVRIP);
            msghdr.msg_flags= MSG_FLAGS_MORE ;            

            head.dvrip_cmd= ACK_SEND_AUDIO;    
            head.dvrip_p[4] = pHead->dvrip_p[4];
               // 屏蔽对讲功能
            if ( 0 == g_CapsEx.HasAudioBoard)
            {
                head.dvrip_p[0] = 0;
                DvrNetSendMsg(c,&msghdr);
                return 0;
            }
			else//对讲，web过来的音频不作处理 mod 20161223
            {
              //_printd("3");
              //  head.dvrip_p[0] = 0;
              //  DvrNetSendMsg(c,&msghdr);
              // return 0;
            }

            if (2 == pHead->dvrip_p[0])
            {            
                //tracef("to Play Audio!dvrip_extlen=[%d] dvrip_p[5]=[%d]\r\n",pHead->dvrip_extlen,pHead->dvrip_p[5]);
                if ( pHead->dvrip_extlen <= 0 || pHead->dvrip_extlen > 10*1024)
                {
                    head.dvrip_p[0] = 0;
                    DvrNetSendMsg(c,&msghdr);
                    return 0;
                }
#if 1
				#ifdef MOBILE_COUNTRY
				//由于底层已经修改接口所以用SDK的接口方便接起来 modify by jwd on 20190221
				g_MobileCountrySdkApi.AppendTalkAudioData((uchar*)&(pHead->dvrip_p[24]), pHead->dvrip_extlen);
				#endif
//				 GLNK_AudioDecode_CallBack(0,(char*) &(pHead->dvrip_p[24]), pHead->dvrip_extlen);
#else

                g_AudioManager.Lock();
                
                if (!g_AudioManager.PlayVoice(&(pHead->dvrip_p[24]),pHead->dvrip_extlen, AUDIO_TALK_TYPE, pHead->dvrip_p[5]))
                {
                    m_bIsAudioStarted = false;
                }
                else
                {
                    if (m_bIsAudioStarted == false)
                    {
                        SetAudioInFormat(pHead->dvrip_p[4], pHead->dvrip_p[5]);
                    }
                    m_bIsAudioStarted = true;
                }
                                
                g_AudioManager.UnLock();
#endif				
                head.dvrip_p[0] = 1;
                DvrNetSendMsg(c,&msghdr);
                return 0;
                
            }
            else if (3 == pHead->dvrip_p[0])
            {
                tracef("to stop Audio!\n");

                if (false == m_bIsAudioStarted)
                {
                    __fline;
                    trace("Audio has not been started.");
                    return -1;
                }
                
                g_AudioManager.StopAudio(AUDIO_TALK_TYPE);

                m_bIsAudioStarted = false;
                
                head.dvrip_p[0] = 1;
                DvrNetSendMsg(c,&msghdr);
                
                return 0;                    
            }

            break;
        }
        case REQ_AUDIO_DATA:
        {
            int iChn  = pHead->dvrip_p[0];
            if (CHANNEL_AUDIO_DATA_ON == pHead->dvrip_p[4])
            {

                if (false == m_bIsChnOnAudioCapture[iChn])
                {
                    m_bIsChnOnAudioCapture[iChn] = StartAudioCapture(iChn);
                    m_bNeedTransAudioData[iChn] = m_bIsChnOnAudioCapture[iChn];
                }
                else
                {
                    tracef("Audio has been started\n"); 
                    m_bNeedTransAudioData[iChn] = true;
                }
            }
            else if (CHANNEL_AUDIO_DATA_OFF == pHead->dvrip_p[4]
                && true == m_bIsChnOnAudioCapture[iChn])
            {
                m_bNeedTransAudioData[iChn] = false;

                int kk = 0;
                for (kk=0; kk<C_MAX_TCP_CONN_NUM; kk++)
                {
                    if (true == m_bNeedTransAudioData[iChn])
                    {
                        break;
                    }
                }

                if (kk >= C_MAX_TCP_CONN_NUM)
                {
                    m_bIsChnOnAudioCapture[iChn] = !(this->StopAudioCapture(iChn));
                    tracef("Do stop monitor:%d\n", m_bIsChnOnAudioCapture[iChn]); 
                }
                else
                {
                    tracef("Audio has been used, do not stop here.\n"); 
                }
            }
            break;
        }
        default:
            break;

    }

    return 0;
}

void AudioHelper::OnAudioCapture(int iChannel, CPacket *pPacket)
{
/*
    if(1 == isQueueOverLimit())
    {
        NetDvrSendQueue(m_OprConn, iChannel, ACK_REQ_AUDIO_DATA);
        return;    
    }
*/    
//fyj 20170227
    DVRIP head;
    memset(&head, 0, sizeof(head));
    head.dvrip_cmd= ACK_REQ_AUDIO_DATA;
    head.dvrip_p[0]     = iChannel;
    head.dvrip_extlen	= pPacket->GetLength() - 8;

    msghdr_t msghdr[2];

    memset(&msghdr, 0, sizeof(msghdr));
    
    pPacket->AddRef(); 


    msghdr[0].msgCmd   = ACK_REQ_AUDIO_DATA;
    msghdr[0].chn      = iChannel;

    
    msghdr[0].buf      = &head;
    msghdr[0].buf_len  = sizeof(head);
    msghdr[0].msg_flags= MSG_FLAGS_MORE ;
    msghdr[0].next     = &msghdr[1];

    msghdr[1].msgCmd   = ACK_REQ_AUDIO_DATA;
    msghdr[1].chn      = iChannel;

    msghdr[1].buf      = pPacket->GetBuffer();
    msghdr[1].buf_len  = pPacket->GetLength() - 8;//不要时间戳
    msghdr[1].msg_flags= 0;
    msghdr[1].callback = ReleasePacket;
    msghdr[1].context  = pPacket;
    
    DvrNetSendChain(m_conn, msghdr);
}

bool AudioHelper::StartAudioCapture(int iChannel)
{
    if (iChannel != 0)
    {
        __trip;
    }

    CDevAudioIn *pCapture = CDevAudioIn::instance(iChannel);

    if (NULL != pCapture)
    {
        trace("StartAudioCapture(%d)\n", iChannel);

        return pCapture->Start(this,
                               (CDevAudioIn::SIG_DEV_CAP_BUFFER)&AudioHelper::OnAudioCapture) == TRUE;
    }
    else
    {
        __fline;
        trace("GetAudioIn failed!!\n");
    }

    return false;
}

bool AudioHelper::StopAudioCapture(int iChannel)
{
    CDevAudioIn *pCapture = CDevAudioIn::instance(iChannel);

    if (NULL != pCapture)
    {
        trace("StopAudioCapture(%d)\n", iChannel);
        pCapture->Stop(this, (CDevAudioIn::SIG_DEV_CAP_BUFFER)&AudioHelper::OnAudioCapture);
    }
    else
    {
        __fline;
        trace("GetAudioIn failed!!\n");
    }
    return true;
}
int AudioHelper::stopAllAudioCapture()
{
    printf("CaptureHelper::stopAllVideoCapture():in\r\n");
    for(int i = 0; i < g_nAudioIn; i ++) 
    {
        StopAudioCapture(i);
    }

    return 0;
}
//设置设备端语音对讲编码模式
bool AudioHelper::SetAudioInFormat(int iChannel, int iFormat)
{
    CDevAudioIn *pCapture = CDevAudioIn::instance(iChannel);

    if (NULL != pCapture)
    {
        AUDIOIN_FORMAT strAudio_Format;
        memset(&strAudio_Format, 0, sizeof(AUDIOIN_FORMAT));

        switch(iFormat)
        {
            case AUDIO_FORMAT_PCM8:
                strAudio_Format.EncodeType = PCM_ALAW;
                //strAudio_Format.EncodeType = PCM_8TO16BIT;
                break;
            case AUDIO_FORMAT_G711a:
                strAudio_Format.EncodeType = G711_ALAW;
                break;
            case AUDIO_FORMAT_AMR:
                strAudio_Format.EncodeType = AMR8K16BIT;
                break;
            case AUDIO_FORMAT_G711u:
                strAudio_Format.EncodeType = G711_ULAW;
                break;
            default:                
                strAudio_Format.EncodeType = PCM_ALAW;
                //strAudio_Format.EncodeType = PCM_8TO16BIT;
                break;            
        }
        pCapture->SetFormat(iChannel, &strAudio_Format);        
        trace("SetAudioInFormat::iChannel = %d ## iFormat = %d ## strAudio_Format.EncodeType = %d\n",
            iChannel, iFormat, strAudio_Format.EncodeType);
    }
    else
    {
        trace("SetAudioInFormat::GetAudio Failed!\n");
        return false;
    }
    return true;
}

VD_BOOL AudioHelper::GetAllocFlag()
{
    return m_BAllocFlag;
}

VD_VOID AudioHelper::SetAllocFlag(VD_BOOL BFlag)
{
    m_BAllocFlag = BFlag;
}
VD_VOID AudioHelper::SetCon(struct conn *c)
{
    m_conn = c;
}
PATTERN_SINGLETON_IMPLEMENT(CaptureHelperManager);

CaptureHelperManager::CaptureHelperManager()
#ifdef RECORD_NET_THREAD_MULTI 
    :CThread("CaptureHelperManager", TP_NET)
{
    CreateThread();
}
#else
{
    
}
#endif

CaptureHelperManager::~CaptureHelperManager()
{
    
}

CaptureHelper *CaptureHelperManager::AllocCaptureHelper(struct conn *c)
{
    assert(c);
    CGuard tmp(m_list_Pool_Mutex);
    
    CapHlp_List::iterator nodeIt;
    int i=0;

//    printf("CaptureHelper::Reset():g_iResetNum = %d g_iAllocNum = %d g_iFreeNum = %dm_CHPool = %d\r\n",
//        g_iResetNum,g_iAllocNum,g_iFreeNum,m_CHPool.size());
    
    for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++,i++ )
    {
        if (0 == (*nodeIt)->GetAllocFlag())
        {
            (*nodeIt)->SetCon(c);
            (*nodeIt)->SetAllocFlag(1);
            (*nodeIt)->Init();
            return *nodeIt;
        }
    }

    if (m_CHPool.size() > 51)
    {
        __trip;
        printf("m_CHPool.size() = %d\r\n",m_CHPool.size());
//        return NULL;
    }
    
    CaptureHelper* pNetCH = new CaptureHelper(c);
    assert(pNetCH);
    
    m_CHPool.push_back(pNetCH);    
    pNetCH->SetAllocFlag(1);
    
    return pNetCH;
}

bool CaptureHelperManager::FreeCaptureHelper(CaptureHelper* pstCH)
{
    CGuard tmp(m_list_Pool_Mutex);
    
    int i=0;
    CapHlp_List::iterator nodeIt;

    for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++,i++ )
    {
        if( *nodeIt == pstCH )
        {
   		(*nodeIt)->stopAllVideoCapture();      
            (*nodeIt)->SetAllocFlag(0);
            (*nodeIt)->Reset();
            return true;
        }
    }
    
    return false;
}


VodHelper *CaptureHelperManager::AllocCaptureVodHelper(struct conn *c)
{
    assert(c);
    CGuard tmp(m_Vodlist_Pool_Mutex);
    
    CapVodHlp_List::iterator nodeIt;
    int i=0;

//    printf("CaptureHelper::Reset():g_iResetNum = %d g_iAllocNum = %d g_iFreeNum = %dm_CHPool = %d\r\n",
//        g_iResetNum,g_iAllocNum,g_iFreeNum,m_CHPool.size());
    
    for( nodeIt = m_CHVodPool.begin(); nodeIt != m_CHVodPool.end(); nodeIt++,i++ )
    {
        if (0 == (*nodeIt)->GetAllocFlag())
        {
            (*nodeIt)->SetCon(c);
            (*nodeIt)->SetAllocFlag(1);
            (*nodeIt)->Init();
            return *nodeIt;
        }
    }

    if (m_CHVodPool.size() > 51)
    {
        __trip;
        printf("m_CHPool.size() = %d\r\n",m_CHVodPool.size());
//        return NULL;
    }
    
    VodHelper* pNetCH = new VodHelper(c);
    assert(pNetCH);
    
    m_CHVodPool.push_back(pNetCH);    
    pNetCH->SetAllocFlag(1);
    
    return pNetCH;
}
bool CaptureHelperManager::FreeCaptureVodHelper(VodHelper* pstCH)
{
    CGuard tmp(m_Vodlist_Pool_Mutex);
    
    int i=0;
    CapVodHlp_List::iterator nodeIt;

    for( nodeIt = m_CHVodPool.begin(); nodeIt != m_CHVodPool.end(); nodeIt++,i++ )
    {
        if( *nodeIt == pstCH )
        {
   		(*nodeIt)->stopAllVodCapture();      
            (*nodeIt)->SetAllocFlag(0);
     //       (*nodeIt)->Reset();
            return true;
        }
    }
    
    return false;
}
AudioHelper *CaptureHelperManager::AllocCaptureAudioHelper(struct conn *c)
{
    assert(c);
    CGuard tmp(m_Audiolist_Pool_Mutex);
    
    CapAudioHlp_List::iterator nodeIt;
    int i=0;

//    printf("CaptureHelper::Reset():g_iResetNum = %d g_iAllocNum = %d g_iFreeNum = %dm_CHPool = %d\r\n",
//        g_iResetNum,g_iAllocNum,g_iFreeNum,m_CHPool.size());
    
    for( nodeIt = m_CHAudioPool.begin(); nodeIt != m_CHAudioPool.end(); nodeIt++,i++ )
    {
        if (0 == (*nodeIt)->GetAllocFlag())
        {
            (*nodeIt)->SetCon(c);
            (*nodeIt)->SetAllocFlag(1);
            (*nodeIt)->Init();
            return *nodeIt;
        }
    }

    if (m_CHAudioPool.size() > 51)
    {
        __trip;
        printf("m_CHPool.size() = %d\r\n",m_CHAudioPool.size());
//        return NULL;
    }
    
    AudioHelper* pNetCH = new AudioHelper(c);
    assert(pNetCH);
    
    m_CHAudioPool.push_back(pNetCH);    
    pNetCH->SetAllocFlag(1);
    
    return pNetCH;
}
bool CaptureHelperManager::FreeCaptureAudioHelper(AudioHelper* pstCH)	
{
    CGuard tmp(m_Audiolist_Pool_Mutex);
    
    int i=0;
    CapAudioHlp_List::iterator nodeIt;

    for( nodeIt = m_CHAudioPool.begin(); nodeIt != m_CHAudioPool.end(); nodeIt++,i++ )
    {
        if( *nodeIt == pstCH )
        {
   		(*nodeIt)->stopAllAudioCapture();      
            (*nodeIt)->SetAllocFlag(0);
    //        (*nodeIt)->Reset();
            return true;
        }
    }
    
    return false;
}
void CaptureHelperManager::StopAllMonitor()
{
    CGuard tmp(m_list_Pool_Mutex);
    
    CapHlp_List::iterator nodeIt;

    for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++)
    {
        (*nodeIt)->stopAllVideoCapture();
    }
    
    return;
}

/* 正在监视的通道数目 */
int CaptureHelperManager::GetMonitorNum()
{
    CGuard tmp(m_list_Pool_Mutex);
    int iMonitorNum = 0;
    CapHlp_List::iterator nodeIt;

    for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++)
    {
        if (1 == (*nodeIt)->GetAllocFlag())
        {
            iMonitorNum+= (*nodeIt)->GetMonitorNum();
        }
    }
    
    printf("CaptureHelperManager::GetMonitorNum(): %d\r\n",iMonitorNum);
    
    return iMonitorNum;
}

/* 正在监视的码流大小(K) */
int CaptureHelperManager::GetMonitorRate()
{
    CGuard tmp(m_list_Pool_Mutex);
    int iMonitorRate = 0;
    CapHlp_List::iterator nodeIt;

    for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++)
    {
        if (1 == (*nodeIt)->GetAllocFlag())
        {
            iMonitorRate+= (*nodeIt)->GetMonitorRate();
        }
    }
    
    printf("CaptureHelperManager::GetMonitorRate(): %d\r\n",iMonitorRate);
    return iMonitorRate;
}


#ifdef RECORD_NET_THREAD_MULTI 

void CaptureHelperManager::ThreadProc()
{
    bool bSleep = true;
    uint count_sleep = 0;
    CapHlp_List::iterator nodeIt;

	SET_THREAD_NAME("CaptureHelperManager");
	
    while( m_bLoop )
    {
        bSleep = true;

        m_list_Pool_Mutex.Enter();
        for( nodeIt = m_CHPool.begin(); nodeIt != m_CHPool.end(); nodeIt++)
        {
            CaptureHelper* pCH = (*nodeIt);
            if( pCH->GetAllocFlag() == 1 )
            {
                if( pCH->SendChnToNet() )
                {
                    bSleep = false;
                }
            }
        }
        m_list_Pool_Mutex.Leave();

        //!是否需要休眠
        if( bSleep )
        {
            count_sleep++;
            if( count_sleep > 0 )
            {
                SystemSleep(10);
                count_sleep = 0;
            }
        }
        else
        {
            count_sleep = 0;
        }
    }
}

#endif

