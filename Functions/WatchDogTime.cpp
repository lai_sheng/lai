/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
/*
 * WatchDogTime.cpp.cpp - _explain_
 *
 * Copyright (C) 2005 Technologies, All Rights Reserved.
 *
 * v 1.0.0.1 2006-02-16 14:13:32 wangk 
 *
 */
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
#if defined(FUNC_WDT) 

#include "APIs/DVRDEF.H"
#include "Functions/WatchDogTime.h"
#include "System/BaseTypedef.h"

#include "Net/NetConfig.h"
#include "Configs/ConfigManager.h"

#include "APIs/WatchDog.h"
#include "APIs/System.h"
#include "Main.h"

PATTERN_SINGLETON_IMPLEMENT(WatchDogTime);

WatchDogTime::WatchDogTime():CThread("WATCHDOGTIME", WATCH_DOG)
{
	trace("WatchDogTime::WatchDogTime()>>>>>>>>>\n");
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

WatchDogTime::~WatchDogTime()
{
	WatchdogDestory();
	trace("WatchdogDestory::leave app off monitor\n");
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/
extern "C" {
	        int ReadResetState();

}

void WatchDogTime::ThreadProc(void)
{
	int uRet = 40; 
	int iInterval = 0;
	int iRet = -1;
	bool bChecked = false;

	do 
	{
#ifdef IPC_JZ_NEW	
	if(m_irebootflag < 2 && iInterval < 120 )
	{
		WatchdogClear();//?11·
		SystemSleep(1000*1);
	}
#else
	if(m_irebootflag < 2 && iInterval < 120 )
	{
		WatchdogWrite(&uRet, 4);
		#if defined(VN_IPC) 
		WatchdogClear();
		#endif
	}
	SystemSleep(1000*1000);
	//sleep(1);
	#ifdef SDK_3512 //zhaohy debug 20130408
	if(ReadResetState())
	{
		g_Config.SetDefaultConfig(DEFAULT_CFG_ALL);
		sleep(1);
		system("reboot -f");
	}
	#endif
#endif
		if(1 == m_irebootflag)
		{
			iInterval++; //大于2分钟未初始化完成重启
			_printd("iInterval[%d]", iInterval);
		}

	} while (m_bLoop);
	
	trace("watchdog thread is stop!\n");
}

/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

void WatchDogTime::Start(void)
{
	int time_out_sec = 120;
	int ret = -1;
	m_irebootflag = 0;
	trace("WatchDogTime::Start()>>>>>>>>>>>>>>>>>>>>>\n");
#ifdef IPC_JZ_NEW
	WatchdogCreate();
	WatchdogWrite(&time_out_sec,1);
	CreateThread();
#else
	WatchdogCreate();	
	#if defined(VN_IPC)
		WatchdogClear();
	#endif//(VN_IPC)
	CreateThread();
#endif
}

void WatchDogTime::Stop()
{
	WatchdogDestory();
}
void WatchDogTime::SetRebootFlag(int rebootflag)
{
	m_irebootflag = rebootflag;
	_printd("=========m_irebootflag========[%d]",m_irebootflag);
}
#endif
/*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-*/

