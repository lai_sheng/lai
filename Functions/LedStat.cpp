#include <fstream>
#include <iostream>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include "Functions/LedStat.h"
#include "Functions/WifiLink.h"
#include "Configs/ConfigGeneral.h"

#include "System/Msg.h"
#include "APIs/DVRDEF.H"
#include "System/BaseTypedef.h"
#include "Intervideo/RealTime_apiv1.h"
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HWComunication.h"
#endif
#include <sys/shm.h>

#define LEDSWITCH_PATH "/tmp/Led_Switch"

struct Msg{
	long msgtype;
	int  msgtxt;
};
struct shared_use_st
{
	int ledstat;
};

PATTERN_SINGLETON_IMPLEMENT(LedStat);

LedStat::LedStat():CThread("LEDSTAT", TP_GUI)
{
	m_nSeverStatus = 0;
	printf("==>>LedStat::stat()>>>>>>>>>\n");
}


LedStat::~LedStat()
{
	printf("LedStat::end\n");
}

//1s test once, 1 minutes to send a light status, if the network state changes, timely hair
void LedStat::ThreadProc(void)
{
	static int oldledstat = -1;
	int count = 0;
	int wifilink = 0;
	int nRet = -1;
	static int msqid = -1;	
	static int nstat = LED_STAT_NO_LINK;
	struct Msg data;

	static void *shm = NULL;//分配的共享内存的原始首地址
	static struct shared_use_st *shared = NULL;//指向shm
	static int shmid = -1;//共享内存标识符
	
	//sleep(60);
	while(m_bLoop) 
	{
	#if 1  
		g_WifiLink.GetWifiLinkStat(&wifilink);
		if(1 == wifilink){
			SystemSleep(1*1000);
			continue;
		}
	#endif
		
		#if 0
			if(msqid < 0)
			{
				msqid = msgget(IPCAM_LED_STAT, IPC_CREAT|0666);
				if(msqid <0)
				{
					_printd("failed to create msq | errno MSGKEY[%d]\n", IPCAM_LED_STAT); 
					SystemSleep(3*1000);
					continue;
				}
			}

			//0 link PASS     -1 not link
			nRet = RealTimeConnectToServerStatus();
			nstat = (nRet == 0) ? LED_STAT_LINK : LED_STAT_NO_LINK;
			if(oldledstat != nstat || ((oldledstat == nstat) && (count >=3))){
				_printd("==>>LedStat===>>>>%s\n", nstat==1 ? "link" : "no link");
				memset(&data,0,sizeof(struct Msg));
				data.msgtype = 11; //这里要配合daemon改，不能随便动
				data.msgtxt  = nstat;
				if(msgsnd(msqid, (void*)&data, sizeof(struct Msg) - sizeof(long), IPC_NOWAIT) == -1)  
				{  
					msqid = msgget(IPCAM_LED_STAT, IPC_CREAT|0666);
					_printd("failed to snd msg | errno MSGKEY[%d]\n", IPCAM_LED_STAT); 	 
				} 
				count = 0;
			}else{
				count++;
			}
			oldledstat = nstat;

		#else
	
		if (shmid < 0){
		    //创建共享内存
		    shmid = shmget((key_t)IPCAM_LED_STAT, sizeof(struct shared_use_st), 0666|IPC_CREAT);
		    if(shmid < 0 ){
		        perror("shmget failed\n");
			  _printd("shmget failed\n");
		    }
		}

		if (shm == NULL){
		    //将共享内存连接到当前进程的地址空间
		    shm = shmat(shmid, 0, 0);
		    if(shm == NULL){
		        perror("shmat failed\n");
			  _printd("shmat failed\n") ; 	
		    }
		    _printd("\nMemory attached at %X\n", (int)shm);
		}

		if (shared == NULL){
		    //设置共享内存
			shared = (struct shared_use_st*)shm;
			shared->ledstat = LED_STAT_NO_LINK;
		}else {
#ifdef HEMU_CLOUD
		 	nRet = g_HWComm.GetConectStatus();
#endif
#ifdef MOBILE_COUNTRY
			nRet = m_nSeverStatus;
#endif
			nstat = (nRet == 2) ? LED_STAT_LINK : LED_STAT_NO_LINK;
			if (nstat != shared->ledstat){
				shared->ledstat = nstat;
			}
		}	
		#endif
		SystemSleep(1*1000);
	}
	
	_printd("LedStat thread stop!\n");
}

void LedStat::Start(void)
{
	_printd("==>>LedStat thread start!\n");
	CreateThread();
}




int LedStat::OpenLedSwitch()
{
	ofstream ledFile(LEDSWITCH_PATH);

	if(!ledFile.is_open()){
		_printd("==>>open %s fail!\n",LEDSWITCH_PATH);
		return 1;
	}

	ledFile <<"ON"<<endl;

	ledFile.close();

	return 0;

}
int LedStat::CloseLedSwitch()
{
	ofstream ledFile(LEDSWITCH_PATH);

	if(!ledFile.is_open()){
		_printd("==>>open %s fail!\n",LEDSWITCH_PATH);
		return 1;
	}

	ledFile <<"OFF"<<endl;

	ledFile.close();

	return 0;

}
int LedStat::ForceOpenLed()
{
	OpenLedSwitch();

	return 0;

}
int LedStat::Resume_ForceOpenLed()
{
	int val = 0;
	CConfigGeneral cCfgGeneral;

	cCfgGeneral.update();
	CONFIG_GENERAL &config	= cCfgGeneral.getConfig();
	val = config.iLedSwitch;

	std::cout<<"val:"<<val<<"\n"<<endl;
	if(1 == val){
		return CloseLedSwitch();
	}
	else{
		return OpenLedSwitch();
	}
	

	
}

int LedStat::SetLedSwitch(int val/*1:turn off ; other:open*/,int update)
{
	if(1 == val){
		CloseLedSwitch();
	}
	else{
		OpenLedSwitch();
	}

	if(update){
		CConfigGeneral cCfgGeneral;
		cCfgGeneral.update();
		
		CONFIG_GENERAL &config  = cCfgGeneral.getConfig();
		if(1 == val){
			config.iLedSwitch = 1;
		}
		else{
			config.iLedSwitch = 0;
		}
		
		cCfgGeneral.commit();

	}
	
	return 0;
}
/*1:turn off ; 0: turn on*/
int LedStat::GetLedSwitch()
{
	CConfigGeneral cCfgGeneral;
	cCfgGeneral.update();
	
	CONFIG_GENERAL &config  = cCfgGeneral.getConfig();
	
	return config.iLedSwitch;
}

