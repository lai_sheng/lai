#include <stdio.h>
#include <error.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "Functions/MP3Play.h"
#include "Net/Dlg/DlgNtpCli.h"
#include "System/BaseTypedef.h"
#include "Functions/IntelligentDoorLock.h"
#include "Devices/DevExternal.h"


#define LOCK_WARN_VOICE_ID			(100)
#define LOCK_WARN_VOICE_NAME		"warn_voice"

static int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  

static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) 
	{  
        fwrite(ptr, nmemb, size,  fp); 
		fclose(fp);
    }  
    _printd("download size[%dkb]\n",len/1024);      
    return written;  
} 

PATTERN_SINGLETON_IMPLEMENT(CDoorLockManager);

CDoorLockManager::CDoorLockManager():CThread("CDoorLockManager",TP_EXT)
{
	m_lock_count 		= 0;
	m_alarm_msg_count	= 0;
	m_down_count		= 0;
	ntpInfoUpdate		= VD_FALSE;
	m_lock_awake_flg	= VD_FALSE;
	m_lock_gettoken_flg	= VD_FALSE;
}
CDoorLockManager::~CDoorLockManager()
{
    
}

#ifdef X1000_LOCK
void CDoorLockManager::ProcessLockMsgRecv(lockMsgInfo *pRcvMsg)
{ 
    VD_BOOL iRet    = VD_TRUE;     
    static lockMsgInfo lastLockInfo; //用于过滤完全一样的重复信息  
    RT_LockMsgInfo lockinfo ;
    lockMsgInfo *pLock = NULL;
    
    CGuard m_Guard(m_lock_mutex);

    pLock = pRcvMsg;
    if(pLock == NULL)   return ;
    
    memset(&lockinfo,0,sizeof(lockinfo));
    iRet = VD_TRUE;
    if(pLock->Cmd == LOCK_OPENLOCK){
        lockinfo.MsgType = RT_LOCK_OPEN;
        
        memcpy(lockinfo.Open.Name,pLock->OpenInfo.Name,sizeof(lockinfo.Open.Name));
    
        _printd("UnlockType:%d",pLock->OpenInfo.UnlockType);
        _printd("UserNumber:%d",pLock->OpenInfo.UserNumber);
    
        lockinfo.Open.UserNumber = pLock->OpenInfo.UserNumber;
    
        if(0 >= pLock->OpenInfo.UnlockType &&
            0 >= pLock->OpenInfo.UserNumber){
            _printd("error data");
            return ;
        }
    
        
        CONFIG_OPENTYPE OpenType;
        
        if( LOCKOPEN_PSW == pLock->OpenInfo.UnlockType){
            lockinfo.Open.UnlockType = LOCK_PSW;
            OpenType = _OPEN_PSW;
        }
        else if( LOCKOPEN_FINGER == pLock->OpenInfo.UnlockType){
            lockinfo.Open.UnlockType = LOCK_FINGER;
            OpenType = _OPEN_FINGER;
        }
        else if(LOCKOPEN_TEMPPSW == pLock->OpenInfo.UnlockType){
            lockinfo.Open.UnlockType = LOCK_TEMPPSW;
            OpenType = _OPEN_TEMPPSW;
        }
        else{
            lockinfo.Open.UnlockType = LOCK_CARD;
            OpenType = _OPEN_CARD;
        }
        
        if(pLock->OpenInfo.UnlockTime.year < 2019){
            SYSTEM_TIME sTime = {0};
            
            SystemGetCurrentTime(&sTime);
            /*
            lockinfo.Open.UnlockTime.m_year  = sTime.year;
            lockinfo.Open.UnlockTime.m_month = sTime.month;
            lockinfo.Open.UnlockTime.m_day   = sTime.day;
            lockinfo.Open.UnlockTime.m_hour  = sTime.hour;
            lockinfo.Open.UnlockTime.m_minute= sTime.minute;
            lockinfo.Open.UnlockTime.m_second= sTime.second;
            */
            lockinfo.TimeStamp  = time(NULL) - 2;
            
            _printd("get time %4d-%02d-%02d %02d:%02d:%02d\n\n", sTime.year,
                   sTime.month, sTime.day, sTime.hour, sTime.minute, sTime.second);
        }
        else{
            CConfigNetNTP m_cCfgNtp;
            m_cCfgNtp.update();
            CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();
    
            int fTimeZone = ((float)cfgnetntp.TimeZone)/10;
    
            struct tm mtime = {0};
            
            mtime.tm_year   = pLock->OpenInfo.UnlockTime.year - 1900;
            mtime.tm_mon    = pLock->OpenInfo.UnlockTime.month - 1;
            mtime.tm_mday   = pLock->OpenInfo.UnlockTime.day;
            mtime.tm_hour   = pLock->OpenInfo.UnlockTime.hour;
            mtime.tm_min    = pLock->OpenInfo.UnlockTime.minute;
            mtime.tm_sec    = pLock->OpenInfo.UnlockTime.second;
            
            lockinfo.TimeStamp = mktime(&mtime) - fTimeZone*3600;
    
            if(VD_TRUE == cfgnetntp.DaylightTime.Enable &&
                lockinfo.TimeStamp >= cfgnetntp.DaylightTime.StartTime &&
                lockinfo.TimeStamp <= cfgnetntp.DaylightTime.EndTime){
                lockinfo.TimeStamp -= 3600;
            }
    
            _printd("open lock:%04d-%02d-%02d %02d:%02d:%02d(%ld)",
                mtime.tm_year+1900,mtime.tm_mon+1,mtime.tm_mday,
                mtime.tm_hour,mtime.tm_min,mtime.tm_sec,
                lockinfo.TimeStamp);
            
        }
    
        if(lastLockInfo.OpenInfo.UserNumber         ==  pLock->OpenInfo.UserNumber      &&
            lastLockInfo.OpenInfo.UnlockType        ==  pLock->OpenInfo.UnlockType      &&
            lastLockInfo.OpenInfo.UnlockTime.year   ==  pLock->OpenInfo.UnlockTime.year &&
            lastLockInfo.OpenInfo.UnlockTime.month  ==  pLock->OpenInfo.UnlockTime.month &&
            lastLockInfo.OpenInfo.UnlockTime.day    ==  pLock->OpenInfo.UnlockTime.day  &&
            lastLockInfo.OpenInfo.UnlockTime.hour   ==  pLock->OpenInfo.UnlockTime.hour &&
            lastLockInfo.OpenInfo.UnlockTime.minute ==  pLock->OpenInfo.UnlockTime.minute &&
            lastLockInfo.OpenInfo.UnlockTime.second ==  pLock->OpenInfo.UnlockTime.second ){
            _printd("The message is repeated,so dump it");
            return ;
        }   
        memcpy(&lastLockInfo,pLock,sizeof(lockMsgInfo));
    
        PlayLeaveMessage(lockinfo.Open.UserNumber,OpenType);
    }
    else if(pLock->Cmd == LOCK_ACTIONINFO){
        CONFIG_OPENTYPE OpenType;
        
        _printd("Action:%d",pLock->ActionInfo.Action);
        _printd("UserNumber:%d",pLock->ActionInfo.UserNumber);
        _printd("UserType:%d",pLock->ActionInfo.UserType);
        _printd("UserTypeNumber:%d",pLock->ActionInfo.UserTypeNumber);
        _printd("UserPriType:%d",pLock->ActionInfo.UserPriType);
        
        switch(pLock->ActionInfo.Action){
            case LOCKMSG_ALARM      ://防撬报警
                lockinfo.MsgType    = RT_LOCK_PICKALARM;
                break;
            case LOCKMSG_ADDUSER    ://添加密钥，数据内包含用户ＩＤ及密钥ＩＤ                
                lockinfo.MsgType    = RT_LOCK_ADDUSER;
                
                if( LOCKOPEN_PSW == pLock->ActionInfo.UserType){
                    lockinfo.User.UnlockType = LOCK_PSW;
                }
                else if( LOCKOPEN_FINGER == pLock->ActionInfo.UserType){
                    lockinfo.User.UnlockType = LOCK_FINGER;
                }
                else{
                    lockinfo.User.UnlockType = LOCK_CARD;
                }                   
                lockinfo.User.UserNumber= pLock->ActionInfo.UserNumber;
                lockinfo.User.UnlockTypeNumber = pLock->ActionInfo.UserTypeNumber;
                lockinfo.User.UserPriType = pLock->ActionInfo.UserPriType;
                break;
            case LOCKMSG_DELUSER    ://删除密钥，数据内包含用户ＩＤ及密钥ＩＤ
                lockinfo.MsgType        = RT_LOCK_DELUSER;
                
                if( LOCKOPEN_PSW == pLock->ActionInfo.UserType){
                    lockinfo.User.UnlockType = LOCK_PSW;
                }
                else if( LOCKOPEN_FINGER == pLock->ActionInfo.UserType){
                    lockinfo.User.UnlockType = LOCK_FINGER;
                }
                else{
                    lockinfo.User.UnlockType = LOCK_CARD;
                }                   
                lockinfo.User.UserNumber= pLock->ActionInfo.UserNumber;
                lockinfo.User.UnlockTypeNumber = pLock->ActionInfo.UserTypeNumber;
                lockinfo.User.UserPriType = pLock->ActionInfo.UserPriType;                       
                break;
            case LOCKMSG_DOORBELL   ://门铃
                lockinfo.MsgType    = RT_LOCK_DOORBELL;
                break;
            case LOCKMSG_SYSTEMLOCK ://系统被锁定
                lockinfo.MsgType    = RT_LOCK_SYSTEMLOCK;
                break;
            case LOCKMSG_LOCKINIT://门锁初始化
                lockinfo.MsgType    = RT_LOCK_RESET;
                DelAllUser();
                break;
            case LOCKMSG_AWAKE:
                iRet = VD_FALSE;
                m_lock_awake_flg = VD_TRUE;
                _printd("lock awake");
                break;
            case LOCKMSG_CLOSELOCK:
                iRet = VD_FALSE;
                m_lock_awake_flg = VD_FALSE;
                _printd("lock close");                      
                break;
            default:
                iRet = VD_FALSE;
                _printd("unknow action");
                break;
            }
    
            lockinfo.TimeStamp = time(NULL) - 2;
    }
    else if(pLock->Cmd == LOCK_GETSECRETKEY){
        _printd("LOCK_GETSECRETKEY");
        m_lock_gettoken_flg = VD_TRUE;
        return ;
    }
    if( VD_TRUE != iRet) return ;
    
    m_alarm_msg_count ++;
    m_lock_awake_flg = VD_FALSE;
    
    CPacket *pPacket = g_PacketManager.GetPacket(sizeof(RT_LockMsgInfo)+1);
    if(pPacket == NULL){
        _printd("Ex -- GetPacket failed.\n");
        return ;
    }
    
    memcpy((RT_LockMsgInfo*)(pPacket->GetBuffer()),&lockinfo,sizeof(RT_LockMsgInfo));
    
    pPacket->AddRef();
    m_lock_count ++;
    m_lockList.push_back(pPacket);
}

extern "C" void lock_msg_recv_callback(lockMsgInfo *pRcvMsg)
{
    g_DoorLockManager.ProcessLockMsgRecv(pRcvMsg);
}

int CDoorLockManager::GetUserInfoList(RT_LockUserList *pUserInfo)
{
    if (pUserInfo == NULL) {
        _printd("pUserInfo is NULL\n");
        return -1;
    }
    
    int iRet = 0;
    int i = 0;
    int j = 0;
    int k = 0;
    LockApiUserInfo stLockUserInfo;
    RT_UnlockType unLockType;
    
    memset(&stLockUserInfo, 0, sizeof(stLockUserInfo));
    iRet = GetLockUserInfo(&stLockUserInfo);
    if (iRet != 0) {
        _printd("GetLockUserInfo failed\n");
        return -1;
    }

    pUserInfo->UserNum = stLockUserInfo.userCnt;
    for (i = 0; i < stLockUserInfo.userCnt; i++)
    {
        pUserInfo->UserInfo[i].UserNumber = stLockUserInfo.userAttr[i].userId;

        for (j = 0; j < stLockUserInfo.userAttr[i].unlockTypeCnt; j++)
        {
            if (LOCKOPEN_PSW == stLockUserInfo.userAttr[i].unlockTypeAttr[j].type) {
                pUserInfo->UserInfo[i].UnlockType[LOCK_PSW] = RT_ENABLE;
                unLockType = LOCK_PSW;
            } else if (LOCKOPEN_FINGER == stLockUserInfo.userAttr[i].unlockTypeAttr[j].type) {
                pUserInfo->UserInfo[i].UnlockType[LOCK_FINGER] = RT_ENABLE;
                unLockType = LOCK_FINGER;
            } else if (LOCKOPEN_CARD == stLockUserInfo.userAttr[i].unlockTypeAttr[j].type) {
                pUserInfo->UserInfo[i].UnlockType[LOCK_CARD] = RT_ENABLE;
                unLockType = LOCK_CARD;
            }

			pUserInfo->UserInfo[i].UnLockTypeIdCnt[unLockType] = stLockUserInfo.userAttr[i].unlockTypeAttr[j].idCnt;
            for (k = 0; k < stLockUserInfo.userAttr[i].unlockTypeAttr[j].idCnt; k++)
            {
                 pUserInfo->UserInfo[i].UnLockTypeId[unLockType][k] = 
                                        stLockUserInfo.userAttr[i].unlockTypeAttr[j].idList[k];
            }           
        }
     
        pUserInfo->UserInfo[i].UserPriType = stLockUserInfo.userAttr[i].userPriType;
    }
    
    return 0;
}
#endif /* X1000_LOCK */

VD_BOOL CDoorLockManager::Start()
{
	int iRet = 0;

#ifdef X1000_LOCK
    LockInit();
    LockMsgRegisterRecv(lock_msg_recv_callback);
#endif
	m_LockUserInfo.update();
	
	m_LockLeaveMsg.attach(this, (TCONFIG_PROC)&CDoorLockManager::onConfigLockLeaveMsg);
	m_LockLeaveMsg.update();
	onConfigLockLeaveMsg(m_LockLeaveMsg,iRet);

	m_cCfgNtp.attach(this, (TCONFIG_PROC)&CDoorLockManager::OnConfigNtp);
	m_cCfgNtp.update();
	OnConfigNtp(m_cCfgNtp,iRet);

	m_LockWarnVoice.attach(this, (TCONFIG_PROC)&CDoorLockManager::onConfigLockWarnVoice);
	m_LockWarnVoice.update();
	onConfigLockWarnVoice(m_LockWarnVoice,iRet);

	m_LockMsgCache.update();

	PopLockMsgCache();
	
	if(!m_bLoop)	CreateThread();  

	return VD_TRUE;
}

VD_BOOL CDoorLockManager::Stop()
{
	_printd("Stop<<<<<<<");
	m_lockTimer.Stop();
	
	if(m_bLoop)		DestroyThread();

#ifdef X1000_LOCK
    LockDeinit();
    LockMsgUnRegisterRecv();
#endif
	return VD_TRUE;
}

VD_BOOL MoveFile(char *oldPath,char *newPath)
{
	if(NULL == oldPath || NULL == newPath){
		_printd("param null");
		return VD_FALSE;
	}

	ifstream oldFile(oldPath);	
	ofstream newFile(newPath);

	VD_BOOL ret = VD_TRUE;
	char buf[1024] = {0};
	
	if(!oldFile.is_open()){
		_printd("==>>open %s fail!\n",oldPath);
		return VD_FALSE;
	}

	if(!newFile.is_open()){
		_printd("==>>open %s fail!\n",newPath);
		return VD_FALSE;
	}	
	while(0 == oldFile.eof()){
		oldFile.read(buf,sizeof(buf));

		if(oldFile.gcount() < sizeof(buf)){
			_printd("read ,len:%d",oldFile.gcount());
		}
		#if 0
		if(0 >= newFile.write(buf,oldFile.gcount())){
			_printd("write error");
			ret = VD_FALSE;
			break;
		}
		#else
		newFile.write(buf,oldFile.gcount());
		#endif
	}

	oldFile.close();
	newFile.close();

	if(VD_TRUE == ret){
		remove(oldPath);
	}
	else{
		remove(newPath);
	}
	
	return ret;
}

void CDoorLockManager::onConfigLockWarnVoice(CConfigLockWarnVoice& cCfg, int& ret)
{
	char filePath[128] = {0};

	CONDIG_WARNVOICE &configOld = m_LockWarnVoice.getConfig(0);
	CONDIG_WARNVOICE &configNew = cCfg.getConfig(0);

	snprintf(filePath,sizeof(filePath),"%s/%s.mp3",LEAVEMESSAGE_DIR, LOCK_WARN_VOICE_NAME);

	if(VD_FALSE == configNew.bEnable){
	    if (file_exists((char*)filePath)){ 
			_printd("remove %s",filePath);
			remove(filePath);
	    }
	}
	else
	{
		if(0 == configNew.cPlayUrl[0]	||
		   0 != strncmp(configOld.cPlayUrl,configNew.cPlayUrl,strlen(configOld.cPlayUrl))){
			if (file_exists((char*)filePath)){ 
				_printd("remove %s",filePath);
				remove(filePath);
		    }
		}	
		if (0 == file_exists((char*)filePath)){
			CGuard m_Guard(m_down_mutex);

			int	len = 0;
			AudioDownInfo downInfo = {0};
			
			downInfo.id = LOCK_WARN_VOICE_ID;

			len = (sizeof(downInfo.downUrl) > sizeof(configNew.cPlayUrl)?
				sizeof(configNew.cPlayUrl):sizeof(downInfo.downUrl));	
			memcpy(downInfo.downUrl,configNew.cPlayUrl,len);
			
			m_downList.push_back(downInfo);
			m_down_count ++;
		}
	}

	configOld = configNew;
}

void CDoorLockManager::onConfigLockLeaveMsg(CConfigLockLeaveMsg& cCfg, int& ret)
{
	char filePath[128] = {0};
	
	for(int i = 0 ; i < LEAVEMSG_MAX; i ++){
		CONFIG_LEAVEMSG &configOld = m_LockLeaveMsg.getConfig(i);
		CONFIG_LEAVEMSG &configNew = cCfg.getConfig(i);
		snprintf(filePath,sizeof(filePath),"%s/%d.mp3",LEAVEMESSAGE_DIR,i);
		
		if(VD_FALSE == configNew.bEnable){
		    if (file_exists((char*)filePath)){ 
				_printd("remove %s",filePath);
				remove(filePath);
		    }
		}
		else{
			//留言语音文件变了,则删除旧文件
			/*
			_printd("configOld.cPlayUrl:%s,configNew.cPlayUrl:%s...%d",
			configOld.cPlayUrl,configNew.cPlayUrl,
			strncmp(configOld.cPlayUrl,configNew.cPlayUrl,strlen(configOld.cPlayUrl)));
			*/
			if(0 == configNew.cPlayUrl[0]	||
			   0 != strncmp(configOld.cPlayUrl,configNew.cPlayUrl,strlen(configOld.cPlayUrl))){
				if (file_exists((char*)filePath)){ 
					_printd("remove %s",filePath);
					remove(filePath);
			    }
			}		
			
			if (0 == file_exists((char*)filePath)){
				CGuard m_Guard(m_down_mutex);

				int	len = 0;
				AudioDownInfo downInfo = {0};
				
				downInfo.id = i;

				len = (sizeof(downInfo.downUrl) > sizeof(configNew.cPlayUrl)?
					sizeof(configNew.cPlayUrl):sizeof(downInfo.downUrl));	
				memcpy(downInfo.downUrl,configNew.cPlayUrl,len);
				
				m_downList.push_back(downInfo);
				m_down_count ++;
			}
		}

		
		configOld = configNew;
	}
}
VD_BOOL CDoorLockManager::AddUserOpenType(int UserNum,CONFIG_OPENTYPE OpenType)
{
	if(UserNum > USERNUM_MAX || UserNum < 1){
		_printd("ERROR,The UserNum(%d) is unnormal",UserNum);
		return VD_FALSE;
	}

	int ret = 0;
	
	m_LockUserInfo.update(UserNum - 1);
	CONFIG_USERINFO& cfgLock = m_LockUserInfo.getConfig(UserNum - 1);

	cfgLock.bEnable 				= VD_TRUE;
	cfgLock.bSupportType[OpenType]	= VD_TRUE;

	ret = m_LockUserInfo.commit(NULL,UserNum - 1);

	_printd("ret:%d",ret);

	return VD_TRUE;
}	
VD_BOOL CDoorLockManager::DelUserOpenType(int UserNum,CONFIG_OPENTYPE OpenType)
{
	if(UserNum > USERNUM_MAX || UserNum < 1){
		_printd("ERROR,The UserNum(%d) is unnormal",UserNum);
		return VD_FALSE;
	}

	int ret = 0;
	
	m_LockUserInfo.update(UserNum - 1);
	CONFIG_USERINFO& cfgLock = m_LockUserInfo.getConfig(UserNum - 1);

	cfgLock.bSupportType[OpenType]	= VD_FALSE;

	if(VD_TRUE == cfgLock.bEnable){
		cfgLock.bEnable = VD_FALSE;
		for(int i = 0 ;i < _OPEN_OTHER; i++){
			if(VD_TRUE == cfgLock.bSupportType[i]){
				cfgLock.bEnable = VD_TRUE;
				break;
			}
		}
	}

	ret = m_LockUserInfo.commit(NULL,UserNum - 1);

	_printd("ret:%d",ret);

	return VD_TRUE;
}	
VD_BOOL CDoorLockManager::DelAllUser()
{
	//删除门锁用户配置
	m_LockUserInfo.recall();
	m_LockUserInfo.commit();

	//删除语音留言配置
	m_LockLeaveMsg.recall();
	m_LockLeaveMsg.commit();
	
	return VD_TRUE;
}

VD_BOOL CDoorLockManager::SyncLockTime()
{
#ifdef DOOR_LOCK	
	SYSTEM_TIME sTime = {0};
	
	SystemGetCurrentTime(&sTime);

	if( 0 == SetLockTime(sTime)){
		_printd("SyncLockTime success");
		return VD_TRUE;
	}

	_printd("SyncLockTime fail");
	return VD_FALSE;
#else
	_printd("Don't support SyncLockTime");
	return VD_FALSE;
#endif
}
void CDoorLockManager::OnConfigNtp(CConfigNetNTP& cfg_in_tmp, int &ret)
{	
	_printd("The info of ntp have benn updated");
	ntpInfoUpdate = VD_TRUE;
}

int CDoorLockManager::DownLoadFile(AudioDownInfo stDownInfo)
{
	int ret = 0;
	CURL *curl	= NULL;  
    CURLcode res = CURLE_RECV_ERROR;  
	
	char filePath[128] 		= {0};
	char tempFilePath[128] 	= {0};

	char Id = 0;
	char DownUrl[1024] = "";

#ifdef X1000_LOCK
	Id = stDownInfo.id;
	snprintf(DownUrl, sizeof(DownUrl), "%s", stDownInfo.stAvInfo.playUrl);
#else
	Id = stDownInfo.id;
	snprintf(DownUrl, sizeof(DownUrl), "%s", stDownInfo.downUrl);
#endif
	
	if(Id < 0){
		_printd("Id:%d, is error",Id);
		return -1;
	}

	 curl = curl_easy_init();  
	 
	_printd("Id:%d,DownUrl:%s",Id,DownUrl);
	if (curl) 
	{
		curl_easy_setopt(curl, CURLOPT_URL, DownUrl); 
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);


#ifdef X1000_LOCK
		if (stDownInfo.stAvInfo.playType == 0) { //audio
			mkdir_mul(LEAVEMESSAGE_TEMP_DIR);
			snprintf(tempFilePath,sizeof(tempFilePath),"%s/%d.mp3",LEAVEMESSAGE_TEMP_DIR,Id);
		    if (file_exists((char*)tempFilePath)){ 
	           remove(tempFilePath);
		    }		
		} else if (stDownInfo.stAvInfo.playType == 1) { //video
			mkdir_mul(LEAVEMESSAGE_TEMP_DIR);
			snprintf(tempFilePath,sizeof(tempFilePath),"%s/%d.mp4",LEAVEMESSAGE_TEMP_DIR,Id);
		    if (file_exists((char*)tempFilePath)){ 
	           remove(tempFilePath);
		    }			
		}
#else
		if (Id == LOCK_WARN_VOICE_ID) {
			//删除本文件
			mkdir_mul(LEAVEMESSAGE_DIR);
			snprintf(filePath,sizeof(filePath),"%s/%s.mp3",LEAVEMESSAGE_DIR,LOCK_WARN_VOICE_NAME);
		    if (file_exists((char*)filePath)){ 
	           remove(filePath);
		    }

			//删除临时文件
			mkdir_mul(LEAVEMESSAGE_TEMP_DIR);
			snprintf(tempFilePath,sizeof(tempFilePath),"%s/%s.mp3",LEAVEMESSAGE_TEMP_DIR,LOCK_WARN_VOICE_NAME);
		    if (file_exists((char*)tempFilePath)){ 
	           remove(tempFilePath);
		    }

		} else {
			//删除本文件
			mkdir_mul(LEAVEMESSAGE_DIR);
			snprintf(filePath,sizeof(filePath),"%s/%d.mp3",LEAVEMESSAGE_DIR,Id);
		    if (file_exists((char*)filePath)){ 
	           remove(filePath);
		    }

			//删除临时文件
			mkdir_mul(LEAVEMESSAGE_TEMP_DIR);
			snprintf(tempFilePath,sizeof(tempFilePath),"%s/%d.mp3",LEAVEMESSAGE_TEMP_DIR,Id);
		    if (file_exists((char*)tempFilePath)){ 
	           remove(tempFilePath);
		    }
		}
#endif		
		
		//指定回调函数  
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefile_callback);  
		//先下载到临时文件夹
	    curl_easy_setopt(curl, CURLOPT_WRITEDATA, tempFilePath);
        res = curl_easy_perform(curl);  
		curl_easy_cleanup(curl);  

		if(CURLE_OK == res){
#ifdef X1000_LOCK
			int mid = 0;
			ret = SetLockAvInfo(stDownInfo.stAvInfo, tempFilePath, &mid);
			if (ret != 0) {
				_printd("SetLockAvInfo failed\n");
			}

#else
			if(MoveFile(tempFilePath,filePath) < 0){
				_printd("rename error,%s to %s",tempFilePath,filePath);
				perror("MoveFile error::");
			}
#endif			
		}
	}
	else{
		_printd("curl_easy_init error(%d)",res);
	}
	return res;
}
//播放对应用户的留言留言
VD_BOOL CDoorLockManager::PlayLeaveMessage(int UserNum,CONFIG_OPENTYPE OpenType)
{
	m_LockLeaveMsg.update();

	for(int i = 0; i < LEAVEMSG_MAX;i ++){
		CONFIG_LEAVEMSG& cfgInfo = m_LockLeaveMsg.getConfig(i);
		if(VD_TRUE == cfgInfo.bEnable){
			/*
			for(int j = 0 ; j < _OPEN_OTHER ; j ++){
				_printd("UserNum:%ld,OpenType(%d):%d",cfgInfo.iUserNum,j,cfgInfo.bSupportType[j]);
			}
			*/
			if(UserNum == cfgInfo.iUserNum && VD_TRUE == cfgInfo.bSupportType[OpenType]){
				int play = 0;
				int wday = 0;
				time_t	t= 0;
				struct tm *ntime = NULL;
	
				t 		= g_NtpClient.GetCurrentTimeStamp();
				ntime 	= gmtime(&t);

				#if 0
				if(0 >= ntime->tm_wday){
					wday = 6;
				}
				else{
					wday = ntime->tm_wday - 1;
				}
				#else
				if(ntime->tm_wday > 6 || ntime->tm_wday < 0){
					wday = 0;
				}
				#endif
				_printd("t:%ld,wday:%d(%d)",t,wday,ntime->tm_wday);
				
				if(VD_FALSE == cfgInfo.daySwitch[wday]){
					_printd("this day is unable");
					return VD_FALSE;
				}
				
				if(cfgInfo.iReplay >= 100){ //暂定，认为大于100次可无限播放
					play = 1;
				}
				else{ 
					if(cfgInfo.iHavePlay < cfgInfo.iReplay){
						play = 1;
						cfgInfo.iHavePlay++;
					}
					else{
						_printd("The number to play leavemessage has been reached to upper(iHavePlay:%d,iReplay:%d)",
							cfgInfo.iHavePlay,cfgInfo.iReplay);		
					}
				}
				
				if(play){
					_printd("Play music %s",cfgInfo.cPlayUrl);

					char filePath[128] = {0};
					snprintf(filePath,sizeof(filePath),"%s/%d.mp3",LEAVEMESSAGE_DIR,i);
				    if (file_exists((char*)filePath)){ 
			        	AudioPlayAudioStart(filePath, (enum audio_encode_type)16, 0);
				    }
					else{
						_printd("play error ,%s is't exist",filePath);
					}
					
				}

				m_LockLeaveMsg.commit(NULL,i);
				return VD_TRUE;
			}
		}
	}
	
	_printd("Play error,there is not leavemessage for opentype(%d) of the user(%d)",
					OpenType,UserNum);
	return VD_FALSE;
}

VD_BOOL CDoorLockManager::PlayWarnVoice(void)
{
	char filePath[128] = {0};
	snprintf(filePath,sizeof(filePath),"%s/%s.mp3",LEAVEMESSAGE_DIR,LOCK_WARN_VOICE_NAME);
    if (file_exists((char*)filePath)){ 
    	AudioPlayAudioStart(filePath, (enum audio_encode_type)16, 0);
    }
	else{
		_printd("play error ,%s is't exist",filePath);
		return VD_FALSE;
	}

	return VD_TRUE;
}
//保存锁端消息到配置
int CDoorLockManager::PushLockMsgCache(RT_LockMsgInfo LockInfo)
{
	int i = 0;	

	m_LockMsgCache.update();
	
	for(i = 0; i < MSGCACHE_MAX;i ++){
		CONFIG_LOCKMSGCACHE& cfgInfo = m_LockMsgCache.getConfig(i);
	#if 0
		if(VD_FALSE == cfgInfo.bEnable){
			cfgInfo.bEnable = VD_TRUE;
			switch(LockInfo.MsgType){
				case RT_LOCK_OPEN:		//开锁事件
					cfgInfo.sMsgType = _LOCKMSG_OPEN;
					break;
				case RT_LOCK_PICKALARM:	//撬锁事件
					cfgInfo.sMsgType = _LOCKMSG_ALARM;
					break;
				case RT_LOCK_ADDUSER:	//添加用户事件
					cfgInfo.sMsgType = _LOCKMSG_ADDUSER;
					break;
				case RT_LOCK_DELUSER:	//删除用户事件
					cfgInfo.sMsgType = _LOCKMSG_DELUSER;
					break;
				case RT_LOCK_DOORBELL:	//门铃事件
					cfgInfo.sMsgType = _LOCKMSG_DOORBELL;
					break;
				case RT_LOCK_SYSTEMLOCK://系统锁定事件(例如:连续输错5次密码会自动锁定)
					cfgInfo.sMsgType = _LOCKMSG_SYSTEMLOCK;
					break;
				case RT_LOCK_RESET:		//恢复出厂设置报警
					cfgInfo.sMsgType = _LOCKMSG_LOCKINIT;
					break;					
				case RT_LOCK_LOWBAT:	//低电报警 不缓存
				default:
					_printd("dump this msg(:%d) ...",LockInfo.MsgType);
					cfgInfo.bEnable  = VD_FALSE; 
					break;

			}
	
			if(VD_TRUE == cfgInfo.bEnable){

				_printd("save msg(cfgInfo.sMsgType:%d,i:%d)",cfgInfo.sMsgType,i);
				
				if(_LOCKMSG_OPEN == cfgInfo.sMsgType){
					cfgInfo.iUserNum = LockInfo.Open.UserNumber;
					switch(LockInfo.Open.UnlockType){
						case LOCK_KEY:  	//钥匙开锁
							cfgInfo.sOpenType = _OPEN_KEY;
							break;
						case LOCK_PSW:  	//密码开锁
							cfgInfo.sOpenType = _OPEN_PSW;
							break;
						case LOCK_FINGER:  	//指纹开锁
							cfgInfo.sOpenType = _OPEN_FINGER;
							break;
						case LOCK_CARD:  	//门卡开锁
							cfgInfo.sOpenType = _OPEN_CARD;
							break;
						case LOCK_TEMPPSW:  //临时密码开锁		
							cfgInfo.sOpenType = _OPEN_TEMPPSW;
							break;
						default:
							_printd("LockInfo.Open.UnlockType:%d ... error",LockInfo.Open.UnlockType);
							cfgInfo.bEnable = VD_FALSE;
							break;
					}
				}
				else if(RT_LOCK_ADDUSER == cfgInfo.sMsgType ||
						RT_LOCK_DELUSER == cfgInfo.sMsgType	){
						cfgInfo.iUserNum = LockInfo.User.UserNumber;
						switch(LockInfo.User.UnlockType){
							case LOCK_KEY:  	//钥匙开锁
								cfgInfo.sOpenType = _OPEN_KEY;
								break;
							case LOCK_PSW:  	//密码开锁
								cfgInfo.sOpenType = _OPEN_PSW;
								break;
							case LOCK_FINGER:  	//指纹开锁
								cfgInfo.sOpenType = _OPEN_FINGER;
								break;
							case LOCK_CARD:  	//门卡开锁
								cfgInfo.sOpenType = _OPEN_CARD;
								break;
							case LOCK_TEMPPSW:  //临时密码开锁		
								cfgInfo.sOpenType = _OPEN_TEMPPSW;
								break;
							default:
								_printd("LockInfo.User.UnlockType:%d ... error",LockInfo.User.UnlockType);
								cfgInfo.bEnable = VD_FALSE;
								break;		
							}
	
				}

				cfgInfo.uTimeStamp = LockInfo.TimeStamp; 			
			}
			
			break;
		}
		#endif
		if(i >= MSGCACHE_MAX){
			_printd("lockmsg cache have been full");
		}
	}	

	m_LockMsgCache.commit();
	return VD_TRUE;
}
//发送配置中保存下来的锁端消息
int CDoorLockManager::PopLockMsgCache()
{
#if 0
	CGuard m_Guard(m_lock_mutex);

	int i = 0;	

	m_LockMsgCache.update();
	
	for(i = 0; i < MSGCACHE_MAX;i ++){
		CONFIG_LOCKMSGCACHE& cfgInfo = m_LockMsgCache.getConfig(i);
	
		if(VD_FALSE == cfgInfo.bEnable){
			break;
		}

		_printd("pop msg(cfgInfo.sMsgType:%d,i:%d)",cfgInfo.sMsgType,i);
		cfgInfo.bEnable = VD_FALSE;
		
		int iRet = 1;
		RT_LockMsgInfo LockInfo;
		switch(cfgInfo.sMsgType){
			case _LOCKMSG_OPEN://开锁事件
				LockInfo.MsgType = RT_LOCK_OPEN;
				break;
			case _LOCKMSG_ALARM://撬锁事件
				LockInfo.MsgType = RT_LOCK_PICKALARM;
				break;	
			case _LOCKMSG_ADDUSER://添加用户事件
				LockInfo.MsgType = RT_LOCK_ADDUSER;
				break;
			case _LOCKMSG_DELUSER://删除用户事件
				LockInfo.MsgType = RT_LOCK_DELUSER;
				break;
			case _LOCKMSG_DOORBELL://门铃事件
				LockInfo.MsgType = RT_LOCK_DOORBELL;
				break;
			case _LOCKMSG_SYSTEMLOCK://系统锁定事件(例如:连续输错5次密码会自动锁定)
				LockInfo.MsgType = RT_LOCK_SYSTEMLOCK;
				break;
			case _LOCKMSG_LOCKINIT://恢复出厂设置报警
				LockInfo.MsgType = RT_LOCK_RESET;
				break;	
			default:
				_printd("cfgInfo.sMsgType:%d...error",cfgInfo.sMsgType);
				iRet = 0;
				break;
		}
		
		if(0 == iRet){
			continue;
		}
		if(_LOCKMSG_OPEN == cfgInfo.sMsgType){
			LockInfo.Open.UserNumber = cfgInfo.iUserNum;
			switch(cfgInfo.sOpenType){
				case _OPEN_KEY:  	//钥匙开锁
					LockInfo.Open.UnlockType = LOCK_KEY;
					break;
				case _OPEN_PSW:  	//密码开锁
					LockInfo.Open.UnlockType = LOCK_PSW;
					break;
				case _OPEN_FINGER:  	//指纹开锁
					LockInfo.Open.UnlockType = LOCK_FINGER;
					break;
				case _OPEN_CARD:  	//门卡开锁
					LockInfo.Open.UnlockType = LOCK_CARD;
					break;
				case _OPEN_TEMPPSW:  //临时密码开锁		
					LockInfo.Open.UnlockType = LOCK_TEMPPSW;
					break;
				default:
					_printd("cfgInfo.sOpenType:%d ... error",cfgInfo.sOpenType);
					break;
			}
		}
		else if(RT_LOCK_ADDUSER == cfgInfo.sMsgType ||
				RT_LOCK_DELUSER == cfgInfo.sMsgType	){
				LockInfo.Open.UserNumber = cfgInfo.iUserNum;
				switch(cfgInfo.sOpenType){
					case _OPEN_KEY:  	//钥匙开锁
						LockInfo.User.UnlockType = LOCK_KEY;
						break;
					case _OPEN_PSW:  	//密码开锁
						LockInfo.User.UnlockType = LOCK_PSW;
						break;
					case _OPEN_FINGER:  //指纹开锁
						LockInfo.User.UnlockType = LOCK_FINGER;
						break;
					case _OPEN_CARD:  	//门卡开锁
						LockInfo.User.UnlockType = LOCK_CARD;
						break;
					case _OPEN_TEMPPSW:  //临时密码开锁		
						LockInfo.User.UnlockType = LOCK_TEMPPSW;
						break;
					default:
						_printd("cfgInfo.sOpenType:%d ... error",cfgInfo.sOpenType);
						break;
				}

		}

		LockInfo.TimeStamp = cfgInfo.uTimeStamp;
		
		cfgInfo.uTimeStamp = 0;

		CPacket *pPacket = g_PacketManager.GetPacket(sizeof(RT_LockMsgInfo)+1);
		if(pPacket == NULL){
			_printd("Ex -- GetPacket failed.\n");
			continue;
		}

		memcpy((RT_LockMsgInfo*)(pPacket->GetBuffer()),&LockInfo,sizeof(RT_LockMsgInfo));

		pPacket->AddRef();
		m_lock_count ++;
		m_lockList.push_back(pPacket);
			
	}

	m_LockMsgCache.commit();
#endif	
	return VD_TRUE;
}

int CDoorLockManager::QueryLockTimer(int arg)
{
	VD_BOOL iRet 	= VD_TRUE;
	lockAllMsg Info = {0};	

	static char inFlag = 0;
//用于过滤完全一样的重复信息
	static lockMsgInfo lastLockInfo;

	if(1 == inFlag){
		_printd("have been in\n");
		return 0;
	}
	inFlag = 1;	
#ifdef DOOR_LOCK
	if( 0 == GetLockMsgInfo(&Info) ||
		Info.MsgCnt > 0){
#else
	if(0){
#endif
		#if 0
		CGuard m_Guard(m_lock_mutex);

		RT_LockMsgInfo lockinfo ;
		lockMsgInfo *pLock = NULL;
		
		for(int i = 0;i < Info.MsgCnt ; i ++){

			pLock = &(Info.Msg[i]);
			if(pLock == NULL)	continue;
			
			memset(&lockinfo,0,sizeof(lockinfo));
			iRet = VD_TRUE;
			if(pLock->Cmd == LOCK_OPENLOCK){
				lockinfo.MsgType = RT_LOCK_OPEN;
				
				memcpy(lockinfo.Open.Name,pLock->OpenInfo.Name,sizeof(lockinfo.Open.Name));

				_printd("UnlockType:%d",pLock->OpenInfo.UnlockType);
				_printd("UserNumber:%d",pLock->OpenInfo.UserNumber);

				lockinfo.Open.UserNumber = pLock->OpenInfo.UserNumber;

				if(0 >= pLock->OpenInfo.UnlockType &&
					0 >= pLock->OpenInfo.UserNumber){
					_printd("error data");
					continue;
				}

				
				CONFIG_OPENTYPE OpenType;
				
				if( LOCKOPEN_PSW == pLock->OpenInfo.UnlockType){
					lockinfo.Open.UnlockType = LOCK_PSW;
					OpenType = _OPEN_PSW;
				}
				else if( LOCKOPEN_FINGER == pLock->OpenInfo.UnlockType){
					lockinfo.Open.UnlockType = LOCK_FINGER;
					OpenType = _OPEN_FINGER;
				}
				else if(LOCKOPEN_TEMPPSW == pLock->OpenInfo.UnlockType){
					lockinfo.Open.UnlockType = LOCK_TEMPPSW;
					OpenType = _OPEN_TEMPPSW;
				}
				else{
					lockinfo.Open.UnlockType = LOCK_CARD;
					OpenType = _OPEN_CARD;
				}
				
				if(pLock->OpenInfo.UnlockTime.year < 2019){
					SYSTEM_TIME sTime = {0};
					
					SystemGetCurrentTime(&sTime);
					/*
					lockinfo.Open.UnlockTime.m_year  = sTime.year;
					lockinfo.Open.UnlockTime.m_month = sTime.month;
					lockinfo.Open.UnlockTime.m_day   = sTime.day;
					lockinfo.Open.UnlockTime.m_hour  = sTime.hour;
					lockinfo.Open.UnlockTime.m_minute= sTime.minute;
					lockinfo.Open.UnlockTime.m_second= sTime.second;
					*/
					lockinfo.TimeStamp	= time(NULL) - 2;
					
					_printd("get time %4d-%02d-%02d %02d:%02d:%02d\n\n", sTime.year,
						   sTime.month, sTime.day, sTime.hour, sTime.minute, sTime.second);
				}
				else{
					CConfigNetNTP m_cCfgNtp;
					m_cCfgNtp.update();
					CONFIG_NET_NTP& cfgnetntp = m_cCfgNtp.getConfig();

					int fTimeZone = ((float)cfgnetntp.TimeZone)/10;
			
					struct tm mtime = {0};
					
					mtime.tm_year  	= pLock->OpenInfo.UnlockTime.year - 1900;
					mtime.tm_mon	= pLock->OpenInfo.UnlockTime.month - 1;
					mtime.tm_mday   = pLock->OpenInfo.UnlockTime.day;
					mtime.tm_hour  	= pLock->OpenInfo.UnlockTime.hour;
					mtime.tm_min	= pLock->OpenInfo.UnlockTime.minute;
					mtime.tm_sec	= pLock->OpenInfo.UnlockTime.second;
					
					lockinfo.TimeStamp = mktime(&mtime) - fTimeZone*3600;
		
					if(VD_TRUE == cfgnetntp.DaylightTime.Enable &&
						lockinfo.TimeStamp >= cfgnetntp.DaylightTime.StartTime &&
						lockinfo.TimeStamp <= cfgnetntp.DaylightTime.EndTime){
						lockinfo.TimeStamp -= 3600;
					}

					_printd("open lock:%04d-%02d-%02d %02d:%02d:%02d(%ld)",
						mtime.tm_year+1900,mtime.tm_mon+1,mtime.tm_mday,
						mtime.tm_hour,mtime.tm_min,mtime.tm_sec,
						lockinfo.TimeStamp);
					
				}

				if(lastLockInfo.OpenInfo.UserNumber 		== 	pLock->OpenInfo.UserNumber 		&&
					lastLockInfo.OpenInfo.UnlockType 		== 	pLock->OpenInfo.UnlockType		&&
					lastLockInfo.OpenInfo.UnlockTime.year 	==  pLock->OpenInfo.UnlockTime.year &&
					lastLockInfo.OpenInfo.UnlockTime.month 	==  pLock->OpenInfo.UnlockTime.month &&
					lastLockInfo.OpenInfo.UnlockTime.day 	==  pLock->OpenInfo.UnlockTime.day 	&&
					lastLockInfo.OpenInfo.UnlockTime.hour 	==  pLock->OpenInfo.UnlockTime.hour &&
					lastLockInfo.OpenInfo.UnlockTime.minute ==  pLock->OpenInfo.UnlockTime.minute &&
					lastLockInfo.OpenInfo.UnlockTime.second ==  pLock->OpenInfo.UnlockTime.second ){
					_printd("The message is repeated,so dump it");
					continue;
				}	
				memcpy(&lastLockInfo,pLock,sizeof(lockMsgInfo));

				PlayLeaveMessage(lockinfo.Open.UserNumber,OpenType);
			}
			else if(pLock->Cmd == LOCK_ACTIONINFO){
				CONFIG_OPENTYPE OpenType;
				
				_printd("Action:%d",pLock->ActionInfo.Action);
				_printd("UserNumber:%d",pLock->ActionInfo.UserNumber);
				_printd("UserType:%d",pLock->ActionInfo.UserType);

				switch(pLock->ActionInfo.Action){
					case LOCKMSG_ALARM 		://防撬报警
						lockinfo.MsgType 	= RT_LOCK_PICKALARM;
						break;
					case LOCKMSG_ADDUSER	://添加密钥，数据内包含用户ＩＤ及密钥ＩＤ				
						lockinfo.MsgType 	= RT_LOCK_ADDUSER;
						
						if( LOCKOPEN_PSW == pLock->ActionInfo.UserType){
							lockinfo.User.UnlockType = LOCK_PSW;
							OpenType				 = _OPEN_PSW;
						}
						else if( LOCKOPEN_FINGER == pLock->ActionInfo.UserType){
							lockinfo.User.UnlockType = LOCK_FINGER;
							OpenType				 = _OPEN_FINGER;
						}
						else{
							lockinfo.User.UnlockType = LOCK_CARD;
							OpenType				 = _OPEN_CARD;
						}					
						lockinfo.User.UserNumber= pLock->ActionInfo.UserNumber;

						AddUserOpenType(lockinfo.User.UserNumber,OpenType);
						
						break;
					case LOCKMSG_DELUSER	://删除密钥，数据内包含用户ＩＤ及密钥ＩＤ
						lockinfo.MsgType 		= RT_LOCK_DELUSER;
						
						if( LOCKOPEN_PSW == pLock->ActionInfo.UserType){
							lockinfo.User.UnlockType = LOCK_PSW;
							OpenType				 = _OPEN_PSW;
						}
						else if( LOCKOPEN_FINGER == pLock->ActionInfo.UserType){
							lockinfo.User.UnlockType = LOCK_FINGER;
							OpenType				 = _OPEN_FINGER;
						}
						else{
							lockinfo.User.UnlockType = LOCK_CARD;
							OpenType				 = _OPEN_CARD;
						}					
						lockinfo.User.UserNumber= pLock->ActionInfo.UserNumber;

						DelUserOpenType(lockinfo.User.UserNumber,OpenType);
						break;
					case LOCKMSG_DOORBELL	://门铃
						lockinfo.MsgType 	= RT_LOCK_DOORBELL;
						break;
					case LOCKMSG_SYSTEMLOCK	://系统被锁定
						lockinfo.MsgType 	= RT_LOCK_SYSTEMLOCK;
						break;
					case LOCKMSG_LOCKINIT://门锁初始化
						lockinfo.MsgType 	= RT_LOCK_RESET;
						DelAllUser();
						break;
					case LOCKMSG_AWAKE:
						iRet = VD_FALSE;
						m_lock_awake_flg = VD_TRUE;
						_printd("lock awake");
						break;
					case LOCKMSG_CLOSELOCK:
						iRet = VD_FALSE;
						m_lock_awake_flg = VD_FALSE;
						_printd("lock close");						
						break;
					default:
						iRet = VD_FALSE;
						_printd("unknow action");
						break;
					}

					lockinfo.TimeStamp = time(NULL) - 2;
			}
			else if(pLock->Cmd == LOCK_GETSECRETKEY){
				_printd("LOCK_GETSECRETKEY");
				m_lock_gettoken_flg = VD_TRUE;
				continue;
			}
			else{
				_printd("unknowed Cmd:%d",pLock->Cmd);
				continue;
			}
			if( VD_TRUE != iRet) continue;

			m_alarm_msg_count ++;
			m_lock_awake_flg = VD_FALSE;
			
			CPacket *pPacket = g_PacketManager.GetPacket(sizeof(RT_LockMsgInfo)+1);
			if(pPacket == NULL){
				_printd("Ex -- GetPacket failed.\n");
				continue;
			}

			memcpy((RT_LockMsgInfo*)(pPacket->GetBuffer()),&lockinfo,sizeof(RT_LockMsgInfo));

			pPacket->AddRef();
			m_lock_count ++;
			m_lockList.push_back(pPacket);
		}
		#endif
	}
	
	inFlag = 0;
	
	return 0;
}
int CDoorLockManager::ProcessLockMsg()
{
	CGuard m_Guard(m_lock_mutex);

#ifdef LIVECAM
	//0 link PASS     -1 not link
	if( 0 != RealTimeConnectToServerStatus()){
		_printd("The sever of pass is error");
		return -1;
	}
#endif

	RT_LockMsgInfo *pLockMsg = NULL;

	LockMsg_List::iterator it;
	CPacket *pPacket = NULL;
		
	while(m_lock_count > 0){
		
		it = m_lockList.begin();
		pPacket = *it;

		m_lockList.pop_front();	
		m_lock_count --;
		
		if( pPacket == NULL ){
			_printd("pPacket NULL.\n");
			return -1;
		}		
#if 0
		pLockMsg = (RT_LockMsgInfo *)(pPacket->GetBuffer());

		if( pLockMsg == NULL ){
			_printd("pLockMsg NULL.\n");
			pPacket->Release();
			return -1;
		}	

		if( 0 == RealTimeUploadLockMsgInfo(*pLockMsg)){
			pPacket->Release();
		}
		else{
			_printd("RealTimeUploadLockMsgInfo error");
			m_lock_count ++;
			m_lockList.push_back(pPacket);

			return -1;
		}
#endif		
			
		
	}
	return 0;
}
int CDoorLockManager::ProcessRemainLockMsg()
{
	CGuard m_Guard(m_lock_mutex);

	RT_LockMsgInfo *pLockMsg = NULL;

	LockMsg_List::iterator it;
	CPacket *pPacket = NULL;
		
	while(m_lock_count > 0){
		
		it = m_lockList.begin();
		pPacket = *it;

		m_lockList.pop_front();	
		m_lock_count --;
		
		if( pPacket == NULL ){
			_printd("pPacket NULL.\n");
			continue;
		}		

		pLockMsg = (RT_LockMsgInfo *)(pPacket->GetBuffer());

		if( pLockMsg == NULL ){
			_printd("pLockMsg NULL.\n");
			continue;
		}	
#ifdef LIVECAM		
		//0 link PASS     -1 not link
		if( 0 == RealTimeConnectToServerStatus() &&
			0 == RealTimeUploadLockMsgInfo(*pLockMsg)){
#else
		if(0){
#endif
		}		
		else{
			PushLockMsgCache(*pLockMsg);
		}
			
		pPacket->Release();
	}
	return 0;
}

int CDoorLockManager::PushDownInfo(AudioDownInfo stDownInfo)
{
	_printd("type:%d url:%s\n", stDownInfo.stAvInfo.playType, stDownInfo.stAvInfo.playUrl);
		
	static char audioId = 0;
	static char videoId = 0;
	
	CGuard m_Guard(m_down_mutex);

	if (stDownInfo.stAvInfo.playType == 0) { //audio
		stDownInfo.id = audioId++;
	} else if (stDownInfo.stAvInfo.playType == 1) { //video
		stDownInfo.id = videoId++;
	}
	
	m_downList.push_back(stDownInfo);
	m_down_count ++;


	return 0;
}


int CDoorLockManager::ProcessDownInfo()
{
	readyToDown_List::iterator it;
	AudioDownInfo downInfo = {0};

	if(m_down_count > 0){
		m_down_mutex.Enter();
		it = m_downList.begin();

#ifdef X1000_LOCK
		downInfo.id = it->id;
		memcpy(&downInfo.stAvInfo, &it->stAvInfo, sizeof(downInfo.stAvInfo));		
#else
		downInfo.id = it->id;
		memcpy(downInfo.downUrl,it->downUrl,sizeof(downInfo.downUrl));
#endif
		m_downList.pop_front();
		m_down_count -- ;
		m_down_mutex.Leave();
		
		if( 0 != DownLoadFile(downInfo)){
			//若下载失败则重现放进下载队列
			CGuard m_Guard(m_down_mutex);
			_printd("DownLoadFile error(%s)",downInfo.downUrl);
			
			m_downList.push_back(downInfo);
			m_down_count ++;
		}
	}
	
	return 0;
}
VD_BOOL CDoorLockManager::SendTidTokenToLock()
{
#if 0
	CConfigTidAuth	cfgTidAuth;
	cfgTidAuth.update();
	
	CONFIG_TIDAUTH &config = cfgTidAuth.getConfig();

	if(0 == config.Token[0]){
		_printd("Token is null");
		return VD_FALSE;
	}
	
	char TokenHex[128] 	= {0};
	int	 TokenLen		= 0; 	
	TokenLen =  hexStr2bytes(config.Token,(uchar *)TokenHex,sizeof(TokenHex));

	
	if( TokenLen > 0 && 0 == GetSecretKeyResp((char *)TokenHex,TokenLen)){
		_printd("GetSecretKeyResp:%s success",config.Token);
		return VD_TRUE;
	}	

	return VD_FALSE;
#else
	return VD_TRUE;
#endif
}
void CDoorLockManager::ThreadProc()
{
	char setVerFlag = 0;
	int ntp_update = 0;
	lockInfo sInfo = {0};

#ifndef X1000_LOCK
	QueryLockTimer(0);
	m_lockTimer.Start(this,(VD_TIMERPROC)&CDoorLockManager::QueryLockTimer,1*1000,1000);
#endif

	while(m_bLoop)
	{
		SystemSleep(1000);  //休眠 1s
		ProcessDownInfo();
		ProcessLockMsg();

#ifdef DOOR_LOCK		
		//等开锁消息发送后在做其他处理
		if(g_DevExternal.Dev_GetWakeUpFlag() == SYS_WAKEUP_FLAG_LOCK &&
			m_alarm_msg_count <= 0){
			_printd("lock msg[m_alarm_msg_count:%d]",m_alarm_msg_count);
			continue;
		}
		else if(VD_TRUE == m_lock_awake_flg && VD_TRUE != m_lock_gettoken_flg){
			//若门锁被唤醒了，则等待开锁命令或其它有关收到后在做其他设置处理
			_printd("Lock have been wakenup ,so wait open message");
			continue;			
		}
 		else if(VD_TRUE == m_lock_gettoken_flg ){
			if(VD_TRUE == SendTidTokenToLock()){
				m_lock_gettoken_flg = VD_FALSE;
			}

			continue;
		}
		
		if( (g_NtpClient.GetNtpUpdateCount() != ntp_update &&
			0 == ntp_update) || VD_TRUE == ntpInfoUpdate){
		
			if( VD_TRUE == SyncLockTime()){
				_printd("updating time of lock by ntp");
				ntp_update 		= g_NtpClient.GetNtpUpdateCount();
				ntpInfoUpdate 	= VD_FALSE;
			}
			else{
				ntp_update = 0;
			}
			
			//锁端相关操作消耗时间较长，故每设置完便continue休眠
			continue;
		}
			
#ifndef X1000_LOCK
		//ntp更新后在检查
		if(0 < ntp_update){		

			if(0 == sInfo.Electric){
				//开机检测一次锁端电量，若电量低于50则发送低电量报警
				if(0 == GetLockStatus(&sInfo) && 
					sInfo.Electric != 0 &&
					sInfo.Electric < 50){

					//一天只上报一次低电报警
					if (g_DevExternal.CheckBatAlarmTime(BAT_ALARM_TYPE_LOCK) < 0) {
						continue;
					}

					RT_LockMsgInfo lockinfo ;
					memset(&lockinfo,0,sizeof(lockinfo));

					lockinfo.MsgType	= RT_LOCK_LOWBAT;
					lockinfo.Electric 	= sInfo.Electric;	
					
					CPacket *pPacket = g_PacketManager.GetPacket(sizeof(RT_LockMsgInfo)+1);
					if(pPacket == NULL){
						_printd("Ex -- GetPacket failed.\n");
					}
					else{
						CGuard m_Guard(m_lock_mutex);
						
						memcpy((RT_LockMsgInfo *)(pPacket->GetBuffer()),&lockinfo,sizeof(RT_LockMsgInfo));

						pPacket->AddRef();
						m_lock_count ++;
						m_lockList.push_back(pPacket);
					}	
				}
				_printd("sInfo.Electric:%d.\n",sInfo.Electric);
				
				//锁端相关操作消耗时间较长，故每设置完便continue休眠
				continue;
			}			
		}

		if(!setVerFlag){
			char Version[32] = {0};
			GetDevVersion(Version,sizeof(Version));

			//考虑到锁端旧版本(20190708之前的版本)不支持该条命令，
			//如果一直失败一直尝试，会影响到开锁记录的获取
			//故只设置一次就不设置了
			if( 0 == SetLockModuleVersion(Version,"20190813")){
				//setVerFlag = 1;

				//GetLockSoftVersion(Version);
				//_printd("Lock Version:%s",Version);
			}
			setVerFlag = 1;
			
			//锁端相关操作消耗时间较长，故每设置完便continue休眠
			continue;
		}
#endif

#endif	

	}
}
