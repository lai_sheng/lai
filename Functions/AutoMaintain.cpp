

#include "config-x.h"
#include "Functions/AutoMaintain.h"
#include "Main.h"
#include "System/AppConfig.h"
#include "System/Support.h"

// global variable
//static char gs_szFileNameAuto[MAX_PATH] = CONFIG_DIR"/Auto";		// 用于保存自动维护配置的文件名

PATTERN_SINGLETON_IMPLEMENT(CAutoMaintain);

CAutoMaintain::CAutoMaintain() : m_timerAuto("AutoMaintain")
{
//	memset(&m_conDefaultAuto, 0, sizeof(CONFIG_AUTO));
//	m_conDefaultAuto.verConfig.dwVersion = AUTO_CUR_VERSION;
	m_pfnMaintain[MAINTAIN_REBOOT] = (MAINTAINPROC)&CAutoMaintain::OnReboot;
	m_pfnMaintain[MAINTAIN_DELETE_FILES] = NULL;
	SystemGetCurrentTime(&m_LastTime[MAINTAIN_REBOOT]);
}

CAutoMaintain::~CAutoMaintain()
{
}

/*
BOOL CAutoMaintain::LoadAutoConfig(CONFIG_AUTO * pconAuto)
{
	memcpy(pconAuto, &m_conAuto, sizeof(CONFIG_AUTO));

	return TRUE;
}
*/
void CAutoMaintain::Start()
{
	//获取自动维护默认值 20060712 XFL
/*	m_conDefaultAuto.verConfig.dwVersion = AUTO_CUR_VERSION;
	m_conDefaultAuto.AutoRebootDay = (BYTE)AppConfig::instance()->getNumber("Global.DefaultAutoRebootDay", 0);	
	m_conDefaultAuto.AutoRebootTime = (BYTE)AppConfig::instance()->getNumber("Global.DefaultAutoRebootTime", 0);	
	m_conDefaultAuto.AutoDeleteFilesTime = (BYTE)AppConfig::instance()->getNumber("Global.AutoDeleteFilesTime", 0);	
*/
	//ReadConfig();
	m_ConfigAutoMaintain.update();
	
	m_ConfigAutoMaintain.attach(this, (TCONFIG_PROC)&CAutoMaintain::OnConfigAutoMaintain);
	int ret =0;
	OnConfigAutoMaintain(m_ConfigAutoMaintain,ret);
}
void CAutoMaintain::OnConfigAutoMaintain(CConfigAutoMaintain& config, int& ret)
{
//	CDHFile file;
	CGuard guard(m_mutexCout);

	m_ConfigAutoMaintain.update();
//	file.SetFileHoldTime(m_ConfigAutoMaintain[0].iAutoDeleteFilesDays * 24 * 3600); //fyj
	m_timerAuto.Start(this, (VD_TIMERPROC)&CAutoMaintain::OnTimer, 0, 10 * 1000);
}

void CAutoMaintain::OnTimer(uint par)
{
	int i;

	for (i = 0; i < MAINTAIN_TYPES; i++)
	{
		if (!m_pfnMaintain[i])
		{
			continue;
		}
		if (IsMaintainNow(i))
		{
			(this->*m_pfnMaintain[i])(par);
		}
	}
}

/*!	
	函数名	: IsMaintainNow
==  ==================================================================
	功能描述: 用于判断当前自动维护是否要执行
	输入参数: 
	输出参数: 
	返回值	: 
	创建日期: 2006-1-18		zhouwei		Create the Function
==  ==================================================================
	实现原理: 
==  ==================================================================
*/
VD_BOOL CAutoMaintain::IsMaintainNow(int iMaintainType)
{
	VD_BOOL bRet = FALSE;
	SYSTEM_TIME stCurTime, stMaintainTime;
	CGuard guard(m_mutexCout);

	// 输入参数检查
	if (iMaintainType < 0 || iMaintainType >= MAINTAIN_TYPES)
	{
		trace("iMaintainType error!\n");
		tracepoint();
		return FALSE;
	}

	SystemGetCurrentTime(&stCurTime);
	if(iMaintainType == MAINTAIN_REBOOT)
	{
		stMaintainTime = m_LastTime[iMaintainType];
		if(m_ConfigAutoMaintain[0].iAutoRebootDay == 1 //每天重启
			|| (m_ConfigAutoMaintain[0].iAutoRebootDay > 1 && 0 != m_ConfigAutoMaintain[0].iAutoRebootHour &&stMaintainTime.wday % 7 == m_ConfigAutoMaintain[0].iAutoRebootDay - 2)		//每周重启,非零点重启
			|| (m_ConfigAutoMaintain[0].iAutoRebootDay > 1 && 0 == m_ConfigAutoMaintain[0].iAutoRebootHour && 2 != m_ConfigAutoMaintain[0].iAutoRebootDay && stMaintainTime.wday % 7 == m_ConfigAutoMaintain[0].iAutoRebootDay - 3)  //设置为每周0点重启时，日期正好相差1天，所以要减去3，非周末
			|| (2 == m_ConfigAutoMaintain[0].iAutoRebootDay && 0 == m_ConfigAutoMaintain[0].iAutoRebootHour && stMaintainTime.wday % 7 == m_ConfigAutoMaintain[0].iAutoRebootDay + 4))  //如果设置为每周日的话，相差一周时间，所以要＋4	
		{
			stMaintainTime.hour = m_ConfigAutoMaintain[0].iAutoRebootHour;
			stMaintainTime.minute = 0;
			stMaintainTime.second = 0;

			if (0 == stMaintainTime.hour)
			{
				//说明是零点,转到下一天判断
				timeadd(&stMaintainTime, &stMaintainTime, 24 * 60 * 60);
			}

			int Seconds = time2second(&stCurTime, &stMaintainTime);
			if (timecompare(&m_LastTime[iMaintainType], &stMaintainTime) < 0 //时间跨越监视点, 触发自动维护服务
				&& Seconds <= 15
				&& Seconds >= 0)// 处理还没到的时间和手动修改系统时间的情况
				{
					bRet = TRUE;
				}
		}
	}
	
	m_LastTime[iMaintainType] = stCurTime;
	return bRet;
}

/*!	
	函数名	: OnConChanged
==  ==================================================================
	功能描述: 每次配置保存后，查看开始时间是否有变化，如果有变化，则更新定时计数器
	输入参数: 
	输出参数: 
	返回值	: 
	创建日期: 2006-1-18		zhouwei		Create the Function
==  ==================================================================
	实现原理: 
	获取自动维护开始时间和当前时间的差值；
	根据维护周期类型来设置计数器m_aiCount
==  ==================================================================
*/
/*
void CAutoMaintain::OnConChanged()
{
	CDHFile file;
	CGuard guard(m_mutexCout);

	file.SetFileHoldTime(file_hold_days[m_conAuto.AutoDeleteFilesTime] * 24 * 3600);
	m_timerAuto.Start(this, (TIMERPROC)&CAutoMaintain::OnTimer, 0, 10 * 1000);
}
*/
/*!	
	函数名	: OnReboot
==  ==================================================================
	功能描述: 自动维护的回调函数
	输入参数: 
	输出参数: 
	返回值	: 
	创建日期: 2006-1-19		zhouwei		Create the Function
==  ==================================================================
	实现原理: 
==  ==================================================================
*/
void CAutoMaintain::OnReboot(uint par)
{
	SYSTEM_TIME stCurTime;

	SystemGetCurrentTime(&stCurTime);
	trace("The reboot is %d-%d-%d  %d:%d:%d\n", 
		stCurTime.year, stCurTime.month, stCurTime.day, stCurTime.hour, stCurTime.minute, stCurTime.second);
	g_Challenger.Reboot();
}

