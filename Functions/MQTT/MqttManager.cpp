#include "Intervideo/DevSearch/DevSearch.h"
#include "System/File.h"

#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>

#include "Functions/MQTT/MqttManager.h"
#include "APIs/CommonBSP.h"



#define MQTTADDRESS     "wja.ukyun.top"
#define MQTTPORT      8092

#define USERNAME    "admin"
#define PASSWORD    "123456"
//#define CLIENTID    "700002"
//#define CAFILE      "/app/ipcam/hms-cert.pem"
#define TOPIC0      "hms/103/003"      //hearbeat
#define TOPIC1      "hms/101/003"	   //hms data	
#define TOPIC2      "hms/102/003"      //alert data
#define QOS         1
#define TIMEOUT     10000L
#define MQTTCLIENT_PERSISTENCE_NONE 1



using namespace std; 


 
 

PATTERN_SINGLETON_IMPLEMENT(MqttManager);

MqttManager::MqttManager():CThread("MqttManager", TP_NET)
{
	_printd("MqttManager");
	m_iSdStatus = 0;
	m_iSDerroCount = 0;
}
MqttManager::~MqttManager()
{
	_printd("MqttManager");
}
int MqttManager::GetUUID()
{	
	char cuuid[32] = {0};
	if(0 != GetUuid(cuuid))
	{
		_printd("Read Factory Info Err");
		return -1;
	}
	m_cUUID = cuuid;
	return 0;
}
int MqttManager::GetMac()
{
	struct ifreq ifreq;
	int sock;
	if((sock=socket(AF_INET,SOCK_STREAM,0))<0)
	{
		perror("socket");
		return 2;
	}
	strcpy(ifreq.ifr_name,"wlan0");
	if(ioctl(sock,SIOCGIFHWADDR,&ifreq)<0)
	{
		perror("ioctl");
		return 3;
	}
	printf("%02X:%02X:%02X:%02X:%02X:%02X\n",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	snprintf(m_cMac,20,"%02X%02X%02X%02X%02X%02X",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	return 0;
}
 int MqttManager::CheckSDStatus()
{

	return 0;
}

int MqttManager::GetDevTime(char date[HMSDATELEN])
{
    time_t cur_time = 0;
    struct tm *ptm = NULL;
    struct timeval  tv;
    struct timezone tz;
	time_t 	t  = time(NULL);
	
    gettimeofday(&tv, &tz);
    t += tz.tz_minuteswest*60;
	ptm = gmtime(&t);
    printf("cur time: %04d-%02d-%02d %02d:%02d:%02d\n", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	snprintf(date,20,"%04d%02d%02d%02d%02d%02d", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	return 0;
}
long MqttManager::GetDevUptime()
{
	struct sysinfo info;
    sysinfo(&info);
    return info.uptime;	
}
int MqttManager::GetSysBootTime()
{
	struct sysinfo info;
    time_t boot_time = 0;
    struct tm *ptm = NULL;
	char date[128] = {0};
	
	struct timeval  tv;
    struct timezone tz;
	time_t 	cur_time  = time(NULL);
	
    gettimeofday(&tv, &tz);
    cur_time += tz.tz_minuteswest*60;
	
    if (sysinfo(&info)) {
    fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n",
        errno, strerror(errno));
    return -1;
    }
    if (cur_time > info.uptime) {
    boot_time = cur_time - info.uptime;
    }
    else {
    boot_time = info.uptime - cur_time;
    }
	ptm = gmtime(&boot_time);
    printf("System boot time: %04d-%02d-%02d %02d:%02d:%02d\n", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	snprintf(date,sizeof(date),"%04d-%02d-%02d %02d:%02d:%02d", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	m_cbootime = date;
	return 0;
}
int MqttManager::GetIP(char date[HMSDATELEN])
{
	GetDevIp(date,HMSDATELEN);
	return 0;
}

VD_INT32 MqttManager::Start()
{
	int iRet;
	int RebootFlag = 0;
	m_UUIDStatus = 0;     
	m_iMqttConStatus = 0; 
	m_iMqttConFailCount = 0; 
	
	_printd("MqttManager init!!!");
	GetUUID();
	GetMac();
//	m_cTimer.Start(this,(VD_TIMERPROC)&MqttManager::onTimer,30000,30000);
	CreateThread();
	_printd ("MqttManager Init Over");
	return 0;
}
VD_INT32 MqttManager::Stop()
{
	_printd("MqttManager :: Stop ");
	return 0;
}
void MqttManager::onTimer(uint arg)
{
	_printd("onTimer !!!!");
	
}

void MqttManager::ThreadProc()
{
	int timeout = 0;
	int packet_length = 0;
	int msgreq =1;
	while (m_bLoop) 
	{		
		m_mutex.Enter();
		if(m_iMqttConStatus)
		{
			msgreq = 0;
			packet_length = mqtt_read_packet(&m_broker,1);
			if(packet_length == -1)
			{
				_printd("Error(%d) on read packet!\n", packet_length);
				mqtt_close_socket(&m_broker);
				m_iMqttConStatus = 0;
				msgreq = 0; 
				m_mutex.Leave();
				continue;
			}
			else if(packet_length > 0)
			{
				//_printd("Packet Header: 0x%x...", (m_psock->buffer)[0]);
				if(MQTTParseMessageType(m_psock->buffer) == MQTT_MSG_PUBLISH)
				{
					uint8_t topic[255], msg[1000];
					uint16_t len;
					len = mqtt_parse_pub_topic(m_psock->buffer, topic);
					topic[len] = '\0'; // for printf
					len = mqtt_parse_publish_msg(m_psock->buffer, msg);
					msg[len] = '\0'; // for printf
					printf("%s %s\n", topic, msg);
					msgreq =1;
				}
			}
			
		}
		else
		{			
			ConnectMqttServer();
			if(!m_iMqttConStatus)
			{
				sleep(10);
			}
		}
		m_mutex.Leave();
		if(msgreq)
		{
			GetSysBootTime();
			if(!m_cbootime.empty())
			{
				char msg[128] ={0};
				snprintf(msg,128,"{BOOTTIME: %s}",m_cbootime.c_str());
				SendData(0,"Device/Resp", msg);
				_printd("send mqtt Device/Resp");
			}
			else
			{
				SendData(0,"Device/Resp", "{BOOTTIME: \"ERROR\"}");
			}
		}

		if(timeout++ >= 50 && m_iMqttConStatus)
		{
			timeout = 0;
			SendData(1,NULL, NULL);
		}
		//sleep(1);
	}	
}

int MqttManager::PackData(string &packdata)
{

    return 0;

}
int MqttManager::ConnectMqttServer()
{
	int ret = 0;
	uint16_t msg_id, msg_id_rcv;
	if(!m_cUUID.empty())
	{
		if(-1 == GetUUID())
		{ return -1;}
	}
	mqtt_init(&m_broker, m_cUUID.c_str());
	mqtt_init_auth(&m_broker, USERNAME, PASSWORD);
	ret = mqtt_init_socket(&m_broker, MQTTADDRESS, MQTTPORT);
	if(-1 != ret)
	{
		int packet_length = 0;
		mqtt_set_alive(&m_broker, 60);
		mqtt_connect(&m_broker);
		m_psock = (CSocketInfo *)m_broker.socket_info;
		packet_length = mqtt_read_packet(&m_broker,3);
		if(packet_length < 0)
		{
			_printd("Error(%d) on read packet!\n", packet_length);
			mqtt_close_socket(&m_broker);
			return -1;
		}

		if(MQTTParseMessageType(m_psock->buffer) != MQTT_MSG_CONNACK)
		{
			_printd("CONNACK expected!\n");
			mqtt_close_socket(&m_broker);
			return -2;
		}
		if((m_psock->buffer)[3] != 0x00)
		{
			_printd("CONNACK failed!\n");
			mqtt_close_socket(&m_broker);
			return -2;
		}
		
		// >>>>> SUBSCRIBE
		//����
		mqtt_subscribe(&m_broker, "Device/Req", &msg_id);
		// <<<<< SUBACK
		packet_length = mqtt_read_packet(&m_broker,2);
		if(packet_length < 0)
		{
			mqtt_close_socket(&m_broker);
			_printd("Error(%d) on read packet!\n", packet_length);
			return -1;
		}

		if(MQTTParseMessageType(m_psock->buffer) != MQTT_MSG_SUBACK)
		{
			_printd("SUBACK expected!");
			mqtt_close_socket(&m_broker);
			return -2;
		}

		msg_id_rcv = mqtt_parse_msg_id(m_psock->buffer);
		if(msg_id != msg_id_rcv)
		{
			_printd("%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
			mqtt_close_socket(&m_broker);
			return -3;
		}
		m_iMqttConStatus = 1;
		return 0;
	}
	return -1;
}
int MqttManager::SendData(int     isping , const char* topic, const char* msg)
{
	int  packet_length = 0;
	uint16_t msg_id, msg_id_rcv;
	CGuard guard(m_mutex);
	if(0 == m_iMqttConStatus)
	{
		if(ConnectMqttServer()<0)
		{
			return -1;
		}
	}
	
	if(isping)
	{
		//����
		mqtt_ping(&m_broker);
		_printd("send ping");
		return 0;
	}
	mqtt_publish_with_qos(&m_broker, topic, msg, 1, 2, &msg_id); // Retain
	// <<<<< PUBREC
	packet_length = mqtt_read_packet(&m_broker,1);
	if(packet_length < 0)
	{
		_printd("Error(%d) on read packet!", packet_length);
		mqtt_close_socket(&m_broker);
		m_iMqttConStatus = 0;
		return -1;
	}

	if(MQTTParseMessageType(m_psock->buffer) != MQTT_MSG_PUBREC)
	{
		_printd("PUBREC expected!");
		mqtt_close_socket(&m_broker);
		m_iMqttConStatus = 0; 
		return -2;
	}

	msg_id_rcv = mqtt_parse_msg_id(m_psock->buffer);
	if(msg_id != msg_id_rcv)
	{
		_printd("%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
		m_iMqttConStatus = 0;
		return -3;
	}
	// >>>>> PUBREL
	mqtt_pubrel(&m_broker, msg_id);
	// <<<<< PUBCOMP
	packet_length = mqtt_read_packet(&m_broker,1);
	if(packet_length < 0)
	{
		_printd("Error(%d) on read packet!", packet_length);
		mqtt_close_socket(&m_broker);
		m_iMqttConStatus = 0;
		return -1;
	}

	if(MQTTParseMessageType(m_psock->buffer) != MQTT_MSG_PUBCOMP)
	{
		_printd("PUBCOMP expected!");
		mqtt_close_socket(&m_broker);
		m_iMqttConStatus = 0;
		return -2;
	}

	msg_id_rcv = mqtt_parse_msg_id(m_psock->buffer);
	if(msg_id != msg_id_rcv)
	{
		_printd("%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
		m_iMqttConStatus = 0;
		mqtt_close_socket(&m_broker);
		return -3;
	}
	
	return 0;
}
int MqttManager::PackSendAlertData(int AlertCode,int chn,int Reason,int Raise)
{
	return 0;
}
int MqttManager::PackSendHeartBeatData()
{
	

	return 0;
}

int MqttManager::TestSendDataToHmsServer(char *HmsServerUrl,char *Pwd)
{
	m_mutex.Enter();

	m_mutex.Leave();
    return 0;
}
int MqttManager::GetWlanIp(std::string & result)
{

	return 0;
}

int MqttManager::GetRecordStartTime(char date[HMSDATELEN])
{

	return 0;
}