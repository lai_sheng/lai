/*
 * This file is part of libemqtt.
 *
 * libemqtt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemqtt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with libemqtt.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *
 * Created by Vicente Ruiz Rodríguez
 * Copyright 2012 Vicente Ruiz Rodríguez <vruiz2.0@gmail.com>. All rights reserved.
 *
 */



#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <linux/tcp.h>
#include <fcntl.h>
#include <error.h>
#include <stdlib.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>

#include "Functions/MQTT/libemqtt.h"
#include "Functions/MQTT/libmqttsub.h"

#define _printd(msg,...) printf ("\033[22m\033[34m[%s][%d]\033[22m\033[37m"msg"\n", (char *)(__FILE__), __LINE__ , ##__VA_ARGS__)




int GetMqttServerIpAddr(const char *Server, char *Ipaddr)
{
	struct addrinfo * res, *pt;   
	struct sockaddr_in *sinp;   
	struct addrinfo hints;
	const char *addr;  
	int succ=0,i=0; 
	char abuf[INET_ADDRSTRLEN];  
		
	if(inet_addr(Server) == INADDR_NONE)
	{
	    memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_INET; /* Allow IPv4 */
	    hints.ai_flags = AI_PASSIVE; /* For wildcard IP address */
	    hints.ai_protocol = 0; /* Any protocol */
	    hints.ai_socktype = SOCK_STREAM;
	    succ = getaddrinfo(Server, NULL, &hints, &res);   
	    if(succ != 0)   
	    {   
	    	if(-2 == succ)
	    	{
	    	}
	       return -1;
	    }
		else
		{
			if (res->ai_addr->sa_family == AF_INET)
	        {
	            if (Ipaddr != NULL)
	            {
		            struct sockaddr_in* pServAddr = (struct sockaddr_in *)res->ai_addr;
		            inet_ntop(AF_INET, &(pServAddr->sin_addr), Ipaddr, INET_ADDRSTRLEN);
					//REAL_DEBUG("Ipaddr:%s",Ipaddr);
	            }

		   }
		}
		_printd("Ipaddr:%s",Ipaddr);

		freeaddrinfo(res);
	}
	else
	{
		memcpy(Ipaddr,Server,INET_ADDRSTRLEN);
	}
	return 0;
}

int mqtt_send_packet(void* socket_info, const void* buf, unsigned int count)
{
	CSocketInfo *psock = (CSocketInfo*)socket_info;
	int fd = psock->socketfd;
	return send(fd, buf, count, 0);
}

int mqtt_init_socket(mqtt_broker_handle_t* broker, const char* hostname, short port)
{
	int ret = 0;
	int flag = 1;
	int keepalive = 15; // Seconds
	int socketfd = -1;
	CSocketInfo *psock = NULL;
	char ipaddr[16] = {0};
	fd_set wds;
	struct timeval tv;
	
	GetMqttServerIpAddr(hostname,ipaddr);
	psock = malloc(sizeof(CSocketInfo));
	if(NULL == psock)
	{
		_printd("error");
		goto reterror;
	}
	memset(psock, 0, sizeof(CSocketInfo));
	psock->buffer = malloc(1024);
	if(NULL == psock->buffer)
	{
		_printd("error");
		goto reterror;
	}
	psock->bufsize = 1024;
	// Create the socket
	if((socketfd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	{
		_printd("error");
		goto reterror;
	}
	// Disable Nagle Algorithm
	if (setsockopt(socketfd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag)) < 0)
	{
		_printd("error");
		goto reterror;
	}
	if ((flag = fcntl(socketfd, F_GETFL, 0)) == -1)
	{
		_printd("fcntl(F_GETFL, O_NONBLOCK)");
    }
    if (fcntl(socketfd, F_SETFL, flag | O_NONBLOCK) == -1)
	{
        _printd("fcntl(F_SETFL, O_NONBLOCK)");
    }
	struct sockaddr_in socket_address;
	// Create the stuff we need to connect
	socket_address.sin_family = AF_INET;
	socket_address.sin_port = htons(port);
	socket_address.sin_addr.s_addr = inet_addr(ipaddr);

	// Connect the socket
	ret = connect(socketfd, (struct sockaddr *)&socket_address,sizeof(socket_address));

	if(0 > ret)
	{  
		int iRet;
		int len;
		int error;
        FD_ZERO(&wds);
        FD_SET(socketfd,&wds);
        tv.tv_sec  = 3;
        tv.tv_usec = 0;
        iRet = select(socketfd + 1, NULL, &wds, NULL, &tv);
        if(iRet>0 && FD_ISSET(socketfd,&wds))
		{
            len = sizeof(error);
            getsockopt(socketfd, SOL_SOCKET, SO_ERROR, (char*)&error, (socklen_t*)&len);
			if(0 != error)
			{
				_printd("connect failed");
				ret = -1;
				goto reterror;
			}
			else
			{
				ret = 0;
			}
       	}
	}

	// MQTT stuffs
	mqtt_set_alive(broker, keepalive);
	psock->socketfd = socketfd;
	broker->socket_info = (void*)psock;
	broker->send = mqtt_send_packet;

	return 0;
reterror:
	if(-1 != socketfd)
	{
		close(socketfd);
	}
	if(psock)
	{
		if(psock->buffer)
			free(psock->buffer);
		free(psock);
	}
	return -1;

}

int mqtt_close_socket(mqtt_broker_handle_t* broker)
{
	CSocketInfo *psock = (CSocketInfo*)broker->socket_info;
	int fd = psock->socketfd;
	free(psock->buffer);
	free(psock);
	return close(fd);
}




int mqtt_read_packet(mqtt_broker_handle_t* broker,int timeout)
{
	int ret = 0;
	CSocketInfo *psock = (CSocketInfo *)broker->socket_info;
	if(timeout > 0)
	{
		
		fd_set readfds;
		struct timeval tmv;

		// Initialize the file descriptor set
		FD_ZERO (&readfds);
		FD_SET (psock->socketfd, &readfds);

		// Initialize the timeout data structure
		tmv.tv_sec = timeout;
		tmv.tv_usec = 0;

		// select returns 0 if timeout, 1 if input available, -1 if error
		//if(0 == select(psock->socketfd + 1, &readfds, NULL, NULL, &tmv))
		//	return -2;

		ret = select(psock->socketfd + 1, &readfds, NULL, NULL, &tmv);
		if (ret <= 0) 
	    {
	       // _printd("select: %s\r\n", strerror(errno));
	        return -2;
	    }
	}

	int total_bytes = 0, bytes_rcvd, packet_length;
	memset(psock->buffer, 0, psock->bufsize);

	while(total_bytes < 2) // Reading fixed header
	{
		if((bytes_rcvd = recv(psock->socketfd, (psock->buffer+total_bytes), psock->bufsize-total_bytes, 0)) <= 0)
			return -1;
		total_bytes += bytes_rcvd; // Keep tally of total bytes
	}

	packet_length = (psock->buffer)[1] + 2; // Remaining length + fixed header length

	while(total_bytes < packet_length) // Reading the packet
	{
		if((bytes_rcvd = recv(psock->socketfd, (psock->buffer+total_bytes), psock->bufsize-total_bytes, 0)) <= 0)
			return -1;
		total_bytes += bytes_rcvd; // Keep tally of total bytes
	}

	return packet_length;
}






#if 0
int main(int argc, char* argv[])
{
	int packet_length;
	uint16_t msg_id, msg_id_rcv;
	mqtt_broker_handle_t broker;
	CSocketInfo *psock = NULL;
	_printd("1");
	mqtt_init(&broker, "avengalvon");
	_printd("2");
	mqtt_init_auth(&broker, "cid", "campeador");
	_printd("3");
	mqtt_init_socket(&broker, "wja.ukyun.top", 8092);
	_printd("4");
	// >>>>> CONNECT
	mqtt_connect(&broker);

	psock = (CSocketInfo *)broker.socket_info;
	_printd("5");
	// <<<<< CONNACK
	packet_length = mqtt_read_packet(&broker,3);
	if(packet_length < 0)
	{
		fprintf(stderr, "Error(%d) on read packet!\n", packet_length);
		return -1;
	}

	if(MQTTParseMessageType(psock->buffer) != MQTT_MSG_CONNACK)
	{
		fprintf(stderr, "CONNACK expected!\n");
		return -2;
	}

	if((psock->buffer)[3] != 0x00)
	{
		fprintf(stderr, "CONNACK failed!\n");
		return -2;
	}
while(1)
{
	// >>>>> PUBLISH QoS 0
	printf("Publish: QoS 0\n");
	mqtt_publish(&broker, "hello/123456", "Example: QoS 0", 0);
		sleep(5);
	}

	// >>>>> PUBLISH QoS 1
	printf("Publish: QoS 1\n");
	mqtt_publish_with_qos(&broker, "hello/emqtt", "Example: QoS 1", 0, 1, &msg_id);
	// <<<<< PUBACK
	packet_length = mqtt_read_packet(&broker,1);
	if(packet_length < 0)
	{
		fprintf(stderr, "Error(%d) on read packet!\n", packet_length);
		return -1;
	}

	if(MQTTParseMessageType(psock->buffer) != MQTT_MSG_PUBACK)
	{
		fprintf(stderr, "PUBACK expected!\n");
		return -2;
	}

	msg_id_rcv = mqtt_parse_msg_id(psock->buffer);
	if(msg_id != msg_id_rcv)
	{
		fprintf(stderr, "%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
		return -3;
	}

	// >>>>> PUBLISH QoS 2
	printf("Publish: QoS 2\n");
	mqtt_publish_with_qos(&broker, "hello/emqtt", "Example: QoS 2", 1, 2, &msg_id); // Retain
	// <<<<< PUBREC
	packet_length = mqtt_read_packet(&broker,1);
	if(packet_length < 0)
	{
		fprintf(stderr, "Error(%d) on read packet!\n", packet_length);
		return -1;
	}

	if(MQTTParseMessageType(psock->buffer) != MQTT_MSG_PUBREC)
	{
		fprintf(stderr, "PUBREC expected!\n");
		return -2;
	}

	msg_id_rcv = mqtt_parse_msg_id(psock->buffer);
	if(msg_id != msg_id_rcv)
	{
		fprintf(stderr, "%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
		return -3;
	}
	// >>>>> PUBREL
	mqtt_pubrel(&broker, msg_id);
	// <<<<< PUBCOMP
	packet_length = mqtt_read_packet(&broker,1);
	if(packet_length < 0)
	{
		fprintf(stderr, "Error(%d) on read packet!\n", packet_length);
		return -1;
	}

	if(MQTTParseMessageType(psock->buffer) != MQTT_MSG_PUBCOMP)
	{
		fprintf(stderr, "PUBCOMP expected!\n");
		return -2;
	}

	msg_id_rcv = mqtt_parse_msg_id(psock->buffer);
	if(msg_id != msg_id_rcv)
	{
		fprintf(stderr, "%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
		return -3;
	}

	// >>>>> DISCONNECT
	mqtt_disconnect(&broker);
	mqtt_close_socket(&broker);
	return 0;
}
#endif
