#include <assert.h>
#include <sys/mount.h>
#include <sys/statfs.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <linux/fs.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <utime.h>
#include <pthread.h>
#include "Mp4/Mp4Pack.h"
#include "Functions/FFFile.h"
#include "Functions/Mkfs.fat.h" 
#include "Functions/DriverManager.h"


typedef long long int64_t;
typedef unsigned long long uint64_t;

static char mount_flag = 0; //0 未挂载 1 已挂载 2 正在挂载 3 格式化中
static char source_dir[20]={0}; //挂载设备路径

char force_umount =  0;
int m_iPostion = 0;  //测试不录像地方
char g_DelFileFlag = 0; //删除录像再查询，避免频繁查询开始录像文件
static void remove_waste_file();
static int  remove_earlist_videofile(char* dirPath);
static void recover_recfile(char *dirpath);
static int filter_videolist(const struct dirent *dp);
static int get_sdcard_cap(int* total,int* free);

static CMutex query_mutex;
static CMutex mount_mutex;
static CMutex remove_mutex;
static CMutex dev_mutex; 
SYSTEM_TIME recsystime = {0};  
int repair_flag  =0;
static int Get_videoRate()
{
	int bitrate = 25;
#ifdef IPC_JZ_NEW
	bitrate = 25;
#elif defined(IPC_HISI) 
	bitrate = 20;
#endif
	return bitrate;
}

static int  create_dir(const char* PathName)
{
	char buf[256]={0};

	if(access((const char *)PathName, F_OK) != 0)
	{
		if(mkdir(PathName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
		{
			snprintf(buf,sizeof(buf),"mkdir -p %s",PathName);
			_printd("error:%s",buf);
			return -1;
		}
	}	
	return 0;
	
}
//创建多级目录
int mkdir_mul(char *mulDir) 
{
	int ret = 0;
    int i,len;
    char str[512] = {0};    
	if(mulDir == NULL)
	{
		return -1;
	}
	if( 0 == access(mulDir,F_OK))
	{
		return 0;
	}
	_printd("%s",mulDir);
    snprintf(str,sizeof(str),"%s", mulDir);
    len = strlen(str);

	for( i = 1; i < len; i ++ )
    {
        if( str[i]=='/' )
        {
            str[i] = '\0';

			_printd("%s",str);
             ret = create_dir(str);
			if(ret)
			{
				return -1;
			}
            str[i] = '/';
        }
    }
    if( len > 0 && access(str,F_OK) != 0 ){
       ret = create_dir(str);
    }
	return ret;
}

static int filter_dev(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".")==0  || strcmp(dp->d_name,"..")==0)  
    	return 0; 

	int size = strlen(dp->d_name);
    if( size > 10 || size < 7)     
    	return 0;  

	if(strstr(dp->d_name,"mmcblk") == NULL)
		return 0;

	return 1;
}

static bool source_dir_init()
{
	CGuard m_Cguard(dev_mutex);
	
	int    n;	
	struct dirent **filelist = NULL;

	//add:多次拔插sd卡，app小概率查询不到sd卡
	if(access("/dev/mmcblk0p1",F_OK) == 0){
		memset(source_dir,0,sizeof(source_dir));
		snprintf(source_dir,sizeof(source_dir),"%s", "/dev/mmcblk0p1");
		_printd("sourcd dir:%s\n",source_dir);
		return true;
	}else if(access("/dev/mmcblk0",F_OK) == 0){
		memset(source_dir,0,sizeof(source_dir));
		snprintf(source_dir,sizeof(source_dir),"%s", "/dev/mmcblk0");
		_printd("sourcd dir:%s\n",source_dir);
		return true;
	}

	//原代码
	if(access(SOURCE_DEV_DIR,F_OK) != 0){
		_printd("addr:%s\n",SOURCE_DEV_DIR);
		return false;
	}
	
	n = scandir(SOURCE_DEV_DIR,&filelist,filter_dev,alphasort);

	if(n <= 0)
	{
		if(filelist != NULL)
			_printd("addr:%d\n",filelist);

		free(filelist);
		return false;
	}
	
	memset(source_dir,0,sizeof(source_dir));

	snprintf(source_dir,sizeof(source_dir),"%s%s",SOURCE_DEV_DIR,filelist[n-1]->d_name);

	for(int i = 0 ; i < n ; i++){
		free(filelist[i]);
	}
	free(filelist);
	
	_printd("sourcd dir:%s\n",source_dir);
	
	return true;
}
static int  checkismount(char *pDevName, char *pDirName)
{
	FILE  *fp   = NULL;
	char  strDevName[48] = {0};
	char  strDirName[48] = {0};
	char  strFileType[16]= {0};
	char  strMountParam[255] = {0};
	int   other[2] = {};
	char  buf[255] = {0};
	int   nret = 0;
	if(NULL == pDevName || NULL == pDirName){
		return -1;
	}	
	
	if (NULL == (fp = fopen("/proc/mounts", "r")))
	{
		return -1;
	}
	
	while(!feof(fp))
	{
		fgets((char *)buf, 254, fp);
		sscanf(buf, "%s %s %s %s %d %d" , strDevName, 
			strDirName, strFileType, strMountParam, &other[0], &other[1]);
		//printf("strDevName=%s strDirName=%s strFileType=%s strMountParam=%s  %d  %d\n", 
		//       strDevName, strDirName, strFileType, strMountParam, other[0], other[1]);
		if ((0 == strcmp(pDevName, strDevName)) && (0 == strcmp(pDirName, strDirName)))
		{
			nret = 1;
			break;
		}
	}
	fclose(fp);
	
	if(1 != nret)		return -3;

	strMountParam[2] = '\0';
	_printd("postion[%d]mount Permission:%s",m_iPostion,strMountParam);
	if(strcmp(strMountParam,"rw") != 0)	
	{
		if(mount(pDevName, pDirName, FILE_SYSTEM, MS_REMOUNT, "-o remount,rw"))
		{
			char tmp[64];
	   		memset(tmp,0,sizeof(tmp));
	   		snprintf(tmp,sizeof(tmp),"mount -o rw,remount %s",TARGET_DIR);
	   		system(tmp);
		}
		return 2;
	}
	
	return 0;
}

int  mountStatus()
{
	return checkismount(source_dir, TARGET_DIR);
}



static int check_tf_valid(char *device)
{
	if (device == NULL) {
		return -1;
	}

	char ch;
	int fd = -1;

	fd = open(device, O_RDONLY);
	if (fd < 0) {
		perror(device);
		return -1;
	}

	loff_t offset = 1;
	while (lseek(fd, offset, SEEK_SET) > 0)
	{
		if (read(fd, &ch, 1) < 1)
		{
			_printd("%s is invalid !!!\n", device);
			close(fd);
			return -1;
		}
		offset *= 2;
	}

	close(fd);
	return 0;
}
int GetNewDir(char getNewDir[1024],char *basePath,int i)
{
    DIR *dir = NULL;
    struct dirent *ptr =NULL;
    char base[1024] = {0};
	char tmppath[1024] = {0};
	int  count = 0;
	int  iRet = 1;

	int  fileNum  = 0;
	
	if(2 == i)
	{
		snprintf(getNewDir,1024,"%s",basePath);
		printf("basePath:%s\n",basePath);
		return 0;
	}
    if ((dir=opendir(basePath)) == NULL)
    {
        perror("Open dir error...");
        return 1;
    }

    while ((ptr=readdir(dir)) != NULL)
    {
        if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
            continue;
        else if(ptr->d_type == 4)    ///dir
        {
			int ret = 0;
     
			if(0 == count)
			{
				snprintf(tmppath,1024,"%s",ptr->d_name);
			}
			else
			{
				ret = strncmp(tmppath,ptr->d_name,1024);
				if(ret < 0)
				{
					snprintf(tmppath,1024,"%s",ptr->d_name);
				}
			}
                      
			count++;
        }
		fileNum ++;
		//若文件数大于200 ，则认为该文件夹异常
		if(fileNum > 200){ //
			_printd("error dir:%s\n",basePath);
			break;
		}
    }
	if(count)
	{
		memset(base,'\0',sizeof(base));
		strcpy(base,basePath);
		strcat(base,"/");
		strcat(base,tmppath);
		printf("base:%s\n",base);
		iRet = GetNewDir(getNewDir,base,i+1);
		
	}
    closedir(dir);
    return iRet;
}

int CleanUpFileList(char* dirPath)
{
    DIR *dir;
	struct dirent *ptr;
	char filepath[128]={0};
	char trgtpath[128]={0};
	char trgtdir[128] ={0};	
	int  i 		= 0;
	int  n 		= 0;
	int  errorcount = 0;

	int year,mon,day,hour,min,sec;
	int timelen,snum;
	int ret = 0;

	CGuard l_cGuard(remove_mutex);
	
	if(dirPath == NULL)
	{
		printf("NULL\n");
		return -1;
	}
    dir = opendir(dirPath);
	if (NULL != dir)
	{
        while ((ptr = readdir(dir)) != NULL)
		{
            if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
				continue;
            if (DT_DIR != ptr->d_type)
			{
				if(!filter_videolist(ptr))
				{
					continue;
				}

				ret = sscanf(ptr->d_name,"%04d%02d%02d%02d%02d%02d_%d_%d.mp4",&year,&mon,&day,&hour,&min,&sec,&timelen,&snum);
				
				if(8 == ret)
				{
					snprintf(filepath,sizeof(filepath), "%s/%s", dirPath,ptr->d_name);
					snprintf(trgtdir,sizeof(trgtdir), "%s/%04d-%02d-%02d/%02d", dirPath,year,mon,day,hour);
					snprintf(trgtpath,sizeof(trgtpath), "%s/%s",trgtdir,ptr->d_name);	
					ret = mkdir_mul(trgtdir);
					if(0 == access(trgtdir,F_OK))
					{
						errorcount = 0;
						rename(filepath,trgtpath);
						_printd("move %s to %s\n",filepath,trgtpath);
					}
					else
					{
						//连续20次则为坏卡
						if(errorcount++ > 20)
						{
							break;
						}
						_printd("error:move %s to %s\n",filepath,trgtpath);
					}
				}
				n++;			
				continue;
			}
			
			
        }
		closedir(dir);
	}
	return n;
}
static pthread_t init_pid = 0;
void* thread_init_process(void* arg)
{
	int     f_total  = 0;
	int     f_free 	 = 0;
	struct statfs sdInfo = {0};
	char getNewDir[1024] = {0};
	int ret = 0;

	pthread_detach(pthread_self());
//	if(0 == recsystime.year)
	{
		SYSTEM_TIME systime;
		DHTIME dhtime;
		g_Cmos.GetExitTime(&dhtime);
		timedh2sys(&recsystime, &dhtime);
		if(recsystime.year != 0)
		{
			char lastpath[256] = {0};
			snprintf(lastpath,sizeof(lastpath),"%s/%04d-%02d-%02d",TIMER_DIR,recsystime.year,recsystime.month,recsystime.day);
			_printd("lastpath:%s", lastpath);
			if(0 == access(lastpath,F_OK))
			{
				//0--24小时修复
				for(int i = 0; i < 24; i++)
				{
					snprintf(getNewDir,sizeof(getNewDir),"%s/%02d",lastpath,i);
					if(0 == access(getNewDir,F_OK))
					{
						_printd("the current new dir is : %s",getNewDir);
						recover_recfile(getNewDir); 
					}
				}
			}
		}
		else
		{
			_printd("record last time : %04d-%02d-%02d",recsystime.year,recsystime.month,recsystime.day);
		}
	}
	
	ret = GetNewDir(getNewDir,TIMER_DIR,0);
	if(0 == ret){
		_printd("the current new dir is : %s",getNewDir);
		recover_recfile(getNewDir); 
	}
	
	_printd("recover file start...\n");
	recover_recfile(TIMER_DIR);
	recover_recfile(ALARM_DIR);
	CleanUpFileList(TIMER_DIR);

	if(statfs(TARGET_DIR,&sdInfo) >= 0)
	{
		f_total = (sdInfo.f_blocks / 1024.0) * (sdInfo.f_bsize);
		f_free  = (sdInfo.f_bfree / 1024.0)  * (sdInfo.f_bsize);
		if(f_total >  f_free && f_free <= ISFULL_CAPACITY)
		{
			_printd("full..free:%dkb\n",f_free);
			remove_waste_file();
			remove_earlist_videofile(TIMER_DIR);
		}
	}
	init_pid = 0;
	repair_flag = 1;
	checkismount(source_dir,TARGET_DIR);
	pthread_exit(0);
}

int file_sys_init()
{	
	CGuard 	m_Cguard(mount_mutex);
	char 	buf[40] 	 = {0};	
	char    mount_dir[20]= {0};

	if(force_umount )		return -1;	
	if(file_sys_get_linkstat() < 0)		return -1;

	//已挂载
	if(1 == mount_flag) return 0;
	//正在挂载 或者 格式化中
	if(2 == mount_flag || 3 == mount_flag ) return -1;
	
	mount_flag = 2;

	if(source_dir[0] == 0 || source_dir == NULL || source_dir[0] == '\0'){
		if(source_dir_init() == false){
			mount_flag = 0;
			return -1;
		}
	}	

	memcpy(mount_dir,source_dir,sizeof(mount_dir));
	
	if(mount(mount_dir,TARGET_DIR,FILE_SYSTEM,0,NULL) < 0)
	{
		perror("error:");
		if(errno == EBUSY)
		{
			if(0 == checkismount(mount_dir,TARGET_DIR))
			{
				printf("mounted already\n");
			}
			else 
			{
				#if 0
				int ret;
				memset(buf,0,40);
				sprintf(buf,"umount -fl %s \n",TARGET_DIR);
				ret = system(buf);	
				_printd("umount ret[%d]");
				#if 0
				if(0 != ret)
				{
					ret = system("umount -fl /tmp/SDdir\n");	
					_printd("umount ret[%d]");
				}
				#endif
				#else
				_printd("file_umount_all!!!!");
				file_umount_all(source_dir);
				#endif
				if(mount(mount_dir,TARGET_DIR,FILE_SYSTEM,0,NULL) < 0)
				{
					mount_flag = 0;
					_printd("mount error\n");
					return -1;
				}
			}	
		}
		#if 1
		else if(errno == EINVAL)
		{
			#if 1
			if (check_tf_valid(mount_dir) >= 0)
			{
				memset(buf,0,sizeof(buf));
				snprintf(buf,sizeof(buf),"/app/bin/mkfs.vfat %s",mount_dir);	
				int ret;
				ret = system(buf);
				if(ret >= 0 && ret != 127 &&
				   mount(mount_dir,TARGET_DIR,FILE_SYSTEM,0,NULL) < 0)
				{
					mount_flag = 0;
					return -1;
				}
			}
			else
			{
				mount_flag = 0;
				return -1;				
			}
				
			#else
			//用不了直接格式化  也可以在此发送提示格式化
			//新的64G 卡要此格式化/dev/mmcblk0p1
			//不存在/dev/mmcblk0p1 则认为是坏卡，不格式化
			#if 1
			if(0 == strncmp(mount_dir,"/dev/mmcblk0p1",20))
			{
				memset(buf,0,sizeof(buf));
				snprintf(buf,sizeof(buf),"mkfs.vfat %s",mount_dir);
				
				int ret;
				ret = system(buf);
				if(ret >= 0 && ret != 127 &&
				   mount(mount_dir,TARGET_DIR,FILE_SYSTEM,0,NULL) < 0)
				{
					mount_flag = 0;
					return -1;
				}
			}
			#endif
			perror("error ");
			mount_flag = 0;
			return -1;
			#endif
		}
		#endif
		else
		{
			perror("error ");
			mount_flag = 0;
			return -1;
		}
	}
	else
	{
		checkismount(mount_dir,TARGET_DIR);
	}
	_printd("create_dir");
	create_dir(TARGET_DIR);

	if(0 == init_pid && 0 == pthread_create(&init_pid,NULL,thread_init_process,NULL)){
		printf("thread create success\n");
	}
	else{
		printf("thread create fail\n");	
	}
	
	printf("mount success\n");

	mount_flag = 1;
	g_DelFileFlag = 0;
	return 0;
}
//static CMutex umount_mutex;
int file_sys_deinit()
{
	int ret = 0;
	
	CGuard m_Cguard(mount_mutex);

	//printf("mount_flag:%d\n",mount_flag);
	
	if( 2 == mount_flag || 3 == mount_flag )
		return -1;
	
	if(1 == mount_flag && umount2(TARGET_DIR,MNT_DETACH) < 0)
	{		
		char buf[128] = {0};
		if(source_dir[0] != 0 && source_dir != NULL && source_dir[0] != '\0')
		{
#if 0
			sprintf(buf,"umount -fl %s",TARGET_DIR);
			_printd("%s\n",buf);
			ret = system(buf);
			if(0 != ret)
			{
				perror("umount::");
				ret = system("umount -fl /tmp/SDdir");
				if(ret == 127)
				{
					perror("umount::");	
				}
			}
			if(ret == 127) ret = -1;
#else
			_printd("file_umount_all!!!!");
			file_umount_all(source_dir);

#endif
		}
	}
		
	mount_flag = 0;
	memset(source_dir,0,sizeof(source_dir));
	
	return ret;
}
int file_umount_all(char* pSourceNode)
{
	FILE  *fp   = NULL;
	char  strDevName[48] = {0};
	char  strDirName[48] = {0};
	char  strFileType[16]= {0};
	char  strMountParam[255] = {0};
	int   other[2] = {};
	char  buf[255] = {0};
	int   ret = 0;

	if( NULL == pSourceNode){
		return -1;
	}

	if (NULL == (fp = fopen("/proc/mounts", "r"))){
		return -1;
	}
	
	while(!feof(fp))
	{
		if(NULL == fgets((char *)buf, 254, fp))
			break;
		
		sscanf(buf, "%s %s %s %s %d %d" , strDevName, 
			strDirName, strFileType, strMountParam, &other[0], &other[1]);
		if (0 == strcmp(pSourceNode, strDevName)) 
		{
			printf("umount dir:%s\n",strDirName);
			if( umount2(strDirName,MNT_DETACH) < 0)
			{		
				char cmd[256] = {0};
				snprintf(cmd,sizeof(cmd),"umount -fl %s",strDirName);
				ret = system(cmd);
				_printd("umount ret[%d]",ret);				
				if(0 != ret){
					perror("umount::");
				}
			}
		}
	}
	
	fclose(fp);
	
	return 0;	
}
int GetPidByName(char* task_name)
{
	CHAR   buf[64] = {0};
	INT    Fd      = -1;
	pid_t  pid     =  0;

	INT    nRet    = FALSE;
	DIR  * pDir    = NULL;
	struct dirent *pDirent  = NULL;
	CHAR   filename[100]    = {0};
	CHAR   pProgramName[64] = {0};
	INT    i,j;
	
	if (NULL == (pDir=opendir("/proc"))){
		printf("Open /proc Error!");
		return FALSE;
	}

	snprintf(pProgramName,sizeof(pProgramName),"%s/",task_name);
	while (NULL != (pDirent=readdir(pDir))){
		if ((pid=atoi(pDirent->d_name))>0){
			memset(filename, 0, sizeof(filename));
			snprintf(filename,sizeof(filename),"/proc/%u/task/%u/cmdline", pid, pid);

			if (0>(Fd=open(filename, O_RDONLY)))
				continue;

			memset(buf, 0, sizeof(buf));
			if (0 >= (j=read(Fd, buf, sizeof(buf)))){
				close(Fd);
				continue;
			}
			close(Fd);
		//	printf("filename:%s\n",filename);
			for(i = 0 ;i<j;i++)
			{
			//	printf("%c,",buf[i]);
				if(i != j-1 && buf[i]==0)
					buf[i]=0xff;
			}
			//printf("\n");
			if (strstr(buf, task_name)){
				if(NULL == strstr(buf, pProgramName))
				{
					nRet = TRUE;
					break;
				}
				
			}
		}
	}
	closedir(pDir);

	if(nRet == TRUE)
		return (int)pid;
	else
		return -1;
}

int file_sys_format()
{
	int ret 		= -1;
	int pid			= 0;
	char fmtdir[20]	= {0}; 
	CGuard m_Cguard(mount_mutex);

	if(file_sys_get_linkstat() < 0)		return -1;

	g_DriverManager.DetectEnable(FALSE);
	force_umount = 1;
//	file_sys_deinit();

	mount_flag = 3;

	pid = GetPidByName("FAC_VERSION_CHECK.sh");
	if(pid > 0){
		printf("pid:%d\n",pid);
		kill( pid,SIGKILL );
	}

	if(source_dir_init() == true)
	{
		memcpy(fmtdir,source_dir,sizeof(fmtdir));
		file_umount_all(fmtdir);
		printf("Format %s Start......\n",fmtdir);
		ret = mkfs_vfat(fmtdir);
		printf("Format %s End......\n",fmtdir);
	}
	
	mount_flag 		= 0;
	force_umount 	= 0;
	memset(source_dir,0,sizeof(source_dir));
	g_DriverManager.DetectEnable(TRUE);
	
	return ret;
}
/******************************get_link_state******************************/
static bool linked = false;

static bool get_sd_status()
{
	if(source_dir[0] == 0 || source_dir == NULL || source_dir[0] == '\0'){
		if(source_dir_init() == false){
			return false;
		}
	}	

	if(0 != access(source_dir,F_OK))
	{
		_printd("file_sys_deinit...%s\n",source_dir);
		file_sys_deinit();		
	}

	//printf("source_dir:%s\n",source_dir);
	
	return true;
}
void* thread_read_linkstat(void* arg)
{
	pthread_detach(pthread_self());
	SET_THREAD_NAME("thread_read_linkstat");

	while(1)
	{
		linked = get_sd_status();
		sleep(1);
	}
}
static CMutex link_mutex;
int file_sys_get_linkstat()
{		
	CGuard m_Cguard(link_mutex);

	static pthread_t pid 	= 0;
	static int    pid_flag  = 0;
	
	if(pid_flag == 0)
	{
		printf("thread create\n");
		if(pthread_create(&pid,NULL,thread_read_linkstat,NULL))
		{
			printf("thread fail\n");
			pid_flag = 0;
		}
		else
		{
			printf("thread success\n");
			pid_flag = 1;
			//sleep(1);//等待连接结果出来
			linked = get_sd_status();
		}
	}
	
	if(linked == true)
		return 0;
	else
		return -1;

}
/******************************get_sd_capacity******************************/
static bool IsTimeOut(uint uiTimeStart,uint uiTimeNow,uint uiTimeOut)
{ 
    /* 没有越界 */
    if (0xFFFFFFFF - uiTimeStart > uiTimeOut)
    {
        if (uiTimeStart + uiTimeOut < uiTimeNow) 
        {
            return true;
        }
        else if (uiTimeNow < uiTimeStart)
        {
            return true;
        }
    }
    /* 越界 */
    else if ((uiTimeOut - (0xFFFFFFFF - uiTimeStart) < uiTimeNow)&& ( uiTimeNow < uiTimeStart))
    {
        return true;
    }
    return false;
}

static uint SystemGetMS()
{
	uint t;
	t = (times(NULL) & 0xffffffff) * 10;
	return	t;
}
static CMutex getcap_mutex;
int get_sdcard_cap(int* total,int* free)
{
	if(file_sys_init() < 0)
	{
		if(total != NULL)	*total = 0;
		if(free != NULL)	*free  = 0;
		
		return -1;
	}
	
	static uint   start_time = 0;
	static int    f_total 	 = 0;
	static int    f_free 	 = 0;
	struct statfs sdInfo;
	
	if(f_total == 0)
	{
		if(statfs(TARGET_DIR,&sdInfo) < 0)
			return -1;
		
		f_total = (sdInfo.f_blocks / 1024.0) * (sdInfo.f_bsize);
		f_free  = (sdInfo.f_bfree / 1024.0)  * (sdInfo.f_bsize);//KB为单位
	}

	if(start_time == 0)
		start_time = SystemGetMS();
	else
	{
		uint now_time = SystemGetMS();
		//至少 100s 才读取一次,非人为操作 100s不会出现大量消耗容量
		if(IsTimeOut(start_time,now_time,100*1000) == true)
		{	
		
			start_time = now_time;

			if(statfs(TARGET_DIR,&sdInfo) < 0)
				return -1;
			else
			{
				f_total = (sdInfo.f_blocks / 1024.0) * (sdInfo.f_bsize);
				f_free  = (sdInfo.f_bfree / 1024.0)  * (sdInfo.f_bsize);//KB为单位
			}
		}
	}
	if(total != NULL)
		*total = f_total;
	if(free != NULL)
		*free  = f_free;

	return 0;
}
int file_sys_get_cap(int* total,int* free)
{
	CGuard m_Cguard(remove_mutex);

	return get_sdcard_cap(total,free);
}
int get_sdcard_cap_bit(int64_t* total,int64_t* free)
{
	if(file_sys_init() < 0)
	{
		if(total != NULL)	*total = 0;
		if(free != NULL)	*free  = 0;
		
		return -1;
	}
	
	static uint   start_time = 0;
	static int64_t    f_total 	 = 0;
	static int64_t    f_free 	 = 0;
	struct statfs sdInfo;
	
	if (statfs(TARGET_DIR, &sdInfo) < 0)
		return -1;

	f_total = (int64_t)sdInfo.f_blocks * (int64_t)sdInfo.f_bsize;
	f_free = (int64_t)sdInfo.f_bfree  * (int64_t)sdInfo.f_bsize;//bit为单位

	//if(f_total == 0)
	//{
	//	if(statfs(TARGET_DIR,&sdInfo) < 0)
	//		return -1;
	//	
	//	f_total = (int64_t)sdInfo.f_blocks * (int64_t)sdInfo.f_bsize;
	//	f_free  = (int64_t)sdInfo.f_bfree  * (int64_t)sdInfo.f_bsize;//bit为单位
	//}

	//if(start_time == 0)
	//	start_time = SystemGetMS();
	//else
	//{
	//	uint now_time = SystemGetMS();
	//	//至少 100s 才读取一次,非人为操作 100s不会出现大量消耗容量
	//	if(IsTimeOut(start_time,now_time,100*1000) == true)
	//	{	
	//	
	//		start_time = now_time;

	//		if(statfs(TARGET_DIR,&sdInfo) < 0)
	//			return -1;
	//		else
	//		{
	//			f_total = (int64_t)sdInfo.f_blocks * (int64_t)sdInfo.f_bsize;
	//			f_free	= (int64_t)sdInfo.f_bfree  * (int64_t)sdInfo.f_bsize;//bit为单位
	//		}
	//	}
	//}
	if(total)
		*total = f_total;
	if(free)
		*free  = f_free;

	return 0;
}
int file_sys_get_cap_bit(int64_t* total,int64_t* free)
{
	CGuard m_Cguard(remove_mutex);

	return get_sdcard_cap_bit(total,free);
}

bool file_sys_is_full()
{
	int Total,Free;

	Total = 0;
	Free  = 0;

	if(get_sdcard_cap(&Total,&Free) < 0)
		return true;
	if(Free < ISFULL_CAPACITY)
	{
		printf("IS FULL ,Free:%d\n",Free);
		return true;
	}
	return false;
}
/******************************get_file_list******************************/

int Comp_SmallSort(const void*p1,const void*p2)
{
	return strcmp(((FilePath*)p1)->FileName,((FilePath*)p2)->FileName);
}
int Comp_BigSort(const void*p1,const void*p2)
{
	return strcmp(((FilePath*)p2)->FileName,((FilePath*)p1)->FileName);
}
static int filter_videolist(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".")==0  || strcmp(dp->d_name,"..")==0)  
    	return 0; 
	  
	int size = strlen(dp->d_name);      
	
    if(size < 20)		return 0;  
                                                    //只处理.mp4扩展名的文件名  
	if(strcmp( ( dp->d_name + (size - 4) ) , ".mp4") != 0&&
	   strcmp( ( dp->d_name + (size - 4) ) , ".avi") != 0) 
   	    return 0; 

	if(strchr(dp->d_name,'_') == NULL)
		return 0; 
	if(dp->d_name[14] != '_' && dp->d_name[19] != '_')
		return 0; 	

	return 1;
}

int filter_date_ymd(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".")==0  || strcmp(dp->d_name,"..")==0)  
    	return 0; 

	if(DT_DIR != dp->d_type)
		return 0; 

	if(strlen(dp->d_name) < 8 )
		return 0;
	
	if(dp->d_name[4] != '-' && dp->d_name[7] != '-')
		return 0; 	

	return 1;
}
int file_get_eardir_videolist(char* dirPath,
	VideoFiles *list,
	int queryNum,
	int(*Comp)(const void*p1,const void*p2))
{
	
    DIR *dir;
	struct dirent *ptr;

	char DayDir[128]	= {0};
	char tgrtDir[128]	= {0};

	int maxNum	= FILENUMS;

	int	tmpCnt	=	0;
	
	int	i=0,n=0;
	int d,h;
	int h_max = 23;

//正常一个小时内产生的文件数
	int normalFileNums = ((60*60) /INTERVAL_VIDEO) + 10;
		
	if(dirPath == NULL || access(dirPath,F_OK) != 0)
		return -1;

	struct dirent **filelist = NULL;

	_printd("normalFileNums:%d\n",normalFileNums);	

	n = scandir(dirPath,&filelist,filter_date_ymd,alphasort);

	if(n <= 0){	
		free(filelist);
		_printd("file num:[%d]\n",n);	
		return -1;
	}

	maxNum = queryNum >= FILENUMS ? (FILENUMS-1):queryNum;
	
	memset(list,0,sizeof(VideoFiles));

	
	for(d=0; d < n; d ++)
	{	
		int dayfilecount =0;
		memset(DayDir,0,sizeof(DayDir));
		snprintf(DayDir,sizeof(DayDir),"%s/%s",dirPath,
				filelist[d]->d_name);
		_printd("DayDir:%s",DayDir);
		if( 0 != access(DayDir,F_OK))
		{
			continue;
		}
		for(h = 0; h <= h_max; h ++)
		{
			int hourfilecount = 0;
			memset(tgrtDir,0,sizeof(tgrtDir));
			snprintf(tgrtDir,sizeof(tgrtDir),"%s/%s/%02d",dirPath,
				filelist[d]->d_name,h);

			if( 0 != access(tgrtDir,F_OK)){
				continue;
			}
			dayfilecount++;  //每天文件夹下子文件个数
//			_printd("tgrtDir:%s\n",tgrtDir);
			dir = opendir(tgrtDir);
			if (NULL != dir)
			{
				tmpCnt = list->FilesCount;
				
				while ((ptr = readdir(dir)) != NULL)
				{
					
					if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
						continue;
					if (DT_DIR != ptr->d_type)
					{
						hourfilecount++; //小时下文件夹个数
//						_printd("ptr->d_name:%s\n",ptr->d_name);

						if(hourfilecount > normalFileNums){ //大于normalFileNums证明该文件夹出异常问题
							_printd("error dir:%s\n",tgrtDir);
							break;
						}
						
						if(!filter_videolist(ptr))
						{
							continue;
						}
						if((list->FilesCount < maxNum) && (list->FilesCount < FILENUMS))
						{
							strncpy(list->FilesList[list->FilesCount].FileName,ptr->d_name,128);
							strncpy(list->FilesList[list->FilesCount].DirPath,tgrtDir,128);
							list->FilesCount ++;
						}
						else
						{ 	
							break;
						}
							
					}
						
					
				}			
				closedir(dir);

			}
			//hourfilecount:文件夹为空则删除
			if(0 == hourfilecount && rmdir(tgrtDir) == -1) 
			{
				checkismount(source_dir,TARGET_DIR);
				_printd("rmdir error for %s\n", tgrtDir);						
			}

			if(list->FilesCount >= maxNum)
			{
					goto QUERY_EXIT;
			}
		}
		_printd("dayfilecount[%d]",dayfilecount);
		if(0 == dayfilecount && rmdir(DayDir) == -1) 
		{
			checkismount(source_dir,TARGET_DIR);
			_printd("rmdir error for %s\n", DayDir);						
		}
	}
		
	

	qsort(list->FilesList,list->FilesCount,sizeof(list->FilesList[0]),Comp);
	
QUERY_EXIT:	
#if 0
	for(i = 0 ; i < list->FilesCount ; i++)
	{
		_printd("[%d]%s/%s\n",i,
		list->FilesList[i].DirPath,
		list->FilesList[i].FileName);
	}
#endif	
	if(filelist)
	{
		for(i = 0 ; i < n ; i++)
		{
			free(filelist[i]);
		}
		free(filelist);
	}
	return list->FilesCount;
}

#if 1
int file_get_dir_videolist(char* dirPath,
	VideoFiles *list,
	int queryNum,
	SYSTEM_TIME* startTime,
	SYSTEM_TIME* endTime  ,
	int(*Comp)(const void*p1,const void*p2))
{
    DIR *dir;
	struct dirent *ptr;

	char DayDir[128]	= {0};
	char tgrtDir[128]	= {0};
	char sTime[128]	= {0};
	char eTime[128]	= {0};

	int maxNum	= FILENUMS;
//正常一个小时内产生的文件数 6   异常最多给产生512个
	int normalFileNums = 1000;

	int	tmpCnt	=	0;
	VideoFiles tmpList  = {0};
	SYSTEM_TIME start_time,end_time;
	
	int	i = 0;
	int n = 0;
	int y,m,d,h;
	int m_max,d_max,h_max;
	
	if(dirPath == NULL || access(dirPath,F_OK) != 0)
		return -1;
		
	if( NULL == startTime || NULL == endTime ||
		startTime->year <= 1970 || endTime->year <= 1970)
	{
		struct dirent **filelist = NULL;

		n = scandir(dirPath,&filelist,filter_date_ymd,alphasort);

		if(n <= 0)
		{	
			if(filelist)
			{
				free(filelist);
			}
			printf("file num:[%d]\n",i);	
			return -1;
		}

		memset(&start_time,0,sizeof(start_time));
		memset(&end_time  ,0,sizeof(end_time));

		sscanf(filelist[0]->d_name,"%04d-%02d-%02d",
		&start_time.year,&start_time.month,&start_time.day);

		sscanf(filelist[n-1]->d_name,"%04d-%02d-%02d",
		&end_time.year,&end_time.month,&end_time.day);

		end_time.hour 	= 23;
		end_time.minute	= 59;
		end_time.second	= 59;	
		_printd("start path:%s",filelist[0]->d_name);
		_printd("end   path:%s",filelist[n-1]->d_name);
		if(filelist)
		{
			for(i = 0 ; i < n ; i++)
			{
				free(filelist[i]);
			}
			free(filelist);
		}
	}
	if( NULL != startTime && startTime->year > 1970){
		snprintf(sTime,sizeof(sTime),"%04d%02d%02d%02d%02d%02d", startTime->year,startTime->month,startTime->day,
			startTime->hour,startTime->minute,startTime->second);
		memcpy(&start_time,startTime,sizeof(start_time));
	}	
	
	if( NULL != endTime ){
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", endTime->year,endTime->month,endTime->day,
			endTime->hour,endTime->minute,endTime->second);
		memcpy(&end_time,endTime,sizeof(end_time));
	}
	else{
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", end_time.year,end_time.month,end_time.day,
			end_time.hour,end_time.minute,end_time.second);
	}
	printf("start:%s,end:%s\n",sTime,eTime);

	if(list == NULL)	return -1;

	maxNum = queryNum >= FILENUMS ? FILENUMS:queryNum;
	
	memset(list,0,sizeof(VideoFiles));

#if 1
	_printd("start_time %04d-%02d-%02d %02d:%02d:%02d",start_time.year ,start_time.month,
		start_time.day ,start_time.hour,start_time.minute,
		start_time.second);	
	_printd("end_time %04d-%02d-%02d %02d:%02d:%02d",end_time.year ,end_time.month,
		end_time.day ,end_time.hour,end_time.minute,
		end_time.second);			
#endif	
	for(y = start_time.year; y <= end_time.year; y ++){
//正常情况不会产生该时间的录像，直接跳过
		if(y > 1970 && y < 2018 ){
			y = 2018;
		}
		
		m_max = (y != end_time.year)? 12  :  end_time.month;
		m 	  = (y != start_time.year)? 1 :  start_time.month;
		for(; m <= m_max; m ++){
			if(y == start_time.year && m == start_time.month){
				d = start_time.day;
			}
			else{
				d = 1;
			}

			if(y == end_time.year && m == end_time.month){
				d_max = end_time.day;
			}
			else{
				d_max = 31;
			}
			
			for(; d <= d_max; d ++)
			{
				int dayfilecount = 0;
				if(y == start_time.year &&
				   m == start_time.month&&
				   d == start_time.day){
					h	= start_time.hour;
				}else{
					h 	= 0;
				}
				
				
				if(y == end_time.year &&
				   m == end_time.month&&
				   d == end_time.day){
					h_max 	= end_time.hour;
				}else{
					h_max 	= 23;
				}
				
				memset(DayDir,0,sizeof(DayDir));
				snprintf(DayDir,sizeof(DayDir),"%s/%04d-%02d-%02d",dirPath,
						y,m,d);

				if( 0 != access(DayDir,F_OK))
				{
					continue;
				}
				_printd("DayDir:%s",DayDir);
				for(; h <= h_max; h ++){
					int hourfilecount = 0;
					memset(tgrtDir,0,sizeof(tgrtDir));
					snprintf(tgrtDir,sizeof(tgrtDir),"%s/%04d-%02d-%02d/%02d",dirPath,
						y,m,d,h);
						
					if( 0 != access(tgrtDir,F_OK))
					{
						continue;
					}
					dayfilecount++;  //每天文件夹下子文件个数
#if 0
					dir = opendir(tgrtDir);
					if (NULL != dir)
					{
						_printd("tgrtDir:%s\n",tgrtDir);
						tmpCnt = list->FilesCount;
						
				        while ((ptr = readdir(dir)) != NULL )
						{
							//printf("ptr->d_name:%s\n",ptr->d_name);
							if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
								continue;
							
							hourfilecount++; //小时下文件夹个数
							if(hourfilecount > normalFileNums){
								_printd("error dir:%s",tgrtDir);
								break;
							}
								
				            if (DT_DIR != ptr->d_type)
							{
								if(!filter_videolist(ptr)){
									continue;
								}

								if(strncmp(ptr->d_name,sTime,strlen(sTime)) >= 0 &&
								   strncmp(ptr->d_name,eTime,strlen(eTime)) <= 0)
								{
									if((list->FilesCount < maxNum) && (list->FilesCount< FILENUMS)){
										strncpy(list->FilesList[list->FilesCount].FileName,ptr->d_name,128);
										strncpy(list->FilesList[list->FilesCount].DirPath,tgrtDir,128);

										list->FilesCount ++;
									}
									else{ 	//剩下的容量放不下
											if(tmpList.FilesCount < FILENUMS)
											{
												strncpy(tmpList.FilesList[tmpList.FilesCount].FileName,ptr->d_name,128);
												strncpy(tmpList.FilesList[tmpList.FilesCount].DirPath,tgrtDir,128);
			
												tmpList.FilesCount ++;
											}
									}
									
								}
								
							}
				        }
						
						if( tmpList.FilesCount > 0)
						{
							//汇合该级目录所读到的文件 
							for(i = 0 ; i < maxNum - tmpCnt ; i++)
							{
								if(tmpList.FilesCount < maxNum)
									memcpy(&(tmpList.FilesList[tmpList.FilesCount++]),&(list->FilesList[tmpCnt+i]),sizeof(FilePath));
							}
							
							qsort(tmpList.FilesList,tmpList.FilesCount,sizeof(tmpList.FilesList[0]),Comp);
							
							for(i = 0 ; i < maxNum - tmpCnt ; i++)
							{
								memcpy(&(list->FilesList[tmpCnt+i]),&(tmpList.FilesList[i]),sizeof(FilePath));
								/*
								printf("[%d]%s/%s\n",tmpCnt+i,
											list->FilesList[tmpCnt+i].DirPath,
											list->FilesList[tmpCnt+i].FileName);
											*/
							}

							memset(&tmpList,0,sizeof(tmpList));
						}

						closedir(dir);

					}
#else
					struct dirent **namelist;
					int ncount;
					ncount = scandir(tgrtDir, &namelist, 0, alphasort);
					//dir = opendir(tgrtDir);
					//if (NULL != dir)
					_printd("ncount:%d", ncount);
					if (ncount > 0)
					{
						_printd("tgrtDir:%s\n", tgrtDir);
						tmpCnt = list->FilesCount;
						int index = 0;
						while (index < ncount)						
						{
							//_printd("ptr->d_name:%s\n", namelist[index]->d_name);
							if ((strcmp(namelist[index]->d_name, ".") == 0) || (strcmp(namelist[index]->d_name, "..") == 0))
							{
								free(namelist[index]);
								index++;
								continue;
							}
							hourfilecount++; //小时下文件夹的文件个数
							if (hourfilecount > normalFileNums){
								_printd("error dir:%s", tgrtDir);
								break;
							}

							if (DT_DIR != namelist[index]->d_type)
							{
								if (!filter_videolist(namelist[index])){
									continue;
								}

								if (strncmp(namelist[index]->d_name, sTime, strlen(sTime)) >= 0 &&
									strncmp(namelist[index]->d_name, eTime, strlen(eTime)) <= 0)
								{
									if ((list->FilesCount < maxNum) && (list->FilesCount< FILENUMS)){
										strncpy(list->FilesList[list->FilesCount].FileName, namelist[index]->d_name, 128);
										strncpy(list->FilesList[list->FilesCount].DirPath, tgrtDir, 128);

										list->FilesCount++;
									}
									else
									{ 	//剩下的容量放不下
										if (tmpList.FilesCount < FILENUMS)
										{
											strncpy(tmpList.FilesList[tmpList.FilesCount].FileName, namelist[index]->d_name, 128);
											strncpy(tmpList.FilesList[tmpList.FilesCount].DirPath, tgrtDir, 128);

											tmpList.FilesCount++;
										}
									}

								}

							}
							free(namelist[index]);
							index++;
						}						
					

					}
#endif
					//hourfilecount:文件夹为空则删除
					if(0 == hourfilecount && -1 == rmdir(tgrtDir)) 
					{
						checkismount(source_dir,TARGET_DIR);
						_printd("rmdir error for %s\n", tgrtDir);						
					}

					if(list->FilesCount >= maxNum)
					{
							goto QUERY_EXIT;
					}
				}
				//dayfilecount:文件夹为空则删除
				if(0 == dayfilecount && -1 == rmdir(DayDir)) 
				{
					checkismount(source_dir,TARGET_DIR);
					_printd("rmdir error for %s\n", DayDir);						
				}
			}
		}
	}

	qsort(list->FilesList,list->FilesCount,sizeof(list->FilesList[0]),Comp);
	
QUERY_EXIT:	
#if 0
	for(i = 0 ; i < list->FilesCount ; i++)
	{
		_printd("[%d]%s/%s\n",i,
		list->FilesList[i].DirPath,
		list->FilesList[i].FileName);
	}
#endif	

	return list->FilesCount;
}

#else
int file_get_dir_videolist(char* dirPath,
	VideoFiles *list,
	int queryNum,
	SYSTEM_TIME* startTime,
	SYSTEM_TIME* endTime  ,
	int(*Comp)(const void*p1,const void*p2))
{
    DIR *dir;
	struct dirent *ptr;

	char tgrtDir[128]	= {0};
	char sTime[128]	= {0};
	char eTime[128]	= {0};

	int maxNum	= FILENUMS;

	int	tmpCnt	=	0;
	VideoFiles tmpList  = {0};
	SYSTEM_TIME start_time,end_time;
	
	int	i,index;
	int y,m,d,h;
	int m_max,d_max,h_max;
	
	if(dirPath == NULL || access(dirPath,F_OK) != 0)
		return -1;
		
	if( NULL == startTime || NULL == endTime ||
		startTime->year <= 1970 || endTime->year <= 1970)
	{
		struct dirent **filelist = NULL;

		i = scandir(dirPath,&filelist,filter_date_ymd,alphasort);

		if(i <= 0){	
			free(filelist);
			printf("file num:[%d]\n",i);	
			return -1;
		}

		memset(&start_time,0,sizeof(start_time));
		memset(&end_time  ,0,sizeof(end_time));

		sscanf(filelist[0]->d_name,"%04d-%02d-%02d",
		&start_time.year,&start_time.month,&start_time.day);

		sscanf(filelist[i-1]->d_name,"%04d-%02d-%02d",
		&end_time.year,&end_time.month,&end_time.day);

		end_time.hour 	= 23;
		end_time.minute	= 59;
		end_time.second	= 59;	

		index = 0;
		while(index < i){		
			free(filelist[index++]);
		}
		free(filelist);
	}
	if( NULL != startTime && startTime->year > 1970){
		snprintf(sTime,sizeof(sTime),"%04d%02d%02d%02d%02d%02d", startTime->year,startTime->month,startTime->day,
			startTime->hour,startTime->minute,startTime->second);
		memcpy(&start_time,startTime,sizeof(start_time));
	}	
	
	if( NULL != endTime ){
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", endTime->year,endTime->month,endTime->day,
			endTime->hour,endTime->minute,endTime->second);
		memcpy(&end_time,endTime,sizeof(end_time));
	}
	else{
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", end_time.year,end_time.month,end_time.day,
			end_time.hour,end_time.minute,end_time.second);
	}
	printf("start:%s,end:%s\n",sTime,eTime);

	if(list == NULL)	return -1;

	maxNum = queryNum > FILENUMS ? FILENUMS:queryNum;
	
	memset(list,0,sizeof(VideoFiles));

#if 0
	printf("start_time %d,%d,%d,%d,%d,%d\n",start_time.year ,start_time.month,
		start_time.day ,start_time.hour,start_time.minute,
		start_time.second);	
	printf("end_time %d,%d,%d,%d,%d,%d\n",end_time.year ,end_time.month,
		end_time.day ,end_time.hour,end_time.minute,
		end_time.second);			
#endif	
	for(y = start_time.year; y <= end_time.year; y ++){
//正常情况不会产生该时间的录像，直接跳过
		if(y > 1970 && y < 2018 ){
			y = 2018;
		}
		
		m_max = y < end_time.year? 12 :  end_time.month;
		m = y < end_time.year? 1 :  start_time.month;
		for(; m <= m_max; m ++){
			if(y >= end_time.year && m >= end_time.month){
				d		= start_time.day;
				d_max 	= end_time.day;
			}
			else{
				d	  = 1;
				d_max = 31;
			}
			for(; d <= d_max; d ++){
				if(y >= end_time.year &&
				   m >= end_time.month&&
				   d >= end_time.day){
				    h 		= start_time.hour;
					h_max 	= end_time.hour;
				}else{
					h		= 0;
					h_max 	= 23;
				}
				
				memset(tgrtDir,0,sizeof(tgrtDir));
				snprintf(tgrtDir,sizeof(tgrtDir),"%s/%04d-%02d-%02d",dirPath,
						y,m,d);
				//printf("tgrtDir:%s\n",tgrtDir);
				if( 0 != access(tgrtDir,F_OK)){
					continue;
				}
				for(; h <= h_max; h ++){
					
					memset(tgrtDir,0,sizeof(tgrtDir));
					snprintf(tgrtDir,sizeof(tgrtDir),"%s/%04d-%02d-%02d/%02d",dirPath,
						y,m,d,h);
					//printf("tgrtDir:%s\n",tgrtDir);
					dir = opendir(tgrtDir);
					if (NULL != dir)
					{
						tmpCnt = list->FilesCount;
						
				        while ((ptr = readdir(dir)) != NULL)
						{
							//printf("ptr->d_name:%s\n",ptr->d_name);
							if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
								continue;
				            if (DT_DIR != ptr->d_type)
							{
								if(!filter_videolist(ptr))
								{
									continue;
								}
								if(strncmp(ptr->d_name,sTime,strlen(sTime)) >= 0 &&
								   strncmp(ptr->d_name,eTime,strlen(eTime)) <= 0)
								{
									if(list->FilesCount < maxNum ){
										strncpy(list->FilesList[list->FilesCount].FileName,ptr->d_name,128);
										strncpy(list->FilesList[list->FilesCount].DirPath,tgrtDir,128);

										list->FilesCount ++;
									}
									else{ //剩下的容量放不下
										strncpy(tmpList.FilesList[tmpList.FilesCount].FileName,ptr->d_name,128);
										strncpy(tmpList.FilesList[tmpList.FilesCount].DirPath,tgrtDir,128);
	
										tmpList.FilesCount ++;
									}
									
								}
								
							}
				        }
						
						if( tmpList.FilesCount > 0)
						{
							//汇合该级目录所读到的文件 
							for(i = 0 ; i < maxNum - tmpCnt ; i++)
							{
								if(tmpList.FilesCount < maxNum)
									memcpy(&(tmpList.FilesList[tmpList.FilesCount++]),&(list->FilesList[tmpCnt+i]),sizeof(FilePath));
							}
							
							qsort(tmpList.FilesList,tmpList.FilesCount,sizeof(tmpList.FilesList[0]),Comp);
							
							for(i = 0 ; i < maxNum - tmpCnt ; i++)
							{
								memcpy(&(list->FilesList[tmpCnt+i]),&(tmpList.FilesList[i]),sizeof(FilePath));
								/*
								printf("[%d]%s/%s\n",tmpCnt+i,
											list->FilesList[tmpCnt+i].DirPath,
											list->FilesList[tmpCnt+i].FileName);
											*/
							}

							memset(&tmpList,0,sizeof(tmpList));
						}

						closedir(dir);

						}

						if(list->FilesCount >= maxNum){
							goto QUERY_EXIT;
					}
				}
			}
		}
	}

	qsort(list->FilesList,list->FilesCount,sizeof(list->FilesList[0]),Comp);
	
QUERY_EXIT:	
#if 0
	for(i = 0 ; i < list->FilesCount ; i++)
	{
		printf("[%d]%s/%s\n",i,
		list->FilesList[i].DirPath,
		list->FilesList[i].FileName);
	}
#endif	
	return list->FilesCount;
}
#endif
int file_get_all_videolist(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time)
{
	CGuard l_cGuard(remove_mutex);
	
	return file_get_dir_videolist(TIMER_DIR,list,FILENUMS, start_time,end_time, Comp_SmallSort);
}
int file_get_earlist_videolist(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time)
{
	CGuard l_cGuard(remove_mutex);
	return file_get_eardir_videolist(TIMER_DIR,list,1,Comp_SmallSort);
	//return file_get_dir_videolist(TIMER_DIR,list,FILENUMS,start_time,end_time, Comp_SmallSort);
}
int file_get_all_videolist_smallsort(VideoFiles *list, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time)//大序排列
{
	CGuard l_cGuard(remove_mutex);

	return file_get_dir_videolist(TIMER_DIR, list, FILENUMS, start_time, end_time, Comp_SmallSort);
}
int file_get_all_videolist_bigsort(VideoFiles *list,SYSTEM_TIME* start_time, SYSTEM_TIME* end_time)//大序排列
{
	CGuard l_cGuard(remove_mutex);
	
	return file_get_dir_videolist(TIMER_DIR,list,FILENUMS, start_time,end_time, Comp_BigSort);
}
int file_sys_getlist(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, uint type, uint *num, FILE_INFO *info, uint hint /* = 0 */, uint driver_type)
{
	int    n = 0;	
	char   str[128] = {0};
	VideoFiles List;
	CGuard l_cGuard(remove_mutex);
	char Path[64] = {0};
	
	if(access(VIDEO_DIR,F_OK) != 0)
	{
		*num = 0;
		return -1;
	}
	
	memset(&List,0,sizeof(VideoFiles));
	
	switch (type)
	{
		case 0:
				strcpy(Path,TIMER_DIR);
		break;
		case 1:
				strcpy(Path,ALARM_DIR);
		break;
		default:
				strcpy(Path,TIMER_DIR);
		break;
	}

	n = file_get_dir_videolist(Path,&List,FILENUMS,start_time,end_time, Comp_SmallSort);
	
	
	printf("List.FilesCount = %d \n",List.FilesCount);
#if 0	
	for(int z=0;z < n;z++)
	{
		printf("Filename[%d] = %s %s\n \n",z,List.FilesList[z].DirPath,List.FilesList[z].FileName);
	}
#endif	
	if(n <= 0)
	{	
		*num = 0;
		return -1;
	}

	int		i = 0;
	int 	Total,Free;
	int     year,mon,day,hour,min,sec;
	char    tmp[10];
	int     timelen,snum;
	struct tm Stime;
	struct tm p;  
	time_t t;

	struct  stat fbuf;

	*num = n>FILENUMS?FILENUMS:n;
	printf("file num:%d\n",*num);
	
	for(i = 0 ; i < *num ; i++)
	{

		memset(str,0,60);
		snprintf(str,sizeof(str),"%s/%s",List.FilesList[i].DirPath,List.FilesList[i].FileName);
		memcpy(info[i].base_name,List.FilesList[i].FileName,sizeof(info[i].base_name));	
		
		memset(&fbuf,0,sizeof(struct stat));
		stat(str,&fbuf);
		//printf("@@@@@@@@@@ %s,size:%0.2fkb\n",str,fbuf.st_size / 1024.0);	

		info[i].file_length = fbuf.st_size / 1024.0;
		info[i].channel     = 0;
		
		sscanf(List.FilesList[i].FileName,"%04d%02d%02d%02d%02d%02d_%d_%d.mp4",&year,&mon,&day,&hour,&min,&sec,&timelen,&snum);

		//printf("%d,%d,%d,%d,%d,%d,len:%d\n",year,mon,day,hour,min,sec,timelen);

		//文件格式  年(4)月(2)日(2)时(2)分(2)秒(2)_视频长度(分(2)秒(2))_序号.avi  
		//20170101230000_1001_0001.avi  代表视频开始时间2017-01-01  23:00:00  时长10分钟01秒
		
		info[i].start_time.year		= year - 2000;
		info[i].start_time.month	= mon;
		info[i].start_time.day		= day;
		info[i].start_time.hour		= hour;
		info[i].start_time.minute	= min;
		info[i].start_time.second	= sec;

		info[i].dur_time = 60*(timelen/100)+timelen%100;

		Stime.tm_year	= year - 1900;
		Stime.tm_mon	= mon - 1;
		Stime.tm_mday	= day;
		Stime.tm_hour	= hour;
		Stime.tm_min	= min;
		Stime.tm_sec	= sec;
		
		t = mktime(&Stime); 
	
		t += (timelen%100);
		t += (60*(timelen/100));
		if(NULL ==  gmtime_r(&t,&p)){
			memset(&p,0,sizeof(p));
		}
		info[i].end_time.year		= p.tm_year + 1900 - 2000;
		info[i].end_time.month		= p.tm_mon+1;
		info[i].end_time.day		= p.tm_mday;
		info[i].end_time.hour		= p.tm_hour;
		info[i].end_time.minute		= p.tm_min;
		info[i].end_time.second     = p.tm_sec;	
		
	}
	
	return 0;
}
int file_get_video_timelist(VideoTimeList *list,SYSTEM_TIME* startTime, SYSTEM_TIME* endTime)
{
	CGuard l_cGuard(remove_mutex);
	
  	DIR *dir = NULL;
	struct dirent *ptr = NULL;

	char DayDir[128]	= {0};
	char tgrtDir[128]	= {0};
	char sTime[128]		= {0};
	char eTime[128]		= {0};
	char dirPath[]		= TIMER_DIR;

	int	FileExistFlg	= 0;
	SYSTEM_TIME start_time,end_time;
	
	int	i = 0;
	int n = 0;
	int y,m,d,h;
	int m_max,d_max,h_max;

	int err_num = 0;
	
	if(access(dirPath,F_OK) != 0)
		return -1;
		
	if( NULL == startTime || NULL == endTime ||
		startTime->year <= 1970 || endTime->year <= 1970)
	{
		struct dirent **filelist = NULL;

		n = scandir(dirPath,&filelist,filter_date_ymd,alphasort);

		if(n <= 0){	
			if(filelist){
				free(filelist);
			}
			printf("file num:[%d]\n",i);	
			return -1;
		}

		memset(&start_time,0,sizeof(start_time));
		memset(&end_time  ,0,sizeof(end_time));

		sscanf(filelist[0]->d_name,"%04d-%02d-%02d",
		&start_time.year,&start_time.month,&start_time.day);

		sscanf(filelist[n-1]->d_name,"%04d-%02d-%02d",
		&end_time.year,&end_time.month,&end_time.day);

		end_time.hour 	= 23;
		end_time.minute	= 59;
		end_time.second	= 59;	
		_printd("start path:%s",filelist[0]->d_name);
		_printd("end   path:%s",filelist[n-1]->d_name);
		if(filelist){
			for(i = 0 ; i < n ; i++){
				free(filelist[i]);
			}
			free(filelist);
		}
	}
	if( NULL != startTime && startTime->year > 1970){
		snprintf(sTime,sizeof(sTime),"%04d%02d%02d%02d%02d%02d", startTime->year,startTime->month,startTime->day,
			startTime->hour,startTime->minute,startTime->second);
		memcpy(&start_time,startTime,sizeof(start_time));
	}	
	
	if( NULL != endTime ){
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", endTime->year,endTime->month,endTime->day,
			endTime->hour,endTime->minute,endTime->second);
		memcpy(&end_time,endTime,sizeof(end_time));
	}
	else{
		snprintf(eTime,sizeof(eTime),"%04d%02d%02d%02d%02d%02d", end_time.year,end_time.month,end_time.day,
			end_time.hour,end_time.minute,end_time.second);
	}
	printf("start:%s,end:%s\n",sTime,eTime);

	if(list == NULL)	return -1;
	
	memset(list,0,sizeof(VideoTimeList));

#if 1
	_printd("start_time %d,%d,%d,%d,%d,%d\n",start_time.year ,start_time.month,
		start_time.day ,start_time.hour,start_time.minute,
		start_time.second);	
	_printd("end_time %d,%d,%d,%d,%d,%d\n",end_time.year ,end_time.month,
		end_time.day ,end_time.hour,end_time.minute,
		end_time.second);			
#endif	
	for(y = start_time.year; y <= end_time.year; y ++){
//正常情况不会产生该时间的录像，直接跳过
		if(y > 1970 && y < 2018 ){
			y = 2018;
		}
		
		m_max = (y != end_time.year)? 12  :  end_time.month;
		m 	  = (y != start_time.year)? 1 :  start_time.month;
		for(; m <= m_max; m ++){
			if(y == start_time.year && m == start_time.month){
				d = start_time.day;
			}
			else{
				d = 1;
			}

			if(y == end_time.year && m == end_time.month){
				d_max = end_time.day;
			}
			else{
				d_max = 31;
			}
			
			for(; d <= d_max; d ++)
			{
				if(y == start_time.year &&
				   m == start_time.month&&
				   d == start_time.day){
					h	= start_time.hour;
				}else{
					h 	= 0;
				}
				
				
				if(y == end_time.year &&
				   m == end_time.month&&
				   d == end_time.day){
					h_max 	= end_time.hour;
				}else{
					h_max 	= 23;
				}
				
				FileExistFlg = 0;
					
				memset(DayDir,0,sizeof(DayDir));
				snprintf(DayDir,sizeof(DayDir),"%s/%04d-%02d-%02d",dirPath,
						y,m,d);

				if( 0 != access(DayDir,F_OK)){
					continue;
				}
				_printd("DayDir:%s",DayDir);
				for(; h <= h_max; h ++){
					memset(tgrtDir,0,sizeof(tgrtDir));
					snprintf(tgrtDir,sizeof(tgrtDir),"%s/%04d-%02d-%02d/%02d",dirPath,
						y,m,d,h);
						
					if( 0 != access(tgrtDir,F_OK)){
						continue;
					}

					dir = opendir(tgrtDir);
					if (NULL != dir){
						_printd("tgrtDir:%s\n",tgrtDir);

						err_num = 0;
						//如果文件数超过20则证明该目录异常，跳过该目录
				        while ((ptr = readdir(dir)) != NULL && err_num ++ < 20){
							//printf("ptr->d_name:%s\n",ptr->d_name);
							if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0)){
								continue;
							}
				            if (DT_DIR != ptr->d_type){
								if(!filter_videolist(ptr)){
									continue;
								}
								//查询有录像文件则代表该天有录像，即可跳出循环
								FileExistFlg = 1;
								_printd("file:%s",ptr->d_name);
								break;
							}
				        }
						if(err_num > 20){
							_printd("error dir:%s",tgrtDir);
						}
						closedir(dir);

					}

					if(FileExistFlg){
						break;
					}
				}

				if(FileExistFlg && list->TimeCount < TIMELISTNUMS){

					struct tm sTime			= {0};

					sTime.tm_year	= y - 1900;
					sTime.tm_mon	= m - 1;
					sTime.tm_mday	= d;
					sTime.tm_hour	= 0;
					sTime.tm_min	= 0;
					sTime.tm_sec	= 0;
								
					list->TimeList[list->TimeCount] = mktime(&sTime);

					_printd("TimeList[%d]:%lu\n",list->TimeCount,list->TimeList[list->TimeCount]);
					list->TimeCount ++;
				}
									
			}
		}
	}
	
	return list->TimeCount;
}
/******************************remove_file******************************/

static int remove_earlist_videofile(char* dirPath)
{
	
	int    i,n;	
	int    filecount = 10;
	char   str[256];

	VideoFiles  queryList = {0};

	int 	Total,Free;
	struct  stat fbuf;
	CGuard l_cGuard(remove_mutex);
		
	if(dirPath == NULL || access(dirPath,F_OK) != 0)
		return -1;
Reapeat:

	file_get_eardir_videolist(dirPath,&queryList, filecount,Comp_SmallSort);

	get_sdcard_cap(&Total,&Free);

	for(i = 0 ; i < queryList.FilesCount ; i++)
	{
		memset(str,0,256);
		snprintf(str,sizeof(str),"%s/%s",queryList.FilesList[i].DirPath,queryList.FilesList[i].FileName);
		
		memset(&fbuf,0,sizeof(struct stat));
		stat(str,&fbuf);

		#if 1
		if(remove(str) < 0)
		{
			_printd("remove error:%s",str);
			//system(cmd);
			//重新挂载为可读可写
			checkismount(source_dir,TARGET_DIR);
			continue;
		}
		g_DelFileFlag = 1;
		_printd("remove %s,size:%0.2fkb\n",queryList.FilesList[i].FileName,fbuf.st_size / 1024.0);	

		Free += (fbuf.st_size / 1024.0);	
		if(Free >= MIN_CAPACITY)
			break;
		#else
		printf("remove %s,size:%0.2fkb\n",queryList.FilesList[i].FileName,fbuf.st_size / 1024.0);	
		#endif
		
	}
	


	if(Free < MIN_CAPACITY)
	{
		filecount +=10;
		if(filecount < FILENUMS)
		goto Reapeat;
	}

	return 0;
}
int filter_waste(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".") == 0  || 
	   strcmp(dp->d_name,"..")== 0  ||
	   strcmp(dp->d_name,"list.txt") == 0)   
    	return 0; 	 
	
	int size = strlen(dp->d_name);                                              
   
	if(strcmp( ( dp->d_name + (size - 4) ) , ".mp4") == 0)
	{
		 //timer 下的文件命名规则
		 //过滤掉第一个字符不是 1或者2 的文件
		if( dp->d_name[14] == '_' &&
			dp->d_name[19] == '_' && 
			dp->d_name[0] > '0'   &&
			dp->d_name[0] < '3'){
			return 0;
		}
		//alarm 下的文件命名规则
		if(dp->d_name[16] == '_' &&
		   dp->d_name[31] == '_' ){
			return 0;
		}
	}
	
	return 1;	
}
//返回值为检索到的文件个数
int remove_rubbish(char *dirPath,int depth = 0)
{
    DIR *dir;
	struct dirent *ptr;
	char trgtpath[256]={0};
	char trgtdir[256] ={0};
	int	 filenums = 0;
	int  n 		  = 0;
	int  errcount = 0;
	
	if(dirPath == NULL){
		printf("NULL\n");
		return -1;
	}
	if(depth < 0 ) depth = 0;
	
	if(depth > 4)  return 1;  //如果该目录结构超过4层,则认为其有问题不作处理
	
    dir = opendir(dirPath);
	if (NULL != dir)
	{
        while ((ptr = readdir(dir)) != NULL)
		{	
            if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0))
				continue;
			
			//_printd("ptr:%s",ptr->d_name);
			
            if (DT_DIR != ptr->d_type)
			{
				filenums ++;
				
				if(!filter_waste(ptr)){
					continue;
				}
				
				snprintf(trgtpath,sizeof(trgtpath), "%s/%s", dirPath,ptr->d_name);
				
				_printd("remove trgtpath:%s\n",trgtpath);
				
				if(remove(trgtpath)<0)
				{
					char cmd[256];
					snprintf(cmd,sizeof(cmd),"rm -f %s",trgtpath);
					_printd("error:%s",cmd);
					//system(cmd);
					errcount++;
					if(errcount > 20)
					{
						_printd("SD error!!!!!!!\n");
						break;
					}
				}
				else
				{
					errcount = 0;
				}
							
				continue;
			}
			else
			{
			//	snprintf(trgtdir,sizeof(trgtdir), "%s/%s", dirPath,ptr->d_name);
			//	n = remove_rubbish(trgtdir,depth+1);
				
			//	if( n > 0 ){
			//		filenums += n;
			//	}				
			}
			
			
        }
		closedir(dir);

		if(filenums <= 0){
			if( 0 != strcmp(dirPath,VIDEO_DIR) &&
				0 != strcmp(dirPath,TIMER_DIR) && 
				0 != strcmp(dirPath,ALARM_DIR) &&
				0 != strcmp(dirPath,RECEXT_DIR)){  
				_printd("remove trgtdir:%s\n",dirPath);
		
				if(rmdir(dirPath)<0){
					char cmd[256];

					snprintf(cmd,sizeof(cmd),"rm -rf %s",dirPath);
					_printd("error:%s",cmd);
					//system(cmd);
				}
			}			
			
		}
	}	

	return filenums;
}
static void remove_waste_file()
{
	//上电只删除一次无关文件否则SD卡
	//出部分文件夹出问题时会不录像
	static int times = 0;
	CGuard l_cGuard(remove_mutex);

	if(0 == times){
		remove_rubbish(VIDEO_DIR);
		times = 1; 
	}
//	remove_rubbish(TIMER_DIR);
//	remove_rubbish(ALARM_DIR);
//	remove_rubbish(IMAGE_DIR);
}
static void clear_file()
{
	int Total,Free;

	remove_waste_file();
		
	get_sdcard_cap(&Total,&Free);

	if(Free < MIN_CAPACITY)
	{
		remove_earlist_videofile(TIMER_DIR);
	}
}


/******************************************************************/
static int filter_frag(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".")==0  || strcmp(dp->d_name,"..")==0)  
    	return 0; 	 

	if(DT_DIR == dp->d_type )  
		return 0;
	
	int size = strlen(dp->d_name); 

	if(strcmp( ( dp->d_name + (size - 4) ) , ".mp4") != 0)  
   	    return 0; 
	
                                             
    //因为自身命名规则的名字为18字节
    //过滤掉第一个字符不是 1或者2 的文件
	if(size != 18 || dp->d_name[0] > '2')
		return 0; 	

	return 1;	
}

static void recover_recfile(char *dirpath)
{
	CGuard l_cGuard(remove_mutex); //恢复文件过程中不允许删除文件
	
	if(dirpath == NULL || access(dirpath,F_OK) != 0)
		return;
	
	int    i= 0, n = 0 ;	
	char   str[60] = {0};
	char   strcahche[60] = {0};
	struct dirent **filelist = NULL;

	n = scandir(dirpath,&filelist,filter_frag,alphasort);

	if(n > 0)
	{
		for(i = 0; i < n; i++)
		{
			memset(str,0,60);
			snprintf(str,60,"%s/%s",dirpath,filelist[i]->d_name);			
			snprintf(strcahche,60,"%s",str);	
			_printd("recover %s",str);
			strncpy(&(strcahche[strlen(str) - 3]),"cache",5);
			_printd("recover %s",strcahche);
			_printd("recover %s",str);
			if(0 == access((const char *)str, F_OK)
				&& 0 == access((const char *)strcahche, F_OK))
			{
				Try_RepairMp4File(str);
			}
			if(0 == access((const char *)str, F_OK))
			{
				remove(str);
			}
			if(0 == access((const char *)strcahche, F_OK))
			{
				remove(strcahche);
			}

			_printd("recover %s over",str);
		}		
	}
	for(i = 0; i < n; i++){
		free(filelist[i]);
	}
	free(filelist);	
}
/******************************FFFile******************************/

int  ext_rec = PIRREC_SET;
char now_extfile[128] = {0};   //临时用于记录最新小录像的文件路径

FFFile::FFFile()//:CThread("CFile", TP_FILE)
{
	v_count = 1;
	i_count = 1;
	almtype	= F_HAND;
	m_fd    = NULL;
	handle  = NULL;

	handle_ext = NULL;
	m_fd_ext   = NULL;

	memset(&start_tm,0,sizeof(start_tm));
	memset(&pre_end_tm,0,sizeof(pre_end_tm));

	file_sys_init();
}
FFFile::~FFFile()
{
	if(m_fd)	fclose(m_fd);

	m_fd 	= NULL;
	handle  = NULL;
	_printd("file_sys_deinit...");
	file_sys_deinit();
}
VD_BOOL FFFile::Open(char mtype/* = VIDEO */)
{	
	CGuard guard(m_Mutex);
	
	time_t          t 	= time(NULL);
    struct tm       *ntime;
	char	dir[128]	= {0};
	VD_BOOL			ret = true;
	VD_BOOL			ret1 = 0;

	if(file_sys_init() < 0)
	{
		ret = VD_FALSE;
		goto VOPEN_EXIT;
	}
	
	//删除 文件夹中多余的文件
	clear_file();

#ifndef IPC_HISI
    struct timeval  tv;
    struct timezone tz;

    gettimeofday(&tv, &tz);
    t += tz.tz_minuteswest*60;
	ntime = gmtime(&t);
#else
	ntime = gmtime(&t);
#endif	
	type = mtype;
	
	if(VIDEO == type)
	{
		if( NULL != handle && NULL != m_fd){
			_printd("The mp4File(%s) has been opened",namebuf);
			return true;
		}
	
		_printd("create_dir");
		create_dir(VIDEO_DIR);
		create_dir(TIMER_DIR);
		create_dir(ALARM_DIR);
		
		memset(namebuf,0,sizeof(namebuf));
		if(almtype == F_DYNAMIC)
		{
			snprintf(namebuf,sizeof(namebuf),"%s/%4d%02d%02d%02d%02d%02d.mp4",ALARM_DIR,ntime->tm_year + 1900,ntime->tm_mon + 1,ntime->tm_mday,
						ntime->tm_hour,ntime->tm_min,ntime->tm_sec);
		}
		else
		{
			snprintf(dir, sizeof(dir), "%s/%04d-%02d-%02d/%02d",TIMER_DIR,ntime->tm_year + 1900,ntime->tm_mon + 1,ntime->tm_mday,
				ntime->tm_hour);
			ret1 = mkdir_mul(dir);
			if(ret1)
			{
				goto VOPEN_EXIT;
			}

			snprintf(namebuf,sizeof(namebuf),"%s/%4d%02d%02d%02d%02d%02d.mp4",dir,ntime->tm_year + 1900,ntime->tm_mon + 1,ntime->tm_mday,
						ntime->tm_hour,ntime->tm_min,ntime->tm_sec);
		}

		if(NULL == (handle = MP4Writer_Open(namebuf)))
		{
			_printd("%s error open\n",namebuf);		
			ret = VD_FALSE;
			goto VOPEN_EXIT;
		}
		else
		{
			m_fd = MP4Writer_GetFp(handle);
			printf("%s open successly\n",namebuf);	
		}

	}
	else
	{
		_printd("create_dir");
		create_dir(IMAGE_DIR);
		
		memset(namebuf,0,sizeof(namebuf));
		snprintf(namebuf,sizeof(namebuf),"%s/%4d%02d%02d%02d%02d%02d.jpg",IMAGE_DIR,ntime->tm_year + 1900,ntime->tm_mon + 1,ntime->tm_mday,
					ntime->tm_hour,ntime->tm_min,ntime->tm_sec);
		_printd("open file %s\n",namebuf);
		if ((m_fd = fopen(namebuf,"wb+")) == NULL)  
		{
			printf("error open\n");
			return false;
		}			
	}

	if(m_fd != NULL)
	{
		g_FSync.SetFd(&m_fd);
		g_FSync.Start();
	}
	m_iWriteError = 0;
VOPEN_EXIT:
	if(PIRREC_ON == ext_rec && NULL == handle_ext)
	{	
		if(NULL == (handle_ext = ExtOpen()))	
		{
			_printd("%s error open\n",namebuf_ext);		
		}
		else
		{
			m_fd_ext = MP4Writer_GetFp(handle_ext);

			ret = true;
		}
	}
	else
	{
		int retmout = 0;
		retmout = checkismount(source_dir,TARGET_DIR);
		if(0 != retmout && ret == VD_FALSE && NULL == handle_ext)
		{
			_printd("file_sys_deinit...");
			if(-3 == retmout)
			{
				mount(source_dir,TARGET_DIR,FILE_SYSTEM,0,NULL);
			}
			else
			{
				file_sys_deinit();
			}
			g_DriverManager.SetSDStatus(4);
		}
			
		if(0 == retmout)
		{
			mount_flag = 1; //已经正常挂载
			g_DriverManager.SetSDStatus(0);
		}
	}
	
	return ret;
}

VD_BOOL FFFile::Close(bool divide /* = false*/)
{

	int 	Total			= 0;
	int		Free			= 0;
	char 	temp[128]		= {0};
	char    recrename[128]	= {0};  //防止移动不成功
	char	dir[128]		= {0};
	char    recpath[128] 	= {0};   //修复
	char 	*s 				= NULL;
	time_t 	t  				= time(NULL);
	time_t  tmpT			= 0;
	time_t  endT			= 0;
    struct tm  ntime		= {0};	
    struct timeval  tv		= {0};
    struct timezone tz		= {0};	
	int ret 				= 0;

	CGuard guard(m_Mutex);	

	if(NULL != handle_ext )	
	{
		printf("ExtClose\n");
		ExtClose();
	}	

	g_FSync.SetFd(NULL);
	
	if(m_fd == NULL)
	{
		return TRUE;
	}	
	m_iPostion = 116;
	checkismount(source_dir,TARGET_DIR);
	if(file_sys_get_linkstat() < 0 || file_sys_get_cap(&Total,&Free) < 0)
	{
		if(VIDEO == type)
		{
			MP4Writer_Close(handle);
			handle = NULL;
		}
		else
		{		
			if(m_fd != NULL)	fclose(m_fd);
		}
		m_fd = NULL;
		printf("Close unexpectly\n");
		goto ERROR;
	}

	m_iPostion = 16;
	checkismount(source_dir,TARGET_DIR);
	fflush(m_fd);
	
	if(VIDEO == type)	
	{
		int  playtime;

		m_iPostion = 17;

		if(divide == false)
		{
			g_FSync.Data_in = VD_FALSE;
			g_FSync.m_SyncMutex.Enter();
		}
		
		playtime = Mp4_GetCurrentPTime(handle);
		MP4Writer_Close(handle);
		m_fd 	 = NULL;
		handle   = NULL;
		checkismount(source_dir,TARGET_DIR);
		
		m_iPostion = 18;

		if(divide == false)
		{
			g_FSync.m_SyncMutex.Leave();		
		}
		
		m_iPostion = 19;


#if 0 //open 时处理过
		if(Free < MIN_CAPACITY)
		{

			//remove_earlist_videofile();
			remove_earlist_videofile(TIMER_DIR);
		}
#endif
		if(playtime < 1)
		{
			if(remove(namebuf)<0)
			{
				char str[256];
				snprintf(str,sizeof(str),"rm -f %s",namebuf);
				_printd("error:%s",str);
				//system(str);
			}
			_printd("remove %s\n",namebuf);
			
			goto END;
		}
		
		if(access((const char *)namebuf, F_OK) != 0)
		{
			goto ERROR;
		}

		t = t - playtime;
#ifndef IPC_HISI
	    gettimeofday(&tv, &tz);
	    t += tz.tz_minuteswest*60;
#else	
#endif	
		if( NULL == gmtime_r(&t,&ntime)){
			memset(&ntime,0,sizeof(ntime));
		}

		s = strrchr(namebuf,'/');
		if(s == NULL)	goto ERROR;
		s[0] = '\0';
/////////////////此处用来修复SD卡的测试/////////////////////////
		strncpy(recpath,namebuf,s-namebuf);
		recpath[s-namebuf] ='\0';
//		_printd("recpath:%s",recpath);
//		recover_recfile(recpath);
//		goto ERROR;
//////////////////////////////////////////////////
		memset(temp,0,sizeof(temp));

//检测首帧私有头带的时间
		start_tm.tm_year += 100;
		start_tm.tm_mon  -= 1;
		tmpT = mktime(&start_tm);

		end_tm.tm_year += 100;
		end_tm.tm_mon  -= 1;
		endT = mktime(&end_tm);

		if(playtime >= INTERVAL_VIDEO - 1){ //正常打包的视频才做这步检查

			_printd("record start:%4d-%02d-%02d %02d:%02d:%02d\n",start_tm.tm_year + 1900,start_tm.tm_mon + 1,
				start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec);
		
#if 0		
			/* 对比上个文件结束时间 和该文件开始时间若在 2秒误差内则采用上个文件结束时间*/
			if(abs(tmpT - mktime(&pre_end_tm)) < 2){
				memcpy(&start_tm,&pre_end_tm,sizeof(start_tm));
				tmpT = mktime(&pre_end_tm);
			}
			_printd("last record end:%4d-%02d-%02d %02d:%02d:%02d\n",pre_end_tm.tm_year + 1900,pre_end_tm.tm_mon + 1,
				pre_end_tm.tm_mday,pre_end_tm.tm_hour,pre_end_tm.tm_min,pre_end_tm.tm_sec);
#endif	
		}

		
		/* 对比该文件开始时间与系统时间,若误差较小内则采用文件首帧私有头时间*/
		//if(abs(tmpT - endT) > 30)
	   {
				
			int iplaytime = abs(tmpT - endT);
			//时间调整则使用MP4内时间
			if(iplaytime > INTERVAL_VIDEO + 60)
			{
				iplaytime = playtime;
			}
			if(start_tm.tm_year + 1900 <= 2018){
				start_tm.tm_year = 2018 - 1900;
			}
		
			snprintf(dir,sizeof(dir),"%s/%04d-%02d-%02d/%02d",TIMER_DIR,start_tm.tm_year + 1900,start_tm.tm_mon + 1,start_tm.tm_mday,
				start_tm.tm_hour);
			
			ret = mkdir_mul(dir);
			m_iPostion = 191;
			checkismount(source_dir,TARGET_DIR);		
#if 0			
			snprintf(temp,sizeof(temp),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",dir,start_tm.tm_year + 1900,start_tm.tm_mon + 1,
				start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec,
				playtime/60,playtime%60,v_count);
#endif		

			if(0 == ret){
			    snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",dir,start_tm.tm_year + 1900,start_tm.tm_mon + 1,
								start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec,
								iplaytime/60,iplaytime%60,v_count);
			}
			else{
				_printd("mkdir %s error\n",dir);
			    snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",namebuf,start_tm.tm_year + 1900,start_tm.tm_mon + 1,
								start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec,
								iplaytime/60,iplaytime%60,v_count);
			}


			//printf("tm_year:%d,ntime.tm_year:%d\n",start_tm.tm_year,ntime->tm_year);
			tmpT += iplaytime;
			
#if 0
			if( NULL == gmtime_r(&tmpT,&pre_end_tm)){
				memset(&pre_end_tm,0,sizeof(pre_end_tm));
			}
#endif			
		}
#if 0		
		else{
			int iplaytime = abs(tmpT - endT);
			if(ntime.tm_year + 1900 <= 2018){
				ntime.tm_year = 2018 - 1900;
			}	
			
			snprintf(dir,sizeof(dir),"%s/%04d-%02d-%02d/%02d",TIMER_DIR,ntime.tm_year + 1900,ntime.tm_mon + 1,ntime.tm_mday,
				ntime.tm_hour);

			ret = mkdir_mul(dir);
			m_iPostion = 192;
		    checkismount(source_dir,TARGET_DIR);
#if 0
			snprintf(temp,sizeof(temp),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",dir,ntime.tm_year + 1900,ntime.tm_mon + 1,
				ntime.tm_mday,ntime.tm_hour,ntime.tm_min,ntime.tm_sec,
				playtime/60,playtime%60,v_count);
#endif	
			if(0 == ret){
			    snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",dir,start_tm.tm_year + 1900,start_tm.tm_mon + 1,
								start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec,
								iplaytime/60,iplaytime%60,v_count);
			}
			else{
				_printd("mkdir %s error\n",dir);
			    snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",namebuf,start_tm.tm_year + 1900,start_tm.tm_mon + 1,
								start_tm.tm_mday,start_tm.tm_hour,start_tm.tm_min,start_tm.tm_sec,
								iplaytime/60,iplaytime%60,v_count);
			}
#if 0

			if(0 == ret){
				#if 0
			    snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",dir,ntime.tm_year + 1900,ntime.tm_mon + 1,
								ntime.tm_mday,ntime.tm_hour,ntime.tm_min,ntime.tm_sec,
								playtime/60,playtime%60,v_count);
				#endif
			}
			else{
				_printd("mkdir %s error\n",dir);
		
				snprintf(recrename,sizeof(recrename),"%s/%4d%02d%02d%02d%02d%02d_%02d%02d_%04d.mp4",namebuf,ntime.tm_year + 1900,ntime.tm_mon + 1,
					ntime.tm_mday,ntime.tm_hour,ntime.tm_min,ntime.tm_sec,
					playtime/60,playtime%60,v_count);
				
			}
#endif

			tmpT = t + playtime;
#if 0
			if( NULL == gmtime_r(&tmpT,&pre_end_tm)){
				memset(&pre_end_tm,0,sizeof(pre_end_tm));
			}
#endif
		}
#endif		
		s[0] = '/';	

	//命名格式   年月日时分秒_视频时长_序号(0-1000).avi
		//if(rename(namebuf,temp) < 0)
		_printd("namebuf:%s",namebuf);
		_printd("recrename:%s",recrename);
		m_iPostion = 193;
		checkismount(source_dir,TARGET_DIR);
		if(rename(namebuf,recrename) < 0)
		{
		//	char str[256];
		//	snprintf(str,sizeof(str),"mv %s %s",namebuf,temp);
			_printd("error:%s\n",recrename);
		//	rename(namebuf,recrename); //修改名保存到当前目录
			//变可读则修复
			checkismount(source_dir,TARGET_DIR);
			_printd("repair path:%s",recpath);
     		recover_recfile(recpath);
			checkismount(source_dir,TARGET_DIR);
			//system(str);
		}
		else
		{
			_printd("rename %s\n",recrename);
		}

		v_count ++;
		if(v_count > 1000) v_count = 1;	
	}
	#if 0
	else
	{	
		if(access((const char *)namebuf, F_OK) != 0)
		{
			fclose(m_fd);
			goto ERROR;
		}
		s = strchr(namebuf,'.');
		if(s == NULL)
		{
			fclose(m_fd);
			goto ERROR;
		}
		s[0] = '\0';
		memset(temp,0,256);
		sprintf(temp,"%s_0000_%04d.jpg",namebuf,i_count);
		s[0] = '.';
	//命名格式   年月日时分秒_0000_序号(0-1000).jpg
		if(rename(namebuf,temp) < 0)
		{
			char str[256];
			sprintf(str,"mv %s %s",namebuf,temp);
			system(str);
		}
		else
			printf("rename %s\n",temp);
		
		i_count ++;
		if(i_count > 1000) i_count = 1;
		
		fclose(m_fd);
		m_fd = NULL;	
	}

	#endif
	s = NULL;
END:
#if 1
//同步在windos上的文件修改时间，但缺点是linux上会晚(8)个小时	
	struct stat fbuf;
	struct utimbuf ubuf;

	if( tz.tz_minuteswest > 0 )
	{
		if(stat(recrename,&fbuf) < 0){
			perror("read file error");
		}
		else
		{
			//加上时区时间 * 2才是正常时间
			ubuf.actime 	= fbuf.st_ctime + tz.tz_minuteswest*60*2;
			ubuf.modtime 	= fbuf.st_mtime + tz.tz_minuteswest*60*2;	

			if(utime(recrename,&ubuf) < 0){
				perror("modify file error");
			}
		}
	}
#endif
//	_printd("2222222222222");
//	sync();

	memset(&start_tm,0,sizeof(start_tm));
	
	return TRUE;
ERROR:
	
	m_fd = NULL;
	memset(&start_tm,0,sizeof(start_tm));
	
	return FALSE;	
}

VD_BOOL FFFile::IsOpened()
{
	if(m_fd == NULL || handle == NULL)
		return VD_FALSE;
	else
		return VD_TRUE;	
}
int FFFile::GetPosition()
{	
	if(file_sys_get_linkstat() < 0)
		return -1;
	
	if(m_fd == NULL || handle == NULL)
		return -1;
	else
		return ftell(m_fd);
}
int FFFile::Seek(uint lOff, uint nFrom)
{	
	if(file_sys_get_linkstat() < 0)
		return -1;
	
	if(m_fd == NULL || handle == NULL )
		return -1;
	else
		return fseek(m_fd,lOff,nFrom);
}
int FFFile::Write(void *pBuffer, uint dwCount,VD_BOOL bPacket) //bPacket 没作用，为配合兼容私有文件系统的写函数而添加上去
{
	if(file_sys_get_linkstat() < 0 || m_fd == NULL)
		return -1;

	int len;
	int ptslen = 0;
	char frame_type[4];
	long long tmppts;
	static long long startpts = 0;
	
	if(VIDEO == type)
	{
		if(MP4Writer_GetCurrentPTime(handle) >= (INTERVAL_VIDEO - 1))
		{
			memcpy(frame_type,pBuffer,4);
#ifdef IPC_HISI
			memcpy(&tmppts,pBuffer+dwCount-sizeof(long long),sizeof(long long));
			
			if(frame_type[3] == 0xfd || frame_type[3] == 0xfc) 
			{
				ptslen = 13;
				if(startpts == 0 && tmppts < 0xfffffffffffff) 
				{
					startpts = tmppts;
				}
			}
			else
			{
				ptslen = 8;
			}

			if(tmppts < startpts || startpts <= 0 || tmppts < 0)
			{
				printf("error tmppts:%lld,startpts:%lld\n",tmppts,startpts);
				return -1;
			}
#endif		
			if(frame_type[3] == 0xad || frame_type[3] == 0xfd)
			{

				char unit_type[5];
				int  n; //I帧的私有头长度
				if(frame_type[3] == 0xad) n = 20; 
				else 					  n = 16;
#ifdef IPC_HISI				
				memcpy(unit_type,pBuffer+n,5);
				if(7 == (unit_type[4]&0x1f) || 8 == (unit_type[4]&0x1f) || 5 == (unit_type[4]&0x1f))//IDR帧  
#endif
				{
					g_FSync.Data_in = VD_FALSE;
					g_FSync.m_SyncMutex.Enter();
					Close(true);
					if(Open() != true)
					{
						g_FSync.m_SyncMutex.Leave();
						return -1;
					}	
					g_FSync.m_SyncMutex.Leave();
					startpts = 0;
				}
			}
		}	
		len = MP4Writer_Write((unsigned char *)pBuffer,dwCount-ptslen,handle);
	}
	else
	{
		len = fwrite((unsigned char *)pBuffer,dwCount,1,m_fd);
		//Read_Mp4_Close();
	}
	
	g_FSync.Data_in = VD_TRUE;
	SystemUSleep(1*1000);
	return len;

}
#ifdef RECORD_NET_THREAD_MULTI 
VD_BOOL FFFile::WriteGroup(PACKET_GROUP_RECORD* pPacketGroup)
{
	static  unsigned long long 	startpts  = 0;
	unsigned long long  		tmppts = 0;

	static int error_num 	= 0;
	
	unsigned char frame_type[4]={0};
	//int			ptslen = 0;	
	VD_BOOL 	ret	   = VD_TRUE;
	VD_BOOL 	divide = VD_FALSE;
	int         writelen = 0;

	m_iPostion = 1;
	if(handle_ext == NULL && (file_sys_get_linkstat() < 0 || handle == NULL || m_fd == NULL))

	{
		ret  = VD_FALSE;
		if(NULL == handle)
		{
			Open();
		}
		goto FREE_PAC;
	}
	m_iPostion = 2;
	if(handle != NULL && MP4Writer_GetCurrentPTime(handle) >= (INTERVAL_VIDEO -1))
	{
		divide = VD_TRUE;
	}
	m_iPostion = 3;
	for(int i = 0; i < pPacketGroup->count; i++)
	{
		PACKET_RECORD *Record;
		Record = &(pPacketGroup->pPacketRecord[i]);


		if(handle_ext == NULL && (file_sys_get_linkstat() < 0 || handle == NULL || m_fd == NULL))
		{
			ret  = VD_FALSE;
			goto FREE_PAC;
		}
		memcpy(frame_type,Record->pPacket->GetBuffer()+Record->dwStartPos,4);
//#ifdef IPC_HISI
#if 1
		memcpy(&tmppts,Record->pPacket->GetBuffer()+Record->dwCount-sizeof(long long),sizeof(long long));
		
		if(frame_type[3] == 0xfd || frame_type[3] == 0xfc ||
		   frame_type[3] == 0xad || frame_type[3] == 0xac) 
		{
			//ptslen = 13;
			if(startpts == 0 && tmppts < 0xfffffffffffff) 
			{
				startpts = tmppts;
			}
		}
		else
		{
			//ptslen = 8;
		}
		
		if(tmppts < startpts || startpts <= 0 || tmppts < 0)
		{
			printf("error tmppts:%lld,startpts:%lld\n",tmppts,startpts);
			//if error_num big than 50,continue to write recordfile ignoring error
			if(++error_num < 50) 
				continue;
		}
#endif	
		m_iPostion = 4;
		if(handle != NULL)
		{
			if(frame_type[3] == 0xad || frame_type[3] == 0xfd) 
			{
				unsigned int datatime = 0;
				struct tm cur_tm = {0};	
				if(frame_type[3] == 0xad)
					memcpy(&datatime,Record->pPacket->GetBuffer()+Record->dwStartPos+12,4);
				else
					memcpy(&datatime,Record->pPacket->GetBuffer()+Record->dwStartPos+8,4);

				cur_tm.tm_year 	= (datatime>>26)&0x3f;
				cur_tm.tm_mon 	= (datatime>>22)&0x0f;
				cur_tm.tm_mday	= (datatime>>17)&0x1f;
				cur_tm.tm_hour	= (datatime>>12)&0x1f;
				cur_tm.tm_min	= (datatime>>6)&0x3f;
				cur_tm.tm_sec	= datatime&0x3f;
				memcpy(&end_tm,&cur_tm,sizeof(struct tm));
				if(start_tm.tm_year != 0 && ( cur_tm.tm_year != start_tm.tm_year 
					|| cur_tm.tm_mon != start_tm.tm_mon || cur_tm.tm_mday != start_tm.tm_mday))
				{
					//文件不跨天
					divide =VD_TRUE;
				}
				if(divide || m_iWriteError)
				{	
#ifdef IPC_HISI
					char unit_type[5];
					int  n; 
					if(frame_type[3] == 0xad) n = 20; 
					else 					  n = 16;
					
					memcpy(unit_type,Record->pPacket->GetBuffer()+Record->dwStartPos+n,5);
					if(7 == (unit_type[4]&0x1f) || 8 == (unit_type[4]&0x1f) || 5 == (unit_type[4]&0x1f))
#endif
					{				
						g_FSync.Data_in = VD_FALSE;
						m_iPostion = 5;
						g_FSync.m_SyncMutex.Enter();
						m_iPostion = 15;
						Close(true);
						m_iPostion = 6;
						if(Open() != true)
						{
							ret  = VD_FALSE;
							m_iPostion = 7;
							g_FSync.m_SyncMutex.Leave();
							goto FREE_PAC;
						}
						m_iPostion = 8;
						divide = VD_FALSE;
						g_FSync.m_SyncMutex.Leave();
						
						if(tmppts < 0xfffffffffffff) {
							startpts = tmppts;
						}		
						error_num = 0;
					}
				}
				//取出私有头带的时间作为 录像开始时间
				if(start_tm.tm_year <= 0 && start_tm.tm_mon <= 0 && start_tm.tm_mday <= 0)
				{	
#if 0
					unsigned int datatime = 0;
					
					if(frame_type[3] == 0xad)
						memcpy(&datatime,Record->pPacket->GetBuffer()+Record->dwStartPos+12,4);
					else
						memcpy(&datatime,Record->pPacket->GetBuffer()+Record->dwStartPos+8,4);
#endif
					
					memcpy(&start_tm,&cur_tm,sizeof(struct tm));
					#if 1
					_printd("record start time:%04d-%02d-%02d %02d:%02d:%02d\n",
						start_tm.tm_year+2000,start_tm.tm_mon,
						start_tm.tm_mday,
						start_tm.tm_hour,start_tm.tm_min,
						start_tm.tm_sec);
				    #endif
				}	
				#if 0
					_printd("record start time:%04d-%02d-%02d %02d:%02d:%02d\n",
						start_tm.tm_year,start_tm.tm_mon,
						start_tm.tm_mday,
						start_tm.tm_hour,start_tm.tm_min,
						start_tm.tm_sec);
				#endif
			}
			m_iPostion = 9;
	 		writelen = MP4Writer_Write((unsigned char*)(Record->pPacket->GetBuffer()+Record->dwStartPos),Record->dwCount,handle);
			if(writelen <= 0)
			{
				_printd(" write Frame error!!!");
				m_iWriteError = 1;
			}
			else
			{
				
			}
			if(frame_type[3] == 0xad)
			{
				_printd("record write I Frame!!!");
				fflush(m_fd);  //fwrite 有卡死 一个I帧刷新一下
		#if 0		
				if(recordcount++ >= 8)
				{
					char wirtebuf[128] = {0};
					SYSTEM_TIME stNow;
					SystemGetCurrentTime(&stNow);
					snprintf(wirtebuf,128,"[%02d-%02d %02d:%02d:%02d] record write I Frame\n",	stNow.month, stNow.day,
					stNow.hour, stNow.minute, stNow.second);
					writefile_callback((void*)wirtebuf,128,1,logfilename);
				}
		#endif
			}
			m_iPostion = 10;
		}
		if(handle_ext != NULL)
		{
			m_iPostion = 11;
			//printf("MP4Writer_Write,MP4Writer_GetCurrentPTime(handle_ext):%d\n",MP4Writer_GetCurrentPTime(handle_ext));
			MP4Writer_Write((unsigned char*)(Record->pPacket->GetBuffer()+Record->dwStartPos),Record->dwCount,handle_ext);
			m_iPostion = 12;
		}
	}
	if(handle_ext != NULL && MP4Writer_GetCurrentPTime(handle_ext) >= PIRREC_DUR)
	{
		m_iPostion = 13;
		//printf("MP4Writer_GetCurrentPTime(handle_ext):%d\n",MP4Writer_GetCurrentPTime(handle_ext));
		ExtClose();
		m_iPostion = 14;
	}

//	printf("MP4Writer_GetCurrentPTime(handle):%d\n",MP4Writer_GetCurrentPTime(handle));

	g_FSync.Data_in = VD_TRUE;
FREE_PAC:
	for(int i = 0; i < pPacketGroup->count; i++)
	{
		PACKET_RECORD* pRecord = &(pPacketGroup->pPacketRecord[i]);
		pRecord->pPacket->Release();
	}
	m_iPostion = 15;
	return ret;
}
#endif
VD_BOOL FFFile::GetList(uint chan, SYSTEM_TIME* start_time, SYSTEM_TIME* end_time, uint type, uint *num, FILE_INFO *info, uint hint /* = 0 */, uint driver_type)
{
	uint ret;
	DHTIME st, et;
    
	CGuard guard(m_Mutex);

	timesys2dh(&st, start_time);
	timesys2dh(&et, end_time);	
	ret  = 0;
	*num = 2;
	file_sys_getlist(chan, start_time, end_time, type,num, info,  hint, driver_type);
	//printf("=======ret = [%d %d  %d]\n", ret, *num, __LINE__);
	if(ret)
	{	
	    return FALSE;
	}
	return TRUE;

}

void* FFFile::ExtOpen()
{
	char 			uid[16] = {0};
	char 			tmp[64] = "0000000000000000";

	memset(namebuf_ext,0,sizeof(namebuf_ext));
	if(0 == GetUuid(uid))
	{
		memcpy(tmp,uid,16);
	}
	
	if(file_sys_init() < 0)
	{
		_printd("create_dir");
		create_dir(PIRALM_DIR);
		snprintf(namebuf_ext,sizeof(namebuf_ext),"%s/%s.mp4",PIRALM_DIR,tmp);
		
	
	}
	else
	{
		_printd("create_dir");
		create_dir(RECEXT_DIR);
		snprintf(namebuf_ext,sizeof(namebuf_ext),"%s/%s.mp4",RECEXT_DIR,tmp);
	}
			
	return MP4Writer_Open(namebuf_ext);
}
VD_BOOL FFFile::ExtClose()
{	
	if(handle_ext == NULL)	return VD_FALSE;

	time_t          t 		= time(NULL) - PIRREC_DUR;
    struct tm       ntime;	
	char			bkpName[128] = {0};
	char 			tmp[128] 	= {0};
	char 			uid[17] 	= {0};
	char			*s 			= NULL;
	
#ifndef IPC_HISI
    struct timeval  tv;
    struct timezone tz;

    gettimeofday(&tv, &tz);
    t += tz.tz_minuteswest*60;
#else	
#endif	
	if(NULL == gmtime_r(&t,&ntime)){
		memset(&ntime,0,sizeof(ntime));
	}
		

	if(MP4Writer_GetCurrentPTime(handle_ext) >= PIRREC_DUR - 1)	
		ext_rec = PIRREC_OFF;
	
	MP4Writer_Close(handle_ext);		
	
	handle_ext = NULL;
	m_fd_ext   = NULL;

	
	memcpy(tmp,namebuf_ext,sizeof(tmp));
	memcpy(bkpName,namebuf_ext,sizeof(bkpName));
	

	if(NULL != strstr(tmp,"0000") && 0 == GetUuid(uid))
	{
		if( NULL != (s = strrchr(tmp,'/')))
		{
			s ++;
			memcpy(s,uid,16);
			s = s + 16;
			s[0] = '\0'; 
		}
	}
	else
	{
		if(NULL != (s = strchr(tmp,'.')))	s[0] = '\0';
	}
	snprintf(namebuf_ext,sizeof(namebuf_ext),"%s_%4d%02d%02d%02d%02d%02d_%02d.mp4",tmp,ntime.tm_year + 1900,ntime.tm_mon + 1,ntime.tm_mday,
		ntime.tm_hour,ntime.tm_min,ntime.tm_sec,PIRREC_DUR);

	rename(bkpName,namebuf_ext);
	memcpy(now_extfile,namebuf_ext,sizeof(now_extfile));
	
	
	return VD_TRUE;
}

//不考虑复杂情况，直接读变量来获取文件路径
VD_BOOL GetExtRecFile(char FileName[128])
{
	if(ext_rec == PIRREC_ON)  //录制过程中
	{
		printf("Ext RecordFile is creating......\n");
		return VD_FALSE;
	}

	if(access((const char *)now_extfile, F_OK) == 0)  
	{
		strncpy(FileName,now_extfile,128);
	}
	else
	{
		return VD_FALSE;
	}
	return VD_TRUE;
}
PATTERN_SINGLETON_IMPLEMENT(FileSync);

FileSync::FileSync():CThread("CFile", TP_FILE)
{
	Sync_fd = NULL;
	Data_in = VD_FALSE;
}
FileSync::~FileSync()
{
}
VD_BOOL FileSync::Start()
{
	if(!m_bLoop)
		CreateThread();  

	return true;
}
VD_BOOL FileSync::Stop()
{
	if(m_bLoop)
		DestroyThread();

	return true;
}
VD_BOOL FileSync::SetFd(FILE **fd)
{
	Sync_fd = fd;
	
	return VD_TRUE;
}
void FileSync::ThreadProc()
{
	FILE *pFd = NULL;
	
	SET_THREAD_NAME("FileSync");

	while(m_bLoop)
	{
		if(Data_in && (Sync_fd != NULL && *Sync_fd != NULL))
		{
			m_SyncMutex.Enter();
			if(Data_in && (Sync_fd != NULL && *Sync_fd != NULL))
			{
				pFd = *Sync_fd;
				fflush(pFd);
				Data_in = VD_FALSE;	
				if(fsync(fileno(pFd)) < 0)
				{
					m_SyncMutex.Leave();
					perror("fsync error:");
					Sync_fd  = NULL;
					
					sleep(5);
					continue;
				}
			}
			m_SyncMutex.Leave();

			SystemUSleep(100*1000);
		}
		else
		{
			SystemUSleep(500*1000);
		}
	}
}


VideoFileReader::VideoFileReader()
{
	m_fd = NULL;
	memset(&ftime,0,sizeof(SYSTEM_TIME));
}
VideoFileReader::~VideoFileReader()
{
	if(m_fd != NULL)
		fclose(m_fd);
}
VD_BOOL VideoFileReader::Open()
{
	int ret = VD_TRUE;
	if(m_fd != NULL || file_sys_init() < 0)
		return VD_FALSE;
		
	if(ftime.year <= 0 || ftime.year >= 3000
	    ||(ftime.month < 1 || ftime.month > 12)
	    ||(ftime.day < 1 ||ftime.day > 31)
	    ||(ftime.hour < 0 ||ftime.hour > 23)
	    ||(ftime.minute < 0 || ftime.minute > 59)
	    ||(ftime.second < 0 || ftime.second > 59))
	{
		return VD_FALSE;
	}

		
	char m_name[20];
	snprintf(m_name,sizeof(m_name),"%4d%02d%02d%02d%02d%02d",ftime.year,ftime.month,ftime.day,
		ftime.hour,ftime.minute,ftime.second);
//	printf("m_name:%s\n",m_name);

	DIR 			* dir = NULL;
    struct dirent 	* ptr = NULL;
	char filename[40] = {0};
	char tmp[15] = {0};
	int  i =0;
	
    if((dir = opendir(VIDEO_DIR)) == NULL)
		return VD_FALSE;

	while((ptr = readdir(dir)) != NULL)
    {
        printf("d_name : %s\n", ptr->d_name);

		memcpy(tmp,ptr->d_name,14);
		tmp[14] = '\0';
		//printf("tmp:%s\n",tmp);
		if(strcmp(tmp,m_name) == 0)
		{
			printf("right_FILE:%s\n",ptr->d_name);
			break;
		}
    }
	if(NULL != ptr)
	{
		snprintf(filename,sizeof(filename),"%s/%s",VIDEO_DIR,ptr->d_name);
		if((m_fd = fopen(filename,"r")) == NULL)
		{
			closedir(dir);
			return VD_FALSE;
		}
	}
	else
	{
		ret = VD_FALSE;
	}
    closedir(dir);

	return ret;
}
VD_BOOL VideoFileReader::IsOpen()
{
	if(m_fd == NULL)
		return VD_FALSE;
	else
		return VD_TRUE;
}
VD_BOOL VideoFileReader::Close()
{
	if(m_fd != NULL)
		fclose(m_fd);

	return VD_TRUE;
}
VD_BOOL VideoFileReader::Locate(SYSTEM_TIME st)
{
	memcpy(&ftime,&st,sizeof(SYSTEM_TIME));
}
uint VideoFileReader::Read(void* pBuffer,uint len)
{
	if(pBuffer == NULL)
		return -1;
	
	if(NULL == m_fd)
	{
		Open();
	}
	if(NULL != m_fd)
	{
		return fread(pBuffer,1,len,m_fd);
	}
	return -1;
}
////////////////////////////////////MP4WRITE////////////////////////////////////////////////
void* MP4Writer_Open (const char *FileName)
{
	if(FileName == NULL)	return NULL;
	
	return Mp4_Open(FileName,Get_videoRate(),INTERVAL_VIDEO);
}
void MP4Writer_Close(void* Handle)
{
	if(Handle != NULL)	Mp4_Close(Handle);
}
FILE* MP4Writer_GetFp(void* Handle)
{
	if(Handle == NULL)	return NULL;

	return MP4_GetFp(Handle);
}
int MP4Writer_GetCurrentPTime(void* Handle)
{
	if(Handle == NULL)	return -1;

	return Mp4_GetCurrentPTime(Handle);
}
int MP4Writer_Write(unsigned char* buf,int len,void* Handle)
{
	int DataLen			= 0;
	int framepos		= 0;
	static int height	= 0;
	static int width	= 0;
	unsigned char *Data	= NULL;
	FrameType 	type;

	long long FrameTimeStamp;

	memcpy(&FrameTimeStamp,buf + len - 8,sizeof(long long));
	
	if(buf[3] == 0xfd || buf[3] == 0xad || buf[3] == 0xae)//i
	{
		if(buf[3] == 0xfd)
			framepos 	= 16;
		else
			framepos 	= 20;
		
		type 		= VIDEO_I_FRAME;

		{
			int Temp = 0;
			//获取视频宽高		
			if(buf[3] == 0xfd)
			{
				memcpy(&Temp,buf+4,sizeof(int));
				width  = 8*((Temp>>16) & 0xff);
				height = 8*((Temp>>24) & 0xff);
			}
			else
			{
				memcpy(&Temp,buf+8,sizeof(int));
				width  = 8*((Temp) & 0xff);
				height = 8*((Temp>>16) & 0xff);
			}
		}
		//printf("%s,%d,%d\n",__FUNCTION__,width,height);
	}
	else if(buf[3] == 0xfc || buf[3] == 0xac)//p
	{
		framepos 	= 8;
		type 		= VIDEO_P_FRAME;
	}
	else if(buf[3] == 0xf0 || buf[3] == 0xa0)//audio
	{
		framepos 	= 8 + sizeof(unsigned int);
		type 		= AUDIO_FRAME;
	}

	Data 	= buf + framepos;
	DataLen = len - framepos - sizeof(long long);  //sizeof(long long)为后面时间戳得长度

#if defined(IPC_JZ) || defined(IPC_JZ_NEW) || defined(IPC_HISI)
	return Mp4_Write_WithTime(Data,DataLen,height,width,FrameTimeStamp/1000,type,Handle);
#else
	return Mp4_Write(Data,DataLen,height,width,type,Handle);
#endif
}
////////////////////////////////////MP4READ////////////////////////////////////////////////

void* Mp4Reader = NULL;
	
int MP4Reader_Open (const char *FileName)
{
	Mp4Reader = Read_Mp4_Open(FileName);

	if(Mp4Reader == NULL)	return 0;
	else					return 1;
}
void MP4Reader_Close()
{
	Read_Mp4_Close(Mp4Reader);
	Mp4Reader = NULL;
}

int MP4Reader_GetVFrameNums(void)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetVideoFrameNums(Mp4Reader);
}

int MP4Reader_GetAFrameNums(void)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetAudioFrameNums(Mp4Reader);
}

int MP4Reader_GetVFrameDur()
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetVideoFrameDur(Mp4Reader);
}

int MP4Reader_GetAFrameDur()
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetAudioFrameDur(Mp4Reader);
}
int MP4Reader_GetVideoTimeStamp(int val)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetVideoTimeStamp(Mp4Reader,val);
}
int MP4Reader_GetAudioTimeStamp(int val)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_GetAudioTimeStamp(Mp4Reader,val);
}
int MP4Reader_GetKeyFrameNums()
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_Get_KeyFrameNums(Mp4Reader);
}
int MP4Reader_Get_I_FrameNum(int val)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_Get_I_FrameNum(Mp4Reader,val);
}
int MP4Reader_VideoData(unsigned char *Buf,int Num)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_VideoData(Mp4Reader,Buf,Num);
}
int MP4Reader_AudioData(unsigned char *Buf,int Num)
{
	if(Mp4Reader == NULL)	return -1;
	
	return Read_Mp4_AudioData(Mp4Reader,Buf,Num);
}

//add by jwd
//判断是否为目录
static int is_dir(const char *path)
{
    struct stat statbuf;
    if(lstat(path, &statbuf) ==0)//lstat返回文件的信息，文件信息存放在stat结构中
    {
        return S_ISDIR(statbuf.st_mode) != 0;//S_ISDIR宏，判断文件类型是否为目录
    }
    return 0;
}

//判断是否为常规文件
static int is_file(const char *path)
{
    struct stat statbuf;
    if(lstat(path, &statbuf) ==0)
        return S_ISREG(statbuf.st_mode) != 0;//判断文件是否为常规文件
    return 0;
}

//判断是否是特殊目录
static int is_special_dir(const char *path)
{
    return strcmp(path, ".") == 0 || strcmp(path, "..") == 0;
}

//生成完整的文件路径
static void get_file_path(const char *path, const char *file_name,  char *file_path)
{
    strcpy(file_path, path);
    if(file_path[strlen(path) - 1] != '/')
        strcat(file_path, "/");
    strcat(file_path, file_name);
}

int delete_file(const char *path,int depth)
{
	int ret = 0;
    DIR *dir;
    struct dirent *dir_info;
    char file_path[PATH_MAX];
	if(depth > 6)
	{
		return 2;
	}
    if(is_file(path))
    {
        ret = remove(path);
		_printd("path:%s [%d]",path,ret);
		ret = checkismount(source_dir,TARGET_DIR);
        return ret;
    }
    if(is_dir(path))
    {
        if((dir = opendir(path)) == NULL)
		{
            return 2;
		}

		_printd("path:%s,errno:%d",path,errno);
		
        while((dir_info = readdir(dir)) != NULL)
        {
			ret = checkismount(source_dir,TARGET_DIR);
			if(2 == ret)
			{
				break;
			}
            get_file_path(path, dir_info->d_name, file_path);
            if(is_special_dir(dir_info->d_name))
                continue;
            ret = delete_file(file_path,depth+1);
			if(0 == ret)
			{
				ret = rmdir(file_path);
			}
			else if(2 == ret)
			{
				_printd("error path :%s",path);
				break;
			}
        }
		closedir(dir);
    }
	_printd("ret[%d]",ret);
	return ret;
} 


static int filter_not_record(const struct dirent *dp)
{
	if(strcmp(dp->d_name,".")==0  || strcmp(dp->d_name,"..")==0)  
    	return 0; 

	if(DT_DIR != dp->d_type)
		return 0; 

	//文件夹长度不够4时会不会产生段错误? 测试?
	if(dp->d_name[4] != '-' && dp->d_name[7] != '-')
	{
		_printd("dp->d_name[%s]\n",dp->d_name);
		return 1;
	}
	return 0;
}
 
void delelte_not_record_dir(char *path, int depth)
{
	struct dirent **name_list;
	char dirname[1024]={0};
	int n = scandir(path,&name_list,filter_not_record,alphasort);
	if(n < 0)
	{ 
		_printd( "scandir return %d \n",n);
	}
	else
	{
		int index=0;
		while(index < n)
		{		
			snprintf(dirname,sizeof(dirname),"%s/%s",path,name_list[index]->d_name);
			_printd("dirname: %s\n", dirname);
			delete_file(dirname,depth+1);
			rmdir(dirname);
			free(name_list[index++]);
		}
		free(name_list);
   	}
}
void delete_top_dir(char *path, int depth)
{
	//static char recordpath[128] = {0}; //已经删除文件夹
	CGuard m_Cguard(remove_mutex);
	int ret = 0;
	struct dirent **name_list;
	char dirname[1024]={0};
	int n = scandir(path,&name_list,filter_date_ymd,alphasort);
	if(n <= 0)
	{ 
		_printd( "scandir return %d \n",n);
		return ;
	}
	else
	{
		int index=0;
		int successfuldel = 0;
		while(index < n)
		{
			_printd("name: %s\n", name_list[index]->d_name);
			snprintf(dirname,sizeof(dirname),"%s%s",path,name_list[index]->d_name);
			if(0 == successfuldel && index != (n-1))
			{
				ret = delete_file(dirname,depth+1);
				if(0 == ret)
				{
					successfuldel =1; //就删除一天
				}
			}
			free(name_list[index++]);
		}
		
		_printd("dirname: %s\n", dirname);
		free(name_list);
		
   	}

}
bool file_sys_del()
{
	int Total,Free ,ret;
	ret = get_sdcard_cap(&Total,&Free);
	static int FreeError = 0;
	static int recordtime = 0;
	if(0 == ret && (Free < MIN_CAPACITY))
	{
		//暂时不用这里处理  后面再改进  by jwd on 20181029
		//delelte_not_record_dir(TIMER_DIR,0);
		if(FreeError++ > 20) //10分钟
		{
			FreeError = 0;
			delete_top_dir(TIMER_DIR,0);
		}
	}
	else
	{
		FreeError = 0;
	}
	_printd("record ret[%d],Total[%d],Free[%d],FreeError[%d]",ret,Total,Free,FreeError);
	_printd("record m_iPostion[%d]",m_iPostion);
	if(repair_flag && recordtime++ >= 2)
	{
		SYSTEM_TIME systime;
		DHTIME dhtime;
		SystemGetCurrentTime(&systime);
		if(recsystime.year != systime.year || recsystime.month !=  systime.month 
			|| recsystime.day != systime.day )
		{
			memcpy(&recsystime,&systime,sizeof(SYSTEM_TIME));
		    timesys2dh(&dhtime, &systime);
		    g_Cmos.SetExitTime(&dhtime);
		}
	}
}

