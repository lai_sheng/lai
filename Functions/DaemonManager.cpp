#include <stdlib.h>
#include <sys/sysinfo.h>
#include "Functions/DaemonManager.h"
#include "Functions/FFFile.h"
#include "Main.h"
#include "System/AppConfig.h"   
#include "System/AppEvent.h"
#include "WFS/adapt.h"
#include "Devices/DevInfo.h"
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#endif


PATTERN_SINGLETON_IMPLEMENT(CDaemonManager);

CDaemonManager::CDaemonManager() : CThread("DaemonManager", TP_DESKTOP, 1024),m_TimerPrint("m_TimerPrint")
{
      trace("CDaemonManager::CDaemonManager()>>>>>>>>>\n");
	  m_iDaemonLossmes = 0;
	  m_iWifiStatus = 0;
	  m_iWifiConectStatus = 0;
	  m_iLedStatus = 0;
	  m_iSock = -1;
}

CDaemonManager::~CDaemonManager()
{
}

VD_BOOL CDaemonManager::Start()
{
    trace("CDaemonManager::Start()>>>>>>>>>\n");
    //创建线程
    #if 1
    CreateThread();
	#endif
	
	m_TimerPrint.Start(this, (VD_TIMERPROC)&CDaemonManager::OnPrintTimer, 0, 30000);
    return TRUE;    
}
void CDaemonManager::OnPrintTimer(uint param)
{
	char date[20]={0};
	_printd("wifi:%d wificonnect:%d Led:%d m_iConnectPostion：%d", m_iWifiStatus,m_iWifiConectStatus,m_iLedStatus,m_iConnectPostion);
	//打印一下IP
	GetDevIp(date,20);
}

VD_BOOL CDaemonManager::ConnectToDaemon()
{
	int sockfd = -1;
	int ret = 0;
	int len = 0;
	struct sockaddr_un address;
	if((sockfd = socket(AF_UNIX, SOCK_STREAM, 0))==-1)//创建socket，指定通信协议为AF_UNIX,数据方式SOCK_STREAM
	{
		perror("socket");
		return FALSE;
	}
		
	//配置server_address
	address.sun_family = AF_UNIX;
	strcpy(address.sun_path, "/tmp/server_socket");
	len = sizeof(address);
 
	ret = connect(sockfd, (struct sockaddr *)&address, len);
	
	if(ret == -1) 
	{
		perror("connect");
		//pidof(daemon);			
	    close(sockfd);
		sleep(1);
		#if 1
		if(-1 == GetPidByName("daemon"))
		{
			_printd("loss daemon %d!!!",120 - m_iDaemonLossmes);
			if(m_iDaemonLossmes++ > 120) //2分钟后重启
			{
				//daemon 丢失，重启标志位13
			//	g_Challenger.Reboot(0,13);
			}
			struct sysinfo info;
			sysinfo(&info);	
			if (info.uptime > 60 && info.uptime < 120)
			{//备份复原daemon
				_printd("unrar x daemon ,reboot >>>>>>>>>!!!");
				char UpgradeFilePath[128] = { 0 };
				sprintf(UpgradeFilePath, "/app/bin/unrar x /app/daemon/daemon.rar -y /app/daemon/ ;%s","reboot -f");
				printf("%s\n", UpgradeFilePath);
				execl("/bin/sh", "sh", "-c", UpgradeFilePath, (char*)0);
			}
		}
		#endif
		return FALSE;				
	}
	else
	{
		m_iDaemonLossmes = 0;
	}
	m_iSock = sockfd;
	return TRUE;
}
VD_BOOL CDaemonManager::Stop()
{
    //销毁线程
    DestroyThread();
    trace("CDaemonManager::Stop()>>>>>>>>>\n");
    return TRUE;
}
VD_BOOL CDaemonManager::CmdProcess(IPC_HEADER cmd)
{
	switch(cmd.ipc_cmd)
	{
		case WIFI_STATUS_POST:
		{
			m_iWifiStatus = cmd.ipc_r0;
			if(m_iWifiStatus > 2)
			{
				_printd("m_iWifiStatus[%d]",m_iWifiStatus);
			}
		}
		break;
		case WIFI_CONNECT_POST:
		{
			m_iWifiConectStatus = cmd.ipc_r0;
			if(1 == cmd.ipc_r0)
			{
				//wifi connect start:1  //write log
				_printd("wifi connect start!!!\n");
			}
			else if(2 == cmd.ipc_r0)
			{
				//wifi connect end:2  wifi_conect.sh exit
				_printd("wifi connect end!!!\n");
			}
			SYSTEM_TIME systime;
			DHTIME dhtime;
			SystemGetCurrentTime(&systime);
		    timesys2dh(&dhtime, &systime);
			//g_Log.Append(LOG_WIFI_CONNET, cmd.ipc_r0, &dhtime, sizeof(DHTIME));	
			
		}
		break;
		case LED_STATUS_POST:
		{
			m_iLedStatus = cmd.ipc_r0;
		}
		break;
		case SYSTEM_CMD_ERROR_POST:
		{
			if(m_iDaemonCmderror++ > 50) //50次失败重启
			{
				//daemon 丢失，重启标志位14
				g_Challenger.Reboot(0,14);
			}
		}
		break;
		case WIFI_CONECTING_POST:
		{
			_printd("m_iConnectPostion[%d]",m_iConnectPostion);
			m_iConnectPostion = cmd.ipc_r0;
		}
		break;
		case WIFI_KEY_PRESS_POST:
		{			
			SYSTEM_TIME systime;
			DHTIME dhtime;
			SystemGetCurrentTime(&systime);
			timesys2dh(&dhtime, &systime);
			//g_Log.Append(LOG_KEY_PRRESS, cmd.ipc_r0, &dhtime, sizeof(DHTIME)); 
			_printd("key_press[%d]", cmd.ipc_r0);
		}
		break;
		case WIFI_MOD_SSID:
		{
#ifdef HEMU_CLOUD
			g_HeMuHttpsCom.SendOpenAPIReq(MSG_P_THD_SET_CAMERA_SETTINGS_REQ);
#endif
		}
		break;
		default:
			break;
	}
	return TRUE;
}

int  CDaemonManager::SendCmdToDaemon(void* buf,unsigned int len)
{
	long sendbyte =0;
	long nRet = 0;
	if(-1 == m_iSock)
	{
		return -1;
	}	

	unsigned char *buftmp = (unsigned char *)buf;
	do
	{
		struct timeval    tv;            /* Timeout for select() */
	    fd_set     write_set;
	    int        max_fd = 0, msec = 5000;

		max_fd = m_iSock;
	    FD_ZERO(&write_set);
		
		tv.tv_sec = msec / 1000;
	    tv.tv_usec = msec % 1000;

		FD_SET(m_iSock, &write_set);
	    /* Check IO readiness */
	    if (select(max_fd + 1, NULL, &write_set, NULL, &tv) < 0) 
	    {
	        _printd("select: %s\r\n", strerror(errno));
			
	        break;
	    }
		
		nRet = send(m_iSock,buftmp + sendbyte, len - sendbyte, 0);
		if(-1 != nRet)
		{
			sendbyte += nRet;
		}
	}while(-1 != nRet && len != sendbyte);
	
	if(-1 != nRet)
	{
		return 0;
	}
	return -1;
}


void CDaemonManager::ThreadProc()
{
    do 
    {
		if(-1 == m_iSock)
		{
			ConnectToDaemon();
		}
		else
		{
			struct timeval timeo;
			int iRet = 0;
			int byte = 0;
			IPC_HEADER ipcrecvf;
			fd_set   read_set;
			FD_ZERO(&read_set);
			FD_SET(m_iSock, &read_set);
			
			timeo.tv_sec = 3;
			timeo.tv_usec = 0;

			iRet = select(m_iSock + 1,&read_set , NULL, NULL, &timeo);

			if (iRet > 0 && FD_ISSET(m_iSock, &read_set))
			{
				memset(&ipcrecvf,0,sizeof(IPC_HEADER));
				byte = read(m_iSock,&ipcrecvf, sizeof(IPC_HEADER));
				if(-1 == byte || 0 == byte  )
				{
					perror("CDaemonManager read:");
					close(m_iSock);
					m_iSock = -1;
					continue;
				}	
				else
				{
					CmdProcess(ipcrecvf);
					//_printd("the massage receiver from client is:cmd %d reserved:%d ",ipcrecvf.ipc_cmd,ipcrecvf.ipc_r0);
				}
				
			}
		}

    } while (m_bLoop);
}

