#include "Functions/Encode.h"
#include "Functions/Record.h"
#include "System/AppConfig.h"
#include "System/Log.h"
#include "Devices/DevCapture.h"
//#include "Functions/Display.h"
#include "APIs/DVRDEF.H"
#include "Devices/DevVideo.h"
#include "APIs/Audio.h"

#ifndef WIN32
#include <sys/stat.h>
#endif

#define CAPTURE_TIME_INTERVAL (30 * 60)
#define CAPTURE_RESET_INTERVAL 10 //modified by wangqin  每隔10秒钟让DSP自行判断一下DSP状态

PATTERN_SINGLETON_IMPLEMENT(CEncode);

void adjustTimeChannelRect(VIDEO_WIDGET* pWidget, int width, int height);

CEncode::CEncode() : m_EncodeTimer("Encode")
{
	for (int i = 0; i < N_SYS_CH; i++)
	{
		m_ResetCapTimes[i] = 0;
		m_CurColorIndex[i] = -1;
		m_iEncodeType[i] = ENCODE_TYPE_TIM;
		m_iSnapType[i] = ENCODE_TYPE_SNAP_TIMER;
		for(int j = 0; j < N_OSD_TITLE; j++)
		{
			memset((void *)&m_OSDTitle[i][j], 0, sizeof(OSDTITLE));
		}
	}
	m_CoverNum = 0;
}

CEncode::~CEncode()
{
	CConfigLocation cCfgLocation;
	cCfgLocation.detach(this, ((TCONFIG_PROC)&CEncode::onConfigLocation));

	CConfigChannelTitle cCfgChannelTitle;
	cCfgChannelTitle.detach(this, ((TCONFIG_PROC)&CEncode::onConfigChannelTitle));
}

VD_BOOL CEncode::Start()
{
	int iConfigRet = 0;
	
	CAPTURE_EXT_STREAM	ExtStream;					/*!< 支持的流 */	
	GetExtCaps(&ExtStream);
	m_dwStreamMask = ExtStream.ExtraStream;
	CAPTURE_CAPS	caps;
	GetCaps(&caps);
	m_CoverNum = caps.CoverBlocks;
	
	m_cCfgColor.update();
	m_cCfgControl.update();
	m_cCfgEncode.update();
	m_cCfgVideoWidget.update();
	
	m_cCfgColor.attach(this, ((TCONFIG_PROC)&CEncode::onConfigVideoColor));
	m_cCfgControl.attach(this, ((TCONFIG_PROC)&CEncode::onConfigVideoControl));
	m_cCfgVideoWidget.attach(this, ((TCONFIG_PROC)&CEncode::onConfigVideoWidget));
	m_cCfgEncode.attach(this, ((TCONFIG_PROC)&CEncode::onConfigEncode));
	CConfigLocation cCfgLocation;
	cCfgLocation.update();
	cCfgLocation.attach(this, ((TCONFIG_PROC)&CEncode::onConfigLocation));

	CConfigChannelTitle cCfgChannelTitle;
	cCfgChannelTitle.update();
	cCfgChannelTitle.attach(this, ((TCONFIG_PROC)&CEncode::onConfigChannelTitle));

	// 设置一下编码时间
	onConfigLocation(cCfgLocation, iConfigRet);

	// 设置一下编码格式
	for (int i = 0; i < g_nCapture; i++)
	{
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30
		CONFIG_ENCODE& cfgNew = m_cCfgEncode.getConfig(i);
		AudioInSetVolume(i, cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn, 
					cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn); //单身道
				    //cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn); //双声道
#endif
		AppEncodeConfig(m_cCfgEncode.getConfig(i), m_cCfgEncode.getConfig(i), i);
	}
	
	// 设置视频覆盖和时间通道标题
	// fix op
	SYSTEM_TIME system_time;
	char szTime[32];
	VD_SIZE fontsize;

	SystemGetCurrentTime(&system_time);
	FormatTimeString(&system_time, szTime, FT_FULL_AMPM);
	int width = g_Locales.GetTextExtent(szTime);
	g_Locales.GetFontSize(&fontsize);
	for (int i = 0; i < g_nCapture; i++)
	{
		adjustTimeChannelRect(&m_cCfgVideoWidget.getConfig(i).TimeTitle, width, fontsize.h);
	}
	m_cCfgVideoWidget.commit();

	onConfigVideoWidget(m_cCfgVideoWidget, iConfigRet);

	// 设置颜色		
	onConfigVideoColor(m_cCfgColor, iConfigRet);
	// 设置控制参数		
	onConfigVideoControl(m_cCfgControl, iConfigRet);
	// 开启定时器
	m_EncodeTimer.Start(this,(VD_TIMERPROC)&CEncode::Monitor, 0, 1000);
	return TRUE;
}

void CEncode::onConfigEncode(CConfigEncode& cConfigEncode, int& iRet)
{
	CAPTURE_DSPINFO capinfo;
	GetDspInfo(&capinfo);	
	CAPTURE_CAPS	caps;
	GetCaps(&caps);
	_printd("CEncode::onConfigEncode");
	if (capinfo.bChannelMaxSetSync == 0)
	{
	   if(caps.Reserved&BITMSK(1))
	   	{
			int rebootflag=0;
			for (int i = 0; i < g_nCapture; i++)
			{
				CONFIG_ENCODE& cfgNew = cConfigEncode.getConfig(i);
				CONFIG_ENCODE& cfgOld = m_cCfgEncode.getConfig(i);
				//!利堡custdown产品HD1设置，需重启
				int oldimagesize1,newimagesize1,oldimagesize2,newimagesize2;
				oldimagesize1=cfgOld.dstMainFmt[0].vfFormat.iResolution;
				newimagesize1=cfgNew.dstMainFmt[0].vfFormat.iResolution;	
				oldimagesize2=cfgOld.dstExtraFmt[0].vfFormat.iResolution;
				newimagesize2=cfgNew.dstExtraFmt[0].vfFormat.iResolution;	
				//!利堡custdown产品主码流切换为HD1时需要重启
				if(oldimagesize1!=newimagesize1&&(newimagesize1==CAPTURE_SIZE_HD1||oldimagesize1==CAPTURE_SIZE_HD1))
				{
					rebootflag=1;
				}
				//!辅码流切换为CIF的时候重启设备生效
				if(oldimagesize2!=newimagesize2&&(newimagesize2==CAPTURE_SIZE_CIF||oldimagesize2==CAPTURE_SIZE_CIF))
				{
					rebootflag=1;
				}	
				if(rebootflag==1)
				{
			    	iRet |= CONFIG_APPLY_REBOOT;	
				}
			}
			//!重启后生效,这里只保存值，但不配置编码器
			if(rebootflag==1)
			{
					for (int i = 0; i < g_nCapture; i++)
					{
						CONFIG_ENCODE& cfgNew = cConfigEncode.getConfig(i);
						CONFIG_ENCODE& cfgOld = m_cCfgEncode.getConfig(i);
						cfgOld = cfgNew;
					}
					return;
			}
		}
		for (int i = 0; i < g_nCapture; i++)
		{
			CONFIG_ENCODE& cfgNew = cConfigEncode.getConfig(i);
			CONFIG_ENCODE& cfgOld = m_cCfgEncode.getConfig(i);
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30	
			if ((cfgNew.dstExtraFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn
				!= cfgOld.dstExtraFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn) 
				|| (cfgNew.dstExtraFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn
				!= cfgOld.dstExtraFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn) 
				)
			{
				AudioInSetVolume(i, cfgNew.dstExtraFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn, 
						    cfgNew.dstExtraFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn); //单身道
						    //cfgNew.dstExtraFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn);//双声道
			}
			if ((cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn
				!= cfgOld.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn) 
				|| (cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn
				!= cfgOld.dstMainFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn) 
				)
			{
				AudioInSetVolume(i, cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn, 
						    cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucLAudioVolumn); //单身道
						    //cfgNew.dstMainFmt[m_iEncodeType[i]].afFormat.ucRAudioVolumn); //双声道
			}
#endif
			AppEncodeConfig(cfgNew, cfgOld, i);
		}
	}
	else if (capinfo.bChannelMaxSetSync == 1)
	{
		int imaxcha = (int)capinfo.nMaxSupportChannel;

		for (int i = 0; i < (g_nCapture + imaxcha - 1) / imaxcha; i++)
		{
			VD_BOOL bStopped = FALSE;
			for (int j = 0; j < imaxcha; j++)
			{
				int curChannel = i * imaxcha + j;
				CONFIG_ENCODE& cfgNew = cConfigEncode.getConfig(curChannel);
				CONFIG_ENCODE& cfgOld = m_cCfgEncode.getConfig(curChannel);

				if (memcmp(&cfgNew, &cfgOld, sizeof(CONFIG_ENCODE)) != 0)
				{
					if (cfgNew.dstMainFmt[m_iEncodeType[curChannel]].vfFormat.iResolution
						!= cfgOld.dstMainFmt[m_iEncodeType[curChannel]].vfFormat.iResolution)
					{
						if (bStopped == FALSE)
						{
							// 关闭编码
							for (int k = 0; k < imaxcha; k++)
							{
								CDevCapture *pDevCapture = CDevCapture::instance(i * imaxcha + k);
								if (pDevCapture->GetState())
								{
									pDevCapture->Stop();
								}							
							}
							bStopped = TRUE;
						}


						// 设置一下新配置
						AppEncodeConfig(cfgNew, cfgOld, curChannel, (0xffffffff & ~BITMSK(CHL_MAIN_T)));

						// 当执行最后一个通道时，打开所有编码
						if (j == imaxcha - 1)
						{
							SystemSleep(500);
							for (int k = 0; k < imaxcha; k++)
							{
								CDevCapture *pDevCapture = CDevCapture::instance(i * imaxcha + k);
								if (pDevCapture->GetState())
								{
									pDevCapture->Start();
								}
							}
						}
					}
					else
					{
					#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30		
						if ((cfgNew.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn
							!= cfgOld.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn) 
							|| (cfgNew.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn
							!= cfgOld.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn) 
							)
						{
							AudioInSetVolume(i, cfgNew.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn, 
									    cfgNew.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn);//单身道
									   // cfgNew.dstExtraFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn); //双声道
						}
						if ((cfgNew.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn
							!= cfgOld.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn) 
							|| (cfgNew.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn
							!= cfgOld.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn) 
							)
						{
							AudioInSetVolume(i, cfgNew.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn, 
									    cfgNew.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucLAudioVolumn); //单身道
									    //cfgNew.dstMainFmt[m_iEncodeType[curChannel]].afFormat.ucRAudioVolumn); //双声道
						}
					#endif
						AppEncodeConfig(cfgNew, cfgOld, curChannel);
					}
				}
			}
		}
	}
}

void convertSize(VD_RECT& desRect, VD_RECT& srcRect, VD_SIZE& size)
{
	desRect.left = srcRect.left * size.w / 8192;
	desRect.top = srcRect.top * size.h / 8192;
	desRect.right = srcRect.right * size.w / 8192;
	desRect.bottom = srcRect.bottom * size.h / 8192;
}

void CompareCovers(VIDEO_WIDGET* pNew, VIDEO_WIDGET* pOld, VD_BOOL* bEncode, VD_BOOL* bPreview)
{
	if (memcmp(&pNew->rcRelativePos, &pOld->rcRelativePos, sizeof(VD_RECT))
		|| (pNew->rgbaBackground != pOld->rgbaBackground)
		|| (pNew->rgbaFrontground != pOld->rgbaFrontground))
	{
		*bEncode = TRUE;
		*bPreview = TRUE;				
	}

	if (pNew->bEncodeBlend != pOld->bEncodeBlend)
	{
		*bEncode = TRUE;
	}

	if (pNew->bPreviewBlend != pOld->bPreviewBlend)
	{
		*bPreview = TRUE;
	}
}


/*!
	\b Description		:	视频叠加时的操作，分别设置区域遮挡和设置时间通道标题\n
	\b Argument			:	[IN/OUT]参数列表
*/
void CEncode::onConfigVideoWidget(CConfigVideoWidget& cConfigVW, int& iRet)
{
	VD_RECT rect;
	for (int i = 0; i < g_nCapture; i++)
	{
		CONFIG_VIDEOWIDGET& newCfg = cConfigVW.getConfig(i);
		CONFIG_VIDEOWIDGET& oldCfg = m_cCfgVideoWidget.getConfig(i);
		VD_BOOL bCompared = TRUE;

		if (&cConfigVW == &m_cCfgVideoWidget)
		{
			//同一个配置，直接生效
			bCompared = FALSE;
		}

		VD_BOOL bEncode;
		VD_BOOL bPreview;
		if (bCompared)
		{
			bEncode = FALSE;
			bPreview = FALSE;
		}
		else
		{
			bEncode = TRUE;
			bPreview = TRUE;
		}

		//设置区域遮盖
		for(int j = 0; j < m_CoverNum; j++)
		{
			if (bCompared)
			{
				bEncode = FALSE;
				bPreview = FALSE;

				CompareCovers(&newCfg.dstCovers[j], &oldCfg.dstCovers[j], &bEncode, &bPreview);
			}

			if (bEncode)
			{
				VD_SIZE size;
				//以P制和N制下分辨率相同的1080P作为换算因子，
				//防止因制式不同导致的osd和区域遮盖位置不准确 modify By Le0 20140805
				if (GetImageSize(&size, CAPTURE_SIZE_1080P))
				{
					return;
				}
	
				convertSize(rect, newCfg.dstCovers[j].rcRelativePos, size);
				rect.left = ((rect.left + 2) / 4) * 4;    //四舍五入
				rect.right = ((rect.right + 2) / 4) * 4;
			
				CDevCapture::instance(i)->SetCover(&rect, 0, newCfg.dstCovers[j].bEncodeBlend, j);
			}
		}

		//设置时间标题和通道标题
		if (bCompared)
		{
			bEncode = FALSE;
			bPreview = FALSE;

			CompareCovers(&newCfg.TimeTitle, &oldCfg.TimeTitle, &bEncode, &bPreview);
			CompareCovers(&newCfg.ChannelTitle, &oldCfg.ChannelTitle, &bEncode, &bPreview);
			CompareCovers(&newCfg.dstMarkers[0], &oldCfg.dstMarkers[0], &bEncode, &bPreview);
		}
		
		oldCfg = newCfg;

		if (bEncode)
		{
			SetTimeChannelTitle(OSD_OVERLAY_TITLE, i);
			SetTimeChannelTitle(OSD_OVERLAY_TIME, i);
		}
	}
	m_cCfgVideoWidget.update();
}

/*!
	\b Description		:	图像控制
	\b Argument			:	CConfigVideoColor *pConfig, int& iRet
	\param[in]	pConfig	:	更新配置的类
*/
void CEncode::onConfigVideoControl(CConfigVideoControl& cConfigVCtrl, int& iRet)
{
	VIDEO_CONTROL cfg;
	CONFIG_VIDEOCONTROL& cfg_videocontrol = cConfigVCtrl.getConfig(0);
	
	cfg.AutoColor2BW = (uchar)cfg_videocontrol.bAutoColor2BW;
	cfg.Backlight = (uchar)cfg_videocontrol.bBacklight;
	cfg.Exposure = (uchar)cfg_videocontrol.nExposure;
	cfg.Mirror =(uchar)cfg_videocontrol.bMirror;

	
	CDevVideo::instance()->SetControl( 0, &cfg );
}

/*!
	\b Description		:	颜色配置变化时对应操作\n
	\b Argument			:	CConfigVideoColor *pConfig, int& iRet
	\param[in]	pConfig	:	更新配置的类	
*/
void CEncode::onConfigVideoColor(CConfigVideoColor& cConfigVC, int& iRet)
{
	SYSTEM_TIME system_time;
	SystemGetCurrentTime(&system_time);

	VIDEOCOLOR *pColor = NULL;

	for (int i = 0 ; i < g_nCapture; i++)
	{
		CONFIG_VIDEOCOLOR& cfgNew = cConfigVC.getConfig(i);
		CONFIG_VIDEOCOLOR& cfgOld = m_cCfgColor.getConfig(i);

		for (int j = 0; j < N_COLOR_SECTION; j++)
		{	
			pColor = &cfgNew.dstVideoColor[j];

			if (((pColor->iEnable == TRUE) && (m_CurColorIndex[i] != j)) 
				|| (memcmp(&cfgNew, &cfgOld, sizeof(CONFIG_VIDEOCOLOR)) != 0))
			{
				int time1 = pColor->TimeSection.startHour * 60 + pColor->TimeSection.startMinute;
				int time2 = pColor->TimeSection.endHour * 60 + pColor->TimeSection.endMinute;
				int systime = system_time.hour * 60 + system_time.minute;
				if (((time1 < time2) && (time1 <= systime) && (time2 > systime))
					|| ((time1 > time2) && ((time1 <= systime) || (time2 > systime))))
				{
					m_CurColorIndex[i] = j;
					CDevVideo::instance()->SetColor(i, pColor->dstColor);
					break;
				}
			}
		}
		cfgOld = cfgNew;
	}
}


void CEncode::onConfigLocation(CConfigLocation& cConfigLocation, int &iRet)
{
	SYSTEM_TIME curtime;
	SystemGetCurrentTime(&curtime);

	for (int i = 0; i < g_nCapture; i++)
	{
		if (!CDevCapture::instance(i)->SetTime(&curtime))
		{
			trace("CEncode::onConfigLocation error when set cha[%d] time !!!!!\n", i + 1);
		}
	}
}

/*!
	\b Description		:	10分钟设置一下颜色，码流为0重启（连续60次）\n
	\b Argument			:	PARAM arg
*/
void CEncode::Monitor(uint arg)
{
	int iConfigRet = 0;

	static int SynIntervalTime = CAPTURE_RESET_INTERVAL + 1;
	static int SetIntervalTime = CAPTURE_TIME_INTERVAL + 1;
	
	// 处理半小时校准一下DSP时间，防止时间过长导致DSP时间和系统时间有偏差
	SetIntervalTime--;
	if (SetIntervalTime == 0)
	{
		CConfigLocation cCfgLocation;
		cCfgLocation.update();
		onConfigLocation(cCfgLocation, iConfigRet);
		SetIntervalTime = CAPTURE_TIME_INTERVAL;
	}
	
	// 每隔5秒钟让DSP自行判断一下DSP状态，如出错，自己重启
	SynIntervalTime--;
	if (SynIntervalTime == 0)
	{
		CDevCapture::Synchronize();
		SynIntervalTime = CAPTURE_RESET_INTERVAL;
	}

	// 看一下码流情况，如果60秒内都没有码流，通知前面板，重启机器	
	for (int i = 0; i < g_nCapture; i++)
	{
		CDevCapture *pDevCap = CDevCapture::instance(i);
		
		CGuard   l_cGuard(m_ResetCapMutex);
		if (pDevCap->GetState(CHL_MAIN_T) && (pDevCap->GetBitRate() == 0) && (g_nLogicNum != 5))
		{
			// 目前没有考虑上限
			m_ResetCapTimes[i]++;
	#ifdef LOG_APPEND_CODERABORT
			if(m_ResetCapTimes[i] >= 60)
			{
				g_Log.Append(LOG_CODER_BREAKDOWN);
			}
	#endif
		}
		else
		{
	#ifdef LOG_APPEND_CODERABORT
			if(m_ResetCapTimes[i] >= 60)
			{
				g_Log.Append(LOG_CODER_BREAKDOWN_RESUME);
			}
	#endif
			m_ResetCapTimes[i] = 0;
		}
	}
	//设置颜色
	onConfigVideoColor(m_cCfgColor, iConfigRet);
}

uint CEncode::costPower(int iChannel)
{
	uint totalpower = 0;
	CDevCapture* pDevCapture = CDevCapture::instance(iChannel);
		
	const CAPTURE_FORMAT *pFormat = NULL;
	
	for (int i = CHL_MAIN_T; i <= CHL_4RTH_T; i++)
	{
		pFormat = pDevCapture->GetFormat(i);
		if (pFormat->AVOption & 1)
		{
			//保留可能开启的视频
			totalpower += calculatePower(pFormat->ImageSize, pFormat->FramesPerSecond);
		}
	}

	return totalpower;
}


void CEncode::setEncodeType(int irecType, int iChannel)
{
	_printd("CEncode::setEncodeType");
	if ((iChannel < 0) || (iChannel >= g_nLogicNum))
	{
		return;
	}
	_printd("CEncode::setEncodeType");
	int oldRecType = m_iEncodeType[iChannel];

	switch(irecType)
	{
	default:
	case REC_TIM:
	case REC_CRD:
	case REC_MAN:
		m_iEncodeType[iChannel] = ENCODE_TYPE_TIM;
		break;
#if defined(ENC_CHANGE_PARAM)
	case REC_MTD:
		m_iEncodeType[iChannel] = ENCODE_TYPE_MTD;
		break;
	case REC_ALM:
		m_iEncodeType[iChannel] = ENCODE_TYPE_ALM;
		break;
#endif
	}

	//为了调用接口，更改对应的值
	CONFIG_ENCODE newcfg = m_cCfgEncode.getConfig(iChannel);

#if defined(ENC_CHANGE_PARAM) && ENC_CHANGE_PARAM == 2
	if ((m_iEncodeType[iChannel] == ENCODE_TYPE_ALM) 
		|| (m_iEncodeType[iChannel] == ENCODE_TYPE_MTD))
	{
		//需要修改newcfg的值
		autoAdjustFrame(newcfg, iChannel);
	}
#endif
	
	CONFIG_ENCODE& oldcfg = m_cCfgEncode.getConfig(iChannel);

	oldcfg.dstMainFmt[m_iEncodeType[iChannel]] = newcfg.dstMainFmt[oldRecType];

	AppEncodeConfig(newcfg, oldcfg, iChannel);
}

int CEncode::getEncodeType(int iChannel)
{
	if ((iChannel < 0) || (iChannel >= g_nCapture))
	{
		return -1;
	}

	return m_iEncodeType[iChannel];
}

VD_BOOL CEncode::IsAlive()
{
	CGuard l_cGuard(m_ResetCapMutex);
	for (int i = 0; i < g_nCapture; i++)
	{
		if (m_ResetCapTimes[i] >= 60)
		{
			tracef("Sorry, the ch[%d] has no data !!!!!, Please Check DSP\n", i + 1);
			return FALSE; 
		}
	}
	return TRUE;
}


/*!
	\b Description		:	设置时间通道标题，由于辅码流的标题DSP会自动调节，不用管它\n
	\b Argument			:	int index, int iChannel
	\param	index		:	标题选择，0：时间标题；1：通道标题
	\param	iChannel	:	通道号	
*/
void CEncode::SetTimeChannelTitle(int index, int iChannel)
{
	VD_SIZE size;
	int width = 0;
	char *pStr = NULL;

	if ((iChannel < 0) || (iChannel >= g_nCapture))
	{
		return;
	}
	//m_cCfgVideoWidget.update();
	//CONFIG_VIDEOWIDGET& cfgVW = m_cCfgVideoWidget.getConfig(iChannel);
	const CONFIG_VIDEOWIDGET& cfgVW = CConfigVideoWidget::getLatest(iChannel);
	const VIDEO_WIDGET * pVideoWidget = NULL;
	if (OSD_OVERLAY_TIME == index)
	{
		//获得时间长度
		SYSTEM_TIME system_time;
		char szTime[32];

		SystemGetCurrentTime(&system_time);
		FormatTimeString(&system_time, szTime, FT_FULL_AMPM);
		width = g_Locales.GetTextExtent(szTime);
		pVideoWidget = &cfgVW.TimeTitle;
	}
	else if (OSD_OVERLAY_TITLE == index)
	{
		//获得通道名称长度和字符串
		pStr = (char *)&CConfigChannelTitle::getLatest(iChannel).strName;
		width = g_Locales.GetTextExtent(pStr);
		pVideoWidget = &cfgVW.ChannelTitle;
	}   
	else 
	{
	    return ;
	}

	//if (GetImageSize(&size, CAPTURE_SIZE_D1))
	//以1080P为转换因子，去除P制和N制的区别 modify by Le0 20140805
	if (GetImageSize(&size, CAPTURE_SIZE_1080P))
	{
		return;
	}

	VD_RECT rect;
	rect.left = pVideoWidget->rcRelativePos.left * size.w / 8192;
	rect.top = pVideoWidget->rcRelativePos.top * size.h / 8192;
	rect.right = pVideoWidget->rcRelativePos.right* size.w / 8192;
	rect.bottom= pVideoWidget->rcRelativePos.bottom* size.h / 8192;
	
	if (rect.left + width > size.w)
	{
		rect.left = size.w - width;
	}

	if (rect.top + 24 > size.h)
	{
		rect.top = size.h - 24;
	}

	int x = rect.left;
	int y =  rect.top;
	CAPTURE_TITLE_PARAM TimeTitle;
	VD_RECT Rect;

	TimeTitle.index = index;
	TimeTitle.enable = pVideoWidget->bEncodeBlend;
	TimeTitle.x = (x/8)*8;//x % 2 ? x + 1 : x;
	TimeTitle.y = (y/2)*2;//y % 8 ? y - y % 8 : y;.
	TimeTitle.width = (ushort)(width % 8 ? width + 8 - width % 8 : width);
	TimeTitle.height = 24;
	TimeTitle.fg_color = pVideoWidget->rgbaFrontground;
	TimeTitle.bg_color = pVideoWidget->rgbaBackground;

	Rect.left = 0,
	Rect.top = 0;
	Rect.right = TimeTitle.width;
	Rect.bottom = TimeTitle.height;
	CDevCapture *pDevCap = CDevCapture::instance(iChannel);
	if (pDevCap)
	{
		pDevCap->SetTitle(&TimeTitle, &Rect, pStr);
	}
}

/*!
	\b Description		:	设置叠加信息标题\n
	主要做一层把设置的D1分辨率转化成当前分辨率, 并增加调用叠加在预览画面上接口\n
	注意: 如果要叠加桌面，字符串里必须有'\t'字符来区分标题和内容
	\b Argument			:	CAPTURE_TITLE_PARAM *pTitle, PCRECT pRect, PCSTR str, int iChannel
	\param	pTitle		:	标题属性设置，注意里面的x,y都是相对于CIF格式而言的	
*/
void CEncode::SetInfoTitle(CAPTURE_TITLE_PARAM *pTitle, VD_PCRECT pRect, VD_PCSTR str, int iChannel)
{
	//SIZE cursize;
	VD_SIZE size;
	// 只处理叠加信息，不处理时间通道标题
	if ((iChannel < 0) || (iChannel >= g_nCapture) || (pTitle->index < 2))
	{
		return;
	}

	GetImageSize(&size, CAPTURE_SIZE_D1);
	if (pTitle->x + pTitle->width > size.w)
	{
		pTitle->x = size.w - pTitle->width < 0 ? 0 : size.w - pTitle->width;
	}
	
	if (pTitle->y + pTitle->height > size.h)
	{
		pTitle->y = size.h - pTitle->height < 0 ? 0 : size.h - pTitle->height;
	}

	pTitle->x = (pTitle->x/8)*8;//pTitle->x % 2 ? pTitle->x + 1 : pTitle->x;
	pTitle->y = (pTitle->y/2)*2;//pTitle->y % 8 ? pTitle->y - pTitle->y % 8 : pTitle->y;

	CDevCapture *pDevCap = CDevCapture::instance(iChannel);
	if (pDevCap)
	{
#ifdef ENC_SHOW_SMALL_FONT
		pDevCap->SetTitle(pTitle, pRect, str, FONTSIZE_SMALL);
#else
		pDevCap->SetTitle(pTitle, pRect, str);
#endif
	}
	
	// 因为DevCapture叠加索引是从2开始，
	// 为了在桌面上对齐显示，在字符串中必然要有\t表示分隔，
	// 且标题不能超过24个字节(含\0)， 内容不能超过32个字节(含\0)
	char content[24 + 32];
	memset(content, 0, 56);
	VD_BOOL bBreak = FALSE;

	if ((NULL != str) && (pTitle->enable == TRUE))
	{
		char *pdes = content;
		char *psrc = (char *)str;
		while (*psrc != '\0')
		{
			if (*psrc != '\t')
			{
				*pdes++ = *psrc;
			}
			else
			{
				pdes = &content[24];
				bBreak = TRUE;
			}
			psrc++;
		}

		// 防止被覆盖
		if (bBreak)
		{
			content[23] = '\0';
			content[55] = '\0';
		}
	}
}

//此处为了提高效率,重新修改一下
void CEncode::setOSDTitle(int index, int layout, VD_BOOL enable, VD_PCSTR str, int iChannel)
{
	CAPTURE_CAPS tmpCapCaps;
	GetCaps(&tmpCapCaps);
	if ((iChannel < 0) || (iChannel >= g_nCapture) || (index >= (tmpCapCaps.TitleCount - 2))
		|| (layout > OSD_TOP_RIGHT_CORNER) || (layout < OSD_BOTTOM_LEFT_CORNER)
		||(index >= N_OSD_TITLE) )
	{
		return;
	}
	
	if (enable)
	{
		if (NULL == str)
		{
			return;
		}
		
		int length = strlen(str);
		if (length >= 32)
		{
			trace("the length of string[%d] is out of range\n", index);
			return;
		}
		memset(&m_OSDTitle[iChannel][index], 0, sizeof(OSDTITLE));
		memcpy(m_OSDTitle[iChannel][index].content, str, length);
		m_OSDTitle[iChannel][index].content[length] = '\0';
	}
	m_OSDTitle[iChannel][index].enable = enable;
	m_OSDTitle[iChannel][index].layout = layout;


	int count = index;
    int total = 8;
	CAPTURE_TITLE_PARAM Title;
	VD_RECT Rect;
	int	length = 0;

	OSDTITLE osdTitleTmp;
	memset(&osdTitleTmp, 0, sizeof(osdTitleTmp));
	memcpy(&osdTitleTmp, &m_OSDTitle[iChannel][index], sizeof(OSDTITLE));
	
	char strTmp[256];
	memset(strTmp, 0, sizeof(strTmp));
	for(int i = 0, j = 0; i < (int)MIN(strlen(m_OSDTitle[iChannel][index].content), sizeof(strTmp)); i++, j++)
	{
		if(m_OSDTitle[iChannel][index].content[i] == '\t')
		{
			strcpy(&strTmp[j], "   ");
			j += 2;
		}
		else
		{
			strTmp[j] =m_OSDTitle[iChannel][index].content[i];
		}
	}

	memcpy(osdTitleTmp.content, strTmp, strlen(strTmp));
	OSDTITLE *osdTitle = &osdTitleTmp;
		
	
#ifdef ENC_SHOW_SMALL_FONT
	length = g_Locales.GetTextExtent(osdTitle->content, strlen(osdTitle->content), FS_SMALL);
#else
	length = g_Locales.GetTextExtent(osdTitle->content);
#endif

	//printf("length**********************:%d\n", length);
	//保证是8的倍数
	if (length % 8)
	{
		length += 8 - length % 8;
	}

	//获得当前的分辨率，只负责处理主码流的标题
	VD_SIZE curimgsize;
#ifdef VN_IPC
	CONFIG_ENCODE& cfgEncode = m_cCfgEncode.getConfig(iChannel);
	if (GetImageSize(&curimgsize, cfgEncode.dstMainFmt[m_iEncodeType[iChannel]].vfFormat.iResolution))
	{
		return;
	}
#endif
	//int interval = 24 * MAX(curimgsize.h / cifsize.h, 1);
	int interval = 24;

	int x = 0;
	int y = 0;

	switch(osdTitle->layout)
	{
	case OSD_BOTTOM_LEFT_CORNER://bottom left corner 
		x = 8;
		y = curimgsize.h - interval * (total - count - 3);
		break;
	case OSD_BOTTOM_CENTER://bottom center
		x = (curimgsize.w - length) /2;
		y = curimgsize.h - interval * (total - count + 1);
		break;
	case OSD_BOTTOM_RIGHT_CORNER://bottom right corner
		x = curimgsize.w - length - 16;
		y = curimgsize.h - interval * (total - count - 5);
		break;
	case OSD_LEFT_CENTER://left center
		x = 8;
		y = (curimgsize.h - 24) /2 - interval * ((total+1)/2 - count + 1);
		break;
	case OSD_CENTER: //center
		x = (curimgsize.w - length) / 2;
		y = (curimgsize.h - 24) /2 - interval * ((total+1)/2 - count + 1);
		break;
	case OSD_RIGHT_CENTER: //right center
		x = curimgsize.w - length - 16;
		y = (curimgsize.h - 24) /2 - interval * ((total+1)/2 - count + 1);
		break;
	case OSD_TOP_LEFT_CORNER://top left corner
		x = 8;
		y = interval * (count + 1);
		break;
	case OSD_TOP_CENTER: //top center
		x =  (curimgsize.w - length) / 2 + 10;
		y = interval * count;
		break;
	case OSD_TOP_RIGHT_CORNER:
		x = curimgsize.w - length - 16;
		y = interval * count;
		break;
	default:
		break;
	}

	Title.index = index + 2;
	Title.enable = osdTitle->enable;
	Title.x = (x/8)*8;//x % 2 ? x - 1 : x;
	Title.y = (y/2)*2;//y % 8 ? y - y % 8 : y;
	Title.width = (ushort)(length % 8 ? length + 8 - length % 8 : length);

	VD_SIZE fontsize;
#ifdef ENC_SHOW_SMALL_FONT
	g_Locales.GetFontSize(&fontsize, FONTSIZE_SMALL);
#else
	g_Locales.GetFontSize(&fontsize);
#endif	
	Title.height = fontsize.h;
	Title.fg_color = VD_RGBA(255, 255, 255, 0);
	Title.bg_color = VD_RGBA(0, 0, 0, 128);

	Rect.left = 0;
	Rect.top = 0;
	Rect.right = Title.width;
	Rect.bottom = Title.height;


	/*
	// 因为DevCapture叠加索引是从2开始，
	// 为了在桌面上对齐显示，在字符串中必然要有\t表示分隔，
	// 且标题不能超过24个字节(含\0)， 内容不能超过32个字节(含\0)
	*/
	char content[24 + 32];
	memset(content, 0, 56);
	VD_BOOL bBreak = FALSE;

	if (Title.enable == TRUE)
	{
		char *pdes = content;
		char *psrc = (char *)m_OSDTitle[iChannel][index].content;
		while (*psrc != '\0')
		{
			if (*psrc != '\t')
			{
				*pdes++ = *psrc;
			}
			else
			{
				pdes = &content[24];
				bBreak = TRUE;
			}
			psrc++;
		}

		// 防止被覆盖
		if (bBreak)
		{
			content[23] = '\0';
			content[55] = '\0';
		}
	}
}

void CEncode::ClearInfoTitle(int iChannel)
{
	CAPTURE_CAPS tmpCapCaps;
	GetCaps(&tmpCapCaps);

	for (int i = 0; i < tmpCapCaps.TitleCount - 2; i++)
	{
#ifdef COM_SUPPORT_GPS
		setOSDTitle(i, OSD_BOTTOM_RIGHT_CORNER, FALSE, NULL, iChannel);
#else
		setOSDTitle(i, OSD_BOTTOM_LEFT_CORNER, FALSE, NULL, iChannel);
#endif
	}
}

void CEncode::CompareEncode(CONFIG_ENCODE& cfgNew, CONFIG_ENCODE& cfgOld, 
							uint* dwStreamChanged, int iChannel)
{
	MEDIA_FORMAT *pnewFormat = NULL;
	MEDIA_FORMAT *poldFormat = NULL;

	for (int ii = 0; ii < CHL_FUNCTION_NUM; ii++)
	{
		if (m_dwStreamMask & BITMSK(ii))
		{
			if (ii == CHL_MAIN_T)
			{
				pnewFormat = &cfgNew.dstMainFmt[m_iEncodeType[iChannel]];
				poldFormat = &cfgOld.dstMainFmt[m_iEncodeType[iChannel]];
			}
			else if (ii == CHL_JPEG_T)
			{
				pnewFormat = &cfgNew.dstSnapFmt[m_iSnapType[iChannel]];
				poldFormat = &cfgOld.dstSnapFmt[m_iSnapType[iChannel]];
			}
			else
			{
				pnewFormat = &cfgNew.dstExtraFmt[ii - CHL_2END_T];
				poldFormat = &cfgOld.dstExtraFmt[ii - CHL_2END_T];
			}

			if ((pnewFormat->vfFormat.iCompression != poldFormat->vfFormat.iCompression)
				|| (pnewFormat->vfFormat.iResolution != poldFormat->vfFormat.iResolution) 
				|| (pnewFormat->vfFormat.iBitRateControl != poldFormat->vfFormat.iBitRateControl)				
				|| (pnewFormat->vfFormat.iQuality != poldFormat->vfFormat.iQuality)
				|| (pnewFormat->vfFormat.nFPS != poldFormat->vfFormat.nFPS)
				|| (pnewFormat->vfFormat.iGOP != poldFormat->vfFormat.iGOP) /* 当gop发生改变时，也要重新设置 */
				|| (pnewFormat->bVideoEnable != poldFormat->bVideoEnable)
				|| (pnewFormat->bAudioEnable != poldFormat->bAudioEnable)
				|| (pnewFormat->vfFormat.nBitRate != poldFormat->vfFormat.nBitRate))
			{
				*dwStreamChanged |= BITMSK(ii);
			}
		}
	}
}

void  CEncode::AppEncodeConfig(CONFIG_ENCODE& cfgNew, CONFIG_ENCODE& cfgOld, int iChannel, uint dwStreamRestart)
{
	uint dwStreamChanged = 0;
	_printd("CEncode::AppEncodeConfig");
	if (&cfgNew == &cfgOld)
	{
		//初始化时，由于Capture先设置，再设置区域遮挡，所以时间通道标题可先不设
		dwStreamChanged |= m_dwStreamMask; //第一次执行，由于录像模块未注册，不会打包
	}
	else
	{
		CompareEncode(cfgNew, cfgOld, &dwStreamChanged, iChannel);
	}
	
	// 把所有码流相关的通道全部设一下
	if (dwStreamChanged)
	{
		CAPTURE_FORMAT capfmt;
		memset(&capfmt, 0, sizeof(CAPTURE_FORMAT));

		MEDIA_FORMAT *pFormat = NULL;
		for (int ii = 0; ii < CHL_FUNCTION_NUM; ii++)
		{
			if (dwStreamChanged & BITMSK(ii))
			{
				if (ii == CHL_MAIN_T)
				{
					pFormat = &cfgNew.dstMainFmt[m_iEncodeType[iChannel]];
				}
				else if (ii == CHL_JPEG_T)
				{
					pFormat = &cfgNew.dstSnapFmt[m_iSnapType[iChannel]];
				}
				else 
				{
					pFormat = &cfgNew.dstExtraFmt[ii - CHL_2END_T];
				}
				
				capfmt.Compression = pFormat->vfFormat.iCompression;
				capfmt.ImageSize = pFormat->vfFormat.iResolution;
				capfmt.BitRateControl = pFormat->vfFormat.iBitRateControl;
				capfmt.ImageQuality = pFormat->vfFormat.iQuality;
				capfmt.FramesPerSecond = pFormat->vfFormat.nFPS;
				capfmt.Gop = pFormat->vfFormat.iGOP;
				capfmt.AVOption = pFormat->bVideoEnable;
				
				//此处需要把码流设置下去
				capfmt.BitRate = pFormat->vfFormat.nBitRate;
				if (pFormat->bAudioEnable)
				{
					capfmt.AVOption |= BITMSK(1);
				}
				CDevCapture *pDevCapture = CDevCapture::instance(iChannel);

				if( ii == CHL_JPEG_T )
				{
					CConfigSnap cCfgSnap;
					cCfgSnap.update();
					CONFIG_SNAP& cfgNew = cCfgSnap.getConfig(iChannel);

					int iQuality = 0;
					switch(cfgNew.SnapQuality)
					{
						case 6:
							iQuality = 0;
							break;
						case 5:
							iQuality = 1;
							break;
						case 4:
							iQuality = 2;
							break;
						case 3:
							iQuality = 3;
							break;
						case 2:
							iQuality = 4;
							break;
						case 1:
							iQuality = 5;
							break;
						default:
							iQuality = 2;

					}

					capfmt.ImageQuality = iQuality;
					//trace("iChannel:%d, SnapInterval:%d\n", iChannel, cfgNew.SnapInterval);
					if(cfgNew.SnapInterval > 1000*120)
					{
						int tmp = cfgNew.SnapInterval/1000;
						capfmt.FramesPerMinute = tmp/60;
						capfmt.FramesPerSecond = tmp%60;
					}
					else if( cfgNew.SnapInterval > 1000 )
					{
						capfmt.FramesPerSecond = (128+cfgNew.SnapInterval/1000);
					}
					else
					{
						capfmt.FramesPerSecond = 1000/cfgNew.SnapInterval;
					}
				}
				pDevCapture->SetFormat(&capfmt, ii);

				if (dwStreamRestart & BITMSK(ii))
				{
					if (pDevCapture->GetState(ii))
					{
						pDevCapture->Stop(ii);
						pDevCapture->Start(ii);
					}
				}
			}
		}
		//SetTimeChannelTitle(0, iChannel);
		//SetTimeChannelTitle(1, iChannel);
	}

	cfgOld = cfgNew;
}

void adjustTimeChannelRect(VIDEO_WIDGET* pWidget, int width, int height)
{
//删除理由:
// 1. D1的设备，在开机时会被按照CIF宽度 调整标题的位置，比如，时间标题最右被调整到当中。
// 2. 调用这个函数只有在开机和修改标题的名称的时候,不够普适。
// 3. 此处已经和SetTimeChannelTitle的内容重复。这个是每次调整坐标位置都会调用的函数
	return;

	VD_SIZE size;
	GetImageSize(&size, CAPTURE_SIZE_CIF);

	int adjustWidth  = width * 8192 / size.w;
	int adjustHeight = height * 8192 / size.h;

	if (pWidget->rcRelativePos.left > 8192 / 2)
	{
		if (pWidget->rcRelativePos.left  +  adjustWidth> 8192)
		{
			pWidget->rcRelativePos.right = 8192;
			pWidget->rcRelativePos.left = pWidget->rcRelativePos.right - adjustWidth;
		}
	}
	else
	{
		pWidget->rcRelativePos.right = MIN(pWidget->rcRelativePos.left + adjustWidth, 8192);
	}

	if (pWidget->rcRelativePos.top > 8192 / 2)
	{
		if (pWidget->rcRelativePos.top  +  adjustHeight > 8192)
		{
			pWidget->rcRelativePos.bottom= 8192;
			pWidget->rcRelativePos.top = pWidget->rcRelativePos.bottom - adjustHeight;
		}
	}
	else
	{
		pWidget->rcRelativePos.bottom = MIN(pWidget->rcRelativePos.top + adjustHeight, 8192);
	}
}
void CEncode::onConfigChannelTitle(CConfigChannelTitle& config, int& ret)
{
	VD_SIZE fontsize;
	char * pStr = NULL;
	int width;
	
	for (int i = 0; i < g_nCapture; i++)
	{
		pStr = (char *)&config.getConfig(i).strName;
		width = g_Locales.GetTextExtent(pStr);
		g_Locales.GetFontSize(&fontsize);
		adjustTimeChannelRect(&m_cCfgVideoWidget.getConfig(i).ChannelTitle, width, fontsize.h);
	}
	m_cCfgVideoWidget.commit();	

	//因为现在标题宽度相同时不设置，所以设置一次, added by billhe at 2009-9-29
#ifdef VN_IPC
    //修改IPC修改通道名称不生效
	onConfigVideoWidget(m_cCfgVideoWidget, width);
#endif
}

void CEncode::autoAdjustFrame(CONFIG_ENCODE& cfgEncode, int iChannel)
{
	/*该函数用于自动调节报警或则是动检时候的帧率*/
	int iMaxFrame = 0;	//能达到的最大帧率
	int supportChannel;		//DSP支持的通道数
	CAPTURE_DSPINFO dspinfo;
	GetDspInfo(&dspinfo);	
	supportChannel = dspinfo.nMaxSupportChannel;
	
	//获得iChannel的分辨率
	VD_SIZE size;
	GetImageSize(&size, cfgEncode.dstMainFmt[m_iEncodeType[iChannel]].vfFormat.iResolution);
	
	if (1 == dspinfo.bChannelMaxSetSync)
	{
		iMaxFrame = dspinfo.nMaxEncodePower / supportChannel / size.h * size.w;
	}
	else
	{
		int totalpower = 0;		//除iChannel外的其余通道的编码能力之和
		for (int i = 0; i < supportChannel; i++)
		{
			if (i != (iChannel % supportChannel))
			{
				//计算除iChannel以外的通道的编码能力之和
				totalpower += costPower((iChannel / supportChannel) * supportChannel + i);
			}
		}

		iMaxFrame = (dspinfo.nMaxEncodePower - totalpower) / (size.h * size.w); 
	}
	if (iMaxFrame > getMaxFrame())
	{
		iMaxFrame = getMaxFrame();
	}
	cfgEncode.dstMainFmt[m_iEncodeType[iChannel]].vfFormat.nFPS = iMaxFrame;
}


void CEncode::getCapFormat(CAPTURE_FORMAT format[])
{
	 CConfigEncode cCfgEncode;
	 cCfgEncode.update();
	 //cCfgEncode.recall();
	 CONFIG_ENCODE *pEncode = NULL;
	 for(int i =0; i<g_nLogicNum; i++)
	 {
		pEncode = &cCfgEncode.getConfig(i);
		format[i].AVOption = pEncode->dstMainFmt[ENCODE_TYPE_TIM].bVideoEnable;
		//解决录像回放播放文件后，没有音频的问题added by wyf on 20100205
		if(pEncode->dstMainFmt[ENCODE_TYPE_TIM].bAudioEnable)
		{
		  format[i].AVOption |= BITMSK(1); 
		}
		format[i].BitRate = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nBitRate;
		format[i].BitRateControl = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iBitRateControl;
		format[i].Compression = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iCompression;
		format[i].FramesPerSecond = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
		format[i].ImageQuality = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality;
		format[i].ImageSize = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
		format[i].Gop = pEncode->dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iGOP;
	 }
}

