#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <errno.h>  
#include <string.h>  
#include <dirent.h>  
#include <errno.h>  
#include <sys/stat.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <time.h> 
#include "Functions/MP3Play.h"
#include "Configs/ConfigPlay.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "System/File.h"

#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "APIs/CommonBSP.h"
#include "APIs/Audio.h"

//如果打开这个宏,全局声音大小使用同一配置
//若关闭，则播放音乐和其它场景 分为两个配置，设置音量时只会改变播放音乐时的音量
#define VOLUME_UNIFY 	


typedef struct stat Stat_t;  
#define StatFunc(x,y) stat(x,y)

using namespace std;

#define MP3FILENAME "/tmp/play.mp3"

static FILE *errStream = stderr;
static int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  

static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) 
	{  
        fwrite(ptr, nmemb, size,  fp); 
		fclose(fp);
    }  
    printf("download size[%dkb]\n",len/1024);      
    return written;  
}  




PATTERN_SINGLETON_IMPLEMENT(MP3Play);

MP3Play::MP3Play():CThread("MP3Play", TP_NET),m_iPlayStatus(0),m_iDownLoadStatus(0)
{	
	_printd("MP3Play");

	m_iVolume			= 0;
	m_iDefaultVolume 	= 0;

	m_iPriority			= 0;
}
MP3Play::~MP3Play()
{
	_printd("MP3Play");
	curl_global_cleanup();
}
VD_INT32 MP3Play::Init()
{
	curl_global_init(CURL_GLOBAL_ALL);

#if 0
	CConfigVoicePlay cCfgVoicePlay;

	cCfgVoicePlay.update();
	CONFIG_VOICEPLAYPARAM &config	= cCfgVoicePlay.getConfig();
	m_iVolume =  config.iVolume;

	AudioOutGetVol(&m_iDefaultVolume);
#endif
	return 0;
}
VD_INT32 MP3Play::Start()
{
	Init();
	CreateThread();
	return 0;
}
VD_INT32 MP3Play::Stop()
{
	_printd("MP3Play :: Stop ");
	return 0;
}



//m_iPlayStatus : 0  为未播放  1：下载中  2：播放失败超时
//3: 播放中  4：播放结束

void MP3Play::ThreadProc()
{
	sleep(5);
#if 1
	CConfigVoicePlay cCfgVoicePlay;

	cCfgVoicePlay.update();
	CONFIG_VOICEPLAYPARAM &config	= cCfgVoicePlay.getConfig();
	m_iVolume =  config.iVolume;
#ifdef VOLUME_UNIFY
	AudioOutSetVol(m_iVolume);
#else
	AudioOutGetVol(&m_iDefaultVolume);
#endif

#endif
	while (m_bLoop) 
	{
 		if(0 == m_iDownLoadStatus)
		{		
			if(3 == m_iPlayStatus && 0 == AudioPlayAudioStatus())
			{
				m_iPlayStatus = 4;
				ResumeVoiceDefaultVolume();
			}
			sleep(1);
		}
		else
		{
			m_iPlayStatus = 1;
			m_iDownLoadStatus = 0;
			if(CURLE_OK == DownLoadMp3File())
			{
				if( 1 == m_iDownLoadStatus){
					_printd("There is an new Mp3 to be downed,so dump this");
					continue;
				}
				if( 4 == m_iPlayStatus || 0 == m_iPlayStatus){
					//m_iDownLoadStatus = 0;
					_printd("Mp3 paly have been stopped");
					continue;
				}
				
				m_iPlayStatus = 3;
				
				int ret = 0;	
				
				//usleep(100*1000);
				//AudioTalkPlayFinish();
				ret = AudioPlayAudioStart(MP3FILENAME,(enum audio_encode_type)16, 0);	
			//	if(1 == AudioPlayAudioStatus())
			//	{
			//		//播放失败再播放一次
			//		AudioPlayAudioStop();
			//		sleep(1);
			//	}
				if(ret == -1)
				{
					_printd("Play 5");	
					AudioPlayAudioStop();
					sleep(1);
					ret = AudioPlayAudioStart(MP3FILENAME,(enum audio_encode_type)16, 0);
				}
			//	ret = AudioPlayAudioStart(MP3FILENAME,(enum audio_encode_type)16, 0);
				if(-1 == ret){
					m_iPlayStatus = 2;
				}
				else{
					SetVoicePlayVolume();				
				}
			}
			else
			{
				//下载文件失败，播放失败
				m_iPlayStatus = 2;
			}
			//m_iDownLoadStatus = 0;
		}
		
	}	
}


VD_INT32 MP3Play::DownLoadMp3File()
{

	CURL *curl;  
    CURLcode res = CURLE_RECV_ERROR;  
    curl = curl_easy_init();  
	if (curl) 
	{
		curl_easy_setopt(curl, CURLOPT_URL, m_imp3url.c_str()); 
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);
        //指定回调函数  
	    if (file_exists((char*)MP3FILENAME))  
           remove(MP3FILENAME);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefile_callback);  
	    curl_easy_setopt(curl, CURLOPT_WRITEDATA, MP3FILENAME);
        res = curl_easy_perform(curl);  
		curl_easy_cleanup(curl);  
	}
	return res;
}

 VOID MP3Play::SetMP3PlayStatus(int status)
 {
 	m_iPlayStatus = 4;
	if(0 == status)
	{
		if(AudioPlayAudioStatus())
		{
			AudioPlayAudioStop();
		}
		
		ResumeVoiceDefaultVolume();
	}
 }

int  MP3Play::GetMP3PlayStatus(char url[1024])
{
	if(url != NULL){
		strncpy(url,m_imp3url.c_str(),strlen(m_imp3url.c_str()));
	}
	return m_iPlayStatus;
}

int MP3Play:: MP3PlayPause()
{
   AudioPlayPause();
   ResumeVoiceDefaultVolume();
   
   return 0;
}
int MP3Play:: MP3PlayResume()
{
	AudioPlayResume();
	SetVoicePlayVolume();
	
	return 0;
}



int MP3Play::SetVoicePlayVolume()
{
#ifdef VOLUME_UNIFY
#else
	if( 0 == m_iDefaultVolume){
		AudioOutGetVol(&m_iDefaultVolume);
	}
#endif	
	AudioOutSetVol(m_iVolume);

	printf("Set VoiceVolume %d\n",m_iVolume);

	return 0;
}
int MP3Play::ResumeVoiceDefaultVolume()
{
#ifdef VOLUME_UNIFY
#else
	if( 0 == m_iDefaultVolume){
		AudioOutGetVol(&m_iDefaultVolume);
	}
	AudioOutSetVol(m_iDefaultVolume);
#endif

	return 0;
}

int MP3Play::SetMP3PlayVoiceVolume(int percent)
{
	CConfigVoicePlay cCfgVoicePlay;

	cCfgVoicePlay.update();
	CONFIG_VOICEPLAYPARAM &config	= cCfgVoicePlay.getConfig();
	config.iVolume = percent;
	cCfgVoicePlay.commit();

	m_iVolume = percent;

	if( 3 == m_iPlayStatus){
		SetVoicePlayVolume();
	}

	printf("Set VoiceVolume  percent %d\n",percent);
	return 0;
}
int MP3Play::GetMP3PlayVoiceVolume()
{
	return m_iVolume;
}

