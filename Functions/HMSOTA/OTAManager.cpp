#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <errno.h>  
#include <string.h>  
#include <dirent.h>  
#include <errno.h>  
#include <sys/stat.h>  
  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <time.h> 


#include "Functions/HMSOTA/OTAManager.h"
#include "Intervideo/DevSearch/DevSearch.h"
#include "System/File.h"

#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Functions/HMSOTA/curl/curl.h"
#include "Functions/HMSOTA/cJSON.h"
#include "APIs/CommonBSP.h"


typedef struct stat Stat_t;  
#define StatFunc(x,y) stat(x,y)

#define UPGRADEDIR "/tmp/"


#define OTAREQMSGURL  "http://ota.instaon.com/ota/api/v1/fw/find"
#define OTAREQDOWNLOAD  "http://ota.instaon.com/ota/api/v1/fw/download"


#define BUIDDATETIME "2020-04-27 15:44:50"


using namespace std;

//#if IPC_JZ	
//#define DEVICEID  "CP018DB0"
//#elif IPC_R2S2
//#define DEVICEID  "CP018DAD"
//#elif IPC_R2P2
//#define DEVICEID  "CP018DAE"
//#elif IPC_R2S1
//#define DEVICEID  "CP018DAC"
//#elif IPC_A3S1
//#define DEVICEID  "CP018DAA"
//#elif IPC_A3S2
//#define DEVICEID  "CP018DAB"
//#elif G3S2
//#define DEVICEID  "CP018DAF"
//#endif

static int  getDeviceId(char *pContest, int nSize)
{
	if(NULL == pContest) return -1;
	#ifdef IPC_JZ_NEW
	switch(PlatformGetHandle()->ProductModel){
		case ProductM_R2S1:
			snprintf(pContest, nSize, "%s", "CP018DAC");
			break;
		case ProductM_R2S2:
			snprintf(pContest, nSize, "%s", "CP018DAD");
			break;
		case ProductM_A3S1:	
			snprintf(pContest, nSize, "%s", "CP018DAA");
			break;
		case ProductM_A3S2:
			snprintf(pContest, nSize, "%s", "CP018DAB");
			break;
		case ProductM_R2P2:
			snprintf(pContest, nSize, "%s", "CP018DAE");
			break;
		case ProductM_G3S2:
			snprintf(pContest, nSize, "%s", "CP018DAF");
			break;	
		case ProductM_Q1:
		case ProductM_Q1PRO:
		case ProductM_QT2:
		case ProductM_A3:
		case ProductM_P2:	
		default: 
			return -1;
	}
	#elif  IPC_JZ
	snprintf(pContest, nSize, "%s", "CP018DB0");
	#endif

	return 0;
}


static FILE *errStream = stderr;
int file_exists(char *filename) {  
    return (access(filename, 0) == 0);  
}  

static size_t writefile_callback(void *ptr, size_t size, size_t nmemb, void *stream) {  
    int len = size * nmemb;  
    int written = len;  
    FILE *fp = NULL;  
    if (access((char*) stream, 0) == -1) {  
        fp = fopen((char*) stream, "wb");  
    } else {  
        fp = fopen((char*) stream, "ab");  
    }  
    if (fp) {  
        fwrite(ptr, size, nmemb, fp);  
		fclose(fp);  
    }  
    printf("download size[%d]\n",size);  
    
    return written;  
}  

size_t write_memory(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    MemChunk *mem = (MemChunk *)userp;

    mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL)
    {
        /* out of memory! */ 
        fprintf(errStream, "not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = '\0';
 
    return realsize;
}



PATTERN_SINGLETON_IMPLEMENT(OTAManager);

OTAManager::OTAManager():CThread("OTAManager", TP_NET)
{	
	m_iUpDateStatus = 0;
	m_iUpGradeMode = 0;
	_printd("OTAManager");
}
OTAManager::~OTAManager()
{
	_printd("OTAManager");
	curl_global_cleanup();
}
VD_INT32 OTAManager::Init()
{
	GetUUID();
	GetMac();
	curl_global_init(CURL_GLOBAL_ALL);

}
VD_INT32 OTAManager::Start()
{
	int iRet;
	_printd("OTAManager init!!!");
	m_configOtaMode.attach(this, (TCONFIG_PROC)&OTAManager::onConfigOta);
	m_configOtaMode.update();
	onConfigOta(m_configOtaMode, iRet);	
	
	_printd("m_configOtaMode.getLatest().Mode[%d]",m_configOtaMode.getLatest().Mode );
	Init();

	CreateThread();
	return 0;
}
VD_INT32 OTAManager::Stop()
{
	_printd("OTAManager :: Stop ");
	return 0;
}
int OTAManager::GetUUID()
{
	memset(m_UUID,'\0',32);
	if(0 != GetUuid(m_UUID))
	{
		_printd("Read Factory Info Err");
	}
	return 0;
}
int OTAManager::GetMac()
{
	memset(m_cMac,'\0',32);
	struct ifreq ifreq;
	int sock;
	if((sock=socket(AF_INET,SOCK_STREAM,0))<0)
	{
		perror("socket");
		return 2;
	}
	strcpy(ifreq.ifr_name,"wlan0");
	if(ioctl(sock,SIOCGIFHWADDR,&ifreq)<0)
	{
		perror("ioctl");
		return 3;
	}
	printf("%02X:%02X:%02X:%02X:%02X:%02X\n",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	snprintf(m_cMac,32,"%02X:%02X:%02X:%02X:%02X:%02X",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	return 0;
}

void OTAManager::ThreadProc()
{
	int ret = 0;
	while (m_bLoop) 
	{
		ret = PackSendOTAMsgData();
		if(0 == ret)
		{
			//检查升级的条件
			CheckUpGradeCondition();
		}
		for(int i = 0; i < m_configOtaMode.getLatest().Interval; i ++)
		{
			for(int j = 0; j < 3600; j++ )
			{
				sleep(1);
				if(m_iUpDateStatus)
				{
					UpGradeVersion();
					m_iUpDateStatus = 0;
				}
			}
			if(m_iUPLoadStatus)
			{
				//反回0是通知成功
				m_iUPLoadStatus = RealTimeUploadOtaNewVersionInfo();
			}
		}
		
	}	
}



int OTAManager::sendRequest(int reqtype,char* url,char* data,
	string & result, long *statusCode,string  filename)
{
	int opc = OPC_REQFAILED;;
    CURL *curl; 
	struct curl_slist *plist;	
    CURLcode res;  
    curl = curl_easy_init();  
	
	MemChunk chunk;
	if(REQ_MESSAGE == reqtype)
	{
	    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
	    chunk.size = 0; 
	}
	if (curl) { 
        curl_easy_setopt(curl, CURLOPT_URL, url);  
		plist = curl_slist_append(NULL,"Content-Type: application/json");  
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, plist);
        curl_easy_setopt(curl, CURLOPT_POST, 1L);  
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data); 

        if(REQ_MESSAGE == reqtype)	
        {
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);  
	        //这个变量可作为接收或传递数据的作用  
	        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
        }
		else if(REQ_DOWNLOAD == reqtype)
		{
			if (file_exists((char*)filename.c_str()))  
            remove(filename.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefile_callback);  
	      	curl_easy_setopt(curl, CURLOPT_WRITEDATA, filename.c_str()); 
		}
        res = curl_easy_perform(curl);  
		
		long httpcode = 0;
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &httpcode);
		if (res != CURLE_OK) {
			fprintf(errStream, "curl_easy_perform() failed: (%d) %s\n", res, curl_easy_strerror(res));
			opc = res;//OPC_SENDFAILED;
		}
		else if (httpcode >= 200 && httpcode < 300 ) {
			//opc =  (chunk.memory, chunk.size, result);
			if(REQ_MESSAGE == reqtype &&  chunk.size > 2)	
			{
				result = (char*)chunk.memory;
				_printd("%s",chunk.memory);
			}
		}
		else {
			opc = OPC_REQFAILED;
		}
		
		curl_easy_cleanup(curl);
		curl_slist_free_all (plist);

		if(REQ_MESSAGE == reqtype)	
		free(chunk.memory);
		
		*statusCode = httpcode;
    }  
	return opc;
}

int OTAManager::PackSendOTAMsgData()
{
	int ret = 0;
	cJSON *root = NULL;
	char *json_data = NULL;
	string  result ;
	long statusCode = 0;
	string  filename;
	char  DeviceId[16]  = {'\0'};

	getDeviceId(DeviceId, sizeof(DeviceId));
	
	root = cJSON_CreateObject();
    cJSON_AddStringToObject(root,"UUID",m_UUID);//UUID, The universal unique ID – As String
 	cJSON_AddStringToObject(root,"MAC",m_cMac);// 12 Digit Hex MAC address – As String
 	cJSON_AddStringToObject(root,"DevID",DeviceId); //Device Date and Time(YYYYMMDDHHMMSS) – As String
	cJSON_AddStringToObject(root,"BuildDateTime", BUIDDATETIME); //Total uptime in secs – As Number
	cJSON_AddStringToObject(root,"Lang", "Eng"); //Shutdown Time(YYYYMMDDHHMMSS) – As String
	
	json_data = cJSON_Print(root);
	printf("===================\n%s\n===================\n",json_data);

	sendRequest((int)REQ_MESSAGE,(char*)OTAREQMSGURL,(char*)json_data,result,&statusCode,filename);
	cJSON_Delete(root);
	free(json_data);
	ret = HandleResponse(result);
	_printd("ret[%d]\n",ret);
	return ret;
}
int OTAManager::HandleResponse(string & result)
{
	if(!result.empty())
	{
		unsigned int filesize;
		cJSON *root = NULL;
		cJSON *UnAttended = NULL;
		cJSON *Critical = NULL;
		cJSON *RelNotes = NULL;  
		cJSON *Lang     = NULL;
		cJSON *FileName = NULL;
		cJSON *FileSize = NULL;
		cJSON *BuildDateTime = NULL;
		Stat_t s;  
	  	int nRet = 0;
		_printd("result.c_str()[%s]",result.c_str());
		root = cJSON_Parse(result.c_str());
		UnAttended = cJSON_GetObjectItem(root,"UnAttended");
		Critical =  cJSON_GetObjectItem(root,"Critical"); 
		RelNotes =  cJSON_GetObjectItem(root,"RelNotes"); 
		Lang     =  cJSON_GetObjectItem(root,"Lang"); 
		FileName =  cJSON_GetObjectItem(root,"FileName");
		FileSize =  cJSON_GetObjectItem(root,"FileSize");
		BuildDateTime = cJSON_GetObjectItem(root,"BuildDateTime");
		if(NULL != root && NULL != FileName && NULL != UnAttended
			&& NULL != Critical && NULL != RelNotes && NULL != Lang
			&& NULL != FileSize	&& NULL != BuildDateTime)
		{
			if(cJSON_String  == UnAttended->type)
			{
				m_uploadInfo.UnAttended = atoi(UnAttended->valuestring);
			}
			else
			{
				m_uploadInfo.UnAttended = UnAttended->valueint;

			}
			if(cJSON_String  == Critical->type)
			{
				m_uploadInfo.Critical = atoi(Critical->valuestring);
			}
			else
			{
				m_uploadInfo.Critical = Critical->valueint;

			}
			if(cJSON_String  == RelNotes->type)
			{
				strncpy(m_uploadInfo.RelNotes, RelNotes->valuestring, 5*1023);
			}
			if(cJSON_String  == Lang->type)
			{
				strncpy(m_uploadInfo.Lang, Lang->valuestring, 64);
			}
			if(cJSON_String  == FileName->type)
			{
				strncpy(m_uploadInfo.FileName, FileName->valuestring, 128);
			}
			if(cJSON_String  == FileSize->type)
			{
				m_uploadInfo.FileSize = atoi(FileSize->valuestring);
			}
			else
			{
				m_uploadInfo.FileSize = FileSize->valueint;
			}
			if(cJSON_String  == BuildDateTime->type)
			{	
				strncpy(m_uploadInfo.BuildDateTime, BuildDateTime->valuestring, 128);
			}
			return 0;
		}
	}
	return 1;
}
int OTAManager::CheckUpGrade(string &filename,unsigned int filesize)
{
	if (file_exists((char*)filename.c_str()))  
	{
		Stat_t s;  
		int nRet = 0; 
   		memset(&s, 0, sizeof(Stat_t));  
   		nRet = StatFunc(filename.c_str(), &s);  
	   	if (nRet)  
	   	{  
	       printf("stat %s failed! error_code: %s", filename.c_str() ,strerror(errno));  
		   if(m_iUpGradeMode)
		   {
				RealTimeSendUpgradeStatus(2,0);
				m_iUpGradeMode = 0;
		   }
		   return -1;  
	   	}  
		//文件大小相等则升级
		_printd("filesize[%u] s.size[%d]",filesize,s.st_size);
		if(filesize == s.st_size)
		{
			_printd("UPGRADE>>>>>>>>>");
			if(m_iUpGradeMode)
			{
				RealTimeSendUpgradeStatus(3,0);
				m_iUpGradeMode =0;
			}
			sleep(2);
			char UpgradeFilePath[150] = {0};
			sprintf(UpgradeFilePath,"/app/upgradeTool  %s",filename.c_str());
			printf("%s\n",UpgradeFilePath);
			execl("/bin/sh","sh","-c",UpgradeFilePath,(char*)0);	
		}
		remove(filename.c_str());
	}
	if(m_iUpGradeMode)
	{
		RealTimeSendUpgradeStatus(2,0);
		m_iUpGradeMode = 0;
	}
	return OPC_OK;
}
void OTAManager::onConfigOta(CConfigNetOta& cCfgOta, int& ret)
{
	CONFIG_NET_OTA &configOld = m_configOtaMode.getConfig();
	CONFIG_NET_OTA &configNew = cCfgOta.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}
	configOld = configNew;
}
int OTAManager::CheckUpGradeCondition()
{
	//语言检查
	_printd("m_uploadInfo.Lang[%s]",m_uploadInfo.Lang);
	_printd("m_uploadInfo.UnAttended[%d]",m_uploadInfo.UnAttended);
	_printd("m_uploadInfo.Critical[%d]",m_uploadInfo.Critical);
	_printd("m_uploadInfo.FileName[%s]",m_uploadInfo.FileName);
	_printd("m_uploadInfo.FileSize[%d]",m_uploadInfo.FileSize);
	_printd("m_uploadInfo.BuildDateTime[%s]",m_uploadInfo.BuildDateTime);
	_printd("m_uploadInfo.RelNotes[%s]",m_uploadInfo.RelNotes);
	_printd("m_configOtaMode.getLatest().Mode[%d]",m_configOtaMode.getLatest().Mode );
	
	if(strstr(m_uploadInfo.Lang,"ENG"))
	{
		//强制升级 或者自动关键更新
		if(m_uploadInfo.UnAttended  || (m_uploadInfo.Critical && m_configOtaMode.getLatest().Mode))
		{
			UpGradeVersion();
		}
		else
		//else if(m_uploadInfo.Critical  && 0 == m_configOtaMode.getLatest().Mode )
		{
			//有新版本都通知用户,忽略则不通知
			if(strncmp(m_configOtaMode.getLatest().IgnoreVersion,m_uploadInfo.FileName,strlen(m_uploadInfo.FileName)))
			{
				m_iUPLoadStatus = RealTimeUploadOtaNewVersionInfo();
			}
		}
	}
	else
	{
		_printd("The LANG is not the SAME!!! ");
	}
}

int OTAManager::UpGradeVersion()
{
	char* ReadVersion = NULL;
	char version1[1024]={'\0'};
	char version2[1024]={'\0'};
	char strDeviceId[16] = {'\0'};
	long statusCode = 0;
	string  filename = UPGRADEDIR; 
	string  result ;
	cJSON *rootresp = NULL;
	char *json_data = NULL;
	char *p1 = NULL;
	char *p2 = NULL;
	char *p3 = NULL;

	filename += m_uploadInfo.FileName;
	getDeviceId(strDeviceId, sizeof(strDeviceId));

	rootresp = cJSON_CreateObject();
	
	cJSON_AddStringToObject(rootresp,"UUID",m_UUID);
	cJSON_AddStringToObject(rootresp,"MAC",m_cMac);
	cJSON_AddStringToObject(rootresp,"DevID",strDeviceId); //Device Date and Time(YYYYMMDDHHMMSS) – As String
	cJSON_AddStringToObject(rootresp,"FileName", m_uploadInfo.FileName); //Total uptime in secs – As Number
	cJSON_AddStringToObject(rootresp,"BuildDateTime", BUIDDATETIME); //
	json_data = cJSON_Print(rootresp);
	printf("===================\n%s\n===================\n",json_data);
	sendRequest((int)REQ_DOWNLOAD,(char*)OTAREQDOWNLOAD,(char*)json_data,result,&statusCode,filename);
	cJSON_Delete(rootresp);
	free(json_data);
	//检查是否升级
	CheckUpGrade(filename,m_uploadInfo.FileSize);
}
int OTAManager::QueryVersionInfo(RT_UPLOADNEWVERINFO* puploadInfo)
{
	int ret = PackSendOTAMsgData();
	if(0 == ret)
	{
		memcpy(puploadInfo,&m_uploadInfo,sizeof(RT_UPLOADNEWVERINFO));
	}
	return ret;
}
int OTAManager::CallBackUpgrade(char FileName[128])
{
	m_iUpDateStatus = 1;
	m_iUpGradeMode = 1;
	strncpy(m_uploadInfo.FileName,FileName,128);
	//UpGradeVersion();
	_printd("upgrade!!!!!");
	return 0;
}

