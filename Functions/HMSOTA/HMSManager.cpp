#include "Intervideo/DevSearch/DevSearch.h"
#include "System/File.h"

#include "Functions/FFFile.h"
#include "Functions/Record.h"
#include "Devices/DevInfo.h"
#include <sys/time.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>

#include "Functions/HMSOTA/HMSManager.h"
#include "Functions/HMSOTA/OTAManager.h"
#include "Functions/HMSOTA/curl/curl.h"
#include "APIs/CommonBSP.h"
#include "APIs/CommonBSP_Battery.h"

//#define ADDRESS     "tcp://mq.worthcloud.net:61613"
//#define CLIENTID    "ExampleClientPub"
//#define TOPIC       "HMS/101/OEMCODE/DEVICECODE/CUSTOMSTRING"
//#define PAYLOAD     "Hello World!"
//#define QOS         2
//#define TIMEOUT     10000L
//#define MQTTCLIENT_PERSISTENCE_NONE 1

#define ADDRESS     "ssl://shms.cppluscloud.com:8883"
//#define CLIENTID    "6712712123"
#define USERNAME    "rajesh"
#define PASSWORD    "rajesh"
#define CLIENTID    "700002"
//#define TOPIC       "HMS/101/OEMCODE/DEVICECODE/CUSTOMSTRING"
#define PAYLOAD     "Hello World!"
#define CAFILE      "/app/ipcam/hms-cert.pem"
#define TOPIC0      "hms/103/003"      //hearbeat
#define TOPIC1      "hms/101/003"	   //hms data	
#define TOPIC2      "hms/102/003"      //alert data
#define QOS         1
#define TIMEOUT     10000L
#define MQTTCLIENT_PERSISTENCE_NONE 1



using namespace std; 

#if 0
static int FacRead(void *buf, int buflen)
{
	FILE *Fp	= NULL;
	Fp	= fopen(FACINFONANDDEVPATH, "r");
	if (NULL == Fp){
		return -1;
	}
	
	if(0 > fread(buf,buflen, 1, Fp)){
		fclose(Fp);
		return -1;
	}
	fclose(Fp);
	return 0;
}
#endif

static int  getDeviceId(char *pContest, int nSize)
{
	if(NULL == pContest) return -1;
	#ifdef IPC_JZ_NEW
	switch(PlatformGetHandle()->ProductModel){
		case ProductM_R2S1:
			snprintf(pContest, nSize, "%s", "CP-EWK-P10");
			break;
		case ProductM_R2S2:
			snprintf(pContest, nSize, "%s", "CP-EWK-P20");
			break;
		case ProductM_A3S1:	
			snprintf(pContest, nSize, "%s", "CP-EWK-C10");
			break;
		case ProductM_A3S2:
			snprintf(pContest, nSize, "%s", "CP-EWK-C20");
			break;
		case ProductM_R2P2:
			snprintf(pContest, nSize, "%s", "CP-EWK-P21");
			break;
		case ProductM_G3S2:
			snprintf(pContest, nSize, "%s", "CP-EWK-T20");
			break;	
		case ProductM_Q1:
		case ProductM_Q1PRO:
		case ProductM_QT2:
		case ProductM_A3:
		case ProductM_P2:	
		default: 
			return -1;
	}
	#elif  IPC_JZ
	switch(BAT_PlatformGetHandle()->ProductModel){
		case BAT_ProductM_S1:
			snprintf(pContest, nSize, "%s", "CP-EWK-BT100");
			break;
		case BAT_ProductM_S1Single:
		case BAT_ProductM_I9:
		default:
			return -1;
	}
	#endif

	return 0;
}
 
 

PATTERN_SINGLETON_IMPLEMENT(HMSManager);

HMSManager::HMSManager():CThread("HMSManager", TP_NET)
{
	_printd("HMSManager");
	m_iSdStatus = 0;
	m_iSDerroCount = 0;
}
HMSManager::~HMSManager()
{
	_printd("HMSManager");
}
int HMSManager::GetUUID()
{
	if(0 != GetUuid(m_UUID))
	{
		m_UUIDStatus = 0;
		_printd("Read Factory Info Err");
	}
	m_UUIDStatus = 1;
	return 0;
}
int HMSManager::GetMac()
{
	struct ifreq ifreq;
	int sock;
	if((sock=socket(AF_INET,SOCK_STREAM,0))<0)
	{
		perror("socket");
		return 2;
	}
	strcpy(ifreq.ifr_name,"wlan0");
	if(ioctl(sock,SIOCGIFHWADDR,&ifreq)<0)
	{
		perror("ioctl");
		return 3;
	}
	printf("%02X:%02X:%02X:%02X:%02X:%02X\n",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	snprintf(m_cMac,20,"%02X%02X%02X%02X%02X%02X",
					(unsigned char)ifreq.ifr_hwaddr.sa_data[0],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[1],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[2],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[3],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[4],
					(unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
	return 0;
}
 int HMSManager::CheckSDStatus()
{
	FILE  *fp   = NULL;
	char  strDevName[24] = {0};
	char  strDirName[24] = {0};
	char  strFileType[16]= {0};
	char  strMountParam[256] = {0};
	int   other[2] = {};
	char  buf[512] = {0};
	int   nret = 0;
	int   SDmount  = 0;

	//_printd("1");
	if (NULL == (fp = fopen("/proc/mounts", "r")))
	{
		_printd("open error!!!");
		return -1;
	}
	memset(strMountParam,0,256);
	while(!feof(fp))
	{
		fgets((char *)buf, 511, fp);
		//_printd("buf[%s]\n",buf);
		int ret =0;
		ret = sscanf(buf, "%s %s %s %s %d %d",strDevName, 
			strDirName, strFileType, strMountParam, &other[0], &other[1]);	
		//printf(">>>ret = %d ,strDevName=%s,strDirName=%s strMountParam = %s\n",ret,strDevName,strDirName,strMountParam);
		if((NULL != strstr(strDevName,"mmcblk")) && (0 == strcmp(TARGET_DIR, strDirName) ))
		{
			SDmount = 1;
			if('w' ==strMountParam[1])
			{
				//printf(" sd WWW \n");
				nret = 0;
			}else if('o' ==strMountParam[1]){
				printf("sd OOO\n");
				nret = -1;
			}
			break;
		}
		else
		{
			//_printd("999999999\n");
		}
		
		memset(buf,0,sizeof(buf));
		memset(strMountParam,0,sizeof(strMountParam));
		memset(strDirName,0,sizeof(strDirName));
		
	}
	fclose(fp);

	if(SDmount)
	{
		if(nret == 0)
		{
			m_iSdStatus = 1;//可写
			m_iSDerroCount = 0;
		}
		else if(nret == -1)
		{
			int ret = 0;
			if(-1 != m_iSdStatus)
			{
				PackSendAlertData(12, 1,2,1);
			}
			m_iSdStatus = -1;//只读
			_printd("umount -fl %s\n",TARGET_DIR);
			ret = umount2(TARGET_DIR, MNT_FORCE);
			if(-1 == ret && 0 == m_iSDerroCount)
			{
				char buf[1024] ={"\0"};
				snprintf(buf,1024,"umount -fl %s\n",TARGET_DIR);
				system(buf);
				m_iSDerroCount = 1; //只强制一次
			}
			
			
		}
	}
	return nret;
}

int HMSManager::GetDevTime(char date[HMSDATELEN])
{
    time_t cur_time = 0;
    struct tm *ptm = NULL;
    struct timeval  tv;
    struct timezone tz;
	time_t 	t  = time(NULL);
	
    gettimeofday(&tv, &tz);
    t += tz.tz_minuteswest*60;
	ptm = gmtime(&t);
    printf("cur time: %04d-%02d-%02d %02d:%02d:%02d\n", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	snprintf(date,20,"%04d%02d%02d%02d%02d%02d", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	return 0;
}
long HMSManager::GetDevUptime()
{
	struct sysinfo info;
    sysinfo(&info);
    return info.uptime;	
}
int HMSManager::GetSysBootTime(char date[HMSDATELEN])
{
	struct sysinfo info;
    time_t boot_time = 0;
    struct tm *ptm = NULL;
	
	struct timeval  tv;
    struct timezone tz;
	time_t 	cur_time  = time(NULL);
	
    gettimeofday(&tv, &tz);
    cur_time += tz.tz_minuteswest*60;
	
    if (sysinfo(&info)) {
    fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n",
        errno, strerror(errno));
    return -1;
    }
 //   time(&cur_time);
    if (cur_time > info.uptime) {
    boot_time = cur_time - info.uptime;
    }
    else {
    boot_time = info.uptime - cur_time;
    }
	ptm = gmtime(&boot_time);
 //   ptm = localtime(&boot_time);
    printf("System boot time: %04d-%02d-%02d %02d:%02d:%02d\n", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	snprintf(date,20,"%04d%02d%02d%02d%02d%02d", ptm->tm_year + 1900,
        ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	return 0;
}
int HMSManager::GetIP(char date[HMSDATELEN])
{
	GetDevIp(date,HMSDATELEN);
	return 0;
}

VD_INT32 HMSManager::Start()
{
	int iRet;
	int RebootFlag = 0;
	m_UUIDStatus = 0;     //~{;qH!~}UUID~{W4L,~}
	m_iMqttConStatus = 0; //~{N4A,=S~}
	m_iMqttConFailCount = 0; //~{A,=SJ'0\4NJ}~}
	
	_printd("HMSManager init!!!");
	memset(m_UUID,'\0',32);
	GetUUID();
	GetMac();
	//~{EdVC1d;/J1M(V*4KO_3L~}
	m_configHmsServer.attach(this, (TCONFIG_PROC)&HMSManager::onConfigHms);
	m_configHmsServer.update();
	onConfigHms(m_configHmsServer, iRet);	

	g_Cmos.Read(CMOS_REBOOTFLAG,&RebootFlag,4);
	_printd("RebootFlag[%d]",RebootFlag);
	m_iRebootStatus = RebootFlag;
	if(0 == RebootFlag)
	{		
		RebootFlag = 1;
		g_Cmos.Write(CMOS_REBOOTFLAG,&RebootFlag,4);
	}
	CreateThread();
	_printd ("Tupu Init Over");
	return 0;
}
VD_INT32 HMSManager::Stop()
{
	_printd("HMSManager :: Stop ");
	return 0;
}
void HMSManager::onConfigHms(CConfigNetHms& cCfgHms, int& ret)
{
	CONFIG_NET_HMS &configOld = m_configHmsServer.getConfig();
	CONFIG_NET_HMS &configNew = cCfgHms.getConfig();

	if (&configOld == &configNew)
	{
		return;
	}
	configOld = configNew;
	m_mutex.Enter();
	if(m_iMqttConStatus)
	{
		MQTTClient_disconnect(m_mqttClient, 10000);
		MQTTClient_destroy(&m_mqttClient);
		m_iMqttConStatus = 0;
	}
	m_mutex.Leave();
}

void HMSManager::ThreadProc()
{
	while (m_bLoop) 
	{
		_printd("m_configHmsServer.getConfig().Enable[%d]",m_configHmsServer.getConfig().Enable);
		if(m_configHmsServer.getConfig().Enable)
		{
			PackSendHealthData(); //HMS data
		}
		for(int i = 0; i < 10; i++)
		{
			if(m_configHmsServer.getConfig().Enable)
			{
				PackSendHeartBeatData(); //ping data
			}
			sleep(30);	
			if(1 == (i%2))
			{
				int ret = 0;
				ret = CheckSDStatus();//1分钟检测一下SD状态
				_printd("HMSanager ret[%d]",ret);
			}
		}
	}	
}

int HMSManager::SendHmsData(int datetype,uchar* data,VD_UINT32 len)
{
	m_mutex.Enter();
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	int rc;

	if(0 == m_iMqttConStatus)
	{
	    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
		MQTTClient_SSLOptions ssl_opts = MQTTClient_SSLOptions_initializer;
	  

		conn_opts.keepAliveInterval = 20;  
		conn_opts.cleansession = 1; 
		conn_opts.username = m_UUID; 
		conn_opts.password = m_configHmsServer.getConfig().Password;   
	//	conn_opts.username = USERNAME; 
	//	conn_opts.password = PASSWORD;   
		ssl_opts.trustStore = CAFILE;  
		conn_opts.ssl = &ssl_opts;
		_printd("HMS serverAddr:%s\n",m_configHmsServer.getConfig().ServerName);
		_printd("username:%s",m_UUID);
		_printd("Password:%s", m_configHmsServer.getConfig().Password);
		
	    MQTTClient_create(&m_mqttClient, m_configHmsServer.getConfig().ServerName, m_UUID,
	        MQTTCLIENT_PERSISTENCE_NONE, NULL);
	    conn_opts.keepAliveInterval = 20;
	    conn_opts.cleansession = 1;

	    if ((rc = MQTTClient_connect(m_mqttClient, &conn_opts)) != MQTTCLIENT_SUCCESS)
	    {
	    	m_mutex.Leave();
	        _printd("Failed to connect, return code %d\n", rc);
			//连续3次连接不上就关闭HMS
			if(4 == rc && m_iMqttConFailCount ++ > 3 )
			{
				CONFIG_NET_HMS &configHMS = m_configHmsServer.getConfig();
				configHMS.Enable = 0;
				m_configHmsServer.commit();
				_printd("Close Conect HMS Sever\n");
			}
			
	        return -1;
	    }
		m_iMqttConStatus = 1;
	}
	m_iMqttConFailCount = 0;
    pubmsg.payload = (void*)data;
    pubmsg.payloadlen = len;
    pubmsg.qos = QOS;
    pubmsg.retained = 0;

	if(0 == datetype)
	{   //ping
   		MQTTClient_publishMessage(m_mqttClient, TOPIC0, &pubmsg, &m_token);
	}
	else if(1 == datetype)
	{
		//health data
		MQTTClient_publishMessage(m_mqttClient, TOPIC1, &pubmsg, &m_token);
	}
	else if(2 == datetype)
	{
		//alert data
		MQTTClient_publishMessage(m_mqttClient, TOPIC2, &pubmsg, &m_token);
	}

	_printd("Waiting for up to %d seconds for publication of %s\n"
            "on topic %s for client with ClientID: %s\n",
            (int)(TIMEOUT/1000), PAYLOAD, TOPIC0, CLIENTID);
    rc = MQTTClient_waitForCompletion(m_mqttClient, m_token, TIMEOUT);
    _printd("Message with delivery token %d delivered\n", m_token);

	if(rc != MQTTCLIENT_SUCCESS)
	{
	    MQTTClient_disconnect(m_mqttClient, 10000);
	    MQTTClient_destroy(&m_mqttClient);
		m_iMqttConStatus = 0;
	}
	m_mutex.Leave();
    return rc;

}
int HMSManager::PackSendHealthData()
{
	//SD卡
	int Remainmem 	= 0;
	int Totalmem 	= 0;
	int ret 		= -1;
	int SDExist = 0;
	
	cJSON *root = NULL;
	char *json_data = NULL;
	cJSON  *pJsonArry,*pJsonsub;
	cJSON  *pJsonArrycg,*pJsonsubcg;
	char date[HMSDATELEN] = {'\0'};
	char bootdate[HMSDATELEN] = {'\0'};
	char recordst[HMSDATELEN] = {'\0'};
	char localip[HMSDATELEN] = {'\0'};
	char ipdate[64] = {'\0'};
	long uptimesec = 0;
	std::string wlanIP;
	char  DeviceId[16]  = {'\0'};

	//SD卡容量	
	ret = file_sys_get_cap(&Totalmem,&Remainmem);
	_printd("Totalmem[%d] Remainmem[%d]",Totalmem,Remainmem);
	if(ret != -1)
	{
		
		Totalmem = Totalmem/1024; //MB
        Remainmem = Remainmem/1024;//MB		
        SDExist = 1;
	}
	else
	{
		Totalmem = 0;
		Remainmem = 0;
		SDExist = 0;
	}
	getDeviceId(DeviceId, sizeof(DeviceId));
	GetDevTime(date);
	GetSysBootTime(bootdate);
	GetIP(localip);
	GetWlanIp(wlanIP);
	if(SDExist && -1 != m_iSdStatus)
	{
		int rsret = 0;
		_printd("000000000000000000000000000");
		rsret = GetRecordStartTime(recordst);
		if(-1 ==  rsret)
		{
			strncpy(recordst,bootdate,HMSDATELEN);
		}
	}
	snprintf(ipdate,64,"%s/%s",localip,wlanIP.c_str());
	root = cJSON_CreateObject();
	cJSON_AddStringToObject(root,"ud",m_UUID);//UUID, The universal unique ID ? As String
	cJSON_AddStringToObject(root,"mc",m_cMac);// 12 Digit Hex MAC address ? As String
	cJSON_AddStringToObject(root,"dd",date); //Device Date and Time(YYYYMMDDHHMMSS) ? As String
	cJSON_AddNumberToObject(root,"ut", GetDevUptime()); //Total uptime in secs ? As Number
	cJSON_AddStringToObject(root,"st", bootdate); //Shutdown Time(YYYYMMDDHHMMSS) ? As String
	cJSON_AddStringToObject(root,"rt", bootdate); //Reboot Time(YYYYMMDDHHMMSS) ? As String
	if(m_iRebootStatus)
	{
		cJSON_AddStringToObject(root,"rf", "1"); //Reboot Flag (0x00, 0x01, 0x02, 0x03, 0x04, 0x05)
	}
	else
	{
		cJSON_AddStringToObject(root,"rf", "0"); 
	}
	cJSON_AddStringToObject(root,"dt", "IPC"); //Device Type ? As String
	cJSON_AddStringToObject(root,"ip", ipdate); //Device WAN IP Address ? As String
	cJSON_AddStringToObject(root,"wp", "na"); //Device Web Port Number, - As Number
	cJSON_AddStringToObject(root,"tp", "na"); //Device TCP Port Number, - As Number
	cJSON_AddStringToObject(root,"up", "na"); //Device UDP Port Number, - As Number
	cJSON_AddStringToObject(root,"rp", "na"); //Device RTSP Port Number, - As Number
	cJSON_AddStringToObject(root,"md", DeviceId); // Device Model Number ? As String
	if(SDExist && -1 != m_iSdStatus)
	{
		cJSON_AddStringToObject(root,"rs", recordst); //Rec. Start Date and Time(YYYYMMDDHHMMSS) ? As String
		cJSON_AddStringToObject(root,"re", date); //Rec. End Date and Time(YYYYMMDDHHMMSS) ? As String
	}
	else
	{
		if(0 == SDExist)
		{
			cJSON_AddStringToObject(root,"rs", "na"); //Rec. Start Date and Time(YYYYMMDDHHMMSS) ? As String
			cJSON_AddStringToObject(root,"re", "na"); //Rec. End Date and Time(YYYYMMDDHHMMSS) ? As String
		}
		else
		{			
			cJSON_AddStringToObject(root,"rs", "0"); //Rec. Start Date and Time(YYYYMMDDHHMMSS) ? As String
			cJSON_AddStringToObject(root,"re", "0"); //Rec. End Date and Time(YYYYMMDDHHMMSS) ? As String
		}
	}
	cJSON_AddNumberToObject(root,"pm", 1); //On Battery or On Power, 1 on Power, 0 on Battery ? As Number
	cJSON_AddStringToObject(root,"ap", "na"); //Wifi AP Status ? 0=No Module/error, 1=Module Present ? Enabled, 2=Module Present ? Disabled - ? As Number
	cJSON_AddNumberToObject(root,"wf", 1); //Wifi Network Status ? Suggested all available status similar as NVR/DVR ? As Number
	cJSON_AddStringToObject(root,"en", "na"); //Wired Network Status ? Suggested all available status similar as NVR/DVR ? As Number
	cJSON_AddStringToObject(root,"4g", "na"); //3g/4g Status ? Suggested all available status similar as NVR/DVR ? As Number
	cJSON_AddNumberToObject(root,"vw", 1);
	
	pJsonArry=cJSON_CreateArray();   /*~{44=(~}hg~{J}Wi~}*/
	cJSON_AddItemToArray(pJsonArry,pJsonsub=cJSON_CreateObject()); 
	cJSON_AddNumberToObject(pJsonsub,"ty", 2); //HDD Index (1,2,3,4,5,6,7,8 ~{!-~}.) - As Number
	cJSON_AddNumberToObject(pJsonsub,"sd", 1);
	cJSON_AddStringToObject(pJsonsub,"sn", "na"); //HDD Manufacturer~{!/~}s Serial Number ? As String
	cJSON_AddStringToObject(pJsonsub,"md", "na"); //HDD Model Number - As String

	if(SDExist && -1 != m_iSdStatus)
	{
		cJSON_AddNumberToObject(pJsonsub,"ts", Totalmem); //HDD Total Space in MBs - As Number
		cJSON_AddNumberToObject(pJsonsub,"fs", Remainmem); //HDD Free Space in MBs ? As Number
		cJSON_AddNumberToObject(pJsonsub,"st", 5); //HDD status(0=No HDD, 1=HDD Error, 2=HDD No Space, 5= Normal) - As Number
	}
	else
	{
		if(0 == SDExist)
		{
			cJSON_AddStringToObject(pJsonsub,"ts", "na"); //HDD Total Space in MBs - As Number
			cJSON_AddStringToObject(pJsonsub,"fs", "na"); //HDD Free Space in MBs ? As Number
		}
		else
		{			
			cJSON_AddStringToObject(pJsonsub,"ts", "0"); //HDD Total Space in MBs - As Number
			cJSON_AddStringToObject(pJsonsub,"fs", "0"); //HDD Free Space in MBs ? As Number
		}
		if(-1 == m_iSdStatus)
		{
			
			cJSON_AddNumberToObject(pJsonsub,"st", 1); 
		}
		else
		{
			cJSON_AddNumberToObject(pJsonsub,"st", 0); 
		}
	}
	cJSON_AddStringToObject(pJsonsub,"sp", "na"); //Seagate SkyHawk HDD Parameters, Comma Separated String.
	cJSON_AddStringToObject(pJsonsub,"tm", "na"); 
	cJSON_AddStringToObject(pJsonsub,"bf", "na"); 
	cJSON_AddItemToObject(root,"sg",pJsonArry);
	
	pJsonArrycg=cJSON_CreateArray();   /*~{44=(~}cg~{J}Wi~}*/
	cJSON_AddItemToArray(pJsonArrycg,pJsonsubcg=cJSON_CreateObject()); 	
	cJSON_AddNumberToObject(pJsonsubcg,"ch", 1); //Channel Number, Integer
	cJSON_AddStringToObject(pJsonsubcg,"cn", "na");
	cJSON_AddStringToObject(pJsonsubcg,"md", DeviceId);
	cJSON_AddNumberToObject(pJsonsubcg,"cc", 0); //Camera Video loss/Disconnected etc 1, If working then 0, Integer
	if(SDExist &&  -1 != m_iSdStatus)
	{
		cJSON_AddNumberToObject(pJsonsubcg,"cr", 1); //camera recording on or off(ON-1,Off-0), Integer
		cJSON_AddStringToObject(pJsonsubcg,"cm", "na"); //Camera masking (Yes-1, No-0), Integer
		cJSON_AddStringToObject(pJsonsubcg,"rs", recordst); //Recording Start Date ? As String
		cJSON_AddStringToObject(pJsonsubcg,"re", date);//Recording End Date ? As String
	}
	else
	{		
		if(-1 == m_iSdStatus)
		{
			cJSON_AddNumberToObject(pJsonsubcg,"cr", 0); //camera recording on or off(ON-1,Off-0), Integer
		}
		else
		{
			cJSON_AddStringToObject(pJsonsubcg,"cr", "na");	
		}
		cJSON_AddStringToObject(pJsonsubcg,"cm", "na"); //Camera masking (Yes-1, No-0), Integer
		if(0 == SDExist)
		{
			cJSON_AddStringToObject(pJsonsubcg,"rs", "na"); //Recording Start Date ? As String
			cJSON_AddStringToObject(pJsonsubcg,"re", "na");//Recording End Date ? As String
		}
		else
		{
			cJSON_AddStringToObject(pJsonsubcg,"rs", "0"); //Recording Start Date ? As String
			cJSON_AddStringToObject(pJsonsubcg,"re", "0");//Recording End Date ? As String
			
		}
	}
	cJSON_AddItemToObject(root,"cg",pJsonArrycg);
	
	json_data = cJSON_Print(root);
//0: ping 1: heathy data 2: acert
	SendHmsData(1,(uchar*)json_data, strlen(json_data));
	printf("===================\n%s\n===================\n",json_data);
	cJSON_Delete(root);
	free(json_data);

	return 0;
}
int HMSManager::PackSendAlertData(int AlertCode,int chn,int Reason,int Raise)
{
	cJSON *root = NULL;
	char *json_data = NULL;
	char date[HMSDATELEN] = {'\0'};
	
	GetDevTime(date);		
	root = cJSON_CreateObject();
	cJSON_AddStringToObject(root,"ud",m_UUID); //Universal unique ID ? As String
	cJSON_AddStringToObject(root,"dd",date); //date time
	cJSON_AddNumberToObject(root,"ac",AlertCode); //Alert Code
	cJSON_AddNumberToObject(root,"ty",2); // 1/2/3/4, // Alarm Reason: HDD Status(1=No HDD, 2=HDD Error, 3=HDD No Space, 4= Temperature high)
	cJSON_AddNumberToObject(root,"sd",1); // 
	cJSON_AddNumberToObject(root,"ar",Reason); //Alarm Reason: 1:Video Loss, 2:Tempering, 3: Disconnect, ? As Number
	cJSON_AddNumberToObject(root,"st",Raise); //Alarm Raise: 1, Alarm Clear: 0 ? As Number
	json_data = cJSON_Print(root);
	printf("===================\n%s\n===================\n",json_data);
	SendHmsData(2,(uchar*)json_data, strlen(json_data));
	cJSON_Delete(root);
	free(json_data);

	return 0;
}
int HMSManager::PackSendHeartBeatData()
{
	
	cJSON *root = NULL;
	char *json_data = NULL;
	char date[HMSDATELEN] = {'\0'};
	
	GetDevTime(date);
	root = cJSON_CreateObject();
	cJSON_AddStringToObject(root,"ud",m_UUID); //Universal unique ID ? As String
	cJSON_AddNumberToObject(root,"me",1); //1 1is integer, 0 in case of malfunction
	cJSON_AddStringToObject(root,"dd",date); //date time
	json_data = cJSON_Print(root);
	printf("===================\n%s\n===================\n",json_data);
	SendHmsData(0,(uchar*)json_data, strlen(json_data));
	cJSON_Delete(root);
	free(json_data);
	return 0;
}

int HMSManager::TestSendDataToHmsServer(char *HmsServerUrl,char *Pwd)
{
	m_mutex.Enter();
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	MQTTClient_SSLOptions ssl_opts = MQTTClient_SSLOptions_initializer;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;

	conn_opts.keepAliveInterval = 20;  
	conn_opts.cleansession = 1; 
	conn_opts.username = m_UUID; 
	conn_opts.password = Pwd;   
	ssl_opts.trustStore = CAFILE;  
	conn_opts.ssl = &ssl_opts;
    MQTTClient_create(&client, HmsServerUrl, m_UUID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;

    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
		m_mutex.Leave();
        return rc;
    }
    pubmsg.payload = (void*)NULL;
    pubmsg.payloadlen = 0;
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
   	MQTTClient_publishMessage(client, TOPIC0, &pubmsg, &token);
	printf("Waiting for up to %d seconds for publication of %s\n"
            "on topic %s for client with ClientID: %s\n",
            (int)(TIMEOUT/1000), PAYLOAD, TOPIC0, CLIENTID);
    rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
    printf("Message with delivery token %d delivered\n", token);

    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
	m_mutex.Leave();
    return rc;
}
int HMSManager::GetWlanIp(std::string & result)
{
	CURL *curl;  
    CURLcode res;  
    curl = curl_easy_init();  
	if (curl) 
	{
		char* url = "http://app.cppluscloud.com/api/v1/myip";
		MemChunk chunk;
	    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
	    chunk.size = 0; 
		curl_easy_setopt(curl, CURLOPT_URL, url);  
        //指定回调函数  
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);  
		//这个变量可作为接收或传递数据的作用  
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

        res = curl_easy_perform(curl);  
		if (CURLE_OK == res) 
		{
			result = (char*)chunk.memory;
		}
		else
		{
			result = "0.0.0.0";
		}
		curl_easy_cleanup(curl);  
		free(chunk.memory);
	}
	return 0;
}

int HMSManager::GetRecordStartTime(char date[HMSDATELEN])
{
	int tmp;
	VideoFiles  List;
	char FileName[128] = {0};
	
	if(access(VIDEO_DIR,F_OK) != 0)
		return -1;

	tmp = file_get_earlist_videolist(&List,NULL,NULL);

	if(tmp < 0)
	{
		_printd("FileCount:%d\n",tmp);
		return -1;	
	}	
	_printd("GetRecordStartTime:%s\n",List.FilesList[0].FileName);
	char*rsstr=strtok(List.FilesList[0].FileName,"_");
	_printd("rs:%s",rsstr);
	strncpy(date,rsstr,HMSDATELEN);
	return 0;
}