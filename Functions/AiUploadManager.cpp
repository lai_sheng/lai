#include <unistd.h>
#include <iostream>
#include <fstream>
#include "APIs/Audio.h"
#include "Functions/PtzTrace.h"
#include "Functions/AiUploadManager.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/RealTime_callback.h"

#include "APIs/Ptz.h"//LHC

using namespace std;
PATTERN_SINGLETON_IMPLEMENT(AiUploadManager);

AiUploadManager::AiUploadManager():CThread("AiUpload",TP_AIUPLOAD),m_bLoop(VD_TRUE)
{
	m_fp 			= NULL;
	hInfo			= {0};
	faceUpFreq		= FACEUPLOAD_S;
	strangerUpFreq	= FACEUPLOAD_S;
	cryUpFreq		= CRYUPLOAD_S;	
	
	m_packet_count 	= 0;
	m_inputData		= 0;



	iSuspend = 0;
	
	CreateThread();	
}
AiUploadManager::~AiUploadManager()
{
	DestroyThread();
	m_bLoop = VD_FALSE;	
}

void remove_func(char *path)
{
	if( NULL == path){
		return;
	}

	if(0 != remove(path)){
		perror("remove::");
		printf("path:%s\n",path);
	}
}

int OnPushFaceData(ScenePicInfo* pPicInfo)
{
	if(pPicInfo == NULL) return -1;

	if(g_AiUpload.m_packet_count > 30){
		printf("m_packet_count(%d) full \n\n",g_AiUpload.m_packet_count);
		g_AiUpload.FreePicBuffer(pPicInfo);
		free(pPicInfo);
		pPicInfo = NULL;
	}

	printf("m_packet_count(%d) \n\n",g_AiUpload.m_packet_count);
	
	g_AiUpload.mMutex.Enter();
	g_AiUpload.PacketList.push_back(pPicInfo);
	g_AiUpload.m_packet_count ++;
	g_AiUpload.mMutex.Leave();

}

void AiUploadManager::Start()
{
	int iRet = 0;

	m_cCfgGeneral.attach(this, (TCONFIG_PROC)&AiUploadManager::onConfigGeneral);
	m_cCfgGeneral.update();
	onConfigGeneral(m_cCfgGeneral,iRet);
	
	m_AiUpload.attach(this, (TCONFIG_PROC)&AiUploadManager::onConfigAiUpload);
	m_AiUpload.update();
	onConfigAiUpload(m_AiUpload,iRet);

}
void AiUploadManager::Stop()
{
	ScenePicInfo* pInfo = NULL;
	
	FaceChangeSnap_Stop();
	
	while(m_packet_count > 0){
		mMutex.Enter();	

		mPACKET_LIST::iterator it;
		
		it = PacketList.begin();
		PacketList.pop_front();		
		m_packet_count -- ;
		pInfo = *it;
		
		mMutex.Leave();	

		FreePicBuffer(pInfo);
		free(pInfo);
		pInfo = NULL;
	}
}
int AiUploadManager::FreePicBuffer(ScenePicInfo* pPicInfo)
{
	int i = 0;
	
	if( pPicInfo == NULL)	return 0;

	if(pPicInfo->pAddr != NULL)	
	{
		free(pPicInfo->pAddr);
		pPicInfo->pAddr = NULL;
	}

	for(i = 0 ; i < pPicInfo->iPackCount ; i ++)
	{
		if(pPicInfo->pBodyInfo == NULL)
			break;

		if(0 != pPicInfo->pBodyInfo[i].picPath[0] &&
			0 == access(pPicInfo->pBodyInfo[i].picPath,F_OK)){
			remove_func(pPicInfo->pBodyInfo[i].picPath);
		}
		
		
		if( pPicInfo->pBodyInfo[i].pAddr != NULL)
		{
			free(pPicInfo->pBodyInfo[i].pAddr);
			pPicInfo->pBodyInfo[i].pAddr = NULL;
		}
	}
	if(pPicInfo->pBodyInfo != NULL)
	{
		free(pPicInfo->pBodyInfo);
		pPicInfo->pBodyInfo = NULL;
	}
	
	return 0;
}
//pPath长度必须要大等于 256
int AiUploadManager::GetStoreDir(char *pPath)
{
#if 0
	char tmp_path[256] 		   = "/tmp/face";
	char face_record_path[256] = {0};
	
	if(pPath == NULL)	return -1;

	snprintf(face_record_path,sizeof(face_record_path),"%s/face",TARGET_DIR);

	if(0 == access(face_record_path,F_OK)){
		memcpy(pPath,face_record_path,sizeof(face_record_path));
		return 0;
		
	}else if(0 == access(tmp_path,F_OK)){
		memcpy(pPath,tmp_path,sizeof(tmp_path));
		return 0;
	}
	
	if( 0 <= file_sys_init()){
		mkdir_mul(face_record_path);
		memcpy(pPath,face_record_path,sizeof(face_record_path));
		return 0;		
	}
	else{
		mkdir_mul(tmp_path);
		memcpy(pPath,tmp_path,sizeof(tmp_path));
		return 0;
	}
#else
	char tmp_path[256] 		   = "/app/face";

	mkdir_mul(tmp_path);
	memcpy(pPath,tmp_path,sizeof(tmp_path));
#endif
	return 0;
}
int AiUploadManager::ProcessList()
{
	ScenePicInfo* pInfo = NULL;
	
	if(m_packet_count > 0){
		mMutex.Enter();	

		mPACKET_LIST::iterator it;
		
		it = PacketList.begin();
		PacketList.pop_front();		
		m_packet_count -- ;
		pInfo = *it;
		
		mMutex.Leave();	

		UploadFaceData(pInfo);
		
		FreePicBuffer(pInfo);
		free(pInfo);
		pInfo = NULL;
		
		return 1;
	}

	return 0;
}

int AiUploadManager::UploadFaceData(ScenePicInfo* pInfo)
{
#ifdef IPC_JZ
	return 0;
#endif
	static char strangerCnt	= 0;
	static uint otherUploadTime[STRANGER_NUM] 	= {0};
 	static FaceJpegInfo mJpegInfo[STRANGER_NUM]	= {0};

	static uint faceUploadTime[FACELIB_MAX] 	= {0};

	
	int i 		= 0;
	int tUid 	= 0;
	RT_UpLoadAlarmInfo 	 faceinfo		= {0};
	RT_QueryRespFaceInfo RespFaceInfo	= {0};
	
	faceinfo.AlarmType = RT_FACE;

	if(iSuspend){
		_printd("Suspending,dump this face");
	}

	printf("UploadFaceData..........................\n\n");

	if(NULL == pInfo || NULL == pInfo->pBodyInfo){
		printf("pInfo->pBodyInfo == NULL..........................\n\n");
		return 0;
	}
	
	for(i = 0; i < pInfo->iPackCount; i ++)
	{
		#if 0
		if(pInfo->pBodyInfo[i].pAddr == NULL || pInfo->pBodyInfo[i].iLen <= 0)
		{
			_printd("The Data of (PersonID :%d) is error",pInfo->pBodyInfo[i].PersonId);
			continue;
		}
		#else
		if(pInfo->pBodyInfo[i].pAddr == NULL){
			_printd("The Data of (PersonID :%d) is error..",pInfo->pBodyInfo[i].PersonId);
			remove_func(pInfo->pBodyInfo[i].picPath);
			continue;
		}		
		#endif
		tUid = pInfo->pBodyInfo[i].PersonId;
//1s内连续上两张还没入库的陌生人照片,第一张用于陌生人入库，后面的就直接丢掉
		if(tUid < 0){
			if(pInfo->mPts <= otherUploadTime[(strangerCnt+STRANGER_NUM-1)%STRANGER_NUM]+2){
				_printd("Dump the face in %lds..",pInfo->mPts);
				remove_func(pInfo->pBodyInfo[i].picPath);
				continue;
			}
		}

//控制 识别同一个人 60s内只上传一次
		if(tUid >= 0 ){

			if(tUid < FACELIB_MAX){//家人
				if(0 != faceUploadTime[tUid] && pInfo->mPts < faceUploadTime[tUid] + faceUpFreq){
					_printd("The face(%d) have been uploaded in %lds..",
						pInfo->pBodyInfo[i].PersonId,faceUploadTime[tUid]);
					remove_func(pInfo->pBodyInfo[i].picPath);
					continue;
				}
				faceUploadTime[tUid] = pInfo->mPts;
			}
			else{//陌生人
				if(tUid >= 100 && tUid < 100 + STRANGER_NUM){
					if(0 != otherUploadTime[tUid-100] &&
					pInfo->mPts < otherUploadTime[tUid-100] + strangerUpFreq){
						_printd("The stranger(%d) have been uploaded in %lds..",
							pInfo->pBodyInfo[i].PersonId,otherUploadTime[tUid-100]);
						remove_func(pInfo->pBodyInfo[i].picPath);
						continue;

					}
					otherUploadTime[tUid-100] = pInfo->mPts;
				}
				else{
					_printd("Error Uid(%d)",tUid);
				}
					
			}
		}
			
		memset(&faceinfo,0,sizeof(faceinfo));

		if( NULL != pInfo->pAddr && 0 < pInfo->iLen){
			faceinfo.ImageInfo.buf  = (uchar*)(pInfo->pAddr);
			faceinfo.ImageInfo.size = pInfo->iLen;
#if 0
			FILE *fp = fopen("/tmp/test.jpeg","w+");
			if(fp != NULL){
				int pppp = 0;
				pppp = fwrite(faceinfo.ImageInfo.buf,1,faceinfo.ImageInfo.size,fp);
				fclose(fp);

				printf("pppp:%d\n",pppp);
		
			}
#endif					
		}
		else{
			printf("pInfo->pAddr == NULL..........................\n\n");
		}
		faceinfo.TimeStamp 		= pInfo->mPts;
		faceinfo.FaceInfo.age 	= pInfo->pBodyInfo[i].FaceInfo.age;
		faceinfo.FaceInfo.glass = pInfo->pBodyInfo[i].FaceInfo.glass;
		if(pInfo->pBodyInfo[i].FaceInfo.sex == 0){
			strcpy(faceinfo.FaceInfo.strsex,"M");
		}
		else{
			strcpy(faceinfo.FaceInfo.strsex,"F");
		}
		memcpy(faceinfo.FaceInfo.strFeeling,pInfo->pBodyInfo[i].FaceInfo.strFeeling,
			sizeof(faceinfo.FaceInfo.strFeeling));

		if( 0 <= pInfo->pBodyInfo[i].PersonId && 
			OPERA_OK == QueryUserFace(pInfo->pBodyInfo[i].PersonId,&RespFaceInfo)){
			faceinfo.IdentifyInfo.fIdentirate = 99.99;
			memcpy(faceinfo.IdentifyInfo.strName,RespFaceInfo.faceinfo[0].strName,
				sizeof(faceinfo.IdentifyInfo.strName));
			memcpy(faceinfo.IdentifyInfo.strFaceUrl,RespFaceInfo.faceinfo[0].strUrl,
				sizeof(faceinfo.IdentifyInfo.strFaceUrl));
		}
		faceinfo.FaceInfo.fangle.x 		= pInfo->pBodyInfo[i].HeadRct.x;
		faceinfo.FaceInfo.fangle.y 		= pInfo->pBodyInfo[i].HeadRct.y;
		faceinfo.FaceInfo.fangle.width	= pInfo->pBodyInfo[i].HeadRct.w;
		faceinfo.FaceInfo.fangle.height	= pInfo->pBodyInfo[i].HeadRct.h;
		
		faceinfo.FaceInfo.buf = (unsigned char*)(pInfo->pBodyInfo[i].pAddr);
		faceinfo.FaceInfo.size= pInfo->pBodyInfo[i].iLen;
#ifndef SUNING
		#ifndef IPC_JZ
		RealTimeUploadAlarmInfo(faceinfo);
		#else
		_printd("ERROR:: this sdk can't support to upload");
		#endif
#endif
#if 1
//陌生人入库
		if(tUid < 0)
		{
			if(mJpegInfo[strangerCnt].uid){
				//mod_user
				Mod_UserFace(mJpegInfo[strangerCnt].uid,mJpegInfo[strangerCnt].name,mJpegInfo[strangerCnt].path,
							mJpegInfo[strangerCnt].name,pInfo->pBodyInfo[i].picPath);
				remove_func(mJpegInfo[strangerCnt].path);
				
				memset(mJpegInfo[strangerCnt].path,0,sizeof(mJpegInfo[strangerCnt].path));
				memcpy(mJpegInfo[strangerCnt].path,pInfo->pBodyInfo[i].picPath,
						sizeof(mJpegInfo[strangerCnt].path));

			}
			else{
				mJpegInfo[strangerCnt].uid = 100+strangerCnt;
				snprintf(mJpegInfo[strangerCnt].name,sizeof(mJpegInfo[strangerCnt].name),
					"Unknow%d",mJpegInfo[strangerCnt].uid);
				memcpy(mJpegInfo[strangerCnt].path,pInfo->pBodyInfo[i].picPath,
						sizeof(mJpegInfo[strangerCnt].path));
				Add_UserFace(mJpegInfo[strangerCnt].uid,mJpegInfo[strangerCnt].name,mJpegInfo[strangerCnt].path);
			}

			otherUploadTime[strangerCnt] = pInfo->mPts;
			if(++strangerCnt >= STRANGER_NUM){
				strangerCnt = 0;
			} 
			
		}
#endif	

		remove_func(pInfo->pBodyInfo[i].picPath);
	}

	return 0;
}

//返回 0 成功 1 失败 2 参数错误 (返回值根据pass定义)
int AiUploadManager::AddUserFace(RT_ModNewFaceInfo FaceInfo)
{
	int i 	= 0 ;
	int pos = -1;	

	FaceJpegInfo mJpegInfo = {0};
	RT_ModifyFaceInfo ModifyFaceInfo = {0};	

	if(NULL == m_fp){
		OpenRecordFile();
	}

	if(NULL != m_fp){
		if(hInfo.cnt == 0){
			pos = 0;
		}else if(hInfo.cnt >= FACELIB_MAX){
			//pos = -1;
			return PARAM_FULL;
		}
//先检测原库是否有相关信息
		memcpy(ModifyFaceInfo.strUrl,FaceInfo.strUrl,sizeof(ModifyFaceInfo.strUrl));
		memcpy(ModifyFaceInfo.strName,FaceInfo.strName,sizeof(ModifyFaceInfo.strName));
		if(OPERA_OK == ModUserFace(ModifyFaceInfo,FaceInfo)){
			return OPERA_OK;
		}

		
		for(i = 0 ; i < FACELIB_MAX; i ++){
			if(0 == hInfo.rec[i]){
				pos = i;
				break;
			}
		}

		if(pos >= 0){
			mJpegInfo.uid	= pos;
			mJpegInfo.age	= FaceInfo.age;
			mJpegInfo.glass	= FaceInfo.glass;
			if(FaceInfo.strsex[0] == 'M'){
				mJpegInfo.sex	= 0;
			}
			else{
				mJpegInfo.sex	= 1;
			}
			_printd("name:%s",FaceInfo.strName);
			memcpy(mJpegInfo.name,FaceInfo.strName,sizeof(mJpegInfo.name));
			memcpy(mJpegInfo.path,FaceInfo.jpegPath,sizeof(mJpegInfo.path));
			memcpy(mJpegInfo.url,FaceInfo.strUrl,1024);
			_printd("name:%s",mJpegInfo.name);
			//add_user();
			Add_UserFace(mJpegInfo.uid,mJpegInfo.name,mJpegInfo.path);
			
			hInfo.cnt ++;
			hInfo.rec[pos] = 1;
			if(0 < fseek(m_fp,sizeof(hInfo)+pos*sizeof(mJpegInfo),SEEK_SET)){
				perror("fseek");
			}
			if(sizeof(mJpegInfo) > fwrite(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
				perror("add fwrite");
			}
			if(0 < fseek(m_fp,0,SEEK_SET)){
				perror("fseek..");
			}
			if(sizeof(hInfo) > fwrite(&hInfo,1,sizeof(hInfo),m_fp)){
				perror("add fwrite..");
			}

			_printd("sizeof(mJpegInfo):%d,cnt:%d",sizeof(mJpegInfo),hInfo.cnt);


			if(0 < fseek(m_fp,0,SEEK_SET)){
				perror("fseek..");
			}
			if(sizeof(hInfo) <= fread(&hInfo,1,sizeof(hInfo),m_fp)){
				_printd("sizeof(hInfo):%d,",sizeof(hInfo));
	

				_printd("hInfo.cnt:%d",hInfo.cnt);
			}
			return OPERA_OK;
		}
	}

	return OPERA_FAIL; 
}
int AiUploadManager::DelUserFace(RT_ModifyFaceInfo FaceInfo)
{
	int i 	= 0 ;
	int r	= 0;
	int len = 0;
	FaceJpegInfo mJpegInfo 		= {0};
	
	if(NULL == m_fp){
		OpenRecordFile();
	}

	if(NULL != m_fp){
		if(hInfo.cnt == 0){
			return OPERA_OK;
		}
		
		for(i = 0 ; i < FACELIB_MAX; i ++){
			if(1 == hInfo.rec[i]){
				if(0 > fseek(m_fp,sizeof(hInfo)+i*sizeof(mJpegInfo),SEEK_SET))
					return OPERA_FAIL;
				
				if(sizeof(mJpegInfo) > fread(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
					perror("del fwrite");
				}
				len = sizeof(mJpegInfo.url)>sizeof(FaceInfo.strUrl)?sizeof(FaceInfo.strUrl):sizeof(mJpegInfo.url);
				if(0 == strncmp(mJpegInfo.url,FaceInfo.strUrl,len)){
					hInfo.rec[i] = 0;
					hInfo.cnt --;

					//del_user
					Del_UserFace(mJpegInfo.uid,mJpegInfo.name,mJpegInfo.path);

					remove(mJpegInfo.path);
					printf("remove %s,curl:%s\n",mJpegInfo.path,mJpegInfo.url);

					fseek(m_fp,0,SEEK_SET);
					if(sizeof(hInfo) > fwrite(&hInfo,1,sizeof(hInfo),m_fp)){
						perror("del fwrite..");
					}	
					return OPERA_OK;
				}
				r++;
			}
			if(r >= hInfo.cnt){
				break;
			}	
		}
	}

	return OPERA_FAIL;
}
int AiUploadManager::ModUserFace(RT_ModifyFaceInfo ModifyFaceInfo,RT_ModNewFaceInfo ModNewFaceInfo)
{
	int i 	= 0;
	int r 	= 0;
	int len = 0;
	FaceJpegInfo mJpegInfo 		= {0};
	FaceJpegInfo mNewJpegInfo 	= {0};	
	
	if(NULL == m_fp){
		OpenRecordFile();
	}

	if(NULL != m_fp){
		if(hInfo.cnt == 0){
			return PARAM_ERR;
		}
		
		for(i = 0 ; i < FACELIB_MAX; i ++){
			if(1 == hInfo.rec[i]){
				if(0 > fseek(m_fp,sizeof(hInfo)+i*sizeof(mJpegInfo),SEEK_SET))
					return OPERA_FAIL;
				
				if(sizeof(mJpegInfo) > fread(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
					perror("del fwrite");
				}
				len = sizeof(mJpegInfo.url)>sizeof(ModifyFaceInfo.strUrl)?sizeof(ModifyFaceInfo.strUrl):sizeof(mJpegInfo.url);

				if(0 == strncmp(mJpegInfo.url,ModifyFaceInfo.strUrl,len)){
					
					mNewJpegInfo.age	= ModNewFaceInfo.age;
					mNewJpegInfo.glass	= ModNewFaceInfo.glass;
					if(ModNewFaceInfo.strsex[0] == 'M'){
						mJpegInfo.sex	= 0; 
					}
					else{
						mJpegInfo.sex	= 1;
					}					
					memcpy(mNewJpegInfo.name,ModNewFaceInfo.strName,sizeof(mJpegInfo.name));
					memcpy(mNewJpegInfo.path,ModNewFaceInfo.jpegPath,sizeof(mJpegInfo.path));
					memcpy(mNewJpegInfo.url,ModNewFaceInfo.strUrl,1024);


					fseek(m_fp,-sizeof(mJpegInfo),SEEK_CUR);
					if(sizeof(mJpegInfo) > fwrite(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
						perror("add fwrite");
					}
					printf("mod %s(%s),curl:%s(%s),\n",mJpegInfo.path,mNewJpegInfo.path,mJpegInfo.url,mNewJpegInfo.url);
					//mod_user
					Mod_UserFace(mJpegInfo.uid,mJpegInfo.name,mJpegInfo.path,mNewJpegInfo.name,mNewJpegInfo.path);

					remove(mJpegInfo.path);
					return OPERA_OK;
				}
				r++;
			}
			if(r >= hInfo.cnt){
				break;
			}			
		}
	}

	return OPERA_FAIL;
}
int AiUploadManager::QueryUserFace(RT_QueryFaceInfo QueryFaceInfo,RT_QueryRespFaceInfo *RespFaceInfo)
{
	int i 	= 0;
	int r	= 0;
	int len = 0;
	FaceJpegInfo mJpegInfo 		= {0};

	if(NULL == RespFaceInfo){
		_printd("null");
		return PARAM_ERR;
	}
	memset(RespFaceInfo,0,sizeof(RT_QueryRespFaceInfo));
	
	if(NULL == m_fp){
		OpenRecordFile();
	}

	if(NULL != m_fp){
		if(hInfo.cnt == 0){
			return OPERA_OK;
		}
		
		for(i = 0 ; i < FACELIB_MAX; i ++){
			if(1 == hInfo.rec[i]){
				if(0 > fseek(m_fp,sizeof(hInfo)+i*sizeof(mJpegInfo),SEEK_SET))
					return OPERA_FAIL;
				
				if(sizeof(mJpegInfo) > fread(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
					perror("del fwrite");
				}
				len = sizeof(mJpegInfo.name)>sizeof(QueryFaceInfo.strName)?sizeof(QueryFaceInfo.strName):sizeof(mJpegInfo.name);
				if(0 == QueryFaceInfo.strName[0] ||
					0 == strncmp(mJpegInfo.name,QueryFaceInfo.strName,len)){
					
					RespFaceInfo->faceinfo[RespFaceInfo->count].age 	= mJpegInfo.age;
					RespFaceInfo->faceinfo[RespFaceInfo->count].glass	= mJpegInfo.glass;
					if(mJpegInfo.sex){
						RespFaceInfo->faceinfo[RespFaceInfo->count].strsex[0] = 'F';
					}
					else{
						RespFaceInfo->faceinfo[RespFaceInfo->count].strsex[0] = 'M';
					}
				//	memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].jpegPath,mJpegInfo.path,
				//		sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].jpegPath));
					memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].strName,mJpegInfo.name,
						sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].strName));
					memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].strUrl,mJpegInfo.url,
						sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].strUrl));
						
					RespFaceInfo->count ++;
				}
				r++;
			}
			if(r >= hInfo.cnt){
				break;
			}	
		}
			
	}
	if(RespFaceInfo->count > 0){
		return OPERA_OK;
	}

	return OPERA_FAIL;
}
int AiUploadManager::QueryUserFace(int FaceUid,RT_QueryRespFaceInfo *RespFaceInfo)
{
	int i 		= 0;
	int len 	= 0;
	FaceJpegInfo mJpegInfo 		= {0};

	if(NULL == RespFaceInfo){
		return PARAM_ERR;
	}
	memset(RespFaceInfo,0,sizeof(RT_QueryRespFaceInfo));
	
	if(NULL == m_fp){
		OpenRecordFile();
	}

	if(NULL != m_fp && FaceUid >= 0){
		if(hInfo.cnt == 0){
			return PARAM_ERR;
		}
		
		if(1 == hInfo.rec[FaceUid]){
			i = FaceUid;
				
			if(0 > fseek(m_fp,sizeof(hInfo)+i*sizeof(mJpegInfo),SEEK_SET))
				return OPERA_FAIL;
			
			if(sizeof(mJpegInfo) > fread(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
				perror("del fwrite");
			}

			RespFaceInfo->faceinfo[RespFaceInfo->count].age 	= mJpegInfo.age;
			RespFaceInfo->faceinfo[RespFaceInfo->count].glass	= mJpegInfo.glass;
		//	memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].jpegPath,mJpegInfo.path,
		//		sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].jpegPath));
			memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].strName,mJpegInfo.name,
				sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].strName));
			memcpy(RespFaceInfo->faceinfo[RespFaceInfo->count].strUrl,mJpegInfo.url,
				sizeof(RespFaceInfo->faceinfo[RespFaceInfo->count].strUrl));
				
			RespFaceInfo->count ++;
		}

			
	}
	if(RespFaceInfo->count > 0){
		return OPERA_OK;
	}

	return OPERA_FAIL;
}
int AiUploadManager::OpenRecordFile()
{
	int l 			= 0;
	int ret 		= -1;
	char dir[256]	= {0};
	char path[256] 	= {0};

	if(NULL == m_fp ){
		#if 1
		if( 0 == GetStoreDir(dir)){
			snprintf(path,sizeof(path),"%s/face_record_fd",dir);
			if(0 == access(path,F_OK)){
				m_fp = fopen(path,"r+");
			}
			else{
				m_fp = fopen(path,"w+");
			}
		}
		#else
			strncpy(path,"/app/face/face_record_fd",sizeof(path));
			if(0 == access(path,F_OK)){
				m_fp = fopen(path,"r+");
			}
			else{
				m_fp = fopen(path,"w+");
			}	
		#endif
	}
	_printd("path:%s",path);
	if(NULL != m_fp){
		fseek(m_fp,0,SEEK_END);
		if(sizeof(hInfo) > (l = ftell(m_fp))){
			fseek(m_fp,0,SEEK_SET);
			if(sizeof(hInfo) <= fwrite(&hInfo,1,sizeof(hInfo),m_fp)){
				ret = 0;
			}
		}
		else{
			fseek(m_fp,0,SEEK_SET);
			if(sizeof(hInfo) <= (l= fread(&hInfo,1,sizeof(hInfo),m_fp))){
				_printd("sizeof(hInfo):%d,,l:%d",sizeof(hInfo),l);
				ret = 0;

				_printd("hInfo.cnt:%d",hInfo.cnt);
			}
			else{
				fseek(m_fp,0,SEEK_SET);
				if(sizeof(hInfo) <= fwrite(&hInfo,1,sizeof(hInfo),m_fp)){
					ret = 0;
				}
			}
		}
		if(0 != ret){
			fclose(m_fp);
		}
	}

	return ret;
}
int AiUploadManager::AddRecordFileInfo()
{
//temp
#if 1
	m_inputData = 1;
	return OPERA_OK;
#endif

	int i 	= 0;
	int r	= 0;
	int pos = -1;	
	FaceJpegInfo mJpegInfo = {0};
	
	if(NULL == m_fp){
		OpenRecordFile();
	}

//test
	//return OPERA_OK;


	if(NULL != m_fp){
		if(hInfo.cnt == 0){
			m_inputData = 1;
			return OPERA_OK;
		}
		
		for(i = 0 ; i < FACELIB_MAX; i ++){
			if(1 == hInfo.rec[i]){
				if(0 >fseek(m_fp,sizeof(hInfo)+i*sizeof(mJpegInfo),SEEK_SET)){
					return OPERA_FAIL;
				}
				if(sizeof(mJpegInfo) > fread(&mJpegInfo,1,sizeof(mJpegInfo),m_fp)){
					perror("read fread");
				}

				#if 0
				if( 0 != access(mJpegInfo.path,F_OK) &&
					0 != DownFile(mJpegInfo.url,mJpegInfo.path)){
					r++;
					continue;
				}
				#endif
				
				Add_UserFace(mJpegInfo.uid,mJpegInfo.name,mJpegInfo.path);
				r ++;			
			}

			if(r >= hInfo.cnt){
				m_inputData = 1;
				return OPERA_OK;
			}
		}
	}

	return OPERA_FAIL;
}
void AiUploadManager::onConfigAiUpload(CConfigAiUpload& cCfgAi, int& ret)
{
	CONFIG_AIUPLOADFREQ &configOld = m_AiUpload.getConfig();
	CONFIG_AIUPLOADFREQ &configNew = cCfgAi.getConfig();

	configOld = configNew;

	faceUpFreq		= configNew.iFaceFreq;
	strangerUpFreq	= configNew.iStrngerFreq;
	cryUpFreq		= configNew.iCryFreq;		

	_printd("faceUpFreq:%ds,strangerUpFreq:%ds,cryUpFreq:%ds",
		faceUpFreq,strangerUpFreq,cryUpFreq);
	
}
void AiUploadManager::onConfigGeneral(CConfigGeneral& cCfg, int& ret)
{
	CONFIG_GENERAL &configOld = m_cCfgGeneral.getConfig();
	CONFIG_GENERAL &configNew = cCfg.getConfig();

	if (&configOld == &configNew){
		return;
	}

	iSuspend = configNew.iSuspendMode;
}
void AiUploadManager::ThreadProc()
{
	int mNeedSleep 	= 0;

	sleep(3);

	while(m_bLoop){
		if(!ProcessList()){
			mNeedSleep ++;
		}
		else{
			mNeedSleep = 0;
		}
		
		if(mNeedSleep > 5){
			sleep(1);
		}
		else{
			usleep(100*1000);
		}

		//处于待机状态
		if(iSuspend){
			continue;
		}

//若人脸库未载入则一直尝试载入
		if(!m_inputData){
			AddRecordFileInfo();
		}	
	}
}
