#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>



static int g_com_fd = -1;

static int set_opt(int fd, int nSpeed, int nBits, char nEvent, int nStop)
{
	struct termios newtio, oldtio;
	if(tcgetattr(fd, &oldtio) != 0)
	{
		fprintf(stderr, "setupserial error %s\n", strerror(errno));
		return -1;
	}
	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag |= CLOCAL | CREAD;
	newtio.c_cflag &= ~CSIZE;

	switch(nBits)
	{
		case 7:
			newtio.c_cflag |= CS7;
			break;
		case 8:
			newtio.c_cflag |= CS8;
			break;
	}

	switch(nEvent)
	{
		case 'O'://奇数
			newtio.c_cflag |= PARENB;
			newtio.c_cflag |= PARODD;
			newtio.c_cflag |= (INPCK | ISTRIP);
			break;
		case 'E'://偶数
			newtio.c_cflag |= (INPCK | ISTRIP);
			newtio.c_cflag |= PARENB;
			newtio.c_cflag &= ~PARODD;
			break;
		case 'N'://无奇偶校验位
			newtio.c_cflag &= ~PARENB;
			break;
	}

	switch(nSpeed)
	{
		case 2400:
			cfsetispeed(&newtio, B2400);
			cfsetospeed(&newtio, B2400);
			break;
		case 4800:
			cfsetispeed(&newtio, B4800);
			cfsetospeed(&newtio, B4800);
			break;
		case 9600:
			cfsetispeed(&newtio, B9600);
			cfsetospeed(&newtio, B9600);
			break;
		case 115200:
			cfsetispeed(&newtio, B115200);
			cfsetospeed(&newtio, B115200);
			break;
		case 460800:
			cfsetispeed(&newtio, B460800);
			cfsetospeed(&newtio, B460800);
			break;
		default:
			cfsetispeed(&newtio, B9600);
			cfsetospeed(&newtio, B9600);
			break;
	}

	if(nStop == 1)
		newtio.c_cflag &= ~CSTOPB;
	else if (nStop == 2)
		newtio.c_cflag |= CSTOPB;

	newtio.c_cc[VTIME] = 0;
	newtio.c_cc[VMIN] = 0;

	tcflush(fd, TCIFLUSH);

	if((tcsetattr(fd, TCSANOW, &newtio)) != 0)
	{
		fprintf(stderr, "tcsetattr error %s\n", strerror(errno));
		return -1;
	}

	printf("set down!\n");

	return 0;
}
int InitLedTty(void)
{
	if((g_com_fd = open("/dev/ttyAMA3", O_RDWR, 0644)) < 0)
	{
		fprintf(stderr, "open ttyAMA3 failed %s\n", strerror(errno));
		return -1;
	}
	printf("open ttyAMA3,g_com_fd is %d\n");
	if((set_opt(g_com_fd, 9600, 8, 'N', 1)) < 0)
	{
		fprintf(stderr, "set_opt error %s\n", strerror(errno));
		return -1;
	}
	return 0;
}

int SetChnLedState(int no, int flag)
{
	int i;
	static int old_chn_state[64];  
	for (i=0; i<64; i++)
	{
		old_chn_state[i] = -1;
	}
	if(flag == old_chn_state[no])
		return 0;
	for(int iChan = 0; iChan < 2; iChan++)
	{
		char cmd[6]="";
		cmd[0]=0x7a;
		cmd[1]=0x6b;
		cmd[2]=0x5e;
		cmd[3]=no;
		switch (flag)
		{
			case 0:
				cmd[4] = 0x00;  //turn off
				break;
			case 1:
				cmd[4] = 0x01; //all the time lighting...
				break;
			case 2:
				cmd[4] = 0x02;  //before 0.5 seconds light turn on   behind 0.5 seconds turn off
				break;
			case 3:
				cmd[4] = 0x03;  //odd seconds light turn on,even seconds light turn off
				break;
			case 4:
				cmd[4] = 0x04;  //fast frequent turn on
				break;
			case 5:
				cmd[5] = 0x05;  //never turn on
				break;
			default:
				break;
		}

		cmd[5]= cmd[0]+cmd[1]+cmd[2]+cmd[3]+cmd[4];
		write(g_com_fd, cmd, 6);
	}
	old_chn_state[no] = flag;
	return 0;
}

int SetNetLedState(int flag)
{
	static int old_net_state = -1;
	if(flag == old_net_state)
		return 0;
	for(int iNet = 0; iNet < 2; iNet++)
	{
		char cmd[6]="";
		cmd[0]=0x7a;
		cmd[1]=0x6b;
		cmd[2]=0x5e;
		cmd[3]=0x80;
		cmd[4]=flag;

		cmd[5] = cmd[0]+cmd[1]+cmd[2]+cmd[3]+cmd[4];
		write(g_com_fd, cmd, 6);
	}
	old_net_state = flag;
	return 0;
}


int SetHddLedState(int flag)
{
	static int old_hdd_state=-1;
	if(flag == old_hdd_state)
		return 0;

	for(int iHdd = 0; iHdd < 2; iHdd++)
	{
		char cmd[6] = "";
		cmd[0] = 0x7a;
		cmd[1] = 0x6b;
		cmd[2] = 0x5e;
		cmd[3] = 0x90;
		cmd[4] = flag;

		cmd[5] = cmd[0]+cmd[1]+cmd[2]+cmd[3]+cmd[4];
		write(g_com_fd, cmd, 6);
	}
	old_hdd_state = flag;

	return 0;
}

