#include "Functions/Ptz.h"
#include "Functions/Alarm.h"
#include "Functions/Encode.h"
#include "System/Console.h"
#include "Devices/DevComm.h"
#include "Net/NetClient/NetCaptureManager.h"
#include "../Net/OprPdu/IVNControl.h"
#include "Configs/ConfigDigiChManager.h"


enum PTZ_OPERATE
{
    PTZ_OPERATE_START,
    PTZ_OPERATE_STOP,
};
#if 0

/* 辅助功能,简化后面的代码编写 */
namespace __HelperFunction__ {
    template <typename T>
    static void pushvalue(lua_State *l, T index)
    {
        lua_pushnumber(l, index);
    }

    static void pushvalue(lua_State *l, const char *str)
    {
        lua_pushstring(l, str);
    }

    template <typename T>
    static void getvalue(T& pDest, lua_State *l)
    {
        pDest = (T)lua_tonumber(l, -1);
    }

    static void getvalue(char *pDest, lua_State *l)
    {
        // 执行下面一语句时要保证pDest的长度大于所要的字符串长度
        strcpy(pDest, (char *)lua_tostring(l, -1));
    }

    template <typename T1, typename T2>
    static int getfield(T1& pDest, lua_State *l, T2 arg)
    {
        pushvalue(l, arg);
        lua_gettable(l, -2);
        getvalue(pDest, l);
        lua_pop(l, 1);
        return 0;
    }

    template <typename T1, typename T2>
    static int getfield(T1 * pDest, lua_State *l, T2 arg)
    {
        pushvalue(l, arg);
        lua_gettable(l, -2);
        getvalue(pDest, l);
        lua_pop(l, 1);
        return 0;
    }
} /* End of Helper Function */


using namespace __HelperFunction__;

#endif
static const uint baudTab[] = {
    1200, 2400, 4800, 9600, 19200, 38400, 76800, 115200,
};

static const uchar BCDTable[] =
{
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
};

inline int GetBaud(uint baud)
{
    int i;
    for (i = 0; i < int(sizeof(baudTab) / sizeof(baudTab[0])); i++)
    {
        if (baudTab[i] == baud) 
        {
            return i;
        }
    }
    return -1;
}

static int GetBCDIndex(uchar BCD)
{
    for (int i = 0; i < int(sizeof(BCDTable) / sizeof(BCDTable[0])); i++)
    {
        if (BCDTable[i] == BCD)
        {
            return i;
        }
    }
    return -1;
}

PATTERN_SINGLETON_IMPLEMENT(CPtz);

CPtz::CPtz():m_pPtz_Opt_Attr(NULL),m_ProtocolCount(0), 
    m_Timers("SemiduplexPtz"), m_PtzLinkTimer("TPtzLink"),MJPtzTimer("MJPtzTimer"),m_MutexLink(MUTEX_FAST)
{
//  m_luaEngine = ScriptEngine::Instance()->NewThread();
    trace("CPtz::CPtz()>>>>>>>>>\n");


	for (int i = 0; i < N_SYS_CH; i++)
	{
		//added by wyf on 20100919
		for(int j = 0; j < ALARM_PTZLINK_NUMS; j++)
		{
			memset(&m_ptzLinkCmd[j][i], 0, sizeof(PTZ_LINK_STRUCT));
			m_ptzLinkCmd[j][i].index = -1;
		}		
		//end added by wyf on 20100919

        /*预置点相关成员变量*/
        memset(m_szPresetInfor[i], 0, sizeof(NET_PRESET_INFOR)*PTZ_PRESETNUM);
        m_iPresetNum[i] = 0; //用于记录当前通道的预置点的数目
        m_iTourGroup = 0; //记录当前的巡航组
		m_iTourGroupArray[i] = 0;//added by wyf on 20100511
        m_iTourPosition[i] = 0; //记录当前巡航组中的当前巡航位置
        m_iTourNum[i] = 0; //记录每个通道有多少条巡航线路
        memset(m_szTourInfor[i], 0, sizeof(NET_TOUR_INFOR)*PTZ_CHANNELS);//各个通道的各条线路的巡航组中的预置点信息
        char szTemp[NAME_LEN] = {0};
        sprintf(szTemp, "TimerTour%2d", i);
        m_pTimerTour[i] = new CTimer(szTemp);
        memset(&m_szDefaultPreset[i], 0, sizeof(DefaultPreset));
#ifdef FUN_ALARMIN_LINKER_PTZ		
	m_iTourPause[i] = FALSE;
	m_iPrePreset[i] = FALSE;
#endif	
#ifdef FUNTION_PRESET_TITLE
        m_iExit[i] = FALSE;
#endif
    }



}

CPtz::~CPtz()
{
    if (m_pPtz_Opt_Attr)
    {
        delete [] m_pPtz_Opt_Attr;
    }
}

#ifdef FUNTION_PRESET_TITLE
void CPtz::onConfigPresetSet(CConfigPreset* pConfig, int & ret)
{
    for (int i = 0; i < N_SYS_CH; i++)
    {
        CONFIG_PRESET& configOld = m_CConfigPreset.getConfig(i);
        CONFIG_PRESET& configNew = pConfig->getConfig(i);
        if (memcmp(&configOld, &configNew, sizeof(CONFIG_PRESET)))
        {
		 m_ConfigPreset[i] = configNew;
	 }
    }
}
#endif
/*!
\b Description        :云台配置更新回调函数\n

\b Revisions        :    
- 2007-04-05        yangb        created
*/

void CPtz::onConfigPTZSet(CConfigPTZ *pConfig, int &ret)
{
    //!如果配置发生变化
    for (int i = 0; i < N_SYS_CH; i++)
    {
        CONFIG_PTZ& configOld = m_CConfigPtz.getConfig(i);
        CONFIG_PTZ& configNew = pConfig->getConfig(i);
        if (memcmp(&configOld, &configNew, sizeof(CONFIG_PTZ)))
        {
            configOld = configNew;
            m_ConfigPtz[i] = configNew;
            AppPtzConfig(&m_ConfigPtz[i], i);
			//!使新配置立即生效					
            PTZ_ATTR strPtzAttr;
            memset(&strPtzAttr, 0, sizeof(PTZ_ATTR));
            strPtzAttr.baudrate = configNew.dstComm.nBaudBase;
            strPtzAttr.databits = configNew.dstComm.nDataBits;
            strPtzAttr.parity   = configNew.dstComm.iParity;
            strPtzAttr.stopbits = configNew.dstComm.iStopBits;
#ifndef KBD_ON_DEV_PTZ     
            /* 只需要设置模拟通道 */
            if( (i >= 0 && i <g_nCapture)
                || 1 == CConfigLocalDigiChCfg::getLatest(i).BPTZLocal)            
            {
                CDevPtz::instance()->SetAttribute(&strPtzAttr);
            }
#endif

#ifdef VN_IPC
            //720P球机设置地址操作
            char chType[128] = {0};
            SystemGetDeviceType(chType);
            if (0 == strcmp(chType, "VP26M2H"))
            {
                SetDomeAddr(m_ConfigPtz[i].ideviceNo);
            }
#endif
        }
    }
}

/*!
\b Description        :云台报警配置更新回调函数\n

\b Revisions        :    
- 2007-04-05        yangb        created
*/
void CPtz::onConfigPTZAlarmSet(CConfigPTZAlarm *pConfig, int &ret)
{
    //!如果配置发生变化
    for (int i = 0; i < N_SYS_CH; i++)
    {
        CONFIG_PTZ& configOld = m_CConfigPtzAlarm.getConfig(i);
        CONFIG_PTZ& configNew = pConfig->getConfig(i);
        if (memcmp(&configOld, &configNew, sizeof(CONFIG_PTZ)))
        {
            configOld = configNew;
            m_ConfigPtzAlarm[i] = configNew;
        }
    }
}

/*!
    \b Description        :    根据参数设置信息\n
    \b Argument            :    CONFIG_PTZ *pConfig, int iChannel
    \param    pConfig        :    配置结构指针
    \param    iChannel    :    通道号
    \return                
    \b Revisions        :    
*/
void CPtz::AppPtzConfig(CONFIG_PTZ *pConfig, int iChannel)
{
    PTZ_OPT_STRUCT ptz_opt;
    if (strcmp(pConfig->strProtocolName, "LTM8230C") == 0)
    {
        memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));

        ptz_opt.cmd = PTZ_OPT_AUXON;
        ptz_opt.arg1 = pConfig->ideviceNo;

        //发3次
        for (int i = 0; i < 3; i++)
        {
            Start(iChannel, &ptz_opt);
            SystemSleep(10);
        }

        memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
        switch(pConfig->dstComm.nBaudBase)
        {
        case 9600:
            ptz_opt.arg1 = 1;
            break;
        case 19200:
            ptz_opt.arg1 = 2;
            break;
        case 38400:
            ptz_opt.arg1 = 3;
            break;
        default:
            //回归原来的值
            pConfig->dstComm.nBaudBase = m_ConfigPtz[iChannel].dstComm.nBaudBase;
            ptz_opt.arg1 = 0;
            break;
        }
        if (ptz_opt.arg1 != 0)
        {
            ptz_opt.cmd = PTZ_OPT_AUXOFF;
                //发3次
            for (int i = 0; i < 3; i++)
            {
                Start(iChannel, &ptz_opt);
                SystemSleep(10);
            }
        }
    }
        
    int index = 0;
    if ((index = GetIndex(m_ConfigPtz[iChannel].strProtocolName)) > 0)
    {
        if (m_pPtz_Opt_Attr[index - 1].Type == PTZ_MATRIX)
        {
            memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
            ptz_opt.cmd = PTZ_OPT_SWITCH;
            ptz_opt.arg1 = m_ConfigPtz[iChannel].iNuberInMatrixs;
            ptz_opt.arg2 = m_ConfigPtz[iChannel].ideviceNo;
            Start(iChannel, &ptz_opt);
        }
    }
}


int CPtz::GetIndex(const char * name)
{
    // 从1开始是因为有个默认值为NONE
    for (int i = 1; i <= m_ProtocolCount; i++)
    {
        if (strcmp(name, m_pPtz_Opt_Attr[i - 1].Name) == 0)
        {
            return i;
        }
    }
    return 0;
}

/*!
    \b Description        :    加载LUA脚本\n

    \b Revisions        :    
*/
VD_BOOL CPtz::LoadLuaScript()
{
#if 0
    lua_State *L = (lua_State *)(*m_luaEngine); 
    int nRet,nTop;
    nTop = lua_gettop(L);
    /* 得到Global.PtzCtrl */
    lua_getglobal(L, "Global");
    lua_pushstring(L, "PtzCtrl");
    lua_gettable(L, -2);
    assert(lua_istable(L, -1));
    lua_pushvalue(L, -1);
    m_ptzCtrlRef = luaL_ref(L, LUA_REGISTRYINDEX);
    /* 在这里我们调用LoadScriptFile函数加载云台控制协议 */
    lua_pushstring(L,"LoadProtocols");
    lua_gettable(L, -2);
    assert(lua_isfunction(L, -1));
    nRet = lua_pcall(L, 0, 0, 0);
    assert(nRet == 0);
    //获得协议的个数
    nRet = GetProtocolCount();
    assert(nRet > 0);
    //从LUA中得到所有协议属性
    GetProtocolAttr();
    //检测协议中的名字
    //CheckName();
    lua_settop(L, nTop);
#endif
    return TRUE;
}

void CPtz::Init()
{
    trace("CPtz::Init()>>>>>>>>>\n");
    m_pDevPtz = CDevPtz::instance();

    //ReadConfig();
    //!获取配置信息
#ifdef FUNTION_PRESET_TITLE
    m_CConfigPreset.update();
#endif
    m_CConfigPtz.update();
    m_CConfigPtzAlarm.update();
    for( int i = 0 ;i < N_SYS_CH; i ++)
    {
        m_ConfigPtz[i] = m_CConfigPtz.getConfig(i);
        m_ConfigPtzAlarm[i] = m_CConfigPtzAlarm.getConfig(i);
#ifdef FUNTION_PRESET_TITLE
	m_ConfigPreset[i]	=  m_CConfigPreset.getConfig(i);
#endif
    }
    //加载LUA脚本，获得相关属性
    LoadLuaScript();
    //!将云台配置的函数注册到配置模块
    m_CConfigPtz.attach(this,(TCONFIG_PROC)&CPtz::onConfigPTZSet);
#ifdef FUNTION_PRESET_TITLE
    m_CConfigPreset.attach(this,(TCONFIG_PROC)&CPtz::onConfigPresetSet);
#endif
    //此处首先检查配置的协议是否在现有协议之中，如果程序升级后协议没有了,但是配置还在这样就会有问题
    for (int i = 0; i < N_SYS_CH; i++)
    {
        CONFIG_PTZ &cfg = m_CConfigPtz.getConfig(i);

        //没有获取到该协议,将协议置空
        if (GetIndex(cfg.strProtocolName) == 0)
        {
            memset(cfg.strProtocolName, '\0', MAX_PTZ_PROTOCOL_LENGTH);
            strcpy(cfg.strProtocolName, "NONE");
        }
    }
    m_CConfigPtz.commit();
    m_CConfigPtzAlarm.attach(this,(TCONFIG_PROC)&CPtz::onConfigPTZAlarmSet);
    CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CPtz::onAppEvent);
       /*added by magic.shi 100114*/
        PTZ_LoadPresetInfoFile();
        PTZ_LoadTourInfoFile();
#ifdef VN_IPC 
    //720P球机使用485与机芯通信，必须在启动时设置Ptz属性
    char chType[128] = {0};
    SystemGetDeviceType(chType);
//设备启动时ptz 默认为9600  ，参数保存不为9600
//双向透明传输 接收时会有问题
	
	{
        PTZ_ATTR sPtzAttr;
        memset(&sPtzAttr, 0, sizeof(PTZ_ATTR));
        sPtzAttr.baudrate = m_ConfigPtz[0].dstComm.nBaudBase;
        sPtzAttr.databits = m_ConfigPtz[0].dstComm.nDataBits;
        sPtzAttr.parity   = m_ConfigPtz[0].dstComm.iParity;
        sPtzAttr.stopbits = m_ConfigPtz[0].dstComm.iStopBits;
        m_pDevPtz->SetAttribute(&sPtzAttr);

        SetDomeAddr(m_ConfigPtz[0].ideviceNo);
    }
#endif
    trace("CPtz::Init()>>>>>>>end @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@>>\n");
}

#ifdef VN_IPC
//仅720P球机使用
void CPtz::SetDomeAddr(char Addr)
{
	char setCmd[] = {0xf9, 0x64/*地址，使用广播*/, 0x01, 0x0a, 0x00, 0x00, 0x01, 0x00/*设置值*/ , 0xff/*校验*/};
	setCmd[7] = Addr;
	setCmd[8] = setCmd[0]+ setCmd[1]+ setCmd[2]+ setCmd[3]+ setCmd[4]+ setCmd[5]+ setCmd[6]+ setCmd[7];
	const int writelen = 9;

	m_pDevPtz->Write((void *)setCmd, writelen, 0);
}
#endif

void CPtz::StartSemiduplex()
{
#ifdef PTZ_RW_SEMIDUPLEX
    m_pDevPtz->Start(this, (CDevPtz::SIG_DEV_PTZ_DATA)&CPtz::SendSemiduplexCMD);

#endif
}

/*!
    \b Description        :    延时处理报警解码器的输出\n
    \b Argument            :    PARAM arg

    \b Revisions        :    
*/
void CPtz::SendAlarmOut(uint arg)
{

}


/*!
    \b Description        :    得到支持的协议数量\n

    \b Revisions        :    
*/
int CPtz::GetProtocolCount()
{
#if 0
    lua_State *L = (lua_State *)(*m_luaEngine); 

    /* 得到总的云台控制协议数 */
    lua_rawgeti(L, LUA_REGISTRYINDEX, m_ptzCtrlRef);
    lua_pushstring(L, "ProtocolCount");
    lua_gettable(L, -2);
    m_ProtocolCount = (int)lua_tonumber(L, -1);
    lua_pop(L, 2);
	return m_ProtocolCount;
#endif    
    return 0;
}

/*!
    \b Description        :    从LUA中获得协议属性\n

    \b Revisions        :    
*/
void CPtz::GetProtocolAttr()
{
#if 0
    lua_State *L = (lua_State *)(*m_luaEngine); 


    assert(m_ProtocolCount > 0);
    if (m_pPtz_Opt_Attr)
    {
        delete [] m_pPtz_Opt_Attr;
    }
    
    m_pPtz_Opt_Attr = new PTZ_OPT_ATTR[m_ProtocolCount];

    if (m_pPtz_Opt_Attr == NULL)
    {
        return;
    }


    lua_rawgeti(L, LUA_REGISTRYINDEX, m_ptzCtrlRef);
    assert(lua_istable(L, -1));

    for (int i = 0; i < m_ProtocolCount; i++)
    {
        int nRet;
        lua_pushstring(L, "GetProtocolAttr");
        lua_gettable(L, -2);
        assert(lua_isfunction(L, -1));

        lua_pushnumber(L, i + 1);
        nRet = lua_pcall(L, 1, 1, 0);

        assert(nRet == 0);
        if ( 0 != nRet)
        {
            trace("error, run function %s\n", lua_tostring(L, -1));
            lua_pop(L, 2);
            return;
        }
        
        assert(lua_istable(L, -1));
        if (!lua_istable(L, -1))
        {
            trace("the return value isn't a table\n");
            lua_pop(L, 2);
            return;
        }

        /*
        使用如下的命令得到相应的值，这是LUA脚本里的值，不可更改
        {"HighMask", "LowMask", "Name",  "CamAddrMin", "CamAddrMax", 
        "MonAddrMin", "MonAddrMax",     "PresetMin", "PresetMax", "TourMin", "TourMax", "PatternMin", "PatternMax",
        "TileSpeedMin", "TileSpeedMax", "PanSpeedMin", "PanSpeedMax",
        "AuxMin","AuxMax", "Internal", "Type"};
        */
        getfield(m_pPtz_Opt_Attr[i].HighMask, L,"HighMask");/*!<获取普通云台控制操作掩码*/
        getfield(m_pPtz_Opt_Attr[i].LowMask, L,"LowMask");
        //getfield(m_pPtz_Opt_Attr[i].HighAuxMask,L,"HighAuxMask");/*!<获取直观辅助操作的高低掩码*/
        getfield(m_pPtz_Opt_Attr[i].LowAuxMask,L,"LowAuxMask");
        getfield(&(m_pPtz_Opt_Attr[i].Name[0]), L, "Name");
        
        getfield(m_pPtz_Opt_Attr[i].CamAddrMin, L, "CamAddrMin");
        getfield(m_pPtz_Opt_Attr[i].CamAddrMax, L, "CamAddrMax");
        getfield(m_pPtz_Opt_Attr[i].MonAddrMin, L, "MonAddrMin");
        getfield(m_pPtz_Opt_Attr[i].MonAddrMax, L, "MonAddrMax");
        getfield(m_pPtz_Opt_Attr[i].PresetMin, L, "PresetMin");
        getfield(m_pPtz_Opt_Attr[i].PresetMax, L, "PresetMax");
        getfield(m_pPtz_Opt_Attr[i].TourMin, L, "TourMin");
        getfield(m_pPtz_Opt_Attr[i].TourMax, L, "TourMax");
        getfield(m_pPtz_Opt_Attr[i].PatternMin, L, "PatternMin");
        getfield(m_pPtz_Opt_Attr[i].PatternMax, L, "PatternMax");
        getfield(m_pPtz_Opt_Attr[i].TileSpeedMin, L, "TileSpeedMin");
        getfield(m_pPtz_Opt_Attr[i].TileSpeedMax, L, "TileSpeedMax");
        getfield(m_pPtz_Opt_Attr[i].PanSpeedMin, L, "PanSpeedMin");
        getfield(m_pPtz_Opt_Attr[i].PanSpeedMax, L, "PanSpeedMax");
        getfield(m_pPtz_Opt_Attr[i].AuxMin, L, "AuxMin");
        getfield(m_pPtz_Opt_Attr[i].AuxMax, L, "AuxMax");
        getfield(m_pPtz_Opt_Attr[i].Internal, L, "Internal");
        getfield(m_pPtz_Opt_Attr[i].Type, L, "Type");
        getfield(m_pPtz_Opt_Attr[i].AlarmLen, L, "AlarmLen");
        
        lua_pop(L,1);  /* 弹出LUA返回的结果 */
    }
    lua_pop(L,1); 
#endif	
}

/*!
    \b Description        :    得到指定通道的协议属性\n
    \b Argument            :    PTZ_OPT_ATTR *pAttr, int index
    \param pAttr        :    保存协议属性的指针
    \param index        :    指定的协议序号

    \b Revisions        :    
*/
void CPtz::GetProtocolAttr(PTZ_OPT_ATTR *pAttr, int index)
{
    if (index <= m_ProtocolCount)
    {
        memcpy(pAttr, &m_pPtz_Opt_Attr[index], sizeof(PTZ_OPT_ATTR));
    }
}

/*!
    \b Description        :    得到指定通道的协议属性\n
    \b Argument            :    PTZ_OPT_ATTR *pAttr, int iChannel
    \param pAttr        :    保存协议属性的指针
    \param iChannel        :    指定的通道号

    \b Revisions        :    
*/
VD_BOOL CPtz::SetProtocol(int iChannel, uchar type)
{
#if 0
	lua_State *L = (lua_State *)(*m_luaEngine); 

    char *Name = NULL;
    PTZ_ATTR Attr;
    uint DestAddr = 0;
    uint MonAddr = 0;

    if (type == PTZ_WITH_VIDEO)
    {
        CONFIG_PTZ *pCfg = &m_ConfigPtz[iChannel];
        Name = pCfg->strProtocolName;
        Attr.baudrate = pCfg->dstComm.nBaudBase;
        Attr.databits = pCfg->dstComm.nDataBits;
        Attr.parity = pCfg->dstComm.iParity;
        Attr.stopbits = pCfg->dstComm.iStopBits;
        Attr.reserved = 0;
        DestAddr = pCfg->ideviceNo;
        MonAddr = pCfg->iNuberInMatrixs;
    }
    else if (type == PTZ_WITHOUT_VIDEO)
    { 
        CONFIG_PTZ *pCfg = &m_ConfigPtzAlarm[iChannel];
        Name = pCfg->strProtocolName;
        Attr.baudrate = pCfg->dstComm.nBaudBase;
        Attr.databits = pCfg->dstComm.nDataBits;
        Attr.parity = pCfg->dstComm.iParity;
        Attr.stopbits = pCfg->dstComm.iStopBits;
        Attr.reserved = 0;
        DestAddr = pCfg->ideviceNo;
    }
    else
    {
        return FALSE;
    }

    int index = 0;
    index = GetIndex(Name);

    if (index == 0)
    {
        return FALSE;
    }

#if defined(DVR_GB)
    m_HDR.bit_rate = GetBaud(Attr.baudrate);//填充数据头

    m_HDR.send_num = 3;
    m_HDR.parity   = Attr.parity;

    //reverse只有一个有用，要留意
    m_HDR.reverse  = 0;
#else

    //此处将配置信息转换位底层接口所需要的数据
    int stopBitsTable[3] = {0,COMM_ONESTOPBIT,COMM_TWOSTOPBITS};
    Attr.stopbits = stopBitsTable[Attr.stopbits];


#ifndef KBD_ON_DEV_PTZ

    /* 只需要设置模拟通道 */
    if((iChannel>= 0 && iChannel <g_nCapture)
        || 1 == CConfigLocalDigiChCfg::getLatest(iChannel).BPTZLocal)
    {
        if(!m_pDevPtz->SetAttribute(&Attr))
        {
            trace("CPTZ::onSent():PTZ set attribute error\n");
            return FALSE;
        }
    }
#endif

#endif

    //下面是设置协议部分
    lua_rawgeti(L, LUA_REGISTRYINDEX, m_ptzCtrlRef);
    assert(lua_istable(L, -1));

    lua_pushstring(L, "SetProtocol");
    lua_gettable(L, -2);
    assert(lua_isfunction(L, -1));

    // LUA的地址从1开始,而值为0时刚好为代表无
    lua_pushnumber(L, index);
    lua_pushnumber(L, DestAddr);
    if (m_pPtz_Opt_Attr[index - 1].Type == PTZ_MATRIX)
    {
        lua_pushnumber(L, MonAddr);
    }
    else
    {
        lua_pushnil(L);
    }
    int nRet = lua_pcall(L, 3, 0, 0); 
    assert(nRet == 0);

    if ( 0 != nRet)
    {
        
        trace("error, run function %s\n", lua_tostring(L, -1));
        lua_pop(L, 1);
        return FALSE;
    }
    lua_pop(L, 1);
#endif	
    return TRUE;
}

void CPtz::Write(uchar *buffer, int length, int iChannel)
{
    m_pDevPtz->Write(buffer, length,iChannel);
}

VD_BOOL CPtz::Start(int iChannel, PTZ_OPT_STRUCT *pPTZStruct)
{
    CGuard  l_cGuard(m_Mutex);
    uchar    buffer[128];
    uchar    lenBuffer[16];
    int        group = 0;
    VD_BOOL    ret = FALSE;
    int     len = 0;

    //是模拟通道，必须进行此操作
    if ((iChannel >= 0) && (iChannel < ICaptureManager::instance()->GetLogicChnNum()) && SetProtocol(iChannel))
    {
        memset(buffer, 0, sizeof(buffer));
        memset(lenBuffer, 0, sizeof(lenBuffer));
		{
	        if ((group = Operate(PTZ_OPERATE_START, pPTZStruct, buffer, lenBuffer)) > 0)
	        {
	            m_pDevPtz->SetDebug(TRUE);
	            for (int i = 0; i < group; i ++)
	            {
	                if (i > 0)
	                {
	                    SystemSleep(200);
	                }

					_printd("group is [%d]  cmd[%d] arg [%d,%d,%d]",group, pPTZStruct->cmd,pPTZStruct->arg1, pPTZStruct->arg2, pPTZStruct->arg3);
					switch(buffer[len+3])
					{
						case 0x20:  //ZOOM Out
						case 0x40:  //ZOOM IN 
							//pPTZStruct->arg1 *= 2;
							//if     (pPTZStruct->arg1> 16) pPTZStruct->arg1 = 16;
							//else if(pPTZStruct->arg1<  0) pPTZStruct->arg1 = 1;
							buffer[len+4] = pPTZStruct->arg1; 
							break;
							
						case 0x23://PATTERN ON
						case 0x1f://RECORD ON
						case 0x21://RECORD OFF
							
						case 0x1b:
						case 0x1d:
						case 0x11:
						case 0x13:
							buffer[len+4] = pPTZStruct->arg1;
							break;
						default:
							break;
					}		 
	                Write(&buffer[len], lenBuffer[i], iChannel);
	                len += lenBuffer[i];
	           }
	            ret = TRUE;
	        }
	}
    }
#ifdef FUNTION_PRESET_TITLE
  if(PTZ_OPT_GOTOPRESET == pPTZStruct->cmd
		&& TRUE == m_ConfigPreset[iChannel].Enable)	
    {
	    NET_PRESET_INFOR *pszPresetInfo = PTZ_GetPreset(iChannel,pPTZStruct->arg1-1);
	    if (NULL != pszPresetInfo)
	    {
	    	   PTZ_SetPresetName(iChannel,pszPresetInfo[0].cPresetName,true,m_ConfigPreset[iChannel].x,m_ConfigPreset[iChannel].y);	
		   m_iExit[iChannel] = TRUE;
	    }		
    }
    else if(m_iExit[iChannel] && !(pPTZStruct->arg1 ==0 &&pPTZStruct->arg2 ==0 ))
    {
		PTZ_SetPresetName(iChannel,"",false,m_ConfigPreset[iChannel].x,m_ConfigPreset[iChannel].y);	
		 m_iExit[iChannel] = FALSE;
    }
#endif
    return ret;
}

/*!
    \b Description        :    发送停止命令\n
    \b Argument            :    int iChannel, PTZ_OPT_STRUCT *pPTZStruct
    \param    iChannel    :    通道号
    \param    pPTZStruct    :    操作命令结构
    \return    成功为TURE，出错FALSE            

    \b Revisions        :    
*/

VD_BOOL CPtz::Stop(int iChannel, PTZ_OPT_STRUCT *pPTZStruct)
{
    CGuard  l_cGuard(m_Mutex);
    uchar    buffer[128];
    uchar    lenBuffer[16];
    int        group = 0;
    VD_BOOL    ret = FALSE;
    int        len = 0;

    if ((iChannel >= 0) && (iChannel < ICaptureManager::instance()->GetLogicChnNum())
        && SetProtocol(iChannel))
    {
        memset(buffer, 0, sizeof(buffer));
        memset(lenBuffer, 0, sizeof(lenBuffer));
        if ((group = Operate(PTZ_OPERATE_STOP, pPTZStruct, buffer, lenBuffer)) > 0)
        {
            //防止停止命令接收不到，发送两次停止命令 
            for (int ii = 0; ii < 2; ii++)
            {
                len = 0;
                m_pDevPtz->SetDebug(ii == 0);
                for (int i = 0; i < group; i ++)
                {
                    if (i > 0)
                    {
                        SystemSleep(200);
                    }
                    Write(&buffer[len], lenBuffer[i], iChannel);
                    len += lenBuffer[i];
                }
            }
            ret = TRUE;
        }
    }
    
    return ret;
}

/*!
    \b Description        :    解析数据\n
    \b Argument            :    BYTE opttype, PTZ_OPT_STRUCT *pPTZStruct)
    \param    opttype        :    数据类型，是开始还是停止
    \param    pPTZStruct    :    数据参数结构
    \return    解析的数据的长度            

    \b Revisions        :    
*/
int CPtz::Operate(uchar opttype, PTZ_OPT_STRUCT *pPTZStruct, uchar *buffer, uchar *lenBuffer)
{
#if 0
    int cmd  = pPTZStruct->cmd;
    int arg1 = PTZ_ARG_NONE;
    int arg2 = PTZ_ARG_NONE;
    int arg3 = PTZ_ARG_NONE;
    lua_State *L = (lua_State *)(*m_luaEngine); 
	
    // 转化数据
    switch(cmd)
    {
    case PTZ_OPT_LEFTUP:
    case PTZ_OPT_RIGHTUP:
    case PTZ_OPT_LEFTDOWN:
    case PTZ_OPT_RIGHTDOWN:
        arg1 = pPTZStruct->arg1;
        arg2 = pPTZStruct->arg2;
        if (cmd == PTZ_OPT_LEFTUP)
        {
            arg3 = getStandardType(PTZ_OPT_LEFT, PTZ_OPT_UP);
        }
        else if (cmd == PTZ_OPT_RIGHTUP)
        {
            arg3 = getStandardType(PTZ_OPT_RIGHT, PTZ_OPT_UP);
        }
        if (cmd == PTZ_OPT_LEFTDOWN)
        {
            arg3 = getStandardType(PTZ_OPT_LEFT, PTZ_OPT_DOWN);
        }
        else if (cmd == PTZ_OPT_RIGHTDOWN)
        {
            arg3 = getStandardType(PTZ_OPT_RIGHT, PTZ_OPT_DOWN);
        }
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_UP:
    case PTZ_OPT_DOWN:
        arg1 = pPTZStruct->arg1;
        arg3 = getStandardType(PTZ_OPT_NONE, cmd);
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_LEFT:
    case PTZ_OPT_RIGHT:
        arg1 = pPTZStruct->arg1;
        arg3 = getStandardType(cmd);
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_ZOOM_WIDE:
    case PTZ_OPT_ZOOM_TELE:
        arg1 = pPTZStruct->arg1;
        arg3 = getStandardType(PTZ_OPT_NONE, PTZ_OPT_NONE, cmd);
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_FOCUS_FAR:
    case PTZ_OPT_FOCUS_NEAR:
        arg1 = pPTZStruct->arg1;
        arg3 = getStandardType(PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, cmd);
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_IRIS_LARGE:
    case PTZ_OPT_IRIS_SMALL:
        arg1 = pPTZStruct->arg1;
        arg3 = getStandardType(PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, PTZ_OPT_NONE, cmd);
        cmd = PTZ_OPT_STANDARD;
        break;
    case PTZ_OPT_STANDARD:
        arg1 = pPTZStruct->arg1;
        arg2 = pPTZStruct->arg2;
        arg3 = pPTZStruct->arg3;
        break;
    case PTZ_OPT_SETPRESET:
    case PTZ_OPT_CLEARPRESET:
    case PTZ_OPT_GOTOPRESET:
        arg1 = pPTZStruct->arg1;
        break;
    case PTZ_OPT_ADDTOUR:
    case PTZ_OPT_DELTOUR:
        arg1 = pPTZStruct->arg1;
        arg2 = pPTZStruct->arg2;
        break;
    case PTZ_OPT_STARTTOUR:
    case PTZ_OPT_STOPTOUR:
    case PTZ_OPT_CLEARTOUR:
        arg1 = pPTZStruct->arg1;
        break;
    case PTZ_OPT_SETPATTERNBEGIN:
    case PTZ_OPT_SETPATTERNEND:
    case PTZ_OPT_STARTPATTERN:
    case PTZ_OPT_STOPPATTERN:
    case PTZ_OPT_CLEARPATTERN:
        arg1 = pPTZStruct->arg1;
        break;
    case PTZ_OPT_POSITION:
        arg1 = pPTZStruct->arg1;
        arg2 = pPTZStruct->arg2;
        arg3 = pPTZStruct->arg3;
        break;
    case PTZ_OPT_AUXON:
    case PTZ_OPT_AUXOFF:
        arg1 = pPTZStruct->arg1;
	if(arg1 > 8)
	{
		arg1 = 1;
	}
        break;
    case PTZ_OPT_ALARM_SEARCH:
    case PTZ_OPT_SWITCH:
        arg1 = pPTZStruct->arg1;
        arg2 = pPTZStruct->arg2;
        break;	
   case PTZ_OPT_AUTOSCANON:		
   case PTZ_OPT_SETLEFTLIMIT:
   case PTZ_OPT_SETRIGHTLIMIT: 	
   case PTZ_OPT_AUTOSCANOFF:
   case PTZ_OPT_SETTOURSTART:
   case PTZ_OPT_ADDPRESET: 
   case PTZ_OPT_SETTOURSTOP:	
   case PTZ_OPT_AUTOHOME:
   case PTZ_OPT_SETAUTOHOME:	
   case PTZ_OPT_WAITETIME: 
   case PTZ_OPT_POWERRESET:
   case PTZ_OPT_VIDEOBLIND:
   case PTZ_OPT_BLINDDEL:
   case PTZ_OPT_SETPARAM:
   case PTZ_OPT_QUERYPPARAM:
   case PTZ_OPT_SETSTANDARD:
   case PTZ_OPT_INIT_END:
        arg1 = pPTZStruct->arg1; //tournum
        arg2 = pPTZStruct->arg2; //preset num
        break;
		
   case PTZ_OPT_BLINDSET:
   case PTZ_OPT_SETHD:
   case PTZ_OPT_POSITIONIN:
   case PTZ_OPT_POSITIONOUT:
        arg1 = pPTZStruct->arg1; //tournum
        arg2 = pPTZStruct->arg2; //preset num
        arg3= pPTZStruct->arg3;
        break;
  case PTZ_OPT_SENSOR:
  	arg1 = pPTZStruct->arg1;
	break;
    default:
        break;
    }

    lua_rawgeti(L, LUA_REGISTRYINDEX, m_ptzCtrlRef);
    assert(lua_istable(L, -1));

    if (opttype == PTZ_OPERATE_START)
    {
        lua_pushstring(L, "StartPTZ");
    }
    else if (opttype == PTZ_OPERATE_STOP)
    {
        lua_pushstring(L, "StopPTZ");
    }
    lua_gettable(L, -2);

    // 压入数据
    lua_pushnumber(L, cmd);
    int arg[] = {arg1, arg2, arg3};
    for (int i = 0; i < 3; i++)
    {
        if (arg[i] != PTZ_ARG_NONE)
        {
            lua_pushnumber(L, arg[i]);
        }
        else
        {
            lua_pushnil(L);
        }
    }

    int nRet = lua_pcall(L, 4, 4, 0);  

    if ( 0 != nRet)
    {
        trace("error, run function %s\n", lua_tostring(L, -1));
        lua_pop(L, 2);
		    assert(nRet == 0);

        return 0;
    }


    if (!lua_istable(L, -2))
    {
        trace("cmd[%d], the return value isn't a table or the command isn't exist\n", cmd);
        lua_pop(L, 5);
        return 0;
    }
    
    if(!lua_istable(L,-4))
    {
        trace("cmd[%d],the return value isn't a table or the command don't have a len\n",cmd);
        lua_pop(L,5);
        return 0;
    }

    int len = 0;
    len = (int)lua_tonumber(L, -1);
    lua_pop(L, 1);
    if (len <= 0)
    {
        lua_pop(L, 4);
        return 0;
    } 

    for (int i = 0; i < len; i++)
    {
        //下标从1开始
        getfield(buffer[i], L, i + 1);
    }
    lua_pop(L, 1);

    int group = 0;
    group = (int)lua_tonumber(L,-1);
    lua_pop(L,1);
    if(len <= 0)
    {
        lua_pop(L, 3);
        return 0;
    }

    for(int i = 0;i < group; i++)
    {
        getfield(lenBuffer[i], L, i + 1);
    }
    lua_pop(L,2);

   

#if defined(DVR_GB)
    m_HDR.cmd_len  = len + 1;

    if(!m_pDevPtz->SetPTZHead((void *)&m_HDR,sizeof(PTZ_HDR)))
    {
        trace("CPTZ::SetProtocol():PTZ set PTZHead error\n");
        return 0;
    }
#endif

    return group;
#else
	return 0;
#endif

}

/*!
    \b Description        :    发送半双工命令\n
    \b Argument            :    void *pData, int length
    \param    pData        :    暂无用
    \param    length        :    暂无用
    \return                

    \b Revisions        :    
*/
void CPtz::SendSemiduplexCMD(void *pData, int length)
{
    static uchar    buffer[128];
    PTZ_OPT_STRUCT opt_struct;
    char *name;

    memset(&opt_struct, 0, sizeof(PTZ_OPT_STRUCT));

    for (int i = 0; i < N_SYS_CH + N_PTZ_ALARM; i++)
    {
        int index = 0;
        if (i < N_SYS_CH)
        {
            if ((index = GetIndex(m_ConfigPtz[i].strProtocolName)) == 0)
            {
                continue;
            }
            name = m_ConfigPtz[i].strProtocolName;
        }
        else
        {
            if ((index = GetIndex(m_ConfigPtzAlarm[i - N_SYS_CH].strProtocolName)) == 0)
            {
                continue;
            }
            name = m_ConfigPtzAlarm[i - N_SYS_CH].strProtocolName;                
        }
        if ((m_pPtz_Opt_Attr[index - 1].HighMask & BITMSK(PTZ_ALARM - 1))
            || (m_pPtz_Opt_Attr[index - 1].LowMask & BITMSK(PTZ_ALARM - 1)))
        {
            m_Mutex.Enter();
            SystemSleep(100);
            opt_struct.cmd = PTZ_OPT_ALARM_SEARCH;
            

            //设置属性
            if (i < N_SYS_CH)
            {
                SetProtocol(i);
                opt_struct.arg1 = i;
            }
            else
            {
                SetProtocol(i - N_SYS_CH, PTZ_WITHOUT_VIDEO);
                opt_struct.arg1 = i - N_SYS_CH;
            }

            uchar buf[128];
            uchar lenBuffer[16];
            memset(buf, 0, sizeof(buf));
            memset(lenBuffer, 0, sizeof(lenBuffer));
            int leng;
            int len = 0;
            
            if ((leng = Operate(PTZ_OPERATE_START, &opt_struct, buf, lenBuffer)) > 0)
            {
                m_pDevPtz->SetDebug(FALSE);

                m_pDevPtz->Write(buf, lenBuffer[leng - 1],i);

            }

            int maxlen = m_pPtz_Opt_Attr[index - 1].AlarmLen;

            leng = m_pDevPtz->Read(&buffer[len], sizeof(buffer) - len);

            if (leng <= 0)
            {
                m_Mutex.Leave();
                continue;
            }
            len += leng;

            if (len >= maxlen)
            {
                int DiscardLength = 0;
                int channel = i >= N_SYS_CH ? i - N_SYS_CH : i;
                if (strcmp(name, "LTM8230C") == 0)
                {
                    DiscardLength = ParseLTM8230CData(buffer, maxlen, channel);
                }
                else if (strcmp(name, "BANKNOTE") == 0)
                {
                    DiscardLength = ParseBanknoteData(buffer, maxlen, channel);
                }

                if (DiscardLength != maxlen)
                {
                    // 扔掉数据
                    uchar *p = &buffer[DiscardLength];
                    for (int k = 0; k < len - DiscardLength; k++)
                    {
                        buffer[k] = *p;
                        p++;
                    }
                }
                len -= DiscardLength;
            }
            m_Mutex.Leave();
    
        }

        SystemSleep(200);

    }    

    SystemSleep(200);

}

/*!
    \b Description        :    找到DH-CC440报警解码器的数据\n
    数据格式：４个字节：0xff＋地址＋报警点数目＋报警点状态
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号
    \return    扔掉处理完的长度或非合理数据的长度            

    \b Revisions        :    
*/
int CPtz::ParseDHCC440Data(uchar *pData, int length, int iChannel)
{
    int startpos = length;
    if (pData[0] == 0xff)
    {
        startpos = 0;
    }

    for (int i = 1; i < length; i++)
    {
        if ((startpos == length) && (pData[i] == 0xff))
        {
            startpos = i;
            break;
        }
    }

    if (startpos == 0)
    {
        DHCC440Proc(pData, length, iChannel);
        startpos = length;
    }

    return startpos;
}

/*!
    \b Description        :    报警解码器的数据处理\n
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号

    \b Revisions        :    
*/
void CPtz::DHCC440Proc(uchar *pdata, int length, int iChannel)
{
    int        	index 	= -1;
    uint    	addr 	= pdata[1];
    uint    	state 	= pdata[3];  //最高的一个字节指示哪一个报警解码器
    //从返回的地址查找协议
    for (int i = 0; i < N_PTZ_ALARM; i++)
    {
        if (m_ConfigPtzAlarm[i].ideviceNo == (int)addr)
        {
            index = i;
            break;
        }
    }
    //trace("%#x, %#x, %#x, %#x, index is %d\n", pdata[0], pdata[1], pdata[2], pdata[3],index);

    if (index != -1)
    {
        state |= index << 24; 
        //这里需要修改，为编译通过暂时屏蔽
        //g_Alarm.SendMessage(XM_ALARM_ALARM, state, ALM_INPUT_PTZ);

    }

}

/*!
    \b Description        :    找到温湿度控制器的数据\n
    数据格式见文档
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号
    \return    扔掉处理完的长度或非合理数据的长度            

\b Revisions        :    
*/
int CPtz::ParseLTM8230CData(uchar *pData, int length, int iChannel)
{
    int startpos = length;
    int stoppos = length;

    if (pData[0] == ':')
    {
        startpos = 0;
    }

    for (int i = 1; i < length; i++)
    {
        if ((startpos == length) && (pData[i] == ':'))
        {
            startpos = i;
            break;
        }

        if ((stoppos == length) && (pData[i] == 0x0A) && (pData[i - 1] == 0x0D))
        {
            stoppos = i;
        }

        if (stoppos - startpos == length)
        {
            //处理数据
            LTM8230CProc(pData, length, iChannel);
            startpos = length;
        }
    }

    return startpos;
}

/*!
    \b Description        :    温湿度控制器的数据处理\n
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号

    \b Revisions        :    
*/
void CPtz::LTM8230CProc(uchar *pData, int length, int iChannel)
{
    uchar data[64];
    int        datalen = (length + 3) / 2;
    /*
    trace("\nValue:");
    for (int i = 1; i < length - 2; i++)
    {
        trace("0x%02x ", pData[i]);
        if (GetBCDIndex(pData[i]) < 0)
        {
            trace("\n");
            tracepoint();
            return;
        }        
    }
    trace("\n");
    */
    
    data[0] = pData[0];
    uchar checksum = 0;
    for (int i = 1; i < datalen - 2; i++)
    {
        uchar hh = GetBCDIndex(pData[2 * i - 1]);
        uchar ll = GetBCDIndex(pData[2 * i]);
        data[i] = (hh << 4) + ll;
        if (i != datalen - 3)
        {
            checksum += data[i];
        }
    }
    
    data[datalen - 2] = pData[length - 2];
    data[datalen - 1] = pData[length - 1];

    //计算校验
    checksum = (~checksum) + 1;
    if (checksum != data[datalen - 3])
    {
        //tracepoint();
        return;
    }

    //提取温度和湿度值
    int temperature = 0;
    int humidity = 0;

    char title[128];
    int titlelen = 0;

    //为了保持精度，温度提高100倍
    temperature = ((((data[6] & 0x07) << 8) + data[7]) * 625)/100;
    if (data[6] & 0x08)
    {
        temperature = -temperature;    
    }

    memset(title, 0, sizeof(title));
    titlelen = strlen(LOADSTR("encodeoverlay.temperature"));
    sprintf(title, "%s %+d.%02d%s", LOADSTR("encodeoverlay.temperature"), temperature/100, ABS(temperature)%100, LOADSTR("encodeoverlay.tempunit")/*℃*/);
    title[titlelen + 1] = '\t';

    for (int i = 0; i < g_nCapture; i++)
    {
        g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, TRUE, title, i);
    }

    humidity = ((data[8] << 8) + data[9]) / 10;
    memset(title, 0, sizeof(title));
    titlelen = strlen(LOADSTR("encodeoverlay.humidity"));
    sprintf(title, "%s %d%s"/*％RH*/, LOADSTR("encodeoverlay.humidity"), humidity, LOADSTR("encodeoverlay.humiunit"));
    title[titlelen + 1] = '\t';

    for (int i = 0; i < g_nCapture; i++)
    {
        g_Encode.setOSDTitle(1, OSD_TOP_LEFT_CORNER, TRUE, title, i);
    }

    m_Timers.Stop();
    m_Timers.Start(this, (VD_TIMERPROC)&CPtz::OnClearTitle, 10000, 0);

    //trace("temp = %d, humidity = %d\n", temperature, humidity);
}

/*!
    \b Description        :    找到点钞机的数据\n
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号
    \return    扔掉处理完的长度或非合理数据的长度            

    \b Revisions        :    
*/
int CPtz::ParseBanknoteData(uchar *pData, int length, int iChannel)
{
    int startpos = length;
    BanknoteProc(pData, length, iChannel);

    return startpos;
}

/*!
    \b Description        :    点钞机的数据处理\n
    数据格式：0xBF(后４位表示地址) + 个位＋ 十位　＋　百位　＋0x00　（个，十，百为ASCII码）
    \b Argument            :    BYTE *pData, int length, int iChannel)
    \param    pData        :    数据指针
    \param    length        :    数据长度
    \param  iChannel    :    通道号

    \b Revisions        :    
*/
void CPtz::BanknoteProc(uchar *pData, int length, int iChannel)
{
    uchar hh = pData[0] & 0xf;
    uchar mm = pData[1] & 0xf;
    uchar ll = pData[2] & 0xf;
    
    uchar pos = (pData[0] >> 6) & 0x3;

    int layout = OSD_TOP_LEFT_CORNER;
    switch (pos)
    {
    default:
    case 0:
        break;
    case 1:
        layout = OSD_TOP_RIGHT_CORNER;
        break;
    case 2:
        layout = OSD_BOTTOM_LEFT_CORNER;
        break;
    case 3:
        layout = OSD_BOTTOM_RIGHT_CORNER;
        break;
    }

    
    char title[128];
    int titlelen = strlen(LOADSTR("encodeoverlay.count"));
    memset(title, 0, sizeof(title));
    sprintf(title, "%s %d", LOADSTR("encodeoverlay.count"), hh * 100 + mm * 10 + ll);
    title[titlelen + 1] = '\t';

    g_Encode.setOSDTitle(0, layout, TRUE, title, iChannel);
    
    m_Timers.Stop();
    m_Timers.Start(this, (VD_TIMERPROC)&CPtz::OnClearTitle, 10000, 0);
}

/*!
    \b Description        :    清除屏幕信息\n
    \b Argument            :    PARAM arg
    \param    arg            :    高８位表示字幕下标，后２４位表示通道号，最多支持24个通道 
*/
void CPtz::OnClearTitle(uint arg)
{
    for (int i = 0; i < g_nCapture; i++)
    {
        g_Encode.ClearInfoTitle(i);
    }
}
void CPtz::WritePtz(uchar * pData, uint length,int iChannel)
{
    CGuard   l_cGuard(m_Mutex);
    uchar     buffer[128];

    if(length > 64)//将写485的数据长度限制为64
    {
        return;
    }

    if(SetProtocol(iChannel,PTZ_WITH_VIDEO))
    {
        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer , pData, (int)length);
        m_pDevPtz->SetDebug(TRUE);
        m_pDevPtz->Write(buffer, (int)length,iChannel);
    }
}

void CPtz::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
	int iAlarmPtzLinkEvent = 0;
    //int i = index;
    if(!m_PtzLinkTimer.IsStarted())
    {
        m_PtzLinkTimer.Start(this, (VD_TIMERPROC)&CPtz::ptzLinkOperate, 200, 200);
    }

    if(!param)
    {
        return;
    }

    CGuard  l_cGuard(m_MutexLink);
    int cmd[4] = {PTZ_OPT_NONE, PTZ_OPT_GOTOPRESET, PTZ_OPT_STARTTOUR, PTZ_OPT_STARTPATTERN};
	switch(code)
	{
	case appEventVideoMotion:
		iAlarmPtzLinkEvent = 0;
		break;
	case appEventVideoBlind:
		iAlarmPtzLinkEvent = 1;
		break;
	case appEventVideoLoss:
		iAlarmPtzLinkEvent = 2;
		break;
	case appEventAlarmLocal:
		iAlarmPtzLinkEvent = 3;
		break;
	default:
		break;
	}
    switch (action)
    {
    case appEventStart:

	    PTZ_OPT_STRUCT ptz_opt;
        for (int i = 0; i < g_nLogicNum; i++)
        {
            if (param->bPtzEn && param->PtzLink[i].iType > PTZ_LINK_NONE && param->PtzLink[i].iValue > 0)
		{
			if (param->PtzLink[i].iType > PTZ_LINK_PATTERN)
			{
			    	continue;
			}

			//added by wyf on 20100919
			if(((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPTOUR && param->PtzLink[i].iType == PTZ_LINK_TOUR) 
				||(m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPPATTERN && param->PtzLink[i].iType == PTZ_LINK_PATTERN))
			&& (m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime != 0))
			{
				continue;
			}

			if((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPTOUR)
				&& (param->PtzLink[i].iType != PTZ_LINK_TOUR)
				&&(m_pTimerTour[i]->IsStarted()))
			{	
				m_pTimerTour[i]->Stop();
				m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime = 0;
			}
			
			if((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPPATTERN)
			 	&& (param->PtzLink[i].iType != PTZ_LINK_PATTERN))
			{
			    m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime = 0;
				memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
				//此时云台设备已经没有了动作,联动直接执行
				ptz_opt.cmd = m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd;
				ptz_opt.arg1 = m_ptzLinkCmd[iAlarmPtzLinkEvent][i].value;		

				Start(i, &ptz_opt);
			}
#ifdef FUN_ALARMIN_LINKER_PTZ
			if( m_iPrePreset[i] > 0)
			{	
				if(param->PtzLink[i].iType == PTZ_LINK_PRESET 
					&& iAlarmPtzLinkEvent == 3)
				{
					m_iPrePreset[i] ++;
				}		
  
    			     continue;
			}	
			
			if(param->PtzLink[i].iType == PTZ_LINK_PRESET 
				&& iAlarmPtzLinkEvent == 3)
			{
				m_iPrePreset[i] ++;
			}	
			
			if((iAlarmPtzLinkEvent == 3)
				&&(param->PtzLink[i].iType == PTZ_LINK_PRESET) 
				&&(m_pTimerTour[i]->IsStarted()))
			{
				m_iTourPause[i] = TRUE;
			}
#endif			
			memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
			//此时云台设备已经没有了动作,联动直接执行
			ptz_opt.cmd = cmd[param->PtzLink[i].iType];
			ptz_opt.arg1 = param->PtzLink[i].iValue;
			//解决云台点间巡航联动失败的BUG added by wyf on 20100511
			bool iRet = false;
#ifdef DEF_SOFT_TOUR
			if(PTZ_OPT_STARTTOUR == ptz_opt.cmd)
			{
				ptz_opt.arg1 = param->PtzLink[i].iValue - 1;
				int iRetTour = PTZ_OnPtzTour(ptz_opt, i);
				if(iRetTour != 0)
				{
					continue;
				}
			}
			else
			{
				iRet = Start(i, &ptz_opt) ? true : false;
				if((iRet == false) || (ptz_opt.cmd ==PTZ_OPT_GOTOPRESET))
				{
				//如果是预制点命令则执行完之后直接去掉不用保存
				//如果联动失败，则不保存此次联动的值
				continue;
				}
			}
#else
			iRet = Start(i, &ptz_opt) ? true : false;
			if((iRet == false) || (ptz_opt.cmd ==PTZ_OPT_GOTOPRESET))
			{
				//如果是预制点命令则执行完之后直接去掉不用保存
				//如果联动失败，则不保存此次联动的值
				continue;
			}
#endif
			//end added by wyf on 20100511

			//命令过来执行成功后保存一下状态
			//此处把命令修改为停止命令，用定时器去执行
			//added by wyf on 20100919
			m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd = cmd[param->PtzLink[i].iType] +1;
			m_ptzLinkCmd[iAlarmPtzLinkEvent][i].value = param->PtzLink[i].iValue;
			m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime = 1000 * 5;
            m_ptzLinkCmd[iAlarmPtzLinkEvent][i].index = index;
			//end added by wyf on 20100919
		}
		else	
		///< 正在巡航过程中，修改了使能，那么延时一段时间后停止巡航
		{
			if (((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPTOUR))
				&& (param->bPtzEn == false ||param->PtzLink[i].iValue == 0 ||param->PtzLink[i].iType == PTZ_LINK_NONE)
				&& (m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime != 0)
				&& (m_ptzLinkCmd[iAlarmPtzLinkEvent][i].index == index))
			{
				if ( m_pTimerTour[i]->IsStarted())
				{
					infof("stop tour.\n");					
					m_pTimerTour[i]->Stop();
				}
			}

			if((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPPATTERN)
			 	&&(param->bPtzEn == false ||param->PtzLink[i].iValue == 0 ||param->PtzLink[i].iType == PTZ_LINK_NONE)
			 	&& (m_ptzLinkCmd[iAlarmPtzLinkEvent][i].index == index))
			{
				memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
				//此时云台设备已经没有了动作,联动直接执行
				ptz_opt.cmd = m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd;
				ptz_opt.arg1 = m_ptzLinkCmd[iAlarmPtzLinkEvent][i].value;		
                m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime = 0;
                
				Start(i, &ptz_opt);
			}
				
		}

        }
        break;
    //报警停止后延时一段时间发送停止命令(30s)
    case appEventStop:
        for (int i = 0; i < g_nLogicNum; i++)
        {
			if(((m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPTOUR)||(m_ptzLinkCmd[iAlarmPtzLinkEvent][i].cmd == PTZ_OPT_STOPPATTERN)))
			{
				if (m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime != 0)
				{
					m_ptzLinkCmd[iAlarmPtzLinkEvent][i].leftTime = 10 * 5;
				}
			}
#ifdef FUN_ALARMIN_LINKER_PTZ
			if(iAlarmPtzLinkEvent == 3)
			{
				m_iTourPause[i] = FALSE;
				if(param->PtzLink[i].iType == PTZ_LINK_PRESET)
				{
					m_iPrePreset[i]--;
					if(m_iPrePreset[i] < 0)
					{
						m_iPrePreset[i] = 0;
					}
				}
								
			}
#endif			
			
        }
        break;
    default:
        break;
    }
}


void CPtz::ptzLinkOperate(uint arg)
{
    static int iChannel = 0;

    PTZ_OPT_STRUCT ptz_opt;
     memset(&ptz_opt, 0, sizeof(PTZ_OPT_STRUCT));
    CGuard  l_cGuard(m_MutexLink);
    //!一次只发送一条命令
    //为了防止485总线数据的有效性,此处只能发送一次命令成功即返回
    for (iChannel = iChannel; iChannel < g_nLogicNum; iChannel++)
    {
		//modified by wyf on 20100919
		for(int j = 0; j < ALARM_PTZLINK_NUMS; j++)
		{
			if ((m_ptzLinkCmd[j][iChannel].leftTime == 0) || (m_ptzLinkCmd[j][iChannel].cmd == PTZ_OPT_NONE))
			{
				continue;
			}

			m_ptzLinkCmd[j][iChannel].leftTime--;

			if (m_ptzLinkCmd[j][iChannel].leftTime == 0)
			{
				ptz_opt.cmd = m_ptzLinkCmd[j][iChannel].cmd;
				ptz_opt.arg1 = m_ptzLinkCmd[j][iChannel].value;
				memset(&m_ptzLinkCmd[j][iChannel], 0, sizeof(PTZ_LINK_STRUCT));
                m_ptzLinkCmd[j][iChannel].index = -1;
				//end modified by wyf on 20100919	
				
				//解决云台点间巡航联动失败的BUG added by wyf on 20100511
			#ifdef DEF_SOFT_TOUR
				if(ptz_opt.cmd == PTZ_OPT_STOPTOUR)
				{
					ptz_opt.arg1 = ptz_opt.arg1 - 1;
					int iRet = PTZ_OnPtzTour(ptz_opt, iChannel); 
					if(iRet != 0)
					{
						continue;
					}				
				}
				else
				{
					if (Start(iChannel, &ptz_opt) == FALSE)
					{
						continue;
					}
				}
				
			#else		
				if (Start(iChannel, &ptz_opt) == FALSE)
				{
					continue;
				}
			#endif
			//end  added by wyf on 20100511
			
				break;		
			}
		}
		//end added 
	}

    iChannel++;

    //此处一定要大于或者等于因为iChannle可能两次自加
    if (iChannel >= g_nCapture)
    {
        iChannel = 0;
    }

}

int getStandardType(int hor, int ver, int zoom, int focus, int iris)
{
    int iRetCMD = 0;
    switch (hor)
    {
    case PTZ_OPT_LEFT:
        iRetCMD |= BITMSK(standardLeft);
        break;
    case PTZ_OPT_RIGHT:
        iRetCMD |= BITMSK(standardRight);
        break;
    default:
        break;    
    }
    
    switch(ver)
    {
    case PTZ_OPT_UP:
        iRetCMD |= BITMSK(standardUp);
        break;
    case PTZ_OPT_DOWN:
        iRetCMD |= BITMSK(standardDown);
        break;
    default:
        break;
    }
    
    switch(zoom)
    {
    case PTZ_OPT_ZOOM_WIDE:
        iRetCMD |= BITMSK(standardZoomWide);
        break;
    case PTZ_OPT_ZOOM_TELE:
        iRetCMD |= BITMSK(standardZoomTele);
        break;
    default:
        break;
    }
    
    switch(focus)
    {
    case PTZ_OPT_FOCUS_FAR:
        iRetCMD |= BITMSK(standardFocusFar);
        break;
    case PTZ_OPT_FOCUS_NEAR:
        iRetCMD |= BITMSK(standardFocusNear);
        break;
    default:
        break;
    }
    
    switch(iris)
    {
    case PTZ_OPT_IRIS_LARGE:
        iRetCMD |= BITMSK(standardIrisLarge);
        break;
    case PTZ_OPT_IRIS_SMALL:
        iRetCMD |= BITMSK(standardIrisSmall);
        break;
    default:
        break;
    }
    
    return iRetCMD;
}

#ifdef FUNTION_PRESET_TITLE
bool CPtz::PTZ_SetPresetName(int iChannel,char *pPresetName,int enable,int x,int y)
{
	const CONFIG_VIDEOWIDGET& cfgVW = CConfigVideoWidget::getLatest(iChannel);
	const VIDEO_WIDGET * pVideoWidget = NULL;
	pVideoWidget = &cfgVW.ChannelTitle;
	
	CAPTURE_TITLE_PARAM stTitleParam;
	stTitleParam.index = OSD_OVERLAY_PRESET;
	stTitleParam.enable = 1;//config.Enable;
	stTitleParam.x = 352;
	stTitleParam.y = 288;
	stTitleParam.height= 24;
	//stTitleParam.fg_color = VD_RGBA(0, 0, 0, 255);
	//stTitleParam.bg_color = VD_RGBA(255, 255, 255, 0);
	stTitleParam.fg_color = pVideoWidget->rgbaFrontground;
	stTitleParam.bg_color = pVideoWidget->rgbaBackground;
	int width = g_Locales.GetTextExtent(pPresetName);
	stTitleParam.width =(ushort)(width % 8 ? width + 8 - width % 8 : width);

	VD_RECT Rect;
	Rect.left = 0,
	Rect.top = 0;
	Rect.right = stTitleParam.width;
	Rect.bottom = stTitleParam.height;

	CDevCapture *pDevCap = CDevCapture::instance(iChannel);
	if (pDevCap)
	{
		pDevCap->SetTitle(&stTitleParam, &Rect, pPresetName);
	}
	
	return 0;
}
#endif
//!添加某通道的预置点配置信息,通道号从0开始,预置点信息中的预置点号从1开始
bool CPtz::PTZ_AddPreset(int iChannel,NET_PRESET_INFOR *pszPresetInfo)
{
    int iPresetIndex = 0;
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) || (NULL == pszPresetInfo) )
    {
        return false;
    }

    iPresetIndex = pszPresetInfo->ucPresetId;
    if ( (iPresetIndex > PTZ_PRESETNUM) || (iPresetIndex <= 0) )
    {
        return false;
    }

    m_szPresetInfor[iChannel][iPresetIndex - 1].ucPresetId = iPresetIndex;
    m_szPresetInfor[iChannel][iPresetIndex - 1].ucDWellTime = pszPresetInfo->ucDWellTime;
    m_szPresetInfor[iChannel][iPresetIndex - 1].ucSpeed = pszPresetInfo->ucSpeed;
    strcpy(m_szPresetInfor[iChannel][iPresetIndex - 1].cPresetName,pszPresetInfo->cPresetName);
    m_iPresetNum[iChannel]++;

    PTZ_OPT_STRUCT szPtzStruct;
    memset(&szPtzStruct, 0, sizeof(PTZ_OPT_STRUCT));
    
    szPtzStruct.cmd = PTZ_OPT_SETPRESET;
    szPtzStruct.arg1 = pszPresetInfo->ucPresetId;
    Start(iChannel,&szPtzStruct);
	
    return true;
}

//!获取某通道的预置点信息,通道号从0开始,预置点号从0开始,-1代表获取所有预置点信息,默认为-1
NET_PRESET_INFOR *CPtz::PTZ_GetPreset(int iChannel,int iPresetIndex) 

{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        return NULL;
    }

    if ( (iPresetIndex < 0) || (iPresetIndex >= PTZ_PRESETNUM) )
    {
        return m_szPresetInfor[iChannel];
    }
    else
    {
        return &m_szPresetInfor[iChannel][iPresetIndex];
    }
}

//!删除某通道的预置点配置信息,通道号从0开始,预置点号从1开始,-1代表删除全部,默认为-1
bool CPtz::PTZ_DeletePreset(int iChannel,int iPresetId)
{
    if ( ((iPresetId <= 0) || (iPresetId > PTZ_PRESETNUM)) && (iPresetId != -1) )
    {
        return false;
    }

    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        return false;
    }

    /* 删除所有的预置点信息 */
    if ( iPresetId == -1 )
    {
        memset(m_szPresetInfor[iChannel], 0, PTZ_PRESETNUM * sizeof(NET_PRESET_INFOR));
        m_iPresetNum[iChannel] = 0;
    }
    /* 将presetID置零即删除 */
    else
    {
        m_szPresetInfor[iChannel][iPresetId - 1].ucPresetId = 0;
		memset(&m_szPresetInfor[iChannel][iPresetId - 1], 0,sizeof(NET_PRESET_INFOR));
        m_iPresetNum[iChannel]--;

	 	PTZ_OPT_STRUCT strPtzOpt;
		strPtzOpt.cmd = PTZ_OPT_CLEARPRESET;
		strPtzOpt.arg1 = iPresetId;
		Start(0, &strPtzOpt);
		
    }

    /* 删除已配置轨迹中的该预置点 */    
    PTZ_OPT_STRUCT szPtzTour;
    memset(&szPtzTour, 0, sizeof(PTZ_OPT_STRUCT));
    
    if ( iPresetId != -1 )
    {
        szPtzTour.cmd = PTZ_OPT_DELTOUR;
        szPtzTour.arg2 = iPresetId;
    }
    else
    {
        szPtzTour.cmd = PTZ_OPT_CLEARTOUR;
    }

    for ( int iIndex = 0; iIndex < PTZ_CHANNELS; iIndex++ )
    {
        szPtzTour.arg1 = iIndex;
        PTZ_OnPtzTour(szPtzTour, iChannel);
    }

    return true;
}

//!修改某通道的预置点配置信息,通道号从0开始,预置点信息中的预置点号从1开始
bool CPtz::PTZ_ModifyPreset(int iChannel,NET_PRESET_INFOR *pszPresetInfo,bool bSetPreset)

{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) || (NULL == pszPresetInfo) )
    {
        return false;
    }

    if ( (pszPresetInfo->ucPresetId <= 0) || (pszPresetInfo->ucPresetId > PTZ_PRESETNUM) )
    {
        return false;
    }
    
    //修改预制点不会修改预制点的ID号
    if ( 0 < strlen(pszPresetInfo->cPresetName) )
    {
        strcpy(m_szPresetInfor[iChannel][pszPresetInfo->ucPresetId -1].cPresetName, pszPresetInfo->cPresetName);
    }

    if ( 0 < pszPresetInfo->ucDWellTime )
    {
        m_szPresetInfor[iChannel][pszPresetInfo->ucPresetId -1].ucDWellTime = pszPresetInfo->ucDWellTime;
    }

    if ( 0 < pszPresetInfo->ucSpeed )
    {
        m_szPresetInfor[iChannel][pszPresetInfo->ucPresetId -1].ucSpeed = pszPresetInfo->ucSpeed;
    }

    if ( true == bSetPreset )
    {
        PTZ_OPT_STRUCT szPtzStruct;
        memset(&szPtzStruct, 0, sizeof(PTZ_OPT_STRUCT));
        
        szPtzStruct.cmd = PTZ_OPT_SETPRESET;
        szPtzStruct.arg1 = pszPresetInfo->ucPresetId;
        Start(iChannel,&szPtzStruct);
    }

    //return PTZ_SavePresetInfoFile();
    return true;
}

//!查询某通道的某个预制点信息,通道号从0开始
NET_PRESET_INFOR &CPtz::PTZ_QueryPreset(int iChannel,int iPresetId)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        return m_szPresetInfor[iChannel][0];
    }

    for(int iPresetIndex = 0; iPresetIndex < PTZ_PRESETNUM; iPresetIndex++)
    {
        if ( m_szPresetInfor[iChannel][iPresetIndex].ucPresetId == (unsigned char)iPresetId )
        {
            return m_szPresetInfor[iChannel][iPresetIndex];
        }
    }

    return m_szPresetInfor[iChannel][0];
}

//巡航操作接口,操作成功返回０，失败返回－１
//使用说明:CMD为相应的命令
//         PTZ_OPT_ADDTOUR：增加预置点到巡航组中
//         PTZ_OPT_DELTOUR：删除巡航组中的预置点
//         PTZ_OPT_STARTTOUR:开始巡航
//         PTZ_OPT_CLEARTOUR：清除这个巡航组。
//arg1:巡航组号码0－7
//arg2:预置点号码1－80
//arg3:巡航间隔
//reserved:巡航速度
int CPtz::PTZ_OnPtzTour(PTZ_OPT_STRUCT szPtzOpt,int iChannel)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_OnPtzTour ChannelNumber is invalid!\n");
        return -1; 
    }
    
    NET_PRESET_INFOR szPresetInfo;
    PTZ_OPT_STRUCT szPtzStruct;
    
    memset(&szPtzStruct, 0, sizeof(PTZ_OPT_STRUCT));
    memset(&szPresetInfo, 0, sizeof(NET_PRESET_INFOR));

    //添加预置点到巡航组中，
    if ( szPtzOpt.cmd == PTZ_OPT_ADDTOUR )
    {
        trace("PTZ_OPT_ADDTOUR Tour Group:<%d>,Preset no:<%d>.\n", szPtzOpt.arg1, szPtzOpt.arg2);

        // 变量合法性判断
        if ( (szPtzOpt.arg1 < 0)
          || (szPtzOpt.arg1 >= PTZ_CHANNELS)
          || (szPtzOpt.arg2 <= 0)
          || (szPtzOpt.arg2 > PTZ_PRESETNUM) )
        {
            trace("PTZ_OPT_ADDTOUR Arg is invalid.\n");
            return -1;
        }
        
        if ( m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt > PTZ_PRESET_IN_TOUR_NUM ) 
        {
            trace("PTZ_OPT_ADDTOUR PresetCnt<%d> is more than <%d>.\n",m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt,PTZ_PRESETNUM);
            return -1;
        }
        
        szPresetInfo.ucPresetId = szPtzOpt.arg2;
        szPresetInfo.ucDWellTime = szPtzOpt.arg3;
        szPresetInfo.ucSpeed = szPtzOpt.reserved;
        if ( false == PTZ_ModifyPreset(iChannel,&szPresetInfo) )
        {
            trace("PTZ_OPT_ADDTOUR PTZ_ModifyPreset Preset<%d> failed.\n",szPtzOpt.arg2);
            return -1;
        }
        else
        {
            //将点间巡航数据放入，在数组中按照顺序存放预置点
             m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum[m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt] = szPtzOpt.arg2;
            m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt ++;
            m_szTourInfor[iChannel][szPtzOpt.arg1].ucDwellTime = szPtzOpt.arg3;  

            m_szTourInfor[iChannel][szPtzOpt.arg1].ucTourIndex = szPtzOpt.arg1 + 1;
        }

        szPtzStruct.cmd = PTZ_OPT_ADDTOUR;
        szPtzStruct.arg1 = szPtzOpt.arg1;
        szPtzStruct.arg2 = szPtzOpt.arg2;
        Start(iChannel,&szPtzStruct);

        return 0;
    }
    else if ( szPtzOpt.cmd == PTZ_OPT_DELTOUR )
    {
        trace("PTZ_OPT_DELTOUR Tour Group:<%d>,Preset no:<%d>.\n", szPtzOpt.arg1, szPtzOpt.arg2);
        
        // 变量合法性判断
        if ( (szPtzOpt.arg1 < 0)
          || (szPtzOpt.arg1 >= PTZ_CHANNELS)
          || (szPtzOpt.arg2 <= 0)
          || (szPtzOpt.arg2 > PTZ_PRESETNUM) )
        {
            trace("PTZ_OPT_DELTOUR Arg is invalid.\n");
            return -1;
        }

        szPtzStruct.cmd = PTZ_OPT_DELTOUR;
        szPtzStruct.arg1 = szPtzOpt.arg1;
        szPtzStruct.arg2 = szPtzOpt.arg2;
        Start(iChannel,&szPtzStruct);

        for (int iIndex = 0 ; iIndex < PTZ_PRESET_IN_TOUR_NUM ; iIndex++)
        {
            if (m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum[iIndex] == (unsigned int)szPtzOpt.arg2)
            {
                //将该预置点号码置零，则表示该预置点不存在
                int i = 0;
                unsigned char PresetNum[PTZ_PRESET_IN_TOUR_NUM];
                memset(PresetNum, 0, PTZ_PRESET_IN_TOUR_NUM);
                memcpy(PresetNum, m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum, PTZ_PRESET_IN_TOUR_NUM);
                for(i = 0; i < iIndex; i++)
                {
                    PresetNum[i] = m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum[i];
                }
                for(i = iIndex; i <  m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt - 1; i++)
                {
                    PresetNum[i] = m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum[i + 1];
                }
                for(i =  m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt - 1; i < PTZ_PRESET_IN_TOUR_NUM; i++)
                {
                    PresetNum[i] = 0;
                }
                memset(m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum, 0, PTZ_PRESET_IN_TOUR_NUM);
                memcpy(m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetNum, PresetNum, PTZ_PRESET_IN_TOUR_NUM);
                m_szTourInfor[iChannel][szPtzOpt.arg1].ucPresetCnt--;                       
                break;
            }
        }

        return 0;
    }
    else if(szPtzOpt.cmd == PTZ_OPT_STOPTOUR)
    {
        if ( m_pTimerTour[iChannel]->IsStarted() )
        {
        	for(int iIndex =0;iIndex <ALARM_PTZLINK_NUMS ;iIndex++)
  	   	{
  	   		m_ptzLinkCmd[iIndex][iChannel].leftTime = 0;	
  	   	}

            trace("PTZ_OPT_STOPTOUR Tour Group:<%d>.\n", szPtzOpt.arg1);
            m_pTimerTour[iChannel]->Stop();
        }

        return 0;
    }
    else if (szPtzOpt.cmd == PTZ_OPT_STARTTOUR)
    {
        trace("PTZ_OPT_STARTTOUR Tour Group:<%d>.\n", szPtzOpt.arg1);
        
        // 变量合法性判断
        if ( (szPtzOpt.arg1 < 0) || (szPtzOpt.arg1 >= PTZ_CHANNELS) )
        {
            trace("PTZ_OPT_STARTTOUR Arg is invalid.\n");
            return -1;
        }

        m_iTourPosition[iChannel] = 0;
        m_iTourGroup = szPtzOpt.arg1;
		m_iTourGroupArray[iChannel] = szPtzOpt.arg1;//added by wyf on 20100511
		
        if (!m_pTimerTour[iChannel]->IsStarted())
        {
            m_pTimerTour[iChannel]->Start(this, (VD_TIMERPROC)&CPtz::PTZ_TourExecute, 0,m_szTourInfor[iChannel][szPtzOpt.arg1].ucDwellTime * 1000,iChannel);
        }

        return 0;
    }
    else if (szPtzOpt.cmd == PTZ_OPT_CLEARTOUR)
    {
        trace("PTZ_OPT_CLEARTOUR Tour Group:<%d>.\n", szPtzOpt.arg1);
        
        // 变量合法性判断
        if ( (szPtzOpt.arg1 < 0) || (szPtzOpt.arg1 >= PTZ_CHANNELS) )
        {
            trace("PTZ_OPT_CLEARTOUR Arg is invalid.\n");
            return -1;
        }

        Start(iChannel,&szPtzOpt);
        memset(&(m_szTourInfor[iChannel][szPtzOpt.arg1]), 0, sizeof(NET_TOUR_INFOR));
        
//#if defined(WHFH300)  //DVR,IPC都用到此处代码,删除此判断
        if ( m_pTimerTour[iChannel]->IsStarted() )
        {
		trace("PTZ_OPT_STOPTOUR Tour Group:<%d>.\n", szPtzOpt.arg1);
		for(int iIndex =0;iIndex <ALARM_PTZLINK_NUMS ;iIndex++)
		{
			m_ptzLinkCmd[iIndex][iChannel].leftTime = 0;	
		}
		
		m_pTimerTour[iChannel]->Stop();
        }
//#endif

        return 0;
    }
    else
    {
        //不处理
    }

    return -1;
}

//获取某通道巡航组数目
int CPtz::PTZ_GetTourNumber(int iChannel)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_GetTourNumber ChannelNumber is invalid!\n");
        return -1; 
    }
    
    int iTourCnt = 0;
    for (int iTourNumber = 0; iTourNumber < PTZ_CHANNELS; iTourNumber++)
    {
        if ( m_szTourInfor[iChannel][iTourNumber].ucPresetCnt > 0 )
        {
            iTourCnt++;
        }
    }
    
    return iTourCnt;
}

//获取某通道某巡航组预置点数目,通道号从0开始,巡航组号从0开始
int CPtz::PTZ_GetTourPresetNumbers(int iChannel,int iTourNumber)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_GetTourPresetNumbers ChannelNumber is invalid!\n");
        return -1; 
    }
    
    if ( (iTourNumber < 0) || (iTourNumber >= PTZ_CHANNELS) )
    {
        trace("PTZ_GetTourPresetNumbers TourNumber is invalid!\n");
        return -1;
    }

    return m_szTourInfor[iChannel][iTourNumber].ucPresetCnt;
}

//根据通道信息和巡航组的序号获取该巡航组中的预置点序列,巡航组号从0开始,-1代表返回所有,默认值为-1
NET_TOUR_INFOR *CPtz::PTZ_GetTourInfo(int iChannel, int iTourNumber)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_SetTourDwellTime ChannelNumber is invalid!\n");
        return m_szTourInfor[iChannel]; 
    }

    if ( (iTourNumber < 0) || (iTourNumber >= PTZ_CHANNELS) )
    {
        return m_szTourInfor[iChannel];
    }

    return &m_szTourInfor[iChannel][iTourNumber];
}

//根据通道信息和巡航组的序号设置巡航点之间的停留时间,通道号从0开始,巡航组号从0开始,操作成功返回0,操作失败返回-1,单位为秒
int CPtz::PTZ_SetTourDwellTime(int iChannel, int iTourNumber, int iDwellTime)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_SetTourDwellTime ChannelNumber is invalid!\n");
        return -1; 
    }
    
    if ( (iTourNumber < 0) || (iTourNumber >= PTZ_CHANNELS) )
    {
        trace("PTZ_SetTourDwellTime TourNumber is invalid!\n");
        return -1;
    }

    m_szTourInfor[iChannel][iTourNumber].ucDwellTime = iDwellTime;
    return 0;
}

//根据通道信息和巡航组的序号获取巡航组的停留时间,通道号从0开始,巡航组号从0开始
int CPtz::PTZ_GetTourDwellTime(int iChannel, int iTourNumber)
{
    if ( (iChannel < 0) || (iChannel >= N_SYS_CH) )
    {
        trace("PTZ_GetTourDwellTime ChannelNumber is invalid!\n");
        return -1; 
    }
    
    if ( (iTourNumber < 0) || (iTourNumber >= PTZ_CHANNELS) )
    {
        trace("PTZ_GetTourDwellTime TourNumber is invalid!\n");
        return -1;
    }
    
    return m_szTourInfor[iChannel][iTourNumber].ucDwellTime;
}

//巡航操作计时器回调函数
void CPtz::PTZ_TourExecute(uint uiArg)
{
    int iChannel = uiArg;
    PTZ_OPT_STRUCT  szPtzOpt;
    memset(&szPtzOpt, 0, sizeof(PTZ_OPT_STRUCT)); 
	
    if (PTZ_GetTourPresetNumbers(iChannel,m_iTourGroupArray[iChannel]) < 2)//modified by wyf on 20100511
    {
        trace("PTZ_TourExecute Preset is less than 2!\n");
        if ( m_pTimerTour[iChannel]->IsStarted() )
        {
            m_pTimerTour[iChannel]->Stop();
        }    
        return;
    }

    szPtzOpt.cmd = PTZ_OPT_GOTOPRESET;

    while ( m_szTourInfor[iChannel][m_iTourGroupArray[iChannel]].ucPresetNum[m_iTourPosition[iChannel]] == 0)//modified by wyf on 20100511
    {
        if (m_iTourPosition[iChannel] + 1 == PTZ_PRESET_IN_TOUR_NUM)
        {
            m_iTourPosition[iChannel] = 0;
        }
        else
        {
            m_iTourPosition[iChannel]++;
        }
    }
    
    int iDweltime = m_szTourInfor[iChannel][m_iTourGroupArray[iChannel]].ucDwellTime;//modified by wyf on 20100511
    
	NET_PRESET_INFOR *pszPresetInfo = PTZ_GetPreset(iChannel, m_szTourInfor[iChannel][m_iTourGroupArray[iChannel]].ucPresetNum[m_iTourPosition[iChannel]] - 1);//modified by wyf on 20100511
    if (NULL != pszPresetInfo)
    {
        iDweltime = pszPresetInfo[0].ucDWellTime;
    }
    
    //预置点的值
    szPtzOpt.arg1= m_szTourInfor[iChannel][m_iTourGroupArray[iChannel]].ucPresetNum[m_iTourPosition[iChannel]];//modified by wyf on 20100511

#ifdef FUN_ALARMIN_LINKER_PTZ
    if(m_iTourPause[iChannel] == FALSE)
#endif		
    {
          Start(iChannel, &szPtzOpt);
    }

    if (m_iTourPosition[iChannel] + 1 == PTZ_PRESET_IN_TOUR_NUM)
    {
        m_iTourPosition[iChannel] = 0;
    }
    else
    {
        m_iTourPosition[iChannel]++;
    }

	if (m_pTimerTour[iChannel]->IsStarted())
	{
		m_pTimerTour[iChannel]->Stop();
	}	 
    m_pTimerTour[iChannel]->Start(this, (VD_TIMERPROC)&CPtz::PTZ_TourExecute, iDweltime * 1000, 0, iChannel);

}

//!保存修改后的预置点配置信息
bool CPtz::PTZ_SavePresetInfoFile(int iChannel)
{
    if ( (iChannel < -1) || (iChannel >= N_SYS_CH) )
    {
        return false;
    }
    CConfigPresetInfor *pConfigPresetInfor = new CConfigPresetInfor();

    if ( pConfigPresetInfor == NULL )
    {
        return false;
    }
    pConfigPresetInfor->update();

    if ( -1 == iChannel )
    {
        for (int iCh = 0; iCh < N_SYS_CH; iCh++)
        {
            SNET_PRESET_INFOR &strSPresetInfor = pConfigPresetInfor->getConfig(iCh);
            memcpy(&(strSPresetInfor.strPresetInfor[0]),&(m_szPresetInfor[iCh][0]),sizeof(NET_PRESET_INFOR)*PTZ_PRESETNUM);
        }

        if (pConfigPresetInfor->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE) & CONFIG_APPLY_VALIT_ERROR)
        {
            delete pConfigPresetInfor;
            pConfigPresetInfor = NULL;
            return false;
        }
    }
    else
    {
        SNET_PRESET_INFOR &strSPresetInfor = pConfigPresetInfor->getConfig(iChannel);
        memcpy(&(strSPresetInfor.strPresetInfor[0]),&(m_szPresetInfor[iChannel][0]),sizeof(NET_PRESET_INFOR)*PTZ_PRESETNUM);

        if (pConfigPresetInfor->commit(NULL, iChannel, CONFIG_APPLY_DELAY_SAVE) & CONFIG_APPLY_VALIT_ERROR)
        {
            delete pConfigPresetInfor;
            pConfigPresetInfor = NULL;
            return false;
        }
    }

    delete pConfigPresetInfor;
    pConfigPresetInfor = NULL;
    
    return true;
}

//!保存修改后的预置点配置信息
bool CPtz::PTZ_SaveTourInfoFile(int iChannel)
{
    if ( (iChannel < -1) || (iChannel >= N_SYS_CH) )
    {
        return false;
    }
    CConfigTourInfor *pConfigTourInfor = new CConfigTourInfor();

    if ( pConfigTourInfor == NULL )
    {
        return false;
    }
    pConfigTourInfor->update();

    if ( -1 == iChannel )
    {
        for (int iCh = 0; iCh < N_SYS_CH; iCh++)
        {
            SNET_TOUR_INFOR &strSTourInfor = pConfigTourInfor->getConfig(iCh);
            memcpy(&(strSTourInfor.strTourInfor[0]),&(m_szTourInfor[iCh][0]),sizeof(NET_TOUR_INFOR)*PTZ_CHANNELS);
        }

        if (pConfigTourInfor->commit(NULL, -1, CONFIG_APPLY_DELAY_SAVE) & CONFIG_APPLY_VALIT_ERROR)
        {            
            delete pConfigTourInfor;
            pConfigTourInfor = NULL;
            return false;
        }
    }
    else
    {   
        SNET_TOUR_INFOR &strSTourInfor = pConfigTourInfor->getConfig(iChannel);
        memcpy(&(strSTourInfor.strTourInfor[0]),&(m_szTourInfor[iChannel][0]),sizeof(NET_TOUR_INFOR)*PTZ_CHANNELS);
        
        if (pConfigTourInfor->commit(NULL, iChannel, CONFIG_APPLY_DELAY_SAVE) & CONFIG_APPLY_VALIT_ERROR)
        {
            delete pConfigTourInfor;
            pConfigTourInfor = NULL;
            return false;
        }
    }

    delete pConfigTourInfor;
    pConfigTourInfor = NULL;
    
    return true;
}

//!获取预置点配置信息
bool CPtz::PTZ_LoadPresetInfoFile(void)
{ 
    CConfigPresetInfor *pConfigPresetInfor = new CConfigPresetInfor();

    if ( pConfigPresetInfor == NULL )
    {
        return false;
    }
    pConfigPresetInfor->update();

    for (int iChannel = 0; iChannel < N_SYS_CH; iChannel++)
    {
        memcpy(&(m_szPresetInfor[iChannel][0]),&(pConfigPresetInfor->getConfig(iChannel).strPresetInfor[0]),sizeof(NET_PRESET_INFOR)*PTZ_PRESETNUM);
        for (int iPresetIndex = 0; iPresetIndex < PTZ_PRESETNUM; iPresetIndex++)
        {
            if ( (m_szPresetInfor[iChannel][iPresetIndex].ucPresetId > 0 )
              && (m_szPresetInfor[iChannel][iPresetIndex].ucPresetId <= PTZ_PRESETNUM) )
            {
                m_iPresetNum[iChannel]++;
            }
        }
    }

    delete pConfigPresetInfor;
    pConfigPresetInfor = NULL;
    
    return true;
}

//!获取巡航组配置信息
bool CPtz::PTZ_LoadTourInfoFile(void)
{
    CConfigTourInfor *pConfigTourInfor = new CConfigTourInfor();

    if ( pConfigTourInfor == NULL )
    {
        return false;
    }
    pConfigTourInfor->update();

    for (int iChannel = 0; iChannel < N_SYS_CH; iChannel++)
    {
        memcpy(&(m_szTourInfor[iChannel][0]),&(pConfigTourInfor->getConfig(iChannel).strTourInfor[0]),sizeof(NET_TOUR_INFOR)*PTZ_CHANNELS);
    }

    delete pConfigTourInfor;
    pConfigTourInfor = NULL;
    
    return true;
}
/* End:Added by ChenChengBin 10782, 2008/2/21 PN: ShangHai Bell Alcatel */ 

bool CPtz::PTZ_DefaultPreset(DefaultPreset *prest, int iChn, int flag)
{
    if (prest && iChn >= 0 && iChn < g_nCapture && 0 != prest->PresetId)
    {
        if (0 == flag)
        {
            memcpy(prest, &m_szDefaultPreset[iChn], sizeof(DefaultPreset));
        }
        else
        {
            memcpy(&m_szDefaultPreset[iChn], prest, sizeof(DefaultPreset));
        }

        return true;
    }

    return false;
}

//透明通道
void CPtz::WritePtzRawData(uchar * pData, uint length,int iChannel)
{
    CGuard   l_cGuard(m_Mutex);
    uchar     buffer[128];
    char *Name = NULL;
    PTZ_ATTR Attr;
    uint DestAddr = 0;
    uint MonAddr = 0;

    if(length > 64)//将写485的数据长度限制为64
    {
                trace("CPTZ::WritePtzRawData():PTZ data too len\n");

        return;
    }

    //先设置属性
    CONFIG_PTZ *pCfg = &m_ConfigPtz[iChannel];
    Name = pCfg->strProtocolName;
    Attr.baudrate = pCfg->dstComm.nBaudBase;
    Attr.databits = pCfg->dstComm.nDataBits;
    Attr.parity = pCfg->dstComm.iParity;
    Attr.stopbits = pCfg->dstComm.iStopBits;
    Attr.reserved = 0;
    DestAddr = pCfg->ideviceNo;
    MonAddr = pCfg->iNuberInMatrixs;

    //此处将配置信息转换位底层接口所需要的数据
    int stopBitsTable[3] = {0,COMM_ONESTOPBIT,COMM_TWOSTOPBITS};
    Attr.stopbits = stopBitsTable[Attr.stopbits];

#ifndef KBD_ON_DEV_PTZ

    /* 只需要设置模拟通道 */
    if((iChannel>= 0 && iChannel <g_nCapture)
         || 1 == CConfigLocalDigiChCfg::getLatest(iChannel).BPTZLocal)
    {
        if(!m_pDevPtz->SetAttribute(&Attr))
        {
            trace("CPTZ::WritePtzRawData():PTZ set attribute error\n");
            return;
        }
    }

#endif

    memset(buffer, 0, sizeof(buffer));
    memcpy(buffer , pData, (int)length);
    m_pDevPtz->SetDebug(TRUE);
    m_pDevPtz->Write(buffer, (int)length,iChannel);
    return;
}

