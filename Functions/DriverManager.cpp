#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "Functions/DriverManager.h"
#include "Main.h"
#include "System/AppConfig.h"   
#include "System/AppEvent.h"
#include "WFS/adapt.h"




static const uint crc32tab[] = {
 0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL,
 0x076dc419L, 0x706af48fL, 0xe963a535L, 0x9e6495a3L,
 0x0edb8832L, 0x79dcb8a4L, 0xe0d5e91eL, 0x97d2d988L,
 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L, 0x90bf1d91L,
 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
 0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L,
 0x136c9856L, 0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL,
 0x14015c4fL, 0x63066cd9L, 0xfa0f3d63L, 0x8d080df5L,
 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L, 0xa2677172L,
 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
 0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L,
 0x32d86ce3L, 0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L,
 0x26d930acL, 0x51de003aL, 0xc8d75180L, 0xbfd06116L,
 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L, 0xb8bda50fL,
 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
 0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL,
 0x76dc4190L, 0x01db7106L, 0x98d220bcL, 0xefd5102aL,
 0x71b18589L, 0x06b6b51fL, 0x9fbfe4a5L, 0xe8b8d433L,
 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL, 0xe10e9818L,
 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
 0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL,
 0x6c0695edL, 0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L,
 0x65b0d9c6L, 0x12b7e950L, 0x8bbeb8eaL, 0xfcb9887cL,
 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L, 0xfbd44c65L,
 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
 0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL,
 0x4369e96aL, 0x346ed9fcL, 0xad678846L, 0xda60b8d0L,
 0x44042d73L, 0x33031de5L, 0xaa0a4c5fL, 0xdd0d7cc9L,
 0x5005713cL, 0x270241aaL, 0xbe0b1010L, 0xc90c2086L,
 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
 0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L,
 0x59b33d17L, 0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL,
 0xedb88320L, 0x9abfb3b6L, 0x03b6e20cL, 0x74b1d29aL,
 0xead54739L, 0x9dd277afL, 0x04db2615L, 0x73dc1683L,
 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
 0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L,
 0xf00f9344L, 0x8708a3d2L, 0x1e01f268L, 0x6906c2feL,
 0xf762575dL, 0x806567cbL, 0x196c3671L, 0x6e6b06e7L,
 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL, 0x67dd4accL,
 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
 0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L,
 0xd1bb67f1L, 0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL,
 0xd80d2bdaL, 0xaf0a1b4cL, 0x36034af6L, 0x41047a60L,
 0xdf60efc3L, 0xa867df55L, 0x316e8eefL, 0x4669be79L,
 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
 0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL,
 0xc5ba3bbeL, 0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L,
 0xc2d7ffa7L, 0xb5d0cf31L, 0x2cd99e8bL, 0x5bdeae1dL,
 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL, 0x026d930aL,
 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
 0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L,
 0x92d28e9bL, 0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L,
 0x86d3d2d4L, 0xf1d4e242L, 0x68ddb3f8L, 0x1fda836eL,
 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L, 0x18b74777L,
 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
 0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L,
 0xa00ae278L, 0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L,
 0xa7672661L, 0xd06016f7L, 0x4969474dL, 0x3e6e77dbL,
 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L, 0x37d83bf0L,
 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
 0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L,
 0xbad03605L, 0xcdd70693L, 0x54de5729L, 0x23d967bfL,
 0xb3667a2eL, 0xc4614ab8L, 0x5d681b02L, 0x2a6f2b94L,
 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL, 0x2d02ef8dL 
};



char FileHDDAlarmName[VD_MAX_PATH] = CONFIG_DIR"/HDDAlarm";

#define IDE_STANDBY_TIME 10 //10 minutes

static int get_file_size(const char* file) {
    struct stat tbuf;
    stat(file, &tbuf);
    return tbuf.st_size;
}


void dhfile_hook_wakeup(uint disk_no)
{
    g_DriverManager.VD_SendMessage(XM_DHFILE_WAKEUP, (uint)disk_no);
}

void dhfile_hook_lock()
{
    g_DriverManager.m_Mutex.Enter();
}

void dhfile_hook_unlock()
{
    g_DriverManager.m_Mutex.Leave();
}

void ClearCached()
{
	char a[3] = { '1', '2', '3' };
	int fd = -1;
	if (fd < 0)
	{
		if ((fd = open("/proc/sys/vm/drop_caches", O_RDWR)) > 0)
		{
			write(fd, &a[0], 1);
			sleep(1);
			lseek(fd, 0, SEEK_SET);
			write(fd, &a[1], 1);
			sleep(1);
			lseek(fd, 0, SEEK_SET);
			sleep(1);
			write(fd, &a[2], 1);
			sleep(1);
			_printd("clear cache @@@@@@@@@@@@@@@@");
			close(fd);
			fd = -1;
		}
		else
		{
			perror("open::");
			fd = -1;
		}
	}
}


PATTERN_SINGLETON_IMPLEMENT(CDriverManager);

CDriverManager::CDriverManager() : CThread("DriverManager", TP_DHFILE, 1024), 
m_TimerHDDAlarm("TimerHDDAlarm")
{
    uint i;
    m_bHDDLowerAlarmed 	= FALSE;
    m_bStopHDDRrrorAlarm= FALSE;
    m_bStopNOHDDAlarm 	= FALSE;
    m_bSnapExist 		= FALSE;
    m_bFormatDriver 	= FALSE;
    m_bHDDNotExistFlag 	= FALSE;  //add by nike.xie 2009-07-08 是否存在硬盘的标志
	m_bDetectEn			= TRUE;
	m_dwSDErrState = 0;
	
    m_dwFSError = FS_NO_ERROR;
    for(i = 0; i < DRIVER_TYPE_NR; i++)
    {
        m_dwFull[i] = FS_NO_ERROR;
    }
    trace("CDriverManager::CDriverManager()>>>>>>>>>\n");
}

CDriverManager::~CDriverManager()
{
}

VD_BOOL CDriverManager::Start()
{
    trace("CDriverManager::Start()>>>>>>>>>\n");
    m_bSnapExist = FALSE;
	
	//放到线程调用
	//IsSnapExist();   
    //InitializeFS();

	
    //创建线程
    #if 1
    CreateThread();
	#endif
	
    return TRUE;    
}

VD_BOOL CDriverManager::Stop()
{
    //销毁线程
    DestroyThread();

    //关闭定时器
    //m_TimerHDDAlarm.Stop();
    trace("CDriverManager::Stop()>>>>>>>>>\n");
    return TRUE;
}


void CDriverManager::InitializeFS()
{
	file_sys_init();
}

//硬盘初始化的时候返回硬盘出错状态
uint CDriverManager::GetFSError()
{
    return m_dwFSError;
}

VD_BOOL CDriverManager::IsSnapExist()
{
	VD_BOOL BSnapExist = FALSE;
	if(file_sys_get_linkstat() == 0)
		BSnapExist = TRUE;
	
    if (BSnapExist != m_bSnapExist)
    {
        m_bSnapExist = BSnapExist;
    }
      
    return m_bSnapExist;
}

VD_BOOL CDriverManager::IsFormatDriver()
{    
    return m_bFormatDriver;
}
VD_BOOL CDriverManager::IsFull(uint *stat, uint driver_type /* = DRIVER_READ_WRITE */)
{
#ifdef NONE_DISK
    *stat = FS_OK;
    return TRUE;
#endif
    *stat = m_dwFull[driver_type];
    return TRUE;
}

void CDriverManager::UpdateState()
{
#if 0 
    CGuard guard(m_Mutex);
    uint i;

    for(i = DRIVER_TYPE_BASE; i < DRIVER_TYPE_NR; i++)
    {
        m_dwFull[i] = wfs_disk_full(i);
    }
#endif
}

/*
*******************************************************************************
*                        GET IDE INFO YUXY 2007-01-16
*
* Description: 得到IDE的信息
*
* Arguments  : pide_info    指向用户申请的用来接收硬盘信息的结构IDE_INFO的指针
*
*
* Returns     : TRUE----调用成功
*               FALSE---调用失败
*********************************************************************************
*/
VD_BOOL CDriverManager::GetIdeInfo(IDE_INFO64 *pide_info)
{
	int ret = 0;
    CGuard guard(m_Mutex);

    memset(pide_info, 0, sizeof(IDE_INFO64));
    ret = ide_getinfo64(pide_info);
	if(ret < 0)
	{
		printf("ide_getinfo64 Failure!!!\n");
		return FALSE;
	}
	
    return TRUE;
}

/*
*******************************************************************************
*                        GET DRIVER INFO YUXY 2007-01-16
*
* Description: 得到IDE的信息
*
* Arguments  : disk_no        硬盘号
*               part            
*               pdriver_info    指向用户申请的用来存放驱动器信息的结构DRIVER_INFO的指针
*
* Returns     : TRUE----调用成功
*               FALSE---调用失败
*********************************************************************************
*/
VD_BOOL CDriverManager::GetDriverInfo(uint disk_no,uint part,DRIVER_INFO *pdriver_info)
{
    uint ret;
    
    CGuard guard(m_Mutex);
//fyj 20161223 查看SD卡内存信息
	if(pdriver_info == NULL)
		return FALSE;

	int total,remain;
	
	memset(pdriver_info,0,sizeof(DRIVER_INFO));
	if(file_sys_get_cap(&total,&remain) < 0)
		return FALSE;
	
	pdriver_info->total_space = total/1024;
	pdriver_info->remain_space = remain/1024;


    return TRUE;
}

VD_BOOL CDriverManager::SetDriverType(uint disk_no,uint part,uint driver_type)
{
#if 0 
    uint ret;
    CGuard guard(m_Mutex);
    ret = wfs_set_driver_type(disk_no, part, driver_type);
    if(ret)
        return FALSE;
    wfs_array_driver();
    return TRUE;
#endif
}

VD_BOOL CDriverManager::GetDriverType(uint disk_no,uint part,uint* pdriver_type)
{
    uint ret;
    CGuard guard(m_Mutex);
 //fyj 20161223
	*pdriver_type = 4;

    return TRUE;
}

VD_BOOL CDriverManager::FormatDisk(uint disk_no)
{
    return TRUE;
}

VD_BOOL CDriverManager::FormatDriver(uint disk_no,uint part)
{
    return TRUE;
}

VD_BOOL CDriverManager::ParoleDriver(uint disk_no,uint part)
{
    return TRUE;
}

VD_BOOL CDriverManager::GetDriverSection(uint disk_no,uint part, uint index, DRIVER_SECTION *pdriver_section)
{
#if 0 
    uint ret;
    CGuard guard(m_Mutex);
    ret = wfs_get_driver_section(disk_no, part, index, pdriver_section);
    if(ret)
        return FALSE;
    return TRUE;
#endif
}

VD_BOOL CDriverManager::GetFSInfo(SYS_USE_INFO *info)
{
#if 0 
    uint ret;
    CGuard guard(m_Mutex);
    ret = wfs_get_fs_info(info);
    if(ret)
        return FALSE;
    return TRUE;
#endif
}

VD_BOOL CDriverManager::WakeupCurrDriver()
{
#if 0 
    CGuard guard(m_Mutex);
    wfs_wakeup_curr_driver();
    return TRUE;
#endif
}

/*
*******************************************************************************
*                        SCAN DISK YUXY 2007-01-16
*
* Description: 检测硬盘错误状态
*
* Arguments  : pError  指向用户申请的用来存放硬盘出错信息结构的指针
*
* Returns     : TRUE----表示没有发现硬盘有出错信息
*               FALSE---表示硬盘有出错信息
*********************************************************************************
*/
VD_BOOL CDriverManager::ScanDisk(DISK_ERROR_INFO64 *pError)
{
#if 0 
    CGuard guard(m_Mutex);
    
    if(wfs_scan_disk64(pError))
    {
        return FALSE;
    }
    
    return TRUE;
#endif
}
int CDriverManager::WriteSDFile(uint8_t *buf, uint len)
{
	FILE *fp = NULL;
	int write_length = 0;
	char sdfilename[128] = {0};
	snprintf(sdfilename,128,"%s/testfile",TARGET_DIR);
	fp = fopen(sdfilename, "wb+");
	if(fp == NULL)
	{
		return -1;
	}
	write_length = fwrite(buf, 1, len, fp);

	printf("write_length = %d\n", write_length);
	fclose(fp);
	fp = NULL;
	if(write_length != len)
	{
		return -1;
	}
	return 0;
}
int CDriverManager::ReadFlashFile(char* pFilename,CPacket *&pPackt, int *len)
{
	int ret = 0;
	FILE *fd;
	CPacket* pPacket = NULL;
	char dirname[128];
	uchar 	*pBuffer = NULL;
	int size = 0;

	if(access(pFilename,F_OK) !=0)
	{
		printf("read flash name [%s]NO  exist \n",pFilename);
		return -1;
	}
	size = get_file_size(pFilename);
	pPacket 	= g_PacketManager.GetPacket(size + 1024);
	if(NULL == pPacket)
	{
		return -1;
	}

	pBuffer 	= pPacket->GetBuffer();

	if(( fd = fopen(pFilename,"r")) == NULL)
	{
		fprintf(stderr,"Can not open [%s] file .\n",pFilename);
        return -1;
		
	}
	else
	{
		ret = fread(pBuffer,1,size,fd);
		if(ret == size)
		{
			*len = ret;
		}
		else
		{
			*len = 0;
		}
		pPackt = pPacket;
	}
	if(fd != NULL)
		fclose(fd);	
	remove(pFilename); 

	return ret; 
}

int CDriverManager::DetectSDStatus()
{
	uint bufcrcvalue = 0;  //flash crc
	uint sdcrcvalue = 0;  //sd crc
	uchar testdata[1024] = {0};
	char sdfilename[128] = {0};
	
	int ret = 0;
	int length;
	CPacket* 	pPacket = NULL;
	//READ FLASH
	for(int i = 0; i < 1024; i++)
	{
		testdata[i] = rand() % 100;
		//_printd("testdata[%d]",testdata[i]);
	}
	bufcrcvalue = SDBufCrc32(testdata,1024);
		
	if(0 == file_sys_init())
	{
		//write SD
		ret = WriteSDFile(testdata, 1024);
		if(ret == -1)
		{
			//SD write error
			m_dwSDErrState = FS_WRITE_ERROR;
			return -1;
		}
		//read SD
		snprintf(sdfilename,128,"%s/testfile",TARGET_DIR);
		ret = ReadFlashFile(sdfilename,pPacket,&length);
		if(NULL == pPacket || ret < 0 || length < 512){
			if(NULL != pPacket){
				pPacket->Release();
			}
			return -1;
		}
		
		sdcrcvalue = SDBufCrc32(pPacket->GetBuffer(),length);
		
		_printd("bufcrcvalue[%u] sdcrcvalue[%u]",bufcrcvalue,sdcrcvalue);
		if(sdcrcvalue != bufcrcvalue){
			m_dwSDErrState = FS_WRITE_ERROR;
		}
		else{
			_printd("SD STATUS[%u]",m_dwSDErrState);
			m_dwSDErrState = FS_OK;
		}
		
	}
	
	if(NULL != pPacket){
		pPacket->Release();
	}
	
	return 0;
}


uint CDriverManager::SDBufCrc32( const unsigned char *buf, uint size)
{
     uint i, crc;
     crc = 0xFFFFFFFF;
     for (i = 0; i < size; i++)
      crc = crc32tab[(crc ^ buf[i]) & 0xff] ^ (crc >> 8);
     return crc^0xFFFFFFFF;
}

void CDriverManager::ThreadProc()
{
    IsSnapExist();
    //初始化文件系统
    InitializeFS();

//检测SD卡是否读写出错
    do 
    {
		ClearCached();
    	sleep(1);
  //  	if(VD_FALSE != m_bDetectEn){
		//	//DetectSDStatus();
		//	//暂时不用这个检查SD 问题  modify by jwd on 20181029
		//	//低于300M  删除文件
		//	m_dwSDErrState = FS_OK;
		//	file_sys_del();
		////	_printd("SD Staus[%u]\n",m_dwSDErrState);
		////	if(FS_WRITE_ERROR == m_dwSDErrState){
		////		_printd("file_sys_deinit!!!!\n");
		////		file_sys_deinit();
		////	}
  //  	}

    } while (m_bLoop);
#if 0
    VD_MSG msg;
    do 
    {
        VD_RecvMessage(&msg);
        Dispatch(msg);
    } while (m_bLoop);
#endif
}

VD_BOOL CDriverManager::Dispatch(VD_MSG &msg)
{
#if 0 
    switch (msg.msg) 
    {    
        case XM_DHFILE_WAKEUP: 
            wfs_wakeup_driver(msg.wpa);
            break;
    
        default:
            break;
    }
    return TRUE;
#endif
}

/******************************************************************/
/*                    以下都是和硬盘报警相关                          */
/******************************************************************/

void CDriverManager::StartHddAlm()
{
    m_configStorageNotExist.update();
    m_configStorageFailure.update();
    m_configStorageLowSpace.update();

    //初始化状态码
    m_dwHddErrState = 0;
    m_dwHddSpaceState = 0;

    m_AlarmState = FALSE;
    m_configStorageNotExist.attach(this, (TCONFIG_PROC)&CDriverManager::onConfigStorageNotExist);
    m_configStorageFailure.attach(this, (TCONFIG_PROC)&CDriverManager::onConfigStorageFailure);
    m_configStorageLowSpace.attach(this, (TCONFIG_PROC)&CDriverManager::onConfigStorageLowSpace);

    //启动定时器
    m_TimerHDDAlarm.Start(this, (VD_TIMERPROC)&CDriverManager::OnHddAlmTimer, 0, 2000);
}

void CDriverManager::onConfigStorageNotExist(CConfigStorageNotExist* pConfig, int& ret)
{
    m_configStorageNotExist.update();
}

void CDriverManager::onConfigStorageFailure(CConfigStorageFailure* pConfig, int& ret)
{
    m_configStorageFailure.update();
}

void CDriverManager::onConfigStorageLowSpace(CConfigStorageLowSpace* pConfig, int& ret)
{
    CGuard guard(m_Mutex);

    //如果已经在报警， 但是配置改变，需要清空状态以处理下一次报警, 并关闭老的报警
    m_configStorageLowSpace.update();
    if(m_bHDDLowerAlarmed)
    {
        m_bHDDLowerAlarmed = FALSE;
        CAppEventManager::instance()->notify(
            appEventStorageFailure,
            0,
            appEventStop,
            &m_configStorageLowSpace.getConfig().handler);//通知硬盘错误
    }
}

void CDriverManager::NotifyHDDError(appEventCode appCode)
{
    CONFIG_GENERIC_EVENT& eventHDDError = m_configStorageFailure.getConfig();

    //打开报警
    if(!m_bStopHDDRrrorAlarm && eventHDDError.enable)
    {
        CAppEventManager::instance()->notify(
            appCode,
            0,
            appEventStart,
            &eventHDDError.handler);//通知硬盘错误
            m_bStopHDDRrrorAlarm = TRUE;
    }
}

void CDriverManager::NotifyHDDSpace(int iPercent)
{
    CONFIG_STORAGE_SPACE_EVENT& eventHDDSpace = m_configStorageLowSpace.getConfig();

    //打开报警, 硬盘容量不足时设置m_bHDDLowerAlarmed, 保证只报一次警. 如果用户手动复位或报警延时完毕,
    //也不会再继续报警，只有配置改变后才可能再次报警.
    if (!m_bHDDLowerAlarmed && eventHDDSpace.enable)
    {
        m_bHDDLowerAlarmed = TRUE;
        
        CAppEventManager::instance()->notify(
            appEventStorageLowSpace,
            0,
            appEventStart,
            &eventHDDSpace.handler);
        //纪录日志        
        g_Log.Append(LOG_HDD_NOSPACE, (uchar)iPercent, 0, 4);
    }        
}

void CDriverManager::NotifyHDDNone()
{
    CONFIG_GENERIC_EVENT& eventHDNone = m_configStorageNotExist.getConfig();

    //打开报警, 硬盘容量不足时设置m_bHDDLowerAlarmed, 保证只报一次警. 如果用户手动复位或报警延时完毕,
    //也不会再继续报警，只有配置改变后才可能再次报警.
    if (!m_bStopNOHDDAlarm && eventHDNone.enable)
    {
        m_bStopNOHDDAlarm = TRUE;
         
         m_bHDDNotExistFlag = TRUE; //add by nike.xie 2009-07-08  标记硬盘不存在
         
        CAppEventManager::instance()->notify(
            appEventStorageNotExist,
            0,
            appEventStart,
            &eventHDNone.handler);
    }    
}

//检查硬盘状态,challenge初始化的时候调用
void CDriverManager::NotifyHDDState()
{
    uint errFS = 0;    

    errFS = GetFSError();
    trace("**************************errFS = %d******************************\n", errFS);
    
    if(errFS == FS_ERROR)
    {
        NotifyHDDNone(); //通知没有硬盘
        g_Log.Append(LOG_HDD_NODISK);
       
        trace("CDriverManager::NotifyHDDState---------No disk!\n");
    }
    else if(errFS == FS_OVERLAY || errFS == FS_TIME_ERROR || errFS == FS_BAD_DISK)
    {
        NotifyHDDError(appEventStorageFailure);//通知硬盘错误
        g_Log.Append(LOG_HDD_ERROR);
    }
    
}

void CDriverManager::SetAlarmState(VD_BOOL bState)
{
    m_AlarmState = bState;
}


VD_BOOL CDriverManager::GetAlarmState()
{
    return m_AlarmState;
}

//add by nike.xie 2009-07-08  获取是否存在硬盘的标志
//修正设备有多个硬盘时，只要第一个盘不是可写盘就会报"无可写盘"警的BUG modified by wyf on 20100518
VD_BOOL CDriverManager::GetHDDNotExistFlag()
{
    return m_bHDDNotExistFlag ;    
}
//end modified by wyf on 20100518 


/*
****************************************************************************************************
*                            GET HDD STATE ---- YXY 2006-12-18
*
* Description: 该函数只是在其他模块需要获得硬盘报警状态的时候才使用到;
*               实际应用中很少被调用。
*
* Arguments  : type     0----硬盘出错 
*                        1----硬盘空间不足
*
*
* Returns     : type == 0时，返回硬盘出错掩码，从低位到高位分别表示哪一个硬盘.
*               type == 1时，表示硬盘空间不足
****************************************************************************************************
*/
uint CDriverManager::GetHDDErrorState(int type)
{
    uint iRet = 0;
    
    switch(type)
    {
        case TYPE_HDD_ERROR:
        {
            iRet = m_dwHddErrState;
            break;
        }

        case TYPE_HDD_NOSPACE:
        {
            iRet = m_dwHddSpaceState;
            break;
        }        
        default:
        {
            break;
        }
    }
    return iRet;
}

VD_BOOL CDriverManager::CheckHDDNotExist()
{
    int ihasRWDisk = 0;
    IDE_INFO64 ide_info;
    GetIdeInfo(&ide_info);

    int dev = 0;
    int hddNums = ide_info.ide_num;
    while(hddNums > 0)
    {        
        enum
        {
            MAX_PARTITIONS = 4,
        };

        for(int ii=0;ii<MAX_PARTITIONS;ii++)
        {
            uint uidriver_type = 0;         
            if(TRUE == GetDriverType(dev,ii,&uidriver_type))
            {
                if(DRIVER_READ_WRITE == uidriver_type) 
                {
                    ihasRWDisk = 1;
                    break;
                }
            }
            
        }
        
        hddNums--;
        dev++;
    }

    m_bHDDNotExistFlag = !ihasRWDisk;

    if(m_bHDDNotExistFlag)
    {
        NotifyHDDNone();
    }
    return 0;
}

void CDriverManager::OnHddAlmTimer(uint param)
{

    SYS_USE_INFO fsinfo;    
    int percent = 0;
    int iwork_drive = 0;
    static int count = 0;
    static uint dwOldReadState = 0;
    static uint dwOldWriteState = 0;
    DISK_ERROR_INFO64 disk_error;
    int i;

    // 硬盘休眠时间为10min, 在此5min唤醒一次
    if(count % 300 == 0)
    {
        WakeupCurrDriver();
    }

    // 每60秒更新硬盘状态, 并处理硬盘错误
    if(count % 30 == 0)
    {
        if(!ScanDisk(&disk_error)) //检测到了硬盘错误
        {        
            m_dwHddErrState = disk_error.removemask;
        
            if(disk_error.removemask)
            {
                NotifyHDDError(appEventStorageFailure);        
            }
            for(i = 0; i < 32; i++) //记录日志
            {
                if(disk_error.readmask & BITMSK(i))
                {
                    g_Log.Append(LOG_HDD_RERR, i+1, &disk_error.offset[i], 4);
                    
                    if ((!disk_error.removemask) && (dwOldReadState != disk_error.readmask))
                    {
                        NotifyHDDError(appEventStorageReadErr);                        
                        dwOldReadState = disk_error.readmask;
                    }
                }
                else if(disk_error.writemask & BITMSK(i))
                {
                    g_Log.Append(LOG_HDD_WERR, i+1, &disk_error.offset[i], 4);
                    if ((!disk_error.removemask) && (dwOldWriteState != disk_error.writemask))
                    {
                        NotifyHDDError(appEventStorageWriteErr);
                        dwOldWriteState = disk_error.writemask;
                    }
                }
            }
        }

        UpdateState();            

        //通知硬盘剩余容量百分比
        GetFSInfo(&fsinfo);
        if(fsinfo.drive_space)
        {
            iwork_drive = fsinfo.work_drive;
            m_dwHddSpaceState |= BITMSK(iwork_drive);
            percent = fsinfo.drive_free_space * 100 / fsinfo.drive_space;            
            
            if(percent <= m_configStorageLowSpace.getConfig().lowerLimit)
            {
                NotifyHDDSpace(percent);                //硬盘空间不足报警
            }
        }
        
        // 检测无硬盘告警
        CheckHDDNotExist();        
        
    }
    count++;
    
    if(m_bHDDLowerAlarmed)
    {
        m_bHDDLowerAlarmed = FALSE;
        
        CAppEventManager::instance()->notify(
            appEventStorageLowSpace,
            0,
            appEventStop,
            &m_configStorageLowSpace.getConfig().handler);//通知硬盘错误
    }
    if (m_bStopHDDRrrorAlarm)
    {
        m_bStopHDDRrrorAlarm = FALSE;
        
        CAppEventManager::instance()->notify(
            appEventStorageFailure,
            0,
            appEventStop,
            &m_configStorageFailure.getConfig().handler);//通知硬盘错误
    }
    
    if (m_bStopNOHDDAlarm)
    {
        m_bStopNOHDDAlarm = FALSE;
        
        CAppEventManager::instance()->notify(
            appEventStorageNotExist,
            0,
            appEventStop,
            &m_configStorageNotExist.getConfig().handler);
    }

}

