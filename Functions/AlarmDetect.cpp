#include <unistd.h>
#include "APIs/Audio.h"
#include "Functions/AlarmDetect.h"
#include "APIs/CommonBSP.h" 
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HWComunication.h"
#endif // DEBUG
#ifdef MOBILE_COUNTRY
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#endif // MOBILE_COUNTRY


CMoveTracker::CMoveTracker():CThread("MoveTracker", TP_TRACE)
	,bTimePlanEnable(1)
{
	
#if	defined(IPC_JZ_T10T20)|| defined(IPC_JZ_T21) || defined(TUPU) || defined(IPC_JZ)
#else //T20X T30
	bTraceEnable = VD_FALSE;
	CreateThread();
#endif
}
CMoveTracker::~CMoveTracker()
{
#if	defined(IPC_JZ_T10T20)|| defined(IPC_JZ_T21) || defined(TUPU) || defined(IPC_JZ)
#else //T20X T30
	DestroyThread();
#endif
}

void CMoveTracker::SwtichTracker(VD_BOOL val)
{
#ifdef MOTOR
	if(VD_FALSE == val){
		_printd("close traceker");
		TrackerRunSet(0);
	}
	else{
		//自动航巡 与 移动追踪 功能不能同时使能，故关闭自动航巡
		g_PtzTrace.PtzTraceClose();
		
		_printd("open traceker");
		TrackerRunSet(1);
	//	TrackerGobackTimeSet(m_);

		
	}
#endif	
}
VD_BOOL CMoveTracker::IsMatchCondition()
{
#ifdef IPC_JZ
	return VD_TRUE;
#endif
////	if( 0 != RealTimeConnectToServerStatus())
//	{
//		//_printd("NOT LINK");
//		return VD_FALSE;
//	}
	if( VD_FALSE == bTraceEnable){
		//_printd("UNABLE");
		return VD_FALSE;
	}	

	return VD_TRUE;
}

void CMoveTracker::ThreadProc()
{
#ifdef IPC_JZ
	return ;
#endif
	int iRet = 0;
	int openStatus = -1;
	SET_THREAD_NAME("CMoveTracker");
	while(m_bLoop){

		//待机状态关闭移动追踪功能
		if(0 != g_General.GetSuspendStatus()){
			iRet = VD_FALSE;
		}
		else{
			iRet = IsMatchCondition();
		}
		
		/*状态有变化 ，则切换*/
		if(openStatus != iRet){
			openStatus = iRet; 
			SwtichTracker(openStatus);
		}

		sleep(1);
	}
}


CVoiceDetecter::CVoiceDetecter():CThread("CVoiceDetecter", TP_TRACE)
//,bTimePlanEnable(1)
{
#if	defined(IPC_JZ_T10T20)|| defined(IPC_JZ_T21) || defined(TUPU) || defined(IPC_JZ)
#else //T20X T30
	bCryEnable		= VD_FALSE;
	bVoiceEnable	= VD_FALSE;
	
	alarmTime_cry	= 0;
	alarmTime_voice = 0;

	CreateThread();
#endif
}
CVoiceDetecter::~CVoiceDetecter()
{
#if	defined(IPC_JZ_T10T20)|| defined(IPC_JZ_T21) || defined(TUPU) || defined(IPC_JZ)
#else //T20X T30
	DestroyThread();
#endif
}

/*推送哭声报警开始信号*/
int CVoiceDetecter::PushCryAlarm()
{ 
#ifdef IPC_JZ
	return 0;
#endif
#ifdef SUNING
	int ret = 0;
	uint cur_time = time(NULL);

	ret = g_MP3Play.GetMP3PlayStatus(NULL);
	if((1 == ret || 3 == ret)
		&& alarmTime_cry 
		&& (cur_time > alarmTime_cry) 
		&& (cur_time - alarmTime_cry < 300))
	{
		//mp3 1:downloading or 3:playing
		//MP3播放期间5分钟内只上传一次哭声事件
		_printd("cur_time cry:%lu",cur_time);
		return 0;
	}
	else if(alarmTime_cry 
			&& (cur_time > alarmTime_cry) 
			&& (cur_time - alarmTime_cry < 15))
	{
		//未播放MP3则15秒上报一次事件
		_printd("cur_time cry:%lu",cur_time);
		return 0;
	}
	alarmTime_cry = cur_time;
	_printd("alarmTime_cry:%lu",alarmTime_cry);
	char *imagebuf = NULL;
	int   imagesize = 0;	
	TImage *pImage = new TImage();
	if(NULL != pImage)
	{		
		jz_snap_pic((char**)(&imagebuf),(int*)&(imagesize));
		if(NULL != imagebuf)
		{
			pImage->setTag("2");
			pImage->setBinary((const void *) imagebuf, imagesize, "Image1.jpeg");
			free(imagebuf);
			imagebuf = NULL;
			g_RecordTsManager.OnEvent(pImage);
		}
		else
		{
			delete pImage;
		}
	}
#else
	static uint 		last_time 	= 0;
	RT_UpLoadAlarmInfo 	faceinfo	= {0}; 

	uint cur_time = time(NULL);

//	if( 0 != RealTimeConnectToServerStatus())
	{
		_printd("The status of server is not link");
		return 0;
	}
	
#if 0
	alarmTime_cry = cur_time;

	if(cur_time < last_time + cryUpFreq){
		_printd("last_time:%ld,cur_time:%ld",last_time,cur_time);
		return 0;
	}

	last_time = cur_time;
#else
	if( 0 < alarmTime_cry){
		alarmTime_cry = cur_time;
		_printd("alarmTime_cry:%ld",alarmTime_cry);
		return 0;
	}

	alarmTime_cry = cur_time;
#endif

	faceinfo.AlarmType = RT_CRY;
	faceinfo.TimeStamp = cur_time;

	int ret = 0;
	ret = jz_snap_pic((char**)(&(faceinfo.ImageInfo.buf)),(int*)&(faceinfo.ImageInfo.size));

	if( 0 != ret){
		_printd("jz_snap_pic...ret:%d,size:%d",ret,faceinfo.ImageInfo.size);
	}
	
	if( 0 != RealTimeUploadAlarmInfo(faceinfo)){
		alarmTime_cry = 0;
	}

	if( NULL != faceinfo.ImageInfo.buf ){
		free(faceinfo.ImageInfo.buf);
		faceinfo.ImageInfo.buf = NULL;	
	}
#endif	
	return 0;
	
}
/*推送声音报警开始信号*/
int CVoiceDetecter::PushVoiceAlarm()
{ 

	uint cur_time = time(NULL);
#if 0
	if( alarmTime_voice > 0)
	{
		//告警状态
		alarmTime_voice = cur_time;
		_printd("alarmTime_voice:%ld",alarmTime_voice);
		return 0;
	}
	alarmTime_voice = cur_time;
#endif
#ifdef HEMU_CLOUD
	g_HWComm.PushEvent(2,1,cur_time);
#endif 	
#ifdef	MOBILE_COUNTRY
	_printd("DETECT SOUND START");
//	g_MobileCountrySdkApi.SendEvent((uint64)cur_time*(uint64)1000,CC_DETECT_SOUND,CC_DETECT_START,NULL);
#endif
	return 0;	
}

/* 定时检测 时间持续时间，若连续15秒没触发 则发送结束事件*/
void CVoiceDetecter::PushAlarmEvent()
{

	uint cur_time = time(NULL);

//	if( 0 != alarmTime_voice && cur_time >= alarmTime_voice + VoiceAlarmStopTime)
//	{
#ifdef HEMU_CLOUD
		g_HWComm.PushEvent(2,0,cur_time);
#endif // HEMU_CLOUD	
#ifdef	MOBILE_COUNTRY
		_printd("SOUND DETECT STOP");
//		g_MobileCountrySdkApi.SendEvent((uint64)cur_time * (uint64)1000, CC_DETECT_SOUND, CC_DETECT_STOP, NULL);
#endif
//		alarmTime_voice = 0;
//	}

}
void CVoiceDetecter::ThreadProc()
{
#ifdef IPC_JZ
	return ;
#endif

	int voiceDetect = 0;
	int nDetectVoiceStaus = EVENT_STOP;
	int nStartCount = 0;
	int nStopCount = 0;
	int voiceGapCnt = VoiceGapTime;
	SET_THREAD_NAME("CVoiceDetecter");
	while(m_bLoop){
#ifdef	MOBILE_COUNTRY
		bVoiceEnable	= VD_TRUE;	
#endif		
		sleep(1);
		voiceDetect = AudioDetectResult(NULL);
		if (0 != g_General.GetSuspendStatus() || (!bVoiceEnable))
		{
			//待机状态或非开启状态
			nStartCount = 0;
			nStopCount = 0;
			if (nDetectVoiceStaus == EVENT_START)
			{
				nDetectVoiceStaus = EVENT_STOP;
				PushAlarmEvent();
			}
			continue;
		}
		if (voiceDetect)
		{//声音检测成功
			nStartCount++;
			nStopCount = 0;
		}
		else
		{
			nStopCount++;
			nStartCount = 0;
		}

		if (nDetectVoiceStaus == EVENT_STOP && nStartCount > 1)
		{//连续两次检测成功则推开始事件
			nDetectVoiceStaus = EVENT_START;
			PushVoiceAlarm();
		}
		else if (nDetectVoiceStaus == EVENT_START && nStopCount > 30)
		{//连续30秒无监测则推结束事件
			nDetectVoiceStaus = EVENT_STOP;
			PushAlarmEvent();
		}
#if  0
//		_printd("voiceDectect[%d]",voiceDetect & AUDIO_DETECT_PUB);
		//哭声检验
		if(bCryEnable){
			if(voiceDetect & AUDIO_DETECT_CRY ){
				//_printd("AudioDetectResult : Crying");
				PushCryAlarm();
			}
		}

//每次声音报警的最短间隔 为 VoiceGapTime 秒

		if( voiceGapCnt++ >= VoiceGapTime)
		{
			//声音检测
			//_printd("bVoiceEnable[%d]",bVoiceEnable);
			if(bVoiceEnable && (voiceDetect & AUDIO_DETECT_PUB))
			{			  
				//如果已处于报警状态,则直接更新最新报警时间
				if(alarmTime_voice)
				{
					voiceSigNum = 2;
				}
				else
			    {
					voiceSigNum ++;
				}
			}
			else
			{
				voiceSigNum = 0;
			}

			if(voiceSigNum >= 2)
			{
				//连续两秒检测到声音，则开始报警
				PushVoiceAlarm();
				voiceGapCnt = 0;
			}
		}
		else
		{
//			_printd("The Detect Event of voice will be started after %d s",VoiceGapTime - voiceGapCnt);
		}
#endif
	}
}

CMotionDetecter::CMotionDetecter():CThread("CMotionDetecter", TP_TRACE)
,bTimePlanEnable(1)
{
#ifndef IPC_JZ
	bMotionEnable	= VD_FALSE;
	bHumanEnable	= VD_FALSE;

	m_dwStopCount		= 0;
	m_dwStartCount		= 0;
	m_MotionDetectStatus= EVENT_STOP;		
	
	CreateThread();
#endif
	#ifdef SUNING
	m_iMotionFrameTimerCount = 0;
	#endif
}
CMotionDetecter::~CMotionDetecter()
{
#ifndef IPC_JZ
	DestroyThread();
#endif
}

/*推送移动报警开始信号*/
int CMotionDetecter::PushMotionDetect(int status)
{

	uint cur_time = time(NULL);

	if (VD_FALSE == bMotionEnable){
		_printd("MotionEnable is unable");
		return -1;
	}
#ifdef HEMU_CLOUD	
	g_HWComm.PushEvent(1,status,cur_time);
#endif
#ifdef	MOBILE_COUNTRY
	if(status)
	{
//		g_MobileCountrySdkApi.SendEvent((uint64)cur_time * (uint64)1000, CC_DETECT_MOTION, CC_DETECT_START, NULL);
	}
	else
	{
//		g_MobileCountrySdkApi.SendEvent((uint64)cur_time * (uint64)1000, CC_DETECT_MOTION, CC_DETECT_STOP, NULL);
	}
#endif
	return 0;
}

/*获取侦测状态*/
uint CMotionDetecter::GetMotionDetectState()
{
	uint udwState 	 = 0;
	uint peopleState = 1;


	if(0 != g_General.GetSuspendStatus()|| 0 == bTimePlanEnable){ //处于待机状态，返回不动
		return 0;
	}
	
	if(bMotionEnable){
		MotionDetectGetState(&udwState);
	}
	else{
		udwState = 0;
	}
	
	if(bHumanEnable){
		#ifdef IPC_JZ
		peopleState = 1;
		#else
		//new adk 20190301
		PersonDetectGetState(&peopleState);
		#endif
	}
	else
	{
		peopleState = 1;
	}
	
	return udwState/*||peopleState*/;
}

void CMotionDetecter::onDetectTimer()
{	
		//static int AlarmTimer	= AlarmGapTime; //计时
		//static int TimeCount	= 0;
		//RTPushAlarm alarm;
	int uDwState = EVENT_STOP;//0:detect fail 1:detect success
		
	uDwState = GetMotionDetectState();

	if (uDwState == EVENT_DECECT_SUCCESS)
	{
		m_dwStartCount++;
		m_dwStopCount = 0;
	}
	else
	{	
		m_dwStopCount++;
		m_dwStartCount = 0;
	}
	//_printd("m_dwStartCount[%d] m_dwStopCount[%d]", m_dwStartCount,m_dwStopCount);
	if (m_MotionDetectStatus == EVENT_STOP && m_dwStartCount > 1)
	{//连续两次检测成功则推开始事件
		m_MotionDetectStatus = EVENT_START;
		PushMotionDetect(1);
		_printd("Motion Event Start\n");
	}
	else if (m_MotionDetectStatus == EVENT_START && m_dwStopCount > 30) 
	{//连续30秒无监测则推结束事件
		m_MotionDetectStatus = EVENT_STOP;
		PushMotionDetect(0);
		_printd("Motion Event End\n");
	}

#if 0
//		_printd("uDwState[%d]",uDwState);
	//移动侦测报警(有时间间隔)
		if(uDwState)	m_dwStopCount = 0;
		else			m_dwStartCount= 0;
	
		AlarmTimer ++;	
		
		if(1 == m_MotionDetectStatus){//start
			TimeCount++;
			_printd("RealTimeStreamCmd_AlarmStatus TimeCount=%d\n",TimeCount);
		}else{
			TimeCount = 0;
		}

		if(m_MotionDetectStatus !=	uDwState || TimeCount > 5*60)
		{
			if( 0 == uDwState || TimeCount > 5*60)
			{
				m_dwStopCount++;
//				_printd("m_dwStopCount[%d]",m_dwStopCount);
				if(m_dwStopCount > AlarmStopTime || TimeCount > 5*60)
				{		
					uDwState = 0;  //qiangzhi
					TimeCount = 0;
					m_dwStopCount = 0;
					m_dwStartCount= 0;
					_printd("RealTimeStreamCmd_AlarmStatus end!!!");					
					m_MotionDetectStatus = uDwState;
					PushMotionDetect(0);
				}
			}
			else if(1 == uDwState && AlarmTimer >= AlarmGapTime) //移动侦测成功
			{
				m_dwStartCount ++;
				if(m_dwStartCount > 1)
				{
					_printd("RealTimeStreamCmd_AlarmStatus start!!!");			
					PushMotionDetect(1);
					AlarmTimer = 0;
					m_MotionDetectStatus = uDwState;					
				}
			}
		}
#endif
}

void CMotionDetecter::ThreadProc()
{
	SET_THREAD_NAME("CMotionDetecter");
	while(m_bLoop){
		#ifdef	MOBILE_COUNTRY
		bMotionEnable	= VD_TRUE;	
		#endif	
		onDetectTimer();
		sleep(1);
	}
}

PATTERN_SINGLETON_IMPLEMENT(CAlarmDetecterManager);

CAlarmDetecterManager::CAlarmDetecterManager():CThread("CAlarmDetecterManager", TP_TRACE)
{
	m_pMoveTracker		= NULL;
	m_pVoiceDetecter	= NULL;
	m_pMotionDetecter	= NULL;

#if	defined(IPC_JZ_T10T20)|| defined(IPC_JZ_T21) || defined(TUPU) || defined(IPC_JZ)

#else
#ifdef MOTOR	
	m_pMoveTracker 		= new CMoveTracker();
#endif
	m_pVoiceDetecter 	= new CVoiceDetecter();
#endif

#if defined(IPC_JZ) || defined(TUPU)
#else
	m_pMotionDetecter	= new CMotionDetecter();
#endif
}
CAlarmDetecterManager::~CAlarmDetecterManager()
{
}
void CAlarmDetecterManager::onConfigAlarmSwitch(CConfigAlarmSwitch& cCfg, int& ret)
{
#ifdef IPC_JZ
	return ;
#endif
	CONFIG_ALARMSWITCH &configOld = m_AlarmSwitch.getConfig();
	CONFIG_ALARMSWITCH &configNew = cCfg.getConfig();

	configOld = configNew;

	if( NULL != m_pMoveTracker){
		m_pMoveTracker->SetTrackerSwitch(configNew.bTraceEnable);
		m_pMoveTracker->SetTimePlan(configNew.bTimeSectionEnable);
		if (configNew.iTraceTimeOut >= 5)
		{
			TrackerGobackTimeSet(configNew.iTraceTimeOut);
		}
	}
	if( NULL != m_pVoiceDetecter){
		m_pVoiceDetecter->SetCryAlarmSwitch(configNew.bCryEnable);
		m_pVoiceDetecter->SetVoiceAlarmSwitch(configNew.bVoiceEnable);
//		m_pVoiceDetecter->SetTimePlan(configNew.bTimeSectionEnable);
	}
	if( NULL != m_pMotionDetecter){
		m_pMotionDetecter->SetHumanDetectSwitch(configNew.bHumanEnable);
		m_pMotionDetecter->SetMotionAlarmSwitch(configNew.bMotionEnable);
	//	m_pVoiceDetecter->SetTimePlan(configNew.bTimeSectionEnable);
	}
	
	_printd("bCryEnable:%d,bMotionEnable:%d,bHumanEnable:%d,bVoiceEnable:%d,bTraceEnable:%d",
		configNew.bCryEnable,configNew.bMotionEnable,configNew.bHumanEnable,
		configNew.bVoiceEnable,configNew.bTraceEnable);
}

void CAlarmDetecterManager::ForceCloseTracker()
{
#ifdef IPC_JZ
	return ;
#endif
	m_AlarmSwitch.update();
	CONFIG_ALARMSWITCH &cfgSwitch = m_AlarmSwitch.getConfig();

	cfgSwitch.bTraceEnable = 0;

	m_AlarmSwitch.commit();

	_printd("turn off the tracker");
}

void CAlarmDetecterManager::Start()
{
#ifdef IPC_JZ
	return ;
#endif
	int iRet = 0;

	m_AlarmSwitch.attach(this, (TCONFIG_PROC)&CAlarmDetecterManager::onConfigAlarmSwitch);
	m_AlarmSwitch.update();
	onConfigAlarmSwitch(m_AlarmSwitch,iRet);
	
	//CreateThread();
}
void CAlarmDetecterManager::Stop()
{
	//DestroyThread();
}

void CAlarmDetecterManager::ThreadProc()
{
#ifdef IPC_JZ
	return ;
#endif
	while(m_bLoop){
		sleep(1);
	}
}

