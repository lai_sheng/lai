

#include "Functions/Linkage.h"

Ilinkage::Ilinkage()
{

}

Ilinkage::~Ilinkage()
{

}

CLinkage::CLinkage()
{
	for (int i = 0; i < appEventAll; i++)
	{
		m_LowEvent[i] = 0;
		m_HighEvent[i] = 0;
	}
}

CLinkage::~CLinkage()
{

}

void CLinkage::setLinkItem(appEventCode code)
{
	if (code >= appEventAll)
	{
		return;
	}

	m_dwEventItem |= BITMSK(code);
}

VD_BOOL CLinkage::start(appEventCode code, int index)
{
	int iHindex = 0;	
	if (index >= 32)
	{
		iHindex = index - 32;
	}

	if ((code >= appEventAll) || (!(m_dwEventItem & BITMSK(code))))
	{
		return FALSE;
	}

	if (index < 32)
	{
		if ((m_LowEvent[code] & BITMSK(index)))
		{
			return FALSE;
		}
		m_LowEvent[code] |= BITMSK(index);
	}
	else
	{
		if ((m_HighEvent[code] & BITMSK(iHindex)))
		{
			return FALSE;
		}
		m_HighEvent[code] |= BITMSK(iHindex);
	}

	return TRUE;
}

VD_BOOL CLinkage::stop(appEventCode code, int index)
{	
	int iHindex = 0;	
	if (index >= 32)
	{
		iHindex = index - 32;
	}
	
	if ((code >= appEventAll) || (!(m_dwEventItem & BITMSK(code))))
	{
		return FALSE;
	}

	if (index < 32)
	{	
		if (!(m_LowEvent[code] & BITMSK(index)))
		{
			return FALSE;
		}
		m_LowEvent[code] &= ~BITMSK(index);
	}
	else
	{
		if (!(m_HighEvent[code] & BITMSK(iHindex)))
		{
			return FALSE;
		}		
		m_HighEvent[code] &= ~BITMSK(iHindex);
	}
	return TRUE;
}

VD_BOOL CLinkage::isEmpty()
{
	VD_BOOL bRet = TRUE;

	for (int i = (int)appEventAlarmLocal; i < (int)appEventAll; i++)
	{
		if (m_dwEventItem & BITMSK(i))
		{
			if ((m_LowEvent[i] != 0) || (m_HighEvent[i] != 0))
			{
//				tracepoint();
				bRet = FALSE;
			}
		}
	}

	return bRet;
}

uint CLinkage::getEventSource(appEventCode code)
{
	if (code != appEventAlarmDecoder)
	{
		return m_LowEvent[code];
	}
	
	return m_HighEvent[code]; 
}
