#include "Functions/Alarm.h"
#include "Functions/Record.h"
#include "Functions/Encode.h"
//#include "Functions/Display.h"
#include "Devices/DevAlarm.h"
#include "Devices/DevDetect.h"
#include "Devices/DevFrontboard.h"
#include "APIs/Split.h"
#include "APIs/Net.h"
#include "System/Log.h"
#include "APIs/Alarm.h"
#include "Devices/DevPtz.h"
#include "Configs/ConfigManager.h"
#include "System/UserManager.h"
#include "Main.h"


#define NLEVER 6
#define CALARM_ALL_CHANNELS -1
#define ALARMIN_LATCH_DELAY 0    //报警输入延时时间的默认值
#define RESET_PRESSDOWN 1

extern SYSTEM_TIME         g_StartTime;            //开机时间
extern int g_nNetAlarmIn;
/*-----------------------------------------------------------------------
    给出，应用仅仅给出灵敏等级。
-----------------------------------------------------------------------*/
int sensitiveness[NLEVER] = {15,12,9,5,2,0};            // 灵敏度

int frameinterval[NLEVER] = {2,14,26,38,50,62};            // 帧间隔

/*-----------------------------------------------------------------------
    CAlarm类的静态成员变量，定义为静态是因为CAlarmManager对象在一开始的
    初始化函数中就要被调用；
-----------------------------------------------------------------------*/
PATTERN_SINGLETON_IMPLEMENT(CAlarm);
void CAlarm::ThreadProc()
{
	//用于计算按下复位键时间，判断是否需要复位 add by Le0
	unsigned int ResetTimes = 0;
    while (m_bLoop)                //读取并分发四种报警信号
    {
        uint state = 0;
#ifdef DVR_HI
        //视频丢失
        if (ICaptureManager::instance()->GetAnalogChnNum() > 0)
        {      
            state = m_pDevLossDetect->GetState();
            /* 无效位置0 */
            state &= ( 0xFFFFFFFF>>(32-ICaptureManager::instance()->GetAnalogChnNum()) );
        }
        m_alarmManager->VD_SendMessage(appEventVideoLoss, state);

        //视频遮挡
        uint uiBlindState = 0;
        uiBlindState = m_pDevBlindDetect->GetState() & (~state);
        if (ICaptureManager::instance()->GetAnalogChnNum() > 0)
        {
            //!有视频丢失的情况下，不再报告视频遮
        	uiBlindState &= ( 0xFFFFFFFF>>(32-ICaptureManager::instance()->GetAnalogChnNum()) );
        }
        m_alarmManager->VD_SendMessage(appEventVideoBlind, uiBlindState);

#else
        m_alarmManager->VD_SendMessage(appEventVideoLoss, m_pDevLossDetect->GetState());
        m_alarmManager->VD_SendMessage(appEventVideoBlind, m_pDevBlindDetect->GetState());
#endif

       // 视频动态检测
        state = 0;
        if (ICaptureManager::instance()->GetAnalogChnNum() > 0)
        {
            state = m_pDevMotionDetect->GetState();

            /* 无效位置0 */
            state &= ( 0xFFFFFFFF>>(32-ICaptureManager::instance()->GetAnalogChnNum()) );
        }
        //printf("appEventVideoMotion state:%#x\n", state);
        m_alarmManager->VD_SendMessage(appEventVideoMotion, state);

        // 输入报警
        m_alarmManager->VD_SendMessage(appEventAlarmLocal, m_pDevAlarm->GetInState(), appEventAlarmLocal);
		//判断系统是否需要复位
		if(RESET_PRESSDOWN == m_pDevAlarm->GetResetState())
		{
			ResetTimes++;
			if(ResetTimes > 25)
			{
				printf("\n\n########Restore the default configuration,system will reboot!#########\n");
				g_Config.SetDefaultConfig(DEFAULT_CFG_ALL);
				//用户账户密码恢复默认设置 Le0 20140806
				g_userManager.setDefault();
				
				//zhy 按尾线RST 10秒重启设备20150701
				SystemReboot();
			}
		}
		else
		{
			ResetTimes = 0;
		}
        SystemSleep(200);
    }
    return;
}

CAlarm::CAlarm() : CThread("Alarm", TP_ALARM),
    m_cTimer("Alarm"),
    m_cReticleTimer("ALM_Reticle"),
    m_cVGATipTimer("ALM_VGATip"),
    m_sigBuffer(32)
{
    m_dwAlarmState = 0;
    m_dwNetAlarmState = 0;
    m_dwMotionDetectState = 0;
    m_dwLossDetectState = 0;
    m_dwBlindDetectState = 0;    
    m_dwAlarmOutState = 0;
    m_dwManualAlarmState = 0;

    for (int type = ALARM_NET_RETICLE; type < ALARM_NET_ALL; type++)
    {
        m_bNetState[type] = FALSE;
    }
    m_dwAlarmOutType = 0;    //报警输出模式初始化为自动输出.
    m_alarmManager = NULL;

    trace("CAlarm::CAlarm()>>>>>>>>>\n");

    for (int ii = 0; ii < N_ALM_OUT; ii++)
    {
        for (int i = (int)appEventAlarmLocal; i < (int)appEventAll; i++)
        {
            m_cAlarmOutLinkage[ii].setLinkItem((appEventCode)i);
        }
    }
    //m_pDevAlarm初始化放在构造函数里面做，以免刚开机就点报警输出时死机。
    m_pDevAlarm = CDevAlarm::instance();

    m_ucVgaStd = 255;
    m_BVgaStdFlag = VD_FALSE;
    m_BVGAAlarmTip = VD_FALSE;
}

CAlarm::~CAlarm()
{
}

VD_BOOL CAlarm::Start()
{
    trace("CAlarm::Start()>>>>>>>>>\n");
    int i = 0;
    m_alarmManager = new CAlarmManager();
    m_alarmManager->Start();

    //读取配置信息
    m_CCfgAlarm.update();
    m_CCfgNetAlarm.update();
    m_CCfgMotion.update();
    m_CCfgLoss.update();
    m_CDecAlarm.update();
    m_CCfgBlind.update();
    m_CCfgNetAbort.update();
    m_CCfgNetArp.update();
 	
#ifdef POLICE_PROJECT 
	SetTwinkleLedStatus(0); //关灯
	m_cTimerLed.Start(this,(VD_TIMERPROC)&CAlarm::onTimerLed,10000,5000);
#endif

    //获取设备对象指针
    m_pDevMotionDetect    = CDevMotionDetect::instance();
    m_pDevLossDetect    = CDevLossDetect::instance();
    m_pDevBlindDetect    = CDevBlindDetect::instance();

    onConfigMotion(m_CCfgMotion, i);
    onConfigBlind(m_CCfgBlind, i);
    //向事件中心注册处理类
    CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CAlarm::onAppEvent);

    //配置变化回掉函数
    m_CCfgAlarm.attach(this, (TCONFIG_PROC)&CAlarm::onConfigAlarm);
    m_CCfgNetAlarm.attach(this, (TCONFIG_PROC)&CAlarm::onConfigNetAlarm);
    m_CCfgMotion.attach(this, (TCONFIG_PROC)&CAlarm::onConfigMotion);
    m_CCfgLoss.attach(this, (TCONFIG_PROC)&CAlarm::onConfigLoss);
    m_CDecAlarm.attach(this, (TCONFIG_PROC)&CAlarm::onConfigDecAlm);
    m_CCfgBlind.attach(this, (TCONFIG_PROC)&CAlarm::onConfigBlind);
    m_CCfgNetAbort.attach(this, (TCONFIG_PROC)&CAlarm::onConfigNetAbort);
    m_CCfgNetArp.attach(this, (TCONFIG_PROC)&CAlarm::onConfigNetArp);

    for (i = 0; i < N_PTZ_ALARM; i++)
    {
        m_dwAlarmPtzState[i] = 0;
    }

    for (int k = 0; k < N_ALM_IN; k++)
    {
        m_iAlarmLatchDelay[k] = ALARMIN_LATCH_DELAY;
    }    	
    for (int k = 0; k < N_SYS_CH; k++)
    {
        m_iMotionLatchDelay[k] = ALARMIN_LATCH_DELAY;
        m_iBeepstate[k] = ALARMIN_LATCH_DELAY;
    }

    //启动报警模块中的线程和定时器
    CreateThread();
#if 0 
	m_cReticleTimer.Start(this, (VD_TIMERPROC)&CAlarm::GetReticleState, 0, 1000);//每秒执行一次

    if (g_nCapture == 5)
    {
        m_cVGATipTimer.Start(this, (VD_TIMERPROC)&CAlarm::VGAAlarmTip, 0, 1000);//每秒执行一次
    }
#endif
    return TRUE;
}

VD_BOOL CAlarm::Stop()
{
    trace("CAlarm::Stop()>>>>>>>>>\n");
    m_alarmManager->Stop();
    delete m_alarmManager;
    //销毁线程和停止定时器
    DestroyThread();
    m_cTimer.Stop();
    return TRUE;
}



/*!   
==    ======================================================================
==    $DSC :    该函数用来注册一个处理报警信号的对象
==    $ARG :    pObj：处理报警信号的对象指针
==         :    pPorc：处理报警信号的回调函数指针
==    ======================================================================
*/
VD_BOOL CAlarm::Attach(CObject * pObj, SIG_ALARM_BUFFER pProc)
{
    int iSlots = m_sigBuffer.Attach(pObj, pProc);
    assert(iSlots >=0);
    
    if (iSlots < 0)
    {
        trace("alarm attach error, error num=%d!\n", iSlots);
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

VD_BOOL CAlarm::Detach(CObject * pObj, SIG_ALARM_BUFFER pProc)
{
    int iSlots = m_sigBuffer.Detach(pObj, pProc);
    if (iSlots < 0)
    {
        trace("alarm detach error!\n");
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

/*!    $FXN :    SendMessage
==    ======================================================================
==    $DSC :    该接口函数提供给外界一个手动产生报警信号的方式
==    $ARG :    msg：报警消息，可以是MESSAGE_ALARM常量中的任何一个值
==         :    wpa：状态值，就相当于dwState变量
==         :    lpa：消息源，表明该消息的出处
==         :    priority：保留
==    ======================================================================
*/
VD_BOOL CAlarm::VD_SendMessage (uint msg, uint wpa, uint lpa, uint priority)
{
    return m_alarmManager->VD_SendMessage(msg, wpa, lpa, priority);
}

//该函数用来手动一次性对所有的联动通道刷新一遍
void CAlarm::SetAlarmOut(uint dwState, int iType)
{
    int i;
    uint dwOutState = 0;
    
    switch (iType)
    {
    case ALARM_OUT_AUTO:
        //只有在自动模式下可以输出
        for (i = 0; i < g_nAlarmOut; i++)
        {
            if (((m_dwAlarmOutType >> (2 * i)) & 0x3) == 0x0)//自动模式下
            {
                dwOutState |= (dwState & BITMSK(i));
                m_dwManualAlarmState &= ~BITMSK(i);
            }
            else if (((m_dwAlarmOutType >> (2 * i)) & 0x3) == 0x1)//手动模式
            {
                m_dwManualAlarmState |= BITMSK(i);
            }
            else
            {
                //关闭报警输出模式，报警输出不可用.
                m_dwManualAlarmState &= ~BITMSK(i);
            }                
        }        
        //m_dwAlarmOutState = dwState;
        m_pDevAlarm->SetOutState(dwOutState | m_dwManualAlarmState);
        break;
    case ALARM_OUT_MANUAL:
        m_dwManualAlarmState = dwState;
        for (i = 0; i < g_nAlarmOut; i++)
        {
            if ((m_dwManualAlarmState & BITMSK(i)) && (!(m_dwAlarmOutState & BITMSK(i))))
            {
                if (((m_dwAlarmOutType >> (2 * i)) & 0x3) == 0x0)//自动模式
                {
                    m_dwManualAlarmState &= ~BITMSK(i);
                }
            }
        }        
        m_pDevAlarm->SetOutState(m_dwManualAlarmState);
        break;
    case ALARM_OUT_CLOSED:        
        break;
    default:
        return;
    }    
}

uint CAlarm::GetAlarmOut()
{
    return m_pDevAlarm->GetOutState();
}

void CAlarm::onConfigAlarm(CConfigAlarm& config, int &ret)
{
    for (int i = 0; i < g_nAlarmIn; i++)
    {
        CONFIG_ALARM& cfgOld = m_CCfgAlarm.getConfig(i);
        CONFIG_ALARM& cfgNew = config.getConfig(i);
        if (memcmp(&cfgOld.hEvent, &cfgNew.hEvent, sizeof(EVENT_HANDLER)) != 0)
        {
            if (cfgNew.iSensorType == cfgOld.iSensorType)//修改常开常闭时，触发的报警信息通过报警线程来发送，不在这里发送
            {
                if ((m_dwAlarmState & BITMSK(i)) && (cfgNew.bEnable))
                {
                    CAppEventManager::instance()->notify(appEventAlarmLocal, i, appEventConfig, &m_CCfgAlarm.getLatest(i).hEvent,
                        &CConfigALMWorksheet::getLatest(i));    
                }
            }
            else//当正在报警联动的过程中,对配置恢复了默认,存在报警输出联动前和恢复默认后的配置不一致;导致联动输出不能停止.
            {
                if ((m_dwAlarmState & BITMSK(i)) && (cfgNew.bEnable)
                    && ((TRUE == cfgOld.hEvent.bAlarmOutEn)))
                {                    
                    CAppEventManager::instance()->notify(appEventAlarmLocal, i, appEventStop, &m_CCfgAlarm.getConfig(i).hEvent,
                        &CConfigALMWorksheet::getLatest(i));

                    //处理正在报警输出时,恢复默认不能停止问题
                    for (int j = 0; j < g_nAlarmOut; j++)
                    {
                        CLinkage *pLinkage = m_cAlarmOutLinkage + j;
                        
                        if ((pLinkage->stop(appEventAlarmLocal, i)) && (pLinkage->isEmpty()) && (cfgOld.hEvent.dwAlarmOut & BITMSK(j)))
                        {
                            m_dwAlarmOutState &= ~BITMSK(j);
                        }
                    }
                }
            }            
        }
    }
    m_CCfgAlarm.update();
}

void CAlarm::onConfigNetAlarm(CConfigNetAlarm &config, int &ret)
{
    for (int i = 0; i < g_nAlarmIn; i++)
    {
        CONFIG_ALARM& cfgOld = m_CCfgNetAlarm.getConfig(i);
        CONFIG_ALARM& cfgNew = config.getConfig(i);
        if (memcmp(&cfgOld.hEvent, &cfgNew.hEvent, sizeof(EVENT_HANDLER)) != 0)
        {
            if ((m_dwNetAlarmState & BITMSK(i)) && (cfgNew.bEnable))
            {
                CAppEventManager::instance()->notify(appEventAlarmNet, i, appEventConfig, &cfgNew.hEvent, 
                    &CConfigNetAlmWorksheet::getLatest(i), NULL);    
            }
        }
    }
    m_CCfgNetAlarm.update();
}

void CAlarm::onConfigMotion(CConfigMotionDetect &config, int &ret)
{
    for (int i = 0; i < g_nCapture; i++)
    {
        CONFIG_MOTIONDETECT& cfgOld = m_CCfgMotion.getConfig(i);
        CONFIG_MOTIONDETECT& cfgNew = config.getConfig(i);
        
        // 设置动态检测区域和灵敏度
        if ((cfgOld.iLevel != cfgNew.iLevel) || (cfgOld.bEnable != cfgNew.bEnable)
            || (memcmp(cfgOld.mRegion, cfgNew.mRegion, sizeof(uint)*MD_REGION_ROW) != 0)
            || (&cfgOld == &cfgNew))
        {
            MOTION_DETECT_PARAM sDMP;
            
            sDMP.iLevel = cfgNew.iLevel;
            memcpy(sDMP.win, cfgNew.mRegion, 4 * 18);
            sDMP.sensitiveness = sensitiveness[cfgNew.iLevel - 1];            
            sDMP.fieldinterval = frameinterval[cfgNew.iLevel - 1];
            sDMP.enable =  cfgNew.bEnable;
            if (!m_pDevMotionDetect->SetParameter(i, &sDMP))
            {
                trace("SetParameter error!\n");
            }
        }
        
        if (memcmp(&cfgOld.hEvent, &cfgNew.hEvent, sizeof(EVENT_HANDLER)) != 0)
        {
            //修改去抖动时间段的过程中，触发的动态检测事件由报警线程来处理，在这里不做处理，否则会多发动检事件。
            if ((m_dwMotionDetectState & BITMSK(i)) && (cfgNew.bEnable)
                && (cfgNew.hEvent.iEventLatch == cfgOld.hEvent.iEventLatch))
            {
                CAppEventManager::instance()->notify(appEventVideoMotion, i, appEventConfig, &cfgNew.hEvent, 
                    &CConfigMTDWorksheet::getLatest(i), NULL);    
            }
        }
    }
    m_CCfgMotion.update();
}

void CAlarm::onConfigLoss(CConfigLossDetect &config, int &ret)
{
    for (int i = 0; i < g_nLogicNum; i++)
    {
        CONFIG_GENERIC_EVENT& cfgOld = m_CCfgLoss.getConfig(i);
        CONFIG_GENERIC_EVENT& cfgNew = config.getConfig(i);
        if (memcmp(&cfgOld.handler, &cfgNew.handler, sizeof(EVENT_HANDLER)) != 0)
        {
            if ((m_dwLossDetectState & BITMSK(i)) && (cfgNew.enable))
            {
                CAppEventManager::instance()->notify(appEventVideoLoss, i, appEventConfig, 
                &m_CCfgLoss.getLatest(i).handler,
                    &CConfigVLTWorksheet::getLatest(i), NULL);    
            }
        }
    }
    m_CCfgLoss.update();
}

void CAlarm::onConfigDecAlm(CConfigDecoderAlarm &config, int &ret)
{
    for (int i = 0; i < g_nLogicNum - g_nCapture; i++)
    {
        CONFIG_GENERIC_EVENT& cfgOld = m_CDecAlarm.getConfig(i);
        CONFIG_GENERIC_EVENT& cfgNew = config.getConfig(i);
        if (memcmp(&cfgOld.handler, &cfgNew.handler, sizeof(EVENT_HANDLER)) != 0)
        {
            if ((m_dwLossDetectState & BITMSK(i+g_nCapture)) && (cfgNew.enable))
            {
                CAppEventManager::instance()->notify(appEventVideoLoss, i+g_nCapture, appEventConfig, 
                &m_CDecAlarm.getLatest(i).handler,
                    &CConfigDecAlmWorksheet::getLatest(i), NULL);    
            }
        }
    }
    m_CDecAlarm.update();
}

void CAlarm::onConfigBlind(CConfigBlindDetect &config, int &ret)
{
    for (int i = 0; i < g_nCapture; i++)
    {
        CONFIG_BLINDDETECT& cfgOld = m_CCfgBlind.getConfig(i);
        CONFIG_BLINDDETECT& cfgNew = config.getConfig(i);
        
        BLIND_DETECT_PARAM  pBLD;
        pBLD.Enable = (int)(cfgNew.bEnable);
        pBLD.Level = (uchar)(cfgNew.iLevel);

        if(!m_pDevBlindDetect->SetParameter(i, &pBLD))
        {
            trace("SetParameter error!\n");
        }
        if (memcmp(&cfgOld.hEvent, &cfgNew.hEvent, sizeof(EVENT_HANDLER)) != 0)
        {
            if ((m_dwBlindDetectState & BITMSK(i)) && (cfgNew.bEnable))
            {
                CAppEventManager::instance()->notify(appEventVideoBlind, i, appEventConfig, &cfgNew.hEvent, 
                    &CConfigBLDWorksheet::getLatest(i), NULL);    
            }
        }
    }
    m_CCfgBlind.update();
}

#ifdef POLICE_PROJECT 
void CAlarm::onTimerLed(int arg)
{
		int iRet = 0;
		iRet = CAppEventManager::instance()->checkTimeSection(&CConfigLEDWorksheet::getLatest()) == TRUE ? 0 : 1;
		if ( 0 == GetTwinkleLedStatus() && 0 == iRet)
		{
			SetTwinkleLedStatus(1); //开灯
		}
		if (1 == GetTwinkleLedStatus() && 1 == iRet) //关灯
		{
			SetTwinkleLedStatus(0);
		}	
}
#endif

void CAlarm::onConfigNetAbort(CConfigNetAbort &config, int &ret)
{
    CONFIG_GENERIC_EVENT& cfgOld = m_CCfgNetAbort.getConfig();
    CONFIG_GENERIC_EVENT& cfgNew = config.getConfig();

    if (memcmp(&cfgOld.handler, &cfgNew.handler, sizeof(EVENT_HANDLER)) != 0)
    {
        if ((TRUE == m_bNetState[ALARM_NET_RETICLE]) && (cfgNew.enable))
        {
            CAppEventManager::instance()->notify(appEventNetAbort, 0, appEventStop, &cfgNew.handler);

            //处理正在报警输出时,恢复默认不能停止问题
            for (int j = 0; j < g_nAlarmOut; j++)
            {
                CLinkage *pLinkage = m_cAlarmOutLinkage + j;
                if ((pLinkage->stop(appEventNetAbort, 0)) 
                    && (pLinkage->isEmpty()) && (cfgOld.handler.dwAlarmOut & BITMSK(j)))
                {
                    m_dwAlarmOutState &= ~BITMSK(j);
                }
            }
        }
    }
    m_CCfgNetAbort.update();
}

void CAlarm::onConfigNetArp(CConfigNetArp &config, int &ret)
{
    CONFIG_GENERIC_EVENT& cfgOld = m_CCfgNetArp.getConfig();
    CONFIG_GENERIC_EVENT& cfgNew = config.getConfig();
    if (memcmp(&cfgOld.handler, &cfgNew.handler, sizeof(EVENT_HANDLER)) != 0)
    {
        if ((TRUE == m_bNetState[ALARM_NET_ARP]) && (cfgNew.enable))
        {            
            CAppEventManager::instance()->notify(appEventNetArp, 0, appEventStop, 
                &cfgNew.handler);
        }
    }
    m_CCfgNetArp.update();
}

//处理报警输出
void CAlarm::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
    int i;    
    if(!param)
    {
        return;
    }
    if ((code == appEventStorageReadErr) || (code == appEventStorageWriteErr) ||(code == appEventStorageFailure))
    {
        return;
    }

    switch(action) 
    {
    case appEventStart:
        for (i = 0; i < g_nAlarmOut; i++)
        {
            CLinkage *pLinkage = m_cAlarmOutLinkage + i;
            if ((param->bAlarmOutEn) && (param->dwAlarmOut & BITMSK(i)))
            {
                m_dwAlarmOutState |= BITMSK(i);
                pLinkage->start(code, index);
            }
            else
            {
                if ((pLinkage->stop(code, index)) && (pLinkage->isEmpty()))
                {
                    m_dwAlarmOutState &= ~BITMSK(i);
                }
            }
        }
        SetAlarmOut(m_dwAlarmOutState);
        if(param->bBeep) 
        {
    		SystemBeep(880,300);     
            m_iBeepstate[index] = 1;
        }
        break;
    case appEventStop:
        for (i = 0; i < g_nAlarmOut; i++)
        {
            CAppEventManager::instance()->latchEvent(this, (SIG_EVENT::SigProc)&CAlarm::onAppEvent, code, index, param->iAOLatch);
        }
        break;
    case appEventLatch:
        for(i = 0; i < g_nAlarmOut; i++)
        {
            CLinkage *pLinkage = m_cAlarmOutLinkage + i;
            ///*去除输出通道的开关校验，目的是防止某输出通道处于延迟输出状态(等待关闭)，
            ///*输出通道的开关已经被用户改变，而造成无法关闭该输出通道的报警输出
            ///*无论某输出通道的是开启还是关闭，只要该通道存在输出，就关闭该通道的输出
            if ((pLinkage->stop(code, index)) && (pLinkage->isEmpty()) )
            {
                m_dwAlarmOutState &= ~BITMSK(i);
            }
        }
        SetAlarmOut(m_dwAlarmOutState);
        m_iBeepstate[index] = 0;
        break;
    default:
        trace("appEventUnknown\n");
        break;
    }
}

void CAlarm::Alarm(uint dwState, int iEventType ,char *pContext)
{
    int    i = 0;    
    if (appEventAlarmLocal == iEventType)
    {
    	EVENT_HANDLER  stEventHandler;
        for (i = 0; i < g_nAlarmIn; i++)
        {
			memcpy( &stEventHandler,  &m_CCfgAlarm.getConfig(i).hEvent , sizeof(EVENT_HANDLER));
            //本地报警开关
            if (TRUE == m_CCfgAlarm[i].bEnable)
            {    
                if (NC == m_CCfgAlarm[i].iSensorType)
                {
                    dwState ^= BITMSK(i);
                }

                //有本地报警输入时，通知事件中心
                if ((dwState ^ m_dwAlarmState )& BITMSK(i))
                {
                    if (m_dwAlarmState & BITMSK(i))
                    {
                         m_dwAlarmState &= ~BITMSK(i);   
                        //本地报警结束，通知事件中心
                        CAppEventManager::instance()->notify(appEventAlarmLocal, i, appEventStop, &stEventHandler, 
                            &CConfigALMWorksheet::getLatest(i), NULL);                                         
                        g_Log.Append(LOG_ALM_END, i+1, 0, 4);
					}
                    else
                    {
                        // 设备报警输入状态
                        if (m_iAlarmLatchDelay[i] == 0)
                        {
                            m_iAlarmLatchDelay[i] = SystemGetMSCount();
                        }

                        if (m_iAlarmLatchDelay[i] != 0)
                        {
                            if ((SystemGetMSCount() - m_iAlarmLatchDelay[i]) >= (unsigned int)m_CCfgAlarm.getConfig(i).hEvent.iEventLatch * 1000)
                            {
                                m_dwAlarmState |= BITMSK(i);                    

                                CAppEventManager::instance()->notify(appEventAlarmLocal, i, appEventStart, &stEventHandler, 
                                    &CConfigALMWorksheet::getLatest(i), NULL);                    

                                g_Log.Append(LOG_ALM_IN, i+1, 0, 4);
                                m_iAlarmLatchDelay[i] = ALARMIN_LATCH_DELAY;
                            }
                        }
                    }
                }
                else
                {
                    if (!(m_dwAlarmState & BITMSK(i)))
                    {
                        m_iAlarmLatchDelay[i] = ALARMIN_LATCH_DELAY;
                    }
                }
            }
            else
            {
                if (m_dwAlarmState & BITMSK(i))
                {
                    //本地报警结束，通知事件中心
                    m_CCfgAlarm.getConfig(i).hEvent.dwAlarmOut = 0x3F;
                    CAppEventManager::instance()->notify(appEventAlarmLocal, i, appEventStop, &stEventHandler, 
                        &CConfigALMWorksheet::getLatest(i), NULL);

                    m_dwAlarmState &= ~BITMSK(i);                    
                    g_Log.Append(LOG_ALM_END, i+1, 0, 4);					
                }
            }            
        }
    }
    else if (appEventAlarmNet == iEventType)
    {
        int alarmin = 0;
        for (i = 0; i < g_nNetAlarmIn; i++)
        {
            //网络报警开关
            if (TRUE == m_CCfgNetAlarm[i].bEnable)
            {
                //有网络报警输入时，通知事件中心
                if ((dwState ^ m_dwNetAlarmState )& BITMSK(i))
                {
                    if (m_dwNetAlarmState & BITMSK(i))
                    {
                         m_dwNetAlarmState &= ~BITMSK(i);  
                         CAppEventManager::instance()->notify(appEventAlarmNet, i, appEventStop, &m_CCfgNetAlarm.getConfig(i).hEvent, 
                            &CConfigNetAlmWorksheet::getLatest(i), NULL);                    
						
                          g_Log.Append(LOG_ALM_END, i+1, 0, 4);

                    }
                    else
                    {
                        m_dwNetAlarmState |= BITMSK(i);                
                        alarmin = i;
                        
                        CAppEventManager::instance()->notify(appEventAlarmNet, i, appEventStart, &m_CCfgNetAlarm.getConfig(i).hEvent,
                            &CConfigNetAlmWorksheet::getLatest(i), NULL);       
			     		g_Log.Append(LOG_NETALM_IN, i+1, 0, 4);			
                    }
                }
            }
            else
            {
                if (m_dwNetAlarmState & BITMSK(i))
                {
                    m_dwNetAlarmState &= ~BITMSK(i);
                    //网络报警结束，通知事件中心
                    CAppEventManager::instance()->notify(appEventAlarmNet, i, appEventStop, &m_CCfgNetAlarm.getConfig(i).hEvent, 
                        &CConfigNetAlmWorksheet::getLatest(i), NULL);                    
		      		g_Log.Append(LOG_ALM_END, i+1, 0, 4);			
                }
            }                            
        }
        
    }
}

void CAlarm::Motion(uint dwState)
{
    int i = 0;
    uint uiLostState = m_pDevLossDetect->GetLossState();

    for (i = 0; i < ICaptureManager::instance()->GetLogicChnNum(); i++)
    {
    	//动检使能
        if (TRUE == m_CCfgMotion[i].bEnable)
        {
        	//tracepoint();
            //报警输入,通知事件中心
            if ((dwState ^ m_dwMotionDetectState) & BITMSK(i))
            {
                if (m_dwMotionDetectState & BITMSK(i))
                {
                     m_dwMotionDetectState &= ~BITMSK(i);    
                    if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                    {
                        //报警结束，通知事件中心
                        CAppEventManager::instance()->notify(appEventVideoMotion, i, appEventStop, &m_CCfgMotion.getLatest(i).hEvent, 
                            &CConfigMTDWorksheet::getLatest(i), NULL);
                        
                        g_Log.Append(LOG_MOTION_END, i+1, 0, 4);//添加日志
                    }
                    m_iMotionLatchDelay[i] = 0 ; //停止报警状态，清除抖动计时时间add by james.xu 2010.01.14
                }
                else
                {
                    //第一次有报警时，记录当前时间，用于去抖动时间比较add by james on 2010.11.13
                    if (m_iMotionLatchDelay[i] == 0)
                    {
                        m_iMotionLatchDelay[i] = SystemGetMSCount();
                    }
					
                    if (m_iMotionLatchDelay[i] != 0)
                    {
                        const CONFIG_WORKSHEET* wkSheet = &CConfigMTDWorksheet::getLatest(i);
                        if (CAppEventManager::instance()->checkTimeSection(wkSheet) &&
                            ((SystemGetMSCount() - m_iMotionLatchDelay[i]) >= (unsigned int)m_CCfgMotion.getConfig(i).hEvent.iEventLatch * 1000))
                        {
                            m_dwMotionDetectState |= BITMSK(i);                    

                            if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                            {
                                CAppEventManager::instance()->notify(appEventVideoMotion, i, appEventStart, &m_CCfgMotion.getConfig(i).hEvent, 
                                    &CConfigMTDWorksheet::getLatest(i), NULL);

                                g_Log.Append(LOG_MOTION_IN, i+1, 0, 4);//添加日志

                            }
                            m_iMotionLatchDelay[i] = 0 ;//触发报警状态后，清除抖动计时时间add by james.xu 2010.01.14
                        }    
                    }
                }
            }
            else  //当前及上次报警状态均无报警时，对  超过去抖动时间没有报警，将状态清零。add by james on 2010.11.13
            {         
                if (m_iMotionLatchDelay[i]>0) 
                {
                    m_CCfgMotion.update();
                    if((SystemGetMSCount() - m_iMotionLatchDelay[i]) >= (unsigned int)m_CCfgMotion.getConfig(i).hEvent.iEventLatch * 1000)
                    {
                        m_iMotionLatchDelay[i] = 0 ;                        
                    }
                }
            }
        }        
        else
        {
            if (m_dwMotionDetectState & BITMSK(i))
            {
                //报警结束，通知事件中心
                m_CCfgMotion.getConfig(i).hEvent.dwAlarmOut = 0x3F;
				//	printf("-----------before LANGTAO3	--Le0!\n");
                if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                {
                    CAppEventManager::instance()->notify(appEventVideoMotion, i, appEventStop, &m_CCfgMotion.getConfig(i).hEvent, 
                        &CConfigMTDWorksheet::getLatest(i), NULL);    
                        g_Log.Append(LOG_MOTION_END, i+1, 0, 4);//添加日志
                }
                m_dwMotionDetectState &= ~BITMSK(i);
            }
        }
    }
}

void CAlarm::Loss(uint dwState)
{
    int i = 0;
    //视频丢失状态改变
    if(dwState != m_dwOldLossState)    
    {
        m_dwOldLossState = dwState;
    }    
    VD_BOOL Enable;
    EVENT_HANDLER *phandler = NULL;
    const CONFIG_WORKSHEET *pWorkSheet = NULL;
    for (i = 0; i < ICaptureManager::instance()->GetLogicChnNum(); i++)
    {    
        //视频丢失开关
        if(i < ICaptureManager::instance()->GetAnalogChnNum())
        {
            Enable = m_CCfgLoss[i].enable;
            phandler = &m_CCfgLoss.getConfig(i).handler;
            pWorkSheet = &CConfigVLTWorksheet::getLatest(i);
        }
        else
        {
            Enable = m_CDecAlarm[i - ICaptureManager::instance()->GetAnalogChnNum()].enable;
            phandler = &m_CDecAlarm.getConfig(i - ICaptureManager::instance()->GetAnalogChnNum()).handler ;
            pWorkSheet = &CConfigDecAlmWorksheet::getLatest(i - ICaptureManager::instance()->GetAnalogChnNum());
        }
        
        if (TRUE == Enable)
        {    
            //通知事件中心
            if ((dwState ^ m_dwLossDetectState )& BITMSK(i))
            {
                if (m_dwLossDetectState & BITMSK(i))
                {
                    m_dwLossDetectState &= ~BITMSK(i);

                    //本地报警结束，通知事件中心
                    CAppEventManager::instance()->notify(appEventVideoLoss, i, appEventStop, phandler,pWorkSheet, NULL);    

                    g_Log.Append(LOG_LOSS_END, i+1, 0, 4);//视频丢失结束，记录日志                  
                }
                else
                {
                    // 设备报警输入状态
                    m_dwLossDetectState |= BITMSK(i);                    
                    CAppEventManager::instance()->notify(appEventVideoLoss, i, appEventStart, phandler,pWorkSheet, NULL);
                    g_Log.Append(LOG_LOSS_IN, i+1, 0, 4);//添加日志
                }
            }
        }
        else
        {
            if (m_dwLossDetectState & BITMSK(i))
            {
                //本地报警结束，通知事件中心
                CAppEventManager::instance()->notify(appEventVideoLoss, i, appEventStop, phandler,pWorkSheet, NULL);                    
                m_dwLossDetectState &= ~BITMSK(i);                
                g_Log.Append(LOG_LOSS_END, i+1, 0, 4);//视频丢失结束，记录日志
            }
        }    
    }
}

void CAlarm::Blind(uint dwState)
{
    int i = 0;
    uint uiLostState = m_pDevLossDetect->GetLossState();

    for (i = 0; i < ICaptureManager::instance()->GetLogicChnNum(); i++)
    {
        //视频遮挡开关
        if (TRUE == m_CCfgBlind[i].bEnable)
        {    
            
            //报警输入时，通知事件中心
            if ((dwState ^ m_dwBlindDetectState )& BITMSK(i))
            {
                if (m_dwBlindDetectState & BITMSK(i))
                {
                    m_dwBlindDetectState &= ~BITMSK(i);
                    if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                    {
                        //本地报警结束，通知事件中心
                        CAppEventManager::instance()->notify(appEventVideoBlind, i, appEventStop, 
                            &m_CCfgBlind.getConfig(i).hEvent, &CConfigBLDWorksheet::getLatest(i), NULL);    
			   g_Log.Append(LOG_BLIND_END, i+1, 0, 4);//添加日志
                    }
                }
                else
                {
                    const CONFIG_WORKSHEET* wkSheet = &CConfigBLDWorksheet::getLatest(i);
                    // 设备报警输入状态
                    if (CAppEventManager::instance()->checkTimeSection(wkSheet))
                    {
                        m_dwBlindDetectState |= BITMSK(i);                    
                        if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                        {
                            CAppEventManager::instance()->notify(appEventVideoBlind, i, appEventStart, 
                                &m_CCfgBlind.getConfig(i).hEvent, &CConfigBLDWorksheet::getLatest(i), NULL); 

				 			g_Log.Append(LOG_BLIND_IN, i+1, 0, 4);//添加日志
                        }
                    }
                }
            }
        }
        else
        {
            if (m_dwBlindDetectState & BITMSK(i))
            {
                if((!(uiLostState & BITMSK(i))) || (i >= g_nCapture))
                {
                    //本地报警结束，通知事件中心
                    CAppEventManager::instance()->notify(appEventVideoBlind, i, appEventStop, 
                        &m_CCfgBlind.getConfig(i).hEvent, &CConfigBLDWorksheet::getLatest(i), NULL);
			 g_Log.Append(LOG_BLIND_END, i+1, 0, 4);//添加日志		
                }
                m_dwBlindDetectState &= ~BITMSK(i);
            }
        }
    }
}

//检测网线连接状态,每隔1秒钟运行一次
void CAlarm::GetReticleState(uint param)
{
    CConfigAudioInFormat pcConfig;
    pcConfig.update();
    CONFIG_AUDIOIN_FORMAT &audio_format = pcConfig.getConfig();
    for (int k = 0; k < N_SYS_CH; k++)
    {
        if(audio_format.LongtimeBeepEnable && m_iBeepstate[k])
        {
            SystemBeep(880,300);
            break;
        }
    }

    for (int type = ALARM_NET_RETICLE; type < ALARM_NET_ALL; type++)
    {
        GetNetState(type);
    } 
    
    uchar ucVgaStd= 255;
    /* VGA子型号 检测前端输入分辩率*/
    if (g_nCapture == 5)
    {
        /* 检查VGA子型号的输入，这里I2C不能太频繁，否则会导致视频丢失检测出问题 */
        static int iNum =0;
        iNum++;
        if((iNum%3) !=1)
        {
            return;
        }

        ucVgaStd = VideoReadRegister(4,0,0);        
        /* 读到合法值 */
        if (ucVgaStd >= 0 && ucVgaStd < 6)
        {
            m_BVgaStdFlag = VD_FALSE;
                
            /* 分辨率是否有变化 */
            if (m_ucVgaStd != 255 && m_ucVgaStd != ucVgaStd)
            {                
                printf("######iVgaStd = %d\r\n",ucVgaStd);
                //VD_MessageBox(strTmp, MT_OK);
                //g_Challenger.Reboot();
            }
            m_ucVgaStd = ucVgaStd;
        }
        else if (m_BVgaStdFlag == VD_FALSE)
        {
                m_BVgaStdFlag = VD_TRUE;
                /* 默认库内会初始化成1280*1024 */
                if(m_ucVgaStd == 255)
                {
                    m_ucVgaStd = 5;//	VGA_STD_1280X1024
                }
                printf("######iVgaStd = %d\r\n",ucVgaStd);
                m_BVGAAlarmTip = VD_TRUE;
				//VD_MessageBox(LOADSTR("VGA.ResolutionNotSupport"), MT_OK);
        }
    }
    
}

/* VGA输入提示 */
void CAlarm::VGAAlarmTip(uint param)
{
    if (m_BVGAAlarmTip)
    {
        char strTmp[1024]={""};
        memset(strTmp, 0, 1024);
        strncpy(strTmp,LOADSTR("VGA.ResolutionNotSupport"),512);
        strncpy(strTmp+strlen(strTmp),"\r\n800X600",64);
        strncpy(strTmp+strlen(strTmp),"   1024X768",64);
        strncpy(strTmp+strlen(strTmp),"  1152X864",64);
        strncpy(strTmp+strlen(strTmp),"  1280X960",64);
        strncpy(strTmp+strlen(strTmp),"  1280X800",64);
        strncpy(strTmp+strlen(strTmp),"  1280X768",64);
        strncpy(strTmp+strlen(strTmp),"  1280X1024",64);
                
#ifndef SYS_NO_GUI
        VD_MessageBox(strTmp, MT_OK);
#endif
        m_BVGAAlarmTip = VD_FALSE;
    }
}


//该函数用于检测网络连接状态和IP地址冲突
void CAlarm::GetNetState(int type)
{
    int iNetState = 0;
    char *eth0 = "eth0";
    appEventCode appCode = appEventInit;
    CONFIG_GENERIC_EVENT *pConfig = NULL;
#ifndef WIN32    
    switch(type)
    {
        case ALARM_NET_RETICLE:
            pConfig = &m_CCfgNetAbort.getConfig();
            
            iNetState = mii_diag(eth0);
            appCode = appEventNetAbort;
            break;
        case ALARM_NET_ARP:
            pConfig = &m_CCfgNetArp.getConfig();
            //tracef("\r\nArpCheck\r\n");
            iNetState = ArpCheck();
            appCode = appEventNetArp;
            break;
        default:
            break;
    }
#else
    return;
#endif
    if (NULL == pConfig)
    {
        tracepoint();
        return;
    }

    if (pConfig->enable)
    {
        //网络连接异常、或则IP地址冲突
        if (0 != iNetState)
        {
            if (FALSE == m_bNetState[type])
            {
#ifdef LOG_APPEND_NETABORT
                if (type == ALARM_NET_RETICLE)
                {
                    g_Log.Append(LOG_NET_ABORT);
                }
#endif
                if(type == ALARM_NET_ARP)
                {                    
                    g_Log.Append(LOG_NET_IP_CONFLICT);
                }
                m_bNetState[type] = TRUE;
                CAppEventManager::instance()->notify(appCode, 0, appEventStart, &pConfig->handler);                 
            }
            //add by nike.xie  不让报警灯长亮
            else
            {
            }
        }
        else
        {
            //连接正常只需要通知一次即可
            if (TRUE == m_bNetState[type])
            {            
#ifdef LOG_APPEND_NETABORT            
                if (type == ALARM_NET_RETICLE)
                {    
                    g_Log.Append(LOG_NET_ABORT_RESUME);
                }
#endif
                m_bNetState[type] = FALSE;
                CAppEventManager::instance()->notify(appCode, 0, appEventStop, &pConfig->handler);                    
            }
        }
    }
    else
    {
        if (TRUE == m_bNetState[type])
        {
            m_bNetState[type] = FALSE;
            CAppEventManager::instance()->notify(appCode, 0, appEventStop, &pConfig->handler);                    
        }
    }
}

uint CAlarm::GetNetShowState(ALARM_NET_EVENT type)
{
    int iState = 0;
    
    switch(type)
    {
        case ALARM_NET_RETICLE:
            iState = m_bNetState[ALARM_NET_RETICLE];
            break;
        case ALARM_NET_ARP:
            iState = m_bNetState[ALARM_NET_ARP];
            break;
        default:
            break;        
    }
    return iState;
}

uint CAlarm::GetAlarmState()
{
    return m_dwAlarmState;
}

uint CAlarm::GetNetAlarmState()
{
    return m_dwNetAlarmState;
}
uint CAlarm::GetAlarmPtzState(int index)
{
    if ((index >= 0) && (index < N_PTZ_ALARM))
    {
        return m_dwAlarmPtzState[index] & (~(255 << 24));
    }

    return 0;
}

uint CAlarm::GetMotionDetectState()
{
    return m_dwMotionDetectState;
}

uint CAlarm::GetLossDetectState()
{
    return m_dwLossDetectState;
}

uint CAlarm::GetBlindDetectState()
{
    return m_dwBlindDetectState;
}

uint CAlarm::GetAlarmOutType()
{
    return m_dwAlarmOutType;    
}
void CAlarm::SetAlarmOutType(uint dwMod)
{
    m_dwAlarmOutType = dwMod;
}

void CAlarm::Dump()
{
    trace("In State:\n");
    trace("Alarm In: %x\n", m_dwAlarmState);
    trace("Motion detect: %x\n", m_dwMotionDetectState);
    trace("Loss detect: %x\n", m_dwLossDetectState);
    trace("Blind detect: %x\n", m_dwBlindDetectState);
}

// CAlarmManager
void CAlarmManager::ThreadProc()
{
    VD_MSG msg;
    do 
    {
        VD_RecvMessage(&msg);
       Dispatch(msg);
    } while (m_bLoop);
}

CAlarmManager::CAlarmManager():CThread("AlarmManager", TP_ALARM, 1024)
{
    bAlarmOut = TRUE;
    trace("CAlarmManager::CAlarmManager()>>>>>>>>>\n");
}

CAlarmManager::~CAlarmManager()
{
}

VD_BOOL CAlarmManager::Start()
{
    VD_BOOL bRet;
    bRet = CreateThread();
    return bRet;
}

VD_BOOL CAlarmManager::Stop()
{
    VD_BOOL bRet;
    bRet = DestroyThread();
    return bRet;
}

VD_BOOL CAlarmManager::Dispatch(VD_MSG &msg)
{
    switch (msg.msg) 
    {
        case appEventAlarmLocal:
        case appEventAlarmNet:		
            g_Alarm.Alarm(msg.wpa, msg.lpa);
            break;
        case appEventVideoMotion:
            {//如果开机到现在不足20秒，则不采取行动。added by billhe at 2009-5-26
                SYSTEM_TIME strNow;
                memset(&strNow, 0, sizeof(SYSTEM_TIME));
                SystemGetCurrentTime(&strNow);
                
                if (g_StartTime.year == strNow.year && 
                    g_StartTime.month == strNow.month && 
                    g_StartTime.day == strNow.day && 
                    g_StartTime.wday == strNow.wday && 
                    g_StartTime.isdst == strNow.isdst)
                {
                    int iTimeStart = g_StartTime.hour * 60 * 60 +  g_StartTime.minute * 60 + g_StartTime.second;
                    int iTimeNow = strNow.hour * 60 * 60 +  strNow.minute * 60 + strNow.second;
                    
                    if ((iTimeNow - iTimeStart) < 20 && (iTimeNow - iTimeStart) > 0)
                    {
                        break;
                    }
                }
            }
            g_Alarm.Motion(msg.wpa);
            break;
        case appEventVideoLoss:
            g_Alarm.Loss(msg.wpa);
            break;
        case appEventVideoBlind:
            g_Alarm.Blind(msg.wpa);
            break;
        default:
            assert(0);
            break;
    }
    return TRUE;
}

