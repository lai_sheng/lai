
#include "Functions/Comm_Base.h"

#include "Functions/Comm.h"
#include "Functions/Record.h"
#include "Functions/Encode.h"
//#include "Functions/Display.h"

CTimer	CCommBase::m_Timer("CommBase");
int		CCommBase::m_ReadTimes;
#ifdef COM_SUPPORT_GPS
uint	CCommBase::m_DisconTime = 2;
#else
uint	CCommBase::m_DisconTime = 60;
#endif
uint	CCommBase::m_ContinuousTime;
VD_BOOL	CCommBase::m_bBroken;

/*!
	\class CCommBase
	\brief 功能串口的基类
*/

CCommBase::CCommBase()
{
	m_pDevComm = CDevComm::instance();
	m_Timer.SetName("CommBase");
	m_ReadTimes  =  0;

}

CCommBase::~CCommBase()
{

}

void CCommBase::OnData(void *pdat, int len)
{
	m_ReadTimes++;
	m_bBroken = FALSE;
	return;
}

int CCommBase::Start(CObject * pObj, CDevComm::SIG_DEV_COMM_DATA Proc)
{
	int iRet = 0;

	m_ReadTimes = 0;
	m_ContinuousTime = 0;
	m_iLen = 0;
	m_bBroken = FALSE;

	m_Timer.Start(this, (VD_TIMERPROC)&CCommBase::OnDetectData, 0, 1000);

	if (pObj != NULL)
	{
		iRet = m_pDevComm->Start(pObj, Proc);
	}
	else
	{
		iRet = m_pDevComm->Start(this, (CDevComm::SIG_DEV_COMM_DATA)&CCommBase::OnData);
	}

	//向事件中心注册处理类
	CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CCommBase::onAppEvent);
	return iRet;
}

int CCommBase::Start()
{
	return Start(NULL, 0);
}

int CCommBase::Stop()
{
	m_Timer.Stop();
	//向事件中心注册处理类
	CAppEventManager::instance()->detach(this, (SIG_EVENT::SigProc)&CCommBase::onAppEvent);
	return m_pDevComm->Stop();
}

void CCommBase::OnDetectData(uint arg)
{
	static int oldTimers = 0;

	if (oldTimers != m_ReadTimes)
	{
		oldTimers = m_ReadTimes;
		m_ReadTimes = 0;
		m_ContinuousTime = 0;
	}
	else
	{
		m_ContinuousTime++;
		//1分钟内检测
		if ((oldTimers == 0) && (m_ContinuousTime == m_DisconTime))
		{
			// 认为断了
			OnNoneData();
			m_ContinuousTime = 0;
		}

		if (m_ContinuousTime == 0xffffffff)
		{
			m_ContinuousTime = 0;
		}
	}
}

void CCommBase::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
}


void CCommBase::OnNoneData()
{
	m_bBroken = TRUE;
}

int CCommBase::KeyParse(uchar *pBuffer)
{
	return 0;
}

VD_BOOL CCommBase::GetState()
{
	return m_bBroken == FALSE;
}

