

#include "Functions/Comm.h"
#include "Functions/CommFactory.h"
#include "System/Locales.h"


/*!
	\class CComm
	\brief 串口的管理对象
*/

PATTERN_SINGLETON_IMPLEMENT(CComm);

CComm::CComm():m_sigBuffer(32), m_dwProtocolMask(0)
{
	trace("CComm::CComm()>>>>>>>>>\n");
	
	memset(m_protocol_name_list, 0, COM_TYPES * COM_NAME_LENGTH);
}

CComm::~CComm()
{
	for (int i = 0; i < COM_TYPES; i++)
	{
		if (m_pCommObj[i])
		{
			delete m_pCommObj[i];
		}
	}
}

VD_BOOL CComm::Start()
{
	trace("CComm::Start()>>>>>>>>>\n");

	m_pDevComm = CDevComm::instance();	

	m_ccCfgCom.update();
	//m_caCfgATM.update();
	CreateProduct();

	m_ccCfgCom.attach(this, (TCONFIG_PROC)&CComm::onConfigComm);
	
	//!程序起来的时候需要根据相应的配置开启串口功能
	int iRet = 0;
	onConfigComm(&m_ccCfgCom,iRet);

	VD_PCSTR protocol_name;
	int str_count;
	uint i, j;
	
	/*载入串口协议字符串*/
	protocol_name = LOADSTR("conf_com.comfunction");

	/*解析串口协议字符串*/
	for (i = 0, j = 0, str_count = 0 ; (str_count < COM_TYPES) && (i < strlen(protocol_name)) ; i++)
	{
		if (protocol_name[i] != '|' && protocol_name[i] != '\0')
		{
			m_protocol_name_list[str_count][j] = protocol_name[i];
			
			j++;
		}
		else
		{
			m_protocol_name_list[str_count][j] = '\0';
			str_count++;
			j = 0;
		}
	}

	return TRUE;
}

VD_BOOL CComm::Attach(CObject *pObj, SIG_COM_BUFFER pProc)
{
	return (m_sigBuffer.Attach(pObj, pProc) > 0) ? TRUE : FALSE;
}

/*!
\b Description		:	注销回调函数\n
\b Argument			:	CObject *pObj, SIG_REC_BUFFER pPorc
\param				:
\param				:
\return				

\b Revisions		:	
*/
VD_BOOL CComm::Detach(CObject *pObj, SIG_COM_BUFFER pPorc)
{
	return (m_sigBuffer.Detach(pObj, pPorc) >= 0 ? TRUE : FALSE);
}
/*!
\b Description		:	读取串口配置文件\n

\b Revisions		:	
*/
void CComm::SendAlarm(uint mask, int type)
{
	CAppEventManager::instance()->notify(appEventComm, 1, appEventStart, NULL, NULL);					
	//m_sigBuffer(mask, type);		//Modified by yuxy
}

VD_BOOL CComm::GetCommState()
{
//	return m_pCommObj[m_ccCfgCom.getConfig().iProtocolName]->GetState();
    return m_pCommObj[CONSOLE]->GetState();
}

CCommBase * CComm::GetWorkingComm()
{
//	return m_pCommObj[m_ccCfgCom.getConfig().iProtocolName];
    return m_pCommObj[CONSOLE];
}
/*!
	\b Description		:	在重启前设置一下串口\n
	主要是给视能用

	\b Revisions		:	
*/
void CComm::ResetComm()
{
	COMM_ATTR caCommAttr;
	CONFIG_COMM_X ccCfgCom;

	memset(&caCommAttr, 0, sizeof(COMM_ATTR));
	memset(&ccCfgCom, 0, sizeof(CONFIG_COMM_X));

	ccCfgCom = m_ccCfgCom.getConfig();
	caCommAttr.baudrate	=	(uint)(ccCfgCom.iCommAttri.nBaudBase);
	caCommAttr.databits	=	(uchar)(ccCfgCom.iCommAttri.nDataBits);
	caCommAttr.parity	=	(uchar)(ccCfgCom.iCommAttri.iParity);
	caCommAttr.stopbits	=	(uchar)(ccCfgCom.iCommAttri.iStopBits);

	//设置是否为串口
//	if (ccCfgCom.iProtocolName == CONSOLE)
	{
		m_pDevComm->AsConsole(TRUE);
	}
//	else
//	{
//		m_pDevComm->AsConsole(FALSE);
//	}
	
	//设置串口属性
	m_pDevComm->SetAttribute(&caCommAttr);
}

/*
**接口名称: GetProtocolCount()
**功           能: 获得串口协议个数
**添加日期: 2006.05.08 by JinJ
**返           回: 串口协议的个数, int型
*/
int CComm::GetProtocolCount()
{
	return COM_TYPES;
}

/*
**接口名称: GetProtocolName(int Index, char *pName)
**功           能: 返回指定的串口协议名称
**添加日期: 2006.05.08 by JinJ
**返           回: 成功:返回串口协议名字字符串指针, 失败:返回NULL
*/
char * CComm::GetProtocolName(int Index)
{	
	/*检查Index是否合法*/
	if (Index >= COM_TYPES || Index < 0)
	{
		return NULL;
	}
	else
	{
		return m_protocol_name_list[Index];
	}
}

void CComm::CreateProduct()
{
	m_dwProtocolMask = 0;

	for (int i = 0; i < COM_TYPES; i++)
	{
		m_pCommObj[i] = NULL;	
	}

	CCommFactory comFactory;
	m_pCommObj[CONSOLE] = comFactory.Create(CONSOLE);
	m_dwProtocolMask |= BITMSK(CONSOLE);
	
//	m_pCommObj[VN_KBD] = comFactory.Create(VN_KBD);
//	m_dwProtocolMask |= BITMSK(VN_KBD);
}

/*!
	\b Description		:	函数描述\n
	\b Argument			:	[IN/OUT]参数列表
	\param				:
	\param				:
	\return				

	\b Revisions		:	
*/
uint CComm::GetProtocolMask()
{
	return m_dwProtocolMask;
}

void CComm::onConfigComm(CConfigComm *pConfig, int& ret)
{
	CONFIG_COMM_X &CfgCom = pConfig->getConfig();
	COMM_ATTR caCommAttr;

	memset(&caCommAttr, 0, sizeof(COMM_ATTR));
	caCommAttr.baudrate	=	(uint)(CfgCom.iCommAttri.nBaudBase);
	caCommAttr.databits	=	(uchar)(CfgCom.iCommAttri.nDataBits);
	caCommAttr.parity	=	(uchar)(CfgCom.iCommAttri.iParity);
	caCommAttr.stopbits	=	(uchar)(CfgCom.iCommAttri.iStopBits);

//	if(CfgCom.iProtocolName == CONSOLE)
//	{
		m_pDevComm->AsConsole(TRUE);
//	}
//	else
//	{
//		m_pDevComm->AsConsole(FALSE);
//	}

	m_pDevComm->SetAttribute(&caCommAttr);

    m_pCommObj[CONSOLE]->Start();
    
#if 0
	if (m_pCommObj[CfgCom.iProtocolName])
	{
		trace("CfgCom.iProtocolName: %d\n", CfgCom.iProtocolName);
		m_pCommObj[CfgCom.iProtocolName]->Start();
	}
	else
	{
		trace("the Object isn't exist\n");
	}
#endif

}
