

#include "Functions/ExtRecord.h"
#include "Functions/Record.h"
#include "Functions/General.h"
#include "Functions/DriverManager.h"

#ifdef ENC_ENCODE_MULTI

CExtRecord::CExtRecord(int iChannel)
{
	memset((void *)&m_Attr, 0, sizeof(STM_ATT));
	m_Attr.alrm = F_COMMON;
	m_Attr.type = recFileAV;
	m_Attr.imgq = 68;
	m_Attr.redu = FALSE;

	CAPTURE_DSPINFO dspinfo;
	GetDspInfo(&dspinfo);

	m_iChannel = iChannel / dspinfo.nExpandChannel * dspinfo.nMaxSupportChannel + iChannel % dspinfo.nExpandChannel;

	//取得相关的设备
	m_pDevCapture = CDevCapture::instance(m_iChannel);
		
	//固定保存在N_SYS_CH上开始保存
	m_Attr.chan = N_SYS_CH + iChannel;
	
	uint dwChannel = 0;
	for (int i = 0; i < dspinfo.nMaxSupportChannel; i++)
	{
		dwChannel |= BITMSK(i);
	}

	m_dwChannel = dwChannel << (iChannel * dspinfo.nMaxSupportChannel);

	m_State = FALSE;
}

CExtRecord::~CExtRecord()
{

}

void CExtRecord::setRecLen(uchar length)
{
	if (m_RecLen != length)
	{
		Pack(true);
	}
	m_RecLen = length;
}

void CExtRecord::OnExtRecord(int iChannel, uint dwStreamType, CPacket *pPkt)
{
	//这是拼起来的数据，写入硬盘
	//检验包
	if (!pPkt)
	{
		tracepoint();
		return;
	}
	
	m_RecMutex.Enter();
	pPkt->AddRef();

	if (!m_DHFile.Write(pPkt))
	{
			trace("DHFile Write error!\n");
			m_RecMutex.Leave();
			Stop();
			return;
	}
	
	m_RecMutex.Leave();
}

VD_BOOL CExtRecord::Start()
{
	if (m_State)
	{
		return FALSE;
	}

	if (CheckDisk())
	{
		tracepoint();
		return FALSE;
	}

	CGuard l_Guard(m_RecMutex);

	SystemGetCurrentTime(&m_Attr.time);

	m_DHFile.Create(&m_Attr);

	if (!m_pDevCapture->extStart(this, (CDevCapture::SIG_DEV_CAP_BUFFER)&CExtRecord::OnExtRecord, DATA_RECORD))
	{
		m_DHFile.Close();
		return FALSE;
	}
	
	m_State = TRUE;
	return TRUE;
}

VD_BOOL CExtRecord::Stop()
{
	CGuard	l_cGuard(m_RecMutex);
	if ((!m_State) || (g_Record.GetState() & m_dwChannel))
	{
		return FALSE;
	}
	
	if (!m_pDevCapture->extStop(this, (CDevCapture::SIG_DEV_CAP_BUFFER)&CExtRecord::OnExtRecord, DATA_RECORD))
	{
		return FALSE;
	}

	//关闭录像文件
	m_DHFile.Close();
	
	m_State = FALSE;

	return TRUE;
}

VD_BOOL CExtRecord::Pack(VD_BOOL now)
{
	VD_BOOL bNow = now;
	if (!m_State)
	{
		return false;
	}
	
		if (!bNow)
	{
		SYSTEM_TIME systime;
		SystemGetCurrentTime(&systime);

		int packtime = ABS((systime.hour * 60 + systime.minute) * 60 + systime.second
					- (m_Attr.time.hour * 60 + m_Attr.time.minute) * 60 - m_Attr.time.second);

				//超过1.5倍时间一定打包，
				//超过0.5倍时间考虑对齐打包(如果打包时间在1小时之内，约数对齐，1小时之外，倍数对齐,否则无需对齐)
				if ((packtime >= m_RecLen * 60 * 3 / 2)
					|| (((packtime > m_RecLen * 60 / 2)
					&& (((m_RecLen <= 60) && (60 % m_RecLen == 0) && (systime.minute % m_RecLen == 0))
					|| ((m_RecLen > 60) && (m_RecLen % 60 == 0) && (systime.minute == 0))))
					|| ((60 % m_RecLen != 0) && (m_RecLen % 60 != 0) && (packtime >= m_RecLen * 60))))
				{
					bNow = TRUE;
				}
	}
	
	if (bNow)
	{
		m_DHFile.Close();
		SystemGetCurrentTime(&m_Attr.time);

		m_DHFile.Create(&m_Attr);	
	}
	
	return true;
}	


VD_BOOL CExtRecord::CheckDisk()
{
	uint disk_stat;

	g_DriverManager.IsFull(&disk_stat);

	if (disk_stat == FS_ERROR)
	{
		return 1;
	}
	else if (disk_stat == FS_NO_SPACE)
	{
		if (!CConfigGeneral::getLatest().iOverWrite)
		{
			return 2;
		}
	}

	return 0;
}

#endif
