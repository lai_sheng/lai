#ifndef _AVIDEF_H
#define _AVIDEF_H

typedef struct   
{  
	unsigned int dwMicroSecPerFrame ;   
	unsigned int dwMaxBytesPerSec;  
	unsigned int dwPaddingGranularity; 
	unsigned int dwFlages;  
	unsigned int dwTotalFrame;   
	unsigned int dwInitialFrames;  
	unsigned int dwStreams;  
	unsigned int dwSuggestedBufferSize;  
	unsigned int dwWidth;  
	unsigned int dwHeight;  
	unsigned int dwReserved[4]; 
}VN_MainAVIHeader; 

typedef struct
{
	unsigned int left;
	unsigned int top;
	unsigned int right;
	unsigned int bottom;
}RECT2;

typedef struct   
{ 
	unsigned int fccType;                 
	//auds ????? 
	unsigned int fccHandler;              
	unsigned int dwFlags;                
	unsigned short wPriority;            
	unsigned short wLanguage;             
	unsigned int dwInitalFrames;          
	unsigned int dwScale;                 
	unsigned int dwRate;                  
	unsigned int dwStart;
	unsigned int dwLength;                 
	unsigned int dwSuggestedBufferSize;   
	unsigned int dwQuality;               
	unsigned int dwSampleSize;             
	RECT2 rcFrame;                        
}VN_AVIStreamHeader; 

typedef struct tagVN_BITMAPINFOHEADER 
{ 
	unsigned int biSize; 
	long biWidth; 
	long biHeight; 
	unsigned short biPlanes; 
	unsigned short biBitCount; 
	unsigned int biCompression; 
	unsigned int biSizeImage; 
	int biXPelsPerMeter; 
	int biYPelsPerMeter;
	unsigned int biClrUsed; 
	unsigned int biClrImportant; 
}VN_BITMAPINFOHEADER; 

typedef struct tagVN_RGBQUAD
{
    unsigned char rgbBlue;
    unsigned char rgbGreen;
    unsigned char rgbRed;
    unsigned char rgbReserved;
}VN_RGBQUAD;

typedef struct tagVN_BITMAPINFO 
{ 
	VN_BITMAPINFOHEADER bmiHeader; 
	//VN_RGBQUAD bmiColors[1]; //??? 
}VN_BITMAPINFO; 

typedef struct _VN_AVIINDEX
{ 
	unsigned int ckid;          //表征本数据块的四字符码
	unsigned int dwFlags;       //说明本数据块是不是关键帧、是不是‘rec ’列表等信息
	unsigned int dwChunkOffset; //本数据块在文件中的偏移量
	unsigned int dwChunkLength; //本数据块的大小
}VN_AVIINDEX;

typedef struct   
{ 
	unsigned short wFormatTag;   
	unsigned short nChannels; 
	unsigned int nSamplesPerSec; 
	unsigned int nAvgBytesPerSec; //WAVE
	unsigned short nBlockAlign; 
	unsigned short wBitsPerSameple;
	unsigned short cbSize/*=5*/;
	unsigned char exmess[5];  //fyj 这里为手动添加的额外信息，可有可无，长度大小根据cbSize定义
}VN_WAVEFORMAT;


#endif
