#if defined (_USE_720P_MODULE_)

#include "Functions/Camera.h"
#include "APIs/Video.h"
#include "APIs/System.h"
#include "Functions/Ptz.h"
#include "System/Console.h"

PATTERN_SINGLETON_IMPLEMENT(CCamera);

CCamera::CCamera() 
{

}

CCamera::~CCamera()
{

}

void CCamera::Start()
{
	
	trace("CCamera::Start()>>>>>>>>>>>>>>>>>>>>\n");
	int iRet = 0;
	m_CcfgCamera.update();
	onConfigCamera(&m_CcfgCamera, iRet);
	m_CcfgCamera.attach(this, ((TCONFIG_PROC)&CCamera::onConfigCamera));
#if 0
	m_ConfigCameraExt.update();
	onConfigCameraExt(&m_ConfigCameraExt, iRet);
	m_ConfigCameraExt.attach(this, ((TCONFIG_PROC)&CCamera::onConfigCameraExt));

 	g_Console.registerCmd(CConsole::Proc(&CCamera::OnConsole, this), "cam", "Camera operation!");	
#endif
}

void CCamera::onConfigCamera(CConfigCamera* cConfigCamera, int& ret)
{
	CONFIG_CAMERA& cfg = cConfigCamera->getConfig();
	CAMERA_FORMAT cam_fmt;
	//将CONFIG_CAMERA和CAMERA_FORMAT结构体设计成一样
	//因此可以直接memcpy，修改结构体必须注意
	cam_fmt.HorReverse =cfg.HorReverse;
	cam_fmt.VerReverse=cfg.VerReverse;
	cam_fmt.ExposureType=cfg.ExposureType;
	cam_fmt.AESensitivity=cfg.AESensitivity;
	cam_fmt.Backlight=cfg.Backlight;
	cam_fmt.WhiteBalanceMode=cfg.WhiteBalanceMode;
	cam_fmt.ShadeCorr=cfg.ShadeCorr;
	cam_fmt.BadCorr=cfg.BadCorr;
	cam_fmt.shutter =cfg.shutter;
	cam_fmt.AntiFlicker =cfg.AntiFlicker;
	cam_fmt.ColorTemp=cfg.ColorTemp;
	cam_fmt.YUVCut=cfg.YUVCut;
	cam_fmt.IRCut=cfg.IRCut;
	cam_fmt.PAA=cfg.PAA;
	cam_fmt.Color2WhiteBlack=cfg.Color2WhiteBlack;
	cam_fmt.SensorType=cfg.SensorType;
	cam_fmt.res[0]=cfg.res[0];
	cam_fmt.res[1]=cfg.res[1];
	cam_fmt.res[2]=cfg.res[2];
	cam_fmt.res[3]=cfg.res[3];
	cam_fmt.res[4]=cfg.res[4];
	
	ret = VideoInSetCamera(0, &cam_fmt);

	//VP26M2H色彩转换需要用预置点来控制
#ifndef SDK_3516		
	tracepoint();
	PTZ_OPT_STRUCT strPtzOptStruct;
	strPtzOptStruct.cmd = PTZ_OPT_GOTOPRESET;
	
	if(COLOR_2_WHITEBLACK_COLOR == cam_fmt.Color2WhiteBlack)
	{
		//强制彩色
		strPtzOptStruct.arg1 = 90;	
	}
	else if(COLOR_2_WHITEBLACK_WHITEBLACK == cam_fmt.Color2WhiteBlack)
	{
		//强制黑白
		strPtzOptStruct.arg1 = 91;	
	}
	else if(COLOR_2_WHITEBLACK_AUTO == cam_fmt.Color2WhiteBlack)
	{
		strPtzOptStruct.arg1 = 89;	
	}
	g_Ptz.Start(0, &strPtzOptStruct);
#endif

	return;
	
}

void CCamera::onConfigCameraExt(CConfigCameraExt* cConfigCameraExt, int& ret)
{
	CONFIG_CAMERA_EXT& cfg = cConfigCameraExt->getConfig();
	CAMERA_FORMAT_EXT cam_fmt;
	//将CONFIG_CAMERA和CAMERA_FORMAT结构体设计成一样
	//因此可以直接memcpy，修改结构体必须注意
	memcpy(&cam_fmt, &cfg, sizeof(CAMERA_FORMAT_EXT));
#ifdef SDK_3516	
	ret = VideoInSetCameraExt(0, &cam_fmt);
#endif
	return;
	
}

int CCamera::OnConsole(int argc, char **argv)
{
    char *pszCmd0, *pArg1 = NULL, *pArg2 = NULL,*pArg3 = NULL;
    CConsoleArg arg(argc, argv);

    pszCmd0 = arg.getOptions();

    if (pszCmd0 == NULL || argc <4)
    {
      OnHelp( "help", NULL, NULL );
        return 0;
    }

    if( argc >= 2  )
    {
        pArg1 = arg.GetArg(0);
    }
    if( argc >= 3 )
    {
        pArg2 = arg.GetArg(1);
    }
    if( argc >= 4 )
    {
        pArg3 = arg.GetArg(2);
    }


#ifdef SDK_3516
	SetCameraVal(atoi(pArg1),atoi(pArg2),atoi(pArg3));	
#endif
}
void CCamera::OnHelp( char* opr, char* value1, char* value2)
{
        trace("CCamera set console help!\n");
        trace("cam -s  val1      val2         val3:\n");
        trace("               0  SetExposure        !\n");
        trace("                          0        ExposureType     0  AUTO   1  MANUAL  \n");		
 	 trace("                     ISP_AE_ATTR_S   \n");					
        trace("                          1        enAEMode  0  LOW_NOISE   1   FRAME_RATE \n");	
        trace("                          2        u16ExpTimeMax   \n");		
        trace("                          3        u16ExpTimeMin   \n");		
        trace("                          4        u16DGainMax   \n");	
        trace("                          5        u16DGainMin   \n");			
        trace("                          6        u16AGainMax   \n");		
        trace("                          7        u16AGainMin   \n");		
        trace("                          8        u8ExpStep   \n");	
        trace("                          9        s16ExpTolerance   \n");		
        trace("                          10       u8ExpCompensation   \n");		
 	 trace("                     ISP_ME_ATTR_S   \n");				
        trace("                          11        u32ExpLine   \n");		
        trace("                          12        s32AGain   \n");	
        trace("                          13        s32DGain   \n");	
        trace("                          14        bManualExpLineEnable   \n");		
        trace("                          15        bManualAGainEnable   \n");		
        trace("                          16        bManualDGainEnable   \n");	
		
        trace("               1  SetAWB       !\n");	
        trace("                          0        WBType     0  AUTO   1  MANUAL  \n");		
 	 trace("                     ISP_AWB_ATTR_S   \n");					
        trace("                          1        u8RGStrength  0  LOW_NOISE   1   FRAME_RATE \n");	
        trace("                          2        u8BGStrength   \n");		
        trace("                          3        u8ZoneSel   \n");	
 	 trace("                     ISP_MWB_ATTR_S   \n");				
        trace("                          4        u16Rgain   \n");	
        trace("                          5        u16Bgain   \n");			
		
        trace("               2  SetDefectPixel       !\n");	
        trace("                          0        bEnableStatic     0  AUTO   1  MANUAL  \n");		
        trace("                          1        bEnableDynamic  \n");	
        trace("                          2        bEnableCluster   \n");		
        trace("                          3        bEnableDetect   \n");		
        trace("                          4        u8BadPixelStartThresh   \n");	
        trace("                          5        u8BadPixelFinishThresh   \n");			
        trace("                          6        u16BadPixelCountMax   \n");		
        trace("                          7        u16BadPixelCountMin   \n");		
        trace("                          8        u16BadPixelCount   \n");	
        trace("                          9        u16BadPixelTriggerTime   \n");	
		
        trace("               3  DIS       !\n");	/*数字防抖属性*/
        trace("                          0        enable     0     1    \n");		

        trace("               4  Shading       !\n");	 /*暗角补偿*/
        trace("                          0        enable     0  1   \n");		

        trace("               5  Shading       !\n");	 /*暗角补偿表*/
        trace("                          0        enScale     0:2   1:4   2:8   3:16  \n");		
        trace("                          1        enMesh_R  \n");	
        trace("                          2        enMesh_G   \n");		
        trace("                          3        enMesh_B   \n");		
		
        trace("               6  Denoise       !\n");	 /*定噪点抑制属性*/
        trace("                          0        enable     0:2   1:4   2:8   3:16  \n");		
        trace("                          1        u8ThreshTarget  \n");	

        trace("               7  GAMMA       !\n");	/*定GAMMA属性*/
        trace("                          0        enable     0  1   \n");	

        trace("               8  Antiflicker       !\n");	/*设定抗闪*/
        trace("                          0        enable     0  1   \n");	
        trace("                          1        u8Frequency     0  1   \n");	/*50 表示50hz*/		


        trace("               9  Crosstalk       !\n");		/*Crosstalk Removal 强度*/
        trace("                          0        u8HorStrength     0  0xff   \n");	
        trace("                          1        u8VerStrength     0  0xff   \n");	
		
        trace("               10  AI       !\n");		/*自动光圈的控制属性*/	
        trace("                          0        bIrisEnable     0     \n");	
        trace("                          1        bIrisCalEnable     0     \n");			
        trace("                          2        u32IrisHoldValue     0     \n");	
        trace("                          3        u16IrisStopValue     0     \n");	
        trace("                          4        u16IrisCloseDrive     0     \n");	
        trace("                          5        u16IrisTriggerTime     0     \n");	

        trace("               11   COLORMATRIX       !\n");	
        trace("                          0        u16CorrMatrix[0]     value    \n");			
        trace("                          1        u16CorrMatrix[1]     value    \n");				
        trace("                          8        u16CorrMatrix[8]     value    \n");}
#endif
