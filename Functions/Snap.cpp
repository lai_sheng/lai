

#include "Functions/Snap.h"
#include "Functions/Encode.h"

#include "System/Packet.h"
#include "System/Console.h"
#include "Functions/DriverManager.h"
#include "Net/NetCore.h"
#include "Net/NetWorkService.h"
#include <stdio.h>

#ifndef WIN32
#ifdef    FTP_TRAN_SNAPSHOT
//#include "Net/Dlg/FtpClient.h"
#endif
#endif

#include "Net/Dlg/DlgSmtpCli.h"

#define INIT_SNAP_NUM    0x7fffffff

extern int g_nAlarmIn;
//#define SNAP_DEBUG

void addSnapRef(Snap_Data* pSnap)
{
    CPacket *pPkt = NULL;
    for (int i = 0; i < pSnap->iPktNum; i++)
    {
        pPkt = pSnap->pPkt[i];
        pPkt->AddRef();    
    }
}

void releaseSnapRef(Snap_Data* pSnap)
{
    CPacket *pPkt = NULL;
    // 清空每一个数据包
    for (int i = 0; i < pSnap->iPktNum; i++)
    {
        pPkt = pSnap->pPkt[i];
        pPkt->Release();
    }

    memset(pSnap, 0, sizeof(Snap_Data));
}

CSnapBuffer::CSnapBuffer() : m_pSnapData(NULL), m_iFront(0), m_iRear(0), m_iSize(0)
{

}


CSnapBuffer::~CSnapBuffer()
{

}

void CSnapBuffer::init(int iNum)
{
    m_iSize = iNum + 1;

    // 如果图片数据指针不为NULL, 清空
    if (m_pSnapData)
    {    
        clearData();
        delete[] m_pSnapData;
    }

    // 动态分配图片的内存
    m_pSnapData = new Snap_Data[m_iSize];    
    m_iFront = 0;
    m_iRear = 0;

    // 初始化动态分配的内存空间
    for (int i = 0; i < m_iSize; i++)    
    {
        memset(m_pSnapData + i, 0, sizeof(Snap_Data));        
    }

}

void CSnapBuffer::pushData(CPacket* pPacket, uchar byFlag, DHTIME* pDHTime)
{
    pPacket->AddRef();
    //数据开始,对头和对尾之间至少相差一个空的位置
    if ((m_iRear + 1) % m_iSize == m_iFront)
    {
    	//trace("releaseSnapRef : \n");
        //! 满了，需要释放最老的一张图片
        Snap_Data *pData = m_pSnapData + m_iFront;
        releaseSnapRef(pData);
        memset(pData, 0, sizeof(Snap_Data));

        m_iFront = (m_iFront + 1) % m_iSize;
    }

    Snap_Data *pData = m_pSnapData + m_iRear;
    if( pData->iPktNum >= N_SNAP_PACKET )
    {
#ifndef WIN32    
        ERR_PRINT("error snap ipktnum:%d, m_iRear:%d\n",
                pData->iPktNum, m_iRear);
#endif
    }
    else
    {
        pData->pPkt[pData->iPktNum] = pPacket;
        pData->iPktNum++;
        pData->iLength += pPacket->GetLength();
    }

    //!得到一张图片第一个数据包的系统时间
    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        //取码流里的时间
        timedh2sys(&pData->sys_Createtime, pDHTime);
#if defined(SNAP_DEBUG)
        trace("the pushData %02d:%02d:%02d\n", pData->sys_Createtime.hour, 
                pData->sys_Createtime.minute, pData->sys_Createtime.second);
#endif
    }

    //!数据包的结尾标志
    if ((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        m_iRear = (m_iRear + 1) % m_iSize;
    }
}

/*!
    \b Description        :    从抓图队列里取数据\n
    \b Argument            :    Snap_Data *pSnap, int inum, int iDirection
    \param[in]    inum    :    第几张图片, 以下标１开始
    \param[in]    iDirection    :　取的数据，是由新到旧还是旧到新
    \param[out]    pSnap    :    保存数据的内容
    \return    0：成功；1：空队列２：超出范围            

    \b Revisions        :    
*/
int CSnapBuffer::popData(Snap_Data *pSnap, snapDirection iDirection)
{
    // 指针空或无数据
    if ((!pSnap) || (getSnapNum() == 0))
    {
        return 1;
    }
    
    Snap_Data *pData = NULL;
    switch (iDirection)
    {
    case snapForward:
        pData = m_pSnapData + (m_iSize + m_iRear - 1) % m_iSize;
        m_iRear = (m_iSize + m_iRear - 1) % m_iSize;
        break;
    case snapBackward:
        pData = m_pSnapData + m_iFront;
        m_iFront = (m_iFront + 1) % m_iSize;
        break;
    default:
        return 2;
    }

    *pSnap = *pData;
    addSnapRef(pSnap);
    releaseSnapRef(pData);

    return 0;
}

/*!
    \b Description        :    根据时间（和当前时间比较）取数据\n
    \b Argument            :    Snap_Data* pSnap, int seconds
    \param[in]    seconds    :    当前时间之前的秒数。取值范围为负数或０
    \param[out]    pSnap    :    保存图片内容
    \return                

    \b Revisions        :    
*/
int CSnapBuffer::popData(Snap_Data* pSnap, int seconds)
{
    Snap_Data* pFront = m_pSnapData + m_iFront;
    Snap_Data* pData = NULL;

    if ((!pSnap) || (seconds > 0) || (getSnapNum() == 0))
    {
        return 1;
    }
    
    SYSTEM_TIME currentTime;
    SystemGetCurrentTime(&currentTime);
    
    int t1 = time2second(&currentTime, &pFront->sys_Createtime);

    /*
    int index = m_iFront;

    while (index != m_iRear)
    {
        Snap_Data* ptmp = m_pSnapData + index;
        trace("the pushData %02d:%02d:%02d\n", ptmp->sys_Createtime.hour, 
            ptmp->sys_Createtime.minute, ptmp->sys_Createtime.second);
        index = (index + 1) % m_iSize;
    }
    trace("the num is %d\n", (m_iSize + m_iRear - m_iFront) % m_iSize);
    */
    
    if (ABS(seconds) > t1)
    {
        //找最旧的时间
        trace("the time is too old\n");
        *pSnap = *pFront;
        addSnapRef(pSnap);
        return 0;
    }
    
    if (ABS(seconds) > t1 / 2)  // 说明取的时间靠近保存的旧的数据
    {
        // 从旧数据开始找，目前不释放缓冲区
        int startpos = m_iFront;
        while (startpos != m_iRear)
        {
            pData = m_pSnapData + startpos;    
            //必然找得到
            if (time2second(&currentTime, &pData->sys_Createtime) < ABS(seconds))
            {
                *pSnap = *(m_pSnapData + (m_iSize + startpos - 1) % m_iSize);
                addSnapRef(pSnap);
                return 0;
            }
            startpos = (startpos + 1) % m_iSize;
        }
    }
    else
    {
        // 从新数据开始找，也不释放内存
        int startpos = m_iRear;
        while (startpos != m_iFront)
        {
            startpos = (m_iSize + startpos - 1) % m_iSize;
            pData = m_pSnapData + startpos;
            if (time2second(&currentTime, &pData->sys_Createtime) >= ABS(seconds))
            {
                *pSnap = *pData;
                addSnapRef(pSnap);
                return 0;
            }
        }
    }

    return 3;
}

void CSnapBuffer::clearData()
{
    Snap_Data* pSnap = NULL;
    while (m_iFront != m_iRear)
    {
        pSnap = m_pSnapData + m_iFront % m_iSize;
        releaseSnapRef(pSnap);
        m_iFront = (m_iFront + 1) % m_iSize;
    }
}

int CSnapBuffer::getSnapNum()
{
    return (m_iRear + m_iSize - m_iFront) % m_iSize;
}

/*!
    \b Description        :    构造函数\n
    \b Argument            :    [IN/OUT]参数列表
    \b Revisions        :    
        - 2007-03-29        wangky        Create
*/
CSnap::CSnap():m_SnapMutex(MUTEX_RECURSIVE), m_iSnapState(snapStateStop), m_workMode(snapTriggerOver)
{
    m_iSnapCount = 0;
    m_tmpBuf_len = 0;
	m_TakePicNum = 0;
}

/*!
    \b Description        :    析构函数\n
    \b Argument            :    [IN/OUT]参数列表
    \b Revisions        :    
        - 2007-03-29        wangky        Create
*/
CSnap::~CSnap()
{
    
}

extern int checkDisk(int hddtype = DRIVER_READ_WRITE);

#ifdef SNAP_NET_THREAD_MULTI 
int CSnap::storageHDD(int channel,CPacket *pPacket , uchar byFlag)
{
	PACKET_SNAP packet_snap;
	packet_snap.pPacket = pPacket;
	packet_snap.dwFrameFlag = byFlag;

	if( m_packet_count > 200 )
	{
		debugf("snap list is too large discard it, size:%d!channel %d \n", m_packet_count,channel);
		return -1 ;
	}

	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_snap);
	m_packet_count++;
	m_mutex_list.Leave();

	return 0;
}
bool CSnap::DosStorage()
{

	int i, ret = 0;
	CPacket* pPacket = NULL;
	uint dwframflag = 0;

	if( m_packet_count > 0 )
	{
		m_mutex_list.Enter();
		PACKET_LIST_SNAP::iterator it;

		it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwframflag = it->dwFrameFlag;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();

		if( pPacket != NULL )
		{
			storageHDD(pPacket, dwframflag);
			pPacket->Release();
		}

		return true;
	}

	return false;
}
#endif
int CSnap::storageHDD(CPacket *pPacket, uchar byFlag)
{
    int ret;
	CPacket *pPkt = pPacket;

    pPkt->AddRef(); 
	
    if(m_iCount > 5)
    {
        if (checkDisk(DRIVER_SNAPSHOT))
        {
        	pPkt->Release();
            return 1;
        }
        m_iCount = 0;
    }
   

    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        m_iCount++;
        
     	if(m_File.Open(IMAGE) == false)
        {
       		printf("open error\n");
            pPkt->Release();
            return 2;
        }

/*//同时开启多路抓拍，此处会导致图像卡
        //保证时间正确先写1K数据进去
        pPkt = g_PacketManager.GetPacket(MAX(pPacket->GetLength(), 1024));
        pPkt->SetLength(1024);
        memset(pPkt->GetBuffer(), 0xff, pPkt->GetLength());

        uint pos = m_DHFile.GetPosition();
        
        m_DHFile.Write(pPkt->GetBuffer(), pPkt->GetLength());
        pPkt->Release();
        m_DHFile.Seek(pos, CDHFile::begin);
        pPkt = pPacket;
*/    
    }

    if( (m_tmpBuf_len + pPacket->GetLength() ) >= 512*1024  )
    {
        ret = m_File.Write(m_tmpBuf, m_tmpBuf_len);
        m_tmpBuf_len = 0;
        if( ret < 0 )
        {
            m_File.Close();
	
            pPkt->Release();
            return 3;
        }
    }
//printf("%d\n",m_tmpBuf_len + pPacket->GetLength());

    if( (m_tmpBuf_len + pPacket->GetLength() ) >= 512*1024 )
    {
        debugf("dwCount:%d\n", pPacket->GetLength() );
    }
    assert( (m_tmpBuf_len + pPacket->GetLength() ) < DHSNAP_BUF_SIZE );
    memcpy(m_tmpBuf+m_tmpBuf_len, pPacket->GetBuffer(), pPacket->GetLength() );
    m_tmpBuf_len += pPacket->GetLength();
//printf("%d\n",m_tmpBuf_len);
    if ((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        if( m_tmpBuf_len > 0 )
        {
            m_File.Write(m_tmpBuf, m_tmpBuf_len);
            m_tmpBuf_len = 0;
        }
        m_File.Close();
		m_TakePicNum ++;
    }
    pPkt->Release();
    return 0;
}

/*!
    \b Description        :    初始化函数，抓图一些通道数循环队列等参数的初始化\n
    \b Argument            :    int iNum, int iChannel
    \param    iNum        :    图片的张数
    \param    iChannel    :    通道数

    \b Revisions        :    
        - 2007-04-03        wangky        Create
*/
void CSnap::init(int iNum, int iChannel)
{
    m_iChannel = iChannel;
    m_Buffer.init(iNum);
    //设置录像文件属性
    memset((void *)&m_Attr, 0, sizeof(STM_ATT));
    m_Attr.chan = m_iChannel;
    m_Attr.type |= 0x02;//视频录像
    m_Attr.redu = FALSE;   
    m_Attr.alrm = F_COMMON;
    m_Attr.driver_type = DRIVER_SNAPSHOT;

    m_CurrentAttr.chan = m_iChannel;
    m_CurrentAttr.type |= 0x02;//视频录像
    m_CurrentAttr.redu = FALSE;   
    m_CurrentAttr.alrm = F_COMMON;
    m_CurrentAttr.driver_type = DRIVER_SNAPSHOT;
}

extern void ReleasePacket(const struct tagMsgHdr *msg) ;

int cnt =0;
int CSnap::SendPic(CPacket *pPacket,uchar byFlag,uchar byType,DHTIME *pDHTIME)
{
	conn * p_tmp_con = NULL;
       msghdr_t *msg = NULL;

	   cnt++;
	for(int i= 0; i < C_MAX_TCP_CONN_NUM; i ++)
	{
		p_tmp_con = g_Net.GetConn(i);
		if( NULL != p_tmp_con )
		{
			if( (p_tmp_con->bUpPicFlag == TRUE && byType == F_HAND)
			     || (p_tmp_con->iUpPicMsk & BITMSK(m_iChannel)))	
			{
			       if(byFlag == PACK_CONTAIN_FRAME_HEAD
					||byFlag == PACK_CONTAIN_FRAME_HEADTRAIL)
			    	{
					p_tmp_con->bSend = TRUE;
					cnt =1;
				}	
			}
			
			if(p_tmp_con->bSend == TRUE) 
			{
				pPacket->AddRef();

				DVRIP ip;
				msghdr_t msghdr[2] ;
				memset(&ip, 0, sizeof(ip));
				memset(&msghdr, 0, sizeof(msghdr));

				ip.dvrip_cmd= ACK_MANUL_SNAP;
				ip.dvrip_extlen= pPacket->GetLength();
				
				ip.dvrip_p[0]   = m_iChannel;
				ip.dvrip_p[1]   = 0x00; 
				ip.dvrip_p[2] = 0x40;
				//!合入帧头尾标志信息
				ip.dvrip_p[2] = (ip.dvrip_p[2] | byFlag);
				ip.dvrip_p[3] = 0xff;//!按帧发送时，每个包都只有一帧
				ip.dvrip_p[4] = byType;
				ip.dvrip_p[5] = cnt;
				ip.dvrip_p[16]  = CHL_JPEG_T+4;


				if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
				{    
					memcpy(&ip.dvrip_p[20],pDHTIME,4);   
				}

				msghdr[0].msgCmd   = ACK_MANUL_SNAP;
				msghdr[0].chn      = m_iChannel;
				msghdr[0].buf      = &ip;
				msghdr[0].buf_len  = sizeof(ip);
				msghdr[0].msg_flags= MSG_FLAGS_MORE ;
				msghdr[0].next     = &msghdr[1];

				msghdr[1].msgCmd   = ACK_MANUL_SNAP;
				msghdr[1].chn      = m_iChannel;
				msghdr[1].buf      = pPacket->GetBuffer();
				msghdr[1].buf_len  = pPacket->GetLength();
				
				msghdr[1].msg_flags= 0;
				msghdr[1].callback = ReleasePacket;
				msghdr[1].context  = pPacket;
				msghdr[1].iFrameFlag = PKT_FULL_FRAME;
			
		            MutexEnter(p_tmp_con->hMutex);    

		            for(msg = msghdr;msg; msg = msg->next) 
		            {
		                NetInsertDataToQueue(p_tmp_con, msg);
		            }
								
		            MutexLeave(p_tmp_con->hMutex);
			    if(p_tmp_con->DataTransferQueue[m_iChannel+ 1].totalSize != 0)
			    {
			        NetDvrSendQueue(p_tmp_con,m_iChannel,ACK_MANUL_SNAP);
			    }    

			    NetDvrSendOtherQueue(p_tmp_con,m_iChannel,ACK_MANUL_SNAP);		


			    if(byFlag == PACK_CONTAIN_FRAME_TRAIL
					||byFlag == PACK_CONTAIN_FRAME_HEADTRAIL)
				{
					p_tmp_con->bUpPicFlag = FALSE;
					p_tmp_con->bSend = FALSE;
				}

				//增加等待时间,否则会导致send 过快 网络发送失败问题
                        	SystemSleep(10); 

				
			}
		}

	}
	return 0;

}

int CSnap::SaveSnapByDir(CPacket *pPacket, uchar byFlag, char *PathName)
{
	int ret;	
	SYSTEM_TIME pTime;
	char FileName[64] = "";
	FILE *fp = NULL;
	FILE *Cmdfp = NULL;
	char Cmd[64] = "";
	
	if(pPacket == NULL || PathName == NULL)
	{
		printf("#######SaveSnapByDir error attr!!!\n");
		return -1;
	}

    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
		if(access((const char *)PathName, F_OK) != 0)
		{
			printf("#######Can't find Directory,will create it!\n");
			if(mkdir((const char *)PathName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
			{
				printf("#######Can't create Directory!!!\n");
            	return -1;
			}
			printf("########Create Directory Success!!\n");
		}

		sprintf(Cmd,"rm %s/*.jpg",PathName);
		Cmdfp = popen(Cmd,"r");
		if(Cmdfp == NULL)
		{
			printf("######%s %s Failure!!\n",__FUNCTION__,Cmd);
		}

		if(Cmdfp != NULL)
		{
			pclose(Cmdfp);
		}
		
        SystemGetCurrentTime(&pTime);

		sprintf(FileName,"%s/MD_%4d%02d%02d%02d%02d%02d.jpg",
						PathName,pTime.year,pTime.month,
						pTime.day,pTime.hour,pTime.minute,pTime.second);
	
		printf("###########%s Create FileName:%s\n",__FUNCTION__,FileName);
		fp = fopen(FileName,"wb");
		if(fp < 0)
		{
			printf("%s fopen failure!!!\n",__FUNCTION__);
            return -1;
		}
    }

	printf("##########len:%d buff:%p flag:%d\n",pPacket->GetLength(),pPacket->GetBuffer(),byFlag);
    if (((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL)) && fp != NULL)
    {
		printf("33333333333333333333\n");
		fwrite((char *)pPacket->GetBuffer(),pPacket->GetLength(),1,fp);
		printf("111111111111111111111\n");
        fclose(fp);
		printf("222222222222222222222\n");
    }
	else if(byFlag == PACK_CONTAIN_FRAME_NONHT && fp != NULL)
	{
		fwrite((char *)pPacket->GetBuffer(),pPacket->GetLength(),1,fp);
	}
	
	return 0;
}


/*!
    \b Description        :    通过监视图像的数据获取图片的数据包，将其放入对应通道的环形队列\n
        如果是定时抓图，则按抓图间隔时间存放，如果是边缘触发，则传一张图片就结束，如果是整个时期上传，则立即存放
    \b Argument        :int iChannel, uint dwStreamType, CPacket *pPacket 
    \param[in]     iChannel            : 通道数 
    \param[in]    dwStreamType    : 码流模式
    \param[in]    *pPacket         :数据包指针
    \param[out]            :            

    \b Revisions        :    
        - 2007-04-02        wangky        Create
*/

void CSnap::OnSnap(int iChannel, uint dwStreamType, CPacket *pPacket)
{
    //! 判断是否为抓图的辅码流
    if ((dwStreamType != CHL_JPEG_T) || (!pPacket))
    {
        return;
    }

    CGuard l_Guard(m_SnapMutex);    

    CPacket* pPkt = pPacket;
    PKT_HEAD_INFO    *pkthead = (PKT_HEAD_INFO *)(pPkt->GetHeader());
    uchar byFlag = (uchar)pkthead->FrameInfo[0].FrameFlag;

    DHTIME dhtime;
    bool Intervideoenable = false;
    
    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        /* 新的一张图，更新文件属性 */
        memcpy(&m_CurrentAttr,&m_Attr,sizeof(STM_ATT));
        
        //取码流里的时间
        getFirstFrameTime(pPkt, &dhtime, PACK_TYPE_FRAME_JEPG);

        //!一张图片的第一个数据包的前16个字节去掉
        pPkt = g_PacketManager.GetPacket(pPacket->GetLength());
        if( NULL == pPkt )
        {
            errorf("alloc packet failed!\n");
            return;
        }  
#if 0
        //去掉数据前的12字节(8空字节+ 4 DHTIME)
        pPkt->SetLength(pPacket->GetLength() - 20);
     	memcpy(pPkt->GetHeader(), pPacket->GetHeader(), PKT_HDR_SIZE);
        memcpy(pPkt->GetBuffer(), pPacket->GetBuffer() + 20, pPkt->GetLength());
#else
		pPkt->SetLength(pPacket->GetLength() - 16);
      	memcpy(pPkt->GetHeader(), pPacket->GetHeader(), PKT_HDR_SIZE);
        memcpy(pPkt->GetBuffer(), pPacket->GetBuffer() + 16, pPkt->GetLength());
#endif 
    }

	//printf("pushData:%4d%02d%02d%02d%02d%02d\n",
	//		dhtime.year,dhtime.month,dhtime.day,dhtime.hour,dhtime.minute,dhtime.second);
	
    m_Buffer.pushData(pPkt, byFlag, &dhtime);
    if (m_workMode != snapTriggerOver)
    {    
        if(checkDisk(DRIVER_SNAPSHOT) == 0)
        {
			//printf("1111111111111111111111111===byFlag:%d\n",byFlag);
            int ret = storageHDD(pPkt, byFlag);
            if(ret !=0)
            {
                tracef("wpl storageHDD PIC error %d\n ",ret);
            }
        }        
    }

    if ((byFlag == PACK_CONTAIN_FRAME_HEAD) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        pPkt->Release();
    }

    bool bSnapCountOver = false;
    if ((byFlag == PACK_CONTAIN_FRAME_TRAIL) || (byFlag == PACK_CONTAIN_FRAME_HEADTRAIL))
    {
        int iCount = 0;
        if (m_workMode == snapTriggerEdgeALM
            ||m_workMode == snapTriggerEdgeDYN
            ||m_workMode == snapTriggerManual)
        {
            //说明已经存储成功
            g_Snap.stopSnap(snapTriggerEdgeALM, m_iChannel);
            g_Snap.stopSnap(snapTriggerEdgeDYN, m_iChannel);
	        g_Snap.stopSnap(snapTriggerManual, m_iChannel);	
            m_iSnapCount++; //统计抓拍张数
            iCount= CConfigSnap::getLatest(iChannel).SnapCount;

            if (m_iSnapCount >= iCount)
            {
                /*存储完数据后都回复到默认的普通抓拍*/
                printf("m_iSnapCount = %d iCount = %d\r\n",m_iSnapCount,iCount);
                m_Attr.alrm = F_COMMON;
                m_iSnapCount = 0;
                bSnapCountOver = true;
                m_workMode = snapTriggerTimer;
            }
        }
        else
        {
            /* 定时抓拍时不需要统计张数 */
            bSnapCountOver  = true;
        }

        /* 目前的方式定时抓拍停止时，还会再抓一张才结束 */
        CGuard l_Guard(m_SnapRecMutex);
        if (m_iSnapState == snapStateReadyStop &&(bSnapCountOver) )
        {
            /* 目前处理方式，在联动时开启定时抓图可能无法开启，这里暂时再加
               一个判断 */
            if (CConfigSnap::getLatest(m_iChannel).Enable)
            {
                m_iSnapState = snapStateStart;
                m_iSnapCount = 0;
                return;
            }
            
            if (CDevCapture::instance(m_iChannel)->Stop(this, 
                (CDevCapture::SIG_DEV_CAP_BUFFER)&CSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))
            {
                m_iSnapState = snapStateStop;
                m_workMode = snapTriggerOver;
                m_iSnapCount = 0;
            }
        }
    }
}

//开始抓图
VD_BOOL CSnap::start(snapTriggerMode snapMode)
{
    CGuard l_Guard(m_SnapRecMutex);
    m_workMode = snapMode;
    switch(snapMode)
    {
        case snapTriggerManual:
            m_Attr.alrm = F_HAND;
	     break;		
        case snapTriggerTimer:
            m_Attr.alrm = F_COMMON;
            break;
        case snapTriggerEdgeALM:
            m_Attr.alrm = F_ALERT;
            break;
        case snapTriggerEdgeDYN:
            m_Attr.alrm = F_DYNAMIC;
            break;
        default:
            m_Attr.alrm = F_COMMON;
    }    
    if (m_iSnapState == snapStateStop)
    {
        m_iSnapCount = 0;
        if (CDevCapture::instance(m_iChannel)->Start(this, 
            (CDevCapture::SIG_DEV_CAP_BUFFER)&CSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))
        {            
            m_iSnapState = snapStateStart;
            //清一下残留的缓冲区
            m_Buffer.clearData();
        }
    }
    else if(snapMode == snapTriggerEdgeALM 
        ||snapMode == snapTriggerManual
        || snapMode == snapTriggerEdgeDYN )
    {
        CDevCapture::instance(m_iChannel)->Start(CHL_JPEG_T);
    }
    
    return m_iSnapState == snapStateStart;
}

/*!
    \b Description        :    停止抓图.为了保证一张完整的图片，只能在写数据部分停止\n
    \b Argument            :    [IN/OUT]参数列表
    \return         成功返回 TRUE，不成功返回FALSE        
*/
VD_BOOL CSnap::stop()
{
    CGuard    l_cGuard(m_SnapRecMutex);
    
    if (m_iSnapState == snapStateStart)
    {
        m_iSnapState = snapStateReadyStop;
    }

    return m_iSnapState == snapStateReadyStop || m_iSnapState == snapStateStop;
}

// 此函数立即关闭抓拍
VD_BOOL CSnap::stopSnap()
{
    CGuard l_Guard(m_SnapRecMutex);
    if (m_iSnapState == snapStateReadyStop || m_iSnapState == snapStateStart)
    {
		m_File.Close();

        if (CDevCapture::instance(m_iChannel)->Stop(this, 
            (CDevCapture::SIG_DEV_CAP_BUFFER)&CSnap::OnSnap, DATA_MONITOR, CHL_JPEG_T))
        {
            m_iSnapState = snapStateStop;
            m_workMode = snapTriggerOver;
            m_iSnapCount = 0;
        }
    }
    return 0;
}

uchar CSnap::getState()
{
    CGuard    l_cGuard(m_SnapRecMutex);
    return m_iSnapState != snapStateStop;
}


/*!
    \b Description        :    从内存中取出图片\n
    \b Argument            :    int iChannel，int iNum
    \param iChannel        :    通道数
    \param iNum            :    第几张图片
    \return            成功返回 0，否则返回 1    

    \b Revisions        :    
        - 2007-04-03        wangky        Create
*/
int CSnap::getSnap(Snap_Data *pSnap, CSnapBuffer::snapDirection iDirection)
{
    CGuard l_Guard(m_SnapMutex);
    return m_Buffer.popData(pSnap, iDirection);
}

int CSnap::getSnap(Snap_Data *pSnap, int seconds)
{
    //使用二分法查找，
    //如果找不到所需要的时间，则如果时间靠近最新，则使用最新时间，否则用旧一点的时间
    CGuard l_Guard(m_SnapMutex);
    return m_Buffer.popData(pSnap, seconds);
}

PATTERN_SINGLETON_IMPLEMENT(CSnapManager);

CSnapManager::CSnapManager():
#ifdef SNAP_NET_THREAD_MULTI //asdf
CThread("SnapManager", 20, g_nLogicNum),
#endif
m_iInterval(0)
{
    for (int i = 0; i < g_nCapture; i++)
    {
        m_pSnap[i] = new CSnap();
#if defined(SNAP_REQUIRE_ISTC)
        m_pSnap[i]->init(20,  i);
#elif defined(_USE_720P_MODULE_)
        m_pSnap[i]->init(2,  i);
#else
        m_pSnap[i]->init(5,  i);
#endif
        m_iSnapType[i] = 0;
    }

    m_bSendPicEmailEn =  false;
}

CSnapManager::~CSnapManager()
{
//zanglei added for avoiding memory leak!	
    for (int i = 0; i < g_nCapture; i++)
    {
    	delete m_pSnap[i];
    }
//zanglei ended
}

void CSnapManager::start()
{
    CAPTURE_EXT_STREAM stream;
    GetExtCaps(&stream);
    if (stream.ExtraStream & BITMSK(CHL_JPEG_T))
    {
        m_cCfgSnap.update();
		SynSnapCfg(); 
        m_cCfgSnap.attach(this, (TCONFIG_PROC)&CSnapManager::onConfigSnap);

        //向事件中心注册处理类
        CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CSnapManager::onAppEvent);
    }
#ifdef SNAP_NET_THREAD_MULTI
	CreateThread();
#endif

    g_Console.registerCmd (CConsole::Proc( &CSnapManager::onSnap, this ), "snap", "to snap!");//注册控制台关于网路的操作函数
}

void CSnapManager::stop()
{
    CAPTURE_EXT_STREAM stream;
    GetExtCaps(&stream);
    if (stream.ExtraStream & BITMSK(CHL_JPEG_T))
    {
        //向事件中心注册处理类
        CAppEventManager::instance()->detach(this, (SIG_EVENT::SigProc)&CSnapManager::onAppEvent);

        m_cCfgSnap.detach(this, (TCONFIG_PROC)&CSnapManager::onConfigSnap);
        
        for (int i = 0; i < g_nCapture; i++)
        {
            m_pSnap[i]->stopSnap();
        }
    }
}

void CSnapManager::SynSnapCfg()
{
        for(int iIndex = 0;iIndex< g_nCapture;iIndex++)
        {
            CONFIG_SNAP& cfgNew = m_cCfgSnap.getConfig(iIndex);
            AppConfigSnap(&cfgNew,iIndex);    
        }   
}

int CSnapManager::onSnap(int argc, char **argv)
{
#ifndef ENC_SUPPORT_SNAPSHOT
    tracef("NOT enable ENC_SUPPORT_SNAPSHOT");
    return 0;
#endif

    char *pszCmd0;
    char * pArg1;
    char * pArg2;

    int chn = -1;
    CConsoleArg arg(argc, argv);
    pszCmd0 = arg.getOptions();//获得命令选项
    if(pszCmd0==NULL)//解决用户只输入net命令时，程序崩溃的问题
    {
        trace("snap command:\n");
        trace("snap  -s <chn>: Snap chn\n");
        trace("snap  -t <chn> <1/0>: set timersnap enable chn\n");

        return 0;
    }

    pArg1  = arg.GetArg(0);
    
    chn = atoi(pArg1);
    if(chn < 0 || chn >= g_nCapture)
    {
        trace("Snap chn %derror\n",chn);
        return 0;
    }

    switch(pszCmd0[0])
    {
        case 's':  
        {
            startSnap(CSnap::snapTriggerManual,chn);
            stopSnap(CSnap::snapTriggerManual,chn);
            break;
        }
        case 't':
        {
            pArg2  = arg.GetArg(1);
            bool enable = FALSE;
            if(pArg2)
            {
                enable = atoi(pArg2);
            }
            CConfigSnap* pCfgSnap = new CConfigSnap();
            pCfgSnap->update();

            if(enable == 1)
            {
                pCfgSnap->getConfig(chn).Enable = TRUE;
            }else
            {
                pCfgSnap->getConfig(chn).Enable = FALSE;
            }

            pCfgSnap->commit();
            delete pCfgSnap;            

        }
        
        default:
            break;
        
    }

    return 0;

}


void CSnapManager::startSnap(int snapType, int iChannel)
{
    if ((snapType > CSnap::snapTriggerManual) || (snapType < CSnap::snapTriggerTimer))
    {
        trace("Input snapType error no this type[%d]!\n", snapType);
        return;
    }

    for (int i = CSnap::snapTriggerManual; i > snapType; i--)
    {
        if (m_iSnapType[iChannel] & BITMSK(i))
        {
            // 当前已经有更高优先级的抓图, 那么低优先级的直接返回
            trace("The high priority has occured. Input type[%d]\n", snapType);
            return;
        }
    }
    
    if (m_pSnap[iChannel]->start((CSnap::snapTriggerMode)snapType))
    {
        m_iSnapType[iChannel] |= BITMSK(snapType);
    }
    
    return;    
}

void CSnapManager::stopSnap(int snapType, int iChannel)
{    
    uchar ucSnapType= m_iSnapType[iChannel];
    if(ucSnapType &= BITMSK(snapType))
    {
        m_iSnapType[iChannel] &= ~BITMSK(snapType);
    }else
    {
        return ;
    }

    if (m_iSnapType[iChannel] == 0)
    {
        m_pSnap[iChannel]->stop();
    }
}

void CSnapManager::startSnap(appEventCode appcode, int index, int iChannel, void *pData)
{
    //if (index >= MIN(g_nAlarmIn, g_nCapture))
    if (iChannel >= ICaptureManager::instance()->GetAnalogChnNum())
    {
        tracepoint();
        return;
    }

    //首先设置一下抓图的编码，触发方式
    if(appEventVideoMotion == appcode 
	   ||appEventVideoLoss == appcode
	   || appEventVideoBlind == appcode
	   || appEventVideoCrossline == appcode)
    {
        startSnap(CSnap::snapTriggerEdgeDYN, iChannel);
    }else
    {
        startSnap(CSnap::snapTriggerEdgeALM,iChannel);
    }    
}

void CSnapManager::stopSnap(appEventCode appcode, int index, int iChannel)
{
    if(appEventVideoMotion == appcode
	   ||appEventVideoLoss == appcode
	   || appEventVideoBlind == appcode
	   || appEventVideoCrossline == appcode)
    {
        stopSnap(CSnap::snapTriggerEdgeDYN, iChannel);
    }else
    {
        stopSnap(CSnap::snapTriggerEdgeALM,iChannel);
    }
}
void CSnapManager::take_picture(int iChannel/* = 0*/)
{
	CGuard l_cGuard(m_PictureMutex);
	int i = 0;
	m_pSnap[iChannel]->m_TakePicNum = 0;
	startSnap(CSnap::snapTriggerManual,iChannel);
	while(m_pSnap[iChannel]->m_TakePicNum <= 0 && i < 150) //1.5s内还没抓到图就退出
	{
		i ++;
		usleep(10 * 1000);
	}
	stopSnap(CSnap::snapTriggerManual,iChannel);

}
	
#ifdef SNAP_DEBUG
FILE *pSnapFile;
#endif

void sendSnapData(Snap_Data* pData, char* name)
{
#ifdef SNAP_DEBUG    
    static char snapName[128];
    VD_BOOL bSuccess = FALSE;

    if (memcmp(snapName, name, strlen(name)) != 0)
    {
        char filename[256];
        sprintf(filename, "/home/debug/snap/%s", name);
        trace("the filename is %s\n", filename);
        if ((pSnapFile = fopen(filename, "a+b")) != NULL)
        {
            fseek(pSnapFile, 0, SEEK_END);
            bSuccess = TRUE;
        }
        trace("here is bSuccess :%d\n", bSuccess);
        strcpy(snapName, name);
    }
#endif    

    for (int i = 0; i < pData->iPktNum; i++)
    {
#if 0
#ifdef FTP_TRAN_SNAPSHOT
        int ImgFlag = 0;
        if(i == (pData->iPktNum -1))
        {
            ImgFlag = ImgFlag|BITMSK(0);
        }

        if(i == 0)
        {
            ImgFlag = ImgFlag|BITMSK(2);
        }
        g_FtpCli_Snap.OnSnap( pData->pPkt[i], name, ImgFlag );
#endif
#endif

#ifdef ALM_REQUIRE_SNAP_EMAIL 
        if(g_Snap.isSendPicEmail())
        {
            //发送数据到email部分
            g_SmtpClient.OnSnap(pData->pPkt[i], name, pData->iLength);
        }
#endif

#ifdef SNAP_DEBUG
        if (bSuccess)
        {
            fwrite(pData->pPkt[i]->GetBuffer(), sizeof(uchar), pData->pPkt[i]->GetLength(), pSnapFile);
        }
#endif
        pData->pPkt[i]->Release();
    }

#ifdef SNAP_DEBUG
    if (bSuccess)
    {
        fclose(pSnapFile);
    }
#endif
}

void sendData(Snap_Data *pData, uchar type, int iChannel)
{
    char cSnapName[128];
    sprintf(cSnapName,"ch%.2d_%.4d%.2d%.2d_%.2d%.2d%.2d_%c.jpg", iChannel + 1, pData->sys_Createtime.year, pData->sys_Createtime.month, 
        pData->sys_Createtime.day, pData->sys_Createtime.hour, pData->sys_Createtime.minute, 
        pData->sys_Createtime.second, type);

    sendSnapData(pData, cSnapName);
}


#ifdef SNAP_NET_THREAD_MULTI

void CSnapManager::ThreadProc()
{
	bool bSleep = true;

	while( m_bLoop )
	{
		bSleep = true;
		for (int iChn = 0; iChn < g_nCapture; iChn++)
		{
		
			if( m_pSnap[iChn]->DosStorage())
			{
				bSleep = false;
			}
		}

		//!是否需要休眠
		if( bSleep )
		{
			SystemSleep(1000);

		}

	}
}
#endif
void CSnapManager::sendPolicy(Snap_Group *pGroup, int iChannel)
{
    CSnap *pSnap = m_pSnap[iChannel];

    Snap_Data tmpData;

    //发一张预录，一张触发图片
    if (pGroup->iTimes == 0)
    {
        if (pSnap->getSnap(&tmpData, -1) == 0)
        {
            sendData(&tmpData, 'P', iChannel);
        }        
        pGroup->iTimes++;
    }

    if (pSnap->getSnap(&tmpData, 0) == 0)
    {
        
        sendData(&tmpData, 'E', iChannel);
        
        
    }
}


#if defined(SNAP_REQUIRE_ISTC)
void sendISTCData(Snap_Data *pData, int iChannel, uchar type, int index)
{
    char cSnapName[128];

    sprintf(cSnapName,"%s001%.2d%.2d%.2d%.2d%c.I%06d.jpg", CConfigGeneral::getLatest().chMachineName, iChannel + 1, 
        pData->sys_Createtime.hour, pData->sys_Createtime.minute, pData->sys_Createtime.second, type, index);
    sendSnapData(pData, cSnapName);
}

void CSnapManager::sendISTCPolicy(Snap_Group *pGroup, int iChannel)
{
    CSnap *pSnap = m_pSnap[iChannel];

    Snap_Data tmpData;
    if (pGroup->iTimes < 60)
    {
        pGroup->iTimes++;    
        if (pSnap->getSnap(&tmpData, -60) == 0)
        {
            sendISTCData(&tmpData, iChannel, 'A', ++pGroup->counts);
        }
    }                
    else //第一分钟后,并且报警不结束,发送两张相同的图片
    {
        if (pSnap->getSnap(&tmpData, -1) == 0)
        {    
            sendISTCData(&tmpData, iChannel, 'D', ++pGroup->counts);
        }
    }

    if (pSnap->getSnap(&tmpData, 0) == 0)
    {
        sendISTCData(&tmpData, iChannel, 'D', ++pGroup->counts);
    }
}
#endif

void CSnapManager::AppConfigSnap(CONFIG_SNAP *pConfigSnap,int iChn)
{
    int iQuality = 0;
    switch(pConfigSnap->SnapQuality)  
    {
        case 6:
            iQuality = 0;
            break;
        case 5:
            iQuality = 1;
            break;
        case 4:
            iQuality = 2;
            break;                    
        case 3:
            iQuality = 3;
            break;
        case 2:
            iQuality = 4;
            break;
        case 1:
            iQuality = 5;
            break;
        default:
            iQuality = 2;
            
    }
#ifdef LINUX
    CAPTURE_FORMAT cformat;
    memset( &cformat, 0, sizeof(cformat) );
    cformat.ImageQuality = iQuality;
    printf ("[%10s][%d][%s]i:%d, SnapInterval:%d\n",__FILE__, __LINE__, __func__, iChn, pConfigSnap->SnapInterval);
	if(pConfigSnap->SnapInterval > 1000*120)
	{
		int tmp = pConfigSnap->SnapInterval/1000;
		cformat.FramesPerMinute = tmp/60;
		cformat.FramesPerSecond = tmp%60;
	}	
    else if( pConfigSnap->SnapInterval > 1000 )
    {
        cformat.FramesPerSecond = (128+pConfigSnap->SnapInterval/1000);
    }
    else
    {
        cformat.FramesPerSecond = 1000/pConfigSnap->SnapInterval;
    }

    CDevCapture::instance(iChn)->SetFormat(&cformat, CHL_JPEG_T);
	/*LE0 不管用户如何设置 定时抓图都打开 20150203*/ //修改回原来方案:modify by jwd on 20160119
    if(pConfigSnap->Enable)
    {
        startSnap(CSnap::snapTriggerTimer, iChn);
    }
    else
    {
        stopSnap(CSnap::snapTriggerTimer, iChn);
    }
	
	/*_printd("LE0 set snapTriggerTimer =====================================\n");
	startSnap(CSnap::snapTriggerTimer, iChn);*/
#endif
}
void CSnapManager::onConfigSnap(CConfigSnap& cCfgSnap, int& iRet)
{    
    for(int i = 0;i< g_nCapture;i++)
    {
        CONFIG_SNAP& cfgNew = cCfgSnap.getConfig(i);
        CONFIG_SNAP& cfgOld = m_cCfgSnap.getConfig(i);

        if(cfgNew.SnapQuality!= cfgOld.SnapQuality||cfgNew.Enable !=cfgOld.Enable
            || cfgNew.SnapInterval != cfgOld.SnapInterval)
        {        
            AppConfigSnap(&cfgNew,i);
        }

        cfgOld = cfgNew;        
    }
    
}

void CSnapManager::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* pData)
{
    if (param == NULL)
    {
        return;
    }
    if(appEventVideoMotion != code &&appEventVideoBlind != code 
        &&appEventVideoLoss != code &&appEventAlarmLocal != code
        &&appEventVideoCrossline !=code)
    {
        return ;
    }
    
    if ((appEventUpgrade == code)
        && (NULL != pData) && ((*pData)["State"].asString() == "Preparing"))
    {
        infof("CSnapManager::onApp.\n");
        stop();
        return;
    }

    int i = 0;
    VD_BOOL bNeedSetOSD = FALSE;
    if (appEventComm == code)
    {
        bNeedSetOSD = TRUE;
    }

    switch(action) 
    {
    case appEventStart:
        //trace("appEventStart >>>>>>>>>>>>>>>>>>>>\n");
        for (i = 0; i < g_nCapture; i++)
        {
            if (param->bSnapEn && param->dwSnapShot & BITMSK(i))
            {                
                if (bNeedSetOSD)
                {
                    g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, TRUE, (char *)pData, i);
                }

#ifdef ALM_REQUIRE_SNAP_EMAIL
                if (param->bMail)
                {
                    m_bSendPicEmailEn= true;
                }
#endif
                startSnap(code, index, i, NULL);
            }
            else
            {
                if (bNeedSetOSD)
                {
                    g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, FALSE, (char *)pData, i);
                }

#ifdef ALM_REQUIRE_SNAP_EMAIL
                m_bSendPicEmailEn= false;
#endif
                stopSnap(code, index, i);                
            }
        }
        break;
#ifdef ALM_REQUIRE_SNAP_EMAIL
    case appEventStop:
        for (i = 0; i < g_nCapture; i++)
        {
            //延迟2S确保图片数据能够获取到
            CAppEventManager::instance()->latchEvent(this, (SIG_EVENT::SigProc)&CSnapManager::onAppEvent, code, index, 2);
        }
        break;
    case appEventLatch:
        for(i = 0; i < g_nCapture; i++)
        {
            if (param->dwSnapShot & BITMSK(i))
            {

                if (bNeedSetOSD)
                {
                    g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, FALSE, (char *)pData, i);
                }


#ifdef ALM_REQUIRE_SNAP_EMAIL
                m_bSendPicEmailEn= false;
#endif
                stopSnap(code, index, i);
            }
        }
        break;
#else
    case appEventStop:
        //trace("appEventStop >>>>>>>>>>>>>>>>>>>>\n");
        for(i = 0; i < g_nCapture; i++)
        {
            if (param->dwSnapShot & BITMSK(i))
            {
                
                if (bNeedSetOSD)
                {
                    g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, FALSE, (char *)pData, i);
                }

                stopSnap(code, index, i);
            }
        }
        break;
#endif
    default:
        trace(">>>>>>>>>>>>>>>appEventUnknown\n");
        break;
    }
}

bool CSnapManager::isSendPicEmail()
{
    return m_bSendPicEmailEn;
}
int CSnapManager::getSnapDataByDir(Snap_Data *pSnap,int iChn,CSnapBuffer::snapDirection iDir)
{
    if (iChn < 0 || iChn >= g_nCapture)
    {
        return -1;
    }

    if (NULL != pSnap)
    {
        return m_pSnap[iChn]->getSnap(pSnap, iDir);
    }

    return -1;
}

int CSnapManager::storageHDD(int channel, CPacket *pPacket, uchar byFlag, bool force)
{
    int iRet = 0;
#ifdef SNAP_NET_THREAD_MULTI
	 iRet = m_pSnap[channel]->storageHDD(channel , pPacket,  byFlag);
#else		
     iRet = m_pSnap[channel]->storageHDD(pPacket, byFlag);
#endif
    //printf("Channel [%d] storageHDD ----------------------\n", channel);
    return iRet;
}

