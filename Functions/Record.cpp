

#include <assert.h>

#include "Functions/Record.h"
#include "Functions/Encode.h"
#include "Devices/DevFrontboard.h"
#include "Devices/DevVideo.h"
#include "System/AppConfig.h"
#include "System/Log.h"
#include "Functions/DriverManager.h"
#include "Rtsp/BaseFunc/VDDebug.h"
#include "System/Support.h"
#include "Devices/DevExternal.h"

#ifndef WIN32
#ifdef	FUNC_FTP_UPLOAD
//#include "Net/Dlg/FtpClient.h"
#endif
#endif

#include "Functions/FFFile.h"
#include "Devices/DevCapBuf.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/LiveCam/RealTimeCamInterface.h"

static char record_flag = 0;
int g_RecordStatus = 0;                           //录像状态


int checkDisk(int hddtype = DRIVER_READ_WRITE);

const int consDHFileHead = 64 * 1024;
const int consFrameLength = 16 * 1024;
const int consMotionLength = 16 * 1024;
const int consCardLength = 8 * 1024;

void setDHFileAttr(STM_ATT* pAttr, const MEDIA_FORMAT& format)
{
	//由于画质的概念取消，被码流大小取代，同时图像的帧率也是从1-25所有的帧率可选，在回放的时候，1500/1700
	//已经实现了帧率自适应，因此此处的参数可以不用配置
	/*
	if ((format.vfFormat.nFPS <= 30) && (format.vfFormat.iQuality > 0) && (format.vfFormat.iQuality <= 6))
	{
		int imgq = ((format.vfFormat.nFPS << 4) & 0xf0) | (format.vfFormat.iQuality & 0x0f);
		pAttr->imgq = BYTE(imgq);
	}
	else
	{
		pAttr->imgq = BYTE(((3 << 4) & 0xf0) | (4 & 0x0f));
	}
	*/
	pAttr->type = 0;

	if (format.bAudioEnable)
	{
		pAttr->type |= recFileAudio;
	}

	if (format.bVideoEnable)
	{
		pAttr->type |= recFileVideo;
	}
}

/*
目前正常录像由以下几部分组成：
１）头DHFILEHEAD，标识了整个头的长度（在结构里iTotalLen标识），本身64字节。
２）紧接着是索引信息。分别是I帧索引信息，动检索引信息，卡号索引信息，具体内容见结构定义
３）然后是真实数据部分
*/

VD_IStorage::VD_IStorage(void* p)
{

}

VD_IStorage::~VD_IStorage()
{

}

CDHStorage::CDHStorage(void *pAttr, int iChannel, enum capture_channel_t type) 
: VD_IStorage(pAttr), 
	m_iChannel(iChannel), m_pAttr((STM_ATT *)pAttr), m_pHeadPacket(NULL)
{
	//这儿值先暂定
	m_pHeadPacket = CPacketManager::instance()->GetPacket(consDHFileHead);
	memset(m_pHeadPacket->GetBuffer(), 0, consDHFileHead);
	m_pHeadPacket->SetLength(consDHFileHead);
	
	DHFILEHEAD			DHFileHead;
	memset(&DHFileHead, 0, sizeof(DHFILEHEAD));
	memcpy(&DHFileHead.DHHead, "DHII", 4);
	DHFileHead.iTotalLen = consDHFileHead;
	DHFileHead.FrameIndex.iStartPos = sizeof(DHFILEHEAD);
	DHFileHead.MotionIndex.iStartPos = DHFileHead.FrameIndex.iStartPos + consFrameLength;
	DHFileHead.CardIndex.iStartPos = DHFileHead.MotionIndex.iStartPos + consMotionLength;

	memcpy(m_pHeadPacket->GetBuffer(), &DHFileHead, sizeof(DHFILEHEAD));

	m_tmpBuf_len = 0;
    m_RecordType = type;
#ifdef RECORD_NET_THREAD_MULTI
	m_pg_totoal_len = 0;
	m_packet_group.count = 0;

#endif
}

CDHStorage::~CDHStorage()
{
	m_pHeadPacket->Release();
}

VD_BOOL CDHStorage::createStorage()
{
	CGuard l_cGuard(m_StorageMutex);
#ifndef IPC_JZ	
	//为了解决异步打包时，文件属性还没有更新的问题
	if( m_RecordType == CHL_MAIN_T )
	{
	    setDHFileAttr(m_pAttr, CConfigEncode::getLatest(m_iChannel).dstMainFmt[g_Encode.getEncodeType(m_iChannel)]);
	}
	else
	{
	    setDHFileAttr(m_pAttr, CConfigEncode::getLatest(m_iChannel).dstExtraFmt[m_RecordType-1]);
	}
#endif
	m_File.almtype = m_pAttr->alrm;
	
	return m_File.Open();
}

VD_BOOL CDHStorage::closeStorage()
{
	CGuard l_cGuard(m_StorageMutex);
	
#ifdef RECORD_NET_THREAD_MULTI
	if( m_pg_totoal_len > 0 )
	{
		m_File.WriteGroup(&m_packet_group);
		m_pg_totoal_len = 0;
		m_packet_group.count = 0;

	}
#else
	if( m_tmpBuf_len > 0 )
	{
		m_File.Write(m_tmpBuf, m_tmpBuf_len);
		m_tmpBuf_len = 0;
	}
#endif

#ifndef REC_SUPPORT_HEAD
	if (m_pAttr->alrm == F_CARD)
#endif
	{
		// 数据头再写一次
		if (m_File.GetPosition() != 0)
		{
			m_File.Seek(0,SEEK_SET);
			m_File.Write(m_pHeadPacket->GetBuffer(),m_pHeadPacket->GetLength(), FALSE);
		}
	}
	return m_File.Close();
}


VD_BOOL CDHStorage::writeStorage(void *pBuffer, uint dwCount)
{
	CGuard l_cGuard(m_StorageMutex);

	if (m_File.GetPosition() == 0)
	{
		//说明是第一次，为了快速产生文件，写入头文件,不要头的填1K的0xFF（对齐）, 需要头的直接写头
		if (m_pAttr->alrm == F_CARD)
		{
			m_File.Write(m_pHeadPacket->GetBuffer(),m_pHeadPacket->GetLength());
		}
		else
		{
			CPacket *pPkt = g_PacketManager.GetPacket(1024);

			if (pPkt)
			{
				pPkt->SetLength(1024);
				memset(pPkt->GetBuffer(), 0xff, 1024);
				m_File.Write(pPkt->GetBuffer(),1024, FALSE);

				pPkt->Release();
				// 这一段是为了产生文件用，实际文件里不需要
				m_File.Seek(0,SEEK_SET);
			}
			else
			{
				//pPkt->Release(); //pPkt=NULL
				return FALSE;
			}	
		}
	}

	//!数字通道可能会过来的帧比较小，会影响存储效率，上游如果组包又可能
	//!会影响解码实时性
#if 1
	dwCount = m_File.Write(pBuffer, dwCount);

#else
	if( (m_tmpBuf_len + dwCount) >= DHSTORE_BUF_SIZE  )
	{
		m_cDHFile.Write(m_tmpBuf, m_tmpBuf_len);
		m_tmpBuf_len = 0;
	}

	if( m_tmpBuf_len >= DHSTORE_BUF_SIZE )
	{
		m_cDHFile.Write(pBuffer, dwCount);
		m_tmpBuf_len = 0;
	}
	else
	{
		memcpy(m_tmpBuf+m_tmpBuf_len, pBuffer, dwCount);
		m_tmpBuf_len += dwCount;
	}

#endif
	if(dwCount < 0)
		return FALSE;
	
	return dwCount;
}

#ifdef RECORD_NET_THREAD_MULTI
VD_BOOL CDHStorage::writeStorage(CPacket *pPacket, uint dwCount, uint start_pos)
{
	CGuard l_cGuard(m_StorageMutex);
#if 0
	if (m_File.GetPosition() < 0)
	{
		printf("m_File.GetPosition()\n");
		return VD_FALSE;
	}
#endif
	assert(m_packet_group.count < PACKET_GROUP_MAX_COUNT );

	pPacket->AddRef();
	m_packet_group.pPacketRecord[m_packet_group.count].pPacket = pPacket;
	m_packet_group.pPacketRecord[m_packet_group.count].dwStartPos = start_pos;
	m_packet_group.pPacketRecord[m_packet_group.count].dwCount = pPacket->GetLength() - start_pos;//dwCount;
	m_pg_totoal_len += (pPacket->GetLength() - start_pos);

	m_packet_group.count++;

	if( m_pg_totoal_len >= DHSTORE_BUF_SIZE
		|| m_packet_group.count >= PACKET_GROUP_MAX_COUNT )
	{	
	#ifdef IPC_JZ  //临时修改:因快速启动带来的影响，SD卡挂载速度慢，要在写小录像的同时检测是否能写SD卡
		if(m_File.IsOpened() == VD_FALSE)
			m_File.Open();
	#endif
	
		//!数据已达到要求，要写进去
		m_File.WriteGroup(&m_packet_group);
		m_pg_totoal_len = 0;
		m_packet_group.count = 0;	
	}
		

	return VD_TRUE;
}

#endif

uint CDHStorage::getLength()
{
	CGuard l_cGuard(m_StorageMutex);
	uint dwLen = m_File.GetPosition();
	if (dwLen == 0)
	{
		dwLen += consDHFileHead;
	}
	return dwLen;
}



VD_BOOL CDHStorage::setInfo(void* pContext, int iLength, storageInfoTypes type)
{
	CGuard l_cGuard(m_StorageMutex);

	uchar * pData = NULL;
	DHINFORGROUP *pIndex = NULL;
	int iTotalLen = 0;

	DHFILEHEAD* pHead = (DHFILEHEAD *)m_pHeadPacket->GetBuffer();

	switch (type)
	{
	case storageInfoCard:
		pIndex = &pHead->CardIndex;
		iTotalLen = consCardLength;		
		break;
	case storageInfoMotion:
		pIndex = &pHead->MotionIndex;
		iTotalLen = consMotionLength;
		break;
	case storageInfoFrame:
		pIndex = &pHead->FrameIndex;
		iTotalLen = consFrameLength;
		break;
	default:
		return FALSE;
	}	
	
	if (pIndex->iLength + iLength > iTotalLen)
	{
		tracepoint();
		return FALSE;
	}
	pIndex->bUsed = TRUE;
	pData = m_pHeadPacket->GetBuffer() + pIndex->iStartPos + pIndex->iLength;

	memcpy(pData, pContext, iLength);
	pIndex->iLength += iLength;
	

	if (pIndex->iLength % 256 == 0)
	{
		//把所有卡号信息写在文件头区域
		uint dhfilelen = m_File.GetPosition();
		m_File.Seek(0,SEEK_SET);

		if (!m_File.Write(m_pHeadPacket->GetBuffer(), m_pHeadPacket->GetLength()))		
		{
			trace("Fail to write info message!!!\n");
			return FALSE;
		}

		m_File.Seek(dhfilelen,SEEK_SET);

	}

	return TRUE;
}

/*!
	检查磁盘，如果磁盘满且未设置为覆盖模式，则返回1
	返回0表示成功，磁盘可用
	返回非0表示磁盘不可用
*/
int checkDisk(int hddtype)
{
	if(ext_rec == PIRREC_ON)  //让短录像执行下去 ,没SD卡时保存SPI上
		return 0;

	if(hddtype == DRIVER_CHECK_MEM && file_sys_is_full() == true)
		return 1;
	
	if(	file_sys_get_linkstat()	< 0 )
		return 1;

	return 0;
}

CRecordStorage::CRecordStorage(int iChannel, enum capture_channel_t type, STM_ATT * pAttr, CMutex* pMutex) 
: VD_IStorage(NULL), m_iChannel(iChannel), 
m_pDhfsStorage(NULL), m_pRemoteStorage(NULL), m_pDirectStorage(NULL)
,m_RecordType(type),m_DataType(type==CHL_MAIN_T?DATA_RECORD:DATA_2SND)
,m_bPacketType(packetNone), m_pMutex(pMutex)
{
	//m_pDevCapture = CDevCapture::instance(m_iChannel);	
	m_pDevCapture 	= ICapture::instance(m_iChannel);
	m_pDhfsStorage 	= new CDHStorage(pAttr, m_iChannel, type);	

	m_framerate		= 0;
#ifdef RECORD_NET_THREAD_MULTI
	m_packet_count = 0;
	m_packet_audio_count = 0;
#endif

}

CRecordStorage::~CRecordStorage()
{
	if (m_pDhfsStorage)
	{
		delete m_pDhfsStorage;
	}
}

VD_BOOL CRecordStorage::createStorage()
{
	return m_pDhfsStorage->createStorage();
}

VD_BOOL CRecordStorage::closeStorage()
{
	return m_pDhfsStorage->closeStorage();
}

VD_BOOL CRecordStorage::writeStorage(void *pBuffer, uint dwCount)
{
	//trace("\r\n##########write data len:%d############\r\n",dwCount);
	
#if 0
	static CDFile fpMain;
	if (!fpMain.IsValid())
	{
		fpMain.Open("main.dav", "w");
	}
	fpMain.Write((unsigned char *)pBuffer, dwCount);
#endif
	return m_pDhfsStorage->writeStorage(pBuffer, dwCount);
}

int hdd_write_cnt=0;
#ifdef RECORD_NET_THREAD_MULTI

VD_BOOL CRecordStorage::writePacket(CPacket *pPacket, uint dwCount, uint start_pos)
{
	hdd_write_cnt++;//added by chenzj
	return m_pDhfsStorage->writeStorage(pPacket, dwCount, start_pos);
}
#endif

uint CRecordStorage::getLength()
{
	return m_pDhfsStorage->getLength();
}

VD_BOOL CRecordStorage::setInfo(void* pContext, int iLength, storageInfoTypes type)
{
	return m_pDhfsStorage->setInfo(pContext, iLength, type);
}



bool operator==(const VD_SIZE &size1, const VD_SIZE &size2)
{
	return (size1.w == size2.w) && (size1.h == size2.h);
}
bool operator!=(const VD_SIZE &size1, const VD_SIZE &size2)
{
	return (size1.w != size2.w) || (size1.h != size2.h);
}
#ifdef RECORD_NET_THREAD_MULTI
void CRecordStorage::onRecord(int iChannel, uint dwStreamType, CPacket *pPacket)
{
	if( dwStreamType != m_RecordType)
	{
		trace("CRecordStorage::onRecord-m_RecordType:%d, dwStreamType:%d\n", m_RecordType, dwStreamType);
		return;	
	}

	if((m_framerate <= 0 && m_packet_count > 15*6) ||
	   (m_framerate > 0  && m_packet_count > m_framerate*6))
	{
		debugf("record list is too large discard it, size:%d, chn:%d!\n", m_packet_count, iChannel);
		return ;
	}
	PACKET_RECORD packet_record;
	packet_record.pPacket = pPacket;
	packet_record.dwStreamType = dwStreamType;
	
	m_mutex_list.Enter();
	pPacket->AddRef();
	m_packet_list.push_back(packet_record);
	m_packet_count++;
	m_mutex_list.Leave();
}
VD_INT32 CRecordStorage::OnAudioData(int iChannel, CPacket *pPacket)
{

	if( 0 != m_RecordType)
	{
		printf("m_RecordType:%d\n",m_RecordType);
		return FALSE;	
	}
	//16k 平均 15.625帧 一秒
	if(m_packet_audio_count > 15.625 * 6)
	{
		debugf("audio record list is too large discard it, size:%d, chn:%d!\n", m_packet_audio_count, iChannel);
		return FALSE;
	}

	PACKET_RECORD packet_record;
	packet_record.pPacket = pPacket;
	packet_record.dwStreamType = 0;

	m_mutex_audio.Enter();
	pPacket->AddRef();
	m_packet_audio_list.push_back(packet_record);
	m_packet_audio_count++;
	m_mutex_audio.Leave();
	return TRUE;
}
void CRecordStorage::SaveRecord(int iChannel, uint dwStreamType, CPacket *pPacket)
{
	//检验包
	if (!pPacket)
	{
		tracepoint();
		return;
	}
	if( dwStreamType != m_RecordType)
	{
		tracepoint();
		return;	
	}	
	if (checkDisk())
	{
		//stop
		g_Record.StopRec(g_Record.GetRecMode(iChannel), iChannel);
		return;
	}

	VD_BOOL bError = FALSE;
	int firstIFramePos = -1;
	int VideoFramePos  = 0;
	int frame_rate 	= 0;

	pPacket->AddRef();


	uchar* head = pPacket->GetBuffer();
		
	if(head[0] != 0 || head[1] != 0 || head[2] != 1 )
	{
		pPacket->Release();
		return;
	}

#if 1	
	if(!record_flag)
	{
		if(head[3] == 0xfd || head[3] == 0xad) //I帧
		{
#ifdef IPC_HISI
			char unit_type[5];
			int  n; //I帧的私有头长度
			if(head[3] == 0xad) n = 20; 
			else 			 n = 16;
			
			memcpy(unit_type,pPacket->GetBuffer()+n+VideoFramePos,5);
			if(7 != (unit_type[4]&0x1f) && 8 != (unit_type[4]&0x1f) && 5 != (unit_type[4]&0x1f))//IDR帧 理论上5才表示IDR帧,
			{																 //但上来的数据的第5个字节相与后为7可以代表IDR帧
				pPacket->Release();
				return ;
			}
			else
				record_flag = 1;
#else
			record_flag = 1;
#endif
		}
		else
		{
			pPacket->Release();
			return ;			
		}
	}
#endif
	if(head[3] == 0xfd || head[3] == 0xad)
	{
		firstIFramePos = findFirstFrame(pPacket);
		
		frame_rate = head[5];
		if(0 == m_framerate){
			m_framerate = frame_rate;
		}
		//_printd("frame_rate:%d",frame_rate);
	
	}
	else
	{
		firstIFramePos = -1;
	}
	// 帧头数据结构完整，拷贝数据防止地址不对齐
	int iFrameLength = 0;

	if (firstIFramePos >= 0)
	{
		VD_SIZE configSize;
		capture_size_t size;
		int	tmpIFramePos   = firstIFramePos;
		uint imageSizePara = dwStreamType;
		
		if( ICapture::instance(iChannel)->GetCaptureType() == ENUM_CHANNEL_TYPE_DATA)
		{
			imageSizePara = (uint)(pPacket->GetBuffer() + firstIFramePos);
		}

		ICapture::instance(iChannel)->getImageSize(&size, imageSizePara);
		/*
		if(0 == iChannel || 16 == iChannel)
		{
			printf("iChannel <%d> dwStreamType <%d> size <%d> firstIFramePos <%d>\n",iChannel, dwStreamType, size, firstIFramePos); 
		}
		*/
		GetImageSize(&configSize, size);

		// 设置分辨率后，如果下层传数据相当快，上层的m_bPacketType还没设置好，就会导致多打一个小文件
		m_pMutex->Enter();
	
		//SVAC IPC 720P分辨率为1280*704,而GetImageSize回来的是1280*720
		if ((m_imgSize.w == 1280) && (m_imgSize.h == 704))
		{
			configSize.h = configSize.h - 16;
		}		
		//SVAC IPC 1080P分辨率为1920*1072,而GetImageSize回来的是1920*1080
		else if ((m_imgSize.w == 1920) && (m_imgSize.h == 1072))
		{
			configSize.h = configSize.h - 8;
		}
	
		// 只要判断分辨率即可。因为分辨率设置好的话，m_bPacketType也已经设置好了
		if ((configSize != m_imgSize) || (m_bPacketType == packetNone))
		{
			VD_SIZE old = m_imgSize;
			getImageSizeFromPacket(pPacket, &m_imgSize);
			if ((old == m_imgSize) || (old.w == 0) || (old.h == 0) )
			{
				//分辨率还没有切换，继续等待
				firstIFramePos = -1;
			}
			
			if (firstIFramePos >= 0)
			{
				trace("ch[%d] old(w=%d,h=%d), new(w=%d,h=%d), dev(w=%d,h=%d), m_bPacketType = %d\n", iChannel, 
				old.w, old.h,m_imgSize.w,m_imgSize.h,configSize.w, configSize.h, m_bPacketType);
			}
		}
		m_pMutex->Leave();

	//帧率不同就强制打包
		if(firstIFramePos < 0 && m_framerate != frame_rate){
			firstIFramePos = tmpIFramePos;
		}

		m_framerate = frame_rate;
	}
	/*
	if (getPackFlag(pPacket) == PACK_FLAG_BREAK)
	{
		//切换分辨率引起的打包
		firstIFramePos = findFirstFrame(pPacket);
	}
	else
	{
		if (m_bPacketType != packetNone)
		{
			firstIFramePos = findFirstFrame(pPacket);
		}
	}
	*/

	if (firstIFramePos >= 0)
	{
	#ifdef IPC_JZ
		if (firstIFramePos >= (0 + VideoFramePos))
	#else
		if (firstIFramePos > (0 + VideoFramePos))
	#endif
		{
			bError = !writePacket(pPacket, firstIFramePos, 0 + VideoFramePos);//!writeStorage(pPacket->GetBuffer(), firstIFramePos);
		}

		if (bError == FALSE)
		{
			bError = (!closeStorage())  || (!createStorage());
			
			if (bError == FALSE)
			{
				VD_BOOL bContinued = TRUE;
				if (m_bPacketType == packetAdvanced)
				{
					//调整一下预录数据，如果成功的话，就不需要写剩下的数据，否则继续执行写数据
					//if (m_pDevCapture->adjustPreBuffer())
					if (ICapture::instance(m_iChannel)->adjustPreBuffer())
					{
						// 复位一下,防止在分析数据时发现和预录中数据不一致，又打包
						m_imgSize.w = 0;
						m_imgSize.h = 0;
						bContinued = FALSE;
					}
				}

				if (bContinued == TRUE)
				{
					// 512字节对齐
					if (firstIFramePos % 512)
					{
						int iIFrameLen = pPacket->GetLength() - firstIFramePos;
						CPacket* tmpPkt = g_PacketManager.GetPacket(iIFrameLen);
						if (tmpPkt)
						{
							tmpPkt->SetLength(iIFrameLen);
							memcpy(tmpPkt->GetBuffer(), pPacket->GetBuffer() + firstIFramePos, iIFrameLen);
							bError = !writePacket(tmpPkt, iIFrameLen, 0);//!writeStorage(tmpPkt->GetBuffer(), iIFrameLen);
							tmpPkt->Release();
						}
					}
					else
					{
						bError = !writePacket(pPacket, pPacket->GetLength() - firstIFramePos, firstIFramePos);
								//!writeStorage(pPacket->GetBuffer()
								//+ firstIFramePos, pPacket->GetLength() - firstIFramePos);
					}
				}
			}
		}
		m_bPacketType = packetNone;
	}
	else
	{
		bError = !writePacket(pPacket, pPacket->GetLength(), 0 + VideoFramePos);//!writeStorage(pPacket->GetBuffer(), pPacket->GetLength());
	}

	pPacket->Release();

	if(m_bPacketType == packetForce &&
			ICapture::instance(iChannel)->GetCaptureType() == ENUM_CHANNEL_TYPE_DATA)
	{
		pack(packetForce);
	}

	if (bError)
	{
		//stop
		g_Record.StopRec(g_Record.GetRecMode(iChannel), iChannel);
	}

	//trace("bError:%d, firstIFramePos:%d\n", bError, firstIFramePos);
	//trace("m_bPacketType:%d\n", m_bPacketType);
}

bool CRecordStorage::DoPreview()
{
	int ret = 0;
	CPacket* pPacket = NULL;
	uint dwStreamType = 0;

	if( m_packet_count > 0 )
	{
		m_mutex_list.Enter();
		PACKET_LIST_RECORD::iterator it;

		it = m_packet_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();

		if( pPacket != NULL )
		{
			SaveRecord(m_iChannel, dwStreamType, pPacket);
			pPacket->Release();

			pPacket = NULL;
		}
		ret = 1;

	}

	if(m_packet_audio_count > 0)
	{
		m_mutex_audio.Enter();
		PACKET_LIST_RECORD::iterator it;

		it = m_packet_audio_list.begin();
		pPacket = it->pPacket;
		dwStreamType = it->dwStreamType;
		m_packet_audio_list.pop_front();
		m_packet_audio_count--;
		m_mutex_audio.Leave();

		if( pPacket != NULL )
		{
			SaveRecord(m_iChannel, dwStreamType, pPacket);
			pPacket->Release();

			pPacket = NULL;
		}		
		ret = 1;
	}
		

	if(ret == 1)
		return true;
	
	return false;
}

#else
void CRecordStorage::onRecord(int iChannel, uint dwStreamType, CPacket *pPacket)
{
	if (!pPacket)
	{
		tracepoint();
		return;
	}

	if( dwStreamType != m_RecordType)
	{
		tracepoint();
		return;	
	}
#if 0
	static int count = 0;
	if (count++ % 25 == 0)
	{
        printf("--%d %d %d--\n", iChannel, dwStreamType, pPacket->GetLength());
	}
#endif
	//检验包
	
#if 0
	static CDFile fpMain;
	if (!fpMain.IsValid())
	{
		fpMain.Open("main.dav", "w");
	}
	fpMain.Write((unsigned char *)pPacket->GetBuffer(), pPacket->GetLength());
#endif
	pPacket->AddRef();

	if (checkDisk())
	{
		pPacket->Release();
		g_Record.StopRec(g_Record.GetRecMode(iChannel), iChannel);
		return;
	}

	VD_BOOL bError = FALSE;
	int firstIFramePos = -1;
	bool bPack = false; //!是否进行打包

	

	if( ICapture::instance(iChannel)->GetCaptureSreamType() == ENUM_STREAM_TYPE_8K )
	{
		firstIFramePos = findFirstFrame(pPacket);
	}
	else //!ENUM_STREAM_TYPE_Stream
	{
		firstIFramePos = -1;
	}
		
	// 帧头数据结构完整，拷贝数据防止地址不对齐
	int iFrameLength = 0;

	if (firstIFramePos >= 0)
	{
		VD_SIZE configSize;
		capture_size_t size;

		uint imageSizePara = dwStreamType;
		if( ICapture::instance(iChannel)->GetCaptureType() == ENUM_CHANNEL_TYPE_DATA)
		{
			imageSizePara = (uint)(pPacket->GetBuffer() + firstIFramePos);
		}

		ICapture::instance(iChannel)->getImageSize(&size, imageSizePara);
		/*
		if(0 == iChannel || 16 == iChannel)
		{
			printf("iChannel <%d> dwStreamType <%d> size <%d> firstIFramePos <%d>\n",iChannel, dwStreamType, size, firstIFramePos); 
		}
		*/
		GetImageSize(&configSize, size);

		// 设置分辨率后，如果下层传数据相当快，上层的m_bPacketType还没设置好，就会导致多打一个小文件
		m_pMutex->Enter();
		// 只要判断分辨率即可。因为分辨率设置好的话，m_bPacketType也已经设置好了
	
		//SVAC IPC 720P分辨率为1280*704,而GetImageSize回来的是1280*720
		if ((m_imgSize.w == 1280) && (m_imgSize.h == 704))
		{
			configSize.h = configSize.h - 16;
		}		
		//SVAC IPC 1080P分辨率为1920*1072,而GetImageSize回来的是1920*1080
		else if ((m_imgSize.w == 1920) && (m_imgSize.h == 1072))
		{
			configSize.h = configSize.h - 8;
		}
		
		if ((configSize != m_imgSize) || (m_bPacketType == packetNone))
		{
			VD_SIZE old = m_imgSize;
			getImageSizeFromPacket(pPacket, &m_imgSize);
			if ((old == m_imgSize) || (old.w == 0) || (old.h == 0))
			{
				//分辨率还没有切换，继续等待
				firstIFramePos = -1;
			}
			else
			{
				bPack = true;
			}
			
			if ( bPack )
			{
				trace("onRecord ch[%d] old(w=%d,h=%d), new(w=%d,h=%d), dev(w=%d,h=%d), m_bPacketType = %d\n",
					iChannel, old.w, old.h,m_imgSize.w,m_imgSize.h,configSize.w, configSize.h, m_bPacketType);
			}
		}
		else if ((m_bPacketType == packetAuto) || (m_bPacketType == packetAdvanced))
		{
			bPack = true;
		}		

		m_pMutex->Leave();
	}
	/*
	if (getPackFlag(pPacket) == PACK_FLAG_BREAK)
	{
		//切换分辨率引起的打包
		firstIFramePos = findFirstFrame(pPacket);
	}
	else
	{
		if (m_bPacketType != packetNone)
		{
			firstIFramePos = findFirstFrame(pPacket);
		}
	}
	*/

	if (bPack)
	{
		if (firstIFramePos > 0)
		{
			bError = !writeStorage(pPacket->GetBuffer(), firstIFramePos);
		}

		if (bError == FALSE)
		{
			bError = (!closeStorage())  || (!createStorage());
			
			if (bError == FALSE)
			{
				VD_BOOL bContinued = TRUE;
				if (m_bPacketType == packetAdvanced)
				{
					//调整一下预录数据，如果成功的话，就不需要写剩下的数据，否则继续执行写数据
					//if (m_pDevCapture->adjustPreBuffer())
					if (ICapture::instance(m_iChannel)->adjustPreBuffer())
					{
						// 复位一下,防止在分析数据时发现和预录中数据不一致，又打包
						m_imgSize.w = 0;
						m_imgSize.h = 0;
						bContinued = FALSE;
					}
				}

				if (bContinued == TRUE)
				{
					// 512字节对齐
					if (firstIFramePos % 512)
					{
						int iIFrameLen = pPacket->GetLength() - firstIFramePos;
						CPacket* tmpPkt = g_PacketManager.GetPacket(iIFrameLen);
						if (tmpPkt)
						{
							tmpPkt->SetLength(iIFrameLen);
							memcpy(tmpPkt->GetBuffer(), pPacket->GetBuffer() + firstIFramePos, iIFrameLen);
							bError = !writeStorage(tmpPkt->GetBuffer(), iIFrameLen);
							tmpPkt->Release();
						}
					}
					else
					{
						bError = !writeStorage(pPacket->GetBuffer()
								+ firstIFramePos, pPacket->GetLength() - firstIFramePos);
					}
				}
			}
		}
		m_bPacketType = packetNone;
	}
	else
	{
		bError = !writeStorage(pPacket->GetBuffer(), pPacket->GetLength());
	}
	
	pPacket->Release();

	if(m_bPacketType == packetForce &&
			ICapture::instance(iChannel)->GetCaptureType() == ENUM_CHANNEL_TYPE_DATA)
	{
		pack(packetForce);
	}

	if (bError)
	{
		//stop
		g_Record.StopRec(g_Record.GetRecMode(iChannel), iChannel);
	}

	//trace("bError:%d, firstIFramePos:%d\n", bError, firstIFramePos);
	//trace("m_bPacketType:%d\n", m_bPacketType);
}
#endif

VD_BOOL CRecordStorage::start(VD_BOOL bPre)
{
	uint dwFlag = DATA_BUFFER;
	m_bPre = bPre;

	if (!bPre)
	{
		dwFlag |= m_DataType;
		//if(createStorage() == false)
		//	return FALSE;
		//强制I帧
		//m_pDevCapture->SetIFrame();
		ICapture::instance(m_iChannel)->SetIFrame();
	}
	else
	{
		SYSTEM_TIME systemtime;
		SystemGetCurrentTime(&systemtime);
		//m_pDevCapture->SetTime(&systemtime);
		ICapture::instance(m_iChannel)->SetTime(&systemtime);	
	}
	ICapture::instance(m_iChannel)->SetIFrameNum(0);  
	
	m_imgSize.w = 0;
	m_imgSize.h = 0;

	if (!ICapture::instance(m_iChannel)->Start(this, (ICapture::SIG_DEV_CAP_BUFFER)&CRecordStorage::onRecord, dwFlag, m_RecordType))
	{
		if (!m_bPre)
		{
			//CDevAudioIn::instance(m_iChannel)->Stop(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRecordStorage::OnAudioData);
			closeStorage();
		}
		return FALSE;
	}
	
	if(!bPre && m_RecordType == CHL_MAIN_T)
	{
		CDevBufManager::instance()->clear(m_iChannel);
		CDevAudioIn::instance(m_iChannel)->Start(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRecordStorage::OnAudioData);
	}

	if (!bPre)
	{
		if(createStorage() == false)
		{
			CDevAudioIn::instance(m_iChannel)->Stop(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRecordStorage::OnAudioData);
			ICapture::instance(m_iChannel)->Stop(this, (ICapture::SIG_DEV_CAP_BUFFER)&CRecordStorage::onRecord, dwFlag, m_RecordType);
			return FALSE;
		}
	}
	return TRUE;
}

VD_BOOL CRecordStorage::stop(VD_BOOL bPre)
{
    VD_BOOL bRet = TRUE; 
	uint dwFlag = DATA_READY;
	if (bPre)
	{
		dwFlag |= DATA_BUFFER;
	}
	
	if (!m_bPre)
	{
		//只关自己的录像
		dwFlag |= m_DataType;
	}
	if (!m_bPre)
		CDevAudioIn::instance(m_iChannel)->Stop(this,(CDevAudioIn::SIG_DEV_CAP_BUFFER)&CRecordStorage::OnAudioData);

	if (!ICapture::instance(m_iChannel)->Stop(this, (ICapture::SIG_DEV_CAP_BUFFER)&CRecordStorage::onRecord, dwFlag, m_RecordType))
	{
		closeStorage();
		return FALSE;
	}

	if (!m_bPre)
	{
		closeStorage();
		m_bPre = TRUE;
	}


	m_bPacketType = packetNone;
	return TRUE;
}

/*!
	\b Description		:	录像打包，打包之前必须给下一个录像命名\n
	\b Argument			:	packetTypes types
	\param[in]	types	:	打包类型
*/
VD_BOOL CRecordStorage::pack(packetTypes types)
{
	if ((m_bPre) || (types < packetNone) || (types > packetAdvanced))
	{
		trace("CRecordStorage::pack(%d) error, and storage addr %#x\n", types, (int)m_pDhfsStorage);
		return FALSE;
	}

	if (types == packetForce)
	{
		// 会导致打包的录像文件感觉丢数据，实际上没有丢失
		closeStorage();
		createStorage();
		m_bPacketType = packetNone;
	}
	else
	{
		//m_pDevCapture->SetIFrame();
		ICapture::instance(m_iChannel)->SetIFrame(m_RecordType);
		m_bPacketType = types;

		if( ICapture::instance(m_iChannel)->GetCaptureType() == ENUM_CHANNEL_TYPE_DATA )
		{
			m_bPacketType = packetForce;
		}
	}

	return TRUE;
}

packetTypes CRecordStorage::getPackType() const

{
	return m_bPacketType;
}

CRecord::CRecord(int iChannel, enum capture_channel_t type) 
: m_iChannel(iChannel), m_RecordStorage(iChannel, type, &m_Attr, &m_RecordMutex),
	m_RecordMutex(MUTEX_RECURSIVE)
{
	memset((void *)&m_Attr, 0, sizeof(STM_ATT));
    if(type == CHL_MAIN_T)
    {
        m_Attr.chan = m_iChannel;
    }
    else
    {
        m_Attr.chan = m_iChannel+g_nLogicNum;
    }
	
	m_recMode = 0;
	m_Record_Type = type;
}

CRecord::~CRecord()
{
}

///
///	\b Description		:	切换编码类型\n
/// \param arg			:	录像的类型
///	\return FALSE：不会打包；TRUE：自动会打包
///
///	\b Revisions		:	
///		- 2008-7-1		yuan_shiyong		Create
///
void CRecord::switchEncodeType(uint arg)
{
	uint encodeType = arg;
	switch(arg)
	{
	default:
	case REC_CRD:
	case REC_SYS:
	case REC_MAN:
	case REC_TIM:
		encodeType = REC_TIM;
		break;
	case REC_MTD:
	case REC_ALM:
		//不处理，已经赋过值
		break;
	}
	
	g_Encode.setEncodeType(encodeType, m_iChannel);
}

VD_BOOL CRecord::Start(uint arg)
{
	CGuard cGuard(m_RecordMutex);
	VD_BOOL bRet = FALSE;
	uint currentMode = getMode();
#ifndef IPC_JZ
	if (checkStart(arg))
	{
		return bRet;
	}
	
	switchEncodeType(arg);	
#endif
	// 检测条件成功的话
	if (arg == REC_SYS)
	{
		//能进来说明前次没有开过预录或录像	
		bRet = m_RecordStorage.start(TRUE);
	}
	else
	{		
		setFileType(arg);
		if (currentMode <= REC_SYS)
		{
			bRet = m_RecordStorage.start(FALSE);
		}
		else
		{
			bRet = Pack(packetAdvanced);
		}
	}
	
	if (bRet == FALSE)
	{
		//去除录像状态(注意，动检变卡号没有处理）
		m_recMode &= ~arg;
	}

#ifdef REC_SUPPORT_TYPE_APPEND
	if (bRet)
	{
		switch(arg)
		{
			case REC_ALM:
				g_Encode.setOSDTitle(0,  OSD_TOP_LEFT_CORNER, TRUE, "A", m_iChannel);
				break;
			case REC_MTD:
				g_Encode.setOSDTitle(0, OSD_TOP_LEFT_CORNER, TRUE, "M", m_iChannel);
				break;
		}
	}
#endif
	return bRet;
}

VD_BOOL CRecord::Stop(uint arg)
{
	CGuard cGuard(m_RecordMutex);
	VD_BOOL bRet = FALSE;
	uint currentMode = getMode();

#ifndef IPC_JZ
	if (checkStop(arg))
	{
		return bRet;
	}

	if (arg == REC_CRD || currentMode == REC_CRD) 
	{
		//清屏
		g_Encode.ClearInfoTitle(m_iChannel);
		//g_LuaBase.OnNoneData();
	}
#endif

	uint mode = getMode();
#ifndef IPC_JZ	
	switchEncodeType(mode);
#endif
	if (mode > REC_SYS)
	{
		setFileType(getMode());
		//切换一下分辨率
		bRet = Pack();
	}
	else
	{
		bRet = m_RecordStorage.stop(mode != REC_SYS);
	}
	
	if (bRet == FALSE)
	{
		//还原以前的状态
		m_recMode |= currentMode;
	}

#ifdef REC_SUPPORT_TYPE_APPEND
	if (bRet)
	{
		if (arg == REC_MTD || arg == REC_ALM
			|| currentMode == REC_MTD || currentMode == REC_ALM)
		{
			g_Encode.ClearInfoTitle(m_iChannel);
		}
	}
		
#endif
	return bRet;
}

VD_BOOL CRecord::Pack(packetTypes types)
{
	VD_BOOL bRet = FALSE;
	packetTypes type = types;
	CGuard cGuard(m_RecordMutex);
	if (getMode() > REC_SYS)
	{
		if (m_Attr.type == recFileAudio)
		{
			if (type == packetAuto)
			{
				type = packetForce;
			}
		}
		bRet = m_RecordStorage.pack(type);
	}

	return bRet;
}

int CRecord::checkStart(uint arg)
{	
	CConfigEncode* pCfgEncode = new CConfigEncode;
	if (NULL == pCfgEncode)
	{
		return 0;
	}
	pCfgEncode->update(m_iChannel);

	uint encMod = ENCODE_TYPE_TIM;
#if defined(ENC_CHANGE_PARAM)
	switch(arg)
	{
	case REC_MTD:
		encMod = ENCODE_TYPE_MTD;
		break;
	case REC_ALM:
		encMod = ENCODE_TYPE_ALM;
		break;
	default:
		encMod = ENCODE_TYPE_TIM;
		break;
	}
#endif
	MEDIA_FORMAT& format = pCfgEncode->getConfig(m_iChannel).dstMainFmt[encMod];
	if ((format.bAudioEnable == FALSE) && (format.bVideoEnable == FALSE))
	{
		delete pCfgEncode;
		return 1;
	}
	
	uint currentMode = getMode();

	m_recMode |= arg;
	
#ifdef FUN_SNIFFER 
	if ((arg < currentMode) || (arg == currentMode))
	{
		delete pCfgEncode;
		return 1;
	}
#else
	//如果低于当前的录像模式，或等于当前的录像模式且不是卡号录像，返回
	if ((arg < currentMode) || ((arg == currentMode) && (currentMode != REC_CRD)))
	{
		delete pCfgEncode;
		return 1;
	}

#endif
	

	delete pCfgEncode;
	return 0;
}

int CRecord::checkStop(uint arg)
{

	uint currentMode = getMode();

	// 如果当前没有开启录像或预录，或低于当前录像模式返回
	if (currentMode < REC_SYS)
	{
		return 1;
	}

	//小于或是当前模式，直接去除
	m_recMode &= ~arg;
	if (arg < currentMode)
	{
		return 1;
	}
	else if (arg == currentMode)
	{
		return 0;
	}
	else
	{
		// 非手动或清除，返回
		if ((arg != REC_MAN) && (arg != REC_CLS))
		{
			return 1;
		}

		//手动录像或清除无法关闭预录
		if (currentMode == REC_SYS)
		{
			return 1;
		}

		if (arg == REC_MAN) 
		{
			m_recMode &= ~currentMode;
		}

		else if (arg == REC_CLS)
		{
			m_recMode = m_recMode & REC_SYS ? REC_SYS : 0;
		}
	}

	return 0;
}

/*!
	\b Description		:	获得录像模式,如果没有录像，则获得可用的录像模式\n
	\return	返回录像模式，参照宏定义			

	\b Revisions		:	
*/
uint CRecord::getMode()
{
	if (m_recMode == 0)
	{
		return 0;
	}
	int i = 1;
	while (0 != (m_recMode >> i))
	{
		i++;
	}
	return BITMSK(i - 1);
}

/*!
	\b Description		:	初始化设备，并定时打包和预录\n
	\b Argument			:	[IN/OUT]int iChannel
	\param	iChannel	:   通道号
	\b Revisions		:	
*/
void CRecord::setFileInfo(const MEDIA_FORMAT& format)
{
	setDHFileAttr(&m_Attr, format);
}

/*!
	\b Description		:	获得录像文件的属性\n
	可以是单个录像模式，也可以是多个模式传进来，如果是多个模式，采用级别最高的\n
	\b Argument			:	PARAM rec_mode
	\param	rec_mode	:	录像模式

　	\b Revisions		:	
*/
void CRecord::setFileType(uint rec_mode)
{
	//这个顺序不可变，是根据优先级别进行的
	if (rec_mode & REC_MAN)
	{
		m_Attr.alrm = F_HAND;
	}
	else if (rec_mode & REC_CRD)
	{
		m_Attr.alrm = F_CARD;
	}
	else if (rec_mode & REC_ALM) 
	{
		m_Attr.alrm = F_ALERT;
	} 
	else if (rec_mode & REC_MTD)
	{
		m_Attr.alrm = F_DYNAMIC;
	} 
	else
	{
		m_Attr.alrm = F_COMMON;
	}

	if( m_Record_Type != CHL_MAIN_T )
	{
	    m_Attr.alrm = F_2SND;
	}
}


void CRecord::setRedundancy(uchar redundancy)
{
	m_Attr.redu = redundancy; 
	setFileType(getMode());
	Pack();
}

int CRecord::getPacketLen(SYSTEM_TIME *pTime)
{
	int iPacketLen = 0;
	if (getMode() > REC_SYS)
	{
		iPacketLen = ABS((pTime->hour * 60 + pTime->minute) * 60 + pTime->second
		- (m_Attr.time.hour * 60 + m_Attr.time.minute) * 60 - m_Attr.time.second);
	}

	return iPacketLen;
}


/*!
	\b Description		:	获得当前通道指定模式的录像状态\n
	\b Argument			:	PARAM arg
	\param		arg		:	录像模式
	\return		非0：正在录像；0：没有录像		

	\b Revisions		:	
*/
/*
BOOL CRecord::GetState(PARAM arg)
{
	return (((arg != 0) && (getMode() == arg)) || ((arg == 0) && (getMode() > REC_SYS)));
}
*/

/*!
	\b Description		:	根据打包长度决定是否打包\n
	(1)如果持续写入文件的时间超过打包长度的1.5倍时一定打包\n
	(2)如果打包长度在60分钟内且是60的约数，则约数对齐\n
	(3)如果打包长度超过60分钟且是60的倍数，则倍数对齐\n
	(4)其他情况则以长度达到打包长度就打包\n

	 \b Revisions		:	
*/
/*
void CRecord::timePack()
{

}
*/

/*!
	\b Description		:	处理系统时间修改的情况\n
	\b Argument			:	SYSTEM_TIME *systime
	\param[in]	systime	:	系统时间
	\return				

	\b Revisions		:	
*/
void CRecord::updateSystemTime(SYSTEM_TIME *systime)
{
	memcpy(&m_Attr.time, systime, sizeof(SYSTEM_TIME));
	ICapture::instance(m_iChannel)->SetTime(systime);
}

int CRecord::getFileLength()
{
	return m_RecordStorage.getLength();
}

packetTypes CRecord::getPackType() const
{
	return m_RecordStorage.getPackType();
}


VD_BOOL CRecord::setInfo(void * pcontext, uint length, storageInfoTypes type)
{
	if (type == storageInfoCard)
	{
		if (getMode() != REC_CRD)
		{
			tracepoint();
			return FALSE;
		}

		//把信息写入录像的头８K字节里
		CUSTOM_MESSAGE_HEADER header;
		SYSTEM_TIME systime;

		header.Type = type;
		header.Length = length;

		SystemGetCurrentTime(&systime);
		timesys2dh(&header.Time, &systime);

		m_RecordStorage.setInfo(&header, sizeof(CUSTOM_MESSAGE_HEADER), type);
	}

	m_RecordStorage.setInfo(pcontext, length, type);

	return TRUE;

}

/***********************************************************************
*
* CRecordManger用来管理单个Record，从配置文件中获得参数并初始化对应的Record，
* 并可以获得每个Recorder的状态，在线程中完成录像的开始和停止工作
*
************************************************************************/

/*!
	\b Description		:	设置线程信息\n

	\b Revisions		:	
*/
PATTERN_SINGLETON_IMPLEMENT(CRecordManager);

CRecordManager::CRecordManager() 
#ifdef RECORD_NET_THREAD_MULTI
: CThread("RecordManager", 20, g_nLogicNum)
, m_RecordCtrlMutex(MUTEX_RECURSIVE)
,m_RecordTimer("RecordTimer")

#else
	
	: CThread("RecordManager", TP_REC, g_nLogicNum), m_RecordCtrlMutex(MUTEX_RECURSIVE)
#endif
#ifdef ENC_ENCODE_MULTI
	, m_ExtTimer("ExtTimer")
#endif
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		m_pRecord[i] = new CRecord(i, CHL_MAIN_T);
		m_bAllowPack[i] = TRUE;

#if 0
		m_cAlarmLinkage[i].setLinkItem(appEventAlarmLocal);
		m_cAlarmLinkage[i].setLinkItem(appEventAlarmNet);
		m_cAlarmLinkage[i].setLinkItem(appEventNetAbort);		
		m_cMotionLinkage[i].setLinkItem(appEventVideoMotion);
		m_cMotionLinkage[i].setLinkItem(appEventVideoBlind);
		m_cMotionLinkage[i].setLinkItem(appEventVideoLoss);
#endif
	}	

	m_SuspendMode = 0;
	m_iNtpSuccess = 0;
}

CRecordManager::~CRecordManager()
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		delete m_pRecord[i];
	}
}


VD_BOOL CRecordManager::Start()
{
	trace("CRecordManager::Start()>>>>>>>>>\n");
#ifndef IPC_JZ
	//从CMOS中获得手动录像启动参数值
	CCMOS cmos;
	cmos.Read(CMOS_MANUAL_REC, &m_RecControl, 8);

	if (ValidateCMOS(&m_RecControl))
	{
		cmos.Write(CMOS_MANUAL_REC, &m_RecControl, 8);
	}
#else
	m_RecControl = 0x38;
#endif

#ifdef RECORD_NET_THREAD_MULTI

#ifdef IPC_JZ
	TimerStartRec(0);
	m_RecordTimer.Start(this, (VD_TIMERPROC)&CRecordManager::TimerStartRec, 1000, 3000);
#else
	m_RecordTimer.Start(this, (VD_TIMERPROC)&CRecordManager::TimerStartRec, 10*1000, 3000);
#endif
	//m_RecordTimer.Start(this, (VD_TIMERPROC)&CRecordManager::OnTimer, 0, 500);
#endif
	int iRet = 0;

#ifdef ENC_ENCODE_MULTI
	GetDspInfo(&m_DSPInfo);

	for (int i = 0; i < (int)((g_nCapture + m_DSPInfo.nMaxSupportChannel - 1) / m_DSPInfo.nMaxSupportChannel 
		* m_DSPInfo.nExpandChannel); i++)
	{
		// 目前扩展通道只有一个
		m_pExtRecord[i] = new CExtRecord(i);
	}
#endif

#ifndef IPC_JZ
	//从普通配置中读取硬盘处理方式和打包长度设置录像对象
	m_cCfgGeneral.attach(this, (TCONFIG_PROC)&CRecordManager::onConfigGeneral);
	m_cCfgGeneral.update();
	onConfigGeneral(m_cCfgGeneral, iRet);

	m_cCfgEncode.attach(this, (TCONFIG_PROC)&CRecordManager::onConfigEncode);
	m_cCfgEncode.update();
	onConfigEncode(m_cCfgEncode, iRet);

	m_cCfgRecord.attach(this, (TCONFIG_PROC)&CRecordManager::onConfigRecord);
	m_cCfgRecord.update();

	onConfigRecord(m_cCfgRecord, iRet);
#endif

	//printf("start manual record\n");
	//开启手动录像 fyj

	//向事件中心注册处理类
//	CAppEventManager::instance()->attach(this, (SIG_EVENT::SigProc)&CRecordManager::onAppEvent);
	//取一下系统时间
	SystemGetCurrentTime(&m_SystemTime);
	//开启线程
	CreateThread();
#ifdef ENC_ENCODE_MULTI
sdaf
	// 定时打包
	m_ExtTimer.Start(this, (VD_TIMERPROC)&CRecordManager::OnExtTimer, 0, 1000);
#endif

	return TRUE;
}

int CRecordManager::TimerStartRec(int arg)
{
	return 0;
	
#if defined(IPC_JZ) && defined(LIVECAM)
	char file[128] = {0};
	static int ret = -1;
	static char upFail = 0;

	if(ret != 0  && upFail < 3 && VD_TRUE == GetExtRecFile(file))
	{
		if(g_DevExternal.Dev_GetWakeUpFlag() == SYS_WAKEUP_FLAG_PIR)
			ret = RealTimeUploadFile(file,RT_MP4);
		else
			ret = 0;
		
		_printd("gain %s.....%d\n",file,ret);
		if(0 == ret)	{_printd("remove file:%s....%d",file,remove(file));}
		else			upFail ++;
	}
#endif

#if 0
#ifndef IPC_JZ
	static char ntp_update = g_NtpClient.GetNtpUpdateCount();

	if( g_NtpClient.GetNtpUpdateCount() != ntp_update){
		if( 0 == ntp_update && g_RecordStatus){
			_printd("Repack record file because of ntp");
			g_Record.StopRec(REC_CLS, 0);
		}
		ntp_update = g_NtpClient.GetNtpUpdateCount();
	}
#endif
#endif
	//处于待机状态
	if(m_SuspendMode){ 
		_printd("The System is Suspend");
		if(g_RecordStatus){
			printf("#############StopManulRecord##############\n");
			g_Record.StopRec(REC_CLS, 0);
		}	
		
		return -1;		
	}

	if(/*1 == UpgradeStatus() ||*/
	   4 == g_DriverManager.GetSDStatus() ||
	   checkDisk()){
		_printd("Upgrading  g_RecordStatus[%d]\n",g_RecordStatus);
		
		if(g_RecordStatus){
			_printd("#############StopManulRecord##############\n");
			g_Record.StopRec(REC_CLS, 0);
		}	
		
		return -1;
	}

	if( 0 != g_DriverManager.GetSDStatus()){
		_printd("g_DriverManager.GetSDStatus()[%d]",g_DriverManager.GetSDStatus());
	}
	
	if(!g_RecordStatus){
		_printd("#############StartManulRecord##############");
		//开启手动录像
		_printd("start record[%d] :%d\n",0,StartRec(REC_MAN, 0));
	}	
	
	return 0;
}


#ifdef ENC_ENCODE_MULTI
void CRecordManager::OnExtTimer(uint arg)
{
	for (int i = 0; i < (int)((g_nCapture + m_DSPInfo.nMaxSupportChannel - 1) / m_DSPInfo.nMaxSupportChannel
		* m_DSPInfo.nExpandChannel); i++)
	{
		m_pExtRecord[i]->Pack(false);
	}
}	
#endif

/*!
	\b Description		:	停止所有录像\n

	\b Revisions		:	
		- 2006-11-3			zhongyj		Modify
*/
VD_BOOL CRecordManager::Stop()
{
	trace("CRecordManager::Stop()>>>>>>>>>\n");
	DestroyThread();

	CGuard l_cGuard(m_RecordCtrlMutex);

	m_RecControl = 0;
	for (int i = 0; i < g_nLogicNum; i++)
	{
		StopPair(i, REC_CLS);
	}
#ifdef ENC_ENCODE_MULTI
	// 定时打包
	m_ExtTimer.Stop();
#endif

	CAppEventManager::instance()->detach(this, (SIG_EVENT::SigProc)&CRecordManager::onAppEvent);
	m_cCfgRecord.detach(this, (TCONFIG_PROC)&CRecordManager::onConfigRecord);
	m_cCfgEncode.detach(this, (TCONFIG_PROC)&CRecordManager::onConfigEncode);
	m_cCfgGeneral.detach(this, (TCONFIG_PROC)&CRecordManager::onConfigGeneral);

#ifdef ENC_ENCODE_MULTI
	for (int i = 0; i < (int)((g_nCapture + m_DSPInfo.nMaxSupportChannel - 1) / m_DSPInfo.nMaxSupportChannel 
		* m_DSPInfo.nExpandChannel); i++)
	{
		// 目前扩展通道只有一个
		delete m_pExtRecord[i];
	}
#endif
	
	return TRUE;
}

/*!
	\b Description		:	用线程来代替以前的定时器任务\n

	\b Revisions		:	
*/
#ifdef RECORD_NET_THREAD_MULTI

void CRecordManager::ThreadProc()
{
	bool bSleep = true;
	uint count_sleep = 0;
	uint count_clear = 0;

	SET_THREAD_NAME("CRecordManager");
	SystemSleep(2000);
	while( m_bLoop )
	{
		bSleep = true;
		for (int iChn = 0; iChn < ICaptureManager::instance()->GetLogicChnNum(); iChn++)
		{
			if( m_pRecord[iChn]->m_RecordStorage.DoPreview() )
			{
				bSleep = false;
			}
		}

		//!是否需要休眠
		if( bSleep )
		{
			count_sleep++;
			if( count_sleep > 5 )
			{
				SystemSleep(500);
				count_sleep = 0;
			}
		}
		else
		{
			count_sleep = 0;
			count_clear = 0;
		}

		SystemUSleep(1000);
	}
}

void CRecordManager::OnTimer(uint arg)
{
	SYSTEM_TIME current_time;
	DHTIME dhtm1;
	DHTIME dhtm2;

	SystemGetCurrentTime(&current_time);

	timesys2dh(&dhtm1, &current_time);
	timesys2dh(&dhtm2, &m_SystemTime);

	//只处理时间自动运行的情况,如果人为修改时间差大于10秒，则会调用UpdateSystemTime去打包并设置时间
	if ((current_time.day != m_SystemTime.day) && (ABS(changeto_second(dhtm1, dhtm2)) <= 10))
	{
		m_SystemTime = current_time;
		dateChanged(&current_time);
	}
	else
	{
		if (current_time.second != m_SystemTime.second)
		{
			timerWork(&current_time);
			m_SystemTime = current_time;
		}
	}
}
#else
void CRecordManager::ThreadProc()
{
	SYSTEM_TIME current_time;

	DHTIME dhtm1;
	DHTIME dhtm2;

	while (m_bLoop) 
	{
		SystemGetCurrentTime(&current_time);

		timesys2dh(&dhtm1, &current_time);
		timesys2dh(&dhtm2, &m_SystemTime);

		//只处理时间自动运行的情况,如果人为修改时间差大于10秒，则会调用UpdateSystemTime去打包并设置时间
		if ((current_time.day != m_SystemTime.day) && (ABS(changeto_second(dhtm1, dhtm2)) <= 10))
		{
			m_SystemTime = current_time;
			dateChanged(&current_time);
		}
		else
		{
			if (current_time.second != m_SystemTime.second)
			{
				timerWork(&current_time);
				m_SystemTime = current_time;						
			}
			SystemSleep(200);
		}
	}
}
#endif
void CRecordManager::dateChanged(SYSTEM_TIME *current_time)
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		ICapture::instance(i)->SetTime(current_time);
		uint recMode = getModePair(i);
		
		if (recMode > REC_SYS)
		{
			//根据手动录像参数决定操作
			if (recMode == REC_MAN)
			{
				//立即打包
				PackPair(i);
			}
			else
			{
				if(!CheckSection(recMode, i))
				{
                    PackPair(i);
				}
				else
				{
					StopRec(recMode,i);
				}
			}	
		}		
	}
}

/*!
	\b Description		:	代替定时器工作，即录像过程中打包或停止录像，无录像时开启定时录像\n

	\b Revisions		:	
*/
void CRecordManager::timerWork(SYSTEM_TIME *current_time)
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		uint recMode = getModePair(i);
	    //printf("i <%d> recMode <%d>\n",i ,recMode);
		//正在录像，处理打包
		if (recMode > REC_SYS)
		{
			if (!CheckSection(recMode, i))
			{
				timerPack(current_time, i);
			}
			else
			{
				StopRec(recMode, i);
			}
		}
		else
		{
			if (recMode < REC_SYS)
			{
				if (m_cCfgRecord.getConfig(i).iPreRecord)
				{
					StartRec(REC_SYS, i);
				}
			}

			 if (!m_cAlarmLinkage[i].isEmpty())
			 {
				 StartRec(REC_ALM, i);
			 }

			 if (!m_cMotionLinkage[i].isEmpty())
			 {
				 StartRec(REC_MTD, i);
			 }

			 StartRec(REC_TIM, i);
		}
	}
}

void CRecordManager::pack(packetTypes types, int iChannel)
{
	if (types == packetNone)
	{
		m_bAllowPack[iChannel] = FALSE;
	}
	else
	{
		m_bAllowPack[iChannel] = TRUE;
		if (types == packetForce)
		{
		    PackPair(iChannel, packetForce);	
		}
	}
}

void CRecordManager::timerPack(SYSTEM_TIME *pTime, int iChannel)
{
	int iRecLength = m_cCfgRecord.getConfig(iChannel).iPacketLength;
	
	if ((ABS(time2second(&m_SystemTime, pTime)) <= 10) && (m_SystemTime.minute != pTime->minute))
	{
		int iPacketLen = getPacketLenPair(iChannel, pTime);
        //if (iChannel == 0) debugf("iPacketLen:%d, iRecLength:%d, ballow:%d, minutue:%d\n", 
        //    iPacketLen, iRecLength, m_bAllowPack[iChannel], pTime->minute);
		//超过0.5倍时间考虑对齐打包(如果打包时间在1小时之内，约数对齐，1小时之外，倍数对齐,否则无需对齐)
		if ((((iPacketLen > iRecLength * 60 / 2)
			&& (((iRecLength <= 60) && (60 % iRecLength == 0) && (pTime->minute % iRecLength == 0))
			|| ((iRecLength > 60) && (iRecLength % 60 == 0) && (pTime->minute == 0))))
			|| ((60 % iRecLength != 0) && (iRecLength % 60 != 0) && (iPacketLen >= iRecLength * 60))))
		{
			if (m_bAllowPack[iChannel])
			{
			    PackPair(iChannel);
			}
		}
		else if (iPacketLen >= iRecLength * 60 * 3 / 2)
		{
			//超过1.5倍录像长度强制打包
			if (m_bAllowPack[iChannel])
			{
				trace("ch[%d] must pack unconventionally\n", iChannel);
				PackPair(iChannel, packetForce);			
			}
		}
	}	
}

void CRecordManager::UpdateSystemTime(SYSTEM_TIME *systime)
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		m_pRecord[i]->updateSystemTime(systime);
	}
}
/*!
	\b Description		:	获得所有通道指定模式的状态，默认为获得所有通道的状态\n
	\b Argument			:	PARAM arg
	\param	arg			:	指定的模式
	\return	以位的形式表示录像状态，1为正在录像，0为没有录像			

	\b Revisions		:	
*/
uint CRecordManager::GetState(uint arg)
{
	uint allstate = 0;
	if (arg == 0)
	{
		for (int i = 0; i < g_nLogicNum; i++)
		{
			if (getModePair(i) > REC_SYS)
			{
				allstate |= BITMSK(i);
			}
		}
	}
	else
	{
		for (int i = 0; i < g_nLogicNum; i++)
		{
			if (getModePair(i) == arg)
			{
				allstate |= BITMSK(i);
			}
		}
	}

        if (g_nLogicNum > g_nCapture)
        {
            /* 无可写盘时不显示录像图标 */
            uint disk_stat=0;
            g_DriverManager.IsFull(&disk_stat, DRIVER_READ_WRITE);

            if (disk_stat == FS_ERROR)
            {
                allstate = 0;
            }
        }
        
	return allstate;
}

uint CRecordManager::GetRecMode(int iChannel)
{
	if ((iChannel < 0) || (iChannel >= g_nLogicNum))
	{
		return FALSE;
	}

	return getModePair(iChannel);
}



/*!
	\b Description		:	开始录像\n
	\b Argument			:	PARAM arg, int channel
	\param	arg			:	录像模式
	\param	channel		:	录像的通道数
	\return	以掩码表示成功的通道

	\b Revisions		:	
*/
uint CRecordManager::StartRec(uint arg, int iChannel)
{
	if ((iChannel < -1) || (iChannel >= g_nLogicNum))
	{
		return 0;
	}

	CGuard l_cGuard(m_RecordCtrlMutex);

//现程序只允许同一时间只有一个码流在录像  
	if(g_RecordStatus)
		return 0;


	if(1 != m_iNtpSuccess && REC_SYS != arg) 
	{
		//时间未同步，可以开启预录
		return 0;
	}

	uint ret = 0;

	if (iChannel == -1)
	{
		for (int i = 0; i < g_nLogicNum; i++)
		{
			if (CheckLimit(arg, i))
			{
				continue;
			}
            ret |= StartPair(i, arg);
		}
	}
	else
	{
		if (!CheckLimit(arg, iChannel))
		{
            ret |= StartPair(iChannel, arg);
		}
	}
	
	// 下面是用来更新CMOS的值，用来和真实录像保持一致
	uint64 cmosmask = m_RecControl;

	if (ret != 0)
	{
		switch(arg)
		{
		case REC_TIM:
		case REC_MTD:
		case REC_ALM:
		case REC_CRD:
			for (int i = 0; i < g_nLogicNum; i++)
			{
				if (ret & BITMSK(i))
				{
					cmosmask &= ~(((uint64)0x3) << (2 * i));
					cmosmask |=((uint64)AUTOMATIC_REC )<< (2 * i); 
				}
			}
			break;
		case REC_MAN:
			for (int i = 0; i < g_nLogicNum; i++)
			{
				if (ret & BITMSK(i))
				{
					cmosmask &= ~(((uint64)0x3) << (2 * i));
					cmosmask |= ((uint64)MANUAL_REC) << (2 * i);
				}
			}
			break;	
		default:
			break;
		}
	}
	SaveRecordControl(cmosmask, FALSE);

	if(ret == 1 && iChannel == 0 && arg >= 4)
		g_RecordStatus = 1;

	return ret;
}


/*!
	\b Description		:	停止录像\n
	\b Argument			:	PARAM arg, int channel
	\param	arg			:	录像模式
	\param	channel		:	录像的通道数
	\return	以掩码表示成功的通道	

	\b Revisions		:	
*/
uint CRecordManager::StopRec(uint arg, int iChannel)
{
	if ((iChannel < -1) || (iChannel >= g_nLogicNum))
	{
		return 0;
	}
	
	CGuard l_cGuard(m_RecordCtrlMutex);
	if(!g_RecordStatus)	
	{
		record_flag = 0;
		return 0;
	}

	uint ret = 0;

	if (iChannel == -1)
	{
		for (int i = 0; i < g_nLogicNum; i++)
		{
            ret |= StopPair(i, arg);
		}
	}
	else
	{
	    ret |= StopPair(iChannel, arg);
	}
	// 下面是用来更新CMOS的值，用来和真实录像保持一致
	uint64 cmosmask = m_RecControl;
	if (ret != 0)
	{
		switch(arg)
		{
			case REC_MAN:
				for (int i = 0; i < g_nLogicNum; i++)
				{
					if (ret & BITMSK(i))
					{
						cmosmask &= ~(((uint64)0x3) << (2 * i));
						cmosmask |=((uint64) AUTOMATIC_REC)<< (2 * i); 
					}
				}
				break;
			case REC_CLS:
				for (int i = 0; i < g_nLogicNum; i++)
				{
					if (ret & BITMSK(i))
					{
						cmosmask &= ~(((uint64)0x3) << (2 * i));
						cmosmask |= ((uint64)CLOSED_REC) << (2 * i);
					}
				}
				break;
			default:
				break;
		}
	}
	SaveRecordControl(cmosmask, FALSE);

	if(iChannel == 0 && arg >= 4)
	{
		g_RecordStatus = 0;
		record_flag = 0;
	}
	
	return ret;
}
/*!
	对于定时录像，动检录像，报警录像，检测是否在时间段内
	对于手动录像则检测是否配置允许录像，并且磁盘可用于录像
	返回0表示校验通过，非0表示校验失败
*/
int CRecordManager::CheckLimit(uint arg, int iChannel)
{
	int ret = 0;
	switch (arg)
	{
	case REC_SYS:
	case REC_CLS:
		break;	
	case REC_TIM:
	case REC_MTD:
	case REC_ALM:
	case REC_CRD:
		#if 0
		//由于CheckSection会调用事件中心的CheckSection（内部有锁），而事件中心的回调会调用开启录像，导致死锁
		//所以在这里解锁
		m_RecordCtrlMutex.Leave();
		ret |= CheckSection(arg, iChannel); //fyj
		//printf("CheckLimit iRet <%d> iChannel <%d>\n", ret, iChannel);
		m_RecordCtrlMutex.Enter();
		#endif
		//继续检测CMOS和硬盘
	case REC_MAN:
		if (ret == 0)
		{	
			ret |= checkDisk(); 
		}

		break;
	default:
		ret = 1;
		break;
	}
	return ret;
}

/*!
	\b Description		:	检测定时，动检和报警的时间段\n
	\b Argument			:	[PARAM arg, int iChannel
	\param	arg			:	录像模式
	\param	iChannel	:	录像通道
	\return	0：检测通过，非0：检测失败			

	\b Revisions		:	
*/
int CRecordManager::CheckSection(uint arg, int iChannel)
{
	int iRet = 0;

	if (arg == REC_TIM)
	{
		iRet = CAppEventManager::instance()->checkTimeSection(&CConfigRECWorksheet::getLatest(iChannel)) == TRUE ? 0 : 1;
	}
	else if (arg == REC_ALM)
	{
	    /* End:Added shixiang */
		CLinkage *pLinkage = &m_cAlarmLinkage[iChannel];

		//本地报警高于网络报警--->将断网录像设置为最高优先级		
		appEventCode eventCodeType[] = {appEventNetAbort, appEventAlarmLocal, appEventAlarmNet, appEventAll};
		int i = 0;
		appEventCode eventCode = eventCodeType[i];
		uint alarmin = 0;
		while (eventCode != appEventAll)
		{
			if ((alarmin = pLinkage->getEventSource(eventCode)) != 0)
			{
				break;
			}
			i++;
			eventCode = eventCodeType[i];
		}

		if (alarmin == 0)
		{
			return 1;
		}

		CONFIG_WORKSHEET ws = CConfigRECWorksheet::getLatest(iChannel);
		ConvertState(&ws, &CConfigRECState::getLatest(iChannel), 1);

		if (!CAppEventManager::instance()->checkTimeSection(&ws))
		{
			iRet = 1;
		}
	}
	else if (arg == REC_MTD)
	{
		CLinkage *pLinkage = &m_cMotionLinkage[iChannel];

		appEventCode eventCodeType[] = {appEventVideoLoss, appEventVideoBlind, appEventVideoMotion, appEventAll}; 


		int i = 0;
		appEventCode eventCode = eventCodeType[i];		
		uint motionin = 0;

		while (eventCode != appEventAll)
		{
			if ((motionin = pLinkage->getEventSource(eventCode)) != 0)
			{
				break;
			}
			i++;
			eventCode = eventCodeType[i];
		}

		if (eventCode == appEventAll)
		{
			return 1;
		}
		
		CONFIG_WORKSHEET ws = CConfigRECWorksheet::getLatest(iChannel);
		ConvertState(&ws, &CConfigRECState::getLatest(iChannel), 0);
		if (!CAppEventManager::instance()->checkTimeSection(&ws))
		{
			iRet = 1;
		}
	}
#ifdef FUN_SNIFFER 
	else if (arg == REC_CRD)
	{
		CONFIG_WORKSHEET ws = CConfigRECWorksheet::getLatest(iChannel);
		ConvertState(&ws, &CConfigRECState::getLatest(iChannel), 0);
		iRet = CAppEventManager::instance()->checkTimeSection(&ws) == TRUE ? 0 : 1;

	}
#endif

	return iRet;
}

//flag=0 转换动检时间表 ，flag=1转换报警时间表
void CRecordManager::ConvertState(CONFIG_WORKSHEET* psheet, const CONFIG_RECSTATE* pState,  VD_BOOL type)
{
	if (type > 1)
	{
		tracepoint();
		return;
	}
	
	for(int i = 0; i < N_WEEKS; i++)
	{
		for(int j = 0; j < N_TSECT; j++)
		{
			psheet->tsSchedule[i][j].enable = pState->recstate[i][j] & BITMSK(type);			
		}
	}
}

/*!
	检查通道iChannel是否允许录像
	返回0表示允许录像，返回非0表示不允许录像
*/
VD_BOOL CRecordManager::CheckCmos(int iChannel)
{
	//只要验证是否允许录像即可
	// m_RecControl是32位，每2位表示一个通道的控制方式
	if (((m_RecControl >> (2 * iChannel)) & 0x3) == CLOSED_REC)
	{
		return 1;				
	}

	return 0;
}
int CRecordManager::getLength(int channel)
{
	return m_pRecord[channel]->getFileLength() - consDHFileHead;
}

packetTypes CRecordManager::getPackType(int channel) const
{
	return m_pRecord[channel]->getPackType();
}

VD_BOOL CRecordManager::setInfo(void * pcontext, uint length, storageInfoTypes type, int iChannel)
{
	if ((iChannel < 0) || (iChannel >= g_nLogicNum))
	{
		return FALSE;
	}
	return m_pRecord[iChannel]->setInfo(pcontext, length, type);
}

void CRecordManager::onConfigGeneral(CConfigGeneral& cCfgGeneral, int& ret)
{
	_printd("g_DriverManager.GetSDStatus()[%d]",g_DriverManager.GetSDStatus());
	if(4 == g_DriverManager.GetSDStatus())
	{
		return ;  //SD卡出错直接返回
	}
	CONFIG_GENERAL &configOld = m_cCfgGeneral.getConfig();
	CONFIG_GENERAL &configNew = cCfgGeneral.getConfig();

#ifdef ENC_ENCODE_MULTI
	for (int i = 0; i < (int)((g_nCapture + m_DSPInfo.nMaxSupportChannel - 1) / m_DSPInfo.nMaxSupportChannel
		 * m_DSPInfo.nExpandChannel); i++)
	{
		m_pExtRecord[i]->setRecLen(configNew.iPacketLength);
	}
#endif

	if (&configOld == &configNew)
	{
		return;
	}

	m_SuspendMode = configNew.iSuspendMode;
	if(m_SuspendMode){
		return;
	}
		
	if (configNew.iOverWrite != configOld.iOverWrite)
	{
		CGuard l_cGuard(m_RecordCtrlMutex);
		if (configNew.iOverWrite == TRUE) 
		{
			if (!checkDisk())
			{
				for (int i = 0; i < g_nLogicNum; i++)
				{
					if (((m_RecControl >> (2 * i)) & 0x3) == MANUAL_REC)
					{
						//经过上面的条件后和CheckLimit相同
						StartPair(i, REC_MAN);
					}
				}
			}
		}
	}
	
	configOld = configNew;
}
//!g_nCapture代表的是本地编码通道个数，编码配置只是影响本地编码
void CRecordManager::onConfigEncode(CConfigEncode& cCfgEncode, int& iRet)
{
	for (int i = 0; i < g_nLogicNum; i++)
	{
		CONFIG_ENCODE& configOld = m_cCfgEncode.getConfig(i);
		CONFIG_ENCODE& configNew = cCfgEncode.getConfig(i);

		int encodeType = g_Encode.getEncodeType(i);

		m_pRecord[i]->setFileInfo(configNew.dstMainFmt[g_Encode.getEncodeType(i)]);
		
		if (&configOld == &configNew)
		{
			continue;
		}
		
		if (memcmp(&configOld.dstMainFmt[encodeType], &configNew.dstMainFmt[encodeType], sizeof(MEDIA_FORMAT)))
		{
			MEDIA_FORMAT* pnewFormat = &configNew.dstMainFmt[encodeType];
			//MEDIA_FORMAT* poldFormat = &configOld.dstMainFmt[encodeType];

			if ((pnewFormat->bVideoEnable == FALSE) && (pnewFormat->bAudioEnable == FALSE))
			{
				if (GetRecMode(i) >= REC_SYS)
				{
					//停止录像，由于REC_CLS里不关预录，所以再关预录
					StopPair(i, REC_CLS);
					StopPair(i, REC_SYS);			
				}
			}
			else
			{
				//取消视频选项，直接打包
				if (pnewFormat->bVideoEnable == FALSE)
				{
				    PackPair(i, packetForce);			
				}
				/*
				else
				{
					if ((pnewFormat->vfFormat.iCompression != poldFormat->vfFormat.iCompression)
						|| (pnewFormat->vfFormat.iResolution != poldFormat->vfFormat.iResolution) 
						|| (pnewFormat->vfFormat.iBitRateControl != poldFormat->vfFormat.iBitRateControl)				
						|| (pnewFormat->vfFormat.iQuality != poldFormat->vfFormat.iQuality)
						|| (pnewFormat->vfFormat.nFPS != poldFormat->vfFormat.nFPS))
					{
						m_pRecord[i]->Pack(packetChanged);
					}
					else
					{
						if (pnewFormat->bAudioEnable  != poldFormat->bAudioEnable)
						{
							m_pRecord[i]->Pack(packetAuto);
						}
					}
				}
				*/
			}
			if (m_cCfgRecord.getConfig(i).iPreRecord )
			{
				m_pRecord[i]->Start(REC_SYS);	
			}
		}
		configOld = configNew;
	}

}

void CRecordManager::onConfigRecord(CConfigRecord& cCfgRecord, int& iRet)
{

	for (int i = 0; i < g_nLogicNum; i++)
	{
		CONFIG_RECORD& configOld = m_cCfgRecord.getConfig(i);
		CONFIG_RECORD& configNew = cCfgRecord.getConfig(i);
		VD_BOOL bRedu = FALSE;
		if (configOld.iPacketLength != configNew.iPacketLength)
		{			
			if (getModePair(i) > REC_SYS)
			{
			    PackPair(i);			
			}
		}
		if (&configOld == &configNew)
		{
			bRedu = TRUE;
		}
		else
		{
			if (configNew.bRedundancy != configOld.bRedundancy)
			{
				bRedu = TRUE;			
			}
		}

		if (bRedu)
		{
			m_pRecord[i]->setRedundancy(configNew.bRedundancy);
		}

		ICapture::instance(i)->SetIFrameNum((int)configNew.iPreRecord);

		if (configNew.iPreRecord)
		{
			StartRec(REC_SYS, i);
		}
		else
		{
		//!第一次起来开启预录，目的是开启编码
		//!规避预录为0时，导致动检失效的BUG
			if (&configOld == &configNew)
			{
				StartRec(REC_SYS, i);
			}
			else
			{
				StopRec(REC_SYS, i);
			}
		}
		
		configOld = configNew;
	}

}
/*!
	保存录像控制模式
	手动录像:指在录像控制界面选择始终单选按钮，则为手动录像模式
	自动录像:指在录像控制界面选择配置单选按钮，则为自动录像模式
*/
void CRecordManager::AppRecordControl(uint64 RecCtrl)
{
	_printd("g_DriverManager.GetSDStatus()[%d]",g_DriverManager.GetSDStatus());
	if(4 == g_DriverManager.GetSDStatus())
	{
		return ;  //SD卡出错直接返回
	}
	uint64 old_val = m_RecControl;
	uint channel_val = 0;
	uint automatic_rec = 0;
	uint manual_rec = 0;
	uint closed_rec = 0;	
	// 修改会有一段录像存在的问题
	m_RecordCtrlMutex.Enter();
	for (int i = 0; i < g_nLogicNum; i++)
	{
		channel_val = (RecCtrl >> (2 * i)) & 0x3;
		if (((old_val >> (2 * i)) & 0x3) != channel_val)
		{
			switch(channel_val) 
			{
			case AUTOMATIC_REC:
				automatic_rec |= BITMSK(i);
				StopPair(i, REC_MAN);
				break;
			case MANUAL_REC:
				manual_rec |= BITMSK(i);	
				if (!checkDisk())
				{
				    StartPair(i, REC_MAN);			
				}
				
				break;
			case CLOSED_REC:
			    StopPair(i, REC_CLS);				
				closed_rec |= BITMSK(i);		
				break;
			default:
				//什么也不做
				break;
			}
		}		
	}
	// 修改cmos时，CRecordManager线程会启动定时录像，会抢占SaveRecordControl的写cmos。导致用户保存手动录像状态不成功
	m_RecControl = RecCtrl;
	m_RecordCtrlMutex.Leave();

	if(automatic_rec)
	{
		g_Log.Append(LOG_AUTOMATIC_RECORD, 0, &automatic_rec);
	}

	if(manual_rec)
	{
		g_Log.Append(LOG_MANUAL_RECORD, 0, &manual_rec);
	}

	if (closed_rec)
	{
		g_Log.Append(LOG_CLOSED_RECORD, 0, &closed_rec);
	}
}

/*!
	保存录像控制模式
	bLogSave是否需要写日志并且录像模式改变是否立即控制录像开关操作
*/
VD_BOOL CRecordManager::SaveRecordControl(uint64 RecCtrl, VD_BOOL bLogSave)
{
	uint64 CmosValue = RecCtrl;
	VD_BOOL bNeedWrited = FALSE;
	if (ValidateCMOS(&CmosValue))
	{
		infof("ValidateCMOS warning\r\n");
		return 1;
	}
	//mask　2位代表一个通道的录像控制, 0x00停止(关闭)，0x01手动(始终)，0x10自动(配置)
	if (CmosValue != m_RecControl)
	{
		if (bLogSave)
		{
			AppRecordControl(CmosValue);
		}
		else
		{
			m_RecControl = CmosValue;
		}
		bNeedWrited = TRUE;		
	}
	if (bNeedWrited)
	{
		CCMOS cmos;
		infof("save cmosvalue 0x%llx\r\n",CmosValue);
		cmos.Write(CMOS_MANUAL_REC, &CmosValue, 8);
	}
	return 0;
}

/*!
	获取录像控制模式
*/
uint64 CRecordManager::LoadRecordControl()
{
	return m_RecControl;
}

/*!
	验证CMOS中的录像控制模式的值
	返回0表示验证成功，非0表示失败，自动将录像模式置为自动模式
*/
int CRecordManager::ValidateCMOS(uint64 *dwCMOSValue)
{
	int ret = 0;
	for (int i = 0; i < g_nLogicNum; i++)
	{
		if ((((*dwCMOSValue) >> (2 * i)) & 0x3) >= CONTROL_REC)
		{
			// 验证出错，把值改成自动录像
			*dwCMOSValue &= ~(((uint64)0x3 )<< (2 * i));   //先清0
			*dwCMOSValue |=(((uint64) AUTOMATIC_REC)<<(2 * i));
			ret = 1;
		}
	}
	return ret;
}

//获取某录像通道相关的最高优先级的事件的回调函数
VD_BOOL recordChecker(const EVENT_HANDLER* handler, int index)
{
	if(handler->bRecordEn && (handler->dwRecord & BITMSK(index)))
	{
		return TRUE;
	}
	return FALSE;
}


void CRecordManager::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* pData)
{
    //printf("code <%d> index <%d> action  <%d>\n",code, index, action);
	if ((appEventUpgrade == code)
		&& (NULL != pData) && ((*pData)["State"].asString() == "Preparing"))
	{
		infof("CRecordManager::onApp stop.\n");
		/// 关闭录像功能
		Stop();
		return;
	}
	if ((appEventUpgrade == code) && (pData != NULL) 
		&& ((*pData)["State"].asString() == "Invalid" || (*pData)["State"].asString() == "Cancelled" 
			||(*pData)["State"].asString() == "NotEnoughMemory"
			||(*pData)["State"].asString() == "Succeeded"))
	{
		infof("CRecordManager::onApp start.\n");
		///升级失败，重启开启录像
		Start();
		return;
	}

	if (param == NULL)
	{
		return;
	}
	
	uint recMode = 0;
	CLinkage *pLinkageHead = NULL;
	switch (code)
	{
	case appEventAlarmLocal:
	case appEventAlarmNet:
	case appEventNetAbort:
		recMode = REC_ALM;
		pLinkageHead = m_cAlarmLinkage;
		break;

	case appEventVideoMotion:
	case appEventVideoLoss:
	case appEventVideoBlind:
		recMode = REC_MTD;
		pLinkageHead = m_cMotionLinkage;
		break;
	default:
		return;
	}

	int i = 0;
	CAPTURE_DSPINFO dspinfo;
	GetDspInfo(&dspinfo);
	//trace("Events(%s, %d)\n", CAppEventManager::instance()->getEventName(code), index);

	switch(action) 
	{
	case appEventStart:
		//trace("appEventStart\n");
		for (i = 0; i < g_nLogicNum; i++)
		{
			CLinkage *pLinkage = pLinkageHead + i;
			if (recordChecker(param, i))
			{
				//printf("\nindex = %d i = %d\n",index,i);
				pLinkage->start(code, index);
				StartRec(recMode, i);
			}
			else
			{
				if ((pLinkage->stop(code, index)) && (pLinkage->isEmpty()))
				{
					StopRec(recMode, i);
				}
			}
		}
		break;
	case appEventStop:
		//trace("appEventStop\n");
		for (i = 0; i < g_nLogicNum; i++)
		{
			CAppEventManager::instance()->latchEvent(this, (SIG_EVENT::SigProc)&CRecordManager::onAppEvent, code, index, param->iRecordLatch);
		}
		break;
	case appEventLatch:
		for(i = 0; i < g_nLogicNum; i++)
		{
			CLinkage *pLinkage = pLinkageHead + i;

			if ((pLinkage->stop(code, index)) && (pLinkage->isEmpty()) && (param->dwRecord & BITMSK(i)))
			{
				StopRec(recMode, i);
			}
		}
		break;
	default:
		trace("appEventUnknown\n");
		break;
	}
}


void CRecordManager::StopPreRecord()
{
    for (int i = 0; i < g_nLogicNum; i++)
    {
        ICapture::instance(i)->SetIFrameNum(0);
        StopRec(REC_SYS, i);
    }
}

uint CRecordManager::StartPair(int chn, uint arg)
{
    uint i = chn, ret = 0;

  	//!开启主码流录像
	if (m_pRecord[i]->Start(arg))
	{
		ret |= BITMSK(i);
		debugf("start record channel[%d], mainstream success!\n", i);
	}  

#ifdef ENC_ENCODE_MULTI
	if (arg > REC_SYS) 		// 开启对应的扩展通道
	{
		m_pExtRecord[i / m_DSPInfo.nMaxSupportChannel * m_DSPInfo.nExpandChannel]->Start();
	}
#endif	

    return ret;
}
uint CRecordManager::StopPair(int chn, uint arg)
{
    uint i = chn, ret = 0;
	//
	if (m_pRecord[i]->Stop(arg))
	{
		ret |= BITMSK(i);
	}

#ifdef ENC_ENCODE_MULTI
		if (arg > REC_SYS) // 开启对应的扩展通道
		{
			m_pExtRecord[i / m_DSPInfo.nMaxSupportChannel * m_DSPInfo.nExpandChannel]->Stop();
		}
#endif
    return ret;
}

VD_BOOL CRecordManager::PackPair(int chn, packetTypes types)
{
    VD_BOOL bRet =1;
#ifndef _FUN_RECORD_MULTISTREAM
    bRet &= m_pRecord[chn]->Pack();
#else
    if( m_ConfigMultiRecord.getConfig().streammain & BITMSK(chn) ) bRet &= m_pRecord[chn]->Pack();
    if( m_ConfigMultiRecord.getConfig().stream2snd & BITMSK(chn) ) bRet &=m_pRecord2[chn]->Pack();
#endif

    return bRet;    
}

uint CRecordManager::getModePair(int chn)
{
    return m_pRecord[chn]->getMode();
}

int CRecordManager::getPacketLenPair(int chn, SYSTEM_TIME *pTime)
{
    return m_pRecord[chn]->getPacketLen(pTime);
}
/****************************************/
