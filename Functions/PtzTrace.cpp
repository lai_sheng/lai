#include <unistd.h>
#include "Functions/General.h"
#include "Functions/PtzTrace.h"
#include "Functions/AiUploadManager.h"

PATTERN_SINGLETON_IMPLEMENT(PtzTrace);

PtzTrace::PtzTrace():CThread("PtzTrace", TP_TRACE)
{	
	#ifndef IPC_JZ
	m_CurPresetPoint = 0;
	m_PauseTraceFlg  = VD_FALSE;
	#endif
}

PtzTrace::~PtzTrace()
{
	#ifndef IPC_JZ
	DestroyThread();
	#endif
}

void PtzTrace::onConfigPtzTrace(CConfigPtzTrace& cCfg, int& ret)
{
	#ifdef IPC_JZ
		return ;
	#endif
	
	VD_BOOL	runFlag = VD_FALSE;
	
	CONFIG_TRACE &configOld = m_PtzTrace.getConfig();
	CONFIG_TRACE &configNew = cCfg.getConfig();

	configOld = configNew;

	mMutex.Enter();

	memcpy(&m_PtzTracePlan,&configNew,sizeof(CONFIG_TRACE));
	m_CurPresetPoint = 0;

	for(int i = 0;i < MAXNUM_POINT ;i ++){
		_printd("id:%d ,Enable:%d,x:%d,y:%d",i,m_PtzTracePlan.sPoint[i].Enable,
			m_PtzTracePlan.sPoint[i].x,m_PtzTracePlan.sPoint[i].y);
		_printd("id:%d ,Point:%d,StayTime:%d",i,m_PtzTracePlan.sPlan.iPointQueue[i],
			m_PtzTracePlan.sPlan.iStayTime[i]);
	}

	for(int i = 0 ; i < RUNPERIOD_NUM ; i ++){
		_printd("[%02d:%02d:%02d - %02d:%02d:%02d]",m_PtzTracePlan.sRunTime[i].startHour,
			m_PtzTracePlan.sRunTime[i].startMin,m_PtzTracePlan.sRunTime[i].startSec,
			m_PtzTracePlan.sRunTime[i].endHour,m_PtzTracePlan.sRunTime[i].endMin,
			m_PtzTracePlan.sRunTime[i].endSec);

		if(m_PtzTracePlan.sRunTime[i].endHour > 0 || 
			m_PtzTracePlan.sRunTime[i].endMin > 0 ||
			m_PtzTracePlan.sRunTime[i].endSec > 0 ){
			runFlag = VD_TRUE;
		}
	}

	if(VD_FALSE == m_PtzTracePlan.bRunTraceEnable){
		runFlag = VD_FALSE;
	}
	if(runFlag == VD_TRUE){
		//自动航巡 与 移动追踪 功能不能同时使能，故关闭移动追踪
		g_AlarmDetect.ForceCloseTracker();
	}
	
	mMutex.Leave();
}
void PtzTrace::onConfigCamera(CConfigCamera& cCfg, int& ret)
{
	#ifdef IPC_JZ
		return ;
	#endif
	
	CONFIG_CAMERA &configOld = m_CfgCamera.getConfig();
	CONFIG_CAMERA &configNew = cCfg.getConfig();

	configOld = configNew;

	memcpy(&m_cfgCameraParam,&configNew,sizeof(CONFIG_CAMERA));
}


//关闭该功能，清空航巡时间段配置
void PtzTrace::PtzTraceClose()
{
	#ifdef IPC_JZ
		return ;
	#endif
	
	_printd("clean up the cfg of ptz trace");
		
	m_PtzTrace.update();

	CONFIG_TRACE &config = m_PtzTrace.getConfig();

	config.bRunTraceEnable = VD_FALSE;
//	memset(config.sRunTime,0,RUNPERIOD_NUM*sizeof(config.sRunTime[0]));

	m_PtzTrace.commit();
}
void PtzTrace::PtzRunCmd(m_PtzControlCmd cmd,int presetId)
{
	#ifdef IPC_JZ
		return ;
	#endif

	m_PauseTraceFlg = VD_TRUE;
	
	switch(cmd)
	{
		case PTZRUN_STOP:
			PtzWrite(NULL, 0);
			m_PauseTraceFlg = VD_FALSE;
			break;
		case PTZRUN_UP:
			if(1 == m_cfgCameraParam.VerReverse){
				PtzWrite(NULL, 2);
			}else{
				PtzWrite(NULL, 1);
			}
			break;
		case PTZRUN_DOWN:
			if(1 == m_cfgCameraParam.VerReverse){  //
				PtzWrite(NULL, 1);
			}else{
				PtzWrite(NULL, 2);
			}
			break;
		case PTZRUN_LEFT://R2P2 Q1  right|left
			if(1 == m_cfgCameraParam.HorReverse){
				PtzWrite(NULL, 4);
			}else{
				PtzWrite(NULL, 3);
			}
			break;
		case PTZRUN_RIGHT:
			if(1 == m_cfgCameraParam.HorReverse){
				PtzWrite(NULL, 3);
			}else{
				PtzWrite(NULL, 4);
			}
			break;
		case PTZRUN_GOTO_MID:
			PtzWrite(NULL, 5);
			break;

		case PTZRUN_CRUISE:		
			PtzWrite(NULL, 6);	
			break;	
			
		case PTZRUN_GOTO_PRESET:
			if(presetId < 0 || presetId >= MAXNUM_POINT){
				_printd("presetId:%d error\n",presetId);
			}
			else{
				if(VD_TRUE == m_PtzTracePlan.sPoint[presetId].Enable){
					PtzRunToPrePoint(m_PtzTracePlan.sPoint[presetId].x,m_PtzTracePlan.sPoint[presetId].y);
				}
				else{
					_printd("presetId:%d is not exist\n",presetId);
				}
			}

			m_PauseTraceFlg = VD_FALSE;
			break;
		default:
			m_PauseTraceFlg = VD_FALSE;
			break;
	}
}
//返回值为 阻塞消耗时间
int PtzTrace::PtzRunToPrePoint(int x,int y)
{
	#ifdef IPC_JZ
		return 0;
	#endif
	
	int expendTime = 0;
	SetMotorPresetPos(x,y,&expendTime);
	_printd("arrived to x:%d,y:%d,spend %ds",x,y,expendTime);
	
	return expendTime;
}

void PtzTrace::Start()
{
	#ifdef IPC_JZ
		return ;
	#endif
	
	int iRet = 0;
	
	m_PtzTrace.attach(this, (TCONFIG_PROC)&PtzTrace::onConfigPtzTrace);
	m_PtzTrace.update();
	onConfigPtzTrace(m_PtzTrace,iRet);

	m_CfgCamera.attach(this, (TCONFIG_PROC)&PtzTrace::onConfigCamera);
	m_CfgCamera.update();
	onConfigCamera(m_CfgCamera,iRet);

	if(!m_bLoop){
		CreateThread();
	}
}
void PtzTrace::Stop()
{
	#ifdef IPC_JZ
		return ;
	#endif
	
	if(m_bLoop){
		DestroyThread();	
	}
}

VD_BOOL PtzTrace::IsMatchCondition()
{
	#ifdef IPC_JZ
		return VD_FALSE;
	#endif
	
	int i = 0;
	int iTmp  = 0;
	int iTmps = 0;
	int iTmpe = 0;
	VD_BOOL ret = VD_FALSE;
	SYSTEM_TIME sTime = {0};
		
	static char priCnt = 0;
	
	if(0 != g_General.GetSuspendStatus()){
		return VD_FALSE;
	}

	if(VD_TRUE == m_PauseTraceFlg){
		_printd("the ptz is operated by human");
		return VD_FALSE;
	}
	
	if(priCnt ++ > 10)	priCnt = 0;
	
	if(m_PtzTracePlan.bRunTraceEnable == VD_FALSE){
		if(priCnt > 10) _printd("the ptz trace is unable");
		return VD_FALSE;
	}
	
	SystemGetCurrentTime(&sTime);

	iTmp = sTime.hour << 16 | sTime.minute << 8 | sTime.second;
	
	for(i = 0 ; i < RUNPERIOD_NUM ; i ++ ){

		if(priCnt > 10){
			_printd("[%d][%02d:%02d:%02d - %02d:%02d:%02d]",i,m_PtzTracePlan.sRunTime[i].startHour,
				m_PtzTracePlan.sRunTime[i].startMin,m_PtzTracePlan.sRunTime[i].startSec,
				m_PtzTracePlan.sRunTime[i].endHour,m_PtzTracePlan.sRunTime[i].endMin,
				m_PtzTracePlan.sRunTime[i].endSec);
		}

		iTmps = m_PtzTracePlan.sRunTime[i].startHour << 16 |
				m_PtzTracePlan.sRunTime[i].startMin << 8  |
				m_PtzTracePlan.sRunTime[i].startSec;

		iTmpe = m_PtzTracePlan.sRunTime[i].endHour << 16 |
				m_PtzTracePlan.sRunTime[i].endMin << 8  |
				m_PtzTracePlan.sRunTime[i].endSec;

		if( iTmps > iTmp || iTmpe < iTmp){
			continue;
		}

		if(priCnt > 10){
			_printd("cur:%02d:%02d:%02d,satisfy",sTime.hour,sTime.minute,sTime.second);
		}
	
		ret = VD_TRUE;
		break;
	}
	


	return ret;
}

void PtzTrace::ThreadProc()
{
	#ifdef IPC_JZ
		return ;
	#endif
	int tmp	= 0;
	int mPoint = 0;
	int mTimeCnt = 0;

	CONFIG_TRACE tmp_PtzTracePlan;	
	
	while(m_bLoop){

		if(VD_FALSE == IsMatchCondition()){
			sleep(1);
			continue;
		}
		_printd("mTimeCnt:%d,m_CurPresetPoint:%d",mTimeCnt,m_CurPresetPoint);
		if(mTimeCnt <= 0){
			mMutex.Enter();
			memcpy(&tmp_PtzTracePlan,&m_PtzTracePlan,sizeof(CONFIG_TRACE));
			mMutex.Leave();
			
			if(m_CurPresetPoint < MAXNUM_POINT && tmp_PtzTracePlan.sPlan.iPointQueue[m_CurPresetPoint] >= 0){
				mTimeCnt = tmp_PtzTracePlan.sPlan.iStayTime[m_CurPresetPoint];
				mPoint	 = tmp_PtzTracePlan.sPlan.iPointQueue[m_CurPresetPoint];

				_printd("mPoint:%d,mTimeCnt:%d",mPoint,mTimeCnt);
				if(mPoint >= 0 && mPoint < MAXNUM_POINT && tmp_PtzTracePlan.sPoint[mPoint].Enable){
					tmp = PtzRunToPrePoint(tmp_PtzTracePlan.sPoint[mPoint].x,tmp_PtzTracePlan.sPoint[mPoint].y);
					if(tmp > 0) mTimeCnt -= tmp;
				}
			}
			else{
				mTimeCnt = 0;
			}

			m_CurPresetPoint ++;
			m_CurPresetPoint %= MAXNUM_POINT;
			
		}

		mTimeCnt --;
		sleep(1);
	}
}
