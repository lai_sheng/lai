

#include "Functions/AudioManager.h"
#include "APIs/Audio.h"
#include "APIs/Split.h"
//#include "Functions/Display.h"
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
#include "Configs/ConfigPlay.h"
#include "APIs/Ide.h"
#endif


#include "Configs/ConfigEncode.h"  //audio qjwang 091029  
// 保存声音信息的配置文件名
char 	FileAudioTipName[VD_MAX_PATH] = DATA_DIR"/audio_test.pcm";	
#define VOICEBUF 	1024*2
#define PUTSIZE 	320

PATTERN_SINGLETON_IMPLEMENT(CAudioManager);

CAudioManager::CAudioManager()
{
	m_dwAudioState  = 0;
	m_VoiceType		= -1;
	m_pDevAudio = CDevAudio::instance();
	if(!m_pDevAudio)
	{
		tracef(">>CAudioManager::CAudioManager:Create CDevAudio intance Error!!\n");
		return ;
	}
	trace(">>>>CAudioManager Start!!\n");
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	memset(m_cMusicFile,0,64);
	m_cMusicFile[0] = '\0';
	m_cTimer.Start(this,(VD_TIMERPROC)&CAudioManager::onTimer,20000,10000);
#endif

}
CAudioManager::~CAudioManager()
{
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	m_cTimer.Stop();
#endif
}

void CAudioManager::init()
{
	return;


	//Begin 此代码为语言提示功能，目前无用
	//注册事件处理类
	CAppEventManager::instance()->attach(this,(SIG_EVENT::SigProc)&CAudioManager::onAppEvent);
	memset(&m_StartAudioInfo,0,sizeof(StartAudioInfo));
	//End

#if defined(VN_IPC) 
	//audio qjwang 091029
	//读取并注册配置信息
	int iRet = 0;
	m_CcfgAudio.update();
	onConfigAudio(&m_CcfgAudio, iRet);
	m_CcfgAudio.attach(this, ((TCONFIG_PROC)&CAudioManager::onConfigAudio));
	//end of audio qjwang 091029
#endif
}

//各个模块开启语音时调用;
VD_BOOL CAudioManager::StartAudio(int iAudioType, int iPlayChannel,VD_BOOL bMute)
{
	CGuard	guard(m_AudioMutex);
	VD_BOOL bRet = FALSE;
	
	if(iAudioType<AUDIO_TALK_TYPE || iAudioType>AUDIO_MONITOR_TYPE)
	{
		tracef(">>CAudioManager::StartAudio::param iAudioType Error!\n");
		return bRet;
	}
	if(!m_pDevAudio)
	{
		tracef(">>CAudioManager::StartAudio:m_pDevAudio is NULL!\n");	
		return bRet;
	}
	m_StartAudioInfo[iAudioType].iChannel = iPlayChannel;
	m_StartAudioInfo[iAudioType].bMute=bMute;

	//设置状态位
	SetAudioState(iAudioType);
	
	if(IsAudioRunning(iAudioType))
	{
		switch(iAudioType)
		{
			case AUDIO_TALK_TYPE:
				bRet = TRUE;
				break;
			case AUDIO_TIP_TYPE:
				bRet = TRUE;
				break;
			case AUDIO_PLAY_TYPE:
				if(!bMute)
				{
					m_pDevAudio->Switch(AUDIO_SWITCH_PLAY, iPlayChannel);
				}
				else
				{
					m_pDevAudio->Switch(AUDIO_SWITCH_MUTE, iPlayChannel);
				}
				bRet = TRUE;
				break;
			case AUDIO_MONITOR_TYPE:
				if(!bMute)
				{
					m_pDevAudio->Switch(AUDIO_SWITCH_MONITOR, iPlayChannel);
				}else{
					m_pDevAudio->Switch(AUDIO_SWITCH_MUTE, 0);
				}
				bRet = TRUE;
				break;
			default:
				break;
		}
	}
	return bRet;
}

//关闭语音功能，同时完成恢复低优先级的语音需求
void CAudioManager::StopAudio(int iAudioType)
{
	int i=0,iNext=0;;

	if(iAudioType<AUDIO_TALK_TYPE || iAudioType>AUDIO_MONITOR_TYPE)
	{
		tracef(">>CAudioManager::StopAudio::param iAudioType Error!\n");
		return;
	}

	if (NULL != m_pDevAudio && iAudioType == AUDIO_TIP_TYPE)
	{
		m_pDevAudio->Stop();
	}
	
	//恢复状态位
	ResetAudioState(iAudioType);
	m_VoiceType = -1;
	
	m_StartAudioInfo[iAudioType].iChannel = 0;
	m_StartAudioInfo[iAudioType].bMute=0;

	//当优先级比AUDIO_MONITOR_TYPE高的时候才需要考虑恢复
	for (i = iAudioType; i < AUDIO_MONITOR_TYPE; i++)
	{
		iNext = i + 1;
		if (m_dwAudioState & BITMSK(iNext))
		{
			//恢复语音功能
			trace("Resume Audio::iNext = %d\n", iNext);
			StartAudio(iNext,m_StartAudioInfo[iNext].iChannel,m_StartAudioInfo[iNext].bMute);
		}
	}
}

//判断当前是否已经有优先级更高的事件在使用语音设备
//Return TRUE:当前没有更高优先级的事件
//		 FALSE:设备已经被更高优先级事件使用
VD_BOOL  CAudioManager::IsAudioRunning(int iAudioType)
{
	int  i = 0;
	VD_BOOL  ret = TRUE;
	for (i = AUDIO_TALK_TYPE; i < iAudioType; i++)
	{
		if (m_dwAudioState & BITMSK(i)) 
		{
			//ret = FALSE;
			return FALSE;
		}
		else
		{
			ret = TRUE;
		}		
	}

	return ret;
}	

void CAudioManager::SetAudioState(int iAudioType)
{
	m_dwAudioState |= BITMSK(iAudioType);
}	
void CAudioManager::ResetAudioState(int iAudioType)
{
	m_dwAudioState &= ~BITMSK(iAudioType);
}	
//触发语音提示
void CAudioManager::onAppEvent(appEventCode code, int index, appEventAction action, EVENT_HANDLER *param, const CConfigTable* data)
{
	if ((appEventUpgrade == code)
		&& (NULL != data) && ((*data)["State"].asString() == "Preparing"))
	{
		infof("CAudioManager::onApp Stop.\n");
		m_pDevAudio->Stop();
		return;
	}
	
	if ((NULL == param) || (code != appEventAlarmLocal))
	{
		return;
	}

	if ((action == appEventStart) && (param->bVoice))
	{
		//开启语音提示
		trace("Open Audio Tip:: index = %d\n", index);
		onAudioTip();		
		//语音提示结束，恢复状态位;同时恢复需要恢复的语音功能
		StopAudio(AUDIO_TIP_TYPE);
	}
}

//开启语音提示
void CAudioManager::onAudioTip()
{	
	uint dwLength = 0;
	uchar *pVoiceBuf = NULL; 
	
	if (m_pDevAudio == NULL)
	{
		return;
	}
	
	SetAudioState(AUDIO_TIP_TYPE);
	
	//开始语音提示，设置相应的状态位
	if (m_FileAudioTip.Open(FileAudioTipName, CFile::modeRead)) 
	{
		pVoiceBuf = (uchar *) malloc(VOICEBUF);
		
		Lock();		
		while((dwLength = m_FileAudioTip.Read(pVoiceBuf, PUTSIZE)) != 0)
		{
			if(!PlayVoice(pVoiceBuf, dwLength, AUDIO_TIP_TYPE))
			{
				trace("Play voice error\n");
				break;
			}
		}
		//为了保证语音提示播放完整
		SystemSleep(3500);
		UnLock();
		m_FileAudioTip.Close();
		free(pVoiceBuf);
	}
}

void CAudioManager::Lock()
{
	m_TipTalkMutex.Enter();
}

void CAudioManager::UnLock()
{
	m_TipTalkMutex.Leave();
}

VD_BOOL CAudioManager::PlayVoice(uchar *data, int length, int type, int format)
{
    enum
    {
        AUDIO_FORMAT_PCM8 = 1,
        AUDIO_FORMAT_G711a = 2,
        AUDIO_FORMAT_AMR = 3,
        AUDIO_FORMAT_G711u = 4,
        AUDIO_FORMAT_G726 = 5,		
        AUDIO_FORMAT_IMAADPCM = 6
    };

    if (type < AUDIO_TALK_TYPE || type > AUDIO_TIP_TYPE)
    {
        return FALSE;
    }

    if (type != m_VoiceType)
    {
        tracepoint();
        m_pDevAudio->Stop();

        int iFormat = G711_ALAW;
        switch(format)
        {
        case AUDIO_FORMAT_PCM8:
        	iFormat = PCM_ALAW;
        	//iFormat = PCM_8TO16BIT;
        	break;
        case AUDIO_FORMAT_G711a:
        	iFormat = G711_ALAW;
        	break;
        case AUDIO_FORMAT_AMR:
        	iFormat = AMR8K16BIT;
        	break;
        case AUDIO_FORMAT_G711u:
        	iFormat = G711_ULAW;
        	break;
        case AUDIO_FORMAT_G726:
        	iFormat = G726_16KBIT;
        	break;
        case AUDIO_FORMAT_IMAADPCM:
        	iFormat = IMA_ADPCM_8K16BIT;
        	break;
        default:			
        	iFormat = PCM_ALAW;
        	//iFormat = PCM_8TO16BIT;
        	break;			
        }		
        m_pDevAudio->SetFormat(iFormat);
        trace("AudioOutSetFormat::%d\n", iFormat);
        m_VoiceType = type;

#ifdef VN_IPC
        m_pDevAudio->Switch(1, 9);		// 1--切换到回放输出;9--回放的通道号
#else		
        StartAudio(type, g_nCapture+1);	//modified by qjwang 091224 
        m_pDevAudio->Switch(AUDIO_SWITCH_SPEAK, g_nCapture+1);		//modified by qjwang 091224 调用API函数，Audio为对讲模式
#endif

        if (!m_pDevAudio->Start())
        {
            return FALSE;
        }
    }
    m_pDevAudio->PutBuf(data, length);
    return TRUE;
}

	//audio qjwang 091029 改变音频配置的操作
void CAudioManager::onConfigAudio(CConfigAudioInFormat* pConfig,int& ret)
{
    CONFIG_AUDIOIN_FORMAT &cfgAudio = pConfig->getConfig(0);
    int iSilence = cfgAudio.Silence;
    //系统开机时是静音模式，则调用系统函数使系统静音
    if(TRUE == iSilence)
    {
        SystemSilence(TRUE);
    }
    else
    {
        SystemSilence(FALSE);
    }
    
} 	//end audio qjwang 091029

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
void CAudioManager::onTimer(uint arg)
{	
	SYSTEM_TIME systemtime;
	SystemGetCurrentTime(&systemtime);	
	CConfigMusicPlan* pMusicplan = new CConfigMusicPlan();
    pMusicplan->update();
	for(int i = 0; i < 6; i++)
	{
    	CONFIG_MUSICPLAN& cfgMusicplan = pMusicplan->getConfig(i);
		if(0 == i)
		{
			m_iVolume = cfgMusicplan.iVolume;
		}

		if(cfgMusicplan.iEnable)//计划使能
		{
			if(((1 << systemtime.wday) & cfgMusicplan.iWeekDay) //计划时间 
				&& ((systemtime.hour*60 + systemtime.minute) 
				>= (cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute))
				&& ((systemtime.hour*60 + systemtime.minute) 
				< (cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute + cfgMusicplan.iPlayCount))) //在播放时间内都播放
			{
				///  1  播放状态
				///  0  停止状态
				if ( AudioPlayAudioStatus())
				{
					if ( m_cMusicFile[0] != '\0')
					{
						if(!strncmp(m_cMusicFile,cfgMusicplan.strMusicFileName,strlen(m_cMusicFile)))//播放是否是当前这首
						{
							/*m_iMiniute = cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute + cfgMusicplan.iPlayCount 
								- (systemtime.hour*60 + systemtime.minute) + 1; *///防止计划重叠不播放
							m_iIndex =  i;	
							continue;
						}
						else
						{
							int iMiniute = pMusicplan->getConfig(m_iIndex).iHour*60 + pMusicplan->getConfig(m_iIndex).iMiniute 
										+ pMusicplan->getConfig(m_iIndex).iPlayCount + 1 - (systemtime.hour*60 + systemtime.minute);
							if(iMiniute > 0) //计划重叠文件名不同
							{
								continue;
							}	
						}
						
					}
					AudioPlayAudioStop();
				}								
				int ii=0,iRet = 0;
				IDE_AUDIO_FILE AudioFile[16];
				iRet = IdeGetAudiofiles(IdeGetAudiofileNum(), AudioFile);		
				if(iRet > 0)
				{
					for(ii = 0; ii < iRet; ii++)	
					{						
						if(!strncmp(cfgMusicplan.strMusicFileName,AudioFile[ii].name,strlen(cfgMusicplan.strMusicFileName)))
						{
							AudioPlayIdeAudioStart(AudioFile[ii], 1);
							//AudioOutSetVolume( 0, iVolume, 0);
							m_iIndex =  i;
							memcpy(m_cMusicFile, cfgMusicplan.strMusicFileName, 64); //当前播放文件名
							/*m_iMiniute = cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute + cfgMusicplan.iPlayCount 
										 - (systemtime.hour*60 + systemtime.minute) + 1; //播放的剩余时间*/
							m_cMusicTimer.Start(this,(VD_TIMERPROC)&CAudioManager::onMusicPlayTime,30000,30000);
							AudioOutSetVolume( 0, m_iVolume, 0);							
							break;
						}
						
					}
					
				}
				if(0 == iRet || ii == iRet)
				{
					_printd(" FILE: %s is not exist !!!!!",cfgMusicplan.strMusicFileName);
				}
				
			}
		}/*end if(cfgMusicplan.iEnable)*/
	}/*end for()*/
	delete pMusicplan;
	
}
void CAudioManager::onMusicPlayTime(uint arg)
{
/*	//m_iMiniute--;
	if(m_iMiniute <= 0)
	{
		m_cMusicTimer.Stop();
		if ( AudioPlayAudioStatus())
		{
			AudioPlayAudioStop();
			m_cMusicFile[0] = '\0';
		}
	}
	_printd("\nplay time remain is %d\n",m_iMiniute);
*/
	SYSTEM_TIME systemtime;
	SystemGetCurrentTime(&systemtime);	
	CConfigMusicPlan* pMusicplan = new CConfigMusicPlan();
	pMusicplan->update();
	CONFIG_MUSICPLAN& cfgMusicplan = pMusicplan->getConfig(m_iIndex);
	if((!cfgMusicplan.iEnable) || (!((1 << systemtime.wday) & cfgMusicplan.iWeekDay))
		|| ((systemtime.hour*60 + systemtime.minute) 
			< (cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute))  
		|| ((systemtime.hour*60 + systemtime.minute) 
			> (cfgMusicplan.iHour*60 + cfgMusicplan.iMiniute + cfgMusicplan.iPlayCount)))
		{
			m_cMusicTimer.Stop();
			if ( AudioPlayAudioStatus())
			{
				AudioPlayAudioStop();
				m_cMusicFile[0] = '\0';
			}
		}
	delete pMusicplan;
}

#endif

