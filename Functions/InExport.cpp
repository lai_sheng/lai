//////////////////////////////////////////////////////////////////////////
/// 配置导入导出功能类，供网络和本地GUI调用，方便后续添加新需求和维护
/// 
//////////////////////////////////////////////////////////////////////////

#include <string.h>

#include "Functions/InExport.h"
#include "APIs/System.h"
#include "APIs/Video.h"

#include "ez_libs/ezutil/crc.h"

#ifdef WIN32
#undef OEM_TYPE
#define OEM_TYPE "VS"
#endif

static char* confFileArray[CONFIG_MAX] = {
	CONFIG_DIR"/ppp/pppoe-enable",
	CONFIG_DIR"/dhcp.cfg",	
	CONFIG_DIR"/Account1",
	CONFIG_DIR"/Account2",
	CONFIG_DIR"/Config1",
	CONFIG_DIR"/Config2",
	CONFIG_DIR"/DigiChConfig1",
	CONFIG_DIR"/DigiChConfig2",
	CONFIG_DIR"/network",
	CONFIG_DIR"/resolv.conf"
	};
	
static int confFileIntArray[CONFIG_MAX] = {
	CONFIG_PPPOE_ENABLE,
	CONFIG_DHCP_CONFIG,//modified by wyf on 091125
	CONFIG_ACCOUNT1,
	CONFIG_ACCOUNT2,
	CONFIG_CONFIG1,
	CONFIG_CONFIG2,
	CONFIG_DIGICHCONFIG1,
	CONFIG_DIGICHCONFIG2,
	CONFIG_NETWORK,
	CONFIG_RESOLV_CONF,	
	};

PATTERN_SINGLETON_IMPLEMENT(CInExport);

CInExport::CInExport() : m_Mutex(MUTEX_FAST)
{
	tracef("CInExport::CInExport Create.\n");
}

CInExport::~CInExport()
{
	tracef("CInExport::~CInExport Destroy.\n");
}

int CInExport::ExportConfig(int type, CPacket *pPacket)
{
	CGuard guard(m_Mutex);

	FILE *fp = NULL;
	uchar *pcTempBuf = NULL;
	int iReadLen = 0;
	int iSunLen = 0;
	int iFileNum = 0;//导出的配置文件数目
		
	uchar *pPacketBuf = NULL;
	if (NULL == pPacket)
	{
		errorf("CInExport::ExportConfig pPacket == NULL.\n");
		return -1;	///< 内存分配失败
	}
	pPacketBuf = pPacket->GetBuffer();	
	pcTempBuf = pPacketBuf + enum_crchead_len + enum_filehead_len
					+ enum_vision_len + enum_languagevideo_len;//该指针初始化指向第一个配置文件头

	for(int i = 0;i < CONFIG_MAX;i++)
	{
		CONFFILEHEADER conf_header;
		conf_header.type = confFileIntArray[i];//配置文件类型

		//获取配置文件长度
		fp = fopen(confFileArray[i],"rb");
		if(fp == NULL)
		{
			//if(confFileIntArray[i] == CONFIG_ACCOUNT2)
			{
				continue;//因为最初始的时候account2配置文件可能不存在
			}

			/// 只要有一个文件打开出错，就认为是失败了
			//return -2; ///< 文件不全，失败
		}
		long fileLen = 0;
		fseek(fp,0,SEEK_END);
		fileLen = ftell(fp);
		fseek(fp,0,SEEK_SET);
		
		conf_header.len = (int)fileLen;//long转换成int
		memcpy(pcTempBuf,&conf_header,sizeof(conf_header));//将某个配置文件类型、长度拷贝到配置缓冲区
		pcTempBuf += sizeof(conf_header);
		iSunLen += sizeof(conf_header);
		iReadLen = fread(pcTempBuf,fileLen,sizeof(char),fp);
		pcTempBuf += fileLen;
		iSunLen += fileLen;
		iFileNum++;
		fclose(fp);
		fp = NULL;
	}
	pcTempBuf = NULL;
	iSunLen += enum_filehead_len + enum_vision_len + enum_languagevideo_len;//现在iSunLen里存放的是除CRC所占有的四个字节之外的所有配置缓冲区的长度

	memcpy(pPacketBuf + 4,&iSunLen,4);//把除CRC校验的四个字节之外的文件中长度存下来
	memcpy(pPacketBuf + 8,&iFileNum,4);
	memcpy(pPacketBuf + 12, OEM_TYPE, strlen(OEM_TYPE));//对OEM_TYPE实
	
#ifndef WIN32	
	int crcValue = ez_crc32((unsigned char*)(pPacketBuf + 4), iSunLen);
	memcpy(pPacketBuf,&crcValue,4);//把CRC校验值的四个字节拷贝到配置缓冲区
#endif//WIN32

	return (iSunLen + enum_crchead_len);	///< 返回总长度
}

int CInExport::InportConfig(int type, CPacket* pPacket, int iRecvDataLen)
{
	CGuard guard(m_Mutex);
	if ((NULL == pPacket) || (0 == iRecvDataLen))
	{
		return -1;
	}

	char* pBuf = (char*)pPacket->GetBuffer();

	int inFileTotalLen = 0;//除CRC占用的4个字节
	memcpy(&inFileTotalLen, pBuf + 4, 4);
	
#ifndef WIN32	
	int crcCheck = -1;
	if(iRecvDataLen == (inFileTotalLen + enum_crchead_len))//modified by wyf on 20100113解决导入非法文件DVR会死机的问题
	{
  		crcCheck = ez_crc32((unsigned char *)(pBuf + 4), inFileTotalLen);
	}
	else
	{
		return -1;
	}
#else
	int crcCheck = 0;
#endif//WIN32	

	int crcCheckSrc = 0;
	memcpy(&crcCheckSrc, pBuf, 4);
	//文件校验失败
	if(crcCheck != crcCheckSrc)
	{
		return -1;
	}

	//文件校验成功，则开始导入文件		
	char *exceptHead = pBuf + enum_crchead_len + enum_filehead_len + enum_vision_len + enum_languagevideo_len; 
	FILE *fp = NULL;
	int iFileLen = 0;
	int iFileSumLen = 0; 
	int iFileType = 0;
	int iSumLen = 0;
	int iFileNum = 0;

	char acVersion[enum_vision_len];
	memset(acVersion, 0, enum_vision_len);
	memcpy(acVersion, pBuf + enum_crchead_len + enum_filehead_len , enum_vision_len);

	//版本不同
	if(memcmp(acVersion, OEM_TYPE, strlen(OEM_TYPE)) != 0)//比较的长度改为strlen(OEM_TYPE)，否则比较长度
	{
		return -2;
	}

	memcpy(&iFileSumLen, pBuf + enum_crchead_len,4);
	memcpy(&iFileNum, pBuf + enum_crchead_len + 4,4); 
	//判断文件中保存的文件个数和文件长度是否有效
	if ((iFileNum > CONFIG_MAX) || ((iFileSumLen + enum_crchead_len) > iRecvDataLen))
	{
		return -3;
	}
	iFileSumLen -= enum_filehead_len - enum_vision_len - enum_languagevideo_len;

	for(int i = 0; i < iFileNum; i++)
	{
		memcpy(&iFileType,exceptHead,4);
		if((iFileType >= CONFIG_MAX) || (iFileType < 0) )
		{
			return -4;
		}
		exceptHead += 4;
		
		iFileLen = 0;
		memcpy(&iFileLen,exceptHead,4);
		iSumLen += iFileLen + sizeof(CONFFILEHEADER);
		if(iSumLen > iFileSumLen)
		{
			return -5;
		}
		
		fp = fopen(confFileArray[iFileType],"w");
		if(NULL == fp)
		{
			return -6;
		}
		exceptHead += 4;		
		fwrite(exceptHead, 1, iFileLen,fp); 	
		exceptHead += iFileLen; 	
		
		fclose(fp);
		fp = NULL;				
	}
	exceptHead = NULL;


	return 0;
}


