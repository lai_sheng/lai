

#include "Configs/ConfigLocation.h"
#include "Functions/Daylight.h"
#include "Functions/General.h"
#include "System/AppConfig.h"
#include <time.h>

PATTERN_SINGLETON_IMPLEMENT(CDaylight);

CDaylight::CDaylight() : m_Timer("Daylight")
{
    memset(&m_LastTime, 0, sizeof(SYSTEM_TIME)); //触发开机时定时器调用PrepareTime()
}

CDaylight::~CDaylight()
{
      
}

void CDaylight::Start()
{    
    trace("CDaylight::Start()>>>>>>>>>>>>>>>>>>>>\n");
    int iRet = 0;
    m_bTuned = g_Cmos.ReadFlag(CMOS_DST_TUNE);
//ZHY 20150527
	m_bTuned = TRUE;//夏令时第一次使能无效
    m_CcfgLocation.update();
    onConfigLocationChange(&m_CcfgLocation, iRet);
    m_CcfgLocation.attach(this, ((TCONFIG_PROC)&CDaylight::onConfigLocation));

}

void CDaylight::onConfigLocation(CConfigLocation* pConfig, int& ret)
{
    /* 夏令时如果没有修改的话，不进行夏令时相关操作 */
    if (m_CcfgLocation.getConfig().iDSTRule != pConfig->getConfig().iDSTRule
        || 0 != memcmp(&m_CcfgLocation.getConfig().iDST[0],&pConfig->getConfig().iDST[0],2*sizeof(DST_POINT)))
    {
        m_CcfgLocation.update();
        onConfigLocationChange(pConfig,ret);
        return;
    }

    m_CcfgLocation.update();
}

void CDaylight::onConfigLocationChange(CConfigLocation* pConfig, int& ret)
{
    SYSTEM_TIME stCurTime;
    //process time
    SystemGetCurrentTime(&stCurTime);
    PrepareTime(stCurTime.year);
    if (pConfig->getConfig().iDSTRule)
    {
        if (!m_Timer.IsStarted())
        {
            m_Timer.Start(this, (VD_TIMERPROC)&CDaylight::OnTimer, 0, 1000 * 2);
        }
        
        //夏令时开关一直打开，只设置时间更改的情况modified by qjwang 091012
        else
        {
            //复制下面夏令时从打开到关闭的操作
            m_Timer.Stop();
        
            //if(m_bNorth)
            //{
                if(m_bTuned == FALSE)//恢复为正常时间
                {
                    timeadd(&stCurTime, &stCurTime, -3600);
                    SystemSetCurrentTime(&stCurTime);
                    m_bTuned = TRUE;
                    g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
                }
            //}
            /*else
            {
                if(m_bTuned == TRUE)
                {
                    timeadd(&stCurTime, &stCurTime, 3600);
                    SystemSetCurrentTime(&stCurTime);
                    m_bTuned = FALSE;
                    g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
                }
            }*/

            //复制上面夏令时从关闭到打开的操作
            /* SystemSetCurrentTime后马上SystemGetCurrentTime不能读到正确的值，这里定
                          时器延时100ms启动 */
            m_Timer.Start(this, (VD_TIMERPROC)&CDaylight::OnTimer, 100, 1000 * 2);
        }
        //end of modification by qjwang 091012
    }
    else
    {
        if(m_Timer.IsStarted())
        {
            m_Timer.Stop();
        
            //if(m_bNorth)
            //{
                if(m_bTuned == FALSE)//恢复为正常时间
                {
                    timeadd(&stCurTime, &stCurTime, -3600);
                    SystemSetCurrentTime(&stCurTime);
                    m_bTuned = TRUE;
                    g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
                }
            //}
            /*else
            {
                if(m_bTuned == TRUE)
                {
                    timeadd(&stCurTime, &stCurTime, 3600);
                    SystemSetCurrentTime(&stCurTime);
                    m_bTuned = FALSE;
                    g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
                }
            }*/
        }
    }    
}

//随时检测夏令时
void CDaylight::OnTimer(uint par)
{
    SYSTEM_TIME stCurTime;
    VD_BOOL flag;

    SystemGetCurrentTime(&stCurTime);
    //trace("+++CDaylight::OnTimer: y:%d h:%d m:%d s:%d\n",stCurTime.year,stCurTime.hour, stCurTime.minute, stCurTime.second);
    if(stCurTime.year != m_LastTime.year) //年变化， 重新准备转换时刻
    {
        if (PrepareTime(stCurTime.year))
        {
            m_Timer.Stop();
        }
    }

    //仔细读下面的代码，显然上面的注释的意思是6月是夏令时，
    //但事实上，应该是调快了一个小时才能称为夏令时//noted by qjwang 091108
    if(m_bTuned)
    {
        if(m_bNorth)
        {
            flag = timecompare(&stCurTime, &m_DecreasedTime) < 0
                && timecompare(&stCurTime, &m_IncreasingTime) >= 0;
            //trace("+CDaylight::OnTimer+flag:%d m_IncreasingTime:y:%d h:%d :m:%d s:%d m_DecreasedTime:y:%d h:%d :m:%d s:%d\n",flag, 
                //m_IncreasingTime.year, m_IncreasingTime.hour, m_IncreasingTime.minute,m_IncreasingTime.second,
                //m_DecreasedTime.year, m_DecreasedTime.hour, m_DecreasedTime.minute,m_DecreasedTime.second);
        }
        else
        {
            flag = timecompare(&stCurTime, &m_DecreasedTime) < 0
                || timecompare(&stCurTime, &m_IncreasingTime) >= 0;
        }
        if(flag) //调快一小时
        {
            timeadd(&stCurTime, &stCurTime, 3600);

            g_General.UpdateSystemTime(&stCurTime);

            m_bTuned = FALSE;
            g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
        }
    }
    else
    {
        if(m_bNorth)
        {
            flag = timecompare(&stCurTime, &m_IncreasedTime) < 0
                || timecompare(&stCurTime, &m_DecreasingTime) >= 0;
        }
        else
        {
            flag = timecompare(&stCurTime, &m_IncreasedTime) < 0
                && timecompare(&stCurTime, &m_DecreasingTime) >= 0;
        }
        if(flag) //调慢一小时
        {
            timeadd(&stCurTime, &stCurTime, -3600);
            g_General.UpdateSystemTime(&stCurTime);
            m_bTuned = TRUE;
            g_Cmos.WriteFlag(CMOS_DST_TUNE, m_bTuned);
        }
    }

    m_LastTime = stCurTime;
}

void CDaylight::prepareDSTtime(SYSTEM_TIME* pDesTime, DST_POINT* pDST, int year)
{
    memset(pDesTime, 0, sizeof(SYSTEM_TIME));
    if (pDST->iWeek == 0) // 按天计算
    {
        //按天计算的年需要校验
        pDesTime->year = pDST->iYear;
        pDesTime->month = pDST->iMonth;
        pDesTime->day = pDST->iDays;
    }
    else
    {
        SYSTEM_TIME sysTime;
        memset(&sysTime, 0, sizeof(SYSTEM_TIME));
        sysTime.year = year;
        sysTime.day = 1; //1号
        if (pDST->iWeek > 0)
        {
            //正数第几周,从第一天开始算
            sysTime.month = pDST->iMonth;
            *pDesTime = sysTime;
        }
        else 
        {
             //因为12月计算最后一周的时候会出来13月，在计算系统时间的时候会出错，所以分开处理。modified by qjwang 091021
            if (pDST->iMonth<12)
            {
                //从下月开始计算
                sysTime.month = pDST->iMonth + 1;
                timeadd(&sysTime,pDesTime, -1);
            }
            else
            {
                sysTime.year += 1;
                sysTime.month = 1;
                timeadd(&sysTime, pDesTime, -1);
            }
        }

        struct tm t;
        memset(&t, 0, sizeof(tm));
        t.tm_year = year - 1900;

        t.tm_mday = pDesTime->day;
        t.tm_mon = pDesTime->month - 1;
        mktime(&t);

        if (pDST-> iWeek> 0)
        {

            if (pDST->iWeekDay < t.tm_wday)
            {//计算上一周的
                pDesTime->day = t.tm_mday + pDST->iWeek * 7 + (pDST->iWeekDay - t.tm_wday);
                //有可能无效
            }
            else
            {
                pDesTime->day = t.tm_mday + (pDST->iWeek - 1) * 7 + (pDST->iWeekDay - t.tm_wday);
            }
        }
        else
        {
            //最后一周
            //转到前一天
            if (pDST->iWeekDay <= t.tm_wday)
            {
                pDesTime->day = t.tm_mday + (pDST->iWeek + 1) * 7 - (t.tm_wday - pDST->iWeekDay);
            }
            else
            {//计算下一周的
                pDesTime->day = t.tm_mday + pDST->iWeek * 7 + (pDST->iWeekDay - t.tm_wday);
            }
        }
    }
    pDesTime->hour = pDST->time.Hour;
    pDesTime->minute = pDST->time.Minute;
    pDesTime->second = 0;// 用周计算时会改变为59,所以在此重新设置
    //trace("\r\n++++++++preparedsttime:month:%d old:week:%d weekday:%d new:day:%d hour:%d min:%d\n",
        //pDST->iMonth, pDST->iWeek, pDST->iWeekDay, pDesTime->day,pDesTime->hour, pDesTime->minute);
}

//准备好当年的转换时刻
int CDaylight::PrepareTime(int year)
{
    int iRet = 1;
    
    CONFIG_LOCATION cfglocation = m_CcfgLocation.getConfig();
    prepareDSTtime(&m_IncreasingTime, &cfglocation.iDST[0], year);
    prepareDSTtime(&m_DecreasingTime, &cfglocation.iDST[1], year);

    m_bNorth = (timecompare(&m_IncreasingTime, &m_DecreasingTime) < 0);
    //按天表示，判断有效期    
    timeadd(&m_IncreasingTime, &m_IncreasedTime, 3600);
    timeadd(&m_DecreasingTime, &m_DecreasedTime, -3600);
    if ((m_IncreasingTime.year <= year) && (m_DecreasingTime.year >= year))
    {
        iRet = 0;
    }
    return iRet;
}
