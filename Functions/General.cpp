#include "Functions/General.h"
#include "Functions/Record.h"
#include "Functions/Comm.h"
#include "Main.h"
#include "System/AppConfig.h"
#include "Devices/DevExCapture.h"
#include "Functions/Daylight.h"
#include "Devices/DevVideo.h"
#include "Configs/ConfigPlay.h"
#include "Devices/DevSplit.h"
#include "System/Support.h"

PATTERN_SINGLETON_IMPLEMENT(CGeneral);

CGeneral::CGeneral() : m_DetectTimer("DetectTimer")
{
	trace("CGeneral::CGeneral()>>>>>>>>>\n");
}

CGeneral::~CGeneral()
{
}

VD_BOOL CGeneral::Start()
{
	trace("CGeneral::Start()>>>>>>>>>\n");
	int iRet = 0;
	
	m_CCfgLocation.update();

	//设置所有设备的制式
    CONFIG_LOCATION &cfgLocation = m_CCfgLocation.getConfig();

	int videoMode = SystemGetVideoMode();

       printf("cfgLocation.iVideoFormat = %d videoMode = %d\r\n",
               cfgLocation.iVideoFormat,videoMode);
       
    //部分产品不保存直接返回的PAL
    if  (cfgLocation.iVideoFormat != videoMode) 
    {
        if (VIDEO_STANDARD_NTSC == videoMode)
        {
            __trip;
			cfgLocation.iVideoFormat = videoMode;
			m_CCfgLocation.commit();
			m_CCfgLocation.update();        
			printf("2222cfgLocation.iVideoFormat = %d videoMode = %d\r\n",
			       cfgLocation.iVideoFormat,videoMode);
        }
        else
        {
            __trip;
            SystemSetVideoMode(VIDEO_STANDARD_NTSC);
        }
    }
        
	int iVSD = cfgLocation.iVideoFormat;

#ifdef GRAPHICS_SUPPORT_SETRESOLUTION
	m_configGUISet.update();
#endif

	for (int i = 0; i < g_nVideoOut; i++)
	{
		CDevSplit::instance(i)->SetVideoStandard(iVSD);
		CDevVideo::instance(i)->SetVstd(0, iVSD);
	}

	for (int i = 0; i < g_nCapture; i++)
	{
		CDevCapture::instance(i)->SetVstd(iVSD);
	}
	CDevExCapture::instance(0)->SetVstd(iVSD);

	//CConfigPlay ConfigPlay;
	//ConfigPlay.update();

	//ConfigPlay[0].iChannels = g_nPlay;
	//ConfigPlay.commit();
	//for (int i = 0; i < g_nPlay; i++)//ipcam 无本地回放
	//{
	//	CDevPlay::instance()->SetVstd(i, iVSD);
	//}
	//开启定时器检测时间
	SYSTEM_TIME systime;
	SystemGetCurrentTime(&systime);
	timesys2dh(&m_DHTime, &systime);

	m_CCfgLocation.attach(this, (TCONFIG_PROC)&CGeneral::onConfigLocation);

	//为了让录像对象创建成功，多延迟一点
	m_DetectTimer.Start(this, (VD_TIMERPROC)&CGeneral::OnDetect, 100000, 1000);

/// 初始化
#if defined(DEF_RESOLUTION_ADJUST)
	int iEeType = ENUM_VIDOUT_1024x768p60;

	CConfigGeneral general;
	general.update();
	CONFIG_GENERAL &cGeneral = general.getConfig();
	iEeType = cGeneral.iFixType;	
	infof("Set Pixel(0--1080p, 1--1280*1024, 2--720p, 3--1024*768) type is %d.\n", iEeType);
	SystemSetVGAPixel(iEeType);	
#endif

	m_cCfgGeneral.attach(this, (TCONFIG_PROC)&CGeneral::onConfigGeneral);
	m_cCfgGeneral.update();
	onConfigGeneral(m_cCfgGeneral,iRet);
	
	return TRUE;
}

VD_BOOL CGeneral::Stop()
{
	trace("CGeneral::Stop()>>>>>>>>>\n");
	return TRUE;
}

/*!
	\b Description		:	当改变General配置时，通知录像\n
	\b Argument			:	CONFIG_GENERAL* pConfig, int& ret
	\param[in]			:
	\param[in]			:
	\param[out]			:
	\return				

	\b Revisions		:	
*/


void CGeneral::onConfigLocation(CConfigLocation& cCfgLocation, int& ret)
{
	CONFIG_LOCATION &configOld = m_CCfgLocation.getConfig();
	CONFIG_LOCATION &configNew = cCfgLocation.getConfig();

	if (configNew.iLanguage != configOld.iLanguage)
	{	
	}
	
	if (configNew.iVideoFormat != configOld.iVideoFormat)
	{

	}

	configOld = configNew; 
}
void CGeneral::onConfigGeneral(CConfigGeneral& cCfg, int& ret)
{
	CONFIG_GENERAL &configOld = m_cCfgGeneral.getConfig();
	CONFIG_GENERAL &configNew = cCfg.getConfig();

	configOld = configNew; 

	if(configNew.iSuspendMode){
		g_Challenger.Suspend();
		m_iSuspend = 1;
	}
	else{
		g_Challenger.Resume();
		m_iSuspend = 0;
	}
}

int CGeneral::GetSuspendStatus()
{
	return m_iSuspend;
}

VD_BOOL CGeneral::UpdateSystemTime(SYSTEM_TIME *systime, int iToleranceSeconds /* = 0 */)
{
 	SYSTEM_TIME tm;
 	SystemGetCurrentTime(&tm); 
	trace("old_time is %04d-%02d-%02d %02d:%02d:%02d, new_time is %04d-%02d-%02d %02d:%02d:%02d\n",
			tm.year, tm.month, tm.day, tm.hour, tm.minute, tm.second, systime->year, systime->month, systime->day,
			systime->hour, systime->minute, systime->second);

	if(ABS(time2second(systime, &tm)) > iToleranceSeconds+1) //允许systime与tm存在一秒的误差
	{
		uint64 oldparam = g_Record.LoadRecordControl();
		uint64 manual_param = 0;
		for (int i = 0; i < g_nLogicNum; i++)
		{
			manual_param |=((uint64)CLOSED_REC)<< (2 * i);
		}
		g_Record.SaveRecordControl(manual_param, TRUE);
		g_Record.StopRec(REC_CLS, -1);
		SystemSetCurrentTime(systime);
		g_Record.SaveRecordControl(oldparam, TRUE);
		return TRUE;
	}
	
	return FALSE;
}

void CGeneral::OnDetect(uint arg)
{
	SYSTEM_TIME systime;
	DHTIME dhtime ;
	int value;
	memcpy(&dhtime, &m_DHTime, sizeof(DHTIME));
	SystemGetCurrentTime(&systime);
	timesys2dh(&m_DHTime, &systime);

	value = ABS(changeto_second(m_DHTime, dhtime));

	if ( value > 10)
	{
		g_Record.UpdateSystemTime(&systime);
	}
	else if (value > 2)
	{
		for (int i = 0; i < g_nCapture; i++)
		{
			CDevCapture::instance(i)->SetTime(&systime);
		}
	}
}

