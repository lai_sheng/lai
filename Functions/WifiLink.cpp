#include "Functions/WifiLink.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <time.h>
#include <fcntl.h>
#include <stdlib.h>
#include <net/if.h>
#include <sys/stat.h> 
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <linux/sockios.h>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

#include "Functions/LedStat.h"
#include "Functions/MP3Play.h"

#include "System/Msg.h"
#include "APIs/Audio.h"
#include "APIs/ExtDev.h"
#include "Intervideo/RealTime_apiv1.h"
#include "Intervideo/LiveCam/RealTimeTaskPushStream.h"

#ifdef LIVECAM
#include "Intervideo/LiveCam/LiveCamTrans.h"
#endif
#include "Configs/ConfigManager.h"
#include "System/UserManager.h"
#include "System/User.h"

#ifdef _NET_USE_DHCPCLIENT/*是否使用dhcp*/
#include "Net/Dlg/DlgDhcpCli.h"
#endif
#include "APIs/CommonBSP_Battery.h"
#include "Devices/DevAudioOut.h"  //LHC 20190327
#ifdef HEMU_CLOUD
#include "Intervideo/HeMu/HWComunication.h"
#include "Intervideo/HeMu/HemuHttpsComunication.h"
#endif
#ifdef  MOBILE_COUNTRY
#include "Intervideo/MobileCountry/MobileCountrySdkAPI.h"
#endif

extern int g_QrScanStatus;

static CMutex PushStaMutex;
/*
	检测SSID 格式是否正确
	ASCII和utf-8中文 为正确格式
	返回:1 
*/
bool CheckSsid(char* str,int len)  
{  
    unsigned char chr; 
    int nBytes = 0;
	int  i;

    bool bAllAscii = TRUE; 
	
    for( i = 0; i < len; ++i)  
    {  
        chr= *(str+i);  
        if( (chr&0x80) != 0 ) // 判断是否ASCII编码,如果不是,说明有可能是UTF-8,ASCII用7位编码,但用一个字节存,最高位标记为0,o0xxxxxxx  
        {
        	bAllAscii= FALSE; 
			
        }
        if(nBytes==0) //如果不是ASCII码,应该是多字节符,计算字节数  
        {  
            if(chr>=0x80)  {  
                if(chr>=0xFC&&chr<=0xFD)  
                    nBytes=6;  
                else if(chr>=0xF8)  
                    nBytes=5;  
                else if(chr>=0xF0)  
                    nBytes=4;  
                else if(chr>=0xE0)  
                    nBytes=3;  
                else if(chr>=0xC0)  
                    nBytes=2;  
                else{
                	return FALSE;  
                }
                nBytes--;  
		
            }  			
        }  
        else //多字节符的非首字节,应为 10xxxxxx  
        {  
            if( (chr&0xC0) != 0x80 ){
            	return FALSE;  
            }
            nBytes--;  
        }  
    }  
    if( nBytes > 0 ) //中文且非utf-8
    {
    	return FALSE;  
    }
	if( bAllAscii ) //全部都是ASCII
    {
    	return TRUE;  
	}
	
    return TRUE;  
}

static char *Ssid_Rework(char *before)
{
    unsigned int i;
    char *ch1;
    int flag_hex = FALSE;
    static char after[32 * 4 + 1];

    memset(after, 0, 32 * 4 + 1);
    if (before == NULL)
    	return after;
    // process unicode SSID, without '"'
    for (i = 0; i < strlen(before); i++) {
            if (before[i] < 32 || before[i] > 127){
                    flag_hex = TRUE;
                    break;
            }
    }
    ch1 = &after[0];
    if (flag_hex) {
            i = 0;
            for (;;) {
                    if (before[i] == 0)
                            break;
                    sprintf(ch1,"%02x", (unsigned char)before[i]);
                    ch1 += 2;
                    i++;
            }
    }else { // ASCII SSID, need '"'
            after[0] ='"';
            strcpy(after+1, before);
            after[strlen(before) + 1] = '"';
    }

	
    return after;
}

int Store_Cfgfile(const char *filepath, char *ssid,char *psw)
{
	FILE *fd = NULL;

	char commset[]={"ctrl_interface=/var/run/wpa_supplicant\n"};	
	char WPAstr[] ={"network={\nssid=%s\npsk=\"%s\"\nscan_ssid=1\n}\n"};
	//char OPENstr[]={"network={\nssid=%s\nkey_mgmt=NONE\ndisabled=1\nscan_ssid=1\n}\n"};//LHC
	char OPENstr[]={"network={\nssid=%s\nkey_mgmt=NONE\nscan_ssid=1\n}\n"};
	char CmdStr[1024] = {0};
	
	fd=fopen(filepath, "w+");
	if( fd == NULL ) {	
		printf("file open can not create file:%s !!! \n",filepath);
		return -1;
	}
	sprintf(CmdStr, commset);
	
	fprintf(fd,"%s", CmdStr);

	if(psw == NULL)
		sprintf(CmdStr, OPENstr, Ssid_Rework(ssid));
	else
		sprintf(CmdStr, WPAstr, Ssid_Rework(ssid), psw);
	
	fprintf(fd,"%s", CmdStr);
	
	fclose(fd);

	return 0;
}
static void* RealTimeSta(void* arg)
{
	int* PushSta = (int*)arg;

	pthread_detach(pthread_self());
	SET_THREAD_NAME("RealTimeSta");
#ifdef MOTOR	
	SetMotorResetPos();
#endif	
	while(1)
	{
		usleep(200*1000);
		if(*PushSta == 0)
		{
			printf("\nStop Push\n");

			
#ifdef LIVECAM  
			if(1 == g_LiveCamTrans.m_initFlag);//~{Ft6/JSF5BkAw4+Jd~}	
				RealTimeServerClose();
#endif
			printf("\nStop Push Over\n");
			PushStaMutex.Enter();
			if(*PushSta == 0)
			{
				*PushSta = -1;
			}
			PushStaMutex.Leave();
		}
		else if(*PushSta == 1)
		{
			printf("Start Push\n");
#ifdef LIVECAM  
			if(1 == g_LiveCamTrans.m_initFlag);//~{Ft6/JSF5BkAw4+Jd~}	
				RealTimeServerReStart();				
#endif
			printf("Start Push Over\n");
			PushStaMutex.Enter();
			if(*PushSta == 1)
			{
				*PushSta = -1;
			}
			PushStaMutex.Leave();
		}
		
	}
}

PATTERN_SINGLETON_IMPLEMENT(WifiLink);

WifiLink::WifiLink():CThread("WifiLink", TP_WIFI),m_bLoop(VD_TRUE)
{
	memset(Id,0,sizeof(Id)/sizeof(Id[0]));
	m_iPlayStatus = 0;
	m_wifilinkstat = 0;  //1:wifilink 0:over
	m_config_over = 0 ;	
	m_nQcoreStatus = QRCORE_INIT;
	m_nWifiConectStatus = WIFI_CONECT_INIT;

	m_packet_count = 0;
}

WifiLink::~WifiLink()
{
	DestroyThread();
}
void WifiLink::Start()
{
	m_bLoop = 1;
	CreateThread();	
}

void WifiLink::OnPacket(void *pData, int nDataLen)
{
	void *pQrcodeDate = malloc((nDataLen/1024 + 1) * 1024);
	if (pQrcodeDate)
	{
		PACKET_QRCORE_DATA packet_qrcode;
		memset(pQrcodeDate, 0, nDataLen);
		memcpy(pQrcodeDate, pData, nDataLen);
		packet_qrcode.pQrodeData = pQrcodeDate;
		packet_qrcode.nLenght = nDataLen;
		m_mutex_list.Enter();
		m_packet_count++;
		m_packet_list.push_back(packet_qrcode);
		m_mutex_list.Leave();
	}
}

void* WifiLink::PopPacket(int *pLenght)
{
	void *pQrcodeDate = NULL;
	if (m_packet_count > 0)
	{
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pQrcodeDate = it->pQrodeData;
		*pLenght = it->nLenght;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();
	}
	return pQrcodeDate;
}
void WifiLink::ClearQrcodePacket()
{
	void *pQrcodeDate = NULL;
	while (m_packet_count > 0)
	{
		m_mutex_list.Enter();
		PACKET_LIST::iterator it = m_packet_list.begin();
		pQrcodeDate = it->pQrodeData;
		m_packet_list.pop_front();
		m_packet_count--;
		m_mutex_list.Leave();
		free(pQrcodeDate);
	}
}

static void AudioForcePlayCfg(char* path)//配置语音播放不允许被其他语音播放中断
{
	int r_play;

#ifdef 	MP3PLAYWITHURL
	if(AudioPlayAudioStatus()){
		g_MP3Play.ResumeVoiceDefaultVolume();
	}
#endif
	r_play = AudioPlayAudioStart(path, (enum audio_encode_type)16, 0);
	if (r_play == -1)
	{
		AudioPlayAudioStop();
		usleep(20*1000);
		AudioPlayAudioStart(path, (enum audio_encode_type)16, 0);
	}
	AudioPlayForceSet();//
}


#if 0
int QRCode_Exchange(char* pSrcBuf, struct SSIDWiFiInfo* pWifiInfo)
{
    if (pSrcBuf == NULL){
        return -1;
    }
    sscanf(pSrcBuf,"[SSID]=%s\n[PWD]=%s\n[UID]=%s\n",pWifiInfo->ssid,pWifiInfo->pwd,pWifiInfo->userid);
    if (pWifiInfo->ssid[0] != 0){
        pWifiInfo->ssidLen = strlen(pWifiInfo->ssid);
    }
    if (pWifiInfo->pwd != 0){
        pWifiInfo->pwdLen = strlen(pWifiInfo->ssid);
    }
    _printd("GetQRCodeWifiMsg ssid = [%s],pwd = [%s] \n",pWifiInfo->ssid,pWifiInfo->pwd);  
    return 0;
}
#endif
void SplitString(const string& s, vector<string>& v, const string& c)
{
    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while(string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2-pos1));
         
        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if(pos1 != s.length())
        v.push_back(s.substr(pos1));
}
int  WifiLink::SetWifiInfo(struct SSIDWiFiInfo *wifi)
{
	memcpy(&m_oWifiInfo, wifi, sizeof(struct SSIDWiFiInfo));
	m_nQcoreStatus = QRCORE_SUCCESS;
}
void WifiLink::WaitPlayAudioExit()
{
	for (size_t i = 0; i < 8; i++)
	{
		if (AudioPlayAudioStatus())
		{
			sleep(1);
		}
		else
		{
			break;
		}
	}
	_printd("===========Play End===========\n");
}
#ifndef IPC_JZ
void WifiLink::ThreadProc()
{
	int*    ret;
	int     pushsta = -1;
	int		confsta = -1;
	pthread_t tid 	= 0 ;
	struct SSIDWiFiInfo* wifi = NULL;

	int r_play = 0;
	
	if(0 != pthread_create(&tid, NULL, &RealTimeSta, (void*)&pushsta))
	{
		tid = -1;
	}

#if 1//LHC
	CDevAudio::instance();	//LHC 先邋AOcreat 再播音乐
//	usleep(200*1000);
//	AudioForcePlayCfg(AUDIO_WELCOME);
//	sleep(3);
#endif

	while(m_bLoop)
	{
		usleep(20 * 1000);
	
		//if((wifi = GetWaveWifiMsg()) != NULL)
		//if((wifi = GetQRCodeWifiMsg()) != NULL)
		if (QRCORE_SUCCESS == m_nQcoreStatus)
		{
			sleep(1);
			AudioForcePlayCfg(AUDIO_RCVWAVE_SUCCESS);
		//	char tmpBuf[256] = {0};
		//	GetQRCodeOriMsg(tmpBuf);
#if 0
			//_printd("qrbuf:%s",tmpBuf);
			string s = tmpBuf;
		    vector<string> v;
		    SplitString(s, v,"\n");
			if(v.size() > 3)
			{
				for(int i = 0; i != v.size(); ++i)
        		cout << v[i] << " ";
				//g_HeMuHttpsCom.QRcodeInfo(v[1],v[2]);
			}
#endif
			//string qrbuf = tmpBuf;
			wifi = &m_oWifiInfo;
			if(wifi->pwdLen <= 0)
				Store_Cfgfile(STORE_WIFI_CONFIG,wifi->ssid,NULL);
			else
				Store_Cfgfile(STORE_WIFI_CONFIG,wifi->ssid,wifi->pwd);	
			
			snd_msg_to_daemon(SND_WAVE_FINISH);
			m_nQcoreStatus = QRCORE_INIT;
			//StopRcvWave();
			StopScanQrCode();
			
			m_wifilinkstat = 0;
			m_config_over = 0;
			WaitPlayAudioExit();
			ClearQrcodePacket();
		}
		else
		{

		}
		
		if(*(ret = rcv_msg_from_daemon()) > 0)
		{
			if(*ret == RCV_START_CON){
				g_ledstat.ForceOpenLed();			

				if(confsta == 1)
				{
					
					//StartRcvWave();
					StartScanQrCode();
					#ifdef HEMU_CLOUD
						g_HWComm.SetConect(0);
					#endif // DEBUG
					AudioForcePlayCfg(AUDIO_START_CON);
#ifdef MOBILE_COUNTRY
					g_MobileCountrySdkApi.StartBind(1);
#endif					
					m_nQcoreStatus = QRCORE_START;
					m_nWifiConectStatus = WIFI_CONECT_INIT;
					_printd("===========Start Config Wifi===========\n");

					m_config_over = 0;//LHC
					WaitPlayAudioExit();
					
				}

				//RealTimeImageMirrorFlip_Callback(NORMAL);
			}
			else if(*ret == RCV_QUIT_CON){
				
				AudioForcePlayCfg(AUDIO_QUIT_CON);
				WaitPlayAudioExit();
			}
			else if(*ret == RCV_LINK_SUCC){
			
				AudioForcePlayCfg(AUDIO_LINK_SUCCESS);
				
				confsta = 0;
				m_wifilinkstat = 0;
				m_config_over = 1;
				m_nWifiConectStatus = WIFI_CONECT_SUCCESS;
				
				g_ledstat.Resume_ForceOpenLed();
				WaitPlayAudioExit();
			}
			else if(*ret == RCV_LINK_FAIL){	
				AudioForcePlayCfg(AUDIO_LINK_FAIL);
				g_ledstat.Resume_ForceOpenLed();

#ifdef MOTOR
				SetMotorTopPos();
#endif
				WaitPlayAudioExit();
				m_nWifiConectStatus = WIFI_CONECT_FAIL;
				//m_config_over = 1;
			}
			else if(*ret == RCV_WIFI_PSK_ERR){		
				AudioForcePlayCfg(AUDIO_WIFI_PSK_ERR);
				g_ledstat.Resume_ForceOpenLed();

#ifdef MOTOR
				SetMotorTopPos();
#endif
				WaitPlayAudioExit();
				m_nWifiConectStatus = WIFI_CONECT_FAIL;
				//m_config_over = 1;
			}
			else if(*ret == RCV_WAVE_LINKFIN)//~{M#V9Iy2(=SJU~}
			{
				_printd("===========Stop Config WIFI===========\n");
				
				//StopRcvWave();
				StopScanQrCode();
				
				confsta = 0;
				
				m_wifilinkstat = 0;
				WaitPlayAudioExit();
				ClearQrcodePacket();
				//m_config_over = 0;
			}
			else if(*ret == RCV_WAVE_START) //~{?*J<Iy2(=SJU~}
			{
				g_ledstat.ForceOpenLed();
				
				snd_msg_to_daemon(SND_WAVE_STARTOVER);

				//StartRcvWave();
				StartScanQrCode();
				#ifdef HEMU_CLOUD
				g_HWComm.SetConect(0);
				#endif
				AudioForcePlayCfg(AUDIO_START_CON);
#ifdef MOBILE_COUNTRY
				g_MobileCountrySdkApi.StartBind(1);
#endif
				m_nQcoreStatus = QRCORE_START;
				confsta = 1;
				m_wifilinkstat = 1;//LHC
				m_config_over = 0;//LHC
				
				m_nWifiConectStatus = WIFI_CONECT_INIT;
			
				_printd("===========Start Config WIFI===========\n");				
				//RealTimeImageMirrorFlip_Callback(NORMAL);
				WaitPlayAudioExit();
			}
			else if(*ret == RCV_INPUT_APMODE) //~{GP;;VA~}AP~{D#J=~}
			{
#ifdef LIVECAM  
				if(tid == -1)
				{
					if(1 == g_LiveCamTrans.m_initFlag);//~{9X1UJSF5BkAw4+Jd~}	
						RealTimeServerClose();
				}
				else
				{
					PushStaMutex.Enter();
					pushsta = 0;
					PushStaMutex.Leave();
				}					
				//StopRcvWave();
				StopScanQrCode();
				ClearQrcodePacket();
#endif					 
			}
			else if(*ret == RCV_RESET_INFO)
			{
				_printd("reset all config....");
				#if 0
				g_userManager.modPassword("guest","123456",needEncrypt);
				g_Config.SetDefaultConfig(DEFAULT_CFG_ALL);
				g_ledstat.Resume_ForceOpenLed();
				#else
				ResetAllCfg();
				#endif
				AudioForcePlayCfg(AUDIO_RESET_SUCCESS);	
				WaitPlayAudioExit();
			}
			#if 0//LHC suning test
			else if (*ret == RCV_START_COMPLETE)
			{
			//	AudioForcePlayCfg(AUDIO_START_COMPLETE);
			}
			#endif
/*			else	
				continue;
*/		
			*ret = EMPTY;
		}
			///////////////////////////////////
		if (m_config_over == 1)
		{	
			//_printd("===========AAAAAAAAAAAAAAAAAAAAA===========\n");	
			//int nRet = RealTimeConnectToServerStatus();
			//if (nRet == 0)//linl paas
			{
#ifdef MOTOR
				SetMotorMidPos();
#endif
				sleep(5);
				AudioForcePlayCfg(AUDIO_LINK_PAAS);
				m_config_over = 0;
				WaitPlayAudioExit();
				m_nWifiConectStatus = WIFI_CONECT_SUCCESS;
			}
		}
		//////////////////////////////////////
	}
}
#else
#define KEY_PRESS_WIFI_TIME  (1000 * 1000 * 1) //1s
#define KEY_PRESS_RESET_TIME (1000 * 1000 * 6) //6s

static inline uint64_t getTimer(void)
{
	struct timeval tv_date;
	gettimeofday( &tv_date, NULL );
	return( (uint64_t) tv_date.tv_sec * 1000000 + (uint64_t) tv_date.tv_usec );
}

void WifiLink::ThreadProc()
{
	int*    ret;
	int     pushsta = -1;
	int		confsta = -1;
	pthread_t tid 	= 0 ;
	struct SSIDWiFiInfo* wifi = NULL;
	
	int r_play = 0;

	int keySta = 0;
	int preKeySta = 0;
	
	uint64_t curTime = 0;
	uint64_t preTime = 0;

	AudioPlayAudioStart(AUDIO_START_CON, (enum audio_encode_type)16, 0);
	
	while(m_bLoop)
	{
		usleep(200 * 1000);

		if (QRCORE_SUCCESS == m_nQcoreStatus)
		{
			AudioForcePlayCfg(AUDIO_RCVWAVE_SUCCESS);
			sleep(1);

			wifi = &m_oWifiInfo;
			#if 0
			if(wifi->pwdLen <= 0)
				Store_Cfgfile(STORE_WIFI_CONFIG,wifi->ssid,NULL);
			else
				Store_Cfgfile(STORE_WIFI_CONFIG,wifi->ssid,wifi->pwd);	
			#endif
			m_nQcoreStatus = QRCORE_INIT;

			if( 0 == BCAM_ConnectWifi(wifi->ssid,wifi->pwd) ){
				AudioPlayAudioStart(AUDIO_LINK_SUCCESS, (enum audio_encode_type)16, 0);	
				m_nWifiConectStatus = WIFI_CONECT_SUCCESS;
			}
			else{
				AudioPlayAudioStart(AUDIO_LINK_FAIL, (enum audio_encode_type)16, 0);
				m_nWifiConectStatus = WIFI_CONECT_FAIL;
			}	
			
			StopScanQrCode();
			
			m_wifilinkstat = 0;
			m_config_over = 0;
			WaitPlayAudioExit();
			ClearQrcodePacket();
		}
		else
		{

		}

		if( 0 != BCAM_GetKeyStatus(&keySta) || 0 == keySta){
			if(curTime - preTime > KEY_PRESS_RESET_TIME){
				
			}
			else if(curTime - preTime > KEY_PRESS_WIFI_TIME){

				m_nWifiConectStatus = WIFI_CONECT_INIT;
				
				StartScanQrCode();

				#ifdef MOBILE_COUNTRY
				g_MobileCountrySdkApi.StartBind(1);
				#endif	
				m_nQcoreStatus = QRCORE_START;
				
				_printd("===========Start Config Wifi===========\n");

				m_config_over = 0;//LHC
					
			}

			curTime = 0;
			preTime = 0;
		}
		else{//按下
			if(0 == preKeySta){
				preTime = getTimer();
			}

			curTime = getTimer();

			if(curTime - preTime > KEY_PRESS_RESET_TIME){
				
			}
			else if(curTime - preTime > KEY_PRESS_WIFI_TIME){
				AudioPlayAudioStart(AUDIO_START_CON, (enum audio_encode_type)16, 0);
			}
			else{

			}
			

		}
		preKeySta = keySta;
		
		//////////////////////////////////////
	}
}
#endif
void WifiLink::SetUserId(char* id)
{
	if(id != NULL)
		memcpy(Id,id,sizeof(Id));
}
void WifiLink::GetUserId(char* DevId,int Len)
{
	if(DevId == NULL || Len < USERIDLEN)
		return ;
	memcpy(DevId,Id,USERIDLEN);
//	printf("id:%s\n",Id);
}

void WifiLink::GetWifiLinkStat(int *pLinkStat)
{
	*pLinkStat = m_wifilinkstat;
}
void WifiLink::ResetAllCfg()
{
#ifdef MOBILE_COUNTRY
	g_MobileCountrySdkApi.ApiFactoryReset();
#endif
	g_userManager.modPassword("guest","123456",needEncrypt);
	g_Config.SetDefaultConfig(DEFAULT_CFG_ALL);
	g_ledstat.Resume_ForceOpenLed();

#ifdef IPC_JZ
	if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel)	
	{
		BCAM_NetWorkReset();
	}
#else
	remove(STORE_WIFI_CONFIG);
	remove("/mnt/mtd/Config/MotorPos.cfg");
#endif
}
#define NETCARD "wlan0"
//down wlan0
int WifiLink::ResetNetwork()
{
#ifdef IPC_JZ
	if (BAT_ProductM_I9M == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_I10 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_S50 == BAT_PlatformGetHandle()->ProductModel ||
	    BAT_ProductM_F5 == BAT_PlatformGetHandle()->ProductModel)
	{
		BCAM_NetWorkReset();
		return 0;
	}
#endif
	int ret			 = 0;
	int skfd 		 = 0;
    struct ifreq ifr ={0};
	struct sockaddr_in sai = {0};
	
	if (( skfd= socket(AF_INET, SOCK_DGRAM, 0 )) < 0)
        return -1;
	
	memset(&ifr, 0,sizeof(ifr));
    strncpy(ifr.ifr_name, NETCARD,sizeof(ifr.ifr_name)- 1);
	
	sai.sin_family 		= AF_INET;
	sai.sin_port 		= 0;
	sai.sin_addr.s_addr = inet_addr("0.0.0.0");
	
	memcpy((((char *)(&ifr)) + ((int) (&((struct ifreq *)0)->ifr_addr))), 
		&sai, sizeof(struct sockaddr));

	
	if ( ioctl(skfd, SIOCSIFADDR, &ifr) < 0 ){
		ret = -1;
	}	


	if ( ioctl(skfd, SIOCGIFFLAGS, &ifr) < 0 ){
		ret = -1;
	}
	
	ifr.ifr_flags &= ~IFF_UP;
	
	if ( ioctl(skfd, SIOCSIFFLAGS, &ifr) < 0 ){
		ret = -1;
	}

	close(skfd);

	return ret;

}
