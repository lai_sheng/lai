#include "Configs/ConfigBase.h"
#include <string>
#include <assert.h>
#ifdef SHREG
#include "../intervideo/Regulatory/SHReg.h"
#endif

#ifdef WIN32
	#define stricmp _stricmp
#else
	#define stricmp strcasecmp
#endif

CMutex CConfigExchange::m_mutex(MUTEX_RECURSIVE);
const char* CConfigExchange::m_strUser = NULL;

CConfigExchange::CConfigExchange(int n, int& used, int id, TCONFIG_SIGNAL& sig)
:m_eleNumber(n), m_eleUsed(n), m_eleUsedLatest(used), m_id(id), m_sigRef(sig)
{
	m_iState = CS_VALIDATING;
	m_bValid = TRUE;
}

void CConfigExchange::setValidity(VD_BOOL bValid)
{
	m_bValid = bValid;
}

VD_BOOL CConfigExchange::getValidity()
{
	return m_bValid;
}

CConfigExchange::CONFIG_STATE CConfigExchange::getState() const
{
	return m_iState;
}

void CConfigExchange::setState(CONFIG_STATE state)
{
	m_iState = state;
}

const char* CConfigExchange::getValue(CConfigTable& table, CConfigKey key, const char* dflt)
{
	CConfigTable& v = getValue(table, key);

	return v.isString() ? v.asCString() : dflt;
}

int CConfigExchange::getValue(CConfigTable& table, CConfigKey key, int dflt)
{
	CConfigTable& v = getValue(table, key);

	return v.isInt() ? v.asInt() : dflt;
}

bool CConfigExchange::getValue(CConfigTable& table, CConfigKey key, bool dflt)
{
	CConfigTable& v = getValue(table, key);

	return v.isBool() ? v.asBool() : dflt;
}

CConfigTable& CConfigExchange::getValue(CConfigTable& table, CConfigKey key)
{

	if (key.m_kind == CConfigKey::kindIndex)
	{
		return table[key.m_value.index];
	}
	else
	{
		return table[key.m_value.name];
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, int& value, int scope, int dflt, ENUM_MAP* map)
{
	const char* str;

	switch(m_iState)
	{
	case CS_SAVING:
		while(map->name)
		{
			if(map->value == value)
			{
				getValue(table, key) = map->name;
				break;
			}
			map++;
		}
		break;

	case CS_LOADING:
		str = getValue(table, key, "");
		value = dflt;
		while(map->name)
		{
			if(stricmp(str, map->name) == 0)
			{
				value = map->value;
				break;
			}
			map++;
		}
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	case CS_VALIDATING:
		if(getValidity())//如果已经无效,则不必也不能再检查
		{
			if(((1 << value) & scope) == 0)
			{
				setValidity(FALSE);
			}

			while(map->name)
			{
				if(map->value == value)
				{
					break;
				}
				map++;
			}
			if(!map->name)
			{
				setValidity(FALSE);
			}
		}
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, int& value, int min, int max, int dflt, ENUM_MAP* map)
{
	const char* str;

	switch(m_iState)
	{
	case CS_SAVING:
		while(map->name)
		{
			if(map->value == value)
			{
				getValue(table, key) = map->name;
				break;
			}
			map++;
		}
		break;

	case CS_LOADING:
		str = getValue(table, key, "");
		value = dflt;
		while(map->name)
		{
			if(stricmp(str, map->name) == 0)
			{
				value = map->value;
				break;
			}
			map++;
		}
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	case CS_VALIDATING:
		if(getValidity())//如果已经无效,则不必也不能再检查
		{
			assert(min <= max);
			if(value < min || value > max)
			{
				setValidity(FALSE);
			}

			while(map->name)
			{
				if(map->value == value)
				{
					break;
				}
				map++;
			}
			if(!map->name)
			{
				setValidity(FALSE);
			}
		}
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, int& value, int min, int max, int dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = value;
		break;

	case CS_LOADING:
		value = getValue(table, key, dflt);
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	case CS_VALIDATING:
		if(getValidity())//如果已经无效,则不必也不能再检查
		{
			if(value < min || value > max)
			{
				setValidity(FALSE);
			}
		}
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, std::string& value, const std::string& scope, const std::string& dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = value;
		break;

	case CS_LOADING:
		value = getValue(table, key, dflt.c_str());
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	case CS_VALIDATING:
		if(getValidity())//如果已经无效,则不必也不能再检查
		{
			if(scope.find(value) == scope.npos)
			{
				setValidity(FALSE);
			}
		}
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, std::string& value, const std::string& dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = value;
		break;

	case CS_LOADING:
		value = getValue(table, key, dflt.c_str());
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	case CS_VALIDATING:
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, char* value, const char* scope, const char* dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = value;
		break;

	case CS_LOADING:
		strcpy(value, getValue(table, key, dflt));
		break;

	case CS_DEFAUTING:
		strcpy(value, dflt);
		break;

	case CS_VALIDATING:
		if(getValidity())//如果已经无效,则不必也不能再检查
		{
			if(!strstr(scope, value))
			{
				setValidity(FALSE);
			}
		}
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, char* value, const char* dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = value;
		break;

	case CS_LOADING:
		strcpy(value, getValue(table, key, dflt));
		break;

	case CS_DEFAUTING:
		strcpy(value, dflt);
		break;

	case CS_VALIDATING:
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, VD_BOOL& value, VD_BOOL dflt)
{
	switch(m_iState)
	{
	case CS_SAVING:
		getValue(table, key) = (value ? true : false);
		break;

	case CS_LOADING:
		value = getValue(table, key, dflt ? true : false) ? TRUE : FALSE;
		break;

	case CS_DEFAUTING:		
		value = dflt ? TRUE : FALSE;
		break;

	default:
		break;
	}
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, uint& value, uint dflt)
{
	char buf[16];
	const char* str;

	switch(m_iState)
	{
	case CS_SAVING:
		sprintf(buf, "0x%08X", value);
		getValue(table, key) = buf;
		break;

	case CS_LOADING:
		str = getValue(table, key, "");
		sscanf(str, "0x%08X", &value);
		break;

	case CS_DEFAUTING:
		value = dflt;
		break;

	default:
		break;
	}
}

//取默认值
void CConfigExchange::recall(int index /* = -1 */)
{
	CGuard guard(m_mutex);
	CConfigTable empty;

	setState(CS_DEFAUTING);
	if(index == -1)
	{
		for(int n = 0; n < m_eleNumber; n++)
		{
			exchangeTable(empty, n);
		}
	}
	else
	{
		exchangeTable(empty, index);
	}
};

//检查合法性
VD_BOOL CConfigExchange::validate(int index /* = -1 */)
{
	CGuard guard(m_mutex);
	CConfigTable empty;

	setValidity(TRUE);
	setState(CS_VALIDATING);
	if(index == -1)
	{
		for(int n = 0; n < m_eleUsed; n++)
		{
			exchangeTable(empty, n);
		}
	}
	else
	{
		exchangeTable(empty, index);
	}

	return getValidity();
};

//更新到最新配置
void CConfigExchange::update(int index /* = -1 */)
{
	CGuard guard(m_mutex);
	int n;

	if(index == -1)
	{
		for(n = 0; n < m_eleNumber; n++)
		{
			copy(n, FALSE);
		}
	}
	else
	{
		copy(index, FALSE);
	}

	m_eleUsed = m_eleUsedLatest;
};

//提交配置，含合法性检查。返回值为配置应用的结果，为CONFIG_APPLY_RET按位组合。
//配置应用结果是在各回调函数里设置的。
int CConfigExchange::commit(const char* user /* = NULL */, int index /* = -1 */, int retInitial /* = 0 */)
{
	CGuard guard(m_mutex);
	CConfigTable empty;
	int ret = retInitial;
	int n;

	m_strUser = user;
	//值没有变化
	if(m_eleUsed == m_eleUsedLatest)
	{
		if(index == -1)
		{
			for(n = 0; n < m_eleNumber; n++)
			{
				if(!equal(n))
				{
					break;
				}
			}
			if(n == m_eleNumber)
			{
				return ret;
			}
		}
		else
		{
			if(equal(index))
			{
				return ret;
			}
		}
	}

	setValidity(TRUE);
	setState(CS_VALIDATING);

	if(index == -1)
	{
		for(n = 0; n < m_eleUsed; n++)
		{
			exchangeTable(empty, n);
		}
	}
	else
	{
		exchangeTable(empty, index);
	}

	if(getValidity())
	{
		if(index == -1)
		{
			for(n = 0; n < m_eleNumber; n++)
			{
				copy(n, TRUE);
			}
		}
		else
		{
			copy(index, TRUE);
		}

		m_eleUsedLatest = m_eleUsed;
		
		m_mutex.Leave();
		m_sigRef(*this, ret);
		m_mutex.Enter();
	}
	else
	{
		ret |= CONFIG_APPLY_VALIT_ERROR;
	}

#ifdef SHREG
	g_SHReg.SetState(SH_TYPE_SAVECONFIG);
#endif
	return ret;
};

// 设置配置的字符串表
void CConfigExchange::setTable(CConfigTable& table)
{
	CGuard guard(m_mutex);
	setState(CS_LOADING);

	m_eleUsed = table.size();
	if(m_eleUsed > m_eleNumber)
	{
		m_eleUsed = m_eleNumber;
	}

	for(int n = 0; n < m_eleUsed; n++)
	{
		exchangeTable(table[n], n);
	}
}

// 获取配置的字符串表
void CConfigExchange::getTable(CConfigTable& table)
{
	CGuard guard(m_mutex);
	setState(CS_SAVING);

	table.resize(m_eleUsed);
	for(int n = 0; n < m_eleUsed; n++)
	{
		exchangeTable(table[n], n);
	}
}

// 注册观察函数
int CConfigExchange::attach(CObject * pObj, TCONFIG_PROC pProc)
{
	CGuard guard(m_mutex);
	return m_sigRef.Attach(pObj, pProc);
}

// 销毁观察函数
int CConfigExchange::detach(CObject * pObj, TCONFIG_PROC pProc)
{
	CGuard guard(m_mutex);
	return m_sigRef.Detach(pObj, pProc);
}

const char* CConfigExchange::getUser()
{
	return m_strUser;
}

int CConfigExchange::getID()
{
	return m_id;
}

void CConfigExchange::setUsed(int used)
{
	m_eleUsed = used;
}

int CConfigExchange::getUsed()
{
	return m_eleUsed;
}

int CConfigExchange::getNumber()
{
	return m_eleNumber;
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, unsigned char& value, unsigned char min, unsigned char max, unsigned char dflt)
{
    switch(m_iState)
    {
        case CS_SAVING:
            getValue(table, key) = value;
            break;

        case CS_LOADING:
            value = getValue(table, key, dflt);
            break;

        case CS_DEFAUTING:
            value = dflt;
            break;

        case CS_VALIDATING:
            if(getValidity())//如果已经无效,则不必也不能再检查    
            {
                if(value < min || value > max)
                {
                    setValidity(FALSE);
                }
            }
            break;

        default:
            break;
    }
}
void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, unsigned short & value, unsigned short min, unsigned short max, unsigned short dflt)
{
    switch(m_iState)
    {
        case CS_SAVING:
            getValue(table, key) = value;
            break;

        case CS_LOADING:
            value = getValue(table, key, dflt);
            break;

        case CS_DEFAUTING:
            value = dflt;
            break;

        case CS_VALIDATING:
            if(getValidity())//如果已经无效,则不必也不能再检查
            {
                if(value < min || value > max)
                {
                    setValidity(FALSE);
                }
            }
            break;

        default:
            break;
    }
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, unsigned short & value, unsigned short dflt)
{
    switch(m_iState)
    {
        case CS_SAVING:
            getValue(table, key) = value;
            break;

        case CS_LOADING:
            value = getValue(table, key, dflt);
            break;

        case CS_DEFAUTING:
            value = dflt;
            break;

        default:
            break;
    }
}

void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, bool& value, bool dflt)
{
    const char* str;

    switch(m_iState)
    {
        case CS_SAVING:
            getValue(table, key) = (value ? "True" : "False");
            break;

        case CS_LOADING:
            str = getValue(table, key, "");
            if(stricmp("True", str) == 0)
            {
                value = TRUE;
            }
            else if(stricmp("False", str) == 0)
            {
                value = FALSE;
            }
            else
            {
                value = dflt;
            }
            break;

        case CS_DEFAUTING:
            value = dflt;
            break;

        default:
            break;
    }
}


void CConfigExchange::exchange(CConfigTable& table, CConfigKey key, unsigned int & value, unsigned int min, unsigned int max, unsigned int dflt)
{
    switch(m_iState)
    {
        case CS_SAVING:
            getValue(table, key) = value;
            break;

        case CS_LOADING:
				{
			int iValue = getValue(table, key, (int)dflt);
			if ( iValue < 0) 
				{
				 value = dflt;
				}
			else
				{
				 value = iValue;
				}
        	}
            break;

        case CS_DEFAUTING:
            value = dflt;
            break;

        case CS_VALIDATING:
            if(getValidity())//如果已经无效,则不必也不能再检查
            {
                if(value < min || value > max)
                {
                    setValidity(FALSE);
                }
            }
            break;

        default:
            break;
    }
}
