
#include "config-x.h"
#include "APIs/Capture.h"
#include "Configs/ConfigConversion.h"
#include "System/AppConfig.h"
#include "ez_libs/ezutil/str_opr.h"
#include "Configs/ConfigGeneral.h"
#include "Configs/ConfigLocation.h"
#include "Configs/ConfigGUISet.h"
#include "Configs/ConfigComm.h"

#include "Configs/ConfigMonitorTour.h"
#include "Configs/ConfigTVAdjust.h"
#include "Configs/ConfigPTZ.h"
#include "Configs/ConfigWorksheet.h"
#include "Configs/ConfigRecord.h"
#include "Configs/ConfigNet.h"
#include "Net/NetApp.h"

//! 转换旧的Color配置到新的配置
void exchangeColorOld2New(CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig);

//! 转换新的Color配置到旧的配置
void exchangeColorNew2Old(CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig);

//! 转换旧的PTZ配置到旧的配置
void exchangePtzOld2New(CONFIG_PTZ& cfgPtz,CONFIG_PTZ_OLD* pConfig);

//!转换新的PTZ配置到新的配置 
void exchangePtzNew2Old(CONFIG_PTZ& cfgPtz,CONFIG_PTZ_OLD* pConfig);

//! 转换新的云台报警设备协议配置到新的配置
void exchangePtzAlarmOld2New(CONFIG_PTZ& cfgPtzAlarm, CONFIG_PTZALARM_OLD* pConfig);

//! 转换旧的云台报警设备协议配置到旧的配置
void exchangePtzAlarmNew2Old(CONFIG_PTZ& cfgPtzAlarm, CONFIG_PTZALARM_OLD* pConfig);


CConfigConversion::CConfigConversion()
{

}


CConfigConversion::~CConfigConversion()
{

}

void CConfigConversion::Convert()
{

	ConvertVideoColor();


	ConvertEncode();



	ConvertNetWork();

}



//从旧的文件里把Color转换成新的配置\n
void CConfigConversion::ConvertVideoColor()
{
	CFile Cfile;
	uint l_dwReaded;

	CONFIG_COLOR_OLD cfgColorOld[N_SYS_CH];

	if (Cfile.Open(CONFIG_DIR"/imagecolor", CFile::modeReadWrite))
	{	
		l_dwReaded = Cfile.Read(&cfgColorOld, N_SYS_CH * sizeof(CONFIG_COLOR_OLD));
		if (l_dwReaded == N_SYS_CH * sizeof(CONFIG_COLOR_OLD))
		{
			SaveOldColor(cfgColorOld);
		}
		Cfile.Close();
		//为了下次不再转换数据
		remove(CONFIG_DIR"/imagecolor");
	}
	
	if (Cfile.Open(CONFIG_DIR"/Color", CFile::modeReadWrite))
	{	
		l_dwReaded = Cfile.Read(&cfgColorOld, N_SYS_CH * sizeof(CONFIG_COLOR_OLD));
		if (l_dwReaded == N_SYS_CH * sizeof(CONFIG_COLOR_OLD))
		{
			SaveOldColor(cfgColorOld);
		}
		Cfile.Close();
		//为了下次不再转换数据
		remove(CONFIG_DIR"/Color");
	}
}




//从旧的文件里转换编码配置结构
void CConfigConversion::ConvertEncode()
{
	CFile Cfile;
	uint l_dwReaded = 0;

	if(Cfile.Open(CONFIG_DIR"/capture", CFile::modeReadWrite))
	{
		CONFIG_CAPTURE_OLD cfgCaptureOld[N_SYS_CH];
		l_dwReaded = Cfile.Read(&cfgCaptureOld, N_SYS_CH * sizeof(CONFIG_CAPTURE_OLD));
		if (l_dwReaded == N_SYS_CH * sizeof(CONFIG_CAPTURE_OLD))
		{
			SaveOldEncode(cfgCaptureOld);
		}
		Cfile.Close();
		//为了下次不再转换数据
		remove(CONFIG_DIR"/capture");
	}

	if(Cfile.Open(CONFIG_DIR"/Capture", CFile::modeReadWrite))
	{	
		CONFIG_CAPTURE_V2201 cfgCaptureOld[N_SYS_CH];
		l_dwReaded = Cfile.Read(cfgCaptureOld, N_SYS_CH * sizeof(CONFIG_CAPTURE_V2201));
		if (l_dwReaded == N_SYS_CH * sizeof(CONFIG_CAPTURE_V2201))
		{
			SaveOldEncode_V2201(cfgCaptureOld);
		}
		Cfile.Close();
		//为了下次不再转换数据
		remove(CONFIG_DIR"/Capture");
	}

}



//从旧的配置文件network里转换配置数据
void CConfigConversion::ConvertNetWork()
{
	CFile mfile;

	//处理原网络配置文件, 以字符串的形式存在的需要逐行获取
	if (mfile.Open(NET_CFG_FNAME,CFile::modeRead))          
	{
		m_pCfgNetOld = new CONFIG_NET;
		memset(m_pCfgNetOld, 0, sizeof(CONFIG_NET));
		LoadNetConfigOld(m_pCfgNetOld);
		m_bOldUseDHCP = -1;

		char buf[__MAX_LINE_SIZE];
		m_configParse.setSpliter("=");
		m_configParse.setTrim(true);
		while ( mfile.Gets(buf, __MAX_LINE_SIZE) != NULL)
		{
			if (isCommentLine(buf))
			{
				continue;
			}
			m_configParse.Parse(buf);
			if (2 == m_configParse.Size())
			{
				ParseConfig();                  //将配置文件中的数据取出
			}
		}

		mfile.Close();
	  	remove(NET_CFG_FNAME);
		SaveNetConfigOld(m_pCfgNetOld, m_bOldUseDHCP,0);
		delete m_pCfgNetOld ;
	}
}


/*!
	\b Description				:	转换旧的Color配置到新的配置\n
	\b Argument					:	CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig
	\param[in]	cfgVideoColor	:	新结构的引用
	\param[in]	pConfig			:	旧结构指针
*/
void exchangeColorOld2New(CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig)
{
	for (int i = 0; i < N_COLOR_SECTION; i++)
	{

		cfgVideoColor.dstVideoColor[i].TimeSection.startHour = pConfig->Color[i].Sector.StartHour;
		cfgVideoColor.dstVideoColor[i].TimeSection.startMinute = pConfig->Color[i].Sector.StartMin;
		cfgVideoColor.dstVideoColor[i].TimeSection.startSecond = pConfig->Color[i].Sector.StartSec;
		cfgVideoColor.dstVideoColor[i].TimeSection.endHour = pConfig->Color[i].Sector.EndHour;
		cfgVideoColor.dstVideoColor[i].TimeSection.endMinute = pConfig->Color[i].Sector.EndMin;
		cfgVideoColor.dstVideoColor[i].TimeSection.endSecond = pConfig->Color[i].Sector.EndSec;

		cfgVideoColor.dstVideoColor[i].dstColor.nBrightness = pConfig->Color[i].Brightness;
		cfgVideoColor.dstVideoColor[i].dstColor.nContrast = pConfig->Color[i].Contrast;
		cfgVideoColor.dstVideoColor[i].dstColor.nHue = pConfig->Color[i].Hue;
		cfgVideoColor.dstVideoColor[i].dstColor.nSaturation = pConfig->Color[i].Saturation;
		cfgVideoColor.dstVideoColor[i].dstColor.mGain = pConfig->Color[i].Gain;

		cfgVideoColor.dstVideoColor[i].iEnable = pConfig->Color[i].Sector.State == 0 ? FALSE : TRUE;
	}
}

/*!
	\b Description				:	转换新的Color配置到旧的配置\n
	\b Argument					:	CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig
	\param[in]	cfgVideoColor	:	新结构的引用
	\param[in]	pConfig			:	旧结构指针
*/
void exchangeColorNew2Old(CONFIG_VIDEOCOLOR& cfgVideoColor, CONFIG_COLOR_OLD* pConfig)
{
	for (int i = 0; i < N_COLOR_SECTION; i++)
	{
		pConfig->Color[i].Sector.StartHour = cfgVideoColor.dstVideoColor[i].TimeSection.startHour;
		pConfig->Color[i].Sector.StartMin = cfgVideoColor.dstVideoColor[i].TimeSection.startMinute;
		pConfig->Color[i].Sector.StartSec = cfgVideoColor.dstVideoColor[i].TimeSection.startSecond;
		pConfig->Color[i].Sector.EndHour = cfgVideoColor.dstVideoColor[i].TimeSection.endHour;
		pConfig->Color[i].Sector.EndMin = cfgVideoColor.dstVideoColor[i].TimeSection.endMinute;
		pConfig->Color[i].Sector.EndSec = cfgVideoColor.dstVideoColor[i].TimeSection.endSecond;

		pConfig->Color[i].Brightness = cfgVideoColor.dstVideoColor[i].dstColor.nBrightness;
		pConfig->Color[i].Contrast = cfgVideoColor.dstVideoColor[i].dstColor.nContrast;
		pConfig->Color[i].Hue = cfgVideoColor.dstVideoColor[i].dstColor.nHue;
		pConfig->Color[i].Saturation = cfgVideoColor.dstVideoColor[i].dstColor.nSaturation;
		pConfig->Color[i].Gain = cfgVideoColor.dstVideoColor[i].dstColor.mGain;
		pConfig->Color[i].Sector.State = cfgVideoColor.dstVideoColor[i].iEnable;
	}
}

/*!
	\b Description			:	保存旧的Color配置\n
	\b Argument				:	CONFIG_COLOR_OLD* pConfig, int iChannel
	\param[in]	pConfig		:	旧结构的指针
	\param[in]	iChannel	:	保存的通道号，-1:表示所有通道
	\return	保存配置的结果				
*/
int CConfigConversion::SaveOldColor(CONFIG_COLOR_OLD* pConfig, int iChannel)
{
	int iRet = 0;

	CConfigVideoColor* pCfgVideoColor = new CConfigVideoColor();
	pCfgVideoColor->update();

	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			exchangeColorOld2New(pCfgVideoColor->getConfig(i), pConfig + i);
		}
	}
	else
	{
		exchangeColorOld2New(pCfgVideoColor->getConfig(iChannel), pConfig + iChannel);
	}
	
	iRet |= pCfgVideoColor->commit(NULL, iChannel);

	delete pCfgVideoColor;

	return iRet;
}

/*!
	\b Description			:	从新的配置里加载旧的Color配置\n
	\b Argument				:	CONFIG_COLOR_OLD* pConfig, int iChannel, VD_BOOL bDefault
	\param[out]	pConfig		:	旧结构的指针
	\param[in]	iChannel	:	保存的通道号; -1:表示所有通道
	\param[in]	bDefault	:	是否加载默认值, TRUE为加载默认值
	\return	保存配置的结果				
*/
void CConfigConversion::LoadOldColor(CONFIG_COLOR_OLD* pConfig, int iChannel, VD_BOOL bDefault)
{
	//转换数据
	CConfigVideoColor* pCfgVideoColor = new CConfigVideoColor();

	if (bDefault)
	{
		pCfgVideoColor->recall();
	}
	else
	{
		pCfgVideoColor->update();
	}
	
	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			exchangeColorNew2Old(pCfgVideoColor->getConfig(i), pConfig + i);
		}
	}
	else
	{
		exchangeColorNew2Old(pCfgVideoColor->getConfig(iChannel), pConfig + iChannel);
	}

	delete pCfgVideoColor;
}

/*转换监视配置*/
void CConfigConversion::LoadOldDisplay(CONFIG_DISPLAY_OLD *pConfig)
{
	CConfigMonitorTour configMonitorTour;
	CConfigGUISet configGUISet;
	CConfigTVAdjust configTVAdjust;

	//	CConfigChannelTitle configChannelTitle;
	configMonitorTour.update();
	configGUISet.update();
	configTVAdjust.update();

	//	configChannelTitle.update();
	pConfig->CardInfoEn = false;//configATM[0].bShowCardInfo;			/*!< ATM卡号叠加信息显示使能 */
	pConfig->WindowAlpha = configGUISet[0].iWindowAlpha;		/*!< 窗口透明度：200～255 */
	pConfig->TimeTitleEn = configGUISet[0].bTimeTitleEn;		/*!< 时间标题显示使能 */
	pConfig->ChannelTitleEn = configGUISet[0].bChannelTitleEn;		/*!< 通道标题显示使能 */
	pConfig->TVMarginLeft = configTVAdjust[0].rctMargin.left;		/*!< TV左边距：0～100 */
	pConfig->TVMarginTop = configTVAdjust[0].rctMargin.top;		/*!< TV上边距：0～100 */
	pConfig->TVMarginRight = configTVAdjust[0].rctMargin.right;		/*!< TV右边距：0～100 */
	pConfig->TVMarginBottom = configTVAdjust[0].rctMargin.bottom;		/*!< TV下边距：0～100 */
	pConfig->TVBrightness = configTVAdjust[0].iBrightness;		/*!< 亮度：0～100 */
	pConfig->TVContrast = configTVAdjust[0].iContrast;			/*!< 对比度：0～100 */
	pConfig->TVAntiDither = configTVAdjust[0].iAntiDither;		/*!< 去抖动：0～100 */

	//ScreenPtcInfo[0],屏保开启使能
	//ScreenPtctInfo[1],屏保开启时间*/
	pConfig->ScreenPtctEn = 0;		/*!< 屏保使能标志 */
	pConfig->ScreenPtctTime = 0;	/*!< 屏保时间 */
	//pConfig->ReservedB2[3];		/*!< 保留 */
	//uint	ChannelLock;		/*!< 通道锁定 */
	pConfig->bTour = configMonitorTour[0].bEnable;				/*!< 进行轮训使能 */
	pConfig->TourInterval = configMonitorTour[0].iInterval;		/*!< 轮巡时间间隔：5～120 sec */
	for(int i = 0; i < N_SPLIT; i++)
	{
		pConfig->TourMask[i] = configMonitorTour[0].iMask[i];	/*!< 轮巡掩码 */
	}
	pConfig->CardInfoEn = false;//configATM[0].bShowCardInfo;			/*!< ATM卡号叠加信息显示使能 */
	pConfig->WindowAlpha = configGUISet[0].iWindowAlpha;		/*!< 窗口透明度：200～255 */
	pConfig->TimeTitleEn = configGUISet[0].bTimeTitleEn;		/*!< 时间标题显示使能 */
	pConfig->ChannelTitleEn = configGUISet[0].bChannelTitleEn;		/*!< 通道标题显示使能 */
	pConfig->TVMarginLeft = configTVAdjust[0].rctMargin.left;		/*!< TV左边距：0～100 */
	pConfig->TVMarginTop = configTVAdjust[0].rctMargin.top;		/*!< TV上边距：0～100 */
	pConfig->TVMarginRight = configTVAdjust[0].rctMargin.right;		/*!< TV右边距：0～100 */
	pConfig->TVMarginBottom = configTVAdjust[0].rctMargin.bottom;		/*!< TV下边距：0～100 */
	pConfig->TVBrightness = configTVAdjust[0].iBrightness;		/*!< 亮度：0～100 */
	pConfig->TVContrast = configTVAdjust[0].iContrast;			/*!< 对比度：0～100 */
	pConfig->TVAntiDither = configTVAdjust[0].iAntiDither;		/*!< 去抖动：0～100 */

	//ScreenPtcInfo[0],屏保开启使能
	//ScreenPtctInfo[1],屏保开启时间*/
	pConfig->ScreenPtctEn = 0;		/*!< 屏保使能标志 */
	pConfig->ScreenPtctTime = 0;	/*!< 屏保时间 */
	//pConfig->ReservedB2[3];		/*!< 保留 */
	//uint	ChannelLock;		/*!< 通道锁定 */
	pConfig->bTour = configMonitorTour[0].bEnable;				/*!< 进行轮训使能 */
	pConfig->TourInterval = configMonitorTour[0].iInterval;		/*!< 轮巡时间间隔：5～120 sec */
	for(int i = 0; i < N_SPLIT; i++)
	{
		pConfig->TourMask[i] = configMonitorTour[0].iMask[i];	/*!< 轮巡掩码 */

	}
}


int CConfigConversion::SaveOldDisplay(CONFIG_DISPLAY_OLD *pConfig)
{
	int iRet = 0;
	CConfigMonitorTour configMonitorTour;
	CConfigGUISet configGUISet;
	CConfigTVAdjust configTVAdjust;

	configMonitorTour.update();
	configGUISet.update();
	configTVAdjust.update();

	configGUISet[0].iWindowAlpha = pConfig->WindowAlpha;		/*!< 窗口透明度：200～255 */
	configGUISet[0].bTimeTitleEn = pConfig->TimeTitleEn;		/*!< 时间标题显示使能 */
	configGUISet[0].bChannelTitleEn = pConfig->ChannelTitleEn;		/*!< 通道标题显示使能 */
	configTVAdjust[0].rctMargin.left = pConfig->TVMarginLeft;		/*!< TV左边距：0～100 */
	configTVAdjust[0].rctMargin.top = pConfig->TVMarginTop;		/*!< TV上边距：0～100 */
	configTVAdjust[0].rctMargin.right = pConfig->TVMarginRight;		/*!< TV右边距：0～100 */
	configTVAdjust[0].rctMargin.bottom = pConfig->TVMarginBottom;		/*!< TV下边距：0～100 */
	configTVAdjust[0].iBrightness = pConfig->TVBrightness;		/*!< 亮度：0～100 */
	configTVAdjust[0].iContrast = pConfig->TVContrast;			/*!< 对比度：0～100 */
	configTVAdjust[0].iAntiDither = pConfig->TVAntiDither;		/*!< 去抖动：0～100 */

	configMonitorTour[0].bEnable = pConfig->bTour;				/*!< 进行轮训使能 */
	configMonitorTour[0].iInterval = pConfig->TourInterval;		/*!< 轮巡时间间隔：5～120 sec */
	for(int i = 0; i < N_SPLIT; i++)
	{
		configMonitorTour[0].iMask[i] = pConfig->TourMask[i];	/*!< 轮巡掩码 */
	}

	iRet |= configMonitorTour.commit() | configGUISet.commit() | configTVAdjust.commit();
	return iRet;
}

/*转换通道名称配置*/
int CConfigConversion::SaveOldChannelTitle(CONFIG_TITLE_OLD*pConfig)
{
	int iRet = 0;
	CConfigChannelTitle* pCfgChannelTitle = new CConfigChannelTitle();
	pCfgChannelTitle->update();
	for(int i = 0; i < N_SYS_CH; i++)
	{
		//此处最好不用strcpy,容易拷贝出错
		memset(pCfgChannelTitle->getConfig(i).strName, '\0', CHANNEL_NAME_MAX_LEN);
		memcpy(pCfgChannelTitle->getConfig(i).strName, (char*)pConfig[i].Title, MIN(strlen((char *)pConfig[i].Title), CHANNEL_NAME_MAX_LEN));
	}
	iRet |= pCfgChannelTitle->commit();
	delete pCfgChannelTitle;
	return iRet;
}
/*!
	\b Description				:	转换老的云台配置的属性为新的配置属性\n
	\b Argument					:	CONFIG_PTZ& cfgPtz, CONFIG_PTZ_OLD* pConfig
	\param[in]	CONFIG_PTZ		:	新配置的引用
	\param[in]	pConfig			:	旧结构指针
*/
void exchangePtzOld2New(CONFIG_PTZ& cfgPtz, CONFIG_PTZ_OLD* pConfig)
{
	cfgPtz.ideviceNo = pConfig->DestAddr;
	cfgPtz.iNuberInMatrixs = pConfig->MonitorAddr;
	cfgPtz.iPortNo = 2;
	cfgPtz.dstComm.iParity = pConfig->PTZAttr.parity;
	//对停止位进行特殊处理
	if(pConfig->PTZAttr.stopbits == 0)
	{
		cfgPtz.dstComm.iStopBits = 1;
	}
	else
	{
		cfgPtz.dstComm.iStopBits = 2;
	}
	//cfgPtz.dstComm.iStopBits = pConfig->PTZAttr.stopbits;
	cfgPtz.dstComm.nBaudBase = pConfig->PTZAttr.baudrate;
	cfgPtz.dstComm.nDataBits = pConfig->PTZAttr.databits;
	memcpy(&(cfgPtz.strProtocolName), &(pConfig->Name), sizeof(pConfig->Name));
}


/*!
	\b Description				:	转换新的云台配置的属性为旧的配置属性\n
	\b Argument					:	CONFIG_PTZ& cfgPtz, CONFIG_PTZ_OLD* pConfig
	\param[in]	CONFIG_PTZ		:	新配置的引用
	\param[in]	pConfig			:	旧结构指
*/
void exchangePtzNew2Old(CONFIG_PTZ& cfgPtz,CONFIG_PTZ_OLD* pConfig)
{
	memset(pConfig, 0, sizeof(CONFIG_PTZ_OLD));
	pConfig->DestAddr = cfgPtz.ideviceNo;
	pConfig->MonitorAddr = cfgPtz.iNuberInMatrixs;
	pConfig->PTZAttr.baudrate = cfgPtz.dstComm.nBaudBase;
	pConfig->PTZAttr.databits = cfgPtz.dstComm.nDataBits;
	pConfig->PTZAttr.parity = cfgPtz.dstComm.iParity;
	if(cfgPtz.dstComm.iStopBits == 1)
	{
		pConfig->PTZAttr.stopbits = 0;
	}
	else
	{
		pConfig->PTZAttr.stopbits = 2;
	}
	//pConfig->PTZAttr.stopbits = cfgPtz.dstComm.iStopBits;
	memcpy(&(pConfig->Name), &(cfgPtz.strProtocolName), sizeof(pConfig->Name));
}

/*!
	\b Description		:	保存旧的云台配置到文件里\n
	\b Argument			:	CONFIG_PTZ_OLD *pConfig
	\param[in]	pConfig	:	旧配置的结构指针
	\return	保存结果			
*/
int CConfigConversion::SaveOldPtzConfig(CONFIG_PTZ_OLD *pConfig, int iChannel)
{
	int iRet = 0;

	//转换数据
	CConfigPTZ* pCfgPtz = new CConfigPTZ();
	pCfgPtz->update();
	if (-1 == iChannel)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			exchangePtzOld2New(pCfgPtz->getConfig(i), pConfig + i);
		}
	}
	else
	{
		exchangePtzOld2New(pCfgPtz->getConfig(iChannel), pConfig + iChannel);
	}
	iRet |= pCfgPtz->commit(NULL, iChannel);
	delete pCfgPtz;
	return iRet;
}

/*!
	\b Description		:	保存旧的云台报警设备配置到文件里\n
	\b Argument			:	CONFIG_PTZ_OLD *pConfig
	\param[in]	pConfig	:	旧配置的结构指针
	\return	保存结果			
*/
int CConfigConversion::SaveOldPtzAlarmConfig(CONFIG_PTZ_OLD *pConfig,int iChannel)
{
	int iRet = 0;

	//转换数据
	CConfigPTZAlarm*  pCfgPtzAlarm = new CConfigPTZAlarm();
	pCfgPtzAlarm->update();

	if (-1 == iChannel)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			exchangePtzOld2New(pCfgPtzAlarm->getConfig(i), pConfig + i);
		}
	}
	else
	{
		exchangePtzOld2New(pCfgPtzAlarm->getConfig(iChannel), pConfig + iChannel);
	}

	iRet |= pCfgPtzAlarm->commit(NULL, iChannel);

	delete pCfgPtzAlarm;

	return iRet;
}

/*!
	\b Description			:	从新配置里转换成旧的配置\n
	\b Argument				:	CONFIG_PTZ_OLD *pConfig, VD_BOOL bDefault
	\param[in|out]	pConfig	:	旧配置的指针
	\param[in]		bDefault:	是否默认配置，TRUE为默认配置
*/
void CConfigConversion::LoadOldPtzConfig(CONFIG_PTZ_OLD *pConfig, int iChannel, VD_BOOL bDefault)
{
	CConfigPTZ* pCfgPtz = new CConfigPTZ();
	
	if (bDefault)
	{
		pCfgPtz->recall();
	}
	else
	{
		pCfgPtz->update();
	}

	if (-1 == iChannel)
	{
		for (int i = 0; i < N_SYS_CH; i ++)
		{
			exchangePtzNew2Old(pCfgPtz->getConfig(i), pConfig + i);
		}
	}
	else
	{
		exchangePtzNew2Old(pCfgPtz->getConfig(iChannel),pConfig + iChannel);
	}

	delete pCfgPtz;
}

/*!
	\b Description			:	从新配置里转换成旧的云台报警设备配置\n
	\b Argument				:	CONFIG_PTZALARM_OLD *pConfig, VD_BOOL bDefault
	\param[in|out]	pConfig	:	旧配置的指针
	\param[in]		bDefault:	是否默认配置，TRUE为默认配置
*/
void CConfigConversion::LoadOldPtzAlarmConfig(CONFIG_PTZ_OLD *pConfig,int iChannel, VD_BOOL bDefault)
{
	CConfigPTZAlarm* pCfgPtzAlarm = new CConfigPTZAlarm();
	
	if (bDefault)
	{
		pCfgPtzAlarm->recall();
	}
	else
	{
		pCfgPtzAlarm->update();
	}

	if (-1 == iChannel)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			exchangePtzNew2Old(pCfgPtzAlarm->getConfig(i), pConfig + i);
		}

	}
	else
	{
		exchangePtzNew2Old(pCfgPtzAlarm->getConfig(iChannel), pConfig + iChannel);
	}

	delete pCfgPtzAlarm;
}

void ConvertRectOld2New(VIDEO_WIDGET& vw, ENCODE_TITLE *pTitle)
{
	vw.bEncodeBlend = pTitle->TitleEnable;
	vw.rgbaFrontground = pTitle->TitlefgRGBA;
	vw.rgbaBackground = pTitle->TitlebgRGBA;
	vw.rcRelativePos.left = pTitle->TitleLeft == 8192 ? 8191 : pTitle->TitleLeft;
	vw.rcRelativePos.top = pTitle->TitleTop == 8192 ? 8191 : pTitle->TitleTop;
	vw.rcRelativePos.right = pTitle->TitleRight == 8192 ? 8191 : pTitle->TitleRight;
	vw.rcRelativePos.bottom = pTitle->TitleBottom == 8192 ? 8191 : pTitle->TitleBottom;
}

void ConvertRectNew2Old(VIDEO_WIDGET& vw, ENCODE_TITLE *pTitle)
{
	pTitle->TitleEnable = vw.bEncodeBlend;
	pTitle->TitlefgRGBA = vw.rgbaFrontground;
	pTitle->TitlebgRGBA = vw.rgbaBackground;
	pTitle->TitleLeft = vw.rcRelativePos.left;
	pTitle->TitleTop = vw.rcRelativePos.top;
	pTitle->TitleRight = vw.rcRelativePos.right;
	pTitle->TitleBottom = vw.rcRelativePos.bottom;
}

int fps_pal[]  = {1, 2, 3, 6, 12, 25};
int fps_ntsc[] = {1, 2, 4, 7, 15, 20, 30};

int fps_snap[] = {1, -2 , -3, -4, -5, -6, -7};

//先进行帧率转换
/*
>      客户端传给应用程序的抓图帧率接口,传递的是索引号,值从0~31,格式如下:
>            0,1,2,3,4,5,6,x,x.....x ,17,18,19,20,x,x......x   
>      0----1秒1帧
>      1----2秒1帧
>      2----3秒1帧
>      3----4秒1帧
>      4----5秒1帧
>      5----6秒1帧
>      保留
>      17----1秒2帧
>      18----1秒3帧
>      19----1秒4帧
>      20----1秒5帧 
>      保留
*/
int framsTable[32] = {1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15, -16, -17, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
int convertSnapRealFrame(int iframeIndex)
{
	if (iframeIndex >= 32)
	{
		iframeIndex = 0;
	}
	
	if (iframeIndex < 0)
	{
		iframeIndex = 0;
	}

	return framsTable[iframeIndex];
}

int convertSnapFrameIndex(int frame)
{
	int i = 0;
	for (i = 0; i < 32; i++)
	{
		
		if (framsTable[i] == frame)
		{
			break;
		}
	}
	return i;
}
int convertRealFrame(int iframeIndex)
{
	if (CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_PAL)
	{
		return fps_pal[iframeIndex];
	}
	else
	{
		return fps_ntsc[iframeIndex];	
	}
}

int convertFrameIndex(int frame)
{
	int i = 0;

	if (CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_PAL)
	{
		for (i = 0; i <= 5; i++)
		{
			if (fps_pal[i] >= frame)
			{
				break;
			}
		}
	}
	else
	{
		for (i = 0; i <= 6; i++)
		{
			if (fps_ntsc[i] >= frame)
			{
				break;
			}
		}
	}

	return i;
}

void extrangeEncodeOld2New(CONFIG_ENCODE& cfgEncode, CONFIG_VIDEOWIDGET& cfgVW, CONFIG_CAPTURE_OLD *pConfig)
{
	for (int i = 0; i < ENCODE_TYPE_NUM; i++)
	{
		cfgEncode.dstMainFmt[i].vfFormat.iCompression = pConfig->Compression;
		cfgEncode.dstMainFmt[i].vfFormat.iResolution = pConfig->MainOption[i].ImageSize;
		cfgEncode.dstMainFmt[i].vfFormat.iBitRateControl = pConfig->MainOption[i].BitRateControl;
		cfgEncode.dstMainFmt[i].vfFormat.iQuality = pConfig->MainOption[i].ImgQlty;
		//直接转换成帧值
		cfgEncode.dstMainFmt[i].vfFormat.nFPS = pConfig->MainOption[i].Frames;
		cfgEncode.dstMainFmt[i].vfFormat.iGOP = pConfig->MainOption[i].IFrameInterval;

		//获取码流值
		cfgEncode.dstMainFmt[i].vfFormat.nBitRate = pConfig->MainOption[i].usBitRate;
		cfgEncode.dstMainFmt[i].bVideoEnable = pConfig->MainOption[i].AVEnable & CAPTURE_START_VIDEO ? TRUE : FALSE;
#ifdef FUNC_COURT_DISPLAY
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30
		cfgEncode.dstMainFmt[i].afFormat.ucLAudioVolumn = pConfig->Volume;   //左声道音量
		cfgEncode.dstMainFmt[i].afFormat.ucRAudioVolumn = pConfig->Volume;   //右声道音量，单声道的设备此值无效
#else
		cfgEncode.dstMainFmt[i].afFormat.nMaxVolume = pConfig->Volume;
#endif
#endif
		cfgEncode.dstMainFmt[i].bAudioEnable = pConfig->MainOption[i].AVEnable & CAPTURE_START_AUDIO ? TRUE : FALSE;
	}

	for (int i = 0; i < EXTRATYPES; i++)
	{
		cfgEncode.dstExtraFmt[i].vfFormat.iCompression = pConfig->Compression;
		cfgEncode.dstExtraFmt[i].vfFormat.iResolution = pConfig->AssiOption[i].ImageSize;
		cfgEncode.dstExtraFmt[i].vfFormat.iBitRateControl = pConfig->AssiOption[i].BitRateControl;
		cfgEncode.dstExtraFmt[i].vfFormat.iQuality = pConfig->AssiOption[i].ImgQlty;

		//码流值保存
		cfgEncode.dstExtraFmt[i].vfFormat.nBitRate = pConfig->AssiOption[i].usBitRate;

		if ((cfgEncode.dstExtraFmt[i].vfFormat.iQuality < 1) || (cfgEncode.dstExtraFmt[i].vfFormat.iQuality > 6))
		{
			cfgEncode.dstExtraFmt[i].vfFormat.iQuality = 4;
		}
		//直接转换成帧值
		cfgEncode.dstExtraFmt[i].vfFormat.nFPS = pConfig->AssiOption[i].Frames;
		cfgEncode.dstExtraFmt[i].bVideoEnable = pConfig->AssiOption[i].AVEnable & CAPTURE_START_VIDEO ? TRUE : FALSE;
		cfgEncode.dstExtraFmt[i].bAudioEnable = pConfig->AssiOption[i].AVEnable & CAPTURE_START_AUDIO ? TRUE : FALSE;
		cfgEncode.dstExtraFmt[i].vfFormat.iGOP = pConfig->AssiOption[i].IFrameInterval;
	}

	for (int i = 0; i < 2; i++)
	{
		cfgEncode.dstSnapFmt[i].vfFormat.iCompression = pConfig->Compression;
		cfgEncode.dstSnapFmt[i].vfFormat.iResolution = pConfig->PicOption[i].ImageSize;
		cfgEncode.dstSnapFmt[i].vfFormat.iBitRateControl = pConfig->PicOption[i].BitRateControl;
		cfgEncode.dstSnapFmt[i].vfFormat.iQuality = pConfig->PicOption[i].ImgQlty;
		if ((cfgEncode.dstSnapFmt[i].vfFormat.iQuality < 1) || (cfgEncode.dstSnapFmt[i].vfFormat.iQuality > 6))
		{
			cfgEncode.dstSnapFmt[i].vfFormat.iQuality = 4;
		}

		//此处暂时也把码流值保存起来，以后也同时可以限制图片的大小
		cfgEncode.dstSnapFmt[i].vfFormat.nBitRate = cfgEncode.dstSnapFmt[i].vfFormat.nBitRate;

		cfgEncode.dstSnapFmt[i].vfFormat.nFPS = convertSnapRealFrame(pConfig->PicOption[i].Frames);
		cfgEncode.dstSnapFmt[i].bVideoEnable = pConfig->PicOption[i].AVEnable & CAPTURE_START_VIDEO ? TRUE : FALSE;
		cfgEncode.dstSnapFmt[i].bAudioEnable = pConfig->PicOption[i].AVEnable & CAPTURE_START_AUDIO ? TRUE : FALSE;
	}
	//cfgVW.dstCovers[0].bEncodeBlend = pConfig->CoverEnable & ENC_COVER_MONITOR ? TRUE : FALSE;
	//cfgVW.dstCovers[0].bPreviewBlend = pConfig->CoverEnable & ENC_COVER_PREVIEW ? TRUE : FALSE;
	//cfgVW.dstCovers[0].rcRelativePos = pConfig->Cover;
	ConvertRectOld2New(cfgVW.ChannelTitle, &pConfig->ChannelTitle);
	ConvertRectOld2New(cfgVW.TimeTitle, &pConfig->TimeTitle);
}

void extrangeEncodeNew2Old(CONFIG_ENCODE& cfgEncode, CONFIG_VIDEOWIDGET& cfgVW, CONFIG_CAPTURE_OLD *pConfig)
{
	pConfig->Compression = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iCompression;
#ifdef FUNC_COURT_DISPLAY
#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制 2010-6-30
	pConfig->Volume = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].afFormat.ucLAudioVolumn;  //左声道音量
#else
	pConfig->Volume = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].afFormat.nMaxVolume;
#endif
#endif
	for (int i = 0; i < ENCODE_TYPE_NUM; i++)
	{
		pConfig->MainOption[i].ImageSize = cfgEncode.dstMainFmt[i].vfFormat.iResolution;
		pConfig->MainOption[i].BitRateControl  = cfgEncode.dstMainFmt[i].vfFormat.iBitRateControl;
		pConfig->MainOption[i].ImgQlty = cfgEncode.dstMainFmt[i].vfFormat.iQuality;
		//交换码流值
		pConfig->MainOption[i].usBitRate = cfgEncode.dstMainFmt[i].vfFormat.nBitRate;
		pConfig->MainOption[i].IFrameInterval = cfgEncode.dstMainFmt[i].vfFormat.iGOP;

		//直接需要转换为下标
		pConfig->MainOption[i].Frames = cfgEncode.dstMainFmt[i].vfFormat.nFPS;
		pConfig->MainOption[i].AVEnable = cfgEncode.dstMainFmt[i].bVideoEnable == TRUE ? CAPTURE_START_VIDEO : 0;
		pConfig->MainOption[i].AVEnable |= cfgEncode.dstMainFmt[i].bAudioEnable == TRUE ? CAPTURE_START_AUDIO : 0;
	}

	for (int i = 0; i < EXTRATYPES; i++)
	{
		pConfig->AssiOption[i].ImageSize = cfgEncode.dstExtraFmt[i].vfFormat.iResolution;
		pConfig->AssiOption[i].BitRateControl  = cfgEncode.dstExtraFmt[i].vfFormat.iBitRateControl;
		pConfig->AssiOption[i].ImgQlty = cfgEncode.dstExtraFmt[i].vfFormat.iQuality;

		//交换码流值
		pConfig->AssiOption[i].usBitRate = cfgEncode.dstExtraFmt[i].vfFormat.nBitRate;
		pConfig->AssiOption[i].IFrameInterval = cfgEncode.dstExtraFmt[i].vfFormat.iGOP;

		//直接转换成帧值
		pConfig->AssiOption[i].Frames = cfgEncode.dstExtraFmt[i].vfFormat.nFPS;
		pConfig->AssiOption[i].AVEnable = cfgEncode.dstExtraFmt[i].bVideoEnable == TRUE ? CAPTURE_START_VIDEO : 0;
		pConfig->AssiOption[i].AVEnable |= cfgEncode.dstExtraFmt[i].bAudioEnable == TRUE ? CAPTURE_START_AUDIO : 0;
	}

	for (int i = 0; i < 2; i++)
	{
		pConfig->PicOption[i].ImageSize = cfgEncode.dstSnapFmt[i].vfFormat.iResolution;
		pConfig->PicOption[i].BitRateControl = cfgEncode.dstSnapFmt[i].vfFormat.iBitRateControl;
		pConfig->PicOption[i].ImgQlty = cfgEncode.dstSnapFmt[i].vfFormat.iQuality;

		//此处暂时也把码流值保存起来，以后也同时可以限制图片的大小
		pConfig->PicOption[i].usBitRate = cfgEncode.dstSnapFmt[i].afFormat.nBitRate;
		
		pConfig->PicOption[i].Frames = convertSnapFrameIndex(cfgEncode.dstSnapFmt[i].vfFormat.nFPS);
		pConfig->PicOption[i].AVEnable = cfgEncode.dstSnapFmt[i].bVideoEnable == TRUE ? CAPTURE_START_VIDEO : 0;
		pConfig->PicOption[i].AVEnable |= cfgEncode.dstSnapFmt[i].bAudioEnable == TRUE ? CAPTURE_START_AUDIO : 0;
	}

	ConvertRectNew2Old(cfgVW.ChannelTitle, &pConfig->ChannelTitle);
	ConvertRectNew2Old(cfgVW.TimeTitle, &pConfig->TimeTitle);
}

void extrangeCapture2201Old2New(CONFIG_ENCODE& cfgEncode, CONFIG_VIDEOWIDGET& cfgVW, CONFIG_CAPTURE_V2201 *pConfig)
{
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iCompression = pConfig->VideoType;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution = pConfig->CifMode;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iBitRateControl = pConfig->EncodeMode;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality = pConfig->ImgQlty;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = pConfig->Frames;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].bVideoEnable = pConfig->VideoEn;
	cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].bAudioEnable = pConfig->AudioEn;

	//cfgVW.dstCovers[ENCODE_TYPE_TIM].bEncodeBlend = pConfig->CoverEnable;
	//cfgVW.dstCovers[ENCODE_TYPE_TIM].bPreviewBlend = pConfig->CoverEnable;
	//cfgVW.dstCovers[ENCODE_TYPE_TIM].rcRelativePos = pConfig->Cover;
	cfgVW.ChannelTitle.bEncodeBlend = pConfig->ChTitleEn;
	cfgVW.ChannelTitle.bPreviewBlend = pConfig->ChTitleEn;
	cfgVW.ChannelTitle.rcRelativePos.left = pConfig->ChannelTitleL == 8192 ? 8191 : pConfig->ChannelTitleL;
	cfgVW.ChannelTitle.rcRelativePos.top = pConfig->ChannelTitleT == 8192 ? 8191 : pConfig->ChannelTitleT;
	cfgVW.ChannelTitle.rcRelativePos.right = pConfig->ChannelTitleR == 8192 ? 8191 : pConfig->ChannelTitleR;
	cfgVW.ChannelTitle.rcRelativePos.bottom = pConfig->ChannelTitleB == 8192 ? 8191 : pConfig->ChannelTitleB;

	cfgVW.TimeTitle.bEncodeBlend = pConfig->TimeTilteEn;
	cfgVW.TimeTitle.bPreviewBlend = pConfig->TimeTilteEn;
	cfgVW.TimeTitle.rcRelativePos.left = pConfig->TimeTitleL == 8192 ? 8191 : pConfig->TimeTitleL;
	cfgVW.TimeTitle.rcRelativePos.top = pConfig->TimeTitleT == 8192 ? 8191 : pConfig->TimeTitleT;
	cfgVW.TimeTitle.rcRelativePos.right = pConfig->TimeTitleR == 8192 ? 8191 : pConfig->TimeTitleR;
	cfgVW.TimeTitle.rcRelativePos.bottom = pConfig->TimeTitleB == 8192 ? 8191 : pConfig->TimeTitleB;
}

void extrangeCapture2201New2Old(CONFIG_ENCODE& cfgEncode, CONFIG_VIDEOWIDGET& cfgVW, CONFIG_CAPTURE_V2201 *pConfig)
{
	pConfig->VideoType = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iCompression;
	pConfig->CifMode = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
	pConfig->EncodeMode = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iBitRateControl;
	pConfig->ImgQlty = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iQuality;
	pConfig->Frames = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
	pConfig->VideoEn = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].bVideoEnable;
	pConfig->AudioEn = cfgEncode.dstMainFmt[ENCODE_TYPE_TIM].bAudioEnable;

	//pConfig->CoverEnable = cfgVW.dstCovers[ENCODE_TYPE_TIM].bEncodeBlend;
	//pConfig->CoverEnable = cfgVW.dstCovers[ENCODE_TYPE_TIM].bPreviewBlend;
	//pConfig->Cover = cfgVW.dstCovers[ENCODE_TYPE_TIM].rcRelativePos;
	pConfig->ChTitleEn = cfgVW.ChannelTitle.bEncodeBlend | cfgVW.ChannelTitle.bPreviewBlend;
	pConfig->ChannelTitleL = cfgVW.ChannelTitle.rcRelativePos.left;
	pConfig->ChannelTitleT = cfgVW.ChannelTitle.rcRelativePos.top;
	pConfig->ChannelTitleR = cfgVW.ChannelTitle.rcRelativePos.right;
	pConfig->ChannelTitleB = cfgVW.ChannelTitle.rcRelativePos.bottom;

	pConfig->TimeTilteEn = cfgVW.TimeTitle.bEncodeBlend | cfgVW.TimeTitle.bPreviewBlend;
	pConfig->TimeTitleL = cfgVW.TimeTitle.rcRelativePos.left;
	pConfig->TimeTitleT = cfgVW.TimeTitle.rcRelativePos.top;
	pConfig->TimeTitleR = cfgVW.TimeTitle.rcRelativePos.right;
	pConfig->TimeTitleB = cfgVW.TimeTitle.rcRelativePos.bottom;
}


/*!
	\b Description		:	保存旧的双码流配置结构\n
	\b Argument			:	CONFIG_CAPTURE_OLD* pConfig, int iChannel
	\param[in]	pConfig	:	旧的双码流结构指针
	\param[out]	iChannel:	保存的通道号; -1:表示所有通道	
*/
int CConfigConversion::SaveOldEncode(CONFIG_CAPTURE_OLD* pConfig, int iChannel /* = -1 */)
{
	int iRet = 0;

	CConfigEncode* pCfgEncode = new CConfigEncode();
	CConfigVideoWidget*	pCfgVideoWidget = new CConfigVideoWidget();
	CConfigRecord* pCfgRecord = new CConfigRecord();
	pCfgRecord->update();

	pCfgEncode->update();
	pCfgVideoWidget->update();

	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			extrangeEncodeOld2New(pCfgEncode->getConfig(i), pCfgVideoWidget->getConfig(i), pConfig + i);


			if((pConfig + i)->PicOption[0].AVEnable)
			{
				pCfgRecord->getConfig(i).bSnapShot = TRUE;
			}
			else
			{
				pCfgRecord->getConfig(i).bSnapShot = FALSE;
			}
		}
	}
	else
	{
		extrangeEncodeOld2New(pCfgEncode->getConfig(iChannel), pCfgVideoWidget->getConfig(iChannel), pConfig);

		if((pConfig + iChannel)->PicOption[0].AVEnable)
		{
			pCfgRecord->getConfig(iChannel).bSnapShot = TRUE;
		}
		else
		{
			pCfgRecord->getConfig(iChannel).bSnapShot = FALSE;
		}
	}
	iRet |= pCfgEncode->commit() | pCfgVideoWidget->commit() |pCfgRecord->commit();
	delete pCfgEncode;
	delete pCfgVideoWidget;
	delete pCfgRecord;
	return iRet;
}

/*!
	\b Description			:	从新的配置文件里转成双码流配置结构\n
	\b Argument				:	CONFIG_CAPTURE_OLD* pConfig, int iChannel, VD_BOOL bDefault
	\param[out]	pConfig		:	旧结构的指针
	\param[in]	iChannel	:	保存的通道号; -1:表示所有通道
	\param[in]	bDefault	:	是否加载默认值, TRUE为加载默认值
*/
void CConfigConversion::LoadOldEncode(CONFIG_CAPTURE_OLD* pConfig, int iChannel /* = -1 */, VD_BOOL bDefault /* = FALSE */)
{
	//转换数据
	CConfigEncode* pCfgEncode = new CConfigEncode();
	CConfigVideoWidget*	pCfgVideoWidget = new CConfigVideoWidget();

	if (bDefault)
	{
		pCfgEncode->recall();
		pCfgVideoWidget->recall();
	}
	else
	{
		pCfgEncode->update();
		pCfgVideoWidget->update();
	}

	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			extrangeEncodeNew2Old(pCfgEncode->getConfig(i), pCfgVideoWidget->getConfig(i), pConfig + i);
		}
	}
	else
	{
		extrangeEncodeNew2Old(pCfgEncode->getConfig(iChannel), pCfgVideoWidget->getConfig(iChannel), pConfig + iChannel);
	}

	delete pCfgEncode;
	delete pCfgVideoWidget;
}

/*!
	\b Description		:	保存旧的单码流配置结构\n
	\b Argument			:	CONFIG_CAPTURE_V2201* pConfig, int iChannel
	\param[in]	pConfig	:	旧的双码流结构指针
	\param[out]	iChannel:	保存的通道号; -1:表示所有通道	
*/
int CConfigConversion::SaveOldEncode_V2201(CONFIG_CAPTURE_V2201* pConfig, int iChannel /* = -1 */)
{
	int iRet = 0;

	CConfigEncode* pCfgEncode = new CConfigEncode();
	CConfigVideoWidget*	pCfgVideoWidget = new CConfigVideoWidget();

	pCfgEncode->update();
	pCfgVideoWidget->update();

	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			extrangeCapture2201Old2New(pCfgEncode->getConfig(i), pCfgVideoWidget->getConfig(i), pConfig + i);
		}
	}
	else
	{
		extrangeCapture2201Old2New(pCfgEncode->getConfig(iChannel), pCfgVideoWidget->getConfig(iChannel), pConfig);
	}

	iRet |= pCfgEncode->commit() | pCfgVideoWidget->commit();
	delete pCfgEncode;
	delete pCfgVideoWidget;
	return iRet;
}

/*!
	\b Description			:	从新的配置文件里转成单码流配置结构\n
	\b Argument				:	CONFIG_CAPTURE_V2201* pConfig, int iChannel, VD_BOOL bDefault
	\param[out]	pConfig		:	旧结构的指针
	\param[in]	iChannel	:	保存的通道号; -1:表示所有通道
	\param[in]	bDefault	:	是否加载默认值, TRUE为加载默认值	
*/
void CConfigConversion::LoadOldEncode_V2201(CONFIG_CAPTURE_V2201* pConfig, int iChannel /* = -1 */, VD_BOOL bDefault /* = FALSE */)
{
	//转换数据
	CConfigEncode* pCfgEncode = new CConfigEncode();
	CConfigVideoWidget*	pCfgVideoWidget = new CConfigVideoWidget();

	if (bDefault)
	{
		pCfgEncode->recall();
		pCfgVideoWidget->recall();
	}
	else
	{
		pCfgEncode->update();
		pCfgVideoWidget->update();
	}

	if (iChannel == -1)
	{
		for (int i = 0; i < N_SYS_CH; i++)
		{
			extrangeCapture2201New2Old(pCfgEncode->getConfig(i),  pCfgVideoWidget->getConfig(i), pConfig + i);
		}
	}
	else
	{
		extrangeCapture2201New2Old(pCfgEncode->getConfig(iChannel),  pCfgVideoWidget->getConfig(iChannel), pConfig + iChannel);
	}

	delete pCfgEncode;
	delete pCfgVideoWidget;
}


int CConfigConversion::SaveOldRecord(CONFIG_RECORD_OLD* pConfig)
{
	int iRet = 0;

	CConfigRECWorksheet* pCfgWorksheet = new CConfigRECWorksheet();
	CConfigRecord* pCfgRecord = new CConfigRecord();

	pCfgWorksheet->update();
	pCfgRecord->update();

	//只把第1通道的时间做为第一个工作表，其他通道不管
	CONFIG_WORKSHEET& cfgWorksheet = pCfgWorksheet->getConfig(0);
	CONFIG_RECORD&	cfgReocrd = pCfgRecord->getConfig(0);
	
	for (int i = 0; i < N_WEEKS; i++)
	{
		for (int j = 0; j < N_TSECT; j++)
		{
			cfgWorksheet.tsSchedule[i][j].startHour = pConfig->Sector[i][j].StartHour;
			cfgWorksheet.tsSchedule[i][j].endHour = pConfig->Sector[i][j].EndHour;
			cfgWorksheet.tsSchedule[i][j].startMinute = pConfig->Sector[i][j].StartMin;
			cfgWorksheet.tsSchedule[i][j].endMinute = pConfig->Sector[i][j].EndMin;
			cfgWorksheet.tsSchedule[i][j].startSecond = pConfig->Sector[i][j].StartSec;
			cfgWorksheet.tsSchedule[i][j].endSecond = pConfig->Sector[i][j].EndSec;
			cfgWorksheet.tsSchedule[i][j].enable  = pConfig->Sector[i][j].State;
		}

		cfgReocrd.bRedundancy = pConfig->Redundancy;
		cfgReocrd.iPreRecord = pConfig->PreRecord;
	}
	
	iRet |= pCfgWorksheet->commit() | pCfgRecord->commit();
	delete pCfgWorksheet;
	delete pCfgRecord;
	return iRet;
}

void CConfigConversion::LoadOldRecord(CONFIG_RECORD_OLD* pConfig, int iChannel, VD_BOOL bDefault)
{
	CConfigRECWorksheet* pCfgWorksheet = new CConfigRECWorksheet();
	CConfigRecord* pCfgRecord = new CConfigRecord();

	if (bDefault)
	{
		pCfgWorksheet->recall();
		pCfgRecord->recall();
	}
	else
	{
		pCfgWorksheet->update();
		pCfgRecord->update();
	}
	
	//只把第1通道的时间做为第一个工作表，其他通道不管
	CONFIG_WORKSHEET& cfgWorksheet = pCfgWorksheet->getConfig(0);
	CONFIG_RECORD&	cfgReocrd = pCfgRecord->getConfig(0);
	CONFIG_RECORD_OLD* pCfg = NULL;

	if (iChannel == -1)
	{
		pCfg = pConfig;
	}
	else
	{
		pCfg = pConfig + iChannel;
	}

	for (int i = 0; i < N_WEEKS; i++)
	{
		for (int j = 0; j < N_TSECT; j++)
		{
			pCfg->Sector[i][j].StartHour = cfgWorksheet.tsSchedule[i][j].startHour;
			pCfg->Sector[i][j].EndHour = cfgWorksheet.tsSchedule[i][j].endHour;
			pCfg->Sector[i][j].StartMin = cfgWorksheet.tsSchedule[i][j].startMinute;
			pCfg->Sector[i][j].EndMin = cfgWorksheet.tsSchedule[i][j].endMinute;
			pCfg->Sector[i][j].StartSec = cfgWorksheet.tsSchedule[i][j].startSecond;
			pCfg->Sector[i][j].EndSec = cfgWorksheet.tsSchedule[i][j].endSecond;
			pCfg->Sector[i][j].State = cfgWorksheet.tsSchedule[i][j].enable;
		}

		pCfg->Redundancy = cfgReocrd.bRedundancy;
		pCfg->PreRecord = cfgReocrd.iPreRecord;
	}

	if (iChannel == -1)
	{
		for (int i = 1; i < N_SYS_CH; i++)
		{
			memcpy(pConfig + i, pCfg, sizeof(CONFIG_RECORD_OLD));
		}
	}

	delete pCfgWorksheet;
	delete pCfgRecord;
}

int CConfigConversion::SaveOldAutoMain(CONFIG_AUTO_OLD* pConfig)
{
    int iRet = 0;
    CConfigAutoMaintain* pCfgAutoMain = new CConfigAutoMaintain();
    
    pCfgAutoMain->update();
    CONFIG_AUTOMAINTAIN& cfgAutomain = pCfgAutoMain->getConfig();
    
    cfgAutomain.iAutoRebootDay = pConfig->AutoRebootDay;
    cfgAutomain.iAutoRebootHour = pConfig->AutoRebootTime;
    cfgAutomain.iAutoDeleteFilesDays = pConfig->AutoDeleteFilesTime;

    iRet = pCfgAutoMain->commit();
    delete pCfgAutoMain;
    return iRet;
}

void CConfigConversion::LoadOldAutoMain(CONFIG_AUTO_OLD* pConfig,  VD_BOOL bDefault)
{
    CConfigAutoMaintain* pCfgAutoMain = new CConfigAutoMaintain();

    if(bDefault)
    {
        pCfgAutoMain->recall();
    }
    else
    {
        pCfgAutoMain->update();
    }
    CONFIG_AUTOMAINTAIN& cfgAutomain = pCfgAutoMain->getConfig();
    pConfig->AutoRebootDay = cfgAutomain.iAutoRebootDay;
    pConfig->AutoRebootTime = cfgAutomain.iAutoRebootHour;
    pConfig->AutoDeleteFilesTime = cfgAutomain.iAutoDeleteFilesDays;

    delete pCfgAutoMain;    
}
//!旧的基本网络配置结构转换与保存
void CConfigConversion::LoadNetConfigOld(CONFIG_NET *pNetConfig, VD_BOOL bDefault)
{
	if(NULL == pNetConfig)
	{
		return ;
	}
	CConfigNetCommon *pCfgNetCommon = new CConfigNetCommon();
	CConfigNetMultiCast *pCfgMultCast = new CConfigNetMultiCast();
	CConfigNetAlarmServer *pCfgAlarmCenter = new CConfigNetAlarmServer();
	if(bDefault)
	{
		pCfgNetCommon->recall();
		pCfgMultCast->recall();
		pCfgAlarmCenter->recall();
	}
	else
	{
		pCfgNetCommon->update();
		pCfgMultCast->update();
		pCfgAlarmCenter->update();
	}
	CONFIG_NET_COMMON& cfg = pCfgNetCommon->getConfig();
	strcpy((char *)pNetConfig->Version, NET_CFG_VERSION);
	memset(pNetConfig->HostName, 0, sizeof(pNetConfig->HostName));
	memcpy(pNetConfig->HostName, cfg.HostName, sizeof(pNetConfig->HostName) - 1);
	
	IPInfo info;
	g_NetApp.GetNetIPInfo(NULL, &info,TRUE);
	pNetConfig->HostIP.l= info.HostIP.l;
	pNetConfig->Submask.l =info.SubMask.l;
	pNetConfig->GateWayIP.l = info.GateWay.l;
	pNetConfig->HttpPort = cfg.HttpPort;
	pNetConfig->TCPPort = cfg.TCPPort;
	pNetConfig->SSLPort = cfg.SSLPort;
	pNetConfig->UDPPort = cfg.UDPPort;
	pNetConfig->TCPMaxConn = cfg.MaxConn;
	pNetConfig->MonMode = cfg.MonMode;

	CConfigNetAlarmServer cCfgNetAlarmServer;
	cCfgNetAlarmServer.update();
	CONFIG_NET_ALARMSERVER &cfgServer = cCfgNetAlarmServer.getConfig();

	pNetConfig->AlarmServerIP.l = cfgServer.Server.ip.l;
	pNetConfig->AlarmServerPort = cfgServer.Server.Port;
	pNetConfig->AlmSvrStat = cfgServer.Enable;

	
	// .................还有一些配置项需要从其它配置结构中取
	
	// 组播IP
	pNetConfig->McastIP.l = pCfgMultCast->getConfig().Server.ip.l;
	pNetConfig->McastPort = pCfgMultCast->getConfig().Server.Port;

	//报警中心
	CONFIG_NET_ALARMSERVER cfgalarm = pCfgAlarmCenter->getConfig(0);
	pNetConfig->AlarmServerIP.l = cfgalarm.Server.ip.l;
	pNetConfig->AlarmServerPort = cfgalarm.Server.Port;
	pNetConfig->AlmSvrStat = cfgalarm.Enable;
	delete pCfgNetCommon;
	delete pCfgMultCast;
	delete pCfgAlarmCenter;
	return ;

}
//保存基本网络配置
int CConfigConversion::SaveNetConfigOld(CONFIG_NET *pNetConfig, int bUseDHCP, int bUpdate)
{
	int iRet = 0;
	if(NULL == pNetConfig)
		return 0;
	CConfigNetCommon *pCfgNetCommon = new CConfigNetCommon();
	pCfgNetCommon->update();
	CONFIG_NET_COMMON& cfgComm = pCfgNetCommon->getConfig();
	strcpy(cfgComm.HostName , pNetConfig->HostName);

	IPInfo netipinfo;
	if(pNetConfig->HostIP.l)
		netipinfo.HostIP.l = pNetConfig->HostIP.l;
	if(pNetConfig->Submask.l)
		netipinfo.SubMask.l=pNetConfig->Submask.l;
	if(pNetConfig->GateWayIP.l)
		netipinfo.GateWay.l= pNetConfig->GateWayIP.l;
	if(g_NetApp.SetNetIPInfo(NULL, &netipinfo,bUpdate))
		iRet |=CONFIG_APPLY_VALIT_ERROR;
	if(pNetConfig->HttpPort)
		cfgComm.HttpPort = pNetConfig->HttpPort;
	if(pNetConfig->TCPPort)
		cfgComm.TCPPort = pNetConfig->TCPPort;
	if(pNetConfig->SSLPort)
		cfgComm.SSLPort = pNetConfig->SSLPort;
	if(pNetConfig->UDPPort)
		cfgComm.UDPPort = pNetConfig->UDPPort;
	cfgComm.MaxConn = pNetConfig->TCPMaxConn;
	cfgComm.MonMode = pNetConfig->MonMode;
	iRet |=pCfgNetCommon->commit();
	delete pCfgNetCommon;
	trace("iRet===========================%d, Line======%d\n", iRet, __LINE__);
//组播设置转换
	CConfigNetMultiCast *pCfgNetMultiCast = new CConfigNetMultiCast();
	pCfgNetMultiCast->update();
	CONFIG_NET_MULTICAST &cfgnetmulticast = pCfgNetMultiCast->getConfig();
	if(pNetConfig->McastIP.l)
	{
		cfgnetmulticast.Server.ip.l = pNetConfig->McastIP.l;
	}
	if(pNetConfig->McastPort)
	{
		cfgnetmulticast.Server.Port= pNetConfig->McastPort;
	}
	iRet |=pCfgNetMultiCast->commit();
	delete pCfgNetMultiCast;
	trace("iRet===========================%d, Line======%d\n", iRet, __LINE__);
//报警中心设置转换
	CConfigNetAlarmServer *pCfgNetAlarmServer = new CConfigNetAlarmServer();
	pCfgNetAlarmServer->update();
	CONFIG_NET_ALARMSERVER& cfgCommAlarmServer = pCfgNetAlarmServer->getConfig(0);
	if(pNetConfig->AlarmServerIP.l)
	{
		cfgCommAlarmServer.Server.ip.l = pNetConfig->AlarmServerIP.l;
	}
	if(pNetConfig->AlarmServerPort)
	{
		cfgCommAlarmServer.Server.Port = pNetConfig->AlarmServerPort;
	}
	cfgCommAlarmServer.Enable = pNetConfig->AlmSvrStat;
	iRet |=pCfgNetAlarmServer->commit();
	delete pCfgNetAlarmServer;
	trace("iRet===========================%d, Line======%d\n", iRet, __LINE__);
	return iRet;

}

void CConfigConversion::ParseConfig()
{ 
		std::string strItem = m_configParse.getWord();
		if (NULL == m_pCfgNetOld )
		{
			tracef("new old network config error \n");
			return;
		}
		if (NET_CFG_ITEM_VERSION == strItem)
		{
			// 8字节的版本信息
			memset(m_pCfgNetOld->Version, 0, sizeof(m_pCfgNetOld->Version));
			memcpy(m_pCfgNetOld->Version, m_configParse.getWord().c_str(), 4);
		}
		else if (NET_CFG_ITEM_HOSTNAME == strItem)
		{
			memcpy(m_pCfgNetOld->HostName, m_configParse.getWord().c_str(), 15);
			m_pCfgNetOld->HostName[15] = '\0';
		}
			else if (NET_CFG_ITEM_USEDHCP == strItem)
		{
			m_bOldUseDHCP = (1==m_configParse.getValue())?true:false;
		}
		else if (NET_CFG_ITEM_HOSTIP == strItem)
		{
			m_pCfgNetOld->HostIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_SUBMASK == strItem)
		{
			m_pCfgNetOld->Submask.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_GATEWAYIP == strItem)
		{
			m_pCfgNetOld->GateWayIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_DNSIP == strItem)
		{
			m_pCfgNetOld->DNSIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_ALARMSERVERIP == strItem)
		{
			m_pCfgNetOld->AlarmServerIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_ALARMSERVERPORT == strItem)
		{
			m_pCfgNetOld->AlarmServerPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_SMTPSERVERIP == strItem)
		{
			m_pCfgNetOld->SMTPServerIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_SMTPSERVERPORT == strItem)
		{
			m_pCfgNetOld->SMTPServerPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_LOGSERVERIP == strItem)
		{
			m_pCfgNetOld->LogServerIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_LOGSERVERPORT == strItem)
		{
			m_pCfgNetOld->LogServerPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_HTTPPORT   == strItem)
		{
			m_pCfgNetOld->HttpPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_HTTPSPORT  == strItem)
		{
			m_pCfgNetOld->HttpsPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_TCPPORT    == strItem)
		{
			m_pCfgNetOld->TCPPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_TCPMAXCONN   == strItem)
		{
			m_pCfgNetOld->TCPMaxConn = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_SSLPORT    == strItem)
		{
			m_pCfgNetOld->SSLPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_UDPPORT    == strItem)
		{
			m_pCfgNetOld->UDPPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_MCASTIP    == strItem)
		{
			m_pCfgNetOld->McastIP.l = Str2Ip((char *)m_configParse.getWord().c_str());
		}
		else if (NET_CFG_ITEM_MCASTPORT  == strItem)
		{
			m_pCfgNetOld->McastPort = (ushort) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_MONMODE    == strItem)
		{
			m_pCfgNetOld->MonMode = (uchar) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_PLAYMODE   == strItem)
		{
			m_pCfgNetOld->PlayMode = (uchar) m_configParse.getValue();
		}
		else if (NET_CFG_ITEM_ALMSVRSTAT == strItem)
		{
			m_pCfgNetOld->AlmSvrStat = (uchar) m_configParse.getValue();
		}
		else
		{
		}

	return ;
}

