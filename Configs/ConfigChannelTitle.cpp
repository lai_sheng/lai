#include "Configs/ConfigChannelTitle.h"
#include "System/Locales.h"


#if defined(_VENDOR_APHD_CHNAME)
#define MAX_CHANNEL_WITH 640
#elif defined(LMT_TITLE_LEN)
#define MAX_CHANNEL_WITH 170
#else 
#define MAX_CHANNEL_WITH 320
#endif

char* spt1[32] = {
	"conf_img.channel1","conf_img.channel2","conf_img.channel3",
		"conf_img.channel4","conf_img.channel5","conf_img.channel6",
		"conf_img.channel7","conf_img.channel8","conf_img.channel9",
		"conf_img.channel10","conf_img.channel11","conf_img.channel12",
		"conf_img.channel13","conf_img.channel14","conf_img.channel15",
		"conf_img.channel16","conf_img.channel17","conf_img.channel18",
		"conf_img.channel19","conf_img.channel20","conf_img.channel21",
		"conf_img.channel22","conf_img.channel23","conf_img.channel24",
		"conf_img.channel25","conf_img.channel26","conf_img.channel27",
		"conf_img.channel28","conf_img.channel29","conf_img.channel30",
		"conf_img.channel31","conf_img.channel32",
};

template <> void exchangeTable<CONFIG_CHANNELTITLE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_CHANNELTITLE& config, int index, int app)
{
	xchg.exchange(table, "Name", config.strName, LOADSTR(spt1[index]));

	if (g_Locales.GetTextExtent(config.strName) > MAX_CHANNEL_WITH)
	{
		trace("the channel[%d]'s NAME  is out of range\n", index + 1);
		xchg.setValidity(FALSE);
		return;
	}
	
	uint l,h;
	l = (uint)(config.iSerialNo);
	h = (uint)(config.iSerialNo >> 32);

	xchg.exchange(table, "SerialNoLow", l, (uint)0);
	xchg.exchange(table, "SerialNoHight", h, (uint)0);

	config.iSerialNo = (int64)(((int64)h << 32) + l);
}

template <> void exchangeTable<CONFIG_OSDPARAM>(CConfigExchange& xchg, CConfigTable& table, CONFIG_OSDPARAM& config, int index, int app)
{
	xchg.exchange(table, "ZoomSize", config.size, 1, 1, 1);
	xchg.exchange(table, "index", config.index, 0, N_SYS_CH, index);
}


