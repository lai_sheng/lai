#include "Configs/ConfigAwakeTime.h"

VD_BOOL checkValidity(int mTime)
{
	int hour = (mTime >> 16)&0xff;
	int min  = (mTime >>  8)&0xff;
	int sec  =  mTime&0xff;
//_printd("hour:%d,min:%d,sec:%d",hour,min,sec);


	if(hour < 0 || hour > 23)	return VD_FALSE;
	if(min < 0 || min > 59)		return VD_FALSE;
	if(sec < 0 || sec > 59)		return VD_FALSE;

	return VD_TRUE;
}
template <> void exchangeTable<CONFIG_AWAKETIME>(CConfigExchange& xchg, CConfigTable& table, CONFIG_AWAKETIME& config, int index, int app)
{
#if 0
	xchg.exchange(table, "StartTimeHour", config.s_hour, 0, 23, 0);
	xchg.exchange(table, "StartTimeMin", config.s_min, 0, 59, 0);
	xchg.exchange(table, "StartTimeSec", config.s_sec, 0, 59, 0);

	xchg.exchange(table, "EndTimeHour", config.e_hour, 0, 23, 23);
	xchg.exchange(table, "EndTimeMin", config.e_min, 0, 59, 59);
	xchg.exchange(table, "EndTimeSec", config.e_sec, 0, 59, 59);	
#endif

	char str[64] = {0};
	int  min   = (0 <<16 | 0 <<8 | 0);
	int  max   = (23<<16 | 59<<8 | 59);
	int	 dlf_s = (0 <<16 | 0 <<8 | 0);
	int  dlf_e = (23<<16 | 59<<8 | 59);
	int  zerohour = (24 << 16 | 0 << 8 | 0);
	

	for(int i = 0; i < UNITS ; i++)
	{
		snprintf(str,sizeof(str),"Enable[%02d]",i);
		//xchg.exchange(table, str, config.DayPlan[i].Enable, FALSE);
		xchg.exchange(table, str, config.DayPlan[i].Enable, TRUE); //Ĭ�ϴ�PIR

		snprintf(str,sizeof(str),"StartTime[%02d]",i);
		if(VD_FALSE == checkValidity(config.DayPlan[i].StartTime))
		{
			_printd("Error %s %x",str,config.DayPlan[i].StartTime);
			xchg.exchange(table, str, config.DayPlan[i].StartTime, min,max,dlf_s);
		}
		else{
			xchg.exchange(table, str, config.DayPlan[i].StartTime, min,max,dlf_s);
		}

		snprintf(str,sizeof(str),"EndTime[%02d]",i);
		if(config.DayPlan[i].EndTime == zerohour)
			config.DayPlan[i].EndTime = max;
		
		if(VD_FALSE == checkValidity(config.DayPlan[i].EndTime))
		{
			_printd("Error %s %x",str,config.DayPlan[i].EndTime);
			xchg.exchange(table, str, config.DayPlan[i].EndTime, min,max,dlf_e);
		}
		else{
			xchg.exchange(table, str, config.DayPlan[i].EndTime, min,max,dlf_e);
		}
	}
}

template <> void exchangeTable<CONFIG_TIMERTASK_LIST>(CConfigExchange& xchg, CConfigTable& table, CONFIG_TIMERTASK_LIST& config, int index, int app)
{
	char str[64] = {0};
	int  min   = (0 <<16 | 0 <<8 | 0);
	int  max   = (23<<16 | 59<<8 | 59);
	int	 dlf_s = (0 <<16 | 0 <<8 | 0);
	int  dlf_e = (23<<16 | 59<<8 | 59);
	int  zerohour = (24 << 16 | 0 << 8 | 0);
	
	xchg.exchange(table, "TaskNum", config.taskNum, 0, TIMERTASK_MAXNUM, 0);
	
	for(int i = 0; i < TIMERTASK_MAXNUM ; i++){

		snprintf(str,sizeof(str),"taskId[%02d]",i);
		xchg.exchange(table, str, config.task[i].taskId, 0);
		snprintf(str,sizeof(str),"mainSwitch[%02d]",i);
		xchg.exchange(table, str, config.task[i].mainSwitch, VD_FALSE);
		
		for(int j = 0 ; j < WDAYS; j ++){
			snprintf(str,sizeof(str),"daySwitch[%02d][%02d]",i,j);
			xchg.exchange(table, str, config.task[i].daySwitch[j], VD_FALSE);
		}
		
		for(int j = 0 ; j < UNITS; j ++){
			snprintf(str,sizeof(str),"dayEnable[%02d][%02d]",i,j);
			xchg.exchange(table, str, config.task[i].dayPlan[j].Enable, VD_FALSE);


			snprintf(str,sizeof(str),"dayStartTime[%02d][%02d]",i,j);
			
			if(VD_FALSE == checkValidity(config.task[i].dayPlan[j].StartTime)){
				_printd("Error %s %x",str,config.task[i].dayPlan[j].StartTime);
				xchg.exchange(table, str, config.task[i].dayPlan[j].StartTime, min,max,dlf_s);
			}
			else{
				xchg.exchange(table, str, config.task[i].dayPlan[j].StartTime, min,max,dlf_s);
			}

			snprintf(str,sizeof(str),"dayEndTime[%02d][%02d]",i,j);
			if(config.task[i].dayPlan[j].EndTime == zerohour){
				config.task[i].dayPlan[j].EndTime = max;
			}
			
			if(VD_FALSE == checkValidity(config.task[i].dayPlan[j].EndTime)){
				_printd("Error %s %x",str,config.task[i].dayPlan[j].EndTime);
				xchg.exchange(table, str, config.task[i].dayPlan[j].EndTime, min,max,dlf_e);
			}
			else{
				xchg.exchange(table, str, config.task[i].dayPlan[j].EndTime, min,max,dlf_e);
			}
		}		

		for(int j = 0; j < TAG_TIMERTASK_MAX ; j ++){
			snprintf(str,sizeof(str),"taskDo[%02d][%02d]",i,j);
			xchg.exchange(table, str, config.task[i].taskDo[j], VD_FALSE);
		}
	}

}

template <> void exchangeTable<CONFIG_DELAYTASK_LIST>(CConfigExchange& xchg, CConfigTable& table, CONFIG_DELAYTASK_LIST& config, int index, int app)
{
	char str[64] = {0};
	
	xchg.exchange(table, "TaskNum", config.taskNum, 0, TIMERTASK_MAXNUM, 0);
	
	for(int i = 0; i < TIMERTASK_MAXNUM ; i++){

		//_printd("config.task[%d].taskId:%d",i,config.task[i].taskId);
		//_printd("config.task[%d].startTimeStamp:%d",i,config.task[i].startTimeStamp);
		//_printd("config.task[%d].delaySec:%d",i,config.task[i].delaySec);

		snprintf(str,sizeof(str),"taskId[%02d]",i);
		xchg.exchange(table, str, config.task[i].taskId, 0,1000,0);

		snprintf(str,sizeof(str),"startTimeStamp[%02d]",i);
		xchg.exchange(table, str, config.task[i].startTimeStamp, 0);	


		snprintf(str,sizeof(str),"delaySec[%02d]",i);
		xchg.exchange(table, str, config.task[i].delaySec, 0 ,6*3600,0);	
		
		for(int j = 0; j < TAG_TIMERTASK_MAX ; j ++){

			//_printd("config.task[%d].taskSwitch[%d]:%d",i,config.task[i].taskSwitch[j]);
			//_printd("config.task[%d].taskDo[%d]:%d",i,config.task[i].taskDo[j]);
		
			snprintf(str,sizeof(str),"taskSwitch[%02d][%02d]",i,j);
			xchg.exchange(table, str, config.task[i].taskSwitch[j], VD_FALSE);	
			
			snprintf(str,sizeof(str),"taskDo[%02d][%02d]",i,j);
			xchg.exchange(table, str, config.task[i].taskDo[j], VD_FALSE);
		}
	}

}

template <> void exchangeTable<CONFIG_BATALARM>(CConfigExchange& xchg, CConfigTable& table, CONFIG_BATALARM& config, int index, int app)
{
	xchg.exchange(table, "CamLowBatTime", config.strCamLowBatTime, "");
	xchg.exchange(table, "LockLowBatTime", config.strLockLowBatTime, "");
}

template <> void exchangeTable<CONFIG_PIRLEVEL>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PIRLEVEL& config, int index, int app)
{
	xchg.exchange(table, "powerLevel", config.level, 0, PIR_LEVEL_MAX, PIR_LEVEL_5);
	xchg.exchange(table, "pushMsgLevel", config.pushMsgLevel, 0, PIR_PUSH_MSG_MAX, PIR_PUSH_MSG_NORMAL);
}