#include "Configs/ConfigVideoColor.h"
#include "APIs/System.h"
#include "APIs/Video.h"

void exchangeTimeSection(CConfigExchange& configExchange, CConfigTable& table, TIMESECTION& timesection);
void VideoColorValidate(CConfigExchange& configExchange, CONFIG_VIDEOCOLOR& config);
void VideoColorExchange(CConfigExchange& configExchange, CConfigTable& table, VIDEOCOLOR_PARAM& vc);

template<> void exchangeTable<CONFIG_VIDEOCOLOR>(CConfigExchange& xchg, CConfigTable& table, CONFIG_VIDEOCOLOR& config, int index, int app)
{
	if (xchg.getState() == CConfigExchange::CS_VALIDATING)
	{
		VideoColorValidate(xchg, config);
		if (xchg.getValidity() == FALSE)
		{
			return;
		}
	}
	
	for (int i = 0; i < N_COLOR_SECTION; i++)
	{
		CConfigTable& tb1 = table[i];		
		exchangeTimeSection(xchg, tb1["TimeSection"], config.dstVideoColor[i].TimeSection);
		VideoColorExchange(xchg, tb1["VideoColorParam"], config.dstVideoColor[i].dstColor);
		xchg.exchange(tb1["Selected"], "Enable", config.dstVideoColor[i].iEnable, FALSE, TRUE, i == 0 ? TRUE : FALSE);
	}

}

template<> void exchangeTable<CONFIG_VIDEOCONTROL>(CConfigExchange& xchg, CConfigTable& table, CONFIG_VIDEOCONTROL& config, int index, int app)
{
	xchg.exchange(table, "Exposure", config.nExposure , 0, 6, 0);
	xchg.exchange(table, "Backlight", config.bBacklight, FALSE, TRUE, FALSE);
	xchg.exchange(table, "AutoColor2BW", config.bAutoColor2BW, FALSE, TRUE, FALSE);
    xchg.exchange(table,"Mirror",config.bMirror,FALSE, TRUE, FALSE);
}

void VideoColorExchange(CConfigExchange& configExchange, CConfigTable& table, VIDEOCOLOR_PARAM& vcp)
{
	//BNC输出模式下，视频图像画质默认偏白，建议更改 色度50  亮度 43  对比度 53  饱和度 50
	int dftValue = 50;
	configExchange.exchange(table, "Contrast", vcp.nContrast, 0, 100, dftValue);
	configExchange.exchange(table, "Brightness", vcp.nBrightness, 0, 100, dftValue);
	configExchange.exchange(table, "Saturation", vcp.nSaturation, 0, 100, dftValue);
	configExchange.exchange(table, "Hue", vcp.nHue, 0, 100, dftValue);
	configExchange.exchange(table, "Gain", vcp.mGain, 0, 100 + 0x80, 50 | 0x80);
	configExchange.exchange(table, "Whitebalance", vcp.mWhitebalance, 0, 0x82, 0x80);//默认白电平使能打开
}


/*!
	\b Description				:	验证图像颜色配置文件\n
	\b Argument					:	(CConfigExchange& configExchange, CONFIG_VIDEOCOLOR& config
	\param[in]	configExchange	:	配置对象
	\param[in]	config			:	配置信息
*/
void VideoColorValidate(CConfigExchange& configExchange, CONFIG_VIDEOCOLOR& config)
{
	typedef struct  
	{
		int index;
		int times2min;
	}TIMEQUE;

	TIMESECTION* pTimeSec = NULL;
	TIMESECTION* ppreTimeSec = NULL;
	uint dwinterval = 0;

	TIMEQUE timesque[N_COLOR_SECTION];
	for (int i = 0; i < N_COLOR_SECTION; i++)
	{
		timesque[i].index = -1;
		timesque[i].times2min = 0;
	}
	
	//按照开始时间排序
	int index = 0;
	for (int i = 0; i < N_COLOR_SECTION; i++)
	{
		if (config.dstVideoColor[i].iEnable == TRUE)
		{
			timesque[index].index = i;
			timesque[index].times2min = config.dstVideoColor[i].TimeSection.startHour * 60 + config.dstVideoColor[i].TimeSection.startMinute;
			index++;
		}
	}

	//进行插入排序
	int j = 0;
	TIMEQUE tmp;
	for (int i = 1; i < index; i++)
	{
		j = i;
		tmp = timesque[i];
		while(j > 0 && tmp.times2min < timesque[j - 1].times2min)
		{
			timesque[j] = timesque[j - 1];
			j--;
		}
		timesque[j] = tmp;
	}

	//验证时间段是否连续
	for (int i = 1; i < index; i++)
	{
		ppreTimeSec = &config.dstVideoColor[timesque[i - 1].index].TimeSection;
		pTimeSec = &config.dstVideoColor[timesque[i].index].TimeSection;
		if (ppreTimeSec->endHour * 60 + ppreTimeSec->endMinute != pTimeSec->startHour * 60 + pTimeSec->startMinute)
		{
			configExchange.setValidity(FALSE);
			return;
		}
	}
	
	//验证时间段是否２４小时封闭
	if (index != 0)
	{
		ppreTimeSec = &config.dstVideoColor[timesque[0].index].TimeSection;
		pTimeSec = &config.dstVideoColor[timesque[index - 1].index].TimeSection;
		dwinterval = ABS((pTimeSec->endHour - ppreTimeSec->startHour) * 60 + pTimeSec->endMinute - ppreTimeSec->startMinute);
	}
}

template<> void exchangeTable<VIDEOCOLOR_PARAM>(CConfigExchange& xchg, CConfigTable& table, VIDEOCOLOR_PARAM& config, int index, int app)
{
	int dftValue = 50;
  	xchg.exchange(table, "Brightness", config.nBrightness, 0, 100, dftValue);
	xchg.exchange(table, "Contrast", config.nContrast, 0, 100, dftValue);
	xchg.exchange(table, "Saturation", config.nSaturation, 0, 100, dftValue);
	xchg.exchange(table, "Hue", config.nHue, 0, 100, dftValue);
	xchg.exchange(table, "Gain", config.mGain, 0, 100 + 0x80, 50 | 0x80);
	xchg.exchange(table, "Whitebalance", config.mWhitebalance, 0, 100, dftValue);//默认白电平使能打开
}

