#include "Configs/ConfigComm.h"
#include "System/AppConfig.h"
//#include "APIs/System.h"

ENUM_MAP em_comm_protocol[] = 
{
    {"Console", CONSOLE},
    {"DH_KBD",  VN_KBD},
    {NULL,    }
};

enum 
{
    NOPARITY = 0,
    ODDPARITY,
    EVENPARITY,
    MARKPARITY,
    SPACEPARITY,
    PARITYS
}ENUMPARITY;

enum
{
    CA_BOUDBASE = 0,
    CA_PARITY = 1,
    CA_DATABITS = 2,
    CA_STOPBITS = 3,
};

ENUM_MAP em_comm_parity[] = 
{
    {"None", NOPARITY},
    {"Odd", ODDPARITY},
    {"Even", EVENPARITY},
    {"Mark", MARKPARITY},
    {"Space", SPACEPARITY},
    {NULL, }
};

template<> void exchangeTable<CONFIG_COMM_X>(CConfigExchange& xchg, CConfigTable& table, CONFIG_COMM_X& config, int index, int app)
{
    xchg.exchange(table, "ProtocolName",
        config.iProtocolName,
        CONSOLE, 
        COM_TYPES,
        CONSOLE,
        em_comm_protocol
        );

    xchg.exchange(table, "PortNo",
        config.iPortNo,
        1, 4,
        (int)AppConfig::instance()->getNumber("Comm.PortNoDefault", 1)
        );

    CommExchange(xchg, table["CommAttri"], config.iCommAttri);
}
#ifdef _USE_720P_MODULE_

//本函数仅供720p的球机vp26m2h调用485相关配置时使用（因设备物理上固定，因此波特率也需要固定）
void CommExchange_vp26m2h(CConfigExchange& configExchange, CConfigTable& table, COMMATTRI& commattri)
{
	configExchange.exchange(table, CA_BOUDBASE, commattri.nBaudBase, 9600, 9600, 9600 );
    configExchange.exchange(table, CA_PARITY,  commattri.iParity, NOPARITY, NOPARITY, NOPARITY, em_comm_parity );
    configExchange.exchange(table, CA_DATABITS, commattri.nDataBits, 8, 8, 8 );
    configExchange.exchange(table, CA_STOPBITS, commattri.iStopBits, 1, 1, 1 );	
}
#endif

void CommExchange(CConfigExchange& configExchange, CConfigTable& table, COMMATTRI& commattri)
{
    configExchange.exchange(table, CA_BOUDBASE,
        commattri.nBaudBase,
        1200,115200,
        115200
    );

    configExchange.exchange(table, CA_PARITY,
        commattri.iParity,
        NOPARITY, PARITYS,
        NOPARITY,
        em_comm_parity
        );
    configExchange.exchange(table, CA_DATABITS,
        commattri.nDataBits,
        4, 8,
        8
        );
    configExchange.exchange(table, CA_STOPBITS,
        commattri.iStopBits,
        1, 2,
        1
        );
}

template<> void exchangeTable<CONFIG_KB_X>(CConfigExchange& xchg, CConfigTable& table, CONFIG_KB_X& config, int index, int app)
{
    xchg.exchange(table, 5,
        config.iProtocol,
        KBDFunc_PTZ_RAW_DATA_IN, 
        KBDFunc_HK,
        KBDFunc_PTZ_RAW_DATA_IN
        );

    xchg.exchange(table, CA_BOUDBASE,
        config.iCommAttri.nBaudBase,
        1200,115200,
        2400
    );

    xchg.exchange(table, CA_PARITY,
        config.iCommAttri.iParity,
        NOPARITY, PARITYS,
        NOPARITY,
        em_comm_parity
        );
    xchg.exchange(table, CA_DATABITS,
        config.iCommAttri.nDataBits,
        4, 8,
        8
        );
    
    xchg.exchange(table, CA_STOPBITS,
        config.iCommAttri.iStopBits,
        1, 2,
        1
        );
}

//#endif

