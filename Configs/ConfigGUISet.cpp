#include "Configs/ConfigGUISet.h"
#include "config-x.h"
#include "APIs/System.h"

extern SYSTEM_CAPS_EX		g_CapsEx;
template<> void exchangeTable<CONFIG_GUISET>(CConfigExchange& xchg, CConfigTable& table, CONFIG_GUISET& config, int index, int app)
{
    xchg.exchange(table, "WindowAlpha", config.iWindowAlpha, 128, 255, 192); //add langzi 2009-11-06 默认半透明
    VD_BOOL BFlag = VD_TRUE;

	xchg.exchange(table, "TimeTitleEn", config.bTimeTitleEn, BFlag);
	xchg.exchange(table, "ChannelTitleEn", config.bChannelTitleEn, BFlag);
	xchg.exchange(table, "AlarmStateEn", config.bAlarmStatus, BFlag);
	xchg.exchange(table, "RecordStateEn", config.bRecordStatus, BFlag);
	xchg.exchange(table, "ChanStateLckEn", config.bChanStateLckEn, BFlag);
	xchg.exchange(table, "ChanStateVlsEn", config.bChanStateVlsEn, BFlag);
	xchg.exchange(table, "ChanStateRecEn", config.bChanStateRecEn, BFlag);
	xchg.exchange(table, "ChanStateMtdEn", config.bChanStateMtdEn, BFlag);
	xchg.exchange(table, "ChanStateBitRateEn", config.bBitRateEn, BFlag);
//数字通道
	xchg.exchange(table, "DigiTimeTitleEn", config.bDigiTimeTitleEn, BFlag);
	xchg.exchange(table, "DigiChannelTitleEn", config.bDigiChannelTitleEn, BFlag);
	xchg.exchange(table, "DigiAlarmStateEn", config.bDigiAlarmStatus, BFlag);
	xchg.exchange(table, "DigiRecordStateEn", config.bDigiRecordStatus, BFlag);
	xchg.exchange(table, "DigiLoginPreview", config.bLoginPreview, 0x00000000, 0xffffff, 0xffffff);
	
	CalibrateExchange(xchg,table["TS_Calibrate"],config.ts_Calibrate);
}

void CalibrateExchange(CConfigExchange& configExchange, CConfigTable& table,TS_CALIBRATE& ts)
{
	CConfigTable &tableDisplay=table["Display"];
	CConfigTable &tableScreen=table["Screen"];

	configExchange.exchange(tableDisplay[0u], "x", ts.ptDisplay[0].x, 0, RELATIVE_MAX_X, 0);
	configExchange.exchange(tableDisplay[0u], "y", ts.ptDisplay[0].y, 0, RELATIVE_MAX_Y, 0);
	configExchange.exchange(tableDisplay[1], "x", ts.ptDisplay[1].x, 0, RELATIVE_MAX_X, RELATIVE_MAX_X);
	configExchange.exchange(tableDisplay[1], "y", ts.ptDisplay[1].y, 0, RELATIVE_MAX_Y, RELATIVE_MAX_Y / 2);
	configExchange.exchange(tableDisplay[2], "x", ts.ptDisplay[2].x, 0, RELATIVE_MAX_X, RELATIVE_MAX_X/2);
	configExchange.exchange(tableDisplay[2], "y", ts.ptDisplay[2].y, 0, RELATIVE_MAX_Y, RELATIVE_MAX_Y);

	configExchange.exchange(tableScreen[0u], "x", ts.ptScreen[0].x, 0, TOUCHSCREEN_MAX_X, 0);
	configExchange.exchange(tableScreen[0u], "y", ts.ptScreen[0].y, 0, TOUCHSCREEN_MAX_Y, TOUCHSCREEN_MAX_Y);
	configExchange.exchange(tableScreen[1], "x", ts.ptScreen[1].x, 0, TOUCHSCREEN_MAX_X, TOUCHSCREEN_MAX_X);
	configExchange.exchange(tableScreen[1], "y", ts.ptScreen[1].y, 0, TOUCHSCREEN_MAX_Y, TOUCHSCREEN_MAX_Y / 2);
	configExchange.exchange(tableScreen[2], "x", ts.ptScreen[2].x, 0, TOUCHSCREEN_MAX_X, TOUCHSCREEN_MAX_X / 2);
	configExchange.exchange(tableScreen[2], "y", ts.ptScreen[2].y, 0, TOUCHSCREEN_MAX_Y, 0);
}

