#include "Configs/ConfigPTZ.h"
#include "APIs/System.h"


void CommExchange(CConfigExchange& configExchange, CConfigTable& table, COMMATTRI& commattri);

template<> void exchangeTable<CONFIG_PTZ>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PTZ& config, int index, int app)
{
	xchg.exchange(table, "ProtocolName", config.strProtocolName, "PELCOD");
	xchg.exchange(table, "DeviceNo",config.ideviceNo,0,255,1);
	xchg.exchange(table, "NuberInMatrixs", config.iNuberInMatrixs, 0, 99, 0);
	xchg.exchange(table, "PortNo", config.iPortNo, 1, 4, 2);

	CommExchange(xchg, table["Attribute"], config.dstComm);
	// 修改云台的波特率默认值为9600
	int default_baudbase = 9600;
	xchg.exchange(table["Attribute"], 0, config.dstComm.nBaudBase, 1200,115200, default_baudbase);

}
#ifdef FUNTION_PRESET_TITLE
template<> void exchangeTable<CONFIG_PRESET>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PRESET& config, int index, int app)
{
	xchg.exchange(table, "Enable", config.Enable, 0,1,1);
	xchg.exchange(table, "x", config.x,0,700,0);
	xchg.exchange(table, "y", config.y, 0, 570, 0);
}
#endif

template<> void exchangeTable<CONFIG_TRACE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_TRACE& config, int index, int app)
{
	int i 		  = 0;
	char str[128] = {0};
	for(i = 0; i < MAXNUM_POINT ; i ++){
		snprintf(str,128,"Trace%d",i);
		xchg.exchange(table[str], "Enable", config.sPoint[i].Enable, 0,1,0);
		xchg.exchange(table[str], "x", config.sPoint[i].x,0,100*1000,0);
		xchg.exchange(table[str], "y", config.sPoint[i].y,0,100*1000,0);

		xchg.exchange(table[str], "Queue", config.sPlan.iPointQueue[i], -1,MAXNUM_POINT-1,-1);
		xchg.exchange(table[str], "StayTime", config.sPlan.iStayTime[i], 0);
	}

	for(i = 0; i < RUNPERIOD_NUM ; i ++){
		snprintf(str,128,"TraceTime%d",i);
		xchg.exchange(table[str], "startHour", config.sRunTime[i].startHour, 0,24,0);
		xchg.exchange(table[str], "startMin", config.sRunTime[i].startMin,0,59,0);
		xchg.exchange(table[str], "startSec", config.sRunTime[i].startSec,0,59,0);

		xchg.exchange(table[str], "endHour", config.sRunTime[i].endHour, 0,24,0);
		xchg.exchange(table[str], "endMin", config.sRunTime[i].endMin,0,59,0);
		xchg.exchange(table[str], "endSec", config.sRunTime[i].endSec,0,59,0);
	}	

	xchg.exchange(table, "TraceEnable", config.bRunTraceEnable,FALSE,TRUE,FALSE);
}