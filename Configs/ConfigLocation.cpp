#include "Configs/ConfigLocation.h"
#include "APIs/Video.h"
#include "System/AppConfig.h"
#include "Language.h"

ENUM_MAP emVideoFormat[] = 
{
	{"Notset", NOTSET},
	{"PAL", VIDEO_STANDARD_PAL},
	{"NTSC", VIDEO_STANDARD_NTSC},
	{"SECAM", VIDEO_STANDARD_SECAM},
	{NULL,	},
};

ENUM_MAP emLanguage[] = 
{
	{"Notset", NOTSET},
	{"English", ENGLISH},
	{"SimpChinese", CHINESE_S},
	{"TradChinese", CHINESE_T},
	{"Italian", ITALIAN},
	{"Spanish", SPANISH},
	{"Japanese", JAPANESE},
	{"Russian", RUSSIAN},
	{"French", FRENCH},
	{"German", GERMAN},
	{"Portugal",PORTUGAL},
	{"Turkey",TURKEY},
	{"Poland",POLAND},
	{"Romanian",ROMANIAN},
	{"Hungarian",HUNGARIAN},
	{"Finnish",FINNISH},
	{"Estonian",ESTONIAN},
	{"Korean",KOREAN},
	{"Farsi",FARSI},
	{"Dansk",DANSK},
	{"Bulgaria",BULGARIA},
	{"Arabic",ARABIC},		
	{"HEBREW",HEBREW},
	{NULL,	},
};

ENUM_MAP emDateFormat[] = 
{
	{"YYMMDD", DF_YYMMDD},
	{"MMDDYY", DF_MMDDYY},
	{"DDMMYY", DF_DDMMYY},
	{NULL,	},
};

ENUM_MAP emDateSeparator[] = 
{
	{".", DS_DOT},
	{"-", DS_DASH},
	{"/", DS_SLASH},
	{NULL,	},
};

ENUM_MAP emTimeFormat[] = 
{
	{"12", TF_12},
	{"24", TF_24},
	{NULL,	},
};

ENUM_MAP emDSTRule[] = 
{
	{"Off", DST_OFF},
	{"Australia", DST_AUSTRALIA},
	{"Italy", DST_ITALY},
	{"American", DST_AMERICAN},
	{NULL,	},
};

#define DST_START 0
#define DST_END 1

void exchangeDSTTimes(CConfigExchange& configExchange, CConfigTable& table, DSTTIMES& dsttimes)
{
	configExchange.exchange(table, "hour", dsttimes.Hour, 0, 23, 1);//0,23,0表示范围为0-23，默认为1
	configExchange.exchange(table, "min", dsttimes.Minute, 0, 59, 0);//0,59,0表示范围为0-59，默认为0
}

void exchangeDST(CConfigExchange& configExchange, CConfigTable& table, DST_POINT& dst, int index, int app, int dst_start_end)
{
	configExchange.exchange(table, "year",  dst.iYear, 2000, 2037, 2009);

	//因为夏令时开始时间和结束时间会造成夏令时判断上的差错，更重要的是
	//开始和结束的时间设置成一样在逻辑上是费解的，因此
	//默认设置夏令时的开始和结束时间不相同。modified by qjwang 091019
	//默认夏令时采用欧洲规则: 三月最后一个星期天到十月最后一个星期天凌晨1点
	if (DST_START == dst_start_end)
	{	
		configExchange.exchange(table, "month",  dst.iMonth, 1, 12, 3);
	}
	if (DST_END == dst_start_end)
	{
		configExchange.exchange(table, "month",  dst.iMonth, 1, 12, 10);
	}
	//end of modification by qjwang 091019
	
	configExchange.exchange(table, "week",  dst.iWeek, -1, 4, -1);//默认为按周设置
	if(dst.iWeek == 0)
	{
		configExchange.exchange(table, "day",  dst.iDays, 1, 31, 1);
	}
	else
	{
		configExchange.exchange(table, "weekday",  dst.iWeekDay, 0, 6, 0);
	}
	exchangeDSTTimes(configExchange, table["Time"], dst.time);
}

template<> void exchangeTable<CONFIG_LOCATION>(CConfigExchange& xchg, CConfigTable& table, CONFIG_LOCATION& config, int index, int app)
{
	xchg.exchange(table, "VideoFormat",
		config.iVideoFormat,
		(int)AppConfig::instance()->getNumber("Global.VideoStand", 0x07),
		//(int)AppConfig::instance()->getNumber("Global.VideoStandDefault", VIDEO_STANDARD_PAL),
		NOTSET,
		emVideoFormat
		);	

	xchg.exchange(table, "Language",
		config.iLanguage,
		(int)AppConfig::instance()->getNumber("Global.Language", 0x87),
		//(int)AppConfig::instance()->getNumber("Global.LanguageDefault", ENGLISH),
		NOTSET,
		emLanguage
		);

	xchg.exchange(table, "OldLanguage",
		config.iOldLanguage,
		(int)AppConfig::instance()->getNumber("Global.Language", 0x87),
		//(int)AppConfig::instance()->getNumber("Global.LanguageDefault", ENGLISH),
		NOTSET,
		emLanguage
		);

	xchg.exchange(table, "DateFormat",
		config.iDateFormat,
		DF_YYMMDD,
		DF_DDMMYY,
		(int)AppConfig::instance()->getNumber("Global.DateFormatDefault", DF_YYMMDD),
		emDateFormat
		);
	
	xchg.exchange(table, "DateSeparator",
		config.iDateSeparator,
		DS_DOT,
		DS_SLASH,
		(int)AppConfig::instance()->getNumber("Global.DateSeparatorDefault", DS_DASH),
		emDateSeparator
		);

	xchg.exchange(table, "TimeFormat",
		config.iTimeFormat,
		TF_24,
		TF_12,
		(int)AppConfig::instance()->getNumber("Global.TimeFormatDefault", TF_24),
		emTimeFormat
		);

	xchg.exchange(table, "DSTRule",
		config.iDSTRule,
		(int)AppConfig::instance()->getNumber("Global.DaylightSavingTime", FALSE)
		);
    	xchg.exchange(table, "WorkDay", config.iWorkDay, 0, 127, 62); //工作日默认1111100, 用位来表示
  	 // xchg.exchange(table, "FreeDay", config.iFreeDay, 0, 127, 96 );//非工作日默认0000011，用位来表示
   	CConfigTable& tbdst = table["DST"];
	CConfigTable& tbdststart= tbdst["start"];
	CConfigTable& tbdstend= tbdst["end"];
	exchangeDST(xchg, tbdststart, config.iDST[0], index, app, DST_START);
	exchangeDST(xchg, tbdstend, config.iDST[1], index, app, DST_END);
}
