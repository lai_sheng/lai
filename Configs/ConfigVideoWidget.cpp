#include "Configs/ConfigVideoWidget.h"


//! 叠加类型
enum emVideoWidgetType
{
	VW_COVERS,				/*!< 区域遮挡 */
	VW_CHANNEL,				/*!< 通道标题 */
	VW_TITLE,				/*!< 时间标题 */
	VW_MARKER,				/*!< 数字水印 */
};

void exchangeChannelRect(CConfigExchange& xchg, CConfigTable& table, VD_RECT& rect);
void exchangeTitleRect(CConfigExchange& xchg, CConfigTable& table, VD_RECT& rect);
void exchangeCover(CConfigExchange& xchg, CConfigTable& table, VIDEO_WIDGET& vw, emVideoWidgetType type);

template<> void exchangeTable<CONFIG_VIDEOWIDGET>(CConfigExchange& xchg, CConfigTable& table, CONFIG_VIDEOWIDGET& config, int index, int app)
{
	CConfigTable& tb = table["Covers"];
	for(int i = 0; i < COVERNUM; i++)
	{
		exchangeCover(xchg, tb[i], config.dstCovers[i], VW_COVERS);
	}
	exchangeCover(xchg, table["ChannelTitle"], config.ChannelTitle, VW_CHANNEL);
	exchangeCover(xchg, table["TimeTitle"], config.TimeTitle, VW_TITLE);
	xchg.exchange(table, "CoversNum", config.iCoverNum, 0, COVERNUM, 0);
}

void exchangeRect(CConfigExchange& xchg, CConfigTable& table, VD_RECT& rect)
{
	xchg.exchange(table, 0, rect.left, 0, 8192, 1024);
	xchg.exchange(table, 1, rect.top, 0, 8192, 1024);
	xchg.exchange(table, 2, rect.right, 0, 8192, 2048);
	xchg.exchange(table, 3, rect.bottom, 0, 8192, 2048);
}

void exchangeChannelRect(CConfigExchange& xchg, CConfigTable& table, VD_RECT& rect)
{
	xchg.exchange(table, 0, rect.left, 0, 8192, 0);
	xchg.exchange(table, 1, rect.top, 0, 8192, 8192 - 24 * 8192/288);
	xchg.exchange(table, 2, rect.right, 0, 8192, 8192);
	xchg.exchange(table, 3, rect.bottom, 0, 8192, 8192);
}

void exchangeTitleRect(CConfigExchange& xchg, CConfigTable& table, VD_RECT& rect)
{
	xchg.exchange(table, 0, rect.left, 0, 8192, 8192/2);
	xchg.exchange(table, 1, rect.top, 0, 8192, 8 * 8192/352);
	xchg.exchange(table, 2, rect.right, 0, 8192, 8192);
	xchg.exchange(table, 3, rect.bottom, 0, 8192, 1024);
}

void exchangeCover(CConfigExchange& xchg, CConfigTable& table, VIDEO_WIDGET& vw, emVideoWidgetType type)
{
	xchg.exchange(table, "FrontColor", vw.rgbaFrontground, VD_RGBA(255, 255, 255, 0));

	xchg.exchange(table, "BackColor", vw.rgbaBackground, VD_RGBA(0, 0, 0, 0));

	switch (type)
	{
	case VW_COVERS:
		exchangeRect(xchg, table["RelativePos"], vw.rcRelativePos);
		break;
	case VW_CHANNEL:
		exchangeChannelRect(xchg, table["RelativePos"], vw.rcRelativePos);
		break;
	case VW_TITLE:
		exchangeTitleRect(xchg, table["RelativePos"], vw.rcRelativePos);
		break;
	default:
		break;
	}
    VD_BOOL dft = VD_FALSE;
	if(type == VW_TITLE)
	{
	    dft = VD_TRUE;
	}
	xchg.exchange(table, "PreviewBlend", vw.bPreviewBlend, dft);
	xchg.exchange(table, "EncodeBlend", vw.bEncodeBlend, dft);
}
