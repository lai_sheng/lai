#include "Configs/ConfigWorksheet.h"

template<> void exchangeTable<CONFIG_WORKSHEET>(CConfigExchange& xchg, CConfigTable& table, CONFIG_WORKSHEET& config, int index, int app)
{
	xchg.exchange(table, "RecNo", config.iName, index, index + 1, index);
	CConfigTable& tb = table["TimeSector"];
	for(int i = 0; i < N_WEEKS; i++)
	{
		CConfigTable& tb1 = tb[i];
		for(int j = 0; j < N_TSECT; j++)
		{
			exchangeTimeSection(xchg, tb1[j], config.tsSchedule[i][j]);			
		}
		if (xchg.getState() == CConfigExchange::CS_DEFAUTING) 
		{
			config.tsSchedule[i][0].enable = FALSE;
			//config.tsSchedule[i][0].enable = TRUE;
		}
	}
}

template<> void exchangeTable<CONFIG_RECSTATE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_RECSTATE& config, int index, int app)
{
	CConfigTable& tb = table["RecState"];
	for(int i = 0; i < N_WEEKS; i++)
	{
		CConfigTable& tb1 = tb[i];
		for(int j = 0; j < N_TSECT; j++)
		{				
			xchg.exchange(tb1[j], "RecState",  config.recstate[i][j], 0, 65535, 0);
		}
		if (xchg.getState() == CConfigExchange::CS_DEFAUTING) 
		{
			config.recstate[i][0] = 0x3;//默认开启动态检测和报警的录像使能
			//config.recstate[i][0] = 0x0;//默认不开启动态检测和报警的录像使能
		}
	}
}

void exchangeTimeSection(CConfigExchange& configExchange, CConfigTable& table, TIMESECTION& timesection)
{
	char buf[32];
    
	switch(configExchange.getState()) {
	case CConfigExchange::CS_DEFAUTING:
		memset(&timesection, 0, sizeof(TIMESECTION));
        timesection.enable = FALSE; //使能默认值
		timesection.endHour = 24;
		break;

	case CConfigExchange::CS_VALIDATING:
		if(timesection.startHour < 0 || timesection.startHour > 24
			|| timesection.startMinute < 0 || timesection.startMinute > 59
			|| timesection.startSecond < 0 || timesection.startSecond > 59
			|| timesection.endHour < 0 || timesection.endHour > 24
			|| timesection.endMinute < 0 || timesection.endMinute > 59
			|| timesection.endSecond < 0 || timesection.endSecond > 59)
		{
			configExchange.setValidity(FALSE);
		}
		break;

	case CConfigExchange::CS_SAVING:
		sprintf(buf,
			"%d %02d:%02d:%02d-%02d:%02d:%02d",
			timesection.enable,
			timesection.startHour,
			timesection.startMinute,
			timesection.startSecond,
			timesection.endHour,
			timesection.endMinute,
			timesection.endSecond);
		table = buf;
		break;

	case CConfigExchange::CS_LOADING:
		sscanf(table.asString().c_str(),
			"%d %02d:%02d:%02d-%02d:%02d:%02d",
			&timesection.enable,
			&timesection.startHour,
			&timesection.startMinute,
			&timesection.startSecond,
			&timesection.endHour,
			&timesection.endMinute,
			&timesection.endSecond);
		break;

	default:
		break;
	}
}

/*根据界面显示工作表设置工作的工作表*/
bool SetConfigWSSchedule(CONFIG_WORKSHEET *pConfigWS,UI_WORKSHEET_SCHEDULE *pUISh)
{
    if ((NULL == pUISh)||(NULL == pConfigWS))
        return false;
    
    for(int ii=0;ii<N_WEEKS;ii++)
    {
       memcpy((void *)pConfigWS->tsSchedule[ii],
              (void *)pUISh->tsSchedule[ii],
              (N_TSECT<N_UI_TSECT ? N_TSECT : N_UI_TSECT)*sizeof(TIMESECTION));
    }

    return true;
}

/*根据实际工作的工作表获取界面显示工作表*/
bool GetUIWSSchedule(CONFIG_WORKSHEET *pConfigWS,UI_WORKSHEET_SCHEDULE *pUISh)
{
    if ((NULL == pUISh)||(NULL == pConfigWS))
        return false;
    
    for(int ii=0;ii<N_WEEKS;ii++)
    {
       memcpy((void *)pUISh->tsSchedule[ii],
              (void *)pConfigWS->tsSchedule[ii],
              (N_TSECT<N_UI_TSECT ? N_TSECT : N_UI_TSECT)*sizeof(TIMESECTION));
    }

    return true;
}


