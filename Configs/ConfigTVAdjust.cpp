#include "Configs/ConfigTVAdjust.h"

template<> void exchangeTable<CONFIG_TVADJUST>(CConfigExchange& xchg, CConfigTable& table, CONFIG_TVADJUST& config, int index, int app)
{
    xchg.exchange(table["Margin"], 0, config.rctMargin.left, 0, 100, 0);
    xchg.exchange(table["Margin"], 1, config.rctMargin.top, 0, 100, 0);
    xchg.exchange(table["Margin"], 2, config.rctMargin.right, 0, 100, 0);
    xchg.exchange(table["Margin"], 3, config.rctMargin.bottom, 0, 100, 0);
    xchg.exchange(table, "Brightness", config.iBrightness, 0, 100, 50);
    xchg.exchange(table, "Contrast", config.iContrast, 0, 100, 70);
	xchg.exchange(table, "AntiDither", config.iAntiDither, 0, 100, 0);	
}

template<> void exchangeTable<CONFIG_CROP>(CConfigExchange& xchg, CConfigTable& table, CONFIG_CROP& config, int index, int app)
{
	xchg.exchange(table, "HorizalOffset", config.iHoffset, 0, 16, 0);
	xchg.exchange(table, "VerticalOffset", config.iVoffset, 0, 16, 0);
	xchg.exchange(table, "CropWidth", config.iWidth, 720, 720, 720);
	xchg.exchange(table, "CropHeight", config.iHeight, 576, 576, 576);
}


