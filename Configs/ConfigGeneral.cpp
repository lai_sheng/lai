#include "Configs/ConfigGeneral.h"
#include "System/AppConfig.h"
#include "APIs/System.h"


ENUM_MAP em_overwrite[] = 
{
	{"StopRecord", 0},
	{"OverWrite", 1},
	{NULL,	},
};

template<> void exchangeTable<CONFIG_GENERAL>(CConfigExchange& xchg, CConfigTable& table, CONFIG_GENERAL& config, int index, int app)
{
	xchg.exchange(table, "LocalNo",
		config.iLocalNo,
		0, 998,
		(int)AppConfig::instance()->getNumber("LocalNoDefault", 8)
		);
	
	xchg.exchange(table, "OverWrite",
		config.iOverWrite,
		0, 1,
		(int)AppConfig::instance()->getNumber("OverWriteDefault", 1),
		em_overwrite
		);

	xchg.exchange(table, "SnapInterval", config.iSnapInterval, 1, 600, 2);
    xchg.exchange(table,"MachineName", config.chMachineName, "IPCAM");
	xchg.exchange(table, "LCDScreenClsDown", config.iLCDScreenClsDown, 0, 120,10);//默认为0，表示永远不会自动关闭LCD屏幕
	xchg.exchange(table,"VideoStartOutPut", config.iVideoStartOutPut, VIDEO_OUTPUT_AUTO,VIDEO_OUTPUT_TV,VIDEO_OUTPUT_VGA);
	xchg.exchange(table,"ScreenSaveTime", config.iScreenSaveTime, 0, 120, 0);
	xchg.exchange(table,"iAutoLogout", config.iAutoLogout, 0, 120, 10);
#if defined(DEF_RESOLUTION_ADJUST)        
	xchg.exchange(table,"iFixType", config.iFixType, ENUM_VIDOUT_1920x1080p60, ENUM_VIDOUT_NUM, ENUM_VIDOUT_1024x768p60);	///< 默认为1080p
#endif

	xchg.exchange(table,"LedSwitch", config.iLedSwitch, 0, 1, 0);
	xchg.exchange(table,"SuspendMode", config.iSuspendMode, 0, 1, 0);
	xchg.exchange(table, "iOnOffMode", config.iOnOffMode, 0, 1, 0);

}

