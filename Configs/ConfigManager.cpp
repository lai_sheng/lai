#include "Configs/ConfigManager.h"
#include "config-x.h"

#include "Configs/ConfigConversion.h"
#include "Configs/ConfigDigiChManager.h"
#include "System/Log.h"
#include "Net/NetApp.h"
#include "APIs/Net.h"
//#include "Net/Dlg/DlgDDNSCli.h"
#include "Language.h"
//#include "Functions/Upgrader.h"
#include <unistd.h>

#include "Configs/ConfigEncode.h"
#include "Devices/DevInfo.h"

#ifdef WIN32
    typedef Json::StyledWriter CConfigWriter;
#else
    typedef Json::FastWriter CConfigWriter;
#endif


char configFileFirst[VD_MAX_PATH] = CONFIG_DIR"/Config1";
char configFileSecond[VD_MAX_PATH] = CONFIG_DIR"/Config2";
#ifdef SUNING 
char configdefualt[VD_MAX_PATH] = CONFIG_DIR"/defualt_config";
#endif

// 配置最大长度暂时固定，以后再动态调节。如果配置长度超过configStreamSize，写配置会变慢。
const int configStreamSize = (512 * 1024);

std::string getString(const CConfigTable &table)
{
    std::string serizal;

    CConfigWriter write(serizal);

    write.write(table);
    return serizal;
}


PATTERN_SINGLETON_IMPLEMENT(CConfigManager);

CConfigManager::CConfigManager() : m_Timer("TConfigManager")
{
}

void CConfigManager::initialize()
{    
    m_changed = FALSE;

    CConfigReader reader;

#ifdef SUNING
	if(0 == access(configdefualt,F_OK))
	{
		//存在defualt_config文件则
		//恢复默认值
		remove(configdefualt);
		remove(configFileFirst);
		remove(configFileSecond);
	}
#endif

    m_stream.reserve(configStreamSize);

    VD_BOOL bToRead2ndFile = TRUE;

    if(readConfig(configFileFirst, m_stream))
    {
        bToRead2ndFile = !reader.parse(m_stream, m_configAll);
    }
    else
    {
        trace("CConfigManager readConfig file1 failed\n");
    }

    if (bToRead2ndFile) 
    {
        trace("CConfigManager::initialize() first config file parsing failed.\n");

        if(readConfig(configFileSecond, m_stream))
        {
            bToRead2ndFile = !reader.parse(m_stream, m_configAll);
        }
        else
        {
            trace("CConfigManager readConfig file2 failed\n");
        }    
    }

    if (bToRead2ndFile)
    {         
        trace("CConfigManager::initialize() second config file parsing failed too.\n");
    }
    
    //初始化配置名称和配置类型映射表    
    m_mapTranslate.clear();
    m_mapTranslate.insert(valueType("Location", CFG_LOCATION));    
    m_mapTranslate.insert(valueType("General", CFG_GENERAL));    

    m_mapTranslate.insert(valueType("SNAPWorksheet", CFG_SNAPWORKSHEET));
    m_mapTranslate.insert(valueType("Record", CFG_RECORD));



    m_mapTranslate.insert(valueType("Comm", CFG_COMM));
 //   m_mapTranslate.insert(valueType("ChannelTitle", CFG_TITLE));
//zsliu add
#ifdef OUT_COLOR_SET
    m_mapTranslate.insert(valueType("TVOUTColorSet", CFG_OUT_COlOR_SET));    
#endif 
	m_mapTranslate.insert(valueType("OSDZoom", CFG_OSD_PARAM));
	
	
    m_mapTranslate.insert(valueType("Encode", CFG_DSPINFO));
    m_mapTranslate.insert(valueType("VideoWidget", CFG_VIDEOWIDGET));
    m_mapTranslate.insert(valueType("Net", CFG_NET));

#if defined (_USE_720P_MODULE_)
    m_mapTranslate.insert(valueType("Camera", CFG_CAMERA));
	m_mapTranslate.insert(valueType("WhiteBalance", CFG_WHITEBALANCE));
  	m_mapTranslate.insert(valueType("Sensor", CFG_SENSOR));
    m_mapTranslate.insert(valueType("DRC", CFG_DRC));
#endif

    m_mapTranslate.insert(valueType("NetCommon", CFG_NET_COMMON));
#ifndef CAMERA2
    m_mapTranslate.insert(valueType("NetNTP", CFG_NET_NTP));
#endif

	m_mapTranslate.insert(valueType("RECState", CFG_RECState));
    m_mapTranslate.insert(valueType("RegServer",CFG_REG_SERVER));

#ifdef VSERVER   //add by kyle xu in 20160107
    m_mapTranslate.insert(valueType("NetVServer", CFG_NET_VSERVER));
#endif

    m_mapTranslate.insert(valueType("NetPreset", CFG_NET_PRESET));
    m_mapTranslate.insert(valueType("SnapAttr",ENCODE_SNAP_STREAM_TYPE));

	m_mapTranslate.insert(valueType("AwakeTime",S1C_AWAKETIME_TYPE));
	m_mapTranslate.insert(valueType("PtzTrace",DEV_PTZ_TRACE));
	m_mapTranslate.insert(valueType(LOCKUSERINFO_STR,DEV_LOCK_USER_INFO));
	
	m_mapTranslate.insert(valueType(LOCKLEAVEMSG_STR,DEV_LOCK_LEAVE_MSG));
	m_mapTranslate.insert(valueType("BatAlarm",DEV_BAT_ALARM_MSG));
	m_mapTranslate.insert(valueType("PirLevel",DEV_PIR_LEVEL_MSG));
	m_mapTranslate.insert(valueType("LockWarnVoice",DEV_LOCK_WARN_VOICE));
	m_mapTranslate.insert(valueType("LockMsgCache",DEV_LOCK_MSG_CACHE));
 
    //装载配置,请不要随意修改下面的name,日志解析中会作为key来使用.
    setupConfig("Location", m_configLocation);    
    
    CONFIG_LOCATION &cfgLocation = m_configLocation.getConfig();

	//制式判断
    if (NOTSET == cfgLocation.iVideoFormat)
    {        
        cfgLocation.iVideoFormat = SystemGetVideoMode();
        m_configLocation.commit();
    }
#ifdef VN_IPC
    //球机的日立机芯做特殊处理，以底层获取的制式为准
    else
    {
        char chType[128] = {0};
        SystemGetDeviceType(chType);
        if (0 == strcmp(chType, "VP26M2H"))
        {
            int iDomeVideoMode = SystemGetVideoMode();
            if (cfgLocation.iVideoFormat != iDomeVideoMode )
            {
                cfgLocation.iVideoFormat = iDomeVideoMode;
                m_configLocation.commit();
            }
        }
    }
#endif
 
    setupConfig("General", m_configGeneral);
    
    setupConfig("RECWorksheet", m_configRECWorksheet);
	
    setupConfig("SNAPWorksheet", m_configSnapWorksheet);
	//休眠时间段
	setupConfig("ScheduleWorksheet",m_configScheduleWorksheet);
	//非告警时间段
	setupConfig("ScheduleWorksheet",m_coffigHemuAlarmWorksheet);
	//hemu 配置
	setupConfig("HemuUserInfo",m_configHemuUserInfo);
	setupConfig("HemuAppCtrl",m_configHemuAppCtrl);

	
    setupConfig("Record", m_configRecord);

	setupConfig("AwakeTime",m_AwakeTime);
	
	setupConfig("Motion", m_configMotion);

#if defined (_USE_720P_MODULE_) 
    setupConfig("Camera", m_configCamera);
	setupConfig("WhiteBalance", m_configWhiteBalance);
	setupConfig("Sensor", m_configSensor);	
	setupConfig("DRC", m_configCameraExt);		
#endif

#ifdef TUPU
	setupConfig("Tupu",m_configTupu);	
#endif
	setupConfig("AiUpload",m_AiUpload);	
	setupConfig("VoicePlayParam",m_VoicePlay); 

	setupConfig("AlarmSwitch",m_AlarmSwitch); 
#ifdef HMSOTA
	setupConfig("HMS",m_configHmsServer);	
	setupConfig("OTA",m_configOtaMode);
#endif

    setupConfig("Comm", m_configComm);
    setupConfig("Encode", m_configEncode);
    setupConfig("OSDZoom", m_configOSDParam);
    setupConfig("VideoWidget", m_configVideoWidget);

    setupConfig("SnapAttr",m_configSnap);
    m_Timer.SetName("ConfigTimer");
    
    setupConfig("NetCommon", m_configNetCommon);

    setupConfig("PtzTrace", m_PtzTrace);
	

#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
	setupConfig("MusicPlayPlan",m_configMusicPlan);
	setupConfig("WifiInfo",m_configWlanInfo);
#endif

#ifdef POLICE_PROJECT 
	setupConfig("LedWorkWeek",m_configLedWorkWeek);
#endif
    setupConfig("NetNTP", m_configNetNTP);
    setupConfig("NetSniffer", m_configNetSniffer);
    setupConfig("RegServer",m_configRegServer);

    setupConfig("AudioInFormat", m_configAudioInFormat);


#ifdef OUT_COLOR_SET
    setupConfig("TVOUTColorSet",m_ConfigTvOutColorSet);
#endif

#ifdef _2761_EXCEPTION_CHK_
	m_mapTranslate.insert(valueType("ENCODERDETECT", CFG_ENCODER_DETECT));
	setupConfig("ENCODERDETECT", m_EncoderDetect);
#endif

	setupConfig(LOCKUSERINFO_STR, m_LockUserInfo);
	setupConfig(LOCKLEAVEMSG_STR, m_LockLeaveMsg);
	setupConfig("BatAlarm",m_configBatAlarm);
	setupConfig("PirLevel",m_configPirLevel);
	setupConfig("LockWarnVoice",m_LockWarnVoice);
	setupConfig("LockMsgCache",m_LockMsgCache);
	
}


// 装载配置
void CConfigManager::setupConfig(const char* name, CConfigExchange& xchg)
{
    m_map.insert(CONFIG_MAP::value_type(name, &xchg));
    m_mapReverse.insert(CONFIG_MAPR::value_type(xchg.getID(), name));
    
    xchg.setTable(m_configAll[name]);
    if(xchg.getUsed() == 0 || !xchg.validate())
    {
        xchg.setUsed(xchg.getNumber()); // 已使用项数初始化为数组长度
        trace("CConfigManager::setupConfig, recall config : '%s'\n", name);
        xchg.recall();
    }
    xchg.getTable(m_configAll[name]); //保存校验后的配置
    m_changed = TRUE;
    xchg.commit(NULL, -1, CONFIG_APPLY_NOT_SAVE); // 更新配置类的静态成员-配置结构的数据
    xchg.attach(this, (TCONFIG_PROC)&CConfigManager::onConfigChanged); //最后注册，这样初始化时不会多于地写配置文件
}

// 保存文件
void CConfigManager::saveFile()
{
    static CMutex fileMutex;
    CGuard l_cGuard(fileMutex);
    CConfigWriter writer(m_stream);

    if(!m_changed)
    {
        return;
    }

    m_changed = FALSE;

    m_stream = "";
    writer.write(m_configAll);

    remove(configFileSecond);
    rename(configFileFirst, configFileSecond);

    m_fileConfig = gzopen(configFileFirst, "wb");
    if((int)m_stream.size() != gzwrite(m_fileConfig, (char*)m_stream.c_str(), m_stream.size()))
    {
        trace("write config file failed!\n");
    }
    gzflush(m_fileConfig, Z_FINISH);
    gzclose(m_fileConfig);
}

// 配置变化时的统一处理
void CConfigManager::onConfigChanged(CConfigExchange& xchg, int& ret)
{
    const char *name = nameFromID(xchg.getID());
    MAP_TRANSLATE::iterator it;

    if(!name)return;

    //更新全局表格
    xchg.getTable(m_configAll[name]);
    m_changed = TRUE;
    
    //保存文件,优先处理延迟保存
    if (ret & CONFIG_APPLY_DELAY_SAVE)
    {
        m_Timer.Start(this, (VD_TIMERPROC)&CConfigManager::onTimer,1000,0);
    }
    else
    {
        if (!(ret & CONFIG_APPLY_NOT_SAVE))
        {
            saveFile();
        }
    }

    it = m_mapTranslate.find(name); 
    if (it != m_mapTranslate.end())
    {
        //记录日志
        g_Log.Append(LOG_CONFSAVE, (*it).second);
    }
}

// 从配置ID得到配置名称
const char* CConfigManager::nameFromID(int id)
{
    CONFIG_MAPR::iterator pi;
    pi = m_mapReverse.find(id);
    if(pi != m_mapReverse.end())
    {
        return (*pi).second.c_str();
    }
    else
    {
        trace("CConfigManager::nameFromID(%d) failed.\n", id);
        return NULL;
    }
}

// 从配置名称得到配置类指针
CConfigExchange* CConfigManager::xchgFromName(const char* name)
{
    CONFIG_MAP::iterator pi;
    pi = m_map.find(name);
    if(pi != m_map.end())
    {
        return (*pi).second;
    }
    else
    {
        trace("CConfigManager::xchgFromName('%s') failed.\n", name);
        return NULL;
    }
}

VD_BOOL CConfigManager::readConfig(const char* chPath, std::string& input)
{
#ifdef IPC_JZ
	static char checkMount = 0;
	//首次读取 ，检查该目录是否挂载
	if(NULL != strstr(chPath,"/mnt") && 0 == checkMount)
	{
		while(checkMount ++ < 3*10)
		{
			if(0 == access("/mnt/.tag",F_OK))
			{
				_printd("checkMount:%d",checkMount);
				break;
			}

			SystemUSleep(100*1000);
			_printd("nCnt:%d",checkMount);
		}
	}
#endif

    m_fileConfig = gzopen(chPath, "rb");
    if(!m_fileConfig)
        return FALSE;

    const int size = 32*1024;
    char* buf = new char[size + 1];
	int nLen = 	0;
		
    input = "";

    while (1)
    {
        nLen = gzread(m_fileConfig, buf, size);

		if(nLen <=0 )	break;
		
        buf[nLen] = 0;
        input += buf;
    }
    input += '\0';
    gzclose(m_fileConfig);
    //input = buf;
    delete []buf;

    return TRUE;
}

int CConfigManager::recallConfig(const char* name, const char* user /* = NULL */, int index /*= -1*/)
{
    CConfigExchange* xchg = xchgFromName(name);

    if(!xchg)
    {
        return CONFIG_APPLY_NOT_EXSIST;
    }
    
    // 更新一下配置，主要是处理一些不需要更新的数据
    xchg->update(index);
    xchg->recall(index);
    return xchg->commit(user, index, CONFIG_APPLY_NOT_SAVE);
}

int CConfigManager::recallConfigAll(const char* user /* = NULL */)
{
    int iRet = 0;
    CONFIG_MAP::iterator pi;

    for(pi = m_map.begin(); pi != m_map.end(); pi++)
    {
    	if((LOCKUSERINFO_STR == (*pi).first || LOCKLEAVEMSG_STR == (*pi).first)){
			_printd("%s %s not recall", LOCKUSERINFO_STR, LOCKLEAVEMSG_STR);
		}
		else{

        	(*pi).second->recall();
        	iRet |= (*pi).second->commit(user, -1, CONFIG_APPLY_NOT_SAVE);
    	}
    }

    if(iRet)
    {
        saveFile();
    }
	
    char PrimaryDNS[24] ={0};
	char SecondDNS[24] ={0};
	NetSetDNSAddress(PrimaryDNS,SecondDNS);

    return iRet;
}                 
void CConfigManager::onTimer(uint arg)
{
    saveFile();
}

int CConfigManager::setDefaultNetConfig()
{
    int iRet = 0;

    IPInfo sysipinfo;
    sysipinfo.HostIP.l = Str2Ip(g_strDefaultHostIp.c_str());
    sysipinfo.SubMask.l = Str2Ip(g_strDefaultNetMask.c_str());
    sysipinfo.GateWay.l = Str2Ip(g_strDefaultGateway.c_str());
    sysipinfo.iDHCP = FALSE;
    g_NetApp.SetNetIPInfo(NULL, &sysipinfo);

    NET_PPPOE_CONFIG cfgpppoe;    
    memset(&cfgpppoe, 0, sizeof(NET_PPPOE_CONFIG));
    NetSetPPPoEConfig(&cfgpppoe);
    return iRet;
}

int CConfigManager::SetDefaultConfig(int iConfigType)
{
    int iRet = 0;
    if ((iConfigType > DEFAULT_CFG_END) || (iConfigType < DEFAULT_CFG_GENERAL))
    {
        tracepoint();
        return CONFIG_APPLY_FILE_ERROR;        
    }
        
    switch(iConfigType)
    {
        case DEFAULT_CFG_GENERAL:
        {
            //不更改语言和视频制式，added by billhe at 2009-6-22
            CConfigLocation cCfgLocation;
            cCfgLocation.update();
            CONFIG_LOCATION &cfgLocation = cCfgLocation.getConfig();
            int iVideoFormat = cfgLocation.iVideoFormat;
            int iLanguage = cfgLocation.iLanguage;  
            int iOldLanguage = cfgLocation.iOldLanguage; //add langzi 2010-1-7
            //普通设置 //nike.xie 语言或制式的改变都不会引发重启.General.cpp中做此修改
            iRet |= recallConfig("General");

            //add langzi 2010-1-7
            cCfgLocation.recall();
            cfgLocation.iVideoFormat = iVideoFormat;
            cfgLocation.iLanguage = iLanguage;
            cfgLocation.iOldLanguage = iOldLanguage; 
            iRet |= cCfgLocation.commit(NULL, -1, CONFIG_APPLY_NOT_SAVE);
   
            cCfgLocation.commit();
            
            //恢复默认后，系统是有声音模式，bSilence的值为FALSE，右键菜单应该显示"静音"字样
            iRet |= recallConfig("AudioInFormat");
            break;
        }
        case DEFAULT_CFG_ENCODE:
        {
            //编码设置
            iRet |= recallConfig("Encode");
            iRet |= recallConfig("VideoWidget");
            iRet |= recallConfig("OSDZoom");
            iRet |= recallConfig("SnapAttr");		
            break;
        }
        case DEFAULT_CFG_RECORD:
        {
            //录像设置
            iRet |= recallConfig("Record");
            iRet |= recallConfig("RECWorksheet");
            iRet |= recallConfig("RECState");
            break;
        }
        case DEFAULT_CFG_COMM:
        {
            //串口设置
            iRet |= recallConfig("Comm");
            break;
        }
        case DEFAULT_CFG_NET:
        {
            //网络设置
 	        iRet |= setDefaultNetConfig();
            iRet |= recallConfig("NetCommon");
            iRet |= recallConfig("NetFTPServer");
            iRet |= recallConfig("NetFTPApplication");
            iRet |= recallConfig("NetIPFilter");
            iRet |= recallConfig("NetMultiCast");
            iRet |= g_Config.recallConfig("NetPPPOE");//uncommented by wyf on 090930 
            iRet |= g_Config.recallConfig("NetDDNS");    //uncommented by billhe at 2009-6-27
            iRet |= recallConfig("NetAlarmServer");
            iRet |= recallConfig("NetNTP");
            iRet |= recallConfig("NetEmail");
            iRet |= recallConfig("NetSniffer");

			iRet |= recallConfig("RegServer");

			iRet |= recallConfig("DHCP");
			iRet |= recallConfig("NetUPNP");     

#ifdef FUNCTION_SUPPORT_RTSP
            iRet |= recallConfig("RtspSet");
#endif            

	       	char PrimaryDNS[24] ={0};

			char SecondDNS[24] ={0};
			NetSetDNSAddress(PrimaryDNS,SecondDNS);
            break;
        }
        case DEFAULT_CFG_ALARM:
        {
            //报警设置
            iRet |= recallConfig("Alarm");
            iRet |= recallConfig("NetAlarm");            
            iRet |= recallConfig("ALMWorksheet");
            break;
        }
        case DEFAULT_CFG_DETECT:
        {
            //视频检测      
         iRet |= recallConfig("Motion");
         iRet |= recallConfig("Loss");
         iRet |= recallConfig("Blind");
	     iRet |= recallConfig("DecAlm");
		 iRet |= recallConfig("MTDWorksheet");
		 iRet |= recallConfig("BLDWorksheet");
		 iRet |= recallConfig("VLTWorksheet");
	     iRet |= recallConfig("SNAPWorksheet");
	     iRet |= recallConfig("DecAlmWorksheet");	 
            break;
        }
        case DEFAULT_CFG_PTZ:
        {
            //云台配置
#ifdef FUNTION_PRESET_TITLE
            iRet |= recallConfig("Preset");
#endif


            iRet |= recallConfig("PTZ");
            iRet |= recallConfig("PTZAlarm");

#ifdef DOME_S2L
	_printd("**************************clear ptz config!");
	system("rm /mnt/mtd/Config/patn*.cfg  /mnt/mtd/Config/pst.cfg  /mnt/mtd/Config/zone.cfg -rf");
#endif
		
            break;
        }
        case DEFAULT_CFG_AUT:
        {
            //输出模式            
            iRet |= recallConfig("MonitorTour");
            iRet |= recallConfig("GUISet");
            iRet |= recallConfig("TVAdjust");
#ifdef OUT_COLOR_SET			
            iRet |= recallConfig("TVOUTColorSet");			
#endif			
            break;
        }
        case DEFAULT_CFG_CHANTITLE:
        {
            //通道名称
            iRet |= recallConfig("ChannelTitle");
            break;
        }
        case DEFAULT_CFG_EXCEP:
        {
            iRet |= recallConfig("StorageNotExist");
            iRet |= recallConfig("StorageFailure");
            iRet |= recallConfig("StorageLowSpace");
            iRet |= recallConfig("NetAbort");
            iRet |= recallConfig("NetArp");                
            break;
        }
        case DEFAULT_CFG_ALL:
        {
            CConfigLocation cCfgLocation;
            cCfgLocation.update();
            CONFIG_LOCATION &cfgLocation = cCfgLocation.getConfig();
            int iVideoFormat = cfgLocation.iVideoFormat;
            int iLanguage = cfgLocation.iLanguage;  
            int iOldLanguage = cfgLocation.iOldLanguage; 

			iRet |= recallConfig("General");
			
			//不更改语言和视频制式，added by billhe at 2009-6-22
            cCfgLocation.recall();
            cfgLocation.iVideoFormat = iVideoFormat;
            cfgLocation.iLanguage = iLanguage;
            cfgLocation.iOldLanguage = iOldLanguage; 
            iRet |= cCfgLocation.commit(NULL, -1, CONFIG_APPLY_NOT_SAVE);
			cCfgLocation.commit();

            //恢复全部,网络端
            trace("Recall all \n"); 
            iRet |= recallConfigAll();
            //网络端的配置恢复默认
            iRet |= setDefaultNetConfig();
   
            //因为夏令时的默认设置为关，因此将CMOS中的这个开关也置为关qjwang 091102
            g_Cmos.WriteFlag(CMOS_DST_TUNE, TRUE);

            //added by wyf on 090924
            //恢复默认后，系统是有声音模式，bSilence的值为FALSE，右键菜单应该显示"静音"字样
            SystemSilence(FALSE);
            //end added by wyf on 090924
            
            break;
        }
        default:
            trace("###########default \n");
            break;
    }

    return iRet;
}

/// 按Json格式导入导出，主要接入使用
int CConfigManager::UnZipConfig(CConfigTable& cfg)
{
    CConfigReader JsonReader;

    //判断文件是否存在
    FileInfo info;
    if (!CFile::Stat(TempZipConfigFile, &info))
    {
        tracef(TempZipConfigFile);
        tracef(" is not exist!\n");
        return -1;
    }
    
    //打开文件
    m_ZipConfig = gzopen(TempZipConfigFile, "rb");    
    if (NULL != m_ZipConfig)
    {
        const int size = 32*1024;
        char* buf = new char[size + 1];

        m_StrConfig = "";

        while (1)
        {        
            int nLen = gzread(m_ZipConfig, buf, size);
            if(nLen <=0 )
                break;
            buf[nLen] = 0;
            m_StrConfig += buf;
        }
        m_StrConfig += '\0';
        gzclose(m_ZipConfig);
        delete []buf;
    }
    else
    {
        return -1;
    }

    //将数据转换成Json格式
    if(JsonReader.parse(m_StrConfig, cfg) == false)
    {
        tracef("config string to json failed!\n");
        return -1;
    }
    return 0;
}

int CConfigManager::WriteConfigData(void* pBuf, int iLen)
{
    FileInfo stFileInfo;

    //判断文件是否存在
    if(CFile::Stat(TempZipConfigFile, &stFileInfo) == true)
    {
        //存在删除文件
        if(CFile::Remove(TempZipConfigFile) == false)
        {
            tracef(TempZipConfigFile);
            tracef(" can not remove!\n");

            return -1;
        }
    }

    //打开创建交换文件
    if(m_pFileReadWrite.Open(TempZipConfigFile, CFile::modeCreate | CFile::modeWrite) == false)
    {
        tracef(TempZipConfigFile);
        tracef(" can not create!\n");
        return -1;
    }
    else if(m_pFileReadWrite.Write(pBuf, iLen) != (uint)iLen)
    {
        tracef(TempZipConfigFile);
        tracef(" write failed!\n");

        m_pFileReadWrite.Close();

        return -1;
    }

    m_pFileReadWrite.Close();
    return 0;
}

int CConfigManager::ZipConfig(CConfigTable& cfg)
{
    //转换Json数据
    m_StrConfig.clear();
    CConfigWriter JsonWriter(m_StrConfig);

    //m_pJsonConfigConvert->write(cfg);
    if(JsonWriter.write(cfg) == false)
    {
        tracef("Convert Json to string failed!\n");
        return -1;
    }

    //打开zip压缩文件
    
    m_ZipConfig = gzopen(TempZipConfigFile, "rb");    
    if(!m_ZipConfig)
    {
        tracef("pZipConfig->Open(%s)\n", TempZipConfigFile);
        return -1;
    }
    //压缩数据    
    else if((int)m_StrConfig.size() != gzwrite(m_ZipConfig, (char*)m_StrConfig.c_str(), m_StrConfig.size()))
    {
        tracef("Zip config data failed!\n");
        gzclose(m_ZipConfig);
        return -1;
    }
    gzflush(m_fileConfig, Z_FINISH);
    gzclose(m_ZipConfig);
    return 0;
}

int CConfigManager::CopyConfigData(void* pBuf, int& iLen)
{
    FileInfo stFileInfo;

    //判断文件是否存在
    if(CFile::Stat(TempZipConfigFile, &stFileInfo) == false)
    {
        tracef(TempZipConfigFile);
        tracef(" is not exist!\n");
        return -1;
    }
    //判断缓冲区长度是否足够
    else if(stFileInfo.size > (uint64)iLen)
    {
        tracef("Buf length is no enough! buf need %d\n", stFileInfo.size);
        return -1;
    }
    //打开文件
    else if(m_pFileReadWrite.Open(TempZipConfigFile, CFile::modeRead) == false)
    {
        return -1;
    }
    //读取数据
    else if((iLen = (uint)m_pFileReadWrite.Read(pBuf, stFileInfo.size)) < 0)
    {
        tracef("file read failed!\n");
        return -1;
    }

    return 0;
}


int CConfigManager::GetConfigFileData(void* pBuf, int& iLen)
{
    //zip config data
    if(ZipConfig(m_configAll) != 0)
    {
        return -1;
    }
    //copy config data
    else if(CopyConfigData(pBuf, iLen) != 0)
    {
        return -1;
    }

    return 0;
}

int CConfigManager::SetConfigFileData(void* pBuf, int iLen, ConfigSetExceptional& cfgExceptional)
{
    Json::Value::Members JsonMember;
    Json::Value::Members::iterator it;
    CConfigTable allConfigTable;        ///< 导入的配置表格
    bool bNeedSave = false;

    //write data to file
    if(WriteConfigData(pBuf, iLen))
    {
        return -1;
    }
    //unzip file
    else if(UnZipConfig(allConfigTable))
    {
        return -1;
    }

    //set config
    JsonMember = allConfigTable.getMemberNames();

    for(it = JsonMember.begin(); it != JsonMember.end(); it++)
    {
        if(cfgExceptional.find(*it) != cfgExceptional.end())
        {
            continue;
        }
        m_configAll[*it] = allConfigTable[*it];
        bNeedSave = true;
    }
    if (bNeedSave)
    {
        m_changed = true;
        saveFile();
        bNeedSave = false;
    }    
    return 0;
}


