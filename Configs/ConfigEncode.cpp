#include "Configs/ConfigEncode.h"
#include "Configs/ConfigLocation.h"
#include "System/AppConfig.h"
#include "APIs/Capture.h"
#include "Devices/DevCapture.h"

#define MIN_CIF_PFRAME_SIZE  7     // CIF最小P帧大小，单位为Kbits
#define MAX_CIF_PFRAME_SIZE  40    // CIF最大P帧大小，单位为Kbits
#define IFRAME_PFRAME_QUOTIENT 3   // 剧烈运动情况下I帧大小与P帧大小之比

ENUM_MAP emVideoCompression[] = 
{
	{"MPEG4", CAPTURE_COMP_DIVX_MPEG4},
	{"MPEG2", CAPTURE_COMP_MPEG2},
	{"MPEG1", CAPTURE_COMP_MPEG1},
	{"MJPG", CAPTURE_COMP_MJPG},
	{"H.263", CAPTURE_COMP_H263},
	{"H.264", CAPTURE_COMP_H264},
	{"H.265", CAPTURE_COMP_H265},
	{NULL, }
};

ENUM_MAP emVideoResolution[] = 
{
	{"D1", CAPTURE_SIZE_D1},
	{"HD1", CAPTURE_SIZE_HD1},
	{"BCIF", CAPTURE_SIZE_BCIF},
	{"CIF", CAPTURE_SIZE_CIF},
	{"QCIF", CAPTURE_SIZE_QCIF},
	{"VGA", CAPTURE_SIZE_VGA},
	{"QVGA", CAPTURE_SIZE_QVGA},
	{"SVCD", CAPTURE_SIZE_SVCD},
	{"QQVGA",CAPTURE_SIZE_QQVGA},
	{"720P",CAPTURE_SIZE_720P},
	{"1080P",CAPTURE_SIZE_1080P},
	{"SVGA",CAPTURE_SIZE_SVGA},
	{"XVGA",CAPTURE_SIZE_XVGA},
	{"WXGA",CAPTURE_SIZE_WXGA},
	{"SXGA",CAPTURE_SIZE_SXGA},
	{"WSXGA",CAPTURE_SIZE_WSXGA},
	{"UXGA",CAPTURE_SIZE_UXGA},
	{"WUXGA",CAPTURE_SIZE_WUXGA},
	{"960P", CAPTURE_SIZE_1280_960},
	{"300W", CAPTURE_SIZE_300W},
	{"500W", CAPTURE_SIZE_500W},
	{"600W", CAPTURE_SIZE_600W},
	{"QFHD", CAPTURE_SIZE_800W},
	{"UHD", CAPTURE_SIZE_4K},
	{"8K", CAPTURE_SIZE_8K},
	{"400W",CAPTURE_SIZE_400W},
	{"896", CAPTURE_SIZE_896_504},
	{"360P",CAPTURE_SIZE_360P},
	{NULL, }
};

ENUM_MAP emVideoBitRateControl[] = 
{
	{"CBR", CAPTURE_BRC_CBR},
	{"VBR",	CAPTURE_BRC_VBR},
	{"MBR",	CAPTURE_BRC_MBR},
	{NULL, }
};

extern enum capture_size_t GetStreamMaxSupportSize(enum capture_channel_t stream);

//! 编码类型，用来区分功能，如主码流还是辅码流
enum ENCODE_STREAM_TYPE
{
	ENCODE_STREAM_MAIN,				/*!< 主码流 */
	ENCODE_STREAM_EXTRA,			/*!< 辅码流 */
	ENCODE_STREAM_SNAP				/*!< 抓图 */
};

void exchangeStreamFormat(CConfigExchange& xchg, CConfigTable& table, MEDIA_FORMAT& format, 
						  int index, int subindex, int streamtype);
void exchangeFormat(CConfigExchange& xchg, CConfigTable& table, MEDIA_FORMAT& format, MEDIA_FORMAT& dftFormat);

void validateEncode(CConfigExchange& xchg, CONFIG_ENCODE& config, int index);

void validateFormat(CConfigExchange& xchg, CONFIG_ENCODE& config, int index);

template<> void exchangeTable<CONFIG_SNAP>(CConfigExchange& xchg, CConfigTable& table, CONFIG_SNAP& config, int index, int app)
{
	xchg.exchange(table,"Enable",config.Enable,0,1,0);
	xchg.exchange(table, "SnapQuality", config.SnapQuality, 1,6,3);
#ifdef SNAPSHOT_EXT  /*抓拍间隔扩大到7200 秒*/	
    xchg.exchange(table, "SnapInterval",  config.SnapInterval, 1, 7200*1000, 2000);
#else
	xchg.exchange(table, "SnapInterval",  config.SnapInterval, 1, 120000, 2000);
#endif
    xchg.exchange(table, "SnapCount", config.SnapCount,0,10,1 );
}

template<> void exchangeTable<CONFIG_ENCODE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_ENCODE& config, int index, int app)
{
//	printf("CONFIG_ENCODE\n");
	static int imgsize, fps;
	if (xchg.getState() == CConfigExchange::CS_VALIDATING)
	{
		//先验证一下分辨率是否一致
		CAPTURE_DSPINFO dspinfo;
		GetDspInfo(&dspinfo);

		if (dspinfo.bChannelMaxSetSync == 1)
		{
			if (index % dspinfo.nMaxSupportChannel == 0)
			{
				imgsize = config.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution;
				fps = config.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS;
			}

			if (imgsize != config.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution)
			{
				if((index % dspinfo.nMaxSupportChannel) >= (CDevCapture::GetChannels() % dspinfo.nMaxSupportChannel))
				{
					config.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.nFPS = fps;
					config.dstMainFmt[ENCODE_TYPE_TIM].vfFormat.iResolution = imgsize;
				}
				else if((index % dspinfo.nMaxSupportChannel) < (CDevCapture::GetChannels() % dspinfo.nMaxSupportChannel))
				{
					xchg.setValidity(FALSE);
				}
				return;
			}
		}
		//为了完成编码验证，保存时必须是全通道
		validateFormat(xchg, config, index);
		validateEncode(xchg, config, index);
		
		if (xchg.getValidity() == FALSE)
		{
			return;
		}
	}

	for (int i = 0; i < ENCODE_TYPE_NUM; i++)
	{
		exchangeStreamFormat(xchg, table["MainFormat"][i], config.dstMainFmt[i], index, i, ENCODE_STREAM_MAIN);
#ifdef SDK_3516
/*防止Sofia 配置为720P, 删除 sensor.ini 文件导致的现实分辨率和实际编码分辨率不一致的问题*/
/*针对设备为 1080P 机芯 可设置 1080P  720P 两种分辨率的设备*/
		VD_SENSOR_TYPE_E tmptype = VideoInGetSensorType(0);
		if((tmptype == PANASONIC_34041_1080P) 
			|| (tmptype == VISIONDIGI_SONY_EH4300_1080P)
			|| (tmptype == BT1120_1080P)
			|| (tmptype == YOKO_RYK_H231)
			|| (tmptype == VISIONDIGI_SAMSUNG_6200)
			|| (tmptype == VISIONDIGI_HITACHI_220_1080P)
			|| (tmptype == VISIONDIGI_LG_1220)
			|| (tmptype == YOKO_RYK_H231) )		
		{
			if(config.dstMainFmt[i].vfFormat.iResolution == CAPTURE_SIZE_720P
				&& VD_SENSOR_NORM ==VideoInGetSensorMode(0))
			{
#ifndef LIVECAM		//modify by jwd on 20161119	
//asdf
				config.dstMainFmt[i].vfFormat.iResolution = CAPTURE_SIZE_1080P;
#endif
			}
		}
	
#endif	
		exchangeStreamFormat(xchg, table["SnapFormat"][i], config.dstSnapFmt[i], index, i, ENCODE_STREAM_SNAP);
	}
	
	CConfigTable& extratb = table["ExtraFormat"];
	for (int i = 0; i < EXTRATYPES; i++)
	{
		exchangeStreamFormat(xchg, extratb[i], config.dstExtraFmt[i], index, i, ENCODE_STREAM_EXTRA);
	}
}

void exchangeFormat(CConfigExchange& xchg, CConfigTable& table, MEDIA_FORMAT& format, MEDIA_FORMAT& dftFormat)
{

	xchg.exchange(table["Video"], "Compression", format.vfFormat.iCompression, 
		CAPTURE_COMP_DIVX_MPEG4, CAPTURE_COMP_H265, dftFormat.vfFormat.iCompression, emVideoCompression); //modify by jwd on 20151207

	xchg.exchange(table["Video"], "Resolution", format.vfFormat.iResolution, 
		CAPTURE_SIZE_D1, CAPTURE_SIZE_NR, dftFormat.vfFormat.iResolution, emVideoResolution);//解决VS100控件分辨率设置不成功的问题modified by wyf on 20100630
	
#if defined(_FUNC_JINANCHENGFENG)
	xchg.exchange(table["Video"], "BitRateControl", format.vfFormat.iBitRateControl,
		CAPTURE_BRC_CBR, CAPTURE_BRC_MBR, CAPTURE_BRC_VBR, emVideoBitRateControl);
#else
	xchg.exchange(table["Video"], "BitRateControl", format.vfFormat.iBitRateControl, 
		CAPTURE_BRC_CBR, CAPTURE_BRC_MBR, dftFormat.vfFormat.iBitRateControl, emVideoBitRateControl);
#endif

	xchg.exchange(table["Video"], "Quality", format.vfFormat.iQuality, 1, 6, dftFormat.vfFormat.iQuality);
	xchg.exchange(table["Video"], "FPS", format.vfFormat.nFPS, -7, getMaxFrame(), dftFormat.vfFormat.nFPS);

	//gop以0.1秒为单位，折算成100ms, 范围是0.1s~10s, added by billhe at 2009-8-16
	xchg.exchange(table["Video"], "GOP", format.vfFormat.iGOP, 1, 100, dftFormat.vfFormat.iGOP); //此处和IPC保持一致

	xchg.exchange(table["Video"], "BitRate", format.vfFormat.nBitRate, 0, ENCODE_MAX_BITRATE, dftFormat.vfFormat.nBitRate);
	xchg.exchange(table["Audio"], "Bitrate", format.afFormat.nBitRate, 0, 100, dftFormat.afFormat.nBitRate);
	xchg.exchange(table["Audio"], "Frequency", format.afFormat.nFrequency, 0, 100, dftFormat.afFormat.nFrequency);

#ifdef _FUNC_ADJUST_VOLUME_   //add langzi 音量控制	
	xchg.exchange(table["Audio"], "LAudioVolumn", format.afFormat.ucLAudioVolumn, 0, 100, dftFormat.afFormat.ucLAudioVolumn);
	xchg.exchange(table["Audio"], "RAudioVolumn", format.afFormat.ucRAudioVolumn, 0, 100, dftFormat.afFormat.ucRAudioVolumn);
#else
	xchg.exchange(table["Audio"], "MaxVolume", format.afFormat.nMaxVolume, 0, 100, dftFormat.afFormat.nMaxVolume);
#endif

	xchg.exchange(table, "VideoEn", format.bVideoEnable, FALSE, TRUE, dftFormat.bVideoEnable);
	xchg.exchange(table, "AudioEn", format.bAudioEnable, FALSE, TRUE, dftFormat.bAudioEnable);
}

void exchangeStreamFormat(CConfigExchange& xchg, CConfigTable& table, MEDIA_FORMAT& format, int index, int subindex, int streamtype)
{
	MEDIA_FORMAT dftFormat;
	CAPTURE_CAPS caps;

	GetCaps(&caps);    

	//优先考虑H265
	if (caps.Compression & BITMSK(CAPTURE_COMP_H265))
	{
		dftFormat.vfFormat.iCompression = CAPTURE_COMP_H265;
	}
	else if (caps.Compression & BITMSK(CAPTURE_COMP_H264))
	{
		dftFormat.vfFormat.iCompression = CAPTURE_COMP_H264;
	}
	else
	{
		dftFormat.vfFormat.iCompression = CAPTURE_COMP_DIVX_MPEG4;
	}	
	if (streamtype == ENCODE_STREAM_MAIN)
	{
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_360P;//(int)AppConfig::instance()->getNumber("Global.DefaultImageSize", CAPTURE_SIZE_720P);

		if(caps.ImageSize & BITMSK(CAPTURE_SIZE_8K))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_8K;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_4K))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_4K;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_800W))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_800W;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_600W))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_600W;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_500W))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_500W;
		}
		else if (caps.ImageSize & BITMSK(CAPTURE_SIZE_400W))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_400W;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_300W))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_300W;
		}
		else if (caps.ImageSize & BITMSK(CAPTURE_SIZE_1080P))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_1080P;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_1280_960))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_1280_960;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_UXGA))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_720P;
		}
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_SXGA))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_SXGA;
		}			
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_720P))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_720P;
		}
#ifdef TUPU
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_896_504))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_SXGA;
		}			
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_360P))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_720P;
		}
#else
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_896_504))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_896_504;
		}			
		else if(caps.ImageSize & BITMSK(CAPTURE_SIZE_360P))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_360P;
		}
#endif
		else
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_D1;
		}
	}
	else if (streamtype == ENCODE_STREAM_EXTRA)
	{
#if defined (VN_IPC) || defined (DVS)
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_CIF;
		if (!(caps.ImageSize & BITMSK(CAPTURE_SIZE_D1)))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_QCIF;
		}		
#else
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_QCIF;
#endif

#ifdef _USE_720P_MODULE_
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_CIF;
	#ifdef SDK_3516
		dftFormat.vfFormat.iResolution = (int)GetStreamMaxSupportSize((enum capture_channel_t)(subindex+CHL_2END_T));
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_VGA;
	#endif
#endif
	}
	else if (streamtype == ENCODE_STREAM_SNAP)
	{
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_D1;
		if (!(caps.ImageSize & BITMSK(CAPTURE_SIZE_D1)))
		{
			dftFormat.vfFormat.iResolution = CAPTURE_SIZE_CIF;
		}		

#ifdef SDK_3516
	dftFormat.vfFormat.iResolution = (int)GetStreamMaxSupportSize(CHL_JPEG_T);
#endif
	}

	dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
	dftFormat.vfFormat.iQuality = 4;
//国联板UDP应用场合，默认I帧间隔半秒
#if defined(SDK_3516)
//有效 Le0
	dftFormat.vfFormat.iGOP = 50;
#else
	dftFormat.vfFormat.iGOP = 20;
#endif


   if(caps.Reserved&BITMSK(0)||caps.Reserved&BITMSK(1)||caps.Reserved&BITMSK(2))
   {
	if (streamtype == ENCODE_STREAM_MAIN)
	{
		dftFormat.vfFormat.iResolution = CAPTURE_SIZE_CIF;
	}
   }
	// 这里需要知道当前的制式，该对象必须在Location对象之后创建
	getMaxDefaultFrame(dftFormat);	
	if (streamtype == ENCODE_STREAM_MAIN)
	{
		//针对非实时
		if (g_CapsEx.ForNRT & BITMSK(index))
		{
			dftFormat.vfFormat.nFPS = 6;
		}
	}
	else if (streamtype == ENCODE_STREAM_SNAP)
	{		
		dftFormat.vfFormat.nFPS = 2;
	}

	dftFormat.afFormat.nBitRate = 10;
	dftFormat.afFormat.nFrequency = 10;

	dftFormat.afFormat.ucLAudioVolumn = 80;   //左声道音量
	dftFormat.afFormat.ucRAudioVolumn = 80;   //右声道音量，单声道的设备此值无效


	dftFormat.bAudioEnable = FALSE;
	dftFormat.bVideoEnable = FALSE;
	
	if (streamtype == ENCODE_STREAM_MAIN)
	{
		dftFormat.bVideoEnable = subindex == 0 ? TRUE : FALSE;
	}

#ifdef VN_IPC //目前，IPC默认编子码流,added by billhe at 2009-9-11
	if (streamtype == ENCODE_STREAM_EXTRA)
	{	
		dftFormat.bVideoEnable = TRUE; //!子码流视频使能，默认都开启
	}
#endif

	if (streamtype == ENCODE_STREAM_SNAP)
	{
		dftFormat.bVideoEnable = TRUE;
	}

	int minSuggestBitRate = 768;
	int maxSuggestBitRate = 4000;
	if(caps.Reserved&BITMSK(0)||caps.Reserved&BITMSK(1)||caps.Reserved&BITMSK(2))
	{
		///< ZSLB默认辅码流打开，帧率为7
		if (streamtype == ENCODE_STREAM_EXTRA)
		{
			dftFormat.bVideoEnable = TRUE;
			 dftFormat.vfFormat.iResolution = CAPTURE_SIZE_QCIF;
			dftFormat.vfFormat.nFPS = 7;
		}
	}
	getImageBitRate(dftFormat.vfFormat.iResolution, dftFormat.vfFormat.nFPS, minSuggestBitRate, maxSuggestBitRate);

	if(CAPTURE_SIZE_500W == dftFormat.vfFormat.iResolution)
	{
		dftFormat.vfFormat.nBitRate = 1024*8;	
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;	
		dftFormat.vfFormat.nFPS = 25;		
	}
	else if (CAPTURE_SIZE_400W == dftFormat.vfFormat.iResolution)
	{
		dftFormat.vfFormat.nBitRate = 8*1024;
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;
		dftFormat.vfFormat.nFPS = 25 ;
	}
	else if(CAPTURE_SIZE_300W == dftFormat.vfFormat.iResolution)
	{
		dftFormat.vfFormat.nBitRate = 1024*6;	
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;	
		dftFormat.vfFormat.nFPS = 25;		
	}
	else if (CAPTURE_SIZE_1080P  == dftFormat.vfFormat.iResolution)
	{
		dftFormat.vfFormat.nBitRate = 1024*4;	
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;	
		dftFormat.vfFormat.nFPS = 25;	
	}
	else if((CAPTURE_SIZE_1280_960 == dftFormat.vfFormat.iResolution) || (CAPTURE_SIZE_UXGA == dftFormat.vfFormat.iResolution) ||(CAPTURE_SIZE_SXGA == dftFormat.vfFormat.iResolution) || (CAPTURE_SIZE_720P == dftFormat.vfFormat.iResolution))
	{
#ifdef TUPU
		dftFormat.vfFormat.nBitRate = 512;//1024*2;
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;	
		dftFormat.vfFormat.nFPS = 25;
#else		
		dftFormat.vfFormat.nBitRate = 640;//1024*2;
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_VBR;
		dftFormat.vfFormat.iQuality = 1;	
		dftFormat.vfFormat.nFPS = 20;
		dftFormat.vfFormat.iGOP = 40;
#endif
	}
	else
	{

#ifdef TUPU
		//设置默认码流大小为512
		dftFormat.vfFormat.nBitRate = 512;
		dftFormat.vfFormat.nFPS = 25;

#else
			//设置默认码流大小为512
		dftFormat.vfFormat.nBitRate = 160;//1024*2;
		dftFormat.vfFormat.iBitRateControl = CAPTURE_BRC_CBR;
		dftFormat.vfFormat.iQuality = 4;	
		dftFormat.vfFormat.nFPS = 20;
		dftFormat.vfFormat.iGOP = 40;
#endif
	}

 	exchangeFormat(xchg, table, format, dftFormat);
}

int getMaxFrame()
{
	int iRetFrame = 6;
	if (CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_PAL)
	{
		iRetFrame = 25;
	}
	else if (CConfigLocation::getLatest().iVideoFormat == VIDEO_STANDARD_NTSC)
	{
		iRetFrame= 30;
	}

	return iRetFrame;
}

int getRetImgSize(int iSimuImg, int iCurImg, int iRetImg)
{
	for (int i = (int)CAPTURE_SIZE_D1; i < CAPTURE_SIZE_NR; i++)
	{
		if ((iSimuImg & BITMSK(i)) && (iCurImg == i))
		{
			return iRetImg;
		}
	}

	return iCurImg;
}

int getRetCompress(int iSimuCompress, int iCurCompress, int iRetCompress)
{
	for (int i = (int)CAPTURE_COMP_DIVX_MPEG4; i < CAPTURE_COMP_NR; i++)
	{
		if ((iSimuCompress & BITMSK(i)) && (iCurCompress == i))
		{
			return iRetCompress;
		}
	}

	return iCurCompress;
}

int getRealCompress(int iCompress)
{
	int retCompress = iCompress;
#ifdef ENC_ADD_MP4_COMPRESS
	retCompress = getRetCompress(ENC_ADD_MP4_COMPRESS, iCompress, CAPTURE_COMP_DIVX_MPEG4);
#endif
	return retCompress;
}

int getRealImageSize(int imgSize)
{
	int retImgSize = imgSize;
#ifdef ENC_ADD_CIF_IMG
	retImgSize = getRetImgSize(ENC_ADD_CIF_IMG, imgSize, CAPTURE_SIZE_CIF);
#endif

#ifdef ENC_ADD_BCIF_IMG
	retImgSize = getRetImgSize(ENC_ADD_BCIF_IMG, imgSize, CAPTURE_SIZE_BCIF);
#endif

#ifdef ENC_ADD_HD1_IMG
	retImgSize = getRetImgSize(ENC_ADD_HD1_IMG, imgSize, CAPTURE_SIZE_HD1);
#endif
	return retImgSize;
}

//CAPTURE_SIZE_300W,CAPTURE_SIZE_500W,CAPTURE_SIZE_600W,CAPTURE_SIZE_800W,CAPTURE_SIZE_4K,CAPTURE_SIZE_8K,CAPTURE_SIZE_400W
#ifdef VN_IPC
static VD_SIZE ntsc_imgsize[CAPTURE_SIZE_NR] 
	= {{704, 512},{704, 256}, {352, 512},{352, 256}, 
	{176, 128}, {640, 480},	{320, 240}, {480, 480}, {160,128}, {1280, 720}, {1920, 1080},
	{800,592},{1024,768},{1280,800},{1280,1024},{1600,1024},{1600,1200},{1920,1200},
	{1280,960},{2048,1536},{2592,1944},{3072,2048},{3840,2160},{4096,2160},{7680,4320},{2688,1520},
	{896,504},{640,360},
	};
static VD_SIZE pal_imgsize[CAPTURE_SIZE_NR]
	= {{704, 576}, {704, 288}, {352, 576}, {352, 288},
	{176, 144}, {640, 480}, {320, 240}, {480, 480},{160,128}, {1280, 720}, {1920, 1080},
	{800,592},{1024,768},{1280,800},{1280,1024},{1600,1024},{1600,1200},{1920,1200},
	{1280,960},{2048,1536},{2592,1944},{3072,2048},{3840,2160},{4096,2160},{7680,4320},{2688,1520},
	{896,504},{640,360},
	};
#else
static VD_SIZE ntsc_imgsize[CAPTURE_SIZE_NR] 
	= {{704, 480},{704, 240}, {352, 480},{352, 240}, 
	{176, 120}, {640, 480},	{320, 240}, {480, 480}, {160,128}, {1280, 720}, {1920, 1080},
	{800,592},{1024,768},{1280,800},{1280,1024},{1600,1024},{1600,1200},{1920,1200},
	{1280,960},{2048,1536},{2592,1944},{3072,2048},{3840,2160},{4096,2160},{7680,4320},{2688,1520}
	};
static VD_SIZE pal_imgsize[CAPTURE_SIZE_NR]
	= {{704, 576},{704, 288},  {352, 576}, {352, 288},
	{176, 144}, {640, 480}, {320, 240}, {480, 480},{160,128}, {1280, 720}, {1920, 1080},
	{800,592},{1024,768},{1280,800},{1280,1024},{1600,1024},{1600,1200},{1920,1200},
	{1280,960},{2048,1536},{2592,1944},{3072,2048},{3840,2160},{4096,2160},{7680,4320},{2688,1520}
	};
#endif
/*!
	\b Description		:	取得指定分辨率的大小\n
	\b Argument			:	SIZE *pSize, int imgtype
	\param[out]	pSize	:	分辨率的大小
	\param[in]	imgtype	:	指定的分辨率
	\return	０：表示成功；非０：失败			

	\b Revisions		:	
*/
int GetImageSize(VD_SIZE *pSize, int imgtype)
{
	
	if ((imgtype >= CAPTURE_SIZE_NR) || (!pSize))
	{
		return -1;
	}

	int realImgType = getRealImageSize(imgtype);
	int video = CConfigLocation::getLatest().iVideoFormat;

	if (video == VIDEO_STANDARD_NTSC)
	{
		*pSize = ntsc_imgsize[realImgType];
	}
	else
	{
		*pSize = pal_imgsize[realImgType];
	}
	return 0;
}

/*!
\b Description		:	计算编码能力，使用分辨率乘上帧率\n
\b Argument			:	BYTE imgtype, BYTE frame
\param	imgtype		:	图像分辨率类型
\param	frame		:	帧率的档次
\return	编码能力			

\b Revisions		:	
*/

uint calculatePower(uchar imgtype, uchar frame)
{
	VD_SIZE imgsize;
	
	GetImageSize(&imgsize, imgtype);

	return (uint)imgsize.w * (uint)imgsize.h * frame;
}

void validateFormat(CConfigExchange& xchg, CONFIG_ENCODE& config, int index)
{
	CAPTURE_CAPS caps;
	CAPTURE_EXT_STREAM stream;

	GetCaps(&caps);  
	GetExtCaps(&stream);

	
	for (int ii = 0; ii < ENCODE_TYPE_NUM; ii++)
	{
		if (stream.ExtraStream & BITMSK(ii))
		{
			// 这里只验证了主码流的数据，辅码流未验证，因为比较的值没有
			if (ii == CHL_MAIN_T)
			{
				for(int jj = ENCODE_TYPE_TIM; jj < ENCODE_TYPE_NUM; jj++)
				{	
					if (!(BITMSK(config.dstMainFmt[jj].vfFormat.iCompression) & caps.Compression))
					{
//						trace("the compress of encode type[%d] is %d\n", jj, config.dstMainFmt[jj].vfFormat.iCompression);
						xchg.setValidity(FALSE);
					}

					if (!(BITMSK(config.dstMainFmt[jj].vfFormat.iResolution) & caps.ImageSize))
					{
//						trace("the imagesize of  encode type[%d] is %d\n", jj, config.dstMainFmt[jj].vfFormat.iResolution);
						xchg.setValidity(FALSE);
					}
					
					//如果音频开的话，视频也开，不允许出现只有音频的情况
					if (config.dstMainFmt[jj].bAudioEnable == TRUE)
					{
						config.dstMainFmt[jj].bVideoEnable = TRUE;
					}
				}
			}
		}
	}	
}

/*!
	\b Description		:	验证编码能力，注意计算的所有通道的，而不是单个通道\n
	\b Argument			:	CConfigExchange& xchg, CONFIG_ENCODE& config, int index
	\param	xchg		:	配置类
	\param	config		:	配置文件
	\param	index		:	配置的通道

	\b Revisions		:	
*/
void validateEncode(CConfigExchange& xchg, CONFIG_ENCODE& config, int index)
{
	//只需要验证每块DSP的第一个通道即可
	// 验证DSP支持的能力
	CAPTURE_DSPINFO dspinfo;
	CAPTURE_EXT_STREAM caps;
	GetDspInfo(&dspinfo);
	GetExtCaps(&caps);

	int supportchannel = dspinfo.nMaxSupportChannel;

	if (index % supportchannel != 0)
	{
		return;
	}
	//将N_SYS_CHN扩展到24引发的问题，这里加个限制，不然会越界
	if(index>=g_nCapture)
	{
		return;
	}
	CONFIG_ENCODE* pCfg = &config;
	
	uint grouppower = 0; //一块DSP的总能力

	//add by xql.xxx 根据实际采集通道所需要的计算能力
	supportchannel = g_nCapture;

	for (int ii = 0; ii < supportchannel; ii++)
	{
		grouppower += getChannelPower(pCfg);
		pCfg++;
	}

	if (grouppower > dspinfo.nMaxEncodePower)
	{
		xchg.setValidity(FALSE);
#if  defined(SDK_3516)
		xchg.setValidity(TRUE);
#endif
	}
}

//返回当前通道从DSP中能够得到的最大编码能力.
uint getChannelRemainedPower(CONFIG_ENCODE *pConfig, int iChannel)
{	
	CAPTURE_DSPINFO dspinfo;
	GetDspInfo(&dspinfo);

	uint supportedchannel = dspinfo.nMaxSupportChannel;
	uint grouppower = 0;
	int bsync = dspinfo.bChannelMaxSetSync;

	CONFIG_ENCODE *pCfg = NULL;
	//MEDIA_FORMAT *pFormat = NULL;

	if (iChannel == -1)
	{
		bsync = 1;
	}
	else
	{
		pCfg = pConfig + iChannel - iChannel % supportedchannel;
	}

	if (bsync == 0)
	{
		for (int i = 0; i < (int)supportedchannel; i++)
		{		
			//先计算和该通道同用一块DSP的通道当前所占用的编码能力
			if (i != iChannel % (int)supportedchannel)
			{
				grouppower += getChannelPower(pCfg);;
			}
			pCfg++;
		}
	}
	else if (bsync == 1)
	{
		if (dspinfo.nMaxSupportChannel == 1)
		{
			grouppower = 0;
		}	
		else
		{
			grouppower = dspinfo.nMaxEncodePower * (dspinfo.nMaxSupportChannel - 1) / dspinfo.nMaxSupportChannel;
		}
	}

	//得到当前通道iChannel能够获取的最大编码能力
	return dspinfo.nMaxEncodePower - grouppower;
}

uint getChannelPower(CONFIG_ENCODE* pConfig)
{
	CAPTURE_EXT_STREAM caps;
	GetExtCaps(&caps);
	uint totalpower = 0; //一个通道的编码能力

	for (int jj = 0; jj < CHL_FUNCTION_NUM; jj++)
	{
		if (caps.ExtraStream & BITMSK(jj))
		{
			totalpower += GetStreamPower(pConfig, jj);
		}
	}
	return totalpower;
}

int GetStreamPower(CONFIG_ENCODE* pConfig, uint type)
{
	uint power = 0;
	uint tmpPower = 0;
	
	switch(type)
	{
		case CHL_MAIN_T:
			for (int i = 0; i < ENCODE_TYPE_NUM; i++)
			{
				if (pConfig->dstMainFmt[i].bVideoEnable)
				{
					tmpPower = calculatePower(pConfig->dstMainFmt[i].vfFormat.iResolution, 
						pConfig->dstMainFmt[i].vfFormat.nFPS);
					power = MAX(power, tmpPower);
				}
			}			
			break;
		case CHL_2END_T:
		case CHL_3IRD_T:
		case CHL_4RTH_T:
			if (pConfig->dstExtraFmt[type - 1].bVideoEnable)
			{
				tmpPower = calculatePower(pConfig->dstExtraFmt[type - 1].vfFormat.iResolution, 
							pConfig->dstExtraFmt[type - 1].vfFormat.nFPS);
						power = MAX(power, tmpPower);
			}
			break;
	}	
	return power;
}

int VD_round(int value, int divisor)
{
	int modulus = value % divisor;
	if(modulus < divisor / 2)
	{
		return value - modulus;
	}
	else
	{
		return value + divisor - modulus;
	}
}

//此码流值主要是根据GBU的实际设备测试出来并经过一定处理。
//不保证数据绝对有效，只是作为一个参考值使用
/*
ImageSize  TotalFrames	 IFrame min(KB/S) max(KB/S) PFrame min(KB/S) max(KB/S)
D1			3			50				90				14			32	

6											13			14

12											8			9

25											7			8

HD1			3			23				33				13			15

6											7			10

12											5			6.5

25											4.3			5

BCIF		3			25				50				10			15

6											7			10

12											5			6.5

25											4.3			5

CIF			3			17				33				4			15

6											5			14

12											4			7

25											2			4

*/

//目前主要计算上述几种分辨率的码流大小
int getImageBitRate(int imageSize, int frames, int &minBitRate, int&maxBitRate)
{
#if 1
	/*
	int GOP = 50;
	int FPS = frames;
	VD_SIZE size;
	GetImageSize(&size, imageSize);

	uint dwUP ,dwDown;
	dwUP = (GOP+IFRAME_PFRAME_QUOTIENT-1)*FPS*MIN_CIF_PFRAME_SIZE;
	dwDown = GOP ;

	minBitRate = (dwUP / dwDown) * size.w * size.h	/ (352 * 288);

	if (minBitRate >= 4)
	{
		minBitRate = VD_round(minBitRate, (1 << log2i(minBitRate)) / 4);  // 按16K，32K，64K等等对齐
	}

	dwUP = (GOP+IFRAME_PFRAME_QUOTIENT-1)*FPS*MAX_CIF_PFRAME_SIZE ;
	dwDown = GOP ;

	maxBitRate = (dwUP / dwDown) * size.w * size.h	/ (352 * 288) ; 
	if (maxBitRate >= 4)
	{
		maxBitRate = VD_round(maxBitRate , (1 << log2i(maxBitRate )) / 4);  // 按16K，32K，64K等等对齐
	}
	//*/

	//LE0 应李龙要求修改最大码率为8M和最低码率为32kbps 同海康一样
	//maxBitRate = 8192;
	//minBitRate = 32;
	//zhongheyuan升级8K 修改最大码率为32M
	maxBitRate = ENCODE_MAX_BITRATE;
	minBitRate = 32;
	return 0;
#else
	if(imageSize > CAPTURE_SIZE_NR - 1 || imageSize < CAPTURE_SIZE_D1)
	{
		trace("Not Support this imageSize\n");
		return 0;
	}

	//以下只是根据统计值的一个粗略计算
	if((imageSize == CAPTURE_SIZE_D1) || (imageSize == CAPTURE_SIZE_VGA))
	{
		minBitRate = 50*8 + 7*8*(frames);
		maxBitRate = 90*8 + 7*8*(frames);
	}
	else if((imageSize == CAPTURE_SIZE_HD1) || (imageSize == CAPTURE_SIZE_SVCD))
	{
		minBitRate =(int)((25*8 + 5*8*(frames))*80/100);
		maxBitRate = 30*8 + 6*8*(frames);
	}
	else if(imageSize == CAPTURE_SIZE_BCIF)
	{
		minBitRate = 25*8 + 5*8*(frames);
		maxBitRate = 30*8 + 6*8*(frames);
	}
	else if(imageSize == CAPTURE_SIZE_CIF)
	{
		minBitRate = 10*8 + 2*8*(frames);
		maxBitRate = 20*8 + 3*8*(frames);
	}
	// 对720P ，暂时写成D1 的2倍
	else if (imageSize == CAPTURE_SIZE_720P)
	{
		minBitRate = (50*8 + 7*8*(frames))*2;
		maxBitRate = (90*8 + 7*8*(frames))*2;
	}
	else
	{
		minBitRate = 256;
		maxBitRate = 512;
	}

	return 0;
#endif
}


//根据默认的分辨率获取最大的帧率
int getMaxDefaultFrame(MEDIA_FORMAT &dftFormat)
{	
	int maxFrame = getMaxFrame();
	CAPTURE_DSPINFO dspinfo;
	GetDspInfo(&dspinfo);

	//uint supportedchannel = dspinfo.nMaxSupportChannel;

	VD_SIZE sizeTmp;
	GetImageSize(&sizeTmp, dftFormat.vfFormat.iResolution);

	int restFrameRate = dspinfo.nMaxEncodePower/ (dspinfo.nMaxSupportChannel * sizeTmp.h * sizeTmp.w);

	dftFormat.vfFormat.nFPS = MIN(maxFrame, restFrameRate);

	return 0;
}

template<> void exchangeTable<CONFIG_AUDIOIN_FORMAT>(CConfigExchange& xchg, CConfigTable&table, CONFIG_AUDIOIN_FORMAT& config, int index, int app)
{
	xchg.exchange(table, "ABitRate", config.BitRate, 128);
	xchg.exchange(table, "ASampleRate", config.SampleRate, 44100);
	xchg.exchange(table, "ASampleBit", config.SampleBit, 8);
	if (0 != index)
	{
		xchg.exchange(table, "AEncodeType", config.EncodeType, 15);
	}
	else
	{
		xchg.exchange(table, "AEncodeType", config.EncodeType, 11);
	}

	uint defaultValue = 0;

#ifdef _DEF_FUNCTION_SUPPORT_97XX
	defaultValue = 0;
#else
	defaultValue = 1;
#endif

	xchg.exchange(table, "ASourceType", config.AudioSourceType, defaultValue); 

	xchg.exchange(table, "ASilence", config.Silence, false);
	xchg.exchange(table, "APlayEnable", config.PlayAudioEnable, true);
	xchg.exchange(table, "ADisplayEnable", config.DisplayAudioEnable, true);
    xchg.exchange(table, "ALongtimeBeepEnable", config.LongtimeBeepEnable, false);

	defaultValue = 50;
    xchg.exchange(table, "ALAudioVolumn", config.LAudioVolumn, 0, 100, defaultValue);
	xchg.exchange(table, "ARAudioVolumn", config.RAudioVolumn, 0, 100, defaultValue);
}


#ifdef _2761_EXCEPTION_CHK_
template<> void exchangeTable<ENCODER_DETCETE>(CConfigExchange& xchg, CConfigTable&table, ENCODER_DETCETE& config, int index, int app)
{
	xchg.exchange(table, "bEnable", config.bEnable, 0, 1, 0);
}
#endif
