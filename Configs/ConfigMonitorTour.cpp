#include "Configs/ConfigMonitorTour.h"
#include "System/AppConfig.h"
#include "APIs/Split.h"


template<> void exchangeTable<CONFIG_MONITORTOUR>(CConfigExchange& xchg, CConfigTable& table, CONFIG_MONITORTOUR& config, int index, int app)
{
#if 0
	xchg.exchange(table, "Enable", config.bEnable, FALSE);
	xchg.exchange(table, "Interval", config.iInterval, 5, 120, 5);

	CConfigTable &tbMask = table["Mask"];
	for(int i=0; i<N_SPLIT; i++)
	{
		xchg.exchange(tbMask, i, config.iMask[i], UINTEGER_MAX);
	}
	CConfigTable &tbMT = table["MotionTourType"];
	for(int i = 0; i < MT_NR; i++)
	{
		xchg.exchange(tbMT, i, config.iMotionTourType[i], 0, SPLIT_NR, (int)AppConfig::instance()->getNumber("Global.TourLinkType", SPLIT1));
	}
	CConfigTable &tbAM = table["AlarmTourType"];
	for(int i = 0; i < AM_NR; i++)
	{
		xchg.exchange(tbAM, i, config.iAlarmTourType[i], 0, SPLIT_NR, (int)AppConfig::instance()->getNumber("Global.TourLinkType", SPLIT1));
	}
#endif
}

