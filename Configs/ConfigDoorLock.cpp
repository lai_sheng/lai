#include "Configs/ConfigDoorLock.h"

template <> void exchangeTable<CONFIG_USERINFO>(CConfigExchange& xchg, CConfigTable& table, CONFIG_USERINFO& config, int index, int app)
{
	char str[64] = {0};
	
	xchg.exchange(table, "Enable", config.bEnable, VD_FALSE);
	
	for(int i = 0; i < _OPEN_OTHER ; i++){
		sprintf(str,"SupportLockOpenType[%d]",i);
		xchg.exchange(table, str, config.bSupportType[i],VD_FALSE,VD_TRUE,VD_FALSE);
	}
}

template <> void exchangeTable<CONFIG_LEAVEMSG>(CConfigExchange& xchg, CConfigTable& table, CONFIG_LEAVEMSG& config, int index, int app)
{
	char str[64] = {0};
	
	xchg.exchange(table, "Enable", config.bEnable, VD_FALSE);
	xchg.exchange(table, "UserNum",  config.iUserNum,0,USERNUM_MAX,0);

	for(int i = 0; i < _OPEN_OTHER ; i++){
		sprintf(str,"SupportLockOpenType[%d]",i);
		xchg.exchange(table, str, config.bSupportType[i],VD_FALSE,VD_TRUE,VD_FALSE);
	}	

	for(int i = 0; i < WDAYS ; i++){
		sprintf(str,"daySwitch[%d]",i);
		xchg.exchange(table, str, config.daySwitch[i],VD_FALSE,VD_TRUE,VD_FALSE);
	}	
	
	xchg.exchange(table, "Replay",   config.iReplay,0,65536,0);
	xchg.exchange(table, "HavePlay", config.iHavePlay,0,65536,0);
	xchg.exchange(table, "PlayUrl",  config.cPlayUrl,"");	
}

template <> void exchangeTable<CONDIG_WARNVOICE>(CConfigExchange& xchg, CConfigTable& table, CONDIG_WARNVOICE& config, int index, int app)
{	
	xchg.exchange(table, "Enable", config.bEnable, VD_FALSE);
	xchg.exchange(table, "PlayUrl", config.cPlayUrl, "");	
}

template <> void exchangeTable<CONFIG_LOCKMSGCACHE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_LOCKMSGCACHE& config, int index, int app)
{	
	xchg.exchange(table, "Enable", config.bEnable, VD_FALSE);
	xchg.exchange(table, "sMsgType", (int&)config.sMsgType,_LOCKMSG_OPEN,_LOCKMSG_OTHER,_LOCKMSG_OTHER);	
	xchg.exchange(table, "iUserNum", config.iUserNum,0,USERNUM_MAX,0);	
	xchg.exchange(table, "sOpenType",(int&)config.sOpenType,_OPEN_KEY,_OPEN_OTHER,_OPEN_OTHER);	
	xchg.exchange(table, "uTimeStamp",config.uTimeStamp,0);
}