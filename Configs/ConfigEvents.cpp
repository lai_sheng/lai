#include "Configs/ConfigEvents.h"
#include "Configs/ConfigWorksheet.h"
#include "Configs/ConfigVideoWidget.h"

#include <limits.h>

ENUM_MAP em_sensorType[] =
{
	{"NC", FALSE},
	{"NO", TRUE},
	{NULL, }
};

ENUM_MAP emPTZLinkType[] = 
{
	{"None", PTZ_LINK_NONE},
	{"Preset", PTZ_LINK_PRESET},
	{"Tour", PTZ_LINK_TOUR},
	{"Pattern", PTZ_LINK_PATTERN},
	{NULL,	}
};

//! 事件处理结构转换
void exchangeEventHandler(CConfigExchange& configExchange, CConfigTable& table, EVENT_HANDLER& hEvent, int index, int app)
{
	configExchange.exchange(table, "Record", hEvent.dwRecord, (uint)BITMSK(index));
	/* 修改BUG：.联动本地录像时，录像长度10－300秒，
	在文本框里输入标点符号"."点击保存，录像长度变成0秒*/
	configExchange.exchange(table, "RecordLatch", hEvent.iRecordLatch, 10, 300, 10);
	configExchange.exchange(table, "Tour", hEvent.dwTour, (uint)BITMSK(index));
	configExchange.exchange(table, "SnapShot", hEvent.dwSnapShot, (uint)BITMSK(index));
	configExchange.exchange(table, "AlarmOut", hEvent.dwAlarmOut, (uint)BITMSK(index));

	configExchange.exchange(table, "AOLatch", hEvent.iAOLatch, 0, 300, 10);

	CConfigTable& tb1 = table["PtzLink"];
	for (int i = 0; i < N_SYS_CH; i++)
	{
		configExchange.exchange(tb1[i], 0, hEvent.PtzLink[i].iType, PTZ_LINK_NONE, PTZ_LINK_PATTERN, PTZ_LINK_NONE, emPTZLinkType);
		configExchange.exchange(tb1[i], 1, hEvent.PtzLink[i].iValue, 0, 255, 0);
	}

	configExchange.exchange(table, "RecordEnabled", hEvent.bRecordEn, FALSE);
	configExchange.exchange(table, "TourEnabled", hEvent.bTourEn, FALSE);
	configExchange.exchange(table, "SnapEnabled", hEvent.bSnapEn, FALSE);

	configExchange.exchange(table, "AlarmOutEnabled", hEvent.bAlarmOutEn, FALSE);
	configExchange.exchange(table, "PtzEnabled", hEvent.bPtzEn, FALSE);
	if (appEventDecConnect != app)
	{
		configExchange.exchange(table, "TipEnabled", hEvent.bTip, FALSE);
	}
	else
	{
		configExchange.exchange(table, "TipEnabled", hEvent.bTip, TRUE);
	}

	//add by james.xu on 090627 to enable TipEnabled of no_disk or diskerr
	if (app == appEventStorageNotExist || app == appEventStorageFailure || app == appEventNetArp )
	{
		configExchange.exchange(table, "TipEnabled", hEvent.bTip, TRUE);
	}

	configExchange.exchange(table, "MailEnabled", hEvent.bMail, FALSE);
	configExchange.exchange(table, "MessageEnabled", hEvent.bMessage, FALSE);
	configExchange.exchange(table, "BeepEnabled", hEvent.bBeep, FALSE);

	configExchange.exchange(table, "VoiceEnabled", hEvent.bVoice, FALSE);
	configExchange.exchange(table, "FTPEnabled", hEvent.bFTP, FALSE);
	configExchange.exchange(table, "WSName", hEvent.iWsName, 0, INT_MAX, (uint)index);
	configExchange.exchange(table, "Matrix", hEvent.dwMatrix, (uint)BITMSK(index));
	configExchange.exchange(table, "MatrixEnabled", hEvent.bMatrixEn, FALSE);
	configExchange.exchange(table, "LogEnabled", hEvent.bLog, FALSE);
	configExchange.exchange(table, "iEventLatch", hEvent.iEventLatch, 0, 600, 0);
	configExchange.exchange(table, "MtoNetEnabled", hEvent.bMessagetoNet, TRUE);
	CConfigTable& tb2 = table["dwReserved"];
	for (int i = 0; i < 7; i++)
	{
		configExchange.exchange(tb2[i], 0, hEvent.dwReserved[i], 0);
	}
}

//! 报警事件结构转换
template<> void exchangeTable<CONFIG_ALARM>(CConfigExchange& xchg, CConfigTable& table, CONFIG_ALARM& config, int index, int app)
{
	xchg.exchange(table, "Enable",
		config.bEnable,
		FALSE, TRUE,
		TRUE
		);
	xchg.exchange(table, "SensorType",
		config.iSensorType,
		FALSE, TRUE,
		TRUE,
		em_sensorType
		);

	exchangeEventHandler(xchg, table["EventHandler"], config.hEvent, index, app);
}


//! 报警解码器事件结构转换
template<> void exchangeTable<CONFIG_ALARMDECODER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_ALARMDECODER& config, int index, int app)
{
	xchg.exchange(table, "Address", config.iAddress, 0, 255, 1 + index);
	xchg.exchange(table, "Enable", config.bEnable, FALSE);

	//OutSlots
	CConfigTable& tbOut = table["OutSlots"];
	for(int i=0; i<ALARMDEC_OUT_SLOTS_NUM; i++)
	{
		xchg.exchange(tbOut, i, config.iOutSlots[i], 0, ALARMDEV_OUT_SLOT_MAP_NUM, 0);
	}

	//InSlots
	CConfigTable& tbIn = table["InSlots"];
	for(int i=0; i<ALARMDEC_IN_SLOTS_NUM; i++)
	{
		xchg.exchange(tbIn[i], "Enable",
			config.InSlots[i].bEnable,
			FALSE, TRUE,
			TRUE
			);

		xchg.exchange(tbIn[i], "SensorType",
			config.InSlots[i].iSensorType,
			FALSE, TRUE,
			TRUE,
			em_sensorType
			);

		exchangeEventHandler(xchg, tbIn[i]["EventHandler"], config.InSlots[i].hEvent, i, app);
	}
}

//! 视屏遮挡事件结构转换
template<> void exchangeTable<CONFIG_BLINDDETECT>(CConfigExchange& xchg, CConfigTable& table, CONFIG_BLINDDETECT& config, int index, int app)
{
#ifdef _VENDOR_HSK
	xchg.exchange(table, "Enable", config.bEnable, FALSE, TRUE, FALSE);
#else
	xchg.exchange(table, "Enable", config.bEnable, FALSE, TRUE, TRUE);
#endif
	xchg.exchange(table, "Level", config.iLevel, 1, 6, 3);
	exchangeEventHandler(xchg, table["EventHandler"], config.hEvent, index, app);
}

//! 动态检测事件结构转换
template<> void exchangeTable<CONFIG_MOTIONDETECT>(CConfigExchange& xchg, CConfigTable& table, CONFIG_MOTIONDETECT& config, int index, int app)
{
	xchg.exchange(table, "Enable", config.bEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "Level", config.iLevel, 1, 6, 4);

	CConfigTable& tableMDRegion = table["Region"];
	for(int i = 0; i < MD_REGION_ROW; i++)
	{
#ifdef VN_IPC
	//按照销售的反馈，将移动侦测的默认设置改为全部不选中
	xchg.exchange(tableMDRegion, i, config.mRegion[i], 0x00000000);
#else
	xchg.exchange(tableMDRegion, i, config.mRegion[i], 0xffffffff);
#endif	
	}
	exchangeEventHandler(xchg, table["EventHandler"], config.hEvent, index, app);
}

//! 存储空间不足事件结构转换
template<> void exchangeTable<tagCONFIG_STORAGE_SPACE_EVENT>(CConfigExchange& xchg, CConfigTable& table, tagCONFIG_STORAGE_SPACE_EVENT& config, int index, int app)
{
	xchg.exchange(table, "Enable", config.enable, FALSE);
	xchg.exchange(table, "LowerLimit", config.lowerLimit, 0, 99, 20);
	exchangeEventHandler(xchg, table["EventHandler"], config.handler, index, app);
}

//! 基本事件结构转换
template<> void exchangeTable<CONFIG_GENERIC_EVENT>(CConfigExchange& xchg, CConfigTable& table, CONFIG_GENERIC_EVENT& config, int index, int app)
{
	switch(app)
	{
		case appEventVideoLoss:
		case appEventDecConnect:
		case appEventStorageNotExist:
		case appEventStorageFailure:
		case appEventAlarmDecoder:
		case appEventNetArp:
			#ifdef _VENDOR_HSK
			xchg.exchange(table, "Enable", config.enable, FALSE, TRUE, FALSE);
			#else
			xchg.exchange(table, "Enable", config.enable, FALSE, TRUE, TRUE);
			#endif
			break;
#ifdef VN_IPC
		case appEventNetAbort:
		    xchg.exchange(table, "Enable", config.enable, FALSE, TRUE, TRUE);
		    break;
#endif
		default:
			xchg.exchange(table, "Enable", config.enable, FALSE);
			break;			
	}
	exchangeEventHandler(xchg, table["EventHandler"], config.handler, index, app);
}

//! 基本事件结构转换
template<> void exchangeTable<CONFIG_ALARMSWITCH>(CConfigExchange& xchg, CConfigTable& table, CONFIG_ALARMSWITCH& config, int index, int app)
{
#ifdef SUNING	
	xchg.exchange(table, "CryEnable", config.bCryEnable, FALSE, TRUE, FALSE);
	xchg.exchange(table, "HumanEnable", config.bHumanEnable, FALSE, TRUE, FALSE);
	xchg.exchange(table, "MotionEnable", config.bMotionEnable, FALSE, TRUE, FALSE);
	xchg.exchange(table, "VoiceEnable", config.bVoiceEnable, FALSE, TRUE, FALSE);
	xchg.exchange(table, "TraceEnable", config.bTraceEnable, FALSE, TRUE, FALSE);
#elif defined(YIDONG)
	xchg.exchange(table, "CryEnable", config.bCryEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "HumanEnable", config.bHumanEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "MotionEnable", config.bMotionEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "VoiceEnable", config.bVoiceEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "TraceEnable", config.bTraceEnable, FALSE, TRUE, FALSE);
#else
	xchg.exchange(table, "CryEnable", config.bCryEnable, FALSE, TRUE, FALSE);
	xchg.exchange(table, "HumanEnable", config.bHumanEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "MotionEnable", config.bMotionEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "VoiceEnable", config.bVoiceEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "TraceEnable", config.bTraceEnable, FALSE, TRUE, FALSE);	
	xchg.exchange(table, "TimeSectionEnable", config.bTimeSectionEnable, FALSE, TRUE, TRUE);
	xchg.exchange(table, "TraceTimeOut", config.iTraceTimeOut, 0, 1000, 10);
#endif
	
}
