#include "Configs/ConfigRecord.h"
#include "System/AppConfig.h"
#include "Configs/ConfigWorksheet.h"


void exchangeTimeSection(CConfigExchange& configExchange, CConfigTable& table, TIMESECTION& timesection);

template<> void exchangeTable<CONFIG_RECORD>(CConfigExchange& xchg, CConfigTable& table, CONFIG_RECORD& config, int index, int app)
{
	xchg.exchange(table, "PreRecord",
		config.iPreRecord,
		0, 300,
		(int)AppConfig::instance()->getNumber("PreRecord", 5)
		);

	xchg.exchange(table, "Redundancy",
		config.bRedundancy,
		(VD_BOOL)AppConfig::instance()->getNumber("RedundancyDefault", FALSE)
		);
    xchg.exchange(table, "PacketLength",
		config.iPacketLength,
		1, 255,
		(int)AppConfig::instance()->getNumber("PacketLengthDefault", 60)
        );

	xchg.exchange(table, "SnapShot", config.bSnapShot, FALSE);
	xchg.exchange(table, "WorksheetName", config.iWsName, index, index + 1, index);
}

