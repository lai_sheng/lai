#include "Configs/ConfigDigiChManager.h"
#include "config-x.h"
#include "Configs/ConfigConversion.h"
#include "System/Log.h"
#include "Net/NetApp.h"
#include "APIs/Net.h"
#include "Language.h"
#include "Net/NetClient/INetConListener.h"

#ifdef WIN32
    typedef Json::StyledWriter CConfigWriter;
#else
    typedef Json::FastWriter CConfigWriter;
#endif

ENUM_MAP em_netserver_type[] = 
{
    {"DahuaII", 0},
    {"DahuaXTJC", 1},
    {"DahuaDSS", 2},
    {"DahuaRtsp", 3},
    {"DECODEVS300", 4},
    {"SonyRtsp", 5},
    {"SonyPDT", 6},
    {"Bell", 7},
    {"RtspHuangHe", 8},
    {"VIVOTEK", 11},
    {"ShangYangRtsp", 12},
    {"SAMSUNGRtsp", 13},
    {"XWRJRtsp", 14},
    {"Langchi", 15},
    {"DAHUAYAAN", 16},
    {"VILAR", 17},//V8
    {"Onvif", 18},
    {"Expand",19},
    {"ZK",20},
    {NULL,	}
};

template<> void exchangeTable<LOCAL_CH_CFG>(CConfigExchange& xchg, CConfigTable& table, LOCAL_CH_CFG& config, int index, int app)
{
#if defined(_FUNC_AUTO_NETWORK)
    xchg.exchange(table, "BEnable", config.BEnable,0,1,1);
#else
    xchg.exchange(table, "BEnable", config.BEnable,0,1,0);
#endif

    xchg.exchange(table, "iMode", config.iMode, 0,1,0);
    xchg.exchange(table, "uiTourTime", config.uiTourTime,10, 600, 10);
    xchg.exchange(table, "uiNotTourCfgIndex", config.uiNotTourCfgIndex,0,MAX_TOUR_NUM - 1, 0);
    xchg.exchange(table, "LocalChannel", config.iLocalChannelNo,0, 32, index);
    xchg.exchange(table, "ChannelType", config.iDeviceChannelType, 0, 255, index>=g_nCapture?0:1);

    xchg.exchange(table, "iDecodePolicy", config.iDecodePolicy, -2,10000,0);//[-2,2] [100,10000]
    xchg.exchange(table, "iAVEnable", config.iAVEnable, 0, 2, 0);

#ifdef _FUNC_AUTO_NETWORK
    xchg.exchange(table, "BAutoNet", config.BAutoNet, 0, 1, 1);
#else
    xchg.exchange(table, "BAutoNet", config.BAutoNet, 0, 1, 0);
#endif

    xchg.exchange(table, "BPTZLocal", config.BPTZLocal, 0, 1, 0);
    
}
template<> void exchangeTable<REMOTE_DIGI_CH_CFG>(CConfigExchange& xchg, CConfigTable& table, REMOTE_DIGI_CH_CFG& config, int index, int app)
{   
    for (int i=0;i<MAX_TOUR_NUM;i++)
    {
        CConfigTable &tableRemoteCfg = table["RemoteCfg"][i];
        xchg.exchange(tableRemoteCfg, "LocalCh", config.stRemoteCfg[i].iLocalChannelNo,0,32,index);    
        xchg.exchange(tableRemoteCfg, "index", config.stRemoteCfg[i].iCfgIndex,0,MAX_TOUR_NUM,i);    
        xchg.exchange(tableRemoteCfg, "Enable", config.stRemoteCfg[i].BEnable, FALSE);

        char acTemp[12]={0,};
        sprintf(acTemp,"ChConfig%02d",i+1);
        xchg.exchange(tableRemoteCfg, "ChName", config.stRemoteCfg[i].cChName, acTemp);
        xchg.exchange(tableRemoteCfg, "DeviceIP", config.stRemoteCfg[i].DeviceIP.l, Str2Ip("0.0.0.0"));
#if defined(_ZHONGKONG)
        xchg.exchange(tableRemoteCfg, "DeviceType", config.stRemoteCfg[i].iDevType, 0, NCPT_NUM, NCPT_ZK, em_netserver_type);
#else
        xchg.exchange(tableRemoteCfg, "DeviceType", config.stRemoteCfg[i].iDevType, 0, NCPT_NUM, 0, em_netserver_type);
#endif

        xchg.exchange(tableRemoteCfg, "DevicePort", config.stRemoteCfg[i].iDevicePort, 0, 65535, 9008);
        xchg.exchange(tableRemoteCfg, "UserName", config.stRemoteCfg[i].cDeviceUserName, "admin");
        xchg.exchange(tableRemoteCfg, "Password", config.stRemoteCfg[i].cDevicePassword, "123456");
        xchg.exchange(tableRemoteCfg, "RemoteChannel", config.stRemoteCfg[i].iRemoteChannelNo, 0, 32, 0);

        xchg.exchange(tableRemoteCfg, "StreamType", config.stRemoteCfg[i].iStreamType, 0, 1, 0);
        xchg.exchange(tableRemoteCfg, "iRemotePtzPreset", config.stRemoteCfg[i].iRemotePtzPreset, 0, 255, 0); 
        xchg.exchange(tableRemoteCfg, "iRemotePtzPresetEnable", config.stRemoteCfg[i].iRemotePtzPresetEnable, 0, 1, 0);
        xchg.exchange(tableRemoteCfg, "iConType", config.stRemoteCfg[i].iConType, 1, 2, 2);

        CConfigTable& tb2 = tableRemoteCfg["Reserved"];        
        for (int ii = 0; ii < 4; ii++)
        {
            xchg.exchange(tb2[ii], 0, config.stRemoteCfg[i].reserverd[ii], 0);
        }
    }
}

char configDigiChFileFirst[VD_MAX_PATH] = CONFIG_DIR"/DigiChConfig1";
char configDigiChFileSecond[VD_MAX_PATH] = CONFIG_DIR"/DigiChConfig2";

// 配置最大长度暂时固定，以后再动态调节。如果配置长度超过configStreamSize，写配置会变慢。
const int DigiChConfigStreamSize = (512 * 1024);

PATTERN_SINGLETON_IMPLEMENT(CConfigDigiChManager);

CConfigDigiChManager::CConfigDigiChManager() : m_Timer("TDigiChConfigManager")
{
    
}

void CConfigDigiChManager::initialize()
{    
    m_changed = FALSE;

    CConfigReader reader;

    m_stream.reserve(DigiChConfigStreamSize);

    VD_BOOL bToRead2ndFile = TRUE;

    if(readConfig(configDigiChFileFirst, m_stream))
    {
        bToRead2ndFile = !reader.parse(m_stream, m_configAll);
    }
    else
    {
        trace("readConfig file1 failed\n");
    }

    if (bToRead2ndFile) 
    {
        trace("CConfigDigiChManager::initialize() first config file parsing failed.\n");

        if(readConfig(configDigiChFileSecond, m_stream))
        {
            bToRead2ndFile = !reader.parse(m_stream, m_configAll);
        }
        else
        {
            trace("readConfig file2 failed\n");
        }    
    }

    if (bToRead2ndFile)
    {         
        trace("CConfigDigiChManager::initialize() second config file parsing failed too.\n");
    }
    
    //初始化配置名称和配置类型映射表    
    m_mapTranslate.clear();
    m_mapTranslate.insert(valueType("LocalDigiChCfg", CFG_DIGI_CHANNEL));
    m_mapTranslate.insert(valueType("RemoteDigiChCfg", CFG_DIGI_CHANNEL));
    
    setupConfig("LocalDigiChCfg", m_CConfigLocalDigiChCfg);
    setupConfig("RemoteDigiChCfg", m_CConfigRemoteDigiChCfg);

#ifdef ZXBELL
    setupConfig("BellResourceCfg", m_CConfigBellResourceCfg);
#endif

    /*第一次起来写一下配置*/
    saveFile();
}


// 装载配置
void CConfigDigiChManager::setupConfig(const char* name, CConfigExchange& xchg)
{
    m_map.insert(CONFIG_MAP::value_type(name, &xchg));
    m_mapReverse.insert(CONFIG_MAPR::value_type(xchg.getID(), name));
    
    xchg.setTable(m_configAll[name]);
    if(xchg.getUsed() == 0 || !xchg.validate())
    {
        xchg.setUsed(xchg.getNumber()); // 已使用项数初始化为数组长度
        trace("CConfigDigiChManager::setupConfig, recall config : '%s'\n", name);
        xchg.recall();
    }
    xchg.getTable(m_configAll[name]); //保存校验后的配置
    m_changed = TRUE;
    xchg.attach(this, (TCONFIG_PROC)&CConfigDigiChManager::onConfigChanged); //最后注册，这样初始化时不会多于地写配置文件
    xchg.commit(NULL, -1, CONFIG_APPLY_NOT_SAVE); // 更新配置类的静态成员-配置结构的数据
}

// 保存文件
void CConfigDigiChManager::saveFile()
{
    static CMutex fileMutex;
    CGuard l_cGuard(fileMutex);
    CConfigWriter writer(m_stream);

    if(!m_changed)
    {
        return;
    }

    /* 正在升级，不保存配置 */
    //if (CUpgrader::instance()->getState())
    //{
    //    return;
    //}

    m_changed = FALSE;

    m_stream = "";
    writer.write(m_configAll);

    remove(configDigiChFileSecond);
    rename(configDigiChFileFirst, configDigiChFileSecond);

    m_fileConfig = gzopen(configDigiChFileFirst, "wb");
    if((int)m_stream.size() != gzwrite(m_fileConfig, (char*)m_stream.c_str(), m_stream.size()))
    {
        trace("write config file failed!\n");
    }
    gzflush(m_fileConfig, Z_FINISH);
    gzclose(m_fileConfig);
}

// 配置变化时的统一处理
void CConfigDigiChManager::onConfigChanged(CConfigExchange& xchg, int& ret)
{
    const char *name = nameFromID(xchg.getID());
    MAP_TRANSLATE::iterator it;

    if(!name)return;

    //更新全局表格
    xchg.getTable(m_configAll[name]);
    m_changed = TRUE;
    
    //保存文件,优先处理延迟保存
    if (ret & CONFIG_APPLY_DELAY_SAVE)
    {
        m_Timer.Start(this, (VD_TIMERPROC)&CConfigDigiChManager::onTimer,1000,0);
    }
    else
    {
        if (!(ret & CONFIG_APPLY_NOT_SAVE))
        {
            saveFile();
        }
    }

    it = m_mapTranslate.find(name); 
    if (it != m_mapTranslate.end())
    {
        //记录日志
        g_Log.Append(LOG_CONFSAVE, (*it).second);
    }
}

// 从配置ID得到配置名称
const char* CConfigDigiChManager::nameFromID(int id)
{
    CONFIG_MAPR::iterator pi;
    pi = m_mapReverse.find(id);
    if(pi != m_mapReverse.end())
    {
        return (*pi).second.c_str();
    }
    else
    {
        trace("CConfigDigiChManager::nameFromID(%d) failed.\n", id);
        return NULL;
    }
}

// 从配置名称得到配置类指针
CConfigExchange* CConfigDigiChManager::xchgFromName(const char* name)
{
    CONFIG_MAP::iterator pi;
    pi = m_map.find(name);
    if(pi != m_map.end())
    {
        return (*pi).second;
    }
    else
    {
        trace("CConfigDigiChManager::xchgFromName('%s') failed.\n", name);
        return NULL;
    }
}

VD_BOOL CConfigDigiChManager::readConfig(const char* chPath, std::string& input)
{
    m_fileConfig = gzopen(chPath, "rb");
    if(!m_fileConfig)
        return FALSE;

    const int size = 32*1024;
    char* buf = new char[size + 1];

    input = "";

    while (1)
    {
        int nLen =     gzread(m_fileConfig, buf, size);
        if(nLen <=0 )
            break;
        buf[nLen] = 0;
        input += buf;
    }
    input += '\0';
    gzclose(m_fileConfig);
    //input = buf;
    delete []buf;

    return TRUE;
}

int CConfigDigiChManager::recallConfig(const char* name, const char* user /* = NULL */, int index /*= -1*/)
{
    CConfigExchange* xchg = xchgFromName(name);

    if(!xchg)
    {
        return CONFIG_APPLY_NOT_EXSIST;
    }
    
    // 更新一下配置，主要是处理一些不需要更新的数据
    xchg->update(index);
    xchg->recall(index);
    return xchg->commit(user, index, CONFIG_APPLY_NOT_SAVE);
}

int CConfigDigiChManager::recallConfigAll(const char* user /* = NULL */)
{
    int iRet = 0;
    CONFIG_MAP::iterator pi;

    for(pi = m_map.begin(); pi != m_map.end(); pi++)
    {
        (*pi).second->recall();
        iRet |= (*pi).second->commit(user, -1, CONFIG_APPLY_NOT_SAVE);
    }

    if(iRet)
    {
        saveFile();
    }

    return iRet;
}                 
void CConfigDigiChManager::onTimer(uint arg)
{
    printf("CConfigDigiChManager::onTimer():in\r\n");
    saveFile();
}

int CConfigDigiChManager::setDefaultNetConfig()
{
    int iRet = 0;

    IPInfo sysipinfo;
    sysipinfo.HostIP.l = Str2Ip(g_strDefaultHostIp.c_str());
    sysipinfo.SubMask.l = Str2Ip(g_strDefaultNetMask.c_str());
    sysipinfo.GateWay.l = Str2Ip(g_strDefaultGateway.c_str());
    sysipinfo.iDHCP = FALSE;
    g_NetApp.SetNetIPInfo(NULL, &sysipinfo);

    NET_PPPOE_CONFIG cfgpppoe;    
    memset(&cfgpppoe, 0, sizeof(NET_PPPOE_CONFIG));
    NetSetPPPoEConfig(&cfgpppoe);
    return iRet;
}

int CConfigDigiChManager::SetDefaultConfig(int iConfigType)
{
    printf("\r\nSetDefaultConfig():#######iConfigType:%d\r\n",iConfigType);
    int iRet = 0;

    iRet |= recallConfig("StorageNotExist");

    return iRet;
}

/// 按Json格式导入导出，主要接入使用
int CConfigDigiChManager::UnZipConfig(CConfigTable& cfg)
{
    CConfigReader JsonReader;

    //判断文件是否存在
    FileInfo info;
    if (!CFile::Stat(TempZipDigiChConfigFile, &info))
    {
        tracef(TempZipDigiChConfigFile);
        tracef(" is not exist!\n");
        return -1;
    }
    
    //打开文件
    m_ZipConfig = gzopen(TempZipDigiChConfigFile, "rb");    
    if (NULL != m_ZipConfig)
    {
        const int size = 32*1024;
        char* buf = new char[size + 1];

        m_StrConfig = "";

        while (1)
        {        
            int nLen = gzread(m_ZipConfig, buf, size);
            if(nLen <=0 )
                break;
            buf[nLen] = 0;
            m_StrConfig += buf;
        }
        m_StrConfig += '\0';
        gzclose(m_ZipConfig);
        delete []buf;
    }
    else
    {
        return -1;
    }

    //将数据转换成Json格式
    if(JsonReader.parse(m_StrConfig, cfg) == false)
    {
        tracef("config string to json failed!\n");
        return -1;
    }
    return 0;
}

int CConfigDigiChManager::WriteConfigData(void* pBuf, int iLen)
{
    FileInfo stFileInfo;

    //判断文件是否存在
    if(CFile::Stat(TempZipDigiChConfigFile, &stFileInfo) == true)
    {
        //存在删除文件
        if(CFile::Remove(TempZipDigiChConfigFile) == false)
        {
            tracef(TempZipDigiChConfigFile);
            tracef(" can not remove!\n");

            return -1;
        }
    }

    //打开创建交换文件
    if(m_pFileReadWrite.Open(TempZipDigiChConfigFile, CFile::modeCreate | CFile::modeWrite) == false)
    {
        tracef(TempZipDigiChConfigFile);
        tracef(" can not create!\n");
        return -1;
    }
    else if(m_pFileReadWrite.Write(pBuf, iLen) != (uint)iLen)
    {
        tracef(TempZipDigiChConfigFile);
        tracef(" write failed!\n");

        m_pFileReadWrite.Close();

        return -1;
    }

    m_pFileReadWrite.Close();
    return 0;
}

int CConfigDigiChManager::ZipConfig(CConfigTable& cfg)
{
    //转换Json数据
    m_StrConfig.clear();
    CConfigWriter JsonWriter(m_StrConfig);

    //m_pJsonConfigConvert->write(cfg);
    if(JsonWriter.write(cfg) == false)
    {
        tracef("Convert Json to string failed!\n");
        return -1;
    }

    //打开zip压缩文件
    
    m_ZipConfig = gzopen(TempZipDigiChConfigFile, "rb");    
    if(!m_ZipConfig)
    {
        tracef("pZipConfig->Open(%s)\n", TempZipDigiChConfigFile);
        return -1;
    }
    //压缩数据    
    else if((int)m_StrConfig.size() != gzwrite(m_ZipConfig, (char*)m_StrConfig.c_str(), m_StrConfig.size()))
    {
        tracef("Zip config data failed!\n");
        gzclose(m_ZipConfig);
        return -1;
    }
    gzflush(m_fileConfig, Z_FINISH);
    gzclose(m_ZipConfig);
    return 0;
}

int CConfigDigiChManager::CopyConfigData(void* pBuf, int& iLen)
{
    FileInfo stFileInfo;

    //判断文件是否存在
    if(CFile::Stat(TempZipDigiChConfigFile, &stFileInfo) == false)
    {
        tracef(TempZipDigiChConfigFile);
        tracef(" is not exist!\n");
        return -1;
    }
    //判断缓冲区长度是否足够
    else if(stFileInfo.size > (uint64)iLen)
    {
        tracef("Buf length is no enough! buf need %d\n", stFileInfo.size);
        return -1;
    }
    //打开文件
    else if(m_pFileReadWrite.Open(TempZipDigiChConfigFile, CFile::modeRead) == false)
    {
        return -1;
    }
    //读取数据
    else if((iLen = (uint)m_pFileReadWrite.Read(pBuf, stFileInfo.size)) < 0)
    {
        tracef("file read failed!\n");
        return -1;
    }

    return 0;
}


int CConfigDigiChManager::GetConfigFileData(void* pBuf, int& iLen)
{
    //Get config data

    //zip config data
    if(ZipConfig(m_configAll) != 0)
    {
        return -1;
    }
    //copy config data
    else if(CopyConfigData(pBuf, iLen) != 0)
    {
        return -1;
    }

    return 0;
}

int CConfigDigiChManager::SetConfigFileData(void* pBuf, int iLen, ConfigSetExceptional& cfgExceptional)
{
    Json::Value::Members JsonMember;
    Json::Value::Members::iterator it;
    CConfigTable allConfigTable;        ///< 导入的配置表格
    bool bNeedSave = false;

    //write data to file
    if(WriteConfigData(pBuf, iLen))
    {
        return -1;
    }
    //unzip file
    else if(UnZipConfig(allConfigTable))
    {
        return -1;
    }

    //set config
    JsonMember = allConfigTable.getMemberNames();

    for(it = JsonMember.begin(); it != JsonMember.end(); it++)
    {
        if(cfgExceptional.find(*it) != cfgExceptional.end())
        {
            continue;
        }
        //CConfigTable& cfg = allConfigTable[*it];
        //std::string strTemp = getString(cfg);
        m_configAll[*it] = allConfigTable[*it];
        //IConfigManager::instance()->setConfig((*it).c_str(), cfg);
        bNeedSave = true;
    }
    if (bNeedSave)
    {
        m_changed = true;
        saveFile();
        bNeedSave = false;
    }    
    return 0;
}

//#endif

