#include "Configs/ConfigPlay.h"
#include "System/AppConfig.h"
#include "Devices/DeviceManager.h"

//extern int g_nPlay;
template<> void exchangeTable<CONFIG_PLAY>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PLAY& config, int index, int app)
{
#if 0
	xchg.exchange(table, "Channels", config.iChannels, 0, g_nPlay, g_nPlay);
	
	for(int i=0; i<CHANNEL; i++)
	{
		xchg.exchange(table["Volume"], i, config.iVolume[i], 0, 100, 70);
	}

	xchg.exchange(table, "SearchMask", config.iSearchMask, BITMSK(BY_TIME), BITMSK(BY_MAX), BITMSK(BY_TIME));
	xchg.exchange(table, "Continue", config.bContinue, TRUE);
#endif
}
#if defined(GUIZHOU_G_PROJECT) || defined(POLICE_PROJECT)
template<> void exchangeTable<CONFIG_MUSICPLAN>(CConfigExchange& xchg, CConfigTable& table, CONFIG_MUSICPLAN& config, int index, int app)
{
	xchg.exchange(table["MusicPlan"], "iVolume", config.iVolume, 0,255,80);	
	xchg.exchange(table["MusicPlan"], "iEnable", config.iEnable, 0,255,0); 	
	xchg.exchange(table["MusicPlan"], "iPlayCount", config.iPlayCount,0,255, 0); 
	xchg.exchange(table["MusicPlan"], "iWeekDay", config.iWeekDay,0,255, 0); 
	xchg.exchange(table["MusicPlan"], "iHour", config.iHour,0,255, 0); 
	xchg.exchange(table["MusicPlan"], "iMiniute", config.iMiniute,0,255, 0); 
	xchg.exchange(table["MusicPlan"], "strMusicFileName", config.strMusicFileName,""); 
}
#endif

template<> void exchangeTable<CONFIG_VOICEPLAYPARAM>(CConfigExchange& xchg, CConfigTable& table, CONFIG_VOICEPLAYPARAM& config, int index, int app)
{
	xchg.exchange(table, "iVolume", config.iVolume, 0,100,100);	
}


