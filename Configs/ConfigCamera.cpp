#if defined (_USE_720P_MODULE_) 

#include "Configs/ConfigCamera.h"

#include "APIs/Video.h"


template<> void exchangeTable<CONFIG_CAMERA>(CConfigExchange& xchg, CConfigTable& table, CONFIG_CAMERA& config, int index, int app)
{
	//以下默认参数，全部来自MT9D131不设置时读取的值
	//仅快门除外(默认为自动曝光模式，快门无效)
	VIDEOIN_CAPS Caps;
	VideoInGetChCaps(0, &Caps);
	
	xchg.exchange(table, "HorReserve", config.HorReverse, MIRROE_OFF, MIRROE_ON, MIRROE_OFF);

#ifdef BPI_S2L	
	xchg.exchange(table, "VerReverse", config.VerReverse, MIRROE_OFF, MIRROE_ON, MIRROE_ON);
#else
	xchg.exchange(table, "VerReverse", config.VerReverse, MIRROE_OFF, MIRROE_ON, MIRROE_OFF);
#endif
	if ((Caps.uiCamAbility) & (BITMSK(2))) 
	{
		if (Caps.uiRes == 0x04)//手动和自动曝光
		{
			xchg.exchange(table, "Exposure", config.ExposureType, EXPOSURE_AUTO, EXPOSURE_MANUAL, EXPOSURE_AUTO);
		}
		else//默认情况，手动，自动和交通模式
		{
			xchg.exchange(table, "Exposure", config.ExposureType, EXPOSURE_AUTO, EXPOSURE_TRAFFIC, EXPOSURE_AUTO);
		}
	}
	//此处上限255 应该用VideoInGetChCaps()获得的，暂时写死
	xchg.exchange(table, "AESensitivity", config.AESensitivity, 0, 255, 10);
	//此处上限15  应该用VideoInGetChCaps()获得的，暂时写死
	xchg.exchange(table, "Backlight", config.Backlight, 0, 15, 15);
	xchg.exchange(table, "WhiteBalance", config.WhiteBalanceMode, WHITE_BALANCE_OFF, WHITE_BALANCE_NR, WHITE_BALANCE_AUTO);
	xchg.exchange(table, "ShadeCorr", config.ShadeCorr, 0, 1, 0); //暂不支持
	xchg.exchange(table, "BadCorr", config.BadCorr, 0, 1, 0); //暂不支持
	xchg.exchange(table, "shutter", config.shutter, 3, 67500, 3000); 
	xchg.exchange(table, "AntiFlicker", config.AntiFlicker, INDOOR_50HZ, ANTI_FLCKER_NR, AUTO_50HZ);   
	xchg.exchange(table, "ColorTemp", config.ColorTemp, COLD_COLOR_TEMP, WARM_COLOR_TEMP, NEUTRUAL_COLOR_TEMP); 
	xchg.exchange(table, "YUVCut", config.YUVCut, YUV_RANGE_0_255, YUV_RANGE_16_235, YUV_RANGE_0_255); 
	xchg.exchange(table, "IRCut", config.IRCut, IR_CUT_AUTO, IR_CUT_CLOSE, IR_CUT_AUTO); 
	xchg.exchange(table, "PAA", config.PAA, PAA_MODE_LOW, PAA_MODE_HIGH, PAA_MODE_HIGH); 
	xchg.exchange(table, "Color2WhiteBlack", config.Color2WhiteBlack, COLOR_2_WHITEBLACK_COLOR, COLOR_2_WHITEBLACK_WHITEBLACK, COLOR_2_WHITEBLACK_AUTO); 

	xchg.exchange(table, "SensorType", config.SensorType, 0, 1, 0);
#if 0
	//Le0 添加标清环回输出状态值到配置表 20141217 
	xchg.exchange(table, "SDCircleOutput", config.res[2], CIRCLE_OUTPUT_MODE_OFF, CIRCLE_OUTPUT_MODE_ON, CIRCLE_OUTPUT_MODE_OFF);
#endif
}
template<> void exchangeTable<CONFIG_WHITEBALANCE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_WHITEBALANCE& config, int index, int app)
{
	xchg.exchange(table, "WhiteBalance", config.WhiteBalanceMode, WHITE_BALANCE_OFF, WHITE_BALANCE_NR, WHITE_BALANCE_AUTO);
	xchg.exchange(table, "wbr", config.wbr, 0, 255, 128);
	xchg.exchange(table, "wbg", config.wbg, 0, 255, 128);
	xchg.exchange(table, "wbb", config.wbb, 0, 255, 128);
	
	xchg.exchange(table, "res0", config.res[0], 0, 1, 0); //保留
	xchg.exchange(table, "res1", config.res[1], 0, 1, 0); //保留
	xchg.exchange(table, "res2", config.res[2], 0, 1, 0); //保留
	xchg.exchange(table, "res3", config.res[3], 0, 1, 0); //保留
}

template<> void exchangeTable<CONFIG_SENSOR>(CConfigExchange& xchg, CConfigTable& table, CONFIG_SENSOR& config, int index, int app)
{
	xchg.exchange(table, "iResolution", config.iResolution, 0, 255, 1);
}

template<> void exchangeTable<CONFIG_CAMERA_EXT>(CConfigExchange& xchg, CConfigTable& table, CONFIG_CAMERA_EXT& config, int index, int app)
{
	xchg.exchange(table, "ispWDRenable", config.ispWDRenable, 0, 1, 1);
	xchg.exchange(table, "ispWDRStrength", config.ispWDRStrength, 0, 255, 128);	
	xchg.exchange(table, "SlowFrameRate", config.SlowFrameRate, 1, 0xf, 1);
	xchg.exchange(table, "snrWDREnable", config.snrWDREnable, 0, 1, 0);
}

#endif

#ifdef TUPU
template<> void exchangeTable<CONFIG_TUPU>(CConfigExchange& xchg, CConfigTable& table, CONFIG_TUPU& config, int index, int app)
{
	xchg.exchange(table, "iSnapFreq", config.iSnapFreq, 1, 3600, 10);

}
#endif
template<> void exchangeTable<CONFIG_AIUPLOADFREQ>(CConfigExchange& xchg, CConfigTable& table, CONFIG_AIUPLOADFREQ& config, int index, int app)
{
	xchg.exchange(table, "iFaceFreq", config.iFaceFreq, 1, 3600, 60);
	xchg.exchange(table, "iStrngerFreq", config.iStrngerFreq, 1, 3600, 60);
	xchg.exchange(table, "iCryFreq", config.iCryFreq, 1, 3600, 10);
}

template<> void exchangeTable<CONFIG_HEMUUSERINFO>(CConfigExchange& xchg, CConfigTable& table, CONFIG_HEMUUSERINFO& config, int index, int app)
{
	xchg.exchange(table, "BindStatus", config.BindStatus, 0, 1, 0);
	xchg.exchange(table, "UserMobile", config.UserMobile, "000000");
	xchg.exchange(table, "UserToken",  config.UserToken, "000000");
	xchg.exchange(table, "devicetoken", config.devicetoken, "000000");
	xchg.exchange(table, "unifiedId",  config.unifiedId, "000000");
	xchg.exchange(table, "ipaddr", config.ipaddr, "0");
	xchg.exchange(table, "port", config.iport, 0, 0xffffffff,0);
	xchg.exchange(table, "ipupdatetime", config.ipupdatetime, "0");
}

template<> void exchangeTable<CONFIG_HEMUAPPCTRL>(CConfigExchange& xchg, CConfigTable& table, CONFIG_HEMUAPPCTRL& config, int index, int app)
{
	xchg.exchange(table, "iAduioSensitivity", config.iAduioSensitivity, 1, 5, 3);
	xchg.exchange(table, "iMotionSensitivity", config.iMotionSensitivity, 1, 5, 3);
	xchg.exchange(table, "iAduioEnable", config.iAduioEnable,0,1,1);
	
}
