#include "Configs/ConfigAutoMaintain.h"
#include "System/AppConfig.h"

ENUM_MAP em_autorebootday[] =
{
	{"Never", 0},
	{"Everyday", 1},
	{"Sunday", 2},
	{"Monday", 3},
	{"Tuesday", 4},
	{"Wednesday", 5},
	{"Thursday", 6},
	{"Friday", 7},
	{"Saturday", 8},
	{NULL, }
};

template<> void exchangeTable<CONFIG_AUTOMAINTAIN>(CConfigExchange& xchg, CConfigTable& table, CONFIG_AUTOMAINTAIN& config, int index, int app)
{
    xchg.exchange(table, "AutoRebootDay", config.iAutoRebootDay, 0, 8,
        (int)AppConfig::instance()->getNumber("Global.DefaultAutoRebootDay", 0), em_autorebootday);
	xchg.exchange(table, "AutoRebootHour", config.iAutoRebootHour, 0, 23, 
		(int)AppConfig::instance()->getNumber("Global.DefaultAutoRebootTime", 0));
	
	xchg.exchange(table, "AutoDeleteFilesDays", config.iAutoDeleteFilesDays, 0, 31, 0);
}
