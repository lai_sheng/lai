#include "Configs/ConfigNet.h"
#include "System/AppConfig.h"
#include "ez_libs/ezutil/str_opr.h"
#include "System/Log.h"
#include "Net/NetWorkService.h"

#include "Configs/ConfigManager.h"
#include "APIs/Net.h"
ENUM_MAP em_net_protocol[] = 
{
    {"TCP", 0},
    {"UDP", 1},
    {"MCAST", 2},
    {NULL,    }
};

char * GetDdnsstrByID( int ddns_id )
{
    switch(ddns_id)
    {
    case DDNS_TYPE_NOIP_ID:
        return NEW_DDNS_TYPE_NOIP;
    case DDNS_TYPE_DAHUA_ID:
        return NEW_DDNS_TYPE_DAHUA;
    case DDNS_TYPE_3322_ID:
        return NEW_DDNS_TYPE_3322;
    case DDNS_TYPE_DYNDNS_ID:
        return NEW_DDNS_DYNDNS;
    case DDNS_TYPE_ORAY_ID:
        return NEW_DDNS_ORAYDNS;
    case DDNS_TYPE_VISIONDIGI_ID:
        return NEW_DDNS_VISIONDIGI;
    case DDNS_TYPE_ZK_ID:                  // gong
        return NEW_DDNS_ZK;
    default:
        return "";
    }
}

const char* hostName = "IPC";
template<> void exchangeTable<CONFIG_NET_COMMON>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_COMMON& config, int index, int app)
{
	char tmpbuf[16] ={0};
	if (g_nCapture ==0 &&  g_nLogicNum > 0)
	{
		strcpy(tmpbuf, "NVR");
	}
	else if (g_nCapture > 0 && g_nLogicNum > g_nCapture)//HVR
	{
	    strcpy(tmpbuf, "HVR");
	}else
	{
		strcpy(tmpbuf, hostName);
	}

    xchg.exchange(table, "HostName",
        config.HostName,
        tmpbuf
        );
    xchg.exchange(table, "HostIP",
        config.HostIP.l,
        (uint)AppConfig::instance()->getNumber("HostIP", Str2Ip("192.168.0.128"))
        );

    xchg.exchange(table, "Submask",
        config.Submask.l,
        (uint)AppConfig::instance()->getNumber("Submask", Str2Ip("255.255.255.0"))
        );

    xchg.exchange(table, "GateWay",
        config.Gateway.l,
        (uint)AppConfig::instance()->getNumber("GateWay", Str2Ip("192.168.0.1"))
        );
    xchg.exchange(table, "HttpPort",
        config.HttpPort,0, 65535,
    (int)AppConfig::instance()->getNumber("Global.DefaultHttpIp",80)       
        );

    xchg.exchange(table, "TCPPort", config.TCPPort, 0, 65535, (int)AppConfig::instance()->getNumber("Global.DefaultTCPPort", 9008));
        
        
    xchg.exchange(table, "TCPMaxConn",
        config.MaxConn,0, 50,
        (int)AppConfig::instance()->getNumber("TCPMaxConn", 50)
        );

    xchg.exchange(table, "SSLPort",
        config.SSLPort,0, 65535,
        (int)AppConfig::instance()->getNumber("SSLPort", 8443)
        );
    xchg.exchange(table, "UDPPort",
        config.UDPPort,0, 65535,
        (int)AppConfig::instance()->getNumber("UDPPort", 37778)
        );
    xchg.exchange(table, "MonMode",
        config.MonMode,
        0, 2,
        (int)AppConfig::instance()->getNumber("MonMode", 0),
        em_net_protocol
        );
    xchg.exchange(table, "MaxBps",
        config.MaxBps,
        0, 65535,
        0
        );
    xchg.exchange(table, "TransferPlan", config.TransferPlan, IMAGE_TRANSFER_QUAILITY_PRIOR, IMAGE_AUTOADAPT, IMAGE_FLUENCY_PRIOR);

    xchg.exchange(table, "bUseTransferPlan",config.bUseTransferPlan, FALSE);

    xchg.exchange(table, "bUseHSDownLoad", config.bUseHSDownLoad, FALSE);
}

void exchangeServer(CConfigExchange& configExchange, CConfigTable& table, REMOTE_SERVER& server)
{
    configExchange.exchange(table, "ip", server.ip.l, (uint)0);
    configExchange.exchange(table, "ServerName", server.ServerName, "\0");
    configExchange.exchange(table, "Port", server.Port, 0, 65535, 1024);
    configExchange.exchange(table, "UserName", server.UserName, "\0");
    configExchange.exchange(table, "Password",server.Password, "\0"); 
    configExchange.exchange(table, "Anonymity", server.Anonymity, FALSE);
}

ENUM_MAP em_ipfilt_type[] = 
{
    {"Trust", 0},
    {"Banned", 1},
    {NULL,    }
};
template<> void exchangeTable<CONFIG_NET_IPFILTER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_IPFILTER& config, int index, int app)
{
    int i;
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    xchg.exchange(table, "FilterType", config.FilterType, 0,1, 0, em_ipfilt_type);
    CConfigTable& tb = table["Banned"];
    
    for(i = 0; i < MAX_FILTERIP_NUM; i++)
    {
        xchg.exchange(tb, i,config.BannedList[i].l ,(uint)0);
    }

    CConfigTable& tb1 = table["Trusted"];
    for(i = 0; i < MAX_FILTERIP_NUM; i++)
    {
        xchg.exchange(tb1, i,config.TrustList[i].l ,(uint)0);
    }
}

template<> void exchangeTable<CONFIG_NET_MULTICAST>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_MULTICAST& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    exchangeServer(xchg, table["MCastServer"], config.Server);
    xchg.exchange(table["MCastip"], "ip",  config.Server.ip.l, uint(Str2Ip("239.255.42.42")));
    xchg.exchange(table["MCastport"], "Port",  config.Server.Port, 0, 65535, 36666);
    xchg.exchange(table["MCastServer"], "ServerName", config.Server.ServerName, "MCastServer");

}
template<> void exchangeTable<CONFIG_NET_PPPOE>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_PPPOE& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    exchangeServer(xchg, table["PPPOEServer"], config.Server);
    xchg.exchange(table["PPPOEServer"], "ServerName", config.Server.ServerName, "PPPOEServer");
}

#ifdef VSERVER   //add by kyle xu in 20160107
template<> void exchangeTable<CONFIG_NET_VSERVER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_VSERVER& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
	xchg.exchange(table["VServerip"], "ip",  config.Server.ip.l, uint(Str2Ip("211.139.201.225")));
    xchg.exchange(table["VServerPort"], "Port",  config.Server.Port, 0, 65535, 554);
}
#endif

template<> void exchangeTable<CONFIG_NET_DDNS>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_DDNS& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    xchg.exchange(table, "DDNSKey", config.DDNSKey, 0,DDNS_MAX_NUM,index);
    exchangeServer(xchg, table["DDNSServer"], config.Server);
    xchg.exchange(table["DDNSServer"], "ServerName", config.Server.ServerName, "\0");
    xchg.exchange(table["DDNSServer"], "ip", config.Server.ip.l, (uint)Str2Ip("\0"));
    xchg.exchange(table["DDNSServer"], "Port", config.Server.Port, 0, 49151,80);
}

ENUM_MAP em_alarmserver_type[] = 
{
    {"JUFENG", 0},
    {"BOSH", 1},
    {"Sel_Defined", 2},
    {NULL,    }
};
template<> void exchangeTable<CONFIG_NET_ALARMSERVER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_ALARMSERVER& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    xchg.exchange(table, "AlarmServerKey", config.AlarmServerKey, 0, 2, 0, em_alarmserver_type);
    exchangeServer(xchg, table["ALarmServer"], config.Server);
    xchg.exchange(table["Alarmip"], "ip",  config.Server.ip.l, (uint)Str2Ip("10.1.0.2"));
    xchg.exchange(table["Alarmport"], "Port",  config.Server.Port, 0, 65535, 0);
    xchg.exchange(table["ALarmServer"], "ServerName", config.Server.ServerName, "ALarmServer");
}
template<> void exchangeTable<CONFIG_FTP_SERVER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_FTP_SERVER& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    xchg.exchange(table, "SpareIP", config.SpareIP.l, (uint)0);
    exchangeServer(xchg, table["Server"], config.Server);

    std::string server_name_record, server_name_image;

    server_name_record = "Record FTP";
    server_name_image = "Image FTP";
    if(index == 0)
    {
        xchg.exchange(table["Server"], "ServerName", config.Server.ServerName, server_name_record.c_str());
    }
    else
    {
        xchg.exchange(table["Server"], "ServerName", config.Server.ServerName, server_name_image.c_str());
    }
    xchg.exchange(table, "RemotePathName",  config.RemotePathName, "\0");
    xchg.exchange(table, "FileMaxLen",  config.FileMaxLen, 0,65536, 0);
    CConfigTable& tb2 = table["UpLoadPeriod"];
    for(int i = 0; i < N_MIN_TSECT; i++)
    {
        exchangeTimeSection(xchg, tb2[i], config.UpLoadPeriod[i]);
    }
}

template<> void exchangeTable<CONFIG_FTP_APPLICATION>(CConfigExchange& xchg, CConfigTable& table, CONFIG_FTP_APPLICATION& config, int index, int app)
{
    char buf[32];
    sprintf(buf, "Period chan[%d]",index);
    CConfigTable&tb = table[buf];
    for(int i = 0; i < N_WEEKS; i++)
    {
        CConfigTable& tb1 = tb[i*2];
        CConfigTable& tb2 = tb[i*2+1];
        for(int j = 0; j < N_MIN_TSECT; j++)
        {
            exchangeTimeSection(xchg, tb1[j], config.RecordPeriodSet[i][j].Tsect);
            xchg.exchange(tb2, j, config.RecordPeriodSet[i][j].StateMask.l,(uint)0);
        }

    }
}

template<> void exchangeTable<CONFIG_NET_NTP>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_NTP& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, TRUE);   //
    exchangeServer(xchg, table["NTPServer"], config.Server);
    xchg.exchange(table["NTPServer"], "Port", config.Server.Port, 0, 65535, 123);
    xchg.exchange(table["NTPServer"], "ServerName", config.Server.ServerName, "time.windows.com");
    xchg.exchange(table, "UpdatePeriod", config.UpdatePeriod, 1,60*24, 30);//
    xchg.exchange(table, "TimeZone", config.TimeZone, -120,120, 80);//中国加8  0 33 13
	//xchg.exchange(table, "DaylightEnable", config.DaylightEnable, FALSE);
	xchg.exchange(table, "DaylightStartTime", config.DaylightTime.StartTime , 0,(uint)(-1), 0);
	xchg.exchange(table, "DaylightEndTime"	, config.DaylightTime.EndTime	, 0,(uint)(-1), 0);
	xchg.exchange(table, "DaylightEnable"	, config.DaylightTime.Enable	, FALSE);
	xchg.exchange(table, "TimeZoneExtData"	, config.TimeZoneExtData		, "");
}

template<> void exchangeTable<CONFIG_NET_EMAIL>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_EMAIL& config, int index, int app)
{
    int i;
    xchg.exchange(table, "Enable", config.Enable, FALSE);
    xchg.exchange(table, "UseSSL", config.bUseSSL, FALSE);
    exchangeServer(xchg, table["MailServer"], config.Server);
    xchg.exchange(table["Mailport"], "Port",  config.Server.Port, 0, 65535, 25);
    xchg.exchange(table["Mailip"], "ip",  config.Server.ip.l, uint(Str2Ip("10.6.0.3")));

    std::string mail_server;
    std::string revAddr;

    mail_server = "MailServer";
    revAddr = "none@xxx.com";
    xchg.exchange(table["MailServer"], "ServerName", config.Server.ServerName, mail_server.c_str());
    xchg.exchange(table["MailServer"], "UserName", config.Server.UserName, revAddr.c_str());
    xchg.exchange(table["MailServer"], "Password", config.Server.Password, "\0");
    xchg.exchange(table, "Title", config.Title, "ALARM");    // by ilena zhou 2010-05-13 统一所有设备的邮件名称
    xchg.exchange(table, "SendAddr", config.SendAddr, revAddr.c_str());
    CConfigTable& tb1 = table["Recievers"];
    for(i = 0; i < MAX_EMAIL_RECIEVERS; i++)
    {
        xchg.exchange( tb1, i, config.Recievers[i], revAddr.c_str() );
    }
    CConfigTable& tb2 = table["Schedule"];
    for(i = 0; i < N_MIN_TSECT; i++)
    {
        exchangeTimeSection(xchg, tb2[i], config.Schedule[i]);
    }
    xchg.exchange(table, "AccePic", config.dwAccePic, 0x00000000);    
}
template<> void exchangeTable<CONFIG_NET_SNIFFER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_SNIFFER& config, int index, int app)
{
    xchg.exchange(table, "SrcIP", config.SrcIP.l, (uint)0);
    xchg.exchange(table, "SrcPort", config.SrcPort,0,65535, 666);
    xchg.exchange(table, "DestIP", config.DestIP.l, (uint)0);
    xchg.exchange(table, "DestPort", config.DestPort, 0, 65535,777);
}

template<> void exchangeTable<CONFIG_REG_SERVER>(CConfigExchange& xchg, CConfigTable& table, CONFIG_REG_SERVER& config, int index, int app)
{
    std::string device_id;

    device_id = "default";
    xchg.exchange(table,"enable",config.enable,    (ulong)0);
    xchg.exchange(table,"DeviceID", config.deviceID, device_id.c_str() );
    xchg.exchange(table,"iConnectType",config.iConnectType,    (ulong)0);
    for(int ii=0;ii<SERVER_NUM;ii++)
    {
        xchg.exchange(table["Server"][ii],"strUsr",config.serverInfo[ii].strUsr,    "admin");
        xchg.exchange(table["Server"][ii],"strPwd",config.serverInfo[ii].strPwd,    "123456");
        xchg.exchange(table["Server"][ii],"Ip",config.serverInfo[ii].ServerIp,    "0.0.0.0");
        xchg.exchange(table["Server"][ii], "Port", config.serverInfo[ii].ServerPort,    0,    65535,     8000);
    }
}

template<> void exchangeTable < CONFIG_DHCP_CFG >(CConfigExchange & xchg, CConfigTable & table, CONFIG_DHCP_CFG & config, int index, int app)
{
    xchg.exchange(table,"Enable",config.enable,0);
    if(index == 0)
    {
        xchg.exchange(table,"Interface",config.ifName,"eth0");
    }
    else if(index == 1)
    {
        xchg.exchange(table,"Interface",config.ifName,"eth1");
    }
}

template<> void exchangeTable < CONFIG_NET_UPNP >(CConfigExchange & xchg, CConfigTable & table, CONFIG_NET_UPNP & config, int index, int app)
{
    xchg.exchange(table,"Enable",config.Enable,0);
    xchg.exchange(table,"WebPort",config.WebPort,1,65535,80);
    xchg.exchange(table,"TCPPort",config.TCPPort,1,65535,9008);
}

template<> void exchangeTable<SNET_PRESET_INFOR>(CConfigExchange& xchg, CConfigTable& table, SNET_PRESET_INFOR& config, int index, int app)
{ 
    CConfigTable& tbPresetID = table["PresetID"];
    CConfigTable& tbPresetName = table["PresetName"];
    CConfigTable& tbSpeed = table["Speed"];
    CConfigTable& tbDWellTime = table["DWellTime"];
    for(int iIndex = 0; iIndex < PTZ_PRESETNUM; iIndex++)
    {
        xchg.exchange(tbPresetID, iIndex, config.strPresetInfor[iIndex].ucPresetId, 0, PTZ_PRESETNUM, 0);
        xchg.exchange(tbPresetName, iIndex, config.strPresetInfor[iIndex].cPresetName, "\0");
        xchg.exchange(tbSpeed, iIndex, config.strPresetInfor[iIndex].ucSpeed, 0, 100, 0);
        xchg.exchange(tbDWellTime, iIndex, config.strPresetInfor[iIndex].ucDWellTime, 0, 255, 0);
    }
}

void TourInforExchange(CConfigExchange& xchg, CConfigTable& table, NET_TOUR_INFOR& config) 
{     
    xchg.exchange(table, "TourIndex", config.ucTourIndex, 0, PTZ_CHANNELS, 0);
    xchg.exchange(table, "DwellTime", config.ucDwellTime, 0, 255, 0);
    xchg.exchange(table, "PresetCnt", config.ucPresetCnt, 0, PTZ_PRESET_IN_TOUR_NUM, 0);

    CConfigTable& tbPresetNum = table["PresetNum"];
    for(int iIndex = 0; iIndex < PTZ_PRESET_IN_TOUR_NUM; iIndex++)
    {
        xchg.exchange(tbPresetNum, iIndex, config.ucPresetNum[iIndex], 0, PTZ_PRESETNUM, 0);    
    }
}

template<> void exchangeTable<SNET_TOUR_INFOR>(CConfigExchange& xchg, CConfigTable& table, SNET_TOUR_INFOR& config, int index, int app)

{
    CConfigTable &tbTourInfor = table["TourInfor"];
    for(int iIndex = 0; iIndex < PTZ_CHANNELS; iIndex++)
    {
        TourInforExchange(xchg, tbTourInfor[iIndex], config.strTourInfor[iIndex]);
    }
}

template<> void exchangeTable<CONFIG_PTZREGRESS>(CConfigExchange& xchg, CConfigTable& table, CONFIG_PTZREGRESS& config, int index, int app)
{
    xchg.exchange(table, "EnableRegress", config.usEnableRegress, 0, 1, 0);
    xchg.exchange(table, "RegressTime", config.usRegressTime, 1, 65535, 30);
    xchg.exchange(table, "PresetIndex", config.usPresetIndex, 1, PTZ_PRESETNUM, 1);
    xchg.exchange(table, "PresetName", config.szPresetName, "\0");
}
#ifdef HMSOTA
template<> void exchangeTable<CONFIG_NET_HMS>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_HMS& config, int index, int app)
{
    xchg.exchange(table, "Enable", config.Enable, TRUE);
    xchg.exchange(table, "ServerName", config.ServerName, "ssl://shms.cppluscloud.com:8883");
    xchg.exchange(table, "Port", config.Port, 0, 65535, 8883);
    xchg.exchange(table, "UserName", config.UserName, "rajesh");
    xchg.exchange(table, "Password", config.Password, "");//
}

template<> void exchangeTable<CONFIG_NET_OTA>(CConfigExchange& xchg, CConfigTable& table, CONFIG_NET_OTA& config, int index, int app)
{
    xchg.exchange(table, "Mode", config.Mode, TRUE);
	xchg.exchange(table, "Interval", config.Interval, 0, 65535, 6);
	xchg.exchange(table, "IgnoreVersion", config.IgnoreVersion, "xxxxxxxxxxxxxxxxxxxx.bin");
}

#endif

